.class public final Lcom/sec/android/app/shealth/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final About_healthy_steps:I = 0x7f090f1f

.field public static final Abouthealthysteps:I = 0x7f090b6f

.field public static final Add_picture:I = 0x7f090934

.field public static final EMMMdyyyy:I = 0x7f0901a4

.field public static final EdMMMyyyy:I = 0x7f0901a3

.field public static final EddMMMyyyy:I = 0x7f0901a6

.field public static final I_do_not_agree:I = 0x7f0908c2

.field public static final MMM_dd_yyyy:I = 0x7f090715

.field public static final MMM_dd_yyyy_HH_mm:I = 0x7f090718

.field public static final MMM_dd_yyyy_hh_mm_a:I = 0x7f09071b

.field public static final Pictures:I = 0x7f09007f

.field public static final Reference:I = 0x7f0910ad

.field public static final Sleep_activity_tracker:I = 0x7f090d82

.field public static final abnormal_goal_value_message:I = 0x7f090a6a

.field public static final abnormal_height_value_message:I = 0x7f090845

.field public static final abnormal_weight_value_message:I = 0x7f090846

.field public static final about_bmi:I = 0x7f090847

.field public static final about_bmr:I = 0x7f090855

.field public static final about_bmr_msg2:I = 0x7f09085c

.field public static final about_caoching_guide_2:I = 0x7f090af0

.field public static final about_caoching_guide_3:I = 0x7f090af1

.field public static final about_coaching:I = 0x7f090aed

.field public static final about_coaching_guide_1:I = 0x7f090aef

.field public static final about_coaching_guide_4:I = 0x7f090af2

.field public static final about_s_health:I = 0x7f0907ed

.field public static final accept:I = 0x7f090058

.field public static final accepted_range:I = 0x7f0909b8

.field public static final accepted_range_of_step:I = 0x7f090b61

.field public static final access_your_location:I = 0x7f090ac8

.field public static final accessary:I = 0x7f0907b4

.field public static final accessory_error:I = 0x7f090bae

.field public static final account_address:I = 0x7f090875

.field public static final accounts:I = 0x7f0907b9

.field public static final accounts_hint:I = 0x7f0907ba

.field public static final achievement:I = 0x7f090899

.field public static final action_not_defined_for_this_item:I = 0x7f09018b

.field public static final action_reset:I = 0x7f090b64

.field public static final action_settings:I = 0x7f09001a

.field public static final active_since:I = 0x7f0908e0

.field public static final activity_level:I = 0x7f090819

.field public static final activity_summary:I = 0x7f0908fa

.field public static final activity_time:I = 0x7f090ba0

.field public static final add:I = 0x7f09003f

.field public static final add_anothr_field:I = 0x7f09093c

.field public static final add_food_info:I = 0x7f090930

.field public static final add_foods:I = 0x7f090981

.field public static final add_memo:I = 0x7f090128

.field public static final add_my_food:I = 0x7f090942

.field public static final add_photo:I = 0x7f090933

.field public static final add_to_my_food:I = 0x7f09093a

.field public static final added_to_favorites:I = 0x7f0909ba

.field public static final added_to_my_food:I = 0x7f09093b

.field public static final additional_law_text_kor:I = 0x7f0905c1

.field public static final additional_law_title_kor:I = 0x7f0905c0

.field public static final adidas_device:I = 0x7f090e17

.field public static final adidas_device_hrm:I = 0x7f090e18

.field public static final adidas_device_link:I = 0x7f090e01

.field public static final after_meal:I = 0x7f0901b0

.field public static final after_meal_short:I = 0x7f0901b1

.field public static final after_workout:I = 0x7f0906e6

.field public static final ag_goal_50_achieved:I = 0x7f090a81

.field public static final ag_goal_90_achieved:I = 0x7f090a82

.field public static final ag_new_goal:I = 0x7f090a86

.field public static final ag_workout_ended:I = 0x7f090a85

.field public static final ag_workout_paused:I = 0x7f090a84

.field public static final ag_workout_restarted:I = 0x7f090a83

.field public static final ag_workout_started:I = 0x7f090a87

.field public static final ag_workout_started_with_hrm:I = 0x7f090a88

.field public static final age:I = 0x7f090b7c

.field public static final age_grp:I = 0x7f090f81

.field public static final age_grp_1:I = 0x7f090f84

.field public static final age_grp_10:I = 0x7f090f8d

.field public static final age_grp_11:I = 0x7f090f8e

.field public static final age_grp_12:I = 0x7f090f8f

.field public static final age_grp_2:I = 0x7f090f85

.field public static final age_grp_3:I = 0x7f090f86

.field public static final age_grp_4:I = 0x7f090f87

.field public static final age_grp_5:I = 0x7f090f88

.field public static final age_grp_6:I = 0x7f090f89

.field public static final age_grp_7:I = 0x7f090f8a

.field public static final age_grp_8:I = 0x7f090f8b

.field public static final age_grp_9:I = 0x7f090f8c

.field public static final agree:I = 0x7f0908c0

.field public static final alert:I = 0x7f0900d8

.field public static final alert_interval:I = 0x7f0907c2

.field public static final all:I = 0x7f090076

.field public static final all_changes_discard:I = 0x7f090083

.field public static final all_data_will_be_deleted_msg:I = 0x7f090d54

.field public static final all_food:I = 0x7f090945

.field public static final allow_app_permission:I = 0x7f090ac6

.field public static final alphabet_plus:I = 0x7f0905d4

.field public static final alphabet_x:I = 0x7f090914

.field public static final already_connected_toast:I = 0x7f0900ab

.field public static final already_exists:I = 0x7f090943

.field public static final am:I = 0x7f090181

.field public static final am_upper:I = 0x7f0900e6

.field public static final amount_per_serving:I = 0x7f090970

.field public static final and_324_scale_text_1:I = 0x7f0902d5

.field public static final and_324_scale_text_2:I = 0x7f0902d6

.field public static final and_851_bp_text_1:I = 0x7f0902d7

.field public static final and_851_bp_text_2:I = 0x7f0902d8

.field public static final and_scale_1:I = 0x7f091143

.field public static final and_scale_2:I = 0x7f091144

.field public static final and_scale_3:I = 0x7f091145

.field public static final and_scale_4:I = 0x7f091146

.field public static final and_ua851_bp_text_1:I = 0x7f0902e9

.field public static final and_ua851_bp_text_2:I = 0x7f0902ea

.field public static final and_uc324_scale_text_1:I = 0x7f0902e6

.field public static final and_uc324_scale_text_2:I = 0x7f0902e7

.field public static final and_uc324_scale_text_3:I = 0x7f0902e8

.field public static final aoubt_bmi_msg1:I = 0x7f090848

.field public static final aoubt_bmi_msg2:I = 0x7f09084b

.field public static final aoubt_bmr_msg1:I = 0x7f090858

.field public static final app_name:I = 0x7f090019

.field public static final app_name_upper:I = 0x7f090913

.field public static final app_summary:I = 0x7f090916

.field public static final appwidget_goal_steps:I = 0x7f090b68

.field public static final appwidget_walk_initial_dashboard:I = 0x7f090ba7

.field public static final appwidget_walk_initial_text:I = 0x7f090ba8

.field public static final appwidget_walkingmate_initial_text:I = 0x7f090ba9

.field public static final april:I = 0x7f0900ff

.field public static final april_short:I = 0x7f09010b

.field public static final ascent_descent:I = 0x7f090a59

.field public static final ask_pair_hrm_q:I = 0x7f090aca

.field public static final attempt_to_connect_wlan:I = 0x7f0907a3

.field public static final audio_guide:I = 0x7f090a79

.field public static final audio_guide_not_supported:I = 0x7f090a35

.field public static final audio_guide_not_supported_for_arabic:I = 0x7f090a34

.field public static final audio_guide_not_supported_for_this_language:I = 0x7f09112e

.field public static final audio_guide_off:I = 0x7f090a78

.field public static final audio_guide_on:I = 0x7f090a77

.field public static final audio_guide_settings:I = 0x7f0906ea

.field public static final august:I = 0x7f090103

.field public static final august_short:I = 0x7f09010f

.field public static final auth_client_needs_enabling_title:I = 0x7f090014

.field public static final auth_client_needs_installation_title:I = 0x7f090015

.field public static final auth_client_needs_update_title:I = 0x7f090016

.field public static final auth_client_play_services_err_notification_msg:I = 0x7f090017

.field public static final auth_client_requested_by_msg:I = 0x7f090018

.field public static final auth_client_using_bad_version_title:I = 0x7f090013

.field public static final auto_backup:I = 0x7f090274

.field public static final auto_backup_activity_header:I = 0x7f090d21

.field public static final auto_backup_interval:I = 0x7f090d22

.field public static final auto_backup_off_reminder_msg:I = 0x7f090808

.field public static final auto_set_maximum_hr_guide:I = 0x7f090add

.field public static final avarage_heart_rate_outside_range:I = 0x7f090f92

.field public static final avarage_heart_rate_range:I = 0x7f090f7e

.field public static final average:I = 0x7f09006d

.field public static final average_short:I = 0x7f09006e

.field public static final average_speed:I = 0x7f090b55

.field public static final avg_burnt_calories:I = 0x7f0908fe

.field public static final avg_intake_calories:I = 0x7f0908fd

.field public static final avg_more:I = 0x7f090f80

.field public static final avg_pace:I = 0x7f090a53

.field public static final avg_range:I = 0x7f090f82

.field public static final avg_speed:I = 0x7f090a51

.field public static final avg_steps:I = 0x7f0908fc

.field public static final avg_week_kcal:I = 0x7f090904

.field public static final avg_week_steps:I = 0x7f090903

.field public static final backup:I = 0x7f090879

.field public static final backup_activity_header:I = 0x7f090d1e

.field public static final backup_activity_header_2:I = 0x7f090d1f

.field public static final backup_agreement_text:I = 0x7f090882

.field public static final backup_and_restore:I = 0x7f090874

.field public static final backup_and_restore_activity_text:I = 0x7f090872

.field public static final backup_before_restore:I = 0x7f090d44

.field public static final backup_complete:I = 0x7f0907fc

.field public static final backup_data:I = 0x7f0908f8

.field public static final backup_failed:I = 0x7f090800

.field public static final backup_message:I = 0x7f09087c

.field public static final backup_now:I = 0x7f090d20

.field public static final backup_restoration_in_progress:I = 0x7f0907f5

.field public static final backup_restore_progress:I = 0x7f090d40

.field public static final badges:I = 0x7f090918

.field public static final barcode_cannot_find_the_food_information_message:I = 0x7f0909b3

.field public static final barcode_failed:I = 0x7f0909b7

.field public static final barcode_search:I = 0x7f0909b6

.field public static final basic_information:I = 0x7f090815

.field public static final beat_my_previous_record:I = 0x7f0905fd

.field public static final beat_my_previous_record_hint:I = 0x7f0905fa

.field public static final best_distance:I = 0x7f090b76

.field public static final best_record:I = 0x7f090a55

.field public static final best_step:I = 0x7f090ba2

.field public static final best_steps:I = 0x7f090b8f

.field public static final bg_infopia:I = 0x7f09114c

.field public static final bg_input_field_image_button_back:I = 0x7f0901a7

.field public static final bg_input_field_image_button_next:I = 0x7f0901a8

.field public static final bg_isens:I = 0x7f09114d

.field public static final bg_lifescan:I = 0x7f0902eb

.field public static final birth_date_alert_message:I = 0x7f090119

.field public static final birthday:I = 0x7f090816

.field public static final blank_dash_board:I = 0x7f090dfb

.field public static final blank_space:I = 0x7f090277

.field public static final blood_glucose:I = 0x7f0901ae

.field public static final blood_glucose_connectivity_text:I = 0x7f090295

.field public static final blood_glucose_meter:I = 0x7f09018e

.field public static final blood_glucose_test_strip:I = 0x7f090e0f

.field public static final blood_pressure:I = 0x7f0901b4

.field public static final blood_pressure_connectivity_text:I = 0x7f090294

.field public static final blood_pressure_log_filter_all:I = 0x7f0910c7

.field public static final blood_pressure_log_filter_outside:I = 0x7f0910c9

.field public static final blood_pressure_log_filter_within:I = 0x7f0910c8

.field public static final blood_pressure_monitor:I = 0x7f09018f

.field public static final bloodglucose:I = 0x7f09026b

.field public static final bloodglucose_unit:I = 0x7f09026d

.field public static final bloodpressure:I = 0x7f09026c

.field public static final bloodpressure_unit:I = 0x7f090270

.field public static final blooe_pressure:I = 0x7f0910c3

.field public static final bluetooth_not_supported:I = 0x7f0902a3

.field public static final bluetooth_turnoff_toast:I = 0x7f09008e

.field public static final bmi_classification:I = 0x7f09084c

.field public static final bmi_kg_m:I = 0x7f09080a

.field public static final bmi_kg_m_total_unit:I = 0x7f09080c

.field public static final bmi_kg_m_unit:I = 0x7f09080b

.field public static final bmi_normal:I = 0x7f09084f

.field public static final bmi_normal_range:I = 0x7f090850

.field public static final bmi_notice:I = 0x7f090849

.field public static final bmi_notice3:I = 0x7f09084a

.field public static final bmi_obese:I = 0x7f090853

.field public static final bmi_obese_range:I = 0x7f090854

.field public static final bmi_overweight:I = 0x7f090851

.field public static final bmi_overweight_range:I = 0x7f090852

.field public static final bmi_underweight:I = 0x7f09084d

.field public static final bmi_underweight_range:I = 0x7f09084e

.field public static final bmr_exercise_msg:I = 0x7f09085f

.field public static final bmr_from:I = 0x7f0908d2

.field public static final bmr_introduce:I = 0x7f0908cd

.field public static final bmr_men:I = 0x7f0908ce

.field public static final bmr_men_calculation:I = 0x7f0908cf

.field public static final bmr_notice:I = 0x7f0908d3

.field public static final bmr_women:I = 0x7f0908d0

.field public static final bmr_women_calculation:I = 0x7f0908d1

.field public static final body_accepted_range_cm:I = 0x7f090843

.field public static final body_accepted_range_ftinch:I = 0x7f090840

.field public static final body_accepted_range_kg:I = 0x7f090841

.field public static final body_accepted_range_lb:I = 0x7f090844

.field public static final body_information:I = 0x7f090818

.field public static final body_information_content:I = 0x7f09083a

.field public static final body_range:I = 0x7f09083b

.field public static final body_range_cm:I = 0x7f09083c

.field public static final body_range_etc:I = 0x7f090842

.field public static final body_range_ftinch:I = 0x7f09083d

.field public static final body_range_kg:I = 0x7f09083e

.field public static final body_range_lb:I = 0x7f09083f

.field public static final body_temp_acceptable_range:I = 0x7f0910bb

.field public static final body_temp_oor_dialog_text:I = 0x7f0910ba

.field public static final body_temparature:I = 0x7f09023f

.field public static final body_temperature:I = 0x7f090cf6

.field public static final bp_and_type1:I = 0x7f091149

.field public static final bp_and_type2:I = 0x7f09114a

.field public static final bp_chart_inform_area_pressure_color:I = 0x7f0910cd

.field public static final bp_data_received_toast:I = 0x7f0902a8

.field public static final bp_delete_info:I = 0x7f0910c4

.field public static final bp_heart_rate:I = 0x7f0910cc

.field public static final bp_help:I = 0x7f0910d3

.field public static final bp_input_activity_alert_dialog_message:I = 0x7f0910ce

.field public static final bp_input_activity_alert_dialog_message_integer_range:I = 0x7f0910cf

.field public static final bp_no_data:I = 0x7f0910d5

.field public static final bp_nodata_diastolic:I = 0x7f0910c1

.field public static final bp_nodata_pulse:I = 0x7f0910c2

.field public static final bp_nodata_systolic:I = 0x7f0910c0

.field public static final bp_normal:I = 0x7f0910d4

.field public static final bp_omron:I = 0x7f09114b

.field public static final bp_outside_normal_range:I = 0x7f0910d6

.field public static final bp_reset_daily_data:I = 0x7f0910d0

.field public static final bp_settings:I = 0x7f0910d2

.field public static final bp_share_via:I = 0x7f0910d1

.field public static final bp_update:I = 0x7f0910ca

.field public static final bpm:I = 0x7f0900d2

.field public static final bpm_upper:I = 0x7f09080d

.field public static final bracket_bpm:I = 0x7f090cea

.field public static final bracket_fitness:I = 0x7f090ceb

.field public static final bracket_kcal:I = 0x7f090ce8

.field public static final bracket_km_h:I = 0x7f090cec

.field public static final bracket_mi_h:I = 0x7f090813

.field public static final bracket_min_km:I = 0x7f090814

.field public static final bracket_min_mi:I = 0x7f090812

.field public static final bracket_steps:I = 0x7f090ced

.field public static final bracket_upper_bpm:I = 0x7f090ce9

.field public static final breakfast:I = 0x7f090920

.field public static final bt_accepted_range:I = 0x7f0910e4

.field public static final bt_and_pressure_1:I = 0x7f0902b9

.field public static final bt_and_pressure_2:I = 0x7f0902ba

.field public static final bt_and_pressure_3:I = 0x7f0902bb

.field public static final bt_and_pressure_4:I = 0x7f0902bc

.field public static final bt_and_scale_1:I = 0x7f0902bd

.field public static final bt_and_scale_2:I = 0x7f0902be

.field public static final bt_and_scale_3:I = 0x7f0902bf

.field public static final bt_and_scale_4:I = 0x7f0902c0

.field public static final bt_delete_item_dialog_text:I = 0x7f0910e3

.field public static final bt_first_measure_warning_1:I = 0x7f0910d9

.field public static final bt_first_measure_warning_2:I = 0x7f0910da

.field public static final bt_first_measure_warning_3:I = 0x7f0910db

.field public static final bt_info_avg:I = 0x7f0910e2

.field public static final bt_information_first:I = 0x7f0910dc

.field public static final bt_information_second:I = 0x7f0910dd

.field public static final bt_information_third:I = 0x7f0910de

.field public static final bt_omron_pressure_1:I = 0x7f0902b5

.field public static final bt_omron_pressure_2:I = 0x7f0902b8

.field public static final bt_omron_pressure_3:I = 0x7f0902b6

.field public static final bt_omron_weight_1:I = 0x7f0902b7

.field public static final bt_popup_receiving:I = 0x7f0900af

.field public static final bt_ready:I = 0x7f0910df

.field public static final bt_ready_message:I = 0x7f0910e0

.field public static final btm_back_key_finish:I = 0x7f0910d8

.field public static final btm_discard_reading:I = 0x7f0910e1

.field public static final btn_switch_type_chart:I = 0x7f090a47

.field public static final btn_use_health_account:I = 0x7f090d39

.field public static final burnt_calories:I = 0x7f090a19

.field public static final cadence:I = 0x7f090680

.field public static final calcium:I = 0x7f090969

.field public static final calendar:I = 0x7f090069

.field public static final calendar_wrong_selected:I = 0x7f09060e

.field public static final calorie:I = 0x7f0900ba

.field public static final calorie_burn:I = 0x7f090954

.field public static final calorie_burn_german:I = 0x7f090788

.field public static final calorie_burnt_medal:I = 0x7f0908eb

.field public static final calorie_intake:I = 0x7f090953

.field public static final calorie_per_serving:I = 0x7f09098b

.field public static final calories:I = 0x7f0900bb

.field public static final calories_goal:I = 0x7f090a25

.field public static final calories_goal_hint:I = 0x7f090a26

.field public static final camera:I = 0x7f0900df

.field public static final can_not_enter_earlier_then_today_today_will_be_set_as_start_date:I = 0x7f090988

.field public static final cancel:I = 0x7f090048

.field public static final cannot_build_dialog_without_title_exception:I = 0x7f090197

.field public static final cannot_build_dialog_without_type_exception:I = 0x7f090198

.field public static final capital_carbohydrate:I = 0x7f090962

.field public static final carbohydrate:I = 0x7f0909ca

.field public static final card_view_default_cycling:I = 0x7f090b51

.field public static final card_view_default_hiking:I = 0x7f090b52

.field public static final card_view_default_running:I = 0x7f090b4f

.field public static final card_view_default_walking:I = 0x7f090b50

.field public static final care_giver:I = 0x7f090cf4

.field public static final category:I = 0x7f090086

.field public static final caution:I = 0x7f0900d7

.field public static final challenge:I = 0x7f090183

.field public static final challenges:I = 0x7f0908f3

.field public static final change_password:I = 0x7f09088b

.field public static final change_view_mode:I = 0x7f09021d

.field public static final chart:I = 0x7f090a1a

.field public static final chart_view:I = 0x7f090063

.field public static final check_for_updates:I = 0x7f0907ea

.field public static final check_the_input_range:I = 0x7f09098d

.field public static final check_the_input_range_float:I = 0x7f09098e

.field public static final checking_updates:I = 0x7f0908b5

.field public static final chinese_colon:I = 0x7f0900d5

.field public static final cholesterol:I = 0x7f090964

.field public static final cigna:I = 0x7f09002e

.field public static final cigna_app_name:I = 0x7f0902ef

.field public static final cigna_assessment_tracker_says:I = 0x7f0902f0

.field public static final cigna_awesome:I = 0x7f0902f1

.field public static final cigna_badge:I = 0x7f0902f2

.field public static final cigna_button2_set_goal:I = 0x7f090c8a

.field public static final cigna_button_accept_goal_abb:I = 0x7f090c7d

.field public static final cigna_button_accept_mission_abb:I = 0x7f090c8e

.field public static final cigna_button_assess_abb:I = 0x7f090c45

.field public static final cigna_button_get_started:I = 0x7f090c4b

.field public static final cigna_button_get_started_abb3:I = 0x7f090c49

.field public static final cigna_button_i_forgot:I = 0x7f090c48

.field public static final cigna_button_later:I = 0x7f090c89

.field public static final cigna_button_learn_more_abb:I = 0x7f090c4a

.field public static final cigna_button_new_mission_abb:I = 0x7f090c7b

.field public static final cigna_button_ok_abb3:I = 0x7f090c4d

.field public static final cigna_button_other_reason_abb:I = 0x7f090c4c

.field public static final cigna_button_reassess_abb:I = 0x7f090c52

.field public static final cigna_button_repeat_challenge_abb:I = 0x7f090c46

.field public static final cigna_button_resume_assessment_abb:I = 0x7f090c82

.field public static final cigna_button_set:I = 0x7f090c47

.field public static final cigna_button_set_different_goal_abb:I = 0x7f090c54

.field public static final cigna_button_today:I = 0x7f090c8b

.field public static final cigna_button_tomorrow_abb:I = 0x7f090c53

.field public static final cigna_button_too_busy:I = 0x7f090c4f

.field public static final cigna_button_too_easy:I = 0x7f090c50

.field public static final cigna_button_too_hard:I = 0x7f090c51

.field public static final cigna_button_try_again_abb:I = 0x7f090c8c

.field public static final cigna_button_view_new_scores_abb:I = 0x7f090c4e

.field public static final cigna_cancelled:I = 0x7f0902f3

.field public static final cigna_change_to_your_heigh_and_weight:I = 0x7f0902f4

.field public static final cigna_cinnecting_third_party_deivce_and_program_txt_ar_ae:I = 0x7f0904cf

.field public static final cigna_cinnecting_third_party_deivce_and_program_txt_au:I = 0x7f0904de

.field public static final cigna_cinnecting_third_party_deivce_and_program_txt_ca:I = 0x7f0904ed

.field public static final cigna_cinnecting_third_party_deivce_and_program_txt_de_at:I = 0x7f0904fc

.field public static final cigna_cinnecting_third_party_deivce_and_program_txt_de_ch:I = 0x7f09050b

.field public static final cigna_cinnecting_third_party_deivce_and_program_txt_en_ch:I = 0x7f09051a

.field public static final cigna_cinnecting_third_party_deivce_and_program_txt_en_fr:I = 0x7f090529

.field public static final cigna_cinnecting_third_party_deivce_and_program_txt_en_in:I = 0x7f090538

.field public static final cigna_cinnecting_third_party_deivce_and_program_txt_en_pa:I = 0x7f090547

.field public static final cigna_cinnecting_third_party_deivce_and_program_txt_en_ph:I = 0x7f090375

.field public static final cigna_cinnecting_third_party_deivce_and_program_txt_en_us:I = 0x7f090384

.field public static final cigna_cinnecting_third_party_deivce_and_program_txt_es:I = 0x7f090394

.field public static final cigna_cinnecting_third_party_deivce_and_program_txt_es_pa:I = 0x7f090558

.field public static final cigna_cinnecting_third_party_deivce_and_program_txt_es_us:I = 0x7f0903a3

.field public static final cigna_cinnecting_third_party_deivce_and_program_txt_fr:I = 0x7f0903b2

.field public static final cigna_cinnecting_third_party_deivce_and_program_txt_fr_ca:I = 0x7f0903c1

.field public static final cigna_cinnecting_third_party_deivce_and_program_txt_fr_ch:I = 0x7f090567

.field public static final cigna_cinnecting_third_party_deivce_and_program_txt_gb:I = 0x7f090366

.field public static final cigna_cinnecting_third_party_deivce_and_program_txt_hi:I = 0x7f0903d0

.field public static final cigna_cinnecting_third_party_deivce_and_program_txt_in:I = 0x7f0903df

.field public static final cigna_cinnecting_third_party_deivce_and_program_txt_it_it:I = 0x7f0903ee

.field public static final cigna_cinnecting_third_party_deivce_and_program_txt_iw:I = 0x7f0903fd

.field public static final cigna_cinnecting_third_party_deivce_and_program_txt_jp:I = 0x7f09040c

.field public static final cigna_cinnecting_third_party_deivce_and_program_txt_ko_kr:I = 0x7f09041b

.field public static final cigna_cinnecting_third_party_deivce_and_program_txt_mx:I = 0x7f090576

.field public static final cigna_cinnecting_third_party_deivce_and_program_txt_my:I = 0x7f09042a

.field public static final cigna_cinnecting_third_party_deivce_and_program_txt_nl:I = 0x7f090439

.field public static final cigna_cinnecting_third_party_deivce_and_program_txt_no:I = 0x7f090588

.field public static final cigna_cinnecting_third_party_deivce_and_program_txt_nz:I = 0x7f090597

.field public static final cigna_cinnecting_third_party_deivce_and_program_txt_pl:I = 0x7f090448

.field public static final cigna_cinnecting_third_party_deivce_and_program_txt_pt_br:I = 0x7f090457

.field public static final cigna_cinnecting_third_party_deivce_and_program_txt_ru:I = 0x7f090466

.field public static final cigna_cinnecting_third_party_deivce_and_program_txt_sg:I = 0x7f0905a6

.field public static final cigna_cinnecting_third_party_deivce_and_program_txt_sv:I = 0x7f090475

.field public static final cigna_cinnecting_third_party_deivce_and_program_txt_th:I = 0x7f090484

.field public static final cigna_cinnecting_third_party_deivce_and_program_txt_tr:I = 0x7f090493

.field public static final cigna_cinnecting_third_party_deivce_and_program_txt_za:I = 0x7f0905b5

.field public static final cigna_cinnecting_third_party_deivce_and_program_txt_zh_cn:I = 0x7f0904a2

.field public static final cigna_cinnecting_third_party_deivce_and_program_txt_zh_hk:I = 0x7f0904b1

.field public static final cigna_cinnecting_third_party_deivce_and_program_txt_zh_tw:I = 0x7f0904c0

.field public static final cigna_cinnecting_third_party_device_and_program_title_ar_ae:I = 0x7f0904ce

.field public static final cigna_cinnecting_third_party_device_and_program_title_au:I = 0x7f0904dd

.field public static final cigna_cinnecting_third_party_device_and_program_title_ca:I = 0x7f0904ec

.field public static final cigna_cinnecting_third_party_device_and_program_title_de_at:I = 0x7f0904fb

.field public static final cigna_cinnecting_third_party_device_and_program_title_de_ch:I = 0x7f09050a

.field public static final cigna_cinnecting_third_party_device_and_program_title_en_ch:I = 0x7f090519

.field public static final cigna_cinnecting_third_party_device_and_program_title_en_fr:I = 0x7f090528

.field public static final cigna_cinnecting_third_party_device_and_program_title_en_in:I = 0x7f090537

.field public static final cigna_cinnecting_third_party_device_and_program_title_en_pa:I = 0x7f090546

.field public static final cigna_cinnecting_third_party_device_and_program_title_en_ph:I = 0x7f090374

.field public static final cigna_cinnecting_third_party_device_and_program_title_en_us:I = 0x7f090383

.field public static final cigna_cinnecting_third_party_device_and_program_title_es:I = 0x7f090393

.field public static final cigna_cinnecting_third_party_device_and_program_title_es_pa:I = 0x7f090557

.field public static final cigna_cinnecting_third_party_device_and_program_title_es_us:I = 0x7f0903a2

.field public static final cigna_cinnecting_third_party_device_and_program_title_fr:I = 0x7f0903b1

.field public static final cigna_cinnecting_third_party_device_and_program_title_fr_ca:I = 0x7f0903c0

.field public static final cigna_cinnecting_third_party_device_and_program_title_fr_ch:I = 0x7f090566

.field public static final cigna_cinnecting_third_party_device_and_program_title_gb:I = 0x7f090365

.field public static final cigna_cinnecting_third_party_device_and_program_title_hi:I = 0x7f0903cf

.field public static final cigna_cinnecting_third_party_device_and_program_title_in:I = 0x7f0903de

.field public static final cigna_cinnecting_third_party_device_and_program_title_it_it:I = 0x7f0903ed

.field public static final cigna_cinnecting_third_party_device_and_program_title_iw:I = 0x7f0903fc

.field public static final cigna_cinnecting_third_party_device_and_program_title_jp:I = 0x7f09040b

.field public static final cigna_cinnecting_third_party_device_and_program_title_ko_kr:I = 0x7f09041a

.field public static final cigna_cinnecting_third_party_device_and_program_title_mx:I = 0x7f090575

.field public static final cigna_cinnecting_third_party_device_and_program_title_my:I = 0x7f090429

.field public static final cigna_cinnecting_third_party_device_and_program_title_nl:I = 0x7f090438

.field public static final cigna_cinnecting_third_party_device_and_program_title_no:I = 0x7f090587

.field public static final cigna_cinnecting_third_party_device_and_program_title_nz:I = 0x7f090596

.field public static final cigna_cinnecting_third_party_device_and_program_title_pl:I = 0x7f090447

.field public static final cigna_cinnecting_third_party_device_and_program_title_pt_br:I = 0x7f090456

.field public static final cigna_cinnecting_third_party_device_and_program_title_ru:I = 0x7f090465

.field public static final cigna_cinnecting_third_party_device_and_program_title_sg:I = 0x7f0905a5

.field public static final cigna_cinnecting_third_party_device_and_program_title_sv:I = 0x7f090474

.field public static final cigna_cinnecting_third_party_device_and_program_title_th:I = 0x7f090483

.field public static final cigna_cinnecting_third_party_device_and_program_title_tr:I = 0x7f090492

.field public static final cigna_cinnecting_third_party_device_and_program_title_za:I = 0x7f0905b4

.field public static final cigna_cinnecting_third_party_device_and_program_title_zh_cn:I = 0x7f0904a1

.field public static final cigna_cinnecting_third_party_device_and_program_title_zh_hk:I = 0x7f0904b0

.field public static final cigna_cinnecting_third_party_device_and_program_title_zh_tw:I = 0x7f0904bf

.field public static final cigna_cinnecting_third_partyivce_and_program_txt_de:I = 0x7f090357

.field public static final cigna_cinnecting_third_partyvice_and_program_title_de:I = 0x7f090356

.field public static final cigna_coach_guidance:I = 0x7f0902f5

.field public static final cigna_coach_help_step1:I = 0x7f0902f6

.field public static final cigna_coach_help_step1_description:I = 0x7f0902f7

.field public static final cigna_coach_help_step1_name:I = 0x7f0902f8

.field public static final cigna_coach_help_step2_description:I = 0x7f0902f9

.field public static final cigna_coach_help_step2_name:I = 0x7f0902fa

.field public static final cigna_coach_help_step3_description:I = 0x7f0902fb

.field public static final cigna_coach_help_step3_name:I = 0x7f0902fc

.field public static final cigna_coach_help_step4_description:I = 0x7f0902fd

.field public static final cigna_coach_help_step4_name:I = 0x7f0902fe

.field public static final cigna_coach_total_help:I = 0x7f090202

.field public static final cigna_current_goal_empty_message:I = 0x7f0902ff

.field public static final cigna_current_mission_detail_header_details:I = 0x7f090300

.field public static final cigna_current_mission_detail_header_tips:I = 0x7f090301

.field public static final cigna_current_mission_detail_header_xdayleft:I = 0x7f090302

.field public static final cigna_current_mission_detail_header_xdaysleft:I = 0x7f090303

.field public static final cigna_current_mission_detail_tracking_header:I = 0x7f090304

.field public static final cigna_disclaimer_of_warranty_title_ar_ae:I = 0x7f0904d4

.field public static final cigna_disclaimer_of_warranty_title_au:I = 0x7f0904e3

.field public static final cigna_disclaimer_of_warranty_title_ca:I = 0x7f0904f2

.field public static final cigna_disclaimer_of_warranty_title_de:I = 0x7f09035c

.field public static final cigna_disclaimer_of_warranty_title_de_at:I = 0x7f090501

.field public static final cigna_disclaimer_of_warranty_title_de_ch:I = 0x7f090510

.field public static final cigna_disclaimer_of_warranty_title_en_ch:I = 0x7f09051f

.field public static final cigna_disclaimer_of_warranty_title_en_fr:I = 0x7f09052e

.field public static final cigna_disclaimer_of_warranty_title_en_in:I = 0x7f09053d

.field public static final cigna_disclaimer_of_warranty_title_en_pa:I = 0x7f09054c

.field public static final cigna_disclaimer_of_warranty_title_en_ph:I = 0x7f09037a

.field public static final cigna_disclaimer_of_warranty_title_en_us:I = 0x7f090389

.field public static final cigna_disclaimer_of_warranty_title_es:I = 0x7f090399

.field public static final cigna_disclaimer_of_warranty_title_es_pa:I = 0x7f09055d

.field public static final cigna_disclaimer_of_warranty_title_es_us:I = 0x7f0903a8

.field public static final cigna_disclaimer_of_warranty_title_fr:I = 0x7f0903b7

.field public static final cigna_disclaimer_of_warranty_title_fr_ca:I = 0x7f0903c6

.field public static final cigna_disclaimer_of_warranty_title_fr_ch:I = 0x7f09056c

.field public static final cigna_disclaimer_of_warranty_title_gb:I = 0x7f09036b

.field public static final cigna_disclaimer_of_warranty_title_hi:I = 0x7f0903d5

.field public static final cigna_disclaimer_of_warranty_title_in:I = 0x7f0903e4

.field public static final cigna_disclaimer_of_warranty_title_it_it:I = 0x7f0903f3

.field public static final cigna_disclaimer_of_warranty_title_iw:I = 0x7f090402

.field public static final cigna_disclaimer_of_warranty_title_jp:I = 0x7f090411

.field public static final cigna_disclaimer_of_warranty_title_ko_kr:I = 0x7f090420

.field public static final cigna_disclaimer_of_warranty_title_mx:I = 0x7f09057b

.field public static final cigna_disclaimer_of_warranty_title_my:I = 0x7f09042f

.field public static final cigna_disclaimer_of_warranty_title_nl:I = 0x7f09043e

.field public static final cigna_disclaimer_of_warranty_title_no:I = 0x7f09058d

.field public static final cigna_disclaimer_of_warranty_title_nz:I = 0x7f09059c

.field public static final cigna_disclaimer_of_warranty_title_pl:I = 0x7f09044d

.field public static final cigna_disclaimer_of_warranty_title_pt_br:I = 0x7f09045c

.field public static final cigna_disclaimer_of_warranty_title_ru:I = 0x7f09046b

.field public static final cigna_disclaimer_of_warranty_title_sg:I = 0x7f0905ab

.field public static final cigna_disclaimer_of_warranty_title_sv:I = 0x7f09047a

.field public static final cigna_disclaimer_of_warranty_title_th:I = 0x7f090489

.field public static final cigna_disclaimer_of_warranty_title_tr:I = 0x7f090498

.field public static final cigna_disclaimer_of_warranty_title_za:I = 0x7f0905ba

.field public static final cigna_disclaimer_of_warranty_title_zh_cn:I = 0x7f0904a7

.field public static final cigna_disclaimer_of_warranty_title_zh_hk:I = 0x7f0904b6

.field public static final cigna_disclaimer_of_warranty_title_zh_tw:I = 0x7f0904c5

.field public static final cigna_disclaimer_of_warranty_txt_ar_ae:I = 0x7f0904d5

.field public static final cigna_disclaimer_of_warranty_txt_au:I = 0x7f0904e4

.field public static final cigna_disclaimer_of_warranty_txt_ca:I = 0x7f0904f3

.field public static final cigna_disclaimer_of_warranty_txt_de:I = 0x7f09035d

.field public static final cigna_disclaimer_of_warranty_txt_de_at:I = 0x7f090502

.field public static final cigna_disclaimer_of_warranty_txt_de_ch:I = 0x7f090511

.field public static final cigna_disclaimer_of_warranty_txt_en_ch:I = 0x7f090520

.field public static final cigna_disclaimer_of_warranty_txt_en_fr:I = 0x7f09052f

.field public static final cigna_disclaimer_of_warranty_txt_en_in:I = 0x7f09053e

.field public static final cigna_disclaimer_of_warranty_txt_en_pa:I = 0x7f09054d

.field public static final cigna_disclaimer_of_warranty_txt_en_ph:I = 0x7f09037b

.field public static final cigna_disclaimer_of_warranty_txt_en_us:I = 0x7f09038a

.field public static final cigna_disclaimer_of_warranty_txt_es:I = 0x7f09039a

.field public static final cigna_disclaimer_of_warranty_txt_es_pa:I = 0x7f09055e

.field public static final cigna_disclaimer_of_warranty_txt_es_us:I = 0x7f0903a9

.field public static final cigna_disclaimer_of_warranty_txt_fr:I = 0x7f0903b8

.field public static final cigna_disclaimer_of_warranty_txt_fr_ca:I = 0x7f0903c7

.field public static final cigna_disclaimer_of_warranty_txt_fr_ch:I = 0x7f09056d

.field public static final cigna_disclaimer_of_warranty_txt_gb:I = 0x7f09036c

.field public static final cigna_disclaimer_of_warranty_txt_hi:I = 0x7f0903d6

.field public static final cigna_disclaimer_of_warranty_txt_in:I = 0x7f0903e5

.field public static final cigna_disclaimer_of_warranty_txt_it_it:I = 0x7f0903f4

.field public static final cigna_disclaimer_of_warranty_txt_iw:I = 0x7f090403

.field public static final cigna_disclaimer_of_warranty_txt_jp:I = 0x7f090412

.field public static final cigna_disclaimer_of_warranty_txt_ko_kr:I = 0x7f090421

.field public static final cigna_disclaimer_of_warranty_txt_mx:I = 0x7f09057c

.field public static final cigna_disclaimer_of_warranty_txt_my:I = 0x7f090430

.field public static final cigna_disclaimer_of_warranty_txt_nl:I = 0x7f09043f

.field public static final cigna_disclaimer_of_warranty_txt_no:I = 0x7f09058e

.field public static final cigna_disclaimer_of_warranty_txt_nz:I = 0x7f09059d

.field public static final cigna_disclaimer_of_warranty_txt_pl:I = 0x7f09044e

.field public static final cigna_disclaimer_of_warranty_txt_pt_br:I = 0x7f09045d

.field public static final cigna_disclaimer_of_warranty_txt_ru:I = 0x7f09046c

.field public static final cigna_disclaimer_of_warranty_txt_sg:I = 0x7f0905ac

.field public static final cigna_disclaimer_of_warranty_txt_sv:I = 0x7f09047b

.field public static final cigna_disclaimer_of_warranty_txt_th:I = 0x7f09048a

.field public static final cigna_disclaimer_of_warranty_txt_tr:I = 0x7f090499

.field public static final cigna_disclaimer_of_warranty_txt_za:I = 0x7f0905bb

.field public static final cigna_disclaimer_of_warranty_txt_zh_cn:I = 0x7f0904a8

.field public static final cigna_disclaimer_of_warranty_txt_zh_hk:I = 0x7f0904b7

.field public static final cigna_disclaimer_of_warranty_txt_zh_tw:I = 0x7f0904c6

.field public static final cigna_down_to_go:I = 0x7f090305

.field public static final cigna_failed:I = 0x7f090306

.field public static final cigna_generales_title_mx:I = 0x7f090580

.field public static final cigna_generales_txt_1_mx:I = 0x7f090581

.field public static final cigna_generales_txt_2_mx:I = 0x7f090582

.field public static final cigna_goal_history_completed_goals:I = 0x7f090307

.field public static final cigna_goal_history_completed_missions:I = 0x7f090308

.field public static final cigna_goal_history_mission_complete:I = 0x7f090309

.field public static final cigna_goal_history_status_inprogress:I = 0x7f09030a

.field public static final cigna_header_about_cigna_abb:I = 0x7f090c60

.field public static final cigna_header_by_cigna_abb:I = 0x7f090c64

.field public static final cigna_header_by_cigna_abb_eng:I = 0x7f090350

.field public static final cigna_header_change_start_date_abb:I = 0x7f090c7c

.field public static final cigna_header_coach_abb:I = 0x7f090c65

.field public static final cigna_header_coach_abb_eng:I = 0x7f090351

.field public static final cigna_header_exercise_abb:I = 0x7f090c85

.field public static final cigna_header_favourites_abb:I = 0x7f090c55

.field public static final cigna_header_food_abb:I = 0x7f090c86

.field public static final cigna_header_goal_abb:I = 0x7f090c56

.field public static final cigna_header_goal_achieved_abb:I = 0x7f090c57

.field public static final cigna_header_learn_more_abb:I = 0x7f090c58

.field public static final cigna_header_library_abb2:I = 0x7f090c59

.field public static final cigna_header_lifestyle_abb:I = 0x7f090c5a

.field public static final cigna_header_mission:I = 0x7f090c78

.field public static final cigna_header_mission_completed_abb:I = 0x7f090c79

.field public static final cigna_header_new_badge_abb:I = 0x7f090c5b

.field public static final cigna_header_sort_by_abb:I = 0x7f090c63

.field public static final cigna_header_stop_lifestyle_assessment_abb:I = 0x7f090c62

.field public static final cigna_header_stress_abb:I = 0x7f090c87

.field public static final cigna_header_suggested_goals_abb:I = 0x7f090c5c

.field public static final cigna_header_suggested_missions_abb:I = 0x7f090c7a

.field public static final cigna_header_view_badges_abb:I = 0x7f090c5d

.field public static final cigna_header_view_history_abb:I = 0x7f090c5e

.field public static final cigna_header_view_new_scores_abb:I = 0x7f090c61

.field public static final cigna_header_weight_abb:I = 0x7f090c5f

.field public static final cigna_hello:I = 0x7f09030b

.field public static final cigna_intro_string:I = 0x7f09030c

.field public static final cigna_last_lifestyle_score:I = 0x7f090346

.field public static final cigna_library_app_name:I = 0x7f09030d

.field public static final cigna_library_featured:I = 0x7f09030e

.field public static final cigna_library_no_favorite_article_tip:I = 0x7f09030f

.field public static final cigna_library_page_count:I = 0x7f090c8d

.field public static final cigna_lifestyle_score_des:I = 0x7f090310

.field public static final cigna_lifestyle_score_des1_exercise:I = 0x7f090311

.field public static final cigna_lifestyle_score_des1_food:I = 0x7f090312

.field public static final cigna_lifestyle_score_des1_sleep:I = 0x7f090313

.field public static final cigna_lifestyle_score_des1_stress:I = 0x7f090314

.field public static final cigna_lifestyle_score_des1_weight:I = 0x7f090315

.field public static final cigna_lifestyle_score_des2_exercise:I = 0x7f090316

.field public static final cigna_lifestyle_score_des2_food:I = 0x7f090317

.field public static final cigna_lifestyle_score_des2_sleep:I = 0x7f090318

.field public static final cigna_lifestyle_score_des2_stress:I = 0x7f090319

.field public static final cigna_lifestyle_score_des2_weight:I = 0x7f09031a

.field public static final cigna_lifestyle_score_des3_exercise:I = 0x7f09031b

.field public static final cigna_lifestyle_score_des3_food:I = 0x7f09031c

.field public static final cigna_lifestyle_score_des3_sleep:I = 0x7f09031d

.field public static final cigna_lifestyle_score_des3_stress:I = 0x7f09031e

.field public static final cigna_lifestyle_score_des3_weight:I = 0x7f09031f

.field public static final cigna_lifestyle_score_title1:I = 0x7f090320

.field public static final cigna_lifestyle_score_title2:I = 0x7f090321

.field public static final cigna_lifestyle_score_title3:I = 0x7f090322

.field public static final cigna_lifestyle_score_view_title:I = 0x7f090323

.field public static final cigna_lifestyle_score_view_title_exercise:I = 0x7f090324

.field public static final cigna_lifestyle_score_view_title_food:I = 0x7f090325

.field public static final cigna_lifestyle_score_view_title_sleep:I = 0x7f090326

.field public static final cigna_lifestyle_score_view_title_stress:I = 0x7f090327

.field public static final cigna_lifestyle_score_view_title_weight:I = 0x7f090328

.field public static final cigna_lsr_cancelled:I = 0x7f090329

.field public static final cigna_mission_complete:I = 0x7f09032a

.field public static final cigna_no_activity:I = 0x7f09032b

.field public static final cigna_no_badges:I = 0x7f09032c

.field public static final cigna_no_goal_coach_message:I = 0x7f09032d

.field public static final cigna_no_goals:I = 0x7f09032e

.field public static final cigna_no_suggested_goal_description:I = 0x7f09032f

.field public static final cigna_no_suggested_goal_description_2:I = 0x7f090330

.field public static final cigna_no_suggested_goals:I = 0x7f090331

.field public static final cigna_no_suggested_mission_description:I = 0x7f090332

.field public static final cigna_no_suggested_missions:I = 0x7f090333

.field public static final cigna_noti_des_msg1:I = 0x7f090348

.field public static final cigna_noti_head_msg1:I = 0x7f090349

.field public static final cigna_noti_head_msg2:I = 0x7f09034a

.field public static final cigna_noti_head_msg7:I = 0x7f09034b

.field public static final cigna_noti_head_msg8:I = 0x7f09034c

.field public static final cigna_opt_about_cigna_abb:I = 0x7f090c6d

.field public static final cigna_opt_category:I = 0x7f090c6a

.field public static final cigna_opt_learn_more_abb:I = 0x7f090c6e

.field public static final cigna_opt_library:I = 0x7f090c6c

.field public static final cigna_opt_reassess_lifestyle_abb:I = 0x7f090c66

.field public static final cigna_opt_sort_by:I = 0x7f090c67

.field public static final cigna_opt_title_a_z:I = 0x7f090c6b

.field public static final cigna_opt_view_badges_abb:I = 0x7f090c68

.field public static final cigna_opt_view_history_abb:I = 0x7f090c69

.field public static final cigna_parties_title_ar_ae:I = 0x7f0904d6

.field public static final cigna_parties_title_au:I = 0x7f0904e5

.field public static final cigna_parties_title_ca:I = 0x7f0904f4

.field public static final cigna_parties_title_de:I = 0x7f09035e

.field public static final cigna_parties_title_de_at:I = 0x7f090503

.field public static final cigna_parties_title_de_ch:I = 0x7f090512

.field public static final cigna_parties_title_en_ch:I = 0x7f090521

.field public static final cigna_parties_title_en_fr:I = 0x7f090530

.field public static final cigna_parties_title_en_in:I = 0x7f09053f

.field public static final cigna_parties_title_en_pa:I = 0x7f09054e

.field public static final cigna_parties_title_en_ph:I = 0x7f09037c

.field public static final cigna_parties_title_en_us:I = 0x7f09038b

.field public static final cigna_parties_title_es:I = 0x7f09039b

.field public static final cigna_parties_title_es_pa:I = 0x7f09055f

.field public static final cigna_parties_title_es_us:I = 0x7f0903aa

.field public static final cigna_parties_title_fr:I = 0x7f0903b9

.field public static final cigna_parties_title_fr_ca:I = 0x7f0903c8

.field public static final cigna_parties_title_fr_ch:I = 0x7f09056e

.field public static final cigna_parties_title_gb:I = 0x7f09036d

.field public static final cigna_parties_title_hi:I = 0x7f0903d7

.field public static final cigna_parties_title_in:I = 0x7f0903e6

.field public static final cigna_parties_title_it_it:I = 0x7f0903f5

.field public static final cigna_parties_title_iw:I = 0x7f090404

.field public static final cigna_parties_title_jp:I = 0x7f090413

.field public static final cigna_parties_title_ko_kr:I = 0x7f090422

.field public static final cigna_parties_title_mx:I = 0x7f09057d

.field public static final cigna_parties_title_my:I = 0x7f090431

.field public static final cigna_parties_title_nl:I = 0x7f090440

.field public static final cigna_parties_title_no:I = 0x7f09058f

.field public static final cigna_parties_title_nz:I = 0x7f09059e

.field public static final cigna_parties_title_pl:I = 0x7f09044f

.field public static final cigna_parties_title_pt_br:I = 0x7f09045e

.field public static final cigna_parties_title_ru:I = 0x7f09046d

.field public static final cigna_parties_title_sg:I = 0x7f0905ad

.field public static final cigna_parties_title_sv:I = 0x7f09047c

.field public static final cigna_parties_title_th:I = 0x7f09048b

.field public static final cigna_parties_title_tr:I = 0x7f09049a

.field public static final cigna_parties_title_za:I = 0x7f0905bc

.field public static final cigna_parties_title_zh_cn:I = 0x7f0904a9

.field public static final cigna_parties_title_zh_hk:I = 0x7f0904b8

.field public static final cigna_parties_title_zh_tw:I = 0x7f0904c7

.field public static final cigna_parties_txt_1_ar_ae:I = 0x7f0904d7

.field public static final cigna_parties_txt_1_au:I = 0x7f0904e6

.field public static final cigna_parties_txt_1_ca:I = 0x7f0904f5

.field public static final cigna_parties_txt_1_de:I = 0x7f09035f

.field public static final cigna_parties_txt_1_de_at:I = 0x7f090504

.field public static final cigna_parties_txt_1_de_ch:I = 0x7f090513

.field public static final cigna_parties_txt_1_en_ch:I = 0x7f090522

.field public static final cigna_parties_txt_1_en_fr:I = 0x7f090531

.field public static final cigna_parties_txt_1_en_in:I = 0x7f090540

.field public static final cigna_parties_txt_1_en_pa:I = 0x7f09054f

.field public static final cigna_parties_txt_1_en_ph:I = 0x7f09037d

.field public static final cigna_parties_txt_1_en_us:I = 0x7f09038c

.field public static final cigna_parties_txt_1_es:I = 0x7f09039c

.field public static final cigna_parties_txt_1_es_pa:I = 0x7f090560

.field public static final cigna_parties_txt_1_es_us:I = 0x7f0903ab

.field public static final cigna_parties_txt_1_fr:I = 0x7f0903ba

.field public static final cigna_parties_txt_1_fr_ca:I = 0x7f0903c9

.field public static final cigna_parties_txt_1_fr_ch:I = 0x7f09056f

.field public static final cigna_parties_txt_1_gb:I = 0x7f09036e

.field public static final cigna_parties_txt_1_hi:I = 0x7f0903d8

.field public static final cigna_parties_txt_1_in:I = 0x7f0903e7

.field public static final cigna_parties_txt_1_it_it:I = 0x7f0903f6

.field public static final cigna_parties_txt_1_iw:I = 0x7f090405

.field public static final cigna_parties_txt_1_jp:I = 0x7f090414

.field public static final cigna_parties_txt_1_ko_kr:I = 0x7f090423

.field public static final cigna_parties_txt_1_mx:I = 0x7f09057e

.field public static final cigna_parties_txt_1_my:I = 0x7f090432

.field public static final cigna_parties_txt_1_nl:I = 0x7f090441

.field public static final cigna_parties_txt_1_no:I = 0x7f090590

.field public static final cigna_parties_txt_1_nz:I = 0x7f09059f

.field public static final cigna_parties_txt_1_pl:I = 0x7f090450

.field public static final cigna_parties_txt_1_pt_br:I = 0x7f09045f

.field public static final cigna_parties_txt_1_ru:I = 0x7f09046e

.field public static final cigna_parties_txt_1_sg:I = 0x7f0905ae

.field public static final cigna_parties_txt_1_sv:I = 0x7f09047d

.field public static final cigna_parties_txt_1_th:I = 0x7f09048c

.field public static final cigna_parties_txt_1_tr:I = 0x7f09049b

.field public static final cigna_parties_txt_1_za:I = 0x7f0905bd

.field public static final cigna_parties_txt_1_zh_cn:I = 0x7f0904aa

.field public static final cigna_parties_txt_1_zh_hk:I = 0x7f0904b9

.field public static final cigna_parties_txt_1_zh_tw:I = 0x7f0904c8

.field public static final cigna_parties_txt_2_ar_ae:I = 0x7f0904d8

.field public static final cigna_parties_txt_2_au:I = 0x7f0904e7

.field public static final cigna_parties_txt_2_ca:I = 0x7f0904f6

.field public static final cigna_parties_txt_2_de:I = 0x7f090360

.field public static final cigna_parties_txt_2_de_at:I = 0x7f090505

.field public static final cigna_parties_txt_2_de_ch:I = 0x7f090514

.field public static final cigna_parties_txt_2_en_ch:I = 0x7f090523

.field public static final cigna_parties_txt_2_en_fr:I = 0x7f090532

.field public static final cigna_parties_txt_2_en_in:I = 0x7f090541

.field public static final cigna_parties_txt_2_en_pa:I = 0x7f090550

.field public static final cigna_parties_txt_2_en_ph:I = 0x7f09037e

.field public static final cigna_parties_txt_2_en_us:I = 0x7f09038d

.field public static final cigna_parties_txt_2_es:I = 0x7f09039d

.field public static final cigna_parties_txt_2_es_pa:I = 0x7f090561

.field public static final cigna_parties_txt_2_es_us:I = 0x7f0903ac

.field public static final cigna_parties_txt_2_fr:I = 0x7f0903bb

.field public static final cigna_parties_txt_2_fr_ca:I = 0x7f0903ca

.field public static final cigna_parties_txt_2_fr_ch:I = 0x7f090570

.field public static final cigna_parties_txt_2_gb:I = 0x7f09036f

.field public static final cigna_parties_txt_2_hi:I = 0x7f0903d9

.field public static final cigna_parties_txt_2_in:I = 0x7f0903e8

.field public static final cigna_parties_txt_2_it_it:I = 0x7f0903f7

.field public static final cigna_parties_txt_2_iw:I = 0x7f090406

.field public static final cigna_parties_txt_2_jp:I = 0x7f090415

.field public static final cigna_parties_txt_2_ko_kr:I = 0x7f090424

.field public static final cigna_parties_txt_2_mx:I = 0x7f09057f

.field public static final cigna_parties_txt_2_my:I = 0x7f090433

.field public static final cigna_parties_txt_2_nl:I = 0x7f090442

.field public static final cigna_parties_txt_2_no:I = 0x7f090591

.field public static final cigna_parties_txt_2_nz:I = 0x7f0905a0

.field public static final cigna_parties_txt_2_pl:I = 0x7f090451

.field public static final cigna_parties_txt_2_pt_br:I = 0x7f090460

.field public static final cigna_parties_txt_2_ru:I = 0x7f09046f

.field public static final cigna_parties_txt_2_sg:I = 0x7f0905af

.field public static final cigna_parties_txt_2_sv:I = 0x7f09047e

.field public static final cigna_parties_txt_2_th:I = 0x7f09048d

.field public static final cigna_parties_txt_2_tr:I = 0x7f09049c

.field public static final cigna_parties_txt_2_za:I = 0x7f0905be

.field public static final cigna_parties_txt_2_zh_cn:I = 0x7f0904ab

.field public static final cigna_parties_txt_2_zh_hk:I = 0x7f0904ba

.field public static final cigna_parties_txt_2_zh_tw:I = 0x7f0904c9

.field public static final cigna_pop_failed_to_download_language_data:I = 0x7f090c70

.field public static final cigna_pop_sleep_score_updated:I = 0x7f090c71

.field public static final cigna_pop_syncing_s_health_data_ing:I = 0x7f090c84

.field public static final cigna_pop_the_selected_item_will_be_deleted:I = 0x7f090c7e

.field public static final cigna_pop_the_selected_items_will_be_deleted:I = 0x7f090c7f

.field public static final cigna_pop_your_lifestyle_assessment_has_not_been_completed:I = 0x7f090c6f

.field public static final cigna_pop_your_recorded_lifestyle_score_completed:I = 0x7f090c88

.field public static final cigna_prompt_comma:I = 0x7f090334

.field public static final cigna_question:I = 0x7f0901f3

.field public static final cigna_question_mark:I = 0x7f090335

.field public static final cigna_right_and_licences_title_ar_ae:I = 0x7f0904d0

.field public static final cigna_right_and_licences_title_au:I = 0x7f0904df

.field public static final cigna_right_and_licences_title_ca:I = 0x7f0904ee

.field public static final cigna_right_and_licences_title_de:I = 0x7f090358

.field public static final cigna_right_and_licences_title_de_at:I = 0x7f0904fd

.field public static final cigna_right_and_licences_title_de_ch:I = 0x7f09050c

.field public static final cigna_right_and_licences_title_en_ch:I = 0x7f09051b

.field public static final cigna_right_and_licences_title_en_fr:I = 0x7f09052a

.field public static final cigna_right_and_licences_title_en_in:I = 0x7f090539

.field public static final cigna_right_and_licences_title_en_pa:I = 0x7f090548

.field public static final cigna_right_and_licences_title_en_ph:I = 0x7f090376

.field public static final cigna_right_and_licences_title_en_us:I = 0x7f090385

.field public static final cigna_right_and_licences_title_es:I = 0x7f090395

.field public static final cigna_right_and_licences_title_es_pa:I = 0x7f090559

.field public static final cigna_right_and_licences_title_es_us:I = 0x7f0903a4

.field public static final cigna_right_and_licences_title_fr:I = 0x7f0903b3

.field public static final cigna_right_and_licences_title_fr_ca:I = 0x7f0903c2

.field public static final cigna_right_and_licences_title_fr_ch:I = 0x7f090568

.field public static final cigna_right_and_licences_title_gb:I = 0x7f090367

.field public static final cigna_right_and_licences_title_hi:I = 0x7f0903d1

.field public static final cigna_right_and_licences_title_in:I = 0x7f0903e0

.field public static final cigna_right_and_licences_title_it_it:I = 0x7f0903ef

.field public static final cigna_right_and_licences_title_iw:I = 0x7f0903fe

.field public static final cigna_right_and_licences_title_jp:I = 0x7f09040d

.field public static final cigna_right_and_licences_title_ko_kr:I = 0x7f09041c

.field public static final cigna_right_and_licences_title_mx:I = 0x7f090577

.field public static final cigna_right_and_licences_title_my:I = 0x7f09042b

.field public static final cigna_right_and_licences_title_nl:I = 0x7f09043a

.field public static final cigna_right_and_licences_title_no:I = 0x7f090589

.field public static final cigna_right_and_licences_title_nz:I = 0x7f090598

.field public static final cigna_right_and_licences_title_pl:I = 0x7f090449

.field public static final cigna_right_and_licences_title_pt_br:I = 0x7f090458

.field public static final cigna_right_and_licences_title_ru:I = 0x7f090467

.field public static final cigna_right_and_licences_title_sg:I = 0x7f0905a7

.field public static final cigna_right_and_licences_title_sv:I = 0x7f090476

.field public static final cigna_right_and_licences_title_th:I = 0x7f090485

.field public static final cigna_right_and_licences_title_tr:I = 0x7f090494

.field public static final cigna_right_and_licences_title_za:I = 0x7f0905b6

.field public static final cigna_right_and_licences_title_zh_cn:I = 0x7f0904a3

.field public static final cigna_right_and_licences_title_zh_hk:I = 0x7f0904b2

.field public static final cigna_right_and_licences_title_zh_tw:I = 0x7f0904c1

.field public static final cigna_right_and_licences_txt_1_ar_ae:I = 0x7f0904d1

.field public static final cigna_right_and_licences_txt_1_au:I = 0x7f0904e0

.field public static final cigna_right_and_licences_txt_1_ca:I = 0x7f0904ef

.field public static final cigna_right_and_licences_txt_1_de:I = 0x7f090359

.field public static final cigna_right_and_licences_txt_1_de_at:I = 0x7f0904fe

.field public static final cigna_right_and_licences_txt_1_de_ch:I = 0x7f09050d

.field public static final cigna_right_and_licences_txt_1_en_ch:I = 0x7f09051c

.field public static final cigna_right_and_licences_txt_1_en_fr:I = 0x7f09052b

.field public static final cigna_right_and_licences_txt_1_en_in:I = 0x7f09053a

.field public static final cigna_right_and_licences_txt_1_en_pa:I = 0x7f090549

.field public static final cigna_right_and_licences_txt_1_en_ph:I = 0x7f090377

.field public static final cigna_right_and_licences_txt_1_en_us:I = 0x7f090386

.field public static final cigna_right_and_licences_txt_1_es:I = 0x7f090396

.field public static final cigna_right_and_licences_txt_1_es_pa:I = 0x7f09055a

.field public static final cigna_right_and_licences_txt_1_es_us:I = 0x7f0903a5

.field public static final cigna_right_and_licences_txt_1_fr:I = 0x7f0903b4

.field public static final cigna_right_and_licences_txt_1_fr_ca:I = 0x7f0903c3

.field public static final cigna_right_and_licences_txt_1_fr_ch:I = 0x7f090569

.field public static final cigna_right_and_licences_txt_1_gb:I = 0x7f090368

.field public static final cigna_right_and_licences_txt_1_hi:I = 0x7f0903d2

.field public static final cigna_right_and_licences_txt_1_in:I = 0x7f0903e1

.field public static final cigna_right_and_licences_txt_1_it_it:I = 0x7f0903f0

.field public static final cigna_right_and_licences_txt_1_iw:I = 0x7f0903ff

.field public static final cigna_right_and_licences_txt_1_jp:I = 0x7f09040e

.field public static final cigna_right_and_licences_txt_1_ko_kr:I = 0x7f09041d

.field public static final cigna_right_and_licences_txt_1_mx:I = 0x7f090578

.field public static final cigna_right_and_licences_txt_1_my:I = 0x7f09042c

.field public static final cigna_right_and_licences_txt_1_nl:I = 0x7f09043b

.field public static final cigna_right_and_licences_txt_1_no:I = 0x7f09058a

.field public static final cigna_right_and_licences_txt_1_nz:I = 0x7f090599

.field public static final cigna_right_and_licences_txt_1_pl:I = 0x7f09044a

.field public static final cigna_right_and_licences_txt_1_pt_br:I = 0x7f090459

.field public static final cigna_right_and_licences_txt_1_ru:I = 0x7f090468

.field public static final cigna_right_and_licences_txt_1_sg:I = 0x7f0905a8

.field public static final cigna_right_and_licences_txt_1_sv:I = 0x7f090477

.field public static final cigna_right_and_licences_txt_1_th:I = 0x7f090486

.field public static final cigna_right_and_licences_txt_1_tr:I = 0x7f090495

.field public static final cigna_right_and_licences_txt_1_za:I = 0x7f0905b7

.field public static final cigna_right_and_licences_txt_1_zh_cn:I = 0x7f0904a4

.field public static final cigna_right_and_licences_txt_1_zh_hk:I = 0x7f0904b3

.field public static final cigna_right_and_licences_txt_1_zh_tw:I = 0x7f0904c2

.field public static final cigna_right_and_licences_txt_2_ar_ae:I = 0x7f0904d2

.field public static final cigna_right_and_licences_txt_2_au:I = 0x7f0904e1

.field public static final cigna_right_and_licences_txt_2_ca:I = 0x7f0904f0

.field public static final cigna_right_and_licences_txt_2_de:I = 0x7f09035a

.field public static final cigna_right_and_licences_txt_2_de_at:I = 0x7f0904ff

.field public static final cigna_right_and_licences_txt_2_de_ch:I = 0x7f09050e

.field public static final cigna_right_and_licences_txt_2_en_ch:I = 0x7f09051d

.field public static final cigna_right_and_licences_txt_2_en_fr:I = 0x7f09052c

.field public static final cigna_right_and_licences_txt_2_en_in:I = 0x7f09053b

.field public static final cigna_right_and_licences_txt_2_en_pa:I = 0x7f09054a

.field public static final cigna_right_and_licences_txt_2_en_ph:I = 0x7f090378

.field public static final cigna_right_and_licences_txt_2_en_us:I = 0x7f090387

.field public static final cigna_right_and_licences_txt_2_es:I = 0x7f090397

.field public static final cigna_right_and_licences_txt_2_es_pa:I = 0x7f09055b

.field public static final cigna_right_and_licences_txt_2_es_us:I = 0x7f0903a6

.field public static final cigna_right_and_licences_txt_2_fr:I = 0x7f0903b5

.field public static final cigna_right_and_licences_txt_2_fr_ca:I = 0x7f0903c4

.field public static final cigna_right_and_licences_txt_2_fr_ch:I = 0x7f09056a

.field public static final cigna_right_and_licences_txt_2_gb:I = 0x7f090369

.field public static final cigna_right_and_licences_txt_2_hi:I = 0x7f0903d3

.field public static final cigna_right_and_licences_txt_2_in:I = 0x7f0903e2

.field public static final cigna_right_and_licences_txt_2_it_it:I = 0x7f0903f1

.field public static final cigna_right_and_licences_txt_2_iw:I = 0x7f090400

.field public static final cigna_right_and_licences_txt_2_jp:I = 0x7f09040f

.field public static final cigna_right_and_licences_txt_2_ko_kr:I = 0x7f09041e

.field public static final cigna_right_and_licences_txt_2_mx:I = 0x7f090579

.field public static final cigna_right_and_licences_txt_2_my:I = 0x7f09042d

.field public static final cigna_right_and_licences_txt_2_nl:I = 0x7f09043c

.field public static final cigna_right_and_licences_txt_2_no:I = 0x7f09058b

.field public static final cigna_right_and_licences_txt_2_nz:I = 0x7f09059a

.field public static final cigna_right_and_licences_txt_2_pl:I = 0x7f09044b

.field public static final cigna_right_and_licences_txt_2_pt_br:I = 0x7f09045a

.field public static final cigna_right_and_licences_txt_2_ru:I = 0x7f090469

.field public static final cigna_right_and_licences_txt_2_sg:I = 0x7f0905a9

.field public static final cigna_right_and_licences_txt_2_sv:I = 0x7f090478

.field public static final cigna_right_and_licences_txt_2_th:I = 0x7f090487

.field public static final cigna_right_and_licences_txt_2_tr:I = 0x7f090496

.field public static final cigna_right_and_licences_txt_2_za:I = 0x7f0905b8

.field public static final cigna_right_and_licences_txt_2_zh_cn:I = 0x7f0904a5

.field public static final cigna_right_and_licences_txt_2_zh_hk:I = 0x7f0904b4

.field public static final cigna_right_and_licences_txt_2_zh_tw:I = 0x7f0904c3

.field public static final cigna_right_and_licences_txt_3_ar_ae:I = 0x7f0904d3

.field public static final cigna_right_and_licences_txt_3_au:I = 0x7f0904e2

.field public static final cigna_right_and_licences_txt_3_ca:I = 0x7f0904f1

.field public static final cigna_right_and_licences_txt_3_de:I = 0x7f09035b

.field public static final cigna_right_and_licences_txt_3_de_at:I = 0x7f090500

.field public static final cigna_right_and_licences_txt_3_de_ch:I = 0x7f09050f

.field public static final cigna_right_and_licences_txt_3_en_ch:I = 0x7f09051e

.field public static final cigna_right_and_licences_txt_3_en_fr:I = 0x7f09052d

.field public static final cigna_right_and_licences_txt_3_en_in:I = 0x7f09053c

.field public static final cigna_right_and_licences_txt_3_en_pa:I = 0x7f09054b

.field public static final cigna_right_and_licences_txt_3_en_ph:I = 0x7f090379

.field public static final cigna_right_and_licences_txt_3_en_us:I = 0x7f090388

.field public static final cigna_right_and_licences_txt_3_es:I = 0x7f090398

.field public static final cigna_right_and_licences_txt_3_es_pa:I = 0x7f09055c

.field public static final cigna_right_and_licences_txt_3_es_us:I = 0x7f0903a7

.field public static final cigna_right_and_licences_txt_3_fr:I = 0x7f0903b6

.field public static final cigna_right_and_licences_txt_3_fr_ca:I = 0x7f0903c5

.field public static final cigna_right_and_licences_txt_3_fr_ch:I = 0x7f09056b

.field public static final cigna_right_and_licences_txt_3_gb:I = 0x7f09036a

.field public static final cigna_right_and_licences_txt_3_hi:I = 0x7f0903d4

.field public static final cigna_right_and_licences_txt_3_in:I = 0x7f0903e3

.field public static final cigna_right_and_licences_txt_3_it_it:I = 0x7f0903f2

.field public static final cigna_right_and_licences_txt_3_iw:I = 0x7f090401

.field public static final cigna_right_and_licences_txt_3_jp:I = 0x7f090410

.field public static final cigna_right_and_licences_txt_3_ko_kr:I = 0x7f09041f

.field public static final cigna_right_and_licences_txt_3_mx:I = 0x7f09057a

.field public static final cigna_right_and_licences_txt_3_my:I = 0x7f09042e

.field public static final cigna_right_and_licences_txt_3_nl:I = 0x7f09043d

.field public static final cigna_right_and_licences_txt_3_no:I = 0x7f09058c

.field public static final cigna_right_and_licences_txt_3_nz:I = 0x7f09059b

.field public static final cigna_right_and_licences_txt_3_pl:I = 0x7f09044c

.field public static final cigna_right_and_licences_txt_3_pt_br:I = 0x7f09045b

.field public static final cigna_right_and_licences_txt_3_ru:I = 0x7f09046a

.field public static final cigna_right_and_licences_txt_3_sg:I = 0x7f0905aa

.field public static final cigna_right_and_licences_txt_3_sv:I = 0x7f090479

.field public static final cigna_right_and_licences_txt_3_th:I = 0x7f090488

.field public static final cigna_right_and_licences_txt_3_tr:I = 0x7f090497

.field public static final cigna_right_and_licences_txt_3_za:I = 0x7f0905b9

.field public static final cigna_right_and_licences_txt_3_zh_cn:I = 0x7f0904a6

.field public static final cigna_right_and_licences_txt_3_zh_hk:I = 0x7f0904b5

.field public static final cigna_right_and_licences_txt_3_zh_tw:I = 0x7f0904c4

.field public static final cigna_starting:I = 0x7f090336

.field public static final cigna_suggested_goals_header_one:I = 0x7f090337

.field public static final cigna_suggested_goals_header_two:I = 0x7f090338

.field public static final cigna_suggested_height:I = 0x7f090339

.field public static final cigna_suggested_missions_header_two:I = 0x7f09033a

.field public static final cigna_suggested_weight:I = 0x7f09033b

.field public static final cigna_toward_your_mission:I = 0x7f09033c

.field public static final cigna_tpop_article_added_to_favourites:I = 0x7f090c72

.field public static final cigna_tpop_article_removed_from_favourites:I = 0x7f090c73

.field public static final cigna_tpop_cannot_select_future_date:I = 0x7f090c83

.field public static final cigna_tpop_exercise_score_updated:I = 0x7f090c74

.field public static final cigna_tpop_food_score_updated:I = 0x7f090c75

.field public static final cigna_tpop_mission_added_to_ongoing_missions:I = 0x7f090c80

.field public static final cigna_tpop_mission_must_be_set_to_start_within_the_next_pd_days:I = 0x7f090c81

.field public static final cigna_tpop_stress_score_updated:I = 0x7f090c76

.field public static final cigna_tpop_weight_score_updated:I = 0x7f090c77

.field public static final cigna_tracker_mission_info_txt:I = 0x7f090347

.field public static final cigna_ts_select_i_agree_tickbox_to_use_tpop:I = 0x7f090c44

.field public static final cigna_use_of_coach_title_ar_ae:I = 0x7f0904ca

.field public static final cigna_use_of_coach_title_au:I = 0x7f0904d9

.field public static final cigna_use_of_coach_title_ca:I = 0x7f0904e8

.field public static final cigna_use_of_coach_title_de:I = 0x7f090352

.field public static final cigna_use_of_coach_title_de_at:I = 0x7f0904f7

.field public static final cigna_use_of_coach_title_de_ch:I = 0x7f090506

.field public static final cigna_use_of_coach_title_en_ch:I = 0x7f090515

.field public static final cigna_use_of_coach_title_en_fr:I = 0x7f090524

.field public static final cigna_use_of_coach_title_en_in:I = 0x7f090533

.field public static final cigna_use_of_coach_title_en_pa:I = 0x7f090542

.field public static final cigna_use_of_coach_title_en_ph:I = 0x7f090370

.field public static final cigna_use_of_coach_title_en_us:I = 0x7f09037f

.field public static final cigna_use_of_coach_title_es:I = 0x7f09038f

.field public static final cigna_use_of_coach_title_es_pa:I = 0x7f090553

.field public static final cigna_use_of_coach_title_es_us:I = 0x7f09039e

.field public static final cigna_use_of_coach_title_fr:I = 0x7f0903ad

.field public static final cigna_use_of_coach_title_fr_ca:I = 0x7f0903bc

.field public static final cigna_use_of_coach_title_fr_ch:I = 0x7f090562

.field public static final cigna_use_of_coach_title_gb:I = 0x7f090361

.field public static final cigna_use_of_coach_title_hi:I = 0x7f0903cb

.field public static final cigna_use_of_coach_title_in:I = 0x7f0903da

.field public static final cigna_use_of_coach_title_it_it:I = 0x7f0903e9

.field public static final cigna_use_of_coach_title_iw:I = 0x7f0903f8

.field public static final cigna_use_of_coach_title_jp:I = 0x7f090407

.field public static final cigna_use_of_coach_title_ko_kr:I = 0x7f090416

.field public static final cigna_use_of_coach_title_mx:I = 0x7f090571

.field public static final cigna_use_of_coach_title_my:I = 0x7f090425

.field public static final cigna_use_of_coach_title_nl:I = 0x7f090434

.field public static final cigna_use_of_coach_title_no:I = 0x7f090583

.field public static final cigna_use_of_coach_title_nz:I = 0x7f090592

.field public static final cigna_use_of_coach_title_pl:I = 0x7f090443

.field public static final cigna_use_of_coach_title_pt_br:I = 0x7f090452

.field public static final cigna_use_of_coach_title_ru:I = 0x7f090461

.field public static final cigna_use_of_coach_title_sg:I = 0x7f0905a1

.field public static final cigna_use_of_coach_title_sv:I = 0x7f090470

.field public static final cigna_use_of_coach_title_th:I = 0x7f09047f

.field public static final cigna_use_of_coach_title_tr:I = 0x7f09048e

.field public static final cigna_use_of_coach_title_za:I = 0x7f0905b0

.field public static final cigna_use_of_coach_title_zh_cn:I = 0x7f09049d

.field public static final cigna_use_of_coach_title_zh_hk:I = 0x7f0904ac

.field public static final cigna_use_of_coach_title_zh_tw:I = 0x7f0904bb

.field public static final cigna_use_of_coach_txt_1_ar_ae:I = 0x7f0904cb

.field public static final cigna_use_of_coach_txt_1_au:I = 0x7f0904da

.field public static final cigna_use_of_coach_txt_1_ca:I = 0x7f0904e9

.field public static final cigna_use_of_coach_txt_1_de:I = 0x7f090353

.field public static final cigna_use_of_coach_txt_1_de_at:I = 0x7f0904f8

.field public static final cigna_use_of_coach_txt_1_de_ch:I = 0x7f090507

.field public static final cigna_use_of_coach_txt_1_en_ch:I = 0x7f090516

.field public static final cigna_use_of_coach_txt_1_en_fr:I = 0x7f090525

.field public static final cigna_use_of_coach_txt_1_en_in:I = 0x7f090534

.field public static final cigna_use_of_coach_txt_1_en_pa:I = 0x7f090543

.field public static final cigna_use_of_coach_txt_1_en_ph:I = 0x7f090371

.field public static final cigna_use_of_coach_txt_1_en_us:I = 0x7f090380

.field public static final cigna_use_of_coach_txt_1_es:I = 0x7f090390

.field public static final cigna_use_of_coach_txt_1_es_ar:I = 0x7f090551

.field public static final cigna_use_of_coach_txt_1_es_cl:I = 0x7f090552

.field public static final cigna_use_of_coach_txt_1_es_co:I = 0x7f09038e

.field public static final cigna_use_of_coach_txt_1_es_pa:I = 0x7f090554

.field public static final cigna_use_of_coach_txt_1_es_us:I = 0x7f09039f

.field public static final cigna_use_of_coach_txt_1_fr:I = 0x7f0903ae

.field public static final cigna_use_of_coach_txt_1_fr_ca:I = 0x7f0903bd

.field public static final cigna_use_of_coach_txt_1_fr_ch:I = 0x7f090563

.field public static final cigna_use_of_coach_txt_1_gb:I = 0x7f090362

.field public static final cigna_use_of_coach_txt_1_hi:I = 0x7f0903cc

.field public static final cigna_use_of_coach_txt_1_in:I = 0x7f0903db

.field public static final cigna_use_of_coach_txt_1_it_it:I = 0x7f0903ea

.field public static final cigna_use_of_coach_txt_1_iw:I = 0x7f0903f9

.field public static final cigna_use_of_coach_txt_1_jp:I = 0x7f090408

.field public static final cigna_use_of_coach_txt_1_ko_kr:I = 0x7f090417

.field public static final cigna_use_of_coach_txt_1_mx:I = 0x7f090572

.field public static final cigna_use_of_coach_txt_1_my:I = 0x7f090426

.field public static final cigna_use_of_coach_txt_1_nl:I = 0x7f090435

.field public static final cigna_use_of_coach_txt_1_no:I = 0x7f090584

.field public static final cigna_use_of_coach_txt_1_nz:I = 0x7f090593

.field public static final cigna_use_of_coach_txt_1_pl:I = 0x7f090444

.field public static final cigna_use_of_coach_txt_1_pt_br:I = 0x7f090453

.field public static final cigna_use_of_coach_txt_1_ru:I = 0x7f090462

.field public static final cigna_use_of_coach_txt_1_sg:I = 0x7f0905a2

.field public static final cigna_use_of_coach_txt_1_sv:I = 0x7f090471

.field public static final cigna_use_of_coach_txt_1_th:I = 0x7f090480

.field public static final cigna_use_of_coach_txt_1_tr:I = 0x7f09048f

.field public static final cigna_use_of_coach_txt_1_za:I = 0x7f0905b1

.field public static final cigna_use_of_coach_txt_1_zh_cn:I = 0x7f09049e

.field public static final cigna_use_of_coach_txt_1_zh_hk:I = 0x7f0904ad

.field public static final cigna_use_of_coach_txt_1_zh_tw:I = 0x7f0904bc

.field public static final cigna_use_of_coach_txt_2_ar_ae:I = 0x7f0904cc

.field public static final cigna_use_of_coach_txt_2_au:I = 0x7f0904db

.field public static final cigna_use_of_coach_txt_2_ca:I = 0x7f0904ea

.field public static final cigna_use_of_coach_txt_2_de:I = 0x7f090354

.field public static final cigna_use_of_coach_txt_2_de_at:I = 0x7f0904f9

.field public static final cigna_use_of_coach_txt_2_de_ch:I = 0x7f090508

.field public static final cigna_use_of_coach_txt_2_en_ch:I = 0x7f090517

.field public static final cigna_use_of_coach_txt_2_en_fr:I = 0x7f090526

.field public static final cigna_use_of_coach_txt_2_en_in:I = 0x7f090535

.field public static final cigna_use_of_coach_txt_2_en_pa:I = 0x7f090544

.field public static final cigna_use_of_coach_txt_2_en_ph:I = 0x7f090372

.field public static final cigna_use_of_coach_txt_2_en_us:I = 0x7f090381

.field public static final cigna_use_of_coach_txt_2_es:I = 0x7f090391

.field public static final cigna_use_of_coach_txt_2_es_pa:I = 0x7f090555

.field public static final cigna_use_of_coach_txt_2_es_us:I = 0x7f0903a0

.field public static final cigna_use_of_coach_txt_2_fr:I = 0x7f0903af

.field public static final cigna_use_of_coach_txt_2_fr_ca:I = 0x7f0903be

.field public static final cigna_use_of_coach_txt_2_fr_ch:I = 0x7f090564

.field public static final cigna_use_of_coach_txt_2_gb:I = 0x7f090363

.field public static final cigna_use_of_coach_txt_2_hi:I = 0x7f0903cd

.field public static final cigna_use_of_coach_txt_2_in:I = 0x7f0903dc

.field public static final cigna_use_of_coach_txt_2_it_it:I = 0x7f0903eb

.field public static final cigna_use_of_coach_txt_2_iw:I = 0x7f0903fa

.field public static final cigna_use_of_coach_txt_2_jp:I = 0x7f090409

.field public static final cigna_use_of_coach_txt_2_ko_kr:I = 0x7f090418

.field public static final cigna_use_of_coach_txt_2_mx:I = 0x7f090573

.field public static final cigna_use_of_coach_txt_2_my:I = 0x7f090427

.field public static final cigna_use_of_coach_txt_2_nl:I = 0x7f090436

.field public static final cigna_use_of_coach_txt_2_no:I = 0x7f090585

.field public static final cigna_use_of_coach_txt_2_nz:I = 0x7f090594

.field public static final cigna_use_of_coach_txt_2_pl:I = 0x7f090445

.field public static final cigna_use_of_coach_txt_2_pt_br:I = 0x7f090454

.field public static final cigna_use_of_coach_txt_2_ru:I = 0x7f090463

.field public static final cigna_use_of_coach_txt_2_sg:I = 0x7f0905a3

.field public static final cigna_use_of_coach_txt_2_sv:I = 0x7f090472

.field public static final cigna_use_of_coach_txt_2_th:I = 0x7f090481

.field public static final cigna_use_of_coach_txt_2_tr:I = 0x7f090490

.field public static final cigna_use_of_coach_txt_2_za:I = 0x7f0905b2

.field public static final cigna_use_of_coach_txt_2_zh_cn:I = 0x7f09049f

.field public static final cigna_use_of_coach_txt_2_zh_hk:I = 0x7f0904ae

.field public static final cigna_use_of_coach_txt_2_zh_tw:I = 0x7f0904bd

.field public static final cigna_use_of_coach_txt_3_ar_ae:I = 0x7f0904cd

.field public static final cigna_use_of_coach_txt_3_au:I = 0x7f0904dc

.field public static final cigna_use_of_coach_txt_3_ca:I = 0x7f0904eb

.field public static final cigna_use_of_coach_txt_3_de:I = 0x7f090355

.field public static final cigna_use_of_coach_txt_3_de_at:I = 0x7f0904fa

.field public static final cigna_use_of_coach_txt_3_de_ch:I = 0x7f090509

.field public static final cigna_use_of_coach_txt_3_en_ch:I = 0x7f090518

.field public static final cigna_use_of_coach_txt_3_en_fr:I = 0x7f090527

.field public static final cigna_use_of_coach_txt_3_en_in:I = 0x7f090536

.field public static final cigna_use_of_coach_txt_3_en_pa:I = 0x7f090545

.field public static final cigna_use_of_coach_txt_3_en_ph:I = 0x7f090373

.field public static final cigna_use_of_coach_txt_3_en_us:I = 0x7f090382

.field public static final cigna_use_of_coach_txt_3_es:I = 0x7f090392

.field public static final cigna_use_of_coach_txt_3_es_pa:I = 0x7f090556

.field public static final cigna_use_of_coach_txt_3_es_us:I = 0x7f0903a1

.field public static final cigna_use_of_coach_txt_3_fr:I = 0x7f0903b0

.field public static final cigna_use_of_coach_txt_3_fr_ca:I = 0x7f0903bf

.field public static final cigna_use_of_coach_txt_3_fr_ch:I = 0x7f090565

.field public static final cigna_use_of_coach_txt_3_gb:I = 0x7f090364

.field public static final cigna_use_of_coach_txt_3_hi:I = 0x7f0903ce

.field public static final cigna_use_of_coach_txt_3_in:I = 0x7f0903dd

.field public static final cigna_use_of_coach_txt_3_it_it:I = 0x7f0903ec

.field public static final cigna_use_of_coach_txt_3_iw:I = 0x7f0903fb

.field public static final cigna_use_of_coach_txt_3_jp:I = 0x7f09040a

.field public static final cigna_use_of_coach_txt_3_ko_kr:I = 0x7f090419

.field public static final cigna_use_of_coach_txt_3_mx:I = 0x7f090574

.field public static final cigna_use_of_coach_txt_3_my:I = 0x7f090428

.field public static final cigna_use_of_coach_txt_3_nl:I = 0x7f090437

.field public static final cigna_use_of_coach_txt_3_no:I = 0x7f090586

.field public static final cigna_use_of_coach_txt_3_nz:I = 0x7f090595

.field public static final cigna_use_of_coach_txt_3_pl:I = 0x7f090446

.field public static final cigna_use_of_coach_txt_3_pt_br:I = 0x7f090455

.field public static final cigna_use_of_coach_txt_3_ru:I = 0x7f090464

.field public static final cigna_use_of_coach_txt_3_sg:I = 0x7f0905a4

.field public static final cigna_use_of_coach_txt_3_sv:I = 0x7f090473

.field public static final cigna_use_of_coach_txt_3_th:I = 0x7f090482

.field public static final cigna_use_of_coach_txt_3_tr:I = 0x7f090491

.field public static final cigna_use_of_coach_txt_3_za:I = 0x7f0905b3

.field public static final cigna_use_of_coach_txt_3_zh_cn:I = 0x7f0904a0

.field public static final cigna_use_of_coach_txt_3_zh_hk:I = 0x7f0904af

.field public static final cigna_use_of_coach_txt_3_zh_tw:I = 0x7f0904be

.field public static final cigna_web_tts:I = 0x7f090faf

.field public static final cigna_what_to_do:I = 0x7f09033d

.field public static final cigna_why_do_it:I = 0x7f09033e

.field public static final cigna_widget_initial_description:I = 0x7f09033f

.field public static final cigna_widget_no_goal_no_mission_txt:I = 0x7f090345

.field public static final cigna_widget_no_score_text:I = 0x7f090340

.field public static final cigna_widget_score_no_mission_info_text:I = 0x7f090341

.field public static final cigna_woohoo:I = 0x7f090342

.field public static final cigna_you_do_not_finished_lifestyle_assessment:I = 0x7f090343

.field public static final cigna_you_rocked_your_goal:I = 0x7f090344

.field public static final cleared:I = 0x7f09091d

.field public static final climbing_steps:I = 0x7f0907e0

.field public static final close:I = 0x7f090049

.field public static final cm:I = 0x7f0900bc

.field public static final coach:I = 0x7f090029

.field public static final coach_change_acc_restore_failed:I = 0x7f091110

.field public static final coach_not_started_desc_message:I = 0x7f090249

.field public static final coach_not_started_title_message:I = 0x7f090248

.field public static final coach_previous_restore_failed:I = 0x7f09110f

.field public static final coach_shealth_not_started_message:I = 0x7f090247

.field public static final collapse:I = 0x7f090b6d

.field public static final combined_view:I = 0x7f0907db

.field public static final comfort_level_initial_popup_text1:I = 0x7f0910aa

.field public static final comfort_level_initial_popup_text2:I = 0x7f0910ab

.field public static final comfort_level_warning_text:I = 0x7f0910ac

.field public static final comfort_range_must_be_text_c:I = 0x7f0910a6

.field public static final comfort_range_must_be_text_f:I = 0x7f0910a5

.field public static final comfort_range_text:I = 0x7f0910a4

.field public static final comfort_zone_comfortable:I = 0x7f09109e

.field public static final comments:I = 0x7f09007d

.field public static final common_google_play_services_enable_button:I = 0x7f090006

.field public static final common_google_play_services_enable_text:I = 0x7f090005

.field public static final common_google_play_services_enable_title:I = 0x7f090004

.field public static final common_google_play_services_install_button:I = 0x7f090003

.field public static final common_google_play_services_install_text_phone:I = 0x7f090001

.field public static final common_google_play_services_install_text_tablet:I = 0x7f090002

.field public static final common_google_play_services_install_title:I = 0x7f090000

.field public static final common_google_play_services_invalid_account_text:I = 0x7f09000c

.field public static final common_google_play_services_invalid_account_title:I = 0x7f09000b

.field public static final common_google_play_services_network_error_text:I = 0x7f09000a

.field public static final common_google_play_services_network_error_title:I = 0x7f090009

.field public static final common_google_play_services_unknown_issue:I = 0x7f09000d

.field public static final common_google_play_services_unsupported_text:I = 0x7f09000f

.field public static final common_google_play_services_unsupported_title:I = 0x7f09000e

.field public static final common_google_play_services_update_button:I = 0x7f090010

.field public static final common_google_play_services_update_text:I = 0x7f090008

.field public static final common_google_play_services_update_title:I = 0x7f090007

.field public static final common_signin_button_text:I = 0x7f090011

.field public static final common_signin_button_text_long:I = 0x7f090012

.field public static final compare_step_count_with_step_goal:I = 0x7f09110e

.field public static final compatible_accessories:I = 0x7f0907e7

.field public static final compatible_product_text_1:I = 0x7f090114

.field public static final complete:I = 0x7f09099c

.field public static final confirm:I = 0x7f090c8f

.field public static final confirm_password:I = 0x7f090890

.field public static final connect:I = 0x7f09009f

.field public static final connect_accessary:I = 0x7f090180

.field public static final connect_accessory:I = 0x7f090098

.field public static final connect_fail:I = 0x7f090609

.field public static final connect_mobile:I = 0x7f09079d

.field public static final connect_mobile_wlan:I = 0x7f09079c

.field public static final connect_roaming:I = 0x7f09079e

.field public static final connect_to_wlan:I = 0x7f0907a2

.field public static final connected:I = 0x7f090060

.field public static final connected_device_exist_msg:I = 0x7f09009b

.field public static final connected_to_ps:I = 0x7f090095

.field public static final connecting:I = 0x7f0900a4

.field public static final connecting_same_accessory_type_msg:I = 0x7f09009d

.field public static final connecting_third_party_devices_and_programs_text:I = 0x7f090e8b

.field public static final connecting_third_party_devices_and_programs_text_german:I = 0x7f090ec3

.field public static final connecting_third_party_devices_and_programs_text_russia:I = 0x7f090eb1

.field public static final connecting_third_party_devices_and_programs_text_ver2:I = 0x7f090e9a

.field public static final connecting_third_party_devices_and_programs_text_ver2_canada:I = 0x7f090eef

.field public static final connecting_third_party_devices_and_programs_title:I = 0x7f090e8a

.field public static final connecting_third_party_devices_and_programs_title_german:I = 0x7f090ec2

.field public static final connecting_third_party_devices_and_programs_title_ver2:I = 0x7f090e99

.field public static final connection_fail:I = 0x7f090bd3

.field public static final connectivity_accessory_name:I = 0x7f09028c

.field public static final content_steps:I = 0x7f090b8d

.field public static final content_ui_msg:I = 0x7f090272

.field public static final contentui_multiple_message:I = 0x7f090267

.field public static final contentui_single_message:I = 0x7f090268

.field public static final continue_button_text:I = 0x7f090046

.field public static final copyright:I = 0x7f0908c5

.field public static final countdown_to_goal:I = 0x7f090a6d

.field public static final create_challenge:I = 0x7f0908ee

.field public static final create_meal_plan:I = 0x7f09097e

.field public static final current:I = 0x7f09111c

.field public static final current_pace:I = 0x7f0906ff

.field public static final current_wearable_device_on_mygoal:I = 0x7f090902

.field public static final current_weight:I = 0x7f090cb2

.field public static final cycling:I = 0x7f0909d0

.field public static final daily_average_short:I = 0x7f09006f

.field public static final daily_avg:I = 0x7f090ae0

.field public static final daily_calorie_goal:I = 0x7f090ab8

.field public static final daily_goal:I = 0x7f090b8b

.field public static final daily_goal_medal:I = 0x7f090b6b

.field public static final daily_step_goal:I = 0x7f090bd8

.field public static final daily_steps:I = 0x7f090b66

.field public static final daily_values:I = 0x7f090963

.field public static final daily_values_description:I = 0x7f090960

.field public static final dashboard_ecg:I = 0x7f0910e7

.field public static final dashboard_sleep_value:I = 0x7f0910bc

.field public static final data:I = 0x7f0907b5

.field public static final data_backed_up_toast_msg:I = 0x7f090d4e

.field public static final data_connection:I = 0x7f0907a1

.field public static final data_connection_fail:I = 0x7f09079f

.field public static final data_from:I = 0x7f090a8c

.field public static final data_from_accessories:I = 0x7f0906cf

.field public static final data_from_s_health:I = 0x7f0900de

.field public static final data_from_sensor:I = 0x7f090ace

.field public static final data_from_walkingmate:I = 0x7f0906d0

.field public static final data_from_watch:I = 0x7f0906d1

.field public static final data_in_server_notification:I = 0x7f090137

.field public static final data_initialization:I = 0x7f0907e8

.field public static final data_not_in_server_notification:I = 0x7f090138

.field public static final data_received_from_sensor:I = 0x7f09018a

.field public static final data_restored_popup_msg:I = 0x7f090d4f

.field public static final data_synced_from:I = 0x7f0900aa

.field public static final date:I = 0x7f090135

.field public static final date_before_birth:I = 0x7f090084

.field public static final day:I = 0x7f090064

.field public static final days_d:I = 0x7f090d23

.field public static final dd_MMM_yyyy:I = 0x7f090714

.field public static final dd_MMM_yyyy_HH_mm:I = 0x7f090717

.field public static final dd_MMM_yyyy_hh_mm_a:I = 0x7f09071a

.field public static final december:I = 0x7f090107

.field public static final december_short:I = 0x7f090113

.field public static final decilitre_short:I = 0x7f090222

.field public static final decline:I = 0x7f090059

.field public static final default_birthday_toast:I = 0x7f090817

.field public static final default_exercise:I = 0x7f090ab9

.field public static final default_hrm_text_1:I = 0x7f0902e0

.field public static final default_hrm_text_2:I = 0x7f0902e1

.field public static final delete:I = 0x7f090035

.field public static final delete_checkbox:I = 0x7f0905e4

.field public static final delete_data:I = 0x7f090d4a

.field public static final delete_data_msg:I = 0x7f090d4b

.field public static final delete_data_popup_msg:I = 0x7f090d4c

.field public static final delete_entry:I = 0x7f090f1c

.field public static final delete_entry_msg:I = 0x7f090f1d

.field public static final delete_food_info_dialog_text:I = 0x7f090931

.field public static final delete_food_item_information_text:I = 0x7f09093e

.field public static final delete_item_dialog_text:I = 0x7f09078e

.field public static final delete_item_from_pedometer_log:I = 0x7f090f1e

.field public static final delete_previous_data:I = 0x7f0909cd

.field public static final delete_previous_data_text:I = 0x7f0909ce

.field public static final deleted_from_favorites:I = 0x7f0909b9

.field public static final deleted_message_all_items:I = 0x7f09011e

.field public static final deleted_message_one_item:I = 0x7f09011c

.field public static final deleted_message_over_two_items:I = 0x7f09011d

.field public static final deleting:I = 0x7f0900b4

.field public static final delimiter:I = 0x7f090976

.field public static final desc_accessory_bg_1:I = 0x7f090e28

.field public static final desc_accessory_bg_2:I = 0x7f090e29

.field public static final desc_accessory_bg_3:I = 0x7f090e2a

.field public static final desc_accessory_gear_1:I = 0x7f090e22

.field public static final desc_accessory_gear_2:I = 0x7f090e23

.field public static final desc_accessory_hrm_1:I = 0x7f090e1c

.field public static final desc_accessory_hrm_2:I = 0x7f090e1d

.field public static final desc_accessory_hrm_3:I = 0x7f090e1e

.field public static final desc_accessory_hrm_4:I = 0x7f090e1f

.field public static final desc_accessory_hrm_5:I = 0x7f090e20

.field public static final desc_accessory_hrm_6:I = 0x7f090e21

.field public static final desc_accessory_weightscale_1:I = 0x7f090e24

.field public static final desc_accessory_weightscale_2:I = 0x7f090e25

.field public static final desc_accessory_weightscale_bf_1:I = 0x7f090e26

.field public static final desc_accessory_weightscale_bf_2:I = 0x7f090e27

.field public static final description_gear_1:I = 0x7f0901ba

.field public static final description_gear_2:I = 0x7f0901bb

.field public static final description_gear_3:I = 0x7f0901bc

.field public static final description_hrm:I = 0x7f0901be

.field public static final description_samsung_hrm:I = 0x7f0901bd

.field public static final description_text_1:I = 0x7f0905d6

.field public static final description_text_10:I = 0x7f0905e1

.field public static final description_text_2_1:I = 0x7f0905d7

.field public static final description_text_2_2:I = 0x7f0905d8

.field public static final description_text_2_3:I = 0x7f0905d9

.field public static final description_text_3:I = 0x7f0905da

.field public static final description_text_4:I = 0x7f0905db

.field public static final description_text_5:I = 0x7f0905dc

.field public static final description_text_6:I = 0x7f0905dd

.field public static final description_text_7:I = 0x7f0905de

.field public static final description_text_8:I = 0x7f0905df

.field public static final description_text_9:I = 0x7f0905e0

.field public static final description_title:I = 0x7f0905d5

.field public static final deselect_all:I = 0x7f090072

.field public static final details:I = 0x7f09005b

.field public static final detected_accessories:I = 0x7f090091

.field public static final detected_blood_glucose_devices:I = 0x7f090297

.field public static final detected_blood_pressure_devices:I = 0x7f090291

.field public static final detected_hrm_devices:I = 0x7f090a9e

.field public static final detected_weight_devices:I = 0x7f090cd1

.field public static final device_connectivity:I = 0x7f090184

.field public static final device_is_connect_popup:I = 0x7f090791

.field public static final device_name:I = 0x7f0900ad

.field public static final device_null_toast:I = 0x7f0902a4

.field public static final device_pedometer:I = 0x7f090bd4

.field public static final device_storage:I = 0x7f090881

.field public static final devicename:I = 0x7f09029d

.field public static final dialog_without_content_exception:I = 0x7f090196

.field public static final diastolic:I = 0x7f0901b6

.field public static final dietary_fiber:I = 0x7f09096b

.field public static final different_length_exception:I = 0x7f09019a

.field public static final dinner:I = 0x7f090922

.field public static final disable_airplanemode:I = 0x7f090793

.field public static final disable_airplanemode_check:I = 0x7f090794

.field public static final disable_airplanemode_wlan:I = 0x7f090792

.field public static final disable_flight_mode:I = 0x7f0907a4

.field public static final discard_button_text:I = 0x7f090f4d

.field public static final discard_changes:I = 0x7f090081

.field public static final discard_changes_q:I = 0x7f090082

.field public static final disclaimer_of_warranty_text_1:I = 0x7f090e91

.field public static final disclaimer_of_warranty_text_1_2_ver2_canada:I = 0x7f090ef4

.field public static final disclaimer_of_warranty_text_1_french:I = 0x7f090eba

.field public static final disclaimer_of_warranty_text_1_german:I = 0x7f090ec8

.field public static final disclaimer_of_warranty_text_1_russia:I = 0x7f090eb5

.field public static final disclaimer_of_warranty_text_1_ver2:I = 0x7f090ea0

.field public static final disclaimer_of_warranty_text_1_ver2_canada:I = 0x7f090ef3

.field public static final disclaimer_of_warranty_text_2:I = 0x7f090e92

.field public static final disclaimer_of_warranty_text_2_german:I = 0x7f090ec9

.field public static final disclaimer_of_warranty_text_2_russia:I = 0x7f090eb6

.field public static final disclaimer_of_warranty_text_2_ver2:I = 0x7f090ea1

.field public static final disclaimer_of_warranty_title:I = 0x7f090e90

.field public static final disclaimer_of_warranty_title_german:I = 0x7f090ec7

.field public static final disclaimer_of_warranty_title_ver2:I = 0x7f090e9f

.field public static final disclaimer_of_warranty_title_ver2_canada:I = 0x7f090ef2

.field public static final disconnect:I = 0x7f0900a0

.field public static final disconnect_accessory:I = 0x7f090099

.field public static final disconnect_accessory_msg:I = 0x7f09009a

.field public static final distance:I = 0x7f090132

.field public static final distance_goal:I = 0x7f090a21

.field public static final distance_goal_hint:I = 0x7f090a22

.field public static final do_not_show_again:I = 0x7f09078c

.field public static final do_you_want_t0_connect_q:I = 0x7f0900a9

.field public static final done:I = 0x7f090044

.field public static final dosage:I = 0x7f090cf7

.field public static final dosage_alarm:I = 0x7f090cf8

.field public static final double_prime:I = 0x7f0900d4

.field public static final double_tab_to_go:I = 0x7f090bf3

.field public static final double_tap_and_drag_desc:I = 0x7f090adb

.field public static final down:I = 0x7f0901aa

.field public static final download_version_name:I = 0x7f0908c7

.field public static final drawer_menu:I = 0x7f09020e

.field public static final drop_down_button:I = 0x7f090aac

.field public static final duration:I = 0x7f090a1b

.field public static final ea:I = 0x7f0908f6

.field public static final earth:I = 0x7f0906a2

.field public static final eat_meal_plan_alert:I = 0x7f090978

.field public static final eat_meal_plan_alert_breakfast:I = 0x7f090979

.field public static final eat_meal_plan_alert_dinner:I = 0x7f09097b

.field public static final eat_meal_plan_alert_lunch:I = 0x7f09097a

.field public static final eat_meal_plan_alert_snacks:I = 0x7f09097c

.field public static final ecg:I = 0x7f09023e

.field public static final ecg_measure_widget:I = 0x7f0910be

.field public static final edit:I = 0x7f090040

.field public static final edit_comfort_range:I = 0x7f0910a3

.field public static final edit_fav_info:I = 0x7f09090a

.field public static final edit_favorites_care:I = 0x7f09090e

.field public static final edit_favorites_fitness:I = 0x7f09090f

.field public static final edit_home:I = 0x7f090cee

.field public static final edit_memo:I = 0x7f090127

.field public static final edit_profile:I = 0x7f0907b8

.field public static final edit_saved:I = 0x7f090835

.field public static final edit_training_plan:I = 0x7f0906ec

.field public static final elevation:I = 0x7f090a57

.field public static final elevation_low_hight:I = 0x7f090a58

.field public static final elevation_short:I = 0x7f09086b

.field public static final enable_data_roaming:I = 0x7f090799

.field public static final enable_mobile:I = 0x7f090796

.field public static final enable_mobile_data:I = 0x7f0907a5

.field public static final enable_mobile_wlan:I = 0x7f090795

.field public static final enable_roaming:I = 0x7f090798

.field public static final enable_roaming_wlan:I = 0x7f090797

.field public static final end:I = 0x7f090052

.field public static final english_uk:I = 0x7f0909a9

.field public static final english_us:I = 0x7f0909a7

.field public static final enter_food_name:I = 0x7f090941

.field public static final enter_manually:I = 0x7f090685

.field public static final enter_manualy:I = 0x7f0909a4

.field public static final enter_number:I = 0x7f09092e

.field public static final enter_number_range_toast:I = 0x7f09092d

.field public static final enter_password:I = 0x7f09011f

.field public static final enter_the_calories:I = 0x7f0909c3

.field public static final enter_value_between:I = 0x7f090ae4

.field public static final erase_button:I = 0x7f090d47

.field public static final erase_data_and_reset:I = 0x7f090d48

.field public static final erase_popup_msg:I = 0x7f090d49

.field public static final error:I = 0x7f0900d9

.field public static final everyone_rankings_text:I = 0x7f090b73

.field public static final ex_accessories:I = 0x7f090601

.field public static final ex_autosync:I = 0x7f090a18

.field public static final ex_autosync_24:I = 0x7f090bad

.field public static final ex_autosync_off:I = 0x7f090bac

.field public static final ex_calorie:I = 0x7f090b56

.field public static final ex_calroies_out_of_range:I = 0x7f090602

.field public static final ex_calroies_out_of_range_content:I = 0x7f090603

.field public static final ex_device_connectivity_spot:I = 0x7f090baf

.field public static final ex_information_text:I = 0x7f0905ff

.field public static final ex_inputactivity:I = 0x7f090600

.field public static final ex_no_item_frequent:I = 0x7f0905fc

.field public static final ex_no_item_myactivity:I = 0x7f0905fe

.field public static final ex_repetition:I = 0x7f09067e

.field public static final ex_sets:I = 0x7f090681

.field public static final ex_summary_duplicate_popup_text:I = 0x7f090bb2

.field public static final ex_summary_duplicate_popup_title:I = 0x7f090bb1

.field public static final exception_join_toast:I = 0x7f0902a7

.field public static final exercise:I = 0x7f090021

.field public static final exercise_acc:I = 0x7f090186

.field public static final exercise_acc_guid_realtime:I = 0x7f090ad0

.field public static final exercise_acc_guide_kor:I = 0x7f090acf

.field public static final exercise_acc_guide_kor_str:I = 0x7f090ad4

.field public static final exercise_accepted_range_bpm:I = 0x7f090f56

.field public static final exercise_accepted_range_hrs:I = 0x7f090f52

.field public static final exercise_accepted_range_kcal:I = 0x7f090f51

.field public static final exercise_accepted_range_km:I = 0x7f090f6b

.field public static final exercise_accepted_range_miles:I = 0x7f090f6c

.field public static final exercise_accessory_guide:I = 0x7f09089b

.field public static final exercise_american_heart_association:I = 0x7f090866

.field public static final exercise_ascent:I = 0x7f090f5e

.field public static final exercise_average_speed:I = 0x7f090f3f

.field public static final exercise_avg_heart_rate:I = 0x7f090f6a

.field public static final exercise_calorie:I = 0x7f090f40

.field public static final exercise_calorie_goal_entered_out_of_range:I = 0x7f090f54

.field public static final exercise_calories_goal_hint:I = 0x7f090a2c

.field public static final exercise_card_view_data_title:I = 0x7f090f46

.field public static final exercise_card_view_default_cycling:I = 0x7f090f3b

.field public static final exercise_card_view_default_hiking:I = 0x7f090f3c

.field public static final exercise_card_view_default_running:I = 0x7f090f39

.field public static final exercise_card_view_default_title:I = 0x7f090f3d

.field public static final exercise_card_view_default_walking:I = 0x7f090f3a

.field public static final exercise_click:I = 0x7f090fc2

.field public static final exercise_connect_heart_rate_monitor:I = 0x7f090fbf

.field public static final exercise_descent:I = 0x7f090f5f

.field public static final exercise_discard:I = 0x7f090f4c

.field public static final exercise_discard_changes_made_to:I = 0x7f090f4e

.field public static final exercise_distance_goal_entered_out_of_range:I = 0x7f090f50

.field public static final exercise_double_tap_workout:I = 0x7f09109d

.field public static final exercise_ex_device_connectivity_spot:I = 0x7f090bb0

.field public static final exercise_exercise_info_2:I = 0x7f090ad3

.field public static final exercise_finish:I = 0x7f090f62

.field public static final exercise_finish_workout:I = 0x7f090f60

.field public static final exercise_firstbeat_coaching_guide_1:I = 0x7f090f74

.field public static final exercise_firstbeat_coaching_guide_2:I = 0x7f090f75

.field public static final exercise_firstbeat_coaching_guide_3:I = 0x7f090f76

.field public static final exercise_google_play_services_updated:I = 0x7f090fbc

.field public static final exercise_gps_signal_fair:I = 0x7f090a93

.field public static final exercise_gps_signal_off:I = 0x7f090a8e

.field public static final exercise_gps_signal_strong:I = 0x7f090a95

.field public static final exercise_gps_signal_weak:I = 0x7f090a91

.field public static final exercise_hard:I = 0x7f090f78

.field public static final exercise_healthy_heart_rate_range:I = 0x7f090f67

.field public static final exercise_highest_elevation:I = 0x7f090f58

.field public static final exercise_hours:I = 0x7f090f32

.field public static final exercise_hrm_acc_guide:I = 0x7f090f45

.field public static final exercise_hrm_first_measure_warning_4:I = 0x7f090c17

.field public static final exercise_hrm_heart_rate_zone:I = 0x7f090f66

.field public static final exercise_hrm_heart_rate_zone_description:I = 0x7f090f68

.field public static final exercise_hrm_heart_rate_zone_guide_over_range:I = 0x7f090b5b

.field public static final exercise_hrm_heart_rate_zone_guide_under_range:I = 0x7f090b5d

.field public static final exercise_hrm_heart_rate_zone_guide_within_range:I = 0x7f090b5c

.field public static final exercise_improve_fitness_cardiovascular_endurance:I = 0x7f090f65

.field public static final exercise_info_1:I = 0x7f090ad1

.field public static final exercise_info_2:I = 0x7f090ad2

.field public static final exercise_last_hike:I = 0x7f090fb4

.field public static final exercise_last_ride:I = 0x7f090fb3

.field public static final exercise_last_run:I = 0x7f090fb1

.field public static final exercise_last_walk:I = 0x7f090fb2

.field public static final exercise_lock:I = 0x7f090fc4

.field public static final exercise_lowest_elevation:I = 0x7f090f57

.field public static final exercise_maintain_fitness_cardiovascular_endurance:I = 0x7f090f64

.field public static final exercise_maintain_fitness_level:I = 0x7f090aa6

.field public static final exercise_maintain_health_endurance:I = 0x7f090f63

.field public static final exercise_mate:I = 0x7f090cf1

.field public static final exercise_max_duration_workout_hours:I = 0x7f090f55

.field public static final exercise_max_heart_rate:I = 0x7f090f69

.field public static final exercise_minutes:I = 0x7f090f38

.field public static final exercise_moderate:I = 0x7f090f6f

.field public static final exercise_no_network_connection_notification:I = 0x7f090f41

.field public static final exercise_pace:I = 0x7f090f3e

.field public static final exercise_pro:I = 0x7f090cfb

.field public static final exercise_pro_actionbar_running_title:I = 0x7f09060d

.field public static final exercise_pro_running_general:I = 0x7f090b44

.field public static final exercise_pro_training_effect_skip_time_goal:I = 0x7f090a66

.field public static final exercise_prompt_Not_tick:I = 0x7f090fb7

.field public static final exercise_prompt_status_filters:I = 0x7f090fc3

.field public static final exercise_prompt_tab_workout_goal:I = 0x7f090fb0

.field public static final exercise_prompt_tick:I = 0x7f090fb6

.field public static final exercise_prompt_tick_box:I = 0x7f090fb5

.field public static final exercise_realtime_enable_location:I = 0x7f090f7c

.field public static final exercise_realtime_location_off_information:I = 0x7f090f7a

.field public static final exercise_realtime_music_player_no_music:I = 0x7f090f6e

.field public static final exercise_realtime_turn_on_location:I = 0x7f090f7b

.field public static final exercise_save_msg:I = 0x7f090f4f

.field public static final exercise_saved:I = 0x7f090f72

.field public static final exercise_set_effect_running_fitness:I = 0x7f090fbb

.field public static final exercise_set_the_training_day:I = 0x7f090606

.field public static final exercise_share_the_workout_summary:I = 0x7f090f42

.field public static final exercise_shorter_workout_alert_msg:I = 0x7f090f61

.field public static final exercise_show_pace_km_h:I = 0x7f090f5d

.field public static final exercise_show_pace_mins_km:I = 0x7f090f5c

.field public static final exercise_show_pace_mins_mile:I = 0x7f090f5b

.field public static final exercise_showspeed:I = 0x7f090f71

.field public static final exercise_started_training_effect_cycling:I = 0x7f090a32

.field public static final exercise_started_training_effect_hiking:I = 0x7f090a31

.field public static final exercise_started_training_effect_running:I = 0x7f090a2f

.field public static final exercise_started_training_effect_running_follow_audio:I = 0x7f090a2e

.field public static final exercise_started_training_effect_walking:I = 0x7f090a30

.field public static final exercise_step_goal_outside_range:I = 0x7f090f70

.field public static final exercise_stop:I = 0x7f090b5f

.field public static final exercise_tap_buttonview_recordworkout:I = 0x7f090fb9

.field public static final exercise_te_guide_popup_max:I = 0x7f090f44

.field public static final exercise_te_guide_popup_min:I = 0x7f090f43

.field public static final exercise_time_entered_out_of_range:I = 0x7f090f53

.field public static final exercise_total_ascent:I = 0x7f090f59

.field public static final exercise_total_descent:I = 0x7f090f5a

.field public static final exercise_training_effect_goal_hint:I = 0x7f090f37

.field public static final exercise_training_effect_improving:I = 0x7f090f79

.field public static final exercise_training_goal:I = 0x7f090a2d

.field public static final exercise_training_program_arranged_summary:I = 0x7f090700

.field public static final exercise_traininggoal_heartratemonitor:I = 0x7f090fbe

.field public static final exercise_trainning_goal_levlels:I = 0x7f090f77

.field public static final exercise_type:I = 0x7f090b5e

.field public static final exercise_unable_to_add_picture:I = 0x7f090f73

.field public static final exercise_unlock:I = 0x7f090fc5

.field public static final exercise_update_exercise_play:I = 0x7f090fbd

.field public static final exercise_url_open_text:I = 0x7f090fc1

.field public static final exercise_wearable_record_workout_sync:I = 0x7f090fba

.field public static final exercise_why_not_work_out:I = 0x7f090fc0

.field public static final exercise_workout_result_map:I = 0x7f090fb8

.field public static final expand:I = 0x7f090b6e

.field public static final expand_list:I = 0x7f09021c

.field public static final export_completed:I = 0x7f09087f

.field public static final export_data:I = 0x7f0907e9

.field public static final export_data_text:I = 0x7f0907d5

.field public static final export_message:I = 0x7f09087d

.field public static final export_no_application:I = 0x7f0907d7

.field public static final export_save_path:I = 0x7f0907d6

.field public static final facebook_share:I = 0x7f0901f4

.field public static final fail_do_you_want_tty_again:I = 0x7f0909b5

.field public static final failed_to_backup_text:I = 0x7f0907f8

.field public static final failed_to_connect_accessory:I = 0x7f09009c

.field public static final failed_to_restore:I = 0x7f0907f3

.field public static final failed_to_restore_nobackup:I = 0x7f0907f6

.field public static final failed_to_restore_nobackup_notification:I = 0x7f0907fe

.field public static final failed_to_restore_text:I = 0x7f0907f7

.field public static final farscape:I = 0x7f090611

.field public static final fast_walking:I = 0x7f090b42

.field public static final fastest:I = 0x7f090ac3

.field public static final fasting:I = 0x7f0901af

.field public static final fat:I = 0x7f0909cb

.field public static final favorites:I = 0x7f090088

.field public static final favourite:I = 0x7f090087

.field public static final features:I = 0x7f090897

.field public static final february:I = 0x7f0900fd

.field public static final february_short:I = 0x7f090109

.field public static final female:I = 0x7f09081f

.field public static final first:I = 0x7f090b78

.field public static final firstbeat:I = 0x7f090a9d

.field public static final firstbeat_capital:I = 0x7f090d1b

.field public static final firstbeat_coach_feedback_1:I = 0x7f090166

.field public static final firstbeat_coach_feedback_10:I = 0x7f09016f

.field public static final firstbeat_coach_feedback_11:I = 0x7f090170

.field public static final firstbeat_coach_feedback_13:I = 0x7f090171

.field public static final firstbeat_coach_feedback_14:I = 0x7f090172

.field public static final firstbeat_coach_feedback_15:I = 0x7f090173

.field public static final firstbeat_coach_feedback_2:I = 0x7f090167

.field public static final firstbeat_coach_feedback_3:I = 0x7f090168

.field public static final firstbeat_coach_feedback_4:I = 0x7f090169

.field public static final firstbeat_coach_feedback_5:I = 0x7f09016a

.field public static final firstbeat_coach_feedback_6:I = 0x7f09016b

.field public static final firstbeat_coach_feedback_7:I = 0x7f09016c

.field public static final firstbeat_coach_feedback_8:I = 0x7f09016d

.field public static final firstbeat_coach_feedback_9:I = 0x7f09016e

.field public static final firstbeat_coach_fitness_level_1:I = 0x7f090174

.field public static final firstbeat_coach_fitness_level_2:I = 0x7f090175

.field public static final firstbeat_coach_fitness_level_3:I = 0x7f090176

.field public static final firstbeat_coach_fitness_level_4:I = 0x7f090177

.field public static final firstbeat_coach_fitness_level_5:I = 0x7f090178

.field public static final firstbeat_coach_fitness_level_6:I = 0x7f090179

.field public static final firstbeat_coach_fitness_level_7:I = 0x7f09017a

.field public static final firstbeat_coach_longterm_rest:I = 0x7f090151

.field public static final firstbeat_coach_longterm_running:I = 0x7f090150

.field public static final firstbeat_coach_next_workout_1:I = 0x7f09015c

.field public static final firstbeat_coach_next_workout_10:I = 0x7f090165

.field public static final firstbeat_coach_next_workout_2:I = 0x7f09015d

.field public static final firstbeat_coach_next_workout_3:I = 0x7f09015e

.field public static final firstbeat_coach_next_workout_4:I = 0x7f09015f

.field public static final firstbeat_coach_next_workout_5:I = 0x7f090160

.field public static final firstbeat_coach_next_workout_6:I = 0x7f090161

.field public static final firstbeat_coach_next_workout_7:I = 0x7f090162

.field public static final firstbeat_coach_next_workout_8:I = 0x7f090163

.field public static final firstbeat_coach_next_workout_9:I = 0x7f090164

.field public static final firstbeat_coach_next_workout_title_1:I = 0x7f090152

.field public static final firstbeat_coach_next_workout_title_10:I = 0x7f09015b

.field public static final firstbeat_coach_next_workout_title_2:I = 0x7f090153

.field public static final firstbeat_coach_next_workout_title_3:I = 0x7f090154

.field public static final firstbeat_coach_next_workout_title_4:I = 0x7f090155

.field public static final firstbeat_coach_next_workout_title_5:I = 0x7f090156

.field public static final firstbeat_coach_next_workout_title_6:I = 0x7f090157

.field public static final firstbeat_coach_next_workout_title_7:I = 0x7f090158

.field public static final firstbeat_coach_next_workout_title_8:I = 0x7f090159

.field public static final firstbeat_coach_next_workout_title_9:I = 0x7f09015a

.field public static final firstbeat_ete_gudie_335_1:I = 0x7f09013f

.field public static final firstbeat_ete_gudie_335_10:I = 0x7f090144

.field public static final firstbeat_ete_gudie_335_11:I = 0x7f090145

.field public static final firstbeat_ete_gudie_335_14:I = 0x7f090146

.field public static final firstbeat_ete_gudie_335_2:I = 0x7f090140

.field public static final firstbeat_ete_gudie_335_20:I = 0x7f090147

.field public static final firstbeat_ete_gudie_335_22:I = 0x7f090148

.field public static final firstbeat_ete_gudie_335_23:I = 0x7f090149

.field public static final firstbeat_ete_gudie_335_24:I = 0x7f09014a

.field public static final firstbeat_ete_gudie_335_25:I = 0x7f09014b

.field public static final firstbeat_ete_gudie_335_3:I = 0x7f090141

.field public static final firstbeat_ete_gudie_335_30:I = 0x7f09014c

.field public static final firstbeat_ete_gudie_335_31:I = 0x7f09014d

.field public static final firstbeat_ete_gudie_335_32:I = 0x7f09014e

.field public static final firstbeat_ete_gudie_335_33:I = 0x7f09014f

.field public static final firstbeat_ete_gudie_335_6:I = 0x7f090142

.field public static final firstbeat_ete_gudie_335_7:I = 0x7f090143

.field public static final firstbeat_min:I = 0x7f09013a

.field public static final fitness_bike:I = 0x7f090a72

.field public static final fitness_climber:I = 0x7f090a74

.field public static final fitness_elliptical:I = 0x7f0906e0

.field public static final fitness_equipment:I = 0x7f090bb3

.field public static final fitness_equipment_connect_guide:I = 0x7f090a37

.field public static final fitness_equipment_connect_guide_1:I = 0x7f0906da

.field public static final fitness_equipment_connect_guide_2:I = 0x7f0906db

.field public static final fitness_equipment_connected:I = 0x7f090702

.field public static final fitness_equipment_connecting:I = 0x7f090701

.field public static final fitness_equipment_disconnecting:I = 0x7f090703

.field public static final fitness_equipment_stop_guide:I = 0x7f090a38

.field public static final fitness_equipment_text:I = 0x7f0902dd

.field public static final fitness_level:I = 0x7f090a7a

.field public static final fitness_level_description:I = 0x7f090706

.field public static final fitness_nordic_skier:I = 0x7f090a75

.field public static final fitness_rower:I = 0x7f090a73

.field public static final fitness_treadmill:I = 0x7f090a71

.field public static final food:I = 0x7f090028

.field public static final food_add_to_favourites:I = 0x7f09092a

.field public static final food_boohee_root_category_dish:I = 0x7f09078a

.field public static final food_boohee_root_category_food:I = 0x7f090789

.field public static final food_invalid_emoticon_toast_text:I = 0x7f09092c

.field public static final food_meal_discarded_toast:I = 0x7f090940

.field public static final food_medal:I = 0x7f0908e9

.field public static final food_name_is_already_exist:I = 0x7f09098f

.field public static final food_remove_from_favourites:I = 0x7f09092b

.field public static final food_tracker_activity_try_it_finish_stet_1:I = 0x7f090d01

.field public static final food_unable_to_save:I = 0x7f09093f

.field public static final foods:I = 0x7f09091f

.field public static final force_stop_workout:I = 0x7f090ab6

.field public static final force_stop_workout_msg:I = 0x7f090ab7

.field public static final foreground_service_started:I = 0x7f091119

.field public static final forerunner_watch_text:I = 0x7f0902d9

.field public static final forerunner_watch_text_1:I = 0x7f0902da

.field public static final forgot_password:I = 0x7f090121

.field public static final forgot_password_message:I = 0x7f090124

.field public static final forgot_password_message_d:I = 0x7f090123

.field public static final format_date_month_year_period:I = 0x7f0901a0

.field public static final format_month_date:I = 0x7f09019e

.field public static final format_mu_year_month_date:I = 0x7f0901a1

.field public static final format_year:I = 0x7f09019f

.field public static final format_year_month:I = 0x7f09019d

.field public static final format_year_month_date:I = 0x7f0901a2

.field public static final format_year_month_date_day:I = 0x7f09019c

.field public static final french:I = 0x7f0909a8

.field public static final frequency_of_0_25km:I = 0x7f0906d2

.field public static final frequency_of_0_5km:I = 0x7f0906fb

.field public static final frequency_of_10min:I = 0x7f0906d6

.field public static final frequency_of_1hr:I = 0x7f0906fe

.field public static final frequency_of_1km:I = 0x7f0906d3

.field public static final frequency_of_2km:I = 0x7f0906d4

.field public static final frequency_of_30min:I = 0x7f0906d7

.field public static final frequency_of_5min:I = 0x7f0906d5

.field public static final frequency_of_audio_guide:I = 0x7f0906dc

.field public static final frequency_of_average_pace_meter:I = 0x7f0905f1

.field public static final frequency_of_average_pace_mile:I = 0x7f0905f2

.field public static final frequency_of_average_speed_meter:I = 0x7f0905ed

.field public static final frequency_of_average_speed_mile:I = 0x7f0905ee

.field public static final frequency_of_cadence:I = 0x7f0905f8

.field public static final frequency_of_caloires:I = 0x7f0905f3

.field public static final frequency_of_current_pace_meter:I = 0x7f0905ef

.field public static final frequency_of_current_pace_mile:I = 0x7f0905f0

.field public static final frequency_of_current_speed_meter:I = 0x7f0905eb

.field public static final frequency_of_current_speed_mile:I = 0x7f0905ec

.field public static final frequency_of_distance_meter:I = 0x7f0905e9

.field public static final frequency_of_distance_mile:I = 0x7f0905ea

.field public static final frequency_of_duration:I = 0x7f0905e8

.field public static final frequency_of_every_km:I = 0x7f0906fc

.field public static final frequency_of_every_min:I = 0x7f0906fd

.field public static final frequency_of_goal_duration:I = 0x7f0905f6

.field public static final frequency_of_goal_meter:I = 0x7f0905f4

.field public static final frequency_of_goal_mile:I = 0x7f0905f5

.field public static final frequency_of_heart_rate:I = 0x7f0905f7

.field public static final frequency_of_never:I = 0x7f0906fa

.field public static final frequent:I = 0x7f090085

.field public static final fri:I = 0x7f0900f2

.field public static final fri_short:I = 0x7f0900f9

.field public static final friday_three_chars:I = 0x7f090d0a

.field public static final from_s_health:I = 0x7f0900dd

.field public static final ft:I = 0x7f090a1e

.field public static final ft_inch:I = 0x7f0900bd

.field public static final future_date_alert_message:I = 0x7f090118

.field public static final ga_trackingId:I = 0x7f09001c

.field public static final galaxy_gear:I = 0x7f090b63

.field public static final gallery:I = 0x7f0900e0

.field public static final gallery_not_enabled:I = 0x7f090936

.field public static final garmin_device:I = 0x7f090e10

.field public static final garmin_device_hrm:I = 0x7f090e11

.field public static final garmin_device_hrm_premium:I = 0x7f090e12

.field public static final garmin_device_link:I = 0x7f090dfe

.field public static final gear:I = 0x7f090190

.field public static final gear_2:I = 0x7f090bc3

.field public static final gear_2_steps:I = 0x7f090bc8

.field public static final gear_2_steps_without_unit:I = 0x7f090bcc

.field public static final gear_device:I = 0x7f0907cf

.field public static final gear_devices:I = 0x7f090bbf

.field public static final gear_fit:I = 0x7f090bc2

.field public static final gear_fit_steps:I = 0x7f090bc6

.field public static final gear_fit_steps_without_unit:I = 0x7f090bce

.field public static final gear_manager:I = 0x7f090191

.field public static final gear_o:I = 0x7f090bc1

.field public static final gear_o_steps:I = 0x7f090bc5

.field public static final gear_o_steps_without_unit:I = 0x7f090bcb

.field public static final gear_s:I = 0x7f090bc0

.field public static final gear_s_steps:I = 0x7f090bc4

.field public static final gear_s_steps_without_unit:I = 0x7f090bca

.field public static final gear_series:I = 0x7f090bbe

.field public static final gear_steps:I = 0x7f090bc7

.field public static final gear_steps_without_unit:I = 0x7f090bcd

.field public static final general:I = 0x7f0907b3

.field public static final general_setting:I = 0x7f090876

.field public static final german:I = 0x7f0909aa

.field public static final go_to_baidu_search:I = 0x7f09095e

.field public static final go_to_google_search:I = 0x7f09095d

.field public static final goal:I = 0x7f09006b

.field public static final goal_achieved_filter_name:I = 0x7f090077

.field public static final goal_activity_accept_goal_edit_profile:I = 0x7f091137

.field public static final goal_activity_accept_goal_related_tracker_main:I = 0x7f091135

.field public static final goal_activity_accept_goal_related_tracker_optional:I = 0x7f091136

.field public static final goal_activity_chandlechart_goal_txt:I = 0x7f091131

.field public static final goal_activity_info_for_adults_title:I = 0x7f091134

.field public static final goal_activity_info_for_youth_title:I = 0x7f091133

.field public static final goal_activity_info_text:I = 0x7f091132

.field public static final goal_calories:I = 0x7f090924

.field public static final goal_calories_range_toast:I = 0x7f0909bb

.field public static final goal_not_achieved_filter_name:I = 0x7f090078

.field public static final goal_out_of_range:I = 0x7f090b60

.field public static final goal_out_of_range_dialog_text_1:I = 0x7f0909bc

.field public static final goal_out_of_range_dialog_text_2:I = 0x7f0909bd

.field public static final goal_upper:I = 0x7f09006c

.field public static final goal_weight:I = 0x7f090cb0

.field public static final goal_weight_caps:I = 0x7f090cb1

.field public static final goal_with_colon:I = 0x7f090b8c

.field public static final goalsaved:I = 0x7f090ba6

.field public static final gps_not_found:I = 0x7f090a96

.field public static final gps_signal_fair:I = 0x7f090a92

.field public static final gps_signal_locating:I = 0x7f090a8f

.field public static final gps_signal_off:I = 0x7f090a8d

.field public static final gps_signal_strong:I = 0x7f090a94

.field public static final gps_signal_weak:I = 0x7f090a90

.field public static final gram_unit:I = 0x7f0909c9

.field public static final grams_short:I = 0x7f0900bf

.field public static final graph_information_area_date_format_day:I = 0x7f090256

.field public static final graph_information_area_date_format_hour:I = 0x7f090255

.field public static final graph_information_area_date_format_month:I = 0x7f090257

.field public static final graph_information_area_time_format_long:I = 0x7f090258

.field public static final graph_information_area_time_format_short:I = 0x7f090259

.field public static final gym_mode:I = 0x7f09067f

.field public static final head_to_head:I = 0x7f090b80

.field public static final header_samsung_account:I = 0x7f09086f

.field public static final header_text:I = 0x7f090fc7

.field public static final health_account_temrs_1:I = 0x7f090e2c

.field public static final health_account_temrs_10:I = 0x7f090e35

.field public static final health_account_temrs_10_ver2:I = 0x7f090e72

.field public static final health_account_temrs_10_ver2_canada:I = 0x7f090efc

.field public static final health_account_temrs_10_ver2_south_africa:I = 0x7f090ee2

.field public static final health_account_temrs_11:I = 0x7f090e36

.field public static final health_account_temrs_11_ver2:I = 0x7f090e73

.field public static final health_account_temrs_12:I = 0x7f090e37

.field public static final health_account_temrs_12_ver2:I = 0x7f090e74

.field public static final health_account_temrs_12_ver2_canada:I = 0x7f090efd

.field public static final health_account_temrs_13:I = 0x7f090e38

.field public static final health_account_temrs_13_ver2:I = 0x7f090e75

.field public static final health_account_temrs_13_ver2_canada:I = 0x7f090efe

.field public static final health_account_temrs_14:I = 0x7f090e39

.field public static final health_account_temrs_14_ver2:I = 0x7f090e76

.field public static final health_account_temrs_14_ver2_canada:I = 0x7f090eff

.field public static final health_account_temrs_15:I = 0x7f090e3a

.field public static final health_account_temrs_15_ver2:I = 0x7f090e77

.field public static final health_account_temrs_15_ver2_canada:I = 0x7f090f00

.field public static final health_account_temrs_16:I = 0x7f090e3b

.field public static final health_account_temrs_16_ver2:I = 0x7f090e78

.field public static final health_account_temrs_16_ver2_canada:I = 0x7f090f01

.field public static final health_account_temrs_17:I = 0x7f090e3c

.field public static final health_account_temrs_17_ver2:I = 0x7f090e79

.field public static final health_account_temrs_17_ver2_canada:I = 0x7f090f02

.field public static final health_account_temrs_18:I = 0x7f090e3d

.field public static final health_account_temrs_18_ver2:I = 0x7f090e7a

.field public static final health_account_temrs_18_ver2_canada:I = 0x7f090f03

.field public static final health_account_temrs_19:I = 0x7f090e3e

.field public static final health_account_temrs_19_ver2:I = 0x7f090e7b

.field public static final health_account_temrs_19_ver2_canada:I = 0x7f090f04

.field public static final health_account_temrs_1_ver2:I = 0x7f090e3f

.field public static final health_account_temrs_2:I = 0x7f090e2d

.field public static final health_account_temrs_2_ver2:I = 0x7f090e40

.field public static final health_account_temrs_3:I = 0x7f090e2e

.field public static final health_account_temrs_3_ver2:I = 0x7f090e41

.field public static final health_account_temrs_4:I = 0x7f090e2f

.field public static final health_account_temrs_4_ver2:I = 0x7f090e42

.field public static final health_account_temrs_5:I = 0x7f090e30

.field public static final health_account_temrs_5_ver2:I = 0x7f090e6d

.field public static final health_account_temrs_6:I = 0x7f090e31

.field public static final health_account_temrs_6_ver2:I = 0x7f090e6e

.field public static final health_account_temrs_6_ver2_canada:I = 0x7f090ef9

.field public static final health_account_temrs_7:I = 0x7f090e32

.field public static final health_account_temrs_7_ver2:I = 0x7f090e6f

.field public static final health_account_temrs_7_ver2_canada:I = 0x7f090efa

.field public static final health_account_temrs_8:I = 0x7f090e33

.field public static final health_account_temrs_8_ver2:I = 0x7f090e70

.field public static final health_account_temrs_8_ver2_canada:I = 0x7f090efb

.field public static final health_account_temrs_9:I = 0x7f090e34

.field public static final health_account_temrs_9_ver2:I = 0x7f090e71

.field public static final health_account_temrs_header:I = 0x7f090e2b

.field public static final health_account_terms_10_ver2_albania:I = 0x7f090ed1

.field public static final health_account_terms_10_ver2_bosnia:I = 0x7f090ed5

.field public static final health_account_terms_10_ver2_france:I = 0x7f090edc

.field public static final health_account_terms_10_ver2_montenegrin:I = 0x7f090ed9

.field public static final health_account_terms_10_ver2_spain:I = 0x7f090eac

.field public static final health_calendar:I = 0x7f090ce0

.field public static final health_care_input_activity_delete_dialog_text:I = 0x7f09028b

.field public static final health_care_summary_view_default_value:I = 0x7f09027e

.field public static final health_care_summary_view_value_default_mask_float:I = 0x7f09027f

.field public static final health_care_summary_view_value_default_mask_integer:I = 0x7f090280

.field public static final health_content_title_received_data:I = 0x7f090266

.field public static final health_content_title_start_recording:I = 0x7f090265

.field public static final health_diary:I = 0x7f0908f2

.field public static final health_service:I = 0x7f090239

.field public static final health_service_name:I = 0x7f090273

.field public static final health_star_chn:I = 0x7f090f7d

.field public static final healthpro_text:I = 0x7f0910d7

.field public static final healthservice_update:I = 0x7f0907aa

.field public static final healthyStep_popup_text:I = 0x7f090b70

.field public static final healthy_pace:I = 0x7f0907e4

.field public static final healthy_steps_txt:I = 0x7f0907d8

.field public static final heart_rate:I = 0x7f090022

.field public static final heart_rate_double_tap_add:I = 0x7f0907b1

.field public static final heart_rate_double_tap_change:I = 0x7f0907b0

.field public static final heart_rate_monitor:I = 0x7f090024

.field public static final heavy_exercise_msg:I = 0x7f090863

.field public static final hebrewtext:I = 0x7f09071e

.field public static final height:I = 0x7f09012f

.field public static final hello_world:I = 0x7f09001b

.field public static final help:I = 0x7f090039

.field public static final help_download:I = 0x7f090115

.field public static final high:I = 0x7f090a6b

.field public static final hiking:I = 0x7f0909d1

.field public static final home:I = 0x7f09002b

.field public static final home_change_background:I = 0x7f090907

.field public static final home_connect_wearable_text:I = 0x7f09090d

.field public static final home_edit_favorites:I = 0x7f090909

.field public static final home_edit_talkback_prompt_drag:I = 0x7f090cf0

.field public static final home_edit_talkback_prompt_drop:I = 0x7f090cef

.field public static final home_set_background:I = 0x7f090908

.field public static final homesensoreasywidget:I = 0x7f090fc8

.field public static final homesensorinitialview:I = 0x7f090f49

.field public static final homesensorwidget:I = 0x7f090f4b

.field public static final hour:I = 0x7f090065

.field public static final hour_minute_format:I = 0x7f0907ce

.field public static final hour_short:I = 0x7f0900e9

.field public static final hours:I = 0x7f0900e8

.field public static final hours_d:I = 0x7f090d24

.field public static final how_to_use:I = 0x7f09005c

.field public static final hrm_acc_guide:I = 0x7f090ae8

.field public static final hrm_accessory_type:I = 0x7f090c19

.field public static final hrm_add_tags:I = 0x7f090f11

.field public static final hrm_after_meal:I = 0x7f090f0a

.field public static final hrm_after_wakeup:I = 0x7f090f09

.field public static final hrm_angry:I = 0x7f090f24

.field public static final hrm_at_work:I = 0x7f090f0c

.field public static final hrm_average:I = 0x7f090c30

.field public static final hrm_avg:I = 0x7f090f20

.field public static final hrm_avg_bpm:I = 0x7f090f16

.field public static final hrm_avg_resting_range:I = 0x7f090c21

.field public static final hrm_back_key_finish:I = 0x7f090c18

.field public static final hrm_coffee:I = 0x7f090f2f

.field public static final hrm_create_tag:I = 0x7f090f2c

.field public static final hrm_custom_tag:I = 0x7f090f28

.field public static final hrm_cycling:I = 0x7f090f0f

.field public static final hrm_dialog_summary_reset_message:I = 0x7f090c11

.field public static final hrm_discard_reading:I = 0x7f090f93

.field public static final hrm_disclaimer_for_gear:I = 0x7f091117

.field public static final hrm_drinking:I = 0x7f090f30

.field public static final hrm_emotion_tag:I = 0x7f090f29

.field public static final hrm_enter_tag:I = 0x7f090f2a

.field public static final hrm_enter_tag_exists:I = 0x7f090f2b

.field public static final hrm_error_do_not_move:I = 0x7f090c26

.field public static final hrm_error_finger_motion:I = 0x7f090c25

.field public static final hrm_error_finger_pressure:I = 0x7f090c27

.field public static final hrm_error_first_message:I = 0x7f090c28

.field public static final hrm_excited:I = 0x7f090f22

.field public static final hrm_exercise:I = 0x7f090f2d

.field public static final hrm_fearful:I = 0x7f090f26

.field public static final hrm_find_sensor:I = 0x7f090c13

.field public static final hrm_finished:I = 0x7f090c06

.field public static final hrm_first_measure_warning_1:I = 0x7f090c14

.field public static final hrm_first_measure_warning_2:I = 0x7f090c15

.field public static final hrm_first_measure_warning_3:I = 0x7f090c16

.field public static final hrm_font_light:I = 0x7f090d17

.field public static final hrm_font_medium:I = 0x7f090d18

.field public static final hrm_font_regular:I = 0x7f090d16

.field public static final hrm_heart_rate:I = 0x7f09111d

.field public static final hrm_heart_rate_acccessories_guide:I = 0x7f090c29

.field public static final hrm_heart_rate_acccessories_guide_second:I = 0x7f090c2c

.field public static final hrm_heart_rate_device_guide:I = 0x7f090c2a

.field public static final hrm_heart_rate_device_guide_second:I = 0x7f090c2b

.field public static final hrm_heart_rate_guide_for_flip_cover:I = 0x7f090c2d

.field public static final hrm_heartrate_accessories:I = 0x7f09017d

.field public static final hrm_hiking:I = 0x7f090f10

.field public static final hrm_in_love:I = 0x7f090f27

.field public static final hrm_information_fifth:I = 0x7f090c1e

.field public static final hrm_information_first:I = 0x7f090c1a

.field public static final hrm_information_fourth:I = 0x7f090c1d

.field public static final hrm_information_second:I = 0x7f090c1b

.field public static final hrm_information_sixth:I = 0x7f090c1f

.field public static final hrm_information_third:I = 0x7f090c1c

.field public static final hrm_log_no_data_message:I = 0x7f090c10

.field public static final hrm_max:I = 0x7f090c2e

.field public static final hrm_max_bpm:I = 0x7f090f14

.field public static final hrm_measuring:I = 0x7f090c04

.field public static final hrm_measuring_message:I = 0x7f090c23

.field public static final hrm_measuring_message_error:I = 0x7f090c24

.field public static final hrm_min:I = 0x7f090c2f

.field public static final hrm_min_bpm:I = 0x7f090f15

.field public static final hrm_more_tags:I = 0x7f090c0f

.field public static final hrm_next_line:I = 0x7f090d14

.field public static final hrm_no_data:I = 0x7f090f17

.field public static final hrm_no_tagged:I = 0x7f090c0d

.field public static final hrm_non_medical_disclaimer:I = 0x7f090c08

.field public static final hrm_not_avg_resting_range:I = 0x7f090c20

.field public static final hrm_party:I = 0x7f090f2e

.field public static final hrm_percent:I = 0x7f090d15

.field public static final hrm_ready:I = 0x7f090c05

.field public static final hrm_relaxing:I = 0x7f090f0b

.field public static final hrm_reset:I = 0x7f090c12

.field public static final hrm_running:I = 0x7f090f0d

.field public static final hrm_sad:I = 0x7f090f25

.field public static final hrm_scover_openinfo:I = 0x7f090afe

.field public static final hrm_searching_finger_message:I = 0x7f090c22

.field public static final hrm_second_measure_warning:I = 0x7f090c07

.field public static final hrm_select_tag_icon:I = 0x7f0902b4

.field public static final hrm_select_tag_tapcancel:I = 0x7f09112d

.field public static final hrm_smoking:I = 0x7f090f31

.field public static final hrm_sort_by_tag:I = 0x7f090f13

.field public static final hrm_surprise:I = 0x7f090f23

.field public static final hrm_tag_add:I = 0x7f090c0b

.field public static final hrm_tag_edit:I = 0x7f090c0a

.field public static final hrm_tag_general:I = 0x7f090f08

.field public static final hrm_tag_icon_aeroplane:I = 0x7f0910fa

.field public static final hrm_tag_icon_angry:I = 0x7f0910ee

.field public static final hrm_tag_icon_baby_boy:I = 0x7f0910fc

.field public static final hrm_tag_icon_baby_girl:I = 0x7f0910fd

.field public static final hrm_tag_icon_beer:I = 0x7f0910f5

.field public static final hrm_tag_icon_boy:I = 0x7f0910fe

.field public static final hrm_tag_icon_cat:I = 0x7f091107

.field public static final hrm_tag_icon_cocktail:I = 0x7f0910f3

.field public static final hrm_tag_icon_coffee:I = 0x7f0910f4

.field public static final hrm_tag_icon_company:I = 0x7f0910f8

.field public static final hrm_tag_icon_conference:I = 0x7f0910fb

.field public static final hrm_tag_icon_dog:I = 0x7f091106

.field public static final hrm_tag_icon_exercise:I = 0x7f0910f2

.field public static final hrm_tag_icon_fearful:I = 0x7f0910f0

.field public static final hrm_tag_icon_general:I = 0x7f0910e8

.field public static final hrm_tag_icon_girl:I = 0x7f0910ff

.field public static final hrm_tag_icon_happy:I = 0x7f0910f1

.field public static final hrm_tag_icon_horse_riding:I = 0x7f091108

.field public static final hrm_tag_icon_man:I = 0x7f091102

.field public static final hrm_tag_icon_old_man:I = 0x7f091104

.field public static final hrm_tag_icon_old_woman:I = 0x7f091105

.field public static final hrm_tag_icon_pills:I = 0x7f0910f7

.field public static final hrm_tag_icon_pleasure:I = 0x7f0910ed

.field public static final hrm_tag_icon_presentation:I = 0x7f0910f9

.field public static final hrm_tag_icon_running:I = 0x7f0910eb

.field public static final hrm_tag_icon_sad:I = 0x7f0910ef

.field public static final hrm_tag_icon_smoking_symbol:I = 0x7f0910f6

.field public static final hrm_tag_icon_surprised:I = 0x7f0910ec

.field public static final hrm_tag_icon_tropical_island:I = 0x7f0910e9

.field public static final hrm_tag_icon_walking:I = 0x7f0910ea

.field public static final hrm_tag_icon_woman:I = 0x7f091103

.field public static final hrm_tag_icon_young_man:I = 0x7f091100

.field public static final hrm_tag_icon_young_woman:I = 0x7f091101

.field public static final hrm_tag_limit_reached:I = 0x7f090f21

.field public static final hrm_tag_not_selected:I = 0x7f09110a

.field public static final hrm_tag_select:I = 0x7f090c09

.field public static final hrm_tag_selected:I = 0x7f091109

.field public static final hrm_tag_your_status:I = 0x7f090c0e

.field public static final hrm_text_1:I = 0x7f0902db

.field public static final hrm_text_2:I = 0x7f0902dc

.field public static final hrm_time_date:I = 0x7f090c03

.field public static final hrm_view_by_tag:I = 0x7f090f12

.field public static final hrm_walking:I = 0x7f090f0e

.field public static final hrm_warning_message:I = 0x7f090d04

.field public static final hrm_wearable_tag_noedit:I = 0x7f09112c

.field public static final humidity:I = 0x7f0910a2

.field public static final hyphen:I = 0x7f09109c

.field public static final i_agree:I = 0x7f0908c1

.field public static final image_accessories_icon:I = 0x7f090250

.field public static final image_expand_button:I = 0x7f09024e

.field public static final image_file_is_not_supported:I = 0x7f090990

.field public static final image_goal_decrement_button:I = 0x7f09024b

.field public static final image_goal_increment_button:I = 0x7f09024c

.field public static final image_gold_medal:I = 0x7f090251

.field public static final image_intake_volume_background:I = 0x7f09024d

.field public static final image_list_item_dot:I = 0x7f09024a

.field public static final image_memo_icon:I = 0x7f09024f

.field public static final image_open_log_window:I = 0x7f090252

.field public static final in_the_world:I = 0x7f090b85

.field public static final in_your_age_group:I = 0x7f090b86

.field public static final inactive_1_hr:I = 0x7f090be8

.field public static final inactive_1_hr_1_min:I = 0x7f090be9

.field public static final inactive_1_hr_1_min_arab:I = 0x7f090bf6

.field public static final inactive_1_hr_arab:I = 0x7f090bf5

.field public static final inactive_1_hr_pd_mins:I = 0x7f090bea

.field public static final inactive_1_hr_pd_mins_arab:I = 0x7f090bf7

.field public static final inactive_for_over_1_hr:I = 0x7f090bf0

.field public static final inactive_for_over_1_hr_arab:I = 0x7f090bf9

.field public static final inactive_for_over_1_hr_pd_mins:I = 0x7f090bee

.field public static final inactive_for_over_1_hr_pd_mins_arab:I = 0x7f090bfa

.field public static final inactive_for_over_pd_hrs:I = 0x7f090bf1

.field public static final inactive_for_over_pd_hrs_pd_mins:I = 0x7f090bef

.field public static final inactive_for_over_pd_mins:I = 0x7f090bf2

.field public static final inactive_pd_hrs:I = 0x7f090beb

.field public static final inactive_pd_hrs_1_min:I = 0x7f090bec

.field public static final inactive_pd_hrs_1_min_arab:I = 0x7f090bf8

.field public static final inactive_pd_hrs_pd_mins:I = 0x7f090bed

.field public static final inactive_pd_mins:I = 0x7f090be7

.field public static final inactive_period:I = 0x7f0907c5

.field public static final inactive_period_option_1hr:I = 0x7f0907c9

.field public static final inactive_period_option_1hr_30min:I = 0x7f0907ca

.field public static final inactive_period_option_2hrs:I = 0x7f0907cb

.field public static final inactive_period_option_30min:I = 0x7f0907c8

.field public static final inactive_period_option_manual:I = 0x7f0907cc

.field public static final inactive_period_option_period_format:I = 0x7f0907c6

.field public static final inactive_period_option_set_time_txt:I = 0x7f0907cd

.field public static final inactive_period_option_time_always:I = 0x7f0907c7

.field public static final inactive_period_same_alarm:I = 0x7f0907d0

.field public static final inactive_time:I = 0x7f0907e3

.field public static final inactive_time_txt:I = 0x7f0907c0

.field public static final incorrect_password_5_times:I = 0x7f090125

.field public static final indoors:I = 0x7f090687

.field public static final infinity:I = 0x7f090b77

.field public static final infopia_step_1:I = 0x7f0902c1

.field public static final information:I = 0x7f0900e3

.field public static final information_footer:I = 0x7f090f91

.field public static final information_number:I = 0x7f090e04

.field public static final init_password_boot_message:I = 0x7f0908df

.field public static final init_password_protect_your_shealth:I = 0x7f0908de

.field public static final initail_profile_intro_welcome:I = 0x7f0908c9

.field public static final initial_profile_intro_content:I = 0x7f0908ca

.field public static final initial_profile_intro_samsung:I = 0x7f0908c8

.field public static final initial_profile_samsung_account_already_sign:I = 0x7f0908cc

.field public static final initial_profile_samsung_account_content:I = 0x7f0908cb

.field public static final input_activity_alert_dialog_message:I = 0x7f090287

.field public static final input_activity_alert_dialog_message_float_range:I = 0x7f090288

.field public static final input_activity_alert_dialog_message_integer_range:I = 0x7f090289

.field public static final input_error:I = 0x7f090f1a

.field public static final input_name_of_food:I = 0x7f09098c

.field public static final inputmodule_title:I = 0x7f090282

.field public static final inputmodule_unit:I = 0x7f090281

.field public static final inputphotoofexercise:I = 0x7f0907b6

.field public static final insert:I = 0x7f090278

.field public static final inserting_dummy_data:I = 0x7f0902ae

.field public static final installing_updates:I = 0x7f090246

.field public static final intensity_of_workout:I = 0x7f090a68

.field public static final invalid_input:I = 0x7f090286

.field public static final invalid_text_toast:I = 0x7f0909cc

.field public static final invalid_value_entered:I = 0x7f090ae2

.field public static final iron:I = 0x7f09096a

.field public static final isens_step_1:I = 0x7f0902c4

.field public static final isens_step_2:I = 0x7f0902c5

.field public static final isens_step_3:I = 0x7f0902c6

.field public static final italian:I = 0x7f0909ab

.field public static final item_shealth_adding_food_items:I = 0x7f090fca

.field public static final item_shealth_favorites:I = 0x7f090fcb

.field public static final january:I = 0x7f0900fc

.field public static final january_short:I = 0x7f090108

.field public static final japanese:I = 0x7f0909ad

.field public static final jnj_step_1:I = 0x7f0902c3

.field public static final join_trying_toast:I = 0x7f0902a6

.field public static final joined_successfully_toast:I = 0x7f0902a5

.field public static final july:I = 0x7f090102

.field public static final july_short:I = 0x7f09010e

.field public static final june:I = 0x7f090101

.field public static final june_short:I = 0x7f09010d

.field public static final jupiter:I = 0x7f0906a4

.field public static final kcal:I = 0x7f0900b9

.field public static final kg:I = 0x7f0900c0

.field public static final kies_not_supported:I = 0x7f0902ec

.field public static final kies_restoration_failed:I = 0x7f090279

.field public static final kilocalories:I = 0x7f090955

.field public static final kilograms:I = 0x7f0900c1

.field public static final km:I = 0x7f0900c7

.field public static final km_h:I = 0x7f09080f

.field public static final korean:I = 0x7f0909a6

.field public static final label:I = 0x7f0908f4

.field public static final last_30_days:I = 0x7f0908e4

.field public static final last_3_months:I = 0x7f090b9b

.field public static final last_backed_up:I = 0x7f090d3e

.field public static final last_backup:I = 0x7f090d3d

.field public static final last_synced:I = 0x7f09087b

.field public static final last_udpated:I = 0x7f0910b9

.field public static final last_updated:I = 0x7f09081b

.field public static final last_updated_s:I = 0x7f09081a

.field public static final last_weight:I = 0x7f0908ff

.field public static final later:I = 0x7f09004c

.field public static final latest_measurements:I = 0x7f0908e8

.field public static final latest_monthly_average:I = 0x7f090906

.field public static final latest_version:I = 0x7f0908b4

.field public static final latest_workout:I = 0x7f090aa5

.field public static final lb:I = 0x7f0900c2

.field public static final leaders_board:I = 0x7f090b72

.field public static final leave_toast:I = 0x7f0902ab

.field public static final lev1_calculation:I = 0x7f0908d7

.field public static final lev2_calculation:I = 0x7f0908d8

.field public static final lev3_calculation:I = 0x7f0908d9

.field public static final lev4_calculation:I = 0x7f0908da

.field public static final lev5_calculation:I = 0x7f0908db

.field public static final lev_1:I = 0x7f090822

.field public static final lev_1_sub_title:I = 0x7f09082d

.field public static final lev_1_title:I = 0x7f090827

.field public static final lev_2:I = 0x7f090823

.field public static final lev_2_sub_title:I = 0x7f09082e

.field public static final lev_2_title:I = 0x7f090828

.field public static final lev_3:I = 0x7f090824

.field public static final lev_3_sub_title:I = 0x7f09082f

.field public static final lev_3_title:I = 0x7f090829

.field public static final lev_4:I = 0x7f090825

.field public static final lev_4_sub_title:I = 0x7f090830

.field public static final lev_4_title:I = 0x7f09082a

.field public static final lev_5:I = 0x7f090826

.field public static final lev_5_sub_title:I = 0x7f090831

.field public static final lev_5_title:I = 0x7f09082b

.field public static final lev_calculation:I = 0x7f0908d6

.field public static final lev_d:I = 0x7f090821

.field public static final lev_sub_title:I = 0x7f09082c

.field public static final light_exercise_msg:I = 0x7f090861

.field public static final link:I = 0x7f090043

.field public static final link_account:I = 0x7f090d3a

.field public static final list:I = 0x7f090210

.field public static final list_expanded:I = 0x7f0901ef

.field public static final list_of_devices_supported:I = 0x7f09029b

.field public static final list_unexpanded:I = 0x7f090217

.field public static final litre_short:I = 0x7f090223

.field public static final little_to_no_exercise_msg:I = 0x7f090860

.field public static final loading:I = 0x7f0900b3

.field public static final loading_data:I = 0x7f0900b5

.field public static final local_service_label:I = 0x7f09111a

.field public static final lock_screen:I = 0x7f090896

.field public static final log:I = 0x7f09005a

.field public static final log_day_group_header_date_format_day_first:I = 0x7f09025c

.field public static final log_day_group_header_date_format_month_first:I = 0x7f09025d

.field public static final log_meal:I = 0x7f0908f0

.field public static final log_month_group_header_date_format_month_first:I = 0x7f09025a

.field public static final log_month_group_header_date_format_year_first:I = 0x7f09025b

.field public static final log_no_data_message:I = 0x7f09070c

.field public static final log_share_via_description_format:I = 0x7f09025e

.field public static final log_share_via_description_timedate_format:I = 0x7f09025f

.field public static final log_weight:I = 0x7f0908ef

.field public static final low:I = 0x7f090a69

.field public static final lunch:I = 0x7f090921

.field public static final mail:I = 0x7f090919

.field public static final maintain_current_wait:I = 0x7f090c9d

.field public static final male:I = 0x7f09081e

.field public static final manual_of_s_health:I = 0x7f0908c6

.field public static final manual_set_maximum_hr_guide:I = 0x7f090ade

.field public static final map:I = 0x7f090a36

.field public static final map_view_mode_button:I = 0x7f090aad

.field public static final map_view_mode_satellite:I = 0x7f090a98

.field public static final map_view_mode_terrain:I = 0x7f090a99

.field public static final map_view_mode_title:I = 0x7f090a97

.field public static final map_view_mode_traffic:I = 0x7f090a9a

.field public static final march:I = 0x7f0900fe

.field public static final march_short:I = 0x7f09010a

.field public static final mars:I = 0x7f0906a5

.field public static final max_characters_alert:I = 0x7f0900b2

.field public static final max_easy_list_item:I = 0x7f090fc6

.field public static final max_fav_notice:I = 0x7f090dfa

.field public static final max_heart_rate:I = 0x7f090a56

.field public static final max_heart_rate_out_of_rage:I = 0x7f090ae3

.field public static final max_hr_automatic_updata:I = 0x7f090a29

.field public static final max_hr_automatic_update_dec:I = 0x7f090ad6

.field public static final max_hr_dec:I = 0x7f090a2b

.field public static final max_hr_enter_manually_dec:I = 0x7f090ad7

.field public static final max_hr_enter_manualy:I = 0x7f090a2a

.field public static final max_hr_enter_maximum_heart_rate:I = 0x7f090a33

.field public static final max_hr_select_header_info:I = 0x7f090ad5

.field public static final max_pace:I = 0x7f090a54

.field public static final max_speed:I = 0x7f090a52

.field public static final maximum_heart_rate:I = 0x7f090ae1

.field public static final maximum_heart_rate_updated:I = 0x7f090adf

.field public static final maximum_number_of_favorites_exceeded:I = 0x7f090926

.field public static final maximum_number_of_my_food_exceeded:I = 0x7f090927

.field public static final maximum_number_of_my_meals_exceeded:I = 0x7f090925

.field public static final maximum_number_of_photos_exceeded:I = 0x7f090928

.field public static final maximum_number_of_picture_exceeded:I = 0x7f090929

.field public static final may:I = 0x7f090100

.field public static final may_short:I = 0x7f09010c

.field public static final mb:I = 0x7f0907f9

.field public static final meal:I = 0x7f09092f

.field public static final meal_activity_try_it_text:I = 0x7f090d00

.field public static final meal_item_delete_title:I = 0x7f09093d

.field public static final meal_plan:I = 0x7f090948

.field public static final measured_data_discard:I = 0x7f0908a4

.field public static final measurement_failed:I = 0x7f0907ad

.field public static final medals:I = 0x7f0908e5

.field public static final meet_your_coach:I = 0x7f09034d

.field public static final men_bmr:I = 0x7f090859

.field public static final meter:I = 0x7f0900c8

.field public static final meter_short:I = 0x7f0900c9

.field public static final mg:I = 0x7f0900be

.field public static final mgdl:I = 0x7f0900cf

.field public static final mi_h:I = 0x7f090810

.field public static final mile:I = 0x7f0900ca

.field public static final mile_short:I = 0x7f0900cc

.field public static final miles:I = 0x7f0900cb

.field public static final millimole_short:I = 0x7f09021f

.field public static final min:I = 0x7f0900ea

.field public static final min_km:I = 0x7f09080e

.field public static final min_mi:I = 0x7f090811

.field public static final min_short:I = 0x7f0900ed

.field public static final mins:I = 0x7f0900eb

.field public static final minutes:I = 0x7f0900ec

.field public static final ml:I = 0x7f0900c4

.field public static final mmhg:I = 0x7f0900d1

.field public static final mmhgCapital:I = 0x7f0910cb

.field public static final mmoll:I = 0x7f0900d0

.field public static final moderate_exercise_msg:I = 0x7f090862

.field public static final mon:I = 0x7f0900ee

.field public static final mon_short:I = 0x7f0900f5

.field public static final mon_sun:I = 0x7f090134

.field public static final monday_three_chars:I = 0x7f090d06

.field public static final month:I = 0x7f090068

.field public static final monthly_average:I = 0x7f0908fb

.field public static final moon:I = 0x7f0906a3

.field public static final more:I = 0x7f09090b

.field public static final more_apps:I = 0x7f09002a

.field public static final more_apps_catcare:I = 0x7f090cdc

.field public static final more_apps_catwellness:I = 0x7f090cdb

.field public static final more_apps_error_api_msg:I = 0x7f090cd8

.field public static final more_apps_error_api_title:I = 0x7f090cd7

.field public static final more_apps_error_network_msg:I = 0x7f090cd6

.field public static final more_apps_error_network_title:I = 0x7f090cd5

.field public static final more_apps_error_timeout_msg:I = 0x7f090cda

.field public static final more_apps_error_timeout_title:I = 0x7f090cd9

.field public static final more_apps_noapp:I = 0x7f090cdf

.field public static final more_apps_sub_tab_installed:I = 0x7f090cd3

.field public static final more_apps_sub_tab_more:I = 0x7f090cd4

.field public static final more_detail:I = 0x7f0909e1

.field public static final more_four:I = 0x7f090b7b

.field public static final more_options:I = 0x7f090030

.field public static final most_used_for_breakfast:I = 0x7f090956

.field public static final most_used_for_dinner:I = 0x7f090958

.field public static final most_used_for_lunch:I = 0x7f090957

.field public static final most_used_for_snack:I = 0x7f090959

.field public static final msg_alert_input_rename_device:I = 0x7f0900a2

.field public static final msg_app_name:I = 0x7f090ce1

.field public static final msg_backup_inprogress:I = 0x7f090d25

.field public static final msg_dialog_confirm_restore:I = 0x7f090d42

.field public static final msg_dialog_confirm_restore_without_checkbox:I = 0x7f090d43

.field public static final msg_dialog_fitness_with_gear_data_loss:I = 0x7f090d46

.field public static final msg_dialog_restore_fitness_with_gear_data:I = 0x7f090d45

.field public static final msg_disable_samsung_account:I = 0x7f090870

.field public static final msg_popup_restore_completed:I = 0x7f090d28

.field public static final msg_popup_restore_tryagain:I = 0x7f090d29

.field public static final msg_popup_update:I = 0x7f090238

.field public static final msg_popup_upgrade_cancel_update:I = 0x7f090d2d

.field public static final msg_popup_upgrade_failed_to_restore:I = 0x7f090d2e

.field public static final msg_popup_upgrade_feature_not_supported:I = 0x7f090d2a

.field public static final msg_popup_upgrade_to_export_data_go_to_SHealth_settings:I = 0x7f090d2b

.field public static final msg_popup_upgrade_update_failed:I = 0x7f090d2c

.field public static final msg_unlink_account:I = 0x7f09023a

.field public static final my_accessories:I = 0x7f09008f

.field public static final my_food:I = 0x7f090944

.field public static final my_food_no_calorie_value_entered:I = 0x7f090993

.field public static final my_food_no_content_entered:I = 0x7f090991

.field public static final my_food_no_name_entered:I = 0x7f090992

.field public static final my_goals:I = 0x7f0908f7

.field public static final my_location:I = 0x7f090aae

.field public static final my_meal:I = 0x7f090977

.field public static final my_meal_default_name:I = 0x7f090935

.field public static final my_page:I = 0x7f0908e2

.field public static final my_rankings:I = 0x7f090b6a

.field public static final my_workout:I = 0x7f090aa3

.field public static final myfiles:I = 0x7f090880

.field public static final mypage_kcal:I = 0x7f0908f9

.field public static final name:I = 0x7f09081c

.field public static final name_hidden:I = 0x7f0907de

.field public static final name_of_food:I = 0x7f090989

.field public static final name_of_meal:I = 0x7f09098a

.field public static final navigate_up:I = 0x7f09020f

.field public static final neptune:I = 0x7f0906a7

.field public static final netpulse_account:I = 0x7f090871

.field public static final network_error:I = 0x7f0907ff

.field public static final new_data_desr:I = 0x7f090a7b

.field public static final new_data_received:I = 0x7f0906e2

.field public static final new_record_medal:I = 0x7f090b6c

.field public static final new_version_force_update:I = 0x7f0908b2

.field public static final new_version_update:I = 0x7f0908b1

.field public static final next:I = 0x7f090042

.field public static final no:I = 0x7f090c91

.field public static final no_account:I = 0x7f090905

.field public static final no_app_support:I = 0x7f090805

.field public static final no_applications:I = 0x7f090910

.field public static final no_backup_history:I = 0x7f090d41

.field public static final no_blood_glucose_devices:I = 0x7f090298

.field public static final no_blood_pressure_devices:I = 0x7f090292

.field public static final no_connected_devices:I = 0x7f090094

.field public static final no_data:I = 0x7f09006a

.field public static final no_data_to_display:I = 0x7f090afb

.field public static final no_data_to_display_text:I = 0x7f090afc

.field public static final no_data_to_display_text2:I = 0x7f090afd

.field public static final no_detected_accessories:I = 0x7f090092

.field public static final no_detected_accessories_with_dot:I = 0x7f090093

.field public static final no_detected_hr:I = 0x7f0909e0

.field public static final no_food_data:I = 0x7f09094b

.field public static final no_food_item_entered:I = 0x7f09095c

.field public static final no_goals_basic:I = 0x7f090a1f

.field public static final no_goals_hint:I = 0x7f090a20

.field public static final no_gps_info_noti:I = 0x7f090abb

.field public static final no_info:I = 0x7f090bdd

.field public static final no_information:I = 0x7f090a9c

.field public static final no_internet_connection:I = 0x7f09095a

.field public static final no_items_installed:I = 0x7f090915

.field public static final no_medal:I = 0x7f0908f5

.field public static final no_network_connection:I = 0x7f09078f

.field public static final no_plugins_enabled:I = 0x7f090912

.field public static final no_plugins_installed:I = 0x7f090911

.field public static final no_ranking_guide:I = 0x7f090bab

.field public static final no_results_food:I = 0x7f09095f

.field public static final no_sensor_data:I = 0x7f090187

.field public static final no_string:I = 0x7f0906d9

.field public static final no_synced:I = 0x7f090878

.field public static final no_weight_devices:I = 0x7f090cd2

.field public static final normal:I = 0x7f090061

.field public static final normal_pace:I = 0x7f09111b

.field public static final not_available:I = 0x7f090a76

.field public static final not_connected:I = 0x7f0900ae

.field public static final not_enough_memory:I = 0x7f0907f2

.field public static final not_enough_memory_error:I = 0x7f09078b

.field public static final notes:I = 0x7f09007c

.field public static final notes_header:I = 0x7f0907ab

.field public static final notes_hrm:I = 0x7f090f18

.field public static final noti_case_auto_start:I = 0x7f0907d1

.field public static final noti_case_auto_start_stop_text:I = 0x7f0907d3

.field public static final noti_case_auto_start_text:I = 0x7f0907d2

.field public static final noti_case_best_record_text:I = 0x7f090b98

.field public static final noti_case_best_record_title:I = 0x7f090b97

.field public static final noti_case_goal_achieved_text:I = 0x7f090b96

.field public static final noti_case_goal_achieved_title:I = 0x7f090b95

.field public static final noti_case_half_achieved_text:I = 0x7f090b94

.field public static final noti_case_half_achieved_title:I = 0x7f090b93

.field public static final noti_case_inactive_title_txt:I = 0x7f090be6

.field public static final noti_case_not_used_text:I = 0x7f090b99

.field public static final noti_case_not_used_title:I = 0x7f090b92

.field public static final notice:I = 0x7f0900e1

.field public static final notice_colon:I = 0x7f09085b

.field public static final notice_exercise_1:I = 0x7f090d30

.field public static final notice_exercise_2:I = 0x7f090d31

.field public static final notice_food_1:I = 0x7f090d32

.field public static final notice_pedometer:I = 0x7f090d2f

.field public static final notices:I = 0x7f0900e2

.field public static final notification:I = 0x7f0907be

.field public static final notification_settings_hint:I = 0x7f090898

.field public static final notification_setup_profile_body:I = 0x7f090264

.field public static final notification_setup_profile_title:I = 0x7f090263

.field public static final notifications:I = 0x7f0907bf

.field public static final notitle:I = 0x7f090d33

.field public static final november:I = 0x7f090106

.field public static final november_short:I = 0x7f090112

.field public static final nuance_privacy_policy_underline:I = 0x7f090996

.field public static final num_fifty:I = 0x7f090ce7

.field public static final num_one:I = 0x7f090ce4

.field public static final num_three:I = 0x7f090ce6

.field public static final num_two:I = 0x7f090ce5

.field public static final num_zero:I = 0x7f090ce3

.field public static final number_1:I = 0x7f091090

.field public static final number_2:I = 0x7f091091

.field public static final number_3:I = 0x7f091092

.field public static final number_4:I = 0x7f091093

.field public static final number_5:I = 0x7f091094

.field public static final number_6:I = 0x7f091095

.field public static final nutrition_info:I = 0x7f090971

.field public static final october:I = 0x7f090105

.field public static final october_short:I = 0x7f090111

.field public static final ok:I = 0x7f090047

.field public static final omron_device:I = 0x7f090e19

.field public static final omron_device_bf_206bt:I = 0x7f090e1a

.field public static final omron_device_hbf_206it:I = 0x7f090e1b

.field public static final omron_device_link:I = 0x7f090e02

.field public static final omron_scale_2:I = 0x7f09113a

.field public static final on_started_toast:I = 0x7f0902a9

.field public static final on_stoped_toast:I = 0x7f0902aa

.field public static final onetouch:I = 0x7f090e0e

.field public static final onetouch_device_link:I = 0x7f090e03

.field public static final onetouch_model:I = 0x7f090e0d

.field public static final ongoing_challenge:I = 0x7f0908ec

.field public static final ongoing_workout:I = 0x7f090a4b

.field public static final op_menu_next_goal:I = 0x7f0906e5

.field public static final open:I = 0x7f09091a

.field public static final open_folder:I = 0x7f09087e

.field public static final open_source_licence:I = 0x7f0907ef

.field public static final openg_gear_manager:I = 0x7f0900ac

.field public static final option_menu_device_sensor:I = 0x7f09003e

.field public static final option_menu_dummy:I = 0x7f09003d

.field public static final or:I = 0x7f0901ad

.field public static final other:I = 0x7f09002d

.field public static final other_activityes:I = 0x7f090a48

.field public static final others_steps:I = 0x7f090bc9

.field public static final out_of_range:I = 0x7f090b53

.field public static final out_of_range_message:I = 0x7f0900b0

.field public static final outdoors:I = 0x7f090686

.field public static final outside_normal_range:I = 0x7f0901ab

.field public static final outside_normal_range_statistics_float:I = 0x7f09027a

.field public static final outside_normal_range_statistics_int:I = 0x7f09027b

.field public static final oz:I = 0x7f0900c5

.field public static final pace:I = 0x7f090b54

.field public static final pace_default_value:I = 0x7f090acd

.field public static final pair_confirmation_message:I = 0x7f090188

.field public static final pair_confirmation_message_hrm:I = 0x7f090189

.field public static final paired:I = 0x7f0900a1

.field public static final paired_accessories:I = 0x7f090090

.field public static final paired_accessory_title_heart_rate:I = 0x7f090023

.field public static final paired_blood_glucose_devices:I = 0x7f090296

.field public static final paired_blood_pressure_devices:I = 0x7f090290

.field public static final paired_gear:I = 0x7f09089a

.field public static final paired_hrm_devices:I = 0x7f090a9f

.field public static final paired_weight_devices:I = 0x7f090cd0

.field public static final pairing_device_toast:I = 0x7f0902a1

.field public static final pairing_with:I = 0x7f0902a2

.field public static final partner_apps:I = 0x7f090cde

.field public static final password_change_try_again:I = 0x7f090895

.field public static final password_changed:I = 0x7f09088d

.field public static final password_contains_invalid_character:I = 0x7f090891

.field public static final password_do_not_match:I = 0x7f090894

.field public static final password_lock:I = 0x7f0907bb

.field public static final password_lock_notification:I = 0x7f090bf4

.field public static final password_locked_not_running_health_service:I = 0x7f090261

.field public static final password_must_contain_at_least_4_characters:I = 0x7f09088e

.field public static final password_must_contain_letter:I = 0x7f090892

.field public static final password_must_contain_no_more_than_16_characters:I = 0x7f09088f

.field public static final pause:I = 0x7f090053

.field public static final pause_alert:I = 0x7f090b67

.field public static final pause_music:I = 0x7f090f35

.field public static final pause_pedometer:I = 0x7f090054

.field public static final pd_users:I = 0x7f090b8a

.field public static final pedo_acc_guide_info:I = 0x7f090acc

.field public static final pedo_accessory_manager_can_sync:I = 0x7f090bd7

.field public static final pedo_pop_up_acc_manager_can_sync:I = 0x7f090bd6

.field public static final pedo_set_time_from:I = 0x7f0907d9

.field public static final pedo_set_time_to:I = 0x7f0907da

.field public static final pedometer:I = 0x7f090020

.field public static final pedometer_acc:I = 0x7f090185

.field public static final pedometer_achievements:I = 0x7f0907e2

.field public static final pedometer_achievements_txt:I = 0x7f0907d4

.field public static final pedometer_capital_steps:I = 0x7f0907e6

.field public static final pedometer_empty_data:I = 0x7f090c01

.field public static final pedometer_medal:I = 0x7f0908ea

.field public static final pedometer_paused:I = 0x7f0907dc

.field public static final pedometer_steps:I = 0x7f0907e5

.field public static final pedometer_zero_step:I = 0x7f0907dd

.field public static final per_serving_size:I = 0x7f09017c

.field public static final percent:I = 0x7f090c9e

.field public static final percent_right:I = 0x7f0910a9

.field public static final period:I = 0x7f0900d6

.field public static final period_all_times:I = 0x7f09061e

.field public static final period_automatic:I = 0x7f090129

.field public static final period_daily:I = 0x7f090b9a

.field public static final period_daily_last_months:I = 0x7f090b9c

.field public static final period_hours:I = 0x7f090a7c

.field public static final period_last_7_days:I = 0x7f090b9d

.field public static final period_last_week:I = 0x7f090b9e

.field public static final period_one_day:I = 0x7f09012d

.field public static final period_six_hours:I = 0x7f09012b

.field public static final period_this_week:I = 0x7f090b9f

.field public static final period_three_days:I = 0x7f09012e

.field public static final period_three_hours:I = 0x7f09012a

.field public static final period_twelve_hours:I = 0x7f09012c

.field public static final permission:I = 0x7f0907b2

.field public static final personal_data_management:I = 0x7f090877

.field public static final photo:I = 0x7f09007e

.field public static final place_a_barcode_inside_the_viewfinder_rectangle_in_order_to_scan_it:I = 0x7f0909b2

.field public static final planned_meal_breakfast:I = 0x7f090982

.field public static final planned_meal_dinner:I = 0x7f090984

.field public static final planned_meal_lunch:I = 0x7f090983

.field public static final planned_meal_snack:I = 0x7f090985

.field public static final planned_workout:I = 0x7f090aa4

.field public static final platform_not_support_art:I = 0x7f090803

.field public static final play_music:I = 0x7f090a9b

.field public static final play_next_track:I = 0x7f090f34

.field public static final play_previous_track:I = 0x7f090f33

.field public static final playing:I = 0x7f090ac9

.field public static final pm:I = 0x7f090182

.field public static final pm_upper:I = 0x7f0900e7

.field public static final points:I = 0x7f09091b

.field public static final popup_invalid_date:I = 0x7f090117

.field public static final popup_invalid_input_data:I = 0x7f090116

.field public static final popup_sign_out_msg:I = 0x7f090886

.field public static final portion_size:I = 0x7f090937

.field public static final portuguese_latin:I = 0x7f0909ae

.field public static final pounds:I = 0x7f0900c3

.field public static final pp_australia_content_1:I = 0x7f090722

.field public static final pp_australia_content_2:I = 0x7f090723

.field public static final pp_australia_content_3:I = 0x7f090724

.field public static final pp_australia_subject_1_end:I = 0x7f090769

.field public static final pp_australia_subject_1_front:I = 0x7f090768

.field public static final pp_australia_subject_1_title:I = 0x7f090767

.field public static final pp_australia_subject_2_content:I = 0x7f09076b

.field public static final pp_australia_subject_2_title:I = 0x7f09076a

.field public static final pp_australia_subject_3_content_1:I = 0x7f09076d

.field public static final pp_australia_subject_3_content_2:I = 0x7f09076e

.field public static final pp_australia_subject_3_title:I = 0x7f09076c

.field public static final pp_australia_subject_4_content:I = 0x7f090770

.field public static final pp_australia_subject_4_title:I = 0x7f09076f

.field public static final pp_australia_subject_5_content_1:I = 0x7f090772

.field public static final pp_australia_subject_5_content_1_1:I = 0x7f090773

.field public static final pp_australia_subject_5_content_1_2:I = 0x7f090774

.field public static final pp_australia_subject_5_content_2:I = 0x7f090775

.field public static final pp_australia_subject_5_title:I = 0x7f090771

.field public static final pp_australia_subject_6_content_1:I = 0x7f090777

.field public static final pp_australia_subject_6_content_1_1:I = 0x7f090778

.field public static final pp_australia_subject_6_content_1_2:I = 0x7f090779

.field public static final pp_australia_subject_6_content_1_3:I = 0x7f09077a

.field public static final pp_australia_subject_6_content_2:I = 0x7f09077b

.field public static final pp_australia_subject_6_content_2_1:I = 0x7f09077c

.field public static final pp_australia_subject_6_content_2_2:I = 0x7f09077d

.field public static final pp_australia_subject_6_content_2_3:I = 0x7f09077e

.field public static final pp_australia_subject_6_title:I = 0x7f090776

.field public static final pp_australia_subject_7_content_1:I = 0x7f090780

.field public static final pp_australia_subject_7_content_2:I = 0x7f090781

.field public static final pp_australia_subject_7_content_3:I = 0x7f090782

.field public static final pp_australia_subject_7_content_4:I = 0x7f090783

.field public static final pp_australia_subject_7_title:I = 0x7f09077f

.field public static final pp_australia_supplment_content_1:I = 0x7f090763

.field public static final pp_australia_supplment_content_2:I = 0x7f090764

.field public static final pp_australia_supplment_content_3:I = 0x7f090765

.field public static final pp_australia_supplment_content_4:I = 0x7f090766

.field public static final pp_australia_supplment_title:I = 0x7f090762

.field public static final previous:I = 0x7f090041

.field public static final prime:I = 0x7f0900d3

.field public static final print:I = 0x7f090038

.field public static final privacy:I = 0x7f09091c

.field public static final privacy_content_1:I = 0x7f090832

.field public static final privacy_content_2:I = 0x7f090833

.field public static final privacy_policy:I = 0x7f090e43

.field public static final privacy_policy_paragraph_1:I = 0x7f090e45

.field public static final privacy_policy_paragraph_1_ver2:I = 0x7f090e55

.field public static final privacy_policy_paragraph_1_ver2_albania:I = 0x7f090ed0

.field public static final privacy_policy_paragraph_1_ver2_austria:I = 0x7f090ece

.field public static final privacy_policy_paragraph_1_ver2_bosnia:I = 0x7f090ed3

.field public static final privacy_policy_paragraph_1_ver2_canada:I = 0x7f090ef5

.field public static final privacy_policy_paragraph_1_ver2_congo:I = 0x7f090ee4

.field public static final privacy_policy_paragraph_1_ver2_france:I = 0x7f090eda

.field public static final privacy_policy_paragraph_1_ver2_german:I = 0x7f090e4e

.field public static final privacy_policy_paragraph_1_ver2_maxico:I = 0x7f090eaa

.field public static final privacy_policy_paragraph_1_ver2_montenegro:I = 0x7f090ed7

.field public static final privacy_policy_paragraph_1_ver2_morocco:I = 0x7f090ebb

.field public static final privacy_policy_paragraph_1_ver2_portugal:I = 0x7f090ee7

.field public static final privacy_policy_paragraph_1_ver2_south_africa:I = 0x7f090edf

.field public static final privacy_policy_paragraph_2:I = 0x7f090e46

.field public static final privacy_policy_paragraph_2_garman:I = 0x7f090e4f

.field public static final privacy_policy_paragraph_2_ver2:I = 0x7f090e56

.field public static final privacy_policy_paragraph_2_ver2_burnt_calorie:I = 0x7f090e5d

.field public static final privacy_policy_paragraph_2_ver2_download:I = 0x7f090e57

.field public static final privacy_policy_paragraph_2_ver2_end:I = 0x7f090e6a

.field public static final privacy_policy_paragraph_2_ver2_exercise:I = 0x7f090e5e

.field public static final privacy_policy_paragraph_2_ver2_food:I = 0x7f090e5f

.field public static final privacy_policy_paragraph_2_ver2_front:I = 0x7f090e58

.field public static final privacy_policy_paragraph_2_ver2_front_canada:I = 0x7f090ef6

.field public static final privacy_policy_paragraph_2_ver2_front_without_data:I = 0x7f090e59

.field public static final privacy_policy_paragraph_2_ver2_glucose:I = 0x7f090e66

.field public static final privacy_policy_paragraph_2_ver2_goal:I = 0x7f090e5b

.field public static final privacy_policy_paragraph_2_ver2_hr:I = 0x7f090e60

.field public static final privacy_policy_paragraph_2_ver2_humidity:I = 0x7f090e69

.field public static final privacy_policy_paragraph_2_ver2_location:I = 0x7f090e5a

.field public static final privacy_policy_paragraph_2_ver2_pedometer:I = 0x7f090e5c

.field public static final privacy_policy_paragraph_2_ver2_pressure:I = 0x7f090e67

.field public static final privacy_policy_paragraph_2_ver2_sleep:I = 0x7f090e61

.field public static final privacy_policy_paragraph_2_ver2_spo2:I = 0x7f090e64

.field public static final privacy_policy_paragraph_2_ver2_stress:I = 0x7f090e62

.field public static final privacy_policy_paragraph_2_ver2_temperature:I = 0x7f090e68

.field public static final privacy_policy_paragraph_2_ver2_uv:I = 0x7f090e65

.field public static final privacy_policy_paragraph_2_ver2_weight:I = 0x7f090e63

.field public static final privacy_policy_paragraph_2_ver2_without_account:I = 0x7f090e80

.field public static final privacy_policy_paragraph_3:I = 0x7f090e47

.field public static final privacy_policy_paragraph_3_2_garman:I = 0x7f090e51

.field public static final privacy_policy_paragraph_3_ending:I = 0x7f090e49

.field public static final privacy_policy_paragraph_3_ending_ver2:I = 0x7f090e6c

.field public static final privacy_policy_paragraph_3_ending_ver2_canada:I = 0x7f090ef8

.field public static final privacy_policy_paragraph_3_garman:I = 0x7f090e50

.field public static final privacy_policy_paragraph_3_stress_sleep:I = 0x7f090e48

.field public static final privacy_policy_paragraph_3_stress_sleep_ver2:I = 0x7f090e6b

.field public static final privacy_policy_paragraph_3_stress_sleep_ver2_bosnia:I = 0x7f090ed4

.field public static final privacy_policy_paragraph_3_stress_sleep_ver2_canada:I = 0x7f090ef7

.field public static final privacy_policy_paragraph_3_stress_sleep_ver2_france:I = 0x7f090edb

.field public static final privacy_policy_paragraph_3_stress_sleep_ver2_montenegrin:I = 0x7f090ed8

.field public static final privacy_policy_paragraph_3_stress_sleep_ver2_morocco:I = 0x7f090ebc

.field public static final privacy_policy_paragraph_3_stress_sleep_ver2_without_account:I = 0x7f090e81

.field public static final privacy_policy_paragraph_4:I = 0x7f090e4a

.field public static final privacy_policy_paragraph_4_garman:I = 0x7f090e52

.field public static final privacy_policy_paragraph_4_ver2:I = 0x7f090e7d

.field public static final privacy_policy_paragraph_4_ver2_canada:I = 0x7f090f06

.field public static final privacy_policy_paragraph_5:I = 0x7f090e4d

.field public static final privacy_policy_paragraph_5_garman:I = 0x7f090e53

.field public static final privacy_policy_paragraph_5_ver2:I = 0x7f090e7f

.field public static final privacy_policy_paragraph_5_ver2_canada:I = 0x7f090f07

.field public static final privacy_policy_paragraph_6_ver2:I = 0x7f090e7c

.field public static final privacy_policy_paragraph_6_ver2_canada:I = 0x7f090f05

.field public static final privacy_policy_paragraph_kor_2:I = 0x7f0905c6

.field public static final privacy_policy_paragraph_kor_3:I = 0x7f0905c7

.field public static final privacy_policy_paragraph_kor_3_without_account:I = 0x7f0905cf

.field public static final privacy_policy_paragraph_kor_4:I = 0x7f0905c9

.field public static final privacy_policy_paragraph_kor_5:I = 0x7f0905ca

.field public static final privacy_policy_paragraph_kor_6:I = 0x7f0905cb

.field public static final privacy_policy_paragraph_kor_6_without_account:I = 0x7f0905d0

.field public static final privacy_policy_paragraph_kor_7:I = 0x7f0905cd

.field public static final privacy_policy_paragraph_kor_8:I = 0x7f0905ce

.field public static final privacy_policy_sub:I = 0x7f090e4b

.field public static final privacy_policy_sub_canada:I = 0x7f090e54

.field public static final privacy_policy_sub_kor_1:I = 0x7f0905c5

.field public static final privacy_policy_sub_kor_2:I = 0x7f0905c8

.field public static final privacy_policy_sub_kor_3:I = 0x7f0905cc

.field public static final privacy_policy_sub_text_1:I = 0x7f090e4c

.field public static final privacy_policy_sub_text_1_france:I = 0x7f090edd

.field public static final privacy_policy_sub_text_1_russia:I = 0x7f090eb7

.field public static final privacy_policy_sub_text_1_south_africa:I = 0x7f090ee0

.field public static final privacy_policy_sub_ver2:I = 0x7f090e7e

.field public static final privacy_policy_title:I = 0x7f090e44

.field public static final privacy_policy_title_kor:I = 0x7f0905c4

.field public static final problem_occured:I = 0x7f090b75

.field public static final profile:I = 0x7f0907b7

.field public static final profile_activity_level:I = 0x7f090834

.field public static final profile_card:I = 0x7f09081d

.field public static final profile_use_ranking:I = 0x7f090820

.field public static final progressing:I = 0x7f0900b6

.field public static final progressing_restore:I = 0x7f0900b7

.field public static final prompt_Not_tick:I = 0x7f09022c

.field public static final prompt_add:I = 0x7f0901e4

.field public static final prompt_add_picture:I = 0x7f090232

.field public static final prompt_average:I = 0x7f090204

.field public static final prompt_barcode_button:I = 0x7f09023c

.field public static final prompt_beats_per_minute:I = 0x7f090227

.field public static final prompt_bmi_scores:I = 0x7f090201

.field public static final prompt_button:I = 0x7f09020a

.field public static final prompt_cal:I = 0x7f0901d6

.field public static final prompt_calories:I = 0x7f0901c6

.field public static final prompt_camera_button:I = 0x7f0901de

.field public static final prompt_cancel:I = 0x7f09020c

.field public static final prompt_centimeters:I = 0x7f0901bf

.field public static final prompt_chart_german:I = 0x7f090784

.field public static final prompt_check_box:I = 0x7f09022f

.field public static final prompt_checked:I = 0x7f090230

.field public static final prompt_cm:I = 0x7f0901cf

.field public static final prompt_collapse:I = 0x7f09021b

.field public static final prompt_comma:I = 0x7f09020b

.field public static final prompt_continue:I = 0x7f09020d

.field public static final prompt_day_button:I = 0x7f090213

.field public static final prompt_decilitre:I = 0x7f090224

.field public static final prompt_delete:I = 0x7f0901e6

.field public static final prompt_disabled:I = 0x7f09022d

.field public static final prompt_double_tap_to_change:I = 0x7f090200

.field public static final prompt_double_tap_to_close:I = 0x7f090219

.field public static final prompt_double_tap_to_edit:I = 0x7f0901ff

.field public static final prompt_double_tap_to_open:I = 0x7f090218

.field public static final prompt_double_view_log:I = 0x7f0907af

.field public static final prompt_drag_hold:I = 0x7f090244

.field public static final prompt_drop_down_list:I = 0x7f0901ec

.field public static final prompt_expand:I = 0x7f09021a

.field public static final prompt_favourite_button:I = 0x7f0901df

.field public static final prompt_feet:I = 0x7f0901c0

.field public static final prompt_food_pick_add_food_button:I = 0x7f0901e2

.field public static final prompt_food_pick_search_barcode_button:I = 0x7f0901e0

.field public static final prompt_food_pick_search_voice_button:I = 0x7f0901e1

.field public static final prompt_ft:I = 0x7f0901d0

.field public static final prompt_gallery_button:I = 0x7f0901e8

.field public static final prompt_grams:I = 0x7f090228

.field public static final prompt_h:I = 0x7f0901da

.field public static final prompt_header:I = 0x7f0901fd

.field public static final prompt_hour_button:I = 0x7f090211

.field public static final prompt_hours:I = 0x7f0901cc

.field public static final prompt_hr:I = 0x7f0901db

.field public static final prompt_image:I = 0x7f0901e5

.field public static final prompt_in:I = 0x7f0901dc

.field public static final prompt_inches:I = 0x7f0901cd

.field public static final prompt_kcal:I = 0x7f0901d9

.field public static final prompt_kg:I = 0x7f0901d1

.field public static final prompt_kilocalories:I = 0x7f0901cb

.field public static final prompt_kilograms:I = 0x7f0901c1

.field public static final prompt_kilometers:I = 0x7f0901c3

.field public static final prompt_km:I = 0x7f0901d3

.field public static final prompt_lb:I = 0x7f0901d5

.field public static final prompt_lev_1:I = 0x7f090205

.field public static final prompt_lev_2:I = 0x7f090206

.field public static final prompt_lev_3:I = 0x7f090207

.field public static final prompt_lev_4:I = 0x7f090208

.field public static final prompt_lev_5:I = 0x7f090209

.field public static final prompt_litre:I = 0x7f090225

.field public static final prompt_m:I = 0x7f0901d4

.field public static final prompt_map_view:I = 0x7f09021e

.field public static final prompt_meters:I = 0x7f0901c4

.field public static final prompt_mi:I = 0x7f0901d2

.field public static final prompt_miles:I = 0x7f0901c2

.field public static final prompt_milligrams:I = 0x7f090221

.field public static final prompt_millimeter_mercury:I = 0x7f090226

.field public static final prompt_millimoles:I = 0x7f090220

.field public static final prompt_min:I = 0x7f0901dd

.field public static final prompt_minutes:I = 0x7f0901ce

.field public static final prompt_month_button:I = 0x7f090212

.field public static final prompt_music:I = 0x7f0901ee

.field public static final prompt_mypicture:I = 0x7f0901fe

.field public static final prompt_next:I = 0x7f0901f6

.field public static final prompt_not_checked:I = 0x7f090231

.field public static final prompt_not_selected:I = 0x7f0901f2

.field public static final prompt_open:I = 0x7f090214

.field public static final prompt_ounce:I = 0x7f090229

.field public static final prompt_per:I = 0x7f0901c7

.field public static final prompt_per_hour:I = 0x7f0901c8

.field public static final prompt_per_minute:I = 0x7f0901c9

.field public static final prompt_per_sec:I = 0x7f0901d8

.field public static final prompt_per_second:I = 0x7f0901ca

.field public static final prompt_per_symbol:I = 0x7f0901d7

.field public static final prompt_play:I = 0x7f0901ed

.field public static final prompt_portion_size:I = 0x7f0901e7

.field public static final prompt_pounds:I = 0x7f0901c5

.field public static final prompt_quick_input_button:I = 0x7f09023b

.field public static final prompt_radiobtn:I = 0x7f09022e

.field public static final prompt_score_board:I = 0x7f0901e9

.field public static final prompt_search_edit_text:I = 0x7f0901e3

.field public static final prompt_seconds:I = 0x7f090216

.field public static final prompt_seek_control:I = 0x7f090243

.field public static final prompt_selected:I = 0x7f0901f1

.field public static final prompt_skip:I = 0x7f0901f7

.field public static final prompt_start:I = 0x7f0901ea

.field public static final prompt_step_1:I = 0x7f0901f9

.field public static final prompt_step_2:I = 0x7f0901fa

.field public static final prompt_step_3:I = 0x7f0901fb

.field public static final prompt_step_4:I = 0x7f0901fc

.field public static final prompt_step_d:I = 0x7f0901f8

.field public static final prompt_stop:I = 0x7f0901eb

.field public static final prompt_tab:I = 0x7f090203

.field public static final prompt_tab_workout_goal_1:I = 0x7f090867

.field public static final prompt_tab_workout_goal_2:I = 0x7f090868

.field public static final prompt_tab_workout_goal_3:I = 0x7f090869

.field public static final prompt_tab_workout_goal_4:I = 0x7f09086a

.field public static final prompt_tick:I = 0x7f09022b

.field public static final prompt_tick_box:I = 0x7f09022a

.field public static final prompt_today:I = 0x7f090215

.field public static final prompt_toggle:I = 0x7f0901f0

.field public static final prompt_view_log:I = 0x7f0907ac

.field public static final prompt_view_picture:I = 0x7f090233

.field public static final proprietary_rights_and_licenses_text_1:I = 0x7f090e8d

.field public static final proprietary_rights_and_licenses_text_1_german:I = 0x7f090ec4

.field public static final proprietary_rights_and_licenses_text_1_russia:I = 0x7f090eb2

.field public static final proprietary_rights_and_licenses_text_1_spanish:I = 0x7f090eab

.field public static final proprietary_rights_and_licenses_text_1_ver2:I = 0x7f090e9c

.field public static final proprietary_rights_and_licenses_text_1_ver2_canada:I = 0x7f090ef0

.field public static final proprietary_rights_and_licenses_text_2:I = 0x7f090e8e

.field public static final proprietary_rights_and_licenses_text_2_german:I = 0x7f090ec5

.field public static final proprietary_rights_and_licenses_text_2_russia:I = 0x7f090eb3

.field public static final proprietary_rights_and_licenses_text_2_ver2:I = 0x7f090e9d

.field public static final proprietary_rights_and_licenses_text_2_ver2_canada:I = 0x7f090ef1

.field public static final proprietary_rights_and_licenses_text_3:I = 0x7f090e8f

.field public static final proprietary_rights_and_licenses_text_3_german:I = 0x7f090ec6

.field public static final proprietary_rights_and_licenses_text_3_russia:I = 0x7f090eb4

.field public static final proprietary_rights_and_licenses_text_3_ver2:I = 0x7f090e9e

.field public static final proprietary_rights_and_licenses_title:I = 0x7f090e8c

.field public static final proprietary_rights_and_licenses_title_ver2:I = 0x7f090e9b

.field public static final protein:I = 0x7f090966

.field public static final provided_by_KNS:I = 0x7f090975

.field public static final provided_by_boohee:I = 0x7f090d19

.field public static final provided_by_fatsecret:I = 0x7f090974

.field public static final ps_disconnected:I = 0x7f090096

.field public static final pulse:I = 0x7f0901b7

.field public static final pulse_rate:I = 0x7f0910c6

.field public static final pulse_value:I = 0x7f0910c5

.field public static final quick_input:I = 0x7f0909c1

.field public static final quick_input_error_toast:I = 0x7f0909cf

.field public static final quick_input_large:I = 0x7f0909c7

.field public static final quick_input_medium:I = 0x7f0909c6

.field public static final quick_input_skipped:I = 0x7f0909c4

.field public static final quick_input_small:I = 0x7f0909c5

.field public static final range:I = 0x7f0900b1

.field public static final rank_not_yet_updated:I = 0x7f090bde

.field public static final ranking:I = 0x7f090b69

.field public static final ranking_last_update:I = 0x7f090aba

.field public static final rankings_comparison_text_same:I = 0x7f090b81

.field public static final rankings_you_steps_ahead:I = 0x7f090b82

.field public static final realtime_audio_guide_off:I = 0x7f090a3f

.field public static final realtime_audio_guide_on:I = 0x7f090a3e

.field public static final realtime_button_connect_hrm:I = 0x7f090a3a

.field public static final realtime_button_restart:I = 0x7f090a39

.field public static final realtime_button_restart_german:I = 0x7f090787

.field public static final realtime_check_hrm_connect_for_traning_effect:I = 0x7f090a42

.field public static final realtime_data_ahead:I = 0x7f090aa0

.field public static final realtime_data_behind:I = 0x7f090aa1

.field public static final realtime_data_cur_hrm:I = 0x7f090604

.field public static final realtime_data_cur_speed:I = 0x7f090a4f

.field public static final realtime_data_elevation:I = 0x7f090a4e

.field public static final realtime_data_heart_rate:I = 0x7f090a4c

.field public static final realtime_data_pace:I = 0x7f090a4d

.field public static final realtime_data_same:I = 0x7f090aa2

.field public static final realtime_equip_connected:I = 0x7f0906e3

.field public static final realtime_exceed_recording_time:I = 0x7f090a44

.field public static final realtime_gps_off_information:I = 0x7f090a3b

.field public static final realtime_guidance_increase:I = 0x7f090a43

.field public static final realtime_guidance_keep:I = 0x7f090a40

.field public static final realtime_guidance_slowdown:I = 0x7f090a41

.field public static final realtime_music_player:I = 0x7f090a1d

.field public static final realtime_recorded_text:I = 0x7f090a80

.field public static final realtime_recorded_title:I = 0x7f090a7f

.field public static final realtime_recording_text:I = 0x7f090a7e

.field public static final realtime_recording_title:I = 0x7f090a7d

.field public static final realtime_screen_locked:I = 0x7f090a45

.field public static final realtime_screen_unlocked:I = 0x7f090a46

.field public static final realtime_turn_on_gps:I = 0x7f090a3c

.field public static final realtime_unlocked_dec_text:I = 0x7f090abc

.field public static final realtime_wfl_on_information:I = 0x7f090a3d

.field public static final received_data:I = 0x7f09009e

.field public static final recent_list:I = 0x7f0908ed

.field public static final recent_meals:I = 0x7f0906e1

.field public static final recommended_calories:I = 0x7f090856

.field public static final recommended_calories_contents_1:I = 0x7f0908d4

.field public static final recommended_calories_contents_2:I = 0x7f0908d5

.field public static final recommended_calories_contents_3:I = 0x7f0908dc

.field public static final recommended_calories_contents_4:I = 0x7f0908dd

.field public static final recommended_calories_msg1:I = 0x7f09085d

.field public static final recommended_calories_msg2:I = 0x7f09085e

.field public static final recommended_calories_msg3:I = 0x7f09086c

.field public static final recommended_calories_msg4:I = 0x7f09086d

.field public static final recommended_intake:I = 0x7f090857

.field public static final record_popup_text:I = 0x7f090271

.field public static final reenable:I = 0x7f0907a0

.field public static final register:I = 0x7f0910e6

.field public static final remaining_calories:I = 0x7f0906dd

.field public static final remaining_distance:I = 0x7f0906de

.field public static final remaining_time:I = 0x7f0906df

.field public static final remeasure:I = 0x7f0907ae

.field public static final remove:I = 0x7f09003a

.field public static final remove_account:I = 0x7f090883

.field public static final rename:I = 0x7f09008b

.field public static final repeat:I = 0x7f0900e4

.field public static final replace_meal_data:I = 0x7f09094d

.field public static final replace_meal_data_popup:I = 0x7f09094e

.field public static final requesting_permission_to_ps_allow_q:I = 0x7f090ac7

.field public static final reset:I = 0x7f090032

.field public static final reset_comfort_zone:I = 0x7f0910a1

.field public static final reset_daily_data:I = 0x7f09003b

.field public static final reset_daily_data_title:I = 0x7f0902ad

.field public static final reset_daily_data_toast_message:I = 0x7f09028a

.field public static final reset_etc_popup_linked:I = 0x7f0908a7

.field public static final reset_etc_popup_not_linked:I = 0x7f0908ad

.field public static final reset_exercise_popup_linked:I = 0x7f0908a5

.field public static final reset_exercise_popup_not_linked:I = 0x7f0908ab

.field public static final reset_food_popup_linked:I = 0x7f0908a6

.field public static final reset_food_popup_not_linked:I = 0x7f0908ac

.field public static final reset_multicheck_popup_linked:I = 0x7f0908aa

.field public static final reset_multicheck_popup_not_linked:I = 0x7f0908b0

.field public static final reset_password:I = 0x7f090122

.field public static final reset_pedometer_popup_linked:I = 0x7f0908a9

.field public static final reset_pedometer_popup_not_linked:I = 0x7f0908af

.field public static final reset_poopup_desc_all:I = 0x7f09089c

.field public static final reset_poopup_desc_coach:I = 0x7f0908a3

.field public static final reset_poopup_desc_etc:I = 0x7f0908a0

.field public static final reset_poopup_desc_exercise:I = 0x7f09089e

.field public static final reset_poopup_desc_food:I = 0x7f09089f

.field public static final reset_poopup_desc_multi:I = 0x7f09089d

.field public static final reset_poopup_desc_pedo:I = 0x7f0908a2

.field public static final reset_poopup_desc_sleepmonitor:I = 0x7f0908a1

.field public static final reset_popup_text:I = 0x7f090062

.field public static final reset_sleep_popup_linked:I = 0x7f0908a8

.field public static final reset_sleep_popup_not_linked:I = 0x7f0908ae

.field public static final reset_toast_text:I = 0x7f09094a

.field public static final rest_text:I = 0x7f0905e7

.field public static final rest_title:I = 0x7f090aa8

.field public static final resting_hr_female:I = 0x7f090f90

.field public static final resting_hr_male:I = 0x7f090f7f

.field public static final restore:I = 0x7f090139

.field public static final restore_2_5:I = 0x7f09087a

.field public static final restore_activity_header_1:I = 0x7f090d26

.field public static final restore_activity_header_2:I = 0x7f090d27

.field public static final restore_activity_text:I = 0x7f0907f0

.field public static final restore_complete:I = 0x7f0907fb

.field public static final restore_failed:I = 0x7f0907fd

.field public static final restore_now:I = 0x7f0907f1

.field public static final restoring:I = 0x7f090804

.field public static final restoring_data:I = 0x7f090d3f

.field public static final retry:I = 0x7f09004b

.field public static final roaming_warning:I = 0x7f0907fa

.field public static final rpm:I = 0x7f0905fb

.field public static final rtl_number_1:I = 0x7f091096

.field public static final rtl_number_2:I = 0x7f091097

.field public static final rtl_number_3:I = 0x7f091098

.field public static final rtl_number_4:I = 0x7f091099

.field public static final rtl_number_5:I = 0x7f09109a

.field public static final rtl_number_6:I = 0x7f09109b

.field public static final running:I = 0x7f0909d3

.field public static final running_pro:I = 0x7f090cfa

.field public static final running_steps:I = 0x7f0907df

.field public static final russian:I = 0x7f0909ac

.field public static final s_health:I = 0x7f09001d

.field public static final s_health_account:I = 0x7f090d3c

.field public static final s_health_main:I = 0x7f09001e

.field public static final s_will_be_disconnected:I = 0x7f090097

.field public static final sa_auth_fail:I = 0x7f090801

.field public static final samsung_account:I = 0x7f09086e

.field public static final samsung_account_login_reminder_msg:I = 0x7f090809

.field public static final samsung_account_phrase_1:I = 0x7f090e82

.field public static final samsung_account_phrase_1_kor:I = 0x7f0905d1

.field public static final samsung_account_phrase_2:I = 0x7f090e83

.field public static final samsung_account_phrase_2_kor:I = 0x7f0905d2

.field public static final samsung_account_phrase_3:I = 0x7f090e84

.field public static final samsung_account_phrase_3_kor:I = 0x7f0905d3

.field public static final samsung_account_signed_in:I = 0x7f090885

.field public static final samsung_activity_tracker:I = 0x7f090194

.field public static final samsung_app_store_disabled:I = 0x7f090237

.field public static final samsung_device:I = 0x7f090e05

.field public static final samsung_device_activity_tracker:I = 0x7f090e0a

.field public static final samsung_device_ei_hh10:I = 0x7f090e0b

.field public static final samsung_device_ei_hs10:I = 0x7f090e0c

.field public static final samsung_device_gear:I = 0x7f090e06

.field public static final samsung_device_gear2:I = 0x7f090e07

.field public static final samsung_device_gear2_neo:I = 0x7f090e08

.field public static final samsung_device_gear_fit:I = 0x7f090e09

.field public static final samsung_device_link:I = 0x7f090dfd

.field public static final samsung_gear_fit_manager:I = 0x7f090193

.field public static final samsung_gear_manager:I = 0x7f090192

.field public static final samsung_hrm_text:I = 0x7f0902de

.field public static final samsung_privacy_policy:I = 0x7f090999

.field public static final samsung_privacy_policy_underline:I = 0x7f090995

.field public static final samsung_scale:I = 0x7f0902ce

.field public static final samsung_scale_1:I = 0x7f0902c7

.field public static final samsung_scale_2:I = 0x7f0902c8

.field public static final samsung_scale_3:I = 0x7f0902c9

.field public static final samsung_scale_4:I = 0x7f0902ca

.field public static final samsung_scale_5:I = 0x7f0902cb

.field public static final samsung_scale_6:I = 0x7f0902cc

.field public static final samsung_scale_7:I = 0x7f0902cd

.field public static final samsung_watch_text:I = 0x7f0902df

.field public static final sat:I = 0x7f0900f3

.field public static final sat_short:I = 0x7f0900fa

.field public static final saturated_fat:I = 0x7f09096d

.field public static final saturday_three_chars:I = 0x7f090d0b

.field public static final saturn:I = 0x7f0906a6

.field public static final save:I = 0x7f09004f

.field public static final save_data_confirmation:I = 0x7f0902a0

.field public static final saved:I = 0x7f090080

.field public static final scan:I = 0x7f09004d

.field public static final scan_barcode:I = 0x7f0909b4

.field public static final scanning:I = 0x7f0900a3

.field public static final scanningfragment_failed_to_connect:I = 0x7f090245

.field public static final scordboards:I = 0x7f090bd5

.field public static final screen_lock:I = 0x7f090aa9

.field public static final screen_unlock:I = 0x7f090aaa

.field public static final sdk_device_no_support_toast_message:I = 0x7f090236

.field public static final sdk_no_health_service_toast_message:I = 0x7f090235

.field public static final sdk_unsupport_toast_message:I = 0x7f090234

.field public static final search:I = 0x7f09007b

.field public static final search_a_keyword_to_track_your_calories:I = 0x7f090cff

.field public static final second:I = 0x7f090b79

.field public static final security_sign_in_popup_alert_text:I = 0x7f09088c

.field public static final see_more:I = 0x7f090d55

.field public static final seek_control_desc:I = 0x7f090ada

.field public static final select:I = 0x7f090034

.field public static final select_a_food_that_you_have_eaten_and_tap_done:I = 0x7f090cfe

.field public static final select_a_record:I = 0x7f090605

.field public static final select_all:I = 0x7f090071

.field public static final select_menu:I = 0x7f09110b

.field public static final select_password:I = 0x7f090120

.field public static final select_tag:I = 0x7f0902b3

.field public static final select_the_meal_to_plan:I = 0x7f09097d

.field public static final selected:I = 0x7f090075

.field public static final selected_format:I = 0x7f090074

.field public static final selected_item_deleted_message:I = 0x7f09011b

.field public static final selected_items_deleted_message:I = 0x7f09011a

.field public static final september:I = 0x7f090104

.field public static final september_short:I = 0x7f090110

.field public static final server_check_popup_msg:I = 0x7f090807

.field public static final server_check_popup_title:I = 0x7f090806

.field public static final server_error_occured_toast_msg:I = 0x7f090d53

.field public static final server_not_responding:I = 0x7f09095b

.field public static final server_time:I = 0x7f0907e1

.field public static final service_not_connected:I = 0x7f0902ac

.field public static final serving:I = 0x7f09017b

.field public static final serving_format:I = 0x7f090973

.field public static final serving_size:I = 0x7f090972

.field public static final set:I = 0x7f09004a

.field public static final set_date:I = 0x7f090079

.field public static final set_goal:I = 0x7f090031

.field public static final set_goal_recommended_text:I = 0x7f090950

.field public static final set_goal_recommended_text_1:I = 0x7f090951

.field public static final set_goal_recommended_text_2:I = 0x7f090952

.field public static final set_password:I = 0x7f090889

.field public static final set_password_hint:I = 0x7f09088a

.field public static final set_photo:I = 0x7f0908e1

.field public static final set_portion_size:I = 0x7f090938

.field public static final set_quick_input:I = 0x7f09094f

.field public static final set_quick_input_instruction:I = 0x7f0909c2

.field public static final set_some_goals:I = 0x7f09034e

.field public static final set_te_goal_easy:I = 0x7f090af6

.field public static final set_te_goal_guide_1:I = 0x7f090af3

.field public static final set_te_goal_guide_2:I = 0x7f090af4

.field public static final set_te_goal_guide_3:I = 0x7f090af5

.field public static final set_te_goal_improving:I = 0x7f090af8

.field public static final set_te_goal_moderate:I = 0x7f090af7

.field public static final set_the_period:I = 0x7f09097f

.field public static final set_the_repeat:I = 0x7f090980

.field public static final set_the_time_of_day:I = 0x7f0907c4

.field public static final set_time:I = 0x7f09007a

.field public static final setpicture:I = 0x7f0907a7

.field public static final setpicture_item1:I = 0x7f0907a8

.field public static final setpicture_item2:I = 0x7f0907a9

.field public static final setting_training_effect_goal:I = 0x7f090aee

.field public static final settings:I = 0x7f090037

.field public static final settings_bluetooth_bluetooth_connect_more_info:I = 0x7f09106d

.field public static final share_image_storage_error:I = 0x7f09018c

.field public static final share_the_result:I = 0x7f0909e2

.field public static final share_via:I = 0x7f090033

.field public static final shealth_Setting_goals_missions_step_1:I = 0x7f091063

.field public static final shealth_Setting_goals_missions_step_1_l_os:I = 0x7f091111

.field public static final shealth_Setting_goals_missions_step_2:I = 0x7f091068

.field public static final shealth_Setting_goals_missions_step_2_l_os:I = 0x7f091113

.field public static final shealth_Setting_goals_missions_step_3:I = 0x7f09106b

.field public static final shealth_Setting_goals_missions_step_4:I = 0x7f09106c

.field public static final shealth_Setting_goals_missions_step_4_l_os:I = 0x7f091114

.field public static final shealth_Setting_goals_missions_tip_1:I = 0x7f091064

.field public static final shealth_Setting_goals_missions_tip_1_ttk:I = 0x7f091065

.field public static final shealth_Setting_goals_missions_tip_2:I = 0x7f091066

.field public static final shealth_Setting_goals_missions_tip_3:I = 0x7f091067

.field public static final shealth_Setting_goals_missions_tip_3_l_os:I = 0x7f091112

.field public static final shealth_Setting_goals_missions_tip_4:I = 0x7f091069

.field public static final shealth_Setting_goals_missions_tip_5:I = 0x7f09106a

.field public static final shealth_Setting_goals_missions_title:I = 0x7f091062

.field public static final shealth_adding_favorites_description_1:I = 0x7f09102a

.field public static final shealth_adding_favorites_description_1_tts:I = 0x7f09102f

.field public static final shealth_adding_favorites_description_2:I = 0x7f09102b

.field public static final shealth_adding_favorites_description_2_tts:I = 0x7f091030

.field public static final shealth_adding_favorites_description_3:I = 0x7f09102c

.field public static final shealth_adding_favorites_description_more_info_1:I = 0x7f09102d

.field public static final shealth_adding_favorites_description_more_info_2:I = 0x7f09102e

.field public static final shealth_adding_favorites_description_more_info_2_tts:I = 0x7f091031

.field public static final shealth_adding_food_items_description_1:I = 0x7f091037

.field public static final shealth_adding_food_items_description_1_tts:I = 0x7f09103a

.field public static final shealth_adding_food_items_description_2:I = 0x7f091038

.field public static final shealth_adding_food_items_description_2_tts:I = 0x7f09103b

.field public static final shealth_adding_food_items_description_3:I = 0x7f091039

.field public static final shealth_adding_food_items_notice:I = 0x7f09103c

.field public static final shealth_bloodglucose_step_1:I = 0x7f091076

.field public static final shealth_bloodglucose_step_2:I = 0x7f091077

.field public static final shealth_bloodglucose_step_3:I = 0x7f091078

.field public static final shealth_bloodglucose_step_4:I = 0x7f091079

.field public static final shealth_bloodglucose_step_5:I = 0x7f09107a

.field public static final shealth_bloodglucose_step_6:I = 0x7f09107b

.field public static final shealth_bloodglucose_title:I = 0x7f091075

.field public static final shealth_bloodpressure_step_1:I = 0x7f09107d

.field public static final shealth_bloodpressure_step_2:I = 0x7f09107e

.field public static final shealth_bloodpressure_step_3:I = 0x7f09107f

.field public static final shealth_bloodpressure_step_4:I = 0x7f091080

.field public static final shealth_bloodpressure_title:I = 0x7f09107c

.field public static final shealth_bodytemp_step_1:I = 0x7f091071

.field public static final shealth_bodytemp_step_2:I = 0x7f091072

.field public static final shealth_bodytemp_step_3:I = 0x7f091073

.field public static final shealth_bodytemp_tip_1:I = 0x7f091074

.field public static final shealth_bodytemp_title:I = 0x7f091070

.field public static final shealth_clinical_information:I = 0x7f090ea3

.field public static final shealth_comfort_level_1:I = 0x7f090fee

.field public static final shealth_comfort_level_2:I = 0x7f090fef

.field public static final shealth_comfort_level_3:I = 0x7f090ff0

.field public static final shealth_comfort_level_4:I = 0x7f090ff1

.field public static final shealth_comfort_level_5:I = 0x7f090ff2

.field public static final shealth_comfort_level_6:I = 0x7f090ff3

.field public static final shealth_comfort_level_7:I = 0x7f090ff4

.field public static final shealth_comfort_level_8:I = 0x7f090ff5

.field public static final shealth_comfort_level_description:I = 0x7f090fd7

.field public static final shealth_comfort_level_description_1:I = 0x7f090fd8

.field public static final shealth_comfort_level_description_2:I = 0x7f090fd9

.field public static final shealth_comfort_level_description_3:I = 0x7f090fda

.field public static final shealth_comfort_level_notice:I = 0x7f090fcc

.field public static final shealth_comfort_level_notice_2:I = 0x7f090fdb

.field public static final shealth_comfort_level_notice_3:I = 0x7f090fdc

.field public static final shealth_ecg_received:I = 0x7f091089

.field public static final shealth_ecg_step_1:I = 0x7f091082

.field public static final shealth_ecg_step_2:I = 0x7f091083

.field public static final shealth_ecg_step_3:I = 0x7f091084

.field public static final shealth_ecg_step_4:I = 0x7f091086

.field public static final shealth_ecg_step_5:I = 0x7f091087

.field public static final shealth_ecg_tip_1:I = 0x7f091085

.field public static final shealth_ecg_tip_2:I = 0x7f091088

.field public static final shealth_ecg_title:I = 0x7f091081

.field public static final shealth_exercise_diary_description_1:I = 0x7f090ff6

.field public static final shealth_exercise_diary_description_2:I = 0x7f090ff7

.field public static final shealth_features:I = 0x7f090cdd

.field public static final shealth_food_diary_description_1:I = 0x7f090fe7

.field public static final shealth_food_diary_description_2:I = 0x7f090fe8

.field public static final shealth_food_diary_notice_1:I = 0x7f090fe9

.field public static final shealth_food_diary_notice_1_kor:I = 0x7f090fea

.field public static final shealth_health_board_description:I = 0x7f090fd0

.field public static final shealth_health_board_description1:I = 0x7f090fd1

.field public static final shealth_health_board_see_chart:I = 0x7f090fcf

.field public static final shealth_health_board_shown_screen:I = 0x7f090fce

.field public static final shealth_health_board_summary_information:I = 0x7f090fcd

.field public static final shealth_healthcare_diary_weigth_description_1:I = 0x7f090feb

.field public static final shealth_healthcare_diary_weigth_description_3:I = 0x7f090fec

.field public static final shealth_healthcare_diary_weigth_description_4:I = 0x7f090fed

.field public static final shealth_heartrate_icon_1:I = 0x7f09100b

.field public static final shealth_heartrate_step_1:I = 0x7f09100c

.field public static final shealth_heartrate_step_1_no_sensor:I = 0x7f091008

.field public static final shealth_heartrate_step_1_tts_no_sensor:I = 0x7f091009

.field public static final shealth_heartrate_step_2:I = 0x7f09100d

.field public static final shealth_heartrate_step_3:I = 0x7f09100e

.field public static final shealth_heartrate_step_4:I = 0x7f09100f

.field public static final shealth_heartrate_step_5:I = 0x7f091010

.field public static final shealth_heartrate_tip_1:I = 0x7f091011

.field public static final shealth_heartrate_tip_1_no_sensor:I = 0x7f09100a

.field public static final shealth_heartrate_tip_2:I = 0x7f091012

.field public static final shealth_heartrate_tip_3:I = 0x7f091013

.field public static final shealth_heartrate_tip_4:I = 0x7f091014

.field public static final shealth_heartrate_tip_4_1:I = 0x7f091015

.field public static final shealth_heartrate_tip_4_2:I = 0x7f091016

.field public static final shealth_heartrate_tip_4_3:I = 0x7f091017

.field public static final shealth_heartrate_tip_4_4:I = 0x7f091018

.field public static final shealth_heartrate_tip_4_5:I = 0x7f091019

.field public static final shealth_heartrate_tip_4_6:I = 0x7f09101a

.field public static final shealth_heartrate_title:I = 0x7f091006

.field public static final shealth_heartrate_title_no_sensor:I = 0x7f091007

.field public static final shealth_help_intro:I = 0x7f090fc9

.field public static final shealth_help_pedometer_button:I = 0x7f09106f

.field public static final shealth_home_description:I = 0x7f090fd2

.field public static final shealth_home_description_1:I = 0x7f090fd3

.field public static final shealth_home_description_2:I = 0x7f090fd4

.field public static final shealth_home_description_3:I = 0x7f090fd5

.field public static final shealth_home_description_4:I = 0x7f090fd6

.field public static final shealth_installing_more_apps_step_1:I = 0x7f091032

.field public static final shealth_installing_more_apps_step_2:I = 0x7f091033

.field public static final shealth_installing_more_apps_step_3:I = 0x7f091034

.field public static final shealth_installing_more_apps_step_4:I = 0x7f091035

.field public static final shealth_installing_more_apps_title:I = 0x7f091036

.field public static final shealth_managing_weight_step_1:I = 0x7f09103e

.field public static final shealth_managing_weight_step_2:I = 0x7f09103f

.field public static final shealth_managing_weight_step_2_btn_upper:I = 0x7f09112b

.field public static final shealth_managing_weight_step_3:I = 0x7f091040

.field public static final shealth_managing_weight_tip_1:I = 0x7f091041

.field public static final shealth_managing_weight_tip_2:I = 0x7f091042

.field public static final shealth_managing_weight_title:I = 0x7f09103d

.field public static final shealth_measuring_stress_level_icon_1:I = 0x7f091048

.field public static final shealth_measuring_stress_level_step_1:I = 0x7f091049

.field public static final shealth_measuring_stress_level_step_2:I = 0x7f09104a

.field public static final shealth_measuring_stress_level_step_3:I = 0x7f09104b

.field public static final shealth_measuring_stress_level_step_4:I = 0x7f09104c

.field public static final shealth_measuring_stress_level_step_5:I = 0x7f09104d

.field public static final shealth_measuring_stress_level_step_6:I = 0x7f09104e

.field public static final shealth_measuring_stress_level_tip_1:I = 0x7f09104f

.field public static final shealth_measuring_stress_level_tip_2:I = 0x7f091050

.field public static final shealth_measuring_stress_level_tip_3:I = 0x7f091051

.field public static final shealth_measuring_stress_level_tip_4_1:I = 0x7f091052

.field public static final shealth_measuring_stress_level_tip_4_2:I = 0x7f091053

.field public static final shealth_measuring_stress_level_tip_4_3:I = 0x7f091054

.field public static final shealth_measuring_stress_level_title:I = 0x7f091047

.field public static final shealth_measuring_uv_level_for_gear_step_1:I = 0x7f091116

.field public static final shealth_measuring_uv_level_step_1:I = 0x7f091056

.field public static final shealth_measuring_uv_level_step_2_t:I = 0x7f091057

.field public static final shealth_measuring_uv_level_step_3_t:I = 0x7f091058

.field public static final shealth_measuring_uv_level_step_4:I = 0x7f091059

.field public static final shealth_measuring_uv_level_talk_1:I = 0x7f091061

.field public static final shealth_measuring_uv_level_tip_2:I = 0x7f09105a

.field public static final shealth_measuring_uv_level_tip_3:I = 0x7f09105b

.field public static final shealth_measuring_uv_level_tip_4:I = 0x7f09105c

.field public static final shealth_measuring_uv_level_tip_5:I = 0x7f09105d

.field public static final shealth_measuring_uv_level_tip_6:I = 0x7f09105e

.field public static final shealth_measuring_uv_level_tip_7:I = 0x7f09105f

.field public static final shealth_measuring_uv_level_tip_8:I = 0x7f091060

.field public static final shealth_measuring_uv_level_title:I = 0x7f091055

.field public static final shealth_measuring_uv_level_title_for_gear:I = 0x7f091115

.field public static final shealth_measuring_uv_view_latest_data_tts:I = 0x7f091118

.field public static final shealth_news_description_1:I = 0x7f091004

.field public static final shealth_news_description_2:I = 0x7f091005

.field public static final shealth_pedometer_more_button:I = 0x7f09106e

.field public static final shealth_pedometer_select_device_to_view:I = 0x7f090fe6

.field public static final shealth_pedometer_step_1:I = 0x7f090fe0

.field public static final shealth_pedometer_step_2:I = 0x7f090fe1

.field public static final shealth_pedometer_step_2_for_all_model:I = 0x7f090c31

.field public static final shealth_pedometer_step_2_for_menukey:I = 0x7f090fe2

.field public static final shealth_pedometer_step_goal:I = 0x7f090fe5

.field public static final shealth_pedometer_tip_txt_1:I = 0x7f090fe3

.field public static final shealth_pedometer_tip_txt_2:I = 0x7f090fe4

.field public static final shealth_pedometer_title:I = 0x7f090fdf

.field public static final shealth_restore_done:I = 0x7f0902ee

.field public static final shealth_restore_inprogress:I = 0x7f0902ed

.field public static final shealth_running_pro_description_1:I = 0x7f090ff8

.field public static final shealth_running_pro_description_2:I = 0x7f090ff9

.field public static final shealth_set_up_wizard:I = 0x7f0910e5

.field public static final shealth_sleep_monitor_description_1:I = 0x7f091001

.field public static final shealth_sleep_monitor_description_2:I = 0x7f091002

.field public static final shealth_sleep_monitor_notice:I = 0x7f091003

.field public static final shealth_spo2_step_1:I = 0x7f09101c

.field public static final shealth_spo2_step_1_tts:I = 0x7f09101d

.field public static final shealth_spo2_tip_1:I = 0x7f09101e

.field public static final shealth_spo2_tip_3:I = 0x7f09101f

.field public static final shealth_spo2_tip_4:I = 0x7f091020

.field public static final shealth_spo2_tip_4_2:I = 0x7f091021

.field public static final shealth_spo2_tip_5:I = 0x7f091022

.field public static final shealth_spo2_tip_5_1:I = 0x7f091023

.field public static final shealth_spo2_tip_5_1_tts:I = 0x7f091128

.field public static final shealth_spo2_tip_5_2:I = 0x7f091024

.field public static final shealth_spo2_tip_5_3:I = 0x7f091025

.field public static final shealth_spo2_tip_5_4:I = 0x7f091026

.field public static final shealth_spo2_tip_5_5:I = 0x7f091027

.field public static final shealth_spo2_tip_5_5_tts:I = 0x7f091129

.field public static final shealth_spo2_tip_5_6:I = 0x7f091028

.field public static final shealth_spo2_tip_5_7:I = 0x7f091029

.field public static final shealth_spo2_tip_5_7_tts:I = 0x7f09112a

.field public static final shealth_spo2_tip_5_tts:I = 0x7f091127

.field public static final shealth_spo2_title:I = 0x7f09101b

.field public static final shealth_spo2_title_tts:I = 0x7f091126

.field public static final shealth_tgh_step_1:I = 0x7f09108c

.field public static final shealth_tgh_step_2_1:I = 0x7f09108d

.field public static final shealth_tgh_step_2_2:I = 0x7f09108e

.field public static final shealth_tgh_tip_1:I = 0x7f09108f

.field public static final shealth_tgh_title:I = 0x7f09108b

.field public static final shealth_thermohygrometer_title:I = 0x7f09108a

.field public static final shealth_viewing_sleep_data_step_1:I = 0x7f091044

.field public static final shealth_viewing_sleep_data_step_2:I = 0x7f091045

.field public static final shealth_viewing_sleep_data_tip_1:I = 0x7f091046

.field public static final shealth_viewing_sleep_data_title:I = 0x7f091043

.field public static final shealth_walk_mate_description:I = 0x7f090fdd

.field public static final shealth_walk_mate_warning:I = 0x7f090fde

.field public static final shealth_workout_detail_txt_1:I = 0x7f090fff

.field public static final shealth_workout_detail_txt_2:I = 0x7f091000

.field public static final shealth_workout_step_1:I = 0x7f090ffb

.field public static final shealth_workout_step_1_ttk:I = 0x7f090ffc

.field public static final shealth_workout_step_2:I = 0x7f090ffd

.field public static final shealth_workout_tip_1:I = 0x7f090ffe

.field public static final shealth_workout_title:I = 0x7f090ffa

.field public static final sign_in:I = 0x7f090873

.field public static final sign_in_samsung_account_text:I = 0x7f090ce2

.field public static final signed_out:I = 0x7f090884

.field public static final simplified_chinese:I = 0x7f0909b0

.field public static final since:I = 0x7f0908e3

.field public static final sitting_time_fmt:I = 0x7f090ba1

.field public static final skip:I = 0x7f090045

.field public static final skipped:I = 0x7f0909c8

.field public static final sleep:I = 0x7f09017e

.field public static final sleep_1_hr:I = 0x7f090d71

.field public static final sleep_1_hr_arab:I = 0x7f090bff

.field public static final sleep_1_min:I = 0x7f090d73

.field public static final sleep_1_min_arab:I = 0x7f090c00

.field public static final sleep_Sync_not_device:I = 0x7f090d79

.field public static final sleep_Sync_waiting:I = 0x7f090d7e

.field public static final sleep_accessories_guide_1_new:I = 0x7f090d68

.field public static final sleep_accessories_guide_2_new:I = 0x7f090d69

.field public static final sleep_app_name:I = 0x7f090d56

.field public static final sleep_average_no_data:I = 0x7f090d8d

.field public static final sleep_average_time_slept:I = 0x7f090d90

.field public static final sleep_avg_last_days:I = 0x7f090d65

.field public static final sleep_avg_time_spent_motionless:I = 0x7f090d91

.field public static final sleep_consist_notice:I = 0x7f090d6d

.field public static final sleep_deleted:I = 0x7f090d6c

.field public static final sleep_double_digit_minute:I = 0x7f090d85

.field public static final sleep_empty_data:I = 0x7f090d88

.field public static final sleep_finger_accessoryactivity:I = 0x7f090d7f

.field public static final sleep_gm_message:I = 0x7f090d7d

.field public static final sleep_hour_label:I = 0x7f090d5b

.field public static final sleep_hr:I = 0x7f090d5c

.field public static final sleep_hrs:I = 0x7f090d72

.field public static final sleep_hrs_mins:I = 0x7f090d89

.field public static final sleep_last_synced:I = 0x7f090d5f

.field public static final sleep_mins_without_hrs:I = 0x7f090d8a

.field public static final sleep_motionless:I = 0x7f090d8e

.field public static final sleep_motionless_1_hr:I = 0x7f090d70

.field public static final sleep_motionless_1_hr_arab:I = 0x7f090bfe

.field public static final sleep_motionless_1hr_1min:I = 0x7f090d62

.field public static final sleep_motionless_1hr_1min_arab:I = 0x7f090bfc

.field public static final sleep_motionless_1hr_mins:I = 0x7f090d61

.field public static final sleep_motionless_1hr_mins_arab:I = 0x7f090bfb

.field public static final sleep_motionless_for:I = 0x7f090d6a

.field public static final sleep_motionless_hrs:I = 0x7f090d6e

.field public static final sleep_motionless_hrs_1min:I = 0x7f090d63

.field public static final sleep_motionless_hrs_1min_arab:I = 0x7f090bfd

.field public static final sleep_motionless_hrs_and_mins:I = 0x7f090d86

.field public static final sleep_motionless_hrs_mins:I = 0x7f090d60

.field public static final sleep_motionless_hrs_mrs_empty:I = 0x7f090d64

.field public static final sleep_motionless_hrs_one_min:I = 0x7f090d66

.field public static final sleep_motionless_mins:I = 0x7f090d87

.field public static final sleep_motionless_mrs:I = 0x7f090d6f

.field public static final sleep_motionless_one_hr_mins:I = 0x7f090d67

.field public static final sleep_motionless_sleep:I = 0x7f090d58

.field public static final sleep_movement:I = 0x7f090d5e

.field public static final sleep_percent:I = 0x7f090d5d

.field public static final sleep_popup_sleep_efficiency:I = 0x7f090d7a

.field public static final sleep_print_unable_to_print:I = 0x7f090d7c

.field public static final sleep_rate:I = 0x7f090d8b

.field public static final sleep_rating:I = 0x7f090d75

.field public static final sleep_rating_1:I = 0x7f090d74

.field public static final sleep_recent_meals:I = 0x7f090d7b

.field public static final sleep_reset_content:I = 0x7f090d6b

.field public static final sleep_samsung_gear:I = 0x7f090d81

.field public static final sleep_samsung_gear_fit:I = 0x7f090d80

.field public static final sleep_single_digit_minute:I = 0x7f090d84

.field public static final sleep_sleep_accessories:I = 0x7f09017f

.field public static final sleep_sync_finished:I = 0x7f090d78

.field public static final sleep_synced_device:I = 0x7f090d77

.field public static final sleep_time:I = 0x7f090d59

.field public static final sleep_total_no_data:I = 0x7f090d8c

.field public static final sleep_total_sleep:I = 0x7f090d57

.field public static final sleep_total_sleep_new:I = 0x7f090d8f

.field public static final sleep_unknown_device:I = 0x7f090d76

.field public static final sleep_wakeup_time:I = 0x7f090d5a

.field public static final sleep_wingtip:I = 0x7f090d83

.field public static final slowest:I = 0x7f090ac4

.field public static final snacks:I = 0x7f090923

.field public static final sns_uploading:I = 0x7f090af9

.field public static final sodium:I = 0x7f090965

.field public static final sonstiges_german:I = 0x7f090eca

.field public static final sonstiges_text_german:I = 0x7f090ecb

.field public static final spanish:I = 0x7f0909af

.field public static final spanish_latin:I = 0x7f0909b1

.field public static final speak_in_now:I = 0x7f0909a5

.field public static final speak_the_food_you_ate:I = 0x7f0909a1

.field public static final special_t_and_c:I = 0x7f090e93

.field public static final special_t_and_c_text1:I = 0x7f090ea4

.field public static final special_t_and_c_text1_au:I = 0x7f090ea6

.field public static final special_t_and_c_text1_newzealand:I = 0x7f090ea7

.field public static final special_t_and_c_text1_south_africa:I = 0x7f090ee1

.field public static final special_t_and_c_text1_uk:I = 0x7f090ea5

.field public static final speech_settings:I = 0x7f0906e8

.field public static final speed:I = 0x7f090a49

.field public static final spo2:I = 0x7f090240

.field public static final spo2_accessory_type:I = 0x7f090f9a

.field public static final spo2_action_bar_title:I = 0x7f090f95

.field public static final spo2_action_bar_title_tts:I = 0x7f09111e

.field public static final spo2_avg:I = 0x7f090fa7

.field public static final spo2_back_key_finish:I = 0x7f090f97

.field public static final spo2_back_key_finish_tts:I = 0x7f091120

.field public static final spo2_dialog_title:I = 0x7f090fad

.field public static final spo2_dialog_title_tts:I = 0x7f091125

.field public static final spo2_discard_reading:I = 0x7f090f9d

.field public static final spo2_discard_reading_tts:I = 0x7f091121

.field public static final spo2_first_measure_warning_1:I = 0x7f090f96

.field public static final spo2_first_measure_warning_1_tts:I = 0x7f09111f

.field public static final spo2_font_light:I = 0x7f090fa0

.field public static final spo2_font_regular:I = 0x7f090f9f

.field public static final spo2_high_number:I = 0x7f090fa4

.field public static final spo2_information_1:I = 0x7f090fa8

.field public static final spo2_information_2:I = 0x7f090faa

.field public static final spo2_information_2_tts:I = 0x7f091124

.field public static final spo2_information_3:I = 0x7f090fab

.field public static final spo2_information_4:I = 0x7f090fac

.field public static final spo2_information_non_med_1:I = 0x7f090fa9

.field public static final spo2_log_no_data_message:I = 0x7f090f99

.field public static final spo2_low_number:I = 0x7f090fa3

.field public static final spo2_measure_finish_status_msg:I = 0x7f090f9e

.field public static final spo2_measure_finish_status_msg_2:I = 0x7f090f9c

.field public static final spo2_measure_finish_status_msg_3:I = 0x7f090f9b

.field public static final spo2_measure_finish_status_msg_tts:I = 0x7f091122

.field public static final spo2_ninety:I = 0x7f090fa5

.field public static final spo2_ninetyfive:I = 0x7f090fa6

.field public static final spo2_percent:I = 0x7f090fa1

.field public static final spo2_pulse:I = 0x7f090f98

.field public static final spo2_tts:I = 0x7f090241

.field public static final spo2_warning_text:I = 0x7f090fa2

.field public static final spo2_warning_text_tts:I = 0x7f091123

.field public static final stair_climbing:I = 0x7f090b62

.field public static final start:I = 0x7f090050

.field public static final start_pedometer:I = 0x7f090b90

.field public static final start_pedometer_util:I = 0x7f090051

.field public static final start_short:I = 0x7f090c0c

.field public static final start_workout:I = 0x7f090a4a

.field public static final statistic_from_ada:I = 0x7f0901b2

.field public static final statistic_from_jnc7:I = 0x7f0901b9

.field public static final statistic_from_kda:I = 0x7f0901b3

.field public static final statistic_from_who:I = 0x7f0901b8

.field public static final statistics_ada:I = 0x7f09027c

.field public static final statistics_jnc_7:I = 0x7f09027d

.field public static final step:I = 0x7f090baa

.field public static final step_count:I = 0x7f090b71

.field public static final step_status_run:I = 0x7f090621

.field public static final step_status_run_down:I = 0x7f09070a

.field public static final step_status_run_up:I = 0x7f090709

.field public static final step_status_stop:I = 0x7f09061f

.field public static final step_status_walk:I = 0x7f090620

.field public static final step_status_walk_down:I = 0x7f090708

.field public static final step_status_walk_up:I = 0x7f090707

.field public static final steps:I = 0x7f0900c6

.field public static final steps_to_go_to_catch_up:I = 0x7f090b7e

.field public static final stm_average:I = 0x7f090c40

.field public static final stm_back_key_finish:I = 0x7f090c37

.field public static final stm_finalising:I = 0x7f090c32

.field public static final stm_first_measure_finalising:I = 0x7f090c36

.field public static final stm_first_measure_warning_0:I = 0x7f090c3a

.field public static final stm_first_mesure_fourth_phase:I = 0x7f090c35

.field public static final stm_first_mesure_second_phase:I = 0x7f090c33

.field public static final stm_first_mesure_third_phase:I = 0x7f090c34

.field public static final stm_high:I = 0x7f090c3d

.field public static final stm_invaild:I = 0x7f090c3e

.field public static final stm_low:I = 0x7f090c3c

.field public static final stm_measure_discard:I = 0x7f090c3b

.field public static final stm_stress:I = 0x7f090c39

.field public static final stm_stress_level:I = 0x7f090c38

.field public static final stm_very_high:I = 0x7f090c41

.field public static final stm_very_low:I = 0x7f090c3f

.field public static final stop:I = 0x7f09004e

.field public static final stop_pedo_dash_board:I = 0x7f090dfc

.field public static final stop_pedometer:I = 0x7f090b91

.field public static final stress:I = 0x7f090026

.field public static final stress_acccessories_guide_frist:I = 0x7f090c42

.field public static final stress_acccessories_guide_second:I = 0x7f090c43

.field public static final stress_accessories:I = 0x7f090027

.field public static final stress_score_test:I = 0x7f090f47

.field public static final sugars:I = 0x7f09096c

.field public static final sum:I = 0x7f090070

.field public static final summary_view:I = 0x7f09005f

.field public static final summer_str:I = 0x7f09109f

.field public static final sun:I = 0x7f0900f4

.field public static final sun_sat:I = 0x7f090133

.field public static final sun_short:I = 0x7f0900fb

.field public static final sunday_three_chars:I = 0x7f090d05

.field public static final symbol_dot:I = 0x7f090c9c

.field public static final sync_data:I = 0x7f0900a5

.field public static final sync_data_q:I = 0x7f0900a6

.field public static final sync_failed:I = 0x7f0900a7

.field public static final syncing_with_s:I = 0x7f0900a8

.field public static final systolic:I = 0x7f0901b5

.field public static final t_range:I = 0x7f090f83

.field public static final tag:I = 0x7f090f19

.field public static final take_on_missions:I = 0x7f09034f

.field public static final talk_back_hour:I = 0x7f090dc1

.field public static final talk_back_hours:I = 0x7f090dc3

.field public static final talk_back_minute:I = 0x7f090dc2

.field public static final talk_back_minutes:I = 0x7f090dc4

.field public static final tanina_scaletype_1_text_1:I = 0x7f0902e2

.field public static final tanina_scaletype_1_text_2:I = 0x7f0902e3

.field public static final tanina_scaletype_2_text_1:I = 0x7f0902e4

.field public static final tanina_scaletype_2_text_2:I = 0x7f0902e5

.field public static final tanina_type_1_scale_text_1:I = 0x7f0902d1

.field public static final tanina_type_1_scale_text_2:I = 0x7f0902d2

.field public static final tanina_type_2_scale_text_1:I = 0x7f0902d3

.field public static final tanina_type_2_scale_text_2:I = 0x7f0902d4

.field public static final tanita_scaletype_1_text_1:I = 0x7f09113f

.field public static final tanita_scaletype_1_text_2:I = 0x7f091140

.field public static final tanita_scaletype_2_text_1:I = 0x7f091141

.field public static final tanita_scaletype_2_text_2:I = 0x7f091142

.field public static final tanita_type_1_scale_text_1:I = 0x7f09113b

.field public static final tanita_type_1_scale_text_2:I = 0x7f09113c

.field public static final tanita_type_2_scale_text_1:I = 0x7f09113d

.field public static final tanita_type_2_scale_text_2:I = 0x7f09113e

.field public static final tap:I = 0x7f0902d0

.field public static final tap_here_to_unlock_shealth:I = 0x7f090262

.field public static final tap_to_add_a_meal:I = 0x7f090cfd

.field public static final tap_to_add_icon:I = 0x7f09090c

.field public static final tap_to_continue:I = 0x7f090893

.field public static final tap_to_view_your_step_count:I = 0x7f09110d

.field public static final tap_uninstall:I = 0x7f090276

.field public static final target_date:I = 0x7f090caf

.field public static final tc_australia_content_1:I = 0x7f09071f

.field public static final tc_australia_content_2:I = 0x7f090720

.field public static final tc_australia_content_3:I = 0x7f090721

.field public static final tc_australia_start_content_1:I = 0x7f090725

.field public static final tc_australia_start_content_2:I = 0x7f090726

.field public static final tc_australia_start_content_3:I = 0x7f090727

.field public static final tc_australia_start_content_4:I = 0x7f090728

.field public static final tc_australia_subject_10_content_1:I = 0x7f090757

.field public static final tc_australia_subject_10_content_2:I = 0x7f090758

.field public static final tc_australia_subject_10_content_3:I = 0x7f090759

.field public static final tc_australia_subject_10_content_4:I = 0x7f09075a

.field public static final tc_australia_subject_10_content_4_1:I = 0x7f09075b

.field public static final tc_australia_subject_10_content_4_2:I = 0x7f09075c

.field public static final tc_australia_subject_10_content_5:I = 0x7f09075d

.field public static final tc_australia_subject_10_content_6:I = 0x7f09075e

.field public static final tc_australia_subject_10_title:I = 0x7f090756

.field public static final tc_australia_subject_11_content_1:I = 0x7f090760

.field public static final tc_australia_subject_11_title:I = 0x7f09075f

.field public static final tc_australia_subject_12_content_1:I = 0x7f090761

.field public static final tc_australia_subject_1_content_1:I = 0x7f090730

.field public static final tc_australia_subject_1_content_2:I = 0x7f090731

.field public static final tc_australia_subject_1_content_3:I = 0x7f090732

.field public static final tc_australia_subject_1_title:I = 0x7f09072f

.field public static final tc_australia_subject_2_content_1:I = 0x7f090734

.field public static final tc_australia_subject_2_content_2:I = 0x7f090735

.field public static final tc_australia_subject_2_title:I = 0x7f090733

.field public static final tc_australia_subject_3_content_1:I = 0x7f090737

.field public static final tc_australia_subject_3_title:I = 0x7f090736

.field public static final tc_australia_subject_4_content_1:I = 0x7f090739

.field public static final tc_australia_subject_4_content_2:I = 0x7f09073a

.field public static final tc_australia_subject_4_content_3:I = 0x7f09073b

.field public static final tc_australia_subject_4_content_4:I = 0x7f09073c

.field public static final tc_australia_subject_4_content_5:I = 0x7f09073d

.field public static final tc_australia_subject_4_title:I = 0x7f090738

.field public static final tc_australia_subject_5_a_content_1:I = 0x7f09073f

.field public static final tc_australia_subject_5_a_content_2:I = 0x7f090740

.field public static final tc_australia_subject_5_a_content_3:I = 0x7f090741

.field public static final tc_australia_subject_5_a_content_4:I = 0x7f090742

.field public static final tc_australia_subject_5_a_content_5:I = 0x7f090743

.field public static final tc_australia_subject_5_a_title:I = 0x7f09073e

.field public static final tc_australia_subject_5_b_content_1:I = 0x7f090745

.field public static final tc_australia_subject_5_b_content_2:I = 0x7f090746

.field public static final tc_australia_subject_5_b_content_3:I = 0x7f090747

.field public static final tc_australia_subject_5_b_content_4:I = 0x7f090748

.field public static final tc_australia_subject_5_b_content_5:I = 0x7f090749

.field public static final tc_australia_subject_5_b_title:I = 0x7f090744

.field public static final tc_australia_subject_6_content_1:I = 0x7f09074b

.field public static final tc_australia_subject_6_content_2:I = 0x7f09074c

.field public static final tc_australia_subject_6_title:I = 0x7f09074a

.field public static final tc_australia_subject_7_content_1:I = 0x7f09074e

.field public static final tc_australia_subject_7_content_2:I = 0x7f09074f

.field public static final tc_australia_subject_7_title:I = 0x7f09074d

.field public static final tc_australia_subject_8_content_1:I = 0x7f090751

.field public static final tc_australia_subject_8_content_2:I = 0x7f090752

.field public static final tc_australia_subject_8_content_3:I = 0x7f090753

.field public static final tc_australia_subject_8_title:I = 0x7f090750

.field public static final tc_australia_subject_9_content_1:I = 0x7f090755

.field public static final tc_australia_subject_9_title:I = 0x7f090754

.field public static final tc_australia_use_of_shealth_content_1:I = 0x7f09072a

.field public static final tc_australia_use_of_shealth_content_2:I = 0x7f09072b

.field public static final tc_australia_use_of_shealth_content_3:I = 0x7f09072c

.field public static final tc_australia_use_of_shealth_content_4:I = 0x7f09072d

.field public static final tc_australia_use_of_shealth_content_5:I = 0x7f09072e

.field public static final tc_australia_use_of_shealth_title:I = 0x7f090729

.field public static final te_guide_popup:I = 0x7f090ae5

.field public static final te_guide_popup_max:I = 0x7f090ae7

.field public static final te_guide_popup_min:I = 0x7f090ae6

.field public static final temp_string_about_source:I = 0x7f0905e2

.field public static final temp_string_about_source_press:I = 0x7f0905e3

.field public static final temperature:I = 0x7f090130

.field public static final temperature_Fahrenheit:I = 0x7f0910a7

.field public static final temperature_c:I = 0x7f0900cd

.field public static final temperature_centigrade:I = 0x7f0910a8

.field public static final temperature_f:I = 0x7f0900ce

.field public static final term_and_conditions_of_nuance_underline:I = 0x7f090994

.field public static final term_of_service:I = 0x7f09099a

.field public static final term_of_service_separator:I = 0x7f090997

.field public static final terms_and_conditions:I = 0x7f0907eb

.field public static final terms_and_conditions_of_nuance:I = 0x7f090998

.field public static final terms_and_conditions_text_kor:I = 0x7f0905bf

.field public static final terms_and_conditions_text_kor_2:I = 0x7f0905c2

.field public static final terms_and_conditions_text_kor_3:I = 0x7f0905c3

.field public static final terms_health_service_1:I = 0x7f09070e

.field public static final terms_health_service_3:I = 0x7f09070f

.field public static final terms_health_service_4:I = 0x7f090710

.field public static final terms_health_service_5:I = 0x7f090711

.field public static final terms_health_service_6:I = 0x7f090712

.field public static final terms_health_service_7:I = 0x7f090713

.field public static final terms_health_service_title:I = 0x7f09070d

.field public static final terms_of_use:I = 0x7f0907ec

.field public static final terms_of_use_fifth_text:I = 0x7f0908bf

.field public static final terms_of_use_fifth_title:I = 0x7f0908be

.field public static final terms_of_use_fourth_text:I = 0x7f0908bd

.field public static final terms_of_use_fourth_title:I = 0x7f0908bc

.field public static final terms_of_use_second_text:I = 0x7f0908b9

.field public static final terms_of_use_second_title:I = 0x7f0908b8

.field public static final terms_of_use_third_text:I = 0x7f0908bb

.field public static final terms_of_use_third_title:I = 0x7f0908ba

.field public static final text_account_already_signed_in:I = 0x7f090d35

.field public static final text_account_not_signed_in:I = 0x7f090d34

.field public static final text_limit_exceeded:I = 0x7f090f1b

.field public static final text_to_speech_output_setting:I = 0x7f0906e9

.field public static final text_with_percent:I = 0x7f090d1a

.field public static final tgh_checking_humidity:I = 0x7f0910b6

.field public static final tgh_checking_surroundings:I = 0x7f0910b7

.field public static final tgh_checking_temperature:I = 0x7f0910b5

.field public static final tgh_font_light:I = 0x7f0910b0

.field public static final tgh_high:I = 0x7f0910af

.field public static final tgh_high_humidity:I = 0x7f0910b3

.field public static final tgh_high_temperature:I = 0x7f0910b1

.field public static final tgh_low:I = 0x7f0910ae

.field public static final tgh_low_humidity:I = 0x7f0910b4

.field public static final tgh_low_temperature:I = 0x7f0910b2

.field public static final tgh_no_data:I = 0x7f0910b8

.field public static final tgh_title:I = 0x7f090242

.field public static final there_is_already_a_meal_plan_check_the_repeat_and_period_of_meal_plan:I = 0x7f090986

.field public static final there_is_no_backup_data:I = 0x7f090900

.field public static final thermo_hygrometer:I = 0x7f090cf5

.field public static final third:I = 0x7f090b7a

.field public static final this_data_will_be_deleted:I = 0x7f09094c

.field public static final thu:I = 0x7f0900f1

.field public static final thu_short:I = 0x7f0900f8

.field public static final thursday_three_chars:I = 0x7f090d09

.field public static final time:I = 0x7f090136

.field public static final time_active:I = 0x7f090a1c

.field public static final time_and_date:I = 0x7f090a50

.field public static final time_goal:I = 0x7f090a23

.field public static final time_goal_hint:I = 0x7f090a24

.field public static final time_of_day:I = 0x7f0907c3

.field public static final time_without_activity:I = 0x7f0907c1

.field public static final timex_device:I = 0x7f090e13

.field public static final timex_device_hrm:I = 0x7f090e14

.field public static final timex_device_link:I = 0x7f090dff

.field public static final tips:I = 0x7f090c02

.field public static final to_connect_the_accessory:I = 0x7f0902cf

.field public static final today:I = 0x7f09005d

.field public static final today_planned_meal_will_be_deleted:I = 0x7f090987

.field public static final todays_achievements:I = 0x7f0908e7

.field public static final todays_activity:I = 0x7f0908e6

.field public static final top_10_last_7_days:I = 0x7f090b74

.field public static final total:I = 0x7f09008a

.field public static final total_calories:I = 0x7f090939

.field public static final total_carbohydrate:I = 0x7f090961

.field public static final total_fat:I = 0x7f09096f

.field public static final total_steps:I = 0x7f090b65

.field public static final total_steps_count:I = 0x7f090bcf

.field public static final total_steps_for_this_day:I = 0x7f090bd0

.field public static final tracker_pedometer_device_source_pop_up_text:I = 0x7f09112f

.field public static final tracker_pedometer_healthy_pace_information:I = 0x7f091139

.field public static final tracker_pedometer_notification_content_inactive_time:I = 0x7f091130

.field public static final tracker_pedometer_notification_content_target_achieved:I = 0x7f091138

.field public static final training_easy_guide:I = 0x7f090aea

.field public static final training_effect:I = 0x7f090a5a

.field public static final training_effect_about:I = 0x7f090a60

.field public static final training_effect_about_guide:I = 0x7f090ab1

.field public static final training_effect_easy:I = 0x7f090ad9

.field public static final training_effect_easy_content:I = 0x7f090a63

.field public static final training_effect_goal:I = 0x7f090a27

.field public static final training_effect_goal_guide:I = 0x7f090618

.field public static final training_effect_goal_hint:I = 0x7f090a28

.field public static final training_effect_hard_content:I = 0x7f090a65

.field public static final training_effect_improving:I = 0x7f090ad8

.field public static final training_effect_infomation:I = 0x7f090a5b

.field public static final training_effect_infomation_hint:I = 0x7f0906e4

.field public static final training_effect_level_easy:I = 0x7f09013c

.field public static final training_effect_level_easy_guide:I = 0x7f090ab3

.field public static final training_effect_level_easy_text_hint:I = 0x7f090a5d

.field public static final training_effect_level_hard:I = 0x7f09013e

.field public static final training_effect_level_hard_guide:I = 0x7f090ab5

.field public static final training_effect_level_hard_text_hint:I = 0x7f090a5f

.field public static final training_effect_level_low:I = 0x7f09013b

.field public static final training_effect_level_low_guide:I = 0x7f090ab2

.field public static final training_effect_level_low_text_hint:I = 0x7f090a5c

.field public static final training_effect_level_moderate:I = 0x7f09013d

.field public static final training_effect_level_moderate_guide:I = 0x7f090ab4

.field public static final training_effect_level_moderate_text_hint:I = 0x7f090a5e

.field public static final training_effect_moderate_content:I = 0x7f090a64

.field public static final training_effect_recommended:I = 0x7f090a61

.field public static final training_effect_recommended_hour:I = 0x7f090a67

.field public static final training_effect_value:I = 0x7f090ae9

.field public static final training_effect_very_easy_content:I = 0x7f090a62

.field public static final training_improving_guide:I = 0x7f090aec

.field public static final training_keep_fit_guide:I = 0x7f090aeb

.field public static final trans_fat:I = 0x7f09096e

.field public static final try_again:I = 0x7f090057

.field public static final try_it_drag_the_handle:I = 0x7f0906ca

.field public static final try_it_enter_exercise_info:I = 0x7f0906b1

.field public static final try_it_general_interaction_completed:I = 0x7f0906cd

.field public static final try_it_graph:I = 0x7f0906c9

.field public static final try_it_more:I = 0x7f0906c0

.field public static final try_it_more_reset:I = 0x7f0906c6

.field public static final try_it_new_step:I = 0x7f0906c3

.field public static final try_it_pause:I = 0x7f0906c5

.field public static final try_it_reset:I = 0x7f0906c7

.field public static final try_it_select_the_device:I = 0x7f0906b3

.field public static final try_it_set_goal:I = 0x7f0906c1

.field public static final try_it_set_goal_save:I = 0x7f0906c2

.field public static final try_it_set_the_workout_paln:I = 0x7f0906ad

.field public static final try_it_start:I = 0x7f0906c4

.field public static final try_it_tap_log_data:I = 0x7f0906cc

.field public static final try_it_tap_the_pedometer:I = 0x7f0906c8

.field public static final try_it_tap_to_accessory:I = 0x7f0906b2

.field public static final try_it_tap_to_pause:I = 0x7f0906af

.field public static final try_it_tap_to_start:I = 0x7f0906ae

.field public static final try_it_tap_to_stop:I = 0x7f0906b0

.field public static final try_it_tap_view_more:I = 0x7f0906cb

.field public static final try_it_the_selected_device:I = 0x7f0906b4

.field public static final try_it_you_have_completed:I = 0x7f0906b5

.field public static final try_others:I = 0x7f0909a3

.field public static final tryit_add_device:I = 0x7f09028e

.field public static final tryit_device_added:I = 0x7f09028f

.field public static final tryit_finish_glucose:I = 0x7f090299

.field public static final tryit_finish_pressure:I = 0x7f090293

.field public static final tryit_finish_weight:I = 0x7f09029a

.field public static final tryit_scan:I = 0x7f09028d

.field public static final ts_enter_name_to_continue_tpop:I = 0x7f090837

.field public static final ts_invalid_type_character_tpop:I = 0x7f090838

.field public static final ts_select_gender_to_continue_tpop:I = 0x7f090839

.field public static final ts_select_i_agree_tickbox_to_use_s_health_tpop:I = 0x7f0908c3

.field public static final ts_you_cannot_enter_data_for_the_future_tpop:I = 0x7f090836

.field public static final tue:I = 0x7f0900ef

.field public static final tue_short:I = 0x7f0900f6

.field public static final tuesday_three_chars:I = 0x7f090d07

.field public static final turn_on_wifi:I = 0x7f09078d

.field public static final turning_on_bluetooth:I = 0x7f09008d

.field public static final twitter_share:I = 0x7f0901f5

.field public static final txt_account_header:I = 0x7f090d36

.field public static final txt_account_sa:I = 0x7f090d38

.field public static final txt_account_sa_already:I = 0x7f090d37

.field public static final type_of_feedback:I = 0x7f0906e7

.field public static final ultra_mini:I = 0x7f09029c

.field public static final unable_backup_during_shealth_running:I = 0x7f0900b8

.field public static final unable_backup_during_workout:I = 0x7f090802

.field public static final unable_to_add_picture:I = 0x7f090ac5

.field public static final unable_to_backup_toast_msg:I = 0x7f090d50

.field public static final unable_to_connect_data_limit:I = 0x7f09079a

.field public static final unable_to_connect_no_signal:I = 0x7f09079b

.field public static final unable_to_connect_popup:I = 0x7f090790

.field public static final unable_to_device:I = 0x7f0907a6

.field public static final unable_to_duplicated_restore_msg:I = 0x7f090d52

.field public static final unable_to_print:I = 0x7f0900e5

.field public static final unable_to_restore_backed_up_version:I = 0x7f0907f4

.field public static final unable_to_restore_toast_msg:I = 0x7f090d51

.field public static final unable_to_set_goal:I = 0x7f090901

.field public static final undefined_unit:I = 0x7f090d0f

.field public static final under_calorie_goal_value_message:I = 0x7f090a8b

.field public static final under_distance_goal_value_message:I = 0x7f090a8a

.field public static final under_time_goal_value_message:I = 0x7f090a6c

.field public static final undo:I = 0x7f09091e

.field public static final unfavorite:I = 0x7f090089

.field public static final uninstall:I = 0x7f090917

.field public static final uninstall_fitness_with_gear:I = 0x7f090275

.field public static final unit_settings:I = 0x7f0907bc

.field public static final unit_settings_hint:I = 0x7f0907bd

.field public static final unknown:I = 0x7f090d03

.field public static final unknown_dialog_type_choose_type_exception:I = 0x7f09019b

.field public static final unlink_account:I = 0x7f090d3b

.field public static final unlock_s_health:I = 0x7f090260

.field public static final unpair:I = 0x7f09008c

.field public static final unread_talkback:I = 0x7f09071d

.field public static final unselect_all:I = 0x7f090073

.field public static final up:I = 0x7f0901a9

.field public static final update:I = 0x7f090055

.field public static final update_auto:I = 0x7f090adc

.field public static final update_new_version_text:I = 0x7f0900db

.field public static final update_new_version_title:I = 0x7f0900dc

.field public static final update_now:I = 0x7f090056

.field public static final update_to_latest_version:I = 0x7f0908b3

.field public static final upgrade_data_replace_confirm_popup_msg:I = 0x7f090887

.field public static final upgrade_migrating_popup_msg:I = 0x7f090888

.field public static final uranus:I = 0x7f0906a8

.field public static final usb_step_2:I = 0x7f0902c2

.field public static final use_of_s_health:I = 0x7f0908b6

.field public static final use_of_s_health_end_ver2:I = 0x7f090ea2

.field public static final use_of_s_health_text:I = 0x7f0908b7

.field public static final use_of_s_health_text_1:I = 0x7f090e86

.field public static final use_of_s_health_text_1_austria:I = 0x7f090ecd

.field public static final use_of_s_health_text_1_french:I = 0x7f090eb8

.field public static final use_of_s_health_text_1_switzerland:I = 0x7f090ecc

.field public static final use_of_s_health_text_1_ver2:I = 0x7f090e94

.field public static final use_of_s_health_text_1_ver2_angola:I = 0x7f090ee8

.field public static final use_of_s_health_text_1_ver2_bosnia:I = 0x7f090ed2

.field public static final use_of_s_health_text_1_ver2_canada:I = 0x7f090eea

.field public static final use_of_s_health_text_1_ver2_kenya:I = 0x7f090ede

.field public static final use_of_s_health_text_1_ver2_portugal:I = 0x7f090ee5

.field public static final use_of_s_health_text_2:I = 0x7f090e87

.field public static final use_of_s_health_text_2_german:I = 0x7f090ebe

.field public static final use_of_s_health_text_2_russia:I = 0x7f090eae

.field public static final use_of_s_health_text_2_ver2:I = 0x7f090e95

.field public static final use_of_s_health_text_2_ver2_albania:I = 0x7f090ecf

.field public static final use_of_s_health_text_2_ver2_angola:I = 0x7f090ee9

.field public static final use_of_s_health_text_2_ver2_canada:I = 0x7f090eeb

.field public static final use_of_s_health_text_2_ver2_congo:I = 0x7f090ee3

.field public static final use_of_s_health_text_2_ver2_german:I = 0x7f090ebf

.field public static final use_of_s_health_text_2_ver2_maxico:I = 0x7f090ea8

.field public static final use_of_s_health_text_2_ver2_montenegro:I = 0x7f090ed6

.field public static final use_of_s_health_text_2_ver2_morocco:I = 0x7f090eb9

.field public static final use_of_s_health_text_2_ver2_portugal:I = 0x7f090ee6

.field public static final use_of_s_health_text_3:I = 0x7f090e88

.field public static final use_of_s_health_text_3_german:I = 0x7f090ec0

.field public static final use_of_s_health_text_3_russia:I = 0x7f090eaf

.field public static final use_of_s_health_text_3_spanish:I = 0x7f090ea9

.field public static final use_of_s_health_text_3_ver2:I = 0x7f090e96

.field public static final use_of_s_health_text_3_ver2_canada:I = 0x7f090eec

.field public static final use_of_s_health_text_4:I = 0x7f090e89

.field public static final use_of_s_health_text_4_2_ver2_canada:I = 0x7f090eee

.field public static final use_of_s_health_text_4_german:I = 0x7f090ec1

.field public static final use_of_s_health_text_4_russia:I = 0x7f090eb0

.field public static final use_of_s_health_text_4_ver2:I = 0x7f090e97

.field public static final use_of_s_health_text_4_ver2_canada:I = 0x7f090eed

.field public static final use_of_s_health_text_5_ver2:I = 0x7f090e98

.field public static final use_of_s_health_title:I = 0x7f090e85

.field public static final use_of_s_health_title_german:I = 0x7f090ebd

.field public static final use_of_s_health_title_russia:I = 0x7f090ead

.field public static final user_manual:I = 0x7f09003c

.field public static final user_name:I = 0x7f09001f

.field public static final users:I = 0x7f090b89

.field public static final uv:I = 0x7f09002f

.field public static final uv_accessories:I = 0x7f09023d

.field public static final uv_accessories_guide_1:I = 0x7f090d9d

.field public static final uv_accessories_guide_2:I = 0x7f090d9e

.field public static final uv_back_key_finish:I = 0x7f090d99

.field public static final uv_category:I = 0x7f090dba

.field public static final uv_china_medium_recommended_message:I = 0x7f091152

.field public static final uv_china_medium_recommended_message_title:I = 0x7f091153

.field public static final uv_china_strong_recommended_message:I = 0x7f091154

.field public static final uv_china_strong_recommended_message_title:I = 0x7f091155

.field public static final uv_china_stronger_recommended_message:I = 0x7f091156

.field public static final uv_china_stronger_recommended_message_title:I = 0x7f091157

.field public static final uv_china_weak_recommended_message:I = 0x7f091150

.field public static final uv_china_weak_recommended_message_title:I = 0x7f091151

.field public static final uv_china_weakest_recommended_message:I = 0x7f09114e

.field public static final uv_china_weakest_recommended_message_title:I = 0x7f09114f

.field public static final uv_disclaimer:I = 0x7f090df6

.field public static final uv_disclaimer_original_1:I = 0x7f090df7

.field public static final uv_disclaimer_original_2:I = 0x7f090df8

.field public static final uv_disclaimer_original_3:I = 0x7f090df9

.field public static final uv_extreme:I = 0x7f090d98

.field public static final uv_failure_pop_up_string:I = 0x7f090dc6

.field public static final uv_finished_discard:I = 0x7f090df5

.field public static final uv_high:I = 0x7f090d96

.field public static final uv_hrs:I = 0x7f090df2

.field public static final uv_index_widget:I = 0x7f0910bf

.field public static final uv_location_turn_on:I = 0x7f090d92

.field public static final uv_location_turn_on_dialog_content:I = 0x7f090d93

.field public static final uv_low:I = 0x7f090d94

.field public static final uv_max:I = 0x7f090d9c

.field public static final uv_measuring_message:I = 0x7f090d9b

.field public static final uv_medium:I = 0x7f09115a

.field public static final uv_mins:I = 0x7f090df3

.field public static final uv_moderate:I = 0x7f090d95

.field public static final uv_ready_message_vz:I = 0x7f090d9a

.field public static final uv_shealth_information_1:I = 0x7f090de9

.field public static final uv_shealth_information_2:I = 0x7f090deb

.field public static final uv_shealth_information_2_more_info:I = 0x7f090dee

.field public static final uv_shealth_information_3:I = 0x7f090dec

.field public static final uv_shealth_information_4:I = 0x7f090ded

.field public static final uv_shealth_infromation_1_hestia:I = 0x7f090dea

.field public static final uv_shealth_warning_1:I = 0x7f090def

.field public static final uv_shealth_warning_2:I = 0x7f090df0

.field public static final uv_shealth_warning_3_vzw:I = 0x7f090df1

.field public static final uv_skin_dialog_information:I = 0x7f090df4

.field public static final uv_skin_do_not_use_settings_label:I = 0x7f090dad

.field public static final uv_skin_tone_message_always_burns:I = 0x7f090da1

.field public static final uv_skin_tone_message_always_burns_desc:I = 0x7f090da2

.field public static final uv_skin_tone_message_burns_easily:I = 0x7f090da3

.field public static final uv_skin_tone_message_burns_easily_desc:I = 0x7f090da4

.field public static final uv_skin_tone_message_burns_minimally:I = 0x7f090da7

.field public static final uv_skin_tone_message_burns_minimally_desc:I = 0x7f090da8

.field public static final uv_skin_tone_message_burns_moderately:I = 0x7f090da5

.field public static final uv_skin_tone_message_burns_moderately_desc:I = 0x7f090da6

.field public static final uv_skin_tone_message_never_burns:I = 0x7f090dab

.field public static final uv_skin_tone_message_never_burns_desc:I = 0x7f090dac

.field public static final uv_skin_tone_message_rarely_burns:I = 0x7f090da9

.field public static final uv_skin_tone_message_rarely_burns_desc:I = 0x7f090daa

.field public static final uv_skin_tone_question_1:I = 0x7f090d9f

.field public static final uv_skin_tone_question_2:I = 0x7f090da0

.field public static final uv_skin_type_label:I = 0x7f090dae

.field public static final uv_spf_label:I = 0x7f090db1

.field public static final uv_strong:I = 0x7f09115b

.field public static final uv_stronger:I = 0x7f09115c

.field public static final uv_sun_block:I = 0x7f090db0

.field public static final uv_sun_category:I = 0x7f090db9

.field public static final uv_sun_exposure_limit:I = 0x7f090daf

.field public static final uv_sun_protection_iime_1hr:I = 0x7f090db3

.field public static final uv_sun_protection_recommended_messages_1_message_1:I = 0x7f090dd2

.field public static final uv_sun_protection_recommended_messages_1_message_2:I = 0x7f090dd4

.field public static final uv_sun_protection_recommended_messages_1_message_2_new:I = 0x7f090dd3

.field public static final uv_sun_protection_recommended_messages_1_message_3:I = 0x7f090dd5

.field public static final uv_sun_protection_recommended_messages_2_message_1:I = 0x7f090dd6

.field public static final uv_sun_protection_recommended_messages_2_message_2:I = 0x7f090dd7

.field public static final uv_sun_protection_recommended_messages_2_message_3:I = 0x7f090dd8

.field public static final uv_sun_protection_recommended_messages_2_message_4:I = 0x7f090dd9

.field public static final uv_sun_protection_recommended_messages_3_message_1:I = 0x7f090dda

.field public static final uv_sun_protection_recommended_messages_3_message_1_new:I = 0x7f090ddb

.field public static final uv_sun_protection_recommended_messages_3_message_2:I = 0x7f090ddc

.field public static final uv_sun_protection_recommended_messages_3_message_3:I = 0x7f090ddd

.field public static final uv_sun_protection_recommended_messages_3_message_4:I = 0x7f090dde

.field public static final uv_sun_protection_recommended_messages_4_message_1:I = 0x7f090ddf

.field public static final uv_sun_protection_recommended_messages_4_message_1_new:I = 0x7f090de0

.field public static final uv_sun_protection_recommended_messages_4_message_2:I = 0x7f090de1

.field public static final uv_sun_protection_recommended_messages_4_message_3:I = 0x7f090de2

.field public static final uv_sun_protection_recommended_messages_4_message_4:I = 0x7f090de3

.field public static final uv_sun_protection_recommended_messages_5_message_1:I = 0x7f090de5

.field public static final uv_sun_protection_recommended_messages_5_message_1_new:I = 0x7f090de4

.field public static final uv_sun_protection_recommended_messages_5_message_2:I = 0x7f090de6

.field public static final uv_sun_protection_recommended_messages_5_message_3:I = 0x7f090de7

.field public static final uv_sun_protection_recommended_messages_5_message_4:I = 0x7f090de8

.field public static final uv_sun_protection_recommended_messages_title_1:I = 0x7f090dca

.field public static final uv_sun_protection_recommended_messages_title_2:I = 0x7f090dcb

.field public static final uv_sun_protection_recommended_messages_title_3:I = 0x7f090dcc

.field public static final uv_sun_protection_recommended_messages_title_3_mini:I = 0x7f090dcf

.field public static final uv_sun_protection_recommended_messages_title_4:I = 0x7f090dcd

.field public static final uv_sun_protection_recommended_messages_title_4_mini:I = 0x7f090dd0

.field public static final uv_sun_protection_recommended_messages_title_5:I = 0x7f090dce

.field public static final uv_sun_protection_recommended_messages_title_5_mini:I = 0x7f090dd1

.field public static final uv_sun_protection_time_1min:I = 0x7f090db4

.field public static final uv_sun_protection_time_hr:I = 0x7f090db6

.field public static final uv_sun_protection_time_hrs:I = 0x7f090db2

.field public static final uv_sun_protection_time_min:I = 0x7f090db7

.field public static final uv_sun_protection_time_mins:I = 0x7f090db5

.field public static final uv_sun_recommended_minutes_talk_back_1minute:I = 0x7f090dc0

.field public static final uv_sun_recommended_minutes_talk_back_minute:I = 0x7f090dbf

.field public static final uv_sun_recommended_minutes_talk_back_minutes:I = 0x7f090dbe

.field public static final uv_sun_recommended_talk_back_1hour:I = 0x7f090dbc

.field public static final uv_sun_recommended_talk_back_hour:I = 0x7f090dbd

.field public static final uv_sun_recommended_talk_back_hours:I = 0x7f090dbb

.field public static final uv_sun_spf_value:I = 0x7f090db8

.field public static final uv_ts_select_i_agree_tickbox_to_use_tpop:I = 0x7f090dc5

.field public static final uv_veryhigh:I = 0x7f090d97

.field public static final uv_weak:I = 0x7f091159

.field public static final uv_weakest:I = 0x7f091158

.field public static final uv_wearable_no_data_info:I = 0x7f090dc8

.field public static final uv_wearable_no_data_text:I = 0x7f090dc9

.field public static final uv_without_sensor_dialog:I = 0x7f090dc7

.field public static final value_after_meal:I = 0x7f09026a

.field public static final value_fasting:I = 0x7f090269

.field public static final version:I = 0x7f0907ee

.field public static final vertical_progress_counter_default_value:I = 0x7f090285

.field public static final vertical_progressbar_counter_format:I = 0x7f090283

.field public static final vertical_progressbar_legend_format:I = 0x7f090284

.field public static final very_heavy_exercise_msg:I = 0x7f090865

.field public static final very_heavy_exercise_sub:I = 0x7f090864

.field public static final via_wifi_only:I = 0x7f090d4d

.field public static final view:I = 0x7f090947

.field public static final view_by:I = 0x7f090036

.field public static final view_details:I = 0x7f090949

.field public static final view_nutrition_info:I = 0x7f090932

.field public static final view_photos:I = 0x7f090946

.field public static final view_planed_eat_meal:I = 0x7f0909be

.field public static final view_planed_edit_meal:I = 0x7f0909bf

.field public static final view_planed_not_eat_meal:I = 0x7f0909c0

.field public static final viewdetails:I = 0x7f090f4a

.field public static final viewing_your_step_count:I = 0x7f09110c

.field public static final views_mode_selection_text:I = 0x7f090cfc

.field public static final vitamin_a:I = 0x7f090967

.field public static final vitamin_c:I = 0x7f090968

.field public static final voice_search:I = 0x7f09099b

.field public static final voice_search_enter_manualy:I = 0x7f0909a0

.field public static final voice_search_error_dialog_cannot_find_text:I = 0x7f09099d

.field public static final voice_search_error_dialog_fail_text:I = 0x7f09099e

.field public static final voice_search_language_text:I = 0x7f0909a2

.field public static final voice_search_try_others:I = 0x7f09099f

.field public static final wahoo_device:I = 0x7f090e15

.field public static final wahoo_device_hrm:I = 0x7f090e16

.field public static final wahoo_device_link:I = 0x7f090e00

.field public static final walk_activity_tracker:I = 0x7f090bbd

.field public static final walk_all_step_counts:I = 0x7f090bb7

.field public static final walk_cocktail_distance_init:I = 0x7f090be2

.field public static final walk_cocktail_distance_init_unit:I = 0x7f090be3

.field public static final walk_cocktail_kcal_init:I = 0x7f090be4

.field public static final walk_cocktail_kcal_init_unit:I = 0x7f090be5

.field public static final walk_cocktail_step_init:I = 0x7f090be0

.field public static final walk_cocktail_step_init_unit:I = 0x7f090be1

.field public static final walk_device_sensor:I = 0x7f090bb8

.field public static final walk_from_to:I = 0x7f090bdc

.field public static final walk_mate:I = 0x7f090cf9

.field public static final walk_paused:I = 0x7f090a17

.field public static final walk_pedometer_acccessories_guide:I = 0x7f090bd1

.field public static final walk_pedometer_wearable_device_guide:I = 0x7f090bd2

.field public static final walk_refresh_btn_text:I = 0x7f090bda

.field public static final walk_samsung_gear1:I = 0x7f090bba

.field public static final walk_samsung_gear2:I = 0x7f09061c

.field public static final walk_samsung_gear3:I = 0x7f090bbb

.field public static final walk_samsung_gear_fit:I = 0x7f090bb9

.field public static final walk_samsung_gearo:I = 0x7f090bbc

.field public static final walk_step_k:I = 0x7f090bb4

.field public static final walk_sync_btn_text:I = 0x7f090bd9

.field public static final walk_sync_notefound:I = 0x7f0902b2

.field public static final walk_sync_notefound_message:I = 0x7f0902b1

.field public static final walk_synctitle:I = 0x7f0902b0

.field public static final walk_tap_to_start:I = 0x7f090f36

.field public static final walk_update:I = 0x7f090d1c

.field public static final walk_update_from:I = 0x7f090bdb

.field public static final walk_updateing:I = 0x7f090d1d

.field public static final walk_view_step_count:I = 0x7f090bb5

.field public static final walk_view_step_count_text:I = 0x7f090bb6

.field public static final walk_widget_synce_failed:I = 0x7f090bdf

.field public static final walking:I = 0x7f0909d2

.field public static final walking_german:I = 0x7f090785

.field public static final wallpaper_oreder_text:I = 0x7f090fae

.field public static final wallpapers:I = 0x7f090aab

.field public static final warning:I = 0x7f0900da

.field public static final watch:I = 0x7f090025

.field public static final watch_acc_guide:I = 0x7f090afa

.field public static final watch_garmin:I = 0x7f091147

.field public static final watch_samsung_gear:I = 0x7f091148

.field public static final water:I = 0x7f090cf3

.field public static final water_graph_series_legend:I = 0x7f090253

.field public static final water_graph_y_axis_title:I = 0x7f090254

.field public static final water_intake:I = 0x7f090cf2

.field public static final wed:I = 0x7f0900f0

.field public static final wed_short:I = 0x7f0900f7

.field public static final wednesday_three_chars:I = 0x7f090d08

.field public static final week_format:I = 0x7f090131

.field public static final week_of_day:I = 0x7f090067

.field public static final weighing_scale:I = 0x7f09018d

.field public static final weight:I = 0x7f09002c

.field public static final weight_connectivity_text:I = 0x7f090ccf

.field public static final weight_data_from_sensor_device:I = 0x7f09029e

.field public static final weight_data_receive_message:I = 0x7f09029f

.field public static final weight_goal_calorie_count:I = 0x7f090c9b

.field public static final weight_goal_label_burned_calories_per_day:I = 0x7f090c9a

.field public static final weight_goal_label_intake_per_day:I = 0x7f090c99

.field public static final weightmachine_unit:I = 0x7f09026e

.field public static final weightmachine_unit_lb:I = 0x7f09026f

.field public static final wfl_statistics_samsung_account_notice:I = 0x7f090ba5

.field public static final wgt_goal_weight_lost:I = 0x7f090c97

.field public static final wgt_goal_weight_should_lose:I = 0x7f090c98

.field public static final wgt_graph_information_area_amount_view_text_format:I = 0x7f090c95

.field public static final wgt_input_range_foramt:I = 0x7f090c96

.field public static final wgt_input_screen_info_text:I = 0x7f090cae

.field public static final wgt_log_filter_all:I = 0x7f090d13

.field public static final wgt_mobile_device:I = 0x7f090ca1

.field public static final wgt_outside_normal_range:I = 0x7f090c9f

.field public static final wgt_set_goal_goal_lose_calendar_info_1:I = 0x7f090cc8

.field public static final wgt_set_goal_kg_type_1:I = 0x7f090cb4

.field public static final wgt_set_goal_kg_type_2:I = 0x7f090cb5

.field public static final wgt_set_goal_kg_type_3:I = 0x7f090cb6

.field public static final wgt_set_goal_kg_type_4:I = 0x7f090cb7

.field public static final wgt_set_goal_lb_type_1:I = 0x7f090cb8

.field public static final wgt_set_goal_lb_type_2:I = 0x7f090cb9

.field public static final wgt_set_goal_lb_type_3:I = 0x7f090cba

.field public static final wgt_set_goal_lb_type_4:I = 0x7f090cbb

.field public static final wgt_set_goal_lose_progress_1:I = 0x7f090cb3

.field public static final wgt_set_goal_should_delete_message:I = 0x7f090cce

.field public static final wgt_set_goal_view_add_goal_top_label_text:I = 0x7f090cbd

.field public static final wgt_set_goal_view_alert_message_goal_not_less_weight:I = 0x7f090cc7

.field public static final wgt_set_goal_view_cancel_message:I = 0x7f090ccd

.field public static final wgt_set_goal_view_goal_healthy_range_text:I = 0x7f090cc6

.field public static final wgt_set_goal_view_goal_weight_unit_edit_text:I = 0x7f090cc9

.field public static final wgt_set_goal_view_lose_half_kg_one_week:I = 0x7f090cbe

.field public static final wgt_set_goal_view_lose_half_kg_two_weeks:I = 0x7f090cc0

.field public static final wgt_set_goal_view_lose_half_lb_one_week:I = 0x7f090cc2

.field public static final wgt_set_goal_view_lose_half_lb_two_weeks:I = 0x7f090cc4

.field public static final wgt_set_goal_view_lose_one_kg_one_week:I = 0x7f090cbf

.field public static final wgt_set_goal_view_lose_one_kg_two_weeks:I = 0x7f090cc1

.field public static final wgt_set_goal_view_lose_one_lb_one_week:I = 0x7f090cc3

.field public static final wgt_set_goal_view_lose_one_lb_two_weeks:I = 0x7f090cc5

.field public static final wgt_set_goal_view_no_data_text:I = 0x7f090cbc

.field public static final wgt_summary_view_body_year_label_text:I = 0x7f090ca6

.field public static final wgt_summary_view_center_view_bmi_format_text:I = 0x7f090c94

.field public static final wgt_summary_view_center_view_bmi_label_text:I = 0x7f090ca5

.field public static final wgt_summary_view_center_view_bmr_label_text:I = 0x7f090caa

.field public static final wgt_summary_view_center_view_body_fat_label_text:I = 0x7f090ca8

.field public static final wgt_summary_view_center_view_body_year_label_text:I = 0x7f090ca9

.field public static final wgt_summary_view_center_view_goal_gain:I = 0x7f090ca4

.field public static final wgt_summary_view_center_view_goal_lose:I = 0x7f090ca3

.field public static final wgt_summary_view_center_view_height_value_default_text_float:I = 0x7f090c92

.field public static final wgt_summary_view_center_view_height_value_default_text_int:I = 0x7f090c93

.field public static final wgt_summary_view_center_view_last_update_text:I = 0x7f090cad

.field public static final wgt_summary_view_center_view_skeletal_muscle_label_text:I = 0x7f090ca7

.field public static final wgt_summary_view_center_view_values_default_text:I = 0x7f090ca2

.field public static final wgt_summary_view_center_view_visceral_fat_label_text:I = 0x7f090cab

.field public static final wgt_summary_view_disclaimer_message:I = 0x7f090d12

.field public static final wgt_summary_view_disclaimer_skip_message:I = 0x7f090cac

.field public static final wgt_summary_view_no_data_in_profile:I = 0x7f090d11

.field public static final wgt_summary_view_weight_normal_range:I = 0x7f090d10

.field public static final wgt_try_challenge_achieved:I = 0x7f090ccc

.field public static final wgt_try_challenge_new:I = 0x7f090cca

.field public static final wgt_try_challenge_new_gain:I = 0x7f090ccb

.field public static final wgt_within_normal_range:I = 0x7f090ca0

.field public static final widget_app_launch_not_available:I = 0x7f090f48

.field public static final widget_distance:I = 0x7f090ba3

.field public static final widget_distance_m:I = 0x7f090623

.field public static final widget_distance_mi:I = 0x7f090ba4

.field public static final widget_tap_to_measure:I = 0x7f0910bd

.field public static final winter_str:I = 0x7f0910a0

.field public static final within_normal_range:I = 0x7f0901ac

.field public static final women_bmr:I = 0x7f09085a

.field public static final work_out:I = 0x7f0908f1

.field public static final work_plan_no_workout_title:I = 0x7f0906eb

.field public static final workout:I = 0x7f090aa7

.field public static final workout_account:I = 0x7f0909d4

.field public static final workout_account_option:I = 0x7f0909fc

.field public static final workout_activity_added_to_favorite:I = 0x7f090612

.field public static final workout_activity_removed_from_favorite:I = 0x7f090613

.field public static final workout_activitynotfoundexecption_pro:I = 0x7f09060f

.field public static final workout_add_exercise_list:I = 0x7f0909dc

.field public static final workout_add_log:I = 0x7f0909dd

.field public static final workout_add_new_activity:I = 0x7f09067b

.field public static final workout_aerobic_exercise:I = 0x7f09067c

.field public static final workout_aerobic_highimpact:I = 0x7f090690

.field public static final workout_aerobic_low_impact:I = 0x7f090b4b

.field public static final workout_aerobics:I = 0x7f090b0f

.field public static final workout_all_food_list:I = 0x7f0909de

.field public static final workout_all_list_exercises:I = 0x7f0909df

.field public static final workout_alphabet:I = 0x7f0909eb

.field public static final workout_aquarobics:I = 0x7f090b03

.field public static final workout_archery_non_hunting:I = 0x7f090629

.field public static final workout_backstroke_recreational:I = 0x7f090672

.field public static final workout_backstroke_training:I = 0x7f090671

.field public static final workout_badminton:I = 0x7f090b1e

.field public static final workout_badminton_competitive:I = 0x7f090b4c

.field public static final workout_badminton_social_singles_and_doubles:I = 0x7f090630

.field public static final workout_ballet:I = 0x7f090662

.field public static final workout_ballroom_dance_fast:I = 0x7f09062e

.field public static final workout_ballroom_dance_slow:I = 0x7f090b48

.field public static final workout_barbell_benchpress:I = 0x7f090648

.field public static final workout_barbell_bent_over_row:I = 0x7f09064e

.field public static final workout_barbell_curl:I = 0x7f090652

.field public static final workout_barbell_squat:I = 0x7f090659

.field public static final workout_barbell_upright_row:I = 0x7f09064f

.field public static final workout_baseball:I = 0x7f090b06

.field public static final workout_basketball:I = 0x7f090b18

.field public static final workout_basketball_game:I = 0x7f09062d

.field public static final workout_basketball_general:I = 0x7f090b4d

.field public static final workout_bicycling:I = 0x7f090622

.field public static final workout_bicycling_fast:I = 0x7f090636

.field public static final workout_bicycling_moderate:I = 0x7f090666

.field public static final workout_bicycling_slow:I = 0x7f090635

.field public static final workout_bicycling_very_fast:I = 0x7f090637

.field public static final workout_billiard:I = 0x7f090b39

.field public static final workout_billiards:I = 0x7f090b1f

.field public static final workout_body_fat:I = 0x7f0909e8

.field public static final workout_bowling:I = 0x7f090b07

.field public static final workout_boxing:I = 0x7f090b20

.field public static final workout_boxing_in_ring:I = 0x7f090b46

.field public static final workout_boxing_punching_bag:I = 0x7f090669

.field public static final workout_boxing_sparring:I = 0x7f09066a

.field public static final workout_breaststroke_recreational:I = 0x7f090631

.field public static final workout_breaststroke_training:I = 0x7f090673

.field public static final workout_butterfly_stroke_swimming:I = 0x7f090632

.field public static final workout_calores_tab_middle_coment:I = 0x7f090682

.field public static final workout_calorie_burn:I = 0x7f0905f9

.field public static final workout_calorie_info:I = 0x7f09061b

.field public static final workout_calories_burnt_in_30_mins:I = 0x7f09067a

.field public static final workout_canoeing_rowing:I = 0x7f090674

.field public static final workout_change:I = 0x7f0909e5

.field public static final workout_chin_up:I = 0x7f090b14

.field public static final workout_clear_query:I = 0x7f0909ee

.field public static final workout_connect_accessory_from:I = 0x7f090616

.field public static final workout_connect_accessory_to:I = 0x7f090617

.field public static final workout_connect_mobile:I = 0x7f09060a

.field public static final workout_create_training_plan:I = 0x7f09068c

.field public static final workout_crunch:I = 0x7f090660

.field public static final workout_custom_distance:I = 0x7f09068b

.field public static final workout_dance:I = 0x7f090b0d

.field public static final workout_dancing:I = 0x7f090663

.field public static final workout_data_from_gear:I = 0x7f090d0d

.field public static final workout_data_from_gear2:I = 0x7f090d0e

.field public static final workout_data_from_pedometer:I = 0x7f090a11

.field public static final workout_data_from_wingtip:I = 0x7f090d0c

.field public static final workout_delete_goal:I = 0x7f090a08

.field public static final workout_detected_watch:I = 0x7f0909f5

.field public static final workout_disable_airplanemode:I = 0x7f09060b

.field public static final workout_dumbbell_benchpress:I = 0x7f090647

.field public static final workout_dumbbell_kickback:I = 0x7f090657

.field public static final workout_dumbbell_lateral_raise:I = 0x7f09064b

.field public static final workout_dumbbell_overhead_press:I = 0x7f09064d

.field public static final workout_editbox_account_hint:I = 0x7f090a12

.field public static final workout_enter_type_of_activity:I = 0x7f090679

.field public static final workout_error_occur:I = 0x7f0909e9

.field public static final workout_exercise_improve_fitness_level:I = 0x7f090a0e

.field public static final workout_exercise_muscle_input_description:I = 0x7f090684

.field public static final workout_exercise_no_period_set:I = 0x7f090688

.field public static final workout_exercise_of_muscle:I = 0x7f09067d

.field public static final workout_exercise_period_preferred_days:I = 0x7f090689

.field public static final workout_exercise_select_fitness_level_goal:I = 0x7f090a0d

.field public static final workout_exercise_training_program_explanation:I = 0x7f09070b

.field public static final workout_exercise_training_sub_title:I = 0x7f09068a

.field public static final workout_exericse_fitness_belowaverage:I = 0x7f090a0c

.field public static final workout_exericse_fitness_information:I = 0x7f09068e

.field public static final workout_exericse_fitness_wellbelowaverage:I = 0x7f09068d

.field public static final workout_favorite_remove:I = 0x7f0909ea

.field public static final workout_field_hockey:I = 0x7f090694

.field public static final workout_fitness_bike:I = 0x7f090b2c

.field public static final workout_fitness_climber:I = 0x7f090b2e

.field public static final workout_fitness_elliptical:I = 0x7f090b2b

.field public static final workout_fitness_general:I = 0x7f090b29

.field public static final workout_fitness_level:I = 0x7f090a0b

.field public static final workout_fitness_nordic_skier:I = 0x7f090b2f

.field public static final workout_fitness_rower:I = 0x7f090b2d

.field public static final workout_fitness_treadmill:I = 0x7f090b2a

.field public static final workout_flat_bench_dumbbell_flye:I = 0x7f090649

.field public static final workout_free_style_swimming_fast:I = 0x7f090670

.field public static final workout_free_style_swimming_slow:I = 0x7f090b3d

.field public static final workout_goal_weight:I = 0x7f0906ab

.field public static final workout_golf:I = 0x7f090b05

.field public static final workout_half_marathon:I = 0x7f090676

.field public static final workout_handball:I = 0x7f090b21

.field public static final workout_handball_general:I = 0x7f090b4a

.field public static final workout_handball_team:I = 0x7f09066b

.field public static final workout_hanglider:I = 0x7f090b17

.field public static final workout_hangliding:I = 0x7f090b45

.field public static final workout_hockey_ice_competitive:I = 0x7f09062a

.field public static final workout_horseback_riding:I = 0x7f090b08

.field public static final workout_how_far_recently_run:I = 0x7f090a09

.field public static final workout_how_long_5km:I = 0x7f090624

.field public static final workout_hrm_accessary_guide:I = 0x7f09060c

.field public static final workout_hrm_connected:I = 0x7f0909f4

.field public static final workout_hrm_heart_rate_zone_description:I = 0x7f090b57

.field public static final workout_hrm_heart_rate_zone_guide_over_range:I = 0x7f090b58

.field public static final workout_hrm_heart_rate_zone_guide_under_range:I = 0x7f090b5a

.field public static final workout_hrm_heart_rate_zone_guide_within_range:I = 0x7f090b59

.field public static final workout_hula_hoop:I = 0x7f090b11

.field public static final workout_hula_hooping:I = 0x7f090692

.field public static final workout_ice_dancing:I = 0x7f090644

.field public static final workout_ice_hockey_general:I = 0x7f090695

.field public static final workout_ice_skating_general:I = 0x7f090b3e

.field public static final workout_ice_skating_rapidly:I = 0x7f090640

.field public static final workout_ice_skating_slow:I = 0x7f09063f

.field public static final workout_incline_barbell_press:I = 0x7f09064a

.field public static final workout_incline_dumbbell_curl:I = 0x7f090654

.field public static final workout_inline_skating:I = 0x7f090b0e

.field public static final workout_inline_skating_fast:I = 0x7f090665

.field public static final workout_inline_skating_normal:I = 0x7f090b3f

.field public static final workout_inline_skating_slow:I = 0x7f090664

.field public static final workout_invalid_sign_in:I = 0x7f090a00

.field public static final workout_judo:I = 0x7f090b22

.field public static final workout_jumping_rope:I = 0x7f090b10

.field public static final workout_kilocalories:I = 0x7f0909e6

.field public static final workout_last_synced:I = 0x7f0909d7

.field public static final workout_last_updated:I = 0x7f0909d8

.field public static final workout_lat_pulldown:I = 0x7f090651

.field public static final workout_leg_extension:I = 0x7f090658

.field public static final workout_leg_press:I = 0x7f09065a

.field public static final workout_log_noitem_bottom_text:I = 0x7f090683

.field public static final workout_loose_half_one_week_kg:I = 0x7f090696

.field public static final workout_loose_half_two_weeks_kg:I = 0x7f090698

.field public static final workout_loose_one_one_week_kg:I = 0x7f090697

.field public static final workout_loose_one_one_week_lb:I = 0x7f09069a

.field public static final workout_loose_one_two_weeks_kg:I = 0x7f090699

.field public static final workout_loose_one_two_weeks_lb:I = 0x7f09069b

.field public static final workout_loose_two_one_week_lb:I = 0x7f09069c

.field public static final workout_loose_two_two_weeks_lb:I = 0x7f09069d

.field public static final workout_lying_barbell_extension:I = 0x7f090655

.field public static final workout_lying_leg_curl:I = 0x7f09065d

.field public static final workout_marathon:I = 0x7f090677

.field public static final workout_martial_arts_moderate_pace:I = 0x7f090b40

.field public static final workout_martial_arts_slower_pace:I = 0x7f090b32

.field public static final workout_mostly_used_exercises:I = 0x7f090619

.field public static final workout_my_activities:I = 0x7f0909e4

.field public static final workout_my_list:I = 0x7f0909e3

.field public static final workout_netpulse_guide:I = 0x7f0909f7

.field public static final workout_no_items_in_my_exercises:I = 0x7f0909ed

.field public static final workout_noresult:I = 0x7f090678

.field public static final workout_normal_walking:I = 0x7f090b36

.field public static final workout_nothing:I = 0x7f09061d

.field public static final workout_one_arm_dumbbell_row:I = 0x7f090650

.field public static final workout_ongoing_workout:I = 0x7f090a01

.field public static final workout_option_10km:I = 0x7f0906a0

.field public static final workout_option_5km:I = 0x7f09069f

.field public static final workout_option_km:I = 0x7f090a0a

.field public static final workout_paired_watch:I = 0x7f0909f6

.field public static final workout_password_hint:I = 0x7f0909fb

.field public static final workout_period_to_sync:I = 0x7f0909fd

.field public static final workout_pilates:I = 0x7f09068f

.field public static final workout_pistol_shooting:I = 0x7f090693

.field public static final workout_popup_remove_account:I = 0x7f0909ff

.field public static final workout_popup_steps:I = 0x7f0909d6

.field public static final workout_power_walking:I = 0x7f090b0c

.field public static final workout_preacher_curl_machine:I = 0x7f090653

.field public static final workout_press_up:I = 0x7f090b13

.field public static final workout_primary_display_about_pace_km_display:I = 0x7f090abe

.field public static final workout_primary_display_about_pace_mile_display:I = 0x7f090abf

.field public static final workout_primary_display_about_speed:I = 0x7f090abd

.field public static final workout_primary_display_about_speed_description:I = 0x7f0905e5

.field public static final workout_primary_display_about_speed_km_display:I = 0x7f090ac0

.field public static final workout_primary_display_about_speed_list_item_pace:I = 0x7f090a15

.field public static final workout_primary_display_about_speed_list_item_pace_mile:I = 0x7f090a16

.field public static final workout_primary_display_about_speed_list_item_speed:I = 0x7f090a13

.field public static final workout_primary_display_about_speed_list_item_speed_mile:I = 0x7f090a14

.field public static final workout_primary_display_about_speed_mile_display:I = 0x7f090ac1

.field public static final workout_primary_display_about_speed_title:I = 0x7f0906d8

.field public static final workout_pull_up:I = 0x7f090b47

.field public static final workout_push_up:I = 0x7f090646

.field public static final workout_realtime_data_no_goal:I = 0x7f090a02

.field public static final workout_recent_meals:I = 0x7f0909d5

.field public static final workout_recommendation:I = 0x7f090704

.field public static final workout_recommendation_description:I = 0x7f090705

.field public static final workout_recommended_calorie:I = 0x7f090a0f

.field public static final workout_recommended_calorie_2:I = 0x7f090a10

.field public static final workout_remaining_calories:I = 0x7f090a70

.field public static final workout_remaining_distance:I = 0x7f090a6f

.field public static final workout_remaining_time:I = 0x7f090a6e

.field public static final workout_repetition_set:I = 0x7f0906a1

.field public static final workout_reverse_crunch:I = 0x7f090661

.field public static final workout_rock_climbing:I = 0x7f090b15

.field public static final workout_rock_climbing_high_difficulty:I = 0x7f090627

.field public static final workout_rock_climbing_low_to_moderate_difficulty:I = 0x7f090b43

.field public static final workout_romanian_deadlift:I = 0x7f09065c

.field public static final workout_rope_pressdown:I = 0x7f090656

.field public static final workout_rugby_touch_non_competitive:I = 0x7f09062c

.field public static final workout_rugby_union_team_competitive:I = 0x7f09062b

.field public static final workout_running_10_mph:I = 0x7f0906f3

.field public static final workout_running_11_mph:I = 0x7f0906f4

.field public static final workout_running_12_mph:I = 0x7f0906f5

.field public static final workout_running_13_mph:I = 0x7f0906f6

.field public static final workout_running_14_mph:I = 0x7f0906f7

.field public static final workout_running_4_mph:I = 0x7f0906ed

.field public static final workout_running_5_mph:I = 0x7f0906ee

.field public static final workout_running_6_mph:I = 0x7f0906ef

.field public static final workout_running_7_mph:I = 0x7f0906f0

.field public static final workout_running_8_mph:I = 0x7f0906f1

.field public static final workout_running_9_mph:I = 0x7f0906f2

.field public static final workout_running_general:I = 0x7f090607

.field public static final workout_running_mph:I = 0x7f090608

.field public static final workout_running_program:I = 0x7f090610

.field public static final workout_running_program_no_item_popup:I = 0x7f0905e6

.field public static final workout_sailing_yachting_for_leisure:I = 0x7f090633

.field public static final workout_save_in_food_database:I = 0x7f0909ec

.field public static final workout_seated_calf_raise:I = 0x7f09065e

.field public static final workout_seated_leg_curl:I = 0x7f09065b

.field public static final workout_select_exercise:I = 0x7f0909db

.field public static final workout_select_goal_of_workout:I = 0x7f090ab0

.field public static final workout_select_goal_of_workout_german:I = 0x7f090786

.field public static final workout_selected_list_will_be_deleted:I = 0x7f090ac2

.field public static final workout_set_goal_weight:I = 0x7f0906a9

.field public static final workout_set_weight_loss_goal:I = 0x7f0906aa

.field public static final workout_set_your_calorie_goal_info3:I = 0x7f09069e

.field public static final workout_set_your_calorie_goal_info_second:I = 0x7f0909ef

.field public static final workout_short_alphabet:I = 0x7f09061a

.field public static final workout_show_password:I = 0x7f0909f9

.field public static final workout_sit_up:I = 0x7f090b04

.field public static final workout_skating:I = 0x7f090b27

.field public static final workout_skeletal_muscle:I = 0x7f0909e7

.field public static final workout_ski:I = 0x7f090b1b

.field public static final workout_skiing_snowboarding_light:I = 0x7f090643

.field public static final workout_skiing_snowboarding_moderate:I = 0x7f090b37

.field public static final workout_skiing_snowboarding_vigorous:I = 0x7f09063d

.field public static final workout_skin_scuba_diving:I = 0x7f090b26

.field public static final workout_skindiving_fast:I = 0x7f090b38

.field public static final workout_skindiving_moderate:I = 0x7f090675

.field public static final workout_skindiving_scuba_diving_general_rowing:I = 0x7f09063e

.field public static final workout_skipping_100_120_min:I = 0x7f0906f9

.field public static final workout_skipping_100_min:I = 0x7f090691

.field public static final workout_skipping_120_160_min:I = 0x7f0906f8

.field public static final workout_skipping_per_min:I = 0x7f090b41

.field public static final workout_slow_walking:I = 0x7f090625

.field public static final workout_smith_machine_upright_row:I = 0x7f09064c

.field public static final workout_snorkeling_for_leisure:I = 0x7f090634

.field public static final workout_snowboarding:I = 0x7f090b30

.field public static final workout_soccer:I = 0x7f090b1c

.field public static final workout_soccer_casual:I = 0x7f09062f

.field public static final workout_soccer_competitive:I = 0x7f090b49

.field public static final workout_softball:I = 0x7f090b24

.field public static final workout_softball_general:I = 0x7f090b3a

.field public static final workout_softball_pitching:I = 0x7f09066d

.field public static final workout_softball_practice:I = 0x7f09066c

.field public static final workout_speed_skating_competitive:I = 0x7f090641

.field public static final workout_sports_dance:I = 0x7f090b1a

.field public static final workout_squash:I = 0x7f090b19

.field public static final workout_squash_general:I = 0x7f090b3b

.field public static final workout_standing_calf_raise:I = 0x7f09065f

.field public static final workout_started_with_being_recorded:I = 0x7f090a89

.field public static final workout_started_with_being_recorded_with_hrm:I = 0x7f090acb

.field public static final workout_stationary_bicycle:I = 0x7f090b28

.field public static final workout_stationary_bicycle_201_270_watts_very_vigorous_effort:I = 0x7f09063b

.field public static final workout_stationary_bicycle_51_89_watts_light_to_moderate_effort:I = 0x7f09063c

.field public static final workout_stationary_bicycle_moderate_to_vigorous_effort_90_100_watts:I = 0x7f090638

.field public static final workout_stationary_bicycle_very_light_to_light_effort_30_50_watts:I = 0x7f090b3c

.field public static final workout_stationary_bicycle_vigorous_effort:I = 0x7f09063a

.field public static final workout_stationary_bicycle_vigorous_effort_101_160_watts:I = 0x7f090639

.field public static final workout_step_machine:I = 0x7f090b0a

.field public static final workout_steps:I = 0x7f0909d9

.field public static final workout_strength:I = 0x7f090645

.field public static final workout_strength_activity:I = 0x7f090b31

.field public static final workout_stretching_mild:I = 0x7f090626

.field public static final workout_swimming:I = 0x7f090b12

.field public static final workout_sync_now:I = 0x7f0909fe

.field public static final workout_syncing:I = 0x7f0902af

.field public static final workout_table_tennis:I = 0x7f090b09

.field public static final workout_taekwondo:I = 0x7f090b23

.field public static final workout_tai_chi_general:I = 0x7f090b33

.field public static final workout_tai_chi_light_effort:I = 0x7f090628

.field public static final workout_tai_chi_uan:I = 0x7f090b16

.field public static final workout_tb_add_log:I = 0x7f090a07

.field public static final workout_tb_btn_mine:I = 0x7f090a04

.field public static final workout_tb_decrease:I = 0x7f090a06

.field public static final workout_tb_increase:I = 0x7f090a05

.field public static final workout_tb_not_ticked:I = 0x7f0906be

.field public static final workout_tb_tick_box:I = 0x7f0906bf

.field public static final workout_tb_ticked:I = 0x7f0906bd

.field public static final workout_te_goal_achieved:I = 0x7f090b4e

.field public static final workout_tennis:I = 0x7f090b1d

.field public static final workout_tennis_doubles:I = 0x7f090668

.field public static final workout_tennis_general:I = 0x7f090b34

.field public static final workout_tennis_singles:I = 0x7f090667

.field public static final workout_time_value:I = 0x7f090f6d

.field public static final workout_total_exercise_time:I = 0x7f0909da

.field public static final workout_total_exercise_time_values:I = 0x7f0906ac

.field public static final workout_try_added_calories:I = 0x7f0909f1

.field public static final workout_try_enter_exercise:I = 0x7f0909f0

.field public static final workout_try_exercise_input:I = 0x7f0909f3

.field public static final workout_try_exercise_pick:I = 0x7f0909f2

.field public static final workout_try_it:I = 0x7f0906b6

.field public static final workout_try_it_display_the_record:I = 0x7f0906b7

.field public static final workout_try_it_exercise_mate_accessory_manager_button:I = 0x7f0906b9

.field public static final workout_try_it_exercise_mate_no_devices:I = 0x7f0906ce

.field public static final workout_try_it_exercise_mate_scan_button:I = 0x7f0906ba

.field public static final workout_try_it_exercise_mate_set_accessory_completed:I = 0x7f0906bb

.field public static final workout_try_it_exercise_mate_watch_scan_info:I = 0x7f0906bc

.field public static final workout_try_it_exercise_pro_guide_complete:I = 0x7f0906b8

.field public static final workout_volleyball:I = 0x7f090b25

.field public static final workout_volleyball_competitive:I = 0x7f090b35

.field public static final workout_volleyball_general:I = 0x7f09066e

.field public static final workout_walking_downstairs:I = 0x7f090b00

.field public static final workout_walking_upstairs:I = 0x7f090aff

.field public static final workout_watch_head:I = 0x7f090aaf

.field public static final workout_water_skiing:I = 0x7f090b02

.field public static final workout_wateractivities:I = 0x7f09066f

.field public static final workout_weight_machine:I = 0x7f090b0b

.field public static final workout_winter_activities:I = 0x7f090642

.field public static final workout_work_out_now:I = 0x7f090a03

.field public static final workout_xid:I = 0x7f0909f8

.field public static final workout_xid_hint_digit_to_digit:I = 0x7f0909fa

.field public static final workout_yoga:I = 0x7f090b01

.field public static final wrong_choose_type_exception:I = 0x7f090199

.field public static final wrong_dialog_type_exception:I = 0x7f090195

.field public static final wrong_password_message:I = 0x7f090126

.field public static final year:I = 0x7f090066

.field public static final yes:I = 0x7f090c90

.field public static final yesterday:I = 0x7f09005e

.field public static final you:I = 0x7f090b7f

.field public static final you_are_ranked_higher_than:I = 0x7f090b87

.field public static final you_are_ranked_within:I = 0x7f090b88

.field public static final you_can_also_back_up:I = 0x7f0908c4

.field public static final you_have_completed_the_food_guide:I = 0x7f090d02

.field public static final your_info:I = 0x7f090b7d

.field public static final your_total_distance_km_since:I = 0x7f090b83

.field public static final your_total_distance_miles_since:I = 0x7f090b84

.field public static final your_total_header:I = 0x7f090614

.field public static final your_total_header_m:I = 0x7f090615

.field public static final your_total_header_since:I = 0x7f090b8e

.field public static final yrs:I = 0x7f090f94

.field public static final yyyyMMMdE:I = 0x7f0901a5

.field public static final yyyy_MMM_dd:I = 0x7f090716

.field public static final yyyy_MMM_dd_HH_mm:I = 0x7f090719

.field public static final yyyy_MMM_dd_hh_mm_a:I = 0x7f09071c


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12589
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
