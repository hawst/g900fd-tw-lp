.class Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader$2;
.super Landroid/os/Handler;
.source "ExerciseProBitmapLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader;)V
    .locals 0

    .prologue
    .line 63
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 65
    if-nez p1, :cond_1

    .line 72
    :cond_0
    :goto_0
    return-void

    .line 67
    :cond_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader$Holder;

    .line 68
    .local v0, "holder":Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader$Holder;
    iget-object v1, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader$Holder;->bitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    .line 70
    iget-object v1, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader$Holder;->imageView:Landroid/widget/ImageView;

    iget-object v2, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader$Holder;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 71
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader;

    iget-object v2, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader$Holder;->url:Ljava/lang/String;

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader$Holder;->bitmap:Landroid/graphics/Bitmap;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader;->addBitmapToCache(Ljava/lang/String;Landroid/graphics/Bitmap;)V
    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader;->access$100(Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method
