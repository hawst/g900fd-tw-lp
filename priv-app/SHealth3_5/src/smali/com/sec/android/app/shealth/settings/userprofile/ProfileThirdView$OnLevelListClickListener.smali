.class public Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView$OnLevelListClickListener;
.super Ljava/lang/Object;
.source "ProfileThirdView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "OnLevelListClickListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;)V
    .locals 0

    .prologue
    .line 165
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView$OnLevelListClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 169
    const/4 v0, 0x0

    .line 170
    .local v0, "lev":I
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 193
    :goto_0
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-static {p1, v1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->announceForRadioBtnAccessibility(Landroid/view/View;Ljava/lang/String;ZZ)V

    .line 194
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView$OnLevelListClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;

    # invokes: Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->initView(I)V
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->access$100(Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;I)V

    .line 195
    return-void

    .line 172
    :sswitch_0
    const/4 v0, 0x1

    .line 173
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView$OnLevelListClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;

    const v2, 0x2bf21

    # setter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->mActivityType:I
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->access$002(Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;I)I

    goto :goto_0

    .line 176
    :sswitch_1
    const/4 v0, 0x2

    .line 177
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView$OnLevelListClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;

    const v2, 0x2bf22

    # setter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->mActivityType:I
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->access$002(Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;I)I

    goto :goto_0

    .line 180
    :sswitch_2
    const/4 v0, 0x3

    .line 181
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView$OnLevelListClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;

    const v2, 0x2bf23

    # setter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->mActivityType:I
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->access$002(Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;I)I

    goto :goto_0

    .line 184
    :sswitch_3
    const/4 v0, 0x4

    .line 185
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView$OnLevelListClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;

    const v2, 0x2bf24

    # setter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->mActivityType:I
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->access$002(Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;I)I

    goto :goto_0

    .line 188
    :sswitch_4
    const/4 v0, 0x5

    .line 189
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView$OnLevelListClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;

    const v2, 0x2bf25

    # setter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->mActivityType:I
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->access$002(Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;I)I

    goto :goto_0

    .line 170
    :sswitch_data_0
    .sparse-switch
        0x7f080864 -> :sswitch_0
        0x7f08086b -> :sswitch_1
        0x7f080872 -> :sswitch_2
        0x7f08087a -> :sswitch_3
        0x7f080882 -> :sswitch_4
    .end sparse-switch
.end method
