.class public Lcom/sec/android/app/shealth/thermohygrometer/summary/views/ThermoHygrometerSummaryView;
.super Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView;
.source "ThermoHygrometerSummaryView.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 34
    return-void
.end method


# virtual methods
.method protected createContentView()Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;
    .locals 2

    .prologue
    .line 43
    new-instance v0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/ThermoHygrometerContentView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/ThermoHygrometerSummaryView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/ThermoHygrometerContentView;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method protected createHeaderView()Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;
    .locals 2

    .prologue
    .line 38
    new-instance v0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/ThermoHygrometerHeaderView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/ThermoHygrometerSummaryView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/ThermoHygrometerHeaderView;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public updateView(FFLcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;ZZZZ)V
    .locals 1
    .param p1, "temperature"    # F
    .param p2, "humidity"    # F
    .param p3, "balanceState"    # Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;
    .param p4, "updateLeft"    # Z
    .param p5, "updateRight"    # Z
    .param p6, "animationflagtemp"    # Z
    .param p7, "animationflaghumid"    # Z

    .prologue
    .line 74
    if-eqz p6, :cond_0

    if-eqz p7, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, p3, v0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/ThermoHygrometerSummaryView;->updateHeaderViewBalanceState(Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;Z)V

    .line 75
    invoke-virtual {p0, p1, p2, p4, p5}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/ThermoHygrometerSummaryView;->updateHealthCareContentView(FFZZ)V

    .line 76
    return-void

    .line 74
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public updateView(IILcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;ZZZ)V
    .locals 1
    .param p1, "systolic"    # I
    .param p2, "diastolic"    # I
    .param p3, "balanceState"    # Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;
    .param p4, "updateLeft"    # Z
    .param p5, "updateRight"    # Z
    .param p6, "showIntermediateValues"    # Z

    .prologue
    .line 62
    if-eqz p4, :cond_0

    if-eqz p5, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, p3, v0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/ThermoHygrometerSummaryView;->updateHeaderViewBalanceState(Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;Z)V

    .line 63
    invoke-virtual {p0, p1, p2, p4, p5}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/ThermoHygrometerSummaryView;->updateHealthCareContentView(IIZZ)V

    .line 64
    return-void

    .line 62
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
