.class public Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;
.super Ljava/lang/Object;
.source "RealtimeGoalUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils$1;,
        Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils$TECheckTimerTask;,
        Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils$GoalListener;,
        Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils$GoalRate;
    }
.end annotation


# static fields
.field private static BeatMyPrevious_firstcheck:Z = false

.field private static DB_INTERVAL_DISTANCE:I = 0x0

.field private static final GOAL_INDEX_INVALID:I = -0x1

.field static currentGoalIndex:I = 0x0

.field public static db_index:I = 0x0

.field public static dbavgspeed:F = 0.0f

.field public static dbdistance:F = 0.0f

.field public static dbtime:F = 0.0f

.field public static diffdbduration:F = 0.0f

.field public static lastupdatedbdistance:I = 0x0

.field public static lastupdatedistance:I = 0x0

.field private static mAnalyzeCount:I = 0x0

.field private static mContext:Landroid/content/Context; = null

.field private static final mCountToNotify:I = 0x1e

.field private static mCurrentCalorie:I

.field private static mCurrentDistance:I

.field private static mCurrentDuration:I

.field private static mCurrentHR:I

.field private static mCurrentHearbeatTime:I

.field private static mCurrentRealElevation:F

.field private static mCurrentSpeed:F

.field static mGoalRateList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils$GoalRate;",
            ">;"
        }
    .end annotation
.end field

.field private static mIsActive:Z

.field private static mIsGoalAchievd:Z

.field private static mLastAudioGuideId:I

.field private static mLastAudioGuideParam:I

.field static mListener:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils$GoalListener;

.field private static mRealtimeGoalUtils:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;

.field private static mSecToTarget:I

.field private static mTECheckTimer:Ljava/util/Timer;

.field private static mTECheckTimerTask:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils$TECheckTimerTask;

.field private static mTEEnabled:Z

.field private static mType:I

.field private static mValue:I

.field public static pretime:F


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 21
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mRealtimeGoalUtils:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;

    .line 22
    sput-boolean v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mIsActive:Z

    .line 23
    sput-boolean v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mIsGoalAchievd:Z

    .line 37
    sput v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->lastupdatedistance:I

    .line 38
    sput v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->lastupdatedbdistance:I

    .line 39
    sput v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->db_index:I

    .line 40
    const/16 v0, 0x14

    sput v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->DB_INTERVAL_DISTANCE:I

    .line 43
    sput v2, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->dbavgspeed:F

    .line 44
    sput v2, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->dbdistance:F

    .line 45
    sput v2, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->dbtime:F

    .line 46
    sput v2, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->pretime:F

    .line 47
    sput-boolean v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->BeatMyPrevious_firstcheck:Z

    .line 49
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mGoalRateList:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    return-void
.end method

.method public static declared-synchronized TETimerPause()V
    .locals 3

    .prologue
    .line 603
    const-class v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;

    monitor-enter v1

    :try_start_0
    const-string v0, "Goal"

    const-string v2, "TETimerPause"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 604
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mTECheckTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 605
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mTECheckTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 606
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mTECheckTimer:Ljava/util/Timer;

    .line 607
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mTECheckTimerTask:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils$TECheckTimerTask;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 609
    :cond_0
    monitor-exit v1

    return-void

    .line 603
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized TETimerResume()V
    .locals 7

    .prologue
    .line 612
    const-class v6, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;

    monitor-enter v6

    :try_start_0
    const-string v0, "Goal"

    const-string v1, "TETimerResume"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 613
    sget-boolean v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mTEEnabled:Z

    if-eqz v0, :cond_1

    .line 614
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mTECheckTimer:Ljava/util/Timer;

    if-nez v0, :cond_0

    .line 615
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mTECheckTimer:Ljava/util/Timer;

    .line 617
    :cond_0
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils$TECheckTimerTask;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils$TECheckTimerTask;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils$1;)V

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mTECheckTimerTask:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils$TECheckTimerTask;

    .line 618
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mTECheckTimer:Ljava/util/Timer;

    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mTECheckTimerTask:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils$TECheckTimerTask;

    const-wide/16 v2, 0x1388

    const-wide/16 v4, 0x1388

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 620
    :cond_1
    monitor-exit v6

    return-void

    .line 612
    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0
.end method

.method public static declared-synchronized TETimerStart()V
    .locals 7

    .prologue
    .line 591
    const-class v6, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;

    monitor-enter v6

    :try_start_0
    const-string v0, "Goal"

    const-string v1, "TETimerStart"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 592
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mTECheckTimer:Ljava/util/Timer;

    if-nez v0, :cond_0

    .line 593
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mTECheckTimer:Ljava/util/Timer;

    .line 595
    :cond_0
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils$TECheckTimerTask;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils$TECheckTimerTask;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils$1;)V

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mTECheckTimerTask:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils$TECheckTimerTask;

    .line 596
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mTECheckTimer:Ljava/util/Timer;

    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mTECheckTimerTask:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils$TECheckTimerTask;

    const-wide/16 v2, 0x1388

    const-wide/16 v4, 0x1388

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 597
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mAnalyzeCount:I

    .line 598
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mLastAudioGuideId:I

    .line 599
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mLastAudioGuideParam:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 600
    monitor-exit v6

    return-void

    .line 591
    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0
.end method

.method public static declared-synchronized TETimerStop()V
    .locals 3

    .prologue
    .line 623
    const-class v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;

    monitor-enter v1

    :try_start_0
    const-string v0, "Goal"

    const-string v2, "TETimerStop"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 624
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mTECheckTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 625
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mTECheckTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 626
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mTECheckTimer:Ljava/util/Timer;

    .line 627
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mTECheckTimerTask:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils$TECheckTimerTask;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 629
    :cond_0
    monitor-exit v1

    return-void

    .line 623
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic access$000()I
    .locals 1

    .prologue
    .line 17
    sget v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mCurrentHearbeatTime:I

    return v0
.end method

.method static synthetic access$100()I
    .locals 1

    .prologue
    .line 17
    sget v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mCurrentHR:I

    return v0
.end method

.method static synthetic access$200()F
    .locals 1

    .prologue
    .line 17
    sget v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mCurrentSpeed:F

    return v0
.end method

.method static synthetic access$300()F
    .locals 1

    .prologue
    .line 17
    sget v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mCurrentRealElevation:F

    return v0
.end method

.method static synthetic access$402(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 17
    sput p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mSecToTarget:I

    return p0
.end method

.method static synthetic access$500(I)V
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 17
    invoke-static {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->checkGoal(I)V

    return-void
.end method

.method static synthetic access$602(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 17
    sput p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mAnalyzeCount:I

    return p0
.end method

.method static synthetic access$604()I
    .locals 1

    .prologue
    .line 17
    sget v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mAnalyzeCount:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mAnalyzeCount:I

    return v0
.end method

.method static synthetic access$700()I
    .locals 1

    .prologue
    .line 17
    sget v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mLastAudioGuideId:I

    return v0
.end method

.method static synthetic access$702(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 17
    sput p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mLastAudioGuideId:I

    return p0
.end method

.method static synthetic access$802(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 17
    sput p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mLastAudioGuideParam:I

    return p0
.end method

.method private static checkGoal(I)V
    .locals 6
    .param p0, "value"    # I

    .prologue
    .line 318
    const-string v3, "checkGoal"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "real value : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 319
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->isGoalActive()Z

    move-result v3

    if-nez v3, :cond_1

    .line 361
    :cond_0
    :goto_0
    return-void

    .line 322
    :cond_1
    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mGoalRateList:Ljava/util/ArrayList;

    if-nez v3, :cond_2

    .line 323
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeGoalType()I

    move-result v3

    sput v3, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mType:I

    .line 324
    sget v3, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mType:I

    const/4 v4, 0x5

    if-ne v3, v4, :cond_3

    .line 325
    const/16 v3, 0x64

    sput v3, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mValue:I

    .line 330
    :goto_1
    const/4 v3, 0x0

    invoke-static {p0, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->checkGoalRateList(IZ)V

    .line 332
    const/4 v3, -0x1

    sput v3, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->currentGoalIndex:I

    .line 335
    :cond_2
    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mGoalRateList:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    .line 336
    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mGoalRateList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 337
    .local v0, "goalListSize":I
    if-gtz v0, :cond_4

    .line 338
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->clear()V

    goto :goto_0

    .line 327
    .end local v0    # "goalListSize":I
    :cond_3
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeGoalValue()I

    move-result v3

    sput v3, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mValue:I

    goto :goto_1

    .line 344
    .restart local v0    # "goalListSize":I
    :cond_4
    add-int/lit8 v2, v0, -0x1

    .local v2, "index":I
    :goto_2
    sget v3, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->currentGoalIndex:I

    if-lt v2, v3, :cond_0

    .line 345
    if-ltz v2, :cond_0

    .line 347
    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mGoalRateList:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils$GoalRate;

    .line 348
    .local v1, "goalRate":Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils$GoalRate;
    const-string v3, "checkGoal"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "checkGoal  value : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", targetValue : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils$GoalRate;->targetValue:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 349
    iget v3, v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils$GoalRate;->targetValue:I

    if-lt p0, v3, :cond_5

    .line 350
    const-string v4, "checkGoal"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Goal achieved index["

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "]"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " value["

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "]"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " rate["

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mGoalRateList:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils$GoalRate;

    iget v3, v3, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils$GoalRate;->rate:I

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "]"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 356
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->goalAchieved(Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils$GoalRate;I)V

    goto/16 :goto_0

    .line 344
    :cond_5
    add-int/lit8 v2, v2, -0x1

    goto/16 :goto_2
.end method

.method private static checkGoalRateList(IZ)V
    .locals 9
    .param p0, "value"    # I
    .param p1, "isGeneral"    # Z

    .prologue
    const v8, 0x7f090b95

    const/16 v7, 0x64

    .line 183
    sget-object v2, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mGoalRateList:Ljava/util/ArrayList;

    if-nez v2, :cond_0

    .line 184
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    sput-object v2, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mGoalRateList:Ljava/util/ArrayList;

    .line 187
    :cond_0
    const-string v2, "checkGoal"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "checkGoal  type : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget v4, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mType:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", mValue : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget v4, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mValue:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", current : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    const/4 v0, 0x0

    .line 191
    .local v0, "index":I
    sget v2, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mValue:I

    div-int/lit8 v2, v2, 0x2

    if-lez v2, :cond_1

    .line 192
    sget-object v2, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mGoalRateList:Ljava/util/ArrayList;

    add-int/lit8 v1, v0, 0x1

    .end local v0    # "index":I
    .local v1, "index":I
    new-instance v3, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils$GoalRate;

    const/16 v4, 0x32

    sget v5, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mValue:I

    div-int/lit8 v5, v5, 0x2

    const v6, 0x7f090a81

    invoke-direct {v3, v4, v5, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils$GoalRate;-><init>(III)V

    invoke-virtual {v2, v0, v3}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    move v0, v1

    .line 194
    .end local v1    # "index":I
    .restart local v0    # "index":I
    :cond_1
    sget v2, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mValue:I

    mul-int/lit8 v2, v2, 0x9

    div-int/lit8 v2, v2, 0xa

    if-lez v2, :cond_2

    .line 195
    sget-object v2, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mGoalRateList:Ljava/util/ArrayList;

    add-int/lit8 v1, v0, 0x1

    .end local v0    # "index":I
    .restart local v1    # "index":I
    new-instance v3, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils$GoalRate;

    const/16 v4, 0x5a

    sget v5, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mValue:I

    mul-int/lit8 v5, v5, 0x9

    div-int/lit8 v5, v5, 0xa

    const v6, 0x7f090a82

    invoke-direct {v3, v4, v5, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils$GoalRate;-><init>(III)V

    invoke-virtual {v2, v0, v3}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    move v0, v1

    .line 197
    .end local v1    # "index":I
    .restart local v0    # "index":I
    :cond_2
    sget v2, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mValue:I

    if-lez v2, :cond_3

    .line 198
    if-eqz p1, :cond_4

    .line 199
    sget-object v2, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mGoalRateList:Ljava/util/ArrayList;

    add-int/lit8 v1, v0, 0x1

    .end local v0    # "index":I
    .restart local v1    # "index":I
    new-instance v3, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils$GoalRate;

    sget v4, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mValue:I

    invoke-direct {v3, v7, v4, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils$GoalRate;-><init>(III)V

    invoke-virtual {v2, v0, v3}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    move v0, v1

    .line 208
    .end local v1    # "index":I
    .restart local v0    # "index":I
    :cond_3
    :goto_0
    return-void

    .line 204
    :cond_4
    sget-object v2, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mGoalRateList:Ljava/util/ArrayList;

    add-int/lit8 v1, v0, 0x1

    .end local v0    # "index":I
    .restart local v1    # "index":I
    new-instance v3, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils$GoalRate;

    sget v4, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mValue:I

    invoke-direct {v3, v7, v4, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils$GoalRate;-><init>(III)V

    invoke-virtual {v2, v0, v3}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    move v0, v1

    .end local v1    # "index":I
    .restart local v0    # "index":I
    goto :goto_0
.end method

.method private static checkpreviousrecord(I)V
    .locals 5
    .param p0, "value"    # I

    .prologue
    const/4 v2, 0x1

    .line 364
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->isGoalActive()Z

    move-result v0

    if-nez v0, :cond_0

    .line 388
    :goto_0
    return-void

    .line 368
    :cond_0
    sget v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->lastupdatedbdistance:I

    sget v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->DB_INTERVAL_DISTANCE:I

    add-int/2addr v0, v1

    sget v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mCurrentDistance:I

    if-le v0, v1, :cond_1

    sget v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->db_index:I

    if-nez v0, :cond_2

    .line 369
    :cond_1
    sget v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->db_index:I

    if-nez v0, :cond_3

    .line 370
    sput-boolean v2, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->BeatMyPrevious_firstcheck:Z

    .line 374
    :goto_1
    sget v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->db_index:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->db_index:I

    .line 375
    sget v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mCurrentDistance:I

    sput v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->lastupdatedbdistance:I

    .line 378
    :cond_2
    sget-boolean v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->BeatMyPrevious_firstcheck:Z

    if-ne v0, v2, :cond_5

    .line 379
    sget v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mCurrentDuration:I

    const/4 v1, 0x3

    if-ge v0, v1, :cond_4

    .line 380
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->diffdbduration:F

    goto :goto_0

    .line 372
    :cond_3
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->BeatMyPrevious_firstcheck:Z

    goto :goto_1

    .line 382
    :cond_4
    sget v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mCurrentDuration:I

    int-to-float v0, v0

    sget v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mCurrentDistance:I

    int-to-float v1, v1

    sget v2, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->dbavgspeed:F

    div-float/2addr v1, v2

    sub-float/2addr v0, v1

    sput v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->diffdbduration:F

    goto :goto_0

    .line 386
    :cond_5
    sget v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mCurrentDuration:I

    int-to-float v0, v0

    sget v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->pretime:F

    sget v2, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mCurrentDistance:I

    sget v3, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->DB_INTERVAL_DISTANCE:I

    sget v4, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->db_index:I

    add-int/lit8 v4, v4, -0x1

    mul-int/2addr v3, v4

    sub-int/2addr v2, v3

    int-to-float v2, v2

    sget v3, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->dbavgspeed:F

    div-float/2addr v2, v3

    add-float/2addr v1, v2

    sub-float/2addr v0, v1

    sput v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->diffdbduration:F

    goto :goto_0
.end method

.method public static declared-synchronized clear()V
    .locals 2

    .prologue
    .line 491
    const-class v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mGoalRateList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 492
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mGoalRateList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 493
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mGoalRateList:Ljava/util/ArrayList;

    .line 495
    :cond_0
    const/4 v0, -0x1

    sput v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->currentGoalIndex:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 496
    monitor-exit v1

    return-void

    .line 491
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static getCurrentCalorie()I
    .locals 1

    .prologue
    .line 139
    sget v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mCurrentCalorie:I

    return v0
.end method

.method public static getCurrentDistance()I
    .locals 1

    .prologue
    .line 135
    sget v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mCurrentDistance:I

    return v0
.end method

.method public static getCurrentDuration()I
    .locals 1

    .prologue
    .line 131
    sget v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mCurrentDuration:I

    return v0
.end method

.method public static getDataTypeByGoalType(I)I
    .locals 1
    .param p0, "goalType"    # I

    .prologue
    const/4 v0, 0x1

    .line 499
    packed-switch p0, :pswitch_data_0

    .line 511
    const/4 v0, -0x1

    :goto_0
    :pswitch_0
    return v0

    .line 501
    :pswitch_1
    const/4 v0, 0x4

    goto :goto_0

    .line 503
    :pswitch_2
    const/4 v0, 0x2

    goto :goto_0

    .line 509
    :pswitch_3
    const/16 v0, 0x11

    goto :goto_0

    .line 499
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 78
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mRealtimeGoalUtils:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;

    monitor-enter v1

    .line 80
    :try_start_0
    sput-object p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mContext:Landroid/content/Context;

    .line 81
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 82
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mRealtimeGoalUtils:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;

    return-object v0

    .line 81
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static getRemainSecToGoal(J)J
    .locals 8
    .param p0, "sec"    # J

    .prologue
    const-wide/16 v6, 0x3c

    .line 413
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeGoalType()I

    move-result v2

    .line 414
    .local v2, "goalType":I
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeGoalValue()I

    move-result v3

    .line 416
    .local v3, "goalValue":I
    const/4 v4, 0x5

    if-ne v2, v4, :cond_1

    .line 417
    if-lez v3, :cond_0

    .line 418
    int-to-long v4, v3

    mul-long v0, v4, v6

    .line 419
    .local v0, "goal":J
    cmp-long v4, v0, p0

    if-lez v4, :cond_2

    .line 420
    sub-long v4, v0, p0

    .line 433
    .end local v0    # "goal":J
    :goto_0
    return-wide v4

    .line 423
    :cond_0
    sget v4, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mSecToTarget:I

    int-to-long v4, v4

    goto :goto_0

    .line 426
    :cond_1
    const/4 v4, 0x3

    if-ne v2, v4, :cond_2

    .line 427
    int-to-long v4, v3

    mul-long v0, v4, v6

    .line 428
    .restart local v0    # "goal":J
    cmp-long v4, v0, p0

    if-lez v4, :cond_2

    .line 429
    sub-long v4, v0, p0

    goto :goto_0

    .line 433
    .end local v0    # "goal":J
    :cond_2
    const-wide/16 v4, 0x0

    goto :goto_0
.end method

.method public static getRemainValueToGoal(IF)F
    .locals 7
    .param p0, "type"    # I
    .param p1, "value"    # F

    .prologue
    const/4 v5, 0x0

    .line 391
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeGoalType()I

    move-result v2

    .line 392
    .local v2, "goalType":I
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeActivityType()I

    move-result v0

    .line 393
    .local v0, "activityType":I
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6, v0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getActivityTypeGoalValue(Landroid/content/Context;II)I

    move-result v3

    .line 394
    .local v3, "goalValue":I
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->getDataTypeByGoalType(I)I

    move-result v6

    if-eq v6, p0, :cond_1

    .line 408
    :cond_0
    :goto_0
    return v5

    .line 397
    :cond_1
    int-to-float v1, v3

    .line 398
    .local v1, "goal":F
    const/4 v6, 0x2

    if-ne p0, v6, :cond_2

    .line 399
    cmpl-float v6, v1, p1

    if-lez v6, :cond_0

    .line 400
    const/high16 v5, 0x41200000    # 10.0f

    div-float v5, p1, v5

    float-to-int v5, v5

    mul-int/lit8 v4, v5, 0xa

    .line 401
    .local v4, "ivalue":I
    int-to-float v5, v4

    sub-float v5, v1, v5

    goto :goto_0

    .line 403
    .end local v4    # "ivalue":I
    :cond_2
    const/4 v6, 0x4

    if-ne p0, v6, :cond_0

    .line 404
    float-to-int v6, p1

    int-to-float v6, v6

    cmpl-float v6, v1, v6

    if-lez v6, :cond_0

    .line 405
    sub-float v5, v1, p1

    goto :goto_0
.end method

.method public static getRoundedValue(F)F
    .locals 1
    .param p0, "value"    # F

    .prologue
    .line 632
    float-to-int v0, p0

    int-to-float v0, v0

    return v0
.end method

.method public static getType()I
    .locals 1

    .prologue
    .line 102
    sget v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mType:I

    return v0
.end method

.method public static getValue()I
    .locals 1

    .prologue
    .line 110
    sget v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mValue:I

    return v0
.end method

.method private static goalAchieved(Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils$GoalRate;I)V
    .locals 6
    .param p0, "goalRate"    # Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils$GoalRate;
    .param p1, "index"    # I

    .prologue
    const/16 v3, 0x32

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 291
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;

    .line 292
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils$GoalRate;->rate:I

    if-ne v1, v3, :cond_1

    .line 293
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f090a81

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->play(Ljava/lang/String;)Z

    .line 306
    :goto_0
    add-int/lit8 v1, p1, 0x1

    sput v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->currentGoalIndex:I

    .line 307
    sget v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->currentGoalIndex:I

    sget-object v2, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mGoalRateList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v1, v2, :cond_0

    .line 308
    invoke-static {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->setGoalActive(Z)V

    .line 310
    :cond_0
    return-void

    .line 294
    :cond_1
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils$GoalRate;->rate:I

    const/16 v2, 0x64

    if-ne v1, v2, :cond_3

    .line 295
    sget v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mType:I

    const/4 v2, 0x5

    if-ne v1, v2, :cond_2

    .line 296
    sget v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mType:I

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeTrainingLevel()I

    move-result v2

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->makeTargetTEValue(II)I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x41200000    # 10.0f

    div-float v0, v1, v2

    .line 297
    .local v0, "te":F
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils$GoalRate;->stringId:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->play(Ljava/lang/String;)Z

    .line 301
    .end local v0    # "te":F
    :goto_1
    sput-boolean v5, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mIsGoalAchievd:Z

    goto :goto_0

    .line 299
    :cond_2
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils$GoalRate;->stringId:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->play(Ljava/lang/String;)Z

    goto :goto_1

    .line 303
    :cond_3
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils$GoalRate;->stringId:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->play(Ljava/lang/String;)Z

    goto :goto_0
.end method

.method public static goalEnd(JD)V
    .locals 1
    .param p0, "mExerciseId"    # J
    .param p2, "distance"    # D

    .prologue
    const/4 v0, 0x0

    .line 122
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->TETimerStop()V

    .line 123
    sput-boolean v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mTEEnabled:Z

    .line 124
    invoke-static {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->saveValuesToPersist(JD)V

    .line 125
    sput v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mCurrentCalorie:I

    sput v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mCurrentDuration:I

    sput v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mCurrentDistance:I

    .line 126
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->setGoalActive(Z)V

    .line 127
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->clear()V

    .line 128
    return-void
.end method

.method public static goalPause()V
    .locals 0

    .prologue
    .line 114
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->TETimerPause()V

    .line 115
    return-void
.end method

.method public static goalResume()V
    .locals 0

    .prologue
    .line 118
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->TETimerResume()V

    .line 119
    return-void
.end method

.method public static isGoalAchieved()Z
    .locals 1

    .prologue
    .line 635
    sget-boolean v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mIsGoalAchievd:Z

    return v0
.end method

.method private static isGoalActive()Z
    .locals 1

    .prologue
    .line 143
    sget-boolean v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mIsActive:Z

    return v0
.end method

.method public static registerListener(Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils$GoalListener;)V
    .locals 0
    .param p0, "listener"    # Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils$GoalListener;

    .prologue
    .line 90
    sput-object p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mListener:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils$GoalListener;

    .line 91
    return-void
.end method

.method private static declared-synchronized setGeneralGoal(IIZ)V
    .locals 5
    .param p0, "type"    # I
    .param p1, "value"    # I
    .param p2, "startWorkout"    # Z

    .prologue
    const/4 v4, 0x3

    .line 152
    const-class v2, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;

    monitor-enter v2

    :try_start_0
    sget v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mType:I

    if-ne v1, v4, :cond_3

    .line 153
    mul-int/lit8 v1, p1, 0x3c

    sput v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mValue:I

    .line 157
    :goto_0
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mGoalRateList:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 158
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->clear()V

    .line 161
    :cond_0
    const/4 v1, 0x1

    invoke-static {p1, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->checkGoalRateList(IZ)V

    .line 163
    const/4 v1, 0x0

    sput v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->currentGoalIndex:I

    .line 165
    if-nez p2, :cond_2

    .line 166
    const/4 v0, 0x0

    .line 168
    .local v0, "currentValue":I
    sget v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mType:I

    const/4 v3, 0x2

    if-ne v1, v3, :cond_4

    .line 169
    sget v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mCurrentDistance:I

    .line 175
    :cond_1
    :goto_1
    const-string v1, "Goal"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Goal Changed in active satate currentValue["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 178
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->checkGoal(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 180
    .end local v0    # "currentValue":I
    :cond_2
    monitor-exit v2

    return-void

    .line 155
    :cond_3
    :try_start_1
    sput p1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mValue:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 152
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1

    .line 170
    .restart local v0    # "currentValue":I
    :cond_4
    :try_start_2
    sget v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mType:I

    if-ne v1, v4, :cond_5

    .line 171
    sget v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mCurrentDuration:I

    goto :goto_1

    .line 172
    :cond_5
    sget v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mType:I

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    .line 173
    sget v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mCurrentCalorie:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public static setGoal()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 211
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeGoalType()I

    move-result v1

    .line 212
    .local v1, "type":I
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeGoalValue()I

    move-result v2

    .line 213
    .local v2, "value":I
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeTrainingLevel()I

    move-result v0

    .line 215
    .local v0, "teLevel":I
    invoke-static {v1, v2, v0, v3, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->setGoal(IIIZZ)V

    .line 216
    return-void
.end method

.method public static setGoal(III)V
    .locals 4
    .param p0, "type"    # I
    .param p1, "value"    # I
    .param p2, "teLevel"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 219
    sput p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mType:I

    .line 220
    sget v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mType:I

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    sget v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mType:I

    if-eq v0, v3, :cond_0

    .line 221
    invoke-static {p0, p1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->setGeneralGoal(IIZ)V

    .line 223
    :cond_0
    sput-boolean v2, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mTEEnabled:Z

    .line 225
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->isGoalActive()Z

    move-result v0

    if-eqz v0, :cond_1

    sget v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mType:I

    if-ne v0, v3, :cond_1

    .line 226
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->setGoalActive(Z)V

    .line 230
    :goto_0
    return-void

    .line 228
    :cond_1
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->setGoalActive(Z)V

    goto :goto_0
.end method

.method public static setGoal(IIIZZ)V
    .locals 8
    .param p0, "type"    # I
    .param p1, "value"    # I
    .param p2, "teLevel"    # I
    .param p3, "hrmPaired"    # Z
    .param p4, "startWorkout"    # Z

    .prologue
    const/4 v7, 0x5

    const/4 v6, -0x1

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 235
    sput p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mType:I

    .line 238
    const-string v1, "Goal"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "setGoal isActive["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->isGoalActive()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " type["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " value["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " teLevel["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " hrmPaired["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " startWorkout["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 247
    sget v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mType:I

    if-eq v1, v7, :cond_0

    sget v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mType:I

    if-eq v1, v4, :cond_0

    .line 248
    invoke-static {p0, p1, p4}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->setGeneralGoal(IIZ)V

    .line 251
    :cond_0
    sput-boolean v5, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mTEEnabled:Z

    .line 252
    if-eqz p4, :cond_6

    .line 253
    if-eqz p3, :cond_3

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isSupportHRM()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeActivityType()I

    move-result v1

    const/16 v2, 0x4653

    if-ne v1, v2, :cond_3

    sget v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mType:I

    if-ne v1, v7, :cond_3

    .line 254
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;

    .line 255
    if-gtz p1, :cond_1

    .line 256
    const/4 p1, 0x0

    .line 258
    :cond_1
    sget v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mType:I

    invoke-static {v1, p2}, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->makeTargetTEValue(II)I

    move-result v1

    sget v2, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mType:I

    invoke-static {v2, p1}, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->makeTargetTime(II)I

    move-result v2

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->initEngine(II)I

    .line 259
    sput v6, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mSecToTarget:I

    .line 260
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->TETimerStart()V

    .line 261
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mListener:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils$GoalListener;

    if-eqz v1, :cond_2

    .line 262
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mListener:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils$GoalListener;

    invoke-interface {v1, v4, v6, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils$GoalListener;->onGoalEvnet(III)V

    .line 264
    :cond_2
    sput-boolean v4, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mTEEnabled:Z

    .line 266
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getFirstbeatAscrActivated()Z

    move-result v1

    if-nez v1, :cond_5

    .line 267
    const-string v1, "Goal"

    const-string v2, "SharedPreferencesHelper.setFirstbeatAscrActivated() is false"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread;

    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread;-><init>(Landroid/content/Context;)V

    .line 269
    .local v0, "thread":Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread;->start()V

    .line 274
    .end local v0    # "thread":Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread;
    :cond_3
    :goto_0
    sput v5, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mCurrentCalorie:I

    sput v5, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mCurrentDuration:I

    sput v5, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mCurrentDistance:I

    .line 275
    sput v5, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mCurrentHearbeatTime:I

    sput v5, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mCurrentHR:I

    .line 276
    const/4 v1, 0x0

    sput v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mCurrentRealElevation:F

    sput v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mCurrentSpeed:F

    .line 282
    :cond_4
    :goto_1
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->isGoalActive()Z

    move-result v1

    if-eqz v1, :cond_7

    sget v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mType:I

    if-ne v1, v4, :cond_7

    .line 283
    invoke-static {v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->setGoalActive(Z)V

    .line 287
    :goto_2
    return-void

    .line 271
    :cond_5
    const-string v1, "Goal"

    const-string v2, "SharedPreferencesHelper.setFirstbeatAscrActivated() is true"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 277
    :cond_6
    sget v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mType:I

    if-eq v1, v4, :cond_4

    .line 278
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;

    .line 279
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f090a86

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->play(Ljava/lang/String;)Z

    goto :goto_1

    .line 285
    :cond_7
    invoke-static {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->setGoalActive(Z)V

    goto :goto_2
.end method

.method private static setGoalActive(Z)V
    .locals 0
    .param p0, "isActive"    # Z

    .prologue
    .line 147
    sput-boolean p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mIsActive:Z

    .line 148
    return-void
.end method

.method public static setGoalChieved(Z)V
    .locals 0
    .param p0, "isGoalAhieved"    # Z

    .prologue
    .line 638
    sput-boolean p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mIsGoalAchievd:Z

    .line 639
    return-void
.end method

.method public static setType(I)V
    .locals 0
    .param p0, "type"    # I

    .prologue
    .line 98
    sput p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mType:I

    .line 99
    return-void
.end method

.method public static setValue(I)V
    .locals 0
    .param p0, "value"    # I

    .prologue
    .line 106
    sput p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mValue:I

    .line 107
    return-void
.end method

.method public static unregisterListener()V
    .locals 1

    .prologue
    .line 94
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mListener:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils$GoalListener;

    .line 95
    return-void
.end method

.method public static workoutProgress(IF)V
    .locals 4
    .param p0, "dataType"    # I
    .param p1, "value"    # F

    .prologue
    .line 437
    float-to-int v0, p1

    .line 438
    .local v0, "intValue":I
    packed-switch p0, :pswitch_data_0

    .line 488
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 440
    :pswitch_1
    sput v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mCurrentCalorie:I

    .line 441
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->getType()I

    move-result v2

    const/4 v3, 0x4

    if-ne v2, v3, :cond_0

    .line 442
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->checkGoal(I)V

    goto :goto_0

    .line 446
    :pswitch_2
    sput v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mCurrentDistance:I

    .line 447
    sget v2, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->lastupdatedistance:I

    sget v3, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->DB_INTERVAL_DISTANCE:I

    add-int/2addr v2, v3

    sget v3, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mCurrentDistance:I

    if-gt v2, v3, :cond_1

    .line 448
    sget v2, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mCurrentDistance:I

    sput v2, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->lastupdatedistance:I

    .line 450
    :cond_1
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->getType()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    .line 451
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->checkGoal(I)V

    .line 453
    :cond_2
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->getType()I

    move-result v2

    const/4 v3, 0x6

    if-ne v2, v3, :cond_0

    .line 454
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->checkpreviousrecord(I)V

    goto :goto_0

    .line 458
    :pswitch_3
    sput v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mCurrentDuration:I

    .line 459
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->getType()I

    move-result v2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_0

    .line 460
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->checkGoal(I)V

    goto :goto_0

    .line 464
    :pswitch_4
    sput v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mCurrentHR:I

    goto :goto_0

    .line 467
    :pswitch_5
    sget v2, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mCurrentHearbeatTime:I

    int-to-float v2, v2

    cmpl-float v2, p1, v2

    if-lez v2, :cond_3

    .line 468
    sget v2, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mCurrentHearbeatTime:I

    if-eqz v2, :cond_3

    .line 469
    sget v2, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mCurrentHearbeatTime:I

    sub-int v1, v0, v2

    .line 470
    .local v1, "rri":I
    sget v2, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mCurrentSpeed:F

    sget v3, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mCurrentRealElevation:F

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->inputRriTrainingData(IFF)I

    .line 473
    .end local v1    # "rri":I
    :cond_3
    sput v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mCurrentHearbeatTime:I

    goto :goto_0

    .line 480
    :pswitch_6
    sput p1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mCurrentRealElevation:F

    goto :goto_0

    .line 483
    :pswitch_7
    sput p1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mCurrentSpeed:F

    goto :goto_0

    .line 438
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_7
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
    .end packed-switch
.end method
