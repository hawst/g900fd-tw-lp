.class public Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;
.super Ljava/lang/Object;
.source "AssessmentQAData.java"


# static fields
.field public static final NO_DATA:I = -0x80000000


# instance fields
.field private mAnswer:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/Answer;",
            ">;"
        }
    .end annotation
.end field

.field private mAnswerIndex:I

.field private mAnswerValue:Ljava/lang/String;

.field private mQuestionGroupID:I

.field private mQuestionID:I

.field private mQuestionImageName:Ljava/lang/String;

.field private mQuestionString:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(ILcom/cigna/coach/apiobjects/QuestionAnswers;)V
    .locals 2
    .param p1, "questionGroupId"    # I
    .param p2, "questionAnswers"    # Lcom/cigna/coach/apiobjects/QuestionAnswers;

    .prologue
    const/4 v1, 0x0

    const/high16 v0, -0x80000000

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;->mQuestionGroupID:I

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;->mQuestionID:I

    .line 16
    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;->mAnswerIndex:I

    .line 17
    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;->mAnswerValue:Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;->mQuestionImageName:Ljava/lang/String;

    .line 20
    iput p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;->mQuestionGroupID:I

    .line 21
    invoke-virtual {p2}, Lcom/cigna/coach/apiobjects/QuestionAnswers;->getQuestionId()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;->mQuestionID:I

    .line 22
    invoke-virtual {p2}, Lcom/cigna/coach/apiobjects/QuestionAnswers;->getQuestion()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;->mQuestionString:Ljava/lang/CharSequence;

    .line 23
    invoke-virtual {p2}, Lcom/cigna/coach/apiobjects/QuestionAnswers;->getAnswerList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;->mAnswer:Ljava/util/List;

    .line 24
    invoke-virtual {p2}, Lcom/cigna/coach/apiobjects/QuestionAnswers;->getQuestionImage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;->mQuestionImageName:Ljava/lang/String;

    .line 25
    return-void
.end method


# virtual methods
.method public getAnswerList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/Answer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;->mAnswer:Ljava/util/List;

    return-object v0
.end method

.method public getAnswerString(I)Ljava/lang/String;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;->mAnswer:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/apiobjects/Answer;

    invoke-virtual {v0}, Lcom/cigna/coach/apiobjects/Answer;->getAnswer()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAnswerType()Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;
    .locals 2

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;->mAnswer:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/apiobjects/Answer;

    invoke-virtual {v0}, Lcom/cigna/coach/apiobjects/Answer;->getAnswerType()Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;

    move-result-object v0

    return-object v0
.end method

.method public getAnsweredID()I
    .locals 3

    .prologue
    .line 68
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;->mAnswer:Ljava/util/List;

    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;->mAnswerIndex:I

    const/high16 v2, -0x80000000

    if-ne v0, v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/apiobjects/Answer;

    invoke-virtual {v0}, Lcom/cigna/coach/apiobjects/Answer;->getAnswerId()I

    move-result v0

    return v0

    :cond_0
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;->mAnswerIndex:I

    goto :goto_0
.end method

.method public getAnsweredValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;->mAnswerValue:Ljava/lang/String;

    return-object v0
.end method

.method public getQuestionGroupID()I
    .locals 1

    .prologue
    .line 32
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;->mQuestionGroupID:I

    return v0
.end method

.method public getQuestionID()I
    .locals 1

    .prologue
    .line 36
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;->mQuestionID:I

    return v0
.end method

.method public getQuestionImageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;->mQuestionImageName:Ljava/lang/String;

    return-object v0
.end method

.method public getQuestionString()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;->mQuestionString:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public setAnsweredIndex(I)V
    .locals 0
    .param p1, "index"    # I

    .prologue
    .line 56
    iput p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;->mAnswerIndex:I

    .line 57
    return-void
.end method

.method public setAnsweredValue(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;->mAnswerValue:Ljava/lang/String;

    .line 61
    return-void
.end method
