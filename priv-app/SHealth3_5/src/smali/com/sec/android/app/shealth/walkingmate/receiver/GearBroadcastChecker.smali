.class public Lcom/sec/android/app/shealth/walkingmate/receiver/GearBroadcastChecker;
.super Ljava/lang/Object;
.source "GearBroadcastChecker.java"


# static fields
.field private static final CONNECT_DEVICE_ACTIVITY_TRACKER:Ljava/lang/String; = "com.samsung.android.shealth.SBAND.SERVICE_STATUS"

.field private static final CONNECT_DEVICE_GEAR1_ANDROID:Ljava/lang/String; = "com.samsung.android.shealth.GEAR1_ANDROID_SERVICE_STATUS"

.field private static final CONNECT_DEVICE_GEAR2_OR_GEAR1_TIZEN:Ljava/lang/String; = "com.samsung.android.shealth.GEAR2.SERVICE_STATUS"

.field private static final CONNECT_DEVICE_GEAR3:Ljava/lang/String; = "com.samsung.android.shealth.GEAR_SERVICE_STATUS"

.field private static final CONNECT_DEVICE_GEAR_FIT:Ljava/lang/String; = "com.samsung.android.shealth.SERVICE_STATUS"

.field private static final GEAR3_NAME_3:Ljava/lang/String; = "Gear 3"

.field private static final GEAR3_NAME_S:Ljava/lang/String; = "Gear S"

.field private static final GEARO_NAME:Ljava/lang/String; = "Gear O"

.field private static MAIN_DB_PEDOMETER_VIEW_STEP_COUNT_WEARABLE:I = 0x0

.field private static final NOT_ASSIGNED:I = -0x1

.field private static final TAG:Ljava/lang/String; = "GearBroadcastChecker"

.field private static mViewStepChanged:Landroid/content/ServiceConnection;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x1

    sput v0, Lcom/sec/android/app/shealth/walkingmate/receiver/GearBroadcastChecker;->MAIN_DB_PEDOMETER_VIEW_STEP_COUNT_WEARABLE:I

    .line 140
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/receiver/GearBroadcastChecker$1;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/walkingmate/receiver/GearBroadcastChecker$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/receiver/GearBroadcastChecker;->mViewStepChanged:Landroid/content/ServiceConnection;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static Checker(Landroid/content/Intent;)V
    .locals 10
    .param p0, "intent"    # Landroid/content/Intent;

    .prologue
    const/16 v7, 0x2728

    const/16 v9, 0x2719

    const/4 v8, 0x1

    .line 55
    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 56
    .local v0, "action":Ljava/lang/String;
    const-string v4, "GearBroadcastChecker"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "action = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    const-string v4, "com.samsung.android.shealth.GEAR1_ANDROID_SERVICE_STATUS"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "com.samsung.android.shealth.SERVICE_STATUS"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "com.samsung.android.shealth.GEAR2.SERVICE_STATUS"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "com.samsung.android.shealth.GEAR_SERVICE_STATUS"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "com.samsung.android.shealth.SBAND.SERVICE_STATUS"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 63
    :cond_0
    const-string v4, "GearBroadcastChecker"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[**] Wearable Device Connect action = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    const/4 v3, -0x1

    .line 66
    .local v3, "viewStepCountFromIntent":I
    const-string v4, "com.samsung.android.shealth.GEAR1_ANDROID_SERVICE_STATUS"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 68
    const/16 v3, 0x2724

    .line 69
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->saveGearConnectedTime(J)V

    .line 105
    :cond_1
    :goto_0
    invoke-static {v3}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->setLastConnectedDeviceType(I)V

    .line 107
    const/4 v4, -0x1

    if-eq v3, v4, :cond_d

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->allowDeviceConnectionSwitch(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 109
    const-string v4, "GearBroadcastChecker"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[FIRSTCONNECTION] "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " is first connected"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    invoke-static {v3}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->saveViewStepCount(I)V

    .line 112
    if-eq v3, v9, :cond_2

    .line 113
    sget v4, Lcom/sec/android/app/shealth/walkingmate/receiver/GearBroadcastChecker;->MAIN_DB_PEDOMETER_VIEW_STEP_COUNT_WEARABLE:I

    invoke-static {v4}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->saveViewStepCountToMainSettingDB(I)V

    .line 116
    :cond_2
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->isRun()Z

    move-result v4

    if-eqz v4, :cond_c

    .line 117
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    new-instance v5, Landroid/content/Intent;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    const-class v7, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    invoke-direct {v5, v6, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    sget-object v6, Lcom/sec/android/app/shealth/walkingmate/receiver/GearBroadcastChecker;->mViewStepChanged:Landroid/content/ServiceConnection;

    invoke-virtual {v4, v5, v6, v8}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 128
    :goto_1
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v4

    if-ne v4, v9, :cond_e

    .line 129
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    move-result-object v4

    invoke-virtual {v4, v8}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->setInactiveMonitor(I)V

    .line 130
    invoke-static {v8}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->setNotificationONOFF(Z)V

    .line 138
    .end local v3    # "viewStepCountFromIntent":I
    :cond_3
    :goto_2
    return-void

    .line 71
    .restart local v3    # "viewStepCountFromIntent":I
    :cond_4
    const-string v4, "com.samsung.android.shealth.SERVICE_STATUS"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 73
    const/16 v3, 0x2723

    goto :goto_0

    .line 75
    :cond_5
    const-string v4, "com.samsung.android.shealth.GEAR2.SERVICE_STATUS"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 77
    const-string v4, "EXTRA_DEVICETYPE"

    invoke-virtual {p0, v4, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 79
    .local v2, "getDevice":I
    if-ne v2, v7, :cond_6

    .line 81
    const/16 v3, 0x2728

    .line 82
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->saveTizenGearConnectedTime(J)V

    goto/16 :goto_0

    .line 84
    :cond_6
    const/16 v4, 0x2726

    if-ne v2, v4, :cond_1

    .line 86
    const/16 v3, 0x2726

    goto/16 :goto_0

    .line 88
    .end local v2    # "getDevice":I
    :cond_7
    const-string v4, "com.samsung.android.shealth.GEAR_SERVICE_STATUS"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 90
    const-string v4, "DEVICE"

    invoke-virtual {p0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 92
    .local v1, "device":Ljava/lang/String;
    const-string v4, "Gear 3"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_8

    const-string v4, "Gear S"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 93
    :cond_8
    const/16 v3, 0x272e

    goto/16 :goto_0

    .line 95
    :cond_9
    const-string v4, "Gear O"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 96
    const/16 v3, 0x2730

    goto/16 :goto_0

    .line 99
    :cond_a
    const-string v4, "GearBroadcastChecker"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "CONNECT_DEVICE_GEAR3 device name:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 101
    .end local v1    # "device":Ljava/lang/String;
    :cond_b
    const-string v4, "com.samsung.android.shealth.SBAND.SERVICE_STATUS"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 103
    const/16 v3, 0x2727

    goto/16 :goto_0

    .line 120
    :cond_c
    const-string v4, "GearBroadcastChecker"

    const-string v5, "[FIRSTCONNECTION] WalkingMateDayStepService is not run. error case"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 124
    :cond_d
    const-string v4, "GearBroadcastChecker"

    const-string v5, "[FIRSTCONNECTION] viewStepCountFromIntent is not assigned"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 132
    :cond_e
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->unsetInactiveMonitor()V

    .line 133
    const/4 v4, 0x0

    invoke-static {v4}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->setNotificationONOFF(Z)V

    goto/16 :goto_2
.end method

.method static synthetic access$000()Landroid/content/ServiceConnection;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/receiver/GearBroadcastChecker;->mViewStepChanged:Landroid/content/ServiceConnection;

    return-object v0
.end method
