.class public interface abstract Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;
.super Ljava/lang/Object;
.source "WeightDao.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime;
.implements Lcom/sec/android/app/shealth/common/commondao/daointerfaces/GoalContainable;
.implements Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperationsWithFilter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/weight/weightdao/WeightDao$WeightContract;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime",
        "<",
        "Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;",
        ">;",
        "Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperationsWithFilter",
        "<",
        "Lcom/sec/android/app/shealth/weight/weightdao/WeightDao$WeightContract;",
        ">;",
        "Lcom/sec/android/app/shealth/common/commondao/daointerfaces/GoalContainable;"
    }
.end annotation


# virtual methods
.method public abstract deleteAllGoals()I
.end method

.method public abstract getLastData(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;
.end method

.method public abstract getLastUpdatedUserDeviceForWeight()Lcom/sec/android/app/shealth/common/utils/hcp/data/UserDeviceData;
.end method

.method public abstract getSecondLastData(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;
.end method
