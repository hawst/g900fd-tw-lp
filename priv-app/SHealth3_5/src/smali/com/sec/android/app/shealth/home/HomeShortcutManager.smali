.class public Lcom/sec/android/app/shealth/home/HomeShortcutManager;
.super Ljava/lang/Object;
.source "HomeShortcutManager.java"


# static fields
.field private static final ADD_FAV_REQUEST:I = 0x65

.field private static final EDIT_FAV_REQUEST:I = 0xc9

.field private static final TAG:Ljava/lang/String; = "HomeFavorites"


# instance fields
.field private addiconTouchListener:Landroid/view/View$OnTouchListener;

.field private empty_favorites:Landroid/widget/LinearLayout;

.field private iconTouchListener:Landroid/view/View$OnTouchListener;

.field iconclick:Landroid/view/View$OnClickListener;

.field private mActivity:Lcom/sec/android/app/shealth/home/HomeActivity;

.field private mIconList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/home/data/IconData;",
            ">;"
        }
    .end annotation
.end field

.field private mOrderedFavlist:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;",
            ">;"
        }
    .end annotation
.end field

.field private parentView:Landroid/view/View;

.field private scroller:Landroid/widget/HorizontalScrollView;

.field private shortcut_container:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/home/HomeActivity;)V
    .locals 1
    .param p1, "activity"    # Lcom/sec/android/app/shealth/home/HomeActivity;

    .prologue
    .line 215
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->mOrderedFavlist:Ljava/util/ArrayList;

    .line 54
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->mIconList:Ljava/util/ArrayList;

    .line 61
    new-instance v0, Lcom/sec/android/app/shealth/home/HomeShortcutManager$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/home/HomeShortcutManager$1;-><init>(Lcom/sec/android/app/shealth/home/HomeShortcutManager;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->iconclick:Landroid/view/View$OnClickListener;

    .line 136
    new-instance v0, Lcom/sec/android/app/shealth/home/HomeShortcutManager$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/home/HomeShortcutManager$2;-><init>(Lcom/sec/android/app/shealth/home/HomeShortcutManager;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->iconTouchListener:Landroid/view/View$OnTouchListener;

    .line 181
    new-instance v0, Lcom/sec/android/app/shealth/home/HomeShortcutManager$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/home/HomeShortcutManager$3;-><init>(Lcom/sec/android/app/shealth/home/HomeShortcutManager;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->addiconTouchListener:Landroid/view/View$OnTouchListener;

    .line 216
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->mActivity:Lcom/sec/android/app/shealth/home/HomeActivity;

    .line 217
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/home/HomeShortcutManager;)Lcom/sec/android/app/shealth/home/HomeActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/HomeShortcutManager;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->mActivity:Lcom/sec/android/app/shealth/home/HomeActivity;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/home/HomeShortcutManager;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/HomeShortcutManager;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->checkActionForLogging(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/home/HomeShortcutManager;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/HomeShortcutManager;
    .param p1, "x1"    # Landroid/graphics/Bitmap;

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->creatIconWithOutLine(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method private addIconsToContainer()V
    .locals 3

    .prologue
    .line 350
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->mIconList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 352
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->shortcut_container:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->mIconList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/home/data/IconData;

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->getView(Lcom/sec/android/app/shealth/home/data/IconData;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v2, v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 350
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 355
    :cond_0
    return-void
.end method

.method private checkActionForLogging(Ljava/lang/String;)V
    .locals 3
    .param p1, "action"    # Ljava/lang/String;

    .prologue
    .line 99
    if-nez p1, :cond_1

    .line 134
    :cond_0
    :goto_0
    return-void

    .line 102
    :cond_1
    const-string v0, "com.sec.shealth.action.PEDOMETER"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 103
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth"

    const-string v2, "HS01"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 104
    :cond_2
    const-string v0, "com.sec.shealth.action.EXERCISE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 105
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth"

    const-string v2, "HS02"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 106
    :cond_3
    const-string v0, "com.sec.shealth.action.HEART_RATE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 107
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth"

    const-string v2, "HS03"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 108
    :cond_4
    const-string v0, "com.sec.shealth.action.FOOD"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 109
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth"

    const-string v2, "HS04"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 110
    :cond_5
    const-string v0, "com.sec.shealth.action.WEIGHT"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 111
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth"

    const-string v2, "HS05"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 112
    :cond_6
    const-string v0, "com.sec.shealth.action.SLEEP"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 113
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth"

    const-string v2, "HS06"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 114
    :cond_7
    const-string v0, "com.sec.shealth.action.STRESS"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 115
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth"

    const-string v2, "HS07"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 116
    :cond_8
    const-string v0, "com.sec.shealth.action.COACH"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 117
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth"

    const-string v2, "HS08"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 118
    :cond_9
    const-string v0, "android.shealth.action.MOREAPPS"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 119
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth"

    const-string v2, "HS10"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 120
    :cond_a
    const-string v0, "com.sec.shealth.action.UV"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 121
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth"

    const-string v2, "HS11"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 122
    :cond_b
    const-string v0, "com.sec.shealth.action.SPO2"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 123
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth"

    const-string v2, "HS12"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 124
    :cond_c
    const-string v0, "com.sec.shealth.action.BLOOD_GLUCOSE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 125
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth"

    const-string v2, "HS13"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 126
    :cond_d
    const-string v0, "com.sec.shealth.action.ECG"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 127
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth"

    const-string v2, "HS14"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 128
    :cond_e
    const-string v0, "com.sec.shealth.action.BODYTEMP"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 129
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth"

    const-string v2, "HS15"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 130
    :cond_f
    const-string v0, "com.sec.shealth.action.bloodpressure.BLOODPRESSURE_MAIN_ACTIVITY"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 131
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth"

    const-string v2, "HS16"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 132
    :cond_10
    const-string v0, "com.sec.shealth.action.thermohygrometer.THERMOHYGROMETER_MAIN_ACTIVITY"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 133
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth"

    const-string v2, "HS17"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private checkToShowCigna()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 277
    iget-object v4, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->mActivity:Lcom/sec/android/app/shealth/home/HomeActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/home/HomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090029

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->mActivity:Lcom/sec/android/app/shealth/home/HomeActivity;

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryDbManagerWithProvider;->getPluginRegistryDataFromName(Ljava/lang/String;Landroid/content/Context;)Ljava/util/List;

    move-result-object v1

    .line 278
    .local v1, "pluginlist":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;>;"
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_0

    .line 280
    invoke-interface {v1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    .line 281
    .local v0, "appData":Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->isUseCigna()Z

    move-result v4

    if-nez v4, :cond_0

    .line 283
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    iget-object v5, v0, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->packagename:Ljava/lang/String;

    iget v6, v0, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->pluginId:I

    invoke-static {v4, v5, v7, v6}, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryDbManagerWithProvider;->updateAppFavoriteStatus(Landroid/content/Context;Ljava/lang/String;II)I

    .line 284
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget v5, v0, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->pluginId:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->packagename:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 286
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget v5, v0, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->pluginId:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->packagename:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->getOrder(Ljava/lang/String;)I

    move-result v2

    .line 287
    .local v2, "pos":I
    invoke-static {}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->getTotalFavoriteCount()I

    move-result v3

    .line 288
    .local v3, "total":I
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget v5, v0, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->pluginId:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->packagename:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->remove(Ljava/lang/String;)V

    .line 289
    invoke-static {v2}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->updateOrderOnDelete(I)V

    .line 290
    add-int/lit8 v4, v3, -0x1

    invoke-static {v4}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->setTotalFavoriteCount(I)V

    .line 296
    .end local v0    # "appData":Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;
    .end local v2    # "pos":I
    .end local v3    # "total":I
    :cond_0
    return-void
.end method

.method private creatIconWithOutLine(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 20
    .param p1, "srcIcon"    # Landroid/graphics/Bitmap;

    .prologue
    .line 310
    new-instance v11, Landroid/graphics/BlurMaskFilter;

    const/high16 v17, 0x40c00000    # 6.0f

    sget-object v18, Landroid/graphics/BlurMaskFilter$Blur;->NORMAL:Landroid/graphics/BlurMaskFilter$Blur;

    move/from16 v0, v17

    move-object/from16 v1, v18

    invoke-direct {v11, v0, v1}, Landroid/graphics/BlurMaskFilter;-><init>(FLandroid/graphics/BlurMaskFilter$Blur;)V

    .line 311
    .local v11, "innerBlurMaskFilter":Landroid/graphics/BlurMaskFilter;
    new-instance v15, Landroid/graphics/BlurMaskFilter;

    const/high16 v17, 0x41400000    # 12.0f

    sget-object v18, Landroid/graphics/BlurMaskFilter$Blur;->OUTER:Landroid/graphics/BlurMaskFilter$Blur;

    move/from16 v0, v17

    move-object/from16 v1, v18

    invoke-direct {v15, v0, v1}, Landroid/graphics/BlurMaskFilter;-><init>(FLandroid/graphics/BlurMaskFilter$Blur;)V

    .line 312
    .local v15, "outerBlurMaskFilter":Landroid/graphics/BlurMaskFilter;
    new-instance v13, Landroid/graphics/BlurMaskFilter;

    const/high16 v17, 0x40000000    # 2.0f

    sget-object v18, Landroid/graphics/BlurMaskFilter$Blur;->OUTER:Landroid/graphics/BlurMaskFilter$Blur;

    move/from16 v0, v17

    move-object/from16 v1, v18

    invoke-direct {v13, v0, v1}, Landroid/graphics/BlurMaskFilter;-><init>(FLandroid/graphics/BlurMaskFilter$Blur;)V

    .line 314
    .local v13, "mediumOuterBlurMaskFilter":Landroid/graphics/BlurMaskFilter;
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v17

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v18

    sget-object v19, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static/range {v17 .. v19}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v9

    .line 315
    .local v9, "iconWithOutLine":Landroid/graphics/Bitmap;
    new-instance v8, Landroid/graphics/Canvas;

    invoke-direct {v8, v9}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 317
    .local v8, "canvas":Landroid/graphics/Canvas;
    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    .line 318
    .local v5, "blurPaint":Landroid/graphics/Paint;
    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    .line 319
    .local v4, "blurColor":Landroid/graphics/Paint;
    const/16 v17, -0x1

    move/from16 v0, v17

    invoke-virtual {v4, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 321
    invoke-virtual {v5, v15}, Landroid/graphics/Paint;->setMaskFilter(Landroid/graphics/MaskFilter;)Landroid/graphics/MaskFilter;

    .line 322
    const/16 v17, 0x2

    move/from16 v0, v17

    new-array v0, v0, [I

    move-object/from16 v16, v0

    .line 323
    .local v16, "outerBlurOffset":[I
    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v5, v1}, Landroid/graphics/Bitmap;->extractAlpha(Landroid/graphics/Paint;[I)Landroid/graphics/Bitmap;

    move-result-object v14

    .line 325
    .local v14, "outerBlur":Landroid/graphics/Bitmap;
    invoke-virtual {v5, v13}, Landroid/graphics/Paint;->setMaskFilter(Landroid/graphics/MaskFilter;)Landroid/graphics/MaskFilter;

    .line 326
    const/16 v17, 0x2

    move/from16 v0, v17

    new-array v7, v0, [I

    .line 327
    .local v7, "brightOutlineOffset":[I
    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v7}, Landroid/graphics/Bitmap;->extractAlpha(Landroid/graphics/Paint;[I)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 329
    .local v6, "brightOutline":Landroid/graphics/Bitmap;
    invoke-virtual {v5, v11}, Landroid/graphics/Paint;->setMaskFilter(Landroid/graphics/MaskFilter;)Landroid/graphics/MaskFilter;

    .line 330
    const/16 v17, 0x2

    move/from16 v0, v17

    new-array v12, v0, [I

    .line 331
    .local v12, "innerBlurOffset":[I
    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v12}, Landroid/graphics/Bitmap;->extractAlpha(Landroid/graphics/Paint;[I)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 333
    .local v10, "innerBlur":Landroid/graphics/Bitmap;
    const/16 v17, 0x0

    sget-object v18, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    move/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v8, v0, v1}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 334
    const/16 v17, 0x0

    aget v17, v16, v17

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    const/16 v18, 0x1

    aget v18, v16, v18

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v8, v14, v0, v1, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 335
    const/16 v17, 0x0

    aget v17, v7, v17

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    const/16 v18, 0x1

    aget v18, v7, v18

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v8, v6, v0, v1, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 336
    const/16 v17, 0x0

    aget v17, v12, v17

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    const/16 v18, 0x1

    aget v18, v12, v18

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v8, v10, v0, v1, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 338
    invoke-virtual {v14}, Landroid/graphics/Bitmap;->recycle()V

    .line 339
    invoke-virtual {v6}, Landroid/graphics/Bitmap;->recycle()V

    .line 340
    invoke-virtual {v10}, Landroid/graphics/Bitmap;->recycle()V

    .line 342
    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v18

    move-object/from16 v3, v19

    invoke-virtual {v8, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 344
    return-object v9
.end method

.method private getAppIconDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 6
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 439
    const/4 v0, 0x0

    .line 440
    .local v0, "appIcon":Landroid/graphics/drawable/Drawable;
    iget-object v4, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->mActivity:Lcom/sec/android/app/shealth/home/HomeActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/home/HomeActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 442
    .local v3, "packageManager":Landroid/content/pm/PackageManager;
    new-instance v2, Landroid/content/Intent;

    const-string v4, "com.sec.shealth.action.STEALTH_MODE"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 443
    .local v2, "intent":Landroid/content/Intent;
    if-eqz v2, :cond_0

    .line 445
    invoke-virtual {v2, p1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 448
    :try_start_0
    invoke-virtual {v3, v2}, Landroid/content/pm/PackageManager;->getActivityIcon(Landroid/content/Intent;)Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 457
    :cond_0
    :goto_0
    return-object v0

    .line 450
    :catch_0
    move-exception v1

    .line 452
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 453
    iget-object v4, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->mActivity:Lcom/sec/android/app/shealth/home/HomeActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/home/HomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f02020b

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method

.method private getAppIconDrawable(Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "drawableName"    # Ljava/lang/String;

    .prologue
    .line 419
    const/4 v0, 0x0

    .line 423
    .local v0, "appIcon":Landroid/graphics/drawable/Drawable;
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->mActivity:Lcom/sec/android/app/shealth/home/HomeActivity;

    invoke-static {v2, p1}, Lcom/sec/android/app/shealth/common/utils/ResourceUtil;->getInstance(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/app/shealth/common/utils/ResourceUtil;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/sec/android/app/shealth/common/utils/ResourceUtil;->getDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 434
    :goto_0
    return-object v0

    .line 425
    :catch_0
    move-exception v1

    .line 427
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    if-nez v0, :cond_0

    .line 429
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->mActivity:Lcom/sec/android/app/shealth/home/HomeActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/home/HomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f02020b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 431
    :cond_0
    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method private getView(Lcom/sec/android/app/shealth/home/data/IconData;)Landroid/view/View;
    .locals 10
    .param p1, "iconData"    # Lcom/sec/android/app/shealth/home/data/IconData;

    .prologue
    const/4 v9, 0x0

    const/16 v8, 0x8

    const/4 v7, 0x1

    .line 360
    iget-object v5, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->mActivity:Lcom/sec/android/app/shealth/home/HomeActivity;

    invoke-static {v5}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    const v6, 0x7f030130

    invoke-virtual {v5, v6, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 361
    .local v2, "iconView":Landroid/view/View;
    const v5, 0x7f08033b

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    .line 362
    .local v3, "imageContainer":Landroid/widget/RelativeLayout;
    const v5, 0x7f0804f2

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 363
    .local v1, "iconText":Landroid/widget/TextView;
    const v5, 0x7f0804f0

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 364
    .local v0, "iconImage":Landroid/widget/ImageView;
    const v5, 0x7f08033d

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    .line 365
    .local v4, "thirdPartyIcon":Landroid/widget/ImageView;
    invoke-virtual {v3, v9}, Landroid/widget/RelativeLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 367
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/home/data/IconData;->getIconType()I

    move-result v5

    if-ne v5, v7, :cond_0

    .line 369
    invoke-virtual {v4, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 371
    iget-object v5, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->mActivity:Lcom/sec/android/app/shealth/home/HomeActivity;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/home/HomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f020431

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 372
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->mActivity:Lcom/sec/android/app/shealth/home/HomeActivity;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/home/HomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f090040

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->mActivity:Lcom/sec/android/app/shealth/home/HomeActivity;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/home/HomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f09020a

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 403
    :goto_0
    invoke-virtual {v2, p1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 404
    iget-object v5, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->iconclick:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 405
    iget-object v5, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->iconTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v2, v5}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 406
    return-object v2

    .line 376
    :cond_0
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/home/data/IconData;->getAppName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 377
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/home/data/IconData;->getAppType()I

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/home/data/IconData;->getAppType()I

    move-result v5

    if-ne v5, v7, :cond_2

    .line 379
    :cond_1
    invoke-virtual {v4, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 385
    :goto_1
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/home/data/IconData;->getAppDisplayPlugInIcons()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_4

    .line 387
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/home/data/IconData;->getAppPluginId()I

    move-result v5

    if-lez v5, :cond_3

    .line 389
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/home/data/IconData;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/home/data/IconData;->getAppDisplayPlugInIcons()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v5, v6}, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->getAppIconDrawable(Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 383
    :cond_2
    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 393
    :cond_3
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/home/data/IconData;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->getAppIconDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 398
    :cond_4
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/home/data/IconData;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->getAppIconDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method private getandOrderFavoriteApps()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 463
    iget-object v6, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->mActivity:Lcom/sec/android/app/shealth/home/HomeActivity;

    invoke-static {v8, v6}, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryDbManagerWithProvider;->getAllAppRegistryDatas(ZLandroid/content/Context;)Ljava/util/List;

    move-result-object v4

    .line 465
    .local v4, "pluginlist":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;>;"
    iget-object v6, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->mOrderedFavlist:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    .line 468
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 469
    .local v2, "mFavPluginlist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;>;"
    if-eqz v4, :cond_1

    .line 471
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    .line 473
    .local v5, "temp":Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;
    iget v6, v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->isFavorite:I

    if-ne v6, v8, :cond_0

    .line 475
    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 476
    iget-object v6, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->mOrderedFavlist:Ljava/util/ArrayList;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 483
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v5    # "temp":Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v0, v6, :cond_3

    .line 485
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget v6, v6, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->pluginId:I

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-object v6, v6, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->packagename:Ljava/lang/String;

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->getOrder(Ljava/lang/String;)I

    move-result v3

    .line 486
    .local v3, "order":I
    iget-object v6, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->mOrderedFavlist:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v3, v6, :cond_2

    if-ltz v3, :cond_2

    .line 487
    iget-object v6, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->mOrderedFavlist:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v6, v3, v7}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 483
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 489
    :cond_2
    const-string v7, "HomeFavorites"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Order Index Error for position="

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ", plugin name= "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-object v6, v6, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->appName:Ljava/lang/String;

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v7, v6}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 491
    .end local v3    # "order":I
    :cond_3
    return-void
.end method

.method private initContainer()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 252
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->shortcut_container:Landroid/widget/LinearLayout;

    if-nez v0, :cond_0

    .line 270
    :goto_0
    return-void

    .line 254
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->shortcut_container:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 255
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->checkToShowCigna()V

    .line 256
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->getandOrderFavoriteApps()V

    .line 257
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->setIconsList()V

    .line 258
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->addIconsToContainer()V

    .line 259
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->mIconList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 261
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->scroller:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v0, v2}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    .line 262
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->empty_favorites:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 266
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->scroller:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    .line 267
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->empty_favorites:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method private setIconsList()V
    .locals 6

    .prologue
    .line 496
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->mIconList:Ljava/util/ArrayList;

    if-nez v3, :cond_1

    .line 527
    :cond_0
    :goto_0
    return-void

    .line 500
    :cond_1
    const-string v3, "HomeFavorites"

    const-string v4, "Setting Icons List"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 501
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->mIconList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 502
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->mOrderedFavlist:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_3

    .line 504
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->mOrderedFavlist:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 506
    new-instance v2, Lcom/sec/android/app/shealth/home/data/IconData;

    invoke-direct {v2}, Lcom/sec/android/app/shealth/home/data/IconData;-><init>()V

    .line 507
    .local v2, "iconData":Lcom/sec/android/app/shealth/home/data/IconData;
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->mOrderedFavlist:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-object v3, v3, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->appName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/home/data/IconData;->setAppName(Ljava/lang/String;)V

    .line 508
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->mOrderedFavlist:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget v3, v3, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->appType:I

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/home/data/IconData;->setAppType(I)V

    .line 509
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->mOrderedFavlist:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-object v3, v3, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->displayPlugInIcons:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/home/data/IconData;->setAppDisplayPlugInIcons(Ljava/lang/String;)V

    .line 510
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->mOrderedFavlist:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget v3, v3, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->pluginId:I

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/home/data/IconData;->setAppPluginId(I)V

    .line 511
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->mOrderedFavlist:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-object v3, v3, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->packagename:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/home/data/IconData;->setPackageName(Ljava/lang/String;)V

    .line 512
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/home/data/IconData;->setIconType(I)V

    .line 513
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->mOrderedFavlist:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-object v3, v3, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->actions:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/home/data/IconData;->setAction(Ljava/lang/String;)V

    .line 514
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->mIconList:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 515
    const-string v3, "HomeFavorites"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "Icon data is : appname= "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/home/data/IconData;->getAppName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " |pluginID= "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/home/data/IconData;->getAppPluginId()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " |pluginICon="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/home/data/IconData;->getAppDisplayPlugInIcons()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " |packagename="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/home/data/IconData;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 502
    .end local v2    # "iconData":Lcom/sec/android/app/shealth/home/data/IconData;
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_1

    .line 521
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->mOrderedFavlist:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-eqz v3, :cond_0

    .line 523
    new-instance v0, Lcom/sec/android/app/shealth/home/data/IconData;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/home/data/IconData;-><init>()V

    .line 524
    .local v0, "editIcon":Lcom/sec/android/app/shealth/home/data/IconData;
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/home/data/IconData;->setIconType(I)V

    .line 525
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->mIconList:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method


# virtual methods
.method public destroy()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 531
    iput-object v0, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->mActivity:Lcom/sec/android/app/shealth/home/HomeActivity;

    .line 532
    iput-object v0, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->parentView:Landroid/view/View;

    .line 533
    iput-object v0, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->shortcut_container:Landroid/widget/LinearLayout;

    .line 534
    iput-object v0, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->empty_favorites:Landroid/widget/LinearLayout;

    .line 535
    iput-object v0, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->mOrderedFavlist:Ljava/util/ArrayList;

    .line 536
    iput-object v0, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->iconclick:Landroid/view/View$OnClickListener;

    .line 537
    iput-object v0, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->iconTouchListener:Landroid/view/View$OnTouchListener;

    .line 539
    return-void
.end method

.method public getFavCount()I
    .locals 1

    .prologue
    .line 411
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->mIconList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 412
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->mIconList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 414
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public initialise()Landroid/view/View;
    .locals 4

    .prologue
    .line 221
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->mActivity:Lcom/sec/android/app/shealth/home/HomeActivity;

    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/home/HomeActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 222
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x7f030132

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->parentView:Landroid/view/View;

    .line 223
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->parentView:Landroid/view/View;

    const v3, 0x7f080513

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->shortcut_container:Landroid/widget/LinearLayout;

    .line 224
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->parentView:Landroid/view/View;

    const v3, 0x7f080512

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/HorizontalScrollView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->scroller:Landroid/widget/HorizontalScrollView;

    .line 226
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->parentView:Landroid/view/View;

    const v3, 0x7f080514

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->empty_favorites:Landroid/widget/LinearLayout;

    .line 229
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->initContainer()V

    .line 231
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->parentView:Landroid/view/View;

    const v3, 0x7f080515

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 232
    .local v0, "imageView":Landroid/widget/ImageView;
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->mActivity:Lcom/sec/android/app/shealth/home/HomeActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/home/HomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09003f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 233
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->addiconTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 234
    new-instance v2, Lcom/sec/android/app/shealth/home/HomeShortcutManager$4;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/home/HomeShortcutManager$4;-><init>(Lcom/sec/android/app/shealth/home/HomeShortcutManager;)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 246
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->parentView:Landroid/view/View;

    return-object v2
.end method

.method public refresh()V
    .locals 0

    .prologue
    .line 303
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->initContainer()V

    .line 304
    return-void
.end method
