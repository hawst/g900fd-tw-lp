.class Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$7;
.super Ljava/lang/Object;
.source "EditProfileActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->customizeActionBar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)V
    .locals 0

    .prologue
    .line 1051
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$7;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1056
    instance-of v2, p1, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 1058
    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;

    .line 1060
    .local v0, "ab":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;->getActionButtonIndex()I

    move-result v2

    if-nez v2, :cond_1

    .line 1062
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$7;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    # invokes: Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->showDiscardPopup()V
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->access$2000(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)V

    .line 1085
    .end local v0    # "ab":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;
    :cond_0
    :goto_0
    return-void

    .line 1064
    .restart local v0    # "ab":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;
    :cond_1
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;->getActionButtonIndex()I

    move-result v2

    if-ne v2, v3, :cond_0

    .line 1066
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$7;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    # invokes: Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->checkInput()I
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->access$2100(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)I

    move-result v1

    .line 1068
    .local v1, "passFlag":I
    if-nez v1, :cond_2

    .line 1069
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$7;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    # invokes: Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->checkEditFocus()Z
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->access$200(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1071
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$7;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    # invokes: Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->saveProfile()Z
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->access$2200(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)Z

    goto :goto_0

    .line 1074
    :cond_2
    if-ne v1, v3, :cond_3

    .line 1075
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$7;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    const v3, 0x7f090837

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1077
    :cond_3
    const/4 v2, 0x2

    if-ne v1, v2, :cond_4

    .line 1078
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$7;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    const v3, 0x7f090839

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1081
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$7;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    const v3, 0x7f090ae2

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
