.class public Lcom/sec/android/app/shealth/common/config/FeatureTable;
.super Ljava/lang/Object;
.source "FeatureTable.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/common/config/FeatureTable$1;,
        Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;,
        Lcom/sec/android/app/shealth/common/config/FeatureTable$UV;,
        Lcom/sec/android/app/shealth/common/config/FeatureTable$Weight;,
        Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodGlucose;,
        Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodPressure;,
        Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;,
        Lcom/sec/android/app/shealth/common/config/FeatureTable$SPO2;,
        Lcom/sec/android/app/shealth/common/config/FeatureTable$Sleep;,
        Lcom/sec/android/app/shealth/common/config/FeatureTable$Stress;,
        Lcom/sec/android/app/shealth/common/config/FeatureTable$Support;,
        Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;,
        Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;
    }
.end annotation


# static fields
.field private static final LOG_DELIMITER:Ljava/lang/String; = "-----<<<<<"

.field private static final TAG:Ljava/lang/String;

.field private static hashMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 12
    const-class v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->TAG:Ljava/lang/String;

    .line 114
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    .line 119
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "WW"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 120
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "KR"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 121
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "US"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 122
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "CA"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 123
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "RS"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "ME"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "BA"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "AL"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 127
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "RU"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "BY"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 129
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "UA"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 130
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "MD"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 131
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "KZ"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "UZ"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 133
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "KG"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 134
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "TM"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "TJ"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 136
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "MN"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "AZ"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "AM"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "GE"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 140
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "CN"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 141
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "HK"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 142
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "SG"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 143
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "MY"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 144
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "AU"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 145
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "NZ"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 146
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "PH"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 147
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "VN"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 148
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "ID"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 149
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "TW"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 150
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "TH"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 151
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "KH"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 152
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "MM"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 153
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "LA"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 154
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "PG"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 155
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "IN"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 156
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "LK"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 157
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "BD"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 158
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "NP"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 159
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "BR"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 160
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "MX"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 161
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "AR"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 162
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "UY"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 163
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "PY"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 164
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "CL"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 165
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "BO"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 166
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "CO"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 167
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "PE"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 168
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "JM"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 169
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "PR"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 170
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "TT"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 171
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "VE"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 172
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "GT"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 173
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "HN"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 174
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "NI"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 175
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "SV"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 176
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "CR"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 177
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "PA"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 178
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "EC"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 179
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "DZ"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 180
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "EG"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 181
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "SA"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 182
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "PK"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 183
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "AF"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 184
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "MA"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 185
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "JO"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 186
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "IQ"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 187
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "LB"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 188
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "PS"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 189
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "AE"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 190
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "KW"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 191
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "QA"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 192
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "BH"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 193
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "OM"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 194
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "YE"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 195
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "UAE"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 196
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "IL"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 197
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "TN"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 198
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "LY"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 199
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "KE"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 200
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "TZ"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 201
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "CD"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 202
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "NG"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 203
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "GH"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 204
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "SN"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 205
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "CI"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 206
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "ZA"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 207
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "ZM"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "MU"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 209
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "AO"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 210
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "JP"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 211
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "TR"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 212
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "SD"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 213
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "DM"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 214
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "SY"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "IR"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 218
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "MO"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 219
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "FJ"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 220
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "WS"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 221
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "DO"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 222
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "UG"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 223
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "SO"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 224
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "ET"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 225
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "SS"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 226
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "CG"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 227
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "CM"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 228
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "GA"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 229
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "NE"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 230
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "TD"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 231
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "BJ"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 232
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "MR"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 233
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "ML"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 234
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "BF"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 235
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "SL"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 236
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "GN"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 237
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "GM"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 238
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "MZ"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 239
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "NA"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 240
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "BW"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 241
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "ZW"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 242
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "LS"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 243
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "MG"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 244
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "SZ"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 245
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "MW"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 248
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "IT"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->TRUE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 249
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "ES"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->TRUE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 250
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "DE"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->TRUE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 251
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "PT"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->TRUE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 252
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "GB"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->TRUE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 253
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "IE"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->TRUE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 254
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "NL"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->TRUE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 255
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "BE"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->TRUE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 256
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "LU"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->TRUE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 257
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "SE"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->TRUE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 258
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "FI"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->TRUE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 259
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "NO"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->TRUE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 260
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "DK"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->TRUE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 261
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "IS"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->TRUE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 262
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "FR"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->TRUE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 263
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "HR"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->TRUE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 264
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "MK"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->TRUE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 265
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "AT"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->TRUE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 266
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "CH"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->TRUE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 267
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "SI"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->TRUE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 268
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "LT"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->TRUE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 269
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "LV"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->TRUE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 270
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "EE"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->TRUE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 271
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "CZ"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->TRUE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 272
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "SK"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->TRUE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 273
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "GR"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->TRUE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 274
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "CY"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->TRUE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 275
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "HU"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->TRUE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 276
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "PL"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->TRUE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 277
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "RO"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->TRUE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 278
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "BG"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->TRUE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 279
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    const-string v1, "XK"

    new-instance v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->TRUE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 283
    invoke-static {}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->setStress()V

    .line 284
    invoke-static {}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->setSleep()V

    .line 285
    invoke-static {}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->setSPO2()V

    .line 286
    invoke-static {}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->setHR()V

    .line 287
    invoke-static {}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->setBP()V

    .line 288
    invoke-static {}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->setBG()V

    .line 289
    invoke-static {}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->setWeight()V

    .line 290
    invoke-static {}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->setUV()V

    .line 291
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    return-void
.end method

.method public static checkFeature(Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;)Lcom/sec/android/app/shealth/common/config/FeatureTable$Support;
    .locals 4
    .param p0, "type"    # Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    .prologue
    .line 648
    invoke-static {}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->getCountryCode()Ljava/lang/String;

    move-result-object v0

    .line 649
    .local v0, "countryCode":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 651
    sget-object v2, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    .line 652
    .local v1, "feature":Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;
    if-eqz v1, :cond_0

    .line 654
    sget-object v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$1;->$SwitchMap$com$sec$android$app$shealth$common$config$FeatureTable$FeatureType:[I

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 677
    .end local v1    # "feature":Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;
    :cond_0
    sget-object v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$1;->$SwitchMap$com$sec$android$app$shealth$common$config$FeatureTable$FeatureType:[I

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_1

    .line 697
    const/4 v2, 0x0

    :goto_0
    return-object v2

    .line 657
    .restart local v1    # "feature":Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;
    :pswitch_0
    # getter for: Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->stress:Lcom/sec/android/app/shealth/common/config/FeatureTable$Stress;
    invoke-static {v1}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->access$100(Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;)Lcom/sec/android/app/shealth/common/config/FeatureTable$Stress;

    move-result-object v2

    goto :goto_0

    .line 659
    :pswitch_1
    # getter for: Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->sleep:Lcom/sec/android/app/shealth/common/config/FeatureTable$Sleep;
    invoke-static {v1}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->access$200(Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;)Lcom/sec/android/app/shealth/common/config/FeatureTable$Sleep;

    move-result-object v2

    goto :goto_0

    .line 661
    :pswitch_2
    # getter for: Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->spo2:Lcom/sec/android/app/shealth/common/config/FeatureTable$SPO2;
    invoke-static {v1}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->access$300(Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;)Lcom/sec/android/app/shealth/common/config/FeatureTable$SPO2;

    move-result-object v2

    goto :goto_0

    .line 663
    :pswitch_3
    # getter for: Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->heartRate:Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;
    invoke-static {v1}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->access$400(Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;)Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;

    move-result-object v2

    goto :goto_0

    .line 665
    :pswitch_4
    # getter for: Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->bloodPressure:Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodPressure;
    invoke-static {v1}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->access$500(Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;)Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodPressure;

    move-result-object v2

    goto :goto_0

    .line 667
    :pswitch_5
    # getter for: Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->bloodGlucose:Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodGlucose;
    invoke-static {v1}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->access$600(Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;)Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodGlucose;

    move-result-object v2

    goto :goto_0

    .line 669
    :pswitch_6
    # getter for: Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->weight:Lcom/sec/android/app/shealth/common/config/FeatureTable$Weight;
    invoke-static {v1}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->access$700(Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;)Lcom/sec/android/app/shealth/common/config/FeatureTable$Weight;

    move-result-object v2

    goto :goto_0

    .line 671
    :pswitch_7
    # getter for: Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->uv:Lcom/sec/android/app/shealth/common/config/FeatureTable$UV;
    invoke-static {v1}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->access$800(Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;)Lcom/sec/android/app/shealth/common/config/FeatureTable$UV;

    move-result-object v2

    goto :goto_0

    .line 680
    .end local v1    # "feature":Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;
    :pswitch_8
    sget-object v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Stress;->SUPPORTED:Lcom/sec/android/app/shealth/common/config/FeatureTable$Stress;

    goto :goto_0

    .line 682
    :pswitch_9
    sget-object v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Sleep;->SUPPORTED:Lcom/sec/android/app/shealth/common/config/FeatureTable$Sleep;

    goto :goto_0

    .line 684
    :pswitch_a
    sget-object v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$SPO2;->FULL:Lcom/sec/android/app/shealth/common/config/FeatureTable$SPO2;

    goto :goto_0

    .line 686
    :pswitch_b
    sget-object v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;->FULL:Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;

    goto :goto_0

    .line 688
    :pswitch_c
    sget-object v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodPressure;->SUPPORTED:Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodPressure;

    goto :goto_0

    .line 690
    :pswitch_d
    sget-object v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodGlucose;->SUPPORTED:Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodGlucose;

    goto :goto_0

    .line 692
    :pswitch_e
    sget-object v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Weight;->SUPPORTED:Lcom/sec/android/app/shealth/common/config/FeatureTable$Weight;

    goto :goto_0

    .line 694
    :pswitch_f
    sget-object v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$UV;->SUPPORTED:Lcom/sec/android/app/shealth/common/config/FeatureTable$UV;

    goto :goto_0

    .line 654
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch

    .line 677
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
    .end packed-switch
.end method

.method private static getCountryCode()Ljava/lang/String;
    .locals 9

    .prologue
    .line 491
    const/4 v2, 0x0

    .line 495
    .local v2, "countryCode":Ljava/lang/String;
    :try_start_0
    const-string v5, "android.os.SystemProperties"

    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 496
    .local v1, "cl":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v5, "get"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v7, 0x0

    const-class v8, Ljava/lang/String;

    aput-object v8, v6, v7

    invoke-virtual {v1, v5, v6}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    .line 497
    .local v4, "method":Ljava/lang/reflect/Method;
    const/4 v5, 0x0

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-string/jumbo v8, "ro.csc.countryiso_code"

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    move-object v0, v5

    check-cast v0, Ljava/lang/String;

    move-object v2, v0

    .line 499
    sget-object v5, Lcom/sec/android/app/shealth/common/config/FeatureTable;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "country code:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 505
    .end local v1    # "cl":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v4    # "method":Ljava/lang/reflect/Method;
    :goto_0
    return-object v2

    .line 501
    :catch_0
    move-exception v3

    .line 503
    .local v3, "e":Ljava/lang/Exception;
    invoke-static {v3}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->logThrowable(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private static getSalesCode()Ljava/lang/String;
    .locals 9

    .prologue
    .line 510
    const/4 v4, 0x0

    .line 514
    .local v4, "salesCode":Ljava/lang/String;
    :try_start_0
    const-string v5, "android.os.SystemProperties"

    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 515
    .local v1, "cl":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v5, "get"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v7, 0x0

    const-class v8, Ljava/lang/String;

    aput-object v8, v6, v7

    invoke-virtual {v1, v5, v6}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 516
    .local v3, "method":Ljava/lang/reflect/Method;
    const/4 v5, 0x0

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-string/jumbo v8, "ro.csc.sales_code"

    aput-object v8, v6, v7

    invoke-virtual {v3, v5, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    move-object v0, v5

    check-cast v0, Ljava/lang/String;

    move-object v4, v0

    .line 518
    sget-object v5, Lcom/sec/android/app/shealth/common/config/FeatureTable;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "sales code:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 524
    .end local v1    # "cl":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v3    # "method":Ljava/lang/reflect/Method;
    :goto_0
    return-object v4

    .line 520
    :catch_0
    move-exception v2

    .line 522
    .local v2, "e":Ljava/lang/Exception;
    invoke-static {v2}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->logThrowable(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static isChinaModel()Z
    .locals 2

    .prologue
    .line 611
    invoke-static {}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->getCountryCode()Ljava/lang/String;

    move-result-object v0

    .line 612
    .local v0, "countryCode":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v1, "CN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isEURModel()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 574
    invoke-static {}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->getCountryCode()Ljava/lang/String;

    move-result-object v0

    .line 575
    .local v0, "countryCode":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 577
    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    .line 578
    .local v1, "feature":Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;
    if-eqz v1, :cond_0

    # getter for: Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->eur:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;
    invoke-static {v1}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->access$900(Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;)Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    move-result-object v3

    sget-object v4, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->TRUE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    if-ne v3, v4, :cond_0

    const/4 v2, 0x1

    .line 580
    .end local v1    # "feature":Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;
    :cond_0
    return v2
.end method

.method public static isGrandChinaModel()Z
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 593
    const/4 v5, 0x3

    new-array v1, v5, [Ljava/lang/String;

    const-string v5, "CN"

    aput-object v5, v1, v4

    const-string v5, "HK"

    aput-object v5, v1, v3

    const/4 v5, 0x2

    const-string v6, "TW"

    aput-object v6, v1, v5

    .line 594
    .local v1, "grandChinaList":[Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->getCountryCode()Ljava/lang/String;

    move-result-object v0

    .line 595
    .local v0, "countryCode":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 597
    array-length v5, v1

    add-int/lit8 v2, v5, -0x1

    .local v2, "i":I
    :goto_0
    if-ltz v2, :cond_1

    .line 599
    aget-object v5, v1, v2

    invoke-virtual {v0, v5}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_0

    .line 605
    .end local v2    # "i":I
    :goto_1
    return v3

    .line 597
    .restart local v2    # "i":I
    :cond_0
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    .end local v2    # "i":I
    :cond_1
    move v3, v4

    .line 605
    goto :goto_1
.end method

.method public static isMmolCountry()Z
    .locals 9

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 619
    const/16 v6, 0x20

    new-array v1, v6, [Ljava/lang/String;

    const-string v6, "GB"

    aput-object v6, v1, v5

    const-string v6, "IE"

    aput-object v6, v1, v4

    const/4 v6, 0x2

    const-string v7, "NL"

    aput-object v7, v1, v6

    const/4 v6, 0x3

    const-string v7, "SE"

    aput-object v7, v1, v6

    const/4 v6, 0x4

    const-string v7, "FI"

    aput-object v7, v1, v6

    const/4 v6, 0x5

    const-string v7, "NO"

    aput-object v7, v1, v6

    const/4 v6, 0x6

    const-string v7, "DK"

    aput-object v7, v1, v6

    const/4 v6, 0x7

    const-string v7, "IS"

    aput-object v7, v1, v6

    const/16 v6, 0x8

    const-string v7, "HR"

    aput-object v7, v1, v6

    const/16 v6, 0x9

    const-string v7, "MK"

    aput-object v7, v1, v6

    const/16 v6, 0xa

    const-string v7, "AT"

    aput-object v7, v1, v6

    const/16 v6, 0xb

    const-string v7, "CH"

    aput-object v7, v1, v6

    const/16 v6, 0xc

    const-string v7, "SI"

    aput-object v7, v1, v6

    const/16 v6, 0xd

    const-string v7, "LT"

    aput-object v7, v1, v6

    const/16 v6, 0xe

    const-string v7, "LV"

    aput-object v7, v1, v6

    const/16 v6, 0xf

    const-string v7, "EE"

    aput-object v7, v1, v6

    const/16 v6, 0x10

    const-string v7, "CZ"

    aput-object v7, v1, v6

    const/16 v6, 0x11

    const-string v7, "SK"

    aput-object v7, v1, v6

    const/16 v6, 0x12

    const-string v7, "HU"

    aput-object v7, v1, v6

    const/16 v6, 0x13

    const-string v7, "BG"

    aput-object v7, v1, v6

    const/16 v6, 0x14

    const-string v7, "CA"

    aput-object v7, v1, v6

    const/16 v6, 0x15

    const-string v7, "RU"

    aput-object v7, v1, v6

    const/16 v6, 0x16

    const-string v7, "UA"

    aput-object v7, v1, v6

    const/16 v6, 0x17

    const-string v7, "KZ"

    aput-object v7, v1, v6

    const/16 v6, 0x18

    const-string v7, "CN"

    aput-object v7, v1, v6

    const/16 v6, 0x19

    const-string v7, "HK"

    aput-object v7, v1, v6

    const/16 v6, 0x1a

    const-string v7, "SG"

    aput-object v7, v1, v6

    const/16 v6, 0x1b

    const-string v7, "MY"

    aput-object v7, v1, v6

    const/16 v6, 0x1c

    const-string v7, "NZ"

    aput-object v7, v1, v6

    const/16 v6, 0x1d

    const-string v7, "VN"

    aput-object v7, v1, v6

    const/16 v6, 0x1e

    const-string v7, "QA"

    aput-object v7, v1, v6

    const/16 v6, 0x1f

    const-string v7, "ZA"

    aput-object v7, v1, v6

    .line 624
    .local v1, "countryList":[Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->getCountryCode()Ljava/lang/String;

    move-result-object v0

    .line 625
    .local v0, "countryCode":Ljava/lang/String;
    if-eqz v0, :cond_2

    .line 627
    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    .line 628
    .local v3, "strCountryCode":Ljava/lang/String;
    sget-object v6, Lcom/sec/android/app/shealth/common/config/FeatureTable;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "strCountryCode : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 630
    array-length v6, v1

    add-int/lit8 v2, v6, -0x1

    .local v2, "i":I
    :goto_0
    if-ltz v2, :cond_1

    .line 632
    aget-object v6, v1, v2

    invoke-virtual {v3, v6}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_0

    .line 634
    sget-object v5, Lcom/sec/android/app/shealth/common/config/FeatureTable;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " --> mmol/L"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 643
    .end local v2    # "i":I
    .end local v3    # "strCountryCode":Ljava/lang/String;
    :goto_1
    return v4

    .line 630
    .restart local v2    # "i":I
    .restart local v3    # "strCountryCode":Ljava/lang/String;
    :cond_0
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    .line 639
    :cond_1
    sget-object v4, Lcom/sec/android/app/shealth/common/config/FeatureTable;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " --> mg/dL"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move v4, v5

    .line 640
    goto :goto_1

    .end local v2    # "i":I
    .end local v3    # "strCountryCode":Ljava/lang/String;
    :cond_2
    move v4, v5

    .line 643
    goto :goto_1
.end method

.method public static isUSModel()Z
    .locals 2

    .prologue
    .line 586
    invoke-static {}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->getCountryCode()Ljava/lang/String;

    move-result-object v0

    .line 587
    .local v0, "countryCode":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v1, "US"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static logThrowable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "throwable"    # Ljava/lang/Throwable;

    .prologue
    .line 562
    invoke-static {p0, p1, p2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 563
    invoke-virtual {p2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    .line 564
    .local v0, "cause":Ljava/lang/Throwable;
    if-eqz v0, :cond_0

    .line 566
    const-string v1, "-----<<<<<Was caused by"

    invoke-static {p0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 567
    invoke-static {p0, v0}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 569
    :cond_0
    return-void
.end method

.method private static logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "throwable"    # Ljava/lang/Throwable;

    .prologue
    .line 549
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "-----<<<<<"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, p1}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->logThrowable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 550
    return-void
.end method

.method private static logThrowable(Ljava/lang/Throwable;)V
    .locals 1
    .param p0, "throwable"    # Ljava/lang/Throwable;

    .prologue
    .line 537
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable;->TAG:Ljava/lang/String;

    invoke-static {v0, p0}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 538
    return-void
.end method

.method private static setBG()V
    .locals 4

    .prologue
    .line 428
    const/16 v2, 0x11

    new-array v0, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "AO"

    aput-object v3, v0, v2

    const/4 v2, 0x1

    const-string v3, "GB"

    aput-object v3, v0, v2

    const/4 v2, 0x2

    const-string v3, "IE"

    aput-object v3, v0, v2

    const/4 v2, 0x3

    const-string v3, "FR"

    aput-object v3, v0, v2

    const/4 v2, 0x4

    const-string v3, "NL"

    aput-object v3, v0, v2

    const/4 v2, 0x5

    const-string v3, "BE"

    aput-object v3, v0, v2

    const/4 v2, 0x6

    const-string v3, "LU"

    aput-object v3, v0, v2

    const/4 v2, 0x7

    const-string v3, "AT"

    aput-object v3, v0, v2

    const/16 v2, 0x8

    const-string v3, "SK"

    aput-object v3, v0, v2

    const/16 v2, 0x9

    const-string v3, "GR"

    aput-object v3, v0, v2

    const/16 v2, 0xa

    const-string v3, "CY"

    aput-object v3, v0, v2

    const/16 v2, 0xb

    const-string v3, "SI"

    aput-object v3, v0, v2

    const/16 v2, 0xc

    const-string v3, "DZ"

    aput-object v3, v0, v2

    const/16 v2, 0xd

    const-string v3, "IR"

    aput-object v3, v0, v2

    const/16 v2, 0xe

    const-string v3, "CA"

    aput-object v3, v0, v2

    const/16 v2, 0xf

    const-string v3, "KR"

    aput-object v3, v0, v2

    const/16 v2, 0x10

    const-string v3, "WW"

    aput-object v3, v0, v2

    .line 448
    .local v0, "countryBlackList":[Ljava/lang/String;
    array-length v2, v0

    add-int/lit8 v1, v2, -0x1

    .local v1, "i":I
    :goto_0
    if-ltz v1, :cond_0

    .line 450
    sget-object v2, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    aget-object v3, v0, v1

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodGlucose;->NOT_SUPPORTED:Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodGlucose;

    # setter for: Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->bloodGlucose:Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodGlucose;
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->access$602(Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodGlucose;)Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodGlucose;

    .line 448
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 452
    :cond_0
    return-void
.end method

.method private static setBP()V
    .locals 4

    .prologue
    .line 399
    const/16 v2, 0x11

    new-array v0, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "AO"

    aput-object v3, v0, v2

    const/4 v2, 0x1

    const-string v3, "GB"

    aput-object v3, v0, v2

    const/4 v2, 0x2

    const-string v3, "IE"

    aput-object v3, v0, v2

    const/4 v2, 0x3

    const-string v3, "FR"

    aput-object v3, v0, v2

    const/4 v2, 0x4

    const-string v3, "NL"

    aput-object v3, v0, v2

    const/4 v2, 0x5

    const-string v3, "BE"

    aput-object v3, v0, v2

    const/4 v2, 0x6

    const-string v3, "LU"

    aput-object v3, v0, v2

    const/4 v2, 0x7

    const-string v3, "AT"

    aput-object v3, v0, v2

    const/16 v2, 0x8

    const-string v3, "SK"

    aput-object v3, v0, v2

    const/16 v2, 0x9

    const-string v3, "GR"

    aput-object v3, v0, v2

    const/16 v2, 0xa

    const-string v3, "CY"

    aput-object v3, v0, v2

    const/16 v2, 0xb

    const-string v3, "SI"

    aput-object v3, v0, v2

    const/16 v2, 0xc

    const-string v3, "DZ"

    aput-object v3, v0, v2

    const/16 v2, 0xd

    const-string v3, "IR"

    aput-object v3, v0, v2

    const/16 v2, 0xe

    const-string v3, "CA"

    aput-object v3, v0, v2

    const/16 v2, 0xf

    const-string v3, "KR"

    aput-object v3, v0, v2

    const/16 v2, 0x10

    const-string v3, "WW"

    aput-object v3, v0, v2

    .line 419
    .local v0, "countryBlackList":[Ljava/lang/String;
    array-length v2, v0

    add-int/lit8 v1, v2, -0x1

    .local v1, "i":I
    :goto_0
    if-ltz v1, :cond_0

    .line 421
    sget-object v2, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    aget-object v3, v0, v1

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodPressure;->NOT_SUPPORTED:Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodPressure;

    # setter for: Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->bloodPressure:Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodPressure;
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->access$502(Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodPressure;)Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodPressure;

    .line 419
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 423
    :cond_0
    return-void
.end method

.method private static setHR()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 371
    new-array v0, v6, [Ljava/lang/String;

    const-string v3, "AO"

    aput-object v3, v0, v5

    .line 375
    .local v0, "countryBlackList":[Ljava/lang/String;
    array-length v3, v0

    add-int/lit8 v2, v3, -0x1

    .local v2, "i":I
    :goto_0
    if-ltz v2, :cond_0

    .line 377
    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    aget-object v4, v0, v2

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v4, Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;->NOT_SUPPORTED:Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;

    # setter for: Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->heartRate:Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;
    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->access$402(Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;)Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;

    .line 375
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    .line 381
    :cond_0
    const/4 v3, 0x6

    new-array v1, v3, [Ljava/lang/String;

    const-string v3, "GB"

    aput-object v3, v1, v5

    const-string v3, "DZ"

    aput-object v3, v1, v6

    const/4 v3, 0x2

    const-string v4, "LY"

    aput-object v4, v1, v3

    const/4 v3, 0x3

    const-string v4, "JP"

    aput-object v4, v1, v3

    const/4 v3, 0x4

    const-string v4, "KR"

    aput-object v4, v1, v3

    const/4 v3, 0x5

    const-string v4, "WW"

    aput-object v4, v1, v3

    .line 390
    .local v1, "countryNonMedicalList":[Ljava/lang/String;
    array-length v3, v1

    add-int/lit8 v2, v3, -0x1

    :goto_1
    if-ltz v2, :cond_1

    .line 392
    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    aget-object v4, v1, v2

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v4, Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;->NOT_MEDICAL_ONLY:Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;

    # setter for: Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->heartRate:Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;
    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->access$402(Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;)Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;

    .line 390
    add-int/lit8 v2, v2, -0x1

    goto :goto_1

    .line 394
    :cond_1
    return-void
.end method

.method private static setSPO2()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 337
    const/16 v3, 0xc

    new-array v0, v3, [Ljava/lang/String;

    const-string v3, "AO"

    aput-object v3, v0, v5

    const/4 v3, 0x1

    const-string v4, "ZA"

    aput-object v4, v0, v3

    const/4 v3, 0x2

    const-string v4, "CN"

    aput-object v4, v0, v3

    const/4 v3, 0x3

    const-string v4, "FR"

    aput-object v4, v0, v3

    const/4 v3, 0x4

    const-string v4, "SK"

    aput-object v4, v0, v3

    const/4 v3, 0x5

    const-string v4, "DZ"

    aput-object v4, v0, v3

    const/4 v3, 0x6

    const-string v4, "IR"

    aput-object v4, v0, v3

    const/4 v3, 0x7

    const-string v4, "LY"

    aput-object v4, v0, v3

    const/16 v3, 0x8

    const-string v4, "TH"

    aput-object v4, v0, v3

    const/16 v3, 0x9

    const-string v4, "JP"

    aput-object v4, v0, v3

    const/16 v3, 0xa

    const-string v4, "KR"

    aput-object v4, v0, v3

    const/16 v3, 0xb

    const-string v4, "WW"

    aput-object v4, v0, v3

    .line 352
    .local v0, "countryBlackList":[Ljava/lang/String;
    array-length v3, v0

    add-int/lit8 v2, v3, -0x1

    .local v2, "i":I
    :goto_0
    if-ltz v2, :cond_0

    .line 354
    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    aget-object v4, v0, v2

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v4, Lcom/sec/android/app/shealth/common/config/FeatureTable$SPO2;->NOT_SUPPORTED:Lcom/sec/android/app/shealth/common/config/FeatureTable$SPO2;

    # setter for: Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->spo2:Lcom/sec/android/app/shealth/common/config/FeatureTable$SPO2;
    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->access$302(Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;Lcom/sec/android/app/shealth/common/config/FeatureTable$SPO2;)Lcom/sec/android/app/shealth/common/config/FeatureTable$SPO2;

    .line 352
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    .line 358
    :cond_0
    new-array v1, v5, [Ljava/lang/String;

    .line 362
    .local v1, "countryPartialList":[Ljava/lang/String;
    array-length v3, v1

    add-int/lit8 v2, v3, -0x1

    :goto_1
    if-ltz v2, :cond_1

    .line 364
    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    aget-object v4, v1, v2

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v4, Lcom/sec/android/app/shealth/common/config/FeatureTable$SPO2;->NOT_MEDICAL_ONLY:Lcom/sec/android/app/shealth/common/config/FeatureTable$SPO2;

    # setter for: Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->spo2:Lcom/sec/android/app/shealth/common/config/FeatureTable$SPO2;
    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->access$302(Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;Lcom/sec/android/app/shealth/common/config/FeatureTable$SPO2;)Lcom/sec/android/app/shealth/common/config/FeatureTable$SPO2;

    .line 362
    add-int/lit8 v2, v2, -0x1

    goto :goto_1

    .line 366
    :cond_1
    return-void
.end method

.method private static setSleep()V
    .locals 4

    .prologue
    .line 323
    const/4 v2, 0x2

    new-array v0, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "JP"

    aput-object v3, v0, v2

    const/4 v2, 0x1

    const-string v3, "DZ"

    aput-object v3, v0, v2

    .line 328
    .local v0, "countryList":[Ljava/lang/String;
    array-length v2, v0

    add-int/lit8 v1, v2, -0x1

    .local v1, "i":I
    :goto_0
    if-ltz v1, :cond_0

    .line 330
    sget-object v2, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    aget-object v3, v0, v1

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$Sleep;->NOT_SUPPORTED:Lcom/sec/android/app/shealth/common/config/FeatureTable$Sleep;

    # setter for: Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->sleep:Lcom/sec/android/app/shealth/common/config/FeatureTable$Sleep;
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->access$202(Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;Lcom/sec/android/app/shealth/common/config/FeatureTable$Sleep;)Lcom/sec/android/app/shealth/common/config/FeatureTable$Sleep;

    .line 328
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 332
    :cond_0
    return-void
.end method

.method private static setStress()V
    .locals 4

    .prologue
    .line 297
    const/16 v2, 0xe

    new-array v0, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "AO"

    aput-object v3, v0, v2

    const/4 v2, 0x1

    const-string v3, "ZA"

    aput-object v3, v0, v2

    const/4 v2, 0x2

    const-string v3, "GB"

    aput-object v3, v0, v2

    const/4 v2, 0x3

    const-string v3, "FR"

    aput-object v3, v0, v2

    const/4 v2, 0x4

    const-string v3, "AT"

    aput-object v3, v0, v2

    const/4 v2, 0x5

    const-string v3, "HU"

    aput-object v3, v0, v2

    const/4 v2, 0x6

    const-string v3, "CZ"

    aput-object v3, v0, v2

    const/4 v2, 0x7

    const-string v3, "GR"

    aput-object v3, v0, v2

    const/16 v2, 0x8

    const-string v3, "SI"

    aput-object v3, v0, v2

    const/16 v2, 0x9

    const-string v3, "DZ"

    aput-object v3, v0, v2

    const/16 v2, 0xa

    const-string v3, "TH"

    aput-object v3, v0, v2

    const/16 v2, 0xb

    const-string v3, "CA"

    aput-object v3, v0, v2

    const/16 v2, 0xc

    const-string v3, "CY"

    aput-object v3, v0, v2

    const/16 v2, 0xd

    const-string v3, "JP"

    aput-object v3, v0, v2

    .line 314
    .local v0, "countryBlackList":[Ljava/lang/String;
    array-length v2, v0

    add-int/lit8 v1, v2, -0x1

    .local v1, "i":I
    :goto_0
    if-ltz v1, :cond_0

    .line 316
    sget-object v2, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    aget-object v3, v0, v1

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$Stress;->NOT_SUPPORTED:Lcom/sec/android/app/shealth/common/config/FeatureTable$Stress;

    # setter for: Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->stress:Lcom/sec/android/app/shealth/common/config/FeatureTable$Stress;
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->access$102(Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;Lcom/sec/android/app/shealth/common/config/FeatureTable$Stress;)Lcom/sec/android/app/shealth/common/config/FeatureTable$Stress;

    .line 314
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 318
    :cond_0
    return-void
.end method

.method private static setUV()V
    .locals 4

    .prologue
    .line 479
    const/4 v2, 0x1

    new-array v0, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "DZ"

    aput-object v3, v0, v2

    .line 483
    .local v0, "countryBlackList":[Ljava/lang/String;
    array-length v2, v0

    add-int/lit8 v1, v2, -0x1

    .local v1, "i":I
    :goto_0
    if-ltz v1, :cond_0

    .line 485
    sget-object v2, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    aget-object v3, v0, v1

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$UV;->NOT_SUPPORTED:Lcom/sec/android/app/shealth/common/config/FeatureTable$UV;

    # setter for: Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->uv:Lcom/sec/android/app/shealth/common/config/FeatureTable$UV;
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->access$802(Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;Lcom/sec/android/app/shealth/common/config/FeatureTable$UV;)Lcom/sec/android/app/shealth/common/config/FeatureTable$UV;

    .line 483
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 487
    :cond_0
    return-void
.end method

.method private static setWeight()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 457
    new-array v0, v5, [Ljava/lang/String;

    .line 461
    .local v0, "countryBlackList":[Ljava/lang/String;
    array-length v3, v0

    add-int/lit8 v2, v3, -0x1

    .local v2, "i":I
    :goto_0
    if-ltz v2, :cond_0

    .line 463
    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    aget-object v4, v0, v2

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v4, Lcom/sec/android/app/shealth/common/config/FeatureTable$Weight;->NOT_SUPPORTED:Lcom/sec/android/app/shealth/common/config/FeatureTable$Weight;

    # setter for: Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->weight:Lcom/sec/android/app/shealth/common/config/FeatureTable$Weight;
    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->access$702(Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;Lcom/sec/android/app/shealth/common/config/FeatureTable$Weight;)Lcom/sec/android/app/shealth/common/config/FeatureTable$Weight;

    .line 461
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    .line 467
    :cond_0
    const/4 v3, 0x1

    new-array v1, v3, [Ljava/lang/String;

    const-string v3, "AO"

    aput-object v3, v1, v5

    .line 471
    .local v1, "countryNonMedicalList":[Ljava/lang/String;
    array-length v3, v1

    add-int/lit8 v2, v3, -0x1

    :goto_1
    if-ltz v2, :cond_1

    .line 472
    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable;->hashMap:Ljava/util/HashMap;

    aget-object v4, v1, v2

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    sget-object v4, Lcom/sec/android/app/shealth/common/config/FeatureTable$Weight;->NOT_MEDICAL_ONLY:Lcom/sec/android/app/shealth/common/config/FeatureTable$Weight;

    # setter for: Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->weight:Lcom/sec/android/app/shealth/common/config/FeatureTable$Weight;
    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->access$702(Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;Lcom/sec/android/app/shealth/common/config/FeatureTable$Weight;)Lcom/sec/android/app/shealth/common/config/FeatureTable$Weight;

    .line 471
    add-int/lit8 v2, v2, -0x1

    goto :goto_1

    .line 474
    :cond_1
    return-void
.end method
