.class Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$3;
.super Ljava/lang/Object;
.source "GeneralMealActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->getContentInitializationListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;)V
    .locals 0

    .prologue
    .line 566
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$3;->this$0:Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onContentInitialization(Landroid/view/View;Landroid/app/Activity;Landroid/app/Dialog;Landroid/os/Bundle;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V
    .locals 2
    .param p1, "content"    # Landroid/view/View;
    .param p2, "parentActivity"    # Landroid/app/Activity;
    .param p3, "dialog"    # Landroid/app/Dialog;
    .param p4, "savedInstanceState"    # Landroid/os/Bundle;
    .param p5, "okButtonHandler"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;

    .prologue
    .line 572
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$3;->this$0:Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;

    const v0, 0x7f080398

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    # setter for: Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mMyMealEditTextFromPopup:Landroid/widget/EditText;
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->access$602(Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;Landroid/widget/EditText;)Landroid/widget/EditText;

    .line 573
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$3;->this$0:Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mMyMealEditTextFromPopup:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->access$600(Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;)Landroid/widget/EditText;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$3;->this$0:Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;

    # invokes: Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->generateDefaultNameForMyMeal()Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->access$700(Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 574
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$3;->this$0:Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mMyMealEditTextFromPopup:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->access$600(Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;)Landroid/widget/EditText;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$3$1;

    invoke-direct {v1, p0, p5}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$3$1;-><init>(Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$3;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 581
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$3;->this$0:Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mMyMealEditTextFromPopup:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->access$600(Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;)Landroid/widget/EditText;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$3$2;

    invoke-direct {v1, p0, p2}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$3$2;-><init>(Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$3;Landroid/app/Activity;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->post(Ljava/lang/Runnable;)Z

    .line 589
    return-void
.end method
