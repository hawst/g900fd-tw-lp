.class public Lcom/sec/android/app/shealth/cignacoach/library/TipArticleListActivity;
.super Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;
.source "TipArticleListActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mArticlelistData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;",
            ">;"
        }
    .end annotation
.end field

.field private mSelectedCategoryId:I

.field private mSelectedSubCategoryId:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleListActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleListActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;-><init>()V

    .line 23
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleListActivity;->mArticlelistData:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, -0x1

    .line 31
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 32
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "extra_name_category_id"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleListActivity;->mSelectedCategoryId:I

    .line 33
    const-string v2, "extra_name_sub_category_id"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleListActivity;->mSelectedSubCategoryId:I

    .line 35
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleListActivity;->validateIntentData()Z

    move-result v2

    if-nez v2, :cond_0

    .line 36
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleListActivity;->finish()V

    .line 52
    :goto_0
    return-void

    .line 40
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 41
    const v2, 0x7f030055

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleListActivity;->setContentView(I)V

    .line 43
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryDataManager;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleListActivity;->mSelectedCategoryId:I

    iget v4, p0, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleListActivity;->mSelectedSubCategoryId:I

    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleListActivity;->mArticlelistData:Ljava/util/ArrayList;

    invoke-virtual {v2, v3, v4, v5}, Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryDataManager;->getLibraryArticle(IILjava/util/ArrayList;)V

    .line 45
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleListActivity;->mArticlelistData:Ljava/util/ArrayList;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleListActivity;->mArticlelistData:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_1

    .line 46
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleListActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleListActivity;->mArticlelistData:Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->getSubCategory()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(Ljava/lang/String;)V

    .line 49
    :cond_1
    const v2, 0x7f0801c3

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    .line 50
    .local v1, "subCategoryList":Landroid/widget/ListView;
    new-instance v2, Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter;

    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleListActivity;->mArticlelistData:Ljava/util/ArrayList;

    invoke-direct {v2, p0, v3}, Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 51
    invoke-virtual {v1, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    goto :goto_0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 57
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;

    .line 59
    .local v0, "cignaLibraryArticleData":Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 60
    .local v1, "intent":Landroid/content/Intent;
    const/high16 v2, 0x24000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 61
    const-string v2, "extra_name_article_id"

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->getId()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 62
    const-string v2, "extra_name_sub_category_id"

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->getSubCategoryId()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 63
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleListActivity;->startActivity(Landroid/content/Intent;)V

    .line 64
    return-void
.end method

.method public validateIntentData()Z
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 68
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleListActivity;->mSelectedCategoryId:I

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleListActivity;->mSelectedSubCategoryId:I

    if-ne v0, v1, :cond_1

    .line 71
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleListActivity;->TAG:Ljava/lang/String;

    const-string v1, "Intent data is error"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    const/4 v0, 0x0

    .line 76
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
