.class Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$18;
.super Ljava/lang/Object;
.source "HeartrateSummaryFragmentNew.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->showErrorDialog(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)V
    .locals 0

    .prologue
    .line 1903
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$18;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onContentInitialization(Landroid/view/View;Landroid/app/Activity;Landroid/app/Dialog;Landroid/os/Bundle;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V
    .locals 3
    .param p1, "content"    # Landroid/view/View;
    .param p2, "parentActivity"    # Landroid/app/Activity;
    .param p3, "dialog"    # Landroid/app/Dialog;
    .param p4, "savedInstanceState"    # Landroid/os/Bundle;
    .param p5, "okButtonHandler"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;

    .prologue
    .line 1907
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$18;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    # setter for: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mErrorView:Landroid/view/View;
    invoke-static {v0, p1}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$4602(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;Landroid/view/View;)Landroid/view/View;

    .line 1908
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$18;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$18;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mErrorView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$4600(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Landroid/view/View;

    move-result-object v0

    const v2, 0x7f080555

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    # setter for: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mAnimationViewErr0:Landroid/widget/ImageView;
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$4702(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;Landroid/widget/ImageView;)Landroid/widget/ImageView;

    .line 1909
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$18;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$18;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mErrorView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$4600(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Landroid/view/View;

    move-result-object v0

    const v2, 0x7f080556

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    # setter for: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mAnimationViewErr1:Landroid/widget/ImageView;
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$4802(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;Landroid/widget/ImageView;)Landroid/widget/ImageView;

    .line 1910
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$18;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$18;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mErrorView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$4600(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Landroid/view/View;

    move-result-object v0

    const v2, 0x7f080551

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    # setter for: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mErrorDialogMsg1:Landroid/widget/TextView;
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$4902(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 1911
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$18;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$18;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mErrorView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$4600(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Landroid/view/View;

    move-result-object v0

    const v2, 0x7f080552

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    # setter for: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mErrorDialogMsg2:Landroid/widget/TextView;
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$5002(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 1912
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$18;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$18;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mErrorView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$4600(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Landroid/view/View;

    move-result-object v0

    const v2, 0x7f080553

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    # setter for: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mErrorDialogMsg3:Landroid/widget/TextView;
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$5102(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 1913
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$18;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$18;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mErrorView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$4600(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Landroid/view/View;

    move-result-object v0

    const v2, 0x7f080554

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    # setter for: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mErrorDialogMsg4:Landroid/widget/TextView;
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$5202(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 1915
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$18;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    # invokes: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->initDialogAnimation()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$5300(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)V

    .line 1916
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$18;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->startDialogAnimation()V

    .line 1917
    return-void
.end method
