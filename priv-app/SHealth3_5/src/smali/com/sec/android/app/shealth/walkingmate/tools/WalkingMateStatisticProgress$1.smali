.class Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress$1;
.super Ljava/lang/Object;
.source "WalkingMateStatisticProgress.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->AnimationStart()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;)V
    .locals 0

    .prologue
    .line 153
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    .prologue
    const/high16 v10, 0x3f800000    # 1.0f

    const-wide/16 v8, 0x64

    .line 156
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->syncObj:Ljava/lang/Object;
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->access$000(Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;)Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    .line 157
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->maxValue:J
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->access$100(Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;)J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v2, v4, v6

    if-lez v2, :cond_5

    .line 158
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->ValueYou:J
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->access$200(Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;)J

    move-result-wide v4

    mul-long/2addr v4, v8

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->maxValue:J
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->access$100(Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;)J

    move-result-wide v6

    div-long/2addr v4, v6

    long-to-float v0, v4

    .line 159
    .local v0, "p1":F
    cmpg-float v2, v0, v10

    if-gez v2, :cond_2

    .line 160
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;

    const-wide/16 v4, 0x1

    # setter for: Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->mStartValueYou:J
    invoke-static {v2, v4, v5}, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->access$302(Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;J)J

    .line 165
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->Valueaverage:J
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->access$400(Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;)J

    move-result-wide v4

    mul-long/2addr v4, v8

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->maxValue:J
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->access$100(Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;)J

    move-result-wide v6

    div-long/2addr v4, v6

    long-to-float v1, v4

    .line 166
    .local v1, "p2":F
    cmpg-float v2, v1, v10

    if-gez v2, :cond_3

    .line 167
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;

    const-wide/16 v4, 0x1

    # setter for: Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->mStartValueaverage:J
    invoke-static {v2, v4, v5}, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->access$502(Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;J)J

    .line 172
    :cond_1
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;

    const-wide/16 v4, 0x1

    # += operator for: Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->mStartValueBest:J
    invoke-static {v2, v4, v5}, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->access$614(Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;J)J

    .line 173
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->mStartValueBest:J
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->access$600(Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;)J

    move-result-wide v4

    cmp-long v2, v4, v8

    if-lez v2, :cond_4

    .line 174
    monitor-exit v3

    .line 182
    .end local v0    # "p1":F
    .end local v1    # "p2":F
    :goto_2
    return-void

    .line 161
    .restart local v0    # "p1":F
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->mStartValueYou:J
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->access$300(Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;)J

    move-result-wide v4

    long-to-float v2, v4

    cmpg-float v2, v2, v0

    if-gez v2, :cond_0

    .line 162
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;

    const-wide/16 v4, 0x1

    # += operator for: Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->mStartValueYou:J
    invoke-static {v2, v4, v5}, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->access$314(Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;J)J

    goto :goto_0

    .line 181
    .end local v0    # "p1":F
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 168
    .restart local v0    # "p1":F
    .restart local v1    # "p2":F
    :cond_3
    :try_start_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->mStartValueaverage:J
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->access$500(Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;)J

    move-result-wide v4

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v2

    int-to-long v6, v2

    cmp-long v2, v4, v6

    if-gez v2, :cond_1

    .line 169
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;

    const-wide/16 v4, 0x1

    # += operator for: Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->mStartValueaverage:J
    invoke-static {v2, v4, v5}, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->access$514(Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;J)J

    goto :goto_1

    .line 176
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->mYouProgress:Landroid/widget/ProgressBar;
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->access$700(Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;)Landroid/widget/ProgressBar;

    move-result-object v2

    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->mStartValueYou:J
    invoke-static {v4}, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->access$300(Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;)J

    move-result-wide v4

    long-to-int v4, v4

    invoke-virtual {v2, v4}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 177
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->mAverageProgress:Landroid/widget/ProgressBar;
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->access$800(Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;)Landroid/widget/ProgressBar;

    move-result-object v2

    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->mStartValueaverage:J
    invoke-static {v4}, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->access$500(Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;)J

    move-result-wide v4

    long-to-int v4, v4

    invoke-virtual {v2, v4}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 178
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->mBestProgress:Landroid/widget/ProgressBar;
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->access$900(Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;)Landroid/widget/ProgressBar;

    move-result-object v2

    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->mStartValueBest:J
    invoke-static {v4}, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->access$600(Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;)J

    move-result-wide v4

    long-to-int v4, v4

    invoke-virtual {v2, v4}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 179
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;

    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->mRunnable:Ljava/lang/Runnable;
    invoke-static {v4}, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->access$1000(Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;)Ljava/lang/Runnable;

    move-result-object v4

    const-wide/16 v5, 0xa

    invoke-virtual {v2, v4, v5, v6}, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 181
    .end local v0    # "p1":F
    .end local v1    # "p2":F
    :cond_5
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2
.end method
