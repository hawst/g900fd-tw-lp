.class public Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewSizeChangedObserver;
.super Landroid/view/View;
.source "ViewSizeChangedObserver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewSizeChangedObserver$OnResizeObserverListener;
    }
.end annotation


# instance fields
.field mResizeListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewSizeChangedObserver$OnResizeObserverListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 11
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 13
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "arg1"    # Landroid/util/AttributeSet;
    .param p3, "arg2"    # I

    .prologue
    .line 15
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 17
    return-void
.end method


# virtual methods
.method public SetOnResizeListener(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewSizeChangedObserver$OnResizeObserverListener;)V
    .locals 0
    .param p1, "litener"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewSizeChangedObserver$OnResizeObserverListener;

    .prologue
    .line 25
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewSizeChangedObserver;->mResizeListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewSizeChangedObserver$OnResizeObserverListener;

    .line 26
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 1
    .param p1, "xNew"    # I
    .param p2, "yNew"    # I
    .param p3, "xOld"    # I
    .param p4, "yOld"    # I

    .prologue
    .line 31
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    .line 32
    if-lez p1, :cond_0

    if-lez p2, :cond_0

    .line 34
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewSizeChangedObserver;->mResizeListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewSizeChangedObserver$OnResizeObserverListener;

    if-eqz v0, :cond_0

    .line 36
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewSizeChangedObserver;->mResizeListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewSizeChangedObserver$OnResizeObserverListener;

    invoke-interface {v0, p1, p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewSizeChangedObserver$OnResizeObserverListener;->onLayoutSizeChanged(II)V

    .line 39
    :cond_0
    return-void
.end method
