.class Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment$8;
.super Ljava/lang/Object;
.source "AMapExerciseProDetailsMapFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/OnSaveChosenItemListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->getOnSaveChosenItemListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/OnSaveChosenItemListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;)V
    .locals 0

    .prologue
    .line 806
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment$8;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSave(I)V
    .locals 6
    .param p1, "index"    # I

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 809
    add-int/lit8 v2, p1, 0x1

    sput v2, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMapMode:I

    .line 811
    if-nez p1, :cond_0

    .line 812
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment$8;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;

    iget-object v2, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMap:Lcom/amap/api/maps2d/AMap;

    invoke-virtual {v2, v3}, Lcom/amap/api/maps2d/AMap;->setMapType(I)V

    .line 813
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment$8;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;

    iget-object v2, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMap:Lcom/amap/api/maps2d/AMap;

    invoke-virtual {v2, v4}, Lcom/amap/api/maps2d/AMap;->setTrafficEnabled(Z)V

    .line 826
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment$8;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-string/jumbo v3, "mapMode"

    invoke-virtual {v2, v3, v4}, Landroid/support/v4/app/FragmentActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 827
    .local v1, "viewMode":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 828
    .local v0, "ed":Landroid/content/SharedPreferences$Editor;
    const-string/jumbo v2, "mapMode"

    sget v3, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMapMode:I

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 829
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 830
    return-void

    .line 814
    .end local v0    # "ed":Landroid/content/SharedPreferences$Editor;
    .end local v1    # "viewMode":Landroid/content/SharedPreferences;
    :cond_0
    if-ne p1, v3, :cond_1

    .line 815
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment$8;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;

    iget-object v2, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMap:Lcom/amap/api/maps2d/AMap;

    invoke-virtual {v2, v5}, Lcom/amap/api/maps2d/AMap;->setMapType(I)V

    .line 816
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment$8;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;

    iget-object v2, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMap:Lcom/amap/api/maps2d/AMap;

    invoke-virtual {v2, v4}, Lcom/amap/api/maps2d/AMap;->setTrafficEnabled(Z)V

    goto :goto_0

    .line 817
    :cond_1
    if-ne p1, v5, :cond_2

    .line 818
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment$8;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;

    iget-object v2, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMap:Lcom/amap/api/maps2d/AMap;

    invoke-virtual {v2, v3}, Lcom/amap/api/maps2d/AMap;->setMapType(I)V

    .line 819
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment$8;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;

    iget-object v2, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMap:Lcom/amap/api/maps2d/AMap;

    invoke-virtual {v2, v3}, Lcom/amap/api/maps2d/AMap;->setTrafficEnabled(Z)V

    goto :goto_0

    .line 821
    :cond_2
    const/4 v2, 0x3

    sput v2, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMapMode:I

    .line 822
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment$8;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;

    iget-object v2, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMap:Lcom/amap/api/maps2d/AMap;

    invoke-virtual {v2, v3}, Lcom/amap/api/maps2d/AMap;->setMapType(I)V

    .line 823
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment$8;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;

    iget-object v2, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMap:Lcom/amap/api/maps2d/AMap;

    invoke-virtual {v2, v3}, Lcom/amap/api/maps2d/AMap;->setTrafficEnabled(Z)V

    goto :goto_0
.end method
