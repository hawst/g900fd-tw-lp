.class public Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity;
.super Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;
.source "TwoButtonMessageActivity.java"


# instance fields
.field private TAG:Ljava/lang/String;

.field private mGoalId:I

.field private mMessageID:I

.field private mMissionId:I

.field private mMissionSequenceId:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 20
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;-><init>()V

    .line 21
    const-string v0, "TwoButtonMessageActivity"

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity;->TAG:Ljava/lang/String;

    .line 22
    iput v1, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity;->mMessageID:I

    .line 23
    iput v1, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity;->mGoalId:I

    .line 24
    iput v1, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity;->mMissionId:I

    .line 25
    iput v1, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity;->mMissionSequenceId:I

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity;

    .prologue
    .line 20
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity;->mGoalId:I

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity;

    .prologue
    .line 20
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity;->mMissionId:I

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity;

    .prologue
    .line 20
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity;->mMissionSequenceId:I

    return v0
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 4

    .prologue
    const v2, 0x7f090351

    const v3, 0x7f090350

    .line 50
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->customizeActionBar()V

    .line 53
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(II)V

    .line 54
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getActionBarLayout()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f080304

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const v2, 0x7f09020b

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 57
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    .line 58
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 29
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 32
    const v0, 0x7f030072

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity;->setContentView(I)V

    .line 34
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 35
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_NAME_MESSAGE_ID"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity;->mMessageID:I

    .line 36
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_NAME_GOAL_ID"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity;->mGoalId:I

    .line 37
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_NAME_MISSION_ID"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity;->mMissionId:I

    .line 38
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_NAME_MISSION_SEQUENCE_ID"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity;->mMissionSequenceId:I

    .line 40
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "TwoButtonMessageActivity mMessageID : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity;->mMessageID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 41
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "mGoalId : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity;->mGoalId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 42
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "mMissionId : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity;->mMissionId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 43
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "mMissionSequenceId : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity;->mMissionSequenceId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 45
    :cond_0
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity;->mMessageID:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity;->showView(I)V

    .line 46
    return-void
.end method

.method public showView(I)V
    .locals 9
    .param p1, "message_id"    # I

    .prologue
    const v8, 0x7f090348

    .line 62
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getEngagingMessage : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getEngagingMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    const v5, 0x7f08021b

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 64
    .local v1, "heading":Landroid/widget/TextView;
    const v5, 0x7f08021c

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 65
    .local v0, "description":Landroid/widget/TextView;
    const v5, 0x7f08021d

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    .line 67
    .local v4, "two_btn_view":Landroid/widget/LinearLayout;
    packed-switch p1, :pswitch_data_0

    .line 89
    :goto_0
    :pswitch_0
    const v5, 0x7f08021e

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    .line 90
    .local v2, "leftBtn":Landroid/widget/Button;
    new-instance v5, Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity$1;

    invoke-direct {v5, p0}, Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity$1;-><init>(Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity;)V

    invoke-virtual {v2, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 102
    const v5, 0x7f08021f

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    .line 103
    .local v3, "rightBtn":Landroid/widget/Button;
    new-instance v5, Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity$2;

    invoke-direct {v5, p0}, Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity$2;-><init>(Lcom/sec/android/app/shealth/cignacoach/engaging/TwoButtonMessageActivity;)V

    invoke-virtual {v3, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 114
    return-void

    .line 69
    .end local v2    # "leftBtn":Landroid/widget/Button;
    .end local v3    # "rightBtn":Landroid/widget/Button;
    :pswitch_1
    const v5, 0x7f090349

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(I)V

    .line 70
    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 73
    :pswitch_2
    const v5, 0x7f09034a

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(I)V

    .line 74
    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 77
    :pswitch_3
    const v5, 0x7f09034b

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(I)V

    .line 78
    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 81
    :pswitch_4
    const v5, 0x7f09034c

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(I)V

    .line 82
    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 67
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
