.class public Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;
.super Ljava/lang/Object;


# instance fields
.field private isConnected:Z

.field private mDeviceConnectivityType:I

.field private mDeviceDataType:I

.field private mDeviceId:Ljava/lang/String;

.field private mDeviceName:Ljava/lang/String;

.field private mDeviceType:I

.field private mFromPairedDB:Z

.field private mIsPaired:Z

.field private mShealthSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;


# direct methods
.method public constructor <init>(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;Z)V
    .locals 7

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getType()I

    move-result v2

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getDataType()Ljava/util/List;

    move-result-object v0

    const/4 v3, 0x0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getConnectivityType()I

    move-result v4

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getName()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;-><init>(Ljava/lang/String;IIILjava/lang/String;Z)V

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->mShealthSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IIILjava/lang/String;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->mDeviceId:Ljava/lang/String;

    iput p2, p0, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->mDeviceType:I

    iput p3, p0, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->mDeviceDataType:I

    iput p4, p0, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->mDeviceConnectivityType:I

    iput-object p5, p0, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->mDeviceName:Ljava/lang/String;

    iput-boolean p6, p0, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->mIsPaired:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IIILjava/lang/String;ZLcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->mDeviceId:Ljava/lang/String;

    iput p2, p0, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->mDeviceType:I

    iput p3, p0, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->mDeviceDataType:I

    iput p4, p0, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->mDeviceConnectivityType:I

    iput-object p5, p0, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->mDeviceName:Ljava/lang/String;

    iput-boolean p6, p0, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->mIsPaired:Z

    iput-object p7, p0, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->mShealthSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    return-void
.end method


# virtual methods
.method public getDeviceConnectivityType()I
    .locals 1

    iget v0, p0, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->mDeviceConnectivityType:I

    return v0
.end method

.method public getDeviceDataType()I
    .locals 1

    iget v0, p0, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->mDeviceDataType:I

    return v0
.end method

.method public getDeviceId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->mDeviceId:Ljava/lang/String;

    return-object v0
.end method

.method public getDeviceName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->mDeviceName:Ljava/lang/String;

    return-object v0
.end method

.method public getDeviceType()I
    .locals 1

    iget v0, p0, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->mDeviceType:I

    return v0
.end method

.method public getShealthSensorDevice()Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->mShealthSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    return-object v0
.end method

.method public isConnected()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->isConnected:Z

    return v0
.end method

.method public isFromPairedDB()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->mFromPairedDB:Z

    return v0
.end method

.method public isIsPaired()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->mIsPaired:Z

    return v0
.end method

.method public setDeviceId(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->mDeviceId:Ljava/lang/String;

    return-void
.end method

.method public setDeviceName(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->mShealthSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->mShealthSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->rename(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceWrongStatusException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_4

    :cond_0
    :goto_0
    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->mDeviceName:Ljava/lang/String;

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceWrongStatusException;->printStackTrace()V

    goto :goto_0

    :catch_3
    move-exception v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto :goto_0

    :catch_4
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0
.end method

.method public setFromPairedDB(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->mFromPairedDB:Z

    return-void
.end method

.method public setIsConnected(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->isConnected:Z

    return-void
.end method

.method public setIsPaired(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->mIsPaired:Z

    return-void
.end method

.method public setShealthSensorDevice(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->mShealthSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    return-void
.end method
