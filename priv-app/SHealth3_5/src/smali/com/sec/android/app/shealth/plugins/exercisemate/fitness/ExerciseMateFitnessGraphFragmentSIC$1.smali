.class Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC$1;
.super Ljava/lang/Object;
.source "ExerciseMateFitnessGraphFragmentSIC.java"

# interfaces
.implements Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;)V
    .locals 0

    .prologue
    .line 238
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public OnGraphData(Ljava/util/ArrayList;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/chart/view/SchartHandlerData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "pointData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/chart/view/SchartHandlerData;>;"
    const/4 v4, 0x1

    const-wide/16 v6, 0x0

    const/4 v5, 0x0

    .line 272
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->access$000(Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;)Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;->getChartStyle()Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->getHandlerItemTextVisible()Z

    move-result v1

    if-nez v1, :cond_0

    .line 273
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->access$000(Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;)Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;->getChartStyle()Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    move-result-object v0

    .line 274
    .local v0, "tempStyle":Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemVisible(Z)V

    .line 275
    const/high16 v1, 0x41f80000    # 31.0f

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;

    iget v2, v2, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->density:F

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemWidth(F)V

    .line 276
    const/high16 v1, 0x42480000    # 50.0f

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;

    iget v2, v2, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->density:F

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemHeight(F)V

    .line 277
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0057

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x3f000000    # 0.5f

    mul-float/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0058

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemOffset(F)V

    .line 278
    const/high16 v1, 0x41a00000    # 20.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemTextOffset(F)V

    .line 279
    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemTextVisible(Z)V

    .line 281
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->access$000(Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;)Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;->setChartStyle(Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;)V

    .line 283
    .end local v0    # "tempStyle":Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    :cond_0
    if-eqz p1, :cond_3

    .line 285
    const-string v1, "Handler Data"

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 286
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;

    iget-object v2, v1, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->exercisemateFitnessInformationArea:Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;

    new-instance v3, Ljava/lang/Long;

    invoke-virtual {p1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesXValue()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v1}, Ljava/lang/Long;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    long-to-double v3, v3

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->access$100(Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;)Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    move-result-object v1

    invoke-virtual {v2, v3, v4, v1}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->setDate(DLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    .line 288
    invoke-virtual {p1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesYValueList()Ljava/util/Vector;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v1

    cmpl-double v1, v1, v6

    if-ltz v1, :cond_2

    .line 289
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;

    iget-object v2, v1, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->exercisemateFitnessInformationArea:Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;

    invoke-virtual {p1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesYValueList()Ljava/util/Vector;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->getFitnessData(D)Ljava/lang/String;
    invoke-static {v3, v4, v5}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->access$200(Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;D)Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->getBubbleAreaUnit()Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->access$300(Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->setSecondInformation(Ljava/lang/String;Ljava/lang/String;)V

    .line 293
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;

    iget-object v1, v1, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->exercisemateFitnessInformationArea:Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->refreshInformationAreaView()V

    .line 300
    :cond_1
    :goto_1
    return-void

    .line 291
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;

    iget-object v1, v1, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->exercisemateFitnessInformationArea:Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->getFitnessData(D)Ljava/lang/String;
    invoke-static {v2, v6, v7}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->access$200(Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;D)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->getBubbleAreaUnit()Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->access$300(Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->setSecondInformation(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 295
    :cond_3
    if-nez p1, :cond_1

    .line 297
    const-string v1, "Handler Data"

    const-string v2, "NULL"

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public OnReleaseTimeOut()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 259
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->access$000(Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;)Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;->getChartStyle()Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    move-result-object v0

    .line 261
    .local v0, "tempStyle":Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemWidth(F)V

    .line 262
    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemHeight(F)V

    .line 263
    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemOffset(F)V

    .line 264
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemTextVisible(Z)V

    .line 266
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->access$000(Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;)Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;->setChartStyle(Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;)V

    .line 267
    return-void
.end method

.method public OnVisible()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 243
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->access$000(Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;)Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;->getChartStyle()Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    move-result-object v0

    .line 245
    .local v0, "tempStyle":Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemVisible(Z)V

    .line 246
    const/high16 v1, 0x41f80000    # 31.0f

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;

    iget v2, v2, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->density:F

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemWidth(F)V

    .line 247
    const/high16 v1, 0x42480000    # 50.0f

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;

    iget v2, v2, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->density:F

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemHeight(F)V

    .line 248
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0057

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x3f000000    # 0.5f

    mul-float/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0058

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemOffset(F)V

    .line 249
    const/high16 v1, 0x41a00000    # 20.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemTextOffset(F)V

    .line 250
    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemTextVisible(Z)V

    .line 252
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->access$000(Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;)Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;->setChartStyle(Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;)V

    .line 254
    return-void
.end method
