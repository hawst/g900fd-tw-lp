.class public Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessGoalWarningFragment;
.super Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;
.source "LifeStyleReassessGoalWarningFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 52
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 73
    :goto_0
    return-void

    .line 54
    :pswitch_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 55
    .local v1, "remainCategory":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;>;"
    sget-object v2, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->FOOD:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 56
    sget-object v2, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->EXERCISE:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 57
    sget-object v2, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->SLEEP:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 58
    sget-object v2, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->STRESS:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 59
    sget-object v2, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->WEIGHT:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 60
    invoke-static {v1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 62
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessGoalWarningFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 63
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "intent_lifestyle_categorys"

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v3, v3, [Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 64
    const-string v2, "EXTRA_NAME_REASSESSMENT_EXIST_CURRENT_GOAL"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 65
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessGoalWarningFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 69
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "remainCategory":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;>;"
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessGoalWarningFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->finish()V

    goto :goto_0

    .line 52
    nop

    :pswitch_data_0
    .packed-switch 0x7f080236
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 26
    const v4, 0x7f030075

    const/4 v5, 0x0

    invoke-virtual {p1, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 30
    .local v3, "rootView":Landroid/view/View;
    const v4, 0x7f080236

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 31
    .local v1, "lessThanNo":Landroid/widget/Button;
    const v4, 0x7f080237

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    .line 35
    .local v2, "lessThanYes":Landroid/widget/Button;
    const v4, 0x7f090c89

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setText(I)V

    .line 36
    const v4, 0x7f090047

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setText(I)V

    .line 39
    const v4, 0x7f080235

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 42
    .local v0, "goalWarningDescription":Landroid/widget/TextView;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessGoalWarningFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;->getReassessGoalWarningDescription()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 44
    invoke-virtual {v1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 45
    invoke-virtual {v2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 47
    return-object v3
.end method
