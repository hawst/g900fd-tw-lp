.class public Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter$OnCalendarDayClickedListener;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "OnCalendarDayClickedListener"
.end annotation


# instance fields
.field private final containsMeasurements:Z

.field private final dayWithMeasure:Ljava/lang/Long;

.field final synthetic this$0:Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;Ljava/lang/Long;Z)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter$OnCalendarDayClickedListener;->this$0:Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter$OnCalendarDayClickedListener;->dayWithMeasure:Ljava/lang/Long;

    iput-boolean p3, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter$OnCalendarDayClickedListener;->containsMeasurements:Z

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter$OnCalendarDayClickedListener;->this$0:Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter$OnCalendarDayClickedListener;->dayWithMeasure:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    iget-boolean v3, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter$OnCalendarDayClickedListener;->containsMeasurements:Z

    # invokes: Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;->onCalendarDayClicked(JZ)V
    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;->access$000(Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;JZ)V

    return-void
.end method
