.class public Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;
.super Landroid/widget/LinearLayout;
.source "PortionSizeSliderView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView$IPortionSizeSliderProvider;
    }
.end annotation


# static fields
.field private static final DEFAULT_MAX_VALUE:I = 0x4

.field private static final DEFAULT_MIN_VALUE:I = 0x0

.field private static final DEFAULT_UNIT:I = 0x1d4c1

.field private static final HORIZONTAL_DELTA_FOR_CORRECT_HANDLER_POSITION:I = 0x2

.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private mCurrentPortionValue:F

.field private mHandlerView:Landroid/view/View;

.field private mMainContainerWidth:I

.field private mMaxValue:I

.field private mMinValue:I

.field private mOnQuantityChangedListener:Lcom/sec/android/app/shealth/food/mealinput/portionsize/OnQuantityChangedListener;

.field private mScaleView:Landroid/widget/LinearLayout;

.field private mScaleViewWidth:I

.field private mStartHandlerWidth:I

.field private mTextViewFoodName:Landroid/widget/TextView;

.field private mTextViewMaxValue:Landroid/widget/TextView;

.field private mTextViewMinValue:Landroid/widget/TextView;

.field private mTextViewPortionValue:Landroid/widget/TextView;

.field private onTouchHandlerListener:Landroid/view/View$OnTouchListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->getLogTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 57
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 49
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mMinValue:I

    .line 50
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mMaxValue:I

    .line 131
    new-instance v0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView$2;-><init>(Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->onTouchHandlerListener:Landroid/view/View$OnTouchListener;

    .line 58
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->init()V

    .line 59
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 49
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mMinValue:I

    .line 50
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mMaxValue:I

    .line 131
    new-instance v0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView$2;-><init>(Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->onTouchHandlerListener:Landroid/view/View$OnTouchListener;

    .line 64
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->init()V

    .line 65
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 69
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 49
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mMinValue:I

    .line 50
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mMaxValue:I

    .line 131
    new-instance v0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView$2;-><init>(Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->onTouchHandlerListener:Landroid/view/View$OnTouchListener;

    .line 70
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->init()V

    .line 71
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;

    .prologue
    .line 33
    iget v0, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mMainContainerWidth:I

    return v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;
    .param p1, "x1"    # I

    .prologue
    .line 33
    iput p1, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mMainContainerWidth:I

    return p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;

    .prologue
    .line 33
    iget v0, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mScaleViewWidth:I

    return v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;
    .param p1, "x1"    # I

    .prologue
    .line 33
    iput p1, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mScaleViewWidth:I

    return p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mScaleView:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;

    .prologue
    .line 33
    iget v0, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mStartHandlerWidth:I

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mHandlerView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->moveHandlerToCurrentValue()V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;

    .prologue
    .line 33
    iget v0, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mCurrentPortionValue:F

    return v0
.end method

.method static synthetic access$602(Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;
    .param p1, "x1"    # F

    .prologue
    .line 33
    iput p1, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mCurrentPortionValue:F

    return p1
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;F)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;
    .param p1, "x1"    # F

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->calculatePortionValue(F)F

    move-result v0

    return v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mTextViewPortionValue:Landroid/widget/TextView;

    return-object v0
.end method

.method private calculatePortionValue(F)F
    .locals 4
    .param p1, "x"    # F

    .prologue
    const/high16 v3, 0x41200000    # 10.0f

    .line 178
    iget v1, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mMaxValue:I

    int-to-float v1, v1

    mul-float/2addr v1, p1

    iget v2, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mScaleViewWidth:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    mul-float/2addr v1, v3

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    int-to-float v1, v1

    div-float v0, v1, v3

    .line 179
    .local v0, "currentPortionValue":F
    iget v1, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mMinValue:I

    int-to-float v1, v1

    cmpg-float v1, v0, v1

    if-gtz v1, :cond_1

    .line 180
    iget v1, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mMinValue:I

    int-to-float v0, v1

    .line 184
    :cond_0
    :goto_0
    return v0

    .line 181
    :cond_1
    iget v1, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mMaxValue:I

    int-to-float v1, v1

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_0

    .line 182
    iget v1, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mMaxValue:I

    int-to-float v0, v1

    goto :goto_0
.end method

.method private init()V
    .locals 4

    .prologue
    .line 103
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f03010f

    invoke-static {v2, v3, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 104
    .local v1, "mainContainer":Landroid/view/View;
    const v2, 0x7f080476

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mScaleView:Landroid/widget/LinearLayout;

    .line 105
    const v2, 0x7f080479

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 107
    .local v0, "handlerContainer":Landroid/widget/LinearLayout;
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mScaleView:Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->onTouchHandlerListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 108
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f03010d

    invoke-static {v2, v3, v0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mHandlerView:Landroid/view/View;

    .line 109
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->onTouchHandlerListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 110
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mHandlerView:Landroid/view/View;

    const v3, 0x7f080470

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iget v2, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    iput v2, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mStartHandlerWidth:I

    .line 111
    const v2, 0x7f080475

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mTextViewPortionValue:Landroid/widget/TextView;

    .line 112
    const v2, 0x7f080477

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mTextViewMinValue:Landroid/widget/TextView;

    .line 113
    const v2, 0x7f080478

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mTextViewMaxValue:Landroid/widget/TextView;

    .line 114
    const v2, 0x7f080474

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mTextViewFoodName:Landroid/widget/TextView;

    .line 116
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v2

    new-instance v3, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView$1;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView$1;-><init>(Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;)V

    invoke-virtual {v2, v3}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 129
    return-void
.end method

.method private moveHandlerToCurrentValue()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 163
    iget v2, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mCurrentPortionValue:F

    iget v3, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mScaleViewWidth:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mMaxValue:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    float-to-int v2, v2

    add-int/lit8 v1, v2, -0x2

    .line 165
    .local v1, "paddingLeft":I
    iget v2, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mCurrentPortionValue:F

    iget v3, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mMinValue:I

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-nez v2, :cond_2

    .line 166
    const/4 v1, 0x0

    .line 170
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mScaleView:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getPaddingLeft()I

    move-result v2

    iget v3, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mStartHandlerWidth:I

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    int-to-float v0, v2

    .line 171
    .local v0, "delta":F
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mHandlerView:Landroid/view/View;

    int-to-float v3, v1

    add-float/2addr v3, v0

    float-to-int v3, v3

    invoke-virtual {v2, v3, v4, v4, v4}, Landroid/view/View;->setPadding(IIII)V

    .line 172
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mOnQuantityChangedListener:Lcom/sec/android/app/shealth/food/mealinput/portionsize/OnQuantityChangedListener;

    if-eqz v2, :cond_1

    .line 173
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mOnQuantityChangedListener:Lcom/sec/android/app/shealth/food/mealinput/portionsize/OnQuantityChangedListener;

    iget v3, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mCurrentPortionValue:F

    const v4, 0x1d4c1

    invoke-interface {v2, v3, v4}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/OnQuantityChangedListener;->onQuantityChanged(FI)V

    .line 175
    :cond_1
    return-void

    .line 167
    .end local v0    # "delta":F
    :cond_2
    iget v2, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mCurrentPortionValue:F

    iget v3, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mMaxValue:I

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    .line 168
    iget v1, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mScaleViewWidth:I

    goto :goto_0
.end method


# virtual methods
.method public setOnQuantityChangedListener(Lcom/sec/android/app/shealth/food/mealinput/portionsize/OnQuantityChangedListener;)V
    .locals 0
    .param p1, "mOnQuantityChangedListener"    # Lcom/sec/android/app/shealth/food/mealinput/portionsize/OnQuantityChangedListener;

    .prologue
    .line 78
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mOnQuantityChangedListener:Lcom/sec/android/app/shealth/food/mealinput/portionsize/OnQuantityChangedListener;

    .line 79
    return-void
.end method

.method public setPortionSizeSliderProvider(Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView$IPortionSizeSliderProvider;)V
    .locals 3
    .param p1, "portionSizeSliderProvider"    # Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView$IPortionSizeSliderProvider;

    .prologue
    .line 85
    if-nez p1, :cond_0

    .line 86
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "portionSizeSliderProvider can\'t be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 88
    :cond_0
    invoke-interface {p1}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView$IPortionSizeSliderProvider;->getMaxValue()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mMaxValue:I

    .line 90
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mTextViewMaxValue:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p1}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView$IPortionSizeSliderProvider;->getMaxValue()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView$IPortionSizeSliderProvider;->getUnitName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 92
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mTextViewMinValue:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mMinValue:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView$IPortionSizeSliderProvider;->getUnitName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 94
    invoke-interface {p1}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView$IPortionSizeSliderProvider;->getInitialPortionValue()F

    move-result v0

    iget v1, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mMaxValue:I

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    iget v0, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mMaxValue:I

    int-to-float v0, v0

    :goto_0
    iput v0, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mCurrentPortionValue:F

    .line 96
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mTextViewPortionValue:Landroid/widget/TextView;

    iget v1, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mCurrentPortionValue:F

    invoke-static {v1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 97
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mTextViewFoodName:Landroid/widget/TextView;

    invoke-interface {p1}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView$IPortionSizeSliderProvider;->getFoodName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 99
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->moveHandlerToCurrentValue()V

    .line 100
    return-void

    .line 94
    :cond_1
    invoke-interface {p1}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView$IPortionSizeSliderProvider;->getInitialPortionValue()F

    move-result v0

    goto :goto_0
.end method
