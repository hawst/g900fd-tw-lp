.class public Lcom/sec/android/app/shealth/backup/server/RestoreManager;
.super Ljava/lang/Object;
.source "RestoreManager.java"

# interfaces
.implements Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener;
.implements Lcom/sec/android/service/health/connectionmanager2/IOnResponseListener;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "NewApi"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/backup/server/RestoreManager$SyncStage;
    }
.end annotation


# static fields
.field private static final CONN_TIMEOUT:I = 0x2710

.field private static final SO_TIMEOUT:I = 0x7530

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final CALL_RESTORE_MGR:I

.field private final CMGR_HTTPS:Ljava/lang/String;

.field private final DOWNLOAD_ID_DBRESTORE:I

.field private final DOWNLOAD_ID_IMAGERESTORE:I

.field private final DOWNLOAD_ID_PREFERENCERESTORE:I

.field private final REQUEST_ID_CHECKSTATUS:I

.field private final REQUEST_ID_DBRESTORE:I

.field private final REQUEST_ID_IMAGERESTORE:I

.field private final REQUEST_ID_PREFERENCERESTORE:I

.field private final REQUEST_ID_TRANSACTION:I

.field private final SERVER_URL_CHECKSTATUS:Ljava/lang/String;

.field private final SERVER_URL_DBRESTORE:Ljava/lang/String;

.field private final SERVER_URL_IMAGERESTORE_ALL:Ljava/lang/String;

.field private final SERVER_URL_PREFERENCERESTORE:Ljava/lang/String;

.field private final SERVER_URL_TRANSACTION:Ljava/lang/String;

.field private currentRequestId:J

.field private handler:Landroid/os/Handler;

.field private isCheckingDataToRestore:Z

.field private isRestoreStarted:Z

.field private mAccountID:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mCurrentStage:Lcom/sec/android/app/shealth/backup/server/RestoreManager$SyncStage;

.field private mDeviceID:Ljava/lang/String;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "InlinedApi"
        }
    .end annotation
.end field

.field private mFileSizeDBRestore:J

.field private mFileSizePrefRestore:J

.field private mImageCounter:I

.field private mRestoreListener:Lcom/sec/android/app/shealth/backup/server/IRestoreListener;

.field private mTotalSizeRestore:J

.field private mTransactionID:Ljava/lang/String;

.field private mTryToRenewToken:Z

.field private mUserToken:Ljava/lang/String;

.field private resCheckStatus:Lcom/sec/android/app/shealth/backup/server/data/ResponseCheckStatus;

.field private resImage:Lcom/sec/android/app/shealth/backup/server/data/ResponseImage;

.field private totalDownloaded:J

.field private wl:Landroid/os/PowerManager$WakeLock;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 78
    const-class v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/16 v1, 0x6b

    const-wide/16 v2, 0x0

    .line 114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    sget-object v0, Landroid/os/Build;->SERIAL:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mDeviceID:Ljava/lang/String;

    .line 51
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mTransactionID:Ljava/lang/String;

    .line 53
    const-string v0, "ServerMgrHttps"

    iput-object v0, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->CMGR_HTTPS:Ljava/lang/String;

    .line 57
    const-string/jumbo v0, "v2/shealth/phr/txmanager"

    iput-object v0, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->SERVER_URL_TRANSACTION:Ljava/lang/String;

    .line 58
    const-string/jumbo v0, "v2/shealth/phr/dbrestore"

    iput-object v0, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->SERVER_URL_DBRESTORE:Ljava/lang/String;

    .line 59
    const-string/jumbo v0, "v2/shealth/phr/cfgrestore"

    iput-object v0, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->SERVER_URL_PREFERENCERESTORE:Ljava/lang/String;

    .line 60
    const-string/jumbo v0, "v2/shealth/phr/chkstatus"

    iput-object v0, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->SERVER_URL_CHECKSTATUS:Ljava/lang/String;

    .line 62
    const-string/jumbo v0, "v2/shealth/phr/imgrestoreAll"

    iput-object v0, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->SERVER_URL_IMAGERESTORE_ALL:Ljava/lang/String;

    .line 67
    iput v1, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->REQUEST_ID_TRANSACTION:I

    .line 68
    const/16 v0, 0x6c

    iput v0, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->REQUEST_ID_CHECKSTATUS:I

    .line 69
    const/16 v0, 0x6d

    iput v0, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->REQUEST_ID_DBRESTORE:I

    .line 70
    const/16 v0, 0x6e

    iput v0, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->REQUEST_ID_PREFERENCERESTORE:I

    .line 71
    const/16 v0, 0x6f

    iput v0, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->REQUEST_ID_IMAGERESTORE:I

    .line 72
    const/16 v0, 0x70

    iput v0, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->DOWNLOAD_ID_DBRESTORE:I

    .line 73
    const/16 v0, 0x71

    iput v0, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->DOWNLOAD_ID_PREFERENCERESTORE:I

    .line 74
    const/16 v0, 0x72

    iput v0, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->DOWNLOAD_ID_IMAGERESTORE:I

    .line 76
    iput v1, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->CALL_RESTORE_MGR:I

    .line 92
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->currentRequestId:J

    .line 98
    iput-wide v2, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->totalDownloaded:J

    .line 99
    iput-wide v2, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mFileSizeDBRestore:J

    .line 100
    iput-wide v2, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mFileSizePrefRestore:J

    .line 101
    iput-wide v2, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mTotalSizeRestore:J

    .line 350
    new-instance v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/backup/server/RestoreManager$2;-><init>(Lcom/sec/android/app/shealth/backup/server/RestoreManager;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->handler:Landroid/os/Handler;

    .line 115
    iput-object p1, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mContext:Landroid/content/Context;

    .line 116
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/backup/server/RestoreManager;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/backup/server/RestoreManager;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mUserToken:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/shealth/backup/server/RestoreManager;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/backup/server/RestoreManager;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mUserToken:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/backup/server/RestoreManager;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/backup/server/RestoreManager;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mAccountID:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/shealth/backup/server/RestoreManager;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/backup/server/RestoreManager;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mAccountID:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/backup/server/RestoreManager;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/backup/server/RestoreManager;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/backup/server/RestoreManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/backup/server/RestoreManager;

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->isRestoreStarted:Z

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/backup/server/RestoreManager;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/backup/server/RestoreManager;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mDeviceID:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/backup/server/RestoreManager;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/backup/server/RestoreManager;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/backup/server/RestoreManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/backup/server/RestoreManager;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->restoreStopped()V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/backup/server/RestoreManager;)Lcom/sec/android/app/shealth/backup/server/IRestoreListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/backup/server/RestoreManager;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mRestoreListener:Lcom/sec/android/app/shealth/backup/server/IRestoreListener;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/backup/server/RestoreManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/backup/server/RestoreManager;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->startTransaction()V

    return-void
.end method

.method private cancelTransaction()V
    .locals 10

    .prologue
    const/4 v6, 0x0

    .line 452
    sget-object v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->TAG:Ljava/lang/String;

    const-string v1, "cancelTransaction"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 459
    new-instance v5, Lcom/sec/android/service/health/connectionmanager2/RequestParam;

    invoke-direct {v5}, Lcom/sec/android/service/health/connectionmanager2/RequestParam;-><init>()V

    .line 460
    .local v5, "params":Lcom/sec/android/service/health/connectionmanager2/RequestParam;
    const-string v0, "htxmethod"

    const-string v1, "cancel"

    invoke-virtual {v5, v0, v1}, Lcom/sec/android/service/health/connectionmanager2/RequestParam;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 462
    const-string v0, "ServerMgrHttps"

    invoke-static {v0}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;->getInstance(Ljava/lang/String;)Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    move-result-object v0

    const/16 v2, 0x6b

    sget-object v3, Lcom/sec/android/service/health/connectionmanager2/MethodType;->GET:Lcom/sec/android/service/health/connectionmanager2/MethodType;

    const-string/jumbo v4, "v2/shealth/phr/txmanager"

    const-string v8, "cancel"

    invoke-direct {p0}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->getDefaultHeader()Ljava/util/HashMap;

    move-result-object v9

    move-object v1, p0

    move-object v7, p0

    invoke-virtual/range {v0 .. v9}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->placeRequest(Ljava/lang/Object;ILcom/sec/android/service/health/connectionmanager2/MethodType;Ljava/lang/String;Lcom/sec/android/service/health/connectionmanager2/RequestParam;Ljava/lang/String;Lcom/sec/android/service/health/connectionmanager2/IOnResponseListener;Ljava/lang/Object;Ljava/util/HashMap;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->currentRequestId:J

    .line 464
    iput-object v6, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mTransactionID:Ljava/lang/String;

    .line 465
    return-void
.end method

.method private endTransaction()V
    .locals 10

    .prologue
    const/4 v6, 0x0

    .line 438
    sget-object v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->TAG:Ljava/lang/String;

    const-string v1, "endTransaction"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 440
    new-instance v5, Lcom/sec/android/service/health/connectionmanager2/RequestParam;

    invoke-direct {v5}, Lcom/sec/android/service/health/connectionmanager2/RequestParam;-><init>()V

    .line 441
    .local v5, "params":Lcom/sec/android/service/health/connectionmanager2/RequestParam;
    const-string v0, "htxmethod"

    const-string v1, "end"

    invoke-virtual {v5, v0, v1}, Lcom/sec/android/service/health/connectionmanager2/RequestParam;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 443
    const-string v0, "ServerMgrHttps"

    invoke-static {v0}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;->getInstance(Ljava/lang/String;)Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    move-result-object v0

    const/16 v2, 0x6b

    sget-object v3, Lcom/sec/android/service/health/connectionmanager2/MethodType;->GET:Lcom/sec/android/service/health/connectionmanager2/MethodType;

    const-string/jumbo v4, "v2/shealth/phr/txmanager"

    invoke-direct {p0}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->getDefaultHeader()Ljava/util/HashMap;

    move-result-object v9

    move-object v1, p0

    move-object v7, p0

    move-object v8, v6

    invoke-virtual/range {v0 .. v9}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->placeRequest(Ljava/lang/Object;ILcom/sec/android/service/health/connectionmanager2/MethodType;Ljava/lang/String;Lcom/sec/android/service/health/connectionmanager2/RequestParam;Ljava/lang/String;Lcom/sec/android/service/health/connectionmanager2/IOnResponseListener;Ljava/lang/Object;Ljava/util/HashMap;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->currentRequestId:J

    .line 444
    iput-object v6, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mTransactionID:Ljava/lang/String;

    .line 445
    return-void
.end method

.method private static getCountryCode()Ljava/lang/String;
    .locals 9

    .prologue
    .line 385
    const/4 v2, 0x0

    .line 389
    .local v2, "countryCode":Ljava/lang/String;
    :try_start_0
    const-string v5, "android.os.SystemProperties"

    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 390
    .local v1, "cl":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v5, "get"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v7, 0x0

    const-class v8, Ljava/lang/String;

    aput-object v8, v6, v7

    invoke-virtual {v1, v5, v6}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    .line 391
    .local v4, "method":Ljava/lang/reflect/Method;
    const/4 v5, 0x0

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-string/jumbo v8, "ro.csc.countryiso_code"

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    move-object v0, v5

    check-cast v0, Ljava/lang/String;

    move-object v2, v0

    .line 393
    sget-object v5, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "country code:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 400
    .end local v1    # "cl":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v2    # "countryCode":Ljava/lang/String;
    .end local v4    # "method":Ljava/lang/reflect/Method;
    :goto_0
    return-object v2

    .line 395
    .restart local v2    # "countryCode":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 397
    .local v3, "e":Ljava/lang/Exception;
    sget-object v5, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->TAG:Ljava/lang/String;

    const-string v6, "Exception while retrieving Country Code, setting  country code with respect to Locale Language"

    invoke-static {v5, v6}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 398
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public static getDatabasePath()Ljava/lang/String;
    .locals 2

    .prologue
    .line 334
    sget-object v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->TAG:Ljava/lang/String;

    const-string v1, "getDatabasePath()"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 347
    const-string v0, "/data/data/com.sec.android.app.shealth/databases/shealth_to_upgrade.db"

    return-object v0
.end method

.method private getDefaultHeader()Ljava/util/HashMap;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 366
    sget-object v2, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->TAG:Ljava/lang/String;

    const-string v3, "getDefaultHeader()"

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 367
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 368
    .local v1, "headerParams":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;[Ljava/lang/String;>;"
    const-string v2, "accept"

    new-array v3, v6, [Ljava/lang/String;

    const-string v4, "application/json"

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 369
    const-string v2, "accountid"

    new-array v3, v6, [Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mAccountID:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 370
    const-string v2, "deviceid"

    new-array v3, v6, [Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mDeviceID:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 371
    const-string/jumbo v2, "user_token"

    new-array v3, v6, [Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mUserToken:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 373
    iget-object v2, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mTransactionID:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 375
    const-string v2, "htxid"

    new-array v3, v6, [Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mTransactionID:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 377
    :cond_0
    invoke-static {}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->getCountryCode()Ljava/lang/String;

    move-result-object v0

    .line 378
    .local v0, "countryCode":Ljava/lang/String;
    const-string v2, "country_code"

    new-array v3, v6, [Ljava/lang/String;

    aput-object v0, v3, v5

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 379
    sget-object v2, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "accept : application/json; accountid: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mAccountID:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "; deviceid: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mDeviceID:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "; user_token: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mUserToken:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "; htxid: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mTransactionID:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "; country_code: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ";"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 380
    return-object v1
.end method

.method private initializeConnectionmanager(Z)Z
    .locals 7
    .param p1, "isHttpsEnabled"    # Z

    .prologue
    .line 474
    sget-object v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->TAG:Ljava/lang/String;

    const-string v1, "initializeConnectionmanager()"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 475
    const-string v0, "ServerMgrHttps"

    invoke-static {v0}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;->getInstance(Ljava/lang/String;)Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    move-result-object v0

    if-nez v0, :cond_0

    .line 477
    const-string v0, "ServerMgrHttps"

    invoke-static {v0}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;->createInstance(Ljava/lang/String;)Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    .line 478
    const-string v0, "ServerMgrHttps"

    invoke-static {v0}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;->getInstance(Ljava/lang/String;)Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    move-result-object v0

    sget-object v1, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$HServer;->SERVER_URL_BASE_H:Ljava/lang/String;

    sget v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$HServer;->HTTPS_PORT_NO_H:I

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->setAddressWithoutLookUp(Ljava/lang/String;I)Z

    .line 481
    :try_start_0
    const-string v0, "ServerMgrHttps"

    invoke-static {v0}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;->getInstance(Ljava/lang/String;)Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    move-result-object v0

    const/16 v1, 0x2710

    const/16 v2, 0x7530

    const/4 v4, 0x0

    const/4 v5, 0x0

    move v3, p1

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->initConnectionManager(IIZLjava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 489
    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 483
    :catch_0
    move-exception v6

    .line 485
    .local v6, "ex":Ljava/lang/Exception;
    iget-object v0, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mRestoreListener:Lcom/sec/android/app/shealth/backup/server/IRestoreListener;

    invoke-interface {v0, v6}, Lcom/sec/android/app/shealth/backup/server/IRestoreListener;->onException(Ljava/lang/Exception;)V

    .line 486
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private removeTemporaryRestoredContents()V
    .locals 4

    .prologue
    .line 221
    sget-object v2, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "removeTemporaryRestoredContents()"

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    new-instance v0, Ljava/io/File;

    const-string v2, "/data/data/com.sec.android.app.shealth/databases/cache_shealth2.db"

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 223
    .local v0, "cacheDBFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 225
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 228
    :cond_0
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/sec/android/app/shealth/common/utils/constants/FilePathsConstants;->SHAREDPREF_FOLDER_PATH:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "cache_shared_prefs.xml"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 229
    .local v1, "cachePreferenceFile":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 231
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 239
    :cond_1
    return-void
.end method

.method private requestToDownloadImage()Z
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 782
    sget-object v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "requestToDownloadImage()"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 785
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->resImage:Lcom/sec/android/app/shealth/backup/server/data/ResponseImage;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->resImage:Lcom/sec/android/app/shealth/backup/server/data/ResponseImage;

    iget-object v0, v0, Lcom/sec/android/app/shealth/backup/server/data/ResponseImage;->imglist:Ljava/util/ArrayList;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->resImage:Lcom/sec/android/app/shealth/backup/server/data/ResponseImage;

    iget-object v0, v0, Lcom/sec/android/app/shealth/backup/server/data/ResponseImage;->imglist:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v1, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mImageCounter:I

    if-le v0, v1, :cond_4

    .line 787
    const/4 v8, 0x0

    .line 788
    .local v8, "file":Ljava/io/File;
    iget-object v0, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->resImage:Lcom/sec/android/app/shealth/backup/server/data/ResponseImage;

    iget-object v0, v0, Lcom/sec/android/app/shealth/backup/server/data/ResponseImage;->imglist:Ljava/util/ArrayList;

    iget v1, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mImageCounter:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 789
    .local v9, "filename":Ljava/lang/String;
    const-string/jumbo v0, "user_photo.jpg"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 793
    new-instance v8, Ljava/io/File;

    .end local v8    # "file":Ljava/io/File;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/shealth/common/utils/constants/FilePathsConstants;->SDCARD_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/shealth/common/utils/constants/FilePathsConstants;->CACHED_IMAGES_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_temp/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v8, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 809
    .restart local v8    # "file":Ljava/io/File;
    :goto_0
    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 811
    invoke-virtual {v8}, Ljava/io/File;->mkdirs()Z

    .line 813
    :cond_0
    new-instance v4, Ljava/io/FileOutputStream;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 814
    .local v4, "fileOutputStream":Ljava/io/FileOutputStream;
    const-string v0, "ServerMgrHttps"

    invoke-static {v0}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;->getInstance(Ljava/lang/String;)Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    move-result-object v0

    const/16 v1, 0x72

    iget-object v2, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->resImage:Lcom/sec/android/app/shealth/backup/server/data/ResponseImage;

    iget-object v2, v2, Lcom/sec/android/app/shealth/backup/server/data/ResponseImage;->imgurls:Ljava/util/ArrayList;

    iget v3, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mImageCounter:I

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v2, p0

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->downloadPublicUrl(ILcom/sec/android/service/health/connectionmanager2/IOnDownloadListener;Ljava/lang/String;Ljava/io/OutputStream;Ljava/lang/Object;Ljava/util/ArrayList;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->currentRequestId:J

    .line 815
    const/4 v0, 0x1

    .line 826
    .end local v4    # "fileOutputStream":Ljava/io/FileOutputStream;
    .end local v8    # "file":Ljava/io/File;
    .end local v9    # "filename":Ljava/lang/String;
    :goto_1
    return v0

    .line 795
    .restart local v8    # "file":Ljava/io/File;
    .restart local v9    # "filename":Ljava/lang/String;
    :cond_1
    const-string v0, "background_portrait_image.jpg"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "background_landscape_image.jpg"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 799
    :cond_2
    new-instance v8, Ljava/io/File;

    .end local v8    # "file":Ljava/io/File;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/shealth/common/utils/constants/FilePathsConstants;->CACHED_IMAGES_FULL_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_temp/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v8, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .restart local v8    # "file":Ljava/io/File;
    goto :goto_0

    .line 805
    :cond_3
    new-instance v8, Ljava/io/File;

    .end local v8    # "file":Ljava/io/File;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/shealth/common/utils/constants/FilePathsConstants;->RESTORE_FOOD_IMAGES_FULL_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_temp"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v8, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .restart local v8    # "file":Ljava/io/File;
    goto/16 :goto_0

    .end local v8    # "file":Ljava/io/File;
    .end local v9    # "filename":Ljava/lang/String;
    :cond_4
    move v0, v10

    .line 819
    goto :goto_1

    .line 822
    :catch_0
    move-exception v7

    .line 824
    .local v7, "e":Ljava/io/FileNotFoundException;
    invoke-direct {p0}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->stopRestore()V

    .line 825
    iget-object v0, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mRestoreListener:Lcom/sec/android/app/shealth/backup/server/IRestoreListener;

    invoke-interface {v0, v7}, Lcom/sec/android/app/shealth/backup/server/IRestoreListener;->onException(Ljava/lang/Exception;)V

    move v0, v10

    .line 826
    goto :goto_1
.end method

.method private restoreStarted()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 977
    sget-object v1, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "restoreStarted()"

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 978
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->isRestoreStarted:Z

    .line 979
    iget-object v1, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "power"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 980
    .local v0, "pm":Landroid/os/PowerManager;
    const-string v1, "RestoreManager"

    invoke-virtual {v0, v3, v1}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->wl:Landroid/os/PowerManager$WakeLock;

    .line 981
    iget-object v1, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->wl:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 982
    return-void
.end method

.method private restoreStopped()V
    .locals 2

    .prologue
    .line 986
    sget-object v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "restoreStopped()"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 987
    iget-object v0, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->wl:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    .line 989
    iget-object v0, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->wl:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 990
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->wl:Landroid/os/PowerManager$WakeLock;

    .line 993
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->isRestoreStarted:Z

    .line 994
    return-void
.end method

.method private startTransaction()V
    .locals 10

    .prologue
    const/4 v6, 0x0

    const-wide/16 v2, 0x0

    .line 408
    sget-object v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "startTransaction"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 410
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->initializeConnectionmanager(Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 431
    :goto_0
    return-void

    .line 416
    :cond_0
    iput-wide v2, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mFileSizeDBRestore:J

    .line 417
    iput-wide v2, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mFileSizePrefRestore:J

    .line 418
    iput-wide v2, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mTotalSizeRestore:J

    .line 419
    iput-wide v2, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->totalDownloaded:J

    .line 420
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mImageCounter:I

    .line 422
    iget-object v0, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mAccountID:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 424
    iget-object v0, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/SamsungAccount;->getSamsungAccount(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mAccountID:Ljava/lang/String;

    .line 427
    :cond_1
    new-instance v5, Lcom/sec/android/service/health/connectionmanager2/RequestParam;

    invoke-direct {v5}, Lcom/sec/android/service/health/connectionmanager2/RequestParam;-><init>()V

    .line 428
    .local v5, "params":Lcom/sec/android/service/health/connectionmanager2/RequestParam;
    const-string v0, "htxmethod"

    const-string/jumbo v1, "start"

    invoke-virtual {v5, v0, v1}, Lcom/sec/android/service/health/connectionmanager2/RequestParam;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 430
    const-string v0, "ServerMgrHttps"

    invoke-static {v0}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;->getInstance(Ljava/lang/String;)Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    move-result-object v0

    const/16 v2, 0x6b

    sget-object v3, Lcom/sec/android/service/health/connectionmanager2/MethodType;->GET:Lcom/sec/android/service/health/connectionmanager2/MethodType;

    const-string/jumbo v4, "v2/shealth/phr/txmanager"

    invoke-direct {p0}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->getDefaultHeader()Ljava/util/HashMap;

    move-result-object v9

    move-object v1, p0

    move-object v7, p0

    move-object v8, v6

    invoke-virtual/range {v0 .. v9}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->placeRequest(Ljava/lang/Object;ILcom/sec/android/service/health/connectionmanager2/MethodType;Ljava/lang/String;Lcom/sec/android/service/health/connectionmanager2/RequestParam;Ljava/lang/String;Lcom/sec/android/service/health/connectionmanager2/IOnResponseListener;Ljava/lang/Object;Ljava/util/HashMap;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->currentRequestId:J

    goto :goto_0
.end method

.method private stopRestore()V
    .locals 2

    .prologue
    .line 890
    sget-object v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "stopRestore()"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 891
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->isRestoreStarted:Z

    if-nez v0, :cond_1

    .line 903
    :cond_0
    :goto_0
    return-void

    .line 896
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->removeTemporaryRestoredContents()V

    .line 898
    invoke-direct {p0}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->restoreStopped()V

    .line 899
    const-string v0, "ServerMgrHttps"

    invoke-static {v0}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;->getInstance(Ljava/lang/String;)Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 901
    invoke-direct {p0}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->cancelTransaction()V

    goto :goto_0
.end method


# virtual methods
.method public cancel()V
    .locals 2

    .prologue
    .line 206
    sget-object v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->TAG:Ljava/lang/String;

    const-string v1, "cancelImport()"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 207
    const-string v0, "ServerMgrHttps"

    invoke-static {v0}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;->getInstance(Ljava/lang/String;)Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 209
    const-string v0, "ServerMgrHttps"

    invoke-static {v0}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;->getInstance(Ljava/lang/String;)Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->cancelRequest(Ljava/lang/Object;)V

    .line 210
    const-string v0, "ServerMgrHttps"

    invoke-static {v0}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;->getInstance(Ljava/lang/String;)Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->stopAllDownload()V

    .line 213
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->stopRestore()V

    .line 214
    return-void
.end method

.method public checkDataToRestore(Lcom/sec/android/app/shealth/backup/server/IRestoreListener;)Z
    .locals 3
    .param p1, "listener"    # Lcom/sec/android/app/shealth/backup/server/IRestoreListener;

    .prologue
    const/4 v0, 0x1

    .line 184
    sget-object v1, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->TAG:Ljava/lang/String;

    const-string v2, "checkDataToRestore()"

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->start(Lcom/sec/android/app/shealth/backup/server/IRestoreListener;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 187
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->isCheckingDataToRestore:Z

    .line 191
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public copyRestoredContents()V
    .locals 9

    .prologue
    .line 246
    sget-object v6, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->TAG:Ljava/lang/String;

    const-string v7, "ServerBackupMgr: copyRestoredContents"

    invoke-static {v6, v7}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 269
    new-instance v2, Ljava/io/File;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcom/sec/android/app/shealth/common/utils/constants/FilePathsConstants;->SDCARD_PATH:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/sec/android/app/shealth/common/utils/constants/FilePathsConstants;->CACHED_IMAGES_PATH:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v2, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 270
    .local v2, "imgFolder":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 272
    new-instance v6, Ljava/io/File;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, Lcom/sec/android/app/shealth/common/utils/constants/FilePathsConstants;->SDCARD_PATH:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/sec/android/app/shealth/common/utils/constants/FilePathsConstants;->CACHED_IMAGES_PATH:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "_old"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v6}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 275
    :cond_0
    new-instance v4, Ljava/io/File;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcom/sec/android/app/shealth/common/utils/constants/FilePathsConstants;->SDCARD_PATH:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/sec/android/app/shealth/common/utils/constants/FilePathsConstants;->CACHED_IMAGES_PATH:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "_temp"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 276
    .local v4, "restoredImgFolder":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 278
    new-instance v6, Ljava/io/File;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, Lcom/sec/android/app/shealth/common/utils/constants/FilePathsConstants;->SDCARD_PATH:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/sec/android/app/shealth/common/utils/constants/FilePathsConstants;->CACHED_IMAGES_PATH:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v6}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 284
    :cond_1
    new-instance v0, Ljava/io/File;

    sget-object v6, Lcom/sec/android/app/shealth/common/utils/constants/FilePathsConstants;->CACHED_IMAGES_FULL_PATH:Ljava/lang/String;

    invoke-direct {v0, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 285
    .local v0, "bgimageFolder":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 287
    new-instance v6, Ljava/io/File;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, Lcom/sec/android/app/shealth/common/utils/constants/FilePathsConstants;->CACHED_IMAGES_FULL_PATH:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "_old"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v6}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 290
    :cond_2
    new-instance v3, Ljava/io/File;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcom/sec/android/app/shealth/common/utils/constants/FilePathsConstants;->CACHED_IMAGES_FULL_PATH:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "_temp"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 291
    .local v3, "restoredBGImgFolder":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 293
    new-instance v6, Ljava/io/File;

    sget-object v7, Lcom/sec/android/app/shealth/common/utils/constants/FilePathsConstants;->CACHED_IMAGES_FULL_PATH:Ljava/lang/String;

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v6}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 299
    :cond_3
    new-instance v1, Ljava/io/File;

    sget-object v6, Lcom/sec/android/app/shealth/common/utils/constants/FilePathsConstants;->RESTORE_FOOD_IMAGES_FULL_PATH:Ljava/lang/String;

    invoke-direct {v1, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 300
    .local v1, "foodimgFolder":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 302
    new-instance v6, Ljava/io/File;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, Lcom/sec/android/app/shealth/common/utils/constants/FilePathsConstants;->RESTORE_FOOD_IMAGES_FULL_PATH:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "_old"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 305
    :cond_4
    new-instance v5, Ljava/io/File;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcom/sec/android/app/shealth/common/utils/constants/FilePathsConstants;->RESTORE_FOOD_IMAGES_FULL_PATH:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "_temp"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 306
    .local v5, "restoredfoodImgFolder":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 308
    new-instance v6, Ljava/io/File;

    sget-object v7, Lcom/sec/android/app/shealth/common/utils/constants/FilePathsConstants;->RESTORE_FOOD_IMAGES_FULL_PATH:Ljava/lang/String;

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v6}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 311
    :cond_5
    invoke-direct {p0}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->removeTemporaryRestoredContents()V

    .line 325
    return-void
.end method

.method public finalize()V
    .locals 2

    .prologue
    .line 999
    sget-object v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->TAG:Ljava/lang/String;

    const-string v1, "finalize()"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1000
    invoke-direct {p0}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->restoreStopped()V

    .line 1001
    return-void
.end method

.method public onDownload(JILcom/sec/android/service/health/connectionmanager2/IOnDownloadListener$Status;JLcom/sec/android/service/health/connectionmanager2/NetException;Ljava/lang/Object;Ljava/util/HashMap;)V
    .locals 11
    .param p1, "requestId"    # J
    .param p3, "privateId"    # I
    .param p4, "status"    # Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener$Status;
    .param p5, "size"    # J
    .param p7, "e"    # Lcom/sec/android/service/health/connectionmanager2/NetException;
    .param p8, "tag"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JI",
            "Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener$Status;",
            "J",
            "Lcom/sec/android/service/health/connectionmanager2/NetException;",
            "Ljava/lang/Object;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 909
    .local p9, "headervalues":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;[Ljava/lang/String;>;"
    sget-object v1, Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener$Status;->ONGOING:Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener$Status;

    if-ne p4, v1, :cond_1

    .line 911
    sget-object v1, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "onDownload(): ongoing"

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 912
    iget-object v1, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mRestoreListener:Lcom/sec/android/app/shealth/backup/server/IRestoreListener;

    sget-object v2, Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;->ONGOING:Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;

    iget-wide v3, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->totalDownloaded:J

    add-long v3, v3, p5

    invoke-interface {v1, v2, v3, v4}, Lcom/sec/android/app/shealth/backup/server/IRestoreListener;->onRestoreProgress(Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;J)V

    .line 973
    :cond_0
    :goto_0
    return-void

    .line 914
    :cond_1
    sget-object v1, Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener$Status;->STARTED:Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener$Status;

    if-ne p4, v1, :cond_2

    .line 916
    sget-object v1, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "onDownload(): started"

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 917
    iget-wide v1, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->totalDownloaded:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    .line 919
    iget-object v1, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mRestoreListener:Lcom/sec/android/app/shealth/backup/server/IRestoreListener;

    sget-object v2, Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;->STARTED:Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;

    iget-wide v3, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mTotalSizeRestore:J

    invoke-interface {v1, v2, v3, v4}, Lcom/sec/android/app/shealth/backup/server/IRestoreListener;->onRestoreProgress(Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;J)V

    goto :goto_0

    .line 922
    :cond_2
    sget-object v1, Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener$Status;->DONE:Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener$Status;

    if-ne p4, v1, :cond_9

    .line 924
    sget-object v1, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "onDownload(): done"

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 925
    iget-object v1, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mCurrentStage:Lcom/sec/android/app/shealth/backup/server/RestoreManager$SyncStage;

    sget-object v2, Lcom/sec/android/app/shealth/backup/server/RestoreManager$SyncStage;->DATABASE:Lcom/sec/android/app/shealth/backup/server/RestoreManager$SyncStage;

    if-ne v1, v2, :cond_4

    .line 927
    iget-wide v1, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mFileSizeDBRestore:J

    iput-wide v1, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->totalDownloaded:J

    .line 928
    sget-object v1, Lcom/sec/android/app/shealth/backup/server/RestoreManager$SyncStage;->PREFERENCE:Lcom/sec/android/app/shealth/backup/server/RestoreManager$SyncStage;

    iput-object v1, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mCurrentStage:Lcom/sec/android/app/shealth/backup/server/RestoreManager$SyncStage;

    .line 929
    const-string v1, "ServerMgrHttps"

    invoke-static {v1}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;->getInstance(Ljava/lang/String;)Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    move-result-object v1

    const/16 v3, 0x6e

    sget-object v4, Lcom/sec/android/service/health/connectionmanager2/MethodType;->GET:Lcom/sec/android/service/health/connectionmanager2/MethodType;

    const-string/jumbo v5, "v2/shealth/phr/cfgrestore"

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v9, 0x0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->getDefaultHeader()Ljava/util/HashMap;

    move-result-object v10

    move-object v2, p0

    move-object v8, p0

    invoke-virtual/range {v1 .. v10}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->placeRequest(Ljava/lang/Object;ILcom/sec/android/service/health/connectionmanager2/MethodType;Ljava/lang/String;Lcom/sec/android/service/health/connectionmanager2/RequestParam;Ljava/lang/String;Lcom/sec/android/service/health/connectionmanager2/IOnResponseListener;Ljava/lang/Object;Ljava/util/HashMap;)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->currentRequestId:J

    .line 962
    :cond_3
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mRestoreListener:Lcom/sec/android/app/shealth/backup/server/IRestoreListener;

    sget-object v2, Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;->ONGOING:Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;

    iget-wide v3, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->totalDownloaded:J

    invoke-interface {v1, v2, v3, v4}, Lcom/sec/android/app/shealth/backup/server/IRestoreListener;->onRestoreProgress(Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;J)V

    goto :goto_0

    .line 931
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mCurrentStage:Lcom/sec/android/app/shealth/backup/server/RestoreManager$SyncStage;

    sget-object v2, Lcom/sec/android/app/shealth/backup/server/RestoreManager$SyncStage;->PREFERENCE:Lcom/sec/android/app/shealth/backup/server/RestoreManager$SyncStage;

    if-ne v1, v2, :cond_6

    .line 933
    iget-wide v1, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->totalDownloaded:J

    iget-wide v3, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mFileSizePrefRestore:J

    add-long/2addr v1, v3

    iput-wide v1, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->totalDownloaded:J

    .line 935
    iget-object v1, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->resCheckStatus:Lcom/sec/android/app/shealth/backup/server/data/ResponseCheckStatus;

    iget-object v1, v1, Lcom/sec/android/app/shealth/backup/server/data/ResponseCheckStatus;->imglist:Ljava/util/ArrayList;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->resCheckStatus:Lcom/sec/android/app/shealth/backup/server/data/ResponseCheckStatus;

    iget-object v1, v1, Lcom/sec/android/app/shealth/backup/server/data/ResponseCheckStatus;->imglist:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_5

    .line 939
    sget-object v1, Lcom/sec/android/app/shealth/backup/server/RestoreManager$SyncStage;->IMAGE:Lcom/sec/android/app/shealth/backup/server/RestoreManager$SyncStage;

    iput-object v1, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mCurrentStage:Lcom/sec/android/app/shealth/backup/server/RestoreManager$SyncStage;

    .line 940
    const-string v1, "ServerMgrHttps"

    invoke-static {v1}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;->getInstance(Ljava/lang/String;)Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    move-result-object v1

    const/16 v3, 0x6f

    sget-object v4, Lcom/sec/android/service/health/connectionmanager2/MethodType;->GET:Lcom/sec/android/service/health/connectionmanager2/MethodType;

    const-string/jumbo v5, "v2/shealth/phr/imgrestoreAll"

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v9, 0x0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->getDefaultHeader()Ljava/util/HashMap;

    move-result-object v10

    move-object v2, p0

    move-object v8, p0

    invoke-virtual/range {v1 .. v10}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->placeRequest(Ljava/lang/Object;ILcom/sec/android/service/health/connectionmanager2/MethodType;Ljava/lang/String;Lcom/sec/android/service/health/connectionmanager2/RequestParam;Ljava/lang/String;Lcom/sec/android/service/health/connectionmanager2/IOnResponseListener;Ljava/lang/Object;Ljava/util/HashMap;)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->currentRequestId:J

    goto :goto_1

    .line 944
    :cond_5
    invoke-direct {p0}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->endTransaction()V

    goto :goto_1

    .line 951
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->resCheckStatus:Lcom/sec/android/app/shealth/backup/server/data/ResponseCheckStatus;

    iget-object v1, v1, Lcom/sec/android/app/shealth/backup/server/data/ResponseCheckStatus;->imglistsize:Ljava/util/ArrayList;

    if-eqz v1, :cond_7

    iget v1, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mImageCounter:I

    iget-object v2, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->resCheckStatus:Lcom/sec/android/app/shealth/backup/server/data/ResponseCheckStatus;

    iget-object v2, v2, Lcom/sec/android/app/shealth/backup/server/data/ResponseCheckStatus;->imglistsize:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_7

    .line 952
    iget-wide v2, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->totalDownloaded:J

    iget-object v1, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->resCheckStatus:Lcom/sec/android/app/shealth/backup/server/data/ResponseCheckStatus;

    iget-object v1, v1, Lcom/sec/android/app/shealth/backup/server/data/ResponseCheckStatus;->imglistsize:Ljava/util/ArrayList;

    iget v4, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mImageCounter:I

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    add-long v1, v2, v4

    iput-wide v1, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->totalDownloaded:J

    .line 956
    :goto_2
    iget v1, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mImageCounter:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mImageCounter:I

    .line 957
    invoke-direct {p0}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->requestToDownloadImage()Z

    move-result v1

    if-nez v1, :cond_3

    .line 959
    invoke-direct {p0}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->endTransaction()V

    goto/16 :goto_1

    .line 954
    :cond_7
    sget-object v2, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "mImageCounter is more than resCheckStatus.imglistsize.size "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v3, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mImageCounter:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " , "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v1, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->resCheckStatus:Lcom/sec/android/app/shealth/backup/server/data/ResponseCheckStatus;

    iget-object v1, v1, Lcom/sec/android/app/shealth/backup/server/data/ResponseCheckStatus;->imglistsize:Ljava/util/ArrayList;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->resCheckStatus:Lcom/sec/android/app/shealth/backup/server/data/ResponseCheckStatus;

    iget-object v1, v1, Lcom/sec/android/app/shealth/backup/server/data/ResponseCheckStatus;->imglistsize:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    :goto_3
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_8
    const/4 v1, 0x0

    goto :goto_3

    .line 964
    :cond_9
    sget-object v1, Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener$Status;->STOPPED:Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener$Status;

    if-ne p4, v1, :cond_0

    .line 966
    sget-object v1, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->TAG:Ljava/lang/String;

    const-string v2, "RestoreManager: onDownload: IOnDownloadListener.Status.STOPPED"

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 967
    if-eqz p7, :cond_0

    invoke-virtual/range {p7 .. p7}, Lcom/sec/android/service/health/connectionmanager2/NetException;->getCode()I

    move-result v1

    if-eqz v1, :cond_0

    .line 969
    invoke-direct {p0}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->stopRestore()V

    .line 970
    iget-object v1, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mRestoreListener:Lcom/sec/android/app/shealth/backup/server/IRestoreListener;

    move-object/from16 v0, p7

    invoke-interface {v1, v0}, Lcom/sec/android/app/shealth/backup/server/IRestoreListener;->onException(Ljava/lang/Exception;)V

    goto/16 :goto_0
.end method

.method public onExceptionReceived(JILcom/sec/android/service/health/connectionmanager2/NetException;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3
    .param p1, "requestId"    # J
    .param p3, "privateId"    # I
    .param p4, "ex"    # Lcom/sec/android/service/health/connectionmanager2/NetException;
    .param p5, "tag"    # Ljava/lang/Object;
    .param p6, "ReRequest"    # Ljava/lang/Object;

    .prologue
    .line 863
    sget-object v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "RestoreManager: onExceptionReceived:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p4}, Lcom/sec/android/service/health/connectionmanager2/NetException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 865
    if-nez p5, :cond_0

    .line 867
    invoke-direct {p0}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->stopRestore()V

    .line 868
    iget-object v0, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mRestoreListener:Lcom/sec/android/app/shealth/backup/server/IRestoreListener;

    invoke-interface {v0, p4}, Lcom/sec/android/app/shealth/backup/server/IRestoreListener;->onException(Ljava/lang/Exception;)V

    .line 871
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mTransactionID:Ljava/lang/String;

    .line 872
    return-void
.end method

.method public onRequestCancelled(JILjava/lang/Object;Ljava/lang/Object;)V
    .locals 2
    .param p1, "requestId"    # J
    .param p3, "privateId"    # I
    .param p4, "tag"    # Ljava/lang/Object;
    .param p5, "ReRequest"    # Ljava/lang/Object;

    .prologue
    .line 880
    sget-object v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->TAG:Ljava/lang/String;

    const-string v1, "RestoreManager: onRequestCancelled"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 882
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mTransactionID:Ljava/lang/String;

    .line 886
    return-void
.end method

.method public onResponseReceived(JILjava/lang/Object;Ljava/lang/Object;)V
    .locals 25
    .param p1, "requestId"    # J
    .param p3, "privateId"    # I
    .param p4, "response"    # Ljava/lang/Object;
    .param p5, "tag"    # Ljava/lang/Object;

    .prologue
    .line 495
    sget-object v2, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "onResponseReceived()"

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 496
    new-instance v2, Lcom/google/gson/GsonBuilder;

    invoke-direct {v2}, Lcom/google/gson/GsonBuilder;-><init>()V

    invoke-virtual {v2}, Lcom/google/gson/GsonBuilder;->create()Lcom/google/gson/Gson;

    move-result-object v21

    .line 498
    .local v21, "gson":Lcom/google/gson/Gson;
    packed-switch p3, :pswitch_data_0

    .line 775
    .end local p4    # "response":Ljava/lang/Object;
    :cond_0
    :goto_0
    return-void

    .line 502
    .restart local p4    # "response":Ljava/lang/Object;
    :pswitch_0
    check-cast p4, Ljava/lang/String;

    .end local p4    # "response":Ljava/lang/Object;
    const-class v2, Lcom/sec/android/app/shealth/backup/server/data/ResponseTranaction;

    move-object/from16 v0, v21

    move-object/from16 v1, p4

    invoke-virtual {v0, v1, v2}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/sec/android/app/shealth/backup/server/data/ResponseTranaction;

    .line 503
    .local v24, "res":Lcom/sec/android/app/shealth/backup/server/data/ResponseTranaction;
    if-nez v24, :cond_1

    .line 505
    new-instance v6, Lcom/sec/android/service/health/connectionmanager2/NetException;

    const/4 v2, -0x6

    const/16 v3, 0x1f4

    invoke-direct {v6, v2, v3}, Lcom/sec/android/service/health/connectionmanager2/NetException;-><init>(II)V

    const/4 v8, 0x0

    move-object/from16 v2, p0

    move-wide/from16 v3, p1

    move/from16 v5, p3

    move-object/from16 v7, p5

    invoke-virtual/range {v2 .. v8}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->onExceptionReceived(JILcom/sec/android/service/health/connectionmanager2/NetException;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 508
    :cond_1
    invoke-virtual/range {v24 .. v24}, Lcom/sec/android/app/shealth/backup/server/data/ResponseTranaction;->checkIsResCodeOK()Z

    move-result v2

    if-nez v2, :cond_5

    .line 510
    invoke-virtual/range {v24 .. v24}, Lcom/sec/android/app/shealth/backup/server/data/ResponseTranaction;->getResCode()I

    move-result v2

    const/16 v3, 0x191

    if-eq v2, v3, :cond_2

    invoke-virtual/range {v24 .. v24}, Lcom/sec/android/app/shealth/backup/server/data/ResponseTranaction;->getResCode()I

    move-result v2

    const/16 v3, 0x193

    if-ne v2, v3, :cond_4

    .line 512
    :cond_2
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mTryToRenewToken:Z

    if-eqz v2, :cond_3

    .line 514
    sget-object v2, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->TAG:Ljava/lang/String;

    const-string v3, "2.x Restoration: It is failed to renew token"

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 515
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mRestoreListener:Lcom/sec/android/app/shealth/backup/server/IRestoreListener;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Lcom/sec/android/app/shealth/backup/server/IRestoreListener;->onException(Ljava/lang/Exception;)V

    goto :goto_0

    .line 519
    :cond_3
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mTryToRenewToken:Z

    .line 520
    invoke-static {}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->getInstance()Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    move-result-object v2

    new-instance v3, Lcom/sec/android/app/shealth/backup/server/RestoreManager$3;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/sec/android/app/shealth/backup/server/RestoreManager$3;-><init>(Lcom/sec/android/app/shealth/backup/server/RestoreManager;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->sendRequestForNewUserToken(Lcom/sec/android/service/health/samsungaccount/ISamsungUserTokenListener;)V

    goto :goto_0

    .line 562
    :cond_4
    new-instance v6, Lcom/sec/android/service/health/connectionmanager2/NetException;

    const/4 v2, -0x6

    invoke-virtual/range {v24 .. v24}, Lcom/sec/android/app/shealth/backup/server/data/ResponseTranaction;->getResCode()I

    move-result v3

    invoke-direct {v6, v2, v3}, Lcom/sec/android/service/health/connectionmanager2/NetException;-><init>(II)V

    const/4 v8, 0x0

    move-object/from16 v2, p0

    move-wide/from16 v3, p1

    move/from16 v5, p3

    move-object/from16 v7, p5

    invoke-virtual/range {v2 .. v8}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->onExceptionReceived(JILcom/sec/android/service/health/connectionmanager2/NetException;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 568
    :cond_5
    const-string/jumbo v2, "start"

    move-object/from16 v0, v24

    iget-object v3, v0, Lcom/sec/android/app/shealth/backup/server/data/ResponseTranaction;->htxmethod:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_7

    .line 570
    move-object/from16 v0, v24

    iget-object v2, v0, Lcom/sec/android/app/shealth/backup/server/data/ResponseTranaction;->deviceid:Ljava/lang/String;

    if-nez v2, :cond_6

    .line 572
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->endTransaction()V

    .line 573
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mRestoreListener:Lcom/sec/android/app/shealth/backup/server/IRestoreListener;

    invoke-interface {v2}, Lcom/sec/android/app/shealth/backup/server/IRestoreListener;->onNoData()V

    goto/16 :goto_0

    .line 576
    :cond_6
    move-object/from16 v0, v24

    iget-object v2, v0, Lcom/sec/android/app/shealth/backup/server/data/ResponseTranaction;->htxid:Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mTransactionID:Ljava/lang/String;

    .line 577
    const-string v2, "ServerMgrHttps"

    invoke-static {v2}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;->getInstance(Ljava/lang/String;)Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    move-result-object v2

    const/16 v4, 0x6c

    sget-object v5, Lcom/sec/android/service/health/connectionmanager2/MethodType;->GET:Lcom/sec/android/service/health/connectionmanager2/MethodType;

    const-string/jumbo v6, "v2/shealth/phr/chkstatus"

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v10, 0x0

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->getDefaultHeader()Ljava/util/HashMap;

    move-result-object v11

    move-object/from16 v3, p0

    move-object/from16 v9, p0

    invoke-virtual/range {v2 .. v11}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->placeRequest(Ljava/lang/Object;ILcom/sec/android/service/health/connectionmanager2/MethodType;Ljava/lang/String;Lcom/sec/android/service/health/connectionmanager2/RequestParam;Ljava/lang/String;Lcom/sec/android/service/health/connectionmanager2/IOnResponseListener;Ljava/lang/Object;Ljava/util/HashMap;)J

    move-result-wide v2

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->currentRequestId:J

    goto/16 :goto_0

    .line 579
    :cond_7
    const-string v2, "end"

    move-object/from16 v0, v24

    iget-object v3, v0, Lcom/sec/android/app/shealth/backup/server/data/ResponseTranaction;->htxmethod:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_0

    .line 581
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->restoreStopped()V

    .line 583
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->isCheckingDataToRestore:Z

    if-eqz v2, :cond_8

    .line 585
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mRestoreListener:Lcom/sec/android/app/shealth/backup/server/IRestoreListener;

    invoke-interface {v2}, Lcom/sec/android/app/shealth/backup/server/IRestoreListener;->onData()V

    goto/16 :goto_0

    .line 589
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mRestoreListener:Lcom/sec/android/app/shealth/backup/server/IRestoreListener;

    sget-object v3, Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;->DONE:Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mTotalSizeRestore:J

    invoke-interface {v2, v3, v4, v5}, Lcom/sec/android/app/shealth/backup/server/IRestoreListener;->onRestoreProgress(Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;J)V

    goto/16 :goto_0

    .line 597
    .end local v24    # "res":Lcom/sec/android/app/shealth/backup/server/data/ResponseTranaction;
    .restart local p4    # "response":Ljava/lang/Object;
    :pswitch_1
    check-cast p4, Ljava/lang/String;

    .end local p4    # "response":Ljava/lang/Object;
    const-class v2, Lcom/sec/android/app/shealth/backup/server/data/ResponseCheckStatus;

    move-object/from16 v0, v21

    move-object/from16 v1, p4

    invoke-virtual {v0, v1, v2}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/backup/server/data/ResponseCheckStatus;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->resCheckStatus:Lcom/sec/android/app/shealth/backup/server/data/ResponseCheckStatus;

    .line 598
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->resCheckStatus:Lcom/sec/android/app/shealth/backup/server/data/ResponseCheckStatus;

    if-nez v2, :cond_9

    .line 600
    new-instance v6, Lcom/sec/android/service/health/connectionmanager2/NetException;

    const/4 v2, -0x6

    const/16 v3, 0x1f4

    invoke-direct {v6, v2, v3}, Lcom/sec/android/service/health/connectionmanager2/NetException;-><init>(II)V

    const/4 v8, 0x0

    move-object/from16 v2, p0

    move-wide/from16 v3, p1

    move/from16 v5, p3

    move-object/from16 v7, p5

    invoke-virtual/range {v2 .. v8}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->onExceptionReceived(JILcom/sec/android/service/health/connectionmanager2/NetException;Ljava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 603
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->resCheckStatus:Lcom/sec/android/app/shealth/backup/server/data/ResponseCheckStatus;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/backup/server/data/ResponseCheckStatus;->checkIsResCodeOK()Z

    move-result v2

    if-nez v2, :cond_a

    .line 605
    new-instance v6, Lcom/sec/android/service/health/connectionmanager2/NetException;

    const/4 v2, -0x6

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->resCheckStatus:Lcom/sec/android/app/shealth/backup/server/data/ResponseCheckStatus;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/backup/server/data/ResponseCheckStatus;->getResCode()I

    move-result v3

    invoke-direct {v6, v2, v3}, Lcom/sec/android/service/health/connectionmanager2/NetException;-><init>(II)V

    const/4 v8, 0x0

    move-object/from16 v2, p0

    move-wide/from16 v3, p1

    move/from16 v5, p3

    move-object/from16 v7, p5

    invoke-virtual/range {v2 .. v8}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->onExceptionReceived(JILcom/sec/android/service/health/connectionmanager2/NetException;Ljava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 609
    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->resCheckStatus:Lcom/sec/android/app/shealth/backup/server/data/ResponseCheckStatus;

    iget-object v2, v2, Lcom/sec/android/app/shealth/backup/server/data/ResponseCheckStatus;->dbversion:Ljava/lang/String;

    if-nez v2, :cond_b

    .line 611
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->restoreStopped()V

    .line 612
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mRestoreListener:Lcom/sec/android/app/shealth/backup/server/IRestoreListener;

    invoke-interface {v2}, Lcom/sec/android/app/shealth/backup/server/IRestoreListener;->onNoData()V

    goto/16 :goto_0

    .line 616
    :cond_b
    const/16 v19, 0x0

    .line 619
    .local v19, "appCfgversion":I
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v23

    .line 620
    .local v23, "pm":Landroid/content/pm/PackageManager;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    move-object/from16 v0, v23

    invoke-virtual {v0, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    iget v0, v2, Landroid/content/pm/PackageInfo;->versionCode:I

    move/from16 v19, v0
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 626
    .end local v23    # "pm":Landroid/content/pm/PackageManager;
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->resCheckStatus:Lcom/sec/android/app/shealth/backup/server/data/ResponseCheckStatus;

    iget-object v2, v2, Lcom/sec/android/app/shealth/backup/server/data/ResponseCheckStatus;->cfgversion:Ljava/lang/String;

    if-eqz v2, :cond_e

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->resCheckStatus:Lcom/sec/android/app/shealth/backup/server/data/ResponseCheckStatus;

    iget-object v2, v2, Lcom/sec/android/app/shealth/backup/server/data/ResponseCheckStatus;->cfgversion:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    div-int/lit16 v2, v2, 0x3e8

    move/from16 v0, v19

    div-int/lit16 v3, v0, 0x3e8

    if-gt v2, v3, :cond_e

    .line 638
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->resCheckStatus:Lcom/sec/android/app/shealth/backup/server/data/ResponseCheckStatus;

    iget-object v2, v2, Lcom/sec/android/app/shealth/backup/server/data/ResponseCheckStatus;->dbsize:Ljava/lang/String;

    if-eqz v2, :cond_c

    .line 640
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->resCheckStatus:Lcom/sec/android/app/shealth/backup/server/data/ResponseCheckStatus;

    iget-object v2, v2, Lcom/sec/android/app/shealth/backup/server/data/ResponseCheckStatus;->dbsize:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mFileSizeDBRestore:J

    .line 641
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mTotalSizeRestore:J

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mFileSizeDBRestore:J

    add-long/2addr v2, v4

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mTotalSizeRestore:J

    .line 644
    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->resCheckStatus:Lcom/sec/android/app/shealth/backup/server/data/ResponseCheckStatus;

    iget-object v2, v2, Lcom/sec/android/app/shealth/backup/server/data/ResponseCheckStatus;->cfgsize:Ljava/lang/String;

    if-eqz v2, :cond_d

    .line 646
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->resCheckStatus:Lcom/sec/android/app/shealth/backup/server/data/ResponseCheckStatus;

    iget-object v2, v2, Lcom/sec/android/app/shealth/backup/server/data/ResponseCheckStatus;->cfgsize:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mFileSizePrefRestore:J

    .line 647
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mTotalSizeRestore:J

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mFileSizePrefRestore:J

    add-long/2addr v2, v4

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mTotalSizeRestore:J

    .line 650
    :cond_d
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->isCheckingDataToRestore:Z

    if-eqz v2, :cond_f

    .line 652
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->endTransaction()V

    goto/16 :goto_0

    .line 622
    :catch_0
    move-exception v20

    .line 624
    .local v20, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual/range {v20 .. v20}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_1

    .line 632
    .end local v20    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_e
    sget-object v2, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->TAG:Ljava/lang/String;

    const-string v3, "Current cfg version is not same as server cfg version, so restore not possible"

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 633
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->restoreStopped()V

    .line 634
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mRestoreListener:Lcom/sec/android/app/shealth/backup/server/IRestoreListener;

    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "Invalid version"

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-interface {v2, v3}, Lcom/sec/android/app/shealth/backup/server/IRestoreListener;->onException(Ljava/lang/Exception;)V

    goto/16 :goto_0

    .line 656
    :cond_f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->resCheckStatus:Lcom/sec/android/app/shealth/backup/server/data/ResponseCheckStatus;

    iget-object v2, v2, Lcom/sec/android/app/shealth/backup/server/data/ResponseCheckStatus;->imglistsize:Ljava/util/ArrayList;

    if-eqz v2, :cond_10

    .line 658
    const/16 v22, 0x0

    .local v22, "i":I
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->resCheckStatus:Lcom/sec/android/app/shealth/backup/server/data/ResponseCheckStatus;

    iget-object v2, v2, Lcom/sec/android/app/shealth/backup/server/data/ResponseCheckStatus;->imglistsize:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    move/from16 v0, v22

    if-ge v0, v2, :cond_10

    .line 660
    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mTotalSizeRestore:J

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->resCheckStatus:Lcom/sec/android/app/shealth/backup/server/data/ResponseCheckStatus;

    iget-object v2, v2, Lcom/sec/android/app/shealth/backup/server/data/ResponseCheckStatus;->imglistsize:Ljava/util/ArrayList;

    move/from16 v0, v22

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v7

    add-long v2, v3, v7

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mTotalSizeRestore:J

    .line 658
    add-int/lit8 v22, v22, 0x1

    goto :goto_2

    .line 671
    .end local v22    # "i":I
    :cond_10
    const-string v2, "ServerMgrHttps"

    invoke-static {v2}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;->getInstance(Ljava/lang/String;)Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    move-result-object v2

    const/16 v4, 0x6d

    sget-object v5, Lcom/sec/android/service/health/connectionmanager2/MethodType;->GET:Lcom/sec/android/service/health/connectionmanager2/MethodType;

    const-string/jumbo v6, "v2/shealth/phr/dbrestore"

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v10, 0x0

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->getDefaultHeader()Ljava/util/HashMap;

    move-result-object v11

    move-object/from16 v3, p0

    move-object/from16 v9, p0

    invoke-virtual/range {v2 .. v11}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->placeRequest(Ljava/lang/Object;ILcom/sec/android/service/health/connectionmanager2/MethodType;Ljava/lang/String;Lcom/sec/android/service/health/connectionmanager2/RequestParam;Ljava/lang/String;Lcom/sec/android/service/health/connectionmanager2/IOnResponseListener;Ljava/lang/Object;Ljava/util/HashMap;)J

    move-result-wide v2

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->currentRequestId:J

    goto/16 :goto_0

    .line 679
    .end local v19    # "appCfgversion":I
    .restart local p4    # "response":Ljava/lang/Object;
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mTransactionID:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 684
    check-cast p4, Ljava/lang/String;

    .end local p4    # "response":Ljava/lang/Object;
    const-class v2, Lcom/sec/android/app/shealth/backup/server/data/ResponseDBRestore;

    move-object/from16 v0, v21

    move-object/from16 v1, p4

    invoke-virtual {v0, v1, v2}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/sec/android/app/shealth/backup/server/data/ResponseDBRestore;

    .line 685
    .local v24, "res":Lcom/sec/android/app/shealth/backup/server/data/ResponseDBRestore;
    if-nez v24, :cond_11

    .line 687
    new-instance v6, Lcom/sec/android/service/health/connectionmanager2/NetException;

    const/4 v2, -0x6

    const/16 v3, 0x1f4

    invoke-direct {v6, v2, v3}, Lcom/sec/android/service/health/connectionmanager2/NetException;-><init>(II)V

    const/4 v8, 0x0

    move-object/from16 v2, p0

    move-wide/from16 v3, p1

    move/from16 v5, p3

    move-object/from16 v7, p5

    invoke-virtual/range {v2 .. v8}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->onExceptionReceived(JILcom/sec/android/service/health/connectionmanager2/NetException;Ljava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 692
    :cond_11
    invoke-virtual/range {v24 .. v24}, Lcom/sec/android/app/shealth/backup/server/data/ResponseDBRestore;->checkIsResCodeOK()Z

    move-result v2

    if-nez v2, :cond_12

    .line 694
    new-instance v6, Lcom/sec/android/service/health/connectionmanager2/NetException;

    const/4 v2, -0x6

    invoke-virtual/range {v24 .. v24}, Lcom/sec/android/app/shealth/backup/server/data/ResponseDBRestore;->getResCode()I

    move-result v3

    invoke-direct {v6, v2, v3}, Lcom/sec/android/service/health/connectionmanager2/NetException;-><init>(II)V

    const/4 v8, 0x0

    move-object/from16 v2, p0

    move-wide/from16 v3, p1

    move/from16 v5, p3

    move-object/from16 v7, p5

    invoke-virtual/range {v2 .. v8}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->onExceptionReceived(JILcom/sec/android/service/health/connectionmanager2/NetException;Ljava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 699
    :cond_12
    const/4 v11, 0x0

    .line 702
    .local v11, "fileOutputStream":Ljava/io/FileOutputStream;
    :try_start_1
    sget-object v2, Lcom/sec/android/app/shealth/backup/server/RestoreManager$SyncStage;->DATABASE:Lcom/sec/android/app/shealth/backup/server/RestoreManager$SyncStage;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mCurrentStage:Lcom/sec/android/app/shealth/backup/server/RestoreManager$SyncStage;

    .line 703
    new-instance v6, Ljava/io/FileOutputStream;

    const-string v2, "/data/data/com.sec.android.app.shealth/databases/cache_shealth2.db"

    invoke-direct {v6, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_4

    .line 704
    .end local v11    # "fileOutputStream":Ljava/io/FileOutputStream;
    .local v6, "fileOutputStream":Ljava/io/FileOutputStream;
    :try_start_2
    const-string v2, "ServerMgrHttps"

    invoke-static {v2}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;->getInstance(Ljava/lang/String;)Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    move-result-object v2

    const/16 v3, 0x70

    move-object/from16 v0, v24

    iget-object v5, v0, Lcom/sec/android/app/shealth/backup/server/data/ResponseDBRestore;->dburl:Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v4, p0

    invoke-virtual/range {v2 .. v8}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->downloadPublicUrl(ILcom/sec/android/service/health/connectionmanager2/IOnDownloadListener;Ljava/lang/String;Ljava/io/OutputStream;Ljava/lang/Object;Ljava/util/ArrayList;)J

    move-result-wide v2

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->currentRequestId:J
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_0

    .line 706
    :catch_1
    move-exception v20

    .line 708
    .local v20, "e":Ljava/io/FileNotFoundException;
    :goto_3
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->stopRestore()V

    .line 709
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mRestoreListener:Lcom/sec/android/app/shealth/backup/server/IRestoreListener;

    move-object/from16 v0, v20

    invoke-interface {v2, v0}, Lcom/sec/android/app/shealth/backup/server/IRestoreListener;->onException(Ljava/lang/Exception;)V

    goto/16 :goto_0

    .line 716
    .end local v6    # "fileOutputStream":Ljava/io/FileOutputStream;
    .end local v20    # "e":Ljava/io/FileNotFoundException;
    .end local v24    # "res":Lcom/sec/android/app/shealth/backup/server/data/ResponseDBRestore;
    .restart local p4    # "response":Ljava/lang/Object;
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mTransactionID:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 721
    check-cast p4, Ljava/lang/String;

    .end local p4    # "response":Ljava/lang/Object;
    const-class v2, Lcom/sec/android/app/shealth/backup/server/data/ResponseCfgRestore;

    move-object/from16 v0, v21

    move-object/from16 v1, p4

    invoke-virtual {v0, v1, v2}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/sec/android/app/shealth/backup/server/data/ResponseCfgRestore;

    .line 722
    .local v24, "res":Lcom/sec/android/app/shealth/backup/server/data/ResponseCfgRestore;
    if-nez v24, :cond_13

    .line 724
    new-instance v11, Lcom/sec/android/service/health/connectionmanager2/NetException;

    const/4 v2, -0x6

    const/16 v3, 0x1f4

    invoke-direct {v11, v2, v3}, Lcom/sec/android/service/health/connectionmanager2/NetException;-><init>(II)V

    const/4 v13, 0x0

    move-object/from16 v7, p0

    move-wide/from16 v8, p1

    move/from16 v10, p3

    move-object/from16 v12, p5

    invoke-virtual/range {v7 .. v13}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->onExceptionReceived(JILcom/sec/android/service/health/connectionmanager2/NetException;Ljava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 729
    :cond_13
    invoke-virtual/range {v24 .. v24}, Lcom/sec/android/app/shealth/backup/server/data/ResponseCfgRestore;->checkIsResCodeOK()Z

    move-result v2

    if-nez v2, :cond_14

    .line 731
    new-instance v11, Lcom/sec/android/service/health/connectionmanager2/NetException;

    const/4 v2, -0x6

    invoke-virtual/range {v24 .. v24}, Lcom/sec/android/app/shealth/backup/server/data/ResponseCfgRestore;->getResCode()I

    move-result v3

    invoke-direct {v11, v2, v3}, Lcom/sec/android/service/health/connectionmanager2/NetException;-><init>(II)V

    const/4 v13, 0x0

    move-object/from16 v7, p0

    move-wide/from16 v8, p1

    move/from16 v10, p3

    move-object/from16 v12, p5

    invoke-virtual/range {v7 .. v13}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->onExceptionReceived(JILcom/sec/android/service/health/connectionmanager2/NetException;Ljava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 736
    :cond_14
    const/4 v6, 0x0

    .line 739
    .restart local v6    # "fileOutputStream":Ljava/io/FileOutputStream;
    :try_start_3
    sget-object v2, Lcom/sec/android/app/shealth/backup/server/RestoreManager$SyncStage;->PREFERENCE:Lcom/sec/android/app/shealth/backup/server/RestoreManager$SyncStage;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mCurrentStage:Lcom/sec/android/app/shealth/backup/server/RestoreManager$SyncStage;

    .line 740
    new-instance v11, Ljava/io/FileOutputStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/sec/android/app/shealth/common/utils/constants/FilePathsConstants;->SHAREDPREF_FOLDER_PATH:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "cache_shared_prefs.xml"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v11, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_3

    .line 741
    .end local v6    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v11    # "fileOutputStream":Ljava/io/FileOutputStream;
    :try_start_4
    const-string v2, "ServerMgrHttps"

    invoke-static {v2}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;->getInstance(Ljava/lang/String;)Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    move-result-object v7

    const/16 v8, 0x71

    move-object/from16 v0, v24

    iget-object v10, v0, Lcom/sec/android/app/shealth/backup/server/data/ResponseCfgRestore;->cfgurl:Ljava/lang/String;

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object/from16 v9, p0

    invoke-virtual/range {v7 .. v13}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->downloadPublicUrl(ILcom/sec/android/service/health/connectionmanager2/IOnDownloadListener;Ljava/lang/String;Ljava/io/OutputStream;Ljava/lang/Object;Ljava/util/ArrayList;)J

    move-result-wide v2

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->currentRequestId:J
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_2

    goto/16 :goto_0

    .line 743
    :catch_2
    move-exception v20

    move-object v6, v11

    .line 745
    .end local v11    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v6    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v20    # "e":Ljava/io/FileNotFoundException;
    :goto_4
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->stopRestore()V

    .line 746
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mRestoreListener:Lcom/sec/android/app/shealth/backup/server/IRestoreListener;

    move-object/from16 v0, v20

    invoke-interface {v2, v0}, Lcom/sec/android/app/shealth/backup/server/IRestoreListener;->onException(Ljava/lang/Exception;)V

    goto/16 :goto_0

    .line 752
    .end local v6    # "fileOutputStream":Ljava/io/FileOutputStream;
    .end local v20    # "e":Ljava/io/FileNotFoundException;
    .end local v24    # "res":Lcom/sec/android/app/shealth/backup/server/data/ResponseCfgRestore;
    .restart local p4    # "response":Ljava/lang/Object;
    :pswitch_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mTransactionID:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 757
    check-cast p4, Ljava/lang/String;

    .end local p4    # "response":Ljava/lang/Object;
    const-class v2, Lcom/sec/android/app/shealth/backup/server/data/ResponseImage;

    move-object/from16 v0, v21

    move-object/from16 v1, p4

    invoke-virtual {v0, v1, v2}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/backup/server/data/ResponseImage;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->resImage:Lcom/sec/android/app/shealth/backup/server/data/ResponseImage;

    .line 758
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->resImage:Lcom/sec/android/app/shealth/backup/server/data/ResponseImage;

    if-nez v2, :cond_15

    .line 760
    new-instance v16, Lcom/sec/android/service/health/connectionmanager2/NetException;

    const/4 v2, -0x6

    const/16 v3, 0x1f4

    move-object/from16 v0, v16

    invoke-direct {v0, v2, v3}, Lcom/sec/android/service/health/connectionmanager2/NetException;-><init>(II)V

    const/16 v18, 0x0

    move-object/from16 v12, p0

    move-wide/from16 v13, p1

    move/from16 v15, p3

    move-object/from16 v17, p5

    invoke-virtual/range {v12 .. v18}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->onExceptionReceived(JILcom/sec/android/service/health/connectionmanager2/NetException;Ljava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 765
    :cond_15
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->resImage:Lcom/sec/android/app/shealth/backup/server/data/ResponseImage;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/backup/server/data/ResponseImage;->checkIsResCodeOK()Z

    move-result v2

    if-nez v2, :cond_16

    .line 767
    new-instance v16, Lcom/sec/android/service/health/connectionmanager2/NetException;

    const/4 v2, -0x6

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->resImage:Lcom/sec/android/app/shealth/backup/server/data/ResponseImage;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/backup/server/data/ResponseImage;->getResCode()I

    move-result v3

    move-object/from16 v0, v16

    invoke-direct {v0, v2, v3}, Lcom/sec/android/service/health/connectionmanager2/NetException;-><init>(II)V

    const/16 v18, 0x0

    move-object/from16 v12, p0

    move-wide/from16 v13, p1

    move/from16 v15, p3

    move-object/from16 v17, p5

    invoke-virtual/range {v12 .. v18}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->onExceptionReceived(JILcom/sec/android/service/health/connectionmanager2/NetException;Ljava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 772
    :cond_16
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->requestToDownloadImage()Z

    goto/16 :goto_0

    .line 743
    .restart local v6    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v24    # "res":Lcom/sec/android/app/shealth/backup/server/data/ResponseCfgRestore;
    :catch_3
    move-exception v20

    goto :goto_4

    .line 706
    .end local v6    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v11    # "fileOutputStream":Ljava/io/FileOutputStream;
    .local v24, "res":Lcom/sec/android/app/shealth/backup/server/data/ResponseDBRestore;
    :catch_4
    move-exception v20

    move-object v6, v11

    .end local v11    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v6    # "fileOutputStream":Ljava/io/FileOutputStream;
    goto/16 :goto_3

    .line 498
    :pswitch_data_0
    .packed-switch 0x6b
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public resume()V
    .locals 10

    .prologue
    const/4 v5, 0x0

    .line 197
    sget-object v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "resume()"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 198
    const-string v0, "ServerMgrHttps"

    invoke-static {v0}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;->getInstance(Ljava/lang/String;)Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    move-result-object v0

    const/16 v2, 0x6d

    sget-object v3, Lcom/sec/android/service/health/connectionmanager2/MethodType;->GET:Lcom/sec/android/service/health/connectionmanager2/MethodType;

    const-string/jumbo v4, "v2/shealth/phr/dbrestore"

    invoke-direct {p0}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->getDefaultHeader()Ljava/util/HashMap;

    move-result-object v9

    move-object v1, p0

    move-object v6, v5

    move-object v7, p0

    move-object v8, v5

    invoke-virtual/range {v0 .. v9}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->placeRequest(Ljava/lang/Object;ILcom/sec/android/service/health/connectionmanager2/MethodType;Ljava/lang/String;Lcom/sec/android/service/health/connectionmanager2/RequestParam;Ljava/lang/String;Lcom/sec/android/service/health/connectionmanager2/IOnResponseListener;Ljava/lang/Object;Ljava/util/HashMap;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->currentRequestId:J

    .line 199
    return-void
.end method

.method public start(Lcom/sec/android/app/shealth/backup/server/IRestoreListener;)Z
    .locals 5
    .param p1, "listener"    # Lcom/sec/android/app/shealth/backup/server/IRestoreListener;

    .prologue
    const/4 v0, 0x0

    .line 126
    sget-object v1, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "start()"

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    if-nez p1, :cond_0

    .line 179
    :goto_0
    return v0

    .line 132
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->restoreStarted()V

    .line 134
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->isCheckingDataToRestore:Z

    .line 135
    iput-object p1, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mRestoreListener:Lcom/sec/android/app/shealth/backup/server/IRestoreListener;

    .line 136
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mTransactionID:Ljava/lang/String;

    .line 137
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mTryToRenewToken:Z

    .line 138
    invoke-static {}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->getInstance()Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mContext:Landroid/content/Context;

    const-string v2, "1y90e30264"

    const-string v3, "80E7ECD9D301CB7888C73703639302E5"

    new-instance v4, Lcom/sec/android/app/shealth/backup/server/RestoreManager$1;

    invoke-direct {v4, p0}, Lcom/sec/android/app/shealth/backup/server/RestoreManager$1;-><init>(Lcom/sec/android/app/shealth/backup/server/RestoreManager;)V

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->sendRequestForUserToken(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/service/health/samsungaccount/ISamsungUserTokenListener;)V

    .line 179
    const/4 v0, 0x1

    goto :goto_0
.end method
