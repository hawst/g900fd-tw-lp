.class final Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$StartPreviewRunnable;
.super Ljava/lang/Object;
.source "BarcodeScanner.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "StartPreviewRunnable"
.end annotation


# instance fields
.field private isFocusingEnabled:Z

.field final synthetic this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;)V
    .locals 1

    .prologue
    .line 402
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$StartPreviewRunnable;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 403
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$StartPreviewRunnable;->isFocusingEnabled:Z

    .line 404
    return-void
.end method

.method private constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;Z)V
    .locals 0
    .param p2, "isEnnableFoccusing"    # Z

    .prologue
    .line 406
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$StartPreviewRunnable;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 407
    iput-boolean p2, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$StartPreviewRunnable;->isFocusingEnabled:Z

    .line 408
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;ZLcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;
    .param p2, "x1"    # Z
    .param p3, "x2"    # Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$1;

    .prologue
    .line 399
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$StartPreviewRunnable;-><init>(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;Z)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 412
    sget-object v3, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v4, "starting preview"

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 413
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$StartPreviewRunnable;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->focusStateLock:Ljava/lang/Object;
    invoke-static {v3}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->access$1100(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;)Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    .line 414
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$StartPreviewRunnable;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;

    sget-object v5, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;->IDLE:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;

    # setter for: Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->focusState:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;
    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->access$1202(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;)Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;

    .line 415
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 417
    :try_start_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$StartPreviewRunnable;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->camera:Landroid/hardware/Camera;
    invoke-static {v3}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->access$700(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;)Landroid/hardware/Camera;

    move-result-object v3

    invoke-virtual {v3}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v1

    .line 418
    .local v1, "parameters":Landroid/hardware/Camera$Parameters;
    const-string/jumbo v3, "macro"

    invoke-virtual {v1, v3}, Landroid/hardware/Camera$Parameters;->setFocusMode(Ljava/lang/String;)V

    .line 419
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$StartPreviewRunnable;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;

    iget-object v4, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$StartPreviewRunnable;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;

    iget-object v5, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$StartPreviewRunnable;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->getWidth()I

    move-result v5

    iget-object v6, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$StartPreviewRunnable;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->getHeight()I

    move-result v6

    # invokes: Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->getRatio(II)D
    invoke-static {v4, v5, v6}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->access$1300(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;II)D

    move-result-wide v4

    # invokes: Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->findClosestPreviewSize(DLandroid/hardware/Camera$Parameters;)V
    invoke-static {v3, v4, v5, v1}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->access$1400(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;DLandroid/hardware/Camera$Parameters;)V

    .line 420
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$StartPreviewRunnable;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->camera:Landroid/hardware/Camera;
    invoke-static {v3}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->access$700(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;)Landroid/hardware/Camera;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    .line 421
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$StartPreviewRunnable;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->camera:Landroid/hardware/Camera;
    invoke-static {v3}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->access$700(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;)Landroid/hardware/Camera;

    move-result-object v3

    iget-boolean v4, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$StartPreviewRunnable;->isFocusingEnabled:Z

    if-eqz v4, :cond_0

    new-instance v2, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$BarcodePreviewCallback;

    iget-object v4, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$StartPreviewRunnable;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;

    const/4 v5, 0x0

    invoke-direct {v2, v4, v5}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$BarcodePreviewCallback;-><init>(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$1;)V

    :cond_0
    invoke-virtual {v3, v2}, Landroid/hardware/Camera;->setPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V

    .line 422
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$StartPreviewRunnable;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;

    # invokes: Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->getDisplayRotation()I
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->access$1600(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 430
    :goto_0
    :pswitch_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$StartPreviewRunnable;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->camera:Landroid/hardware/Camera;
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->access$700(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;)Landroid/hardware/Camera;

    move-result-object v2

    invoke-virtual {v2}, Landroid/hardware/Camera;->startPreview()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 435
    .end local v1    # "parameters":Landroid/hardware/Camera$Parameters;
    :goto_1
    return-void

    .line 415
    :catchall_0
    move-exception v2

    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    .line 424
    .restart local v1    # "parameters":Landroid/hardware/Camera$Parameters;
    :pswitch_1
    :try_start_3
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$StartPreviewRunnable;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->camera:Landroid/hardware/Camera;
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->access$700(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;)Landroid/hardware/Camera;

    move-result-object v2

    const/16 v3, 0x5a

    invoke-virtual {v2, v3}, Landroid/hardware/Camera;->setDisplayOrientation(I)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    .line 431
    .end local v1    # "parameters":Landroid/hardware/Camera$Parameters;
    :catch_0
    move-exception v0

    .line 432
    .local v0, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->LOG_TAG:Ljava/lang/String;

    const-string v3, "camera preview failed"

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/Logging;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 433
    sget-object v2, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->LOG_TAG:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 427
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "parameters":Landroid/hardware/Camera$Parameters;
    :pswitch_2
    :try_start_4
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$StartPreviewRunnable;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->camera:Landroid/hardware/Camera;
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->access$700(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;)Landroid/hardware/Camera;

    move-result-object v2

    const/16 v3, 0xb4

    invoke-virtual {v2, v3}, Landroid/hardware/Camera;->setDisplayOrientation(I)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_0

    .line 422
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
