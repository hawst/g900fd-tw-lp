.class public Lcom/sec/android/app/shealth/framework/ui/gesture/GestureDetectorLinearLayout;
.super Landroid/widget/LinearLayout;


# instance fields
.field private enabled:Z

.field private observers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/framework/ui/gesture/GestureObserver;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const/4 v1, 0x1

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/gesture/GestureDetectorLinearLayout;->observers:Ljava/util/List;

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/framework/ui/gesture/GestureDetectorLinearLayout;->enabled:Z

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/framework/ui/gesture/GestureDetectorLinearLayout;->setClickable(Z)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    const/4 v1, 0x1

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/gesture/GestureDetectorLinearLayout;->observers:Ljava/util/List;

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/framework/ui/gesture/GestureDetectorLinearLayout;->enabled:Z

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/framework/ui/gesture/GestureDetectorLinearLayout;->setClickable(Z)V

    return-void
.end method

.method private observeTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/gesture/GestureDetectorLinearLayout;->enabled:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/gesture/GestureDetectorLinearLayout;->observers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/gesture/GestureObserver;

    invoke-interface {v0, p1}, Lcom/sec/android/app/shealth/framework/ui/gesture/GestureObserver;->observeTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public addObserver(Lcom/sec/android/app/shealth/framework/ui/gesture/GestureObserver;)V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/gesture/GestureDetectorLinearLayout;->observers:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/gesture/GestureDetectorLinearLayout;->observeTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/gesture/GestureDetectorLinearLayout;->observeTouchEvent(Landroid/view/MotionEvent;)Z

    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public setGesturesEnabled(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/sec/android/app/shealth/framework/ui/gesture/GestureDetectorLinearLayout;->enabled:Z

    return-void
.end method
