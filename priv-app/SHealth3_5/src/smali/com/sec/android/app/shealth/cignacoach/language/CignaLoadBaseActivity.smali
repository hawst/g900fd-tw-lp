.class public abstract Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadBaseActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;
.source "CignaLoadBaseActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils$OnChangeLanguageListener;
.implements Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$OnCoachRestoreSuccessListener;


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadBaseActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadBaseActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;-><init>()V

    return-void
.end method

.method private startCignaCoachActivity()V
    .locals 3

    .prologue
    .line 63
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 64
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x24000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 65
    const-string v1, "com.sec.android.app.shealth.cignacoach.CIGNA_COACH"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 66
    const-string v1, "EXTRA_NAME_FORCED_CIGNA_SCORE_SCREEN"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 67
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadBaseActivity;->startActivity(Landroid/content/Intent;)V

    .line 68
    return-void
.end method


# virtual methods
.method public onChangeLanguage(Z)V
    .locals 3
    .param p1, "changeLanguage"    # Z

    .prologue
    .line 52
    if-eqz p1, :cond_0

    .line 53
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadBaseActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_NAME_CHANGE_LANGUAGE"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 55
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 21
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 23
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->finishIfCignaUnsupported(Landroid/app/Activity;)V

    .line 25
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;

    move-result-object v2

    invoke-virtual {v2, p0, p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;->registerReceiver(Landroid/content/Context;Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils$OnChangeLanguageListener;)V

    .line 27
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadBaseActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 28
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "EXTRA_NAME_CHANGE_LANGUAGE"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 29
    .local v0, "changeLanguage":Z
    sget-object v2, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadBaseActivity;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "CignaLoadBaseActivity onCreate() changeLanguage: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 32
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadBaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0009

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 33
    if-eqz v0, :cond_0

    .line 34
    const-string v2, "EXTRA_NAME_CHANGE_LANGUAGE"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 35
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadBaseActivity;->startCignaCoachActivity()V

    .line 39
    :cond_0
    invoke-static {p0}, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService;->registerRestoreSuccessListener(Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$OnCoachRestoreSuccessListener;)V

    .line 40
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 44
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onDestroy()V

    .line 46
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;

    move-result-object v0

    invoke-virtual {v0, p0, p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;->unRegisterReceiver(Landroid/content/Context;Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils$OnChangeLanguageListener;)V

    .line 47
    invoke-static {p0}, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService;->unregisterRestoreSuccessListener(Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$OnCoachRestoreSuccessListener;)V

    .line 48
    return-void
.end method

.method public onRestoreSuccess()V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadBaseActivity;->startCignaCoachActivity()V

    .line 60
    return-void
.end method
