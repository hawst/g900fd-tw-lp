.class public Lcom/sec/android/app/shealth/logutils/log/LogListGroupHeader;
.super Landroid/widget/RelativeLayout;
.source "LogListGroupHeader.java"


# instance fields
.field private mExpandButton:Landroid/widget/ImageButton;

.field private mTextRightView:Landroid/widget/TextView;

.field private mTextView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 35
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 43
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 44
    return-void
.end method


# virtual methods
.method public dispatchSetSelected(Z)V
    .locals 0
    .param p1, "selected"    # Z

    .prologue
    .line 86
    return-void
.end method

.method public getExpandButton()Landroid/widget/ImageButton;
    .locals 2

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/log/LogListGroupHeader;->mExpandButton:Landroid/widget/ImageButton;

    if-nez v0, :cond_0

    .line 77
    sget v0, Lcom/sec/android/app/shealth/logutils/R$id;->log_list_expand_button:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/logutils/log/LogListGroupHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/android/app/shealth/logutils/log/LogListGroupHeader;->mExpandButton:Landroid/widget/ImageButton;

    .line 78
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/log/LogListGroupHeader;->mExpandButton:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 80
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/log/LogListGroupHeader;->mExpandButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method public getRightTextView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/log/LogListGroupHeader;->mTextRightView:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 65
    sget v0, Lcom/sec/android/app/shealth/logutils/R$id;->log_list_right_groupe_text:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/logutils/log/LogListGroupHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/logutils/log/LogListGroupHeader;->mTextRightView:Landroid/widget/TextView;

    .line 67
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/log/LogListGroupHeader;->mTextRightView:Landroid/widget/TextView;

    return-object v0
.end method

.method public getTextView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/log/LogListGroupHeader;->mTextView:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 53
    sget v0, Lcom/sec/android/app/shealth/logutils/R$id;->log_list_group_header_left_text:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/logutils/log/LogListGroupHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/logutils/log/LogListGroupHeader;->mTextView:Landroid/widget/TextView;

    .line 55
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/log/LogListGroupHeader;->mTextView:Landroid/widget/TextView;

    return-object v0
.end method
