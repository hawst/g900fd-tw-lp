.class public final enum Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$Mask;
.super Ljava/lang/Enum;
.source "FoodTrackerMainContainer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Mask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$Mask;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$Mask;

.field public static final enum FOUR_IMAGE_MASK:Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$Mask;

.field public static final enum ONE_IMAGE_MASK:Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$Mask;

.field public static final enum THREE_IMAGE_MASK:Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$Mask;

.field public static final enum TWO_IMAGE_MASK:Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$Mask;


# instance fields
.field private mMaskId:I

.field private mSquaresCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x0

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 835
    new-instance v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$Mask;

    const-string v1, "ONE_IMAGE_MASK"

    const v2, 0x7f020647

    invoke-direct {v0, v1, v6, v3, v2}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$Mask;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$Mask;->ONE_IMAGE_MASK:Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$Mask;

    .line 837
    new-instance v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$Mask;

    const-string v1, "TWO_IMAGE_MASK"

    const v2, 0x7f020648

    invoke-direct {v0, v1, v3, v4, v2}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$Mask;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$Mask;->TWO_IMAGE_MASK:Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$Mask;

    .line 839
    new-instance v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$Mask;

    const-string v1, "THREE_IMAGE_MASK"

    const v2, 0x7f020649

    invoke-direct {v0, v1, v4, v5, v2}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$Mask;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$Mask;->THREE_IMAGE_MASK:Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$Mask;

    .line 841
    new-instance v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$Mask;

    const-string v1, "FOUR_IMAGE_MASK"

    const v2, 0x7f02064a

    invoke-direct {v0, v1, v5, v7, v2}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$Mask;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$Mask;->FOUR_IMAGE_MASK:Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$Mask;

    .line 833
    new-array v0, v7, [Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$Mask;

    sget-object v1, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$Mask;->ONE_IMAGE_MASK:Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$Mask;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$Mask;->TWO_IMAGE_MASK:Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$Mask;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$Mask;->THREE_IMAGE_MASK:Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$Mask;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$Mask;->FOUR_IMAGE_MASK:Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$Mask;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$Mask;->$VALUES:[Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$Mask;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3, "squaresCount"    # I
    .param p4, "maskId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 851
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 853
    iput p3, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$Mask;->mSquaresCount:I

    .line 855
    iput p4, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$Mask;->mMaskId:I

    .line 857
    return-void
.end method

.method public static getIdBySquaresCount(I)I
    .locals 8
    .param p0, "count"    # I

    .prologue
    .line 863
    if-gtz p0, :cond_0

    .line 865
    new-instance v6, Ljava/lang/IllegalArgumentException;

    const-string v7, "Squares count should be > 0"

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 869
    :cond_0
    const v1, 0x7fffffff

    .line 871
    .local v1, "delta":I
    const/4 v5, -0x1

    .line 873
    .local v5, "resultId":I
    invoke-static {}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$Mask;->values()[Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$Mask;

    move-result-object v0

    .local v0, "arr$":[Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$Mask;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_4

    aget-object v4, v0, v2

    .line 875
    .local v4, "mask":Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$Mask;
    iget v6, v4, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$Mask;->mSquaresCount:I

    if-ne v6, p0, :cond_2

    .line 877
    iget v5, v4, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$Mask;->mMaskId:I

    .line 895
    .end local v4    # "mask":Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$Mask;
    .end local v5    # "resultId":I
    :cond_1
    return v5

    .line 879
    .restart local v4    # "mask":Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$Mask;
    .restart local v5    # "resultId":I
    :cond_2
    iget v6, v4, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$Mask;->mSquaresCount:I

    sub-int/2addr v6, p0

    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v6

    if-ge v6, v1, :cond_3

    .line 881
    iget v6, v4, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$Mask;->mSquaresCount:I

    sub-int/2addr v6, p0

    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v1

    .line 883
    iget v5, v4, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$Mask;->mMaskId:I

    .line 873
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 889
    .end local v4    # "mask":Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$Mask;
    :cond_4
    const/4 v6, -0x1

    if-ne v5, v6, :cond_1

    .line 891
    new-instance v6, Ljava/lang/IllegalStateException;

    const-string v7, "Id of mask resource not found"

    invoke-direct {v6, v7}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v6
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$Mask;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 833
    const-class v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$Mask;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$Mask;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$Mask;
    .locals 1

    .prologue
    .line 833
    sget-object v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$Mask;->$VALUES:[Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$Mask;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$Mask;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$Mask;

    return-object v0
.end method
