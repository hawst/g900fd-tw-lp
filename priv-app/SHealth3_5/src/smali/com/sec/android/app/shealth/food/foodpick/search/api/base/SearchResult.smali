.class public Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;
.super Ljava/lang/Object;
.source "SearchResult.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Result:",
        "Ljava/lang/Object;",
        "Request:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private mRequest:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TRequest;"
        }
    .end annotation
.end field

.field private final mSearchResult:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TResult;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TResult;)V"
        }
    .end annotation

    .prologue
    .line 26
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;, "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult<TResult;TRequest;>;"
    .local p1, "searchResult":Ljava/lang/Object;, "TResult;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;->mSearchResult:Ljava/lang/Object;

    .line 28
    return-void
.end method


# virtual methods
.method public getRequest()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TRequest;"
        }
    .end annotation

    .prologue
    .line 41
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;, "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult<TResult;TRequest;>;"
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;->mRequest:Ljava/lang/Object;

    return-object v0
.end method

.method public getSearchResult()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TResult;"
        }
    .end annotation

    .prologue
    .line 34
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;, "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult<TResult;TRequest;>;"
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;->mSearchResult:Ljava/lang/Object;

    return-object v0
.end method

.method public setRequest(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TRequest;)V"
        }
    .end annotation

    .prologue
    .line 48
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;, "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult<TResult;TRequest;>;"
    .local p1, "request":Ljava/lang/Object;, "TRequest;"
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;->mRequest:Ljava/lang/Object;

    .line 49
    return-void
.end method
