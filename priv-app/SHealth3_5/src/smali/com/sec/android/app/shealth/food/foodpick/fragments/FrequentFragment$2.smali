.class Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment$2;
.super Ljava/lang/Object;
.source "FrequentFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;->initListViewAdapter(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter$OnItemClickListener",
        "<",
        "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;)V
    .locals 0

    .prologue
    .line 144
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment$2;->this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;)V
    .locals 2
    .param p1, "listItem"    # Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;

    .prologue
    .line 147
    instance-of v0, p1, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FrequentFoodListItem;

    if-eqz v0, :cond_1

    .line 148
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment$2;->this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;->access$300(Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;)Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/shealth/food/app/UserActionLog;->SELECT_FOOD_IN_FAVORITE_FOOD_LISTS_ITEM:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;->logUserAction(Lcom/sec/android/app/shealth/food/app/UserActionLog;)V

    .line 152
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment$2;->this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;->onFoodInfoSelectedListener:Lcom/sec/android/app/shealth/food/foodpick/expandable/listener/OnFoodInfoSelectedListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;->access$400(Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;)Lcom/sec/android/app/shealth/food/foodpick/expandable/listener/OnFoodInfoSelectedListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment$2;->this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;->onFoodInfoSelectedListener:Lcom/sec/android/app/shealth/food/foodpick/expandable/listener/OnFoodInfoSelectedListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;->access$400(Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;)Lcom/sec/android/app/shealth/food/foodpick/expandable/listener/OnFoodInfoSelectedListener;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;->getFoodInfoItem()Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/listener/OnFoodInfoSelectedListener;->onFoodInfoSelected(Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;)Z

    .line 155
    :cond_0
    return-void

    .line 150
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment$2;->this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;->access$300(Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;)Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/shealth/food/app/UserActionLog;->SELECT_FOOD_IN_MOST_USED_FOOD_LISTS_ITEM:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;->logUserAction(Lcom/sec/android/app/shealth/food/app/UserActionLog;)V

    goto :goto_0
.end method

.method public bridge synthetic onItemClick(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 144
    check-cast p1, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment$2;->onItemClick(Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;)V

    return-void
.end method
