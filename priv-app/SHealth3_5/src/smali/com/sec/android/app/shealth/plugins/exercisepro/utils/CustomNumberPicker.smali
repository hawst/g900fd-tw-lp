.class public Lcom/sec/android/app/shealth/plugins/exercisepro/utils/CustomNumberPicker;
.super Ljava/lang/Object;
.source "CustomNumberPicker.java"


# instance fields
.field private maxVal:F

.field private minVal:F

.field private val:F


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    return-void
.end method


# virtual methods
.method public setMaxValue(F)V
    .locals 0
    .param p1, "val"    # F

    .prologue
    .line 17
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/CustomNumberPicker;->maxVal:F

    .line 18
    return-void
.end method

.method public setMinValue(F)V
    .locals 0
    .param p1, "val"    # F

    .prologue
    .line 21
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/CustomNumberPicker;->minVal:F

    .line 22
    return-void
.end method

.method public setValue(F)V
    .locals 1
    .param p1, "val"    # F

    .prologue
    .line 43
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/CustomNumberPicker;->maxVal:F

    cmpl-float v0, p1, v0

    if-lez v0, :cond_0

    .line 44
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/CustomNumberPicker;->maxVal:F

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/CustomNumberPicker;->val:F

    .line 50
    :goto_0
    return-void

    .line 45
    :cond_0
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/CustomNumberPicker;->minVal:F

    cmpg-float v0, p1, v0

    if-gez v0, :cond_1

    .line 46
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/CustomNumberPicker;->minVal:F

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/CustomNumberPicker;->val:F

    goto :goto_0

    .line 48
    :cond_1
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/CustomNumberPicker;->val:F

    goto :goto_0
.end method
