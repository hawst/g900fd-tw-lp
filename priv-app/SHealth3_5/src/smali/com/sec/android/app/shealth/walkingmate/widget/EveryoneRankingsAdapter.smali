.class public Lcom/sec/android/app/shealth/walkingmate/widget/EveryoneRankingsAdapter;
.super Landroid/widget/BaseAdapter;
.source "EveryoneRankingsAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/walkingmate/widget/EveryoneRankingsAdapter$OnItemClickListener;
    }
.end annotation


# instance fields
.field clickListener:Landroid/view/View$OnClickListener;

.field clickListener_inner:Landroid/view/View$OnClickListener;

.field private mContext:Landroid/content/Context;

.field private mDefBackground:Landroid/graphics/drawable/BitmapDrawable;

.field private mDefaultSrc:Landroid/graphics/drawable/BitmapDrawable;

.field private mItemClickListener:Lcom/sec/android/app/shealth/walkingmate/widget/EveryoneRankingsAdapter$OnItemClickListener;

.field private mMyHolder:Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

.field private mWalkersListItem:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;[Landroid/graphics/drawable/BitmapDrawable;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p3, "myholder"    # Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;
    .param p4, "drawables"    # [Landroid/graphics/drawable/BitmapDrawable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;",
            ">;",
            "Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;",
            "[",
            "Landroid/graphics/drawable/BitmapDrawable;",
            ")V"
        }
    .end annotation

    .prologue
    .line 57
    .local p2, "list":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;>;"
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 110
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/widget/EveryoneRankingsAdapter$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/walkingmate/widget/EveryoneRankingsAdapter$1;-><init>(Lcom/sec/android/app/shealth/walkingmate/widget/EveryoneRankingsAdapter;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/EveryoneRankingsAdapter;->clickListener_inner:Landroid/view/View$OnClickListener;

    .line 120
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/widget/EveryoneRankingsAdapter$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/walkingmate/widget/EveryoneRankingsAdapter$2;-><init>(Lcom/sec/android/app/shealth/walkingmate/widget/EveryoneRankingsAdapter;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/EveryoneRankingsAdapter;->clickListener:Landroid/view/View$OnClickListener;

    .line 58
    iput-object p2, p0, Lcom/sec/android/app/shealth/walkingmate/widget/EveryoneRankingsAdapter;->mWalkersListItem:Ljava/util/List;

    .line 59
    iput-object p3, p0, Lcom/sec/android/app/shealth/walkingmate/widget/EveryoneRankingsAdapter;->mMyHolder:Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

    .line 60
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/EveryoneRankingsAdapter;->mContext:Landroid/content/Context;

    .line 62
    const/4 v0, 0x0

    aget-object v0, p4, v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/EveryoneRankingsAdapter;->mDefaultSrc:Landroid/graphics/drawable/BitmapDrawable;

    .line 63
    const/4 v0, 0x1

    aget-object v0, p4, v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/EveryoneRankingsAdapter;->mDefBackground:Landroid/graphics/drawable/BitmapDrawable;

    .line 65
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/walkingmate/widget/EveryoneRankingsAdapter;)Lcom/sec/android/app/shealth/walkingmate/widget/EveryoneRankingsAdapter$OnItemClickListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/widget/EveryoneRankingsAdapter;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/EveryoneRankingsAdapter;->mItemClickListener:Lcom/sec/android/app/shealth/walkingmate/widget/EveryoneRankingsAdapter$OnItemClickListener;

    return-object v0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/EveryoneRankingsAdapter;->mWalkersListItem:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/EveryoneRankingsAdapter;->mWalkersListItem:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "itemId"    # I

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/EveryoneRankingsAdapter;->mWalkersListItem:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/EveryoneRankingsAdapter;->mWalkersListItem:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "arg0"    # I

    .prologue
    .line 82
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1, "position"    # I
    .param p2, "view"    # Landroid/view/View;
    .param p3, "viewGroup"    # Landroid/view/ViewGroup;

    .prologue
    .line 88
    new-instance v1, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/widget/EveryoneRankingsAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;-><init>(Landroid/content/Context;)V

    .line 89
    .local v1, "item":Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;
    move-object p2, v1

    .line 90
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/widget/EveryoneRankingsAdapter;->mWalkersListItem:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

    .line 91
    .local v0, "holder":Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->getRanking()I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/widget/EveryoneRankingsAdapter;->mMyHolder:Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->getRanking()I

    move-result v3

    if-ne v2, v3, :cond_0

    move-object v2, p2

    .line 92
    check-cast v2, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;

    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/widget/EveryoneRankingsAdapter;->mMyHolder:Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/widget/EveryoneRankingsAdapter;->mMyHolder:Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/widget/EveryoneRankingsAdapter;->mDefaultSrc:Landroid/graphics/drawable/BitmapDrawable;

    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/widget/EveryoneRankingsAdapter;->mDefBackground:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->setValuesHolder(Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;Landroid/graphics/drawable/BitmapDrawable;Landroid/graphics/drawable/BitmapDrawable;)V

    :goto_0
    move-object v2, p2

    .line 96
    check-cast v2, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->setAdditionalState(Z)V

    move-object v2, p2

    .line 97
    check-cast v2, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;

    iget-object v2, v2, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->leader_list_item_layout:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/widget/EveryoneRankingsAdapter;->clickListener_inner:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object v2, p2

    .line 98
    check-cast v2, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;

    iget-object v3, v2, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->leader_list_item_layout:Landroid/widget/RelativeLayout;

    move-object v2, p2

    check-cast v2, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;

    invoke-virtual {v3, v2}, Landroid/widget/RelativeLayout;->setTag(Ljava/lang/Object;)V

    .line 99
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 100
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/widget/EveryoneRankingsAdapter;->clickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {p2, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 101
    return-object p2

    :cond_0
    move-object v2, p2

    .line 94
    check-cast v2, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;

    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/widget/EveryoneRankingsAdapter;->mMyHolder:Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/widget/EveryoneRankingsAdapter;->mDefaultSrc:Landroid/graphics/drawable/BitmapDrawable;

    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/widget/EveryoneRankingsAdapter;->mDefBackground:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v2, v0, v3, v4, v5}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->setValuesHolder(Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;Landroid/graphics/drawable/BitmapDrawable;Landroid/graphics/drawable/BitmapDrawable;)V

    goto :goto_0
.end method

.method public onUpdateItems(Ljava/util/List;Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;)V
    .locals 0
    .param p2, "myholder"    # Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;",
            ">;",
            "Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;",
            ")V"
        }
    .end annotation

    .prologue
    .line 105
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;>;"
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/EveryoneRankingsAdapter;->mWalkersListItem:Ljava/util/List;

    .line 106
    iput-object p2, p0, Lcom/sec/android/app/shealth/walkingmate/widget/EveryoneRankingsAdapter;->mMyHolder:Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

    .line 107
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/widget/EveryoneRankingsAdapter;->notifyDataSetChanged()V

    .line 108
    return-void
.end method

.method public setOnItemClickListener(Lcom/sec/android/app/shealth/walkingmate/widget/EveryoneRankingsAdapter$OnItemClickListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/app/shealth/walkingmate/widget/EveryoneRankingsAdapter$OnItemClickListener;

    .prologue
    .line 53
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/EveryoneRankingsAdapter;->mItemClickListener:Lcom/sec/android/app/shealth/walkingmate/widget/EveryoneRankingsAdapter$OnItemClickListener;

    .line 54
    return-void
.end method
