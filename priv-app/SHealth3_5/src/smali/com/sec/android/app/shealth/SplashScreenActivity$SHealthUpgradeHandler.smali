.class Lcom/sec/android/app/shealth/SplashScreenActivity$SHealthUpgradeHandler;
.super Landroid/os/Handler;
.source "SplashScreenActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/SplashScreenActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SHealthUpgradeHandler"
.end annotation


# instance fields
.field private final mViewReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/sec/android/app/shealth/SplashScreenActivity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/SplashScreenActivity;)V
    .locals 1
    .param p1, "activity"    # Lcom/sec/android/app/shealth/SplashScreenActivity;

    .prologue
    .line 1129
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 1130
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity$SHealthUpgradeHandler;->mViewReference:Ljava/lang/ref/WeakReference;

    .line 1131
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    .line 1135
    iget-object v1, p0, Lcom/sec/android/app/shealth/SplashScreenActivity$SHealthUpgradeHandler;->mViewReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/SplashScreenActivity;

    .line 1136
    .local v0, "activity":Lcom/sec/android/app/shealth/SplashScreenActivity;
    if-eqz v0, :cond_0

    .line 1137
    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/SplashScreenActivity;->handleMessage(Landroid/os/Message;)V

    .line 1139
    :cond_0
    return-void
.end method
