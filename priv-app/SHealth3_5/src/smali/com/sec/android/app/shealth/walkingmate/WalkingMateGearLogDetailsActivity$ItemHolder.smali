.class Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$ItemHolder;
.super Ljava/lang/Object;
.source "WalkingMateGearLogDetailsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ItemHolder"
.end annotation


# instance fields
.field contentDesc:Ljava/lang/String;

.field itemIconResId:I

.field itemName:Ljava/lang/String;

.field itemValue:[Ljava/lang/String;

.field title:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 432
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 420
    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$ItemHolder;->title:Ljava/lang/String;

    .line 433
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$ItemHolder;->title:Ljava/lang/String;

    .line 434
    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$ItemHolder;->itemName:Ljava/lang/String;

    .line 435
    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$ItemHolder;->itemValue:[Ljava/lang/String;

    .line 436
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$ItemHolder;->itemIconResId:I

    .line 437
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$ItemHolder;->contentDesc:Ljava/lang/String;

    .line 439
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;[Ljava/lang/String;ILjava/lang/String;)V
    .locals 2
    .param p1, "itemName"    # Ljava/lang/String;
    .param p2, "itemValue"    # [Ljava/lang/String;
    .param p3, "itemIconResId"    # I
    .param p4, "contentDesc"    # Ljava/lang/String;

    .prologue
    .line 426
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 420
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$ItemHolder;->title:Ljava/lang/String;

    .line 427
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$ItemHolder;->itemName:Ljava/lang/String;

    .line 428
    iput-object p2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$ItemHolder;->itemValue:[Ljava/lang/String;

    .line 429
    iput p3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$ItemHolder;->itemIconResId:I

    .line 430
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    if-eqz p4, :cond_0

    .end local p4    # "contentDesc":Ljava/lang/String;
    :goto_0
    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$ItemHolder;->contentDesc:Ljava/lang/String;

    .line 431
    return-void

    .line 430
    .restart local p4    # "contentDesc":Ljava/lang/String;
    :cond_0
    array-length v1, p2

    add-int/lit8 v1, v1, -0x1

    aget-object p4, p2, v1

    goto :goto_0
.end method

.method synthetic constructor <init>(Ljava/lang/String;[Ljava/lang/String;ILjava/lang/String;Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$1;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/String;
    .param p2, "x1"    # [Ljava/lang/String;
    .param p3, "x2"    # I
    .param p4, "x3"    # Ljava/lang/String;
    .param p5, "x4"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$1;

    .prologue
    .line 419
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$ItemHolder;-><init>(Ljava/lang/String;[Ljava/lang/String;ILjava/lang/String;)V

    return-void
.end method
