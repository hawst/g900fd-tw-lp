.class public final Lcom/sec/android/app/shealth/walkingmate/WalkingStatisticsServerResponseDecoder;
.super Ljava/lang/Object;
.source "WalkingStatisticsServerResponseDecoder.java"


# static fields
.field private static final ACCOUNT_ID:Ljava/lang/String; = "accountId"

.field private static final AGE:Ljava/lang/String; = "age"

.field private static final AGE_RANGE:Ljava/lang/String; = "ageRange"

.field private static final AVG_DISTANCE:Ljava/lang/String; = "avgDistance"

.field private static final AVG_DISTANCE_BY_AGE:Ljava/lang/String; = "avgDistancebyAge"

.field private static final EMPTY_STRING:Ljava/lang/String; = ""

.field private static final FINISH_DELIMITER:Ljava/lang/String; = "}"

.field private static final ITEM_DELIMITER:Ljava/lang/String; = ","

.field private static final LIST_ITEM_AGE_SPLIT_INDEX:I = 0x2

.field private static final LIST_ITEM_BITMAP_SPLIT_INDEX:I = 0x4

.field private static final LIST_ITEM_DELIMITER:Ljava/lang/String; = "#"

.field private static final LIST_ITEM_DISTANCE_SPLIT_INDEX:I = 0x5

.field private static final LIST_ITEM_GENDER_SPLIT_INDEX:I = 0x1

.field private static final LIST_ITEM_NAME_SPLIT_INDEX:I = 0x0

.field private static final LIST_ITEM_POSITION_SPLIT_INDEX:I = 0x6

.field private static final LIST_ITEM_SPLITS_QUANTITY:I = 0x7

.field private static final LIST_ITEM_URL_SPLIT_INDEX:I = 0x3

.field private static final MY_PERCENTILE:Ljava/lang/String; = "myPercentile"

.field private static final MY_RANKING:Ljava/lang/String; = "myRanking"

.field private static final MY_TOTAL_DISTANCE:Ljava/lang/String; = "myTotalDistance"

.field private static final NAME_VALUE_DELIMITER:Ljava/lang/String; = ":"

.field private static final NULL_BITMAP:Ljava/lang/String; = "NULL_BITMAP"

.field private static final REQ_DATE:Ljava/lang/String; = "reqDate"

.field private static final START_DELIMITER:Ljava/lang/String; = "{"

.field private static final STATS_TIME:Ljava/lang/String; = "statsTime"

.field private static final STRING_DELIMITER:Ljava/lang/String; = "\""

.field private static final TOP_DISTANCE:Ljava/lang/String; = "topDistance"

.field private static final TOP_DISTANCE_BY_AGE:Ljava/lang/String; = "topDistancebyAge"

.field private static final TOTAL_USER_NUM:Ljava/lang/String; = "totalUserNum"

.field private static final TOTAL_USER_NUM_BY_AGE:Ljava/lang/String; = "totalUserNumbyAge"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static editStringInCaseOfForbiddenValues(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "itemField"    # Ljava/lang/String;

    .prologue
    .line 83
    const-string v0, "#"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static encodeTopWalkersItemIntoString(Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;)Ljava/lang/String;
    .locals 5
    .param p0, "item"    # Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

    .prologue
    .line 67
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 68
    .local v1, "builder":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->getImageBitmap()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    if-nez v2, :cond_0

    const-string v0, "NULL_BITMAP"

    .line 69
    .local v0, "bitmapString":Ljava/lang/String;
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingStatisticsServerResponseDecoder;->editStringInCaseOfForbiddenValues(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->getGender()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/walkingmate/WalkingStatisticsServerResponseDecoder;->editStringInCaseOfForbiddenValues(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->getAge()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/walkingmate/WalkingStatisticsServerResponseDecoder;->editStringInCaseOfForbiddenValues(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->getIconUrl()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/walkingmate/WalkingStatisticsServerResponseDecoder;->editStringInCaseOfForbiddenValues(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingStatisticsServerResponseDecoder;->editStringInCaseOfForbiddenValues(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->getDistance()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/walkingmate/WalkingStatisticsServerResponseDecoder;->editStringInCaseOfForbiddenValues(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->getRanking()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/walkingmate/WalkingStatisticsServerResponseDecoder;->editStringInCaseOfForbiddenValues(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 76
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 68
    .end local v0    # "bitmapString":Ljava/lang/String;
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->getImageBitmap()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/utils/BitmapUtil;->convertDrawableIntoBitmap(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Bitmap;

    move-result-object v2

    const/16 v3, 0x64

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/walkingmate/utils/BitmapUtil;->encodeBitmapIntoString(Landroid/graphics/Bitmap;I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public static encodeTopWalkersListIntoStringSet(Ljava/util/List;)Ljava/util/Set;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 51
    .local p0, "walkersList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;>;"
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 52
    .local v2, "recoverySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

    .line 53
    .local v1, "item":Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingStatisticsServerResponseDecoder;->encodeTopWalkersItemIntoString(Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 55
    .end local v1    # "item":Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;
    :cond_0
    return-object v2
.end method
