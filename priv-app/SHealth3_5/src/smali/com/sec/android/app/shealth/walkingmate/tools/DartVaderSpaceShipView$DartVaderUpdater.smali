.class Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderUpdater;
.super Landroid/os/AsyncTask;
.source "DartVaderSpaceShipView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DartVaderUpdater"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private finishState:Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;

.field final synthetic this$0:Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView;


# direct methods
.method protected constructor <init>(Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView;Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;)V
    .locals 0
    .param p2, "currentState"    # Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;

    .prologue
    .line 232
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderUpdater;->this$0:Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 233
    iput-object p2, p0, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderUpdater;->finishState:Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;

    .line 234
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 229
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderUpdater;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 2
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 238
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderUpdater;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 240
    const-wide/16 v0, 0x82

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 241
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderUpdater;->publishProgress([Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 242
    :catch_0
    move-exception v0

    goto :goto_0

    .line 245
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 229
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderUpdater;->onProgressUpdate([Ljava/lang/Void;)V

    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Void;)V
    .locals 3
    .param p1, "values"    # [Ljava/lang/Void;

    .prologue
    .line 250
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onProgressUpdate([Ljava/lang/Object;)V

    .line 251
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderUpdater;->this$0:Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView;->state:Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView;->access$100(Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView;)Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderUpdater;->finishState:Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderUpdater;->cancel(Z)Z

    .line 257
    :goto_0
    return-void

    .line 253
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderUpdater;->this$0:Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView;->state:Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView;->access$100(Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView;)Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;->getIndex()I

    move-result v0

    .line 254
    .local v0, "prevIndex":I
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderUpdater;->this$0:Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderUpdater;->this$0:Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView;->state:Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView;->access$100(Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView;)Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;->next()Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;

    move-result-object v2

    # setter for: Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView;->state:Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView;->access$102(Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView;Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;)Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;

    .line 255
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderUpdater;->this$0:Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderUpdater;->this$0:Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView;->state:Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView;->access$100(Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView;)Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;->getIndex()I

    move-result v2

    # invokes: Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView;->updateState(II)V
    invoke-static {v1, v0, v2}, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView;->access$200(Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView;II)V

    goto :goto_0
.end method
