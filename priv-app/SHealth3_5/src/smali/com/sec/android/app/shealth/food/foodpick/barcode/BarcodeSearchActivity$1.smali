.class Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$1;
.super Ljava/util/HashMap;
.source "BarcodeSearchActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashMap",
        "<",
        "Ljava/lang/String;",
        "Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 70
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$1;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;

    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    .line 71
    const-string v0, "barcode_search_success_popup"

    new-instance v1, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$SuccessSearchDialogButtonController;

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$1;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$SuccessSearchDialogButtonController;-><init>(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$1;)V

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    const-string v0, "barcode_search_failed_popup"

    new-instance v1, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$FailedSearchDialogButtonController;

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$1;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$FailedSearchDialogButtonController;-><init>(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$1;)V

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    const-string v0, "db_search_by_barcode_failed_popup"

    new-instance v1, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$IncorrectSearchDialogButtonController;

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$1;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$IncorrectSearchDialogButtonController;-><init>(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$1;)V

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    return-void
.end method
