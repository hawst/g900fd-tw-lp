.class Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$3;
.super Ljava/lang/Object;
.source "HeartrateAddTagActivity.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->intitializeView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mAlertToast:Landroid/widget/Toast;

.field final synthetic this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;)V
    .locals 0

    .prologue
    .line 136
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$3;)Landroid/widget/Toast;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$3;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$3;->mAlertToast:Landroid/widget/Toast;

    return-object v0
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 6
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    const/4 v1, 0x0

    .line 148
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->max_character:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->access$400(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;)I

    move-result v2

    if-lt v0, v2, :cond_1

    .line 149
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->oldText:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->access$500(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->oldText:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->access$500(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->max_character:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->access$400(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;)I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_0

    .line 150
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->access$600(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;)Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->max_character:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->access$400(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;)I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {p1, v1, v2}, Landroid/text/Editable;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/framework/ui/common/MemoView;->setText(Ljava/lang/CharSequence;)V

    .line 151
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->access$700(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;)Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/common/MemoView;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    # setter for: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->oldText:Ljava/lang/String;
    invoke-static {v0, v2}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->access$502(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 152
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->access$800(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;)Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->oldText:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->access$500(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/framework/ui/common/MemoView;->setSelection(I)V

    .line 154
    :cond_0
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v2

    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->oldText:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->access$500(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->oldText:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->access$500(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    move-object v0, p1

    move v4, v1

    invoke-interface/range {v0 .. v5}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;II)Landroid/text/Editable;

    .line 155
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$3;->showMaxAlert()V

    .line 157
    :cond_1
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 141
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->max_character:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->access$400(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;)I

    move-result v1

    if-gt v0, v1, :cond_0

    .line 142
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->oldText:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->access$502(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 144
    :cond_0
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 178
    return-void
.end method

.method protected showMaxAlert()V
    .locals 4

    .prologue
    .line 160
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$3;->mAlertToast:Landroid/widget/Toast;

    if-eqz v1, :cond_0

    .line 161
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$3;->mAlertToast:Landroid/widget/Toast;

    invoke-virtual {v1}, Landroid/widget/Toast;->cancel()V

    .line 164
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;

    const v2, 0x7f0900b2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$3;->mAlertToast:Landroid/widget/Toast;

    .line 165
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$3;->mAlertToast:Landroid/widget/Toast;

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 166
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 167
    .local v0, "handler":Landroid/os/Handler;
    new-instance v1, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$3$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$3$1;-><init>(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$3;)V

    const-wide/16 v2, 0x2bc

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 173
    return-void
.end method
