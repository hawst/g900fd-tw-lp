.class Lcom/sec/android/app/shealth/spo2/SpO2Activity$11;
.super Ljava/lang/Object;
.source "SpO2Activity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/spo2/SpO2Activity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/spo2/SpO2Activity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/spo2/SpO2Activity;)V
    .locals 0

    .prologue
    .line 440
    iput-object p1, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity$11;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 444
    instance-of v1, p1, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 445
    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;

    .line 446
    .local v0, "actionBarButton":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;->getActionButtonIndex()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 462
    .end local v0    # "actionBarButton":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;
    :cond_0
    :goto_0
    return-void

    .line 448
    .restart local v0    # "actionBarButton":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity$11;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->onLogSelected()V

    goto :goto_0

    .line 454
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity$11;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mGraphFragment:Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;
    invoke-static {v1}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->access$700(Lcom/sec/android/app/shealth/spo2/SpO2Activity;)Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->getChartReadyToShown()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 455
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity$11;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    # invokes: Lcom/sec/android/app/shealth/spo2/SpO2Activity;->prepareShareView()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->access$800(Lcom/sec/android/app/shealth/spo2/SpO2Activity;)V

    .line 456
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity$11;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "com.sec.android.app.shealth.spo2"

    const-string v3, "SP10"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 446
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
