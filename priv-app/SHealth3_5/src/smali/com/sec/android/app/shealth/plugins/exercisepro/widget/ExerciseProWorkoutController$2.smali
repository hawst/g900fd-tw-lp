.class Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$2;
.super Ljava/lang/Object;
.source "ExerciseProWorkoutController.java"

# interfaces
.implements Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper$IHealthServiecListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)V
    .locals 0

    .prologue
    .line 314
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected()V
    .locals 2

    .prologue
    .line 318
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->getInstance()Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->getRealtimeService()Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    move-result-object v1

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->mRealtimeService:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$002(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    .line 319
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->registerListener()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)V

    .line 320
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->setHealthServiceStatus()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$300(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)V

    .line 322
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->updateIndoorInProgressPopup()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)V

    .line 323
    return-void
.end method

.method public onServiceDisConnected()V
    .locals 3

    .prologue
    .line 327
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->currentMode:I
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$500(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 328
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->mHealthListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$IHealthControllerListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$600(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$IHealthControllerListener;

    move-result-object v0

    const/4 v1, 0x0

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$IHealthControllerListener;->onChannelStateChanged(ILjava/lang/String;)V

    .line 330
    :cond_0
    return-void
.end method
