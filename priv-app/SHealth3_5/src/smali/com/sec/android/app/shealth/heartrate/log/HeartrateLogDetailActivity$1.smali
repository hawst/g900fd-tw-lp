.class Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity$1;
.super Ljava/lang/Object;
.source "HeartrateLogDetailActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;)V
    .locals 0

    .prologue
    .line 220
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x0

    .line 224
    move-object v0, p1

    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;

    .line 225
    .local v0, "actionBarButton":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;->getActionButtonIndex()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 243
    :goto_0
    return-void

    .line 227
    :pswitch_0
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;

    const-class v4, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;

    invoke-direct {v1, v2, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 228
    .local v1, "intent":Landroid/content/Intent;
    const/high16 v2, 0x10000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 229
    const-string v2, "MODE_KEY"

    const-string v4, "Edit"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 230
    const-string v2, "HEART_RATE_ID_KEY"

    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->mHeartrateData:Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;
    invoke-static {v4}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->access$000(Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;)Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getId()J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 231
    const-string v2, "TIME_DATE_KEY"

    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->mTimeDate:Landroid/widget/TextView;
    invoke-static {v4}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->access$100(Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;)Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 232
    const-string v2, "HEART_RATE_KEY"

    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->mHeartrateData:Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;
    invoke-static {v4}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->access$000(Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;)Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getAverage()F

    move-result v4

    float-to-int v4, v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 233
    const-string v4, "COMMENT_KEY"

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->mHeartrateData:Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->access$000(Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;)Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getComment()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    const-string v2, ""

    :goto_1
    invoke-virtual {v1, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 234
    const-string v4, "TAG_TEXT_ID"

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->tagByHeartrateId:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->access$200(Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;)Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    move-result-object v2

    if-nez v2, :cond_2

    move v2, v3

    :goto_2
    invoke-virtual {v1, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 235
    const-string v4, "TAG_INDEX"

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->mHeartrateData:Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->access$000(Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;)Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getTagIndex()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_3

    const-string v2, ""

    :goto_3
    invoke-virtual {v1, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 236
    const-string v4, "TAG_TEXT"

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->tagByHeartrateId:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->access$200(Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;)Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->tagByHeartrateId:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->access$200(Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;)Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    move-result-object v2

    iget-object v2, v2, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mName:Ljava/lang/String;

    if-nez v2, :cond_4

    :cond_0
    const-string v2, ""

    :goto_4
    invoke-virtual {v1, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 237
    const-string v2, "TAG_ICON_ID"

    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->tagByHeartrateId:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
    invoke-static {v4}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->access$200(Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;)Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    move-result-object v4

    if-nez v4, :cond_5

    :goto_5
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 238
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 233
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->mHeartrateData:Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->access$000(Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;)Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getComment()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 234
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->tagByHeartrateId:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->access$200(Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;)Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    move-result-object v2

    iget v2, v2, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mNameId:I

    goto :goto_2

    .line 235
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->mHeartrateData:Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->access$000(Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;)Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getTagIndex()Ljava/lang/String;

    move-result-object v2

    goto :goto_3

    .line 236
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->tagByHeartrateId:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->access$200(Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;)Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    move-result-object v2

    iget-object v2, v2, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mName:Ljava/lang/String;

    goto :goto_4

    .line 237
    :cond_5
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->tagByHeartrateId:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
    invoke-static {v3}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->access$200(Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;)Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    move-result-object v3

    iget v3, v3, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mIconId:I

    goto :goto_5

    .line 225
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
