.class Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$2;
.super Ljava/lang/Object;
.source "ExerciseProDetailsActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->customizeActionBarWithTabs()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;)V
    .locals 0

    .prologue
    .line 364
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 368
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->currentTab:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->access$500(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;)Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;->DETAIL:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    if-eq v0, v1, :cond_0

    .line 369
    new-array v0, v3, [Landroid/view/View;

    aput-object p1, v0, v2

    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->temporarilyDisableClick([Landroid/view/View;)V

    .line 370
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;

    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;->DETAIL:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->lastActionTab:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->access$602(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;)Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    .line 371
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;

    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;->DETAIL:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->switchToTab(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;)V

    .line 372
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->tab1:Landroid/view/View;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->access$700(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;)Landroid/view/View;

    move-result-object v1

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->setCurrentTabContentDescription(Landroid/view/View;Z)V
    invoke-static {v0, v1, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->access$800(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;Landroid/view/View;Z)V

    .line 373
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->tab2:Landroid/view/View;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->access$900(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;)Landroid/view/View;

    move-result-object v1

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->setCurrentTabContentDescription(Landroid/view/View;Z)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->access$800(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;Landroid/view/View;Z)V

    .line 374
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->tab3:Landroid/view/View;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->access$1000(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;)Landroid/view/View;

    move-result-object v1

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->setCurrentTabContentDescription(Landroid/view/View;Z)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->access$800(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;Landroid/view/View;Z)V

    .line 376
    :cond_0
    return-void
.end method
