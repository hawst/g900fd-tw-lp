.class public Lcom/sec/android/app/shealth/sleepmonitor/SleepCalendarActivity;
.super Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;
.source "SleepCalendarActivity.java"


# static fields
.field private static final QUERY:Ljava/lang/String; = "SELECT a._id, AVG(a.efficiency) AS AVGPERSENT, a.bed_time-b.q as dayDataLong, strftime(\'%Y-%m-%d\', (a.bed_time-b.q)/1000,\'unixepoch\',\'localtime\') AS day FROM sleep AS a LEFT OUTER JOIN( SELECT c._id, abs(RISE_Time - BED_TIME)/(1000*60*60) AS range, CASE WHEN cast(strftime(\'%H\', strftime(\'%Y-%m-%d %H:%m\', bed_time/1000,\'unixepoch\',\'localtime\')) as integer) BETWEEN 0 AND 12 THEN 43200000 ELSE 0 END as q FROM sleep AS c) AS b ON b._id = a._id WHERE sync_status != 170004 GROUP BY day ORDER BY a.rise_time ASC"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected getDaysStatuses(JJ)Ljava/util/TreeMap;
    .locals 14
    .param p1, "startMonthTime"    # J
    .param p3, "endMonthTime"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ)",
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 30
    new-instance v8, Ljava/util/TreeMap;

    invoke-direct {v8}, Ljava/util/TreeMap;-><init>()V

    .line 31
    .local v8, "dayMap":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/Long;Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;>;"
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepCalendarActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const-string v3, "SELECT a._id, AVG(a.efficiency) AS AVGPERSENT, a.bed_time-b.q as dayDataLong, strftime(\'%Y-%m-%d\', (a.bed_time-b.q)/1000,\'unixepoch\',\'localtime\') AS day FROM sleep AS a LEFT OUTER JOIN( SELECT c._id, abs(RISE_Time - BED_TIME)/(1000*60*60) AS range, CASE WHEN cast(strftime(\'%H\', strftime(\'%Y-%m-%d %H:%m\', bed_time/1000,\'unixepoch\',\'localtime\')) as integer) BETWEEN 0 AND 12 THEN 43200000 ELSE 0 END as q FROM sleep AS c) AS b ON b._id = a._id WHERE sync_status != 170004 GROUP BY day ORDER BY a.rise_time ASC"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 33
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_2

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_2

    .line 34
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 36
    :cond_0
    const-string v0, "AVGPERSENT"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v12

    .line 37
    .local v12, "val":D
    const-wide/16 v0, 0x0

    cmpl-double v0, v12, v0

    if-lez v0, :cond_1

    .line 38
    const-string v0, "dayDataLong"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    .line 39
    .local v9, "timeStamp":J
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v11

    .line 40
    .local v11, "uDateOne":Ljava/util/Calendar;
    invoke-virtual {v11, v9, v10}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 41
    new-instance v7, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;

    invoke-direct {v7}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;-><init>()V

    .line 42
    .local v7, "dayInfo":Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;
    const v0, 0x7f020035

    invoke-virtual {v7, v0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;->setMedalResourceId(I)V

    .line 43
    invoke-virtual {v11}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v8, v0, v7}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    .end local v7    # "dayInfo":Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;
    .end local v9    # "timeStamp":J
    .end local v11    # "uDateOne":Ljava/util/Calendar;
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 49
    .end local v12    # "val":D
    :cond_2
    if-eqz v6, :cond_3

    .line 50
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 51
    const/4 v6, 0x0

    .line 54
    :cond_3
    const-string v0, "Day Info"

    const-string v1, "Days Info returned"

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->loggingI(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    return-object v8

    .line 49
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_4

    .line 50
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 51
    const/4 v6, 0x0

    :cond_4
    throw v0
.end method

.method public onPeriodSelected(JIZ)V
    .locals 4
    .param p1, "periodStart"    # J
    .param p3, "periodType"    # I
    .param p4, "containsMeasurements"    # Z

    .prologue
    const/16 v3, 0xb

    .line 62
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 63
    .local v0, "cal":Ljava/util/Calendar;
    invoke-virtual {v0, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 64
    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v1

    const/16 v2, 0xc

    if-ge v1, v2, :cond_0

    .line 65
    const/16 v1, 0xd

    invoke-virtual {v0, v3, v1}, Ljava/util/Calendar;->set(II)V

    .line 66
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide p1

    .line 68
    :cond_0
    invoke-super {p0, p1, p2, p3, p4}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->onPeriodSelected(JIZ)V

    .line 69
    return-void
.end method
