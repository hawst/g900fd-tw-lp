.class public Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;
.super Landroid/app/Dialog;
.source "UvSkinTypeDialog.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog$BroadcastSendingTask;
    }
.end annotation


# instance fields
.field private btnOK:Landroid/widget/Button;

.field private clickListener:Landroid/view/View$OnClickListener;

.field mCheckboxLayoutClickListener:Landroid/view/View$OnClickListener;

.field private mContext:Landroid/content/Context;

.field private mInfoView:Landroid/view/View;

.field private mShowAgainCheckLayout:Landroid/widget/RelativeLayout;

.field public mSkinDatabaseHelper:Lcom/sec/android/app/shealth/uv/common/SkinDatabaseHelper;

.field private mSkinSettings:Landroid/widget/CheckBox;

.field private mSkinType1:Landroid/widget/TextView;

.field private mSkinType2:Landroid/widget/TextView;

.field private mSkinType3:Landroid/widget/TextView;

.field private mSkinType4:Landroid/widget/TextView;

.field private mSkinType5:Landroid/widget/TextView;

.field private mSkinType6:Landroid/widget/TextView;

.field private mSkinTypeDesc1:Landroid/widget/TextView;

.field private mSkinTypeDesc2:Landroid/widget/TextView;

.field private mSkinTypeDesc3:Landroid/widget/TextView;

.field private mSkinTypeDesc4:Landroid/widget/TextView;

.field private mSkinTypeDesc5:Landroid/widget/TextView;

.field private mSkinTypeDesc6:Landroid/widget/TextView;

.field public mUserSkinToneType:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;

.field private mUvQuestion1:Landroid/widget/TextView;

.field private mUvQuestion2:Landroid/widget/TextView;

.field private rb_skintype_1:Landroid/widget/RadioButton;

.field private rb_skintype_2:Landroid/widget/RadioButton;

.field private rb_skintype_3:Landroid/widget/RadioButton;

.field private rb_skintype_4:Landroid/widget/RadioButton;

.field private rb_skintype_5:Landroid/widget/RadioButton;

.field private rb_skintype_6:Landroid/widget/RadioButton;

.field private skinScroll:Landroid/widget/ScrollView;

.field private skinTypeListener:Landroid/view/View$OnClickListener;

.field private uvSkinOption1:Landroid/view/View;

.field private uvSkinOption2:Landroid/view/View;

.field private uvSkinOption3:Landroid/view/View;

.field private uvSkinOption4:Landroid/view/View;

.field private uvSkinOption5:Landroid/view/View;

.field private uvSkinOption6:Landroid/view/View;

.field private uvSkinType1:Landroid/widget/ImageView;

.field private uvSkinType2:Landroid/widget/ImageView;

.field private uvSkinType3:Landroid/widget/ImageView;

.field private uvSkinType4:Landroid/widget/ImageView;

.field private uvSkinType5:Landroid/widget/ImageView;

.field private uvSkinType6:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 61
    const v0, 0x7f0c0081

    invoke-direct {p0, p1, v0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 73
    new-instance v0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog$1;-><init>(Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mCheckboxLayoutClickListener:Landroid/view/View$OnClickListener;

    .line 269
    new-instance v0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog$4;-><init>(Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->skinTypeListener:Landroid/view/View$OnClickListener;

    .line 284
    new-instance v0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog$5;-><init>(Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->clickListener:Landroid/view/View$OnClickListener;

    .line 63
    iput-object p1, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mContext:Landroid/content/Context;

    .line 64
    invoke-static {}, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;->getInstance()Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mUserSkinToneType:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;

    .line 65
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/shealth/uv/common/SkinDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/uv/common/SkinDatabaseHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mSkinDatabaseHelper:Lcom/sec/android/app/shealth/uv/common/SkinDatabaseHelper;

    .line 66
    invoke-direct {p0}, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->initLayout()V

    .line 67
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mInfoView:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->setContentView(Landroid/view/View;)V

    .line 68
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mSkinSettings:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;)Landroid/widget/ScrollView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->skinScroll:Landroid/widget/ScrollView;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;)Landroid/widget/RadioButton;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->rb_skintype_5:Landroid/widget/RadioButton;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;)Landroid/widget/RadioButton;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->rb_skintype_6:Landroid/widget/RadioButton;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;Landroid/view/ViewGroup;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;
    .param p1, "x1"    # Landroid/view/ViewGroup;
    .param p2, "x2"    # Z

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->changeState(Landroid/view/ViewGroup;Z)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->clearSelection()V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->updateButton()V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;)Landroid/widget/RadioButton;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->rb_skintype_1:Landroid/widget/RadioButton;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;)Landroid/widget/RadioButton;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->rb_skintype_2:Landroid/widget/RadioButton;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;)Landroid/widget/RadioButton;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->rb_skintype_3:Landroid/widget/RadioButton;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;)Landroid/widget/RadioButton;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->rb_skintype_4:Landroid/widget/RadioButton;

    return-object v0
.end method

.method private changeState(Landroid/view/ViewGroup;Z)V
    .locals 3
    .param p1, "layout"    # Landroid/view/ViewGroup;
    .param p2, "state"    # Z

    .prologue
    .line 533
    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->setEnabled(Z)V

    .line 534
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 535
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 536
    .local v0, "child":Landroid/view/View;
    instance-of v2, v0, Landroid/view/ViewGroup;

    if-eqz v2, :cond_0

    .line 537
    check-cast v0, Landroid/view/ViewGroup;

    .end local v0    # "child":Landroid/view/View;
    invoke-direct {p0, v0, p2}, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->changeState(Landroid/view/ViewGroup;Z)V

    .line 534
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 539
    .restart local v0    # "child":Landroid/view/View;
    :cond_0
    invoke-virtual {v0, p2}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_1

    .line 543
    .end local v0    # "child":Landroid/view/View;
    :cond_1
    invoke-direct {p0, p2}, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->updateColor(Z)V

    .line 544
    return-void
.end method

.method private clearSelection()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 343
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->rb_skintype_1:Landroid/widget/RadioButton;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 344
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->rb_skintype_2:Landroid/widget/RadioButton;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 345
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->rb_skintype_3:Landroid/widget/RadioButton;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 346
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->rb_skintype_4:Landroid/widget/RadioButton;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 347
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->rb_skintype_5:Landroid/widget/RadioButton;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 348
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->rb_skintype_6:Landroid/widget/RadioButton;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 349
    return-void
.end method

.method private getSkinToneDrawableId(I)I
    .locals 1
    .param p1, "skinTone"    # I

    .prologue
    .line 503
    const/4 v0, -0x1

    .line 505
    .local v0, "id":I
    packed-switch p1, :pswitch_data_0

    .line 528
    :goto_0
    return v0

    .line 507
    :pswitch_0
    const v0, 0x7f080ac6

    .line 508
    goto :goto_0

    .line 510
    :pswitch_1
    const v0, 0x7f080ac8

    .line 511
    goto :goto_0

    .line 513
    :pswitch_2
    const v0, 0x7f080aca

    .line 514
    goto :goto_0

    .line 516
    :pswitch_3
    const v0, 0x7f080acc

    .line 517
    goto :goto_0

    .line 519
    :pswitch_4
    const v0, 0x7f080ace

    .line 520
    goto :goto_0

    .line 522
    :pswitch_5
    const v0, 0x7f080ad0

    .line 523
    goto :goto_0

    .line 505
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private getSkinTypeSelectedID(I)I
    .locals 1
    .param p1, "skinType"    # I

    .prologue
    .line 464
    const/4 v0, -0x1

    .line 466
    .local v0, "id":I
    packed-switch p1, :pswitch_data_0

    .line 489
    :goto_0
    return v0

    .line 468
    :pswitch_0
    const v0, 0x7f080ad4

    .line 469
    goto :goto_0

    .line 471
    :pswitch_1
    const v0, 0x7f080ad9

    .line 472
    goto :goto_0

    .line 474
    :pswitch_2
    const v0, 0x7f080ade

    .line 475
    goto :goto_0

    .line 477
    :pswitch_3
    const v0, 0x7f080ae3

    .line 478
    goto :goto_0

    .line 480
    :pswitch_4
    const v0, 0x7f080ae8

    .line 481
    goto :goto_0

    .line 483
    :pswitch_5
    const v0, 0x7f080aed

    .line 484
    goto :goto_0

    .line 466
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private initLayout()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 96
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mContext:Landroid/content/Context;

    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 97
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x7f030265

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mInfoView:Landroid/view/View;

    .line 98
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mInfoView:Landroid/view/View;

    const v3, 0x7f0804ef

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mSkinSettings:Landroid/widget/CheckBox;

    .line 99
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mInfoView:Landroid/view/View;

    const v3, 0x7f0804ee

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mShowAgainCheckLayout:Landroid/widget/RelativeLayout;

    .line 100
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mInfoView:Landroid/view/View;

    const v3, 0x7f08056a

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->btnOK:Landroid/widget/Button;

    .line 101
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mInfoView:Landroid/view/View;

    const v3, 0x7f080ac2

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ScrollView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->skinScroll:Landroid/widget/ScrollView;

    .line 103
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mInfoView:Landroid/view/View;

    const v3, 0x7f080ac4

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mUvQuestion1:Landroid/widget/TextView;

    .line 104
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mInfoView:Landroid/view/View;

    const v3, 0x7f080ad1

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mUvQuestion2:Landroid/widget/TextView;

    .line 106
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mUvQuestion1:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "1. "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mContext:Landroid/content/Context;

    const v5, 0x7f090d9f

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 107
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mUvQuestion2:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "2. "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mContext:Landroid/content/Context;

    const v5, 0x7f090da0

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 109
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mInfoView:Landroid/view/View;

    const v3, 0x7f080ad2

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->uvSkinOption1:Landroid/view/View;

    .line 110
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mInfoView:Landroid/view/View;

    const v3, 0x7f080ad4

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RadioButton;

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->rb_skintype_1:Landroid/widget/RadioButton;

    .line 111
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mInfoView:Landroid/view/View;

    const v3, 0x7f080ac6

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->uvSkinType1:Landroid/widget/ImageView;

    .line 114
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mInfoView:Landroid/view/View;

    const v3, 0x7f080ad7

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->uvSkinOption2:Landroid/view/View;

    .line 115
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mInfoView:Landroid/view/View;

    const v3, 0x7f080ad9

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RadioButton;

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->rb_skintype_2:Landroid/widget/RadioButton;

    .line 116
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mInfoView:Landroid/view/View;

    const v3, 0x7f080ac8

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->uvSkinType2:Landroid/widget/ImageView;

    .line 118
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mInfoView:Landroid/view/View;

    const v3, 0x7f080adc

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->uvSkinOption3:Landroid/view/View;

    .line 119
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mInfoView:Landroid/view/View;

    const v3, 0x7f080ade

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RadioButton;

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->rb_skintype_3:Landroid/widget/RadioButton;

    .line 120
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mInfoView:Landroid/view/View;

    const v3, 0x7f080aca

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->uvSkinType3:Landroid/widget/ImageView;

    .line 122
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mInfoView:Landroid/view/View;

    const v3, 0x7f080ae1

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->uvSkinOption4:Landroid/view/View;

    .line 123
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mInfoView:Landroid/view/View;

    const v3, 0x7f080ae3

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RadioButton;

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->rb_skintype_4:Landroid/widget/RadioButton;

    .line 124
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mInfoView:Landroid/view/View;

    const v3, 0x7f080acc

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->uvSkinType4:Landroid/widget/ImageView;

    .line 126
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mInfoView:Landroid/view/View;

    const v3, 0x7f080ae6

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->uvSkinOption5:Landroid/view/View;

    .line 127
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mInfoView:Landroid/view/View;

    const v3, 0x7f080ae8

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RadioButton;

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->rb_skintype_5:Landroid/widget/RadioButton;

    .line 128
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mInfoView:Landroid/view/View;

    const v3, 0x7f080ace

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->uvSkinType5:Landroid/widget/ImageView;

    .line 130
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mInfoView:Landroid/view/View;

    const v3, 0x7f080aeb

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->uvSkinOption6:Landroid/view/View;

    .line 131
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mInfoView:Landroid/view/View;

    const v3, 0x7f080aed

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RadioButton;

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->rb_skintype_6:Landroid/widget/RadioButton;

    .line 132
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mInfoView:Landroid/view/View;

    const v3, 0x7f080ad0

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->uvSkinType6:Landroid/widget/ImageView;

    .line 134
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mInfoView:Landroid/view/View;

    const v3, 0x7f080ad5

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mSkinType1:Landroid/widget/TextView;

    .line 135
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mInfoView:Landroid/view/View;

    const v3, 0x7f080ad6

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mSkinTypeDesc1:Landroid/widget/TextView;

    .line 137
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mInfoView:Landroid/view/View;

    const v3, 0x7f080ada

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mSkinType2:Landroid/widget/TextView;

    .line 138
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mInfoView:Landroid/view/View;

    const v3, 0x7f080adb

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mSkinTypeDesc2:Landroid/widget/TextView;

    .line 140
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mInfoView:Landroid/view/View;

    const v3, 0x7f080adf

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mSkinType3:Landroid/widget/TextView;

    .line 141
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mInfoView:Landroid/view/View;

    const v3, 0x7f080ae0

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mSkinTypeDesc3:Landroid/widget/TextView;

    .line 143
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mInfoView:Landroid/view/View;

    const v3, 0x7f080ae4

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mSkinType4:Landroid/widget/TextView;

    .line 144
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mInfoView:Landroid/view/View;

    const v3, 0x7f080ae5

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mSkinTypeDesc4:Landroid/widget/TextView;

    .line 146
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mInfoView:Landroid/view/View;

    const v3, 0x7f080ae9

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mSkinType5:Landroid/widget/TextView;

    .line 147
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mInfoView:Landroid/view/View;

    const v3, 0x7f080aea

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mSkinTypeDesc5:Landroid/widget/TextView;

    .line 149
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mInfoView:Landroid/view/View;

    const v3, 0x7f080aee

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mSkinType6:Landroid/widget/TextView;

    .line 150
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mInfoView:Landroid/view/View;

    const v3, 0x7f080aef

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mSkinTypeDesc6:Landroid/widget/TextView;

    .line 152
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->uvSkinOption1:Landroid/view/View;

    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->clickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->uvSkinType1:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->skinTypeListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 153
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->uvSkinOption2:Landroid/view/View;

    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->clickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->uvSkinType2:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->skinTypeListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 154
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->uvSkinOption3:Landroid/view/View;

    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->clickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->uvSkinType3:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->skinTypeListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 155
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->uvSkinOption4:Landroid/view/View;

    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->clickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->uvSkinType4:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->skinTypeListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 156
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->uvSkinOption5:Landroid/view/View;

    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->clickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->uvSkinType5:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->skinTypeListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 157
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->uvSkinOption6:Landroid/view/View;

    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->clickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->uvSkinType6:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->skinTypeListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 159
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mShowAgainCheckLayout:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mCheckboxLayoutClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 161
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mSkinSettings:Landroid/widget/CheckBox;

    new-instance v3, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog$2;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog$2;-><init>(Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;)V

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 179
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->btnOK:Landroid/widget/Button;

    new-instance v3, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog$3;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog$3;-><init>(Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 200
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string/jumbo v3, "skin_type_checked"

    invoke-interface {v2, v3, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 201
    .local v0, "checked":Z
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mSkinSettings:Landroid/widget/CheckBox;

    invoke-virtual {v2, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 202
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mSkinSettings:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 203
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->skinScroll:Landroid/widget/ScrollView;

    const/4 v3, 0x0

    invoke-direct {p0, v2, v3}, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->changeState(Landroid/view/ViewGroup;Z)V

    .line 208
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->btnOK:Landroid/widget/Button;

    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mSkinSettings:Landroid/widget/CheckBox;

    invoke-virtual {v3}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 209
    return-void

    .line 206
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->skinScroll:Landroid/widget/ScrollView;

    invoke-direct {p0, v2, v6}, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->changeState(Landroid/view/ViewGroup;Z)V

    goto :goto_0
.end method

.method private updateButton()V
    .locals 2

    .prologue
    .line 336
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mUserSkinToneType:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;->getSelectedSkinType()Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINTYPE;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINTYPE;->getValue()I

    move-result v0

    sget-object v1, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINTYPE;->INVALID:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINTYPE;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINTYPE;->getValue()I

    move-result v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mUserSkinToneType:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;->getSelectedSkinOption()Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;->getValue()I

    move-result v0

    sget-object v1, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;->INVALID:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;->getValue()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 338
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->btnOK:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 340
    :cond_0
    return-void
.end method

.method private updateColor(Z)V
    .locals 6
    .param p1, "enabled"    # Z

    .prologue
    const v3, 0x7f0700ec

    const v1, 0x7f0700e3

    const v2, 0x7f0700f6

    .line 548
    iget-object v4, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mUvQuestion1:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 549
    iget-object v4, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mUvQuestion2:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    if-eqz p1, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 551
    iget-object v4, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mSkinType1:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    if-eqz p1, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 552
    iget-object v4, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mSkinType2:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    if-eqz p1, :cond_3

    move v0, v1

    :goto_3
    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 553
    iget-object v4, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mSkinType3:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    if-eqz p1, :cond_4

    move v0, v1

    :goto_4
    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 554
    iget-object v4, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mSkinType4:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    if-eqz p1, :cond_5

    move v0, v1

    :goto_5
    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 555
    iget-object v4, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mSkinType5:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    if-eqz p1, :cond_6

    move v0, v1

    :goto_6
    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 556
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mSkinType6:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    if-eqz p1, :cond_7

    :goto_7
    invoke-virtual {v4, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 558
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mSkinTypeDesc1:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    if-eqz p1, :cond_8

    move v0, v3

    :goto_8
    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 559
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mSkinTypeDesc2:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    if-eqz p1, :cond_9

    move v0, v3

    :goto_9
    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 560
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mSkinTypeDesc3:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    if-eqz p1, :cond_a

    move v0, v3

    :goto_a
    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 561
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mSkinTypeDesc4:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    if-eqz p1, :cond_b

    move v0, v3

    :goto_b
    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 562
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mSkinTypeDesc5:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    if-eqz p1, :cond_c

    move v0, v3

    :goto_c
    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 563
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mSkinTypeDesc6:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    if-eqz p1, :cond_d

    :goto_d
    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 564
    return-void

    :cond_0
    move v0, v2

    .line 548
    goto/16 :goto_0

    :cond_1
    move v0, v2

    .line 549
    goto/16 :goto_1

    :cond_2
    move v0, v2

    .line 551
    goto/16 :goto_2

    :cond_3
    move v0, v2

    .line 552
    goto/16 :goto_3

    :cond_4
    move v0, v2

    .line 553
    goto/16 :goto_4

    :cond_5
    move v0, v2

    .line 554
    goto/16 :goto_5

    :cond_6
    move v0, v2

    .line 555
    goto/16 :goto_6

    :cond_7
    move v1, v2

    .line 556
    goto/16 :goto_7

    :cond_8
    move v0, v2

    .line 558
    goto :goto_8

    :cond_9
    move v0, v2

    .line 559
    goto :goto_9

    :cond_a
    move v0, v2

    .line 560
    goto :goto_a

    :cond_b
    move v0, v2

    .line 561
    goto :goto_b

    :cond_c
    move v0, v2

    .line 562
    goto :goto_c

    :cond_d
    move v3, v2

    .line 563
    goto :goto_d
.end method

.method private updateSkinTone(I)V
    .locals 3
    .param p1, "skinTone"    # I

    .prologue
    .line 494
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->getSkinToneDrawableId(I)I

    move-result v0

    .line 495
    .local v0, "id":I
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->setSelectedSkinTone(I)V

    .line 496
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 497
    .local v1, "view":Landroid/widget/ImageView;
    if-eqz v1, :cond_0

    .line 498
    const v2, 0x7f020703

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 499
    :cond_0
    return-void
.end method

.method private updateSkinType(I)V
    .locals 3
    .param p1, "skinType"    # I

    .prologue
    .line 453
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->getSkinTypeSelectedID(I)I

    move-result v0

    .line 454
    .local v0, "id":I
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->setSelectedSkinType(I)V

    .line 455
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    .line 456
    .local v1, "view":Landroid/widget/RadioButton;
    if-eqz v1, :cond_0

    .line 457
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 458
    :cond_0
    return-void
.end method


# virtual methods
.method protected applyOptionsIfSelected()V
    .locals 2

    .prologue
    .line 257
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mUserSkinToneType:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;

    if-eqz v0, :cond_0

    .line 258
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mUserSkinToneType:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;->getSelectedSkinType()Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINTYPE;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINTYPE;->getValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->updateSkinTone(I)V

    .line 259
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mUserSkinToneType:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;->getSelectedSkinOption()Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;->getValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->updateSkinType(I)V

    .line 260
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->btnOK:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 262
    :cond_0
    return-void
.end method

.method protected clearSkinSelection()V
    .locals 2

    .prologue
    const v1, 0x106000d

    .line 436
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->uvSkinType1:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 437
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->uvSkinType2:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 438
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->uvSkinType3:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 439
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->uvSkinType4:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 440
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->uvSkinType5:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 441
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->uvSkinType6:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 442
    return-void
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 1
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    .line 266
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/CompoundButton;->playSoundEffect(I)V

    .line 267
    return-void
.end method

.method protected sendBroadcastForAccesories()V
    .locals 2

    .prologue
    .line 240
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 241
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.sec.shealth.UPDATE_PROFILE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 242
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 243
    return-void
.end method

.method protected setSelectedSkinTone(I)V
    .locals 2
    .param p1, "skinTone"    # I

    .prologue
    .line 381
    packed-switch p1, :pswitch_data_0

    .line 404
    :goto_0
    return-void

    .line 383
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mUserSkinToneType:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;

    sget-object v1, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINTYPE;->SKIN_TYPE_1:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINTYPE;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;->setSelectedSkinType(Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINTYPE;)V

    goto :goto_0

    .line 386
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mUserSkinToneType:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;

    sget-object v1, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINTYPE;->SKIN_TYPE_2:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINTYPE;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;->setSelectedSkinType(Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINTYPE;)V

    goto :goto_0

    .line 389
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mUserSkinToneType:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;

    sget-object v1, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINTYPE;->SKIN_TYPE_3:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINTYPE;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;->setSelectedSkinType(Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINTYPE;)V

    goto :goto_0

    .line 392
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mUserSkinToneType:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;

    sget-object v1, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINTYPE;->SKIN_TYPE_4:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINTYPE;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;->setSelectedSkinType(Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINTYPE;)V

    goto :goto_0

    .line 395
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mUserSkinToneType:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;

    sget-object v1, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINTYPE;->SKIN_TYPE_5:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINTYPE;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;->setSelectedSkinType(Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINTYPE;)V

    goto :goto_0

    .line 398
    :pswitch_5
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mUserSkinToneType:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;

    sget-object v1, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINTYPE;->SKIN_TYPE_6:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINTYPE;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;->setSelectedSkinType(Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINTYPE;)V

    goto :goto_0

    .line 381
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method protected setSelectedSkinType(I)V
    .locals 2
    .param p1, "skinType"    # I

    .prologue
    .line 408
    packed-switch p1, :pswitch_data_0

    .line 431
    :goto_0
    return-void

    .line 410
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mUserSkinToneType:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;

    sget-object v1, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;->SKIN_EFFECT_1:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;->setSelectedSkinOption(Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;)V

    goto :goto_0

    .line 413
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mUserSkinToneType:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;

    sget-object v1, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;->SKIN_EFFECT_2:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;->setSelectedSkinOption(Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;)V

    goto :goto_0

    .line 416
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mUserSkinToneType:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;

    sget-object v1, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;->SKIN_EFFECT_3:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;->setSelectedSkinOption(Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;)V

    goto :goto_0

    .line 419
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mUserSkinToneType:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;

    sget-object v1, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;->SKIN_EFFECT_4:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;->setSelectedSkinOption(Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;)V

    goto :goto_0

    .line 422
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mUserSkinToneType:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;

    sget-object v1, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;->SKIN_EFFECT_5:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;->setSelectedSkinOption(Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;)V

    goto :goto_0

    .line 425
    :pswitch_5
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mUserSkinToneType:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;

    sget-object v1, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;->SKIN_EFFECT_6:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;->setSelectedSkinOption(Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;)V

    goto :goto_0

    .line 408
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method protected setSelectedSkinType(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 353
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 376
    :goto_0
    :pswitch_0
    return-void

    .line 355
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mUserSkinToneType:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;

    sget-object v1, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINTYPE;->SKIN_TYPE_1:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINTYPE;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;->setSelectedSkinType(Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINTYPE;)V

    goto :goto_0

    .line 358
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mUserSkinToneType:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;

    sget-object v1, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINTYPE;->SKIN_TYPE_2:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINTYPE;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;->setSelectedSkinType(Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINTYPE;)V

    goto :goto_0

    .line 361
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mUserSkinToneType:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;

    sget-object v1, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINTYPE;->SKIN_TYPE_3:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINTYPE;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;->setSelectedSkinType(Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINTYPE;)V

    goto :goto_0

    .line 364
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mUserSkinToneType:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;

    sget-object v1, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINTYPE;->SKIN_TYPE_4:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINTYPE;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;->setSelectedSkinType(Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINTYPE;)V

    goto :goto_0

    .line 367
    :pswitch_5
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mUserSkinToneType:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;

    sget-object v1, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINTYPE;->SKIN_TYPE_5:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINTYPE;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;->setSelectedSkinType(Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINTYPE;)V

    goto :goto_0

    .line 370
    :pswitch_6
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mUserSkinToneType:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;

    sget-object v1, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINTYPE;->SKIN_TYPE_6:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINTYPE;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;->setSelectedSkinType(Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINTYPE;)V

    goto :goto_0

    .line 353
    :pswitch_data_0
    .packed-switch 0x7f080ac6
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_6
    .end packed-switch
.end method

.method protected updateButtonStatus(Z)V
    .locals 2
    .param p1, "checked"    # Z

    .prologue
    .line 247
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mUserSkinToneType:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;->getSelectedSkinType()Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINTYPE;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINTYPE;->getValue()I

    move-result v0

    sget-object v1, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINTYPE;->INVALID:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINTYPE;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINTYPE;->getValue()I

    move-result v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mUserSkinToneType:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;->getSelectedSkinOption()Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;->getValue()I

    move-result v0

    sget-object v1, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;->INVALID:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;->getValue()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 249
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->btnOK:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 253
    :goto_0
    return-void

    .line 251
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->btnOK:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method

.method public updateContent(Lcom/sec/android/app/shealth/uv/data/SkinData;)V
    .locals 2
    .param p1, "skinData"    # Lcom/sec/android/app/shealth/uv/data/SkinData;

    .prologue
    .line 446
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/uv/data/SkinData;->getSkinTone()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->updateSkinTone(I)V

    .line 447
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/uv/data/SkinData;->getSkinType()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->updateSkinType(I)V

    .line 448
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->btnOK:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 449
    return-void
.end method

.method public updateText()V
    .locals 3

    .prologue
    .line 212
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mSkinSettings:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    .line 213
    .local v0, "isChecked":Z
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mInfoView:Landroid/view/View;

    .line 214
    invoke-direct {p0}, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->initLayout()V

    .line 215
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mInfoView:Landroid/view/View;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->setContentView(Landroid/view/View;)V

    .line 216
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mSkinSettings:Landroid/widget/CheckBox;

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 217
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->skinScroll:Landroid/widget/ScrollView;

    if-nez v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-direct {p0, v2, v1}, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->changeState(Landroid/view/ViewGroup;Z)V

    .line 218
    if-nez v0, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mUserSkinToneType:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;->getSelectedSkinType()Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINTYPE;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINTYPE;->getValue()I

    move-result v1

    sget-object v2, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINTYPE;->INVALID:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINTYPE;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINTYPE;->getValue()I

    move-result v2

    if-eq v1, v2, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mUserSkinToneType:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;->getSelectedSkinOption()Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;->getValue()I

    move-result v1

    sget-object v2, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;->INVALID:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;->getValue()I

    move-result v2

    if-eq v1, v2, :cond_1

    .line 220
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->applyOptionsIfSelected()V

    .line 224
    :goto_1
    return-void

    .line 217
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 222
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->btnOK:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_1
.end method
