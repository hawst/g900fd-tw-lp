.class public Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;
.super Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment;
.source "WeightWithGoalFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/weight/NonBackStackFragment;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$5;,
        Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$GoalState;
    }
.end annotation


# static fields
.field private static final COMPLETE_PROGRESS:I = 0x64

.field private static final NO_PROGRESS:I = 0x0

.field private static final sStubPeriod:I = -0x1


# instance fields
.field private mBurnLinearLayout:Landroid/widget/LinearLayout;

.field private mCalorieBurnCountTextView:Landroid/widget/TextView;

.field private mCurrentState:Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$GoalState;

.field private mCurrentWeight:F

.field private mCurrentWeightView:Landroid/widget/TextView;

.field private mExpectedDate:Ljava/util/Date;

.field private mExpectedGoalView:Landroid/widget/TextView;

.field private mIntakeCountTextView:Landroid/widget/TextView;

.field private mIntakeLinearLayout:Landroid/widget/LinearLayout;

.field private mLostWeight:Landroid/widget/TextView;

.field private mMaintainWeightTextView:Landroid/widget/TextView;

.field private mPercents:Landroid/widget/TextView;

.field private mProgressBar:Landroid/widget/ProgressBar;

.field private mProgressPercent:I

.field private mProgressTextView:Landroid/widget/TextView;

.field private mRecommendedCalorieTextView:Landroid/widget/TextView;

.field private mShouldLoseWeight:Landroid/widget/TextView;

.field private mWeightDiffCurrent:F

.field private mWeightDiffStart:F

.field private mWeightSetGoal:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment;-><init>()V

    .line 87
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->showAlertDialog(Ljava/lang/String;)V

    return-void
.end method

.method private calculateProgressInfoAndState()Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$GoalState;
    .locals 11

    .prologue
    const/16 v10, 0x64

    .line 109
    new-instance v3, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-direct {v3, v6}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    .line 110
    .local v3, "profile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getWeight()F

    move-result v6

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->getCurrentUnit()Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->convertKgToUnit(FLcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;)F

    move-result v6

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->getActiveGoal()Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->getValue()F

    move-result v7

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->getCurrentUnit()Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->convertKgToUnit(FLcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;)F

    move-result v7

    sub-float/2addr v6, v7

    iput v6, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mWeightDiffCurrent:F

    .line 112
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->getStartWeightValue()F

    move-result v6

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->getCurrentUnit()Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->convertKgToUnit(FLcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;)F

    move-result v6

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->getActiveGoal()Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->getValue()F

    move-result v7

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->getCurrentUnit()Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->convertKgToUnit(FLcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;)F

    move-result v7

    sub-float/2addr v6, v7

    iput v6, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mWeightDiffStart:F

    .line 115
    const/4 v6, 0x0

    iput v6, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mProgressPercent:I

    .line 116
    iget v6, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mWeightDiffStart:F

    iget v7, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mWeightDiffCurrent:F

    cmpl-float v6, v6, v7

    if-lez v6, :cond_0

    .line 117
    iget v6, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mWeightDiffCurrent:F

    const/4 v7, 0x0

    cmpl-float v6, v6, v7

    if-ltz v6, :cond_1

    .line 118
    iget v6, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mWeightDiffStart:F

    iget v7, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mWeightDiffCurrent:F

    sub-float/2addr v6, v7

    iget v7, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mWeightDiffStart:F

    div-float/2addr v6, v7

    const/high16 v7, 0x42c80000    # 100.0f

    mul-float/2addr v6, v7

    float-to-int v6, v6

    iput v6, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mProgressPercent:I

    .line 125
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->getActiveGoal()Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    move-result-object v0

    .line 126
    .local v0, "goalData":Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->getPeriod()I

    move-result v6

    int-to-long v1, v6

    .line 127
    .local v1, "period":J
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->getSetTime()J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    mul-long/2addr v8, v1

    add-long v4, v6, v8

    .line 128
    .local v4, "targetDate":J
    invoke-virtual {p0, v4, v5}, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->getDateFromTime(J)Ljava/util/Date;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mExpectedDate:Ljava/util/Date;

    .line 131
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v6

    iget-object v8, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mExpectedDate:Ljava/util/Date;

    invoke-virtual {v8}, Ljava/util/Date;->getTime()J

    move-result-wide v8

    invoke-static {v8, v9}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v8

    cmp-long v6, v6, v8

    if-gez v6, :cond_3

    .line 132
    iget v6, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mProgressPercent:I

    if-ge v6, v10, :cond_2

    sget-object v6, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$GoalState;->IN_PROGRESS:Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$GoalState;

    .line 138
    :goto_1
    return-object v6

    .line 121
    .end local v0    # "goalData":Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;
    .end local v1    # "period":J
    .end local v4    # "targetDate":J
    :cond_1
    iput v10, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mProgressPercent:I

    goto :goto_0

    .line 132
    .restart local v0    # "goalData":Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;
    .restart local v1    # "period":J
    .restart local v4    # "targetDate":J
    :cond_2
    sget-object v6, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$GoalState;->REACHED_GOAL_EARLY:Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$GoalState;

    goto :goto_1

    .line 135
    :cond_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v6

    iget-object v8, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mExpectedDate:Ljava/util/Date;

    invoke-virtual {v8}, Ljava/util/Date;->getTime()J

    move-result-wide v8

    invoke-static {v8, v9}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v8

    cmp-long v6, v6, v8

    if-nez v6, :cond_5

    .line 136
    iget v6, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mProgressPercent:I

    if-ge v6, v10, :cond_4

    sget-object v6, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$GoalState;->IN_PROGRESS:Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$GoalState;

    goto :goto_1

    :cond_4
    sget-object v6, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$GoalState;->REACHED_GOAL_IN_TIME:Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$GoalState;

    goto :goto_1

    .line 138
    :cond_5
    sget-object v6, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$GoalState;->LOOSED_GOAL:Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$GoalState;

    goto :goto_1
.end method

.method private formCalendarInfoString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 338
    const v1, 0x7f090cb3

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mWeightDiffCurrent:F

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->getCurrentUnit()Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;->getUnitName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/shealth/common/utils/DateFormatHelper;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mExpectedDate:Ljava/util/Date;

    invoke-virtual {v4, v5}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 344
    .local v0, "status":Ljava/lang/String;
    return-object v0
.end method

.method private getStartWeightValue()F
    .locals 6

    .prologue
    .line 353
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->getActiveGoal()Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    move-result-object v0

    .line 354
    .local v0, "goal":Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->getWeightDao()Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;

    move-result-object v3

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->getCreateTime()J

    move-result-wide v4

    invoke-interface {v3, v4, v5}, Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;->getLastDataBeforeTime(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    .line 355
    .local v2, "weight":Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;
    if-nez v2, :cond_0

    .line 356
    new-instance v1, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-direct {v1, v3}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    .line 357
    .local v1, "healthProfile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getWeight()F

    move-result v3

    .line 359
    .end local v1    # "healthProfile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    :goto_0
    return v3

    :cond_0
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getValue()F

    move-result v3

    goto :goto_0
.end method

.method private initGoalStateUI(Landroid/view/View;)V
    .locals 14
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 181
    const v10, 0x7f080c6f

    invoke-virtual {p1, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    iput-object v10, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mProgressTextView:Landroid/widget/TextView;

    .line 183
    const v10, 0x7f080c6a

    invoke-virtual {p1, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    iput-object v10, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mRecommendedCalorieTextView:Landroid/widget/TextView;

    .line 184
    const v10, 0x7f080c6b

    invoke-virtual {p1, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/LinearLayout;

    iput-object v10, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mIntakeLinearLayout:Landroid/widget/LinearLayout;

    .line 185
    const v10, 0x7f080c6c

    invoke-virtual {p1, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    iput-object v10, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mIntakeCountTextView:Landroid/widget/TextView;

    .line 186
    const v10, 0x7f080c6d

    invoke-virtual {p1, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/LinearLayout;

    iput-object v10, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mBurnLinearLayout:Landroid/widget/LinearLayout;

    .line 187
    const v10, 0x7f080c6e

    invoke-virtual {p1, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    iput-object v10, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mCalorieBurnCountTextView:Landroid/widget/TextView;

    .line 189
    const v10, 0x7f080c69

    invoke-virtual {p1, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/LinearLayout;

    iput-object v10, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mWeightSetGoal:Landroid/widget/LinearLayout;

    .line 190
    const v10, 0x7f080c67

    invoke-virtual {p1, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    iput-object v10, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mCurrentWeightView:Landroid/widget/TextView;

    .line 191
    const v10, 0x7f080c61

    invoke-virtual {p1, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    iput-object v10, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mExpectedGoalView:Landroid/widget/TextView;

    .line 192
    const v10, 0x7f080c68

    invoke-virtual {p1, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    iput-object v10, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mMaintainWeightTextView:Landroid/widget/TextView;

    .line 194
    const v10, 0x7f080c71

    invoke-virtual {p1, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    .line 195
    .local v5, "setChallengeButton":Landroid/widget/Button;
    new-instance v9, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v10

    invoke-direct {v9, v10}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;-><init>(Landroid/content/Context;)V

    .line 196
    .local v9, "weightUnitHelper":Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;
    new-instance v4, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v10

    invoke-direct {v4, v10}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    .line 198
    .local v4, "profile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getWeight()F

    move-result v10

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->getCurrentUnit()Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->convertKgToUnit(FLcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;)F

    move-result v10

    iput v10, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mCurrentWeight:F

    .line 199
    iget-object v10, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mCurrentWeightView:Landroid/widget/TextView;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f090cb2

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget v12, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mCurrentWeight:F

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v9}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->getWeightUnit()Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    move-result-object v12

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v13

    invoke-virtual {v12, v13}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;->getUnitName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 201
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->getActiveGoal()Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    move-result-object v1

    .line 202
    .local v1, "goalData":Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->getPeriod()I

    move-result v10

    int-to-long v2, v10

    .line 203
    .local v2, "period":J
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->getSetTime()J

    move-result-wide v10

    const-wide/16 v12, 0x3e8

    mul-long/2addr v12, v2

    add-long v7, v10, v12

    .line 205
    .local v7, "targetDate":J
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v10, "yyyy/MM/dd"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v11

    invoke-direct {v0, v10, v11}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 206
    .local v0, "dateFormat":Ljava/text/SimpleDateFormat;
    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v0, v10}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 207
    .local v6, "tDate":Ljava/lang/String;
    iget-object v10, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mExpectedGoalView:Landroid/widget/TextView;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "by "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 209
    iget-object v10, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mProgressTextView:Landroid/widget/TextView;

    const/16 v11, 0x8

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 210
    iget-object v10, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mCurrentWeightView:Landroid/widget/TextView;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 211
    iget-object v10, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mMaintainWeightTextView:Landroid/widget/TextView;

    const/16 v11, 0x8

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 212
    iget-object v10, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mLostWeight:Landroid/widget/TextView;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 213
    iget-object v10, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mShouldLoseWeight:Landroid/widget/TextView;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 214
    iget-object v10, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mPercents:Landroid/widget/TextView;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 215
    iget-object v10, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mProgressBar:Landroid/widget/ProgressBar;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 216
    iget-object v10, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mExpectedGoalView:Landroid/widget/TextView;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 219
    iget-object v10, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mWeightSetGoal:Landroid/widget/LinearLayout;

    const/16 v11, 0x8

    invoke-virtual {v10, v11}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 220
    const v10, 0x7f080c70

    invoke-virtual {p1, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    const/16 v11, 0x8

    invoke-virtual {v10, v11}, Landroid/view/View;->setVisibility(I)V

    .line 221
    const v10, 0x7f080c5e

    invoke-virtual {p1, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    const/16 v11, 0x8

    invoke-virtual {v10, v11}, Landroid/view/View;->setVisibility(I)V

    .line 223
    new-instance v10, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$1;

    invoke-direct {v10, p0}, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$1;-><init>(Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;)V

    invoke-virtual {v5, v10}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 229
    sget-object v10, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$5;->$SwitchMap$com$sec$android$app$shealth$weight$goal$WeightWithGoalFragment$GoalState:[I

    iget-object v11, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mCurrentState:Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$GoalState;

    invoke-virtual {v11}, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$GoalState;->ordinal()I

    move-result v11

    aget v10, v10, v11

    packed-switch v10, :pswitch_data_0

    .line 263
    new-instance v10, Ljava/lang/IllegalArgumentException;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Unsupported state "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mCurrentState:Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$GoalState;

    invoke-virtual {v12}, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$GoalState;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 231
    :pswitch_0
    iget-object v10, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mWeightSetGoal:Landroid/widget/LinearLayout;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 232
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->showProgressText()V

    .line 266
    :goto_0
    return-void

    .line 235
    :pswitch_1
    const v10, 0x7f080c5e

    invoke-virtual {p1, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/view/View;->setVisibility(I)V

    .line 238
    iget-object v10, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mWeightSetGoal:Landroid/widget/LinearLayout;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 239
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->showProgressText()V

    goto :goto_0

    .line 243
    :pswitch_2
    const v10, 0x7f080c70

    invoke-virtual {p1, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    const/4 v11, 0x4

    invoke-virtual {v10, v11}, Landroid/view/View;->setVisibility(I)V

    .line 244
    const/4 v10, 0x4

    invoke-virtual {v5, v10}, Landroid/widget/Button;->setVisibility(I)V

    .line 246
    iget-object v10, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mMaintainWeightTextView:Landroid/widget/TextView;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 247
    iget-object v10, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mLostWeight:Landroid/widget/TextView;

    const/4 v11, 0x4

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 248
    iget-object v10, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mShouldLoseWeight:Landroid/widget/TextView;

    const/4 v11, 0x4

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 249
    iget-object v10, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mPercents:Landroid/widget/TextView;

    const/4 v11, 0x4

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 250
    iget-object v10, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mProgressBar:Landroid/widget/ProgressBar;

    const/4 v11, 0x4

    invoke-virtual {v10, v11}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 251
    iget-object v10, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mCurrentWeightView:Landroid/widget/TextView;

    const/16 v11, 0x8

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 252
    iget-object v10, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mExpectedGoalView:Landroid/widget/TextView;

    const/16 v11, 0x8

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 253
    iget-object v10, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mWeightSetGoal:Landroid/widget/LinearLayout;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 254
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->showProgressText()V

    goto :goto_0

    .line 257
    :pswitch_3
    const v10, 0x7f080c70

    invoke-virtual {p1, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/view/View;->setVisibility(I)V

    .line 258
    const/4 v10, 0x0

    invoke-virtual {v5, v10}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    .line 229
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private initGoalWeightTextViews(Landroid/view/View;)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 148
    const-string v0, "%.1f"

    .line 149
    .local v0, "floatFormat":Ljava/lang/String;
    const v1, 0x7f080c5f

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->getActiveGoal()Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->getValue()F

    move-result v4

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->getCurrentUnit()Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->convertKgToUnit(FLcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;)F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 151
    const v1, 0x7f080c60

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->getCurrentUnit()Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;->getUnitName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 152
    return-void
.end method

.method private initProgressBar(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 155
    const v0, 0x7f080c63

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mLostWeight:Landroid/widget/TextView;

    .line 156
    const v0, 0x7f080c64

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mShouldLoseWeight:Landroid/widget/TextView;

    .line 157
    const v0, 0x7f080c65

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mPercents:Landroid/widget/TextView;

    .line 158
    const v0, 0x7f080c66

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mProgressBar:Landroid/widget/ProgressBar;

    .line 159
    return-void
.end method

.method private initUIComponents(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 142
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->initGoalWeightTextViews(Landroid/view/View;)V

    .line 143
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->initProgressBar(Landroid/view/View;)V

    .line 144
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->initGoalStateUI(Landroid/view/View;)V

    .line 145
    return-void
.end method

.method private showAlertDialog(Ljava/lang/String;)V
    .locals 4
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    const v3, 0x7f090035

    .line 320
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 321
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelable(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$4;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$4;-><init>(Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 334
    return-void
.end method

.method private showProgressText()V
    .locals 12

    .prologue
    const v11, 0x9c43

    const v10, 0x7f090c9b

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v7, -0x1

    .line 269
    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mProgressTextView:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->formCalendarInfoString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 270
    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mRecommendedCalorieTextView:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->getGoalActivity()Lcom/sec/android/app/shealth/weight/goal/WeightGoalActivity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/weight/goal/WeightGoalActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090857

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->getGoalActivity()Lcom/sec/android/app/shealth/weight/goal/WeightGoalActivity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/weight/goal/WeightGoalActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    new-array v5, v9, [Ljava/lang/Object;

    invoke-static {}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->getDefaultIntakeCalorieGoal()I

    move-result v6

    int-to-float v6, v6

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-virtual {v4, v10, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 272
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/hcp/GoalUtils;->getLatestGoalData(Landroid/content/Context;Ljava/lang/Integer;)Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    move-result-object v1

    .line 273
    .local v1, "calorieIntakeData":Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;
    if-nez v1, :cond_0

    .line 274
    new-instance v1, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    .end local v1    # "calorieIntakeData":Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/NetCalorieUtils;->getRecommendedCalorieIntake()I

    move-result v2

    int-to-float v2, v2

    invoke-direct {v1, v2, v11, v7, v7}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;-><init>(FIII)V

    .line 278
    .restart local v1    # "calorieIntakeData":Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mIntakeCountTextView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->getGoalActivity()Lcom/sec/android/app/shealth/weight/goal/WeightGoalActivity;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/weight/goal/WeightGoalActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    new-array v4, v9, [Ljava/lang/Object;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->getValue()F

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Float;->intValue()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-virtual {v3, v10, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 280
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const v3, 0x9c4b

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/hcp/GoalUtils;->getLatestGoalData(Landroid/content/Context;Ljava/lang/Integer;)Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    move-result-object v0

    .line 281
    .local v0, "calorieBurntData":Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;
    if-nez v0, :cond_1

    .line 282
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    .end local v0    # "calorieBurntData":Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/NetCalorieUtils;->getRecommendedCalorieBurn()D

    move-result-wide v2

    double-to-float v2, v2

    const v3, 0x9c4b

    invoke-direct {v0, v2, v3, v7, v7}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;-><init>(FIII)V

    .line 286
    .restart local v0    # "calorieBurntData":Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mCalorieBurnCountTextView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->getGoalActivity()Lcom/sec/android/app/shealth/weight/goal/WeightGoalActivity;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/weight/goal/WeightGoalActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    new-array v4, v9, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->getValue()F

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Float;->intValue()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-virtual {v3, v10, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 290
    return-void
.end method

.method private updateProgress()V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 162
    iget v1, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mWeightDiffStart:F

    iget v2, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mWeightDiffCurrent:F

    sub-float v0, v1, v2

    .line 163
    .local v0, "weightDiff":F
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->compare(FF)I

    move-result v1

    if-gez v1, :cond_0

    .line 164
    const/4 v0, 0x0

    .line 172
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mLostWeight:Landroid/widget/TextView;

    const v2, 0x7f090c97

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v8, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->getCurrentUnit()Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;->getUnitName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 174
    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mShouldLoseWeight:Landroid/widget/TextView;

    const v2, 0x7f090c98

    new-array v3, v8, [Ljava/lang/Object;

    iget v4, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mWeightDiffStart:F

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->getCurrentUnit()Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;->getUnitName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 176
    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mPercents:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mProgressPercent:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f090c9e

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 177
    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mProgressBar:Landroid/widget/ProgressBar;

    iget v2, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mProgressPercent:I

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 178
    return-void
.end method


# virtual methods
.method protected customizeActionBar(Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;)V
    .locals 7
    .param p1, "actionBarHelper"    # Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 299
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment;->customizeActionBar(Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;)V

    .line 300
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mCurrentState:Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$GoalState;

    sget-object v1, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$GoalState;->IN_PROGRESS:Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$GoalState;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$GoalState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mCurrentState:Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$GoalState;

    sget-object v1, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$GoalState;->REACHED_GOAL_EARLY:Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$GoalState;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$GoalState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 301
    :cond_0
    new-array v0, v6, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v2, 0x7f020299

    const v3, 0x7f090040

    new-instance v4, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$2;

    invoke-direct {v4, p0}, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$2;-><init>(Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;)V

    invoke-direct {v1, v2, v5, v3, v4}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IIILandroid/view/View$OnClickListener;)V

    aput-object v1, v0, v5

    invoke-virtual {p1, v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 309
    :cond_1
    const v0, 0x7f090031

    invoke-virtual {p1, v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 310
    new-array v0, v6, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v2, 0x7f0207b0

    const v3, 0x7f090035

    new-instance v4, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$3;

    invoke-direct {v4, p0}, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$3;-><init>(Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;)V

    invoke-direct {v1, v2, v5, v3, v4}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IIILandroid/view/View$OnClickListener;)V

    aput-object v1, v0, v5

    invoke-virtual {p1, v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 317
    return-void
.end method

.method protected getFragmentType()Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment$GoalFragment;
    .locals 1

    .prologue
    .line 294
    sget-object v0, Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment$GoalFragment;->WITH_GOAL:Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment$GoalFragment;

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 96
    const v1, 0x7f0302bc

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 97
    .local v0, "view":Landroid/view/View;
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->calculateProgressInfoAndState()Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$GoalState;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->mCurrentState:Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$GoalState;

    .line 98
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->initUIComponents(Landroid/view/View;)V

    .line 99
    return-object v0
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 104
    invoke-super {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment;->onResume()V

    .line 105
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->updateProgress()V

    .line 106
    return-void
.end method
