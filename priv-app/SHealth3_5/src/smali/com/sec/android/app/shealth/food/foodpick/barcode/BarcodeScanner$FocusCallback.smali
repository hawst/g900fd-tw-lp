.class Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusCallback;
.super Ljava/lang/Object;
.source "BarcodeScanner.java"

# interfaces
.implements Landroid/hardware/Camera$AutoFocusCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FocusCallback"
.end annotation


# static fields
.field private static final SLEEP_TIME:I = 0x12c


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;)V
    .locals 0

    .prologue
    .line 519
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusCallback;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$1;

    .prologue
    .line 519
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusCallback;-><init>(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;)V

    return-void
.end method


# virtual methods
.method public onAutoFocus(ZLandroid/hardware/Camera;)V
    .locals 4
    .param p1, "success"    # Z
    .param p2, "camera"    # Landroid/hardware/Camera;

    .prologue
    .line 524
    sget-object v1, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "onAutoFocus, success: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 526
    const-wide/16 v1, 0x12c

    :try_start_0
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 530
    :goto_0
    if-eqz p1, :cond_0

    .line 531
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusCallback;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->focusStateLock:Ljava/lang/Object;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->access$1100(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 532
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusCallback;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;

    sget-object v3, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;->FOCUSED:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;

    # setter for: Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->focusState:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;
    invoke-static {v1, v3}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->access$1202(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;)Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;

    .line 533
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 537
    :goto_1
    return-void

    .line 527
    :catch_0
    move-exception v0

    .line 528
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 533
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 535
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusCallback;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;

    # invokes: Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->autoFocus()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->access$1900(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;)V

    goto :goto_1
.end method
