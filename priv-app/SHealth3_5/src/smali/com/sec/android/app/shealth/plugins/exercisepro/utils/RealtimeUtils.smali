.class public Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;
.super Ljava/lang/Object;
.source "RealtimeUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils$WorkoutStastics;
    }
.end annotation


# static fields
.field public static DISP_DATA_LIST:Ljava/util/ArrayList; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final LIST_POPUP_LEFT_PADDING:I = 0xc

.field public static final LIST_POPUP_RIGHT_PADDING:I = 0x12

.field public static PSERVICE_ACCUWEATHER:I

.field public static PSERVICE_NONE:I

.field public static PSERVICE_WEATHERNEWS:I

.field private static final TAG:Ljava/lang/String;

.field public static transparentDrawable:Landroid/graphics/drawable/ColorDrawable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 60
    const-class v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->TAG:Ljava/lang/String;

    .line 65
    const/16 v0, 0xc

    sput v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->PSERVICE_WEATHERNEWS:I

    .line 66
    const/16 v0, 0xd

    sput v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->PSERVICE_ACCUWEATHER:I

    .line 67
    const/16 v0, 0xb

    sput v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->PSERVICE_NONE:I

    .line 187
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils$1;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->DISP_DATA_LIST:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    return-void
.end method

.method public static checkDefaultValue(II)I
    .locals 3
    .param p0, "goalType"    # I
    .param p1, "value"    # I

    .prologue
    const/16 v1, 0xa

    .line 963
    packed-switch p0, :pswitch_data_0

    .line 989
    :cond_0
    :goto_0
    :pswitch_0
    return p1

    .line 965
    :pswitch_1
    const/16 v1, 0x64

    if-ge p1, v1, :cond_0

    .line 966
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getDistanceUnitFromSharedPreferences()Ljava/lang/String;

    move-result-object v1

    const-string v2, "km"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 968
    .local v0, "isUnitKm":Z
    if-eqz v0, :cond_1

    const/16 p1, 0x3e8

    .line 970
    :goto_1
    goto :goto_0

    .line 968
    :cond_1
    const/16 p1, 0x3c5

    goto :goto_1

    .line 973
    .end local v0    # "isUnitKm":Z
    :pswitch_2
    if-gez p1, :cond_2

    .line 974
    const/16 p1, 0x12c

    goto :goto_0

    .line 975
    :cond_2
    if-ge p1, v1, :cond_0

    .line 976
    const/16 p1, 0xa

    goto :goto_0

    .line 980
    :pswitch_3
    if-gez p1, :cond_3

    .line 981
    const/16 p1, 0x1e

    goto :goto_0

    .line 982
    :cond_3
    if-ge p1, v1, :cond_0

    .line 983
    const/16 p1, 0xa

    goto :goto_0

    .line 986
    :pswitch_4
    invoke-static {p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkValueByTrainingEffectLevel(I)I

    move-result p1

    goto :goto_0

    .line 963
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public static checkPaceValue(J)Ljava/lang/String;
    .locals 12
    .param p0, "second"    # J

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 811
    const-wide/16 v6, 0x0

    cmp-long v6, p0, v6

    if-gez v6, :cond_0

    .line 812
    const-string v6, "0\'00\""

    .line 839
    :goto_0
    return-object v6

    .line 819
    :cond_0
    const-wide/16 v6, 0x1770

    cmp-long v6, p0, v6

    if-ltz v6, :cond_1

    .line 820
    const-string v6, "-\'--\""

    goto :goto_0

    .line 822
    :cond_1
    const-wide/16 v2, 0x3c

    .line 823
    .local v2, "MINUTE":J
    const-wide/16 v6, 0x3c

    mul-long v0, v6, v2

    .line 825
    .local v0, "HOUR":J
    rem-long/2addr p0, v0

    .line 827
    const-string v6, "%02d"

    new-array v7, v11, [Ljava/lang/Object;

    div-long v8, p0, v2

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v7, v10

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 828
    .local v4, "minuteText":Ljava/lang/String;
    const-string v6, "%02d"

    new-array v7, v11, [Ljava/lang/Object;

    rem-long v8, p0, v2

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v7, v10

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 839
    .local v5, "secondText":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto :goto_0
.end method

.method public static checkTimeValue(J)Ljava/lang/String;
    .locals 17
    .param p0, "second"    # J

    .prologue
    .line 843
    move-wide/from16 v11, p0

    .line 844
    .local v11, "totalSecond":J
    const-wide/16 v13, 0x0

    cmp-long v13, p0, v13

    if-gez v13, :cond_0

    .line 845
    const-string v13, "--:--:--"

    .line 856
    :goto_0
    return-object v13

    .line 847
    :cond_0
    const-wide/16 v2, 0x3c

    .line 848
    .local v2, "MINUTE":J
    const-wide/16 v13, 0x3c

    mul-long v0, v13, v2

    .line 849
    .local v0, "HOUR":J
    div-long v4, p0, v0

    .line 850
    .local v4, "hour":J
    rem-long p0, p0, v0

    .line 851
    div-long v7, p0, v2

    .line 852
    .local v7, "minute":J
    rem-long p0, p0, v2

    .line 853
    const-string v13, "%02d"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-static {v13, v14}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 854
    .local v6, "hourText":Ljava/lang/String;
    const-string v13, "%02d"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-static {v13, v14}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    .line 855
    .local v9, "minuteText":Ljava/lang/String;
    const-string v13, "%02d"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    invoke-static/range {p0 .. p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-static {v13, v14}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    .line 856
    .local v10, "secondText":Ljava/lang/String;
    const-wide/16 v13, 0xe10

    cmp-long v13, v11, v13

    if-gez v13, :cond_1

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ":"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    goto :goto_0

    :cond_1
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ":"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ":"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    goto :goto_0
.end method

.method public static checkUnit(ILjava/lang/Float;)I
    .locals 5
    .param p0, "type"    # I
    .param p1, "value"    # Ljava/lang/Float;

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x2

    .line 783
    move v0, p0

    .line 785
    .local v0, "flag":I
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->loadUnitsFromSharedPreferences()V

    .line 786
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/CommonCache;->infoDistanceUnit:Ljava/lang/String;

    const-string v2, "km"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 787
    if-ne p0, v3, :cond_0

    .line 788
    const/16 v0, 0xa

    .line 807
    :goto_0
    return v0

    .line 789
    :cond_0
    if-ne p0, v4, :cond_1

    .line 790
    const/16 v0, 0xb

    goto :goto_0

    .line 791
    :cond_1
    const/4 v1, 0x6

    if-ne p0, v1, :cond_2

    .line 792
    const/16 v0, 0xf

    goto :goto_0

    .line 793
    :cond_2
    const/16 v1, 0x13

    if-ne p0, v1, :cond_3

    .line 794
    const/16 v0, 0x14

    goto :goto_0

    .line 795
    :cond_3
    const/16 v1, 0x15

    if-ne p0, v1, :cond_4

    .line 796
    const/16 v0, 0x16

    goto :goto_0

    .line 798
    :cond_4
    move v0, p0

    goto :goto_0

    .line 800
    :cond_5
    if-ne p0, v3, :cond_6

    .line 801
    const/16 v0, 0x8

    goto :goto_0

    .line 802
    :cond_6
    if-ne p0, v4, :cond_7

    .line 803
    const/16 v0, 0x9

    goto :goto_0

    .line 805
    :cond_7
    move v0, p0

    goto :goto_0
.end method

.method public static checkValue(ILjava/lang/Float;)Ljava/lang/String;
    .locals 1
    .param p0, "type"    # I
    .param p1, "val"    # Ljava/lang/Float;

    .prologue
    .line 682
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkValue(ILjava/lang/Float;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static checkValue(ILjava/lang/Float;Z)Ljava/lang/String;
    .locals 11
    .param p0, "type"    # I
    .param p1, "val"    # Ljava/lang/Float;
    .param p2, "shouldRoundDown"    # Z

    .prologue
    .line 686
    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result v5

    float-to-double v5, v5

    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    .line 687
    .local v4, "value":Ljava/lang/Double;
    const-string v0, ""

    .line 688
    .local v0, "check":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->loadUnitsFromSharedPreferences()V

    .line 689
    sget-object v5, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/CommonCache;->infoDistanceUnit:Ljava/lang/String;

    const-string v6, "km"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    const/4 v3, 0x1

    .line 690
    .local v3, "unitMile":Z
    :goto_0
    if-eqz v3, :cond_7

    .line 691
    const/4 v5, 0x2

    if-eq p0, v5, :cond_0

    const/16 v5, 0x15

    if-ne p0, v5, :cond_3

    .line 692
    :cond_0
    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v5

    const-wide v7, 0x3f445c6dea1359a4L    # 6.2137E-4

    mul-double/2addr v5, v7

    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    .line 729
    :cond_1
    :goto_1
    const/16 v5, 0x13

    if-eq p0, v5, :cond_b

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v5

    const-wide v7, 0x40c3880000000000L    # 10000.0

    cmpl-double v5, v5, v7

    if-ltz v5, :cond_b

    .line 730
    const-string v0, "9999+"

    .line 779
    :goto_2
    return-object v0

    .line 689
    .end local v3    # "unitMile":Z
    :cond_2
    const/4 v3, 0x0

    goto :goto_0

    .line 693
    .restart local v3    # "unitMile":Z
    :cond_3
    const/4 v5, 0x3

    if-ne p0, v5, :cond_4

    .line 694
    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v5

    const-wide v7, 0x40ac200000000000L    # 3600.0

    mul-double/2addr v5, v7

    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    .line 695
    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v5

    const-wide v7, 0x3f445c6dea1359a4L    # 6.2137E-4

    mul-double/2addr v5, v7

    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    goto :goto_1

    .line 696
    :cond_4
    const/4 v5, 0x6

    if-ne p0, v5, :cond_5

    .line 697
    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v5

    const-wide v7, 0x400a3f290abb44e5L    # 3.28084

    mul-double/2addr v5, v7

    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    goto :goto_1

    .line 698
    :cond_5
    const/16 v5, 0x10

    if-ne p0, v5, :cond_6

    .line 699
    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v5

    const-wide v7, 0x400a3f290abb44e5L    # 3.28084

    mul-double/2addr v5, v7

    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    goto :goto_1

    .line 700
    :cond_6
    const/16 v5, 0x13

    if-ne p0, v5, :cond_1

    .line 701
    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v5

    const-wide v7, 0x40ac200000000000L    # 3600.0

    mul-double/2addr v5, v7

    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    .line 702
    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v5

    const-wide v7, 0x3f445c6dea1359a4L    # 6.2137E-4

    mul-double/2addr v5, v7

    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    .line 703
    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v5

    const-wide/16 v7, 0x0

    cmpl-double v5, v5, v7

    if-eqz v5, :cond_1

    .line 704
    const-wide v5, 0x40ac200000000000L    # 3600.0

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v7

    div-double/2addr v5, v7

    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    goto/16 :goto_1

    .line 708
    :cond_7
    const/4 v5, 0x2

    if-eq p0, v5, :cond_8

    const/16 v5, 0x15

    if-ne p0, v5, :cond_9

    .line 710
    :cond_8
    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v5

    const-wide v7, 0x408f400000000000L    # 1000.0

    div-double/2addr v5, v7

    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    goto/16 :goto_1

    .line 712
    :cond_9
    const/4 v5, 0x3

    if-ne p0, v5, :cond_a

    .line 713
    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v5

    const-wide v7, 0x40ac200000000000L    # 3600.0

    mul-double/2addr v5, v7

    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    .line 715
    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v5

    const-wide v7, 0x408f400000000000L    # 1000.0

    div-double/2addr v5, v7

    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    goto/16 :goto_1

    .line 718
    :cond_a
    const/16 v5, 0x13

    if-ne p0, v5, :cond_1

    .line 723
    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v5

    const-wide/16 v7, 0x0

    cmpl-double v5, v5, v7

    if-eqz v5, :cond_1

    .line 724
    const-wide v5, 0x40ac200000000000L    # 3600.0

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v7

    const-wide v9, 0x400ccccccccccccdL    # 3.6

    mul-double/2addr v7, v9

    div-double/2addr v5, v7

    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    goto/16 :goto_1

    .line 732
    :cond_b
    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Double;->isNaN(D)Z

    move-result v5

    if-eqz v5, :cond_c

    .line 733
    const-wide/16 v5, 0x0

    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    .line 734
    sget-object v5, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->TAG:Ljava/lang/String;

    const-string/jumbo v6, "value is NaN"

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 738
    :cond_c
    const/4 v5, 0x4

    if-ne p0, v5, :cond_d

    .line 739
    const-string v5, "%d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual {v4}, Ljava/lang/Double;->floatValue()F

    move-result v8

    float-to-int v8, v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 740
    :cond_d
    const/4 v5, 0x5

    if-ne p0, v5, :cond_f

    .line 741
    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v5

    const-wide/16 v7, 0x0

    cmpl-double v5, v5, v7

    if-lez v5, :cond_e

    .line 742
    const-string v5, "%d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Math;->round(D)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 744
    :cond_e
    const-string v0, "--"

    goto/16 :goto_2

    .line 746
    :cond_f
    const/4 v5, 0x3

    if-ne p0, v5, :cond_10

    .line 747
    new-instance v5, Ljava/text/DecimalFormat;

    const-string v6, "0.0"

    invoke-direct {v5, v6}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/text/DecimalFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 748
    :cond_10
    const/4 v5, 0x2

    if-eq p0, v5, :cond_11

    const/16 v5, 0x15

    if-ne p0, v5, :cond_13

    .line 749
    :cond_11
    new-instance v1, Ljava/text/DecimalFormat;

    const-string v5, "0.00"

    invoke-direct {v1, v5}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    .line 750
    .local v1, "df":Ljava/text/DecimalFormat;
    if-eqz p2, :cond_12

    .line 751
    sget-object v5, Ljava/math/RoundingMode;->DOWN:Ljava/math/RoundingMode;

    invoke-virtual {v1, v5}, Ljava/text/DecimalFormat;->setRoundingMode(Ljava/math/RoundingMode;)V

    .line 752
    :cond_12
    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v5

    invoke-virtual {v1, v5, v6}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v0

    .line 753
    goto/16 :goto_2

    .end local v1    # "df":Ljava/text/DecimalFormat;
    :cond_13
    const/4 v5, 0x6

    if-ne p0, v5, :cond_14

    .line 754
    const-string v5, "%d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual {v4}, Ljava/lang/Double;->intValue()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 755
    :cond_14
    const/16 v5, 0x10

    if-ne p0, v5, :cond_15

    .line 756
    new-instance v5, Ljava/text/DecimalFormat;

    const-string v6, "0.0"

    invoke-direct {v5, v6}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/text/DecimalFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 757
    :cond_15
    const/16 v5, 0x12

    if-ne p0, v5, :cond_16

    .line 758
    const-string v5, "%d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual {v4}, Ljava/lang/Double;->intValue()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 759
    :cond_16
    const/16 v5, 0x13

    if-ne p0, v5, :cond_17

    .line 760
    invoke-virtual {v4}, Ljava/lang/Double;->intValue()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 761
    .local v2, "i":Ljava/lang/Integer;
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v5

    int-to-long v5, v5

    invoke-static {v5, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkPaceValue(J)Ljava/lang/String;

    move-result-object v0

    .line 762
    goto/16 :goto_2

    .end local v2    # "i":Ljava/lang/Integer;
    :cond_17
    const/16 v5, 0x17

    if-ne p0, v5, :cond_18

    .line 763
    new-instance v5, Ljava/text/DecimalFormat;

    const-string v6, "0.0"

    invoke-direct {v5, v6}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/text/DecimalFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 764
    :cond_18
    const/16 v5, 0x18

    if-ne p0, v5, :cond_19

    .line 765
    invoke-virtual {v4}, Ljava/lang/Double;->intValue()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 767
    :cond_19
    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v5

    const-wide/high16 v7, 0x4024000000000000L    # 10.0

    cmpg-double v5, v5, v7

    if-gez v5, :cond_1a

    .line 768
    new-instance v5, Ljava/text/DecimalFormat;

    const-string v6, "#.##"

    invoke-direct {v5, v6}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/text/DecimalFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 769
    :cond_1a
    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v5

    const-wide/high16 v7, 0x4024000000000000L    # 10.0

    cmpl-double v5, v5, v7

    if-ltz v5, :cond_1b

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v5

    const-wide/high16 v7, 0x4059000000000000L    # 100.0

    cmpg-double v5, v5, v7

    if-gez v5, :cond_1b

    .line 770
    new-instance v5, Ljava/text/DecimalFormat;

    const-string v6, "#.#"

    invoke-direct {v5, v6}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/text/DecimalFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 776
    :cond_1b
    const-string v5, "%d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual {v4}, Ljava/lang/Double;->intValue()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2
.end method

.method public static checkValueByTrainingEffectLevel(I)I
    .locals 2
    .param p0, "value"    # I

    .prologue
    .line 993
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeTrainingLevel()I

    move-result v0

    .line 994
    .local v0, "level":I
    packed-switch v0, :pswitch_data_0

    .line 1012
    :cond_0
    :goto_0
    return p0

    .line 996
    :pswitch_0
    const/16 v1, 0xa

    if-ge p0, v1, :cond_0

    .line 997
    const/16 p0, 0xa

    goto :goto_0

    .line 1000
    :pswitch_1
    const/16 v1, 0x14

    if-ge p0, v1, :cond_0

    .line 1001
    const/16 p0, 0x14

    goto :goto_0

    .line 1004
    :pswitch_2
    const/16 v1, 0x1e

    if-ge p0, v1, :cond_0

    .line 1005
    const/16 p0, 0x1e

    goto :goto_0

    .line 1008
    :pswitch_3
    const/16 v1, 0x28

    if-ge p0, v1, :cond_0

    .line 1009
    const/16 p0, 0x28

    goto :goto_0

    .line 994
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static convertDpToPx(Landroid/content/res/Resources;I)F
    .locals 1
    .param p0, "resources"    # Landroid/content/res/Resources;
    .param p1, "dp"    # I

    .prologue
    .line 1120
    invoke-virtual {p0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->convertDpToPx(Landroid/util/DisplayMetrics;I)F

    move-result v0

    return v0
.end method

.method public static convertDpToPx(Landroid/util/DisplayMetrics;I)F
    .locals 2
    .param p0, "displayMetrics"    # Landroid/util/DisplayMetrics;
    .param p1, "dp"    # I

    .prologue
    .line 1131
    const/4 v0, 0x1

    int-to-float v1, p1

    invoke-static {v0, v1, p0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    return v0
.end method

.method public static disableClickFor(JLandroid/view/View;)V
    .locals 1
    .param p0, "millis"    # J
    .param p2, "view"    # Landroid/view/View;

    .prologue
    .line 1152
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/view/View;->setClickable(Z)V

    .line 1153
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils$2;

    invoke-direct {v0, p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils$2;-><init>(Landroid/view/View;)V

    invoke-virtual {p2, v0, p0, p1}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1159
    return-void
.end method

.method public static getActivityTypeGoalValue(Landroid/content/Context;II)I
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "activityType"    # I
    .param p2, "goaltype"    # I

    .prologue
    .line 926
    const/4 v0, 0x2

    .line 927
    .local v0, "val":I
    packed-switch p1, :pswitch_data_0

    .line 941
    :goto_0
    invoke-static {p2, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkDefaultValue(II)I

    move-result v1

    return v1

    .line 929
    :pswitch_0
    const v1, 0x9c47

    invoke-static {p0, v1, p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getGoalDataValue(Landroid/content/Context;II)I

    move-result v0

    .line 930
    goto :goto_0

    .line 932
    :pswitch_1
    const v1, 0x9c48

    invoke-static {p0, v1, p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getGoalDataValue(Landroid/content/Context;II)I

    move-result v0

    .line 933
    goto :goto_0

    .line 935
    :pswitch_2
    const v1, 0x9c49

    invoke-static {p0, v1, p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getGoalDataValue(Landroid/content/Context;II)I

    move-result v0

    .line 936
    goto :goto_0

    .line 938
    :pswitch_3
    const v1, 0x9c4a

    invoke-static {p0, v1, p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getGoalDataValue(Landroid/content/Context;II)I

    move-result v0

    goto :goto_0

    .line 927
    nop

    :pswitch_data_0
    .packed-switch 0x4652
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static getAllFromCacheMap(Landroid/database/Cursor;)Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;
    .locals 3
    .param p0, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 1225
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;-><init>()V

    .line 1227
    .local v0, "mapPath":Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;
    const-string v1, "exercise__id"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->setExerciseId(J)V

    .line 1228
    const-string/jumbo v1, "sample_time"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1229
    const-string v1, "create_time"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->setCreateTime(J)V

    .line 1233
    :goto_0
    const-string v1, "latitude"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->setLatitude(D)V

    .line 1234
    const-string v1, "longitude"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->setLongitude(D)V

    .line 1235
    const-string v1, "altitude"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->setAltitude(F)V

    .line 1236
    const-string v1, "accuracy"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->setAccuracy(F)V

    .line 1237
    const-string/jumbo v1, "time_zone"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->setTimeZone(I)V

    .line 1239
    return-object v0

    .line 1231
    :cond_0
    const-string/jumbo v1, "sample_time"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->setCreateTime(J)V

    goto :goto_0
.end method

.method public static getAllFromCacheRealTimeSpeed(Landroid/database/Cursor;)Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;
    .locals 3
    .param p0, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 1256
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;-><init>()V

    .line 1258
    .local v0, "realtimeData":Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;
    const-string v1, "exercise__id"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;->setExerciseId(J)V

    .line 1259
    const-string/jumbo v1, "sample_time"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1260
    const-string v1, "create_time"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;->setCreateTime(J)V

    .line 1264
    :goto_0
    const-string/jumbo v1, "speed_per_hour"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;->setSpeedPerHour(F)V

    .line 1265
    const-string v1, "data_type"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;->setDataType(I)V

    .line 1266
    const-string/jumbo v1, "time_zone"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;->setTimeZone(I)V

    .line 1268
    return-object v0

    .line 1262
    :cond_0
    const-string/jumbo v1, "sample_time"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;->setCreateTime(J)V

    goto :goto_0
.end method

.method public static getBeatMyPreviousRecordStringById(Landroid/content/Context;J)Ljava/lang/String;
    .locals 17
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "id"    # J

    .prologue
    .line 597
    sget-object v2, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "onSContextChanged exId = "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, p1

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 598
    const-string v8, ""

    .line 599
    .local v8, "beatMyPreviousRecordString":Ljava/lang/String;
    const-wide/16 v2, 0x0

    cmp-long v2, p1, v2

    if-eqz v2, :cond_1

    .line 600
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "_id = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-wide/from16 v0, p1

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 601
    .local v5, "selectionClause":Ljava/lang/String;
    const/4 v2, 0x3

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "_id"

    aput-object v3, v4, v2

    const/4 v2, 0x1

    const-string v3, "duration_millisecond"

    aput-object v3, v4, v2

    const/4 v2, 0x2

    const-string v3, "distance"

    aput-object v3, v4, v2

    .line 604
    .local v4, "projection":[Ljava/lang/String;
    const/4 v9, 0x0

    .line 606
    .local v9, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract$Exercise;->CONTENT_URI:Landroid/net/Uri;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 608
    if-eqz v9, :cond_0

    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_0

    .line 609
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    .line 611
    const-string v2, "duration_millisecond"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    const-wide/16 v6, 0x3e8

    div-long/2addr v2, v6

    long-to-int v13, v2

    .line 613
    .local v13, "durationMin":I
    const/4 v10, 0x0

    .line 614
    .local v10, "distance":F
    const-string v2, "distance"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v10

    .line 615
    div-int/lit8 v14, v13, 0x3c

    .line 616
    .local v14, "hrs":I
    mul-int/lit8 v2, v14, 0x3c

    sub-int v15, v13, v2

    .line 617
    .local v15, "mins":I
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    if-eqz v14, :cond_2

    const-string v2, "%02d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v6, v7

    invoke-static {v2, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    :goto_0
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-eqz v15, :cond_3

    const-string v2, "%02d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v6, v7

    invoke-static {v2, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    :goto_1
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 619
    .local v12, "duration":Ljava/lang/String;
    const/high16 v2, 0x447a0000    # 1000.0f

    div-float v11, v10, v2

    .line 620
    .local v11, "distanceKM":F
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "%.2f"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v11}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v16

    aput-object v16, v6, v7

    invoke-static {v3, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " km in "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v8

    .line 623
    .end local v10    # "distance":F
    .end local v11    # "distanceKM":F
    .end local v12    # "duration":Ljava/lang/String;
    .end local v13    # "durationMin":I
    .end local v14    # "hrs":I
    .end local v15    # "mins":I
    :cond_0
    if-eqz v9, :cond_1

    .line 624
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 627
    .end local v4    # "projection":[Ljava/lang/String;
    .end local v5    # "selectionClause":Ljava/lang/String;
    .end local v9    # "cursor":Landroid/database/Cursor;
    :cond_1
    return-object v8

    .line 617
    .restart local v4    # "projection":[Ljava/lang/String;
    .restart local v5    # "selectionClause":Ljava/lang/String;
    .restart local v9    # "cursor":Landroid/database/Cursor;
    .restart local v10    # "distance":F
    .restart local v13    # "durationMin":I
    .restart local v14    # "hrs":I
    .restart local v15    # "mins":I
    :cond_2
    :try_start_1
    const-string v2, "00"

    goto :goto_0

    :cond_3
    const-string v2, "00"
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 623
    .end local v10    # "distance":F
    .end local v13    # "durationMin":I
    .end local v14    # "hrs":I
    .end local v15    # "mins":I
    :catchall_0
    move-exception v2

    if-eqz v9, :cond_4

    .line 624
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v2
.end method

.method public static getBigDataTypeIconByType(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "type"    # I

    .prologue
    const v2, 0x7f020182

    const v1, 0x7f020188

    .line 894
    packed-switch p1, :pswitch_data_0

    .line 921
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 896
    :pswitch_1
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 898
    :pswitch_2
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020180

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 900
    :pswitch_3
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02018a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 902
    :pswitch_4
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02017e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 904
    :pswitch_5
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 906
    :pswitch_6
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 908
    :pswitch_7
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 910
    :pswitch_8
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020184

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 912
    :pswitch_9
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020186

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 914
    :pswitch_a
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 917
    :pswitch_b
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02067f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 919
    :pswitch_c
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020178

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto/16 :goto_0

    .line 894
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_8
        :pswitch_9
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_7
    .end packed-switch
.end method

.method public static getCountryCode()Ljava/lang/String;
    .locals 8

    .prologue
    .line 1360
    const/4 v2, 0x0

    .line 1363
    .local v2, "countryCode":Ljava/lang/String;
    :try_start_0
    const-string v4, "android.os.SystemProperties"

    invoke-static {v4}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 1364
    .local v1, "cl":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v4, "get"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    const-class v7, Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-virtual {v1, v4, v5}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 1365
    .local v3, "method":Ljava/lang/reflect/Method;
    const/4 v4, 0x0

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string/jumbo v7, "ro.csc.countryiso_code"

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Ljava/lang/String;

    move-object v2, v0

    .line 1366
    sget-object v4, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "country code:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1370
    .end local v1    # "cl":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v3    # "method":Ljava/lang/reflect/Method;
    :goto_0
    return-object v2

    .line 1367
    :catch_0
    move-exception v4

    goto :goto_0
.end method

.method public static getDataStringByType(Landroid/content/Context;I)Ljava/lang/String;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "type"    # I

    .prologue
    const-wide/16 v6, 0x0

    const v5, 0x7f090a57

    const v4, 0x7f090a49

    const v3, 0x7f090132

    .line 204
    packed-switch p1, :pswitch_data_0

    .line 261
    :pswitch_0
    const-string v3, ""

    :goto_0
    return-object v3

    .line 206
    :pswitch_1
    const v3, 0x7f090a1b

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 208
    :pswitch_2
    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 210
    :pswitch_3
    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 212
    :pswitch_4
    const v3, 0x7f0900bb

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 214
    :pswitch_5
    const v3, 0x7f090a4c

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 216
    :pswitch_6
    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 218
    :pswitch_7
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeGoalType()I

    move-result v0

    .line 219
    .local v0, "goalType":I
    const/4 v3, 0x4

    if-ne v0, v3, :cond_0

    .line 220
    const v3, 0x7f090a70

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 221
    :cond_0
    const/4 v3, 0x2

    if-ne v0, v3, :cond_1

    .line 222
    const v3, 0x7f090a6f

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 223
    :cond_1
    const/4 v3, 0x3

    if-ne v0, v3, :cond_2

    .line 224
    const v3, 0x7f090a6e

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 225
    :cond_2
    const/4 v3, 0x5

    if-ne v0, v3, :cond_3

    .line 226
    const v3, 0x7f090a6d

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 227
    :cond_3
    const/4 v3, 0x6

    if-ne v0, v3, :cond_6

    .line 228
    const-wide/16 v1, 0x0

    .line 229
    .local v1, "temp_db_value":J
    sget v3, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->diffdbduration:F

    float-to-long v1, v3

    .line 230
    cmp-long v3, v1, v6

    if-gez v3, :cond_4

    .line 231
    const v3, 0x7f090aa0

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 232
    :cond_4
    cmp-long v3, v1, v6

    if-nez v3, :cond_5

    .line 233
    const v3, 0x7f090aa2

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 235
    :cond_5
    const v3, 0x7f090aa1

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 238
    .end local v1    # "temp_db_value":J
    :cond_6
    const v3, 0x7f090a1f

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_0

    .line 241
    .end local v0    # "goalType":I
    :pswitch_8
    invoke-static {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getDataTypeGoal(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_0

    .line 244
    :pswitch_9
    const v3, 0x7f09006b

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_0

    .line 246
    :pswitch_a
    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_0

    .line 248
    :pswitch_b
    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_0

    .line 250
    :pswitch_c
    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_0

    .line 252
    :pswitch_d
    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_0

    .line 254
    :pswitch_e
    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_0

    .line 256
    :pswitch_f
    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->TAG:Ljava/lang/String;

    const-string v4, "getDataStringByType behind "

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 257
    const v3, 0x7f090680

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_0

    .line 259
    :pswitch_10
    const v3, 0x7f090a4d

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_0

    .line 204
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_0
        :pswitch_0
        :pswitch_8
        :pswitch_e
        :pswitch_0
        :pswitch_0
        :pswitch_f
        :pswitch_10
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_9
    .end packed-switch
.end method

.method private static getDataTypeGoal(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 266
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeGoalType()I

    move-result v0

    .line 267
    .local v0, "goalType2":I
    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 268
    const v1, 0x7f090a25

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 276
    :goto_0
    return-object v1

    .line 269
    :cond_0
    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 270
    const v1, 0x7f090a21

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 271
    :cond_1
    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    .line 272
    const v1, 0x7f090a23

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 273
    :cond_2
    const/4 v1, 0x5

    if-ne v0, v1, :cond_3

    .line 274
    const v1, 0x7f090a2d

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 276
    :cond_3
    const v1, 0x7f090a1f

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static getDataUnitByType(Landroid/content/Context;I)Ljava/lang/String;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "type"    # I

    .prologue
    const v1, 0x7f09080f

    const v3, 0x7f0900cc

    const v0, 0x7f0900c9

    const v2, 0x7f0900c7

    .line 332
    packed-switch p1, :pswitch_data_0

    .line 406
    :pswitch_0
    const-string v0, ""

    :goto_0
    return-object v0

    .line 334
    :pswitch_1
    const-string v0, ""

    goto :goto_0

    .line 336
    :pswitch_2
    if-nez p0, :cond_0

    .line 337
    const-string v0, "m"

    goto :goto_0

    .line 339
    :cond_0
    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 341
    :pswitch_3
    if-nez p0, :cond_1

    .line 342
    const-string v0, "m/h"

    goto :goto_0

    .line 344
    :cond_1
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 346
    :pswitch_4
    if-nez p0, :cond_2

    .line 347
    const-string v0, "kcal"

    goto :goto_0

    .line 349
    :cond_2
    const v0, 0x7f0900b9

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 351
    :pswitch_5
    if-nez p0, :cond_3

    .line 352
    const-string v0, "bpm"

    goto :goto_0

    .line 354
    :cond_3
    const v0, 0x7f0900d2

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 356
    :pswitch_6
    if-nez p0, :cond_4

    .line 357
    const-string v0, "m"

    goto :goto_0

    .line 359
    :cond_4
    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 361
    :pswitch_7
    if-nez p0, :cond_5

    .line 362
    const-string v0, "km"

    goto :goto_0

    .line 364
    :cond_5
    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 366
    :pswitch_8
    if-nez p0, :cond_6

    .line 367
    const-string v0, "ft"

    goto :goto_0

    .line 369
    :cond_6
    const v0, 0x7f090a1e

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 371
    :pswitch_9
    if-nez p0, :cond_7

    .line 372
    const-string v0, "km"

    goto :goto_0

    .line 374
    :cond_7
    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 376
    :pswitch_a
    if-nez p0, :cond_8

    .line 377
    const-string v0, "/km"

    goto :goto_0

    .line 379
    :cond_8
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 381
    :pswitch_b
    if-nez p0, :cond_9

    .line 382
    const-string v0, "km/h"

    goto :goto_0

    .line 384
    :cond_9
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 387
    :pswitch_c
    if-nez p0, :cond_a

    .line 388
    const-string/jumbo v0, "mi"

    goto/16 :goto_0

    .line 390
    :cond_a
    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 392
    :pswitch_d
    if-nez p0, :cond_b

    .line 393
    const-string/jumbo v0, "mi/h"

    goto/16 :goto_0

    .line 395
    :cond_b
    const v0, 0x7f090810

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 397
    :pswitch_e
    const v0, 0x7f0905fb

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 399
    :pswitch_f
    if-nez p0, :cond_c

    .line 400
    const-string v0, "/mi"

    goto/16 :goto_0

    .line 402
    :cond_c
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 404
    :pswitch_10
    const-string v0, ""

    goto/16 :goto_0

    .line 332
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_9
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_e
        :pswitch_a
        :pswitch_f
        :pswitch_7
        :pswitch_c
        :pswitch_10
    .end packed-switch
.end method

.method public static getEffectGoalStringByType(Landroid/content/Context;I)Ljava/lang/String;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "level"    # I

    .prologue
    .line 631
    const-string v0, ""

    .line 632
    .local v0, "valueStr":Ljava/lang/String;
    packed-switch p1, :pswitch_data_0

    .line 643
    :goto_0
    return-object v0

    .line 634
    :pswitch_0
    const v1, 0x7f090ad9

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 635
    goto :goto_0

    .line 637
    :pswitch_1
    const v1, 0x7f090a5d

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 638
    goto :goto_0

    .line 640
    :pswitch_2
    const v1, 0x7f090ad8

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 632
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static getEffectStringByType(Landroid/content/Context;II)Ljava/lang/String;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "level"    # I
    .param p2, "value"    # I

    .prologue
    .line 647
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getEffectStringByType(Landroid/content/Context;IIZ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getEffectStringByType(Landroid/content/Context;IIZ)Ljava/lang/String;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "level"    # I
    .param p2, "value"    # I
    .param p3, "isTimeStr"    # Z

    .prologue
    .line 651
    const-string v3, ""

    .line 652
    .local v3, "valueStr":Ljava/lang/String;
    const-string v2, ""

    .line 653
    .local v2, "time":Ljava/lang/String;
    packed-switch p1, :pswitch_data_0

    move-object v4, v3

    .line 677
    :goto_0
    return-object v4

    .line 655
    :pswitch_0
    const v4, 0x7f090ad9

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 670
    :goto_1
    div-int/lit8 v0, p2, 0x3c

    .line 671
    .local v0, "hrs":I
    mul-int/lit8 v4, v0, 0x3c

    sub-int v1, p2, v4

    .line 672
    .local v1, "mins":I
    if-eqz p3, :cond_2

    .line 673
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    if-eqz v0, :cond_0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v6, 0x7f0900e9

    invoke-virtual {p0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    :goto_2
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    if-eqz v1, :cond_1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v6, 0x7f0900ea

    invoke-virtual {p0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    :goto_3
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 675
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " / "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_0

    .line 658
    .end local v0    # "hrs":I
    .end local v1    # "mins":I
    :pswitch_1
    const v4, 0x7f090f6f

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 659
    goto/16 :goto_1

    .line 661
    :pswitch_2
    const v4, 0x7f090ad8

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 662
    goto/16 :goto_1

    .line 664
    :pswitch_3
    const v4, 0x7f09013e

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 665
    goto/16 :goto_1

    .line 673
    .restart local v0    # "hrs":I
    .restart local v1    # "mins":I
    :cond_0
    const-string v4, ""

    goto :goto_2

    :cond_1
    const-string v4, ""

    goto :goto_3

    :cond_2
    move-object v4, v3

    .line 677
    goto/16 :goto_0

    .line 653
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static getGoalDataValue(Landroid/content/Context;II)I
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "goalType"    # I
    .param p2, "activityType"    # I

    .prologue
    .line 1077
    const/4 v2, 0x0

    .line 1078
    .local v2, "projection":[Ljava/lang/String;
    const-string v3, "goal_type=?  AND goal_subtype=? "

    .line 1079
    .local v3, "selectionClause":Ljava/lang/String;
    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x1

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    .line 1082
    .local v4, "mSelectionArgs":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 1083
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v7, -0x1

    .line 1085
    .local v7, "goalValue":I
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Goal;->CONTENT_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1087
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 1088
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1089
    const-string/jumbo v0, "value"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v7

    .line 1093
    :cond_0
    if-eqz v6, :cond_1

    .line 1094
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1097
    :cond_1
    return v7

    .line 1093
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 1094
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method public static getGoalStringByType(Landroid/content/Context;IIIZ)Ljava/lang/String;
    .locals 15
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "type"    # I
    .param p2, "level"    # I
    .param p3, "value"    # I
    .param p4, "flag"    # Z

    .prologue
    .line 526
    packed-switch p1, :pswitch_data_0

    .line 574
    :pswitch_0
    const v12, 0x7f090a1f

    :try_start_0
    invoke-virtual {p0, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 580
    :goto_0
    return-object v12

    .line 528
    :pswitch_1
    const v12, 0x7f090ab0

    invoke-virtual {p0, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v12

    goto :goto_0

    .line 530
    :pswitch_2
    const v12, 0x7f090a1f

    invoke-virtual {p0, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v12

    goto :goto_0

    .line 534
    :pswitch_3
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getDistanceUnitFromSharedPreferences()Ljava/lang/String;

    move-result-object v12

    const-string v13, "km"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 535
    new-instance v12, Ljava/text/DecimalFormat;

    const-string v13, "0.0"

    invoke-direct {v12, v13}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    move/from16 v0, p3

    int-to-float v13, v0

    const/high16 v14, 0x447a0000    # 1000.0f

    div-float/2addr v13, v14

    float-to-double v13, v13

    invoke-virtual {v12, v13, v14}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    .line 537
    .local v3, "distance":Ljava/lang/String;
    const v12, 0x7f0900c7

    invoke-virtual {p0, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 545
    .local v11, "unit":Ljava/lang/String;
    :goto_1
    new-instance v12, Ljava/text/DecimalFormat;

    invoke-direct {v12}, Ljava/text/DecimalFormat;-><init>()V

    invoke-virtual {v12}, Ljava/text/DecimalFormat;->getDecimalFormatSymbols()Ljava/text/DecimalFormatSymbols;

    move-result-object v12

    invoke-virtual {v12}, Ljava/text/DecimalFormatSymbols;->getDecimalSeparator()C

    move-result v12

    invoke-static {v12}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v12

    const-string v13, "."

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_0

    .line 546
    new-instance v12, Ljava/text/DecimalFormat;

    invoke-direct {v12}, Ljava/text/DecimalFormat;-><init>()V

    invoke-virtual {v12}, Ljava/text/DecimalFormat;->getDecimalFormatSymbols()Ljava/text/DecimalFormatSymbols;

    move-result-object v12

    invoke-virtual {v12}, Ljava/text/DecimalFormatSymbols;->getDecimalSeparator()C

    move-result v12

    invoke-static {v12}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v12

    const-string v13, "."

    invoke-virtual {v3, v12, v13}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    .line 547
    :cond_0
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v3

    .line 550
    :try_start_1
    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v12

    invoke-static {v12}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getGoalValueInMetersFromEditText(F)I

    move-result v4

    .line 551
    .local v4, "distance1":I
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeActivityType()I

    move-result v12

    const/4 v13, 0x2

    invoke-static {p0, v12, v13, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->setActivityTypeGoalValue(Landroid/content/Context;III)I

    .line 553
    move/from16 v0, p1

    move/from16 v1, p3

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->saveRealtimeGoalType(II)V
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    .line 557
    .end local v4    # "distance1":I
    :goto_2
    :try_start_2
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const v13, 0x7f090a21

    invoke-virtual {p0, v13}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " / "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    goto/16 :goto_0

    .line 539
    .end local v3    # "distance":Ljava/lang/String;
    .end local v11    # "unit":Ljava/lang/String;
    :cond_1
    const v12, 0xf39a1

    move/from16 v0, p3

    if-le v0, v12, :cond_2

    .line 540
    const-wide/32 v12, 0xf39a1

    invoke-static {v12, v13}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getMilesFromMeters(J)Ljava/lang/String;

    move-result-object v3

    .line 543
    .restart local v3    # "distance":Ljava/lang/String;
    :goto_3
    const v12, 0x7f0900cc

    invoke-virtual {p0, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v11

    .restart local v11    # "unit":Ljava/lang/String;
    goto/16 :goto_1

    .line 542
    .end local v3    # "distance":Ljava/lang/String;
    .end local v11    # "unit":Ljava/lang/String;
    :cond_2
    move/from16 v0, p3

    int-to-long v12, v0

    invoke-static {v12, v13}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getMilesFromMeters(J)Ljava/lang/String;

    move-result-object v3

    .restart local v3    # "distance":Ljava/lang/String;
    goto :goto_3

    .line 554
    .restart local v11    # "unit":Ljava/lang/String;
    :catch_0
    move-exception v6

    .line 555
    .local v6, "exception":Ljava/lang/NumberFormatException;
    sget-object v12, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->TAG:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Number Format exception "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_2

    .line 577
    .end local v3    # "distance":Ljava/lang/String;
    .end local v6    # "exception":Ljava/lang/NumberFormatException;
    .end local v11    # "unit":Ljava/lang/String;
    :catch_1
    move-exception v5

    .line 579
    .local v5, "e":Ljava/lang/IllegalArgumentException;
    sget-object v12, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->TAG:Ljava/lang/String;

    const-string v13, "IllegalArgumentException"

    invoke-static {v12, v13}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 580
    const/4 v12, 0x0

    goto/16 :goto_0

    .line 559
    .end local v5    # "e":Ljava/lang/IllegalArgumentException;
    :pswitch_4
    :try_start_3
    div-int/lit8 v7, p3, 0x3c

    .line 560
    .local v7, "hrs":I
    mul-int/lit8 v12, v7, 0x3c

    sub-int v10, p3, v12

    .line 561
    .local v10, "mins":I
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const v13, 0x7f090a23

    invoke-virtual {p0, v13}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " / "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    if-eqz v7, :cond_3

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v14, " "

    invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const v14, 0x7f0900e9

    invoke-virtual {p0, v14}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v14, " "

    invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    :goto_4
    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    if-eqz v10, :cond_4

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v14, " "

    invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const v14, 0x7f0900ea

    invoke-virtual {p0, v14}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    :goto_5
    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    goto/16 :goto_0

    :cond_3
    const-string v12, ""

    goto :goto_4

    :cond_4
    const-string v12, ""

    goto :goto_5

    .line 565
    .end local v7    # "hrs":I
    .end local v10    # "mins":I
    :pswitch_5
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const v13, 0x7f090a25

    invoke-virtual {p0, v13}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " / "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-static/range {p3 .. p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const v13, 0x7f0900b9

    invoke-virtual {p0, v13}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    goto/16 :goto_0

    .line 568
    :pswitch_6
    move/from16 v0, p2

    move/from16 v1, p3

    move/from16 v2, p4

    invoke-static {p0, v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getEffectStringByType(Landroid/content/Context;IIZ)Ljava/lang/String;

    move-result-object v12

    goto/16 :goto_0

    .line 571
    :pswitch_7
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getPreviousGoalSelectId()J

    move-result-wide v8

    .line 572
    .local v8, "id":J
    invoke-static {p0, v8, v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getBeatMyPreviousRecordStringById(Landroid/content/Context;J)Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_1

    move-result-object v12

    goto/16 :goto_0

    .line 526
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method private static getGoalValueInMetersFromEditText(F)I
    .locals 3
    .param p0, "distance"    # F

    .prologue
    .line 587
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getDistanceUnitFromSharedPreferences()Ljava/lang/String;

    move-result-object v1

    const-string v2, "km"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 588
    const/high16 v1, 0x447a0000    # 1000.0f

    mul-float/2addr v1, p0

    invoke-static {v1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/common/utils/Utils;->getFloatFromString(Ljava/lang/String;)F

    move-result v1

    float-to-int v0, v1

    .line 593
    .local v0, "value":I
    :goto_0
    return v0

    .line 590
    .end local v0    # "value":I
    :cond_0
    invoke-static {p0}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/common/utils/Utils;->getFloatFromString(Ljava/lang/String;)F

    move-result v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/common/utils/UnitUtils$UnitConverter;->convertMilesToMeters(F)I

    move-result v0

    .restart local v0    # "value":I
    goto :goto_0
.end method

.method public static getLastLocation(Landroid/content/Context;)Lcom/google/android/gms/maps/model/LatLng;
    .locals 12
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1173
    const/4 v3, 0x0

    .line 1175
    .local v3, "selectionClause":Ljava/lang/String;
    const/4 v11, 0x0

    .line 1177
    .local v11, "location":Lcom/google/android/gms/maps/model/LatLng;
    const-string v5, "create_time DESC"

    .line 1178
    .local v5, "sortOder":Ljava/lang/String;
    const/4 v6, 0x0

    .line 1180
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$LocationData;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1183
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_2

    .line 1184
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1185
    const-string v0, "latitude"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v7

    .line 1186
    .local v7, "lat":D
    const-string v0, "longitude"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v9

    .line 1187
    .local v9, "lng":D
    new-instance v11, Lcom/google/android/gms/maps/model/LatLng;

    .end local v11    # "location":Lcom/google/android/gms/maps/model/LatLng;
    invoke-direct {v11, v7, v8, v9, v10}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1204
    .end local v7    # "lat":D
    .end local v9    # "lng":D
    .restart local v11    # "location":Lcom/google/android/gms/maps/model/LatLng;
    :cond_0
    :goto_0
    if-eqz v6, :cond_1

    .line 1205
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1208
    :cond_1
    return-object v11

    .line 1189
    :cond_2
    if-eqz v6, :cond_3

    .line 1190
    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1191
    :cond_3
    const-string v3, "latitude > 0  AND longitude > 0 "

    .line 1192
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND altitude < 1e10000"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1193
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$LocationData;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1195
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_0

    .line 1196
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1197
    const-string v0, "latitude"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v7

    .line 1198
    .restart local v7    # "lat":D
    const-string v0, "longitude"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v9

    .line 1199
    .restart local v9    # "lng":D
    new-instance v11, Lcom/google/android/gms/maps/model/LatLng;

    .end local v11    # "location":Lcom/google/android/gms/maps/model/LatLng;
    invoke-direct {v11, v7, v8, v9, v10}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .restart local v11    # "location":Lcom/google/android/gms/maps/model/LatLng;
    goto :goto_0

    .line 1204
    .end local v7    # "lat":D
    .end local v9    # "lng":D
    .end local v11    # "location":Lcom/google/android/gms/maps/model/LatLng;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_4

    .line 1205
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0
.end method

.method public static getListDataStringByType(Landroid/content/Context;I)Ljava/lang/String;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "type"    # I

    .prologue
    const v3, 0x7f090a57

    const v2, 0x7f090a4f

    const v1, 0x7f090132

    .line 281
    packed-switch p1, :pswitch_data_0

    .line 327
    :pswitch_0
    const-string v1, ""

    :goto_0
    return-object v1

    .line 283
    :pswitch_1
    const v1, 0x7f090a1b

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 285
    :pswitch_2
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 287
    :pswitch_3
    const v1, 0x7f090a49

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 289
    :pswitch_4
    const v1, 0x7f0900bb

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 291
    :pswitch_5
    const v1, 0x7f090a4c

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 293
    :pswitch_6
    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 295
    :pswitch_7
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeGoalType()I

    move-result v0

    .line 296
    .local v0, "goalType":I
    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 297
    const v1, 0x7f090a70

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 298
    :cond_0
    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 299
    const v1, 0x7f090a6f

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 300
    :cond_1
    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    .line 301
    const v1, 0x7f090a6e

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 303
    :cond_2
    const v1, 0x7f090a6d

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 306
    .end local v0    # "goalType":I
    :pswitch_8
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 308
    :pswitch_9
    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 310
    :pswitch_a
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 312
    :pswitch_b
    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 314
    :pswitch_c
    invoke-static {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getDataTypeGoal(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 316
    :pswitch_d
    const v1, 0x7f09006b

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 319
    :pswitch_e
    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 321
    :pswitch_f
    const v1, 0x7f0905fd

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 323
    :pswitch_10
    const v1, 0x7f090680

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 325
    :pswitch_11
    const v1, 0x7f090a4d

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 281
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_0
        :pswitch_0
        :pswitch_c
        :pswitch_e
        :pswitch_0
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_d
    .end packed-switch
.end method

.method public static getMapPathDB(Landroid/database/Cursor;)Ljava/util/ArrayList;
    .locals 3
    .param p0, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1212
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1214
    .local v1, "temp_locationItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;>;"
    invoke-interface {p0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1215
    :goto_0
    invoke-interface {p0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1216
    invoke-static {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getAllFromCacheMap(Landroid/database/Cursor;)Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;

    move-result-object v0

    .line 1217
    .local v0, "temp_locationItem":Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1218
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_0

    .line 1221
    .end local v0    # "temp_locationItem":Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;
    :cond_0
    return-object v1
.end method

.method public static getMilesFromMeters(J)Ljava/lang/String;
    .locals 6
    .param p0, "distance"    # J

    .prologue
    .line 1386
    long-to-double v2, p0

    const-wide v4, 0x3f445c6dea1359a4L    # 6.2137E-4

    mul-double v0, v2, v4

    .line 1387
    .local v0, "mi":D
    new-instance v2, Ljava/text/DecimalFormat;

    const-string v3, "0.0"

    invoke-direct {v2, v3}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0, v1}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public static getMilesFromMetersWhileWorkingOut(D)Ljava/lang/String;
    .locals 5
    .param p0, "distance"    # D

    .prologue
    .line 1391
    const-wide v3, 0x3f445c6dea1359a4L    # 6.2137E-4

    mul-double v1, p0, v3

    .line 1392
    .local v1, "mi":D
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v3, "0.00"

    invoke-direct {v0, v3}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    .line 1393
    .local v0, "df":Ljava/text/DecimalFormat;
    sget-object v3, Ljava/math/RoundingMode;->UP:Ljava/math/RoundingMode;

    invoke-virtual {v0, v3}, Ljava/text/DecimalFormat;->setRoundingMode(Ljava/math/RoundingMode;)V

    .line 1395
    invoke-virtual {v0, v1, v2}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public static getModeIconByType(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "type"    # I

    .prologue
    .line 510
    packed-switch p1, :pswitch_data_0

    .line 516
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 512
    :pswitch_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020321

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 514
    :pswitch_1
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020323

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 510
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static getModeStringByType(Landroid/content/Context;I)Ljava/lang/String;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "type"    # I

    .prologue
    .line 448
    packed-switch p1, :pswitch_data_0

    .line 454
    const-string v0, ""

    :goto_0
    return-object v0

    .line 450
    :pswitch_0
    const v0, 0x7f090686

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 452
    :pswitch_1
    const v0, 0x7f090687

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 448
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static getPaceIconByType(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "pace"    # I

    .prologue
    .line 877
    packed-switch p1, :pswitch_data_0

    .line 889
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 879
    :pswitch_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020314

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 881
    :pswitch_1
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f040019

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 883
    :pswitch_2
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f04001a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 885
    :pswitch_3
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020315

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 887
    :pswitch_4
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getTransparentDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 877
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static getPaceStringByType(Landroid/content/Context;I)Ljava/lang/String;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "pace"    # I

    .prologue
    .line 860
    packed-switch p1, :pswitch_data_0

    .line 872
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 862
    :pswitch_0
    const v0, 0x7f090a40

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 864
    :pswitch_1
    const v0, 0x7f090a41

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 866
    :pswitch_2
    const v0, 0x7f090a43

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 868
    :pswitch_3
    const v0, 0x7f090b95

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 870
    :pswitch_4
    const-string v0, ""

    goto :goto_0

    .line 860
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static getPserviceType()I
    .locals 2

    .prologue
    .line 1375
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getCountryCode()Ljava/lang/String;

    move-result-object v0

    const-string v1, "KR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1376
    sget v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->PSERVICE_WEATHERNEWS:I

    .line 1380
    :goto_0
    return v0

    .line 1377
    :cond_0
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getCountryCode()Ljava/lang/String;

    move-result-object v0

    const-string v1, "CN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getCountryCode()Ljava/lang/String;

    move-result-object v0

    const-string v1, "JP"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1378
    :cond_1
    sget v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->PSERVICE_NONE:I

    goto :goto_0

    .line 1380
    :cond_2
    sget v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->PSERVICE_ACCUWEATHER:I

    goto :goto_0
.end method

.method public static getRealTimeDB(Landroid/database/Cursor;)Ljava/util/ArrayList;
    .locals 3
    .param p0, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1243
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1245
    .local v1, "temp_speedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;>;"
    invoke-interface {p0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1246
    :goto_0
    invoke-interface {p0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1247
    invoke-static {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getAllFromCacheRealTimeSpeed(Landroid/database/Cursor;)Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;

    move-result-object v0

    .line 1248
    .local v0, "temp_locationItem":Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1249
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_0

    .line 1252
    .end local v0    # "temp_locationItem":Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;
    :cond_0
    return-object v1
.end method

.method public static getTransparentDrawable()Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 1399
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->transparentDrawable:Landroid/graphics/drawable/ColorDrawable;

    if-nez v0, :cond_0

    .line 1400
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->transparentDrawable:Landroid/graphics/drawable/ColorDrawable;

    .line 1401
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->transparentDrawable:Landroid/graphics/drawable/ColorDrawable;

    return-object v0
.end method

.method public static getWorkoutStatics(Landroid/content/Context;ZJ)Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils$WorkoutStastics;
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "endMode"    # Z
    .param p2, "exId"    # J

    .prologue
    .line 83
    new-instance v11, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils$WorkoutStastics;

    invoke-direct {v11}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils$WorkoutStastics;-><init>()V

    .line 87
    .local v11, "stastics":Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils$WorkoutStastics;
    const/4 v10, 0x0

    .line 89
    .local v10, "cursor":Landroid/database/Cursor;
    :try_start_0
    const-string v3, "data_type = 300001 AND "

    .line 90
    .local v3, "selectionClause":Ljava/lang/String;
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, " AVG (speed_per_hour ) AS AVGSPEED"

    aput-object v1, v2, v0

    const/4 v0, 0x1

    const-string v1, " MAX (speed_per_hour ) AS MAXSPEED"

    aput-object v1, v2, v0

    .line 94
    .local v2, "projection":[Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "exercise__id = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 95
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$RealTimeData;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 97
    if-eqz v10, :cond_0

    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 98
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    .line 99
    const/4 v0, 0x0

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, v11, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils$WorkoutStastics;->avgSpeed:F

    .line 100
    const/4 v0, 0x1

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, v11, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils$WorkoutStastics;->maxSpeed:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 103
    :cond_0
    if-eqz v10, :cond_1

    .line 104
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 108
    :cond_1
    :try_start_1
    const-string v3, "data_type = 300002 AND "

    .line 109
    const/4 v0, 0x2

    new-array v6, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, " AVG (heart_rate_per_min ) AS AVGHEARTBEATRATE"

    aput-object v1, v6, v0

    const/4 v0, 0x1

    const-string v1, " MAX (heart_rate_per_min ) AS MAXHEARTBEATRATE"

    aput-object v1, v6, v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 113
    .end local v2    # "projection":[Ljava/lang/String;
    .local v6, "projection":[Ljava/lang/String;
    :try_start_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "exercise__id = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 126
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/samsung/android/sdk/health/content/ShealthContract$RealTimeData;->CONTENT_URI:Landroid/net/Uri;

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v7, v3

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 128
    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 129
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    .line 130
    const/4 v0, 0x0

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, v11, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils$WorkoutStastics;->avgHR:F

    .line 131
    const/4 v0, 0x1

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, v11, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils$WorkoutStastics;->maxHR:F
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_4

    .line 134
    :cond_2
    if-eqz v10, :cond_3

    .line 135
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 138
    :cond_3
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, " MIN (altitude ) AS MINALTITUDE"

    aput-object v1, v2, v0

    const/4 v0, 0x1

    const-string v1, " MAX (altitude ) AS MAXALTITUDE"

    aput-object v1, v2, v0

    .line 142
    .end local v6    # "projection":[Ljava/lang/String;
    .restart local v2    # "projection":[Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "exercise__id = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 143
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND altitude < 1e10000"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 144
    const-wide/16 v0, 0x0

    iput-wide v0, v11, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils$WorkoutStastics;->minAltitude:D

    .line 145
    const-wide/16 v0, 0x0

    iput-wide v0, v11, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils$WorkoutStastics;->maxAltitude:D

    .line 147
    :try_start_3
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$LocationData;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 149
    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_5

    .line 150
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    .line 151
    const/4 v0, 0x0

    invoke-interface {v10, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_4

    .line 152
    const/4 v0, 0x0

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    float-to-double v0, v0

    iput-wide v0, v11, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils$WorkoutStastics;->minAltitude:D

    .line 153
    :cond_4
    const/4 v0, 0x1

    invoke-interface {v10, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_5

    .line 154
    const/4 v0, 0x1

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    float-to-double v0, v0

    iput-wide v0, v11, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils$WorkoutStastics;->maxAltitude:D
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 158
    :cond_5
    if-eqz v10, :cond_6

    .line 159
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 162
    :cond_6
    sget-boolean v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->SamsungLocationMonitor:Z

    if-eqz v0, :cond_8

    .line 163
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    iput-wide v0, v11, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils$WorkoutStastics;->declineAscent:D

    .line 164
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    iput-wide v0, v11, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils$WorkoutStastics;->inclineAscent:D

    .line 165
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    .end local v2    # "projection":[Ljava/lang/String;
    const/4 v0, 0x0

    const-string v1, " SUM (incline_distance ) AS MINASCENT"

    aput-object v1, v2, v0

    const/4 v0, 0x1

    const-string v1, " SUM (decline_distance ) AS MAXASCENT"

    aput-object v1, v2, v0

    .line 169
    .restart local v2    # "projection":[Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "exercise__id = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 171
    :try_start_4
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$ExerciseActivity;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 173
    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_7

    .line 174
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    .line 175
    const/4 v0, 0x1

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    float-to-double v0, v0

    iput-wide v0, v11, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils$WorkoutStastics;->declineAscent:D

    .line 176
    const/4 v0, 0x0

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    float-to-double v0, v0

    iput-wide v0, v11, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils$WorkoutStastics;->inclineAscent:D
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    .line 179
    :cond_7
    if-eqz v10, :cond_8

    .line 180
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 184
    :cond_8
    return-object v11

    .line 103
    .end local v2    # "projection":[Ljava/lang/String;
    .end local v3    # "selectionClause":Ljava/lang/String;
    :catchall_0
    move-exception v0

    if-eqz v10, :cond_9

    .line 104
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_9
    throw v0

    .line 134
    .restart local v2    # "projection":[Ljava/lang/String;
    .restart local v3    # "selectionClause":Ljava/lang/String;
    :catchall_1
    move-exception v0

    :goto_0
    if-eqz v10, :cond_a

    .line 135
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_a
    throw v0

    .line 158
    :catchall_2
    move-exception v0

    if-eqz v10, :cond_b

    .line 159
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_b
    throw v0

    .line 179
    :catchall_3
    move-exception v0

    if-eqz v10, :cond_c

    .line 180
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_c
    throw v0

    .line 134
    .end local v2    # "projection":[Ljava/lang/String;
    .restart local v6    # "projection":[Ljava/lang/String;
    :catchall_4
    move-exception v0

    move-object v2, v6

    .end local v6    # "projection":[Ljava/lang/String;
    .restart local v2    # "projection":[Ljava/lang/String;
    goto :goto_0
.end method

.method public static isExistGoalData(Landroid/content/Context;II)Z
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "goalType"    # I
    .param p2, "activityType"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 1016
    const/4 v7, 0x0

    .line 1017
    .local v7, "ret":Z
    new-array v2, v5, [Ljava/lang/String;

    const-string/jumbo v0, "value"

    aput-object v0, v2, v1

    .line 1020
    .local v2, "projection":[Ljava/lang/String;
    const-string v3, "goal_type=?  AND goal_subtype=? "

    .line 1021
    .local v3, "selectionClause":Ljava/lang/String;
    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v1

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    .line 1024
    .local v4, "mSelectionArgs":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 1026
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Goal;->CONTENT_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1028
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-lez v0, :cond_0

    .line 1029
    const/4 v7, 0x1

    .line 1031
    :cond_0
    if-eqz v6, :cond_1

    .line 1032
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1034
    :cond_1
    return v7

    .line 1031
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 1032
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method public static varargs oneSecDisableClick([Landroid/view/View;)V
    .locals 6
    .param p0, "views"    # [Landroid/view/View;

    .prologue
    .line 1167
    move-object v0, p0

    .local v0, "arr$":[Landroid/view/View;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 1168
    .local v3, "view":Landroid/view/View;
    const-wide/16 v4, 0x3e8

    invoke-static {v4, v5, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->disableClickFor(JLandroid/view/View;)V

    .line 1167
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1170
    .end local v3    # "view":Landroid/view/View;
    :cond_0
    return-void
.end method

.method public static setActivityTypeGoalValue(Landroid/content/Context;III)I
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "activityType"    # I
    .param p2, "goaltype"    # I
    .param p3, "vlaue"    # I

    .prologue
    .line 945
    const/4 v0, -0x1

    .line 946
    .local v0, "val":I
    packed-switch p1, :pswitch_data_0

    .line 959
    .end local v0    # "val":I
    :goto_0
    return v0

    .line 948
    .restart local v0    # "val":I
    :pswitch_0
    const v1, 0x9c47

    invoke-static {p0, v1, p2, p3}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->setGoalDataValue(Landroid/content/Context;III)I

    move-result v0

    goto :goto_0

    .line 951
    :pswitch_1
    const v1, 0x9c48

    invoke-static {p0, v1, p2, p3}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->setGoalDataValue(Landroid/content/Context;III)I

    move-result v0

    goto :goto_0

    .line 954
    :pswitch_2
    const v1, 0x9c49

    invoke-static {p0, v1, p2, p3}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->setGoalDataValue(Landroid/content/Context;III)I

    move-result v0

    goto :goto_0

    .line 957
    :pswitch_3
    const v1, 0x9c4a

    invoke-static {p0, v1, p2, p3}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->setGoalDataValue(Landroid/content/Context;III)I

    move-result v0

    goto :goto_0

    .line 946
    nop

    :pswitch_data_0
    .packed-switch 0x4652
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static setGoalDataValue(Landroid/content/Context;III)I
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "goalType"    # I
    .param p2, "activityType"    # I
    .param p3, "vlaue"    # I

    .prologue
    const/4 v10, 0x0

    .line 1038
    const/4 v4, -0x1

    .line 1039
    .local v4, "ret":I
    invoke-static {p0, p1, p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->isExistGoalData(Landroid/content/Context;II)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1040
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 1041
    .local v6, "values":Landroid/content/ContentValues;
    const-string/jumbo v7, "value"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1042
    const-string v5, "goal_type=?  AND goal_subtype=? "

    .line 1043
    .local v5, "selectionClause":Ljava/lang/String;
    const/4 v7, 0x2

    new-array v2, v7, [Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v2, v10

    const/4 v7, 0x1

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v2, v7

    .line 1048
    .local v2, "mSelectionArgs":[Ljava/lang/String;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    sget-object v8, Lcom/samsung/android/sdk/health/content/ShealthContract$Goal;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v7, v8, v6, v5, v2}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result v4

    .line 1073
    .end local v2    # "mSelectionArgs":[Ljava/lang/String;
    .end local v5    # "selectionClause":Ljava/lang/String;
    :goto_0
    return v4

    .line 1050
    .restart local v2    # "mSelectionArgs":[Ljava/lang/String;
    .restart local v5    # "selectionClause":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 1051
    .local v1, "ie":Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;
    sget-object v7, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "insert exception -"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1052
    .end local v1    # "ie":Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;
    :catch_1
    move-exception v0

    .line 1053
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 1057
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v2    # "mSelectionArgs":[Ljava/lang/String;
    .end local v5    # "selectionClause":Ljava/lang/String;
    .end local v6    # "values":Landroid/content/ContentValues;
    :cond_0
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 1058
    .restart local v6    # "values":Landroid/content/ContentValues;
    const-string v7, "goal_type"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1059
    const-string v7, "goal_subtype"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1060
    const-string/jumbo v7, "update_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1061
    const-string v7, "create_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1062
    const-string/jumbo v7, "value"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1063
    const-string/jumbo v7, "period"

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1064
    const-string/jumbo v7, "time_zone"

    const/16 v8, 0x37

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1065
    const/4 v3, 0x0

    .line 1067
    .local v3, "rawContactUri":Landroid/net/Uri;
    :try_start_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    sget-object v8, Lcom/samsung/android/sdk/health/content/ShealthContract$Goal;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v7, v8, v6}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_1
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v3

    .line 1071
    :goto_1
    invoke-static {v3}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v7

    long-to-int v4, v7

    goto/16 :goto_0

    .line 1068
    :catch_2
    move-exception v1

    .line 1069
    .restart local v1    # "ie":Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;
    sget-object v7, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "insert exception -"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public static varargs temporarilyDisableClick([Landroid/view/View;)V
    .locals 6
    .param p0, "views"    # [Landroid/view/View;

    .prologue
    .line 1140
    move-object v0, p0

    .local v0, "arr$":[Landroid/view/View;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 1141
    .local v3, "view":Landroid/view/View;
    const-wide/16 v4, 0x64

    invoke-static {v4, v5, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->disableClickFor(JLandroid/view/View;)V

    .line 1140
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1143
    .end local v3    # "view":Landroid/view/View;
    :cond_0
    return-void
.end method
