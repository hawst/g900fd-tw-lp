.class final Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateAngle;
.super Ljava/lang/Object;
.source "HeartrateAngle.java"


# static fields
.field static final ZERO:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateAngle;


# instance fields
.field public final degreeValue:F


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 36
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateAngle;

    invoke-direct {v0, v1, v1}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateAngle;-><init>(FF)V

    sput-object v0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateAngle;->ZERO:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateAngle;

    return-void
.end method

.method private constructor <init>(FF)V
    .locals 0
    .param p1, "degreeValue"    # F
    .param p2, "radianValue"    # F

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput p1, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateAngle;->degreeValue:F

    .line 26
    return-void
.end method

.method static fromDegrees(F)Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateAngle;
    .locals 5
    .param p0, "degreeValue"    # F

    .prologue
    .line 29
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateAngle;

    float-to-double v1, p0

    const-wide v3, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v1, v3

    const-wide v3, 0x4066800000000000L    # 180.0

    div-double/2addr v1, v3

    double-to-float v1, v1

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateAngle;-><init>(FF)V

    return-object v0
.end method

.method static fromRadians(F)Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateAngle;
    .locals 5
    .param p0, "radianValue"    # F

    .prologue
    .line 33
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateAngle;

    const/high16 v1, 0x43340000    # 180.0f

    mul-float/2addr v1, p0

    float-to-double v1, v1

    const-wide v3, 0x400921fb54442d18L    # Math.PI

    div-double/2addr v1, v3

    double-to-float v1, v1

    invoke-direct {v0, v1, p0}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateAngle;-><init>(FF)V

    return-object v0
.end method
