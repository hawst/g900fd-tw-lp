.class Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$GridAdapter$1;
.super Ljava/lang/Object;
.source "ExerciseProGridView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$GridAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$GridAdapter;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$GridAdapter;)V
    .locals 0

    .prologue
    .line 226
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$GridAdapter$1;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$GridAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 229
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 230
    .local v1, "pos":I
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$GridAdapter$1;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$GridAdapter;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$GridAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;

    .line 231
    .local v0, "exercise":Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$GridAdapter$1;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$GridAdapter;

    iget-object v2, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$GridAdapter;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->bitmapLoader:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;)Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->getFilePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader;->deleteCache(Ljava/lang/String;)V

    .line 232
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$GridAdapter$1;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$GridAdapter;

    iget-object v2, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$GridAdapter;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->onGridItemListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$OnExerciseGridItemListener;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->access$100(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$OnExerciseGridItemListener;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 233
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$GridAdapter$1;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$GridAdapter;

    iget-object v2, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$GridAdapter;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->onGridItemListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$OnExerciseGridItemListener;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->access$100(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$OnExerciseGridItemListener;

    move-result-object v3

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$GridAdapter$1;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$GridAdapter;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$GridAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;

    invoke-interface {v3, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$OnExerciseGridItemListener;->onDeleteButtonClick(Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;)V

    .line 235
    :cond_0
    return-void
.end method
