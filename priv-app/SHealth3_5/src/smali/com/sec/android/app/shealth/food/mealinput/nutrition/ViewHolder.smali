.class Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;
.super Ljava/lang/Object;
.source "ViewHolder.java"


# static fields
.field static final SPLIT_LINE_IDS:[I


# instance fields
.field calories:Landroid/widget/TextView;

.field cholesterol:Landroid/widget/TextView;

.field cholesterolPercent:Landroid/widget/TextView;

.field dailyValues:Landroid/widget/TextView;

.field dailyValuesInfo:Landroid/widget/TextView;

.field dietaryFiber:Landroid/widget/TextView;

.field dietaryFiberPercent:Landroid/widget/TextView;

.field dropDownButton:Landroid/view/View;

.field dropDownClickableArea:Landroid/view/View;

.field nutritionBlocksView:Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionBlockView;

.field protein:Landroid/widget/TextView;

.field saturatedFat:Landroid/widget/TextView;

.field saturatedFatPercent:Landroid/widget/TextView;

.field servingSize:Landroid/widget/TextView;

.field sodium:Landroid/widget/TextView;

.field sodiumPercent:Landroid/widget/TextView;

.field splitLines:[Landroid/view/View;

.field sugars:Landroid/widget/TextView;

.field titleName:Landroid/widget/TextView;

.field totalCarbohydrate:Landroid/widget/TextView;

.field totalCarbohydratePercent:Landroid/widget/TextView;

.field totalFat:Landroid/widget/TextView;

.field totalFatPercent:Landroid/widget/TextView;

.field transFat:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const/16 v0, 0x9

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;->SPLIT_LINE_IDS:[I

    return-void

    :array_0
    .array-data 4
        0x7f08042c
        0x7f08042f
        0x7f080433
        0x7f080436
        0x7f080439
        0x7f08043c
        0x7f080440
        0x7f080442
        0x7f080421
    .end array-data
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
