.class public Lcom/sec/android/app/shealth/food/app/FoodConfiguration;
.super Ljava/lang/Object;
.source "FoodConfiguration.java"

# interfaces
.implements Lcom/sec/android/app/shealth/food/app/IFoodConfiguration;


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/app/FoodConfiguration;->mContext:Landroid/content/Context;

    .line 28
    return-void
.end method


# virtual methods
.method public isAddFoodEnabled()Z
    .locals 2

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/app/FoodConfiguration;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b000e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method public isBarcodeSearchEnabled()Z
    .locals 2

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/app/FoodConfiguration;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b000d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method public isEditSelectedItemEnabled()Z
    .locals 2

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/app/FoodConfiguration;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0012

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method public isHorizontalViewEnabled()Z
    .locals 2

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/app/FoodConfiguration;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0013

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method public isMultiSelectedFromGalleryEnabled()Z
    .locals 2

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/app/FoodConfiguration;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0010

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method public isMyMealEnabled()Z
    .locals 2

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/app/FoodConfiguration;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0011

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method public isVoiceSearchEnabled()Z
    .locals 2

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/app/FoodConfiguration;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b000c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    return v0
.end method
