.class public Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarAnimator;
.super Ljava/lang/Object;


# instance fields
.field private final MOVE_MAX:I

.field private final MOVE_MIN:I

.field private final MOVE_RANGE:I

.field private mBackButton:Landroid/view/View;

.field private mContext:Landroid/content/Context;

.field private mMoveLp:Landroid/widget/LinearLayout$LayoutParams;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarAnimator;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarAnimator;->mBackButton:Landroid/view/View;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarAnimator;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$dimen;->action_bar_back_button_move_animation_range:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarAnimator;->MOVE_RANGE:I

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarAnimator;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$dimen;->action_bar_up_button_margin_left:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarAnimator;->MOVE_MAX:I

    iget v0, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarAnimator;->MOVE_MAX:I

    iget v1, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarAnimator;->MOVE_RANGE:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarAnimator;->MOVE_MIN:I

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarAnimator;->mBackButton:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarAnimator;->mMoveLp:Landroid/widget/LinearLayout$LayoutParams;

    :cond_0
    return-void
.end method

.method private updateViewToMove(I)V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarAnimator;->mBackButton:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarAnimator;->mMoveLp:Landroid/widget/LinearLayout$LayoutParams;

    iput p1, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarAnimator;->mBackButton:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    :cond_0
    return-void
.end method


# virtual methods
.method public animateActionBar(F)V
    .locals 3

    iget v0, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarAnimator;->MOVE_MIN:I

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarAnimator;->MOVE_RANGE:I

    int-to-float v1, v1

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float/2addr v2, p1

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    float-to-int v0, v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarAnimator;->updateViewToMove(I)V

    return-void
.end method
