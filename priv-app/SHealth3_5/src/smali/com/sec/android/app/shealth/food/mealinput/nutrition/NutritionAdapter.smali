.class public Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;
.super Landroid/widget/BaseAdapter;
.source "NutritionAdapter.java"


# static fields
.field private static final LOG_TAG:Ljava/lang/String;

.field private static final SELECTED_ITEM_POSITION:Ljava/lang/String; = "SELECTED_ITEM_POSITION"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDataArray:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;",
            ">;"
        }
    .end annotation
.end field

.field private mFoodInfoDataArray:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;",
            ">;"
        }
    .end annotation
.end field

.field private mLastSelectedDropDownContainer:Landroid/view/View;

.field private mLastSelectedDropDownIcon:Landroid/view/View;

.field private mNutritionItemFiller:Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionItemFiller;

.field private mSelectedItemPosition:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->getLogTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/List;Ljava/util/List;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 50
    .local p2, "mealItems":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;>;"
    .local p3, "foodInfoDatas":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;>;"
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 40
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;->mSelectedItemPosition:I

    .line 51
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;->mContext:Landroid/content/Context;

    .line 52
    iput-object p2, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;->mDataArray:Ljava/util/List;

    .line 53
    iput-object p3, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;->mFoodInfoDataArray:Ljava/util/List;

    .line 54
    new-instance v0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionItemFiller;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionItemFiller;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;->mNutritionItemFiller:Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionItemFiller;

    .line 55
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;Landroid/view/View;Landroid/view/View;ZI)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # Landroid/view/View;
    .param p3, "x3"    # Z
    .param p4, "x4"    # I

    .prologue
    .line 34
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;->toggleDropdownState(Landroid/view/View;Landroid/view/View;ZI)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;->mLastSelectedDropDownIcon:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;->mLastSelectedDropDownContainer:Landroid/view/View;

    return-object v0
.end method

.method private fillNutritionInfo(Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;)V
    .locals 4
    .param p1, "viewHolder"    # Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;
    .param p2, "mealItem"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;
    .param p3, "foodInfoData"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    .prologue
    .line 146
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {p2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->getFoodInfoId()J

    move-result-wide v2

    invoke-direct {p0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;->loadExtraFoodInfo(Landroid/content/Context;J)Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;

    move-result-object v0

    .line 147
    .local v0, "extraFoodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;->mNutritionItemFiller:Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionItemFiller;

    invoke-virtual {v1, p1, p2, p3, v0}, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionItemFiller;->fillView(Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;)V

    .line 148
    return-void
.end method

.method private initDropDownButton(Landroid/view/View;I)V
    .locals 2
    .param p1, "itemView"    # Landroid/view/View;
    .param p2, "position"    # I

    .prologue
    .line 88
    const v1, 0x7f080424

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 89
    .local v0, "dropDownButton":Landroid/view/View;
    new-instance v1, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter$1;-><init>(Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;Landroid/view/View;I)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 104
    new-instance v1, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter$2;

    invoke-direct {v1, p0, v0}, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter$2;-><init>(Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;Landroid/view/View;)V

    invoke-virtual {p1, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 112
    iget v1, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;->mSelectedItemPosition:I

    if-ne p2, v1, :cond_0

    .line 113
    invoke-virtual {v0}, Landroid/view/View;->callOnClick()Z

    .line 115
    :cond_0
    return-void
.end method

.method private initViewHolder(Landroid/view/View;)Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;
    .locals 4
    .param p1, "itemView"    # Landroid/view/View;

    .prologue
    .line 156
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;

    .line 157
    .local v1, "viewHolder":Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;
    if-nez v1, :cond_1

    .line 158
    new-instance v1, Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;

    .end local v1    # "viewHolder":Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;
    invoke-direct {v1}, Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;-><init>()V

    .line 159
    .restart local v1    # "viewHolder":Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;
    const v2, 0x7f080425

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v1, Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;->titleName:Landroid/widget/TextView;

    .line 160
    const v2, 0x7f080426

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v1, Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;->servingSize:Landroid/widget/TextView;

    .line 161
    const v2, 0x7f080428

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v1, Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;->calories:Landroid/widget/TextView;

    .line 162
    const v2, 0x7f08042d

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v1, Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;->totalFat:Landroid/widget/TextView;

    .line 163
    const v2, 0x7f08043a

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v1, Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;->totalCarbohydrate:Landroid/widget/TextView;

    .line 164
    const v2, 0x7f080441

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v1, Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;->protein:Landroid/widget/TextView;

    .line 165
    const v2, 0x7f080430

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v1, Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;->saturatedFat:Landroid/widget/TextView;

    .line 166
    const v2, 0x7f080432

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v1, Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;->transFat:Landroid/widget/TextView;

    .line 167
    const v2, 0x7f080434

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v1, Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;->cholesterol:Landroid/widget/TextView;

    .line 168
    const v2, 0x7f080437

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v1, Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;->sodium:Landroid/widget/TextView;

    .line 169
    const v2, 0x7f08043d

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v1, Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;->dietaryFiber:Landroid/widget/TextView;

    .line 170
    const v2, 0x7f08043f

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v1, Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;->sugars:Landroid/widget/TextView;

    .line 171
    const v2, 0x7f08042e

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v1, Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;->totalFatPercent:Landroid/widget/TextView;

    .line 172
    const v2, 0x7f080431

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v1, Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;->saturatedFatPercent:Landroid/widget/TextView;

    .line 173
    const v2, 0x7f080435

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v1, Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;->cholesterolPercent:Landroid/widget/TextView;

    .line 174
    const v2, 0x7f080438

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v1, Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;->sodiumPercent:Landroid/widget/TextView;

    .line 175
    const v2, 0x7f08043b

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v1, Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;->totalCarbohydratePercent:Landroid/widget/TextView;

    .line 176
    const v2, 0x7f08043e

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v1, Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;->dietaryFiberPercent:Landroid/widget/TextView;

    .line 177
    const v2, 0x7f08042b

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v1, Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;->dailyValues:Landroid/widget/TextView;

    .line 178
    const v2, 0x7f080444

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v1, Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;->dailyValuesInfo:Landroid/widget/TextView;

    .line 179
    const v2, 0x7f080424

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;->dropDownButton:Landroid/view/View;

    .line 180
    const v2, 0x7f080443

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionBlockView;

    iput-object v2, v1, Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;->nutritionBlocksView:Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionBlockView;

    .line 181
    const v2, 0x7f080423

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;->dropDownClickableArea:Landroid/view/View;

    .line 182
    sget-object v2, Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;->SPLIT_LINE_IDS:[I

    array-length v2, v2

    new-array v2, v2, [Landroid/view/View;

    iput-object v2, v1, Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;->splitLines:[Landroid/view/View;

    .line 183
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v2, Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;->SPLIT_LINE_IDS:[I

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 184
    iget-object v2, v1, Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;->splitLines:[Landroid/view/View;

    sget-object v3, Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;->SPLIT_LINE_IDS:[I

    aget v3, v3, v0

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    aput-object v3, v2, v0

    .line 183
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 186
    :cond_0
    invoke-virtual {p1, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 188
    .end local v0    # "i":I
    :cond_1
    return-object v1
.end method

.method private loadExtraFoodInfo(Landroid/content/Context;J)Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "foodInfoId"    # J

    .prologue
    .line 151
    new-instance v0, Lcom/sec/android/app/shealth/food/fooddao/ExtraFoodInfoDaoImplDb;

    invoke-direct {v0, p1}, Lcom/sec/android/app/shealth/food/fooddao/ExtraFoodInfoDaoImplDb;-><init>(Landroid/content/Context;)V

    .line 152
    .local v0, "extraFoodInfoDao":Lcom/sec/android/app/shealth/food/fooddao/ExtraFoodInfoDao;
    invoke-interface {v0, p2, p3}, Lcom/sec/android/app/shealth/food/fooddao/ExtraFoodInfoDao;->getExtraByFoodId(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;

    move-result-object v1

    return-object v1
.end method

.method private toggleDropdownState(Landroid/view/View;Landroid/view/View;ZI)V
    .locals 3
    .param p1, "dropDownIcon"    # Landroid/view/View;
    .param p2, "dropDownContainer"    # Landroid/view/View;
    .param p3, "isUnfold"    # Z
    .param p4, "position"    # I

    .prologue
    const/4 v1, 0x0

    .line 136
    invoke-virtual {p1, p3}, Landroid/view/View;->setSelected(Z)V

    .line 138
    if-eqz p3, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f09021b

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 139
    if-eqz p3, :cond_1

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 140
    if-eqz p3, :cond_2

    .end local p1    # "dropDownIcon":Landroid/view/View;
    :goto_2
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;->mLastSelectedDropDownIcon:Landroid/view/View;

    .line 141
    if-eqz p3, :cond_3

    .end local p2    # "dropDownContainer":Landroid/view/View;
    :goto_3
    iput-object p2, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;->mLastSelectedDropDownContainer:Landroid/view/View;

    .line 142
    if-eqz p3, :cond_4

    .end local p4    # "position":I
    :goto_4
    iput p4, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;->mSelectedItemPosition:I

    .line 143
    return-void

    .line 138
    .restart local p1    # "dropDownIcon":Landroid/view/View;
    .restart local p2    # "dropDownContainer":Landroid/view/View;
    .restart local p4    # "position":I
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f09021a

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 139
    :cond_1
    const/16 v0, 0x8

    goto :goto_1

    :cond_2
    move-object p1, v1

    .line 140
    goto :goto_2

    .end local p1    # "dropDownIcon":Landroid/view/View;
    :cond_3
    move-object p2, v1

    .line 141
    goto :goto_3

    .line 142
    .end local p2    # "dropDownContainer":Landroid/view/View;
    :cond_4
    const/4 p4, -0x1

    goto :goto_4
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;->mDataArray:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;->mDataArray:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 69
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 74
    move-object v0, p2

    .line 75
    .local v0, "view":Landroid/view/View;
    if-nez v0, :cond_0

    .line 76
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;->mContext:Landroid/content/Context;

    const v3, 0x7f0300fd

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 81
    :goto_0
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;->initViewHolder(Landroid/view/View;)Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;

    move-result-object v1

    .line 82
    .local v1, "viewHolder":Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;
    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;->initDropDownButton(Landroid/view/View;I)V

    .line 83
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;->mDataArray:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;->mFoodInfoDataArray:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    invoke-direct {p0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;->fillNutritionInfo(Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;)V

    .line 84
    return-object v0

    .line 78
    .end local v1    # "viewHolder":Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;
    :cond_0
    const v2, 0x7f08042a

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 79
    const v2, 0x7f080424

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setSelected(Z)V

    goto :goto_0
.end method

.method public restoreInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "restoreBundle"    # Landroid/os/Bundle;

    .prologue
    .line 132
    const-string v0, "SELECTED_ITEM_POSITION"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;->mSelectedItemPosition:I

    .line 133
    return-void
.end method

.method public saveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 123
    const-string v0, "SELECTED_ITEM_POSITION"

    iget v1, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;->mSelectedItemPosition:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 124
    return-void
.end method
