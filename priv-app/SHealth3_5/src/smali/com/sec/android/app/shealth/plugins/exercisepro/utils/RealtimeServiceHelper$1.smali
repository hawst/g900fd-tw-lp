.class Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper$1;
.super Ljava/lang/Object;
.source "RealtimeServiceHelper.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;)V
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "className"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 81
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "########## [StatusActivity] onServiceConnected ##########"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;

    check-cast p2, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$RealtimeHealthBinder;

    .end local p2    # "service":Landroid/os/IBinder;
    invoke-virtual {p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$RealtimeHealthBinder;->getService()Landroid/app/Service;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->mRealtimeService:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->access$102(Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    .line 88
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->mHealthListener:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper$IHealthServiecListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;)Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper$IHealthServiecListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->mHealthListener:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper$IHealthServiecListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;)Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper$IHealthServiecListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper$IHealthServiecListener;->onServiceConnected()V

    .line 91
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->mIsConnected:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->access$302(Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;Z)Z

    .line 92
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "className"    # Landroid/content/ComponentName;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->mIsConnected:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->access$302(Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;Z)Z

    .line 99
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->mHealthListener:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper$IHealthServiecListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;)Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper$IHealthServiecListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 100
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->mHealthListener:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper$IHealthServiecListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;)Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper$IHealthServiecListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper$IHealthServiecListener;->onServiceDisConnected()V

    .line 103
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->mRealtimeService:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->access$102(Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    .line 104
    return-void
.end method
