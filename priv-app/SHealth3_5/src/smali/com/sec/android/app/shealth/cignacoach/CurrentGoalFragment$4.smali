.class Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$4;
.super Ljava/lang/Object;
.source "CurrentGoalFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->expandGroup(IZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;

.field final synthetic val$groupPosition:I

.field final synthetic val$isRetain:Z


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;ZI)V
    .locals 0

    .prologue
    .line 621
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$4;->this$0:Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;

    iput-boolean p2, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$4;->val$isRetain:Z

    iput p3, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$4;->val$groupPosition:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 625
    iget-boolean v2, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$4;->val$isRetain:Z

    if-eqz v2, :cond_0

    .line 626
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$4;->this$0:Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mExpandableListAdapter:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;
    invoke-static {v2}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->access$1000(Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;)Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->getGroupCount()I

    move-result v0

    .line 627
    .local v0, "adapterGroupCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 628
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$4;->this$0:Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mExpandableListView:Landroid/widget/ExpandableListView;
    invoke-static {v2}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->access$1100(Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;)Landroid/widget/ExpandableListView;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/widget/ExpandableListView;->collapseGroup(I)Z

    .line 627
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 631
    .end local v0    # "adapterGroupCount":I
    .end local v1    # "i":I
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$4;->this$0:Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;

    iget v3, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$4;->val$groupPosition:I

    # invokes: Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->expandGroup(I)V
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->access$1200(Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;I)V

    .line 632
    return-void
.end method
