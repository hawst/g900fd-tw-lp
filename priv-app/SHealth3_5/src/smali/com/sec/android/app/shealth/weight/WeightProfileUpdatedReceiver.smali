.class public Lcom/sec/android/app/shealth/weight/WeightProfileUpdatedReceiver;
.super Landroid/content/BroadcastReceiver;
.source "WeightProfileUpdatedReceiver.java"


# static fields
.field private static final HEALTH_PROFILE_UPDATED:Ljava/lang/String; = "com.samsung.android.sdk.health.sensor.action.PROFILE_UPDATED"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mWeightDao:Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-class v0, Lcom/sec/android/app/shealth/weight/WeightProfileUpdatedReceiver;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/weight/WeightProfileUpdatedReceiver;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private addWeightDataFromProfile(Landroid/content/Context;Lcom/samsung/android/sdk/health/content/ShealthProfile;)V
    .locals 5
    .param p1, "aContext"    # Landroid/content/Context;
    .param p2, "profile"    # Lcom/samsung/android/sdk/health/content/ShealthProfile;

    .prologue
    .line 43
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {p2}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getLastProfileUpdateTime()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-gez v1, :cond_0

    .line 44
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    invoke-virtual {p2}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getWeight()F

    move-result v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getTimeWithoutSeconds(J)J

    move-result-wide v2

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;-><init>(FJ)V

    .line 52
    .local v0, "wtData":Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;
    :goto_0
    invoke-virtual {p2}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getHeight()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->setHeight(F)V

    .line 53
    sget-object v1, Lcom/sec/android/app/shealth/weight/WeightProfileUpdatedReceiver;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "~~ onReceive WeightProfileUpdatedReceiver ~~ updating in DB"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 54
    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/WeightProfileUpdatedReceiver;->mWeightDao:Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;

    invoke-interface {v1, v0}, Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;->insertOrUpdateData(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;)J

    .line 55
    return-void

    .line 48
    .end local v0    # "wtData":Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;
    :cond_0
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    invoke-virtual {p2}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getWeight()F

    move-result v1

    invoke-virtual {p2}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getLastProfileUpdateTime()J

    move-result-wide v2

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;-><init>(FJ)V

    .restart local v0    # "wtData":Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;
    goto :goto_0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "aContext"    # Landroid/content/Context;
    .param p2, "aIntent"    # Landroid/content/Intent;

    .prologue
    .line 22
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    const-string v2, "com.samsung.android.sdk.health.sensor.action.PROFILE_UPDATED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 24
    sget-object v2, Lcom/sec/android/app/shealth/weight/WeightProfileUpdatedReceiver;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "~~ onReceive WeightProfileUpdatedReceiver ~~"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 25
    new-instance v0, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-direct {v0, p1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    .line 26
    .local v0, "profile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    new-instance v2, Lcom/sec/android/app/shealth/weight/weightdao/WeightDaoImplDb;

    invoke-direct {v2, p1}, Lcom/sec/android/app/shealth/weight/weightdao/WeightDaoImplDb;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/weight/WeightProfileUpdatedReceiver;->mWeightDao:Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;

    .line 27
    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/WeightProfileUpdatedReceiver;->mWeightDao:Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-interface {v2, v3, v4}, Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;->getLastData(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    move-result-object v1

    .line 28
    .local v1, "wgtData":Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getValue()F

    move-result v2

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getWeight()F

    move-result v3

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_0

    .line 30
    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/shealth/weight/WeightProfileUpdatedReceiver;->addWeightDataFromProfile(Landroid/content/Context;Lcom/samsung/android/sdk/health/content/ShealthProfile;)V

    .line 33
    .end local v0    # "profile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    .end local v1    # "wgtData":Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;
    :cond_0
    return-void
.end method
