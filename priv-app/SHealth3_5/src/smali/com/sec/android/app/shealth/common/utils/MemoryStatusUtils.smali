.class public Lcom/sec/android/app/shealth/common/utils/MemoryStatusUtils;
.super Ljava/lang/Object;
.source "MemoryStatusUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static externalMemoryAvailable()Z
    .locals 2

    .prologue
    .line 21
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "mounted"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private static getAvailableBlocks(Landroid/os/StatFs;)J
    .locals 2
    .param p0, "statFs"    # Landroid/os/StatFs;

    .prologue
    .line 122
    if-nez p0, :cond_0

    .line 124
    const-wide/16 v0, 0x0

    .line 133
    :goto_0
    return-wide v0

    .line 127
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_1

    .line 129
    invoke-virtual {p0}, Landroid/os/StatFs;->getAvailableBlocksLong()J

    move-result-wide v0

    goto :goto_0

    .line 133
    :cond_1
    invoke-virtual {p0}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v0

    int-to-long v0, v0

    goto :goto_0
.end method

.method public static getAvailableExternalMemorySize()D
    .locals 6

    .prologue
    .line 59
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/MemoryStatusUtils;->externalMemoryAvailable()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 60
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    .line 61
    .local v0, "path":Ljava/io/File;
    new-instance v1, Landroid/os/StatFs;

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 62
    .local v1, "statFs":Landroid/os/StatFs;
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x12

    if-lt v2, v3, :cond_0

    .line 64
    invoke-virtual {v1}, Landroid/os/StatFs;->getAvailableBytes()J

    move-result-wide v2

    long-to-double v2, v2

    .line 69
    :goto_0
    return-wide v2

    .line 66
    :cond_0
    invoke-static {v1}, Lcom/sec/android/app/shealth/common/utils/MemoryStatusUtils;->getAvailableBlocks(Landroid/os/StatFs;)J

    move-result-wide v2

    invoke-static {v1}, Lcom/sec/android/app/shealth/common/utils/MemoryStatusUtils;->getBlocksSize(Landroid/os/StatFs;)J

    move-result-wide v4

    mul-long/2addr v2, v4

    long-to-double v2, v2

    goto :goto_0

    .line 69
    .end local v0    # "path":Ljava/io/File;
    .end local v1    # "statFs":Landroid/os/StatFs;
    :cond_1
    const-wide/high16 v2, -0x4010000000000000L    # -1.0

    goto :goto_0
.end method

.method public static getAvailableInternalMemorySize()D
    .locals 6

    .prologue
    .line 32
    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v0

    .line 33
    .local v0, "path":Ljava/io/File;
    new-instance v1, Landroid/os/StatFs;

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 35
    .local v1, "statFs":Landroid/os/StatFs;
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x12

    if-lt v2, v3, :cond_0

    .line 37
    invoke-virtual {v1}, Landroid/os/StatFs;->getAvailableBytes()J

    move-result-wide v2

    long-to-double v2, v2

    .line 39
    :goto_0
    return-wide v2

    :cond_0
    invoke-static {v1}, Lcom/sec/android/app/shealth/common/utils/MemoryStatusUtils;->getAvailableBlocks(Landroid/os/StatFs;)J

    move-result-wide v2

    invoke-static {v1}, Lcom/sec/android/app/shealth/common/utils/MemoryStatusUtils;->getBlocksSize(Landroid/os/StatFs;)J

    move-result-wide v4

    mul-long/2addr v2, v4

    long-to-double v2, v2

    goto :goto_0
.end method

.method private static getBlockCount(Landroid/os/StatFs;)J
    .locals 2
    .param p0, "statFs"    # Landroid/os/StatFs;

    .prologue
    .line 140
    if-nez p0, :cond_0

    .line 142
    const-wide/16 v0, 0x0

    .line 151
    :goto_0
    return-wide v0

    .line 145
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_1

    .line 147
    invoke-virtual {p0}, Landroid/os/StatFs;->getBlockCountLong()J

    move-result-wide v0

    goto :goto_0

    .line 151
    :cond_1
    invoke-virtual {p0}, Landroid/os/StatFs;->getBlockCount()I

    move-result v0

    int-to-long v0, v0

    goto :goto_0
.end method

.method private static getBlocksSize(Landroid/os/StatFs;)J
    .locals 2
    .param p0, "statFs"    # Landroid/os/StatFs;

    .prologue
    .line 104
    if-nez p0, :cond_0

    .line 106
    const-wide/16 v0, 0x0

    .line 115
    :goto_0
    return-wide v0

    .line 109
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_1

    .line 111
    invoke-virtual {p0}, Landroid/os/StatFs;->getBlockSizeLong()J

    move-result-wide v0

    goto :goto_0

    .line 115
    :cond_1
    invoke-virtual {p0}, Landroid/os/StatFs;->getBlockSize()I

    move-result v0

    int-to-long v0, v0

    goto :goto_0
.end method

.method public static getTotalExternalMemorySize()D
    .locals 6

    .prologue
    .line 78
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/MemoryStatusUtils;->externalMemoryAvailable()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 79
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    .line 80
    .local v0, "path":Ljava/io/File;
    new-instance v1, Landroid/os/StatFs;

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 81
    .local v1, "statFs":Landroid/os/StatFs;
    invoke-static {v1}, Lcom/sec/android/app/shealth/common/utils/MemoryStatusUtils;->getBlockCount(Landroid/os/StatFs;)J

    move-result-wide v2

    invoke-static {v1}, Lcom/sec/android/app/shealth/common/utils/MemoryStatusUtils;->getBlocksSize(Landroid/os/StatFs;)J

    move-result-wide v4

    mul-long/2addr v2, v4

    long-to-double v2, v2

    .line 84
    :goto_0
    return-wide v2

    .end local v0    # "path":Ljava/io/File;
    .end local v1    # "statFs":Landroid/os/StatFs;
    :cond_0
    const-wide/high16 v2, -0x4010000000000000L    # -1.0

    goto :goto_0
.end method

.method public static getTotalInternalMemorySize()D
    .locals 6

    .prologue
    .line 48
    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v0

    .line 49
    .local v0, "path":Ljava/io/File;
    new-instance v1, Landroid/os/StatFs;

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 50
    .local v1, "statFs":Landroid/os/StatFs;
    invoke-static {v1}, Lcom/sec/android/app/shealth/common/utils/MemoryStatusUtils;->getBlockCount(Landroid/os/StatFs;)J

    move-result-wide v2

    invoke-static {v1}, Lcom/sec/android/app/shealth/common/utils/MemoryStatusUtils;->getBlocksSize(Landroid/os/StatFs;)J

    move-result-wide v4

    mul-long/2addr v2, v4

    long-to-double v2, v2

    return-wide v2
.end method

.method public static isNotEnoughMemory()Z
    .locals 6

    .prologue
    .line 93
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string/jumbo v3, "storage"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/storage/StorageManager;

    .line 94
    .local v0, "storageManager":Landroid/os/storage/StorageManager;
    invoke-virtual {v0}, Landroid/os/storage/StorageManager;->getVolumeList()[Landroid/os/storage/StorageVolume;

    move-result-object v1

    .line 96
    .local v1, "storageVolumes":[Landroid/os/storage/StorageVolume;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/MemoryStatusUtils;->getAvailableInternalMemorySize()D

    move-result-wide v2

    const-wide/high16 v4, 0x4189000000000000L    # 5.24288E7

    cmpg-double v2, v2, v4

    if-ltz v2, :cond_0

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/MemoryStatusUtils;->getAvailableExternalMemorySize()D

    move-result-wide v2

    const-wide/high16 v4, 0x4199000000000000L    # 1.048576E8

    cmpg-double v2, v2, v4

    if-lez v2, :cond_0

    array-length v2, v1

    const/4 v3, 0x2

    if-ge v2, v3, :cond_1

    .line 97
    :cond_0
    const/4 v2, 0x1

    .line 99
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method
