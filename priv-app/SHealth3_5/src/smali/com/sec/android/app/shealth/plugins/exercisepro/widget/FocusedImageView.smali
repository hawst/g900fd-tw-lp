.class public Lcom/sec/android/app/shealth/plugins/exercisepro/widget/FocusedImageView;
.super Landroid/widget/ImageView;
.source "FocusedImageView.java"


# instance fields
.field private mBitmap:Landroid/graphics/Bitmap;

.field mBitmapRect:Landroid/graphics/Rect;

.field paint:Landroid/graphics/Paint;

.field rect:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 19
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/FocusedImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 21
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    .line 24
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 14
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/FocusedImageView;->paint:Landroid/graphics/Paint;

    .line 15
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v1, v1, v1, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/FocusedImageView;->rect:Landroid/graphics/Rect;

    .line 16
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v1, v1, v1, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/FocusedImageView;->mBitmapRect:Landroid/graphics/Rect;

    .line 25
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/FocusedImageView;->mBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/FocusedImageView;->mBitmapRect:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/FocusedImageView;->rect:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/FocusedImageView;->paint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 32
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/FocusedImageView;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 33
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/FocusedImageView;->paint:Landroid/graphics/Paint;

    const v1, -0xff0001

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 34
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/FocusedImageView;->paint:Landroid/graphics/Paint;

    const/high16 v1, 0x41200000    # 10.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 36
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/FocusedImageView;->rect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v1, v0

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/FocusedImageView;->rect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    int-to-float v2, v0

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/FocusedImageView;->rect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    int-to-float v3, v0

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/FocusedImageView;->rect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    int-to-float v4, v0

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/FocusedImageView;->paint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 37
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/FocusedImageView;->rect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v1, v0

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/FocusedImageView;->rect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    int-to-float v2, v0

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/FocusedImageView;->rect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v3, v0

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/FocusedImageView;->rect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v4, v0

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/FocusedImageView;->paint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 38
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/FocusedImageView;->rect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v1, v0

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/FocusedImageView;->rect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v2, v0

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/FocusedImageView;->rect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    int-to-float v3, v0

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/FocusedImageView;->rect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v4, v0

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/FocusedImageView;->paint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 39
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/FocusedImageView;->rect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    int-to-float v1, v0

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/FocusedImageView;->rect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    int-to-float v2, v0

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/FocusedImageView;->rect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    int-to-float v3, v0

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/FocusedImageView;->rect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v4, v0

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/FocusedImageView;->paint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 41
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onDraw(Landroid/graphics/Canvas;)V

    .line 42
    return-void
.end method

.method protected onMeasure(II)V
    .locals 0
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 47
    invoke-super {p0, p1, p2}, Landroid/widget/ImageView;->onMeasure(II)V

    .line 48
    return-void
.end method

.method public onSizeChanged(IIII)V
    .locals 1
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "oldWidth"    # I
    .param p4, "oldHeight"    # I

    .prologue
    .line 66
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ImageView;->onSizeChanged(IIII)V

    .line 67
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/FocusedImageView;->rect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/FocusedImageView;->rect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    if-eq v0, p2, :cond_1

    .line 68
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/FocusedImageView;->rect:Landroid/graphics/Rect;

    iput p1, v0, Landroid/graphics/Rect;->right:I

    .line 69
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/FocusedImageView;->rect:Landroid/graphics/Rect;

    iput p2, v0, Landroid/graphics/Rect;->bottom:I

    .line 70
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/FocusedImageView;->invalidate()V

    .line 72
    :cond_1
    return-void
.end method

.method public setImageBitmap(Landroid/graphics/Bitmap;)V
    .locals 4
    .param p1, "bm"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v3, 0x0

    .line 53
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/FocusedImageView;->mBitmap:Landroid/graphics/Bitmap;

    .line 54
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/FocusedImageView;->mBitmapRect:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/FocusedImageView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/FocusedImageView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 55
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/FocusedImageView;->invalidate()V

    .line 56
    return-void
.end method

.method public setSelected(Z)V
    .locals 0
    .param p1, "selected"    # Z

    .prologue
    .line 61
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 62
    return-void
.end method
