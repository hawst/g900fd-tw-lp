.class public Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;
.super Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;
.source "FrequentFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment$3;
    }
.end annotation


# instance fields
.field private mFavoriteAdapter:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAndAllAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAndAllAdapter",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;",
            ">;"
        }
    .end annotation
.end field

.field private mMealType:Lcom/sec/android/app/shealth/food/constants/MealType;

.field private mTaskRunner:Lcom/sec/android/app/shealth/food/foodpick/loader/TaskRunner;

.field private mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

.field private onFoodInfoSelectedListener:Lcom/sec/android/app/shealth/food/foodpick/expandable/listener/OnFoodInfoSelectedListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;-><init>()V

    .line 62
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;->setRetainInstance(Z)V

    .line 63
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;)Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAndAllAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;->mFavoriteAdapter:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAndAllAdapter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;->showNoDataScreen(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;->updateFavoriteAdapter(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;)Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;)Lcom/sec/android/app/shealth/food/foodpick/expandable/listener/OnFoodInfoSelectedListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;->onFoodInfoSelectedListener:Lcom/sec/android/app/shealth/food/foodpick/expandable/listener/OnFoodInfoSelectedListener;

    return-object v0
.end method

.method private getFavoritesFromDB()Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 191
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 192
    .local v5, "result":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;>;"
    new-instance v2, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-direct {v2, v6}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;-><init>(Landroid/content/Context;)V

    .line 193
    .local v2, "foodInfoDao":Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;
    new-instance v0, Lcom/sec/android/app/shealth/food/fooddao/ExtraFoodInfoDaoImplDb;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-direct {v0, v6}, Lcom/sec/android/app/shealth/food/fooddao/ExtraFoodInfoDaoImplDb;-><init>(Landroid/content/Context;)V

    .line 194
    .local v0, "extraFoodInfoDao":Lcom/sec/android/app/shealth/food/fooddao/ExtraFoodInfoDao;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->getFavoritesFoods(Landroid/content/Context;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    .line 195
    .local v3, "foodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    invoke-virtual {v3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getId()J

    move-result-wide v6

    invoke-interface {v0, v6, v7}, Lcom/sec/android/app/shealth/food/fooddao/ExtraFoodInfoDao;->getExtraByFoodId(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;

    move-result-object v1

    .line 196
    .local v1, "extraFoodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;
    new-instance v6, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;

    invoke-direct {v6, v3, v2, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;-><init>(Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;)V

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 198
    .end local v1    # "extraFoodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;
    .end local v3    # "foodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    :cond_0
    return-object v5
.end method

.method private getFrequentFromDB()Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 181
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 182
    .local v9, "result":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;>;"
    new-instance v0, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;-><init>(Landroid/content/Context;)V

    .line 183
    .local v0, "foodInfoDao":Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;
    const-wide/high16 v1, -0x8000000000000000L

    const-wide v3, 0x7fffffffffffffffL

    iget-object v5, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;->mMealType:Lcom/sec/android/app/shealth/food/constants/MealType;

    iget v5, v5, Lcom/sec/android/app/shealth/food/constants/MealType;->mealTypeId:I

    invoke-interface/range {v0 .. v5}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;->getFoodInfoFrequencyByMeal(JJI)Ljava/util/Map;

    move-result-object v8

    .line 184
    .local v8, "listItems":Ljava/util/Map;, "Ljava/util/Map<Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;Ljava/lang/Integer;>;"
    invoke-interface {v8}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Map$Entry;

    .line 185
    .local v6, "frequentEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;Ljava/lang/Integer;>;"
    new-instance v3, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FrequentFoodListItem;

    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-direct {v3, v1, v2, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FrequentFoodListItem;-><init>(Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;ILcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;)V

    invoke-interface {v9, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 187
    .end local v6    # "frequentEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;Ljava/lang/Integer;>;"
    :cond_0
    return-object v9
.end method

.method private init()V
    .locals 3

    .prologue
    .line 88
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "MEAL_TYPE"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 89
    .local v0, "mealTypeOrdinal":I
    invoke-static {}, Lcom/sec/android/app/shealth/food/constants/MealType;->values()[Lcom/sec/android/app/shealth/food/constants/MealType;

    move-result-object v1

    aget-object v1, v1, v0

    iput-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;->mMealType:Lcom/sec/android/app/shealth/food/constants/MealType;

    .line 90
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/food/foodpick/loader/TaskRunnerAccessor;

    invoke-interface {v1}, Lcom/sec/android/app/shealth/food/foodpick/loader/TaskRunnerAccessor;->getTaskRunner()Lcom/sec/android/app/shealth/food/foodpick/loader/TaskRunner;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;->mTaskRunner:Lcom/sec/android/app/shealth/food/foodpick/loader/TaskRunner;

    .line 91
    return-void
.end method

.method private initListViewAdapter(Landroid/view/View;)V
    .locals 9
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 103
    const v5, 0x7f080451

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ListView;

    .line 106
    .local v2, "itemsListView":Landroid/widget/ListView;
    const/4 v5, 0x0

    invoke-virtual {v2, v5}, Landroid/widget/ListView;->setFocusable(Z)V

    .line 108
    const/4 v5, 0x1

    invoke-virtual {v2, v5}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 110
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;->getFrequentFromDB()Ljava/util/List;

    move-result-object v0

    .line 111
    .local v0, "allItems":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;>;"
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;->getFavoritesFromDB()Ljava/util/List;

    move-result-object v1

    .line 113
    .local v1, "favoriteListItems":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;>;"
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 114
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;->showNoDataScreen(Landroid/view/View;)V

    .line 159
    :goto_0
    return-void

    .line 116
    :cond_0
    const/4 v3, 0x0

    .line 117
    .local v3, "mostUsed":I
    sget-object v5, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment$3;->$SwitchMap$com$sec$android$app$shealth$food$constants$MealType:[I

    iget-object v6, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;->mMealType:Lcom/sec/android/app/shealth/food/constants/MealType;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/food/constants/MealType;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 131
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-static {v5, v3}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->getCaptionForCategoryInListView(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v4

    .line 132
    .local v4, "title":Ljava/lang/String;
    new-instance v5, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAndAllAdapter;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-direct {v5, v6, v1, v0, v4}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAndAllAdapter;-><init>(Landroid/content/Context;Ljava/util/List;Ljava/util/List;Ljava/lang/String;)V

    iput-object v5, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;->mFavoriteAdapter:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAndAllAdapter;

    .line 133
    iget-object v5, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;->mFavoriteAdapter:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAndAllAdapter;

    new-instance v6, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/OnFoodFavoriteClickListener;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;->mTaskRunner:Lcom/sec/android/app/shealth/food/foodpick/loader/TaskRunner;

    invoke-direct {v6, v7, v8}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/OnFoodFavoriteClickListener;-><init>(Landroid/content/Context;Lcom/sec/android/app/shealth/food/foodpick/loader/TaskRunner;)V

    invoke-virtual {v5, v6}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAndAllAdapter;->addOnFavoriteClickListener(Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter$OnFavoriteClickListener;)V

    .line 134
    iget-object v5, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;->mFavoriteAdapter:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAndAllAdapter;

    new-instance v6, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment$1;

    invoke-direct {v6, p0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment$1;-><init>(Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;)V

    invoke-virtual {v5, v6}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAndAllAdapter;->addOnFavoriteClickListener(Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter$OnFavoriteClickListener;)V

    .line 144
    iget-object v5, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;->mFavoriteAdapter:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAndAllAdapter;

    new-instance v6, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment$2;

    invoke-direct {v6, p0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment$2;-><init>(Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;)V

    invoke-virtual {v5, v6}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAndAllAdapter;->setOnItemClickListener(Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter$OnItemClickListener;)V

    .line 157
    iget-object v5, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;->mFavoriteAdapter:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAndAllAdapter;

    invoke-virtual {v2, v5}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0

    .line 119
    .end local v4    # "title":Ljava/lang/String;
    :pswitch_0
    const v3, 0x7f090956

    .line 120
    goto :goto_1

    .line 122
    :pswitch_1
    const v3, 0x7f090957

    .line 123
    goto :goto_1

    .line 125
    :pswitch_2
    const v3, 0x7f090958

    .line 126
    goto :goto_1

    .line 128
    :pswitch_3
    const v3, 0x7f090959

    goto :goto_1

    .line 117
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private showNoDataScreen(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 175
    const v1, 0x7f080451

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 176
    const v1, 0x7f080459

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 177
    .local v0, "noDataLayout":Landroid/view/View;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 178
    return-void
.end method

.method private updateFavoriteAdapter(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 162
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;->getFrequentFromDB()Ljava/util/List;

    move-result-object v0

    .line 163
    .local v0, "allItems":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;>;"
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;->getFavoritesFromDB()Ljava/util/List;

    move-result-object v1

    .line 164
    .local v1, "favoriteListItems":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;>;"
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 165
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;->showNoDataScreen(Landroid/view/View;)V

    .line 172
    :goto_0
    return-void

    .line 167
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;->mFavoriteAdapter:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAndAllAdapter;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAndAllAdapter;->clear()V

    .line 168
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;->mFavoriteAdapter:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAndAllAdapter;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAndAllAdapter;->addListItems(Ljava/util/List;)V

    .line 169
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;->mFavoriteAdapter:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAndAllAdapter;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAndAllAdapter;->updateFavoritesItems(Ljava/util/List;)V

    .line 170
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;->mFavoriteAdapter:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAndAllAdapter;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAndAllAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 81
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onAttach(Landroid/app/Activity;)V

    .line 82
    instance-of v0, p1, Lcom/sec/android/app/shealth/food/foodpick/expandable/listener/OnFoodInfoSelectedListener;

    if-eqz v0, :cond_0

    .line 83
    check-cast p1, Lcom/sec/android/app/shealth/food/foodpick/expandable/listener/OnFoodInfoSelectedListener;

    .end local p1    # "activity":Landroid/app/Activity;
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;->onFoodInfoSelectedListener:Lcom/sec/android/app/shealth/food/foodpick/expandable/listener/OnFoodInfoSelectedListener;

    .line 85
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 67
    const v1, 0x7f030104

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 68
    .local v0, "view":Landroid/view/View;
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;->init()V

    .line 69
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplication()Landroid/app/Application;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLoggerHolder;

    invoke-interface {v1}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLoggerHolder;->getUserActionLogger()Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    .line 70
    return-object v0
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 75
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onResume()V

    .line 76
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;->reload()V

    .line 77
    return-void
.end method

.method public reload()V
    .locals 1

    .prologue
    .line 97
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;->isDetached()Z

    move-result v0

    if-nez v0, :cond_0

    .line 98
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;->initListViewAdapter(Landroid/view/View;)V

    .line 100
    :cond_0
    return-void
.end method
