.class public Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
.super Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;
.source "FoodInfoData.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;",
            ">;"
        }
    .end annotation
.end field

.field public static final FOOD_DESCRIPTION_DELIMITER:Ljava/lang/String; = ", "


# instance fields
.field private favorite:Z

.field private kCal:F

.field private mDescription:Ljava/lang/String;

.field private mServerId:Ljava/lang/String;

.field private mServerLocale:Ljava/lang/String;

.field private mServerRootCategory:Ljava/lang/String;

.field private mServerRootCategoryId:Ljava/lang/String;

.field private mServerSourceType:I

.field private mServerSubCategory:Ljava/lang/String;

.field private mServerSubCategoryId:Ljava/lang/String;

.field private name:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 166
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData$1;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 97
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;-><init>()V

    .line 16
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->name:Ljava/lang/String;

    .line 20
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mServerId:Ljava/lang/String;

    .line 21
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mServerSourceType:I

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mDescription:Ljava/lang/String;

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mServerLocale:Ljava/lang/String;

    .line 24
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mServerRootCategory:Ljava/lang/String;

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mServerRootCategoryId:Ljava/lang/String;

    .line 26
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mServerSubCategory:Ljava/lang/String;

    .line 27
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mServerSubCategoryId:Ljava/lang/String;

    .line 98
    return-void
.end method

.method public constructor <init>(JLjava/lang/String;FZLjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJI)V
    .locals 9
    .param p1, "id"    # J
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "kCal"    # F
    .param p5, "favorite"    # Z
    .param p6, "serverId"    # Ljava/lang/String;
    .param p7, "serverSourceType"    # I
    .param p8, "description"    # Ljava/lang/String;
    .param p9, "serverLocale"    # Ljava/lang/String;
    .param p10, "serverRootCategory"    # Ljava/lang/String;
    .param p11, "serverRootCategoryId"    # Ljava/lang/String;
    .param p12, "serverSubCategory"    # Ljava/lang/String;
    .param p13, "serverSubCategoryId"    # Ljava/lang/String;
    .param p14, "createTime"    # J
    .param p16, "updateTime"    # J
    .param p18, "timeZone"    # I

    .prologue
    .line 52
    move-object v1, p0

    move-wide v2, p1

    move-wide/from16 v4, p14

    move-wide/from16 v6, p16

    move/from16 v8, p18

    invoke-direct/range {v1 .. v8}, Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;-><init>(JJJI)V

    .line 16
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->name:Ljava/lang/String;

    .line 20
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mServerId:Ljava/lang/String;

    .line 21
    const/4 v1, -0x1

    iput v1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mServerSourceType:I

    .line 22
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mDescription:Ljava/lang/String;

    .line 23
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mServerLocale:Ljava/lang/String;

    .line 24
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mServerRootCategory:Ljava/lang/String;

    .line 25
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mServerRootCategoryId:Ljava/lang/String;

    .line 26
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mServerSubCategory:Ljava/lang/String;

    .line 27
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mServerSubCategoryId:Ljava/lang/String;

    .line 53
    iput-object p3, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->name:Ljava/lang/String;

    .line 54
    iput p4, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->kCal:F

    .line 55
    iput-boolean p5, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->favorite:Z

    .line 56
    iput-object p6, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mServerId:Ljava/lang/String;

    .line 57
    move/from16 v0, p7

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mServerSourceType:I

    .line 58
    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mDescription:Ljava/lang/String;

    .line 59
    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mServerLocale:Ljava/lang/String;

    .line 60
    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mServerRootCategory:Ljava/lang/String;

    .line 61
    move-object/from16 v0, p12

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mServerSubCategory:Ljava/lang/String;

    .line 62
    move-object/from16 v0, p11

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mServerRootCategoryId:Ljava/lang/String;

    .line 63
    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mServerSubCategoryId:Ljava/lang/String;

    .line 64
    return-void
.end method

.method public constructor <init>(JLjava/lang/String;FZLjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "id"    # J
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "kCal"    # F
    .param p5, "favorite"    # Z
    .param p6, "sorting1"    # Ljava/lang/String;
    .param p7, "sorting2"    # Ljava/lang/String;

    .prologue
    .line 69
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;-><init>(J)V

    .line 16
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->name:Ljava/lang/String;

    .line 20
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mServerId:Ljava/lang/String;

    .line 21
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mServerSourceType:I

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mDescription:Ljava/lang/String;

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mServerLocale:Ljava/lang/String;

    .line 24
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mServerRootCategory:Ljava/lang/String;

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mServerRootCategoryId:Ljava/lang/String;

    .line 26
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mServerSubCategory:Ljava/lang/String;

    .line 27
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mServerSubCategoryId:Ljava/lang/String;

    .line 70
    iput-object p3, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->name:Ljava/lang/String;

    .line 71
    iput p4, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->kCal:F

    .line 72
    iput-boolean p5, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->favorite:Z

    .line 73
    return-void
.end method

.method public constructor <init>(JLjava/lang/String;FZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p1, "id"    # J
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "kCal"    # F
    .param p5, "favorite"    # Z
    .param p6, "sorting1"    # Ljava/lang/String;
    .param p7, "sorting2"    # Ljava/lang/String;
    .param p8, "serverId"    # Ljava/lang/String;
    .param p9, "serverSourceType"    # I
    .param p10, "desc"    # Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 91
    invoke-direct/range {p0 .. p7}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;-><init>(JLjava/lang/String;FZLjava/lang/String;Ljava/lang/String;)V

    .line 92
    iput-object p8, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mServerId:Ljava/lang/String;

    .line 93
    iput p9, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mServerSourceType:I

    .line 94
    iput-object p10, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mDescription:Ljava/lang/String;

    .line 95
    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    const/4 v0, 0x1

    .line 148
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;-><init>()V

    .line 16
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->name:Ljava/lang/String;

    .line 20
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mServerId:Ljava/lang/String;

    .line 21
    const/4 v1, -0x1

    iput v1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mServerSourceType:I

    .line 22
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mDescription:Ljava/lang/String;

    .line 23
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mServerLocale:Ljava/lang/String;

    .line 24
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mServerRootCategory:Ljava/lang/String;

    .line 25
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mServerRootCategoryId:Ljava/lang/String;

    .line 26
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mServerSubCategory:Ljava/lang/String;

    .line 27
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mServerSubCategoryId:Ljava/lang/String;

    .line 149
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v1

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setId(J)V

    .line 150
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setName(Ljava/lang/String;)V

    .line 151
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setkCal(F)V

    .line 152
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setFavorite(Z)V

    .line 153
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setServerId(Ljava/lang/String;)V

    .line 154
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setServerSourceType(I)V

    .line 155
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setDescription(Ljava/lang/String;)V

    .line 156
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setServerLocale(Ljava/lang/String;)V

    .line 157
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setServerRootCategory(Ljava/lang/String;)V

    .line 158
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setServerRootCategoryId(Ljava/lang/String;)V

    .line 159
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setServerSubCategory(Ljava/lang/String;)V

    .line 160
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setServerSubCategoryId(Ljava/lang/String;)V

    .line 161
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setCreateTime(J)V

    .line 162
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setUpdateTime(J)V

    .line 163
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setTimeZone(I)V

    .line 164
    return-void

    .line 152
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static createDefaultFoodDescription(Landroid/content/res/Resources;F)Ljava/lang/String;
    .locals 3
    .param p0, "resources"    # Landroid/content/res/Resources;
    .param p1, "calories"    # F

    .prologue
    .line 349
    sget v2, Lcom/sec/android/app/shealth/common/utils/R$string;->serving:I

    invoke-virtual {p0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 350
    .local v1, "portionName":Ljava/lang/String;
    const/4 v0, 0x1

    .line 351
    .local v0, "defaultServingSize":I
    invoke-static {p0, p1, v0, v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->createFoodDescription(Landroid/content/res/Resources;FILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public static createFoodDescription(Landroid/content/res/Resources;FILjava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p0, "res"    # Landroid/content/res/Resources;
    .param p1, "calories"    # F
    .param p2, "servingSize"    # I
    .param p3, "portionName"    # Ljava/lang/String;

    .prologue
    .line 365
    invoke-static {p1}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    .line 366
    .local v1, "kCalValue":Ljava/lang/String;
    sget v3, Lcom/sec/android/app/shealth/common/utils/R$string;->kcal:I

    invoke-virtual {p0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 367
    .local v0, "kCal":Ljava/lang/String;
    sget v3, Lcom/sec/android/app/shealth/common/utils/R$string;->per_serving_size:I

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {p0, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 370
    .local v2, "perOneServing":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 181
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 187
    if-ne p0, p1, :cond_1

    .line 206
    :cond_0
    :goto_0
    return v1

    .line 190
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 191
    goto :goto_0

    .line 193
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-ne v3, v4, :cond_4

    move-object v0, p1

    .line 194
    check-cast v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    .line 198
    .local v0, "item":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getId()J

    move-result-wide v3

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getId()J

    move-result-wide v5

    cmp-long v3, v3, v5

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->name:Ljava/lang/String;

    iget-object v4, v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget v3, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->kCal:F

    iget v4, v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->kCal:F

    cmpl-float v3, v3, v4

    if-nez v3, :cond_3

    iget v3, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mServerSourceType:I

    iget v4, v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mServerSourceType:I

    if-ne v3, v4, :cond_3

    iget-boolean v3, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->favorite:Z

    iget-boolean v4, v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->favorite:Z

    if-eq v3, v4, :cond_0

    :cond_3
    move v1, v2

    .line 203
    goto :goto_0

    .end local v0    # "item":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    :cond_4
    move v1, v2

    .line 196
    goto :goto_0
.end method

.method public getDescription(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1
    .param p1, "res"    # Landroid/content/res/Resources;

    .prologue
    .line 258
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->isAddedByUser()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 259
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getkCal()F

    move-result v0

    invoke-static {p1, v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->createDefaultFoodDescription(Landroid/content/res/Resources;F)Ljava/lang/String;

    move-result-object v0

    .line 267
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mDescription:Ljava/lang/String;

    goto :goto_0
.end method

.method public getFavorite()Z
    .locals 1

    .prologue
    .line 117
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->favorite:Z

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getServerId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mServerId:Ljava/lang/String;

    return-object v0
.end method

.method public getServerLocale()Ljava/lang/String;
    .locals 1

    .prologue
    .line 278
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mServerLocale:Ljava/lang/String;

    return-object v0
.end method

.method public getServerRootCategory()Ljava/lang/String;
    .locals 1

    .prologue
    .line 286
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mServerRootCategory:Ljava/lang/String;

    return-object v0
.end method

.method public getServerRootCategoryId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 297
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mServerRootCategoryId:Ljava/lang/String;

    return-object v0
.end method

.method public getServerSourceType()I
    .locals 1

    .prologue
    .line 241
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mServerSourceType:I

    return v0
.end method

.method public getServerSubCategory()Ljava/lang/String;
    .locals 1

    .prologue
    .line 322
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mServerSubCategory:Ljava/lang/String;

    return-object v0
.end method

.method public getServerSubCategoryId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 311
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mServerSubCategoryId:Ljava/lang/String;

    return-object v0
.end method

.method public getkCal()F
    .locals 1

    .prologue
    .line 109
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->kCal:F

    return v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 211
    const/16 v0, 0x1f

    .line 212
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 213
    .local v1, "result":I
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getId()J

    move-result-wide v2

    long-to-int v2, v2

    add-int/lit8 v1, v2, 0x1f

    .line 214
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->kCal:F

    float-to-int v3, v3

    add-int v1, v2, v3

    .line 215
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mServerSourceType:I

    add-int v1, v2, v3

    .line 216
    mul-int/lit8 v2, v1, 0x1f

    iget-object v3, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->name:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    add-int v1, v2, v3

    .line 217
    mul-int/lit8 v3, v1, 0x1f

    iget-boolean v2, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->favorite:Z

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    add-int v1, v3, v2

    .line 218
    return v1

    .line 217
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public isAddedByUser()Z
    .locals 2

    .prologue
    .line 335
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mServerSourceType:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mServerSourceType:I

    const v1, 0x46cd1

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setDescription(Ljava/lang/String;)V
    .locals 0
    .param p1, "description"    # Ljava/lang/String;

    .prologue
    .line 274
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mDescription:Ljava/lang/String;

    .line 275
    return-void
.end method

.method public setFavorite(Z)V
    .locals 0
    .param p1, "favorite"    # Z

    .prologue
    .line 121
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->favorite:Z

    .line 122
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->name:Ljava/lang/String;

    .line 106
    return-void
.end method

.method public setServerId(Ljava/lang/String;)V
    .locals 0
    .param p1, "serverId"    # Ljava/lang/String;

    .prologue
    .line 234
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mServerId:Ljava/lang/String;

    .line 235
    return-void
.end method

.method public setServerLocale(Ljava/lang/String;)V
    .locals 0
    .param p1, "mServerLocale"    # Ljava/lang/String;

    .prologue
    .line 282
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mServerLocale:Ljava/lang/String;

    .line 283
    return-void
.end method

.method public setServerRootCategory(Ljava/lang/String;)V
    .locals 0
    .param p1, "mServerRootCategory"    # Ljava/lang/String;

    .prologue
    .line 290
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mServerRootCategory:Ljava/lang/String;

    .line 291
    return-void
.end method

.method public setServerRootCategoryId(Ljava/lang/String;)V
    .locals 0
    .param p1, "serverRootCategoryId"    # Ljava/lang/String;

    .prologue
    .line 304
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mServerRootCategoryId:Ljava/lang/String;

    .line 305
    return-void
.end method

.method public setServerSourceType(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 248
    iput p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mServerSourceType:I

    .line 249
    return-void
.end method

.method public setServerSubCategory(Ljava/lang/String;)V
    .locals 0
    .param p1, "mServerSubCategory"    # Ljava/lang/String;

    .prologue
    .line 326
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mServerSubCategory:Ljava/lang/String;

    .line 327
    return-void
.end method

.method public setServerSubCategoryId(Ljava/lang/String;)V
    .locals 0
    .param p1, "serverSubCategoryId"    # Ljava/lang/String;

    .prologue
    .line 318
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mServerSubCategoryId:Ljava/lang/String;

    .line 319
    return-void
.end method

.method public setkCal(F)V
    .locals 0
    .param p1, "kCal"    # F

    .prologue
    .line 113
    iput p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->kCal:F

    .line 114
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "parcel"    # Landroid/os/Parcel;
    .param p2, "i"    # I

    .prologue
    .line 126
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getId()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 127
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->name:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 128
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->kCal:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 129
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->favorite:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 130
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mServerId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 131
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mServerSourceType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 132
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mDescription:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 133
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mServerLocale:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 134
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mServerRootCategory:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 135
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mServerRootCategoryId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 136
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mServerSubCategory:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 137
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->mServerSubCategoryId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 138
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getCreateTime()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 139
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getUpdateTime()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 140
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getTimeZone()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 141
    return-void

    .line 129
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
