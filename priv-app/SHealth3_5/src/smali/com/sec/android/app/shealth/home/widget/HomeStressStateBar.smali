.class public Lcom/sec/android/app/shealth/home/widget/HomeStressStateBar;
.super Landroid/widget/LinearLayout;
.source "HomeStressStateBar.java"


# instance fields
.field private ivPolygon:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 19
    const v0, 0x7f030134

    invoke-static {p1, v0, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 20
    const v0, 0x7f08051e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/home/widget/HomeStressStateBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/widget/HomeStressStateBar;->ivPolygon:Landroid/widget/ImageView;

    .line 21
    return-void
.end method

.method private convertDptoPx(I)I
    .locals 5
    .param p1, "dp"    # I

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/widget/HomeStressStateBar;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 45
    .local v2, "res":Landroid/content/res/Resources;
    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 46
    .local v0, "metrics":Landroid/util/DisplayMetrics;
    int-to-float v3, p1

    iget v4, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float v1, v3, v4

    .line 47
    .local v1, "px":F
    float-to-int v3, v1

    return v3
.end method


# virtual methods
.method public changePolygonImage(I)V
    .locals 1
    .param p1, "res"    # I

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/HomeStressStateBar;->ivPolygon:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/HomeStressStateBar;->ivPolygon:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 53
    :cond_0
    return-void
.end method

.method public moveToPolygon(D)V
    .locals 12
    .param p1, "stressScore"    # D

    .prologue
    .line 24
    const/16 v8, 0xaa

    invoke-direct {p0, v8}, Lcom/sec/android/app/shealth/home/widget/HomeStressStateBar;->convertDptoPx(I)I

    move-result v7

    .line 25
    .local v7, "width":I
    const-wide/16 v5, 0x0

    .line 26
    .local v5, "stressScoreMargin":D
    const-wide/16 v2, 0x0

    .line 27
    .local v2, "conversionScore":D
    const-wide/high16 v8, -0x3ff8000000000000L    # -3.0

    cmpg-double v8, p1, v8

    if-gez v8, :cond_0

    .line 28
    const-wide/high16 v2, -0x3ff8000000000000L    # -3.0

    .line 34
    :goto_0
    const-wide/high16 v8, 0x4018000000000000L    # 6.0

    div-double v8, v2, v8

    const-wide/high16 v10, 0x4059000000000000L    # 100.0

    mul-double v0, v8, v10

    .line 35
    .local v0, "converScorePer":D
    const-wide/high16 v8, 0x4059000000000000L    # 100.0

    div-double v8, v0, v8

    int-to-double v10, v7

    mul-double v5, v8, v10

    .line 37
    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v8, -0x2

    const/4 v9, -0x2

    invoke-direct {v4, v8, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 38
    .local v4, "params":Landroid/widget/LinearLayout$LayoutParams;
    const/4 v8, 0x1

    invoke-direct {p0, v8}, Lcom/sec/android/app/shealth/home/widget/HomeStressStateBar;->convertDptoPx(I)I

    move-result v8

    int-to-double v8, v8

    add-double/2addr v8, v5

    invoke-static {v8, v9}, Ljava/lang/Math;->round(D)J

    move-result-wide v8

    long-to-int v8, v8

    iput v8, v4, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 39
    const/4 v8, 0x3

    iput v8, v4, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 40
    iget-object v8, p0, Lcom/sec/android/app/shealth/home/widget/HomeStressStateBar;->ivPolygon:Landroid/widget/ImageView;

    invoke-virtual {v8, v4}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 41
    return-void

    .line 29
    .end local v0    # "converScorePer":D
    .end local v4    # "params":Landroid/widget/LinearLayout$LayoutParams;
    :cond_0
    const-wide/high16 v8, 0x4008000000000000L    # 3.0

    cmpl-double v8, p1, v8

    if-lez v8, :cond_1

    .line 30
    const-wide/high16 v2, 0x4008000000000000L    # 3.0

    goto :goto_0

    .line 32
    :cond_1
    const-wide/high16 v8, 0x4008000000000000L    # 3.0

    add-double v2, p1, v8

    goto :goto_0
.end method
