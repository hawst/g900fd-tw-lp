.class public Lcom/sec/android/app/shealth/award/medaldata/AwardGoalData;
.super Ljava/lang/Object;
.source "AwardGoalData.java"


# instance fields
.field private goalTime:Ljava/lang/String;

.field private value:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    return-void
.end method


# virtual methods
.method public getGoalTime()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/shealth/award/medaldata/AwardGoalData;->goalTime:Ljava/lang/String;

    return-object v0
.end method

.method public getValue()J
    .locals 2

    .prologue
    .line 31
    iget-wide v0, p0, Lcom/sec/android/app/shealth/award/medaldata/AwardGoalData;->value:J

    return-wide v0
.end method

.method public setGoalTime(Ljava/lang/String;)V
    .locals 0
    .param p1, "goalTime"    # Ljava/lang/String;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/sec/android/app/shealth/award/medaldata/AwardGoalData;->goalTime:Ljava/lang/String;

    .line 45
    return-void
.end method

.method public setValue(J)V
    .locals 0
    .param p1, "value"    # J

    .prologue
    .line 36
    iput-wide p1, p0, Lcom/sec/android/app/shealth/award/medaldata/AwardGoalData;->value:J

    .line 37
    return-void
.end method
