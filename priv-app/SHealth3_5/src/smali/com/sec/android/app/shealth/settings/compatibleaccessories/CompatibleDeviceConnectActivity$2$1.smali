.class Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$2$1;
.super Ljava/lang/Object;
.source "CompatibleDeviceConnectActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$2;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$2;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$2;)V
    .locals 0

    .prologue
    .line 162
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$2$1;->this$1:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 2
    .param p1, "arg0"    # Landroid/content/DialogInterface;

    .prologue
    .line 168
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$2$1;->this$1:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$2;

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;->isScanning:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;->access$200(Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 170
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$2$1;->this$1:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$2;

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;->mHealthSensor:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;->access$300(Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 172
    const-string v0, "CompatibleDeviceConnectActivity"

    const-string v1, "Scan Cancelled."

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$2$1;->this$1:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$2;

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;

    # invokes: Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;->stopHealthSersorScan()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;->access$400(Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;)V

    .line 177
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$2$1;->this$1:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$2;

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;->mHealthSensor:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;->access$300(Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 179
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$2$1;->this$1:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$2;

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;->mHealthSensor:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;->access$300(Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->close()V

    .line 180
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$2$1;->this$1:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$2;

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;->mHealthSensor:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;->access$302(Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    .line 183
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$2$1;->this$1:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$2;

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;->isScanning:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;->access$202(Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;Z)Z

    .line 184
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$2$1;->this$1:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$2;

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;->finish()V

    .line 185
    return-void
.end method
