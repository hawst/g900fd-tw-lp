.class Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity$5;
.super Ljava/lang/Object;
.source "SetQuickInputActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$EmptyFieldListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;)V
    .locals 0

    .prologue
    .line 392
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity$5;->this$0:Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public processEmptyField()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 397
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity$5;->this$0:Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;

    # invokes: Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->showRangeToast()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->access$500(Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;)V

    .line 398
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity$5;->this$0:Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;

    # getter for: Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mCalorieValueEditText:Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->access$300(Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;)Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;->setText(Ljava/lang/CharSequence;)V

    .line 399
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity$5;->this$0:Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;

    # getter for: Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mCalorieValueEditText:Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->access$300(Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;)Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;->selectAll()V

    .line 400
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity$5;->this$0:Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;

    # getter for: Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mCalorieValueEditText:Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->access$300(Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;)Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;->requestFocus()Z

    .line 401
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity$5;->this$0:Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity$5;->this$0:Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;

    # getter for: Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mCalorieValueEditText:Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->access$300(Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;)Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInputFromInputMethod(Landroid/os/IBinder;I)V

    .line 403
    return-void
.end method
