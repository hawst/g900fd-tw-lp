.class Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel$10;
.super Ljava/lang/Object;
.source "QuickInputImagePanel.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$EmptyFieldListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;)V
    .locals 0

    .prologue
    .line 610
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel$10;->this$0:Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public processEmptyField()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 615
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel$10;->this$0:Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;

    # getter for: Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mCalorieValueEditText:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->access$100(Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;)Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->setText(Ljava/lang/CharSequence;)V

    .line 616
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel$10;->this$0:Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;

    # getter for: Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mCalorieValueEditText:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->access$100(Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;)Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->selectAll()V

    .line 617
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel$10;->this$0:Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;

    # getter for: Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mCalorieValueEditText:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->access$100(Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;)Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->requestFocus()Z

    .line 618
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel$10;->this$0:Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel$10;->this$0:Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;

    # getter for: Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mCalorieValueEditText:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->access$100(Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;)Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInputFromInputMethod(Landroid/os/IBinder;I)V

    .line 619
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel$10;->this$0:Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;

    # invokes: Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->showMinMaxToast()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->access$600(Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;)V

    .line 621
    return-void
.end method
