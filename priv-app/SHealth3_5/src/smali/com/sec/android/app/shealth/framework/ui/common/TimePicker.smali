.class public Lcom/sec/android/app/shealth/framework/ui/common/TimePicker;
.super Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;


# static fields
.field public static final TIME_PICKER_DIALOG:Ljava/lang/String; = "time_picker_dialog"


# instance fields
.field private mFromDate:Ljava/util/Date;

.field numberPicker1:Landroid/view/ViewGroup;

.field numberPicker2:Landroid/view/ViewGroup;

.field v:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>(Landroid/widget/Button;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;-><init>(Landroid/widget/Button;)V

    return-void
.end method

.method public constructor <init>(Landroid/widget/Button;Ljava/util/Date;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;-><init>(Landroid/widget/Button;Ljava/util/Date;)V

    iput-object p2, p0, Lcom/sec/android/app/shealth/framework/ui/common/TimePicker;->mFromDate:Ljava/util/Date;

    return-void
.end method


# virtual methods
.method protected getDialogTag()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "time_picker_dialog"

    return-object v0
.end method

.method protected getSimpleDateFormatter()Ljava/text/DateFormat;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/TimePicker;->context:Landroid/content/Context;

    invoke-static {v0}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    return-object v0
.end method

.method protected initDialog()V
    .locals 4

    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/common/TimePicker;->context:Landroid/content/Context;

    const/4 v2, 0x2

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$string;->set_time:I

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;II)V

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$string;->set:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$layout;->timepicker_shealth:I

    new-instance v2, Lcom/sec/android/app/shealth/framework/ui/common/TimePicker$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/framework/ui/common/TimePicker$1;-><init>(Lcom/sec/android/app/shealth/framework/ui/common/TimePicker;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(ILcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setNeedRefreshFocusables(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/TimePicker;->dialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    return-void
.end method
