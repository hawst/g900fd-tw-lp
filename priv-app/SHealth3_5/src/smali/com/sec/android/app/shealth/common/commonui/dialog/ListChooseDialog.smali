.class public Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;
.super Landroid/app/DialogFragment;
.source "ListChooseDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$TitleUpdate;,
        Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;,
        Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$IOnDismissListener;
    }
.end annotation


# static fields
.field protected static final ADDITIONAL_TOP_TEXT_KEY:Ljava/lang/String; = "additional_top_text"

.field protected static final CHECKED_ITEMS_KEY:Ljava/lang/String; = "checked_items"

.field protected static final CHOOSE_TYPE_KEY:Ljava/lang/String; = "choose_type"

.field public static final CHOOSE_TYPE_MULTIPLE_ITEMS:I = 0xb

.field public static final CHOOSE_TYPE_PICK:I = 0xa

.field public static final CHOOSE_TYPE_PICK_WITHOUT_BUTTON:I = 0x9

.field public static final CHOOSE_TYPE_SINGLE_ITEM:I = 0xc

.field protected static final DISABLED_ITEM_KEY:Ljava/lang/String; = "disabled_items"

.field public static final FLAGS:Ljava/lang/String; = "flags"

.field protected static final ITEMS_DESCRIPTION_KEY:Ljava/lang/String; = "description_of_items"

.field protected static final ITEMS_TEXT_KEY:Ljava/lang/String; = "text_for_items"

.field private static final MAX_LIST_ITEM_TO_SHOW:I = 0x9

.field public static final MULTI_CHECKBOX:Ljava/lang/String; = "multi_checkbox_dialog"

.field protected static final SET_DEFAULT_CONTENT_MARGINS:Ljava/lang/String; = "set_default_content_margins"

.field private static final TAG:Ljava/lang/String;

.field protected static final TITLE_RES_ID_KEY:Ljava/lang/String; = "title_res_id"

.field protected static final TOP_TEXT_KEY:Ljava/lang/String; = "top_text"

.field protected static final TOP_TEXT__DESCRIPTION_KEY:Ljava/lang/String; = "top_description_text"

.field public static final TYPE_CANCEL:I = 0x1

.field protected static final TYPE_KEY:Ljava/lang/String; = "type"

.field public static final TYPE_OK:I = 0x0

.field public static final TYPE_OK_CANCEL:I = 0x2

.field protected static final TYPE_WITHOUT_BUTTON:I = 0x3


# instance fields
.field protected adapter:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialogAdapter;

.field private cancelButtonClickListener:Landroid/view/View$OnClickListener;

.field private complexStringBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/IContainComplexString;

.field protected descriptions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected isChecked:[Z

.field protected items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected listView:Landroid/widget/ListView;

.field private mChoose_type:I

.field private mDismissListener:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$IOnDismissListener;

.field private mIsInstanceStateLoss:Z

.field private okButtonClickListener:Landroid/view/View$OnClickListener;

.field private onSelectedListener:Lcom/sec/android/app/shealth/common/commonui/dialog/OnSelectedItemListener;

.field private titleManual:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const-class v0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 74
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->titleManual:Ljava/lang/String;

    .line 82
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->mIsInstanceStateLoss:Z

    .line 634
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->dismissAllowStateLoss()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;)Landroid/view/View$OnClickListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->okButtonClickListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;Landroid/view/View$OnClickListener;)Landroid/view/View$OnClickListener;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;
    .param p1, "x1"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 42
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->okButtonClickListener:Landroid/view/View$OnClickListener;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    .prologue
    .line 42
    iget v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->mChoose_type:I

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;)Lcom/sec/android/app/shealth/common/commonui/dialog/OnSelectedItemListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->onSelectedListener:Lcom/sec/android/app/shealth/common/commonui/dialog/OnSelectedItemListener;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;)Landroid/view/View$OnClickListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->cancelButtonClickListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method static synthetic access$402(Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;Landroid/view/View$OnClickListener;)Landroid/view/View$OnClickListener;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;
    .param p1, "x1"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 42
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->cancelButtonClickListener:Landroid/view/View$OnClickListener;

    return-object p1
.end method

.method private dismissAllowStateLoss()V
    .locals 1

    .prologue
    .line 650
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->mIsInstanceStateLoss:Z

    if-eqz v0, :cond_0

    .line 652
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->mIsInstanceStateLoss:Z

    .line 653
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->dismissAllowingStateLoss()V

    .line 659
    :goto_0
    return-void

    .line 657
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->dismiss()V

    goto :goto_0
.end method


# virtual methods
.method protected getGeneralParentContainer(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 18
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "arguments"    # Landroid/os/Bundle;

    .prologue
    .line 303
    sget v12, Lcom/sec/android/app/shealth/common/commonui/R$layout;->dialog_list:I

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v12, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v9

    .line 304
    .local v9, "parentContent":Landroid/view/View;
    sget v12, Lcom/sec/android/app/shealth/common/commonui/R$id;->ok_button:I

    invoke-virtual {v9, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/Button;

    .line 305
    .local v7, "okButton":Landroid/widget/Button;
    sget v12, Lcom/sec/android/app/shealth/common/commonui/R$id;->cancel_button:I

    invoke-virtual {v9, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    .line 308
    .local v3, "cancelButton":Landroid/widget/Button;
    const-string/jumbo v12, "title_res_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v12}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v11

    .line 311
    .local v11, "titleResId":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->getActivity()Landroid/app/Activity;

    move-result-object v8

    .line 312
    .local v8, "parent":Landroid/app/Activity;
    instance-of v12, v8, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$TitleUpdate;

    if-eqz v12, :cond_0

    .line 313
    check-cast v8, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$TitleUpdate;

    .end local v8    # "parent":Landroid/app/Activity;
    move-object/from16 v0, p3

    invoke-interface {v8, v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$TitleUpdate;->updateTitle(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->setTitle(Ljava/lang/String;)V

    .line 315
    :cond_0
    sget v12, Lcom/sec/android/app/shealth/common/commonui/R$id;->list_title:I

    invoke-virtual {v9, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/TextView;

    const-string/jumbo v13, "sec-roboto-light"

    const/4 v14, 0x1

    invoke-static {v13, v14}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v13

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 316
    sget v12, Lcom/sec/android/app/shealth/common/commonui/R$id;->list_title:I

    invoke-virtual {v9, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v13

    sget v14, Lcom/sec/android/app/shealth/common/commonui/R$style;->boldType:I

    invoke-virtual {v12, v13, v14}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 317
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->titleManual:Ljava/lang/String;

    if-eqz v12, :cond_5

    .line 318
    sget v12, Lcom/sec/android/app/shealth/common/commonui/R$id;->list_title:I

    invoke-virtual {v9, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->titleManual:Ljava/lang/String;

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 324
    :cond_1
    :goto_0
    const-string v12, "flags"

    const/4 v13, -0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v12, v13}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v4

    .line 325
    .local v4, "flags":I
    if-lez v4, :cond_2

    .line 327
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v12

    invoke-virtual {v12}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v12

    invoke-virtual {v12, v4}, Landroid/view/Window;->addFlags(I)V

    .line 329
    :cond_2
    const-string/jumbo v12, "type"

    move-object/from16 v0, p3

    invoke-virtual {v0, v12}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v12

    packed-switch v12, :pswitch_data_0

    .line 349
    :goto_1
    :pswitch_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->getActivity()Landroid/app/Activity;

    move-result-object v12

    instance-of v12, v12, Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerGetter;

    if-eqz v12, :cond_4

    .line 351
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->getActivity()Landroid/app/Activity;

    move-result-object v12

    check-cast v12, Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerGetter;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->getTag()Ljava/lang/String;

    move-result-object v13

    invoke-interface {v12, v13}, Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerGetter;->getPositiveButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonController;

    move-result-object v10

    .line 352
    .local v10, "positiveButtonController":Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonController;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->getActivity()Landroid/app/Activity;

    move-result-object v12

    check-cast v12, Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerGetter;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->getTag()Ljava/lang/String;

    move-result-object v13

    invoke-interface {v12, v13}, Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerGetter;->getPositiveButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonController;

    move-result-object v5

    .line 353
    .local v5, "negativeButtonController":Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonController;
    if-eqz v10, :cond_3

    .line 355
    iget v12, v10, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonController;->textResId:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->getStringFromResource(I)Ljava/lang/String;

    move-result-object v6

    .line 356
    .local v6, "okBtnText":Ljava/lang/String;
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_3

    .line 358
    invoke-virtual {v7, v6}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 361
    .end local v6    # "okBtnText":Ljava/lang/String;
    :cond_3
    if-eqz v5, :cond_4

    .line 363
    iget v12, v5, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonController;->textResId:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->getStringFromResource(I)Ljava/lang/String;

    move-result-object v2

    .line 364
    .local v2, "cancelBtnText":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_4

    .line 366
    invoke-virtual {v7, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 370
    .end local v2    # "cancelBtnText":Ljava/lang/String;
    .end local v5    # "negativeButtonController":Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonController;
    .end local v10    # "positiveButtonController":Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonController;
    :cond_4
    new-instance v12, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$6;

    move-object/from16 v0, p0

    invoke-direct {v12, v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$6;-><init>(Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;)V

    invoke-virtual {v7, v12}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 396
    new-instance v12, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$7;

    move-object/from16 v0, p0

    invoke-direct {v12, v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$7;-><init>(Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;)V

    invoke-virtual {v3, v12}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 416
    new-instance v12, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$8;

    move-object/from16 v0, p0

    invoke-direct {v12, v0, v9}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$8;-><init>(Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;Landroid/view/View;)V

    invoke-virtual {v9, v12}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 436
    return-object v9

    .line 319
    .end local v4    # "flags":I
    :cond_5
    if-eqz v11, :cond_1

    .line 321
    sget v12, Lcom/sec/android/app/shealth/common/commonui/R$id;->list_title:I

    invoke-virtual {v9, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/TextView;

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->getStringFromResource(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 332
    .restart local v4    # "flags":I
    :pswitch_1
    const/16 v12, 0x8

    invoke-virtual {v3, v12}, Landroid/widget/Button;->setVisibility(I)V

    .line 333
    sget v12, Lcom/sec/android/app/shealth/common/commonui/R$id;->btns_margin:I

    invoke-virtual {v9, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    const/16 v13, 0x8

    invoke-virtual {v12, v13}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    .line 336
    :pswitch_2
    const/16 v12, 0x8

    invoke-virtual {v7, v12}, Landroid/widget/Button;->setVisibility(I)V

    .line 337
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    sget v13, Lcom/sec/android/app/shealth/common/commonui/R$color;->cancel_ok_green_text_color:I

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getColor(I)I

    move-result v12

    invoke-virtual {v3, v12}, Landroid/widget/Button;->setTextColor(I)V

    .line 338
    sget v12, Lcom/sec/android/app/shealth/common/commonui/R$id;->btns_margin:I

    invoke-virtual {v9, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    const/16 v13, 0x8

    invoke-virtual {v12, v13}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    .line 341
    :pswitch_3
    const/16 v12, 0x8

    invoke-virtual {v3, v12}, Landroid/widget/Button;->setVisibility(I)V

    .line 342
    const/16 v12, 0x8

    invoke-virtual {v7, v12}, Landroid/widget/Button;->setVisibility(I)V

    .line 343
    sget v12, Lcom/sec/android/app/shealth/common/commonui/R$id;->listParent:I

    invoke-virtual {v9, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    sget v14, Lcom/sec/android/app/shealth/common/commonui/R$drawable;->tw_dialog_bottom_holo_light:I

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v13

    invoke-virtual {v12, v13}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 344
    sget v12, Lcom/sec/android/app/shealth/common/commonui/R$id;->listParent:I

    invoke-virtual {v9, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    sget v14, Lcom/sec/android/app/shealth/common/commonui/R$dimen;->dialog_side_padding:I

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v13

    const/4 v14, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    sget v16, Lcom/sec/android/app/shealth/common/commonui/R$dimen;->dialog_side_padding:I

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v15

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    sget v17, Lcom/sec/android/app/shealth/common/commonui/R$dimen;->dialog_bottom_padding:I

    invoke-virtual/range {v16 .. v17}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    invoke-virtual/range {v12 .. v16}, Landroid/view/View;->setPadding(IIII)V

    .line 345
    sget v12, Lcom/sec/android/app/shealth/common/commonui/R$id;->cancel_ok_buttons_layout:I

    invoke-virtual {v9, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    const/16 v13, 0x8

    invoke-virtual {v12, v13}, Landroid/view/View;->setVisibility(I)V

    .line 346
    sget v12, Lcom/sec/android/app/shealth/common/commonui/R$id;->cancel_ok_buttons_container:I

    invoke-virtual {v9, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    const/16 v13, 0x8

    invoke-virtual {v12, v13}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    .line 329
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method protected getStringFromResource(I)Ljava/lang/String;
    .locals 2
    .param p1, "resourceId"    # I

    .prologue
    .line 443
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 450
    :goto_0
    return-object v1

    .line 445
    :catch_0
    move-exception v0

    .line 447
    .local v0, "rnf":Landroid/content/res/Resources$NotFoundException;
    sget-object v1, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->TAG:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 450
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 108
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 109
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->setRetainInstance(Z)V

    .line 110
    const/4 v0, 0x2

    sget v1, Lcom/sec/android/app/shealth/common/commonui/R$style;->Dialog_NoTitleBar_And_Frame:I

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->setStyle(II)V

    .line 111
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 125
    new-instance v0, Landroid/app/Dialog;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->getTheme()I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 23
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 131
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v5

    .line 132
    .local v5, "arguments":Landroid/os/Bundle;
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-virtual {v0, v1, v2, v5}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->getGeneralParentContainer(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v12

    .line 133
    .local v12, "parentContent":Landroid/view/View;
    sget v19, Lcom/sec/android/app/shealth/common/commonui/R$id;->dialog_top_text:I

    move/from16 v0, v19

    invoke-virtual {v12, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/TextView;

    .line 134
    .local v18, "topTextView":Landroid/widget/TextView;
    sget v19, Lcom/sec/android/app/shealth/common/commonui/R$id;->text_list_divider:I

    move/from16 v0, v19

    invoke-virtual {v12, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    .line 135
    .local v9, "divider":Landroid/view/View;
    const-string/jumbo v19, "top_text"

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v17

    .line 136
    .local v17, "topTextResId":I
    const-string v19, "additional_top_text"

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 137
    .local v4, "additionalTopTextResId":I
    const-string v19, "disabled_items"

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v8

    .line 139
    .local v8, "disabledItems":[I
    if-eqz v18, :cond_1

    .line 141
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->getStringFromResource(I)Ljava/lang/String;

    move-result-object v15

    .line 142
    .local v15, "topText":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->getStringFromResource(I)Ljava/lang/String;

    move-result-object v3

    .line 143
    .local v3, "additionalTopText":Ljava/lang/String;
    const-string/jumbo v19, "top_description_text"

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 144
    .local v16, "topTextDescription":Ljava/lang/String;
    if-eqz v16, :cond_0

    .line 146
    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 148
    :cond_0
    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v19

    if-nez v19, :cond_1

    .line 150
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v19

    if-nez v19, :cond_4

    .line 152
    const/16 v19, 0x1

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    aput-object v3, v19, v20

    move-object/from16 v0, v19

    invoke-static {v15, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 158
    :goto_0
    const/16 v19, 0x0

    invoke-virtual/range {v18 .. v19}, Landroid/widget/TextView;->setVisibility(I)V

    .line 159
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v9, v0}, Landroid/view/View;->setVisibility(I)V

    .line 162
    .end local v3    # "additionalTopText":Ljava/lang/String;
    .end local v15    # "topText":Ljava/lang/String;
    .end local v16    # "topTextDescription":Ljava/lang/String;
    :cond_1
    sget v19, Lcom/sec/android/app/shealth/common/commonui/R$id;->list_in_dialog:I

    move/from16 v0, v19

    invoke-virtual {v12, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v19

    check-cast v19, Landroid/widget/ListView;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->listView:Landroid/widget/ListView;

    .line 163
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->getActivity()Landroid/app/Activity;

    move-result-object v19

    move-object/from16 v0, v19

    instance-of v0, v0, Lcom/sec/android/app/shealth/common/commonui/dialog/IComplexStringBuilderGetter;

    move/from16 v19, v0

    if-eqz v19, :cond_2

    .line 165
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->getActivity()Landroid/app/Activity;

    move-result-object v19

    check-cast v19, Lcom/sec/android/app/shealth/common/commonui/dialog/IComplexStringBuilderGetter;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->getTag()Ljava/lang/String;

    move-result-object v20

    invoke-interface/range {v19 .. v20}, Lcom/sec/android/app/shealth/common/commonui/dialog/IComplexStringBuilderGetter;->getComplexStringBuilder(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IContainComplexString;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->complexStringBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/IContainComplexString;

    .line 167
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->complexStringBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/IContainComplexString;

    move-object/from16 v19, v0

    if-nez v19, :cond_3

    .line 169
    const-string/jumbo v19, "text_for_items"

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->items:Ljava/util/List;

    .line 170
    const-string v19, "description_of_items"

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->descriptions:Ljava/util/List;

    .line 172
    :cond_3
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->items:Ljava/util/List;

    .line 173
    .local v10, "itemsStr":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->descriptions:Ljava/util/List;

    .line 174
    .local v7, "descriptionsStr":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v19, "checked_items"

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getBooleanArray(Ljava/lang/String;)[Z

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->isChecked:[Z

    .line 175
    const-string v19, "choose_type"

    const/16 v20, -0x1

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v5, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v19

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->mChoose_type:I

    .line 178
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->mChoose_type:I

    move/from16 v19, v0

    packed-switch v19, :pswitch_data_0

    .line 292
    new-instance v19, Ljava/lang/IllegalStateException;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    sget v22, Lcom/sec/android/app/shealth/common/commonui/R$string;->unknown_dialog_type_choose_type_exception:I

    invoke-virtual/range {v21 .. v22}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "="

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->mChoose_type:I

    move/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v19

    .line 156
    .end local v7    # "descriptionsStr":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v10    # "itemsStr":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v3    # "additionalTopText":Ljava/lang/String;
    .restart local v15    # "topText":Ljava/lang/String;
    .restart local v16    # "topTextDescription":Ljava/lang/String;
    :cond_4
    move-object/from16 v0, v18

    invoke-virtual {v0, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 181
    .end local v3    # "additionalTopText":Ljava/lang/String;
    .end local v15    # "topText":Ljava/lang/String;
    .end local v16    # "topTextDescription":Ljava/lang/String;
    .restart local v7    # "descriptionsStr":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v10    # "itemsStr":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :pswitch_0
    new-instance v19, Lcom/sec/android/app/shealth/common/commonui/dialog/SingleChoicePopupAdapter;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->isChecked:[Z

    move-object/from16 v20, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-direct {v0, v10, v7, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SingleChoicePopupAdapter;-><init>(Ljava/util/List;Ljava/util/List;[Z)V

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->adapter:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialogAdapter;

    .line 182
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->adapter:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialogAdapter;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialogAdapter;->setDisabled([I)V

    .line 184
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->adapter:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialogAdapter;

    move-object/from16 v19, v0

    check-cast v19, Lcom/sec/android/app/shealth/common/commonui/dialog/SingleChoicePopupAdapter;

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/shealth/common/commonui/dialog/SingleChoicePopupAdapter;->getIndexChecked()I

    move-result v14

    .line 185
    .local v14, "selectedItemIndex":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->adapter:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialogAdapter;

    move-object/from16 v19, v0

    new-instance v20, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$1;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$1;-><init>(Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;)V

    invoke-virtual/range {v19 .. v20}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialogAdapter;->setOnDialogListItemClick(Lcom/sec/android/app/shealth/common/commonui/dialog/OnDialogListItemClick;)V

    .line 204
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->listView:Landroid/widget/ListView;

    move-object/from16 v19, v0

    new-instance v20, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$2;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$2;-><init>(Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;)V

    invoke-virtual/range {v19 .. v20}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 215
    if-eqz v10, :cond_5

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v19

    const/16 v20, 0x9

    move/from16 v0, v19

    move/from16 v1, v20

    if-lt v0, v1, :cond_5

    .line 216
    sget v19, Lcom/sec/android/app/shealth/common/commonui/R$id;->middle_layout:I

    move/from16 v0, v19

    invoke-virtual {v12, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/LinearLayout;

    .line 217
    .local v13, "rl":Landroid/widget/LinearLayout;
    invoke-virtual {v13}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v11

    .line 219
    .local v11, "params":Landroid/view/ViewGroup$LayoutParams;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    sget v20, Lcom/sec/android/app/shealth/common/commonui/R$dimen;->max_height_listpopup:I

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v19

    move/from16 v0, v19

    float-to-int v0, v0

    move/from16 v19, v0

    move/from16 v0, v19

    iput v0, v11, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 220
    invoke-virtual {v13, v11}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 223
    .end local v11    # "params":Landroid/view/ViewGroup$LayoutParams;
    .end local v13    # "rl":Landroid/widget/LinearLayout;
    :cond_5
    sget v19, Lcom/sec/android/app/shealth/common/commonui/R$id;->cancel_button:I

    move/from16 v0, v19

    invoke-virtual {v12, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/Button;

    .line 224
    .local v6, "cancelButton":Landroid/widget/Button;
    if-eqz v6, :cond_6

    .line 226
    new-instance v19, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$3;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$3;-><init>(Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;)V

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Landroid/widget/Button;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 241
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->listView:Landroid/widget/ListView;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->adapter:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialogAdapter;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 242
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->listView:Landroid/widget/ListView;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v14}, Landroid/widget/ListView;->setSelection(I)V

    .line 243
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v19

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 295
    .end local v6    # "cancelButton":Landroid/widget/Button;
    .end local v14    # "selectedItemIndex":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->listView:Landroid/widget/ListView;

    move-object/from16 v19, v0

    const/16 v20, 0x1

    invoke-virtual/range {v19 .. v20}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 297
    return-object v12

    .line 247
    :pswitch_1
    new-instance v19, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialogAdapter;

    move-object/from16 v0, v19

    invoke-direct {v0, v10, v7}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialogAdapter;-><init>(Ljava/util/List;Ljava/util/List;)V

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->adapter:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialogAdapter;

    .line 248
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->adapter:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialogAdapter;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialogAdapter;->setDisabled([I)V

    .line 249
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->adapter:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialogAdapter;

    move-object/from16 v19, v0

    new-instance v20, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$4;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$4;-><init>(Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;)V

    invoke-virtual/range {v19 .. v20}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialogAdapter;->setOnDialogListItemClick(Lcom/sec/android/app/shealth/common/commonui/dialog/OnDialogListItemClick;)V

    .line 261
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->listView:Landroid/widget/ListView;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->adapter:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialogAdapter;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 262
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v19

    const/16 v20, 0x1

    invoke-virtual/range {v19 .. v20}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    goto :goto_1

    .line 265
    :pswitch_2
    new-instance v19, Lcom/sec/android/app/shealth/common/commonui/dialog/MultipleChoicePopupAdapter;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->isChecked:[Z

    move-object/from16 v20, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-direct {v0, v10, v7, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/MultipleChoicePopupAdapter;-><init>(Ljava/util/List;Ljava/util/List;[Z)V

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->adapter:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialogAdapter;

    .line 266
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->adapter:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialogAdapter;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialogAdapter;->setDisabled([I)V

    .line 269
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->adapter:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialogAdapter;

    move-object/from16 v19, v0

    new-instance v20, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$5;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$5;-><init>(Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;)V

    invoke-virtual/range {v19 .. v20}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialogAdapter;->setOnDialogListItemClick(Lcom/sec/android/app/shealth/common/commonui/dialog/OnDialogListItemClick;)V

    .line 285
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->getActivity()Landroid/app/Activity;

    move-result-object v19

    check-cast v19, Lcom/sec/android/app/shealth/common/commonui/dialog/ISelectedItemListenerGetter;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->getTag()Ljava/lang/String;

    move-result-object v20

    invoke-interface/range {v19 .. v20}, Lcom/sec/android/app/shealth/common/commonui/dialog/ISelectedItemListenerGetter;->getOnSelectedItemListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/OnSelectedItemListener;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->onSelectedListener:Lcom/sec/android/app/shealth/common/commonui/dialog/OnSelectedItemListener;

    .line 287
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->listView:Landroid/widget/ListView;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->adapter:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialogAdapter;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 288
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v19

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    goto/16 :goto_1

    .line 178
    :pswitch_data_0
    .packed-switch 0x9
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 456
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->getRetainInstance()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 457
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setDismissMessage(Landroid/os/Message;)V

    .line 458
    :cond_0
    iput-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->listView:Landroid/widget/ListView;

    .line 459
    invoke-super {p0}, Landroid/app/DialogFragment;->onDestroyView()V

    .line 460
    return-void
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 465
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 466
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->getActivity()Landroid/app/Activity;

    move-result-object v1

    instance-of v1, v1, Lcom/sec/android/app/shealth/common/commonui/dialog/IDismissControllerProvider;

    if-eqz v1, :cond_1

    .line 468
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/common/commonui/dialog/IDismissControllerProvider;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->getTag()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/IDismissControllerProvider;->getDismissController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDismissController;

    move-result-object v0

    .line 469
    .local v0, "dismissController":Lcom/sec/android/app/shealth/common/commonui/dialog/IDismissController;
    if-eqz v0, :cond_1

    .line 471
    invoke-interface {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/IDismissController;->onDismiss()V

    .line 479
    .end local v0    # "dismissController":Lcom/sec/android/app/shealth/common/commonui/dialog/IDismissController;
    :cond_0
    :goto_0
    return-void

    .line 475
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->mDismissListener:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$IOnDismissListener;

    if-eqz v1, :cond_0

    .line 477
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->mDismissListener:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$IOnDismissListener;

    invoke-interface {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$IOnDismissListener;->onDismiss()V

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 116
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->mIsInstanceStateLoss:Z

    .line 117
    const-string v0, "WORKAROUND_FOR_BUG_19917_KEY"

    const-string v1, "WORKAROUND_FOR_ANDROID_SUPPORT_LIB_BUG_19917_VALUE"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 119
    const-string v0, "checked_items"

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->isChecked:[Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBooleanArray(Ljava/lang/String;[Z)V

    .line 120
    return-void
.end method

.method public setOnDismissListener(Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$IOnDismissListener;)V
    .locals 0
    .param p1, "dismissListener"    # Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$IOnDismissListener;

    .prologue
    .line 102
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->mDismissListener:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$IOnDismissListener;

    .line 103
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 626
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->titleManual:Ljava/lang/String;

    .line 627
    return-void
.end method
