.class Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader$BitmapDownloaderTask;
.super Landroid/os/AsyncTask;
.source "ExerciseProBitmapLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "BitmapDownloaderTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field private cropAndScale:Z

.field private height:I

.field private final imageViewReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/widget/ImageView;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader;

.field private url:Ljava/lang/String;

.field private width:I


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader;Landroid/widget/ImageView;)V
    .locals 1
    .param p2, "imageView"    # Landroid/widget/ImageView;

    .prologue
    const/16 v0, 0x190

    .line 94
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader$BitmapDownloaderTask;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 91
    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader$BitmapDownloaderTask;->width:I

    .line 92
    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader$BitmapDownloaderTask;->height:I

    .line 95
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader$BitmapDownloaderTask;->imageViewReference:Ljava/lang/ref/WeakReference;

    .line 96
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader;Landroid/widget/ImageView;Z)V
    .locals 1
    .param p2, "imageView"    # Landroid/widget/ImageView;
    .param p3, "cropAndScale"    # Z

    .prologue
    const/16 v0, 0x190

    .line 98
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader$BitmapDownloaderTask;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 91
    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader$BitmapDownloaderTask;->width:I

    .line 92
    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader$BitmapDownloaderTask;->height:I

    .line 99
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader$BitmapDownloaderTask;->imageViewReference:Ljava/lang/ref/WeakReference;

    .line 100
    iput-boolean p3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader$BitmapDownloaderTask;->cropAndScale:Z

    .line 101
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 4
    .param p1, "params"    # [Ljava/lang/String;

    .prologue
    .line 105
    const/4 v0, 0x0

    aget-object v0, p1, v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader$BitmapDownloaderTask;->url:Ljava/lang/String;

    .line 106
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader$BitmapDownloaderTask;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader$BitmapDownloaderTask;->url:Ljava/lang/String;

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader$BitmapDownloaderTask;->width:I

    iget v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader$BitmapDownloaderTask;->height:I

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader;->downloadBitmap(Ljava/lang/String;II)Landroid/graphics/Bitmap;
    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader;->access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader;Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 87
    check-cast p1, [Ljava/lang/String;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader$BitmapDownloaderTask;->doInBackground([Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Landroid/graphics/Bitmap;)V
    .locals 4
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 111
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader$BitmapDownloaderTask;->isCancelled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 112
    const/4 p1, 0x0

    .line 114
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader$BitmapDownloaderTask;->imageViewReference:Ljava/lang/ref/WeakReference;

    if-eqz v1, :cond_1

    .line 115
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader$BitmapDownloaderTask;->imageViewReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 116
    .local v0, "imageView":Landroid/widget/ImageView;
    if-nez p1, :cond_2

    .line 126
    .end local v0    # "imageView":Landroid/widget/ImageView;
    :cond_1
    :goto_0
    return-void

    .line 118
    .restart local v0    # "imageView":Landroid/widget/ImageView;
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader$BitmapDownloaderTask;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader$BitmapDownloaderTask;->url:Ljava/lang/String;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader;->addBitmapToCache(Ljava/lang/String;Landroid/graphics/Bitmap;)V
    invoke-static {v1, v2, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader;->access$100(Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 119
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader$BitmapDownloaderTask;->cropAndScale:Z

    if-eqz v1, :cond_3

    .line 120
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader$BitmapDownloaderTask;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getWidth()I

    move-result v2

    invoke-virtual {v0}, Landroid/widget/ImageView;->getHeight()I

    move-result v3

    invoke-static {p1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/BitmapUtil;->cropAndScaleBitmap(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v2

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader;->setImageBitmap(Landroid/widget/ImageView;Landroid/graphics/Bitmap;)V
    invoke-static {v1, v0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader;->access$300(Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader;Landroid/widget/ImageView;Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 122
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader$BitmapDownloaderTask;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader;->setImageBitmap(Landroid/widget/ImageView;Landroid/graphics/Bitmap;)V
    invoke-static {v1, v0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader;->access$300(Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader;Landroid/widget/ImageView;Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 87
    check-cast p1, Landroid/graphics/Bitmap;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader$BitmapDownloaderTask;->onPostExecute(Landroid/graphics/Bitmap;)V

    return-void
.end method
