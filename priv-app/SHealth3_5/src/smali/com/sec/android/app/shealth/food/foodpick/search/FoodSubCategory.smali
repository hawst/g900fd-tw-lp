.class public Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategory;
.super Ljava/lang/Object;
.source "FoodSubCategory.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/sec/android/app/shealth/food/foodpick/search/TextOnlyData;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategory;",
            ">;"
        }
    .end annotation
.end field

.field private static final DEFAULT_ID:Ljava/lang/String; = "0"


# instance fields
.field private mSubCategoryId:Ljava/lang/String;

.field private mSubCategoryName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 83
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategory$1;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategory$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategory;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "parcel"    # Landroid/os/Parcel;

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategory;->mSubCategoryName:Ljava/lang/String;

    .line 50
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategory;->mSubCategoryId:Ljava/lang/String;

    .line 51
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "subCategoryName"    # Ljava/lang/String;

    .prologue
    .line 31
    const-string v0, "0"

    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategory;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "subCategoryId"    # Ljava/lang/String;
    .param p2, "subCategoryName"    # Ljava/lang/String;

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategory;->mSubCategoryName:Ljava/lang/String;

    .line 40
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategory;->mSubCategoryId:Ljava/lang/String;

    .line 41
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 74
    const/4 v0, 0x0

    return v0
.end method

.method public getSubCategoryId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategory;->mSubCategoryId:Ljava/lang/String;

    return-object v0
.end method

.method public getSubCategoryName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategory;->mSubCategoryName:Ljava/lang/String;

    return-object v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategory;->getSubCategoryName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "parcel"    # Landroid/os/Parcel;
    .param p2, "i"    # I

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategory;->mSubCategoryName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 80
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategory;->mSubCategoryId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 81
    return-void
.end method
