.class public Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;
.super Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$WearableSyncEventListener;,
        Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$DeviceInfo;,
        Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$FragmentEventListener;,
        Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$ScannedDeviceListAdapter;,
        Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$WearableStatusObserver;
    }
.end annotation


# static fields
.field public static ACTION_BAR_TITLE_RES_ID:Ljava/lang/String; = null

.field public static CONNECTIVITY_TYPE_KEY:Ljava/lang/String; = null

.field public static DATA_TYPE_KEY:Ljava/lang/String; = null

.field public static DEVICE_TYPES_KEY:Ljava/lang/String; = null

.field public static DEVICE_TYPE_TEXT_KEY:Ljava/lang/String; = null

.field public static DEVICE_TYPE_TEXT_RES_ID_KEY:Ljava/lang/String; = null

.field private static final DISPLAY_PAIRED_DEVICES:I = 0x65

.field private static final FINISH_ON_PAIRED:I = 0x6c

.field private static final FINISH_SYNC:I = 0x70

.field public static HEADER_TEXT_ID:Ljava/lang/String; = null

.field public static HEADER_TEXT_RES_ID_KEYS:Ljava/lang/String; = null

.field protected static HIDE_NO_DEVICES:I = 0x0

.field private static HRM_ANT_CONNECTED:Ljava/lang/String; = null

.field private static HRM_ANT_DISCONNECTED:Ljava/lang/String; = null

.field private static HRM_SAP_CONNECTED:Ljava/lang/String; = null

.field private static HRM_SAP_DISCONNECTED:Ljava/lang/String; = null

.field private static final INITIATED_SYNC:I = 0x6e

.field private static KEY_FOR_BT_DEVICES_LIST:Ljava/lang/String; = null

.field private static KEY_FOR_BT_DEVICES_MAP:Ljava/lang/String; = null

.field public static KEY_NO_SUPPORT_SCAN:Ljava/lang/String; = null

.field public static KEY_SHOW_CAMPANION_DEV:Ljava/lang/String; = null

.field public static NO_DEVICES_TEXT_KEY:Ljava/lang/String; = null

.field public static NO_DEVICES_TEXT_RES_ID_KEY:Ljava/lang/String; = null

.field public static PAIRED_TEXT_KEY:Ljava/lang/String; = null

.field public static PAIRED_TEXT_RES_ID_KEY:Ljava/lang/String; = null

.field private static final REMOVE_LEFT_DEVICE:I = 0x6b

.field public static final REQUEST_PAIRED_SETTINGS:I = 0x3e9

.field public static SCANNING_SUB_TABS_INFO:Ljava/lang/String; = null

.field public static SCANNING_SUB_TABS_INFO_RES_IDS_KEY:Ljava/lang/String; = null

.field public static SCANNING_TYPE_INFO_CONTENT:Ljava/lang/String; = null

.field public static SCANNING_TYPE_INFO_CONTENT_IDS:Ljava/lang/String; = null

.field public static SCANNING_TYPE_INFO_TEXT:Ljava/lang/String; = null

.field public static SCANNING_TYPE_INFO_TEXT_RES_ID_KEY:Ljava/lang/String; = null

.field private static final SCAN_DURATION_SEC:I = 0x28

.field private static SETTINGS_RESOURCE:Ljava/lang/String; = null

.field private static final SHOW_FAILURE_DIALOG:I = 0x73

.field protected static SHOW_WEIGHTSCALE_ALERT_BOX:I = 0x0

.field protected static START_RECIEVE_DATA_AGAIN:I = 0x0

.field private static final START_SCANNING_DEVICES:I = 0x6a

.field private static final START_SYNC:I = 0x6f

.field private static TAG:Ljava/lang/String; = null

.field private static final UPDATE_SCANNING_FRAGMENT:I = 0x71

.field private static final WEARABLE_STATUS_CHANGED:I = 0x72

.field public static connectedHrmUIName:Ljava/lang/String;

.field private static mDeviceResMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static mUpdateScanStatus:Z


# instance fields
.field private IsAutoScanRequired:Z

.field private IsJoining:Z

.field private IsScreenLocked:Z

.field protected JoinDeviceID:Ljava/lang/Object;

.field public SETTINGS_CONNECT_TAG:Ljava/lang/String;

.field public SETTINGS_DISCONNECT_TAG:Ljava/lang/String;

.field public SETTINGS_RENAME_TAG:Ljava/lang/String;

.field public SETTINGS_UNPAIR_TAG:Ljava/lang/String;

.field private checkIsHrmSwitchReq:Z

.field private connectToastReq:Z

.field private connectedHRM:Ljava/lang/String;

.field private contentId:I

.field deviceIDRename:Ljava/lang/String;

.field private deviceMap:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;",
            ">;"
        }
    .end annotation
.end field

.field private deviceTypeTextResId:I

.field private dialog:Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;

.field private disconnectToastReq:Z

.field handler:Landroid/os/Handler;

.field private headerText:Landroid/widget/TextView;

.field private isBluetoothTurningON:Z

.field private isFragmetRunning:Z

.field private isPairingNeeded:Z

.field private isReScanNeeded:Z

.field private isScanning:Z

.field private mActionBarButtonBuilder:Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

.field private mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

.field private mConnectivityType:I

.field private mContext:Landroid/content/Context;

.field private mDataType:I

.field private mDeviceTypeText:Ljava/lang/String;

.field private mDeviceTypes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mDevicename:Landroid/widget/TextView;

.field private mExerciseScanningInstance:Z

.field private mHeaderLayout:Landroid/widget/LinearLayout;

.field private mHeaderTextResIds:[I

.field private mInfoContent:Z

.field private mInfoContentIds:[I

.field private mInformationView:Landroid/widget/TextView;

.field private mIsNoSupportScan:Z

.field private mPairedDeviceMap:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;",
            ">;"
        }
    .end annotation
.end field

.field private mPairedDevicesLayout:Landroid/widget/LinearLayout;

.field private mPairedListContainer:Landroid/widget/LinearLayout;

.field private mPairedListLayout:Landroid/widget/LinearLayout;

.field private mPairedText:Ljava/lang/String;

.field private mPairedTextResId:I

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field private mScanlistImage:Landroid/widget/LinearLayout;

.field private mScannedDeviceListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$ScannedDeviceListAdapter;

.field private mScannedDevicesList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/framework/ui/base/PairOfDeviceIdAndName;",
            ">;"
        }
    .end annotation
.end field

.field private mScannedlist:Landroid/widget/LinearLayout;

.field private mScanningFragmentController:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;

.field private mScanningFragmentProgressBar:Landroid/widget/ImageView;

.field mScanningFragmentScanListener:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$ScanningFragmentScanListener;

.field private mScanningInfoTextResId:I

.field mSensorConnectionListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;

.field private mSubTabsContent:[Ljava/lang/String;

.field private mSubTabsContentResIds:[I

.field private mSubTabsGrp:Landroid/widget/RadioGroup;

.field private mfirstSubTab:Landroid/widget/RadioButton;

.field private mscanningIcon:Landroid/widget/ImageView;

.field private msecondSubTab:Landroid/widget/RadioButton;

.field private noDeviceString:Ljava/lang/String;

.field private noDeviceStringResId:I

.field private nodevices:Landroid/widget/FrameLayout;

.field private nodevicesImage:Landroid/widget/ImageView;

.field private nodevicesText:Landroid/widget/TextView;

.field private pairDeviceId:Ljava/lang/String;

.field private progressDlg:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

.field private refreshPairedList:Z

.field private rename:Ljava/lang/String;

.field private rootview:Landroid/view/View;

.field private scanbutton:Landroid/widget/Button;

.field private scanningInfoText:Ljava/lang/String;

.field private scanningLayout:Landroid/widget/RelativeLayout;

.field private selectedDeviceId:Ljava/lang/String;

.field private showCompanionDevices:Z

.field private spinner:Landroid/widget/ProgressBar;

.field private syncProgressDlg:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

.field private unpairedSensorDeviceDetails:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

.field private wearableContentObserver:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$WearableStatusObserver;


# direct methods
.method static constructor <clinit>()V
    .locals 33

    const/16 v0, 0x12

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x4

    const/4 v6, 0x5

    const/4 v7, 0x6

    const/4 v8, 0x7

    const/16 v9, 0x8

    const/16 v10, 0x9

    const/16 v11, 0xa

    const/16 v12, 0xb

    const/16 v13, 0xc

    const/16 v14, 0xd

    const/16 v15, 0xe

    const/16 v16, 0xf

    const/16 v17, 0x10

    const/16 v18, 0x11

    const/16 v19, -0x5b

    aput v19, v1, v18

    const/16 v18, -0x7a92

    aput v18, v1, v17

    const/16 v17, -0x1a

    aput v17, v1, v16

    const/16 v16, -0x3c

    aput v16, v1, v15

    const/16 v15, -0x38

    aput v15, v1, v14

    const/16 v14, -0x2e

    aput v14, v1, v13

    const/4 v13, -0x3

    aput v13, v1, v12

    const/16 v12, -0x6a

    aput v12, v1, v11

    const/16 v11, -0x19

    aput v11, v1, v10

    const/16 v10, -0x52d5

    aput v10, v1, v9

    const/16 v9, -0x28

    aput v9, v1, v8

    const/16 v8, -0x19f1

    aput v8, v1, v7

    const/16 v7, -0x47

    aput v7, v1, v6

    const/16 v6, -0x2f

    aput v6, v1, v5

    const/16 v5, -0x63ed

    aput v5, v1, v4

    const/16 v4, -0x3d

    aput v4, v1, v3

    const/16 v3, -0x5a2

    aput v3, v1, v2

    const/16 v2, -0x6d

    aput v2, v1, v0

    const/16 v0, 0x12

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, 0x10

    const/16 v19, 0x11

    const/16 v20, -0x35

    aput v20, v0, v19

    const/16 v19, -0x7af1

    aput v19, v0, v18

    const/16 v18, -0x7b

    aput v18, v0, v17

    const/16 v17, -0x49

    aput v17, v0, v16

    const/16 v16, -0x69

    aput v16, v0, v15

    const/16 v15, -0x5a

    aput v15, v0, v14

    const/16 v14, -0x71

    aput v14, v0, v13

    const/4 v13, -0x7

    aput v13, v0, v12

    const/16 v12, -0x69

    aput v12, v0, v11

    const/16 v11, -0x52a5

    aput v11, v0, v10

    const/16 v10, -0x53

    aput v10, v0, v9

    const/16 v9, -0x1984

    aput v9, v0, v8

    const/16 v8, -0x1a

    aput v8, v0, v7

    const/16 v7, -0x42

    aput v7, v0, v6

    const/16 v6, -0x6383

    aput v6, v0, v5

    const/16 v5, -0x64

    aput v5, v0, v4

    const/16 v4, -0x5d3

    aput v4, v0, v3

    const/4 v3, -0x6

    aput v3, v0, v2

    const/4 v2, 0x0

    :goto_0
    array-length v3, v0

    if-lt v2, v3, :cond_0

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_1
    array-length v3, v0

    if-lt v2, v3, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->KEY_NO_SUPPORT_SCAN:Ljava/lang/String;

    const/16 v0, 0x16

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x4

    const/4 v6, 0x5

    const/4 v7, 0x6

    const/4 v8, 0x7

    const/16 v9, 0x8

    const/16 v10, 0x9

    const/16 v11, 0xa

    const/16 v12, 0xb

    const/16 v13, 0xc

    const/16 v14, 0xd

    const/16 v15, 0xe

    const/16 v16, 0xf

    const/16 v17, 0x10

    const/16 v18, 0x11

    const/16 v19, 0x12

    const/16 v20, 0x13

    const/16 v21, 0x14

    const/16 v22, 0x15

    const/16 v23, -0x27

    aput v23, v1, v22

    const/16 v22, -0x6e

    aput v22, v1, v21

    const/16 v21, -0x68d4

    aput v21, v1, v20

    const/16 v20, -0x2

    aput v20, v1, v19

    const/16 v19, -0x489e

    aput v19, v1, v18

    const/16 v18, -0x2e

    aput v18, v1, v17

    const/16 v17, -0x17

    aput v17, v1, v16

    const/16 v16, 0x305

    aput v16, v1, v15

    const/16 v15, 0x466d

    aput v15, v1, v14

    const/16 v14, 0x3429

    aput v14, v1, v13

    const/16 v13, -0x43a3

    aput v13, v1, v12

    const/16 v12, -0x2e

    aput v12, v1, v11

    const/16 v11, -0x1a

    aput v11, v1, v10

    const/16 v10, 0x1230

    aput v10, v1, v9

    const/16 v9, 0x67f

    aput v9, v1, v8

    const/16 v8, 0x869

    aput v8, v1, v7

    const/16 v7, -0x7795

    aput v7, v1, v6

    const/16 v6, -0x29

    aput v6, v1, v5

    const/16 v5, -0x6f

    aput v5, v1, v4

    const/16 v4, 0x2f58

    aput v4, v1, v3

    const/16 v3, -0x2eb9

    aput v3, v1, v2

    const/16 v2, -0x5e

    aput v2, v1, v0

    const/16 v0, 0x16

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, 0x10

    const/16 v19, 0x11

    const/16 v20, 0x12

    const/16 v21, 0x13

    const/16 v22, 0x14

    const/16 v23, 0x15

    const/16 v24, -0x56

    aput v24, v0, v23

    const/16 v23, -0x9

    aput v23, v0, v22

    const/16 v22, -0x68b1

    aput v22, v0, v21

    const/16 v21, -0x69

    aput v21, v0, v20

    const/16 v20, -0x48ec

    aput v20, v0, v19

    const/16 v19, -0x49

    aput v19, v0, v18

    const/16 v18, -0x73

    aput v18, v0, v17

    const/16 v17, 0x35a

    aput v17, v0, v16

    const/16 v16, 0x4603

    aput v16, v0, v15

    const/16 v15, 0x3446

    aput v15, v0, v14

    const/16 v14, -0x43cc

    aput v14, v0, v13

    const/16 v13, -0x44

    aput v13, v0, v12

    const/16 v12, -0x79

    aput v12, v0, v11

    const/16 v11, 0x1240

    aput v11, v0, v10

    const/16 v10, 0x612

    aput v10, v0, v9

    const/16 v9, 0x806

    aput v9, v0, v8

    const/16 v8, -0x77f8

    aput v8, v0, v7

    const/16 v7, -0x78

    aput v7, v0, v6

    const/16 v6, -0x1a

    aput v6, v0, v5

    const/16 v5, 0x2f37

    aput v5, v0, v4

    const/16 v4, -0x2ed1

    aput v4, v0, v3

    const/16 v3, -0x2f

    aput v3, v0, v2

    const/4 v2, 0x0

    :goto_2
    array-length v3, v0

    if-lt v2, v3, :cond_2

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_3
    array-length v3, v0

    if-lt v2, v3, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->KEY_SHOW_CAMPANION_DEV:Ljava/lang/String;

    const/4 v0, 0x5

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x4

    const/16 v6, -0x53

    aput v6, v1, v5

    const/16 v5, -0x18be

    aput v5, v1, v4

    const/16 v4, -0x5a

    aput v4, v1, v3

    const/16 v3, 0x1d6e

    aput v3, v1, v2

    const/16 v2, 0x3155

    aput v2, v1, v0

    const/4 v0, 0x5

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/16 v7, -0x61

    aput v7, v0, v6

    const/16 v6, -0x188e

    aput v6, v0, v5

    const/16 v5, -0x19

    aput v5, v0, v4

    const/16 v4, 0x1d3c

    aput v4, v0, v3

    const/16 v3, 0x311d

    aput v3, v0, v2

    const/4 v2, 0x0

    :goto_4
    array-length v3, v0

    if-lt v2, v3, :cond_4

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_5
    array-length v3, v0

    if-lt v2, v3, :cond_5

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->HRM_ANT_DISCONNECTED:Ljava/lang/String;

    const/4 v0, 0x5

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x4

    const/4 v6, -0x1

    aput v6, v1, v5

    const/16 v5, -0x7290

    aput v5, v1, v4

    const/16 v4, -0x34

    aput v4, v1, v3

    const/16 v3, 0x813

    aput v3, v1, v2

    const/16 v2, 0x4840

    aput v2, v1, v0

    const/4 v0, 0x5

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/16 v7, -0x32

    aput v7, v0, v6

    const/16 v6, -0x72c0

    aput v6, v0, v5

    const/16 v5, -0x73

    aput v5, v0, v4

    const/16 v4, 0x841

    aput v4, v0, v3

    const/16 v3, 0x4808

    aput v3, v0, v2

    const/4 v2, 0x0

    :goto_6
    array-length v3, v0

    if-lt v2, v3, :cond_6

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_7
    array-length v3, v0

    if-lt v2, v3, :cond_7

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->HRM_ANT_CONNECTED:Ljava/lang/String;

    const/4 v0, 0x5

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x4

    const/16 v6, -0x23

    aput v6, v1, v5

    const/16 v5, -0x64

    aput v5, v1, v4

    const/16 v4, 0x6269

    aput v4, v1, v3

    const/16 v3, -0xd0

    aput v3, v1, v2

    const/16 v2, -0x49

    aput v2, v1, v0

    const/4 v0, 0x5

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/16 v7, -0x11

    aput v7, v0, v6

    const/16 v6, -0x54

    aput v6, v0, v5

    const/16 v5, 0x623a

    aput v5, v0, v4

    const/16 v4, -0x9e

    aput v4, v0, v3

    const/4 v3, -0x1

    aput v3, v0, v2

    const/4 v2, 0x0

    :goto_8
    array-length v3, v0

    if-lt v2, v3, :cond_8

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_9
    array-length v3, v0

    if-lt v2, v3, :cond_9

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->HRM_SAP_DISCONNECTED:Ljava/lang/String;

    const/4 v0, 0x5

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x4

    const/16 v6, -0x65fa

    aput v6, v1, v5

    const/16 v5, -0x56

    aput v5, v1, v4

    const/16 v4, -0x60

    aput v4, v1, v3

    const/16 v3, 0x2106

    aput v3, v1, v2

    const/16 v2, -0x2b97

    aput v2, v1, v0

    const/4 v0, 0x5

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/16 v7, -0x65c9

    aput v7, v0, v6

    const/16 v6, -0x66

    aput v6, v0, v5

    const/16 v5, -0xd

    aput v5, v0, v4

    const/16 v4, 0x2154

    aput v4, v0, v3

    const/16 v3, -0x2bdf

    aput v3, v0, v2

    const/4 v2, 0x0

    :goto_a
    array-length v3, v0

    if-lt v2, v3, :cond_a

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_b
    array-length v3, v0

    if-lt v2, v3, :cond_b

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->HRM_SAP_CONNECTED:Ljava/lang/String;

    const/16 v0, 0x14

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x4

    const/4 v6, 0x5

    const/4 v7, 0x6

    const/4 v8, 0x7

    const/16 v9, 0x8

    const/16 v10, 0x9

    const/16 v11, 0xa

    const/16 v12, 0xb

    const/16 v13, 0xc

    const/16 v14, 0xd

    const/16 v15, 0xe

    const/16 v16, 0xf

    const/16 v17, 0x10

    const/16 v18, 0x11

    const/16 v19, 0x12

    const/16 v20, 0x13

    const/16 v21, 0xa7e

    aput v21, v1, v20

    const/16 v20, -0x4c92

    aput v20, v1, v19

    const/16 v19, -0x26

    aput v19, v1, v18

    const/16 v18, -0x71

    aput v18, v1, v17

    const/16 v17, -0x5b

    aput v17, v1, v16

    const/16 v16, -0x78

    aput v16, v1, v15

    const/16 v15, -0x77b1

    aput v15, v1, v14

    const/16 v14, -0x1f

    aput v14, v1, v13

    const/16 v13, -0x52b3

    aput v13, v1, v12

    const/16 v12, -0xe

    aput v12, v1, v11

    const/16 v11, -0x32

    aput v11, v1, v10

    const/4 v10, -0x8

    aput v10, v1, v9

    const/16 v9, 0x2912

    aput v9, v1, v8

    const/16 v8, -0x478a

    aput v8, v1, v7

    const/16 v7, -0x2a

    aput v7, v1, v6

    const/16 v6, -0x36

    aput v6, v1, v5

    const/16 v5, -0x72

    aput v5, v1, v4

    const/16 v4, -0x3dd

    aput v4, v1, v3

    const/16 v3, -0x61

    aput v3, v1, v2

    const/16 v2, 0x611d

    aput v2, v1, v0

    const/16 v0, 0x14

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, 0x10

    const/16 v19, 0x11

    const/16 v20, 0x12

    const/16 v21, 0x13

    const/16 v22, 0xa0d

    aput v22, v0, v21

    const/16 v21, -0x4cf6

    aput v21, v0, v20

    const/16 v20, -0x4d

    aput v20, v0, v19

    const/16 v19, -0x30

    aput v19, v0, v18

    const/16 v18, -0x40

    aput v18, v0, v17

    const/16 v17, -0x1c

    aput v17, v0, v16

    const/16 v16, -0x77c5

    aput v16, v0, v15

    const/16 v15, -0x78

    aput v15, v0, v14

    const/16 v14, -0x52c7

    aput v14, v0, v13

    const/16 v13, -0x53

    aput v13, v0, v12

    const/16 v12, -0x44

    aput v12, v0, v11

    const/16 v11, -0x67

    aput v11, v0, v10

    const/16 v10, 0x2970

    aput v10, v0, v9

    const/16 v9, -0x47d7

    aput v9, v0, v8

    const/16 v8, -0x48

    aput v8, v0, v7

    const/16 v7, -0x5b

    aput v7, v0, v6

    const/16 v6, -0x19

    aput v6, v0, v5

    const/16 v5, -0x3a9

    aput v5, v0, v4

    const/4 v4, -0x4

    aput v4, v0, v3

    const/16 v3, 0x617c

    aput v3, v0, v2

    const/4 v2, 0x0

    :goto_c
    array-length v3, v0

    if-lt v2, v3, :cond_c

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_d
    array-length v3, v0

    if-lt v2, v3, :cond_d

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->ACTION_BAR_TITLE_RES_ID:Ljava/lang/String;

    const/16 v0, 0x1e

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x4

    const/4 v6, 0x5

    const/4 v7, 0x6

    const/4 v8, 0x7

    const/16 v9, 0x8

    const/16 v10, 0x9

    const/16 v11, 0xa

    const/16 v12, 0xb

    const/16 v13, 0xc

    const/16 v14, 0xd

    const/16 v15, 0xe

    const/16 v16, 0xf

    const/16 v17, 0x10

    const/16 v18, 0x11

    const/16 v19, 0x12

    const/16 v20, 0x13

    const/16 v21, 0x14

    const/16 v22, 0x15

    const/16 v23, 0x16

    const/16 v24, 0x17

    const/16 v25, 0x18

    const/16 v26, 0x19

    const/16 v27, 0x1a

    const/16 v28, 0x1b

    const/16 v29, 0x1c

    const/16 v30, 0x1d

    const/16 v31, -0x78

    aput v31, v1, v30

    const/16 v30, -0x2b97

    aput v30, v1, v29

    const/16 v29, -0x75

    aput v29, v1, v28

    const/16 v28, -0x48b7

    aput v28, v1, v27

    const/16 v27, -0x2e

    aput v27, v1, v26

    const/16 v26, -0x6b

    aput v26, v1, v25

    const/16 v25, 0x2a18

    aput v25, v1, v24

    const/16 v24, -0x6ba2

    aput v24, v1, v23

    const/16 v23, -0x14

    aput v23, v1, v22

    const/16 v22, -0x64e6

    aput v22, v1, v21

    const/16 v21, -0x11

    aput v21, v1, v20

    const/16 v20, -0x7c

    aput v20, v1, v19

    const/16 v19, -0x5

    aput v19, v1, v18

    const/16 v18, -0x42f7

    aput v18, v1, v17

    const/16 v17, -0x2d

    aput v17, v1, v16

    const/16 v16, 0xd2a

    aput v16, v1, v15

    const/16 v15, 0x5a52

    aput v15, v1, v14

    const/16 v14, -0x67c1

    aput v14, v1, v13

    const/16 v13, -0x18

    aput v13, v1, v12

    const/16 v12, 0x253c

    aput v12, v1, v11

    const/16 v11, 0x1451

    aput v11, v1, v10

    const/16 v10, 0x494b

    aput v10, v1, v9

    const/16 v9, -0x45d2

    aput v9, v1, v8

    const/16 v8, -0x2c

    aput v8, v1, v7

    const/16 v7, -0x64

    aput v7, v1, v6

    const/16 v6, -0x67

    aput v6, v1, v5

    const/16 v5, -0x9

    aput v5, v1, v4

    const/16 v4, 0x6221

    aput v4, v1, v3

    const/16 v3, -0x7dff

    aput v3, v1, v2

    const/16 v2, -0xf

    aput v2, v1, v0

    const/16 v0, 0x1e

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, 0x10

    const/16 v19, 0x11

    const/16 v20, 0x12

    const/16 v21, 0x13

    const/16 v22, 0x14

    const/16 v23, 0x15

    const/16 v24, 0x16

    const/16 v25, 0x17

    const/16 v26, 0x18

    const/16 v27, 0x19

    const/16 v28, 0x1a

    const/16 v29, 0x1b

    const/16 v30, 0x1c

    const/16 v31, 0x1d

    const/16 v32, -0x14

    aput v32, v0, v31

    const/16 v31, -0x2c00

    aput v31, v0, v30

    const/16 v30, -0x2c

    aput v30, v0, v29

    const/16 v29, -0x48c6

    aput v29, v0, v28

    const/16 v28, -0x49

    aput v28, v0, v27

    const/16 v27, -0x19

    aput v27, v0, v26

    const/16 v26, 0x2a47

    aput v26, v0, v25

    const/16 v25, -0x6bd6

    aput v25, v0, v24

    const/16 v24, -0x6c

    aput v24, v0, v23

    const/16 v23, -0x6481

    aput v23, v0, v22

    const/16 v22, -0x65

    aput v22, v0, v21

    const/16 v21, -0x25

    aput v21, v0, v20

    const/16 v20, -0x6c

    aput v20, v0, v19

    const/16 v19, -0x4291

    aput v19, v0, v18

    const/16 v18, -0x43

    aput v18, v0, v17

    const/16 v17, 0xd43

    aput v17, v0, v16

    const/16 v16, 0x5a0d

    aput v16, v0, v15

    const/16 v15, -0x67a6

    aput v15, v0, v14

    const/16 v14, -0x68

    aput v14, v0, v13

    const/16 v13, 0x2545

    aput v13, v0, v12

    const/16 v12, 0x1425

    aput v12, v0, v11

    const/16 v11, 0x4914

    aput v11, v0, v10

    const/16 v10, -0x45b7

    aput v10, v0, v9

    const/16 v9, -0x46

    aput v9, v0, v8

    const/16 v8, -0xb

    aput v8, v0, v7

    const/16 v7, -0x9

    aput v7, v0, v6

    const/16 v6, -0x67

    aput v6, v0, v5

    const/16 v5, 0x6240

    aput v5, v0, v4

    const/16 v4, -0x7d9e

    aput v4, v0, v3

    const/16 v3, -0x7e

    aput v3, v0, v2

    const/4 v2, 0x0

    :goto_e
    array-length v3, v0

    if-lt v2, v3, :cond_e

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_f
    array-length v3, v0

    if-lt v2, v3, :cond_f

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->SCANNING_TYPE_INFO_TEXT_RES_ID_KEY:Ljava/lang/String;

    const/16 v0, 0x18

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x4

    const/4 v6, 0x5

    const/4 v7, 0x6

    const/4 v8, 0x7

    const/16 v9, 0x8

    const/16 v10, 0x9

    const/16 v11, 0xa

    const/16 v12, 0xb

    const/16 v13, 0xc

    const/16 v14, 0xd

    const/16 v15, 0xe

    const/16 v16, 0xf

    const/16 v17, 0x10

    const/16 v18, 0x11

    const/16 v19, 0x12

    const/16 v20, 0x13

    const/16 v21, 0x14

    const/16 v22, 0x15

    const/16 v23, 0x16

    const/16 v24, 0x17

    const/16 v25, -0x43c6

    aput v25, v1, v24

    const/16 v24, -0x28

    aput v24, v1, v23

    const/16 v23, -0x7a

    aput v23, v1, v22

    const/16 v22, 0x6375

    aput v22, v1, v21

    const/16 v21, 0x7e10

    aput v21, v1, v20

    const/16 v20, -0x79e5

    aput v20, v1, v19

    const/16 v19, -0xc

    aput v19, v1, v18

    const/16 v18, -0x28

    aput v18, v1, v17

    const/16 v17, -0x29e7

    aput v17, v1, v16

    const/16 v16, -0x48

    aput v16, v1, v15

    const/4 v15, -0x4

    aput v15, v1, v14

    const/16 v14, -0x79

    aput v14, v1, v13

    const/16 v13, 0x5072

    aput v13, v1, v12

    const/16 v12, -0x51c1

    aput v12, v1, v11

    const/16 v11, -0x33

    aput v11, v1, v10

    const/16 v10, -0x56

    aput v10, v1, v9

    const/16 v9, 0x7354

    aput v9, v1, v8

    const/16 v8, 0x3811

    aput v8, v1, v7

    const/16 v7, 0x3a59

    aput v7, v1, v6

    const/16 v6, -0x7ab2

    aput v6, v1, v5

    const/16 v5, -0x26

    aput v5, v1, v4

    const/16 v4, 0x3430

    aput v4, v1, v3

    const/16 v3, 0x2541

    aput v3, v1, v2

    const/16 v2, 0xa56

    aput v2, v1, v0

    const/16 v0, 0x18

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, 0x10

    const/16 v19, 0x11

    const/16 v20, 0x12

    const/16 v21, 0x13

    const/16 v22, 0x14

    const/16 v23, 0x15

    const/16 v24, 0x16

    const/16 v25, 0x17

    const/16 v26, -0x43b7

    aput v26, v0, v25

    const/16 v25, -0x44

    aput v25, v0, v24

    const/16 v24, -0x11

    aput v24, v0, v23

    const/16 v23, 0x632a

    aput v23, v0, v22

    const/16 v22, 0x7e63

    aput v22, v0, v21

    const/16 v21, -0x7982

    aput v21, v0, v20

    const/16 v20, -0x7a

    aput v20, v0, v19

    const/16 v19, -0x79

    aput v19, v0, v18

    const/16 v18, -0x2993

    aput v18, v0, v17

    const/16 v17, -0x2a

    aput v17, v0, v16

    const/16 v16, -0x67

    aput v16, v0, v15

    const/16 v15, -0xd

    aput v15, v0, v14

    const/16 v14, 0x501c

    aput v14, v0, v13

    const/16 v13, -0x51b0

    aput v13, v0, v12

    const/16 v12, -0x52

    aput v12, v0, v11

    const/16 v11, -0xb

    aput v11, v0, v10

    const/16 v10, 0x7327

    aput v10, v0, v9

    const/16 v9, 0x3873

    aput v9, v0, v8

    const/16 v8, 0x3a38

    aput v8, v0, v7

    const/16 v7, -0x7ac6

    aput v7, v0, v6

    const/16 v6, -0x7b

    aput v6, v0, v5

    const/16 v5, 0x3452

    aput v5, v0, v4

    const/16 v4, 0x2534

    aput v4, v0, v3

    const/16 v3, 0xa25

    aput v3, v0, v2

    const/4 v2, 0x0

    :goto_10
    array-length v3, v0

    if-lt v2, v3, :cond_10

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_11
    array-length v3, v0

    if-lt v2, v3, :cond_11

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->SCANNING_SUB_TABS_INFO_RES_IDS_KEY:Ljava/lang/String;

    const/16 v0, 0x12

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x4

    const/4 v6, 0x5

    const/4 v7, 0x6

    const/4 v8, 0x7

    const/16 v9, 0x8

    const/16 v10, 0x9

    const/16 v11, 0xa

    const/16 v12, 0xb

    const/16 v13, 0xc

    const/16 v14, 0xd

    const/16 v15, 0xe

    const/16 v16, 0xf

    const/16 v17, 0x10

    const/16 v18, 0x11

    const/16 v19, -0x12

    aput v19, v1, v18

    const/16 v18, -0x60

    aput v18, v1, v17

    const/16 v17, -0x1d8

    aput v17, v1, v16

    const/16 v16, -0x73

    aput v16, v1, v15

    const/4 v15, -0x8

    aput v15, v1, v14

    const/16 v14, -0x14

    aput v14, v1, v13

    const/16 v13, -0x31

    aput v13, v1, v12

    const/16 v12, 0x6b64

    aput v12, v1, v11

    const/16 v11, 0x7713

    aput v11, v1, v10

    const/16 v10, -0x59ee

    aput v10, v1, v9

    const/16 v9, -0x2e

    aput v9, v1, v8

    const/16 v8, -0x25dc

    aput v8, v1, v7

    const/16 v7, -0x42

    aput v7, v1, v6

    const/16 v6, -0x5fce

    aput v6, v1, v5

    const/16 v5, -0x2e

    aput v5, v1, v4

    const/16 v4, -0x58dc

    aput v4, v1, v3

    const/16 v3, -0x3a

    aput v3, v1, v2

    const/16 v2, -0x29

    aput v2, v1, v0

    const/16 v0, 0x12

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, 0x10

    const/16 v19, 0x11

    const/16 v20, -0x76

    aput v20, v0, v19

    const/16 v19, -0x37

    aput v19, v0, v18

    const/16 v18, -0x189

    aput v18, v0, v17

    const/16 v17, -0x2

    aput v17, v0, v16

    const/16 v16, -0x63

    aput v16, v0, v15

    const/16 v15, -0x62

    aput v15, v0, v14

    const/16 v14, -0x70

    aput v14, v0, v13

    const/16 v13, 0x6b10

    aput v13, v0, v12

    const/16 v12, 0x776b

    aput v12, v0, v11

    const/16 v11, -0x5989

    aput v11, v0, v10

    const/16 v10, -0x5a

    aput v10, v0, v9

    const/16 v9, -0x2585

    aput v9, v0, v8

    const/16 v8, -0x26

    aput v8, v0, v7

    const/16 v7, -0x5fa9

    aput v7, v0, v6

    const/16 v6, -0x60

    aput v6, v0, v5

    const/16 v5, -0x58b3

    aput v5, v0, v4

    const/16 v4, -0x59

    aput v4, v0, v3

    const/16 v3, -0x59

    aput v3, v0, v2

    const/4 v2, 0x0

    :goto_12
    array-length v3, v0

    if-lt v2, v3, :cond_12

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_13
    array-length v3, v0

    if-lt v2, v3, :cond_13

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->PAIRED_TEXT_RES_ID_KEY:Ljava/lang/String;

    const/16 v0, 0x17

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x4

    const/4 v6, 0x5

    const/4 v7, 0x6

    const/4 v8, 0x7

    const/16 v9, 0x8

    const/16 v10, 0x9

    const/16 v11, 0xa

    const/16 v12, 0xb

    const/16 v13, 0xc

    const/16 v14, 0xd

    const/16 v15, 0xe

    const/16 v16, 0xf

    const/16 v17, 0x10

    const/16 v18, 0x11

    const/16 v19, 0x12

    const/16 v20, 0x13

    const/16 v21, 0x14

    const/16 v22, 0x15

    const/16 v23, 0x16

    const/16 v24, -0x5200

    aput v24, v1, v23

    const/16 v23, -0x39

    aput v23, v1, v22

    const/16 v22, -0x77fe

    aput v22, v1, v21

    const/16 v21, -0x5

    aput v21, v1, v20

    const/16 v20, 0x156c

    aput v20, v1, v19

    const/16 v19, -0x4699

    aput v19, v1, v18

    const/16 v18, -0x1a

    aput v18, v1, v17

    const/16 v17, -0x36

    aput v17, v1, v16

    const/16 v16, 0x1570

    aput v16, v1, v15

    const/16 v15, 0x6b70

    aput v15, v1, v14

    const/16 v14, 0x5c1f

    aput v14, v1, v13

    const/16 v13, 0x2003

    aput v13, v1, v12

    const/16 v12, 0x5d45

    aput v12, v1, v11

    const/16 v11, -0x5cd3

    aput v11, v1, v10

    const/16 v10, -0x26

    aput v10, v1, v9

    const/16 v9, -0x22

    aput v9, v1, v8

    const/16 v8, -0x3a6

    aput v8, v1, v7

    const/16 v7, -0x67

    aput v7, v1, v6

    const/16 v6, -0x4a

    aput v6, v1, v5

    const/16 v5, -0x98

    aput v5, v1, v4

    const/16 v4, -0x77

    aput v4, v1, v3

    const/16 v3, -0x6b3

    aput v3, v1, v2

    const/16 v2, -0x63

    aput v2, v1, v0

    const/16 v0, 0x17

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, 0x10

    const/16 v19, 0x11

    const/16 v20, 0x12

    const/16 v21, 0x13

    const/16 v22, 0x14

    const/16 v23, 0x15

    const/16 v24, 0x16

    const/16 v25, -0x519c

    aput v25, v0, v24

    const/16 v24, -0x52

    aput v24, v0, v23

    const/16 v23, -0x77a3

    aput v23, v0, v22

    const/16 v22, -0x78

    aput v22, v0, v21

    const/16 v21, 0x1509

    aput v21, v0, v20

    const/16 v20, -0x46eb

    aput v20, v0, v19

    const/16 v19, -0x47

    aput v19, v0, v18

    const/16 v18, -0x42

    aput v18, v0, v17

    const/16 v17, 0x1508

    aput v17, v0, v16

    const/16 v16, 0x6b15

    aput v16, v0, v15

    const/16 v15, 0x5c6b

    aput v15, v0, v14

    const/16 v14, 0x205c

    aput v14, v0, v13

    const/16 v13, 0x5d20

    aput v13, v0, v12

    const/16 v12, -0x5ca3

    aput v12, v0, v11

    const/16 v11, -0x5d

    aput v11, v0, v10

    const/16 v10, -0x56

    aput v10, v0, v9

    const/16 v9, -0x3fb

    aput v9, v0, v8

    const/4 v8, -0x4

    aput v8, v0, v7

    const/16 v7, -0x2b

    aput v7, v0, v6

    const/16 v6, -0xff

    aput v6, v0, v5

    const/4 v5, -0x1

    aput v5, v0, v4

    const/16 v4, -0x6d8

    aput v4, v0, v3

    const/4 v3, -0x7

    aput v3, v0, v2

    const/4 v2, 0x0

    :goto_14
    array-length v3, v0

    if-lt v2, v3, :cond_14

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_15
    array-length v3, v0

    if-lt v2, v3, :cond_15

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->DEVICE_TYPE_TEXT_RES_ID_KEY:Ljava/lang/String;

    const/16 v0, 0x15

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x4

    const/4 v6, 0x5

    const/4 v7, 0x6

    const/4 v8, 0x7

    const/16 v9, 0x8

    const/16 v10, 0x9

    const/16 v11, 0xa

    const/16 v12, 0xb

    const/16 v13, 0xc

    const/16 v14, 0xd

    const/16 v15, 0xe

    const/16 v16, 0xf

    const/16 v17, 0x10

    const/16 v18, 0x11

    const/16 v19, 0x12

    const/16 v20, 0x13

    const/16 v21, 0x14

    const/16 v22, 0x1d64

    aput v22, v1, v21

    const/16 v21, 0x2174

    aput v21, v1, v20

    const/16 v20, -0x2382

    aput v20, v1, v19

    const/16 v19, -0x51

    aput v19, v1, v18

    const/16 v18, -0x63ab

    aput v18, v1, v17

    const/16 v17, -0x12

    aput v17, v1, v16

    const/16 v16, -0x6

    aput v16, v1, v15

    const/16 v15, -0x2d

    aput v15, v1, v14

    const/16 v14, -0x75

    aput v14, v1, v13

    const/16 v13, -0x24

    aput v13, v1, v12

    const/16 v12, -0x23

    aput v12, v1, v11

    const/16 v11, 0x230e

    aput v11, v1, v10

    const/16 v10, 0x7746

    aput v10, v1, v9

    const/16 v9, 0x5014

    aput v9, v1, v8

    const/16 v8, 0x5e39

    aput v8, v1, v7

    const/16 v7, -0x7dd8

    aput v7, v1, v6

    const/16 v6, -0x19

    aput v6, v1, v5

    const/16 v5, -0x1bff

    aput v5, v1, v4

    const/16 v4, -0x45

    aput v4, v1, v3

    const/16 v3, -0x18

    aput v3, v1, v2

    const/16 v2, -0x53

    aput v2, v1, v0

    const/16 v0, 0x15

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, 0x10

    const/16 v19, 0x11

    const/16 v20, 0x12

    const/16 v21, 0x13

    const/16 v22, 0x14

    const/16 v23, 0x1d00

    aput v23, v0, v22

    const/16 v22, 0x211d

    aput v22, v0, v21

    const/16 v21, -0x23df

    aput v21, v0, v20

    const/16 v20, -0x24

    aput v20, v0, v19

    const/16 v19, -0x63d0

    aput v19, v0, v18

    const/16 v18, -0x64

    aput v18, v0, v17

    const/16 v17, -0x5b

    aput v17, v0, v16

    const/16 v16, -0x59

    aput v16, v0, v15

    const/16 v15, -0xd

    aput v15, v0, v14

    const/16 v14, -0x47

    aput v14, v0, v13

    const/16 v13, -0x57

    aput v13, v0, v12

    const/16 v12, 0x2351

    aput v12, v0, v11

    const/16 v11, 0x7723

    aput v11, v0, v10

    const/16 v10, 0x5077

    aput v10, v0, v9

    const/16 v9, 0x5e50

    aput v9, v0, v8

    const/16 v8, -0x7da2

    aput v8, v0, v7

    const/16 v7, -0x7e

    aput v7, v0, v6

    const/16 v6, -0x1b9b

    aput v6, v0, v5

    const/16 v5, -0x1c

    aput v5, v0, v4

    const/16 v4, -0x79

    aput v4, v0, v3

    const/16 v3, -0x3d

    aput v3, v0, v2

    const/4 v2, 0x0

    :goto_16
    array-length v3, v0

    if-lt v2, v3, :cond_16

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_17
    array-length v3, v0

    if-lt v2, v3, :cond_17

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->NO_DEVICES_TEXT_RES_ID_KEY:Ljava/lang/String;

    const/16 v0, 0x16

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x4

    const/4 v6, 0x5

    const/4 v7, 0x6

    const/4 v8, 0x7

    const/16 v9, 0x8

    const/16 v10, 0x9

    const/16 v11, 0xa

    const/16 v12, 0xb

    const/16 v13, 0xc

    const/16 v14, 0xd

    const/16 v15, 0xe

    const/16 v16, 0xf

    const/16 v17, 0x10

    const/16 v18, 0x11

    const/16 v19, 0x12

    const/16 v20, 0x13

    const/16 v21, 0x14

    const/16 v22, 0x15

    const/16 v23, 0x525f

    aput v23, v1, v22

    const/16 v22, -0x4aca

    aput v22, v1, v21

    const/16 v21, -0x24

    aput v21, v1, v20

    const/16 v20, -0x39

    aput v20, v1, v19

    const/16 v19, -0x63ee

    aput v19, v1, v18

    const/16 v18, -0xe

    aput v18, v1, v17

    const/16 v17, 0x108

    aput v17, v1, v16

    const/16 v16, -0x4d8d

    aput v16, v1, v15

    const/16 v15, -0x3a

    aput v15, v1, v14

    const/16 v14, -0x66f6

    aput v14, v1, v13

    const/16 v13, -0x3a

    aput v13, v1, v12

    const/16 v12, -0x6c

    aput v12, v1, v11

    const/16 v11, -0x60

    aput v11, v1, v10

    const/16 v10, -0x13b8

    aput v10, v1, v9

    const/16 v9, -0x68

    aput v9, v1, v8

    const/16 v8, -0x1e

    aput v8, v1, v7

    const/16 v7, -0xb

    aput v7, v1, v6

    const/16 v6, -0x24

    aput v6, v1, v5

    const/16 v5, 0x3616

    aput v5, v1, v4

    const/16 v4, 0x4e57

    aput v4, v1, v3

    const/16 v3, 0x2a2b

    aput v3, v1, v2

    const/16 v2, 0x6942

    aput v2, v1, v0

    const/16 v0, 0x16

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, 0x10

    const/16 v19, 0x11

    const/16 v20, 0x12

    const/16 v21, 0x13

    const/16 v22, 0x14

    const/16 v23, 0x15

    const/16 v24, 0x522c

    aput v24, v0, v23

    const/16 v23, -0x4aae

    aput v23, v0, v22

    const/16 v22, -0x4b

    aput v22, v0, v21

    const/16 v21, -0x68

    aput v21, v0, v20

    const/16 v20, -0x638b

    aput v20, v0, v19

    const/16 v19, -0x64

    aput v19, v0, v18

    const/16 v18, 0x161

    aput v18, v0, v17

    const/16 v17, -0x4dff

    aput v17, v0, v16

    const/16 v16, -0x4e

    aput v16, v0, v15

    const/16 v15, -0x6687

    aput v15, v0, v14

    const/16 v14, -0x67

    aput v14, v0, v13

    const/16 v13, -0x20

    aput v13, v0, v12

    const/16 v12, -0x28

    aput v12, v0, v11

    const/16 v11, -0x13d3

    aput v11, v0, v10

    const/16 v10, -0x14

    aput v10, v0, v9

    const/16 v9, -0x43

    aput v9, v0, v8

    const/16 v8, -0x79

    aput v8, v0, v7

    const/16 v7, -0x47

    aput v7, v0, v6

    const/16 v6, 0x3672

    aput v6, v0, v5

    const/16 v5, 0x4e36

    aput v5, v0, v4

    const/16 v4, 0x2a4e

    aput v4, v0, v3

    const/16 v3, 0x692a

    aput v3, v0, v2

    const/4 v2, 0x0

    :goto_18
    array-length v3, v0

    if-lt v2, v3, :cond_18

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_19
    array-length v3, v0

    if-lt v2, v3, :cond_19

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->HEADER_TEXT_RES_ID_KEYS:Ljava/lang/String;

    const/16 v0, 0x14

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x4

    const/4 v6, 0x5

    const/4 v7, 0x6

    const/4 v8, 0x7

    const/16 v9, 0x8

    const/16 v10, 0x9

    const/16 v11, 0xa

    const/16 v12, 0xb

    const/16 v13, 0xc

    const/16 v14, 0xd

    const/16 v15, 0xe

    const/16 v16, 0xf

    const/16 v17, 0x10

    const/16 v18, 0x11

    const/16 v19, 0x12

    const/16 v20, 0x13

    const/16 v21, -0x66

    aput v21, v1, v20

    const/16 v20, -0x32

    aput v20, v1, v19

    const/16 v19, 0x226

    aput v19, v1, v18

    const/16 v18, -0x4c94

    aput v18, v1, v17

    const/16 v17, -0x26

    aput v17, v1, v16

    const/16 v16, -0x72

    aput v16, v1, v15

    const/16 v15, 0x5753

    aput v15, v1, v14

    const/16 v14, 0x4124

    aput v14, v1, v13

    const/16 v13, -0x13e2

    aput v13, v1, v12

    const/16 v12, -0x68

    aput v12, v1, v11

    const/16 v11, -0x33a2

    aput v11, v1, v10

    const/16 v10, -0x57

    aput v10, v1, v9

    const/16 v9, -0x50

    aput v9, v1, v8

    const/16 v8, -0x5a

    aput v8, v1, v7

    const/16 v7, -0xf

    aput v7, v1, v6

    const/16 v6, 0x5e65

    aput v6, v1, v5

    const/16 v5, 0x4b3a

    aput v5, v1, v4

    const/16 v4, -0xcd6

    aput v4, v1, v3

    const/16 v3, -0x6a

    aput v3, v1, v2

    const/16 v2, 0x124d

    aput v2, v1, v0

    const/16 v0, 0x14

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, 0x10

    const/16 v19, 0x11

    const/16 v20, 0x12

    const/16 v21, 0x13

    const/16 v22, -0x2

    aput v22, v0, v21

    const/16 v21, -0x59

    aput v21, v0, v20

    const/16 v20, 0x241

    aput v20, v0, v19

    const/16 v19, -0x4cfe

    aput v19, v0, v18

    const/16 v18, -0x4d

    aput v18, v0, v17

    const/16 v17, -0x4

    aput v17, v0, v16

    const/16 v16, 0x5727

    aput v16, v0, v15

    const/16 v15, 0x4157

    aput v15, v0, v14

    const/16 v14, -0x13bf

    aput v14, v0, v13

    const/16 v13, -0x14

    aput v13, v0, v12

    const/16 v12, -0x33da

    aput v12, v0, v11

    const/16 v11, -0x34

    aput v11, v0, v10

    const/16 v10, -0x3c

    aput v10, v0, v9

    const/4 v9, -0x7

    aput v9, v0, v8

    const/16 v8, -0x7d

    aput v8, v0, v7

    const/16 v7, 0x5e00

    aput v7, v0, v6

    const/16 v6, 0x4b5e

    aput v6, v0, v5

    const/16 v5, -0xcb5

    aput v5, v0, v4

    const/16 v4, -0xd

    aput v4, v0, v3

    const/16 v3, 0x1225

    aput v3, v0, v2

    const/4 v2, 0x0

    :goto_1a
    array-length v3, v0

    if-lt v2, v3, :cond_1a

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_1b
    array-length v3, v0

    if-lt v2, v3, :cond_1b

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->HEADER_TEXT_ID:Ljava/lang/String;

    const/16 v0, 0x10

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x4

    const/4 v6, 0x5

    const/4 v7, 0x6

    const/4 v8, 0x7

    const/16 v9, 0x8

    const/16 v10, 0x9

    const/16 v11, 0xa

    const/16 v12, 0xb

    const/16 v13, 0xc

    const/16 v14, 0xd

    const/16 v15, 0xe

    const/16 v16, 0xf

    const/16 v17, -0x33

    aput v17, v1, v16

    const/16 v16, -0x34ce

    aput v16, v1, v15

    const/16 v15, -0x5e

    aput v15, v1, v14

    const/16 v14, -0x21

    aput v14, v1, v13

    const/16 v13, -0x64

    aput v13, v1, v12

    const/16 v12, 0x90d

    aput v12, v1, v11

    const/16 v11, -0x7894

    aput v11, v1, v10

    const/16 v10, -0xd

    aput v10, v1, v9

    const/16 v9, -0x6ea3

    aput v9, v1, v8

    const/4 v8, -0x2

    aput v8, v1, v7

    const/16 v7, -0x719b

    aput v7, v1, v6

    const/16 v6, -0x2f

    aput v6, v1, v5

    const/16 v5, -0x2094

    aput v5, v1, v4

    const/16 v4, -0x47

    aput v4, v1, v3

    const/16 v3, -0x52

    aput v3, v1, v2

    const/16 v2, -0x32

    aput v2, v1, v0

    const/16 v0, 0x10

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, -0x42

    aput v18, v0, v17

    const/16 v17, -0x34aa

    aput v17, v0, v16

    const/16 v16, -0x35

    aput v16, v0, v15

    const/16 v15, -0x80

    aput v15, v0, v14

    const/16 v14, -0x18

    aput v14, v0, v13

    const/16 v13, 0x963

    aput v13, v0, v12

    const/16 v12, -0x78f7

    aput v12, v0, v11

    const/16 v11, -0x79

    aput v11, v0, v10

    const/16 v10, -0x6ecd

    aput v10, v0, v9

    const/16 v9, -0x6f

    aput v9, v0, v8

    const/16 v8, -0x71fa

    aput v8, v0, v7

    const/16 v7, -0x72

    aput v7, v0, v6

    const/16 v6, -0x20fd

    aput v6, v0, v5

    const/16 v5, -0x21

    aput v5, v0, v4

    const/16 v4, -0x40

    aput v4, v0, v3

    const/16 v3, -0x59

    aput v3, v0, v2

    const/4 v2, 0x0

    :goto_1c
    array-length v3, v0

    if-lt v2, v3, :cond_1c

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_1d
    array-length v3, v0

    if-lt v2, v3, :cond_1d

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->SCANNING_TYPE_INFO_CONTENT_IDS:Ljava/lang/String;

    const/16 v0, 0x10

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x4

    const/4 v6, 0x5

    const/4 v7, 0x6

    const/4 v8, 0x7

    const/16 v9, 0x8

    const/16 v10, 0x9

    const/16 v11, 0xa

    const/16 v12, 0xb

    const/16 v13, 0xc

    const/16 v14, 0xd

    const/16 v15, 0xe

    const/16 v16, 0xf

    const/16 v17, -0x4a

    aput v17, v1, v16

    const/16 v16, -0x7fc5

    aput v16, v1, v15

    const/16 v15, -0x1b

    aput v15, v1, v14

    const/16 v14, 0x264a

    aput v14, v1, v13

    const/16 v13, 0x2748

    aput v13, v1, v12

    const/16 v12, 0xa48

    aput v12, v1, v11

    const/16 v11, -0x2297

    aput v11, v1, v10

    const/16 v10, -0x7e

    aput v10, v1, v9

    const/16 v9, -0x7ea8

    aput v9, v1, v8

    const/16 v8, -0x1d

    aput v8, v1, v7

    const/16 v7, 0x6d2b

    aput v7, v1, v6

    const/16 v6, -0x77e7

    aput v6, v1, v5

    const/16 v5, -0x29

    aput v5, v1, v4

    const/16 v4, -0x60

    aput v4, v1, v3

    const/16 v3, 0x6f21

    aput v3, v1, v2

    const/16 v2, -0x69e4

    aput v2, v1, v0

    const/16 v0, 0x10

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, -0x3e

    aput v18, v0, v17

    const/16 v17, -0x7fab

    aput v17, v0, v16

    const/16 v16, -0x80

    aput v16, v0, v15

    const/16 v15, 0x263e

    aput v15, v0, v14

    const/16 v14, 0x2726

    aput v14, v0, v13

    const/16 v13, 0xa27

    aput v13, v0, v12

    const/16 v12, -0x22f6

    aput v12, v0, v11

    const/16 v11, -0x23

    aput v11, v0, v10

    const/16 v10, -0x7ed5

    aput v10, v0, v9

    const/16 v9, -0x7f

    aput v9, v0, v8

    const/16 v8, 0x6d4a

    aput v8, v0, v7

    const/16 v7, -0x7793

    aput v7, v0, v6

    const/16 v6, -0x78

    aput v6, v0, v5

    const/16 v5, -0x3e

    aput v5, v0, v4

    const/16 v4, 0x6f54

    aput v4, v0, v3

    const/16 v3, -0x6991

    aput v3, v0, v2

    const/4 v2, 0x0

    :goto_1e
    array-length v3, v0

    if-lt v2, v3, :cond_1e

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_1f
    array-length v3, v0

    if-lt v2, v3, :cond_1f

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->SCANNING_SUB_TABS_INFO:Ljava/lang/String;

    const/16 v0, 0xc

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x4

    const/4 v6, 0x5

    const/4 v7, 0x6

    const/4 v8, 0x7

    const/16 v9, 0x8

    const/16 v10, 0x9

    const/16 v11, 0xa

    const/16 v12, 0xb

    const/16 v13, -0x57

    aput v13, v1, v12

    const/16 v12, 0x337

    aput v12, v1, v11

    const/16 v11, 0x466

    aput v11, v1, v10

    const/16 v10, -0x3590

    aput v10, v1, v9

    const/16 v9, -0x5c

    aput v9, v1, v8

    const/16 v8, -0x68

    aput v8, v1, v7

    const/16 v7, -0x6ce0

    aput v7, v1, v6

    const/16 v6, -0x34

    aput v6, v1, v5

    const/16 v5, -0x43d3

    aput v5, v1, v4

    const/16 v4, -0x26

    aput v4, v1, v3

    const/16 v3, -0x43af

    aput v3, v1, v2

    const/16 v2, -0x2b

    aput v2, v1, v0

    const/16 v0, 0xc

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, -0x23

    aput v14, v0, v13

    const/16 v13, 0x359

    aput v13, v0, v12

    const/16 v12, 0x403

    aput v12, v0, v11

    const/16 v11, -0x35fc

    aput v11, v0, v10

    const/16 v10, -0x36

    aput v10, v0, v9

    const/16 v9, -0x9

    aput v9, v0, v8

    const/16 v8, -0x6cbd

    aput v8, v0, v7

    const/16 v7, -0x6d

    aput v7, v0, v6

    const/16 v6, -0x43be

    aput v6, v0, v5

    const/16 v5, -0x44

    aput v5, v0, v4

    const/16 v4, -0x43c1

    aput v4, v0, v3

    const/16 v3, -0x44

    aput v3, v0, v2

    const/4 v2, 0x0

    :goto_20
    array-length v3, v0

    if-lt v2, v3, :cond_20

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_21
    array-length v3, v0

    if-lt v2, v3, :cond_21

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->SCANNING_TYPE_INFO_CONTENT:Ljava/lang/String;

    const/4 v0, 0x4

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/16 v5, 0x426

    aput v5, v1, v4

    const/16 v4, -0x4b9e

    aput v4, v1, v3

    const/16 v3, -0x26

    aput v3, v1, v2

    const/16 v2, -0x35

    aput v2, v1, v0

    const/4 v0, 0x4

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/16 v6, 0x449

    aput v6, v0, v5

    const/16 v5, -0x4bfc

    aput v5, v0, v4

    const/16 v4, -0x4c

    aput v4, v0, v3

    const/16 v3, -0x5e

    aput v3, v0, v2

    const/4 v2, 0x0

    :goto_22
    array-length v3, v0

    if-lt v2, v3, :cond_22

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_23
    array-length v3, v0

    if-lt v2, v3, :cond_23

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->SCANNING_TYPE_INFO_TEXT:Ljava/lang/String;

    const/16 v0, 0xe

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x4

    const/4 v6, 0x5

    const/4 v7, 0x6

    const/4 v8, 0x7

    const/16 v9, 0x8

    const/16 v10, 0x9

    const/16 v11, 0xa

    const/16 v12, 0xb

    const/16 v13, 0xc

    const/16 v14, 0xd

    const/16 v15, -0x5a

    aput v15, v1, v14

    const/16 v14, -0x1cd3

    aput v14, v1, v13

    const/16 v13, -0x7a

    aput v13, v1, v12

    const/16 v12, -0xf

    aput v12, v1, v11

    const/16 v11, -0x80

    aput v11, v1, v10

    const/16 v10, -0x1bb9

    aput v10, v1, v9

    const/16 v9, -0x63

    aput v9, v1, v8

    const/16 v8, -0xf

    aput v8, v1, v7

    const/16 v7, -0xb

    aput v7, v1, v6

    const/16 v6, -0x3b

    aput v6, v1, v5

    const/16 v5, -0x4a9f

    aput v5, v1, v4

    const/16 v4, -0x3d

    aput v4, v1, v3

    const/16 v3, -0x6d9a

    aput v3, v1, v2

    const/16 v2, -0x2a

    aput v2, v1, v0

    const/16 v0, 0xe

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, -0x2e

    aput v16, v0, v15

    const/16 v15, -0x1cab

    aput v15, v0, v14

    const/16 v14, -0x1d

    aput v14, v0, v13

    const/16 v13, -0x5b

    aput v13, v0, v12

    const/16 v12, -0x1b

    aput v12, v0, v11

    const/16 v11, -0x1bc9

    aput v11, v0, v10

    const/16 v10, -0x1c

    aput v10, v0, v9

    const/16 v9, -0x5b

    aput v9, v0, v8

    const/16 v8, -0x70

    aput v8, v0, v7

    const/16 v7, -0x5a

    aput v7, v0, v6

    const/16 v6, -0x4af8

    aput v6, v0, v5

    const/16 v5, -0x4b

    aput v5, v0, v4

    const/16 v4, -0x6dfd

    aput v4, v0, v3

    const/16 v3, -0x6e

    aput v3, v0, v2

    const/4 v2, 0x0

    :goto_24
    array-length v3, v0

    if-lt v2, v3, :cond_24

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_25
    array-length v3, v0

    if-lt v2, v3, :cond_25

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->DEVICE_TYPE_TEXT_KEY:Ljava/lang/String;

    const/16 v0, 0xe

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x4

    const/4 v6, 0x5

    const/4 v7, 0x6

    const/4 v8, 0x7

    const/16 v9, 0x8

    const/16 v10, 0x9

    const/16 v11, 0xa

    const/16 v12, 0xb

    const/16 v13, 0xc

    const/16 v14, 0xd

    const/16 v15, 0x1d15

    aput v15, v1, v14

    const/16 v14, 0x173

    aput v14, v1, v13

    const/16 v13, 0x4268

    aput v13, v1, v12

    const/16 v12, -0x29d0

    aput v12, v1, v11

    const/16 v11, -0x5e

    aput v11, v1, v10

    const/16 v10, -0x5e

    aput v10, v1, v9

    const/16 v9, 0x2b64

    aput v9, v1, v8

    const/16 v8, 0x4f48

    aput v8, v1, v7

    const/16 v7, 0x7026

    aput v7, v1, v6

    const/16 v6, 0x3a06

    aput v6, v1, v5

    const/16 v5, 0x755f

    aput v5, v1, v4

    const/16 v4, -0x41cf

    aput v4, v1, v3

    const/16 v3, -0x2f

    aput v3, v1, v2

    const/16 v2, 0x3542

    aput v2, v1, v0

    const/16 v0, 0xe

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0x1d72

    aput v16, v0, v15

    const/16 v15, 0x11d

    aput v15, v0, v14

    const/16 v14, 0x4201

    aput v14, v0, v13

    const/16 v13, -0x29be

    aput v13, v0, v12

    const/16 v12, -0x2a

    aput v12, v0, v11

    const/16 v11, -0xf

    aput v11, v0, v10

    const/16 v10, 0x2b01

    aput v10, v0, v9

    const/16 v9, 0x4f2b

    aput v9, v0, v8

    const/16 v8, 0x704f

    aput v8, v0, v7

    const/16 v7, 0x3a70

    aput v7, v0, v6

    const/16 v6, 0x753a

    aput v6, v0, v5

    const/16 v5, -0x418b

    aput v5, v0, v4

    const/16 v4, -0x42

    aput v4, v0, v3

    const/16 v3, 0x350c

    aput v3, v0, v2

    const/4 v2, 0x0

    :goto_26
    array-length v3, v0

    if-lt v2, v3, :cond_26

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_27
    array-length v3, v0

    if-lt v2, v3, :cond_27

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->NO_DEVICES_TEXT_KEY:Ljava/lang/String;

    const/16 v0, 0xa

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x4

    const/4 v6, 0x5

    const/4 v7, 0x6

    const/4 v8, 0x7

    const/16 v9, 0x8

    const/16 v10, 0x9

    const/16 v11, -0x5d97

    aput v11, v1, v10

    const/16 v10, -0x26

    aput v10, v1, v9

    const/16 v9, 0x7646

    aput v9, v1, v8

    const/16 v8, -0x3ade

    aput v8, v1, v7

    const/16 v7, -0x5f

    aput v7, v1, v6

    const/16 v6, -0x1bf8

    aput v6, v1, v5

    const/16 v5, -0x6a

    aput v5, v1, v4

    const/16 v4, -0x66

    aput v4, v1, v3

    const/16 v3, -0x55da

    aput v3, v1, v2

    const/4 v2, -0x6

    aput v2, v1, v0

    const/16 v0, 0xa

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, -0x5de3

    aput v12, v0, v11

    const/16 v11, -0x5e

    aput v11, v0, v10

    const/16 v10, 0x7623

    aput v10, v0, v9

    const/16 v9, -0x3a8a

    aput v9, v0, v8

    const/16 v8, -0x3b

    aput v8, v0, v7

    const/16 v7, -0x1b93

    aput v7, v0, v6

    const/16 v6, -0x1c

    aput v6, v0, v5

    const/16 v5, -0xd

    aput v5, v0, v4

    const/16 v4, -0x55b9

    aput v4, v0, v3

    const/16 v3, -0x56

    aput v3, v0, v2

    const/4 v2, 0x0

    :goto_28
    array-length v3, v0

    if-lt v2, v3, :cond_28

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_29
    array-length v3, v0

    if-lt v2, v3, :cond_29

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->PAIRED_TEXT_KEY:Ljava/lang/String;

    const/16 v0, 0x10

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x4

    const/4 v6, 0x5

    const/4 v7, 0x6

    const/4 v8, 0x7

    const/16 v9, 0x8

    const/16 v10, 0x9

    const/16 v11, 0xa

    const/16 v12, 0xb

    const/16 v13, 0xc

    const/16 v14, 0xd

    const/16 v15, 0xe

    const/16 v16, 0xf

    const/16 v17, -0x49

    aput v17, v1, v16

    const/16 v16, -0x3e

    aput v16, v1, v15

    const/16 v15, 0x3773

    aput v15, v1, v14

    const/16 v14, 0x6f63

    aput v14, v1, v13

    const/16 v13, 0x4e16

    aput v13, v1, v12

    const/16 v12, -0x29c6

    aput v12, v1, v11

    const/16 v11, -0x41

    aput v11, v1, v10

    const/16 v10, -0x35

    aput v10, v1, v9

    const/16 v9, -0x408b

    aput v9, v1, v8

    const/16 v8, -0x35

    aput v8, v1, v7

    const/16 v7, -0x1d

    aput v7, v1, v6

    const/16 v6, -0x6eb6

    aput v6, v1, v5

    const/4 v5, -0x1

    aput v5, v1, v4

    const/16 v4, 0x1102

    aput v4, v1, v3

    const/16 v3, -0x7e82

    aput v3, v1, v2

    const/16 v2, -0x3e

    aput v2, v1, v0

    const/16 v0, 0x10

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, -0x2e

    aput v18, v0, v17

    const/16 v17, -0x4e

    aput v17, v0, v16

    const/16 v16, 0x370a

    aput v16, v0, v15

    const/16 v15, 0x6f37

    aput v15, v0, v14

    const/16 v14, 0x4e6f

    aput v14, v0, v13

    const/16 v13, -0x29b2

    aput v13, v0, v12

    const/16 v12, -0x2a

    aput v12, v0, v11

    const/16 v11, -0x43

    aput v11, v0, v10

    const/16 v10, -0x40e4

    aput v10, v0, v9

    const/16 v9, -0x41

    aput v9, v0, v8

    const/16 v8, -0x80

    aput v8, v0, v7

    const/16 v7, -0x6ed1

    aput v7, v0, v6

    const/16 v6, -0x6f

    aput v6, v0, v5

    const/16 v5, 0x116c

    aput v5, v0, v4

    const/16 v4, -0x7eef

    aput v4, v0, v3

    const/16 v3, -0x7f

    aput v3, v0, v2

    const/4 v2, 0x0

    :goto_2a
    array-length v3, v0

    if-lt v2, v3, :cond_2a

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_2b
    array-length v3, v0

    if-lt v2, v3, :cond_2b

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->CONNECTIVITY_TYPE_KEY:Ljava/lang/String;

    const/16 v0, 0xb

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x4

    const/4 v6, 0x5

    const/4 v7, 0x6

    const/4 v8, 0x7

    const/16 v9, 0x8

    const/16 v10, 0x9

    const/16 v11, 0xa

    const/16 v12, -0x39f3

    aput v12, v1, v11

    const/16 v11, -0x5d

    aput v11, v1, v10

    const/16 v10, 0x7c6d

    aput v10, v1, v9

    const/16 v9, -0x61fb

    aput v9, v1, v8

    const/16 v8, -0x36

    aput v8, v1, v7

    const/16 v7, -0x6c

    aput v7, v1, v6

    const/16 v6, 0x257

    aput v6, v1, v5

    const/16 v5, -0x5b95

    aput v5, v1, v4

    const/16 v4, -0x2e

    aput v4, v1, v3

    const/16 v3, -0x32d4

    aput v3, v1, v2

    const/16 v2, -0x77

    aput v2, v1, v0

    const/16 v0, 0xb

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, -0x3982

    aput v13, v0, v12

    const/16 v12, -0x3a

    aput v12, v0, v11

    const/16 v11, 0x7c1d

    aput v11, v0, v10

    const/16 v10, -0x6184

    aput v10, v0, v9

    const/16 v9, -0x62

    aput v9, v0, v8

    const/16 v8, -0xf

    aput v8, v0, v7

    const/16 v7, 0x234

    aput v7, v0, v6

    const/16 v6, -0x5bfe

    aput v6, v0, v5

    const/16 v5, -0x5c

    aput v5, v0, v4

    const/16 v4, -0x32b7

    aput v4, v0, v3

    const/16 v3, -0x33

    aput v3, v0, v2

    const/4 v2, 0x0

    :goto_2c
    array-length v3, v0

    if-lt v2, v3, :cond_2c

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_2d
    array-length v3, v0

    if-lt v2, v3, :cond_2d

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->DEVICE_TYPES_KEY:Ljava/lang/String;

    const/16 v0, 0x8

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x4

    const/4 v6, 0x5

    const/4 v7, 0x6

    const/4 v8, 0x7

    const/16 v9, -0x39

    aput v9, v1, v8

    const/16 v8, 0x6668

    aput v8, v1, v7

    const/16 v7, -0x4ee1

    aput v7, v1, v6

    const/16 v6, -0x1b

    aput v6, v1, v5

    const/16 v5, -0x12

    aput v5, v1, v4

    const/16 v4, -0x43

    aput v4, v1, v3

    const/16 v3, -0x33

    aput v3, v1, v2

    const/16 v2, -0x7c

    aput v2, v1, v0

    const/16 v0, 0x8

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, -0x5e

    aput v10, v0, v9

    const/16 v9, 0x6618

    aput v9, v0, v8

    const/16 v8, -0x4e9a

    aput v8, v0, v7

    const/16 v7, -0x4f

    aput v7, v0, v6

    const/16 v6, -0x71

    aput v6, v0, v5

    const/16 v5, -0x37

    aput v5, v0, v4

    const/16 v4, -0x54

    aput v4, v0, v3

    const/16 v3, -0x40

    aput v3, v0, v2

    const/4 v2, 0x0

    :goto_2e
    array-length v3, v0

    if-lt v2, v3, :cond_2e

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_2f
    array-length v3, v0

    if-lt v2, v3, :cond_2f

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->DATA_TYPE_KEY:Ljava/lang/String;

    const/16 v0, 0x1c

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x4

    const/4 v6, 0x5

    const/4 v7, 0x6

    const/4 v8, 0x7

    const/16 v9, 0x8

    const/16 v10, 0x9

    const/16 v11, 0xa

    const/16 v12, 0xb

    const/16 v13, 0xc

    const/16 v14, 0xd

    const/16 v15, 0xe

    const/16 v16, 0xf

    const/16 v17, 0x10

    const/16 v18, 0x11

    const/16 v19, 0x12

    const/16 v20, 0x13

    const/16 v21, 0x14

    const/16 v22, 0x15

    const/16 v23, 0x16

    const/16 v24, 0x17

    const/16 v25, 0x18

    const/16 v26, 0x19

    const/16 v27, 0x1a

    const/16 v28, 0x1b

    const/16 v29, -0x76

    aput v29, v1, v28

    const/16 v28, 0x2d56

    aput v28, v1, v27

    const/16 v27, 0x4044

    aput v27, v1, v26

    const/16 v26, 0x5a34

    aput v26, v1, v25

    const/16 v25, 0x6c2e

    aput v25, v1, v24

    const/16 v24, 0x6d09

    aput v24, v1, v23

    const/16 v23, -0x75e2

    aput v23, v1, v22

    const/16 v22, -0x2b

    aput v22, v1, v21

    const/16 v21, -0x5

    aput v21, v1, v20

    const/16 v20, -0x8

    aput v20, v1, v19

    const/16 v19, -0x7fbf

    aput v19, v1, v18

    const/16 v18, -0x17

    aput v18, v1, v17

    const/16 v17, -0x52

    aput v17, v1, v16

    const/16 v16, -0x3e

    aput v16, v1, v15

    const/16 v15, -0x7b

    aput v15, v1, v14

    const/16 v14, -0x2a

    aput v14, v1, v13

    const/16 v13, -0x66

    aput v13, v1, v12

    const/16 v12, -0x6e

    aput v12, v1, v11

    const/4 v11, -0x6

    aput v11, v1, v10

    const/16 v10, -0x30

    aput v10, v1, v9

    const/16 v9, 0x3357

    aput v9, v1, v8

    const/16 v8, 0x5447

    aput v8, v1, v7

    const/16 v7, -0x39c8

    aput v7, v1, v6

    const/16 v6, -0x59

    aput v6, v1, v5

    const/16 v5, -0x43b2

    aput v5, v1, v4

    const/16 v4, -0x2c

    aput v4, v1, v3

    const/16 v3, 0x5e55

    aput v3, v1, v2

    const/16 v2, 0x5e2d

    aput v2, v1, v0

    const/16 v0, 0x1c

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, 0x10

    const/16 v19, 0x11

    const/16 v20, 0x12

    const/16 v21, 0x13

    const/16 v22, 0x14

    const/16 v23, 0x15

    const/16 v24, 0x16

    const/16 v25, 0x17

    const/16 v26, 0x18

    const/16 v27, 0x19

    const/16 v28, 0x1a

    const/16 v29, 0x1b

    const/16 v30, -0x13

    aput v30, v0, v29

    const/16 v29, 0x2d38

    aput v29, v0, v28

    const/16 v28, 0x402d

    aput v28, v0, v27

    const/16 v27, 0x5a40

    aput v27, v0, v26

    const/16 v26, 0x6c5a

    aput v26, v0, v25

    const/16 v25, 0x6d6c

    aput v25, v0, v24

    const/16 v24, -0x7593

    aput v24, v0, v23

    const/16 v23, -0x76

    aput v23, v0, v22

    const/16 v22, -0x6b

    aput v22, v0, v21

    const/16 v21, -0x69

    aput v21, v0, v20

    const/16 v20, -0x7fde

    aput v20, v0, v19

    const/16 v19, -0x80

    aput v19, v0, v18

    const/16 v18, -0xf

    aput v18, v0, v17

    const/16 v17, -0x4a

    aput v17, v0, v16

    const/16 v16, -0xa

    aput v16, v0, v15

    const/16 v15, -0x41

    aput v15, v0, v14

    const/16 v14, -0xa

    aput v14, v0, v13

    const/16 v13, -0x33

    aput v13, v0, v12

    const/16 v12, -0x6e

    aput v12, v0, v11

    const/16 v11, -0x71

    aput v11, v0, v10

    const/16 v10, 0x333f

    aput v10, v0, v9

    const/16 v9, 0x5433

    aput v9, v0, v8

    const/16 v8, -0x39ac

    aput v8, v0, v7

    const/16 v7, -0x3a

    aput v7, v0, v6

    const/16 v6, -0x43d5

    aput v6, v0, v5

    const/16 v5, -0x44

    aput v5, v0, v4

    const/16 v4, 0x5e0a

    aput v4, v0, v3

    const/16 v3, 0x5e5e

    aput v3, v0, v2

    const/4 v2, 0x0

    :goto_30
    array-length v3, v0

    if-lt v2, v3, :cond_30

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_31
    array-length v3, v0

    if-lt v2, v3, :cond_31

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->SETTINGS_RESOURCE:Ljava/lang/String;

    const/16 v0, 0x12

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x4

    const/4 v6, 0x5

    const/4 v7, 0x6

    const/4 v8, 0x7

    const/16 v9, 0x8

    const/16 v10, 0x9

    const/16 v11, 0xa

    const/16 v12, 0xb

    const/16 v13, 0xc

    const/16 v14, 0xd

    const/16 v15, 0xe

    const/16 v16, 0xf

    const/16 v17, 0x10

    const/16 v18, 0x11

    const/16 v19, -0x46

    aput v19, v1, v18

    const/16 v18, -0x52

    aput v18, v1, v17

    const/16 v17, 0x1257

    aput v17, v1, v16

    const/16 v16, -0x6c85

    aput v16, v1, v15

    const/16 v15, -0x1b

    aput v15, v1, v14

    const/16 v14, -0x73

    aput v14, v1, v13

    const/16 v13, 0x104b

    aput v13, v1, v12

    const/16 v12, -0x75b1

    aput v12, v1, v11

    const/4 v11, -0x2

    aput v11, v1, v10

    const/4 v10, -0x5

    aput v10, v1, v9

    const/16 v9, 0x2a0f

    aput v9, v1, v8

    const/16 v8, 0x7c58

    aput v8, v1, v7

    const/16 v7, 0x2d13

    aput v7, v1, v6

    const/16 v6, 0x684b

    aput v6, v1, v5

    const/16 v5, 0x3637

    aput v5, v1, v4

    const/16 v4, -0x32b1

    aput v4, v1, v3

    const/16 v3, -0x58

    aput v3, v1, v2

    const/16 v2, -0x53a9

    aput v2, v1, v0

    const/16 v0, 0x12

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, 0x10

    const/16 v19, 0x11

    const/16 v20, -0x37

    aput v20, v0, v19

    const/16 v19, -0x35

    aput v19, v0, v18

    const/16 v18, 0x1234

    aput v18, v0, v17

    const/16 v17, -0x6cee

    aput v17, v0, v16

    const/16 v16, -0x6d

    aput v16, v0, v15

    const/16 v15, -0x18

    aput v15, v0, v14

    const/16 v14, 0x102f

    aput v14, v0, v13

    const/16 v13, -0x75f0

    aput v13, v0, v12

    const/16 v12, -0x76

    aput v12, v0, v11

    const/16 v11, -0x67

    aput v11, v0, v10

    const/16 v10, 0x2a50

    aput v10, v0, v9

    const/16 v9, 0x7c2a

    aput v9, v0, v8

    const/16 v8, 0x2d7c

    aput v8, v0, v7

    const/16 v7, 0x682d

    aput v7, v0, v6

    const/16 v6, 0x3668

    aput v6, v0, v5

    const/16 v5, -0x32ca

    aput v5, v0, v4

    const/16 v4, -0x33

    aput v4, v0, v3

    const/16 v3, -0x53c4

    aput v3, v0, v2

    const/4 v2, 0x0

    :goto_32
    array-length v3, v0

    if-lt v2, v3, :cond_32

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_33
    array-length v3, v0

    if-lt v2, v3, :cond_33

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->KEY_FOR_BT_DEVICES_MAP:Ljava/lang/String;

    const/16 v0, 0xc

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x4

    const/4 v6, 0x5

    const/4 v7, 0x6

    const/4 v8, 0x7

    const/16 v9, 0x8

    const/16 v10, 0x9

    const/16 v11, 0xa

    const/16 v12, 0xb

    const/16 v13, -0x57f6

    aput v13, v1, v12

    const/16 v12, -0x25

    aput v12, v1, v11

    const/16 v11, -0x3fee

    aput v11, v1, v10

    const/16 v10, -0x54

    aput v10, v1, v9

    const/16 v9, -0x78f4

    aput v9, v1, v8

    const/16 v8, -0xb

    aput v8, v1, v7

    const/16 v7, 0x393b

    aput v7, v1, v6

    const/16 v6, -0x6fa1

    aput v6, v1, v5

    const/16 v5, -0x31

    aput v5, v1, v4

    const/16 v4, -0x2f

    aput v4, v1, v3

    const/16 v3, -0x2c

    aput v3, v1, v2

    const/16 v2, -0x6ce2

    aput v2, v1, v0

    const/16 v0, 0xc

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, -0x5782

    aput v14, v0, v13

    const/16 v13, -0x58

    aput v13, v0, v12

    const/16 v12, -0x3f85

    aput v12, v0, v11

    const/16 v11, -0x40

    aput v11, v0, v10

    const/16 v10, -0x78ad

    aput v10, v0, v9

    const/16 v9, -0x79

    aput v9, v0, v8

    const/16 v8, 0x3954

    aput v8, v0, v7

    const/16 v7, -0x6fc7

    aput v7, v0, v6

    const/16 v6, -0x70

    aput v6, v0, v5

    const/16 v5, -0x58

    aput v5, v0, v4

    const/16 v4, -0x4f

    aput v4, v0, v3

    const/16 v3, -0x6c8b

    aput v3, v0, v2

    const/4 v2, 0x0

    :goto_34
    array-length v3, v0

    if-lt v2, v3, :cond_34

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_35
    array-length v3, v0

    if-lt v2, v3, :cond_35

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->KEY_FOR_BT_DEVICES_LIST:Ljava/lang/String;

    const/16 v0, 0xa

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x4

    const/4 v6, 0x5

    const/4 v7, 0x6

    const/4 v8, 0x7

    const/16 v9, 0x8

    const/16 v10, 0x9

    const/16 v11, -0x18

    aput v11, v1, v10

    const/16 v10, -0x56f4

    aput v10, v1, v9

    const/16 v9, -0x40

    aput v9, v1, v8

    const/16 v8, -0x2da4

    aput v8, v1, v7

    const/16 v7, -0x60

    aput v7, v1, v6

    const/16 v6, 0x538

    aput v6, v1, v5

    const/16 v5, 0x5a56

    aput v5, v1, v4

    const/16 v4, 0x7f38

    aput v4, v1, v3

    const/16 v3, -0x75ea

    aput v3, v1, v2

    const/16 v2, -0x1a

    aput v2, v1, v0

    const/16 v0, 0xa

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, -0x73

    aput v12, v0, v11

    const/16 v11, -0x5691

    aput v11, v0, v10

    const/16 v10, -0x57

    aput v10, v0, v9

    const/16 v9, -0x2dd6

    aput v9, v0, v8

    const/16 v8, -0x2e

    aput v8, v0, v7

    const/16 v7, 0x55d

    aput v7, v0, v6

    const/16 v6, 0x5a05

    aput v6, v0, v5

    const/16 v5, 0x7f5a

    aput v5, v0, v4

    const/16 v4, -0x7581

    aput v4, v0, v3

    const/16 v3, -0x76

    aput v3, v0, v2

    const/4 v2, 0x0

    :goto_36
    array-length v3, v0

    if-lt v2, v3, :cond_36

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_37
    array-length v3, v0

    if-lt v2, v3, :cond_37

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->TAG:Ljava/lang/String;

    const/16 v0, 0x66

    sput v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->SHOW_WEIGHTSCALE_ALERT_BOX:I

    const/16 v0, 0x68

    sput v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->HIDE_NO_DEVICES:I

    const/16 v0, 0x69

    sput v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->START_RECIEVE_DATA_AGAIN:I

    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mUpdateScanStatus:Z

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mDeviceResMap:Ljava/util/HashMap;

    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mDeviceResMap:Ljava/util/HashMap;

    const/16 v1, 0x2712

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$drawable;->accessory_temperature_weight:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mDeviceResMap:Ljava/util/HashMap;

    const/16 v1, 0x2714

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$drawable;->accessory_temperature_bg:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mDeviceResMap:Ljava/util/HashMap;

    const/16 v1, 0x2713

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$drawable;->accessory_temperature_bp:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mDeviceResMap:Ljava/util/HashMap;

    const/16 v1, 0x2719

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$drawable;->s_health_temperature_acc_sband:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mDeviceResMap:Ljava/util/HashMap;

    const/16 v1, 0x271d

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$drawable;->s_health_accessory_big_galaxy_gear:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mDeviceResMap:Ljava/util/HashMap;

    const/16 v1, 0x2724

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$drawable;->s_health_temperature_samsung_gear:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mDeviceResMap:Ljava/util/HashMap;

    const/16 v1, 0x2728

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$drawable;->s_health_temperature_samsung_gear:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mDeviceResMap:Ljava/util/HashMap;

    const/16 v1, 0x2726

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$drawable;->s_health_temperature_samsung_gear2:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mDeviceResMap:Ljava/util/HashMap;

    const/16 v1, 0x272e

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$drawable;->s_health_temperature_acc_b2:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mDeviceResMap:Ljava/util/HashMap;

    const/16 v1, 0x2727

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$drawable;->accessory_product_samsung_activitytracker:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mDeviceResMap:Ljava/util/HashMap;

    const/16 v1, 0x2723

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$drawable;->s_health_temperature_samsung_gearfit:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mDeviceResMap:Ljava/util/HashMap;

    const/16 v1, 0x2718

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$drawable;->accessory_temperature_hr:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void

    :cond_0
    aget v3, v0, v2

    aget v4, v1, v2

    xor-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    :cond_1
    aget v3, v1, v2

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1

    :cond_2
    aget v3, v0, v2

    aget v4, v1, v2

    xor-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_2

    :cond_3
    aget v3, v1, v2

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_3

    :cond_4
    aget v3, v0, v2

    aget v4, v1, v2

    xor-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_4

    :cond_5
    aget v3, v1, v2

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_5

    :cond_6
    aget v3, v0, v2

    aget v4, v1, v2

    xor-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_6

    :cond_7
    aget v3, v1, v2

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_7

    :cond_8
    aget v3, v0, v2

    aget v4, v1, v2

    xor-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_8

    :cond_9
    aget v3, v1, v2

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_9

    :cond_a
    aget v3, v0, v2

    aget v4, v1, v2

    xor-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_a

    :cond_b
    aget v3, v1, v2

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_b

    :cond_c
    aget v3, v0, v2

    aget v4, v1, v2

    xor-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_c

    :cond_d
    aget v3, v1, v2

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_d

    :cond_e
    aget v3, v0, v2

    aget v4, v1, v2

    xor-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_e

    :cond_f
    aget v3, v1, v2

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_f

    :cond_10
    aget v3, v0, v2

    aget v4, v1, v2

    xor-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_10

    :cond_11
    aget v3, v1, v2

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_11

    :cond_12
    aget v3, v0, v2

    aget v4, v1, v2

    xor-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_12

    :cond_13
    aget v3, v1, v2

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_13

    :cond_14
    aget v3, v0, v2

    aget v4, v1, v2

    xor-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_14

    :cond_15
    aget v3, v1, v2

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_15

    :cond_16
    aget v3, v0, v2

    aget v4, v1, v2

    xor-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_16

    :cond_17
    aget v3, v1, v2

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_17

    :cond_18
    aget v3, v0, v2

    aget v4, v1, v2

    xor-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_18

    :cond_19
    aget v3, v1, v2

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_19

    :cond_1a
    aget v3, v0, v2

    aget v4, v1, v2

    xor-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1a

    :cond_1b
    aget v3, v1, v2

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1b

    :cond_1c
    aget v3, v0, v2

    aget v4, v1, v2

    xor-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1c

    :cond_1d
    aget v3, v1, v2

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1d

    :cond_1e
    aget v3, v0, v2

    aget v4, v1, v2

    xor-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1e

    :cond_1f
    aget v3, v1, v2

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1f

    :cond_20
    aget v3, v0, v2

    aget v4, v1, v2

    xor-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_20

    :cond_21
    aget v3, v1, v2

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_21

    :cond_22
    aget v3, v0, v2

    aget v4, v1, v2

    xor-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_22

    :cond_23
    aget v3, v1, v2

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_23

    :cond_24
    aget v3, v0, v2

    aget v4, v1, v2

    xor-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_24

    :cond_25
    aget v3, v1, v2

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_25

    :cond_26
    aget v3, v0, v2

    aget v4, v1, v2

    xor-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_26

    :cond_27
    aget v3, v1, v2

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_27

    :cond_28
    aget v3, v0, v2

    aget v4, v1, v2

    xor-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_28

    :cond_29
    aget v3, v1, v2

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_29

    :cond_2a
    aget v3, v0, v2

    aget v4, v1, v2

    xor-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_2a

    :cond_2b
    aget v3, v1, v2

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_2b

    :cond_2c
    aget v3, v0, v2

    aget v4, v1, v2

    xor-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_2c

    :cond_2d
    aget v3, v1, v2

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_2d

    :cond_2e
    aget v3, v0, v2

    aget v4, v1, v2

    xor-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_2e

    :cond_2f
    aget v3, v1, v2

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_2f

    :cond_30
    aget v3, v0, v2

    aget v4, v1, v2

    xor-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_30

    :cond_31
    aget v3, v1, v2

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_31

    :cond_32
    aget v3, v0, v2

    aget v4, v1, v2

    xor-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_32

    :cond_33
    aget v3, v1, v2

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_33

    :cond_34
    aget v3, v0, v2

    aget v4, v1, v2

    xor-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_34

    :cond_35
    aget v3, v1, v2

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_35

    :cond_36
    aget v3, v0, v2

    aget v4, v1, v2

    xor-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_36

    :cond_37
    aget v3, v1, v2

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_37
.end method

.method public constructor <init>()V
    .locals 14

    const/4 v13, 0x3

    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    const/4 v3, 0x0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;-><init>()V

    const/4 v0, 0x6

    new-array v1, v0, [I

    const/4 v0, 0x4

    const/4 v2, 0x5

    const/16 v4, -0x9

    aput v4, v1, v2

    const/16 v2, -0x63

    aput v2, v1, v0

    const/16 v0, -0x16de

    aput v0, v1, v13

    const/16 v0, -0x79

    aput v0, v1, v12

    const/16 v0, -0x76

    aput v0, v1, v11

    const/16 v0, -0x27

    aput v0, v1, v3

    const/4 v0, 0x6

    new-array v0, v0, [I

    const/4 v2, 0x4

    const/4 v4, 0x5

    const/16 v5, -0x6e

    aput v5, v0, v4

    const/16 v4, -0x10

    aput v4, v0, v2

    const/16 v2, -0x16bd

    aput v2, v0, v13

    const/16 v2, -0x17

    aput v2, v0, v12

    const/16 v2, -0x11

    aput v2, v0, v11

    const/16 v2, -0x75

    aput v2, v0, v3

    move v2, v3

    :goto_0
    array-length v4, v0

    if-lt v2, v4, :cond_0

    array-length v0, v1

    new-array v0, v0, [C

    move v2, v3

    :goto_1
    array-length v4, v0

    if-lt v2, v4, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->SETTINGS_RENAME_TAG:Ljava/lang/String;

    const/4 v0, 0x6

    new-array v1, v0, [I

    const/4 v0, 0x4

    const/4 v2, 0x5

    const/16 v4, -0x27e7

    aput v4, v1, v2

    const/16 v2, -0x4f

    aput v2, v1, v0

    const/16 v0, -0x3ada

    aput v0, v1, v13

    const/16 v0, -0x4b

    aput v0, v1, v12

    const/16 v0, -0x14

    aput v0, v1, v11

    const/16 v0, -0x77

    aput v0, v1, v3

    const/4 v0, 0x6

    new-array v0, v0, [I

    const/4 v2, 0x4

    const/4 v4, 0x5

    const/16 v5, -0x2795

    aput v5, v0, v4

    const/16 v4, -0x28

    aput v4, v0, v2

    const/16 v2, -0x3ab9

    aput v2, v0, v13

    const/16 v2, -0x3b

    aput v2, v0, v12

    const/16 v2, -0x7e

    aput v2, v0, v11

    const/16 v2, -0x24

    aput v2, v0, v3

    move v2, v3

    :goto_2
    array-length v4, v0

    if-lt v2, v4, :cond_2

    array-length v0, v1

    new-array v0, v0, [C

    move v2, v3

    :goto_3
    array-length v4, v0

    if-lt v2, v4, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->SETTINGS_UNPAIR_TAG:Ljava/lang/String;

    const/4 v0, 0x7

    new-array v1, v0, [I

    const/4 v0, 0x4

    const/4 v2, 0x5

    const/4 v4, 0x6

    const/16 v5, 0x681d

    aput v5, v1, v4

    const/16 v4, 0x1e0b

    aput v4, v1, v2

    const/16 v2, -0x6a85

    aput v2, v1, v0

    const/4 v0, -0x5

    aput v0, v1, v13

    const/16 v0, -0x4f

    aput v0, v1, v12

    const/16 v0, 0x4b69

    aput v0, v1, v11

    const/16 v0, -0x69d8

    aput v0, v1, v3

    const/4 v0, 0x7

    new-array v0, v0, [I

    const/4 v2, 0x4

    const/4 v4, 0x5

    const/4 v5, 0x6

    const/16 v6, 0x6869

    aput v6, v0, v5

    const/16 v5, 0x1e68

    aput v5, v0, v4

    const/16 v4, -0x6ae2

    aput v4, v0, v2

    const/16 v2, -0x6b

    aput v2, v0, v13

    const/16 v2, -0x21

    aput v2, v0, v12

    const/16 v2, 0x4b06

    aput v2, v0, v11

    const/16 v2, -0x69b5

    aput v2, v0, v3

    move v2, v3

    :goto_4
    array-length v4, v0

    if-lt v2, v4, :cond_4

    array-length v0, v1

    new-array v0, v0, [C

    move v2, v3

    :goto_5
    array-length v4, v0

    if-lt v2, v4, :cond_5

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->SETTINGS_CONNECT_TAG:Ljava/lang/String;

    const/16 v0, 0xa

    new-array v1, v0, [I

    const/4 v0, 0x4

    const/4 v2, 0x5

    const/4 v4, 0x6

    const/4 v5, 0x7

    const/16 v6, 0x8

    const/16 v7, 0x9

    const/16 v8, -0x64

    aput v8, v1, v7

    const/16 v7, -0x23

    aput v7, v1, v6

    const/16 v6, -0x33e5

    aput v6, v1, v5

    const/16 v5, -0x5e

    aput v5, v1, v4

    const/16 v4, -0x7a

    aput v4, v1, v2

    const/16 v2, -0x50d4

    aput v2, v1, v0

    const/16 v0, -0x34

    aput v0, v1, v13

    const/16 v0, 0x4c3c

    aput v0, v1, v12

    const/16 v0, 0x6525

    aput v0, v1, v11

    const/16 v0, 0x3a01

    aput v0, v1, v3

    const/16 v0, 0xa

    new-array v0, v0, [I

    const/4 v2, 0x4

    const/4 v4, 0x5

    const/4 v5, 0x6

    const/4 v6, 0x7

    const/16 v7, 0x8

    const/16 v8, 0x9

    const/16 v9, -0x18

    aput v9, v0, v8

    const/16 v8, -0x42

    aput v8, v0, v7

    const/16 v7, -0x3382

    aput v7, v0, v6

    const/16 v6, -0x34

    aput v6, v0, v5

    const/16 v5, -0x18

    aput v5, v0, v4

    const/16 v4, -0x50bd

    aput v4, v0, v2

    const/16 v2, -0x51

    aput v2, v0, v13

    const/16 v2, 0x4c4f

    aput v2, v0, v12

    const/16 v2, 0x654c

    aput v2, v0, v11

    const/16 v2, 0x3a65

    aput v2, v0, v3

    move v2, v3

    :goto_6
    array-length v4, v0

    if-lt v2, v4, :cond_6

    array-length v0, v1

    new-array v0, v0, [C

    move v2, v3

    :goto_7
    array-length v4, v0

    if-lt v2, v4, :cond_7

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->SETTINGS_DISCONNECT_TAG:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->noDeviceStringResId:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->deviceTypeTextResId:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mPairedTextResId:I

    iput-object v10, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mSubTabsContentResIds:[I

    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mScanningInfoTextResId:I

    iput-boolean v3, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mInfoContent:Z

    iput-boolean v3, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->isBluetoothTurningON:Z

    iput-boolean v3, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->showCompanionDevices:Z

    iput-boolean v3, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mExerciseScanningInstance:Z

    iput-object v10, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->syncProgressDlg:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    iput-object v10, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->progressDlg:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    iput-object v10, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->deviceIDRename:Ljava/lang/String;

    iput-object v10, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->connectedHRM:Ljava/lang/String;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mScannedDevicesList:Ljava/util/ArrayList;

    iput-boolean v3, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->isReScanNeeded:Z

    iput-object v10, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mDeviceTypeText:Ljava/lang/String;

    iput-object v10, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mPairedText:Ljava/lang/String;

    iput-object v10, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->noDeviceString:Ljava/lang/String;

    iput-boolean v3, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->isScanning:Z

    iput-boolean v3, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->isPairingNeeded:Z

    iput-object v10, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->pairDeviceId:Ljava/lang/String;

    iput-object v10, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->selectedDeviceId:Ljava/lang/String;

    iput-boolean v3, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->refreshPairedList:Z

    iput-object v10, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->scanningInfoText:Ljava/lang/String;

    iput-boolean v3, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->checkIsHrmSwitchReq:Z

    iput-boolean v3, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->disconnectToastReq:Z

    iput-boolean v3, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->connectToastReq:Z

    iput-boolean v11, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->IsAutoScanRequired:Z

    iput-boolean v3, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->IsScreenLocked:Z

    iput-object v10, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->wearableContentObserver:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$WearableStatusObserver;

    iput-boolean v3, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->isFragmetRunning:Z

    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$1;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->handler:Landroid/os/Handler;

    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$2;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$3;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mScanningFragmentScanListener:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$ScanningFragmentScanListener;

    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$4;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mSensorConnectionListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;

    return-void

    :cond_0
    aget v4, v0, v2

    aget v5, v1, v2

    xor-int/2addr v4, v5

    aput v4, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    :cond_1
    aget v4, v1, v2

    int-to-char v4, v4

    aput-char v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1

    :cond_2
    aget v4, v0, v2

    aget v5, v1, v2

    xor-int/2addr v4, v5

    aput v4, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_2

    :cond_3
    aget v4, v1, v2

    int-to-char v4, v4

    aput-char v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_3

    :cond_4
    aget v4, v0, v2

    aget v5, v1, v2

    xor-int/2addr v4, v5

    aput v4, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_4

    :cond_5
    aget v4, v1, v2

    int-to-char v4, v4

    aput-char v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_5

    :cond_6
    aget v4, v0, v2

    aget v5, v1, v2

    xor-int/2addr v4, v5

    aput v4, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_6

    :cond_7
    aget v4, v1, v2

    int-to-char v4, v4

    aput-char v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_7
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Landroid/widget/LinearLayout;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mPairedListContainer:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Ljava/util/concurrent/ConcurrentHashMap;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mPairedDeviceMap:Ljava/util/concurrent/ConcurrentHashMap;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Landroid/widget/FrameLayout;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->nodevices:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Landroid/widget/LinearLayout;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mScannedlist:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)I
    .locals 1

    iget v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->noDeviceStringResId:I

    return v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->noDeviceString:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->dialog:Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;

    return-object v0
.end method

.method static synthetic access$1402(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;)Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->dialog:Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;

    return-object p1
.end method

.method static synthetic access$1500(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->toggleScanState()V

    return-void
.end method

.method static synthetic access$1600(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mScannedDevicesList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Ljava/util/concurrent/ConcurrentHashMap;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->deviceMap:Ljava/util/concurrent/ConcurrentHashMap;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->stopSearch()V

    return-void
.end method

.method static synthetic access$1900(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->IsAutoScanRequired:Z

    return v0
.end method

.method static synthetic access$1902(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->IsAutoScanRequired:Z

    return p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->disconnectToastReq:Z

    return v0
.end method

.method static synthetic access$2002(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->disconnectToastReq:Z

    return p1
.end method

.method static synthetic access$2100(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->showToast(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$2200(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->unpairedSensorDeviceDetails:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    return-object v0
.end method

.method static synthetic access$2202(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;)Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->unpairedSensorDeviceDetails:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    return-object p1
.end method

.method static synthetic access$2300(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->IsJoining:Z

    return v0
.end method

.method static synthetic access$2302(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->IsJoining:Z

    return p1
.end method

.method static synthetic access$2400(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->dismissSyncDialog()V

    return-void
.end method

.method static synthetic access$2500(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->showLoadingPopUp(Z)V

    return-void
.end method

.method static synthetic access$2600(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$2700(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mScanningFragmentController:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;

    return-object v0
.end method

.method static synthetic access$2702(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;)Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mScanningFragmentController:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;

    return-object p1
.end method

.method static synthetic access$2900(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->isScanning:Z

    return v0
.end method

.method static synthetic access$2902(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->isScanning:Z

    return p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mIsNoSupportScan:Z

    return v0
.end method

.method static synthetic access$3000(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->isBluetoothTurningON:Z

    return v0
.end method

.method static synthetic access$3002(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->isBluetoothTurningON:Z

    return p1
.end method

.method static synthetic access$3100(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)I
    .locals 1

    iget v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mDataType:I

    return v0
.end method

.method static synthetic access$3200(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->scanbutton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->startDiscoveringSensorDevices()V

    return-void
.end method

.method static synthetic access$3400(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->isPairingNeeded:Z

    return v0
.end method

.method static synthetic access$3402(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->isPairingNeeded:Z

    return p1
.end method

.method static synthetic access$3500(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->pairDeviceId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3502(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->pairDeviceId:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$3600(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->actionFoundScannedDevice(Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;)V

    return-void
.end method

.method static synthetic access$3700(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->selectedDeviceId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3702(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->selectedDeviceId:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$3800(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->stopDiscoveringSensorDevices()V

    return-void
.end method

.method static synthetic access$3900(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->isReScanNeeded:Z

    return v0
.end method

.method static synthetic access$3902(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->isReScanNeeded:Z

    return p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)[I
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mSubTabsContentResIds:[I

    return-object v0
.end method

.method static synthetic access$4000(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->joinWithDevice(Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;)V

    return-void
.end method

.method static synthetic access$4100(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->actionFinishedScan()V

    return-void
.end method

.method static synthetic access$4200(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)I
    .locals 1

    iget v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->contentId:I

    return v0
.end method

.method static synthetic access$4300(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->headerText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$4400(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->setScanningViewsVisibility(Z)V

    return-void
.end method

.method static synthetic access$4500(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)I
    .locals 1

    iget v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mScanningInfoTextResId:I

    return v0
.end method

.method static synthetic access$4600(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->scanningInfoText:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$4700(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Ljava/lang/String;Z)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->leaveDevice(Ljava/lang/String;Z)V

    return-void
.end method

.method static synthetic access$4800(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Landroid/widget/ProgressBar;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->spinner:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$4900(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Landroid/widget/RelativeLayout;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->scanningLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Lcom/sec/android/app/shealth/framework/ui/base/PairOfDeviceIdAndName;Z)Landroid/view/View;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getView(Lcom/sec/android/app/shealth/framework/ui/base/PairOfDeviceIdAndName;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$5000(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Lcom/sec/android/app/shealth/framework/ui/base/PairOfDeviceIdAndName;)Landroid/view/View;
    .locals 1

    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getScannedView(Lcom/sec/android/app/shealth/framework/ui/base/PairOfDeviceIdAndName;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$5100(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->checkIsHrmSwitchReq:Z

    return v0
.end method

.method static synthetic access$5102(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->checkIsHrmSwitchReq:Z

    return p1
.end method

.method static synthetic access$5200(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->connectedHRM:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$5202(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->connectedHRM:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$5300(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->connectToastReq:Z

    return v0
.end method

.method static synthetic access$5302(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->connectToastReq:Z

    return p1
.end method

.method static synthetic access$5400(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Ljava/lang/String;Z)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->leaveDeviceWithOutRefresh(Ljava/lang/String;Z)V

    return-void
.end method

.method static synthetic access$5500(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->progressDlg:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    return-object v0
.end method

.method static synthetic access$5502(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;)Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->progressDlg:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    return-object p1
.end method

.method static synthetic access$5600(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->isFragmetRunning:Z

    return v0
.end method

.method static synthetic access$5700(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->IsScreenLocked:Z

    return v0
.end method

.method static synthetic access$5800(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mDevicename:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$5802(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mDevicename:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$5900(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->syncProgressDlg:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    return-object v0
.end method

.method static synthetic access$5902(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;)Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->syncProgressDlg:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    return-object p1
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Landroid/widget/LinearLayout;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mPairedDevicesLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mInformationView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mExerciseScanningInstance:Z

    return v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->nodevicesText:Landroid/widget/TextView;

    return-object v0
.end method

.method private actionFinishedScan()V
    .locals 42

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->isFragmetRunning:Z

    if-nez v1, :cond_4

    :goto_0
    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/4 v12, -0x2

    aput v12, v2, v11

    const/16 v11, -0x5f

    aput v11, v2, v10

    const/16 v10, -0x2792

    aput v10, v2, v9

    const/16 v9, -0x52

    aput v9, v2, v8

    const/16 v8, -0x78e3

    aput v8, v2, v7

    const/16 v7, -0x1e

    aput v7, v2, v6

    const/16 v6, -0x4d

    aput v6, v2, v5

    const/16 v5, -0x57e3

    aput v5, v2, v4

    const/16 v4, -0x3f

    aput v4, v2, v3

    const/16 v3, -0x428b

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, -0x65

    aput v13, v1, v12

    const/16 v12, -0x3e

    aput v12, v1, v11

    const/16 v11, -0x27f9

    aput v11, v1, v10

    const/16 v10, -0x28

    aput v10, v1, v9

    const/16 v9, -0x7891

    aput v9, v1, v8

    const/16 v8, -0x79

    aput v8, v1, v7

    const/16 v7, -0x20

    aput v7, v1, v6

    const/16 v6, -0x5781

    aput v6, v1, v5

    const/16 v5, -0x58

    aput v5, v1, v4

    const/16 v4, -0x42e7

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_1
    array-length v4, v1

    if-lt v3, v4, :cond_0

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_2
    array-length v4, v1

    if-lt v3, v4, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x24

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, 0x1d

    const/16 v34, 0x1e

    const/16 v35, 0x1f

    const/16 v36, 0x20

    const/16 v37, 0x21

    const/16 v38, 0x22

    const/16 v39, 0x23

    const/16 v40, 0x45e

    aput v40, v2, v39

    const/16 v39, 0x6439

    aput v39, v2, v38

    const/16 v38, -0x32bc

    aput v38, v2, v37

    const/16 v37, -0x56

    aput v37, v2, v36

    const/16 v36, 0x6c17

    aput v36, v2, v35

    const/16 v35, 0x3205

    aput v35, v2, v34

    const/16 v34, -0x11a4

    aput v34, v2, v33

    const/16 v33, -0x80

    aput v33, v2, v32

    const/16 v32, -0x6

    aput v32, v2, v31

    const/16 v31, -0x3bef

    aput v31, v2, v30

    const/16 v30, -0x50

    aput v30, v2, v29

    const/16 v29, 0x3935

    aput v29, v2, v28

    const/16 v28, 0x5f54

    aput v28, v2, v27

    const/16 v27, 0x5038

    aput v27, v2, v26

    const/16 v26, -0x3dcf

    aput v26, v2, v25

    const/16 v25, -0x50

    aput v25, v2, v24

    const/16 v24, -0x6fe

    aput v24, v2, v23

    const/16 v23, -0x76

    aput v23, v2, v22

    const/16 v22, -0x25ae

    aput v22, v2, v21

    const/16 v21, -0x6

    aput v21, v2, v20

    const/16 v20, -0x51

    aput v20, v2, v19

    const/16 v19, -0x3ff0

    aput v19, v2, v18

    const/16 v18, -0x20

    aput v18, v2, v17

    const/16 v17, -0x80

    aput v17, v2, v16

    const/16 v16, -0x71

    aput v16, v2, v15

    const/16 v15, 0x7317

    aput v15, v2, v14

    const/16 v14, -0x77ff

    aput v14, v2, v13

    const/16 v13, -0x13

    aput v13, v2, v12

    const/16 v12, -0x27a4

    aput v12, v2, v11

    const/4 v11, -0x8

    aput v11, v2, v10

    const/16 v10, 0x2705

    aput v10, v2, v9

    const/16 v9, -0x7fab

    aput v9, v2, v8

    const/16 v8, -0xb

    aput v8, v2, v7

    const/16 v7, 0x773d

    aput v7, v2, v6

    const/16 v6, -0x69ee

    aput v6, v2, v3

    const/16 v3, -0x3c

    aput v3, v2, v1

    const/16 v1, 0x24

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, 0x11

    const/16 v23, 0x12

    const/16 v24, 0x13

    const/16 v25, 0x14

    const/16 v26, 0x15

    const/16 v27, 0x16

    const/16 v28, 0x17

    const/16 v29, 0x18

    const/16 v30, 0x19

    const/16 v31, 0x1a

    const/16 v32, 0x1b

    const/16 v33, 0x1c

    const/16 v34, 0x1d

    const/16 v35, 0x1e

    const/16 v36, 0x1f

    const/16 v37, 0x20

    const/16 v38, 0x21

    const/16 v39, 0x22

    const/16 v40, 0x23

    const/16 v41, 0x47e

    aput v41, v1, v40

    const/16 v40, 0x6404

    aput v40, v1, v39

    const/16 v39, -0x329c

    aput v39, v1, v38

    const/16 v38, -0x33

    aput v38, v1, v37

    const/16 v37, 0x6c79

    aput v37, v1, v36

    const/16 v36, 0x326c

    aput v36, v1, v35

    const/16 v35, -0x11ce

    aput v35, v1, v34

    const/16 v34, -0x12

    aput v34, v1, v33

    const/16 v33, -0x71

    aput v33, v1, v32

    const/16 v32, -0x3bbd

    aput v32, v1, v31

    const/16 v31, -0x3c

    aput v31, v1, v30

    const/16 v30, 0x3950

    aput v30, v1, v29

    const/16 v29, 0x5f39

    aput v29, v1, v28

    const/16 v28, 0x505f

    aput v28, v1, v27

    const/16 v27, -0x3db0

    aput v27, v1, v26

    const/16 v26, -0x3e

    aput v26, v1, v25

    const/16 v25, -0x6bc

    aput v25, v1, v24

    const/16 v24, -0x7

    aput v24, v1, v23

    const/16 v23, -0x25c5

    aput v23, v1, v22

    const/16 v22, -0x26

    aput v22, v1, v21

    const/16 v21, -0x24

    aput v21, v1, v20

    const/16 v20, -0x3f8f

    aput v20, v1, v19

    const/16 v19, -0x40

    aput v19, v1, v18

    const/16 v18, -0x5f

    aput v18, v1, v17

    const/16 v17, -0x52

    aput v17, v1, v16

    const/16 v16, 0x7372

    aput v16, v1, v15

    const/16 v15, -0x778d

    aput v15, v1, v14

    const/16 v14, -0x78

    aput v14, v1, v13

    const/16 v13, -0x27cc

    aput v13, v1, v12

    const/16 v12, -0x28

    aput v12, v1, v11

    const/16 v11, 0x276b

    aput v11, v1, v10

    const/16 v10, -0x7fd9

    aput v10, v1, v9

    const/16 v9, -0x80

    aput v9, v1, v8

    const/16 v8, 0x7749

    aput v8, v1, v7

    const/16 v7, -0x6989

    aput v7, v1, v6

    const/16 v6, -0x6a

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_3
    array-length v6, v1

    if-lt v3, v6, :cond_2

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_4
    array-length v6, v1

    if-lt v3, v6, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->isFragmetRunning:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :goto_5
    return-void

    :cond_0
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_1
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    :cond_2
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_3
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$22;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$22;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)V

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_5

    :catch_0
    move-exception v1

    goto/16 :goto_0
.end method

.method private actionFoundScannedDevice(Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;)V
    .locals 29

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    :try_start_0
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    new-instance v3, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$14;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v3, v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$14;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;)V

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    :goto_0
    return-void

    :catch_0
    move-exception v2

    const/16 v2, 0xa

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, -0x70d3

    aput v13, v3, v12

    const/16 v12, -0x14

    aput v12, v3, v11

    const/16 v11, -0x7a

    aput v11, v3, v10

    const/16 v10, 0x1821

    aput v10, v3, v9

    const/16 v9, 0x1c6a

    aput v9, v3, v8

    const/16 v8, 0x1579

    aput v8, v3, v7

    const/16 v7, 0x6c46

    aput v7, v3, v6

    const/16 v6, -0xbf2

    aput v6, v3, v5

    const/16 v5, -0x63

    aput v5, v3, v4

    const/16 v4, -0x2487

    aput v4, v3, v2

    const/16 v2, 0xa

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, -0x70b8

    aput v14, v2, v13

    const/16 v13, -0x71

    aput v13, v2, v12

    const/16 v12, -0x11

    aput v12, v2, v11

    const/16 v11, 0x1857

    aput v11, v2, v10

    const/16 v10, 0x1c18

    aput v10, v2, v9

    const/16 v9, 0x151c

    aput v9, v2, v8

    const/16 v8, 0x6c15

    aput v8, v2, v7

    const/16 v7, -0xb94

    aput v7, v2, v6

    const/16 v6, -0xc

    aput v6, v2, v5

    const/16 v5, -0x24eb

    aput v5, v2, v4

    const/4 v4, 0x0

    :goto_1
    array-length v5, v2

    if-lt v4, v5, :cond_0

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_2
    array-length v5, v2

    if-lt v4, v5, :cond_1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v5

    const/16 v2, 0x17

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, -0xc

    aput v27, v3, v26

    const/16 v26, -0x55

    aput v26, v3, v25

    const/16 v25, -0x51a5

    aput v25, v3, v24

    const/16 v24, -0x3e

    aput v24, v3, v23

    const/16 v23, -0x58ad

    aput v23, v3, v22

    const/16 v22, -0x37

    aput v22, v3, v21

    const/16 v21, -0x21

    aput v21, v3, v20

    const/16 v20, 0x7367

    aput v20, v3, v19

    const/16 v19, -0x77e6

    aput v19, v3, v18

    const/16 v18, -0x58

    aput v18, v3, v17

    const/16 v17, -0x25

    aput v17, v3, v16

    const/16 v16, -0x7b

    aput v16, v3, v15

    const/16 v15, -0x16

    aput v15, v3, v14

    const/16 v14, -0x76

    aput v14, v3, v13

    const/16 v13, -0x58

    aput v13, v3, v12

    const/16 v12, -0x36

    aput v12, v3, v11

    const/16 v11, -0x29

    aput v11, v3, v10

    const/16 v10, -0x1c

    aput v10, v3, v9

    const/16 v9, -0x75d7

    aput v9, v3, v8

    const/16 v8, -0x35

    aput v8, v3, v7

    const/16 v7, 0x501b

    aput v7, v3, v6

    const/16 v6, -0x63cb

    aput v6, v3, v4

    const/4 v4, -0x5

    aput v4, v3, v2

    const/16 v2, 0x17

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, 0x11

    const/16 v23, 0x12

    const/16 v24, 0x13

    const/16 v25, 0x14

    const/16 v26, 0x15

    const/16 v27, 0x16

    const/16 v28, -0x2b

    aput v28, v2, v27

    const/16 v27, -0x76

    aput v27, v2, v26

    const/16 v26, -0x51c9

    aput v26, v2, v25

    const/16 v25, -0x52

    aput v25, v2, v24

    const/16 v24, -0x58da

    aput v24, v2, v23

    const/16 v23, -0x59

    aput v23, v2, v22

    const/16 v22, -0x1

    aput v22, v2, v21

    const/16 v21, 0x7314

    aput v21, v2, v20

    const/16 v20, -0x778d

    aput v20, v2, v19

    const/16 v19, -0x78

    aput v19, v2, v18

    const/16 v18, -0xe

    aput v18, v2, v17

    const/16 v17, -0x53

    aput v17, v2, v16

    const/16 v16, -0x6d

    aput v16, v2, v15

    const/4 v15, -0x2

    aput v15, v2, v14

    const/16 v14, -0x3f

    aput v14, v2, v13

    const/16 v13, -0x44

    aput v13, v2, v12

    const/16 v12, -0x42

    aput v12, v2, v11

    const/16 v11, -0x70

    aput v11, v2, v10

    const/16 v10, -0x75b6

    aput v10, v2, v9

    const/16 v9, -0x76

    aput v9, v2, v8

    const/16 v8, 0x506f

    aput v8, v2, v7

    const/16 v7, -0x63b0

    aput v7, v2, v6

    const/16 v6, -0x64

    aput v6, v2, v4

    const/4 v4, 0x0

    :goto_3
    array-length v6, v2

    if-lt v4, v6, :cond_2

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_4
    array-length v6, v2

    if-lt v4, v6, :cond_3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-static {v5, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_0
    aget v5, v2, v4

    aget v6, v3, v4

    xor-int/2addr v5, v6

    aput v5, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_1

    :cond_1
    aget v5, v3, v4

    int-to-char v5, v5

    aput-char v5, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_2

    :cond_2
    aget v6, v2, v4

    aget v7, v3, v4

    xor-int/2addr v6, v7

    aput v6, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    :cond_3
    aget v6, v3, v4

    int-to-char v6, v6

    aput-char v6, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_4
.end method

.method private dismissSyncDialog()V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->syncProgressDlg:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->syncProgressDlg:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->syncProgressDlg:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->syncProgressDlg:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private getScannedView(Lcom/sec/android/app/shealth/framework/ui/base/PairOfDeviceIdAndName;)Landroid/view/View;
    .locals 21

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    :try_start_0
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->isAdded()Z

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    const/4 v2, 0x0

    :goto_1
    return-object v2

    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    const/16 v2, 0xf

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, -0x68

    aput v19, v3, v18

    const/16 v18, -0x23

    aput v18, v3, v17

    const/16 v17, 0x7c1c

    aput v17, v3, v16

    const/16 v16, -0x5fe3

    aput v16, v3, v15

    const/16 v15, -0x34

    aput v15, v3, v14

    const/16 v14, 0xe66

    aput v14, v3, v13

    const/16 v13, -0x60a0

    aput v13, v3, v12

    const/16 v12, -0xa

    aput v12, v3, v11

    const/16 v11, -0x47

    aput v11, v3, v10

    const/16 v10, -0x44

    aput v10, v3, v9

    const/16 v9, -0x3ba

    aput v9, v3, v8

    const/16 v8, -0x6d

    aput v8, v3, v7

    const/16 v7, 0x2363

    aput v7, v3, v6

    const/16 v6, 0x5c42

    aput v6, v3, v4

    const/16 v4, -0xad0

    aput v4, v3, v2

    const/16 v2, 0xf

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, -0x16

    aput v20, v2, v19

    const/16 v19, -0x48

    aput v19, v2, v18

    const/16 v18, 0x7c68

    aput v18, v2, v17

    const/16 v17, -0x5f84

    aput v17, v2, v16

    const/16 v16, -0x60

    aput v16, v2, v15

    const/16 v15, 0xe00

    aput v15, v2, v14

    const/16 v14, -0x60f2

    aput v14, v2, v13

    const/16 v13, -0x61

    aput v13, v2, v12

    const/16 v12, -0x1a

    aput v12, v2, v11

    const/16 v11, -0x38

    aput v11, v2, v10

    const/16 v10, -0x3cd

    aput v10, v2, v9

    const/4 v9, -0x4

    aput v9, v2, v8

    const/16 v8, 0x231a

    aput v8, v2, v7

    const/16 v7, 0x5c23

    aput v7, v2, v6

    const/16 v6, -0xaa4

    aput v6, v2, v4

    const/4 v4, 0x0

    :goto_2
    array-length v6, v2

    if-lt v4, v6, :cond_1

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_3
    array-length v6, v2

    if-lt v4, v6, :cond_2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$layout;->accessory_item:I

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$id;->device_name:I

    invoke-virtual {v5, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mDevicename:Landroid/widget/TextView;

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$id;->paired_devices_divider:I

    invoke-virtual {v5, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$id;->main_item:I

    invoke-virtual {v5, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$id;->device_connected_status:I

    invoke-virtual {v5, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$id;->device_image_name:I

    invoke-virtual {v5, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mDevicename:Landroid/widget/TextView;

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairOfDeviceIdAndName;->deviceName:Ljava/lang/String;

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairOfDeviceIdAndName;->deviceId:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->deviceMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v4, v6}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getShealthSensorDevice()Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    move-result-object v4

    :try_start_1
    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getConnectivityType()I

    move-result v7

    const/4 v8, 0x7

    if-ne v7, v8, :cond_3

    :goto_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->deviceMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2, v6}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->setDrawable(Landroid/view/View;Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;)V

    move-object v2, v5

    goto/16 :goto_1

    :cond_1
    aget v6, v2, v4

    aget v7, v3, v4

    xor-int/2addr v6, v7

    aput v6, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_2

    :cond_2
    aget v6, v3, v4

    int-to-char v6, v6

    aput-char v6, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_3

    :catch_0
    move-exception v7

    :cond_3
    new-instance v7, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v7, v0, v1, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Lcom/sec/android/app/shealth/framework/ui/base/PairOfDeviceIdAndName;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)V

    invoke-virtual {v2, v7}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_4

    :catch_1
    move-exception v2

    goto/16 :goto_0
.end method

.method private getView(Lcom/sec/android/app/shealth/framework/ui/base/PairOfDeviceIdAndName;Z)Landroid/view/View;
    .locals 21

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    const/16 v2, 0xf

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, -0x3b

    aput v19, v3, v18

    const/16 v18, -0x77

    aput v18, v3, v17

    const/16 v17, -0x6f

    aput v17, v3, v16

    const/16 v16, -0x6a

    aput v16, v3, v15

    const/16 v15, 0x2e5a

    aput v15, v3, v14

    const/16 v14, -0x62b8

    aput v14, v3, v13

    const/16 v13, -0xd

    aput v13, v3, v12

    const/16 v12, 0x2c2e

    aput v12, v3, v11

    const/16 v11, 0x7b73

    aput v11, v3, v10

    const/16 v10, 0x730f

    aput v10, v3, v9

    const/16 v9, -0x7fa

    aput v9, v3, v8

    const/16 v8, -0x69

    aput v8, v3, v7

    const/16 v7, 0x682b

    aput v7, v3, v6

    const/16 v6, 0x7809

    aput v6, v3, v4

    const/16 v4, 0x514

    aput v4, v3, v2

    const/16 v2, 0xf

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, -0x49

    aput v20, v2, v19

    const/16 v19, -0x14

    aput v19, v2, v18

    const/16 v18, -0x1b

    aput v18, v2, v17

    const/16 v17, -0x9

    aput v17, v2, v16

    const/16 v16, 0x2e36

    aput v16, v2, v15

    const/16 v15, -0x62d2

    aput v15, v2, v14

    const/16 v14, -0x63

    aput v14, v2, v13

    const/16 v13, 0x2c47

    aput v13, v2, v12

    const/16 v12, 0x7b2c

    aput v12, v2, v11

    const/16 v11, 0x737b

    aput v11, v2, v10

    const/16 v10, -0x78d

    aput v10, v2, v9

    const/4 v9, -0x8

    aput v9, v2, v8

    const/16 v8, 0x6852

    aput v8, v2, v7

    const/16 v7, 0x7868

    aput v7, v2, v6

    const/16 v6, 0x578

    aput v6, v2, v4

    const/4 v4, 0x0

    :goto_0
    array-length v6, v2

    if-lt v4, v6, :cond_1

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_1
    array-length v6, v2

    if-lt v4, v6, :cond_2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$layout;->accessory_item:I

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$id;->device_name:I

    invoke-virtual {v7, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$id;->main_item:I

    invoke-virtual {v7, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    sget v4, Lcom/sec/android/app/shealth/framework/ui/R$id;->device_connected_status:I

    invoke-virtual {v7, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    sget v5, Lcom/sec/android/app/shealth/framework/ui/R$id;->setting_layout:I

    invoke-virtual {v7, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    sget v6, Lcom/sec/android/app/shealth/framework/ui/R$id;->setting:I

    invoke-virtual {v7, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/Button;

    sget v8, Lcom/sec/android/app/shealth/framework/ui/R$drawable;->accessory_settings_button_selector:I

    invoke-virtual {v6, v8}, Landroid/widget/Button;->setBackgroundResource(I)V

    sget v8, Lcom/sec/android/app/shealth/framework/ui/R$id;->device_image_name:I

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    sget v8, Lcom/sec/android/app/shealth/framework/ui/R$id;->paired_devices_divider:I

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairOfDeviceIdAndName;->deviceName:Ljava/lang/String;

    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairOfDeviceIdAndName;->deviceId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mPairedDeviceMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2, v8}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getDeviceConnectivityType()I

    move-result v9

    const/4 v10, 0x7

    if-ne v9, v10, :cond_3

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getDeviceType()I

    move-result v9

    const/16 v10, 0x2724

    if-eq v9, v10, :cond_0

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getDeviceType()I

    move-result v9

    const/16 v10, 0x2728

    if-eq v9, v10, :cond_0

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getDeviceType()I

    move-result v9

    const/16 v10, 0x2723

    if-eq v9, v10, :cond_0

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getDeviceType()I

    move-result v9

    const/16 v10, 0x2726

    if-eq v9, v10, :cond_0

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getDeviceType()I

    move-result v9

    const/16 v10, 0x272e

    if-eq v9, v10, :cond_0

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getDeviceType()I

    move-result v9

    const/16 v10, 0x2727

    if-ne v9, v10, :cond_3

    :cond_0
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getShealthSensorDevice()Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    move-result-object v3

    :try_start_0
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$drawable;->s_health_h_list_icon_sync:I

    invoke-virtual {v6, v3}, Landroid/widget/Button;->setBackgroundResource(I)V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/sec/android/app/shealth/framework/ui/R$string;->sync_data:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v3}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    const/4 v3, 0x1

    invoke-virtual {v5, v3}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    new-instance v3, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$17;

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$17;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;)V

    invoke-virtual {v5, v3}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_2
    move-object/from16 v0, p0

    invoke-direct {v0, v7, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->setDrawable(Landroid/view/View;Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;)V

    move-object v2, v7

    :goto_3
    return-object v2

    :cond_1
    aget v6, v2, v4

    aget v7, v3, v4

    xor-int/2addr v6, v7

    aput v6, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_0

    :cond_2
    aget v6, v3, v4

    int-to-char v6, v6

    aput-char v6, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_1

    :catch_0
    move-exception v2

    const/4 v2, 0x0

    goto :goto_3

    :cond_3
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getDeviceConnectivityType()I

    move-result v9

    const/4 v10, 0x1

    if-eq v9, v10, :cond_5

    const/4 v9, 0x1

    invoke-virtual {v3, v9}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    new-instance v9, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v9, v0, v2, v1, v8}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;Lcom/sec/android/app/shealth/framework/ui/base/PairOfDeviceIdAndName;Ljava/lang/String;)V

    invoke-virtual {v3, v9}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    if-eqz p2, :cond_4

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v9, Lcom/sec/android/app/shealth/framework/ui/R$string;->connected:I

    invoke-virtual {v3, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_4
    invoke-virtual {v6, v8}, Landroid/widget/Button;->setTag(Ljava/lang/Object;)V

    invoke-virtual {v5, v8}, Landroid/widget/LinearLayout;->setTag(Ljava/lang/Object;)V

    const/4 v3, 0x0

    :try_start_1
    invoke-virtual {v6, v3}, Landroid/widget/Button;->setVisibility(I)V

    const/4 v3, 0x0

    invoke-virtual {v5, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    const/4 v3, 0x1

    invoke-virtual {v5, v3}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    new-instance v3, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$19;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$19;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)V

    invoke-virtual {v5, v3}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V
    :try_end_1
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    :catch_1
    move-exception v3

    invoke-virtual {v3}, Landroid/content/res/Resources$NotFoundException;->printStackTrace()V

    goto :goto_2

    :cond_4
    const/16 v3, 0x8

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_4

    :cond_5
    new-instance v9, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$20;

    move-object/from16 v0, p0

    invoke-direct {v9, v0, v2, v8}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$20;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;Ljava/lang/String;)V

    invoke-virtual {v3, v9}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v6, v8}, Landroid/widget/Button;->setTag(Ljava/lang/Object;)V

    invoke-virtual {v5, v8}, Landroid/widget/LinearLayout;->setTag(Ljava/lang/Object;)V

    const/4 v3, 0x0

    :try_start_2
    invoke-virtual {v6, v3}, Landroid/widget/Button;->setVisibility(I)V

    const/4 v3, 0x0

    invoke-virtual {v5, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    const/4 v3, 0x1

    invoke-virtual {v5, v3}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    new-instance v3, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$21;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$21;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)V

    invoke-virtual {v5, v3}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    if-eqz p2, :cond_6

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v5, Lcom/sec/android/app/shealth/framework/ui/R$string;->connected:I

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_2
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_2

    :catch_2
    move-exception v3

    invoke-virtual {v3}, Landroid/content/res/Resources$NotFoundException;->printStackTrace()V

    goto/16 :goto_2

    :cond_6
    :try_start_3
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getDeviceConnectivityType()I

    move-result v3

    const/4 v5, 0x1

    if-ne v3, v5, :cond_7

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v5, Lcom/sec/android/app/shealth/framework/ui/R$string;->paired:I

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    :cond_7
    const/16 v3, 0x8

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setVisibility(I)V
    :try_end_3
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_3 .. :try_end_3} :catch_2

    goto/16 :goto_2
.end method

.method private joinWithDevice(Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;)V
    .locals 4

    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_3

    :try_start_1
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getDeviceId()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->deviceMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->deviceMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getShealthSensorDevice()Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->deviceMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v1

    :try_start_2
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    :try_start_3
    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mScanningFragmentController:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    :try_start_4
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_4
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_4} :catch_5
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    :try_start_5
    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mScanningFragmentController:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;

    new-instance v2, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$FragmentEventListener;

    const/4 v3, 0x1

    invoke-direct {v2, p0, v0, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$FragmentEventListener;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;Z)V

    invoke-virtual {v1, p1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;->joinWithDevice(Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$ScanningFragmentEventListener;)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :catch_3
    move-exception v0

    goto :goto_0

    :catch_4
    move-exception v0

    goto :goto_0

    :catch_5
    move-exception v0

    goto :goto_0
.end method

.method private leaveDevice(Ljava/lang/String;Z)V
    .locals 52

    :try_start_0
    invoke-virtual/range {p1 .. p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xc48

    aput v12, v2, v11

    const/16 v11, 0x106f

    aput v11, v2, v10

    const/16 v10, -0x4c87

    aput v10, v2, v9

    const/16 v9, -0x3b

    aput v9, v2, v8

    const/16 v8, -0xe

    aput v8, v2, v7

    const/16 v7, -0x7b

    aput v7, v2, v6

    const/16 v6, -0x73

    aput v6, v2, v5

    const/16 v5, -0x77

    aput v5, v2, v4

    const/16 v4, -0x2e

    aput v4, v2, v3

    const/16 v3, -0x6f99

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xc2d

    aput v13, v1, v12

    const/16 v12, 0x100c

    aput v12, v1, v11

    const/16 v11, -0x4cf0

    aput v11, v1, v10

    const/16 v10, -0x4d

    aput v10, v1, v9

    const/16 v9, -0x80

    aput v9, v1, v8

    const/16 v8, -0x20

    aput v8, v1, v7

    const/16 v7, -0x22

    aput v7, v1, v6

    const/16 v6, -0x15

    aput v6, v1, v5

    const/16 v5, -0x45

    aput v5, v1, v4

    const/16 v4, -0x6ff5

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v4, v1

    if-lt v3, v4, :cond_5

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v4, v1

    if-lt v3, v4, :cond_6

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0xe

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, -0x7f

    aput v18, v2, v17

    const/16 v17, -0x92

    aput v17, v2, v16

    const/16 v16, -0x64

    aput v16, v2, v15

    const/16 v15, -0x79da

    aput v15, v2, v14

    const/16 v14, -0x10

    aput v14, v2, v13

    const/16 v13, -0x23ff

    aput v13, v2, v12

    const/16 v12, -0x68

    aput v12, v2, v11

    const/16 v11, -0x6fef

    aput v11, v2, v10

    const/16 v10, -0x50

    aput v10, v2, v9

    const/16 v9, 0xb5f

    aput v9, v2, v8

    const/16 v8, -0x383

    aput v8, v2, v7

    const/16 v7, -0x63

    aput v7, v2, v6

    const/16 v6, -0x26

    aput v6, v2, v3

    const/16 v3, -0x76f6

    aput v3, v2, v1

    const/16 v1, 0xe

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, -0x44

    aput v19, v1, v18

    const/16 v18, -0xf5

    aput v18, v1, v17

    const/16 v17, -0x1

    aput v17, v1, v16

    const/16 v16, -0x79b1

    aput v16, v1, v15

    const/16 v15, -0x7a

    aput v15, v1, v14

    const/16 v14, -0x239c

    aput v14, v1, v13

    const/16 v13, -0x24

    aput v13, v1, v12

    const/16 v12, -0x6fcf

    aput v12, v1, v11

    const/16 v11, -0x70

    aput v11, v1, v10

    const/16 v10, 0xb3a

    aput v10, v1, v9

    const/16 v9, -0x3f5

    aput v9, v1, v8

    const/4 v8, -0x4

    aput v8, v1, v7

    const/16 v7, -0x41

    aput v7, v1, v6

    const/16 v6, -0x769a

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v6, v1

    if-lt v3, v6, :cond_7

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v6, v1

    if-lt v3, v6, :cond_8

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mPairedDeviceMap:Ljava/util/concurrent/ConcurrentHashMap;

    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_5

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mPairedDeviceMap:Ljava/util/concurrent/ConcurrentHashMap;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mScanningFragmentController:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;

    :try_start_2
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mPairedDeviceMap:Ljava/util/concurrent/ConcurrentHashMap;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    :try_start_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mScanningFragmentController:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;

    move/from16 v0, p2

    invoke-virtual {v2, v1, v0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;->leaveDevice(Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;Z)V

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->IsAutoScanRequired:Z

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->requestPairedDevices()V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_3 .. :try_end_3} :catch_4

    :cond_0
    :goto_4
    return-void

    :catch_0
    move-exception v1

    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, -0x478a

    aput v12, v2, v11

    const/16 v11, -0x25

    aput v11, v2, v10

    const/16 v10, -0x2b

    aput v10, v2, v9

    const/16 v9, -0x22

    aput v9, v2, v8

    const/16 v8, -0xd

    aput v8, v2, v7

    const/16 v7, -0x60

    aput v7, v2, v6

    const/16 v6, -0x6ef

    aput v6, v2, v5

    const/16 v5, -0x65

    aput v5, v2, v4

    const/16 v4, 0x114b

    aput v4, v2, v3

    const/16 v3, -0xf83

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, -0x47ed

    aput v13, v1, v12

    const/16 v12, -0x48

    aput v12, v1, v11

    const/16 v11, -0x44

    aput v11, v1, v10

    const/16 v10, -0x58

    aput v10, v1, v9

    const/16 v9, -0x7f

    aput v9, v1, v8

    const/16 v8, -0x3b

    aput v8, v1, v7

    const/16 v7, -0x6be

    aput v7, v1, v6

    const/4 v6, -0x7

    aput v6, v1, v5

    const/16 v5, 0x1122

    aput v5, v1, v4

    const/16 v4, -0xfef

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_5
    array-length v4, v1

    if-lt v3, v4, :cond_1

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_6
    array-length v4, v1

    if-lt v3, v4, :cond_2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    const/16 v1, 0x2f

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, 0x1d

    const/16 v33, 0x1e

    const/16 v34, 0x1f

    const/16 v35, 0x20

    const/16 v36, 0x21

    const/16 v37, 0x22

    const/16 v38, 0x23

    const/16 v39, 0x24

    const/16 v40, 0x25

    const/16 v41, 0x26

    const/16 v42, 0x27

    const/16 v43, 0x28

    const/16 v44, 0x29

    const/16 v45, 0x2a

    const/16 v46, 0x2b

    const/16 v47, 0x2c

    const/16 v48, 0x2d

    const/16 v49, 0x2e

    const/16 v50, -0x1fb2

    aput v50, v2, v49

    const/16 v49, -0x3f

    aput v49, v2, v48

    const/16 v48, -0x5

    aput v48, v2, v47

    const/16 v47, -0x41ce

    aput v47, v2, v46

    const/16 v46, -0x26

    aput v46, v2, v45

    const/16 v45, 0x7e2b

    aput v45, v2, v44

    const/16 v44, 0x481a

    aput v44, v2, v43

    const/16 v43, -0xedf

    aput v43, v2, v42

    const/16 v42, -0x79

    aput v42, v2, v41

    const/16 v41, 0x1360

    aput v41, v2, v40

    const/16 v40, 0x1a61

    aput v40, v2, v39

    const/16 v39, 0x2e6a

    aput v39, v2, v38

    const/16 v38, -0x62f2

    aput v38, v2, v37

    const/16 v37, -0x12

    aput v37, v2, v36

    const/16 v36, -0x61

    aput v36, v2, v35

    const/16 v35, -0x9

    aput v35, v2, v34

    const/16 v34, 0x615b

    aput v34, v2, v33

    const/16 v33, -0x1b00

    aput v33, v2, v32

    const/16 v32, -0x6d

    aput v32, v2, v31

    const/16 v31, -0x10

    aput v31, v2, v30

    const/16 v30, -0x30

    aput v30, v2, v29

    const/16 v29, -0x40ae

    aput v29, v2, v28

    const/16 v28, -0x31

    aput v28, v2, v27

    const/16 v27, -0x80

    aput v27, v2, v26

    const/16 v26, -0x75

    aput v26, v2, v25

    const/16 v25, -0x22

    aput v25, v2, v24

    const/16 v24, -0x4c

    aput v24, v2, v23

    const/16 v23, -0xbac

    aput v23, v2, v22

    const/16 v22, -0x6d

    aput v22, v2, v21

    const/16 v21, -0x6e

    aput v21, v2, v20

    const/16 v20, -0x3c

    aput v20, v2, v19

    const/16 v19, -0x72

    aput v19, v2, v18

    const/16 v18, 0x192c

    aput v18, v2, v17

    const/16 v17, -0x53dd

    aput v17, v2, v16

    const/16 v16, -0x6a

    aput v16, v2, v15

    const/16 v15, -0x5589

    aput v15, v2, v14

    const/16 v14, -0x31

    aput v14, v2, v13

    const/16 v13, -0x48de

    aput v13, v2, v12

    const/16 v12, -0x22

    aput v12, v2, v11

    const/16 v11, -0x71

    aput v11, v2, v10

    const/16 v10, -0x5f

    aput v10, v2, v9

    const/16 v9, 0x611d

    aput v9, v2, v8

    const/16 v8, -0x10fc

    aput v8, v2, v7

    const/16 v7, -0x67

    aput v7, v2, v6

    const/16 v6, 0x6825

    aput v6, v2, v5

    const/16 v5, 0x3e0d

    aput v5, v2, v3

    const/16 v3, -0x53ae

    aput v3, v2, v1

    const/16 v1, 0x2f

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, 0x1d

    const/16 v34, 0x1e

    const/16 v35, 0x1f

    const/16 v36, 0x20

    const/16 v37, 0x21

    const/16 v38, 0x22

    const/16 v39, 0x23

    const/16 v40, 0x24

    const/16 v41, 0x25

    const/16 v42, 0x26

    const/16 v43, 0x27

    const/16 v44, 0x28

    const/16 v45, 0x29

    const/16 v46, 0x2a

    const/16 v47, 0x2b

    const/16 v48, 0x2c

    const/16 v49, 0x2d

    const/16 v50, 0x2e

    const/16 v51, -0x1f92

    aput v51, v1, v50

    const/16 v50, -0x20

    aput v50, v1, v49

    const/16 v49, -0x26

    aput v49, v1, v48

    const/16 v48, -0x41ee

    aput v48, v1, v47

    const/16 v47, -0x42

    aput v47, v1, v46

    const/16 v46, 0x7e4e

    aput v46, v1, v45

    const/16 v45, 0x487e

    aput v45, v1, v44

    const/16 v44, -0xeb8

    aput v44, v1, v43

    const/16 v43, -0xf

    aput v43, v1, v42

    const/16 v42, 0x130f

    aput v42, v1, v41

    const/16 v41, 0x1a13

    aput v41, v1, v40

    const/16 v40, 0x2e1a

    aput v40, v1, v39

    const/16 v39, -0x62d2

    aput v39, v1, v38

    const/16 v38, -0x63

    aput v38, v1, v37

    const/16 v37, -0x6

    aput v37, v1, v36

    const/16 v36, -0x7e

    aput v36, v1, v35

    const/16 v35, 0x6137

    aput v35, v1, v34

    const/16 v34, -0x1a9f

    aput v34, v1, v33

    const/16 v33, -0x1b

    aput v33, v1, v32

    const/16 v32, -0x30

    aput v32, v1, v31

    const/16 v31, -0x5c

    aput v31, v1, v30

    const/16 v30, -0x40d9

    aput v30, v1, v29

    const/16 v29, -0x41

    aput v29, v1, v28

    const/16 v28, -0x12

    aput v28, v1, v27

    const/16 v27, -0x1e

    aput v27, v1, v26

    const/16 v26, -0x2

    aput v26, v1, v25

    const/16 v25, -0x28

    aput v25, v1, v24

    const/16 v24, -0xbcb

    aput v24, v1, v23

    const/16 v23, -0xc

    aput v23, v1, v22

    const/16 v22, -0x9

    aput v22, v1, v21

    const/16 v21, -0x58

    aput v21, v1, v20

    const/16 v20, -0x1e

    aput v20, v1, v19

    const/16 v19, 0x1945

    aput v19, v1, v18

    const/16 v18, -0x53e7

    aput v18, v1, v17

    const/16 v17, -0x54

    aput v17, v1, v16

    const/16 v16, -0x55a9

    aput v16, v1, v15

    const/16 v15, -0x56

    aput v15, v1, v14

    const/16 v14, -0x48bf

    aput v14, v1, v13

    const/16 v13, -0x49

    aput v13, v1, v12

    const/4 v12, -0x7

    aput v12, v1, v11

    const/16 v11, -0x3c

    aput v11, v1, v10

    const/16 v10, 0x6159

    aput v10, v1, v9

    const/16 v9, -0x109f

    aput v9, v1, v8

    const/16 v8, -0x11

    aput v8, v1, v7

    const/16 v7, 0x6844

    aput v7, v1, v6

    const/16 v6, 0x3e68

    aput v6, v1, v5

    const/16 v5, -0x53c2

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_7
    array-length v5, v1

    if-lt v3, v5, :cond_3

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_8
    array-length v5, v1

    if-lt v3, v5, :cond_4

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    :cond_1
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_5

    :cond_2
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_6

    :cond_3
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_7

    :cond_4
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_8

    :cond_5
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_6
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_7
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    :cond_8
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3

    :catch_1
    move-exception v1

    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0x4c4a

    aput v12, v2, v11

    const/16 v11, -0x8d1

    aput v11, v2, v10

    const/16 v10, -0x62

    aput v10, v2, v9

    const/16 v9, -0xf91

    aput v9, v2, v8

    const/16 v8, -0x7e

    aput v8, v2, v7

    const/16 v7, -0x78

    aput v7, v2, v6

    const/16 v6, -0x57b7

    aput v6, v2, v5

    const/16 v5, -0x36

    aput v5, v2, v4

    const/16 v4, -0x48

    aput v4, v2, v3

    const/16 v3, -0x3b

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0x4c2f

    aput v13, v1, v12

    const/16 v12, -0x8b4

    aput v12, v1, v11

    const/16 v11, -0x9

    aput v11, v1, v10

    const/16 v10, -0xfe7

    aput v10, v1, v9

    const/16 v9, -0x10

    aput v9, v1, v8

    const/16 v8, -0x13

    aput v8, v1, v7

    const/16 v7, -0x57e6

    aput v7, v1, v6

    const/16 v6, -0x58

    aput v6, v1, v5

    const/16 v5, -0x2f

    aput v5, v1, v4

    const/16 v4, -0x57

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_9
    array-length v4, v1

    if-lt v3, v4, :cond_9

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_a
    array-length v4, v1

    if-lt v3, v4, :cond_a

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    const/16 v1, 0x23

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, 0x1d

    const/16 v33, 0x1e

    const/16 v34, 0x1f

    const/16 v35, 0x20

    const/16 v36, 0x21

    const/16 v37, 0x22

    const/16 v38, -0x38d

    aput v38, v2, v37

    const/16 v37, -0x70

    aput v37, v2, v36

    const/16 v36, -0x14

    aput v36, v2, v35

    const/16 v35, 0x7c79

    aput v35, v2, v34

    const/16 v34, 0x215c

    aput v34, v2, v33

    const/16 v33, 0xf52

    aput v33, v2, v32

    const/16 v32, 0x766

    aput v32, v2, v31

    const/16 v31, 0x1927

    aput v31, v2, v30

    const/16 v30, -0x2295

    aput v30, v2, v29

    const/16 v29, -0x48

    aput v29, v2, v28

    const/16 v28, -0x6b

    aput v28, v2, v27

    const/16 v27, -0x7f

    aput v27, v2, v26

    const/16 v26, 0x7722

    aput v26, v2, v25

    const/16 v25, -0xdfb

    aput v25, v2, v24

    const/16 v24, -0x7a

    aput v24, v2, v23

    const/16 v23, -0x68

    aput v23, v2, v22

    const/16 v22, -0x24

    aput v22, v2, v21

    const/16 v21, -0x1b

    aput v21, v2, v20

    const/16 v20, 0x533

    aput v20, v2, v19

    const/16 v19, -0x5295

    aput v19, v2, v18

    const/16 v18, -0x38

    aput v18, v2, v17

    const/16 v17, 0xf17

    aput v17, v2, v16

    const/16 v16, -0x5298

    aput v16, v2, v15

    const/16 v15, -0x34

    aput v15, v2, v14

    const/16 v14, 0x7824

    aput v14, v2, v13

    const/16 v13, 0x503e

    aput v13, v2, v12

    const/16 v12, 0x1237

    aput v12, v2, v11

    const/16 v11, -0x2d84

    aput v11, v2, v10

    const/16 v10, -0x45

    aput v10, v2, v9

    const/16 v9, -0x5d9f

    aput v9, v2, v8

    const/16 v8, -0x34

    aput v8, v2, v7

    const/16 v7, 0x2300

    aput v7, v2, v6

    const/16 v6, -0x1dc0

    aput v6, v2, v5

    const/16 v5, -0x4f

    aput v5, v2, v3

    const/16 v3, -0x78

    aput v3, v2, v1

    const/16 v1, 0x23

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, 0x1d

    const/16 v34, 0x1e

    const/16 v35, 0x1f

    const/16 v36, 0x20

    const/16 v37, 0x21

    const/16 v38, 0x22

    const/16 v39, -0x3e1

    aput v39, v1, v38

    const/16 v38, -0x4

    aput v38, v1, v37

    const/16 v37, -0x67

    aput v37, v1, v36

    const/16 v36, 0x7c17

    aput v36, v1, v35

    const/16 v35, 0x217c

    aput v35, v1, v34

    const/16 v34, 0xf21

    aput v34, v1, v33

    const/16 v33, 0x70f

    aput v33, v1, v32

    const/16 v32, 0x1907

    aput v32, v1, v31

    const/16 v31, -0x22e7

    aput v31, v1, v30

    const/16 v30, -0x23

    aput v30, v1, v29

    const/16 v29, -0x7

    aput v29, v1, v28

    const/16 v28, -0x13

    aput v28, v1, v27

    const/16 v27, 0x774d

    aput v27, v1, v26

    const/16 v26, -0xd89

    aput v26, v1, v25

    const/16 v25, -0xe

    aput v25, v1, v24

    const/16 v24, -0xa

    aput v24, v1, v23

    const/16 v23, -0x4d

    aput v23, v1, v22

    const/16 v22, -0x5a

    aput v22, v1, v21

    const/16 v21, 0x547

    aput v21, v1, v20

    const/16 v20, -0x52fb

    aput v20, v1, v19

    const/16 v19, -0x53

    aput v19, v1, v18

    const/16 v18, 0xf7a

    aput v18, v1, v17

    const/16 v17, -0x52f1

    aput v17, v1, v16

    const/16 v16, -0x53

    aput v16, v1, v15

    const/16 v15, 0x7856

    aput v15, v1, v14

    const/16 v14, 0x5078

    aput v14, v1, v13

    const/16 v13, 0x1250

    aput v13, v1, v12

    const/16 v12, -0x2dee

    aput v12, v1, v11

    const/16 v11, -0x2e

    aput v11, v1, v10

    const/16 v10, -0x5df1

    aput v10, v1, v9

    const/16 v9, -0x5e

    aput v9, v1, v8

    const/16 v8, 0x2361

    aput v8, v1, v7

    const/16 v7, -0x1ddd

    aput v7, v1, v6

    const/16 v6, -0x1e

    aput v6, v1, v5

    const/16 v5, -0x1b

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_b
    array-length v5, v1

    if-lt v3, v5, :cond_b

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_c
    array-length v5, v1

    if-lt v3, v5, :cond_c

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    :cond_9
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_9

    :cond_a
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_a

    :cond_b
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_b

    :cond_c
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_c

    :catch_2
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_4

    :catch_3
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto/16 :goto_4

    :catch_4
    move-exception v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto/16 :goto_4

    :catch_5
    move-exception v1

    goto/16 :goto_4
.end method

.method private leaveDeviceWithOutRefresh(Ljava/lang/String;Z)V
    .locals 52

    :try_start_0
    invoke-virtual/range {p1 .. p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, -0x6ed3

    aput v12, v2, v11

    const/16 v11, -0xe

    aput v11, v2, v10

    const/16 v10, -0x20

    aput v10, v2, v9

    const/16 v9, -0x38

    aput v9, v2, v8

    const/16 v8, -0x4a

    aput v8, v2, v7

    const/16 v7, 0x5e13

    aput v7, v2, v6

    const/16 v6, -0x2bf3

    aput v6, v2, v5

    const/16 v5, -0x4a

    aput v5, v2, v4

    const/16 v4, -0x5f

    aput v4, v2, v3

    const/16 v3, -0x5288

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, -0x6eb8

    aput v13, v1, v12

    const/16 v12, -0x6f

    aput v12, v1, v11

    const/16 v11, -0x77

    aput v11, v1, v10

    const/16 v10, -0x42

    aput v10, v1, v9

    const/16 v9, -0x3c

    aput v9, v1, v8

    const/16 v8, 0x5e76

    aput v8, v1, v7

    const/16 v7, -0x2ba2

    aput v7, v1, v6

    const/16 v6, -0x2c

    aput v6, v1, v5

    const/16 v5, -0x38

    aput v5, v1, v4

    const/16 v4, -0x52ec

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v4, v1

    if-lt v3, v4, :cond_5

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v4, v1

    if-lt v3, v4, :cond_6

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0xe

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, -0x6083

    aput v18, v2, v17

    const/16 v17, -0x6

    aput v17, v2, v16

    const/16 v16, -0x5be9

    aput v16, v2, v15

    const/16 v15, -0x33

    aput v15, v2, v14

    const/4 v14, -0x5

    aput v14, v2, v13

    const/16 v13, 0x1c75

    aput v13, v2, v12

    const/16 v12, -0x53a8

    aput v12, v2, v11

    const/16 v11, -0x74

    aput v11, v2, v10

    const/16 v10, -0x41d8

    aput v10, v2, v9

    const/16 v9, -0x25

    aput v9, v2, v8

    const/16 v8, 0x5e69

    aput v8, v2, v7

    const/16 v7, -0x44c1

    aput v7, v2, v6

    const/16 v6, -0x22

    aput v6, v2, v3

    const/16 v3, -0x47

    aput v3, v2, v1

    const/16 v1, 0xe

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, -0x60c0

    aput v19, v1, v18

    const/16 v18, -0x61

    aput v18, v1, v17

    const/16 v17, -0x5b8c

    aput v17, v1, v16

    const/16 v16, -0x5c

    aput v16, v1, v15

    const/16 v15, -0x73

    aput v15, v1, v14

    const/16 v14, 0x1c10

    aput v14, v1, v13

    const/16 v13, -0x53e4

    aput v13, v1, v12

    const/16 v12, -0x54

    aput v12, v1, v11

    const/16 v11, -0x41f8

    aput v11, v1, v10

    const/16 v10, -0x42

    aput v10, v1, v9

    const/16 v9, 0x5e1f

    aput v9, v1, v8

    const/16 v8, -0x44a2

    aput v8, v1, v7

    const/16 v7, -0x45

    aput v7, v1, v6

    const/16 v6, -0x2b

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v6, v1

    if-lt v3, v6, :cond_7

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v6, v1

    if-lt v3, v6, :cond_8

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mPairedDeviceMap:Ljava/util/concurrent/ConcurrentHashMap;

    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_5

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mPairedDeviceMap:Ljava/util/concurrent/ConcurrentHashMap;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mScanningFragmentController:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;

    :try_start_2
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mPairedDeviceMap:Ljava/util/concurrent/ConcurrentHashMap;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    :try_start_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mScanningFragmentController:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;

    move/from16 v0, p2

    invoke-virtual {v2, v1, v0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;->leaveDevice(Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;Z)V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_3 .. :try_end_3} :catch_4

    :cond_0
    :goto_4
    return-void

    :catch_0
    move-exception v1

    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, -0x13

    aput v12, v2, v11

    const/16 v11, -0x41

    aput v11, v2, v10

    const/4 v10, -0x3

    aput v10, v2, v9

    const/16 v9, 0x6b3f

    aput v9, v2, v8

    const/16 v8, 0x1719

    aput v8, v2, v7

    const/16 v7, 0x7472

    aput v7, v2, v6

    const/16 v6, -0x22d9

    aput v6, v2, v5

    const/16 v5, -0x41

    aput v5, v2, v4

    const/16 v4, -0x28

    aput v4, v2, v3

    const/16 v3, -0x739c

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, -0x78

    aput v13, v1, v12

    const/16 v12, -0x24

    aput v12, v1, v11

    const/16 v11, -0x6c

    aput v11, v1, v10

    const/16 v10, 0x6b49

    aput v10, v1, v9

    const/16 v9, 0x176b

    aput v9, v1, v8

    const/16 v8, 0x7417

    aput v8, v1, v7

    const/16 v7, -0x228c

    aput v7, v1, v6

    const/16 v6, -0x23

    aput v6, v1, v5

    const/16 v5, -0x4f

    aput v5, v1, v4

    const/16 v4, -0x73f8

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_5
    array-length v4, v1

    if-lt v3, v4, :cond_1

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_6
    array-length v4, v1

    if-lt v3, v4, :cond_2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    const/16 v1, 0x2f

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, 0x1d

    const/16 v33, 0x1e

    const/16 v34, 0x1f

    const/16 v35, 0x20

    const/16 v36, 0x21

    const/16 v37, 0x22

    const/16 v38, 0x23

    const/16 v39, 0x24

    const/16 v40, 0x25

    const/16 v41, 0x26

    const/16 v42, 0x27

    const/16 v43, 0x28

    const/16 v44, 0x29

    const/16 v45, 0x2a

    const/16 v46, 0x2b

    const/16 v47, 0x2c

    const/16 v48, 0x2d

    const/16 v49, 0x2e

    const/16 v50, -0x17

    aput v50, v2, v49

    const/16 v49, -0x4795

    aput v49, v2, v48

    const/16 v48, -0x67

    aput v48, v2, v47

    const/16 v47, 0x466e

    aput v47, v2, v46

    const/16 v46, -0x56de

    aput v46, v2, v45

    const/16 v45, -0x34

    aput v45, v2, v44

    const/16 v44, -0x80

    aput v44, v2, v43

    const/16 v43, 0x4944

    aput v43, v2, v42

    const/16 v42, 0x113f

    aput v42, v2, v41

    const/16 v41, -0x7782

    aput v41, v2, v40

    const/16 v40, -0x6

    aput v40, v2, v39

    const/16 v39, 0x5b1e

    aput v39, v2, v38

    const/16 v38, -0xd85

    aput v38, v2, v37

    const/16 v37, -0x7f

    aput v37, v2, v36

    const/16 v36, 0x3132

    aput v36, v2, v35

    const/16 v35, 0x7844

    aput v35, v2, v34

    const/16 v34, 0x6c14

    aput v34, v2, v33

    const/16 v33, 0x230d

    aput v33, v2, v32

    const/16 v32, -0x6bab

    aput v32, v2, v31

    const/16 v31, -0x4c

    aput v31, v2, v30

    const/16 v30, -0x59

    aput v30, v2, v29

    const/16 v29, -0x1d

    aput v29, v2, v28

    const/16 v28, -0x1c

    aput v28, v2, v27

    const/16 v27, -0x5d

    aput v27, v2, v26

    const/16 v26, -0x45

    aput v26, v2, v25

    const/16 v25, -0x2d8b

    aput v25, v2, v24

    const/16 v24, -0x42

    aput v24, v2, v23

    const/16 v23, 0xe4a

    aput v23, v2, v22

    const/16 v22, -0x1397

    aput v22, v2, v21

    const/16 v21, -0x77

    aput v21, v2, v20

    const/16 v20, -0xda8

    aput v20, v2, v19

    const/16 v19, -0x62

    aput v19, v2, v18

    const/16 v18, 0x24e

    aput v18, v2, v17

    const/16 v17, 0x7538

    aput v17, v2, v16

    const/16 v16, 0x674f

    aput v16, v2, v15

    const/16 v15, 0x5e47

    aput v15, v2, v14

    const/16 v14, 0x343b

    aput v14, v2, v13

    const/16 v13, 0x5e57

    aput v13, v2, v12

    const/16 v12, -0x7c9

    aput v12, v2, v11

    const/16 v11, -0x72

    aput v11, v2, v10

    const/16 v10, -0x1a4

    aput v10, v2, v9

    const/16 v9, -0x46

    aput v9, v2, v8

    const/16 v8, -0xb

    aput v8, v2, v7

    const/16 v7, -0x15a7

    aput v7, v2, v6

    const/16 v6, -0x75

    aput v6, v2, v5

    const/16 v5, -0x13

    aput v5, v2, v3

    const/16 v3, -0x2c

    aput v3, v2, v1

    const/16 v1, 0x2f

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, 0x1d

    const/16 v34, 0x1e

    const/16 v35, 0x1f

    const/16 v36, 0x20

    const/16 v37, 0x21

    const/16 v38, 0x22

    const/16 v39, 0x23

    const/16 v40, 0x24

    const/16 v41, 0x25

    const/16 v42, 0x26

    const/16 v43, 0x27

    const/16 v44, 0x28

    const/16 v45, 0x29

    const/16 v46, 0x2a

    const/16 v47, 0x2b

    const/16 v48, 0x2c

    const/16 v49, 0x2d

    const/16 v50, 0x2e

    const/16 v51, -0x37

    aput v51, v1, v50

    const/16 v50, -0x47b6

    aput v50, v1, v49

    const/16 v49, -0x48

    aput v49, v1, v48

    const/16 v48, 0x464e

    aput v48, v1, v47

    const/16 v47, -0x56ba

    aput v47, v1, v46

    const/16 v46, -0x57

    aput v46, v1, v45

    const/16 v45, -0x1c

    aput v45, v1, v44

    const/16 v44, 0x492d

    aput v44, v1, v43

    const/16 v43, 0x1149

    aput v43, v1, v42

    const/16 v42, -0x77ef

    aput v42, v1, v41

    const/16 v41, -0x78

    aput v41, v1, v40

    const/16 v40, 0x5b6e

    aput v40, v1, v39

    const/16 v39, -0xda5

    aput v39, v1, v38

    const/16 v38, -0xe

    aput v38, v1, v37

    const/16 v37, 0x3157

    aput v37, v1, v36

    const/16 v36, 0x7831

    aput v36, v1, v35

    const/16 v35, 0x6c78

    aput v35, v1, v34

    const/16 v34, 0x236c

    aput v34, v1, v33

    const/16 v33, -0x6bdd

    aput v33, v1, v32

    const/16 v32, -0x6c

    aput v32, v1, v31

    const/16 v31, -0x2d

    aput v31, v1, v30

    const/16 v30, -0x6a

    aput v30, v1, v29

    const/16 v29, -0x6c

    aput v29, v1, v28

    const/16 v28, -0x33

    aput v28, v1, v27

    const/16 v27, -0x2e

    aput v27, v1, v26

    const/16 v26, -0x2dab

    aput v26, v1, v25

    const/16 v25, -0x2e

    aput v25, v1, v24

    const/16 v24, 0xe2b

    aput v24, v1, v23

    const/16 v23, -0x13f2

    aput v23, v1, v22

    const/16 v22, -0x14

    aput v22, v1, v21

    const/16 v21, -0xdcc

    aput v21, v1, v20

    const/16 v20, -0xe

    aput v20, v1, v19

    const/16 v19, 0x227

    aput v19, v1, v18

    const/16 v18, 0x7502

    aput v18, v1, v17

    const/16 v17, 0x6775

    aput v17, v1, v16

    const/16 v16, 0x5e67

    aput v16, v1, v15

    const/16 v15, 0x345e

    aput v15, v1, v14

    const/16 v14, 0x5e34

    aput v14, v1, v13

    const/16 v13, -0x7a2

    aput v13, v1, v12

    const/4 v12, -0x8

    aput v12, v1, v11

    const/16 v11, -0x1c7

    aput v11, v1, v10

    const/4 v10, -0x2

    aput v10, v1, v9

    const/16 v9, -0x70

    aput v9, v1, v8

    const/16 v8, -0x15d1

    aput v8, v1, v7

    const/16 v7, -0x16

    aput v7, v1, v6

    const/16 v6, -0x78

    aput v6, v1, v5

    const/16 v5, -0x48

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_7
    array-length v5, v1

    if-lt v3, v5, :cond_3

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_8
    array-length v5, v1

    if-lt v3, v5, :cond_4

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    :cond_1
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_5

    :cond_2
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_6

    :cond_3
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_7

    :cond_4
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_8

    :cond_5
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_6
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_7
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    :cond_8
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3

    :catch_1
    move-exception v1

    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, -0x1a

    aput v12, v2, v11

    const/16 v11, -0x17

    aput v11, v2, v10

    const/16 v10, -0x6c

    aput v10, v2, v9

    const/16 v9, -0x15

    aput v9, v2, v8

    const/16 v8, -0x1f

    aput v8, v2, v7

    const/16 v7, -0x37

    aput v7, v2, v6

    const/16 v6, -0x1a

    aput v6, v2, v5

    const/16 v5, -0x39ea

    aput v5, v2, v4

    const/16 v4, -0x51

    aput v4, v2, v3

    const/16 v3, -0x32e7

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, -0x7d

    aput v13, v1, v12

    const/16 v12, -0x76

    aput v12, v1, v11

    const/4 v11, -0x3

    aput v11, v1, v10

    const/16 v10, -0x63

    aput v10, v1, v9

    const/16 v9, -0x6d

    aput v9, v1, v8

    const/16 v8, -0x54

    aput v8, v1, v7

    const/16 v7, -0x4b

    aput v7, v1, v6

    const/16 v6, -0x398c

    aput v6, v1, v5

    const/16 v5, -0x3a

    aput v5, v1, v4

    const/16 v4, -0x328b

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_9
    array-length v4, v1

    if-lt v3, v4, :cond_9

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_a
    array-length v4, v1

    if-lt v3, v4, :cond_a

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    const/16 v1, 0x23

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, 0x1d

    const/16 v33, 0x1e

    const/16 v34, 0x1f

    const/16 v35, 0x20

    const/16 v36, 0x21

    const/16 v37, 0x22

    const/16 v38, -0xa

    aput v38, v2, v37

    const/16 v37, 0x5f77

    aput v37, v2, v36

    const/16 v36, -0x41d6

    aput v36, v2, v35

    const/16 v35, -0x30

    aput v35, v2, v34

    const/16 v34, 0x3b6e

    aput v34, v2, v33

    const/16 v33, 0x7848

    aput v33, v2, v32

    const/16 v32, -0x6bef

    aput v32, v2, v31

    const/16 v31, -0x4c

    aput v31, v2, v30

    const/16 v30, -0x37

    aput v30, v2, v29

    const/16 v29, -0x67

    aput v29, v2, v28

    const/16 v28, -0x4686

    aput v28, v2, v27

    const/16 v27, -0x2b

    aput v27, v2, v26

    const/16 v26, -0x15

    aput v26, v2, v25

    const/16 v25, -0x4e

    aput v25, v2, v24

    const/16 v24, -0xcf9

    aput v24, v2, v23

    const/16 v23, -0x63

    aput v23, v2, v22

    const/16 v22, -0x21

    aput v22, v2, v21

    const/16 v21, -0x6

    aput v21, v2, v20

    const/16 v20, 0x2d62

    aput v20, v2, v19

    const/16 v19, -0x10bd

    aput v19, v2, v18

    const/16 v18, -0x76

    aput v18, v2, v17

    const/16 v17, -0xd

    aput v17, v2, v16

    const/16 v16, -0x11af

    aput v16, v2, v15

    const/16 v15, -0x71

    aput v15, v2, v14

    const/16 v14, 0x66a

    aput v14, v2, v13

    const/16 v13, -0x67c0

    aput v13, v2, v12

    const/4 v12, -0x1

    aput v12, v2, v11

    const/16 v11, 0x5a29

    aput v11, v2, v10

    const/16 v10, 0x4e33

    aput v10, v2, v9

    const/16 v9, -0xfe0

    aput v9, v2, v8

    const/16 v8, -0x62

    aput v8, v2, v7

    const/16 v7, -0x60c6

    aput v7, v2, v6

    const/4 v6, -0x4

    aput v6, v2, v5

    const/16 v5, -0x3290

    aput v5, v2, v3

    const/16 v3, -0x60

    aput v3, v2, v1

    const/16 v1, 0x23

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, 0x1d

    const/16 v34, 0x1e

    const/16 v35, 0x1f

    const/16 v36, 0x20

    const/16 v37, 0x21

    const/16 v38, 0x22

    const/16 v39, -0x66

    aput v39, v1, v38

    const/16 v38, 0x5f1b

    aput v38, v1, v37

    const/16 v37, -0x41a1

    aput v37, v1, v36

    const/16 v36, -0x42

    aput v36, v1, v35

    const/16 v35, 0x3b4e

    aput v35, v1, v34

    const/16 v34, 0x783b

    aput v34, v1, v33

    const/16 v33, -0x6b88

    aput v33, v1, v32

    const/16 v32, -0x6c

    aput v32, v1, v31

    const/16 v31, -0x45

    aput v31, v1, v30

    const/16 v30, -0x4

    aput v30, v1, v29

    const/16 v29, -0x46ea

    aput v29, v1, v28

    const/16 v28, -0x47

    aput v28, v1, v27

    const/16 v27, -0x7c

    aput v27, v1, v26

    const/16 v26, -0x40

    aput v26, v1, v25

    const/16 v25, -0xc8d

    aput v25, v1, v24

    const/16 v24, -0xd

    aput v24, v1, v23

    const/16 v23, -0x50

    aput v23, v1, v22

    const/16 v22, -0x47

    aput v22, v1, v21

    const/16 v21, 0x2d16

    aput v21, v1, v20

    const/16 v20, -0x10d3

    aput v20, v1, v19

    const/16 v19, -0x11

    aput v19, v1, v18

    const/16 v18, -0x62

    aput v18, v1, v17

    const/16 v17, -0x11ca

    aput v17, v1, v16

    const/16 v16, -0x12

    aput v16, v1, v15

    const/16 v15, 0x618

    aput v15, v1, v14

    const/16 v14, -0x67fa

    aput v14, v1, v13

    const/16 v13, -0x68

    aput v13, v1, v12

    const/16 v12, 0x5a47

    aput v12, v1, v11

    const/16 v11, 0x4e5a

    aput v11, v1, v10

    const/16 v10, -0xfb2

    aput v10, v1, v9

    const/16 v9, -0x10

    aput v9, v1, v8

    const/16 v8, -0x60a5

    aput v8, v1, v7

    const/16 v7, -0x61

    aput v7, v1, v6

    const/16 v6, -0x32dd

    aput v6, v1, v5

    const/16 v5, -0x33

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_b
    array-length v5, v1

    if-lt v3, v5, :cond_b

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_c
    array-length v5, v1

    if-lt v3, v5, :cond_c

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    :cond_9
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_9

    :cond_a
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_a

    :cond_b
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_b

    :cond_c
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_c

    :catch_2
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_4

    :catch_3
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto/16 :goto_4

    :catch_4
    move-exception v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto/16 :goto_4

    :catch_5
    move-exception v1

    goto/16 :goto_4
.end method

.method public static lp()Ljava/lang/String;
    .locals 59

    const/16 v0, 0x38

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x4

    const/4 v6, 0x5

    const/4 v7, 0x6

    const/4 v8, 0x7

    const/16 v9, 0x8

    const/16 v10, 0x9

    const/16 v11, 0xa

    const/16 v12, 0xb

    const/16 v13, 0xc

    const/16 v14, 0xd

    const/16 v15, 0xe

    const/16 v16, 0xf

    const/16 v17, 0x10

    const/16 v18, 0x11

    const/16 v19, 0x12

    const/16 v20, 0x13

    const/16 v21, 0x14

    const/16 v22, 0x15

    const/16 v23, 0x16

    const/16 v24, 0x17

    const/16 v25, 0x18

    const/16 v26, 0x19

    const/16 v27, 0x1a

    const/16 v28, 0x1b

    const/16 v29, 0x1c

    const/16 v30, 0x1d

    const/16 v31, 0x1e

    const/16 v32, 0x1f

    const/16 v33, 0x20

    const/16 v34, 0x21

    const/16 v35, 0x22

    const/16 v36, 0x23

    const/16 v37, 0x24

    const/16 v38, 0x25

    const/16 v39, 0x26

    const/16 v40, 0x27

    const/16 v41, 0x28

    const/16 v42, 0x29

    const/16 v43, 0x2a

    const/16 v44, 0x2b

    const/16 v45, 0x2c

    const/16 v46, 0x2d

    const/16 v47, 0x2e

    const/16 v48, 0x2f

    const/16 v49, 0x30

    const/16 v50, 0x31

    const/16 v51, 0x32

    const/16 v52, 0x33

    const/16 v53, 0x34

    const/16 v54, 0x35

    const/16 v55, 0x36

    const/16 v56, 0x37

    const/16 v57, 0x331f

    aput v57, v1, v56

    const/16 v56, 0x7109

    aput v56, v1, v55

    const/16 v55, 0x6f51

    aput v55, v1, v54

    const/16 v54, -0xee5

    aput v54, v1, v53

    const/16 v53, -0x62

    aput v53, v1, v52

    const/16 v52, -0x1787

    aput v52, v1, v51

    const/16 v51, -0x38

    aput v51, v1, v50

    const/16 v50, 0x3e3d

    aput v50, v1, v49

    const/16 v49, -0xdaf

    aput v49, v1, v48

    const/16 v48, -0x2e

    aput v48, v1, v47

    const/16 v47, 0x483b

    aput v47, v1, v46

    const/16 v46, 0x1026

    aput v46, v1, v45

    const/16 v45, 0x7579

    aput v45, v1, v44

    const/16 v44, -0x43e5

    aput v44, v1, v43

    const/16 v43, -0x2e

    aput v43, v1, v42

    const/16 v42, -0x64ac

    aput v42, v1, v41

    const/16 v41, -0x8

    aput v41, v1, v40

    const/16 v40, -0x4e

    aput v40, v1, v39

    const/16 v39, 0x1b30

    aput v39, v1, v38

    const/16 v38, -0x2b98

    aput v38, v1, v37

    const/16 v37, -0x63

    aput v37, v1, v36

    const/16 v36, -0x3afe

    aput v36, v1, v35

    const/16 v35, -0x1

    aput v35, v1, v34

    const/16 v34, -0x3a

    aput v34, v1, v33

    const/16 v33, -0x2ec6

    aput v33, v1, v32

    const/16 v32, -0x7

    aput v32, v1, v31

    const/16 v31, -0x15f5

    aput v31, v1, v30

    const/16 v30, -0x7c

    aput v30, v1, v29

    const/16 v29, 0x7133

    aput v29, v1, v28

    const/16 v28, -0x50e4

    aput v28, v1, v27

    const/16 v27, -0x38

    aput v27, v1, v26

    const/16 v26, -0x43

    aput v26, v1, v25

    const/16 v25, -0x679f

    aput v25, v1, v24

    const/16 v24, -0x22

    aput v24, v1, v23

    const/16 v23, 0x424b

    aput v23, v1, v22

    const/16 v22, 0x2e2c

    aput v22, v1, v21

    const/16 v21, 0x5547

    aput v21, v1, v20

    const/16 v20, 0x213b

    aput v20, v1, v19

    const/16 v19, 0x234f

    aput v19, v1, v18

    const/16 v18, -0x4ebe

    aput v18, v1, v17

    const/16 v17, -0x2e

    aput v17, v1, v16

    const/16 v16, -0x2ed2

    aput v16, v1, v15

    const/16 v15, -0x4c

    aput v15, v1, v14

    const/16 v14, -0x51

    aput v14, v1, v13

    const/16 v13, -0x33

    aput v13, v1, v12

    const/16 v12, -0x5eb3

    aput v12, v1, v11

    const/16 v11, -0x2f

    aput v11, v1, v10

    const/16 v10, -0x45

    aput v10, v1, v9

    const/16 v9, -0x15d4

    aput v9, v1, v8

    const/16 v8, -0x71

    aput v8, v1, v7

    const/16 v7, 0x11d

    aput v7, v1, v6

    const/16 v6, 0x6668

    aput v6, v1, v5

    const/16 v5, 0x3c15

    aput v5, v1, v4

    const/16 v4, -0x52ae

    aput v4, v1, v3

    const/16 v3, -0x3c

    aput v3, v1, v2

    const/16 v2, 0x2a15

    aput v2, v1, v0

    const/16 v0, 0x38

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, 0x10

    const/16 v19, 0x11

    const/16 v20, 0x12

    const/16 v21, 0x13

    const/16 v22, 0x14

    const/16 v23, 0x15

    const/16 v24, 0x16

    const/16 v25, 0x17

    const/16 v26, 0x18

    const/16 v27, 0x19

    const/16 v28, 0x1a

    const/16 v29, 0x1b

    const/16 v30, 0x1c

    const/16 v31, 0x1d

    const/16 v32, 0x1e

    const/16 v33, 0x1f

    const/16 v34, 0x20

    const/16 v35, 0x21

    const/16 v36, 0x22

    const/16 v37, 0x23

    const/16 v38, 0x24

    const/16 v39, 0x25

    const/16 v40, 0x26

    const/16 v41, 0x27

    const/16 v42, 0x28

    const/16 v43, 0x29

    const/16 v44, 0x2a

    const/16 v45, 0x2b

    const/16 v46, 0x2c

    const/16 v47, 0x2d

    const/16 v48, 0x2e

    const/16 v49, 0x2f

    const/16 v50, 0x30

    const/16 v51, 0x31

    const/16 v52, 0x32

    const/16 v53, 0x33

    const/16 v54, 0x34

    const/16 v55, 0x35

    const/16 v56, 0x36

    const/16 v57, 0x37

    const/16 v58, 0x333f

    aput v58, v0, v57

    const/16 v57, 0x7133

    aput v57, v0, v56

    const/16 v56, 0x6f71

    aput v56, v0, v55

    const/16 v55, -0xe91

    aput v55, v0, v54

    const/16 v54, -0xf

    aput v54, v0, v53

    const/16 v53, -0x17e9

    aput v53, v0, v52

    const/16 v52, -0x18

    aput v52, v0, v51

    const/16 v51, 0x3e4f

    aput v51, v0, v50

    const/16 v50, -0xdc2

    aput v50, v0, v49

    const/16 v49, -0xe

    aput v49, v0, v48

    const/16 v48, 0x485c

    aput v48, v0, v47

    const/16 v47, 0x1048

    aput v47, v0, v46

    const/16 v46, 0x7510

    aput v46, v0, v45

    const/16 v45, -0x438b

    aput v45, v0, v44

    const/16 v44, -0x44

    aput v44, v0, v43

    const/16 v43, -0x64cb

    aput v43, v0, v42

    const/16 v42, -0x65

    aput v42, v0, v41

    const/16 v41, -0x3f

    aput v41, v0, v40

    const/16 v40, 0x1b10

    aput v40, v0, v39

    const/16 v39, -0x2be5

    aput v39, v0, v38

    const/16 v38, -0x2c

    aput v38, v0, v37

    const/16 v37, -0x3ade

    aput v37, v0, v36

    const/16 v36, -0x3b

    aput v36, v0, v35

    const/16 v35, -0x1a

    aput v35, v0, v34

    const/16 v34, -0x2eed

    aput v34, v0, v33

    const/16 v33, -0x2f

    aput v33, v0, v32

    const/16 v32, -0x1581

    aput v32, v0, v31

    const/16 v31, -0x16

    aput v31, v0, v30

    const/16 v30, 0x7156

    aput v30, v0, v29

    const/16 v29, -0x508f

    aput v29, v0, v28

    const/16 v28, -0x51

    aput v28, v0, v27

    const/16 v27, -0x24

    aput v27, v0, v26

    const/16 v26, -0x67ed

    aput v26, v0, v25

    const/16 v25, -0x68

    aput v25, v0, v24

    const/16 v24, 0x422c

    aput v24, v0, v23

    const/16 v23, 0x2e42

    aput v23, v0, v22

    const/16 v22, 0x552e

    aput v22, v0, v21

    const/16 v21, 0x2155

    aput v21, v0, v20

    const/16 v20, 0x2321

    aput v20, v0, v19

    const/16 v19, -0x4edd

    aput v19, v0, v18

    const/16 v18, -0x4f

    aput v18, v0, v17

    const/16 v17, -0x2e83

    aput v17, v0, v16

    const/16 v16, -0x2f

    aput v16, v0, v15

    const/16 v15, -0x25

    aput v15, v0, v14

    const/16 v14, -0x54

    aput v14, v0, v13

    const/16 v13, -0x5ed7

    aput v13, v0, v12

    const/16 v12, -0x5f

    aput v12, v0, v11

    const/16 v11, -0x32

    aput v11, v0, v10

    const/16 v10, -0x15f4

    aput v10, v0, v9

    const/16 v9, -0x16

    aput v9, v0, v8

    const/16 v8, 0x179

    aput v8, v0, v7

    const/16 v7, 0x6601

    aput v7, v0, v6

    const/16 v6, 0x3c66

    aput v6, v0, v5

    const/16 v5, -0x52c4

    aput v5, v0, v4

    const/16 v4, -0x53

    aput v4, v0, v3

    const/16 v3, 0x2a35

    aput v3, v0, v2

    const/4 v2, 0x0

    :goto_0
    array-length v3, v0

    if-lt v2, v3, :cond_0

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_1
    array-length v3, v0

    if-lt v2, v3, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    aget v3, v0, v2

    aget v4, v1, v2

    xor-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    aget v3, v1, v2

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method private refreshDeviceData1(Ljava/util/Collection;)V
    .locals 40
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;",
            ">;)V"
        }
    .end annotation

    const/4 v1, 0x2

    new-array v5, v1, [J

    const/4 v1, 0x1

    const-wide/16 v2, 0x1

    aput-wide v2, v5, v1

    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0x555a

    aput v13, v2, v12

    const/16 v12, -0x3ca

    aput v12, v2, v11

    const/16 v11, -0x6b

    aput v11, v2, v10

    const/16 v10, -0x4e

    aput v10, v2, v9

    const/16 v9, -0x7fd8

    aput v9, v2, v8

    const/16 v8, -0x1b

    aput v8, v2, v7

    const/16 v7, -0x8e4

    aput v7, v2, v6

    const/16 v6, -0x6b

    aput v6, v2, v4

    const/16 v4, 0x525b

    aput v4, v2, v3

    const/16 v3, -0x74c2

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0x553f

    aput v14, v1, v13

    const/16 v13, -0x3ab

    aput v13, v1, v12

    const/4 v12, -0x4

    aput v12, v1, v11

    const/16 v11, -0x3c

    aput v11, v1, v10

    const/16 v10, -0x7fa6

    aput v10, v1, v9

    const/16 v9, -0x80

    aput v9, v1, v8

    const/16 v8, -0x8b1

    aput v8, v1, v7

    const/16 v7, -0x9

    aput v7, v1, v6

    const/16 v6, 0x5232

    aput v6, v1, v4

    const/16 v4, -0x74ae

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v4, v1

    if-lt v3, v4, :cond_0

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v4, v1

    if-lt v3, v4, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    const/16 v1, 0x1a

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, -0x61

    aput v30, v2, v29

    const/16 v29, 0x577a

    aput v29, v2, v28

    const/16 v28, -0x66cd

    aput v28, v2, v27

    const/16 v27, -0x4

    aput v27, v2, v26

    const/16 v26, -0x4d

    aput v26, v2, v25

    const/16 v25, -0xc96

    aput v25, v2, v24

    const/16 v24, -0x6e

    aput v24, v2, v23

    const/16 v23, -0x378e

    aput v23, v2, v22

    const/16 v22, -0x18

    aput v22, v2, v21

    const/16 v21, -0x58

    aput v21, v2, v20

    const/16 v20, -0x60bf

    aput v20, v2, v19

    const/16 v19, -0x2

    aput v19, v2, v18

    const/16 v18, -0x55

    aput v18, v2, v17

    const/16 v17, 0x6466

    aput v17, v2, v16

    const/16 v16, 0x6607

    aput v16, v2, v15

    const/16 v15, 0x470f

    aput v15, v2, v14

    const/16 v14, 0x1a31

    aput v14, v2, v13

    const/16 v13, 0x7e7f

    aput v13, v2, v12

    const/16 v12, 0x3b3a

    aput v12, v2, v11

    const/16 v11, 0x7153

    aput v11, v2, v10

    const/16 v10, 0x7f02

    aput v10, v2, v9

    const/16 v9, 0x7c1a

    aput v9, v2, v8

    const/16 v8, 0x3e0e

    aput v8, v2, v7

    const/16 v7, -0x2da8

    aput v7, v2, v6

    const/16 v6, -0x49

    aput v6, v2, v3

    const/16 v3, -0x21a9

    aput v3, v2, v1

    const/16 v1, 0x1a

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, 0x11

    const/16 v23, 0x12

    const/16 v24, 0x13

    const/16 v25, 0x14

    const/16 v26, 0x15

    const/16 v27, 0x16

    const/16 v28, 0x17

    const/16 v29, 0x18

    const/16 v30, 0x19

    const/16 v31, -0x41

    aput v31, v1, v30

    const/16 v30, 0x575a

    aput v30, v1, v29

    const/16 v29, -0x66a9

    aput v29, v1, v28

    const/16 v28, -0x67

    aput v28, v1, v27

    const/16 v27, -0x21

    aput v27, v1, v26

    const/16 v26, -0xcfa

    aput v26, v1, v25

    const/16 v25, -0xd

    aput v25, v1, v24

    const/16 v24, -0x37ef

    aput v24, v1, v23

    const/16 v23, -0x38

    aput v23, v1, v22

    const/16 v22, -0x37

    aput v22, v1, v21

    const/16 v21, -0x60cb

    aput v21, v1, v20

    const/16 v20, -0x61

    aput v20, v1, v19

    const/16 v19, -0x11

    aput v19, v1, v18

    const/16 v18, 0x6403

    aput v18, v1, v17

    const/16 v17, 0x6664

    aput v17, v1, v16

    const/16 v16, 0x4766

    aput v16, v1, v15

    const/16 v15, 0x1a47

    aput v15, v1, v14

    const/16 v14, 0x7e1a

    aput v14, v1, v13

    const/16 v13, 0x3b7e

    aput v13, v1, v12

    const/16 v12, 0x713b

    aput v12, v1, v11

    const/16 v11, 0x7f71

    aput v11, v1, v10

    const/16 v10, 0x7c7f

    aput v10, v1, v9

    const/16 v9, 0x3e7c

    aput v9, v1, v8

    const/16 v8, -0x2dc2

    aput v8, v1, v7

    const/16 v7, -0x2e

    aput v7, v1, v6

    const/16 v6, -0x21db

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v6, v1

    if-lt v3, v6, :cond_2

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v6, v1

    if-lt v3, v6, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    const/4 v2, 0x0

    array-length v3, v5

    add-int/lit8 v3, v3, -0x1

    aget-wide v3, v5, v3

    long-to-int v3, v3

    if-gtz v3, :cond_4

    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    aget v4, v1, v3

    aget v6, v2, v3

    xor-int/2addr v4, v6

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_1
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_2
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_3
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_4
    const/4 v3, 0x0

    int-to-long v1, v1

    const/16 v4, 0x20

    shl-long/2addr v1, v4

    const/16 v4, 0x20

    ushr-long v6, v1, v4

    aget-wide v1, v5, v3

    const-wide/16 v8, 0x0

    cmp-long v4, v1, v8

    if-eqz v4, :cond_5

    const-wide v8, -0x101ff96bad6677b7L    # -7.775059473742514E230

    xor-long/2addr v1, v8

    :cond_5
    const/16 v4, 0x20

    ushr-long/2addr v1, v4

    const/16 v4, 0x20

    shl-long/2addr v1, v4

    xor-long/2addr v1, v6

    const-wide v6, -0x101ff96bad6677b7L    # -7.775059473742514E230

    xor-long/2addr v1, v6

    aput-wide v1, v5, v3

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mScanningFragmentController:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;

    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-interface/range {p1 .. p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_4
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_14

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    const/16 v2, 0xa

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0x5f78

    aput v15, v3, v14

    const/16 v14, -0x53c4

    aput v14, v3, v13

    const/16 v13, -0x3b

    aput v13, v3, v12

    const/16 v12, -0x21cb

    aput v12, v3, v11

    const/16 v11, -0x54

    aput v11, v3, v10

    const/16 v10, -0x52

    aput v10, v3, v9

    const/16 v9, 0x6223

    aput v9, v3, v8

    const/4 v8, 0x0

    aput v8, v3, v7

    const/16 v7, -0x2a97

    aput v7, v3, v4

    const/16 v4, -0x47

    aput v4, v3, v2

    const/16 v2, 0xa

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/4 v12, 0x6

    const/4 v13, 0x7

    const/16 v14, 0x8

    const/16 v15, 0x9

    const/16 v16, 0x5f1d

    aput v16, v2, v15

    const/16 v15, -0x53a1

    aput v15, v2, v14

    const/16 v14, -0x54

    aput v14, v2, v13

    const/16 v13, -0x21bd

    aput v13, v2, v12

    const/16 v12, -0x22

    aput v12, v2, v11

    const/16 v11, -0x35

    aput v11, v2, v10

    const/16 v10, 0x6270

    aput v10, v2, v9

    const/16 v9, 0x62

    aput v9, v2, v8

    const/16 v8, -0x2b00

    aput v8, v2, v7

    const/16 v7, -0x2b

    aput v7, v2, v4

    const/4 v4, 0x0

    :goto_5
    array-length v7, v2

    if-lt v4, v7, :cond_a

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_6
    array-length v7, v2

    if-lt v4, v7, :cond_b

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v2, 0x15

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v9, 0x2

    const/4 v10, 0x3

    const/4 v11, 0x4

    const/4 v12, 0x5

    const/4 v13, 0x6

    const/4 v14, 0x7

    const/16 v15, 0x8

    const/16 v16, 0x9

    const/16 v17, 0xa

    const/16 v18, 0xb

    const/16 v19, 0xc

    const/16 v20, 0xd

    const/16 v21, 0xe

    const/16 v22, 0xf

    const/16 v23, 0x10

    const/16 v24, 0x11

    const/16 v25, 0x12

    const/16 v26, 0x13

    const/16 v27, 0x14

    const/16 v28, -0x3584

    aput v28, v3, v27

    const/16 v27, -0x16

    aput v27, v3, v26

    const/16 v26, 0x7056

    aput v26, v3, v25

    const/16 v25, -0x51e2

    aput v25, v3, v24

    const/16 v24, -0x25

    aput v24, v3, v23

    const/16 v23, -0x20

    aput v23, v3, v22

    const/16 v22, 0x7460

    aput v22, v3, v21

    const/16 v21, 0x3954

    aput v21, v3, v20

    const/16 v20, 0x405c

    aput v20, v3, v19

    const/16 v19, -0x3bdd

    aput v19, v3, v18

    const/16 v18, -0x53

    aput v18, v3, v17

    const/16 v17, -0x76

    aput v17, v3, v16

    const/16 v16, -0x33

    aput v16, v3, v15

    const/16 v15, -0x3e

    aput v15, v3, v14

    const/16 v14, -0xd

    aput v14, v3, v13

    const/16 v13, -0x6a

    aput v13, v3, v12

    const/16 v12, -0x78

    aput v12, v3, v11

    const/16 v11, 0x6974

    aput v11, v3, v10

    const/16 v10, -0x7800

    aput v10, v3, v9

    const/16 v9, -0x17

    aput v9, v3, v4

    const/16 v4, -0x31

    aput v4, v3, v2

    const/16 v2, 0x15

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/4 v9, 0x1

    const/4 v10, 0x2

    const/4 v11, 0x3

    const/4 v12, 0x4

    const/4 v13, 0x5

    const/4 v14, 0x6

    const/4 v15, 0x7

    const/16 v16, 0x8

    const/16 v17, 0x9

    const/16 v18, 0xa

    const/16 v19, 0xb

    const/16 v20, 0xc

    const/16 v21, 0xd

    const/16 v22, 0xe

    const/16 v23, 0xf

    const/16 v24, 0x10

    const/16 v25, 0x11

    const/16 v26, 0x12

    const/16 v27, 0x13

    const/16 v28, 0x14

    const/16 v29, -0x35bf

    aput v29, v2, v28

    const/16 v28, -0x36

    aput v28, v2, v27

    const/16 v27, 0x7032

    aput v27, v2, v26

    const/16 v26, -0x5190

    aput v26, v2, v25

    const/16 v25, -0x52

    aput v25, v2, v24

    const/16 v24, -0x71

    aput v24, v2, v23

    const/16 v23, 0x7406

    aput v23, v2, v22

    const/16 v22, 0x3974

    aput v22, v2, v21

    const/16 v21, 0x4039

    aput v21, v2, v20

    const/16 v20, -0x3bc0

    aput v20, v2, v19

    const/16 v19, -0x3c

    aput v19, v2, v18

    const/16 v18, -0x4

    aput v18, v2, v17

    const/16 v17, -0x58

    aput v17, v2, v16

    const/16 v16, -0x5a

    aput v16, v2, v15

    const/16 v15, -0x2d

    aput v15, v2, v14

    const/16 v14, -0xe

    aput v14, v2, v13

    const/16 v13, -0x13

    aput v13, v2, v12

    const/16 v12, 0x6906

    aput v12, v2, v11

    const/16 v11, -0x7797

    aput v11, v2, v10

    const/16 v10, -0x78

    aput v10, v2, v9

    const/16 v9, -0x61

    aput v9, v2, v4

    const/4 v4, 0x0

    :goto_7
    array-length v9, v2

    if-lt v4, v9, :cond_c

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_8
    array-length v9, v2

    if-lt v4, v9, :cond_d

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getDeviceName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/16 v2, 0xf

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v9, 0x2

    const/4 v10, 0x3

    const/4 v11, 0x4

    const/4 v12, 0x5

    const/4 v13, 0x6

    const/4 v14, 0x7

    const/16 v15, 0x8

    const/16 v16, 0x9

    const/16 v17, 0xa

    const/16 v18, 0xb

    const/16 v19, 0xc

    const/16 v20, 0xd

    const/16 v21, 0xe

    const/16 v22, -0x319e

    aput v22, v3, v21

    const/16 v21, -0xd

    aput v21, v3, v20

    const/16 v20, 0x4b20

    aput v20, v3, v19

    const/16 v19, 0x382f

    aput v19, v3, v18

    const/16 v18, 0x25d

    aput v18, v3, v17

    const/16 v17, -0x568a

    aput v17, v3, v16

    const/16 v16, -0x36

    aput v16, v3, v15

    const/16 v15, -0x4bca

    aput v15, v3, v14

    const/16 v14, -0x26

    aput v14, v3, v13

    const/16 v13, 0x6630

    aput v13, v3, v12

    const/16 v12, 0x4409

    aput v12, v3, v11

    const/16 v11, 0x907

    aput v11, v3, v10

    const/16 v10, 0x427a

    aput v10, v3, v9

    const/16 v9, -0x38d5

    aput v9, v3, v4

    const/16 v4, -0x19

    aput v4, v3, v2

    const/16 v2, 0xf

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/4 v9, 0x1

    const/4 v10, 0x2

    const/4 v11, 0x3

    const/4 v12, 0x4

    const/4 v13, 0x5

    const/4 v14, 0x6

    const/4 v15, 0x7

    const/16 v16, 0x8

    const/16 v17, 0x9

    const/16 v18, 0xa

    const/16 v19, 0xb

    const/16 v20, 0xc

    const/16 v21, 0xd

    const/16 v22, 0xe

    const/16 v23, -0x31be

    aput v23, v2, v22

    const/16 v22, -0x32

    aput v22, v2, v21

    const/16 v21, 0x4b00

    aput v21, v2, v20

    const/16 v20, 0x384b

    aput v20, v2, v19

    const/16 v19, 0x238

    aput v19, v2, v18

    const/16 v18, -0x56fe

    aput v18, v2, v17

    const/16 v17, -0x57

    aput v17, v2, v16

    const/16 v16, -0x4bad

    aput v16, v2, v15

    const/16 v15, -0x4c

    aput v15, v2, v14

    const/16 v14, 0x665e

    aput v14, v2, v13

    const/16 v13, 0x4466

    aput v13, v2, v12

    const/16 v12, 0x944

    aput v12, v2, v11

    const/16 v11, 0x4209

    aput v11, v2, v10

    const/16 v10, -0x38be

    aput v10, v2, v9

    const/16 v9, -0x39

    aput v9, v2, v4

    const/4 v4, 0x0

    :goto_9
    array-length v9, v2

    if-lt v4, v9, :cond_e

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_a
    array-length v9, v2

    if-lt v4, v9, :cond_f

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->isConnected()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v7, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getShealthSensorDevice()Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    move-result-object v2

    :try_start_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mScanningFragmentController:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;

    new-instance v4, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$FragmentEventListener;

    const/4 v7, 0x1

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v2, v7}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$FragmentEventListener;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;Z)V

    invoke-virtual {v3, v1, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;->joinWithDevice(Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$ScanningFragmentEventListener;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_1 .. :try_end_1} :catch_5

    :goto_b
    const/4 v3, 0x0

    array-length v1, v5

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v5, v1

    long-to-int v1, v1

    if-gtz v1, :cond_10

    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    :catch_0
    move-exception v1

    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, -0xa

    aput v12, v2, v11

    const/16 v11, 0x4a07

    aput v11, v2, v10

    const/16 v10, 0x1923

    aput v10, v2, v9

    const/16 v9, 0x166f

    aput v9, v2, v8

    const/16 v8, 0x2064

    aput v8, v2, v7

    const/16 v7, -0x38bb

    aput v7, v2, v6

    const/16 v6, -0x6c

    aput v6, v2, v5

    const/16 v5, -0x608f

    aput v5, v2, v4

    const/16 v4, -0xa

    aput v4, v2, v3

    const/16 v3, -0x20

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, -0x6d

    aput v13, v1, v12

    const/16 v12, 0x4a64

    aput v12, v1, v11

    const/16 v11, 0x194a

    aput v11, v1, v10

    const/16 v10, 0x1619

    aput v10, v1, v9

    const/16 v9, 0x2016

    aput v9, v1, v8

    const/16 v8, -0x38e0

    aput v8, v1, v7

    const/16 v7, -0x39

    aput v7, v1, v6

    const/16 v6, -0x60ed

    aput v6, v1, v5

    const/16 v5, -0x61

    aput v5, v1, v4

    const/16 v4, -0x74

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_c
    array-length v4, v1

    if-lt v3, v4, :cond_6

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_d
    array-length v4, v1

    if-lt v3, v4, :cond_7

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    const/16 v1, 0x23

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, 0x1d

    const/16 v33, 0x1e

    const/16 v34, 0x1f

    const/16 v35, 0x20

    const/16 v36, 0x21

    const/16 v37, 0x22

    const/16 v38, -0x5ce5

    aput v38, v2, v37

    const/16 v37, -0x31

    aput v37, v2, v36

    const/16 v36, -0x40

    aput v36, v2, v35

    const/16 v35, -0x21

    aput v35, v2, v34

    const/16 v34, -0xe

    aput v34, v2, v33

    const/16 v33, -0x80

    aput v33, v2, v32

    const/16 v32, 0x1e26

    aput v32, v2, v31

    const/16 v31, -0x2c2

    aput v31, v2, v30

    const/16 v30, -0x71

    aput v30, v2, v29

    const/16 v29, -0x17

    aput v29, v2, v28

    const/16 v28, 0x5c22

    aput v28, v2, v27

    const/16 v27, -0x11d0

    aput v27, v2, v26

    const/16 v26, -0x7f

    aput v26, v2, v25

    const/16 v25, -0x62ee

    aput v25, v2, v24

    const/16 v24, -0x17

    aput v24, v2, v23

    const/16 v23, -0x7592

    aput v23, v2, v22

    const/16 v22, -0x1b

    aput v22, v2, v21

    const/16 v21, -0x73

    aput v21, v2, v20

    const/16 v20, -0x5c

    aput v20, v2, v19

    const/16 v19, -0x5e

    aput v19, v2, v18

    const/16 v18, -0x67

    aput v18, v2, v17

    const/16 v17, -0x78

    aput v17, v2, v16

    const/16 v16, -0xa

    aput v16, v2, v15

    const/16 v15, -0x33

    aput v15, v2, v14

    const/16 v14, -0x40b3

    aput v14, v2, v13

    const/4 v13, -0x7

    aput v13, v2, v12

    const/16 v12, -0x62

    aput v12, v2, v11

    const/16 v11, -0x2f

    aput v11, v2, v10

    const/16 v10, -0x78

    aput v10, v2, v9

    const/16 v9, -0x38

    aput v9, v2, v8

    const/16 v8, -0x47

    aput v8, v2, v7

    const/16 v7, -0x5e

    aput v7, v2, v6

    const/16 v6, 0x3a67

    aput v6, v2, v5

    const/16 v5, 0x4169

    aput v5, v2, v3

    const/16 v3, -0x7dd4

    aput v3, v2, v1

    const/16 v1, 0x23

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, 0x1d

    const/16 v34, 0x1e

    const/16 v35, 0x1f

    const/16 v36, 0x20

    const/16 v37, 0x21

    const/16 v38, 0x22

    const/16 v39, -0x5c89

    aput v39, v1, v38

    const/16 v38, -0x5d

    aput v38, v1, v37

    const/16 v37, -0x4b

    aput v37, v1, v36

    const/16 v36, -0x4f

    aput v36, v1, v35

    const/16 v35, -0x2e

    aput v35, v1, v34

    const/16 v34, -0xd

    aput v34, v1, v33

    const/16 v33, 0x1e4f

    aput v33, v1, v32

    const/16 v32, -0x2e2

    aput v32, v1, v31

    const/16 v31, -0x3

    aput v31, v1, v30

    const/16 v30, -0x74

    aput v30, v1, v29

    const/16 v29, 0x5c4e

    aput v29, v1, v28

    const/16 v28, -0x11a4

    aput v28, v1, v27

    const/16 v27, -0x12

    aput v27, v1, v26

    const/16 v26, -0x62a0

    aput v26, v1, v25

    const/16 v25, -0x63

    aput v25, v1, v24

    const/16 v24, -0x7600

    aput v24, v1, v23

    const/16 v23, -0x76

    aput v23, v1, v22

    const/16 v22, -0x32

    aput v22, v1, v21

    const/16 v21, -0x30

    aput v21, v1, v20

    const/16 v20, -0x34

    aput v20, v1, v19

    const/16 v19, -0x4

    aput v19, v1, v18

    const/16 v18, -0x1b

    aput v18, v1, v17

    const/16 v17, -0x6f

    aput v17, v1, v16

    const/16 v16, -0x54

    aput v16, v1, v15

    const/16 v15, -0x40c1

    aput v15, v1, v14

    const/16 v14, -0x41

    aput v14, v1, v13

    const/4 v13, -0x7

    aput v13, v1, v12

    const/16 v12, -0x41

    aput v12, v1, v11

    const/16 v11, -0x1f

    aput v11, v1, v10

    const/16 v10, -0x5a

    aput v10, v1, v9

    const/16 v9, -0x29

    aput v9, v1, v8

    const/16 v8, -0x3d

    aput v8, v1, v7

    const/16 v7, 0x3a04

    aput v7, v1, v6

    const/16 v6, 0x413a

    aput v6, v1, v5

    const/16 v5, -0x7dbf

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_e
    array-length v5, v1

    if-lt v3, v5, :cond_8

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_f
    array-length v5, v1

    if-lt v3, v5, :cond_9

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    :goto_10
    return-void

    :cond_6
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_c

    :cond_7
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_d

    :cond_8
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_e

    :cond_9
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_f

    :cond_a
    aget v7, v2, v4

    aget v8, v3, v4

    xor-int/2addr v7, v8

    aput v7, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_5

    :cond_b
    aget v7, v3, v4

    int-to-char v7, v7

    aput-char v7, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_6

    :cond_c
    aget v9, v2, v4

    aget v10, v3, v4

    xor-int/2addr v9, v10

    aput v9, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_7

    :cond_d
    aget v9, v3, v4

    int-to-char v9, v9

    aput-char v9, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_8

    :cond_e
    aget v9, v2, v4

    aget v10, v3, v4

    xor-int/2addr v9, v10

    aput v9, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_9

    :cond_f
    aget v9, v3, v4

    int-to-char v9, v9

    aput-char v9, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_a

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_b

    :catch_2
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto/16 :goto_b

    :catch_3
    move-exception v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto/16 :goto_b

    :catch_4
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto/16 :goto_b

    :catch_5
    move-exception v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;->printStackTrace()V

    goto/16 :goto_b

    :cond_10
    const/4 v1, 0x0

    aget-wide v1, v5, v1

    const-wide/16 v7, 0x0

    cmp-long v4, v1, v7

    if-eqz v4, :cond_11

    const-wide v7, -0x101ff96bad6677b7L    # -7.775059473742514E230

    xor-long/2addr v1, v7

    :cond_11
    const/16 v4, 0x20

    shl-long/2addr v1, v4

    const/16 v4, 0x20

    shr-long/2addr v1, v4

    long-to-int v1, v1

    add-int/lit8 v1, v1, 0x1

    array-length v2, v5

    add-int/lit8 v2, v2, -0x1

    aget-wide v7, v5, v2

    long-to-int v2, v7

    if-gtz v2, :cond_12

    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_12
    const/4 v3, 0x0

    int-to-long v1, v1

    const/16 v4, 0x20

    shl-long/2addr v1, v4

    const/16 v4, 0x20

    ushr-long v7, v1, v4

    aget-wide v1, v5, v3

    const-wide/16 v9, 0x0

    cmp-long v4, v1, v9

    if-eqz v4, :cond_13

    const-wide v9, -0x101ff96bad6677b7L    # -7.775059473742514E230

    xor-long/2addr v1, v9

    :cond_13
    const/16 v4, 0x20

    ushr-long/2addr v1, v4

    const/16 v4, 0x20

    shl-long/2addr v1, v4

    xor-long/2addr v1, v7

    const-wide v7, -0x101ff96bad6677b7L    # -7.775059473742514E230

    xor-long/2addr v1, v7

    aput-wide v1, v5, v3

    goto/16 :goto_4

    :cond_14
    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, -0x1d4

    aput v12, v2, v11

    const/16 v11, -0x63

    aput v11, v2, v10

    const/16 v10, -0x3ad

    aput v10, v2, v9

    const/16 v9, -0x76

    aput v9, v2, v8

    const/16 v8, -0x14

    aput v8, v2, v7

    const/16 v7, -0x11

    aput v7, v2, v6

    const/16 v6, -0x21

    aput v6, v2, v5

    const/16 v5, -0x5e

    aput v5, v2, v4

    const/16 v4, -0x7ee8

    aput v4, v2, v3

    const/16 v3, -0x13

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, -0x1b7

    aput v13, v1, v12

    const/4 v12, -0x2

    aput v12, v1, v11

    const/16 v11, -0x3c6

    aput v11, v1, v10

    const/4 v10, -0x4

    aput v10, v1, v9

    const/16 v9, -0x62

    aput v9, v1, v8

    const/16 v8, -0x76

    aput v8, v1, v7

    const/16 v7, -0x74

    aput v7, v1, v6

    const/16 v6, -0x40

    aput v6, v1, v5

    const/16 v5, -0x7e8f

    aput v5, v1, v4

    const/16 v4, -0x7f

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_11
    array-length v4, v1

    if-lt v3, v4, :cond_15

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_12
    array-length v4, v1

    if-lt v3, v4, :cond_16

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    const/16 v1, 0x17

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, -0x7e

    aput v26, v2, v25

    const/16 v25, -0x8

    aput v25, v2, v24

    const/16 v24, -0x19

    aput v24, v2, v23

    const/16 v23, 0x185b

    aput v23, v2, v22

    const/16 v22, -0x38af

    aput v22, v2, v21

    const/16 v21, -0x6f

    aput v21, v2, v20

    const/16 v20, -0x6cb

    aput v20, v2, v19

    const/16 v19, -0x43

    aput v19, v2, v18

    const/16 v18, -0x74

    aput v18, v2, v17

    const/16 v17, -0x27

    aput v17, v2, v16

    const/16 v16, -0x43e6

    aput v16, v2, v15

    const/16 v15, -0x12

    aput v15, v2, v14

    const/16 v14, -0x24c0

    aput v14, v2, v13

    const/16 v13, -0x66

    aput v13, v2, v12

    const/16 v12, -0x47

    aput v12, v2, v11

    const/16 v11, 0x1e52

    aput v11, v2, v10

    const/16 v10, 0x7147

    aput v10, v2, v9

    const/16 v9, -0x39d0

    aput v9, v2, v8

    const/16 v8, -0x76

    aput v8, v2, v7

    const/16 v7, 0x1b2d

    aput v7, v2, v6

    const/16 v6, 0x7848

    aput v6, v2, v5

    const/16 v5, 0x1b31

    aput v5, v2, v3

    const/16 v3, 0x7f5f

    aput v3, v2, v1

    const/16 v1, 0x17

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, -0x5e

    aput v27, v1, v26

    const/16 v26, -0x55

    aput v26, v1, v25

    const/16 v25, -0x5e

    aput v25, v1, v24

    const/16 v24, 0x1818

    aput v24, v1, v23

    const/16 v23, -0x38e8

    aput v23, v1, v22

    const/16 v22, -0x39

    aput v22, v1, v21

    const/16 v21, -0x690

    aput v21, v1, v20

    const/16 v20, -0x7

    aput v20, v1, v19

    const/16 v19, -0x2d

    aput v19, v1, v18

    const/16 v18, -0x63

    aput v18, v1, v17

    const/16 v17, -0x43a1

    aput v17, v1, v16

    const/16 v16, -0x44

    aput v16, v1, v15

    const/16 v15, -0x24f7

    aput v15, v1, v14

    const/16 v14, -0x25

    aput v14, v1, v13

    const/16 v13, -0x17

    aput v13, v1, v12

    const/16 v12, 0x1e0d

    aput v12, v1, v11

    const/16 v11, 0x711e

    aput v11, v1, v10

    const/16 v10, -0x398f

    aput v10, v1, v9

    const/16 v9, -0x3a

    aput v9, v1, v8

    const/16 v8, 0x1b7d

    aput v8, v1, v7

    const/16 v7, 0x781b

    aput v7, v1, v6

    const/16 v6, 0x1b78

    aput v6, v1, v5

    const/16 v5, 0x7f1b

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_13
    array-length v5, v1

    if-lt v3, v5, :cond_17

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_14
    array-length v5, v1

    if-lt v3, v5, :cond_18

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->handler:Landroid/os/Handler;

    const/16 v2, 0x65

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->handler:Landroid/os/Handler;

    const/16 v3, 0x65

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_10

    :cond_15
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_11

    :cond_16
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_12

    :cond_17
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_13

    :cond_18
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_14
.end method

.method private setDrawable(Landroid/view/View;Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;)V
    .locals 3

    sget v0, Lcom/sec/android/app/shealth/framework/ui/R$id;->device_image_name:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mDeviceResMap:Ljava/util/HashMap;

    invoke-virtual {p2}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getDeviceType()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_0
    return-void

    :catch_0
    move-exception v1

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$drawable;->s_health_noitem:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method private setNoDeviceImage()V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->nodevicesImage:Landroid/widget/ImageView;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$drawable;->s_health_noitem:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    return-void
.end method

.method private setScanningViewsVisibility(Z)V
    .locals 4

    const/4 v3, 0x0

    const/16 v2, 0x8

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->rootview:Landroid/view/View;

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->rootview:Landroid/view/View;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$id;->detected_devices_header:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->rootview:Landroid/view/View;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$id;->scanButtonLayout:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->rootview:Landroid/view/View;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$id;->Nodevices:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->rootview:Landroid/view/View;

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->rootview:Landroid/view/View;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$id;->scannedlist:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->rootview:Landroid/view/View;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$id;->detected_devices_header:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->rootview:Landroid/view/View;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$id;->scanButtonLayout:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->rootview:Landroid/view/View;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$id;->Nodevices:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method private showAlreadyConnectedDialog()V
    .locals 12

    const/4 v11, 0x4

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$string;->alert:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-static {}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->tzn0VLwU()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$16;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$16;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v5

    const/4 v0, 0x7

    new-array v1, v0, [I

    const/4 v0, 0x5

    const/4 v2, 0x6

    const/16 v6, -0x70

    aput v6, v1, v2

    const/16 v2, 0x406

    aput v2, v1, v0

    const/16 v0, 0x3461

    aput v0, v1, v11

    const/16 v0, 0xb5a

    aput v0, v1, v10

    const/16 v0, 0x3a65

    aput v0, v1, v9

    const/16 v0, 0x4855

    aput v0, v1, v8

    const/16 v0, 0x502b

    aput v0, v1, v3

    const/4 v0, 0x7

    new-array v0, v0, [I

    const/4 v2, 0x5

    const/4 v6, 0x6

    const/16 v7, -0x1c

    aput v7, v0, v6

    const/16 v6, 0x465

    aput v6, v0, v2

    const/16 v2, 0x3404

    aput v2, v0, v11

    const/16 v2, 0xb34

    aput v2, v0, v10

    const/16 v2, 0x3a0b

    aput v2, v0, v9

    const/16 v2, 0x483a

    aput v2, v0, v8

    const/16 v2, 0x5048

    aput v2, v0, v3

    move v2, v3

    :goto_0
    array-length v6, v0

    if-lt v2, v6, :cond_0

    array-length v0, v1

    new-array v0, v0, [C

    :goto_1
    array-length v2, v0

    if-lt v3, v2, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v5, v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void

    :cond_0
    aget v6, v0, v2

    aget v7, v1, v2

    xor-int/2addr v6, v7

    aput v6, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    aget v2, v1, v3

    int-to-char v2, v2

    aput-char v2, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method private showLoadingPopUp(Z)V
    .locals 2

    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$24;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$24;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->syncProgressDlg:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->syncProgressDlg:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;->setCancelable(Z)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->syncProgressDlg:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$25;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$25;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Z)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->syncProgressDlg:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$26;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$26;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->syncProgressDlg:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;->show()V

    return-void
.end method

.method private showToast(Ljava/lang/String;)V
    .locals 13

    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v3, 0x0

    const/16 v0, 0xa

    new-array v1, v0, [I

    const/4 v0, 0x5

    const/4 v2, 0x6

    const/4 v4, 0x7

    const/16 v5, 0x8

    const/16 v6, 0x9

    const/16 v7, -0x5f

    aput v7, v1, v6

    const/16 v6, -0x29

    aput v6, v1, v5

    const/16 v5, -0x64

    aput v5, v1, v4

    const/16 v4, -0x56a6

    aput v4, v1, v2

    const/16 v2, -0x25

    aput v2, v1, v0

    const/16 v0, -0x6fbe

    aput v0, v1, v12

    const/16 v0, -0x3d

    aput v0, v1, v11

    const/16 v0, -0x43c3

    aput v0, v1, v10

    const/16 v0, -0x2b

    aput v0, v1, v9

    const/16 v0, -0x12

    aput v0, v1, v3

    const/16 v0, 0xa

    new-array v0, v0, [I

    const/4 v2, 0x5

    const/4 v4, 0x6

    const/4 v5, 0x7

    const/16 v6, 0x8

    const/16 v7, 0x9

    const/16 v8, -0x3c

    aput v8, v0, v7

    const/16 v7, -0x4c

    aput v7, v0, v6

    const/16 v6, -0xb

    aput v6, v0, v5

    const/16 v5, -0x56d4

    aput v5, v0, v4

    const/16 v4, -0x57

    aput v4, v0, v2

    const/16 v2, -0x6fd9

    aput v2, v0, v12

    const/16 v2, -0x70

    aput v2, v0, v11

    const/16 v2, -0x43a1

    aput v2, v0, v10

    const/16 v2, -0x44

    aput v2, v0, v9

    const/16 v2, -0x7e

    aput v2, v0, v3

    move v2, v3

    :goto_0
    array-length v4, v0

    if-lt v2, v4, :cond_0

    array-length v0, v1

    new-array v0, v0, [C

    move v2, v3

    :goto_1
    array-length v4, v0

    if-lt v2, v4, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v0, 0x8

    new-array v1, v0, [I

    const/4 v0, 0x5

    const/4 v2, 0x6

    const/4 v6, 0x7

    const/16 v7, 0x215

    aput v7, v1, v6

    const/16 v6, -0x76c8

    aput v6, v1, v2

    const/16 v2, -0x57

    aput v2, v1, v0

    const/16 v0, 0x1a33

    aput v0, v1, v12

    const/16 v0, 0x49

    aput v0, v1, v11

    const/16 v0, -0x1ebf

    aput v0, v1, v10

    const/16 v0, -0x52

    aput v0, v1, v9

    const/16 v0, -0x39

    aput v0, v1, v3

    const/16 v0, 0x8

    new-array v0, v0, [I

    const/4 v2, 0x5

    const/4 v6, 0x6

    const/4 v7, 0x7

    const/16 v8, 0x235

    aput v8, v0, v7

    const/16 v7, -0x76fe

    aput v7, v0, v6

    const/16 v6, -0x77

    aput v6, v0, v2

    const/16 v2, 0x1a67

    aput v2, v0, v12

    const/16 v2, 0x1a

    aput v2, v0, v11

    const/16 v2, -0x1f00

    aput v2, v0, v10

    const/16 v2, -0x1f

    aput v2, v0, v9

    const/16 v2, -0x6d

    aput v2, v0, v3

    move v2, v3

    :goto_2
    array-length v6, v0

    if-lt v2, v6, :cond_2

    array-length v0, v1

    new-array v0, v0, [C

    :goto_3
    array-length v2, v0

    if-lt v3, v2, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$23;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$23;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    :goto_4
    return-void

    :cond_0
    aget v4, v0, v2

    aget v5, v1, v2

    xor-int/2addr v4, v5

    aput v4, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    :cond_1
    aget v4, v1, v2

    int-to-char v4, v4

    aput-char v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1

    :cond_2
    aget v6, v0, v2

    aget v7, v1, v2

    xor-int/2addr v6, v7

    aput v6, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_3
    aget v2, v1, v3

    int-to-char v2, v2

    aput-char v2, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :catch_0
    move-exception v0

    goto :goto_4
.end method

.method private startDiscoveringSensorDevices()V
    .locals 40

    const/16 v1, 0xa

    :try_start_0
    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, -0xd

    aput v12, v2, v11

    const/16 v11, -0x1cc7

    aput v11, v2, v10

    const/16 v10, -0x76

    aput v10, v2, v9

    const/16 v9, -0x1f

    aput v9, v2, v8

    const/16 v8, 0xd3c

    aput v8, v2, v7

    const/16 v7, -0x4098

    aput v7, v2, v6

    const/16 v6, -0x14

    aput v6, v2, v5

    const/16 v5, -0x5c

    aput v5, v2, v4

    const/4 v4, -0x5

    aput v4, v2, v3

    const/16 v3, -0x22d9

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, -0x6a

    aput v13, v1, v12

    const/16 v12, -0x1ca6

    aput v12, v1, v11

    const/16 v11, -0x1d

    aput v11, v1, v10

    const/16 v10, -0x69

    aput v10, v1, v9

    const/16 v9, 0xd4e

    aput v9, v1, v8

    const/16 v8, -0x40f3

    aput v8, v1, v7

    const/16 v7, -0x41

    aput v7, v1, v6

    const/16 v6, -0x3a

    aput v6, v1, v5

    const/16 v5, -0x6e

    aput v5, v1, v4

    const/16 v4, -0x22b5

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v4, v1

    if-lt v3, v4, :cond_0

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v4, v1

    if-lt v3, v4, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    const/16 v1, 0xf

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, -0xb

    aput v18, v2, v17

    const/16 v17, -0x72a0

    aput v17, v2, v16

    const/16 v16, -0x4

    aput v16, v2, v15

    const/16 v15, -0x54b1

    aput v15, v2, v14

    const/16 v14, -0x27

    aput v14, v2, v13

    const/16 v13, 0x712f

    aput v13, v2, v12

    const/16 v12, -0x77e1

    aput v12, v2, v11

    const/16 v11, -0x17

    aput v11, v2, v10

    const/16 v10, 0x5e13

    aput v10, v2, v9

    const/16 v9, -0x18f3

    aput v9, v2, v8

    const/16 v8, -0x6d

    aput v8, v2, v7

    const/16 v7, 0x5b31

    aput v7, v2, v6

    const/16 v6, 0xc3a

    aput v6, v2, v5

    const/16 v5, -0x7b88

    aput v5, v2, v3

    const/16 v3, -0x9

    aput v3, v2, v1

    const/16 v1, 0xf

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, -0x25

    aput v19, v1, v18

    const/16 v18, -0x72b2

    aput v18, v1, v17

    const/16 v17, -0x73

    aput v17, v1, v16

    const/16 v16, -0x54d6

    aput v16, v1, v15

    const/16 v15, -0x55

    aput v15, v1, v14

    const/16 v14, 0x710f

    aput v14, v1, v13

    const/16 v13, -0x778f

    aput v13, v1, v12

    const/16 v12, -0x78

    aput v12, v1, v11

    const/16 v11, 0x5e70

    aput v11, v1, v10

    const/16 v10, -0x18a2

    aput v10, v1, v9

    const/16 v9, -0x19

    aput v9, v1, v8

    const/16 v8, 0x5b43

    aput v8, v1, v7

    const/16 v7, 0xc5b

    aput v7, v1, v6

    const/16 v6, -0x7bf4

    aput v6, v1, v5

    const/16 v5, -0x7c

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v5, v1

    if-lt v3, v5, :cond_2

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v5, v1

    if-lt v3, v5, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mScanningFragmentController:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_0 .. :try_end_0} :catch_4

    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_1 .. :try_end_1} :catch_4

    :try_start_2
    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mDataType:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_4

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mScanningFragmentController:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;

    const/4 v2, 0x1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mDeviceTypes:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mDataType:I

    const/16 v5, 0x28

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mScanningFragmentScanListener:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$ScanningFragmentScanListener;

    invoke-virtual/range {v1 .. v6}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;->startScan(ILjava/util/ArrayList;IILcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$ScanningFragmentScanListener;)V

    :goto_4
    return-void

    :cond_0
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_1
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_2
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_3
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mScanningFragmentController:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mDeviceTypes:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mDataType:I

    const/16 v5, 0x28

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mScanningFragmentScanListener:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$ScanningFragmentScanListener;

    invoke-virtual/range {v1 .. v6}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;->startScan(ILjava/util/ArrayList;IILcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$ScanningFragmentScanListener;)V
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_2 .. :try_end_2} :catch_4

    goto :goto_4

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_4

    :catch_1
    move-exception v1

    const/16 v1, 0xa

    :try_start_3
    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0x421d

    aput v12, v2, v11

    const/16 v11, -0x14df

    aput v11, v2, v10

    const/16 v10, -0x7e

    aput v10, v2, v9

    const/16 v9, -0x45

    aput v9, v2, v8

    const/16 v8, -0x58f6

    aput v8, v2, v7

    const/16 v7, -0x3e

    aput v7, v2, v6

    const/16 v6, 0x3231

    aput v6, v2, v5

    const/16 v5, 0x4e50

    aput v5, v2, v4

    const/16 v4, 0x2127

    aput v4, v2, v3

    const/16 v3, -0x1fb3

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0x4278

    aput v13, v1, v12

    const/16 v12, -0x14be

    aput v12, v1, v11

    const/16 v11, -0x15

    aput v11, v1, v10

    const/16 v10, -0x33

    aput v10, v1, v9

    const/16 v9, -0x5888

    aput v9, v1, v8

    const/16 v8, -0x59

    aput v8, v1, v7

    const/16 v7, 0x3262

    aput v7, v1, v6

    const/16 v6, 0x4e32

    aput v6, v1, v5

    const/16 v5, 0x214e

    aput v5, v1, v4

    const/16 v4, -0x1fdf

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_5
    array-length v4, v1

    if-lt v3, v4, :cond_5

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_6
    array-length v4, v1

    if-lt v3, v4, :cond_6

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    const/16 v1, 0x23

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, 0x1d

    const/16 v33, 0x1e

    const/16 v34, 0x1f

    const/16 v35, 0x20

    const/16 v36, 0x21

    const/16 v37, 0x22

    const/16 v38, -0x5e

    aput v38, v2, v37

    const/16 v37, -0x51

    aput v37, v2, v36

    const/16 v36, -0x21

    aput v36, v2, v35

    const/16 v35, 0x6142

    aput v35, v2, v34

    const/16 v34, 0x6341

    aput v34, v2, v33

    const/16 v33, 0x1c10

    aput v33, v2, v32

    const/16 v32, -0x738b

    aput v32, v2, v31

    const/16 v31, -0x54

    aput v31, v2, v30

    const/16 v30, -0x7c

    aput v30, v2, v29

    const/16 v29, 0x6401

    aput v29, v2, v28

    const/16 v28, -0x5ef8

    aput v28, v2, v27

    const/16 v27, -0x33

    aput v27, v2, v26

    const/16 v26, -0x1ef6

    aput v26, v2, v25

    const/16 v25, -0x6d

    aput v25, v2, v24

    const/16 v24, -0x7e

    aput v24, v2, v23

    const/16 v23, -0x67c1

    aput v23, v2, v22

    const/16 v22, -0x9

    aput v22, v2, v21

    const/16 v21, -0x33

    aput v21, v2, v20

    const/16 v20, -0x51bd

    aput v20, v2, v19

    const/16 v19, -0x40

    aput v19, v2, v18

    const/16 v18, -0x18f6

    aput v18, v2, v17

    const/16 v17, -0x76

    aput v17, v2, v16

    const/16 v16, -0x26

    aput v16, v2, v15

    const/16 v15, -0x3a

    aput v15, v2, v14

    const/16 v14, -0x2c

    aput v14, v2, v13

    const/16 v13, 0x942

    aput v13, v2, v12

    const/16 v12, 0x2c6e

    aput v12, v2, v11

    const/16 v11, 0x4242

    aput v11, v2, v10

    const/16 v10, 0x122b

    aput v10, v2, v9

    const/16 v9, -0x6184

    aput v9, v2, v8

    const/16 v8, -0x10

    aput v8, v2, v7

    const/16 v7, 0x1764

    aput v7, v2, v6

    const/16 v6, -0x118c

    aput v6, v2, v5

    const/16 v5, -0x43

    aput v5, v2, v3

    const/16 v3, -0x4dfb

    aput v3, v2, v1

    const/16 v1, 0x23

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, 0x1d

    const/16 v34, 0x1e

    const/16 v35, 0x1f

    const/16 v36, 0x20

    const/16 v37, 0x21

    const/16 v38, 0x22

    const/16 v39, -0x32

    aput v39, v1, v38

    const/16 v38, -0x3d

    aput v38, v1, v37

    const/16 v37, -0x56

    aput v37, v1, v36

    const/16 v36, 0x612c

    aput v36, v1, v35

    const/16 v35, 0x6361

    aput v35, v1, v34

    const/16 v34, 0x1c63

    aput v34, v1, v33

    const/16 v33, -0x73e4

    aput v33, v1, v32

    const/16 v32, -0x74

    aput v32, v1, v31

    const/16 v31, -0xa

    aput v31, v1, v30

    const/16 v30, 0x6464

    aput v30, v1, v29

    const/16 v29, -0x5e9c

    aput v29, v1, v28

    const/16 v28, -0x5f

    aput v28, v1, v27

    const/16 v27, -0x1e9b

    aput v27, v1, v26

    const/16 v26, -0x1f

    aput v26, v1, v25

    const/16 v25, -0xa

    aput v25, v1, v24

    const/16 v24, -0x67af

    aput v24, v1, v23

    const/16 v23, -0x68

    aput v23, v1, v22

    const/16 v22, -0x72

    aput v22, v1, v21

    const/16 v21, -0x51c9

    aput v21, v1, v20

    const/16 v20, -0x52

    aput v20, v1, v19

    const/16 v19, -0x1891

    aput v19, v1, v18

    const/16 v18, -0x19

    aput v18, v1, v17

    const/16 v17, -0x43

    aput v17, v1, v16

    const/16 v16, -0x59

    aput v16, v1, v15

    const/16 v15, -0x5a

    aput v15, v1, v14

    const/16 v14, 0x904

    aput v14, v1, v13

    const/16 v13, 0x2c09

    aput v13, v1, v12

    const/16 v12, 0x422c

    aput v12, v1, v11

    const/16 v11, 0x1242

    aput v11, v1, v10

    const/16 v10, -0x61ee

    aput v10, v1, v9

    const/16 v9, -0x62

    aput v9, v1, v8

    const/16 v8, 0x1705

    aput v8, v1, v7

    const/16 v7, -0x11e9

    aput v7, v1, v6

    const/16 v6, -0x12

    aput v6, v1, v5

    const/16 v5, -0x4d98

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_7
    array-length v5, v1

    if-lt v3, v5, :cond_7

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_8
    array-length v5, v1

    if-lt v3, v5, :cond_8

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_3 .. :try_end_3} :catch_4

    goto/16 :goto_4

    :catch_2
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_4

    :cond_5
    :try_start_4
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_5

    :cond_6
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_6

    :cond_7
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_7

    :cond_8
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3
    :try_end_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_4 .. :try_end_4} :catch_4

    add-int/lit8 v3, v3, 0x1

    goto :goto_8

    :catch_3
    move-exception v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto/16 :goto_4

    :catch_4
    move-exception v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;->printStackTrace()V

    goto/16 :goto_4
.end method

.method private startServiceIfNeeded()V
    .locals 44

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mScanningFragmentController:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;

    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0x7166

    aput v12, v2, v11

    const/16 v11, -0x2bee

    aput v11, v2, v10

    const/16 v10, -0x43

    aput v10, v2, v9

    const/16 v9, -0x69c

    aput v9, v2, v8

    const/16 v8, -0x75

    aput v8, v2, v7

    const/16 v7, 0x4302

    aput v7, v2, v6

    const/16 v6, -0x27f0

    aput v6, v2, v5

    const/16 v5, -0x46

    aput v5, v2, v4

    const/16 v4, -0x26

    aput v4, v2, v3

    const/16 v3, 0x2e69

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0x7103

    aput v13, v1, v12

    const/16 v12, -0x2b8f

    aput v12, v1, v11

    const/16 v11, -0x2c

    aput v11, v1, v10

    const/16 v10, -0x6ee

    aput v10, v1, v9

    const/4 v9, -0x7

    aput v9, v1, v8

    const/16 v8, 0x4367

    aput v8, v1, v7

    const/16 v7, -0x27bd

    aput v7, v1, v6

    const/16 v6, -0x28

    aput v6, v1, v5

    const/16 v5, -0x4d

    aput v5, v1, v4

    const/16 v4, 0x2e05

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v4, v1

    if-lt v3, v4, :cond_0

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v4, v1

    if-lt v3, v4, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    const/16 v1, 0x27

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, 0x1d

    const/16 v33, 0x1e

    const/16 v34, 0x1f

    const/16 v35, 0x20

    const/16 v36, 0x21

    const/16 v37, 0x22

    const/16 v38, 0x23

    const/16 v39, 0x24

    const/16 v40, 0x25

    const/16 v41, 0x26

    const/16 v42, -0x3c

    aput v42, v2, v41

    const/16 v41, 0x4f02

    aput v41, v2, v40

    const/16 v40, 0x163a

    aput v40, v2, v39

    const/16 v39, 0x4f78

    aput v39, v2, v38

    const/16 v38, -0x1e91

    aput v38, v2, v37

    const/16 v37, -0x6b

    aput v37, v2, v36

    const/16 v36, 0x453e

    aput v36, v2, v35

    const/16 v35, -0x2d5

    aput v35, v2, v34

    const/16 v34, -0x23

    aput v34, v2, v33

    const/16 v33, -0x13

    aput v33, v2, v32

    const/16 v32, -0x20

    aput v32, v2, v31

    const/16 v31, 0x1d23

    aput v31, v2, v30

    const/16 v30, 0x706f

    aput v30, v2, v29

    const/16 v29, 0x915

    aput v29, v2, v28

    const/16 v28, -0x549b

    aput v28, v2, v27

    const/16 v27, -0x39

    aput v27, v2, v26

    const/16 v26, 0x4c13

    aput v26, v2, v25

    const/16 v25, 0x6b3e

    aput v25, v2, v24

    const/16 v24, -0x53e1

    aput v24, v2, v23

    const/16 v23, -0x3e

    aput v23, v2, v22

    const/16 v22, 0x6316

    aput v22, v2, v21

    const/16 v21, 0x1820

    aput v21, v2, v20

    const/16 v20, -0x4b94

    aput v20, v2, v19

    const/16 v19, -0x26

    aput v19, v2, v18

    const/16 v18, -0x6e

    aput v18, v2, v17

    const/16 v17, -0x15ad

    aput v17, v2, v16

    const/16 v16, -0x73

    aput v16, v2, v15

    const/16 v15, -0x6a

    aput v15, v2, v14

    const/16 v14, -0x8f8

    aput v14, v2, v13

    const/16 v13, -0x4f

    aput v13, v2, v12

    const/16 v12, -0x7db9

    aput v12, v2, v11

    const/16 v11, -0x14

    aput v11, v2, v10

    const/16 v10, -0x7e

    aput v10, v2, v9

    const/16 v9, -0x4b7

    aput v9, v2, v8

    const/16 v8, -0x6b

    aput v8, v2, v7

    const/16 v7, -0x3acc

    aput v7, v2, v6

    const/16 v6, -0x5a

    aput v6, v2, v5

    const/16 v5, -0x3a

    aput v5, v2, v3

    const/16 v3, -0x53

    aput v3, v2, v1

    const/16 v1, 0x27

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, 0x1d

    const/16 v34, 0x1e

    const/16 v35, 0x1f

    const/16 v36, 0x20

    const/16 v37, 0x21

    const/16 v38, 0x22

    const/16 v39, 0x23

    const/16 v40, 0x24

    const/16 v41, 0x25

    const/16 v42, 0x26

    const/16 v43, -0x58

    aput v43, v1, v42

    const/16 v42, 0x4f6e

    aput v42, v1, v41

    const/16 v41, 0x164f

    aput v41, v1, v40

    const/16 v40, 0x4f16

    aput v40, v1, v39

    const/16 v39, -0x1eb1

    aput v39, v1, v38

    const/16 v38, -0x1f

    aput v38, v1, v37

    const/16 v37, 0x4551

    aput v37, v1, v36

    const/16 v36, -0x2bb

    aput v36, v1, v35

    const/16 v35, -0x3

    aput v35, v1, v34

    const/16 v34, -0x62

    aput v34, v1, v33

    const/16 v33, -0x77

    aput v33, v1, v32

    const/16 v32, 0x1d03

    aput v32, v1, v31

    const/16 v31, 0x701d

    aput v31, v1, v30

    const/16 v30, 0x970

    aput v30, v1, v29

    const/16 v29, -0x54f7

    aput v29, v1, v28

    const/16 v28, -0x55

    aput v28, v1, v27

    const/16 v27, 0x4c7c

    aput v27, v1, v26

    const/16 v26, 0x6b4c

    aput v26, v1, v25

    const/16 v25, -0x5395

    aput v25, v1, v24

    const/16 v24, -0x54

    aput v24, v1, v23

    const/16 v23, 0x6379

    aput v23, v1, v22

    const/16 v22, 0x1863

    aput v22, v1, v21

    const/16 v21, -0x4be8

    aput v21, v1, v20

    const/16 v20, -0x4c

    aput v20, v1, v19

    const/16 v19, -0x9

    aput v19, v1, v18

    const/16 v18, -0x15c2

    aput v18, v1, v17

    const/16 v17, -0x16

    aput v17, v1, v16

    const/16 v16, -0x9

    aput v16, v1, v15

    const/16 v15, -0x886

    aput v15, v1, v14

    const/16 v14, -0x9

    aput v14, v1, v13

    const/16 v13, -0x7de0

    aput v13, v1, v12

    const/16 v12, -0x7e

    aput v12, v1, v11

    const/16 v11, -0x15

    aput v11, v1, v10

    const/16 v10, -0x4d9

    aput v10, v1, v9

    const/4 v9, -0x5

    aput v9, v1, v8

    const/16 v8, -0x3aab

    aput v8, v1, v7

    const/16 v7, -0x3b

    aput v7, v1, v6

    const/16 v6, -0x6b

    aput v6, v1, v5

    const/16 v5, -0x40

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v5, v1

    if-lt v3, v5, :cond_2

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v5, v1

    if-lt v3, v5, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    :goto_4
    return-void

    :catch_0
    move-exception v1

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mSensorConnectionListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mScanningFragmentController:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;

    goto :goto_4

    :cond_0
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_1
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_2
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_3
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3
.end method

.method private stopDiscoveringSensorDevices()V
    .locals 40

    :try_start_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mScanningFragmentController:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_2

    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_1 .. :try_end_1} :catch_2

    const/16 v1, 0xa

    :try_start_2
    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, -0xc

    aput v12, v2, v11

    const/16 v11, 0x3e45

    aput v11, v2, v10

    const/16 v10, 0x5e57

    aput v10, v2, v9

    const/16 v9, -0x20d8

    aput v9, v2, v8

    const/16 v8, -0x53

    aput v8, v2, v7

    const/16 v7, -0x27

    aput v7, v2, v6

    const/16 v6, -0x77

    aput v6, v2, v5

    const/16 v5, -0xc88

    aput v5, v2, v4

    const/16 v4, -0x66

    aput v4, v2, v3

    const/16 v3, 0x2108

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, -0x6f

    aput v13, v1, v12

    const/16 v12, 0x3e26

    aput v12, v1, v11

    const/16 v11, 0x5e3e

    aput v11, v1, v10

    const/16 v10, -0x20a2

    aput v10, v1, v9

    const/16 v9, -0x21

    aput v9, v1, v8

    const/16 v8, -0x44

    aput v8, v1, v7

    const/16 v7, -0x26

    aput v7, v1, v6

    const/16 v6, -0xce6

    aput v6, v1, v5

    const/16 v5, -0xd

    aput v5, v1, v4

    const/16 v4, 0x2164

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v4, v1

    if-lt v3, v4, :cond_0

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v4, v1

    if-lt v3, v4, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    const/16 v1, 0xe

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, -0x2a

    aput v17, v2, v16

    const/16 v16, -0x61

    aput v16, v2, v15

    const/16 v15, -0x4af5

    aput v15, v2, v14

    const/16 v14, -0x30

    aput v14, v2, v13

    const/16 v13, -0x2e

    aput v13, v2, v12

    const/16 v12, -0xcc0

    aput v12, v2, v11

    const/16 v11, -0x63

    aput v11, v2, v10

    const/16 v10, -0x6c

    aput v10, v2, v9

    const/16 v9, 0x31f

    aput v9, v2, v8

    const/16 v8, -0x51b0

    aput v8, v2, v7

    const/16 v7, -0x22

    aput v7, v2, v6

    const/16 v6, 0x4c3f

    aput v6, v2, v5

    const/16 v5, -0x2c8

    aput v5, v2, v3

    const/16 v3, -0x72

    aput v3, v2, v1

    const/16 v1, 0xe

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, -0x8

    aput v18, v1, v17

    const/16 v17, -0x4f

    aput v17, v1, v16

    const/16 v16, -0x4a86

    aput v16, v1, v15

    const/16 v15, -0x4b

    aput v15, v1, v14

    const/16 v14, -0x60

    aput v14, v1, v13

    const/16 v13, -0xca0

    aput v13, v1, v12

    const/16 v12, -0xd

    aput v12, v1, v11

    const/16 v11, -0xb

    aput v11, v1, v10

    const/16 v10, 0x37c

    aput v10, v1, v9

    const/16 v9, -0x51fd

    aput v9, v1, v8

    const/16 v8, -0x52

    aput v8, v1, v7

    const/16 v7, 0x4c50

    aput v7, v1, v6

    const/16 v6, -0x2b4

    aput v6, v1, v5

    const/4 v5, -0x3

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v5, v1

    if-lt v3, v5, :cond_2

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v5, v1

    if-lt v3, v5, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mScanningFragmentController:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;->stopScan()V

    :goto_4
    return-void

    :cond_0
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_1
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_2
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_3
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :catch_0
    move-exception v1

    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, -0x69

    aput v12, v2, v11

    const/16 v11, -0x54

    aput v11, v2, v10

    const/16 v10, -0x67

    aput v10, v2, v9

    const/16 v9, -0x5ac5

    aput v9, v2, v8

    const/16 v8, -0x29

    aput v8, v2, v7

    const/16 v7, -0x4f81

    aput v7, v2, v6

    const/16 v6, -0x1d

    aput v6, v2, v5

    const/16 v5, -0x77

    aput v5, v2, v4

    const/16 v4, -0x6a9

    aput v4, v2, v3

    const/16 v3, -0x6b

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, -0xe

    aput v13, v1, v12

    const/16 v12, -0x31

    aput v12, v1, v11

    const/16 v11, -0x10

    aput v11, v1, v10

    const/16 v10, -0x5ab3

    aput v10, v1, v9

    const/16 v9, -0x5b

    aput v9, v1, v8

    const/16 v8, -0x4fe6

    aput v8, v1, v7

    const/16 v7, -0x50

    aput v7, v1, v6

    const/16 v6, -0x15

    aput v6, v1, v5

    const/16 v5, -0x6c2

    aput v5, v1, v4

    const/4 v4, -0x7

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_5
    array-length v4, v1

    if-lt v3, v4, :cond_4

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_6
    array-length v4, v1

    if-lt v3, v4, :cond_5

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    const/16 v1, 0x23

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, 0x1d

    const/16 v33, 0x1e

    const/16 v34, 0x1f

    const/16 v35, 0x20

    const/16 v36, 0x21

    const/16 v37, 0x22

    const/16 v38, 0x2e3c

    aput v38, v2, v37

    const/16 v37, 0x6c42

    aput v37, v2, v36

    const/16 v36, -0x7de7

    aput v36, v2, v35

    const/16 v35, -0x14

    aput v35, v2, v34

    const/16 v34, -0x71

    aput v34, v2, v33

    const/16 v33, -0x78c7

    aput v33, v2, v32

    const/16 v32, -0x12

    aput v32, v2, v31

    const/16 v31, 0x571d

    aput v31, v2, v30

    const/16 v30, -0x5cdb

    aput v30, v2, v29

    const/16 v29, -0x3a

    aput v29, v2, v28

    const/16 v28, -0x8

    aput v28, v2, v27

    const/16 v27, -0x58

    aput v27, v2, v26

    const/16 v26, -0x5fee

    aput v26, v2, v25

    const/16 v25, -0x2e

    aput v25, v2, v24

    const/16 v24, 0x7b74

    aput v24, v2, v23

    const/16 v23, -0x53eb

    aput v23, v2, v22

    const/16 v22, -0x3d

    aput v22, v2, v21

    const/16 v21, -0x31

    aput v21, v2, v20

    const/16 v20, 0x7b2e

    aput v20, v2, v19

    const/16 v19, 0x6a15

    aput v19, v2, v18

    const/16 v18, -0x14f1

    aput v18, v2, v17

    const/16 v17, -0x7a

    aput v17, v2, v16

    const/16 v16, 0x5e33

    aput v16, v2, v15

    const/16 v15, 0x53f

    aput v15, v2, v14

    const/16 v14, -0x4189

    aput v14, v2, v13

    const/4 v13, -0x8

    aput v13, v2, v12

    const/16 v12, -0xf

    aput v12, v2, v11

    const/16 v11, -0xbf0

    aput v11, v2, v10

    const/16 v10, -0x63

    aput v10, v2, v9

    const/16 v9, -0x61

    aput v9, v2, v8

    const/16 v8, 0x454f

    aput v8, v2, v7

    const/16 v7, 0x324

    aput v7, v2, v6

    const/16 v6, -0x2a0

    aput v6, v2, v5

    const/16 v5, -0x52

    aput v5, v2, v3

    const/16 v3, 0x7736

    aput v3, v2, v1

    const/16 v1, 0x23

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, 0x1d

    const/16 v34, 0x1e

    const/16 v35, 0x1f

    const/16 v36, 0x20

    const/16 v37, 0x21

    const/16 v38, 0x22

    const/16 v39, 0x2e50

    aput v39, v1, v38

    const/16 v38, 0x6c2e

    aput v38, v1, v37

    const/16 v37, -0x7d94

    aput v37, v1, v36

    const/16 v36, -0x7e

    aput v36, v1, v35

    const/16 v35, -0x51

    aput v35, v1, v34

    const/16 v34, -0x78b6

    aput v34, v1, v33

    const/16 v33, -0x79

    aput v33, v1, v32

    const/16 v32, 0x573d

    aput v32, v1, v31

    const/16 v31, -0x5ca9

    aput v31, v1, v30

    const/16 v30, -0x5d

    aput v30, v1, v29

    const/16 v29, -0x6c

    aput v29, v1, v28

    const/16 v28, -0x3c

    aput v28, v1, v27

    const/16 v27, -0x5f83

    aput v27, v1, v26

    const/16 v26, -0x60

    aput v26, v1, v25

    const/16 v25, 0x7b00

    aput v25, v1, v24

    const/16 v24, -0x5385

    aput v24, v1, v23

    const/16 v23, -0x54

    aput v23, v1, v22

    const/16 v22, -0x74

    aput v22, v1, v21

    const/16 v21, 0x7b5a

    aput v21, v1, v20

    const/16 v20, 0x6a7b

    aput v20, v1, v19

    const/16 v19, -0x1496

    aput v19, v1, v18

    const/16 v18, -0x15

    aput v18, v1, v17

    const/16 v17, 0x5e54

    aput v17, v1, v16

    const/16 v16, 0x55e

    aput v16, v1, v15

    const/16 v15, -0x41fb

    aput v15, v1, v14

    const/16 v14, -0x42

    aput v14, v1, v13

    const/16 v13, -0x6a

    aput v13, v1, v12

    const/16 v12, -0xb82

    aput v12, v1, v11

    const/16 v11, -0xc

    aput v11, v1, v10

    const/16 v10, -0xf

    aput v10, v1, v9

    const/16 v9, 0x4521

    aput v9, v1, v8

    const/16 v8, 0x345

    aput v8, v1, v7

    const/16 v7, -0x2fd

    aput v7, v1, v6

    const/4 v6, -0x3

    aput v6, v1, v5

    const/16 v5, 0x775b

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_7
    array-length v5, v1

    if-lt v3, v5, :cond_6

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_8
    array-length v5, v1

    if-lt v3, v5, :cond_7

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_4

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_4

    :cond_4
    :try_start_3
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_5

    :cond_5
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_6

    :cond_6
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_7

    :cond_7
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_3 .. :try_end_3} :catch_2

    add-int/lit8 v3, v3, 0x1

    goto :goto_8

    :catch_2
    move-exception v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto/16 :goto_4
.end method

.method private stopSearch()V
    .locals 5

    const/4 v4, 0x0

    const/16 v3, 0x8

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->scanbutton:Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$string;->scan:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->scanbutton:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->IsAutoScanRequired:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->scanbutton:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->spinner:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->scanningLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mScannedDeviceListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$ScannedDeviceListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$ScannedDeviceListAdapter;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->IsScreenLocked:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->nodevices:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v4}, Landroid/widget/FrameLayout;->setVisibility(I)V

    :cond_1
    iget v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->noDeviceStringResId:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->nodevicesText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->noDeviceStringResId:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mScannedlist:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->spinner:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->scanningLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    return-void

    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->noDeviceString:Ljava/lang/String;

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->nodevicesText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->noDeviceString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->nodevices:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mScannedlist:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private toggleScanState()V
    .locals 40

    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, -0x3d2

    aput v12, v2, v11

    const/16 v11, -0x61

    aput v11, v2, v10

    const/16 v10, -0x4e

    aput v10, v2, v9

    const/16 v9, -0x75

    aput v9, v2, v8

    const/16 v8, -0x10

    aput v8, v2, v7

    const/16 v7, 0x3e0a

    aput v7, v2, v6

    const/16 v6, 0x496d

    aput v6, v2, v5

    const/16 v5, -0x3d5

    aput v5, v2, v4

    const/16 v4, -0x6b

    aput v4, v2, v3

    const/16 v3, -0x42

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, -0x3b5

    aput v13, v1, v12

    const/4 v12, -0x4

    aput v12, v1, v11

    const/16 v11, -0x25

    aput v11, v1, v10

    const/4 v10, -0x3

    aput v10, v1, v9

    const/16 v9, -0x7e

    aput v9, v1, v8

    const/16 v8, 0x3e6f

    aput v8, v1, v7

    const/16 v7, 0x493e

    aput v7, v1, v6

    const/16 v6, -0x3b7

    aput v6, v1, v5

    const/4 v5, -0x4

    aput v5, v1, v4

    const/16 v4, -0x2e

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v4, v1

    if-lt v3, v4, :cond_0

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v4, v1

    if-lt v3, v4, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    const/16 v1, 0xf

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, -0x548f

    aput v18, v2, v17

    const/16 v17, -0x21

    aput v17, v2, v16

    const/16 v16, -0x75

    aput v16, v2, v15

    const/16 v15, -0x35

    aput v15, v2, v14

    const/16 v14, -0x30

    aput v14, v2, v13

    const/16 v13, 0x406c

    aput v13, v2, v12

    const/16 v12, 0x5c21

    aput v12, v2, v11

    const/16 v11, -0x60c1

    aput v11, v2, v10

    const/16 v10, -0x34

    aput v10, v2, v9

    const/16 v9, 0x4103

    aput v9, v2, v8

    const/16 v8, -0x36d3

    aput v8, v2, v7

    const/16 v7, -0x52

    aput v7, v2, v6

    const/16 v6, -0x6dd3

    aput v6, v2, v5

    const/4 v5, -0x3

    aput v5, v2, v3

    const/16 v3, 0x6a30

    aput v3, v2, v1

    const/16 v1, 0xf

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, -0x54ec

    aput v19, v1, v18

    const/16 v18, -0x55

    aput v18, v1, v17

    const/16 v17, -0x16

    aput v17, v1, v16

    const/16 v16, -0x41

    aput v16, v1, v15

    const/16 v15, -0x7d

    aput v15, v1, v14

    const/16 v14, 0x4002

    aput v14, v1, v13

    const/16 v13, 0x5c40

    aput v13, v1, v12

    const/16 v12, -0x60a4

    aput v12, v1, v11

    const/16 v11, -0x61

    aput v11, v1, v10

    const/16 v10, 0x4166

    aput v10, v1, v9

    const/16 v9, -0x36bf

    aput v9, v1, v8

    const/16 v8, -0x37

    aput v8, v1, v7

    const/16 v7, -0x6db6

    aput v7, v1, v6

    const/16 v6, -0x6e

    aput v6, v1, v5

    const/16 v5, 0x6a44

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v5, v1

    if-lt v3, v5, :cond_2

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v5, v1

    if-lt v3, v5, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->IsJoining:Z

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mScanningFragmentController:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;

    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_4
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->isScanning:Z

    if-eqz v1, :cond_8

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->stopSearch()V

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->stopDiscoveringSensorDevices()V

    :goto_5
    return-void

    :cond_0
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_1
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_2
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_3
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :catch_0
    move-exception v1

    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0x655f

    aput v12, v2, v11

    const/16 v11, 0x4006

    aput v11, v2, v10

    const/16 v10, 0x7829

    aput v10, v2, v9

    const/16 v9, 0x450e

    aput v9, v2, v8

    const/16 v8, -0x2dc9

    aput v8, v2, v7

    const/16 v7, -0x49

    aput v7, v2, v6

    const/16 v6, -0x2c

    aput v6, v2, v5

    const/16 v5, -0x2fbf

    aput v5, v2, v4

    const/16 v4, -0x47

    aput v4, v2, v3

    const/16 v3, -0x65

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0x653a

    aput v13, v1, v12

    const/16 v12, 0x4065

    aput v12, v1, v11

    const/16 v11, 0x7840

    aput v11, v1, v10

    const/16 v10, 0x4578

    aput v10, v1, v9

    const/16 v9, -0x2dbb

    aput v9, v1, v8

    const/16 v8, -0x2e

    aput v8, v1, v7

    const/16 v7, -0x79

    aput v7, v1, v6

    const/16 v6, -0x2fdd

    aput v6, v1, v5

    const/16 v5, -0x30

    aput v5, v1, v4

    const/16 v4, -0x9

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_6
    array-length v4, v1

    if-lt v3, v4, :cond_4

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_7
    array-length v4, v1

    if-lt v3, v4, :cond_5

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    const/16 v1, 0x23

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, 0x1d

    const/16 v33, 0x1e

    const/16 v34, 0x1f

    const/16 v35, 0x20

    const/16 v36, 0x21

    const/16 v37, 0x22

    const/16 v38, -0x2484

    aput v38, v2, v37

    const/16 v37, -0x49

    aput v37, v2, v36

    const/16 v36, -0xd85

    aput v36, v2, v35

    const/16 v35, -0x64

    aput v35, v2, v34

    const/16 v34, -0x66

    aput v34, v2, v33

    const/16 v33, -0x60

    aput v33, v2, v32

    const/16 v32, -0x588b

    aput v32, v2, v31

    const/16 v31, -0x79

    aput v31, v2, v30

    const/16 v30, -0x42

    aput v30, v2, v29

    const/16 v29, -0x6

    aput v29, v2, v28

    const/16 v28, -0x74

    aput v28, v2, v27

    const/16 v27, -0xab3

    aput v27, v2, v26

    const/16 v26, -0x66

    aput v26, v2, v25

    const/16 v25, -0x9c7

    aput v25, v2, v24

    const/16 v24, -0x7e

    aput v24, v2, v23

    const/16 v23, -0x56

    aput v23, v2, v22

    const/16 v22, 0x3339

    aput v22, v2, v21

    const/16 v21, 0x4470

    aput v21, v2, v20

    const/16 v20, 0x7030

    aput v20, v2, v19

    const/16 v19, 0x701e

    aput v19, v2, v18

    const/16 v18, -0x13eb

    aput v18, v2, v17

    const/16 v17, -0x7f

    aput v17, v2, v16

    const/16 v16, -0x57

    aput v16, v2, v15

    const/16 v15, -0x63

    aput v15, v2, v14

    const/16 v14, -0x2a

    aput v14, v2, v13

    const/16 v13, -0x12bc

    aput v13, v2, v12

    const/16 v12, -0x76

    aput v12, v2, v11

    const/16 v11, -0x995

    aput v11, v2, v10

    const/16 v10, -0x61

    aput v10, v2, v9

    const/16 v9, -0x6b

    aput v9, v2, v8

    const/16 v8, -0x3287

    aput v8, v2, v7

    const/16 v7, -0x54

    aput v7, v2, v6

    const/16 v6, 0x4851

    aput v6, v2, v5

    const/16 v5, -0x68e5

    aput v5, v2, v3

    const/4 v3, -0x6

    aput v3, v2, v1

    const/16 v1, 0x23

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, 0x1d

    const/16 v34, 0x1e

    const/16 v35, 0x1f

    const/16 v36, 0x20

    const/16 v37, 0x21

    const/16 v38, 0x22

    const/16 v39, -0x24f0

    aput v39, v1, v38

    const/16 v38, -0x25

    aput v38, v1, v37

    const/16 v37, -0xdf2

    aput v37, v1, v36

    const/16 v36, -0xe

    aput v36, v1, v35

    const/16 v35, -0x46

    aput v35, v1, v34

    const/16 v34, -0x2d

    aput v34, v1, v33

    const/16 v33, -0x58e4

    aput v33, v1, v32

    const/16 v32, -0x59

    aput v32, v1, v31

    const/16 v31, -0x34

    aput v31, v1, v30

    const/16 v30, -0x61

    aput v30, v1, v29

    const/16 v29, -0x20

    aput v29, v1, v28

    const/16 v28, -0xadf

    aput v28, v1, v27

    const/16 v27, -0xb

    aput v27, v1, v26

    const/16 v26, -0x9b5

    aput v26, v1, v25

    const/16 v25, -0xa

    aput v25, v1, v24

    const/16 v24, -0x3c

    aput v24, v1, v23

    const/16 v23, 0x3356

    aput v23, v1, v22

    const/16 v22, 0x4433

    aput v22, v1, v21

    const/16 v21, 0x7044

    aput v21, v1, v20

    const/16 v20, 0x7070

    aput v20, v1, v19

    const/16 v19, -0x1390

    aput v19, v1, v18

    const/16 v18, -0x14

    aput v18, v1, v17

    const/16 v17, -0x32

    aput v17, v1, v16

    const/16 v16, -0x4

    aput v16, v1, v15

    const/16 v15, -0x5c

    aput v15, v1, v14

    const/16 v14, -0x12fe

    aput v14, v1, v13

    const/16 v13, -0x13

    aput v13, v1, v12

    const/16 v12, -0x9fb

    aput v12, v1, v11

    const/16 v11, -0xa

    aput v11, v1, v10

    const/4 v10, -0x5

    aput v10, v1, v9

    const/16 v9, -0x32e9

    aput v9, v1, v8

    const/16 v8, -0x33

    aput v8, v1, v7

    const/16 v7, 0x4832

    aput v7, v1, v6

    const/16 v6, -0x68b8

    aput v6, v1, v5

    const/16 v5, -0x69

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_8
    array-length v5, v1

    if-lt v3, v5, :cond_6

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_9
    array-length v5, v1

    if-lt v3, v5, :cond_7

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$string;->service_not_connected:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->showToast(Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto/16 :goto_4

    :cond_4
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_6

    :cond_5
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_7

    :cond_6
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_8

    :cond_7
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_9

    :cond_8
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v1

    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v2

    if-nez v2, :cond_9

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$string;->turning_on_bluetooth:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->showToast(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->enable()Z

    const/4 v1, 0x1

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->isBluetoothTurningON:Z

    goto/16 :goto_5

    :catch_1
    move-exception v1

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$string;->bluetooth_not_supported:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->showToast(Ljava/lang/String;)V

    goto/16 :goto_5

    :cond_9
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->scanbutton:Landroid/widget/Button;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->scanbutton:Landroid/widget/Button;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setBackground(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->startSearch()V

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->startDiscoveringSensorDevices()V

    goto/16 :goto_5
.end method

.method public static tzn0VLwU()Ljava/lang/String;
    .locals 91

    const/16 v0, 0x58

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x4

    const/4 v6, 0x5

    const/4 v7, 0x6

    const/4 v8, 0x7

    const/16 v9, 0x8

    const/16 v10, 0x9

    const/16 v11, 0xa

    const/16 v12, 0xb

    const/16 v13, 0xc

    const/16 v14, 0xd

    const/16 v15, 0xe

    const/16 v16, 0xf

    const/16 v17, 0x10

    const/16 v18, 0x11

    const/16 v19, 0x12

    const/16 v20, 0x13

    const/16 v21, 0x14

    const/16 v22, 0x15

    const/16 v23, 0x16

    const/16 v24, 0x17

    const/16 v25, 0x18

    const/16 v26, 0x19

    const/16 v27, 0x1a

    const/16 v28, 0x1b

    const/16 v29, 0x1c

    const/16 v30, 0x1d

    const/16 v31, 0x1e

    const/16 v32, 0x1f

    const/16 v33, 0x20

    const/16 v34, 0x21

    const/16 v35, 0x22

    const/16 v36, 0x23

    const/16 v37, 0x24

    const/16 v38, 0x25

    const/16 v39, 0x26

    const/16 v40, 0x27

    const/16 v41, 0x28

    const/16 v42, 0x29

    const/16 v43, 0x2a

    const/16 v44, 0x2b

    const/16 v45, 0x2c

    const/16 v46, 0x2d

    const/16 v47, 0x2e

    const/16 v48, 0x2f

    const/16 v49, 0x30

    const/16 v50, 0x31

    const/16 v51, 0x32

    const/16 v52, 0x33

    const/16 v53, 0x34

    const/16 v54, 0x35

    const/16 v55, 0x36

    const/16 v56, 0x37

    const/16 v57, 0x38

    const/16 v58, 0x39

    const/16 v59, 0x3a

    const/16 v60, 0x3b

    const/16 v61, 0x3c

    const/16 v62, 0x3d

    const/16 v63, 0x3e

    const/16 v64, 0x3f

    const/16 v65, 0x40

    const/16 v66, 0x41

    const/16 v67, 0x42

    const/16 v68, 0x43

    const/16 v69, 0x44

    const/16 v70, 0x45

    const/16 v71, 0x46

    const/16 v72, 0x47

    const/16 v73, 0x48

    const/16 v74, 0x49

    const/16 v75, 0x4a

    const/16 v76, 0x4b

    const/16 v77, 0x4c

    const/16 v78, 0x4d

    const/16 v79, 0x4e

    const/16 v80, 0x4f

    const/16 v81, 0x50

    const/16 v82, 0x51

    const/16 v83, 0x52

    const/16 v84, 0x53

    const/16 v85, 0x54

    const/16 v86, 0x55

    const/16 v87, 0x56

    const/16 v88, 0x57

    const/16 v89, -0x71cc

    aput v89, v1, v88

    const/16 v88, -0x13

    aput v88, v1, v87

    const/16 v87, 0xa62

    aput v87, v1, v86

    const/16 v86, 0x457c

    aput v86, v1, v85

    const/16 v85, 0x4520

    aput v85, v1, v84

    const/16 v84, -0x3ddf

    aput v84, v1, v83

    const/16 v83, -0x1e

    aput v83, v1, v82

    const/16 v82, -0x6d

    aput v82, v1, v81

    const/16 v81, 0x7431

    aput v81, v1, v80

    const/16 v80, 0x4b1c

    aput v80, v1, v79

    const/16 v79, 0x313f

    aput v79, v1, v78

    const/16 v78, 0x5011

    aput v78, v1, v77

    const/16 v77, -0x2fdc

    aput v77, v1, v76

    const/16 v76, -0x4d

    aput v76, v1, v75

    const/16 v75, -0x14df

    aput v75, v1, v74

    const/16 v74, -0x7b

    aput v74, v1, v73

    const/16 v73, 0x6c30

    aput v73, v1, v72

    const/16 v72, -0xbfd

    aput v72, v1, v71

    const/16 v71, -0x69

    aput v71, v1, v70

    const/16 v70, 0x4b1f

    aput v70, v1, v69

    const/16 v69, 0x1e2e

    aput v69, v1, v68

    const/16 v68, 0x726c

    aput v68, v1, v67

    const/16 v67, 0x2d1d

    aput v67, v1, v66

    const/16 v66, 0x2c4b

    aput v66, v1, v65

    const/16 v65, -0x2eb7

    aput v65, v1, v64

    const/16 v64, -0x4d

    aput v64, v1, v63

    const/16 v63, 0x4a12

    aput v63, v1, v62

    const/16 v62, -0x12c2

    aput v62, v1, v61

    const/16 v61, -0x7c

    aput v61, v1, v60

    const/16 v60, -0x59

    aput v60, v1, v59

    const/16 v59, -0x5e

    aput v59, v1, v58

    const/16 v58, -0x2f98

    aput v58, v1, v57

    const/16 v57, -0x4b

    aput v57, v1, v56

    const/16 v56, 0x7701

    aput v56, v1, v55

    const/16 v55, 0x4019

    aput v55, v1, v54

    const/16 v54, 0x42f

    aput v54, v1, v53

    const/16 v53, -0x6599

    aput v53, v1, v52

    const/16 v52, -0x17

    aput v52, v1, v51

    const/16 v51, -0x7

    aput v51, v1, v50

    const/16 v50, -0x57

    aput v50, v1, v49

    const/16 v49, -0x21

    aput v49, v1, v48

    const/16 v48, 0x2078

    aput v48, v1, v47

    const/16 v47, 0x6253

    aput v47, v1, v46

    const/16 v46, 0x5003

    aput v46, v1, v45

    const/16 v45, -0x40cb

    aput v45, v1, v44

    const/16 v44, -0x2d

    aput v44, v1, v43

    const/16 v43, -0x67

    aput v43, v1, v42

    const/16 v42, -0x27d0

    aput v42, v1, v41

    const/16 v41, -0xc

    aput v41, v1, v40

    const/16 v40, 0x2a4a

    aput v40, v1, v39

    const/16 v39, -0x7db1

    aput v39, v1, v38

    const/16 v38, -0xa

    aput v38, v1, v37

    const/16 v37, -0x42

    aput v37, v1, v36

    const/16 v36, -0x4d

    aput v36, v1, v35

    const/16 v35, -0x37

    aput v35, v1, v34

    const/16 v34, -0x23

    aput v34, v1, v33

    const/16 v33, -0x14

    aput v33, v1, v32

    const/16 v32, -0x40

    aput v32, v1, v31

    const/16 v31, -0x54

    aput v31, v1, v30

    const/16 v30, -0x6e

    aput v30, v1, v29

    const/16 v29, 0xb61

    aput v29, v1, v28

    const/16 v28, -0x6c96

    aput v28, v1, v27

    const/16 v27, -0xa

    aput v27, v1, v26

    const/16 v26, -0x19c2

    aput v26, v1, v25

    const/16 v25, -0x76

    aput v25, v1, v24

    const/16 v24, -0x51

    aput v24, v1, v23

    const/16 v23, -0x76

    aput v23, v1, v22

    const/16 v22, -0x21a1

    aput v22, v1, v21

    const/16 v21, -0x49

    aput v21, v1, v20

    const/16 v20, -0x19

    aput v20, v1, v19

    const/16 v19, -0xbf2

    aput v19, v1, v18

    const/16 v18, -0x65

    aput v18, v1, v17

    const/16 v17, -0x5392

    aput v17, v1, v16

    const/16 v16, -0x3b

    aput v16, v1, v15

    const/16 v15, -0x17

    aput v15, v1, v14

    const/16 v14, -0x5c9c

    aput v14, v1, v13

    const/16 v13, -0x32

    aput v13, v1, v12

    const/16 v12, 0x137

    aput v12, v1, v11

    const/16 v11, 0x4464

    aput v11, v1, v10

    const/16 v10, -0x47d0

    aput v10, v1, v9

    const/16 v9, -0x27

    aput v9, v1, v8

    const/16 v8, -0x3e

    aput v8, v1, v7

    const/16 v7, -0x30

    aput v7, v1, v6

    const/16 v6, -0x509c

    aput v6, v1, v5

    const/16 v5, -0x23

    aput v5, v1, v4

    const/4 v4, 0x4

    aput v4, v1, v3

    const/16 v3, -0x3c9b

    aput v3, v1, v2

    const/16 v2, -0x75

    aput v2, v1, v0

    const/16 v0, 0x58

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, 0x10

    const/16 v19, 0x11

    const/16 v20, 0x12

    const/16 v21, 0x13

    const/16 v22, 0x14

    const/16 v23, 0x15

    const/16 v24, 0x16

    const/16 v25, 0x17

    const/16 v26, 0x18

    const/16 v27, 0x19

    const/16 v28, 0x1a

    const/16 v29, 0x1b

    const/16 v30, 0x1c

    const/16 v31, 0x1d

    const/16 v32, 0x1e

    const/16 v33, 0x1f

    const/16 v34, 0x20

    const/16 v35, 0x21

    const/16 v36, 0x22

    const/16 v37, 0x23

    const/16 v38, 0x24

    const/16 v39, 0x25

    const/16 v40, 0x26

    const/16 v41, 0x27

    const/16 v42, 0x28

    const/16 v43, 0x29

    const/16 v44, 0x2a

    const/16 v45, 0x2b

    const/16 v46, 0x2c

    const/16 v47, 0x2d

    const/16 v48, 0x2e

    const/16 v49, 0x2f

    const/16 v50, 0x30

    const/16 v51, 0x31

    const/16 v52, 0x32

    const/16 v53, 0x33

    const/16 v54, 0x34

    const/16 v55, 0x35

    const/16 v56, 0x36

    const/16 v57, 0x37

    const/16 v58, 0x38

    const/16 v59, 0x39

    const/16 v60, 0x3a

    const/16 v61, 0x3b

    const/16 v62, 0x3c

    const/16 v63, 0x3d

    const/16 v64, 0x3e

    const/16 v65, 0x3f

    const/16 v66, 0x40

    const/16 v67, 0x41

    const/16 v68, 0x42

    const/16 v69, 0x43

    const/16 v70, 0x44

    const/16 v71, 0x45

    const/16 v72, 0x46

    const/16 v73, 0x47

    const/16 v74, 0x48

    const/16 v75, 0x49

    const/16 v76, 0x4a

    const/16 v77, 0x4b

    const/16 v78, 0x4c

    const/16 v79, 0x4d

    const/16 v80, 0x4e

    const/16 v81, 0x4f

    const/16 v82, 0x50

    const/16 v83, 0x51

    const/16 v84, 0x52

    const/16 v85, 0x53

    const/16 v86, 0x54

    const/16 v87, 0x55

    const/16 v88, 0x56

    const/16 v89, 0x57

    const/16 v90, -0x71af

    aput v90, v0, v89

    const/16 v89, -0x72

    aput v89, v0, v88

    const/16 v88, 0xa0b

    aput v88, v0, v87

    const/16 v87, 0x450a

    aput v87, v0, v86

    const/16 v86, 0x4545

    aput v86, v0, v85

    const/16 v85, -0x3dbb

    aput v85, v0, v84

    const/16 v84, -0x3e

    aput v84, v0, v83

    const/16 v83, -0x20

    aput v83, v0, v82

    const/16 v82, 0x7458

    aput v82, v0, v81

    const/16 v81, 0x4b74

    aput v81, v0, v80

    const/16 v80, 0x314b

    aput v80, v0, v79

    const/16 v79, 0x5031

    aput v79, v0, v78

    const/16 v78, -0x2fb0

    aput v78, v0, v77

    const/16 v77, -0x30

    aput v77, v0, v76

    const/16 v76, -0x14bc

    aput v76, v0, v75

    const/16 v75, -0x15

    aput v75, v0, v74

    const/16 v74, 0x6c5e

    aput v74, v0, v73

    const/16 v73, -0xb94

    aput v73, v0, v72

    const/16 v72, -0xc

    aput v72, v0, v71

    const/16 v71, 0x4b3f

    aput v71, v0, v70

    const/16 v70, 0x1e4b

    aput v70, v0, v69

    const/16 v69, 0x721e

    aput v69, v0, v68

    const/16 v68, 0x2d72

    aput v68, v0, v67

    const/16 v67, 0x2c2d

    aput v67, v0, v66

    const/16 v66, -0x2ed4

    aput v66, v0, v65

    const/16 v65, -0x2f

    aput v65, v0, v64

    const/16 v64, 0x4a32

    aput v64, v0, v63

    const/16 v63, -0x12b6

    aput v63, v0, v62

    const/16 v62, -0x13

    aput v62, v0, v61

    const/16 v61, -0x79

    aput v61, v0, v60

    const/16 v60, -0x2a

    aput v60, v0, v59

    const/16 v59, -0x2ff5

    aput v59, v0, v58

    const/16 v58, -0x30

    aput v58, v0, v57

    const/16 v57, 0x776f

    aput v57, v0, v56

    const/16 v56, 0x4077

    aput v56, v0, v55

    const/16 v55, 0x440

    aput v55, v0, v54

    const/16 v54, -0x65fc

    aput v54, v0, v53

    const/16 v53, -0x66

    aput v53, v0, v52

    const/16 v52, -0x70

    aput v52, v0, v51

    const/16 v51, -0x33

    aput v51, v0, v50

    const/16 v50, -0x1

    aput v50, v0, v49

    const/16 v49, 0x201d

    aput v49, v0, v48

    const/16 v48, 0x6220

    aput v48, v0, v47

    const/16 v47, 0x5062

    aput v47, v0, v46

    const/16 v46, -0x40b0

    aput v46, v0, v45

    const/16 v45, -0x41

    aput v45, v0, v44

    const/16 v44, -0x17

    aput v44, v0, v43

    const/16 v43, -0x27f0

    aput v43, v0, v42

    const/16 v42, -0x28

    aput v42, v0, v41

    const/16 v41, 0x2a2e

    aput v41, v0, v40

    const/16 v40, -0x7dd6

    aput v40, v0, v39

    const/16 v39, -0x7e

    aput v39, v0, v38

    const/16 v38, -0x23

    aput v38, v0, v37

    const/16 v37, -0x2a

    aput v37, v0, v36

    const/16 v36, -0x59

    aput v36, v0, v35

    const/16 v35, -0x4d

    aput v35, v0, v34

    const/16 v34, -0x7d

    aput v34, v0, v33

    const/16 v33, -0x5d

    aput v33, v0, v32

    const/16 v32, -0x74

    aput v32, v0, v31

    const/16 v31, -0x15

    aput v31, v0, v30

    const/16 v30, 0xb05

    aput v30, v0, v29

    const/16 v29, -0x6cf5

    aput v29, v0, v28

    const/16 v28, -0x6d

    aput v28, v0, v27

    const/16 v27, -0x19b4

    aput v27, v0, v26

    const/16 v26, -0x1a

    aput v26, v0, v25

    const/16 v25, -0x32

    aput v25, v0, v24

    const/16 v24, -0x56

    aput v24, v0, v23

    const/16 v23, -0x21d4

    aput v23, v0, v22

    const/16 v22, -0x22

    aput v22, v0, v21

    const/16 v21, -0x39

    aput v21, v0, v20

    const/16 v20, -0xb84

    aput v20, v0, v19

    const/16 v19, -0xc

    aput v19, v0, v18

    const/16 v18, -0x53e6

    aput v18, v0, v17

    const/16 v17, -0x54

    aput v17, v0, v16

    const/16 v16, -0x79

    aput v16, v0, v15

    const/16 v15, -0x5cf5

    aput v15, v0, v14

    const/16 v14, -0x5d

    aput v14, v0, v13

    const/16 v13, 0x117

    aput v13, v0, v12

    const/16 v12, 0x4401

    aput v12, v0, v11

    const/16 v11, -0x47bc

    aput v11, v0, v10

    const/16 v10, -0x48

    aput v10, v0, v9

    const/16 v9, -0x50

    aput v9, v0, v8

    const/16 v8, -0x10

    aput v8, v0, v7

    const/16 v7, -0x50f0

    aput v7, v0, v6

    const/16 v6, -0x51

    aput v6, v0, v5

    const/16 v5, 0x65

    aput v5, v0, v4

    const/16 v4, -0x3d00

    aput v4, v0, v3

    const/16 v3, -0x3d

    aput v3, v0, v2

    const/4 v2, 0x0

    :goto_0
    array-length v3, v0

    if-lt v2, v3, :cond_0

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_1
    array-length v3, v0

    if-lt v2, v3, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    aget v3, v0, v2

    aget v4, v1, v2

    xor-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    aget v3, v1, v2

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method


# virtual methods
.method public clearController()V
    .locals 40

    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, -0x2df7

    aput v12, v2, v11

    const/16 v11, -0x4f

    aput v11, v2, v10

    const/16 v10, -0xcb9

    aput v10, v2, v9

    const/16 v9, -0x7b

    aput v9, v2, v8

    const/4 v8, -0x5

    aput v8, v2, v7

    const/16 v7, -0x2f

    aput v7, v2, v6

    const/16 v6, 0x386f

    aput v6, v2, v5

    const/16 v5, -0x8a6

    aput v5, v2, v4

    const/16 v4, -0x62

    aput v4, v2, v3

    const/16 v3, -0x56

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, -0x2d94

    aput v13, v1, v12

    const/16 v12, -0x2e

    aput v12, v1, v11

    const/16 v11, -0xcd2

    aput v11, v1, v10

    const/16 v10, -0xd

    aput v10, v1, v9

    const/16 v9, -0x77

    aput v9, v1, v8

    const/16 v8, -0x4c

    aput v8, v1, v7

    const/16 v7, 0x383c

    aput v7, v1, v6

    const/16 v6, -0x8c8

    aput v6, v1, v5

    const/16 v5, -0x9

    aput v5, v1, v4

    const/16 v4, -0x3a

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v4, v1

    if-lt v3, v4, :cond_0

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v4, v1

    if-lt v3, v4, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    const/16 v1, 0x20

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, 0x1d

    const/16 v33, 0x1e

    const/16 v34, 0x1f

    const/16 v35, -0xd2

    aput v35, v2, v34

    const/16 v34, -0x22

    aput v34, v2, v33

    const/16 v33, -0x46

    aput v33, v2, v32

    const/16 v32, -0x3d

    aput v32, v2, v31

    const/16 v31, -0x73

    aput v31, v2, v30

    const/16 v30, 0x3a4c

    aput v30, v2, v29

    const/16 v29, 0x2a54

    aput v29, v2, v28

    const/16 v28, 0x264f

    aput v28, v2, v27

    const/16 v27, 0x7955

    aput v27, v2, v26

    const/16 v26, 0x1859

    aput v26, v2, v25

    const/16 v25, 0x7270

    aput v25, v2, v24

    const/16 v24, -0x5efa

    aput v24, v2, v23

    const/16 v23, -0x33

    aput v23, v2, v22

    const/16 v22, -0x41

    aput v22, v2, v21

    const/16 v21, 0x5b54

    aput v21, v2, v20

    const/16 v20, 0x7d33

    aput v20, v2, v19

    const/16 v19, -0x3da3

    aput v19, v2, v18

    const/16 v18, -0x5b

    aput v18, v2, v17

    const/16 v17, -0x16fb

    aput v17, v2, v16

    const/16 v16, -0x80

    aput v16, v2, v15

    const/4 v15, -0x5

    aput v15, v2, v14

    const/16 v14, 0x3127

    aput v14, v2, v13

    const/16 v13, -0x66a3

    aput v13, v2, v12

    const/4 v12, -0x6

    aput v12, v2, v11

    const/16 v11, 0x262

    aput v11, v2, v10

    const/16 v10, -0x7a99

    aput v10, v2, v9

    const/16 v9, -0x9

    aput v9, v2, v8

    const/16 v8, -0x5de5

    aput v8, v2, v7

    const/16 v7, -0x3c

    aput v7, v2, v6

    const/16 v6, -0x6c8

    aput v6, v2, v5

    const/16 v5, -0x65

    aput v5, v2, v3

    const/16 v3, -0x63

    aput v3, v2, v1

    const/16 v1, 0x20

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, 0x1d

    const/16 v34, 0x1e

    const/16 v35, 0x1f

    const/16 v36, -0xf1

    aput v36, v1, v35

    const/16 v35, -0x1

    aput v35, v1, v34

    const/16 v34, -0x66

    aput v34, v1, v33

    const/16 v33, -0x4f

    aput v33, v1, v32

    const/16 v32, -0x1e

    aput v32, v1, v31

    const/16 v31, 0x3a3f

    aput v31, v1, v30

    const/16 v30, 0x2a3a

    aput v30, v1, v29

    const/16 v29, 0x262a

    aput v29, v1, v28

    const/16 v28, 0x7926

    aput v28, v1, v27

    const/16 v27, 0x1879

    aput v27, v1, v26

    const/16 v26, 0x7218

    aput v26, v1, v25

    const/16 v25, -0x5e8e

    aput v25, v1, v24

    const/16 v24, -0x5f

    aput v24, v1, v23

    const/16 v23, -0x22

    aput v23, v1, v22

    const/16 v22, 0x5b31

    aput v22, v1, v21

    const/16 v21, 0x7d5b

    aput v21, v1, v20

    const/16 v20, -0x3d83

    aput v20, v1, v19

    const/16 v19, -0x3e

    aput v19, v1, v18

    const/16 v18, -0x1695

    aput v18, v1, v17

    const/16 v17, -0x17

    aput v17, v1, v16

    const/16 v16, -0x78

    aput v16, v1, v15

    const/16 v15, 0x3148

    aput v15, v1, v14

    const/16 v14, -0x66cf

    aput v14, v1, v13

    const/16 v13, -0x67

    aput v13, v1, v12

    const/16 v12, 0x242

    aput v12, v1, v11

    const/16 v11, -0x7afe

    aput v11, v1, v10

    const/16 v10, -0x7b

    aput v10, v1, v9

    const/16 v9, -0x5d8c

    aput v9, v1, v8

    const/16 v8, -0x5e

    aput v8, v1, v7

    const/16 v7, -0x6a3

    aput v7, v1, v6

    const/4 v6, -0x7

    aput v6, v1, v5

    const/16 v5, -0x43

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v5, v1

    if-lt v3, v5, :cond_2

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v5, v1

    if-lt v3, v5, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mScanningFragmentController:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;

    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mScanningFragmentController:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;->clearController()V

    :goto_4
    return-void

    :cond_0
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_1
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_2
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_3
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :catch_0
    move-exception v1

    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, -0x68a7

    aput v12, v2, v11

    const/16 v11, -0xc

    aput v11, v2, v10

    const/16 v10, -0x2eb7

    aput v10, v2, v9

    const/16 v9, -0x59

    aput v9, v2, v8

    const/16 v8, 0x6a61

    aput v8, v2, v7

    const/16 v7, 0x5e0f

    aput v7, v2, v6

    const/16 v6, -0x56f3

    aput v6, v2, v5

    const/16 v5, -0x35

    aput v5, v2, v4

    const/16 v4, -0x4d2

    aput v4, v2, v3

    const/16 v3, -0x69

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, -0x68c4

    aput v13, v1, v12

    const/16 v12, -0x69

    aput v12, v1, v11

    const/16 v11, -0x2ee0

    aput v11, v1, v10

    const/16 v10, -0x2f

    aput v10, v1, v9

    const/16 v9, 0x6a13

    aput v9, v1, v8

    const/16 v8, 0x5e6a

    aput v8, v1, v7

    const/16 v7, -0x56a2

    aput v7, v1, v6

    const/16 v6, -0x57

    aput v6, v1, v5

    const/16 v5, -0x4b9

    aput v5, v1, v4

    const/4 v4, -0x5

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_5
    array-length v4, v1

    if-lt v3, v4, :cond_4

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_6
    array-length v4, v1

    if-lt v3, v4, :cond_5

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    const/16 v1, 0x23

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, 0x1d

    const/16 v33, 0x1e

    const/16 v34, 0x1f

    const/16 v35, 0x20

    const/16 v36, 0x21

    const/16 v37, 0x22

    const/16 v38, -0x34

    aput v38, v2, v37

    const/16 v37, -0xf84

    aput v37, v2, v36

    const/16 v36, -0x7b

    aput v36, v2, v35

    const/16 v35, -0xfcc

    aput v35, v2, v34

    const/16 v34, -0x30

    aput v34, v2, v33

    const/16 v33, -0x3e

    aput v33, v2, v32

    const/16 v32, -0x53

    aput v32, v2, v31

    const/16 v31, -0x32d2

    aput v31, v2, v30

    const/16 v30, -0x41

    aput v30, v2, v29

    const/16 v29, -0x1e

    aput v29, v2, v28

    const/16 v28, -0x35fa

    aput v28, v2, v27

    const/16 v27, -0x5a

    aput v27, v2, v26

    const/16 v26, -0x5d92

    aput v26, v2, v25

    const/16 v25, -0x30

    aput v25, v2, v24

    const/16 v24, -0xb

    aput v24, v2, v23

    const/16 v23, -0x8

    aput v23, v2, v22

    const/16 v22, -0x70

    aput v22, v2, v21

    const/16 v21, 0x3b2a

    aput v21, v2, v20

    const/16 v20, 0x494f

    aput v20, v2, v19

    const/16 v19, -0x70d9

    aput v19, v2, v18

    const/16 v18, -0x16

    aput v18, v2, v17

    const/16 v17, 0x1d28

    aput v17, v2, v16

    const/16 v16, 0x347a

    aput v16, v2, v15

    const/16 v15, -0x1dab

    aput v15, v2, v14

    const/16 v14, -0x70

    aput v14, v2, v13

    const/16 v13, -0x77

    aput v13, v2, v12

    const/16 v12, -0x3ee8

    aput v12, v2, v11

    const/16 v11, -0x51

    aput v11, v2, v10

    const/16 v10, -0x2aeb

    aput v10, v2, v9

    const/16 v9, -0x45

    aput v9, v2, v8

    const/16 v8, 0x5175

    aput v8, v2, v7

    const/16 v7, 0x3630

    aput v7, v2, v6

    const/16 v6, -0x1aab

    aput v6, v2, v5

    const/16 v5, -0x4a

    aput v5, v2, v3

    const/16 v3, -0x75dd

    aput v3, v2, v1

    const/16 v1, 0x23

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, 0x1d

    const/16 v34, 0x1e

    const/16 v35, 0x1f

    const/16 v36, 0x20

    const/16 v37, 0x21

    const/16 v38, 0x22

    const/16 v39, -0x60

    aput v39, v1, v38

    const/16 v38, -0xff0

    aput v38, v1, v37

    const/16 v37, -0x10

    aput v37, v1, v36

    const/16 v36, -0xfa6

    aput v36, v1, v35

    const/16 v35, -0x10

    aput v35, v1, v34

    const/16 v34, -0x4f

    aput v34, v1, v33

    const/16 v33, -0x3c

    aput v33, v1, v32

    const/16 v32, -0x32f2

    aput v32, v1, v31

    const/16 v31, -0x33

    aput v31, v1, v30

    const/16 v30, -0x79

    aput v30, v1, v29

    const/16 v29, -0x3596

    aput v29, v1, v28

    const/16 v28, -0x36

    aput v28, v1, v27

    const/16 v27, -0x5dff

    aput v27, v1, v26

    const/16 v26, -0x5e

    aput v26, v1, v25

    const/16 v25, -0x7f

    aput v25, v1, v24

    const/16 v24, -0x6a

    aput v24, v1, v23

    const/16 v23, -0x1

    aput v23, v1, v22

    const/16 v22, 0x3b69

    aput v22, v1, v21

    const/16 v21, 0x493b

    aput v21, v1, v20

    const/16 v20, -0x70b7

    aput v20, v1, v19

    const/16 v19, -0x71

    aput v19, v1, v18

    const/16 v18, 0x1d45

    aput v18, v1, v17

    const/16 v17, 0x341d

    aput v17, v1, v16

    const/16 v16, -0x1dcc

    aput v16, v1, v15

    const/16 v15, -0x1e

    aput v15, v1, v14

    const/16 v14, -0x31

    aput v14, v1, v13

    const/16 v13, -0x3e81

    aput v13, v1, v12

    const/16 v12, -0x3f

    aput v12, v1, v11

    const/16 v11, -0x2a84

    aput v11, v1, v10

    const/16 v10, -0x2b

    aput v10, v1, v9

    const/16 v9, 0x511b

    aput v9, v1, v8

    const/16 v8, 0x3651

    aput v8, v1, v7

    const/16 v7, -0x1aca

    aput v7, v1, v6

    const/16 v6, -0x1b

    aput v6, v1, v5

    const/16 v5, -0x75b2

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_7
    array-length v5, v1

    if-lt v3, v5, :cond_6

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_8
    array-length v5, v1

    if-lt v3, v5, :cond_7

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    :cond_4
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_5

    :cond_5
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_6

    :cond_6
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_7

    :cond_7
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_8
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 14

    const/4 v1, 0x2

    new-array v3, v1, [J

    const/4 v1, 0x1

    const-wide/16 v4, 0x2

    aput-wide v4, v3, v1

    const/4 v1, 0x0

    array-length v2, v3

    add-int/lit8 v2, v2, -0x1

    aget-wide v4, v3, v2

    long-to-int v2, v4

    if-gtz v2, :cond_0

    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, v1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    const/4 v4, 0x0

    int-to-long v1, p1

    const/16 v5, 0x20

    shl-long/2addr v1, v5

    const/16 v5, 0x20

    ushr-long v5, v1, v5

    aget-wide v1, v3, v4

    const-wide/16 v7, 0x0

    cmp-long v7, v1, v7

    if-eqz v7, :cond_1

    const-wide v7, -0x77c8759cc9b3035dL    # -4.455956851585829E-269

    xor-long/2addr v1, v7

    :cond_1
    const/16 v7, 0x20

    ushr-long/2addr v1, v7

    const/16 v7, 0x20

    shl-long/2addr v1, v7

    xor-long/2addr v1, v5

    const-wide v5, -0x77c8759cc9b3035dL    # -4.455956851585829E-269

    xor-long/2addr v1, v5

    aput-wide v1, v3, v4

    const/4 v1, 0x1

    array-length v2, v3

    add-int/lit8 v2, v2, -0x1

    aget-wide v4, v3, v2

    long-to-int v2, v4

    if-lt v1, v2, :cond_2

    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, v1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_2
    const/4 v4, 0x0

    move/from16 v0, p2

    int-to-long v1, v0

    const/16 v5, 0x20

    shl-long v5, v1, v5

    aget-wide v1, v3, v4

    const-wide/16 v7, 0x0

    cmp-long v7, v1, v7

    if-eqz v7, :cond_3

    const-wide v7, -0x77c8759cc9b3035dL    # -4.455956851585829E-269

    xor-long/2addr v1, v7

    :cond_3
    const/16 v7, 0x20

    shl-long/2addr v1, v7

    const/16 v7, 0x20

    ushr-long/2addr v1, v7

    xor-long/2addr v1, v5

    const-wide v5, -0x77c8759cc9b3035dL    # -4.455956851585829E-269

    xor-long/2addr v1, v5

    aput-wide v1, v3, v4

    const/4 v5, 0x0

    const/4 v4, 0x0

    array-length v1, v3

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v3, v1

    long-to-int v1, v1

    if-gtz v1, :cond_6

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, 0x47e

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, 0x44e

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v5, v1

    if-lt v3, v5, :cond_4

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v5, v1

    if-lt v3, v5, :cond_5

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_4
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_5
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_6
    const/4 v1, 0x0

    aget-wide v1, v3, v1

    const-wide/16 v6, 0x0

    cmp-long v6, v1, v6

    if-eqz v6, :cond_7

    const-wide v6, -0x77c8759cc9b3035dL    # -4.455956851585829E-269

    xor-long/2addr v1, v6

    :cond_7
    const/16 v6, 0x20

    shl-long/2addr v1, v6

    const/16 v6, 0x20

    shr-long/2addr v1, v6

    long-to-int v6, v1

    array-length v1, v3

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v3, v1

    long-to-int v1, v1

    const/4 v2, 0x1

    if-gt v1, v2, :cond_a

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, -0x8

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, -0x37

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v5, v1

    if-lt v3, v5, :cond_8

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v5, v1

    if-lt v3, v5, :cond_9

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_8
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_9
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_a
    const/4 v1, 0x0

    aget-wide v1, v3, v1

    const-wide/16 v7, 0x0

    cmp-long v7, v1, v7

    if-eqz v7, :cond_b

    const-wide v7, -0x77c8759cc9b3035dL    # -4.455956851585829E-269

    xor-long/2addr v1, v7

    :cond_b
    const/16 v7, 0x20

    shr-long/2addr v1, v7

    long-to-int v1, v1

    move-object/from16 v0, p3

    invoke-super {p0, v6, v1, v0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onActivityResult(IILandroid/content/Intent;)V

    array-length v1, v3

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v3, v1

    long-to-int v1, v1

    if-gtz v1, :cond_e

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, -0x40

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, -0x10

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_4
    array-length v5, v1

    if-lt v3, v5, :cond_c

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_5
    array-length v5, v1

    if-lt v3, v5, :cond_d

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_c
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    :cond_d
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    :cond_e
    const/4 v1, 0x0

    aget-wide v1, v3, v1

    const-wide/16 v6, 0x0

    cmp-long v3, v1, v6

    if-eqz v3, :cond_f

    const-wide v6, -0x77c8759cc9b3035dL    # -4.455956851585829E-269

    xor-long/2addr v1, v6

    :cond_f
    const/16 v3, 0x20

    shl-long/2addr v1, v3

    const/16 v3, 0x20

    shr-long/2addr v1, v3

    long-to-int v1, v1

    const/16 v2, 0x3e9

    if-ne v1, v2, :cond_10

    :try_start_0
    invoke-virtual/range {p3 .. p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    const/16 v1, 0x8

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, -0x7d

    aput v12, v2, v11

    const/16 v11, 0x5d6a

    aput v11, v2, v10

    const/16 v10, -0x5bc8

    aput v10, v2, v9

    const/16 v9, -0x39

    aput v9, v2, v8

    const/16 v8, -0x1e

    aput v8, v2, v7

    const/16 v7, 0x726e

    aput v7, v2, v6

    const/16 v6, -0x7fe9

    aput v6, v2, v3

    const/16 v3, -0x3c

    aput v3, v2, v1

    const/16 v1, 0x8

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, -0x39

    aput v13, v1, v12

    const/16 v12, 0x5d23

    aput v12, v1, v11

    const/16 v11, -0x5ba3

    aput v11, v1, v10

    const/16 v10, -0x5c

    aput v10, v1, v9

    const/16 v9, -0x75

    aput v9, v1, v8

    const/16 v8, 0x7218

    aput v8, v1, v7

    const/16 v7, -0x7f8e

    aput v7, v1, v6

    const/16 v6, -0x80

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_6
    array-length v6, v1

    if-lt v3, v6, :cond_11

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_7
    array-length v6, v1

    if-lt v3, v6, :cond_12

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1a

    const/16 v1, 0x8

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, -0x47

    aput v11, v2, v10

    const/16 v10, -0x20

    aput v10, v2, v9

    const/16 v9, 0x4f01

    aput v9, v2, v8

    const/16 v8, -0x28d4

    aput v8, v2, v7

    const/16 v7, -0x42

    aput v7, v2, v6

    const/16 v6, -0x19

    aput v6, v2, v4

    const/16 v4, -0x1b85

    aput v4, v2, v3

    const/16 v3, -0x60

    aput v3, v2, v1

    const/16 v1, 0x8

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/4 v12, -0x3

    aput v12, v1, v11

    const/16 v11, -0x57

    aput v11, v1, v10

    const/16 v10, 0x4f64

    aput v10, v1, v9

    const/16 v9, -0x28b1

    aput v9, v1, v8

    const/16 v8, -0x29

    aput v8, v1, v7

    const/16 v7, -0x6f

    aput v7, v1, v6

    const/16 v6, -0x1be2

    aput v6, v1, v4

    const/16 v4, -0x1c

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_8
    array-length v4, v1

    if-lt v3, v4, :cond_13

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_9
    array-length v4, v1

    if-lt v3, v4, :cond_14

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    :goto_a
    const/4 v1, 0x6

    new-array v3, v1, [I

    const/4 v1, 0x0

    const/4 v4, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/16 v10, -0x4c

    aput v10, v3, v9

    const/16 v9, -0x5a

    aput v9, v3, v8

    const/16 v8, -0x15ad

    aput v8, v3, v7

    const/16 v7, -0x62

    aput v7, v3, v6

    const/16 v6, -0x51

    aput v6, v3, v4

    const/16 v4, -0xbcf

    aput v4, v3, v1

    const/4 v1, 0x6

    new-array v1, v1, [I

    const/4 v4, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/16 v11, -0x26

    aput v11, v1, v10

    const/16 v10, -0x37

    aput v10, v1, v9

    const/16 v9, -0x15c6

    aput v9, v1, v8

    const/16 v8, -0x16

    aput v8, v1, v7

    const/16 v7, -0x34

    aput v7, v1, v6

    const/16 v6, -0xb90

    aput v6, v1, v4

    const/4 v4, 0x0

    :goto_b
    array-length v6, v1

    if-lt v4, v6, :cond_15

    array-length v1, v3

    new-array v1, v1, [C

    const/4 v4, 0x0

    :goto_c
    array-length v6, v1

    if-lt v4, v6, :cond_16

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_19

    const/4 v1, 0x6

    new-array v3, v1, [I

    const/4 v1, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/16 v9, -0x50

    aput v9, v3, v8

    const/16 v8, -0x58

    aput v8, v3, v7

    const/16 v7, -0x52

    aput v7, v3, v6

    const/16 v6, 0x7835

    aput v6, v3, v5

    const/16 v5, 0x761b

    aput v5, v3, v4

    const/16 v4, -0x17c9

    aput v4, v3, v1

    const/4 v1, 0x6

    new-array v1, v1, [I

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/16 v10, -0x22

    aput v10, v1, v9

    const/16 v9, -0x39

    aput v9, v1, v8

    const/16 v8, -0x39

    aput v8, v1, v7

    const/16 v7, 0x7841

    aput v7, v1, v6

    const/16 v6, 0x7678

    aput v6, v1, v5

    const/16 v5, -0x178a

    aput v5, v1, v4

    const/4 v4, 0x0

    :goto_d
    array-length v5, v1

    if-lt v4, v5, :cond_17

    array-length v1, v3

    new-array v1, v1, [C

    const/4 v4, 0x0

    :goto_e
    array-length v5, v1

    if-lt v4, v5, :cond_18

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v3, v1

    :goto_f
    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mPairedDeviceMap:Ljava/util/concurrent/ConcurrentHashMap;

    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    :try_start_2
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_2

    iget-object v4, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mScanningFragmentController:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mPairedDeviceMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getDeviceConnectivityType()I

    move-result v5

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mPairedDeviceMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getDeviceType()I

    move-result v6

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mPairedDeviceMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getDeviceDataType()I

    move-result v1

    invoke-virtual {v4, v5, v6, v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;->requestPairedDeviceForRename(IIILjava/lang/String;)Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    move-result-object v1

    invoke-virtual {p0, v1, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->settingInfo(Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->deviceIDRename:Ljava/lang/String;

    :cond_10
    :goto_10
    return-void

    :cond_11
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_6

    :cond_12
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_7

    :cond_13
    aget v4, v1, v3

    aget v6, v2, v3

    xor-int/2addr v4, v6

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_8

    :cond_14
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_9

    :cond_15
    aget v6, v1, v4

    aget v7, v3, v4

    xor-int/2addr v6, v7

    aput v6, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_b

    :cond_16
    aget v6, v3, v4

    int-to-char v6, v6

    aput-char v6, v1, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_c

    :cond_17
    aget v5, v1, v4

    aget v6, v3, v4

    xor-int/2addr v5, v6

    aput v5, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_d

    :cond_18
    aget v5, v3, v4

    int-to-char v5, v5

    aput-char v5, v1, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_e

    :catch_0
    move-exception v1

    goto :goto_10

    :catch_1
    move-exception v1

    goto :goto_10

    :catch_2
    move-exception v1

    goto :goto_10

    :cond_19
    move-object v3, v5

    goto/16 :goto_f

    :cond_1a
    move-object v2, v4

    goto/16 :goto_a
.end method

.method public onBackPressed()V
    .locals 23

    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0x363e

    aput v12, v2, v11

    const/16 v11, 0x2a55

    aput v11, v2, v10

    const/16 v10, 0x7c43

    aput v10, v2, v9

    const/16 v9, -0x67f6

    aput v9, v2, v8

    const/16 v8, -0x16

    aput v8, v2, v7

    const/16 v7, 0x93f

    aput v7, v2, v6

    const/16 v6, -0x73a6

    aput v6, v2, v5

    const/16 v5, -0x12

    aput v5, v2, v4

    const/16 v4, -0x4acf

    aput v4, v2, v3

    const/16 v3, -0x27

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0x365b

    aput v13, v1, v12

    const/16 v12, 0x2a36

    aput v12, v1, v11

    const/16 v11, 0x7c2a

    aput v11, v1, v10

    const/16 v10, -0x6784

    aput v10, v1, v9

    const/16 v9, -0x68

    aput v9, v1, v8

    const/16 v8, 0x95a

    aput v8, v1, v7

    const/16 v7, -0x73f7

    aput v7, v1, v6

    const/16 v6, -0x74

    aput v6, v1, v5

    const/16 v5, -0x4aa8

    aput v5, v1, v4

    const/16 v4, -0x4b

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v4, v1

    if-lt v3, v4, :cond_0

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v4, v1

    if-lt v3, v4, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    const/16 v1, 0x12

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, -0x13db

    aput v21, v2, v20

    const/16 v20, -0x77

    aput v20, v2, v19

    const/16 v19, -0x68e8

    aput v19, v2, v18

    const/16 v18, -0x1c

    aput v18, v2, v17

    const/16 v17, -0x38

    aput v17, v2, v16

    const/16 v16, 0x6033

    aput v16, v2, v15

    const/16 v15, 0x3510

    aput v15, v2, v14

    const/16 v14, 0x105e

    aput v14, v2, v13

    const/16 v13, 0x2f73

    aput v13, v2, v12

    const/16 v12, 0x124e

    aput v12, v2, v11

    const/16 v11, -0x2390

    aput v11, v2, v10

    const/4 v10, -0x4

    aput v10, v2, v9

    const/16 v9, -0xd

    aput v9, v2, v8

    const/16 v8, -0x2a

    aput v8, v2, v7

    const/16 v7, -0x42

    aput v7, v2, v6

    const/16 v6, -0x15

    aput v6, v2, v5

    const/16 v5, -0x5db4

    aput v5, v2, v3

    const/16 v3, -0x15

    aput v3, v2, v1

    const/16 v1, 0x12

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, -0x13bf

    aput v22, v1, v21

    const/16 v21, -0x14

    aput v21, v1, v20

    const/16 v20, -0x6895

    aput v20, v1, v19

    const/16 v19, -0x69

    aput v19, v1, v18

    const/16 v18, -0x53

    aput v18, v1, v17

    const/16 v17, 0x6041

    aput v17, v1, v16

    const/16 v16, 0x3560

    aput v16, v1, v15

    const/16 v15, 0x1035

    aput v15, v1, v14

    const/16 v14, 0x2f10

    aput v14, v1, v13

    const/16 v13, 0x122f

    aput v13, v1, v12

    const/16 v12, -0x23ee

    aput v12, v1, v11

    const/16 v11, -0x24

    aput v11, v1, v10

    const/16 v10, -0x6a

    aput v10, v1, v9

    const/16 v9, -0x4e

    aput v9, v1, v8

    const/16 v8, -0x29

    aput v8, v1, v7

    const/16 v7, -0x68

    aput v7, v1, v6

    const/16 v6, -0x5dde

    aput v6, v1, v5

    const/16 v5, -0x5e

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v5, v1

    if-lt v3, v5, :cond_2

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v5, v1

    if-lt v3, v5, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->isScanning:Z

    if-eqz v1, :cond_4

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->stopDiscoveringSensorDevices()V

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->stopSearch()V

    :goto_4
    return-void

    :cond_0
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_1
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_2
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_3
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->finish()V

    invoke-super/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onBackPressed()V

    goto :goto_4
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 51

    invoke-super/range {p0 .. p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onCreate(Landroid/os/Bundle;)V

    const/4 v1, 0x1

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->isFragmetRunning:Z

    new-instance v4, Landroid/content/IntentFilter;

    const/16 v1, 0x2e

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, 0x1d

    const/16 v33, 0x1e

    const/16 v34, 0x1f

    const/16 v35, 0x20

    const/16 v36, 0x21

    const/16 v37, 0x22

    const/16 v38, 0x23

    const/16 v39, 0x24

    const/16 v40, 0x25

    const/16 v41, 0x26

    const/16 v42, 0x27

    const/16 v43, 0x28

    const/16 v44, 0x29

    const/16 v45, 0x2a

    const/16 v46, 0x2b

    const/16 v47, 0x2c

    const/16 v48, 0x2d

    const/16 v49, -0x4af9

    aput v49, v2, v48

    const/16 v48, -0x10

    aput v48, v2, v47

    const/16 v47, -0x54b3

    aput v47, v2, v46

    const/16 v46, -0x1b

    aput v46, v2, v45

    const/16 v45, -0x2d

    aput v45, v2, v44

    const/16 v44, -0x6ab3

    aput v44, v2, v43

    const/16 v43, -0x2a

    aput v43, v2, v42

    const/16 v42, 0x420

    aput v42, v2, v41

    const/16 v41, -0x1cbf

    aput v41, v2, v40

    const/16 v40, -0x49

    aput v40, v2, v39

    const/16 v39, -0x3c

    aput v39, v2, v38

    const/16 v38, -0x24

    aput v38, v2, v37

    const/16 v37, -0x20c9

    aput v37, v2, v36

    const/16 v36, -0xf

    aput v36, v2, v35

    const/16 v35, -0x4a

    aput v35, v2, v34

    const/16 v34, 0xa2c

    aput v34, v2, v33

    const/16 v33, 0x3b63

    aput v33, v2, v32

    const/16 v32, 0x794f

    aput v32, v2, v31

    const/16 v31, 0x251a

    aput v31, v2, v30

    const/16 v30, 0x6644

    aput v30, v2, v29

    const/16 v29, -0x34b8

    aput v29, v2, v28

    const/16 v28, -0x47

    aput v28, v2, v27

    const/16 v27, -0x7fd0

    aput v27, v2, v26

    const/16 v26, -0xc

    aput v26, v2, v25

    const/16 v25, -0x71b9

    aput v25, v2, v24

    const/16 v24, -0x11

    aput v24, v2, v23

    const/16 v23, -0xa88

    aput v23, v2, v22

    const/16 v22, -0x6c

    aput v22, v2, v21

    const/16 v21, -0x19f

    aput v21, v2, v20

    const/16 v20, -0x6a

    aput v20, v2, v19

    const/16 v19, -0x7b7

    aput v19, v2, v18

    const/16 v18, -0x69

    aput v18, v2, v17

    const/16 v17, -0xf

    aput v17, v2, v16

    const/16 v16, 0x135f

    aput v16, v2, v15

    const/16 v15, 0x4776

    aput v15, v2, v14

    const/16 v14, 0x2732

    aput v14, v2, v13

    const/16 v13, -0x63b5

    aput v13, v2, v12

    const/4 v12, -0x2

    aput v12, v2, v11

    const/16 v11, -0x14

    aput v11, v2, v10

    const/16 v10, 0x237b

    aput v10, v2, v9

    const/16 v9, 0x794a

    aput v9, v2, v8

    const/16 v8, -0x16ea

    aput v8, v2, v7

    const/16 v7, -0x65

    aput v7, v2, v6

    const/16 v6, -0xe

    aput v6, v2, v5

    const/16 v5, -0x2993

    aput v5, v2, v3

    const/16 v3, -0x49

    aput v3, v2, v1

    const/16 v1, 0x2e

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, 0x1d

    const/16 v34, 0x1e

    const/16 v35, 0x1f

    const/16 v36, 0x20

    const/16 v37, 0x21

    const/16 v38, 0x22

    const/16 v39, 0x23

    const/16 v40, 0x24

    const/16 v41, 0x25

    const/16 v42, 0x26

    const/16 v43, 0x27

    const/16 v44, 0x28

    const/16 v45, 0x29

    const/16 v46, 0x2a

    const/16 v47, 0x2b

    const/16 v48, 0x2c

    const/16 v49, 0x2d

    const/16 v50, -0x4abd

    aput v50, v1, v49

    const/16 v49, -0x4b

    aput v49, v1, v48

    const/16 v48, -0x54f6

    aput v48, v1, v47

    const/16 v47, -0x55

    aput v47, v1, v46

    const/16 v46, -0x6e

    aput v46, v1, v45

    const/16 v45, -0x6afb

    aput v45, v1, v44

    const/16 v44, -0x6b

    aput v44, v1, v43

    const/16 v43, 0x47f

    aput v43, v1, v42

    const/16 v42, -0x1cfc

    aput v42, v1, v41

    const/16 v41, -0x1d

    aput v41, v1, v40

    const/16 v40, -0x7b

    aput v40, v1, v39

    const/16 v39, -0x78

    aput v39, v1, v38

    const/16 v38, -0x209c

    aput v38, v1, v37

    const/16 v37, -0x21

    aput v37, v1, v36

    const/16 v36, -0x28

    aput v36, v1, v35

    const/16 v35, 0xa43

    aput v35, v1, v34

    const/16 v34, 0x3b0a

    aput v34, v1, v33

    const/16 v33, 0x793b

    aput v33, v1, v32

    const/16 v32, 0x2579

    aput v32, v1, v31

    const/16 v31, 0x6625

    aput v31, v1, v30

    const/16 v30, -0x349a

    aput v30, v1, v29

    const/16 v29, -0x35

    aput v29, v1, v28

    const/16 v28, -0x7fab

    aput v28, v1, v27

    const/16 v27, -0x80

    aput v27, v1, v26

    const/16 v26, -0x71c9

    aput v26, v1, v25

    const/16 v25, -0x72

    aput v25, v1, v24

    const/16 v24, -0xae4

    aput v24, v1, v23

    const/16 v23, -0xb

    aput v23, v1, v22

    const/16 v22, -0x1b1

    aput v22, v1, v21

    const/16 v21, -0x2

    aput v21, v1, v20

    const/16 v20, -0x7c3

    aput v20, v1, v19

    const/16 v19, -0x8

    aput v19, v1, v18

    const/16 v18, -0x62

    aput v18, v1, v17

    const/16 v17, 0x132b

    aput v17, v1, v16

    const/16 v16, 0x4713

    aput v16, v1, v15

    const/16 v15, 0x2747

    aput v15, v1, v14

    const/16 v14, -0x63d9

    aput v14, v1, v13

    const/16 v13, -0x64

    aput v13, v1, v12

    const/16 v12, -0x3e

    aput v12, v1, v11

    const/16 v11, 0x231f

    aput v11, v1, v10

    const/16 v10, 0x7923

    aput v10, v1, v9

    const/16 v9, -0x1687

    aput v9, v1, v8

    const/16 v8, -0x17

    aput v8, v1, v7

    const/16 v7, -0x6a

    aput v7, v1, v6

    const/16 v6, -0x29fd

    aput v6, v1, v5

    const/16 v5, -0x2a

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v5, v1

    if-lt v3, v5, :cond_c

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v5, v1

    if-lt v3, v5, :cond_d

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v4}, Landroid/support/v4/app/FragmentActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const/16 v1, 0x8

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, -0x6b

    aput v11, v2, v10

    const/16 v10, -0x3ada

    aput v10, v2, v9

    const/16 v9, -0x44

    aput v9, v2, v8

    const/16 v8, -0x5f8

    aput v8, v2, v7

    const/16 v7, -0x65

    aput v7, v2, v6

    const/16 v6, -0x3edc

    aput v6, v2, v5

    const/16 v5, -0x60

    aput v5, v2, v3

    const/16 v3, -0x5a

    aput v3, v2, v1

    const/16 v1, 0x8

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, -0x10

    aput v12, v1, v11

    const/16 v11, -0x3aaa

    aput v11, v1, v10

    const/16 v10, -0x3b

    aput v10, v1, v9

    const/16 v9, -0x5a4

    aput v9, v1, v8

    const/4 v8, -0x6

    aput v8, v1, v7

    const/16 v7, -0x3eb0

    aput v7, v1, v6

    const/16 v6, -0x3f

    aput v6, v1, v5

    const/16 v5, -0x1e

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v5, v1

    if-lt v3, v5, :cond_e

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v5, v1

    if-lt v3, v5, :cond_f

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const/16 v1, 0x8

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, -0x6e89

    aput v11, v2, v10

    const/16 v10, -0x1f

    aput v10, v2, v9

    const/16 v9, -0x37a1

    aput v9, v2, v8

    const/16 v8, -0x64

    aput v8, v2, v7

    const/16 v7, 0x783a

    aput v7, v2, v6

    const/16 v6, 0x230c

    aput v6, v2, v5

    const/16 v5, 0x1a42

    aput v5, v2, v3

    const/16 v3, -0x44a2

    aput v3, v2, v1

    const/16 v1, 0x8

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, -0x6eee

    aput v12, v1, v11

    const/16 v11, -0x6f

    aput v11, v1, v10

    const/16 v10, -0x37da

    aput v10, v1, v9

    const/16 v9, -0x38

    aput v9, v1, v8

    const/16 v8, 0x785b

    aput v8, v1, v7

    const/16 v7, 0x2378

    aput v7, v1, v6

    const/16 v6, 0x1a23

    aput v6, v1, v5

    const/16 v5, -0x44e6

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_4
    array-length v5, v1

    if-lt v3, v5, :cond_10

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_5
    array-length v5, v1

    if-lt v3, v5, :cond_11

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mDataType:I

    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const/16 v1, 0xb

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, -0x55a6

    aput v14, v2, v13

    const/16 v13, -0x31

    aput v13, v2, v12

    const/16 v12, -0x33

    aput v12, v2, v11

    const/16 v11, -0x1d

    aput v11, v2, v10

    const/16 v10, -0x429d

    aput v10, v2, v9

    const/16 v9, -0x28

    aput v9, v2, v8

    const/16 v8, -0x27

    aput v8, v2, v7

    const/16 v7, -0x40c6

    aput v7, v2, v6

    const/16 v6, -0x37

    aput v6, v2, v5

    const/16 v5, 0xa5f

    aput v5, v2, v3

    const/16 v3, -0x45b2

    aput v3, v2, v1

    const/16 v1, 0xb

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, -0x55d7

    aput v15, v1, v14

    const/16 v14, -0x56

    aput v14, v1, v13

    const/16 v13, -0x43

    aput v13, v1, v12

    const/16 v12, -0x66

    aput v12, v1, v11

    const/16 v11, -0x42c9

    aput v11, v1, v10

    const/16 v10, -0x43

    aput v10, v1, v9

    const/16 v9, -0x46

    aput v9, v1, v8

    const/16 v8, -0x40ad

    aput v8, v1, v7

    const/16 v7, -0x41

    aput v7, v1, v6

    const/16 v6, 0xa3a

    aput v6, v1, v5

    const/16 v5, -0x45f6

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_6
    array-length v5, v1

    if-lt v3, v5, :cond_12

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_7
    array-length v5, v1

    if-lt v3, v5, :cond_13

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const/16 v1, 0xb

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, -0x17

    aput v14, v2, v13

    const/16 v13, 0x541e

    aput v13, v2, v12

    const/16 v12, -0x29dc

    aput v12, v2, v11

    const/16 v11, -0x51

    aput v11, v2, v10

    const/16 v10, 0x1b5c

    aput v10, v2, v9

    const/16 v9, -0x4082

    aput v9, v2, v8

    const/16 v8, -0x24

    aput v8, v2, v7

    const/16 v7, -0x29c6

    aput v7, v2, v6

    const/16 v6, -0x60

    aput v6, v2, v5

    const/16 v5, -0x7e

    aput v5, v2, v3

    const/16 v3, -0x16a0

    aput v3, v2, v1

    const/16 v1, 0xb

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, -0x66

    aput v15, v1, v14

    const/16 v14, 0x547b

    aput v14, v1, v13

    const/16 v13, -0x29ac

    aput v13, v1, v12

    const/16 v12, -0x2a

    aput v12, v1, v11

    const/16 v11, 0x1b08

    aput v11, v1, v10

    const/16 v10, -0x40e5

    aput v10, v1, v9

    const/16 v9, -0x41

    aput v9, v1, v8

    const/16 v8, -0x29ad

    aput v8, v1, v7

    const/16 v7, -0x2a

    aput v7, v1, v6

    const/16 v6, -0x19

    aput v6, v1, v5

    const/16 v5, -0x16dc

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_8
    array-length v5, v1

    if-lt v3, v5, :cond_14

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_9
    array-length v5, v1

    if-lt v3, v5, :cond_15

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->getIntegerArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mDeviceTypes:Ljava/util/ArrayList;

    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const/16 v1, 0x10

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, -0x70

    aput v19, v2, v18

    const/16 v18, -0x1b

    aput v18, v2, v17

    const/16 v17, -0x6f

    aput v17, v2, v16

    const/16 v16, -0x25a0

    aput v16, v2, v15

    const/16 v15, -0x5d

    aput v15, v2, v14

    const/16 v14, -0x6787

    aput v14, v2, v13

    const/16 v13, -0xf

    aput v13, v2, v12

    const/16 v12, 0x867

    aput v12, v2, v11

    const/16 v11, 0x7661

    aput v11, v2, v10

    const/16 v10, -0x31fe

    aput v10, v2, v9

    const/16 v9, -0x53

    aput v9, v2, v8

    const/16 v8, 0x236e

    aput v8, v2, v7

    const/16 v7, -0x1ab3

    aput v7, v2, v6

    const/16 v6, -0x75

    aput v6, v2, v5

    const/16 v5, -0x18

    aput v5, v2, v3

    const/16 v3, -0x3b

    aput v3, v2, v1

    const/16 v1, 0x10

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, -0xb

    aput v20, v1, v19

    const/16 v19, -0x6b

    aput v19, v1, v18

    const/16 v18, -0x18

    aput v18, v1, v17

    const/16 v17, -0x25cc

    aput v17, v1, v16

    const/16 v16, -0x26

    aput v16, v1, v15

    const/16 v15, -0x67f3

    aput v15, v1, v14

    const/16 v14, -0x68

    aput v14, v1, v13

    const/16 v13, 0x811

    aput v13, v1, v12

    const/16 v12, 0x7608

    aput v12, v1, v11

    const/16 v11, -0x318a

    aput v11, v1, v10

    const/16 v10, -0x32

    aput v10, v1, v9

    const/16 v9, 0x230b

    aput v9, v1, v8

    const/16 v8, -0x1add

    aput v8, v1, v7

    const/16 v7, -0x1b

    aput v7, v1, v6

    const/16 v6, -0x79

    aput v6, v1, v5

    const/16 v5, -0x7a

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_a
    array-length v5, v1

    if-lt v3, v5, :cond_16

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_b
    array-length v5, v1

    if-lt v3, v5, :cond_17

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const/16 v1, 0x10

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, -0x5d

    aput v19, v2, v18

    const/16 v18, -0x69b7

    aput v18, v2, v17

    const/16 v17, -0x11

    aput v17, v2, v16

    const/16 v16, -0x2b

    aput v16, v2, v15

    const/16 v15, 0x2f61

    aput v15, v2, v14

    const/16 v14, -0x15a5

    aput v14, v2, v13

    const/16 v13, -0x7d

    aput v13, v2, v12

    const/16 v12, 0x1233

    aput v12, v2, v11

    const/16 v11, 0x417b

    aput v11, v2, v10

    const/16 v10, -0x39cb

    aput v10, v2, v9

    const/16 v9, -0x5b

    aput v9, v2, v8

    const/16 v8, -0x77

    aput v8, v2, v7

    const/16 v7, -0x25

    aput v7, v2, v6

    const/4 v6, -0x3

    aput v6, v2, v5

    const/16 v5, -0x2c

    aput v5, v2, v3

    const/16 v3, -0x2700

    aput v3, v2, v1

    const/16 v1, 0x10

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, -0x3a

    aput v20, v1, v19

    const/16 v19, -0x69c7

    aput v19, v1, v18

    const/16 v18, -0x6a

    aput v18, v1, v17

    const/16 v17, -0x7f

    aput v17, v1, v16

    const/16 v16, 0x2f18

    aput v16, v1, v15

    const/16 v15, -0x15d1

    aput v15, v1, v14

    const/16 v14, -0x16

    aput v14, v1, v13

    const/16 v13, 0x1245

    aput v13, v1, v12

    const/16 v12, 0x4112

    aput v12, v1, v11

    const/16 v11, -0x39bf

    aput v11, v1, v10

    const/16 v10, -0x3a

    aput v10, v1, v9

    const/16 v9, -0x14

    aput v9, v1, v8

    const/16 v8, -0x4b

    aput v8, v1, v7

    const/16 v7, -0x6d

    aput v7, v1, v6

    const/16 v6, -0x45

    aput v6, v1, v5

    const/16 v5, -0x26bd

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_c
    array-length v5, v1

    if-lt v3, v5, :cond_18

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_d
    array-length v5, v1

    if-lt v3, v5, :cond_19

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mConnectivityType:I

    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const/16 v1, 0x12

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x572c

    aput v21, v2, v20

    const/16 v20, 0x703e

    aput v20, v2, v19

    const/16 v19, 0x672f

    aput v19, v2, v18

    const/16 v18, -0x4fec

    aput v18, v2, v17

    const/16 v17, -0x2b

    aput v17, v2, v16

    const/16 v16, -0x6f

    aput v16, v2, v15

    const/16 v15, -0x1a

    aput v15, v2, v14

    const/16 v14, -0x20

    aput v14, v2, v13

    const/16 v13, -0x5c8f

    aput v13, v2, v12

    const/16 v12, -0x3a

    aput v12, v2, v11

    const/16 v11, 0x4e29

    aput v11, v2, v10

    const/16 v10, 0x311

    aput v10, v2, v9

    const/16 v9, -0x399

    aput v9, v2, v8

    const/16 v8, -0x67

    aput v8, v2, v7

    const/16 v7, -0x42be

    aput v7, v2, v6

    const/16 v6, -0x2c

    aput v6, v2, v5

    const/16 v5, -0x26cc

    aput v5, v2, v3

    const/16 v3, -0x57

    aput v3, v2, v1

    const/16 v1, 0x12

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x5748

    aput v22, v1, v21

    const/16 v21, 0x7057

    aput v21, v1, v20

    const/16 v20, 0x6770

    aput v20, v1, v19

    const/16 v19, -0x4f99

    aput v19, v1, v18

    const/16 v18, -0x50

    aput v18, v1, v17

    const/16 v17, -0x1d

    aput v17, v1, v16

    const/16 v16, -0x47

    aput v16, v1, v15

    const/16 v15, -0x6c

    aput v15, v1, v14

    const/16 v14, -0x5cf7

    aput v14, v1, v13

    const/16 v13, -0x5d

    aput v13, v1, v12

    const/16 v12, 0x4e5d

    aput v12, v1, v11

    const/16 v11, 0x34e

    aput v11, v1, v10

    const/16 v10, -0x3fd

    aput v10, v1, v9

    const/4 v9, -0x4

    aput v9, v1, v8

    const/16 v8, -0x42d0

    aput v8, v1, v7

    const/16 v7, -0x43

    aput v7, v1, v6

    const/16 v6, -0x26ab

    aput v6, v1, v5

    const/16 v5, -0x27

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_e
    array-length v5, v1

    if-lt v3, v5, :cond_1a

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_f
    array-length v5, v1

    if-lt v3, v5, :cond_1b

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1e

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const/16 v1, 0x12

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, -0x28c5

    aput v21, v2, v20

    const/16 v20, -0x42

    aput v20, v2, v19

    const/16 v19, -0x45

    aput v19, v2, v18

    const/16 v18, 0x5364

    aput v18, v2, v17

    const/16 v17, -0x4ca

    aput v17, v2, v16

    const/16 v16, -0x77

    aput v16, v2, v15

    const/16 v15, -0x6d

    aput v15, v2, v14

    const/16 v14, -0x28

    aput v14, v2, v13

    const/16 v13, 0x1140

    aput v13, v2, v12

    const/16 v12, 0x7774

    aput v12, v2, v11

    const/16 v11, -0x70fd

    aput v11, v2, v10

    const/16 v10, -0x30

    aput v10, v2, v9

    const/16 v9, -0x37

    aput v9, v2, v8

    const/16 v8, 0x5800

    aput v8, v2, v7

    const/16 v7, 0x5c2a

    aput v7, v2, v6

    const/16 v6, -0x6dcb

    aput v6, v2, v5

    const/16 v5, -0xd

    aput v5, v2, v3

    const/16 v3, 0x3308

    aput v3, v2, v1

    const/16 v1, 0x12

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, -0x28a1

    aput v22, v1, v21

    const/16 v21, -0x29

    aput v21, v1, v20

    const/16 v20, -0x1c

    aput v20, v1, v19

    const/16 v19, 0x5317

    aput v19, v1, v18

    const/16 v18, -0x4ad

    aput v18, v1, v17

    const/16 v17, -0x5

    aput v17, v1, v16

    const/16 v16, -0x34

    aput v16, v1, v15

    const/16 v15, -0x54

    aput v15, v1, v14

    const/16 v14, 0x1138

    aput v14, v1, v13

    const/16 v13, 0x7711

    aput v13, v1, v12

    const/16 v12, -0x7089

    aput v12, v1, v11

    const/16 v11, -0x71

    aput v11, v1, v10

    const/16 v10, -0x53

    aput v10, v1, v9

    const/16 v9, 0x5865

    aput v9, v1, v8

    const/16 v8, 0x5c58

    aput v8, v1, v7

    const/16 v7, -0x6da4

    aput v7, v1, v6

    const/16 v6, -0x6e

    aput v6, v1, v5

    const/16 v5, 0x3378

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_10
    array-length v5, v1

    if-lt v3, v5, :cond_1c

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_11
    array-length v5, v1

    if-lt v3, v5, :cond_1d

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v4, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mPairedTextResId:I

    :cond_3
    :goto_12
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const/16 v1, 0x15

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, -0x7d7

    aput v24, v2, v23

    const/16 v23, -0x6f

    aput v23, v2, v22

    const/16 v22, -0x2f

    aput v22, v2, v21

    const/16 v21, -0x54b7

    aput v21, v2, v20

    const/16 v20, -0x32

    aput v20, v2, v19

    const/16 v19, -0x37d7

    aput v19, v2, v18

    const/16 v18, -0x69

    aput v18, v2, v17

    const/16 v17, 0x5a2c

    aput v17, v2, v16

    const/16 v16, -0x42de

    aput v16, v2, v15

    const/16 v15, -0x28

    aput v15, v2, v14

    const/16 v14, -0x63a5

    aput v14, v2, v13

    const/16 v13, -0x3d

    aput v13, v2, v12

    const/16 v12, -0x6e

    aput v12, v2, v11

    const/16 v11, -0x67

    aput v11, v2, v10

    const/16 v10, -0x2b

    aput v10, v2, v9

    const/16 v9, -0x64

    aput v9, v2, v8

    const/16 v8, -0x5ece

    aput v8, v2, v7

    const/16 v7, -0x3b

    aput v7, v2, v6

    const/16 v6, -0x1aa

    aput v6, v2, v5

    const/16 v5, -0x6f

    aput v5, v2, v3

    const/16 v3, -0x4a

    aput v3, v2, v1

    const/16 v1, 0x15

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, -0x7b3

    aput v25, v1, v24

    const/16 v24, -0x8

    aput v24, v1, v23

    const/16 v23, -0x72

    aput v23, v1, v22

    const/16 v22, -0x54c6

    aput v22, v1, v21

    const/16 v21, -0x55

    aput v21, v1, v20

    const/16 v20, -0x37a5

    aput v20, v1, v19

    const/16 v19, -0x38

    aput v19, v1, v18

    const/16 v18, 0x5a58

    aput v18, v1, v17

    const/16 v17, -0x42a6

    aput v17, v1, v16

    const/16 v16, -0x43

    aput v16, v1, v15

    const/16 v15, -0x63d1

    aput v15, v1, v14

    const/16 v14, -0x64

    aput v14, v1, v13

    const/16 v13, -0x9

    aput v13, v1, v12

    const/4 v12, -0x6

    aput v12, v1, v11

    const/16 v11, -0x44

    aput v11, v1, v10

    const/16 v10, -0x16

    aput v10, v1, v9

    const/16 v9, -0x5ea9

    aput v9, v1, v8

    const/16 v8, -0x5f

    aput v8, v1, v7

    const/16 v7, -0x1f7

    aput v7, v1, v6

    const/4 v6, -0x2

    aput v6, v1, v5

    const/16 v5, -0x28

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_13
    array-length v5, v1

    if-lt v3, v5, :cond_23

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_14
    array-length v5, v1

    if-lt v3, v5, :cond_24

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_27

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const/16 v1, 0x15

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x2e4f

    aput v24, v2, v23

    const/16 v23, -0xeb9

    aput v23, v2, v22

    const/16 v22, -0x52

    aput v22, v2, v21

    const/16 v21, 0x7c6c

    aput v21, v2, v20

    const/16 v20, 0x7619

    aput v20, v2, v19

    const/16 v19, 0x7d04

    aput v19, v2, v18

    const/16 v18, 0x2a22

    aput v18, v2, v17

    const/16 v17, 0x3c5e

    aput v17, v2, v16

    const/16 v16, -0x4ebc

    aput v16, v2, v15

    const/16 v15, -0x2c

    aput v15, v2, v14

    const/16 v14, -0x7caa

    aput v14, v2, v13

    const/16 v13, -0x24

    aput v13, v2, v12

    const/16 v12, -0x2c

    aput v12, v2, v11

    const/16 v11, -0x7f00

    aput v11, v2, v10

    const/16 v10, -0x18

    aput v10, v2, v9

    const/16 v9, 0x4164

    aput v9, v2, v8

    const/16 v8, 0x524

    aput v8, v2, v7

    const/16 v7, -0x6f9f

    aput v7, v2, v6

    const/16 v6, -0x31

    aput v6, v2, v5

    const/16 v5, -0x63a7

    aput v5, v2, v3

    const/16 v3, -0xe

    aput v3, v2, v1

    const/16 v1, 0x15

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x2e2b

    aput v25, v1, v24

    const/16 v24, -0xed2

    aput v24, v1, v23

    const/16 v23, -0xf

    aput v23, v1, v22

    const/16 v22, 0x7c1f

    aput v22, v1, v21

    const/16 v21, 0x767c

    aput v21, v1, v20

    const/16 v20, 0x7d76

    aput v20, v1, v19

    const/16 v19, 0x2a7d

    aput v19, v1, v18

    const/16 v18, 0x3c2a

    aput v18, v1, v17

    const/16 v17, -0x4ec4

    aput v17, v1, v16

    const/16 v16, -0x4f

    aput v16, v1, v15

    const/16 v15, -0x7cde

    aput v15, v1, v14

    const/16 v14, -0x7d

    aput v14, v1, v13

    const/16 v13, -0x4f

    aput v13, v1, v12

    const/16 v12, -0x7e9d

    aput v12, v1, v11

    const/16 v11, -0x7f

    aput v11, v1, v10

    const/16 v10, 0x4112

    aput v10, v1, v9

    const/16 v9, 0x541

    aput v9, v1, v8

    const/16 v8, -0x6ffb

    aput v8, v1, v7

    const/16 v7, -0x70

    aput v7, v1, v6

    const/16 v6, -0x63ca

    aput v6, v1, v5

    const/16 v5, -0x64

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_15
    array-length v5, v1

    if-lt v3, v5, :cond_25

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_16
    array-length v5, v1

    if-lt v3, v5, :cond_26

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v4, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->noDeviceStringResId:I

    :cond_4
    :goto_17
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const/16 v1, 0x17

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, -0x3e3

    aput v26, v2, v25

    const/16 v25, -0x6b

    aput v25, v2, v24

    const/16 v24, 0x334e

    aput v24, v2, v23

    const/16 v23, -0x37c0

    aput v23, v2, v22

    const/16 v22, -0x53

    aput v22, v2, v21

    const/16 v21, -0x6f

    aput v21, v2, v20

    const/16 v20, -0x6b86

    aput v20, v2, v19

    const/16 v19, -0x20

    aput v19, v2, v18

    const/16 v18, -0x21

    aput v18, v2, v17

    const/16 v17, 0x4a6b

    aput v17, v2, v16

    const/16 v16, -0x22c2

    aput v16, v2, v15

    const/16 v15, -0x7e

    aput v15, v2, v14

    const/16 v14, -0x18

    aput v14, v2, v13

    const/16 v13, -0x4c

    aput v13, v2, v12

    const/16 v12, -0x7b

    aput v12, v2, v11

    const/16 v11, -0x6dc5

    aput v11, v2, v10

    const/16 v10, -0x33

    aput v10, v2, v9

    const/16 v9, -0x228a

    aput v9, v2, v8

    const/16 v8, -0x42

    aput v8, v2, v7

    const/16 v7, -0x53

    aput v7, v2, v6

    const/16 v6, 0x934

    aput v6, v2, v5

    const/16 v5, 0x516c

    aput v5, v2, v3

    const/16 v3, -0x2ecb

    aput v3, v2, v1

    const/16 v1, 0x17

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, -0x387

    aput v27, v1, v26

    const/16 v26, -0x4

    aput v26, v1, v25

    const/16 v25, 0x3311

    aput v25, v1, v24

    const/16 v24, -0x37cd

    aput v24, v1, v23

    const/16 v23, -0x38

    aput v23, v1, v22

    const/16 v22, -0x1d

    aput v22, v1, v21

    const/16 v21, -0x6bdb

    aput v21, v1, v20

    const/16 v20, -0x6c

    aput v20, v1, v19

    const/16 v19, -0x59

    aput v19, v1, v18

    const/16 v18, 0x4a0e

    aput v18, v1, v17

    const/16 v17, -0x22b6

    aput v17, v1, v16

    const/16 v16, -0x23

    aput v16, v1, v15

    const/16 v15, -0x73

    aput v15, v1, v14

    const/16 v14, -0x3c

    aput v14, v1, v13

    const/4 v13, -0x4

    aput v13, v1, v12

    const/16 v12, -0x6db1

    aput v12, v1, v11

    const/16 v11, -0x6e

    aput v11, v1, v10

    const/16 v10, -0x22ed

    aput v10, v1, v9

    const/16 v9, -0x23

    aput v9, v1, v8

    const/16 v8, -0x3c

    aput v8, v1, v7

    const/16 v7, 0x942

    aput v7, v1, v6

    const/16 v6, 0x5109

    aput v6, v1, v5

    const/16 v5, -0x2eaf

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_18
    array-length v5, v1

    if-lt v3, v5, :cond_2c

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_19
    array-length v5, v1

    if-lt v3, v5, :cond_2d

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_30

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const/16 v1, 0x17

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x3b62

    aput v26, v2, v25

    const/16 v25, 0x5452

    aput v25, v2, v24

    const/16 v24, -0x2cf5

    aput v24, v2, v23

    const/16 v23, -0x60

    aput v23, v2, v22

    const/16 v22, 0x3c73

    aput v22, v2, v21

    const/16 v21, 0x484e

    aput v21, v2, v20

    const/16 v20, 0x3a17

    aput v20, v2, v19

    const/16 v19, -0x63b2

    aput v19, v2, v18

    const/16 v18, -0x1c

    aput v18, v2, v17

    const/16 v17, -0x5e

    aput v17, v2, v16

    const/16 v16, -0x2b

    aput v16, v2, v15

    const/16 v15, -0x23a2

    aput v15, v2, v14

    const/16 v14, -0x47

    aput v14, v2, v13

    const/16 v13, -0x69

    aput v13, v2, v12

    const/16 v12, -0x4ee5

    aput v12, v2, v11

    const/16 v11, -0x3b

    aput v11, v2, v10

    const/16 v10, -0x76bb

    aput v10, v2, v9

    const/16 v9, -0x14

    aput v9, v2, v8

    const/16 v8, -0x76

    aput v8, v2, v7

    const/16 v7, 0x7e68

    aput v7, v2, v6

    const/16 v6, 0x4308

    aput v6, v2, v5

    const/16 v5, -0x62da

    aput v5, v2, v3

    const/4 v3, -0x7

    aput v3, v2, v1

    const/16 v1, 0x17

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x3b06

    aput v27, v1, v26

    const/16 v26, 0x543b

    aput v26, v1, v25

    const/16 v25, -0x2cac

    aput v25, v1, v24

    const/16 v24, -0x2d

    aput v24, v1, v23

    const/16 v23, 0x3c16

    aput v23, v1, v22

    const/16 v22, 0x483c

    aput v22, v1, v21

    const/16 v21, 0x3a48

    aput v21, v1, v20

    const/16 v20, -0x63c6

    aput v20, v1, v19

    const/16 v19, -0x64

    aput v19, v1, v18

    const/16 v18, -0x39

    aput v18, v1, v17

    const/16 v17, -0x5f

    aput v17, v1, v16

    const/16 v16, -0x23ff

    aput v16, v1, v15

    const/16 v15, -0x24

    aput v15, v1, v14

    const/16 v14, -0x19

    aput v14, v1, v13

    const/16 v13, -0x4e9e

    aput v13, v1, v12

    const/16 v12, -0x4f

    aput v12, v1, v11

    const/16 v11, -0x76e6

    aput v11, v1, v10

    const/16 v10, -0x77

    aput v10, v1, v9

    const/16 v9, -0x17

    aput v9, v1, v8

    const/16 v8, 0x7e01

    aput v8, v1, v7

    const/16 v7, 0x437e

    aput v7, v1, v6

    const/16 v6, -0x62bd

    aput v6, v1, v5

    const/16 v5, -0x63

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_1a
    array-length v5, v1

    if-lt v3, v5, :cond_2e

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1b
    array-length v5, v1

    if-lt v3, v5, :cond_2f

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v4, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->deviceTypeTextResId:I

    :cond_5
    :goto_1c
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const/16 v1, 0x16

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, -0x7ebd

    aput v25, v2, v24

    const/16 v24, -0x1c

    aput v24, v2, v23

    const/16 v23, -0x51

    aput v23, v2, v22

    const/16 v22, -0xea9

    aput v22, v2, v21

    const/16 v21, -0x79

    aput v21, v2, v20

    const/16 v20, -0x1a

    aput v20, v2, v19

    const/16 v19, -0x4d

    aput v19, v2, v18

    const/16 v18, 0x123e

    aput v18, v2, v17

    const/16 v17, 0x237c

    aput v17, v2, v16

    const/16 v16, 0x404c

    aput v16, v2, v15

    const/16 v15, -0x7d7

    aput v15, v2, v14

    const/16 v14, -0x6a

    aput v14, v2, v13

    const/16 v13, -0x59a9

    aput v13, v2, v12

    const/16 v12, -0x2a

    aput v12, v2, v11

    const/16 v11, -0x29

    aput v11, v2, v10

    const/16 v10, -0x38

    aput v10, v2, v9

    const/16 v9, -0x348a

    aput v9, v2, v8

    const/16 v8, -0x6c

    aput v8, v2, v7

    const/16 v7, -0x6a

    aput v7, v2, v6

    const/16 v6, -0x42c7

    aput v6, v2, v5

    const/16 v5, -0x2b

    aput v5, v2, v3

    const/16 v3, -0x67f5

    aput v3, v2, v1

    const/16 v1, 0x16

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, -0x7ed0

    aput v26, v1, v25

    const/16 v25, -0x7f

    aput v25, v1, v24

    const/16 v24, -0x34

    aput v24, v1, v23

    const/16 v23, -0xec2

    aput v23, v1, v22

    const/16 v22, -0xf

    aput v22, v1, v21

    const/16 v21, -0x7d

    aput v21, v1, v20

    const/16 v20, -0x29

    aput v20, v1, v19

    const/16 v19, 0x1261

    aput v19, v1, v18

    const/16 v18, 0x2312

    aput v18, v1, v17

    const/16 v17, 0x4023

    aput v17, v1, v16

    const/16 v16, -0x7c0

    aput v16, v1, v15

    const/4 v15, -0x8

    aput v15, v1, v14

    const/16 v14, -0x59ca

    aput v14, v1, v13

    const/16 v13, -0x5a

    aput v13, v1, v12

    const/16 v12, -0x46

    aput v12, v1, v11

    const/16 v11, -0x59

    aput v11, v1, v10

    const/16 v10, -0x34eb

    aput v10, v1, v9

    const/16 v9, -0x35

    aput v9, v1, v8

    const/16 v8, -0x1f

    aput v8, v1, v7

    const/16 v7, -0x42aa

    aput v7, v1, v6

    const/16 v6, -0x43

    aput v6, v1, v5

    const/16 v5, -0x6788

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_1d
    array-length v5, v1

    if-lt v3, v5, :cond_35

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1e
    array-length v5, v1

    if-lt v3, v5, :cond_36

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const/16 v1, 0x16

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, -0x4a

    aput v25, v2, v24

    const/16 v24, -0x3a97

    aput v24, v2, v23

    const/16 v23, -0x5a

    aput v23, v2, v22

    const/16 v22, 0x6575

    aput v22, v2, v21

    const/16 v21, -0x9ed

    aput v21, v2, v20

    const/16 v20, -0x6d

    aput v20, v2, v19

    const/16 v19, 0x3b1b

    aput v19, v2, v18

    const/16 v18, -0x2a9c

    aput v18, v2, v17

    const/16 v17, -0x45

    aput v17, v2, v16

    const/16 v16, -0x1c

    aput v16, v2, v15

    const/16 v15, -0x7899

    aput v15, v2, v14

    const/16 v14, -0x17

    aput v14, v2, v13

    const/16 v13, -0x328b

    aput v13, v2, v12

    const/16 v12, -0x43

    aput v12, v2, v11

    const/16 v11, -0x5c82

    aput v11, v2, v10

    const/16 v10, -0x34

    aput v10, v2, v9

    const/16 v9, -0x59e5

    aput v9, v2, v8

    const/4 v8, -0x7

    aput v8, v2, v7

    const/16 v7, -0x28

    aput v7, v2, v6

    const/4 v6, -0x5

    aput v6, v2, v5

    const/16 v5, -0x5f

    aput v5, v2, v3

    const/16 v3, -0x73

    aput v3, v2, v1

    const/16 v1, 0x16

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, -0x3b

    aput v26, v1, v25

    const/16 v25, -0x3af4

    aput v25, v1, v24

    const/16 v24, -0x3b

    aput v24, v1, v23

    const/16 v23, 0x651c

    aput v23, v1, v22

    const/16 v22, -0x99b

    aput v22, v1, v21

    const/16 v21, -0xa

    aput v21, v1, v20

    const/16 v20, 0x3b7f

    aput v20, v1, v19

    const/16 v19, -0x2ac5

    aput v19, v1, v18

    const/16 v18, -0x2b

    aput v18, v1, v17

    const/16 v17, -0x75

    aput v17, v1, v16

    const/16 v16, -0x78f2

    aput v16, v1, v15

    const/16 v15, -0x79

    aput v15, v1, v14

    const/16 v14, -0x32ec

    aput v14, v1, v13

    const/16 v13, -0x33

    aput v13, v1, v12

    const/16 v12, -0x5ced

    aput v12, v1, v11

    const/16 v11, -0x5d

    aput v11, v1, v10

    const/16 v10, -0x5988

    aput v10, v1, v9

    const/16 v9, -0x5a

    aput v9, v1, v8

    const/16 v8, -0x51

    aput v8, v1, v7

    const/16 v7, -0x6c

    aput v7, v1, v6

    const/16 v6, -0x37

    aput v6, v1, v5

    const/4 v5, -0x2

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_1f
    array-length v5, v1

    if-lt v3, v5, :cond_37

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_20
    array-length v5, v1

    if-lt v3, v5, :cond_38

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->showCompanionDevices:Z

    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const/16 v1, 0x1e

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, 0x1d

    const/16 v33, -0x30

    aput v33, v2, v32

    const/16 v32, -0x10d9

    aput v32, v2, v31

    const/16 v31, -0x50

    aput v31, v2, v30

    const/16 v30, -0x1f

    aput v30, v2, v29

    const/16 v29, -0x11

    aput v29, v2, v28

    const/16 v28, -0x79

    aput v28, v2, v27

    const/16 v27, -0xe97

    aput v27, v2, v26

    const/16 v26, -0x7b

    aput v26, v2, v25

    const/16 v25, 0x1824

    aput v25, v2, v24

    const/16 v24, -0x3483

    aput v24, v2, v23

    const/16 v23, -0x41

    aput v23, v2, v22

    const/16 v22, -0x4f9c

    aput v22, v2, v21

    const/16 v21, -0x21

    aput v21, v2, v20

    const/16 v20, 0x4007

    aput v20, v2, v19

    const/16 v19, 0x512e

    aput v19, v2, v18

    const/16 v18, 0x4038

    aput v18, v2, v17

    const/16 v17, -0xbe1

    aput v17, v2, v16

    const/16 v16, -0x6f

    aput v16, v2, v15

    const/16 v15, -0x34

    aput v15, v2, v14

    const/16 v14, -0x6f

    aput v14, v2, v13

    const/16 v13, -0x2c

    aput v13, v2, v12

    const/16 v12, -0x12

    aput v12, v2, v11

    const/4 v11, -0x2

    aput v11, v2, v10

    const/16 v10, -0x48

    aput v10, v2, v9

    const/16 v9, -0x6e

    aput v9, v2, v8

    const/16 v8, 0x596b

    aput v8, v2, v7

    const/16 v7, 0x7b37

    aput v7, v2, v6

    const/16 v6, -0x23e6

    aput v6, v2, v5

    const/16 v5, -0x41

    aput v5, v2, v3

    const/16 v3, 0x780c

    aput v3, v2, v1

    const/16 v1, 0x1e

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, 0x1d

    const/16 v34, -0x4c

    aput v34, v1, v33

    const/16 v33, -0x10b2

    aput v33, v1, v32

    const/16 v32, -0x11

    aput v32, v1, v31

    const/16 v31, -0x6e

    aput v31, v1, v30

    const/16 v30, -0x76

    aput v30, v1, v29

    const/16 v29, -0xb

    aput v29, v1, v28

    const/16 v28, -0xeca

    aput v28, v1, v27

    const/16 v27, -0xf

    aput v27, v1, v26

    const/16 v26, 0x185c

    aput v26, v1, v25

    const/16 v25, -0x34e8

    aput v25, v1, v24

    const/16 v24, -0x35

    aput v24, v1, v23

    const/16 v23, -0x4fc5

    aput v23, v1, v22

    const/16 v22, -0x50

    aput v22, v1, v21

    const/16 v21, 0x4061

    aput v21, v1, v20

    const/16 v20, 0x5140

    aput v20, v1, v19

    const/16 v19, 0x4051

    aput v19, v1, v18

    const/16 v18, -0xbc0

    aput v18, v1, v17

    const/16 v17, -0xc

    aput v17, v1, v16

    const/16 v16, -0x44

    aput v16, v1, v15

    const/16 v15, -0x18

    aput v15, v1, v14

    const/16 v14, -0x60

    aput v14, v1, v13

    const/16 v13, -0x4f

    aput v13, v1, v12

    const/16 v12, -0x67

    aput v12, v1, v11

    const/16 v11, -0x2a

    aput v11, v1, v10

    const/4 v10, -0x5

    aput v10, v1, v9

    const/16 v9, 0x5905

    aput v9, v1, v8

    const/16 v8, 0x7b59

    aput v8, v1, v7

    const/16 v7, -0x2385

    aput v7, v1, v6

    const/16 v6, -0x24

    aput v6, v1, v5

    const/16 v5, 0x787f

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_21
    array-length v5, v1

    if-lt v3, v5, :cond_39

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_22
    array-length v5, v1

    if-lt v3, v5, :cond_3a

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3d

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const/16 v1, 0x1e

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, 0x1d

    const/16 v33, -0x4f

    aput v33, v2, v32

    const/16 v32, -0x3fba

    aput v32, v2, v31

    const/16 v31, -0x61

    aput v31, v2, v30

    const/16 v30, 0x7f05

    aput v30, v2, v29

    const/16 v29, -0x49e6

    aput v29, v2, v28

    const/16 v28, -0x3c

    aput v28, v2, v27

    const/16 v27, -0x24

    aput v27, v2, v26

    const/16 v26, -0x4a

    aput v26, v2, v25

    const/16 v25, -0x5a

    aput v25, v2, v24

    const/16 v24, -0xa

    aput v24, v2, v23

    const/16 v23, -0x26f1

    aput v23, v2, v22

    const/16 v22, -0x7a

    aput v22, v2, v21

    const/16 v21, -0x6f

    aput v21, v2, v20

    const/16 v20, -0xe88

    aput v20, v2, v19

    const/16 v19, -0x61

    aput v19, v2, v18

    const/16 v18, 0x1365

    aput v18, v2, v17

    const/16 v17, 0x274c

    aput v17, v2, v16

    const/16 v16, -0x7cbe

    aput v16, v2, v15

    const/16 v15, -0xd

    aput v15, v2, v14

    const/16 v14, -0x49

    aput v14, v2, v13

    const/16 v13, -0x4596

    aput v13, v2, v12

    const/16 v12, -0x1b

    aput v12, v2, v11

    const/16 v11, 0x405b

    aput v11, v2, v10

    const/16 v10, -0x5cd2

    aput v10, v2, v9

    const/16 v9, -0x36

    aput v9, v2, v8

    const/16 v8, -0x60a5

    aput v8, v2, v7

    const/16 v7, -0xf

    aput v7, v2, v6

    const/16 v6, -0x30f3

    aput v6, v2, v5

    const/16 v5, -0x54

    aput v5, v2, v3

    const/16 v3, -0x44

    aput v3, v2, v1

    const/16 v1, 0x1e

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, 0x1d

    const/16 v34, -0x2b

    aput v34, v1, v33

    const/16 v33, -0x3fd1

    aput v33, v1, v32

    const/16 v32, -0x40

    aput v32, v1, v31

    const/16 v31, 0x7f76

    aput v31, v1, v30

    const/16 v30, -0x4981

    aput v30, v1, v29

    const/16 v29, -0x4a

    aput v29, v1, v28

    const/16 v28, -0x7d

    aput v28, v1, v27

    const/16 v27, -0x3e

    aput v27, v1, v26

    const/16 v26, -0x22

    aput v26, v1, v25

    const/16 v25, -0x6d

    aput v25, v1, v24

    const/16 v24, -0x2685

    aput v24, v1, v23

    const/16 v23, -0x27

    aput v23, v1, v22

    const/16 v22, -0x2

    aput v22, v1, v21

    const/16 v21, -0xee2

    aput v21, v1, v20

    const/16 v20, -0xf

    aput v20, v1, v19

    const/16 v19, 0x130c

    aput v19, v1, v18

    const/16 v18, 0x2713

    aput v18, v1, v17

    const/16 v17, -0x7cd9

    aput v17, v1, v16

    const/16 v16, -0x7d

    aput v16, v1, v15

    const/16 v15, -0x32

    aput v15, v1, v14

    const/16 v14, -0x45e2

    aput v14, v1, v13

    const/16 v13, -0x46

    aput v13, v1, v12

    const/16 v12, 0x403c

    aput v12, v1, v11

    const/16 v11, -0x5cc0

    aput v11, v1, v10

    const/16 v10, -0x5d

    aput v10, v1, v9

    const/16 v9, -0x60cb

    aput v9, v1, v8

    const/16 v8, -0x61

    aput v8, v1, v7

    const/16 v7, -0x3094

    aput v7, v1, v6

    const/16 v6, -0x31

    aput v6, v1, v5

    const/16 v5, -0x31

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_23
    array-length v5, v1

    if-lt v3, v5, :cond_3b

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_24
    array-length v5, v1

    if-lt v3, v5, :cond_3c

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mScanningInfoTextResId:I

    :cond_7
    :goto_25
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const/16 v1, 0x12

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x7a10

    aput v21, v2, v20

    const/16 v20, -0xde5

    aput v20, v2, v19

    const/16 v19, -0x6f

    aput v19, v2, v18

    const/16 v18, -0x7bde

    aput v18, v2, v17

    const/16 v17, -0x25

    aput v17, v2, v16

    const/16 v16, -0x34a6

    aput v16, v2, v15

    const/16 v15, -0x47

    aput v15, v2, v14

    const/16 v14, 0x213f

    aput v14, v2, v13

    const/16 v13, -0x49af

    aput v13, v2, v12

    const/16 v12, -0x3a

    aput v12, v2, v11

    const/16 v11, -0x5c5

    aput v11, v2, v10

    const/16 v10, -0x77

    aput v10, v2, v9

    const/16 v9, -0x681

    aput v9, v2, v8

    const/16 v8, -0x6a

    aput v8, v2, v7

    const/16 v7, -0x59

    aput v7, v2, v6

    const/16 v6, -0x5e

    aput v6, v2, v5

    const/16 v5, 0x2a3e

    aput v5, v2, v3

    const/16 v3, 0x3043

    aput v3, v2, v1

    const/16 v1, 0x12

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x7a7e

    aput v22, v1, v21

    const/16 v21, -0xd86

    aput v21, v1, v20

    const/16 v20, -0xe

    aput v20, v1, v19

    const/16 v19, -0x7baf

    aput v19, v1, v18

    const/16 v18, -0x7c

    aput v18, v1, v17

    const/16 v17, -0x34d2

    aput v17, v1, v16

    const/16 v16, -0x35

    aput v16, v1, v15

    const/16 v15, 0x2150

    aput v15, v1, v14

    const/16 v14, -0x49df

    aput v14, v1, v13

    const/16 v13, -0x4a

    aput v13, v1, v12

    const/16 v12, -0x5b2

    aput v12, v1, v11

    const/4 v11, -0x6

    aput v11, v1, v10

    const/16 v10, -0x6e0

    aput v10, v1, v9

    const/4 v9, -0x7

    aput v9, v1, v8

    const/16 v8, -0x37

    aput v8, v1, v7

    const/4 v7, -0x3

    aput v7, v1, v6

    const/16 v6, 0x2a4d

    aput v6, v1, v5

    const/16 v5, 0x302a

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_26
    array-length v5, v1

    if-lt v3, v5, :cond_42

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_27
    array-length v5, v1

    if-lt v3, v5, :cond_43

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mIsNoSupportScan:Z

    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mDataType:I

    const/4 v2, 0x4

    if-ne v1, v2, :cond_8

    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mConnectivityType:I

    const/4 v2, 0x4

    if-eq v1, v2, :cond_8

    const/4 v1, 0x1

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mExerciseScanningInstance:Z

    :cond_8
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const/16 v1, 0x18

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, -0x5f

    aput v27, v2, v26

    const/16 v26, -0x67c8

    aput v26, v2, v25

    const/16 v25, -0xf

    aput v25, v2, v24

    const/16 v24, -0x63a7

    aput v24, v2, v23

    const/16 v23, -0x11

    aput v23, v2, v22

    const/16 v22, -0x31

    aput v22, v2, v21

    const/16 v21, -0x75

    aput v21, v2, v20

    const/16 v20, -0x11

    aput v20, v2, v19

    const/16 v19, -0x54

    aput v19, v2, v18

    const/16 v18, -0x6a

    aput v18, v2, v17

    const/16 v17, -0x61

    aput v17, v2, v16

    const/16 v16, -0x3c

    aput v16, v2, v15

    const/16 v15, -0x67ee

    aput v15, v2, v14

    const/16 v14, -0x9

    aput v14, v2, v13

    const/16 v13, 0x7134

    aput v13, v2, v12

    const/16 v12, -0x8d2

    aput v12, v2, v11

    const/16 v11, -0x7c

    aput v11, v2, v10

    const/16 v10, -0x47

    aput v10, v2, v9

    const/16 v9, -0x68

    aput v9, v2, v8

    const/16 v8, -0x49

    aput v8, v2, v7

    const/16 v7, -0x9

    aput v7, v2, v6

    const/16 v6, -0x7c

    aput v6, v2, v5

    const/16 v5, -0x5f

    aput v5, v2, v3

    const/16 v3, -0x72d8

    aput v3, v2, v1

    const/16 v1, 0x18

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, -0x2e

    aput v28, v1, v27

    const/16 v27, -0x67a4

    aput v27, v1, v26

    const/16 v26, -0x68

    aput v26, v1, v25

    const/16 v25, -0x63fa

    aput v25, v1, v24

    const/16 v24, -0x64

    aput v24, v1, v23

    const/16 v23, -0x56

    aput v23, v1, v22

    const/16 v22, -0x7

    aput v22, v1, v21

    const/16 v21, -0x50

    aput v21, v1, v20

    const/16 v20, -0x28

    aput v20, v1, v19

    const/16 v19, -0x8

    aput v19, v1, v18

    const/16 v18, -0x6

    aput v18, v1, v17

    const/16 v17, -0x50

    aput v17, v1, v16

    const/16 v16, -0x6784

    aput v16, v1, v15

    const/16 v15, -0x68

    aput v15, v1, v14

    const/16 v14, 0x7157

    aput v14, v1, v13

    const/16 v13, -0x88f

    aput v13, v1, v12

    const/16 v12, -0x9

    aput v12, v1, v11

    const/16 v11, -0x25

    aput v11, v1, v10

    const/4 v10, -0x7

    aput v10, v1, v9

    const/16 v9, -0x3d

    aput v9, v1, v8

    const/16 v8, -0x58

    aput v8, v1, v7

    const/16 v7, -0x1a

    aput v7, v1, v6

    const/16 v6, -0x2c

    aput v6, v1, v5

    const/16 v5, -0x72a5

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_28
    array-length v5, v1

    if-lt v3, v5, :cond_44

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_29
    array-length v5, v1

    if-lt v3, v5, :cond_45

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_48

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const/16 v1, 0x18

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x42c

    aput v27, v2, v26

    const/16 v26, -0x6a0

    aput v26, v2, v25

    const/16 v25, -0x70

    aput v25, v2, v24

    const/16 v24, -0x12

    aput v24, v2, v23

    const/16 v23, -0x2e

    aput v23, v2, v22

    const/16 v22, -0x71

    aput v22, v2, v21

    const/16 v21, 0x1f5e

    aput v21, v2, v20

    const/16 v20, -0x6ac0

    aput v20, v2, v19

    const/16 v19, -0x1f

    aput v19, v2, v18

    const/16 v18, 0x275f

    aput v18, v2, v17

    const/16 v17, 0x5d42

    aput v17, v2, v16

    const/16 v16, -0x45d7

    aput v16, v2, v15

    const/16 v15, -0x2c

    aput v15, v2, v14

    const/16 v14, -0x7b93

    aput v14, v2, v13

    const/16 v13, -0x19

    aput v13, v2, v12

    const/16 v12, -0x7c

    aput v12, v2, v11

    const/16 v11, -0x40

    aput v11, v2, v10

    const/16 v10, -0x48c8

    aput v10, v2, v9

    const/16 v9, -0x2a

    aput v9, v2, v8

    const/16 v8, -0x40c0

    aput v8, v2, v7

    const/16 v7, -0x20

    aput v7, v2, v6

    const/16 v6, 0x4a

    aput v6, v2, v5

    const/16 v5, -0x2e8b

    aput v5, v2, v3

    const/16 v3, -0x5e

    aput v3, v2, v1

    const/16 v1, 0x18

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x45f

    aput v28, v1, v27

    const/16 v27, -0x6fc

    aput v27, v1, v26

    const/16 v26, -0x7

    aput v26, v1, v25

    const/16 v25, -0x4f

    aput v25, v1, v24

    const/16 v24, -0x5f

    aput v24, v1, v23

    const/16 v23, -0x16

    aput v23, v1, v22

    const/16 v22, 0x1f2c

    aput v22, v1, v21

    const/16 v21, -0x6ae1

    aput v21, v1, v20

    const/16 v20, -0x6b

    aput v20, v1, v19

    const/16 v19, 0x2731

    aput v19, v1, v18

    const/16 v18, 0x5d27

    aput v18, v1, v17

    const/16 v17, -0x45a3

    aput v17, v1, v16

    const/16 v16, -0x46

    aput v16, v1, v15

    const/16 v15, -0x7bfe

    aput v15, v1, v14

    const/16 v14, -0x7c

    aput v14, v1, v13

    const/16 v13, -0x25

    aput v13, v1, v12

    const/16 v12, -0x4d

    aput v12, v1, v11

    const/16 v11, -0x48a6

    aput v11, v1, v10

    const/16 v10, -0x49

    aput v10, v1, v9

    const/16 v9, -0x40cc

    aput v9, v1, v8

    const/16 v8, -0x41

    aput v8, v1, v7

    const/16 v7, 0x28

    aput v7, v1, v6

    const/16 v6, -0x2f00

    aput v6, v1, v5

    const/16 v5, -0x2f

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_2a
    array-length v5, v1

    if-lt v3, v5, :cond_46

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_2b
    array-length v5, v1

    if-lt v3, v5, :cond_47

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mSubTabsContentResIds:[I

    :cond_9
    :goto_2c
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const/4 v1, 0x6

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/16 v9, -0x46

    aput v9, v2, v8

    const/16 v8, 0x664e

    aput v8, v2, v7

    const/16 v7, 0x2507

    aput v7, v2, v6

    const/16 v6, -0x30b5

    aput v6, v2, v5

    const/16 v5, -0x56

    aput v5, v2, v3

    const/16 v3, 0x5d08

    aput v3, v2, v1

    const/4 v1, 0x6

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/16 v10, -0x21

    aput v10, v1, v9

    const/16 v9, 0x6623

    aput v9, v1, v8

    const/16 v8, 0x2566

    aput v8, v1, v7

    const/16 v7, -0x30db

    aput v7, v1, v6

    const/16 v6, -0x31

    aput v6, v1, v5

    const/16 v5, 0x5d5a

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_2d
    array-length v5, v1

    if-lt v3, v5, :cond_4d

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_2e
    array-length v5, v1

    if-lt v3, v5, :cond_4e

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const/4 v1, 0x6

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/16 v9, -0x2dbf

    aput v9, v2, v8

    const/16 v8, -0x41

    aput v8, v2, v7

    const/16 v7, -0x3f

    aput v7, v2, v6

    const/16 v6, 0x7304

    aput v6, v2, v5

    const/16 v5, 0x3716

    aput v5, v2, v3

    const/16 v3, 0x4265

    aput v3, v2, v1

    const/4 v1, 0x6

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/16 v10, -0x2ddc

    aput v10, v1, v9

    const/16 v9, -0x2e

    aput v9, v1, v8

    const/16 v8, -0x60

    aput v8, v1, v7

    const/16 v7, 0x736a

    aput v7, v1, v6

    const/16 v6, 0x3773

    aput v6, v1, v5

    const/16 v5, 0x4237

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_2f
    array-length v5, v1

    if-lt v3, v5, :cond_4f

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_30
    array-length v5, v1

    if-lt v3, v5, :cond_50

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->deviceIDRename:Ljava/lang/String;

    :cond_a
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const/16 v1, 0x10

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, -0x19

    aput v19, v2, v18

    const/16 v18, -0x14

    aput v18, v2, v17

    const/16 v17, -0x41d3

    aput v17, v2, v16

    const/16 v16, -0x1f

    aput v16, v2, v15

    const/16 v15, -0x64

    aput v15, v2, v14

    const/16 v14, -0x4f

    aput v14, v2, v13

    const/16 v13, -0x67bb

    aput v13, v2, v12

    const/16 v12, -0x14

    aput v12, v2, v11

    const/16 v11, -0x34

    aput v11, v2, v10

    const/16 v10, -0x4fe4

    aput v10, v2, v9

    const/16 v9, -0x2d

    aput v9, v2, v8

    const/16 v8, -0x2d

    aput v8, v2, v7

    const/16 v7, -0x43b6

    aput v7, v2, v6

    const/16 v6, -0x26

    aput v6, v2, v5

    const/16 v5, -0x33

    aput v5, v2, v3

    const/16 v3, -0x28

    aput v3, v2, v1

    const/16 v1, 0x10

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, -0x6c

    aput v20, v1, v19

    const/16 v19, -0x78

    aput v19, v1, v18

    const/16 v18, -0x41bc

    aput v18, v1, v17

    const/16 v17, -0x42

    aput v17, v1, v16

    const/16 v16, -0x18

    aput v16, v1, v15

    const/16 v15, -0x21

    aput v15, v1, v14

    const/16 v14, -0x67e0

    aput v14, v1, v13

    const/16 v13, -0x68

    aput v13, v1, v12

    const/16 v12, -0x5e

    aput v12, v1, v11

    const/16 v11, -0x4f8d

    aput v11, v1, v10

    const/16 v10, -0x50

    aput v10, v1, v9

    const/16 v9, -0x74

    aput v9, v1, v8

    const/16 v8, -0x43db

    aput v8, v1, v7

    const/16 v7, -0x44

    aput v7, v1, v6

    const/16 v6, -0x5d

    aput v6, v1, v5

    const/16 v5, -0x4f

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_31
    array-length v5, v1

    if-lt v3, v5, :cond_51

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_32
    array-length v5, v1

    if-lt v3, v5, :cond_52

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const/16 v1, 0x10

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x3e4a

    aput v19, v2, v18

    const/16 v18, 0x605a

    aput v18, v2, v17

    const/16 v17, -0x69f7

    aput v17, v2, v16

    const/16 v16, -0x37

    aput v16, v2, v15

    const/16 v15, 0x644

    aput v15, v2, v14

    const/16 v14, -0x6a98

    aput v14, v2, v13

    const/16 v13, -0x10

    aput v13, v2, v12

    const/16 v12, -0x12f2

    aput v12, v2, v11

    const/16 v11, -0x7d

    aput v11, v2, v10

    const/16 v10, -0x6f

    aput v10, v2, v9

    const/16 v9, -0x23

    aput v9, v2, v8

    const/16 v8, -0x5e84

    aput v8, v2, v7

    const/16 v7, -0x32

    aput v7, v2, v6

    const/16 v6, -0x2f

    aput v6, v2, v5

    const/16 v5, -0x65

    aput v5, v2, v3

    const/16 v3, -0x35c8

    aput v3, v2, v1

    const/16 v1, 0x10

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x3e39

    aput v20, v1, v19

    const/16 v19, 0x603e

    aput v19, v1, v18

    const/16 v18, -0x69a0

    aput v18, v1, v17

    const/16 v17, -0x6a

    aput v17, v1, v16

    const/16 v16, 0x630

    aput v16, v1, v15

    const/16 v15, -0x6afa

    aput v15, v1, v14

    const/16 v14, -0x6b

    aput v14, v1, v13

    const/16 v13, -0x1286

    aput v13, v1, v12

    const/16 v12, -0x13

    aput v12, v1, v11

    const/4 v11, -0x2

    aput v11, v1, v10

    const/16 v10, -0x42

    aput v10, v1, v9

    const/16 v9, -0x5edd

    aput v9, v1, v8

    const/16 v8, -0x5f

    aput v8, v1, v7

    const/16 v7, -0x49

    aput v7, v1, v6

    const/16 v6, -0xb

    aput v6, v1, v5

    const/16 v5, -0x35af

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_33
    array-length v5, v1

    if-lt v3, v5, :cond_53

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_34
    array-length v5, v1

    if-lt v3, v5, :cond_54

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mInfoContentIds:[I

    :cond_b
    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$WearableStatusObserver;

    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-direct {v1, v0, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$WearableStatusObserver;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Landroid/os/Handler;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->wearableContentObserver:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$WearableStatusObserver;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Landroid/provider/Settings$System;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->wearableContentObserver:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$WearableStatusObserver;

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    return-void

    :cond_c
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_d
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_e
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    :cond_f
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3

    :cond_10
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_4

    :cond_11
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_5

    :cond_12
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_6

    :cond_13
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_7

    :cond_14
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_8

    :cond_15
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_9

    :cond_16
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_a

    :cond_17
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_b

    :cond_18
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_c

    :cond_19
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_d

    :cond_1a
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_e

    :cond_1b
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_f

    :cond_1c
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_10

    :cond_1d
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_11

    :cond_1e
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, -0xa

    aput v13, v2, v12

    const/16 v12, -0x1cf1

    aput v12, v2, v11

    const/16 v11, -0x7a

    aput v11, v2, v10

    const/16 v10, -0x2fe

    aput v10, v2, v9

    const/16 v9, -0x67

    aput v9, v2, v8

    const/16 v8, 0x3214

    aput v8, v2, v7

    const/16 v7, -0x3c0

    aput v7, v2, v6

    const/16 v6, -0x6b

    aput v6, v2, v5

    const/16 v5, -0x80

    aput v5, v2, v3

    const/16 v3, 0x7458

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, -0x7e

    aput v14, v1, v13

    const/16 v13, -0x1c89

    aput v13, v1, v12

    const/16 v12, -0x1d

    aput v12, v1, v11

    const/16 v11, -0x2aa

    aput v11, v1, v10

    const/4 v10, -0x3

    aput v10, v1, v9

    const/16 v9, 0x3271

    aput v9, v1, v8

    const/16 v8, -0x3ce

    aput v8, v1, v7

    const/4 v7, -0x4

    aput v7, v1, v6

    const/16 v6, -0x1f

    aput v6, v1, v5

    const/16 v5, 0x7408

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_35
    array-length v5, v1

    if-lt v3, v5, :cond_1f

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_36
    array-length v5, v1

    if-lt v3, v5, :cond_20

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0x597a

    aput v13, v2, v12

    const/16 v12, -0x7ddf

    aput v12, v2, v11

    const/16 v11, -0x19

    aput v11, v2, v10

    const/16 v10, -0x1dff

    aput v10, v2, v9

    const/16 v9, -0x7a

    aput v9, v2, v8

    const/16 v8, -0x7cf3

    aput v8, v2, v7

    const/16 v7, -0xf

    aput v7, v2, v6

    const/16 v6, -0x7585

    aput v6, v2, v5

    const/16 v5, -0x15

    aput v5, v2, v3

    const/16 v3, -0x3a

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0x590e

    aput v14, v1, v13

    const/16 v13, -0x7da7

    aput v13, v1, v12

    const/16 v12, -0x7e

    aput v12, v1, v11

    const/16 v11, -0x1dab

    aput v11, v1, v10

    const/16 v10, -0x1e

    aput v10, v1, v9

    const/16 v9, -0x7c98

    aput v9, v1, v8

    const/16 v8, -0x7d

    aput v8, v1, v7

    const/16 v7, -0x75ee

    aput v7, v1, v6

    const/16 v6, -0x76

    aput v6, v1, v5

    const/16 v5, -0x6a

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_37
    array-length v5, v1

    if-lt v3, v5, :cond_21

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_38
    array-length v5, v1

    if-lt v3, v5, :cond_22

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mPairedText:Ljava/lang/String;

    goto/16 :goto_12

    :cond_1f
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_35

    :cond_20
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_36

    :cond_21
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_37

    :cond_22
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_38

    :cond_23
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_13

    :cond_24
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_14

    :cond_25
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_15

    :cond_26
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_16

    :cond_27
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const/16 v1, 0xe

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, -0x9

    aput v17, v2, v16

    const/16 v16, -0x2b

    aput v16, v2, v15

    const/16 v15, -0x11

    aput v15, v2, v14

    const/16 v14, -0x63

    aput v14, v2, v13

    const/16 v13, 0x4f0e

    aput v13, v2, v12

    const/16 v12, 0x211c

    aput v12, v2, v11

    const/16 v11, -0x3bbc

    aput v11, v2, v10

    const/16 v10, -0x59

    aput v10, v2, v9

    const/16 v9, -0x1e

    aput v9, v2, v8

    const/16 v8, -0x75

    aput v8, v2, v7

    const/16 v7, -0x34

    aput v7, v2, v6

    const/16 v6, -0x61

    aput v6, v2, v5

    const/16 v5, -0x57

    aput v5, v2, v3

    const/16 v3, -0x4cfd

    aput v3, v2, v1

    const/16 v1, 0xe

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, -0x70

    aput v18, v1, v17

    const/16 v17, -0x45

    aput v17, v1, v16

    const/16 v16, -0x7a

    aput v16, v1, v15

    const/16 v15, -0x11

    aput v15, v1, v14

    const/16 v14, 0x4f7a

    aput v14, v1, v13

    const/16 v13, 0x214f

    aput v13, v1, v12

    const/16 v12, -0x3bdf

    aput v12, v1, v11

    const/16 v11, -0x3c

    aput v11, v1, v10

    const/16 v10, -0x75

    aput v10, v1, v9

    const/4 v9, -0x3

    aput v9, v1, v8

    const/16 v8, -0x57

    aput v8, v1, v7

    const/16 v7, -0x25

    aput v7, v1, v6

    const/16 v6, -0x3a

    aput v6, v1, v5

    const/16 v5, -0x4cb3

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_39
    array-length v5, v1

    if-lt v3, v5, :cond_28

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3a
    array-length v5, v1

    if-lt v3, v5, :cond_29

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const/16 v1, 0xe

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, -0x3ada

    aput v17, v2, v16

    const/16 v16, -0x55

    aput v16, v2, v15

    const/16 v15, 0x2d4f

    aput v15, v2, v14

    const/16 v14, -0x12a1

    aput v14, v2, v13

    const/16 v13, -0x67

    aput v13, v2, v12

    const/16 v12, -0x6e

    aput v12, v2, v11

    const/16 v11, -0x61a8

    aput v11, v2, v10

    const/4 v10, -0x3

    aput v10, v2, v9

    const/16 v9, -0xa

    aput v9, v2, v8

    const/16 v8, -0xe

    aput v8, v2, v7

    const/16 v7, 0x6d08

    aput v7, v2, v6

    const/16 v6, -0x18d7

    aput v6, v2, v5

    const/16 v5, -0x78

    aput v5, v2, v3

    const/16 v3, 0x500a

    aput v3, v2, v1

    const/16 v1, 0xe

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, -0x3abf

    aput v18, v1, v17

    const/16 v17, -0x3b

    aput v17, v1, v16

    const/16 v16, 0x2d26

    aput v16, v1, v15

    const/16 v15, -0x12d3

    aput v15, v1, v14

    const/16 v14, -0x13

    aput v14, v1, v13

    const/16 v13, -0x3f

    aput v13, v1, v12

    const/16 v12, -0x61c3

    aput v12, v1, v11

    const/16 v11, -0x62

    aput v11, v1, v10

    const/16 v10, -0x61

    aput v10, v1, v9

    const/16 v9, -0x7c

    aput v9, v1, v8

    const/16 v8, 0x6d6d

    aput v8, v1, v7

    const/16 v7, -0x1893

    aput v7, v1, v6

    const/16 v6, -0x19

    aput v6, v1, v5

    const/16 v5, 0x5044

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_3b
    array-length v5, v1

    if-lt v3, v5, :cond_2a

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3c
    array-length v5, v1

    if-lt v3, v5, :cond_2b

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->noDeviceString:Ljava/lang/String;

    goto/16 :goto_17

    :cond_28
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_39

    :cond_29
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3a

    :cond_2a
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3b

    :cond_2b
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3c

    :cond_2c
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_18

    :cond_2d
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_19

    :cond_2e
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1a

    :cond_2f
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1b

    :cond_30
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const/16 v1, 0xe

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, -0x52

    aput v17, v2, v16

    const/16 v16, -0x5c

    aput v16, v2, v15

    const/16 v15, -0x56

    aput v15, v2, v14

    const/16 v14, 0x3e

    aput v14, v2, v13

    const/16 v13, -0x629b

    aput v13, v2, v12

    const/16 v12, -0x13

    aput v12, v2, v11

    const/16 v11, 0x72

    aput v11, v2, v10

    const/16 v10, 0x5a54

    aput v10, v2, v9

    const/16 v9, 0x13f

    aput v9, v2, v8

    const/16 v8, 0x6b62

    aput v8, v2, v7

    const/16 v7, -0x73fe

    aput v7, v2, v6

    const/4 v6, -0x6

    aput v6, v2, v5

    const/16 v5, -0x2b

    aput v5, v2, v3

    const/16 v3, -0x63

    aput v3, v2, v1

    const/16 v1, 0xe

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, -0x26

    aput v18, v1, v17

    const/16 v17, -0x24

    aput v17, v1, v16

    const/16 v16, -0x31

    aput v16, v1, v15

    const/16 v15, 0x6a

    aput v15, v1, v14

    const/16 v14, -0x6300

    aput v14, v1, v13

    const/16 v13, -0x63

    aput v13, v1, v12

    const/16 v12, 0xb

    aput v12, v1, v11

    const/16 v11, 0x5a00

    aput v11, v1, v10

    const/16 v10, 0x15a

    aput v10, v1, v9

    const/16 v9, 0x6b01

    aput v9, v1, v8

    const/16 v8, -0x7395

    aput v8, v1, v7

    const/16 v7, -0x74

    aput v7, v1, v6

    const/16 v6, -0x50

    aput v6, v1, v5

    const/16 v5, -0x27

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_3d
    array-length v5, v1

    if-lt v3, v5, :cond_31

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3e
    array-length v5, v1

    if-lt v3, v5, :cond_32

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const/16 v1, 0xe

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, -0x4c

    aput v17, v2, v16

    const/16 v16, -0x1195

    aput v16, v2, v15

    const/16 v15, -0x75

    aput v15, v2, v14

    const/16 v14, 0x6e73

    aput v14, v2, v13

    const/16 v13, -0x7df5

    aput v13, v2, v12

    const/16 v12, -0xe

    aput v12, v2, v11

    const/16 v11, -0xbf6

    aput v11, v2, v10

    const/16 v10, -0x60

    aput v10, v2, v9

    const/16 v9, -0x71

    aput v9, v2, v8

    const/16 v8, 0x3835

    aput v8, v2, v7

    const/16 v7, 0x2251

    aput v7, v2, v6

    const/16 v6, 0x1854

    aput v6, v2, v5

    const/16 v5, 0x197d

    aput v5, v2, v3

    const/16 v3, 0x685d

    aput v3, v2, v1

    const/16 v1, 0xe

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, -0x40

    aput v18, v1, v17

    const/16 v17, -0x11ed

    aput v17, v1, v16

    const/16 v16, -0x12

    aput v16, v1, v15

    const/16 v15, 0x6e27

    aput v15, v1, v14

    const/16 v14, -0x7d92

    aput v14, v1, v13

    const/16 v13, -0x7e

    aput v13, v1, v12

    const/16 v12, -0xb8d

    aput v12, v1, v11

    const/16 v11, -0xc

    aput v11, v1, v10

    const/16 v10, -0x16

    aput v10, v1, v9

    const/16 v9, 0x3856

    aput v9, v1, v8

    const/16 v8, 0x2238

    aput v8, v1, v7

    const/16 v7, 0x1822

    aput v7, v1, v6

    const/16 v6, 0x1918

    aput v6, v1, v5

    const/16 v5, 0x6819

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_3f
    array-length v5, v1

    if-lt v3, v5, :cond_33

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_40
    array-length v5, v1

    if-lt v3, v5, :cond_34

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mDeviceTypeText:Ljava/lang/String;

    goto/16 :goto_1c

    :cond_31
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3d

    :cond_32
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3e

    :cond_33
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3f

    :cond_34
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_40

    :cond_35
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1d

    :cond_36
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1e

    :cond_37
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1f

    :cond_38
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_20

    :cond_39
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_21

    :cond_3a
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_22

    :cond_3b
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_23

    :cond_3c
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_24

    :cond_3d
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const/4 v1, 0x4

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/16 v7, -0x5b

    aput v7, v2, v6

    const/16 v6, -0x3a

    aput v6, v2, v5

    const/16 v5, -0x6eda

    aput v5, v2, v3

    const/4 v3, -0x8

    aput v3, v2, v1

    const/4 v1, 0x4

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/16 v8, -0x36

    aput v8, v1, v7

    const/16 v7, -0x60

    aput v7, v1, v6

    const/16 v6, -0x6eb8

    aput v6, v1, v5

    const/16 v5, -0x6f

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_41
    array-length v5, v1

    if-lt v3, v5, :cond_3e

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_42
    array-length v5, v1

    if-lt v3, v5, :cond_3f

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const/4 v1, 0x4

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/16 v7, -0x48

    aput v7, v2, v6

    const/16 v6, -0x5fe8

    aput v6, v2, v5

    const/16 v5, -0x32

    aput v5, v2, v3

    const/16 v3, -0x6cb5

    aput v3, v2, v1

    const/4 v1, 0x4

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/16 v8, -0x29

    aput v8, v1, v7

    const/16 v7, -0x5f82

    aput v7, v1, v6

    const/16 v6, -0x60

    aput v6, v1, v5

    const/16 v5, -0x6cde

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_43
    array-length v5, v1

    if-lt v3, v5, :cond_40

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_44
    array-length v5, v1

    if-lt v3, v5, :cond_41

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->scanningInfoText:Ljava/lang/String;

    goto/16 :goto_25

    :cond_3e
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_41

    :cond_3f
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_42

    :cond_40
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_43

    :cond_41
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_44

    :cond_42
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_26

    :cond_43
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_27

    :cond_44
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_28

    :cond_45
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_29

    :cond_46
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2a

    :cond_47
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2b

    :cond_48
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const/16 v1, 0x10

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, -0x22ca

    aput v19, v2, v18

    const/16 v18, -0x4d

    aput v18, v2, v17

    const/16 v17, -0x61ca

    aput v17, v2, v16

    const/16 v16, -0x16

    aput v16, v2, v15

    const/16 v15, 0x607c

    aput v15, v2, v14

    const/16 v14, 0x110f

    aput v14, v2, v13

    const/16 v13, -0x768e

    aput v13, v2, v12

    const/16 v12, -0x2a

    aput v12, v2, v11

    const/16 v11, -0x48

    aput v11, v2, v10

    const/16 v10, -0x1d

    aput v10, v2, v9

    const/16 v9, -0x6e92

    aput v9, v2, v8

    const/16 v8, -0x1b

    aput v8, v2, v7

    const/16 v7, -0x539e

    aput v7, v2, v6

    const/16 v6, -0x32

    aput v6, v2, v5

    const/16 v5, -0x1e

    aput v5, v2, v3

    const/16 v3, 0x1974

    aput v3, v2, v1

    const/16 v1, 0x10

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, -0x22be

    aput v20, v1, v19

    const/16 v19, -0x23

    aput v19, v1, v18

    const/16 v18, -0x61ad

    aput v18, v1, v17

    const/16 v17, -0x62

    aput v17, v1, v16

    const/16 v16, 0x6012

    aput v16, v1, v15

    const/16 v15, 0x1160

    aput v15, v1, v14

    const/16 v14, -0x76ef

    aput v14, v1, v13

    const/16 v13, -0x77

    aput v13, v1, v12

    const/16 v12, -0x35

    aput v12, v1, v11

    const/16 v11, -0x7f

    aput v11, v1, v10

    const/16 v10, -0x6ef1

    aput v10, v1, v9

    const/16 v9, -0x6f

    aput v9, v1, v8

    const/16 v8, -0x53c3

    aput v8, v1, v7

    const/16 v7, -0x54

    aput v7, v1, v6

    const/16 v6, -0x69

    aput v6, v1, v5

    const/16 v5, 0x1907

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_45
    array-length v5, v1

    if-lt v3, v5, :cond_49

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_46
    array-length v5, v1

    if-lt v3, v5, :cond_4a

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const/16 v1, 0x10

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, -0x52

    aput v19, v2, v18

    const/16 v18, -0x4d

    aput v18, v2, v17

    const/16 v17, 0xa1c

    aput v17, v2, v16

    const/16 v16, 0x427e

    aput v16, v2, v15

    const/16 v15, 0x412c

    aput v15, v2, v14

    const/16 v14, 0x1a2e

    aput v14, v2, v13

    const/16 v13, -0x7987

    aput v13, v2, v12

    const/16 v12, -0x27

    aput v12, v2, v11

    const/16 v11, 0xa71

    aput v11, v2, v10

    const/16 v10, 0x6b68

    aput v10, v2, v9

    const/16 v9, -0x39f6

    aput v9, v2, v8

    const/16 v8, -0x4e

    aput v8, v2, v7

    const/16 v7, 0x242d

    aput v7, v2, v6

    const/16 v6, -0x31ba

    aput v6, v2, v5

    const/16 v5, -0x45

    aput v5, v2, v3

    const/16 v3, -0x52ec

    aput v3, v2, v1

    const/16 v1, 0x10

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, -0x26

    aput v20, v1, v19

    const/16 v19, -0x23

    aput v19, v1, v18

    const/16 v18, 0xa79

    aput v18, v1, v17

    const/16 v17, 0x420a

    aput v17, v1, v16

    const/16 v16, 0x4142

    aput v16, v1, v15

    const/16 v15, 0x1a41

    aput v15, v1, v14

    const/16 v14, -0x79e6

    aput v14, v1, v13

    const/16 v13, -0x7a

    aput v13, v1, v12

    const/16 v12, 0xa02

    aput v12, v1, v11

    const/16 v11, 0x6b0a

    aput v11, v1, v10

    const/16 v10, -0x3995

    aput v10, v1, v9

    const/16 v9, -0x3a

    aput v9, v1, v8

    const/16 v8, 0x2472

    aput v8, v1, v7

    const/16 v7, -0x31dc

    aput v7, v1, v6

    const/16 v6, -0x32

    aput v6, v1, v5

    const/16 v5, -0x5299

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_47
    array-length v5, v1

    if-lt v3, v5, :cond_4b

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_48
    array-length v5, v1

    if-lt v3, v5, :cond_4c

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mSubTabsContent:[Ljava/lang/String;

    goto/16 :goto_2c

    :cond_49
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_45

    :cond_4a
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_46

    :cond_4b
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_47

    :cond_4c
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_48

    :cond_4d
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2d

    :cond_4e
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2e

    :cond_4f
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2f

    :cond_50
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_30

    :cond_51
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_31

    :cond_52
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_32

    :cond_53
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_33

    :cond_54
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_34
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 29

    const/4 v1, 0x2

    new-array v5, v1, [J

    const/4 v1, 0x1

    const-wide/16 v2, 0x1

    aput-wide v2, v5, v1

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    instance-of v1, v1, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;

    if-eqz v1, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->removeAllActionBarButtons()V

    :cond_0
    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$layout;->scanning_fragment:I

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->rootview:Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->rootview:Landroid/view/View;

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$id;->scanButton:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->scanbutton:Landroid/widget/Button;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->rootview:Landroid/view/View;

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$id;->scannedlist:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mScannedlist:Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->rootview:Landroid/view/View;

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$id;->paired_list_container:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mPairedListContainer:Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->rootview:Landroid/view/View;

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$id;->Nodevices:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->nodevices:Landroid/widget/FrameLayout;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->rootview:Landroid/view/View;

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$id;->Scanning_layout:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->scanningLayout:Landroid/widget/RelativeLayout;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->rootview:Landroid/view/View;

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$id;->progressBar1:I

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->spinner:Landroid/widget/ProgressBar;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->spinner:Landroid/widget/ProgressBar;

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$drawable;->tw_widget_progressbar:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setProgressDrawable(Landroid/graphics/drawable/Drawable;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->spinner:Landroid/widget/ProgressBar;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->rootview:Landroid/view/View;

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$id;->no_devices_text:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->nodevicesText:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->rootview:Landroid/view/View;

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$id;->no_devices_image:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->nodevicesImage:Landroid/widget/ImageView;

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->setNoDeviceImage()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->rootview:Landroid/view/View;

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$id;->paired_device_layout:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mPairedDevicesLayout:Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mPairedDevicesLayout:Landroid/widget/LinearLayout;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$string;->paired_accessories:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/sec/android/app/shealth/framework/ui/R$string;->prompt_header:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->setContentDescription(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->deviceTypeTextResId:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_9

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->rootview:Landroid/view/View;

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$id;->detected_devices_header:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->deviceTypeTextResId:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/sec/android/app/shealth/framework/ui/R$string;->prompt_header:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->setContentDescription(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->rootview:Landroid/view/View;

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$id;->tv_scanning_info:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mInformationView:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->rootview:Landroid/view/View;

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$id;->paired_list_layout:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mPairedListLayout:Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mPairedText:Ljava/lang/String;

    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->rootview:Landroid/view/View;

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$id;->paired_type_text:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mPairedTextResId:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_a

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mPairedTextResId:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->rootview:Landroid/view/View;

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$id;->device_type_text:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->deviceTypeTextResId:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_b

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->deviceTypeTextResId:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    :goto_3
    new-instance v1, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->deviceMap:Ljava/util/concurrent/ConcurrentHashMap;

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$ScannedDeviceListAdapter;

    move-object/from16 v0, p0

    invoke-direct {v1, v0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$ScannedDeviceListAdapter;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mScannedDeviceListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$ScannedDeviceListAdapter;

    new-instance v1, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mPairedDeviceMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->scanbutton:Landroid/widget/Button;

    new-instance v2, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$5;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$5;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mScanningInfoTextResId:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->scanningInfoText:Ljava/lang/String;

    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_8

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->scanningInfoText:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_3

    :cond_2
    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mScanningInfoTextResId:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_c

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mInformationView:Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mScanningInfoTextResId:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_3
    :goto_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mInfoContentIds:[I

    :try_start_2
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_3

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mInfoContentIds:[I

    array-length v1, v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_d

    const/4 v1, 0x1

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mInfoContent:Z

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mInfoContentIds:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->contentId:I

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$6;

    move-object/from16 v0, p0

    invoke-direct {v1, v0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$6;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)V

    new-instance v2, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$drawable;->s_health_actionbar_info:I

    invoke-direct {v2, v3, v1}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;-><init>(ILandroid/view/View$OnClickListener;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mActionBarButtonBuilder:Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mActionBarButtonBuilder:Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$string;->information:I

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->setHoverTextResID(I)Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    :cond_4
    :goto_5
    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mDataType:I

    const/4 v2, 0x5

    if-eq v1, v2, :cond_16

    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mDataType:I

    const/16 v2, 0xa

    if-eq v1, v2, :cond_16

    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mDataType:I

    const/16 v2, 0xb

    if-eq v1, v2, :cond_16

    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mDataType:I

    const/4 v2, 0x4

    if-ne v1, v2, :cond_5

    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mConnectivityType:I

    const/4 v2, 0x4

    if-eq v1, v2, :cond_16

    :cond_5
    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mDataType:I

    const/16 v2, 0x14

    if-eq v1, v2, :cond_16

    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mDataType:I

    const/4 v2, 0x4

    if-ne v1, v2, :cond_6

    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mConnectivityType:I

    const/4 v2, 0x4

    if-eq v1, v2, :cond_6

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mInfoContent:Z

    if-eqz v1, :cond_6

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mSubTabsContentResIds:[I

    :try_start_3
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_9

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mSubTabsContentResIds:[I

    array-length v1, v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_6

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mActionBarButtonBuilder:Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->create()Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    :cond_6
    :goto_6
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    const/4 v4, 0x0

    const/16 v1, 0x8

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, -0x58

    aput v13, v2, v12

    const/16 v12, -0x48

    aput v12, v2, v11

    const/16 v11, -0x32a9

    aput v11, v2, v10

    const/16 v10, -0x67

    aput v10, v2, v9

    const/16 v9, -0x8b6

    aput v9, v2, v8

    const/16 v8, -0x7d

    aput v8, v2, v7

    const/16 v7, 0x6d3f

    aput v7, v2, v3

    const/16 v3, 0x5429

    aput v3, v2, v1

    const/16 v1, 0x8

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/4 v12, 0x6

    const/4 v13, 0x7

    const/16 v14, -0x33

    aput v14, v1, v13

    const/16 v13, -0x38

    aput v13, v1, v12

    const/16 v12, -0x32d2

    aput v12, v1, v11

    const/16 v11, -0x33

    aput v11, v1, v10

    const/16 v10, -0x8d5

    aput v10, v1, v9

    const/16 v9, -0x9

    aput v9, v1, v8

    const/16 v8, 0x6d5e

    aput v8, v1, v7

    const/16 v7, 0x546d

    aput v7, v1, v3

    const/4 v3, 0x0

    :goto_7
    array-length v7, v1

    if-lt v3, v7, :cond_17

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_8
    array-length v7, v1

    if-lt v3, v7, :cond_18

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    const/16 v1, 0x8

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, -0x26

    aput v13, v2, v12

    const/16 v12, -0x19

    aput v12, v2, v11

    const/16 v11, -0x57

    aput v11, v2, v10

    const/16 v10, -0x48

    aput v10, v2, v9

    const/16 v9, -0x32

    aput v9, v2, v8

    const/16 v8, -0x69

    aput v8, v2, v7

    const/16 v7, -0x4d

    aput v7, v2, v3

    const/16 v3, 0x5d35

    aput v3, v2, v1

    const/16 v1, 0x8

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/4 v12, 0x6

    const/4 v13, 0x7

    const/16 v14, -0x41

    aput v14, v1, v13

    const/16 v13, -0x69

    aput v13, v1, v12

    const/16 v12, -0x30

    aput v12, v1, v11

    const/16 v11, -0x14

    aput v11, v1, v10

    const/16 v10, -0x51

    aput v10, v1, v9

    const/16 v9, -0x1d

    aput v9, v1, v8

    const/16 v8, -0x2e

    aput v8, v1, v7

    const/16 v7, 0x5d71

    aput v7, v1, v3

    const/4 v3, 0x0

    :goto_9
    array-length v7, v1

    if-lt v3, v7, :cond_19

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_a
    array-length v7, v1

    if-lt v3, v7, :cond_1a

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v6, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mDataType:I

    :cond_7
    const/16 v1, 0x16

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, 0x11

    const/16 v23, 0x12

    const/16 v24, 0x13

    const/16 v25, 0x14

    const/16 v26, 0x15

    const/16 v27, -0x78

    aput v27, v2, v26

    const/16 v26, 0x355e

    aput v26, v2, v25

    const/16 v25, 0x385c

    aput v25, v2, v24

    const/16 v24, 0x6267

    aput v24, v2, v23

    const/16 v23, -0x2dfb

    aput v23, v2, v22

    const/16 v22, -0x44

    aput v22, v2, v21

    const/16 v21, -0x6

    aput v21, v2, v20

    const/16 v20, -0x2d

    aput v20, v2, v19

    const/16 v19, -0x7

    aput v19, v2, v18

    const/16 v18, -0x5adb

    aput v18, v2, v17

    const/16 v17, -0x6

    aput v17, v2, v16

    const/16 v16, -0x57bc

    aput v16, v2, v15

    const/16 v15, -0x30

    aput v15, v2, v14

    const/16 v14, -0x4c

    aput v14, v2, v13

    const/16 v13, 0x2132

    aput v13, v2, v12

    const/16 v12, -0x5a82

    aput v12, v2, v11

    const/16 v11, -0x29

    aput v11, v2, v10

    const/16 v10, 0x3538

    aput v10, v2, v9

    const/16 v9, -0x75af

    aput v9, v2, v8

    const/16 v8, -0x15

    aput v8, v2, v7

    const/16 v7, -0x8d5

    aput v7, v2, v3

    const/16 v3, -0x61

    aput v3, v2, v1

    const/16 v1, 0x16

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/4 v12, 0x6

    const/4 v13, 0x7

    const/16 v14, 0x8

    const/16 v15, 0x9

    const/16 v16, 0xa

    const/16 v17, 0xb

    const/16 v18, 0xc

    const/16 v19, 0xd

    const/16 v20, 0xe

    const/16 v21, 0xf

    const/16 v22, 0x10

    const/16 v23, 0x11

    const/16 v24, 0x12

    const/16 v25, 0x13

    const/16 v26, 0x14

    const/16 v27, 0x15

    const/16 v28, -0x5

    aput v28, v1, v27

    const/16 v27, 0x353a

    aput v27, v1, v26

    const/16 v26, 0x3835

    aput v26, v1, v25

    const/16 v25, 0x6238

    aput v25, v1, v24

    const/16 v24, -0x2d9e

    aput v24, v1, v23

    const/16 v23, -0x2e

    aput v23, v1, v22

    const/16 v22, -0x6d

    aput v22, v1, v21

    const/16 v21, -0x5f

    aput v21, v1, v20

    const/16 v20, -0x73

    aput v20, v1, v19

    const/16 v19, -0x5aaa

    aput v19, v1, v18

    const/16 v18, -0x5b

    aput v18, v1, v17

    const/16 v17, -0x57d0

    aput v17, v1, v16

    const/16 v16, -0x58

    aput v16, v1, v15

    const/16 v15, -0x2f

    aput v15, v1, v14

    const/16 v14, 0x2146

    aput v14, v1, v13

    const/16 v13, -0x5adf

    aput v13, v1, v12

    const/16 v12, -0x5b

    aput v12, v1, v11

    const/16 v11, 0x355d

    aput v11, v1, v10

    const/16 v10, -0x75cb

    aput v10, v1, v9

    const/16 v9, -0x76

    aput v9, v1, v8

    const/16 v8, -0x8b2

    aput v8, v1, v7

    const/16 v7, -0x9

    aput v7, v1, v3

    const/4 v3, 0x0

    :goto_b
    array-length v7, v1

    if-lt v3, v7, :cond_1b

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_c
    array-length v7, v1

    if-lt v3, v7, :cond_1c

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1f

    const/16 v1, 0x16

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, -0x319d

    aput v26, v2, v25

    const/16 v25, -0x56

    aput v25, v2, v24

    const/16 v24, -0xab9

    aput v24, v2, v23

    const/16 v23, -0x56

    aput v23, v2, v22

    const/16 v22, -0x2fe8

    aput v22, v2, v21

    const/16 v21, -0x42

    aput v21, v2, v20

    const/16 v20, 0x6d19

    aput v20, v2, v19

    const/16 v19, -0x30e1

    aput v19, v2, v18

    const/16 v18, -0x45

    aput v18, v2, v17

    const/16 v17, -0x3f

    aput v17, v2, v16

    const/16 v16, -0xb

    aput v16, v2, v15

    const/16 v15, -0xa

    aput v15, v2, v14

    const/16 v14, -0x62

    aput v14, v2, v13

    const/16 v13, -0x61

    aput v13, v2, v12

    const/16 v12, -0x48bf

    aput v12, v2, v11

    const/16 v11, -0x18

    aput v11, v2, v10

    const/16 v10, -0x33

    aput v10, v2, v9

    const/16 v9, -0x28

    aput v9, v2, v8

    const/16 v8, -0x7a8c

    aput v8, v2, v7

    const/16 v7, -0x1c

    aput v7, v2, v5

    const/16 v5, -0x5ad6

    aput v5, v2, v3

    const/16 v3, -0x33

    aput v3, v2, v1

    const/16 v1, 0x16

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, 0x11

    const/16 v23, 0x12

    const/16 v24, 0x13

    const/16 v25, 0x14

    const/16 v26, 0x15

    const/16 v27, -0x31f0

    aput v27, v1, v26

    const/16 v26, -0x32

    aput v26, v1, v25

    const/16 v25, -0xad2

    aput v25, v1, v24

    const/16 v24, -0xb

    aput v24, v1, v23

    const/16 v23, -0x2f81

    aput v23, v1, v22

    const/16 v22, -0x30

    aput v22, v1, v21

    const/16 v21, 0x6d70

    aput v21, v1, v20

    const/16 v20, -0x3093

    aput v20, v1, v19

    const/16 v19, -0x31

    aput v19, v1, v18

    const/16 v18, -0x4e

    aput v18, v1, v17

    const/16 v17, -0x56

    aput v17, v1, v16

    const/16 v16, -0x7e

    aput v16, v1, v15

    const/16 v15, -0x1a

    aput v15, v1, v14

    const/4 v14, -0x6

    aput v14, v1, v13

    const/16 v13, -0x48cb

    aput v13, v1, v12

    const/16 v12, -0x49

    aput v12, v1, v11

    const/16 v11, -0x41

    aput v11, v1, v10

    const/16 v10, -0x43

    aput v10, v1, v9

    const/16 v9, -0x7af0

    aput v9, v1, v8

    const/16 v8, -0x7b

    aput v8, v1, v7

    const/16 v7, -0x5ab1

    aput v7, v1, v5

    const/16 v5, -0x5b

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_d
    array-length v5, v1

    if-lt v3, v5, :cond_1d

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_e
    array-length v5, v1

    if-lt v3, v5, :cond_1e

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Landroid/content/Intent;->getIntArrayExtra(Ljava/lang/String;)[I

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mHeaderTextResIds:[I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mHeaderTextResIds:[I

    :try_start_4
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_4
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_4} :catch_4

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mHeaderTextResIds:[I

    array-length v1, v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_3a

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mHeaderTextResIds:[I

    const/4 v3, 0x0

    aget v2, v2, v3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    :goto_f
    const/16 v1, 0xb

    new-array v3, v1, [I

    const/4 v1, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, -0x5b

    aput v15, v3, v14

    const/16 v14, -0x72c3

    aput v14, v3, v13

    const/4 v13, -0x3

    aput v13, v3, v12

    const/16 v12, -0x68

    aput v12, v3, v11

    const/16 v11, -0x5d

    aput v11, v3, v10

    const/16 v10, -0x23

    aput v10, v3, v9

    const/16 v9, -0x22

    aput v9, v3, v8

    const/16 v8, -0x6e

    aput v8, v3, v7

    const/16 v7, 0x3d23

    aput v7, v3, v5

    const/16 v5, 0x7958

    aput v5, v3, v4

    const/16 v4, 0x13d

    aput v4, v3, v1

    const/16 v1, 0xb

    new-array v1, v1, [I

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, -0x2a

    aput v16, v1, v15

    const/16 v15, -0x72a8

    aput v15, v1, v14

    const/16 v14, -0x73

    aput v14, v1, v13

    const/16 v13, -0x1f

    aput v13, v1, v12

    const/16 v12, -0x9

    aput v12, v1, v11

    const/16 v11, -0x48

    aput v11, v1, v10

    const/16 v10, -0x43

    aput v10, v1, v9

    const/4 v9, -0x5

    aput v9, v1, v8

    const/16 v8, 0x3d55

    aput v8, v1, v7

    const/16 v7, 0x793d

    aput v7, v1, v5

    const/16 v5, 0x179

    aput v5, v1, v4

    const/4 v4, 0x0

    :goto_10
    array-length v5, v1

    if-lt v4, v5, :cond_34

    array-length v1, v3

    new-array v1, v1, [C

    const/4 v4, 0x0

    :goto_11
    array-length v5, v1

    if-lt v4, v5, :cond_35

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    const/16 v1, 0xb

    new-array v3, v1, [I

    const/4 v1, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, -0x2fb

    aput v15, v3, v14

    const/16 v14, -0x68

    aput v14, v3, v13

    const/16 v13, -0x25

    aput v13, v3, v12

    const/16 v12, 0x204e

    aput v12, v3, v11

    const/16 v11, -0x768c

    aput v11, v3, v10

    const/16 v10, -0x14

    aput v10, v3, v9

    const/16 v9, -0x44

    aput v9, v3, v8

    const/16 v8, -0x42

    aput v8, v3, v7

    const/16 v7, -0x45

    aput v7, v3, v5

    const/16 v5, 0x615a

    aput v5, v3, v4

    const/16 v4, -0x48db

    aput v4, v3, v1

    const/16 v1, 0xb

    new-array v1, v1, [I

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, -0x28a

    aput v16, v1, v15

    const/4 v15, -0x3

    aput v15, v1, v14

    const/16 v14, -0x55

    aput v14, v1, v13

    const/16 v13, 0x2037

    aput v13, v1, v12

    const/16 v12, -0x76e0

    aput v12, v1, v11

    const/16 v11, -0x77

    aput v11, v1, v10

    const/16 v10, -0x21

    aput v10, v1, v9

    const/16 v9, -0x29

    aput v9, v1, v8

    const/16 v8, -0x33

    aput v8, v1, v7

    const/16 v7, 0x613f

    aput v7, v1, v5

    const/16 v5, -0x489f

    aput v5, v1, v4

    const/4 v4, 0x0

    :goto_12
    array-length v5, v1

    if-lt v4, v5, :cond_36

    array-length v1, v3

    new-array v1, v1, [C

    const/4 v4, 0x0

    :goto_13
    array-length v5, v1

    if-lt v4, v5, :cond_37

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Landroid/content/Intent;->getIntegerArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mDeviceTypes:Ljava/util/ArrayList;

    :cond_8
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->rootview:Landroid/view/View;

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$id;->scanning_header_text:I

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->headerText:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->rootview:Landroid/view/View;

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$id;->header_layout:I

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mHeaderLayout:Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mDataType:I

    if-nez v1, :cond_38

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mHeaderLayout:Landroid/widget/LinearLayout;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :goto_14
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mSubTabsContentResIds:[I

    :try_start_5
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_5
    .catch Ljava/lang/NullPointerException; {:try_start_5 .. :try_end_5} :catch_5

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mSubTabsContentResIds:[I

    array-length v1, v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_39

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mContext:Landroid/content/Context;

    :try_start_6
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_6
    .catch Ljava/lang/NullPointerException; {:try_start_6 .. :try_end_6} :catch_b

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$id;->subtabs_rd_grp:I

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioGroup;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mSubTabsGrp:Landroid/widget/RadioGroup;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$id;->first_sub_tab_rd_btn:I

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mfirstSubTab:Landroid/widget/RadioButton;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$id;->second_sub_tab_rd_btn:I

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->msecondSubTab:Landroid/widget/RadioButton;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mfirstSubTab:Landroid/widget/RadioButton;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mSubTabsContentResIds:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->msecondSubTab:Landroid/widget/RadioButton;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mSubTabsContentResIds:[I

    const/4 v4, 0x1

    aget v3, v3, v4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    :goto_15
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->rootview:Landroid/view/View;

    return-object v1

    :cond_9
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mDeviceTypeText:Ljava/lang/String;

    :try_start_7
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_7
    .catch Ljava/lang/NullPointerException; {:try_start_7 .. :try_end_7} :catch_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->rootview:Landroid/view/View;

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$id;->detected_devices_header:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mDeviceTypeText:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/sec/android/app/shealth/framework/ui/R$string;->prompt_header:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->setContentDescription(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :catch_0
    move-exception v1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->rootview:Landroid/view/View;

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$id;->detected_devices_header:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$string;->detected_accessories:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/sec/android/app/shealth/framework/ui/R$string;->prompt_header:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->setContentDescription(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :catch_1
    move-exception v1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mDeviceTypeText:Ljava/lang/String;

    :try_start_8
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_8
    .catch Ljava/lang/NullPointerException; {:try_start_8 .. :try_end_8} :catch_2

    goto/16 :goto_1

    :catch_2
    move-exception v1

    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mPairedTextResId:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    goto/16 :goto_1

    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mPairedText:Ljava/lang/String;

    :try_start_9
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_9
    .catch Ljava/lang/NullPointerException; {:try_start_9 .. :try_end_9} :catch_6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mPairedText:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    :cond_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mDeviceTypeText:Ljava/lang/String;

    :try_start_a
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_a
    .catch Ljava/lang/NullPointerException; {:try_start_a .. :try_end_a} :catch_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mDeviceTypeText:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    :cond_c
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mInformationView:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->scanningInfoText:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    :catch_3
    move-exception v1

    :cond_d
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const/16 v1, 0xc

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, -0x3cf6

    aput v16, v2, v15

    const/16 v15, -0x53

    aput v15, v2, v14

    const/16 v14, -0x3a

    aput v14, v2, v13

    const/16 v13, 0xa71

    aput v13, v2, v12

    const/16 v12, 0x4164

    aput v12, v2, v11

    const/16 v11, -0x6ad2

    aput v11, v2, v10

    const/16 v10, -0xa

    aput v10, v2, v9

    const/16 v9, -0x4e

    aput v9, v2, v8

    const/16 v8, -0x1cae

    aput v8, v2, v7

    const/16 v7, -0x7b

    aput v7, v2, v6

    const/16 v6, -0x4f

    aput v6, v2, v3

    const/16 v3, 0x783f

    aput v3, v2, v1

    const/16 v1, 0xc

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, -0x3c82

    aput v17, v1, v16

    const/16 v16, -0x3d

    aput v16, v1, v15

    const/16 v15, -0x5d

    aput v15, v1, v14

    const/16 v14, 0xa05

    aput v14, v1, v13

    const/16 v13, 0x410a

    aput v13, v1, v12

    const/16 v12, -0x6abf

    aput v12, v1, v11

    const/16 v11, -0x6b

    aput v11, v1, v10

    const/16 v10, -0x13

    aput v10, v1, v9

    const/16 v9, -0x1cc3

    aput v9, v1, v8

    const/16 v8, -0x1d

    aput v8, v1, v7

    const/16 v7, -0x21

    aput v7, v1, v6

    const/16 v6, 0x7856

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_16
    array-length v6, v1

    if-lt v3, v6, :cond_e

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_17
    array-length v6, v1

    if-lt v3, v6, :cond_f

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const/16 v1, 0xc

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, -0x2ec7

    aput v16, v2, v15

    const/16 v15, -0x41

    aput v15, v2, v14

    const/16 v14, -0x28

    aput v14, v2, v13

    const/16 v13, -0x23

    aput v13, v2, v12

    const/16 v12, 0x1716

    aput v12, v2, v11

    const/16 v11, -0x1488

    aput v11, v2, v10

    const/16 v10, -0x78

    aput v10, v2, v9

    const/16 v9, -0x6a

    aput v9, v2, v8

    const/16 v8, -0x1be6

    aput v8, v2, v7

    const/16 v7, -0x7e

    aput v7, v2, v6

    const/16 v6, -0x3a

    aput v6, v2, v3

    const/16 v3, 0x155a

    aput v3, v2, v1

    const/16 v1, 0xc

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, -0x2eb3

    aput v17, v1, v16

    const/16 v16, -0x2f

    aput v16, v1, v15

    const/16 v15, -0x43

    aput v15, v1, v14

    const/16 v14, -0x57

    aput v14, v1, v13

    const/16 v13, 0x1778

    aput v13, v1, v12

    const/16 v12, -0x14e9

    aput v12, v1, v11

    const/16 v11, -0x15

    aput v11, v1, v10

    const/16 v10, -0x37

    aput v10, v1, v9

    const/16 v9, -0x1b8b

    aput v9, v1, v8

    const/16 v8, -0x1c

    aput v8, v1, v7

    const/16 v7, -0x58

    aput v7, v1, v6

    const/16 v6, 0x1533

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_18
    array-length v6, v1

    if-lt v3, v6, :cond_10

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_19
    array-length v6, v1

    if-lt v3, v6, :cond_11

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const/16 v1, 0xc

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, -0x40

    aput v16, v2, v15

    const/16 v15, -0x5f

    aput v15, v2, v14

    const/16 v14, -0x5a

    aput v14, v2, v13

    const/16 v13, 0x74e

    aput v13, v2, v12

    const/16 v12, 0x4869

    aput v12, v2, v11

    const/16 v11, -0x50d9

    aput v11, v2, v10

    const/16 v10, -0x34

    aput v10, v2, v9

    const/16 v9, -0x31cf

    aput v9, v2, v8

    const/16 v8, -0x5f

    aput v8, v2, v7

    const/16 v7, -0x42

    aput v7, v2, v6

    const/16 v6, -0x30

    aput v6, v2, v3

    const/16 v3, -0x7e81

    aput v3, v2, v1

    const/16 v1, 0xc

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, -0x4c

    aput v17, v1, v16

    const/16 v16, -0x31

    aput v16, v1, v15

    const/16 v15, -0x3d

    aput v15, v1, v14

    const/16 v14, 0x73a

    aput v14, v1, v13

    const/16 v13, 0x4807

    aput v13, v1, v12

    const/16 v12, -0x50b8

    aput v12, v1, v11

    const/16 v11, -0x51

    aput v11, v1, v10

    const/16 v10, -0x3192

    aput v10, v1, v9

    const/16 v9, -0x32

    aput v9, v1, v8

    const/16 v8, -0x28

    aput v8, v1, v7

    const/16 v7, -0x42

    aput v7, v1, v6

    const/16 v6, -0x7eea

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_1a
    array-length v6, v1

    if-lt v3, v6, :cond_12

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1b
    array-length v6, v1

    if-lt v3, v6, :cond_13

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_4

    const/4 v1, 0x1

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mInfoContent:Z

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const/16 v1, 0xc

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, -0x55b5

    aput v16, v2, v15

    const/16 v15, -0x3c

    aput v15, v2, v14

    const/16 v14, 0x5e28

    aput v14, v2, v13

    const/16 v13, 0x322a

    aput v13, v2, v12

    const/16 v12, 0x15c

    aput v12, v2, v11

    const/16 v11, 0x716e

    aput v11, v2, v10

    const/16 v10, -0x1cee

    aput v10, v2, v9

    const/16 v9, -0x44

    aput v9, v2, v8

    const/16 v8, -0x6a8b

    aput v8, v2, v7

    const/16 v7, -0xd

    aput v7, v2, v6

    const/16 v6, -0x3f

    aput v6, v2, v3

    const/16 v3, 0x3f02

    aput v3, v2, v1

    const/16 v1, 0xc

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, -0x55c1

    aput v17, v1, v16

    const/16 v16, -0x56

    aput v16, v1, v15

    const/16 v15, 0x5e4d

    aput v15, v1, v14

    const/16 v14, 0x325e

    aput v14, v1, v13

    const/16 v13, 0x132

    aput v13, v1, v12

    const/16 v12, 0x7101

    aput v12, v1, v11

    const/16 v11, -0x1c8f

    aput v11, v1, v10

    const/16 v10, -0x1d

    aput v10, v1, v9

    const/16 v9, -0x6ae6

    aput v9, v1, v8

    const/16 v8, -0x6b

    aput v8, v1, v7

    const/16 v7, -0x51

    aput v7, v1, v6

    const/16 v6, 0x3f6b

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_1c
    array-length v6, v1

    if-lt v3, v6, :cond_14

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1d
    array-length v6, v1

    if-lt v3, v6, :cond_15

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v4, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->contentId:I

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$7;

    move-object/from16 v0, p0

    invoke-direct {v1, v0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$7;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)V

    new-instance v2, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$drawable;->s_health_actionbar_info:I

    invoke-direct {v2, v3, v1}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;-><init>(ILandroid/view/View$OnClickListener;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mActionBarButtonBuilder:Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mActionBarButtonBuilder:Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$string;->information:I

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->setHoverTextResID(I)Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    goto/16 :goto_5

    :cond_e
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_16

    :cond_f
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_17

    :cond_10
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_18

    :cond_11
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_19

    :cond_12
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1a

    :cond_13
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1b

    :cond_14
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1c

    :cond_15
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1d

    :cond_16
    const/4 v1, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->setScanningViewsVisibility(Z)V

    goto/16 :goto_6

    :cond_17
    aget v7, v1, v3

    aget v8, v2, v3

    xor-int/2addr v7, v8

    aput v7, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_7

    :cond_18
    aget v7, v2, v3

    int-to-char v7, v7

    aput-char v7, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_8

    :cond_19
    aget v7, v1, v3

    aget v8, v2, v3

    xor-int/2addr v7, v8

    aput v7, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_9

    :cond_1a
    aget v7, v2, v3

    int-to-char v7, v7

    aput-char v7, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_a

    :cond_1b
    aget v7, v1, v3

    aget v8, v2, v3

    xor-int/2addr v7, v8

    aput v7, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_b

    :cond_1c
    aget v7, v2, v3

    int-to-char v7, v7

    aput-char v7, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_c

    :cond_1d
    aget v5, v1, v3

    aget v7, v2, v3

    xor-int/2addr v5, v7

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_d

    :cond_1e
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_e

    :cond_1f
    const/16 v1, 0x14

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, 0x11

    const/16 v23, 0x12

    const/16 v24, 0x13

    const/16 v25, -0x7afa

    aput v25, v2, v24

    const/16 v24, -0x14

    aput v24, v2, v23

    const/16 v23, -0x52

    aput v23, v2, v22

    const/16 v22, -0x4993

    aput v22, v2, v21

    const/16 v21, -0x21

    aput v21, v2, v20

    const/16 v20, -0x329f

    aput v20, v2, v19

    const/16 v19, -0x47

    aput v19, v2, v18

    const/16 v18, 0x4b51

    aput v18, v2, v17

    const/16 v17, 0x6314

    aput v17, v2, v16

    const/16 v16, 0x7717

    aput v16, v2, v15

    const/16 v15, -0xdf1

    aput v15, v2, v14

    const/16 v14, -0x69

    aput v14, v2, v13

    const/16 v13, -0x2cca

    aput v13, v2, v12

    const/16 v12, -0x74

    aput v12, v2, v11

    const/16 v11, -0x47

    aput v11, v2, v10

    const/16 v10, -0x33

    aput v10, v2, v9

    const/16 v9, -0x51

    aput v9, v2, v8

    const/16 v8, -0x4c

    aput v8, v2, v7

    const/16 v7, -0x5599

    aput v7, v2, v3

    const/16 v3, -0x3e

    aput v3, v2, v1

    const/16 v1, 0x14

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/4 v12, 0x6

    const/4 v13, 0x7

    const/16 v14, 0x8

    const/16 v15, 0x9

    const/16 v16, 0xa

    const/16 v17, 0xb

    const/16 v18, 0xc

    const/16 v19, 0xd

    const/16 v20, 0xe

    const/16 v21, 0xf

    const/16 v22, 0x10

    const/16 v23, 0x11

    const/16 v24, 0x12

    const/16 v25, 0x13

    const/16 v26, -0x7a9e

    aput v26, v1, v25

    const/16 v25, -0x7b

    aput v25, v1, v24

    const/16 v24, -0x37

    aput v24, v1, v23

    const/16 v23, -0x49fd

    aput v23, v1, v22

    const/16 v22, -0x4a

    aput v22, v1, v21

    const/16 v21, -0x32ed

    aput v21, v1, v20

    const/16 v20, -0x33

    aput v20, v1, v19

    const/16 v19, 0x4b22

    aput v19, v1, v18

    const/16 v18, 0x634b

    aput v18, v1, v17

    const/16 v17, 0x7763

    aput v17, v1, v16

    const/16 v16, -0xd89

    aput v16, v1, v15

    const/16 v15, -0xe

    aput v15, v1, v14

    const/16 v14, -0x2cbe

    aput v14, v1, v13

    const/16 v13, -0x2d

    aput v13, v1, v12

    const/16 v12, -0x35

    aput v12, v1, v11

    const/16 v11, -0x58

    aput v11, v1, v10

    const/16 v10, -0x35

    aput v10, v1, v9

    const/16 v9, -0x2b

    aput v9, v1, v8

    const/16 v8, -0x55fe

    aput v8, v1, v7

    const/16 v7, -0x56

    aput v7, v1, v3

    const/4 v3, 0x0

    :goto_1e
    array-length v7, v1

    if-lt v3, v7, :cond_20

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1f
    array-length v7, v1

    if-lt v3, v7, :cond_21

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2f

    const/16 v1, 0x14

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, -0x6b

    aput v24, v2, v23

    const/16 v23, -0x3f

    aput v23, v2, v22

    const/16 v22, -0x72

    aput v22, v2, v21

    const/16 v21, -0x69

    aput v21, v2, v20

    const/16 v20, 0x1a46

    aput v20, v2, v19

    const/16 v19, 0x6468

    aput v19, v2, v18

    const/16 v18, 0x7b10

    aput v18, v2, v17

    const/16 v17, -0x5af8

    aput v17, v2, v16

    const/16 v16, -0x6

    aput v16, v2, v15

    const/16 v15, -0x3c

    aput v15, v2, v14

    const/16 v14, -0x27

    aput v14, v2, v13

    const/16 v13, -0x30b4

    aput v13, v2, v12

    const/16 v12, -0x45

    aput v12, v2, v11

    const/16 v11, 0x5f1f

    aput v11, v2, v10

    const/16 v10, 0x712d

    aput v10, v2, v9

    const/16 v9, -0x4bec

    aput v9, v2, v8

    const/16 v8, -0x30

    aput v8, v2, v7

    const/16 v7, -0x588e

    aput v7, v2, v4

    const/16 v4, -0x3e

    aput v4, v2, v3

    const/16 v3, -0x69

    aput v3, v2, v1

    const/16 v1, 0x14

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, 0x11

    const/16 v23, 0x12

    const/16 v24, 0x13

    const/16 v25, -0xf

    aput v25, v1, v24

    const/16 v24, -0x58

    aput v24, v1, v23

    const/16 v23, -0x17

    aput v23, v1, v22

    const/16 v22, -0x7

    aput v22, v1, v21

    const/16 v21, 0x1a2f

    aput v21, v1, v20

    const/16 v20, 0x641a

    aput v20, v1, v19

    const/16 v19, 0x7b64

    aput v19, v1, v18

    const/16 v18, -0x5a85

    aput v18, v1, v17

    const/16 v17, -0x5b

    aput v17, v1, v16

    const/16 v16, -0x50

    aput v16, v1, v15

    const/16 v15, -0x5f

    aput v15, v1, v14

    const/16 v14, -0x30d7

    aput v14, v1, v13

    const/16 v13, -0x31

    aput v13, v1, v12

    const/16 v12, 0x5f40

    aput v12, v1, v11

    const/16 v11, 0x715f

    aput v11, v1, v10

    const/16 v10, -0x4b8f

    aput v10, v1, v9

    const/16 v9, -0x4c

    aput v9, v1, v8

    const/16 v8, -0x58ed

    aput v8, v1, v7

    const/16 v7, -0x59

    aput v7, v1, v4

    const/4 v4, -0x1

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_20
    array-length v4, v1

    if-lt v3, v4, :cond_22

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_21
    array-length v4, v1

    if-lt v3, v4, :cond_23

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v6, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    const/4 v2, 0x0

    array-length v3, v5

    add-int/lit8 v3, v3, -0x1

    aget-wide v3, v5, v3

    long-to-int v3, v3

    if-gtz v3, :cond_24

    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_20
    aget v7, v1, v3

    aget v8, v2, v3

    xor-int/2addr v7, v8

    aput v7, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1e

    :cond_21
    aget v7, v2, v3

    int-to-char v7, v7

    aput-char v7, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1f

    :cond_22
    aget v4, v1, v3

    aget v7, v2, v3

    xor-int/2addr v4, v7

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_20

    :cond_23
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_21

    :cond_24
    const/4 v3, 0x0

    int-to-long v1, v1

    const/16 v4, 0x20

    shl-long/2addr v1, v4

    const/16 v4, 0x20

    ushr-long v7, v1, v4

    aget-wide v1, v5, v3

    const-wide/16 v9, 0x0

    cmp-long v4, v1, v9

    if-eqz v4, :cond_25

    const-wide v9, -0x34f69c4aab878693L    # -3.039786992672143E53

    xor-long/2addr v1, v9

    :cond_25
    const/16 v4, 0x20

    ushr-long/2addr v1, v4

    const/16 v4, 0x20

    shl-long/2addr v1, v4

    xor-long/2addr v1, v7

    const-wide v7, -0x34f69c4aab878693L    # -3.039786992672143E53

    xor-long/2addr v1, v7

    aput-wide v1, v5, v3

    array-length v1, v5

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v5, v1

    long-to-int v1, v1

    if-gtz v1, :cond_28

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, 0x2c32

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, 0x2c02

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_22
    array-length v5, v1

    if-lt v3, v5, :cond_26

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_23
    array-length v5, v1

    if-lt v3, v5, :cond_27

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_26
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_22

    :cond_27
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_23

    :cond_28
    const/4 v1, 0x0

    aget-wide v1, v5, v1

    const-wide/16 v3, 0x0

    cmp-long v3, v1, v3

    if-eqz v3, :cond_29

    const-wide v3, -0x34f69c4aab878693L    # -3.039786992672143E53

    xor-long/2addr v1, v3

    :cond_29
    const/16 v3, 0x20

    shl-long/2addr v1, v3

    const/16 v3, 0x20

    shr-long/2addr v1, v3

    long-to-int v1, v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_2a

    const-string v1, ""

    :goto_24
    move-object v2, v1

    goto/16 :goto_f

    :cond_2a
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    array-length v1, v5

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v5, v1

    long-to-int v1, v1

    if-gtz v1, :cond_2d

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, -0x59d1

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, -0x59e1

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_25
    array-length v5, v1

    if-lt v3, v5, :cond_2b

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_26
    array-length v5, v1

    if-lt v3, v5, :cond_2c

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_2b
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_25

    :cond_2c
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_26

    :cond_2d
    const/4 v1, 0x0

    aget-wide v1, v5, v1

    const-wide/16 v4, 0x0

    cmp-long v4, v1, v4

    if-eqz v4, :cond_2e

    const-wide v4, -0x34f69c4aab878693L    # -3.039786992672143E53

    xor-long/2addr v1, v4

    :cond_2e
    const/16 v4, 0x20

    shl-long/2addr v1, v4

    const/16 v4, 0x20

    shr-long/2addr v1, v4

    long-to-int v1, v1

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_24

    :cond_2f
    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, -0x1d

    aput v14, v2, v13

    const/16 v13, 0x1403

    aput v13, v2, v12

    const/16 v12, -0x1c8f

    aput v12, v2, v11

    const/16 v11, -0x49

    aput v11, v2, v10

    const/16 v10, -0x33

    aput v10, v2, v9

    const/16 v9, 0x564d

    aput v9, v2, v8

    const/16 v8, -0x5fce

    aput v8, v2, v7

    const/16 v7, -0x3f

    aput v7, v2, v5

    const/16 v5, -0x19

    aput v5, v2, v3

    const/16 v3, -0x55ec

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, -0x69

    aput v15, v1, v14

    const/16 v14, 0x147b

    aput v14, v1, v13

    const/16 v13, -0x1cec

    aput v13, v1, v12

    const/16 v12, -0x1d

    aput v12, v1, v11

    const/16 v11, -0x41

    aput v11, v1, v10

    const/16 v10, 0x5628

    aput v10, v1, v9

    const/16 v9, -0x5faa

    aput v9, v1, v8

    const/16 v8, -0x60

    aput v8, v1, v7

    const/16 v7, -0x7e

    aput v7, v1, v5

    const/16 v5, -0x55a4

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_27
    array-length v5, v1

    if-lt v3, v5, :cond_30

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_28
    array-length v5, v1

    if-lt v3, v5, :cond_31

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3a

    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, -0x67

    aput v13, v2, v12

    const/16 v12, 0x2161

    aput v12, v2, v11

    const/16 v11, 0xa44

    aput v11, v2, v10

    const/16 v10, 0x165e

    aput v10, v2, v9

    const/16 v9, -0x399c

    aput v9, v2, v8

    const/16 v8, -0x5d

    aput v8, v2, v7

    const/4 v7, -0x8

    aput v7, v2, v5

    const/4 v5, -0x6

    aput v5, v2, v4

    const/16 v4, 0x1d64

    aput v4, v2, v3

    const/16 v3, -0x3dab

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, -0x13

    aput v14, v1, v13

    const/16 v13, 0x2119

    aput v13, v1, v12

    const/16 v12, 0xa21

    aput v12, v1, v11

    const/16 v11, 0x160a

    aput v11, v1, v10

    const/16 v10, -0x39ea

    aput v10, v1, v9

    const/16 v9, -0x3a

    aput v9, v1, v8

    const/16 v8, -0x64

    aput v8, v1, v7

    const/16 v7, -0x65

    aput v7, v1, v5

    const/16 v5, 0x1d01

    aput v5, v1, v4

    const/16 v4, -0x3de3

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_29
    array-length v4, v1

    if-lt v3, v4, :cond_32

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_2a
    array-length v4, v1

    if-lt v3, v4, :cond_33

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    goto/16 :goto_f

    :cond_30
    aget v5, v1, v3

    aget v7, v2, v3

    xor-int/2addr v5, v7

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_27

    :cond_31
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_28

    :cond_32
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_29

    :cond_33
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_2a

    :catch_4
    move-exception v1

    move-object v2, v4

    goto/16 :goto_f

    :cond_34
    aget v5, v1, v4

    aget v7, v3, v4

    xor-int/2addr v5, v7

    aput v5, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_10

    :cond_35
    aget v5, v3, v4

    int-to-char v5, v5

    aput-char v5, v1, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_11

    :cond_36
    aget v5, v1, v4

    aget v7, v3, v4

    xor-int/2addr v5, v7

    aput v5, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_12

    :cond_37
    aget v5, v3, v4

    int-to-char v5, v5

    aput-char v5, v1, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_13

    :cond_38
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mHeaderLayout:Landroid/widget/LinearLayout;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :try_start_b
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_b
    .catch Ljava/lang/NullPointerException; {:try_start_b .. :try_end_b} :catch_a

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->headerText:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_14

    :catch_5
    move-exception v1

    :cond_39
    :goto_2b
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mContext:Landroid/content/Context;

    :try_start_c
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_c
    .catch Ljava/lang/NullPointerException; {:try_start_c .. :try_end_c} :catch_c

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$id;->subtabs_rd_grp:I

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioGroup;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mSubTabsGrp:Landroid/widget/RadioGroup;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mSubTabsGrp:Landroid/widget/RadioGroup;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/RadioGroup;->setVisibility(I)V

    goto/16 :goto_15

    :catch_6
    move-exception v1

    goto/16 :goto_2

    :catch_7
    move-exception v1

    goto/16 :goto_3

    :catch_8
    move-exception v1

    goto/16 :goto_4

    :catch_9
    move-exception v1

    goto/16 :goto_6

    :catch_a
    move-exception v1

    goto/16 :goto_14

    :catch_b
    move-exception v1

    goto :goto_2b

    :catch_c
    move-exception v1

    goto/16 :goto_15

    :cond_3a
    move-object v2, v4

    goto/16 :goto_f
.end method

.method public onDestroy()V
    .locals 41

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->isScanning:Z

    if-eqz v1, :cond_0

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->stopDiscoveringSensorDevices()V

    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->clearController()V

    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0x501e

    aput v12, v2, v11

    const/16 v11, -0x70cd

    aput v11, v2, v10

    const/16 v10, -0x1a

    aput v10, v2, v9

    const/16 v9, -0x55

    aput v9, v2, v8

    const/16 v8, 0xd57

    aput v8, v2, v7

    const/16 v7, 0x7a68

    aput v7, v2, v6

    const/16 v6, 0x7429

    aput v6, v2, v5

    const/16 v5, 0x7f16

    aput v5, v2, v4

    const/16 v4, -0x5ea

    aput v4, v2, v3

    const/16 v3, -0x6a

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0x507b

    aput v13, v1, v12

    const/16 v12, -0x70b0

    aput v12, v1, v11

    const/16 v11, -0x71

    aput v11, v1, v10

    const/16 v10, -0x23

    aput v10, v1, v9

    const/16 v9, 0xd25

    aput v9, v1, v8

    const/16 v8, 0x7a0d

    aput v8, v1, v7

    const/16 v7, 0x747a

    aput v7, v1, v6

    const/16 v6, 0x7f74

    aput v6, v1, v5

    const/16 v5, -0x581

    aput v5, v1, v4

    const/4 v4, -0x6

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v4, v1

    if-lt v3, v4, :cond_2

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v4, v1

    if-lt v3, v4, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    const/16 v1, 0x1f

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, 0x1d

    const/16 v33, 0x1e

    const/16 v34, 0x1a7e

    aput v34, v2, v33

    const/16 v33, 0x463b

    aput v33, v2, v32

    const/16 v32, -0x7b9a

    aput v32, v2, v31

    const/16 v31, -0xa

    aput v31, v2, v30

    const/16 v30, -0x638a

    aput v30, v2, v29

    const/16 v29, -0x11

    aput v29, v2, v28

    const/16 v28, -0x16

    aput v28, v2, v27

    const/16 v27, 0x37d

    aput v27, v2, v26

    const/16 v26, -0x290

    aput v26, v2, v25

    const/16 v25, -0x23

    aput v25, v2, v24

    const/16 v24, -0x3b

    aput v24, v2, v23

    const/16 v23, -0x7f

    aput v23, v2, v22

    const/16 v22, -0x5fb9

    aput v22, v2, v21

    const/16 v21, -0x3f

    aput v21, v2, v20

    const/16 v20, 0x765a

    aput v20, v2, v19

    const/16 v19, 0x471e

    aput v19, v2, v18

    const/16 v18, -0x2099

    aput v18, v2, v17

    const/16 v17, -0x48

    aput v17, v2, v16

    const/16 v16, 0x254c

    aput v16, v2, v15

    const/16 v15, -0x11b4

    aput v15, v2, v14

    const/16 v14, -0x63

    aput v14, v2, v13

    const/16 v13, -0x79

    aput v13, v2, v12

    const/16 v12, -0x10cd

    aput v12, v2, v11

    const/16 v11, -0x74

    aput v11, v2, v10

    const/16 v10, -0x52cf

    aput v10, v2, v9

    const/16 v9, -0x21

    aput v9, v2, v8

    const/16 v8, -0x66

    aput v8, v2, v7

    const/16 v7, -0x1eb8

    aput v7, v2, v6

    const/16 v6, -0x79

    aput v6, v2, v5

    const/16 v5, 0x1607

    aput v5, v2, v3

    const/16 v3, -0x64ca

    aput v3, v2, v1

    const/16 v1, 0x1f

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, 0x1d

    const/16 v34, 0x1e

    const/16 v35, 0x1a5f

    aput v35, v1, v34

    const/16 v34, 0x461a

    aput v34, v1, v33

    const/16 v33, -0x7bba

    aput v33, v1, v32

    const/16 v32, -0x7c

    aput v32, v1, v31

    const/16 v31, -0x63e7

    aput v31, v1, v30

    const/16 v30, -0x64

    aput v30, v1, v29

    const/16 v29, -0x7c

    aput v29, v1, v28

    const/16 v28, 0x318

    aput v28, v1, v27

    const/16 v27, -0x2fd

    aput v27, v1, v26

    const/16 v26, -0x3

    aput v26, v1, v25

    const/16 v25, -0x53

    aput v25, v1, v24

    const/16 v24, -0xb

    aput v24, v1, v23

    const/16 v23, -0x5fd5

    aput v23, v1, v22

    const/16 v22, -0x60

    aput v22, v1, v21

    const/16 v21, 0x763f

    aput v21, v1, v20

    const/16 v20, 0x4776

    aput v20, v1, v19

    const/16 v19, -0x20b9

    aput v19, v1, v18

    const/16 v18, -0x21

    aput v18, v1, v17

    const/16 v17, 0x2522

    aput v17, v1, v16

    const/16 v16, -0x11db

    aput v16, v1, v15

    const/16 v15, -0x12

    aput v15, v1, v14

    const/16 v14, -0x18

    aput v14, v1, v13

    const/16 v13, -0x10a1

    aput v13, v1, v12

    const/16 v12, -0x11

    aput v12, v1, v11

    const/16 v11, -0x52ef

    aput v11, v1, v10

    const/16 v10, -0x53

    aput v10, v1, v9

    const/4 v9, -0x1

    aput v9, v1, v8

    const/16 v8, -0x1ec4

    aput v8, v1, v7

    const/16 v7, -0x1f

    aput v7, v1, v6

    const/16 v6, 0x1646

    aput v6, v1, v5

    const/16 v5, -0x64ea

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v5, v1

    if-lt v3, v5, :cond_4

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v5, v1

    if-lt v3, v5, :cond_5

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->wearableContentObserver:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$WearableStatusObserver;

    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->wearableContentObserver:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$WearableStatusObserver;

    invoke-virtual {v1, v2}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->wearableContentObserver:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$WearableStatusObserver;

    :goto_4
    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, -0x3bce

    aput v12, v2, v11

    const/16 v11, -0x59

    aput v11, v2, v10

    const/16 v10, -0x1dfa

    aput v10, v2, v9

    const/16 v9, -0x6c

    aput v9, v2, v8

    const/16 v8, 0x3f00

    aput v8, v2, v7

    const/16 v7, 0x395a

    aput v7, v2, v6

    const/16 v6, -0x2096

    aput v6, v2, v5

    const/16 v5, -0x43

    aput v5, v2, v4

    const/16 v4, 0x772b

    aput v4, v2, v3

    const/16 v3, -0x30e5

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, -0x3ba9

    aput v13, v1, v12

    const/16 v12, -0x3c

    aput v12, v1, v11

    const/16 v11, -0x1d91

    aput v11, v1, v10

    const/16 v10, -0x1e

    aput v10, v1, v9

    const/16 v9, 0x3f72

    aput v9, v1, v8

    const/16 v8, 0x393f

    aput v8, v1, v7

    const/16 v7, -0x20c7

    aput v7, v1, v6

    const/16 v6, -0x21

    aput v6, v1, v5

    const/16 v5, 0x7742

    aput v5, v1, v4

    const/16 v4, -0x3089

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_5
    array-length v4, v1

    if-lt v3, v4, :cond_6

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_6
    array-length v4, v1

    if-lt v3, v4, :cond_7

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    const/16 v1, 0x24

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, 0x1d

    const/16 v33, 0x1e

    const/16 v34, 0x1f

    const/16 v35, 0x20

    const/16 v36, 0x21

    const/16 v37, 0x22

    const/16 v38, 0x23

    const/16 v39, -0x23d0

    aput v39, v2, v38

    const/16 v38, -0x3

    aput v38, v2, v37

    const/16 v37, -0x3398

    aput v37, v2, v36

    const/16 v36, -0x14

    aput v36, v2, v35

    const/16 v35, -0x69ab

    aput v35, v2, v34

    const/16 v34, -0xb

    aput v34, v2, v33

    const/16 v33, 0x3130

    aput v33, v2, v32

    const/16 v32, 0x2747

    aput v32, v2, v31

    const/16 v31, -0x6cab

    aput v31, v2, v30

    const/16 v30, -0xa

    aput v30, v2, v29

    const/16 v29, -0x65be

    aput v29, v2, v28

    const/16 v28, -0x46

    aput v28, v2, v27

    const/16 v27, -0x56

    aput v27, v2, v26

    const/16 v26, -0x8cf

    aput v26, v2, v25

    const/16 v25, -0x66

    aput v25, v2, v24

    const/16 v24, 0x481b

    aput v24, v2, v23

    const/16 v23, 0x3c27

    aput v23, v2, v22

    const/16 v22, -0x38a1

    aput v22, v2, v21

    const/16 v21, -0x19

    aput v21, v2, v20

    const/16 v20, -0x62

    aput v20, v2, v19

    const/16 v19, -0x19

    aput v19, v2, v18

    const/16 v18, -0x72

    aput v18, v2, v17

    const/16 v17, -0x1492

    aput v17, v2, v16

    const/16 v16, -0x7b

    aput v16, v2, v15

    const/16 v15, -0xe

    aput v15, v2, v14

    const/16 v14, -0x80

    aput v14, v2, v13

    const/16 v13, 0x2f19

    aput v13, v2, v12

    const/16 v12, -0x50a6

    aput v12, v2, v11

    const/16 v11, -0x71

    aput v11, v2, v10

    const/16 v10, -0x182

    aput v10, v2, v9

    const/16 v9, -0x74

    aput v9, v2, v8

    const/16 v8, -0x20d2

    aput v8, v2, v7

    const/16 v7, -0x47

    aput v7, v2, v6

    const/16 v6, 0xa78

    aput v6, v2, v5

    const/16 v5, 0x7b68

    aput v5, v2, v3

    const/16 v3, -0x25a5

    aput v3, v2, v1

    const/16 v1, 0x24

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, 0x1d

    const/16 v34, 0x1e

    const/16 v35, 0x1f

    const/16 v36, 0x20

    const/16 v37, 0x21

    const/16 v38, 0x22

    const/16 v39, 0x23

    const/16 v40, -0x23ef

    aput v40, v1, v39

    const/16 v39, -0x24

    aput v39, v1, v38

    const/16 v38, -0x33b8

    aput v38, v1, v37

    const/16 v37, -0x34

    aput v37, v1, v36

    const/16 v36, -0x69d0

    aput v36, v1, v35

    const/16 v35, -0x6a

    aput v35, v1, v34

    const/16 v34, 0x3159

    aput v34, v1, v33

    const/16 v33, 0x2731

    aput v33, v1, v32

    const/16 v32, -0x6cd9

    aput v32, v1, v31

    const/16 v31, -0x6d

    aput v31, v1, v30

    const/16 v30, -0x65cf

    aput v30, v1, v29

    const/16 v29, -0x66

    aput v29, v1, v28

    const/16 v28, -0x3c

    aput v28, v1, v27

    const/16 v27, -0x8a2

    aput v27, v1, v26

    const/16 v26, -0x9

    aput v26, v1, v25

    const/16 v25, 0x4876

    aput v25, v1, v24

    const/16 v24, 0x3c48

    aput v24, v1, v23

    const/16 v23, -0x38c4

    aput v23, v1, v22

    const/16 v22, -0x39

    aput v22, v1, v21

    const/16 v21, -0x7

    aput v21, v1, v20

    const/16 v20, -0x77

    aput v20, v1, v19

    const/16 v19, -0x19

    aput v19, v1, v18

    const/16 v18, -0x14f6

    aput v18, v1, v17

    const/16 v17, -0x15

    aput v17, v1, v16

    const/16 v16, -0x65

    aput v16, v1, v15

    const/16 v15, -0x1e

    aput v15, v1, v14

    const/16 v14, 0x2f77

    aput v14, v1, v13

    const/16 v13, -0x50d1

    aput v13, v1, v12

    const/16 v12, -0x51

    aput v12, v1, v11

    const/16 v11, -0x1e5

    aput v11, v1, v10

    const/4 v10, -0x2

    aput v10, v1, v9

    const/16 v9, -0x20bf

    aput v9, v1, v8

    const/16 v8, -0x21

    aput v8, v1, v7

    const/16 v7, 0xa1d

    aput v7, v1, v6

    const/16 v6, 0x7b0a

    aput v6, v1, v5

    const/16 v5, -0x2585

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_7
    array-length v5, v1

    if-lt v3, v5, :cond_8

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_8
    array-length v5, v1

    if-lt v3, v5, :cond_9

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0x2546

    aput v12, v2, v11

    const/16 v11, -0xaba

    aput v11, v2, v10

    const/16 v10, -0x64

    aput v10, v2, v9

    const/16 v9, 0x2970

    aput v9, v2, v8

    const/16 v8, -0x3ca5

    aput v8, v2, v7

    const/16 v7, -0x5a

    aput v7, v2, v6

    const/16 v6, 0x7527

    aput v6, v2, v5

    const/16 v5, -0x66e9

    aput v5, v2, v4

    const/16 v4, -0x10

    aput v4, v2, v3

    const/16 v3, -0x3c

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0x2523

    aput v13, v1, v12

    const/16 v12, -0xadb

    aput v12, v1, v11

    const/16 v11, -0xb

    aput v11, v1, v10

    const/16 v10, 0x2906

    aput v10, v1, v9

    const/16 v9, -0x3cd7

    aput v9, v1, v8

    const/16 v8, -0x3d

    aput v8, v1, v7

    const/16 v7, 0x7574

    aput v7, v1, v6

    const/16 v6, -0x668b

    aput v6, v1, v5

    const/16 v5, -0x67

    aput v5, v1, v4

    const/16 v4, -0x58

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_9
    array-length v4, v1

    if-lt v3, v4, :cond_a

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_a
    array-length v4, v1

    if-lt v3, v4, :cond_b

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    const/16 v1, 0x23

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, 0x1d

    const/16 v33, 0x1e

    const/16 v34, 0x1f

    const/16 v35, 0x20

    const/16 v36, 0x21

    const/16 v37, 0x22

    const/16 v38, -0x3195

    aput v38, v2, v37

    const/16 v37, -0x11

    aput v37, v2, v36

    const/16 v36, -0x4

    aput v36, v2, v35

    const/16 v35, -0x25

    aput v35, v2, v34

    const/16 v34, -0x2

    aput v34, v2, v33

    const/16 v33, -0x2dd

    aput v33, v2, v32

    const/16 v32, -0x6c

    aput v32, v2, v31

    const/16 v31, 0x4d22

    aput v31, v2, v30

    const/16 v30, -0x73c1

    aput v30, v2, v29

    const/16 v29, -0x17

    aput v29, v2, v28

    const/16 v28, -0x38ed

    aput v28, v2, v27

    const/16 v27, -0x19

    aput v27, v2, v26

    const/16 v26, -0x40

    aput v26, v2, v25

    const/16 v25, 0x316f

    aput v25, v2, v24

    const/16 v24, 0x445c

    aput v24, v2, v23

    const/16 v23, 0x7a29

    aput v23, v2, v22

    const/16 v22, 0x2a15

    aput v22, v2, v21

    const/16 v21, -0x3db7

    aput v21, v2, v20

    const/16 v20, -0x1e

    aput v20, v2, v19

    const/16 v19, 0x152e

    aput v19, v2, v18

    const/16 v18, -0x4a85

    aput v18, v2, v17

    const/16 v17, -0x24

    aput v17, v2, v16

    const/16 v16, -0x990

    aput v16, v2, v15

    const/16 v15, -0x68

    aput v15, v2, v14

    const/16 v14, -0x21

    aput v14, v2, v13

    const/16 v13, -0x6d

    aput v13, v2, v12

    const/16 v12, -0x53

    aput v12, v2, v11

    const/16 v11, -0x45

    aput v11, v2, v10

    const/4 v10, -0x2

    aput v10, v2, v9

    const/16 v9, -0x30

    aput v9, v2, v8

    const/16 v8, -0x891

    aput v8, v2, v7

    const/16 v7, -0x7d

    aput v7, v2, v6

    const/16 v6, 0x6615

    aput v6, v2, v5

    const/16 v5, 0x1827

    aput v5, v2, v3

    const/16 v3, 0x6f38

    aput v3, v2, v1

    const/16 v1, 0x23

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, 0x1d

    const/16 v34, 0x1e

    const/16 v35, 0x1f

    const/16 v36, 0x20

    const/16 v37, 0x21

    const/16 v38, 0x22

    const/16 v39, -0x31b6

    aput v39, v1, v38

    const/16 v38, -0x32

    aput v38, v1, v37

    const/16 v37, -0x24

    aput v37, v1, v36

    const/16 v36, -0x5

    aput v36, v1, v35

    const/16 v35, -0x65

    aput v35, v1, v34

    const/16 v34, -0x2c0

    aput v34, v1, v33

    const/16 v33, -0x3

    aput v33, v1, v32

    const/16 v32, 0x4d54

    aput v32, v1, v31

    const/16 v31, -0x73b3

    aput v31, v1, v30

    const/16 v30, -0x74

    aput v30, v1, v29

    const/16 v29, -0x38a0

    aput v29, v1, v28

    const/16 v28, -0x39

    aput v28, v1, v27

    const/16 v27, -0x52

    aput v27, v1, v26

    const/16 v26, 0x3100

    aput v26, v1, v25

    const/16 v25, 0x4431

    aput v25, v1, v24

    const/16 v24, 0x7a44

    aput v24, v1, v23

    const/16 v23, 0x2a7a

    aput v23, v1, v22

    const/16 v22, -0x3dd6

    aput v22, v1, v21

    const/16 v21, -0x3e

    aput v21, v1, v20

    const/16 v20, 0x1549

    aput v20, v1, v19

    const/16 v19, -0x4aeb

    aput v19, v1, v18

    const/16 v18, -0x4b

    aput v18, v1, v17

    const/16 v17, -0x9ec

    aput v17, v1, v16

    const/16 v16, -0xa

    aput v16, v1, v15

    const/16 v15, -0x4a

    aput v15, v1, v14

    const/16 v14, -0xf

    aput v14, v1, v13

    const/16 v13, -0x3d

    aput v13, v1, v12

    const/16 v12, -0x32

    aput v12, v1, v11

    const/16 v11, -0x22

    aput v11, v1, v10

    const/16 v10, -0x5e

    aput v10, v1, v9

    const/16 v9, -0x8f6

    aput v9, v1, v8

    const/16 v8, -0x9

    aput v8, v1, v7

    const/16 v7, 0x6673

    aput v7, v1, v6

    const/16 v6, 0x1866

    aput v6, v1, v5

    const/16 v5, 0x6f18

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_b
    array-length v5, v1

    if-lt v3, v5, :cond_c

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_c
    array-length v5, v1

    if-lt v3, v5, :cond_d

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->isFragmetRunning:Z

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mPairedDeviceMap:Ljava/util/concurrent/ConcurrentHashMap;

    :try_start_2
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mPairedDeviceMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    :goto_d
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->dialog:Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;

    :try_start_3
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_3

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->dialog:Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->dialog:Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;->cancel()V

    :cond_1
    :goto_e
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->handler:Landroid/os/Handler;

    :try_start_4
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_4
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_4} :catch_4

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->handler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    :goto_f
    invoke-super/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onDestroy()V

    return-void

    :cond_2
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_3
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_4
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    :cond_5
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3

    :cond_6
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_5

    :cond_7
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_6

    :cond_8
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_7

    :cond_9
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_8

    :cond_a
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_9

    :cond_b
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_a

    :cond_c
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_b

    :cond_d
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_c

    :catch_0
    move-exception v1

    goto/16 :goto_4

    :catch_1
    move-exception v1

    goto/16 :goto_4

    :catch_2
    move-exception v1

    goto/16 :goto_d

    :catch_3
    move-exception v1

    goto/16 :goto_e

    :catch_4
    move-exception v1

    goto/16 :goto_f
.end method

.method public onPause()V
    .locals 0

    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onPause()V

    return-void
.end method

.method public onResume()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->startServiceIfNeeded()V

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->IsAutoScanRequired:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->IsAutoScanRequired:Z

    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->refreshPairedList:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->requestPairedDevices()V

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->refreshPairedList:Z

    :cond_1
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->IsScreenLocked:Z

    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onResume()V

    return-void
.end method

.method public onStop()V
    .locals 2

    const/4 v1, 0x1

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->isScanning:Z

    if-eqz v0, :cond_0

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->IsScreenLocked:Z

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->stopSearch()V

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->stopDiscoveringSensorDevices()V

    :cond_0
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->refreshPairedList:Z

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->IsAutoScanRequired:Z

    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onStop()V

    return-void
.end method

.method public requestPairedDevices()V
    .locals 51

    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, -0x7fff

    aput v12, v2, v11

    const/16 v11, -0x1d

    aput v11, v2, v10

    const/4 v10, -0x6

    aput v10, v2, v9

    const/16 v9, 0x3e3c

    aput v9, v2, v8

    const/16 v8, 0x4b4c

    aput v8, v2, v7

    const/16 v7, 0xd2e

    aput v7, v2, v6

    const/16 v6, 0x365e

    aput v6, v2, v5

    const/16 v5, 0xe54

    aput v5, v2, v4

    const/16 v4, -0x1f99

    aput v4, v2, v3

    const/16 v3, -0x74

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, -0x7f9c

    aput v13, v1, v12

    const/16 v12, -0x80

    aput v12, v1, v11

    const/16 v11, -0x6d

    aput v11, v1, v10

    const/16 v10, 0x3e4a

    aput v10, v1, v9

    const/16 v9, 0x4b3e

    aput v9, v1, v8

    const/16 v8, 0xd4b

    aput v8, v1, v7

    const/16 v7, 0x360d

    aput v7, v1, v6

    const/16 v6, 0xe36

    aput v6, v1, v5

    const/16 v5, -0x1ff2

    aput v5, v1, v4

    const/16 v4, -0x20

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v4, v1

    if-lt v3, v4, :cond_2

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v4, v1

    if-lt v3, v4, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    const/16 v1, 0x1b

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x6b0a

    aput v30, v2, v29

    const/16 v29, 0x610e

    aput v29, v2, v28

    const/16 v28, 0x2b0d

    aput v28, v2, v27

    const/16 v27, 0x6947

    aput v27, v2, v26

    const/16 v26, 0x3308

    aput v26, v2, v25

    const/16 v25, 0x7150

    aput v25, v2, v24

    const/16 v24, 0xe51

    aput v24, v2, v23

    const/16 v23, 0x1e7d

    aput v23, v2, v22

    const/16 v22, 0x567b

    aput v22, v2, v21

    const/16 v21, -0x3dcb

    aput v21, v2, v20

    const/16 v20, -0x55

    aput v20, v2, v19

    const/16 v19, -0x4a

    aput v19, v2, v18

    const/16 v18, -0x1a

    aput v18, v2, v17

    const/16 v17, -0x75c5

    aput v17, v2, v16

    const/16 v16, -0x12

    aput v16, v2, v15

    const/16 v15, -0x6f

    aput v15, v2, v14

    const/16 v14, 0x2457

    aput v14, v2, v13

    const/16 v13, 0x5f4d

    aput v13, v2, v12

    const/16 v12, 0x453e

    aput v12, v2, v11

    const/16 v11, -0x37eb

    aput v11, v2, v10

    const/16 v10, -0x44

    aput v10, v2, v9

    const/16 v9, 0x4a4f

    aput v9, v2, v8

    const/16 v8, -0x70d1

    aput v8, v2, v7

    const/4 v7, -0x6

    aput v7, v2, v6

    const/16 v6, -0x996

    aput v6, v2, v5

    const/16 v5, -0x6d

    aput v5, v2, v3

    const/16 v3, -0x49bc

    aput v3, v2, v1

    const/16 v1, 0x1b

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x6b6e

    aput v31, v1, v30

    const/16 v30, 0x616b

    aput v30, v1, v29

    const/16 v29, 0x2b61

    aput v29, v1, v28

    const/16 v28, 0x692b

    aput v28, v1, v27

    const/16 v27, 0x3369

    aput v27, v1, v26

    const/16 v26, 0x7133

    aput v26, v1, v25

    const/16 v25, 0xe71

    aput v25, v1, v24

    const/16 v24, 0x1e0e

    aput v24, v1, v23

    const/16 v23, 0x561e

    aput v23, v1, v22

    const/16 v22, -0x3daa

    aput v22, v1, v21

    const/16 v21, -0x3e

    aput v21, v1, v20

    const/16 v20, -0x40

    aput v20, v1, v19

    const/16 v19, -0x7d

    aput v19, v1, v18

    const/16 v18, -0x7581

    aput v18, v1, v17

    const/16 v17, -0x76

    aput v17, v1, v16

    const/16 v16, -0xc

    aput v16, v1, v15

    const/16 v15, 0x2425

    aput v15, v1, v14

    const/16 v14, 0x5f24

    aput v14, v1, v13

    const/16 v13, 0x455f

    aput v13, v1, v12

    const/16 v12, -0x37bb

    aput v12, v1, v11

    const/16 v11, -0x38

    aput v11, v1, v10

    const/16 v10, 0x4a3c

    aput v10, v1, v9

    const/16 v9, -0x70b6

    aput v9, v1, v8

    const/16 v8, -0x71

    aput v8, v1, v7

    const/16 v7, -0x9e5

    aput v7, v1, v6

    const/16 v6, -0xa

    aput v6, v1, v5

    const/16 v5, -0x49ca

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v5, v1

    if-lt v3, v5, :cond_4

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v5, v1

    if-lt v3, v5, :cond_5

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, -0x2a

    aput v12, v2, v11

    const/16 v11, -0x74

    aput v11, v2, v10

    const/16 v10, -0x7ff9

    aput v10, v2, v9

    const/16 v9, -0xa

    aput v9, v2, v8

    const/16 v8, -0xac3

    aput v8, v2, v7

    const/16 v7, -0x70

    aput v7, v2, v6

    const/16 v6, 0x3778

    aput v6, v2, v5

    const/16 v5, -0x6fab

    aput v5, v2, v4

    const/4 v4, -0x7

    aput v4, v2, v3

    const/16 v3, 0x110b

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, -0x4d

    aput v13, v1, v12

    const/16 v12, -0x11

    aput v12, v1, v11

    const/16 v11, -0x7f92

    aput v11, v1, v10

    const/16 v10, -0x80

    aput v10, v1, v9

    const/16 v9, -0xab1

    aput v9, v1, v8

    const/16 v8, -0xb

    aput v8, v1, v7

    const/16 v7, 0x372b

    aput v7, v1, v6

    const/16 v6, -0x6fc9

    aput v6, v1, v5

    const/16 v5, -0x70

    aput v5, v1, v4

    const/16 v4, 0x1167

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_4
    array-length v4, v1

    if-lt v3, v4, :cond_6

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_5
    array-length v4, v1

    if-lt v3, v4, :cond_7

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    const/16 v1, 0x2e

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, 0x1d

    const/16 v33, 0x1e

    const/16 v34, 0x1f

    const/16 v35, 0x20

    const/16 v36, 0x21

    const/16 v37, 0x22

    const/16 v38, 0x23

    const/16 v39, 0x24

    const/16 v40, 0x25

    const/16 v41, 0x26

    const/16 v42, 0x27

    const/16 v43, 0x28

    const/16 v44, 0x29

    const/16 v45, 0x2a

    const/16 v46, 0x2b

    const/16 v47, 0x2c

    const/16 v48, 0x2d

    const/16 v49, -0x2

    aput v49, v2, v48

    const/16 v48, 0x1b70

    aput v48, v2, v47

    const/16 v47, 0x2768

    aput v47, v2, v46

    const/16 v46, 0x364e

    aput v46, v2, v45

    const/16 v45, 0x5b7a

    aput v45, v2, v44

    const/16 v44, -0x40c2

    aput v44, v2, v43

    const/16 v43, -0x24

    aput v43, v2, v42

    const/16 v42, 0x3928

    aput v42, v2, v41

    const/16 v41, -0x49b1

    aput v41, v2, v40

    const/16 v40, -0x2d

    aput v40, v2, v39

    const/16 v39, -0x5eba

    aput v39, v2, v38

    const/16 v38, -0x3b

    aput v38, v2, v37

    const/16 v37, -0x2e

    aput v37, v2, v36

    const/16 v36, -0x4

    aput v36, v2, v35

    const/16 v35, 0x5631

    aput v35, v2, v34

    const/16 v34, -0x17c9

    aput v34, v2, v33

    const/16 v33, -0x48

    aput v33, v2, v32

    const/16 v32, 0x7d23

    aput v32, v2, v31

    const/16 v31, 0x115d

    aput v31, v2, v30

    const/16 v30, -0x42c9

    aput v30, v2, v29

    const/16 v29, -0x63

    aput v29, v2, v28

    const/16 v28, 0x4f6b

    aput v28, v2, v27

    const/16 v27, 0x5f2e

    aput v27, v2, v26

    const/16 v26, 0x5612

    aput v26, v2, v25

    const/16 v25, 0x3c33

    aput v25, v2, v24

    const/16 v24, -0x17a1

    aput v24, v2, v23

    const/16 v23, -0x7f

    aput v23, v2, v22

    const/16 v22, -0x3a

    aput v22, v2, v21

    const/16 v21, 0x24a

    aput v21, v2, v20

    const/16 v20, 0x6a46

    aput v20, v2, v19

    const/16 v19, 0xf0e

    aput v19, v2, v18

    const/16 v18, -0x3496

    aput v18, v2, v17

    const/16 v17, -0x47

    aput v17, v2, v16

    const/16 v16, -0xf

    aput v16, v2, v15

    const/16 v15, -0x5ca0

    aput v15, v2, v14

    const/16 v14, -0xd

    aput v14, v2, v13

    const/16 v13, -0xdef

    aput v13, v2, v12

    const/16 v12, -0x2e

    aput v12, v2, v11

    const/16 v11, -0x1e

    aput v11, v2, v10

    const/16 v10, -0x3fca

    aput v10, v2, v9

    const/16 v9, -0x57

    aput v9, v2, v8

    const/16 v8, -0x72

    aput v8, v2, v7

    const/16 v7, 0x7362

    aput v7, v2, v6

    const/16 v6, 0xf16

    aput v6, v2, v5

    const/16 v5, -0x2a9d

    aput v5, v2, v3

    const/16 v3, -0x4a

    aput v3, v2, v1

    const/16 v1, 0x2e

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, 0x1d

    const/16 v34, 0x1e

    const/16 v35, 0x1f

    const/16 v36, 0x20

    const/16 v37, 0x21

    const/16 v38, 0x22

    const/16 v39, 0x23

    const/16 v40, 0x24

    const/16 v41, 0x25

    const/16 v42, 0x26

    const/16 v43, 0x27

    const/16 v44, 0x28

    const/16 v45, 0x29

    const/16 v46, 0x2a

    const/16 v47, 0x2b

    const/16 v48, 0x2c

    const/16 v49, 0x2d

    const/16 v50, -0x22

    aput v50, v1, v49

    const/16 v49, 0x1b04

    aput v49, v1, v48

    const/16 v48, 0x271b

    aput v48, v1, v47

    const/16 v47, 0x3627

    aput v47, v1, v46

    const/16 v46, 0x5b36

    aput v46, v1, v45

    const/16 v45, -0x40a5

    aput v45, v1, v44

    const/16 v44, -0x41

    aput v44, v1, v43

    const/16 v43, 0x3941

    aput v43, v1, v42

    const/16 v42, -0x49c7

    aput v42, v1, v41

    const/16 v41, -0x4a

    aput v41, v1, v40

    const/16 v40, -0x5efe

    aput v40, v1, v39

    const/16 v39, -0x5f

    aput v39, v1, v38

    const/16 v38, -0x49

    aput v38, v1, v37

    const/16 v37, -0x72

    aput v37, v1, v36

    const/16 v36, 0x5658

    aput v36, v1, v35

    const/16 v35, -0x17aa

    aput v35, v1, v34

    const/16 v34, -0x18

    aput v34, v1, v33

    const/16 v33, 0x7d4e

    aput v33, v1, v32

    const/16 v32, 0x117d

    aput v32, v1, v31

    const/16 v31, -0x42ef

    aput v31, v1, v30

    const/16 v30, -0x43

    aput v30, v1, v29

    const/16 v29, 0x4f1b

    aput v29, v1, v28

    const/16 v28, 0x5f4f

    aput v28, v1, v27

    const/16 v27, 0x565f

    aput v27, v1, v26

    const/16 v26, 0x3c56

    aput v26, v1, v25

    const/16 v25, -0x17c4

    aput v25, v1, v24

    const/16 v24, -0x18

    aput v24, v1, v23

    const/16 v23, -0x50

    aput v23, v1, v22

    const/16 v22, 0x22f

    aput v22, v1, v21

    const/16 v21, 0x6a02

    aput v21, v1, v20

    const/16 v20, 0xf6a

    aput v20, v1, v19

    const/16 v19, -0x34f1

    aput v19, v1, v18

    const/16 v18, -0x35

    aput v18, v1, v17

    const/16 v17, -0x68

    aput v17, v1, v16

    const/16 v16, -0x5cff

    aput v16, v1, v15

    const/16 v15, -0x5d

    aput v15, v1, v14

    const/16 v14, -0xd84

    aput v14, v1, v13

    const/16 v13, -0xe

    aput v13, v1, v12

    const/16 v12, -0x7b

    aput v12, v1, v11

    const/16 v11, -0x3fa8

    aput v11, v1, v10

    const/16 v10, -0x40

    aput v10, v1, v9

    const/4 v9, -0x4

    aput v9, v1, v8

    const/16 v8, 0x7303

    aput v8, v1, v7

    const/16 v7, 0xf73

    aput v7, v1, v6

    const/16 v6, -0x2af1

    aput v6, v1, v5

    const/16 v5, -0x2b

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_6
    array-length v5, v1

    if-lt v3, v5, :cond_8

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_7
    array-length v5, v1

    if-lt v3, v5, :cond_9

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mPairedDeviceMap:Ljava/util/concurrent/ConcurrentHashMap;

    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mPairedDeviceMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    :goto_8
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mScanningFragmentController:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;

    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mScanningFragmentController:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mDataType:I

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->showCompanionDevices:Z

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mDeviceTypes:Ljava/util/ArrayList;

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;->requestPairedDevices(IZLjava/util/ArrayList;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mPairedDeviceMap:Ljava/util/concurrent/ConcurrentHashMap;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mPairedDeviceMap:Ljava/util/concurrent/ConcurrentHashMap;

    :try_start_2
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mPairedDeviceMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentHashMap;->size()I

    move-result v1

    if-lez v1, :cond_a

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->scanbutton:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->IsAutoScanRequired:Z

    if-nez v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->scanbutton:Landroid/widget/Button;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    :cond_0
    const/4 v1, 0x1

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->IsAutoScanRequired:Z

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mPairedDeviceMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->refreshDeviceData1(Ljava/util/Collection;)V

    :cond_1
    :goto_9
    return-void

    :cond_2
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_3
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_4
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    :cond_5
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3

    :cond_6
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_4

    :cond_7
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_5

    :cond_8
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_6

    :cond_9
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_7

    :catch_0
    move-exception v1

    :cond_a
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mIsNoSupportScan:Z

    if-nez v1, :cond_f

    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, -0x5a95

    aput v12, v2, v11

    const/16 v11, -0x3a

    aput v11, v2, v10

    const/16 v10, -0x49

    aput v10, v2, v9

    const/16 v9, 0x4575

    aput v9, v2, v8

    const/16 v8, -0x2fc9

    aput v8, v2, v7

    const/16 v7, -0x4b

    aput v7, v2, v6

    const/16 v6, 0x1803

    aput v6, v2, v5

    const/16 v5, -0x6386

    aput v5, v2, v4

    const/16 v4, -0xb

    aput v4, v2, v3

    const/16 v3, -0x80

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, -0x5af2

    aput v13, v1, v12

    const/16 v12, -0x5b

    aput v12, v1, v11

    const/16 v11, -0x22

    aput v11, v1, v10

    const/16 v10, 0x4503

    aput v10, v1, v9

    const/16 v9, -0x2fbb

    aput v9, v1, v8

    const/16 v8, -0x30

    aput v8, v1, v7

    const/16 v7, 0x1850

    aput v7, v1, v6

    const/16 v6, -0x63e8

    aput v6, v1, v5

    const/16 v5, -0x64

    aput v5, v1, v4

    const/16 v4, -0x14

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_a
    array-length v4, v1

    if-lt v3, v4, :cond_b

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_b
    array-length v4, v1

    if-lt v3, v4, :cond_c

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    const/16 v1, 0x19

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, -0x1c

    aput v28, v2, v27

    const/16 v27, -0x26

    aput v27, v2, v26

    const/16 v26, -0x59

    aput v26, v2, v25

    const/16 v25, -0x7e

    aput v25, v2, v24

    const/16 v24, -0x68

    aput v24, v2, v23

    const/16 v23, -0x4e6

    aput v23, v2, v22

    const/16 v22, -0x46

    aput v22, v2, v21

    const/16 v21, -0x6e

    aput v21, v2, v20

    const/16 v20, -0x6a

    aput v20, v2, v19

    const/16 v19, -0x3

    aput v19, v2, v18

    const/16 v18, -0x5ed4

    aput v18, v2, v17

    const/16 v17, -0x11

    aput v17, v2, v16

    const/16 v16, 0x4330

    aput v16, v2, v15

    const/16 v15, 0x5e0d

    aput v15, v2, v14

    const/16 v14, -0x3f0

    aput v14, v2, v13

    const/16 v13, -0x43

    aput v13, v2, v12

    const/16 v12, -0x6f

    aput v12, v2, v11

    const/16 v11, -0x69

    aput v11, v2, v10

    const/16 v10, -0x39

    aput v10, v2, v9

    const/16 v9, -0x4c

    aput v9, v2, v8

    const/16 v8, 0x4d4c

    aput v8, v2, v7

    const/16 v7, -0x67f4

    aput v7, v2, v6

    const/16 v6, -0x24

    aput v6, v2, v5

    const/16 v5, -0xbf8

    aput v5, v2, v3

    const/16 v3, -0x5f

    aput v3, v2, v1

    const/16 v1, 0x19

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, -0x3c

    aput v29, v1, v28

    const/16 v28, -0x72

    aput v28, v1, v27

    const/16 v27, -0x17

    aput v27, v1, v26

    const/16 v26, -0x39

    aput v26, v1, v25

    const/16 v25, -0x2b

    aput v25, v1, v24

    const/16 v24, -0x4a3

    aput v24, v1, v23

    const/16 v23, -0x5

    aput v23, v1, v22

    const/16 v22, -0x40

    aput v22, v1, v21

    const/16 v21, -0x30

    aput v21, v1, v20

    const/16 v20, -0x5e

    aput v20, v1, v19

    const/16 v19, -0x5e95

    aput v19, v1, v18

    const/16 v18, -0x5f

    aput v18, v1, v17

    const/16 v17, 0x4379

    aput v17, v1, v16

    const/16 v16, 0x5e43

    aput v16, v1, v15

    const/16 v15, -0x3a2

    aput v15, v1, v14

    const/4 v14, -0x4

    aput v14, v1, v13

    const/16 v13, -0x2e

    aput v13, v1, v12

    const/16 v12, -0x3c

    aput v12, v1, v11

    const/16 v11, -0x68

    aput v11, v1, v10

    const/16 v10, -0xf

    aput v10, v1, v9

    const/16 v9, 0x4d18

    aput v9, v1, v8

    const/16 v8, -0x67b3

    aput v8, v1, v7

    const/16 v7, -0x68

    aput v7, v1, v6

    const/16 v6, -0xba8

    aput v6, v1, v5

    const/16 v5, -0xc

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_c
    array-length v5, v1

    if-lt v3, v5, :cond_d

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_d
    array-length v5, v1

    if-lt v3, v5, :cond_e

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    :try_start_3
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_3

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$8;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$8;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)V

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    :goto_e
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->handler:Landroid/os/Handler;

    const/16 v2, 0x71

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_9

    :cond_b
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_a

    :cond_c
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_b

    :cond_d
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_c

    :cond_e
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_d

    :cond_f
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mIsNoSupportScan:Z

    if-eqz v1, :cond_10

    sget-boolean v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mUpdateScanStatus:Z

    if-eqz v1, :cond_10

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    :try_start_4
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_4
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_4} :catch_4

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$9;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$9;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)V

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto/16 :goto_9

    :cond_10
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mIsNoSupportScan:Z

    if-eqz v1, :cond_1

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    :try_start_5
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_5
    .catch Ljava/lang/NullPointerException; {:try_start_5 .. :try_end_5} :catch_5

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$10;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$10;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)V

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto/16 :goto_9

    :catch_1
    move-exception v1

    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, -0x1e

    aput v12, v2, v11

    const/16 v11, -0x7088

    aput v11, v2, v10

    const/16 v10, -0x1a

    aput v10, v2, v9

    const/16 v9, -0xd

    aput v9, v2, v8

    const/16 v8, -0x55

    aput v8, v2, v7

    const/16 v7, 0x7827

    aput v7, v2, v6

    const/16 v6, 0x422b

    aput v6, v2, v5

    const/16 v5, -0x68e0

    aput v5, v2, v4

    const/4 v4, -0x2

    aput v4, v2, v3

    const/16 v3, -0x42

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, -0x79

    aput v13, v1, v12

    const/16 v12, -0x70e5

    aput v12, v1, v11

    const/16 v11, -0x71

    aput v11, v1, v10

    const/16 v10, -0x7b

    aput v10, v1, v9

    const/16 v9, -0x27

    aput v9, v1, v8

    const/16 v8, 0x7842

    aput v8, v1, v7

    const/16 v7, 0x4278

    aput v7, v1, v6

    const/16 v6, -0x68be

    aput v6, v1, v5

    const/16 v5, -0x69

    aput v5, v1, v4

    const/16 v4, -0x2e

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_f
    array-length v4, v1

    if-lt v3, v4, :cond_11

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_10
    array-length v4, v1

    if-lt v3, v4, :cond_12

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    const/16 v1, 0x23

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, 0x1d

    const/16 v33, 0x1e

    const/16 v34, 0x1f

    const/16 v35, 0x20

    const/16 v36, 0x21

    const/16 v37, 0x22

    const/16 v38, -0x75ce

    aput v38, v2, v37

    const/16 v37, -0x1a

    aput v37, v2, v36

    const/16 v36, -0x5a

    aput v36, v2, v35

    const/16 v35, -0x54

    aput v35, v2, v34

    const/16 v34, -0x2eeb

    aput v34, v2, v33

    const/16 v33, -0x5e

    aput v33, v2, v32

    const/16 v32, -0x3296

    aput v32, v2, v31

    const/16 v31, -0x13

    aput v31, v2, v30

    const/16 v30, -0x75cc

    aput v30, v2, v29

    const/16 v29, -0x11

    aput v29, v2, v28

    const/16 v28, -0x28

    aput v28, v2, v27

    const/16 v27, 0x5d04

    aput v27, v2, v26

    const/16 v26, 0x3932

    aput v26, v2, v25

    const/16 v25, 0x1a4b

    aput v25, v2, v24

    const/16 v24, 0x666e

    aput v24, v2, v23

    const/16 v23, 0xd08

    aput v23, v2, v22

    const/16 v22, -0x639e

    aput v22, v2, v21

    const/16 v21, -0x21

    aput v21, v2, v20

    const/16 v20, 0x6450

    aput v20, v2, v19

    const/16 v19, -0x11f6

    aput v19, v2, v18

    const/16 v18, -0x75

    aput v18, v2, v17

    const/16 v17, 0x3221

    aput v17, v2, v16

    const/16 v16, -0x5fab

    aput v16, v2, v15

    const/16 v15, -0x3f

    aput v15, v2, v14

    const/16 v14, -0x5b9e

    aput v14, v2, v13

    const/16 v13, -0x1e

    aput v13, v2, v12

    const/4 v12, -0x4

    aput v12, v2, v11

    const/4 v11, -0x2

    aput v11, v2, v10

    const/16 v10, -0x13

    aput v10, v2, v9

    const/16 v9, 0x7404

    aput v9, v2, v8

    const/16 v8, 0x3f1a

    aput v8, v2, v7

    const/16 v7, 0x2d5e

    aput v7, v2, v6

    const/16 v6, -0x68b2

    aput v6, v2, v5

    const/16 v5, -0x3c

    aput v5, v2, v3

    const/16 v3, -0x80

    aput v3, v2, v1

    const/16 v1, 0x23

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, 0x1d

    const/16 v34, 0x1e

    const/16 v35, 0x1f

    const/16 v36, 0x20

    const/16 v37, 0x21

    const/16 v38, 0x22

    const/16 v39, -0x75a2

    aput v39, v1, v38

    const/16 v38, -0x76

    aput v38, v1, v37

    const/16 v37, -0x2d

    aput v37, v1, v36

    const/16 v36, -0x3e

    aput v36, v1, v35

    const/16 v35, -0x2ecb

    aput v35, v1, v34

    const/16 v34, -0x2f

    aput v34, v1, v33

    const/16 v33, -0x32fd

    aput v33, v1, v32

    const/16 v32, -0x33

    aput v32, v1, v31

    const/16 v31, -0x75ba

    aput v31, v1, v30

    const/16 v30, -0x76

    aput v30, v1, v29

    const/16 v29, -0x4c

    aput v29, v1, v28

    const/16 v28, 0x5d68

    aput v28, v1, v27

    const/16 v27, 0x395d

    aput v27, v1, v26

    const/16 v26, 0x1a39

    aput v26, v1, v25

    const/16 v25, 0x661a

    aput v25, v1, v24

    const/16 v24, 0xd66

    aput v24, v1, v23

    const/16 v23, -0x63f3

    aput v23, v1, v22

    const/16 v22, -0x64

    aput v22, v1, v21

    const/16 v21, 0x6424

    aput v21, v1, v20

    const/16 v20, -0x119c

    aput v20, v1, v19

    const/16 v19, -0x12

    aput v19, v1, v18

    const/16 v18, 0x324c

    aput v18, v1, v17

    const/16 v17, -0x5fce

    aput v17, v1, v16

    const/16 v16, -0x60

    aput v16, v1, v15

    const/16 v15, -0x5bf0

    aput v15, v1, v14

    const/16 v14, -0x5c

    aput v14, v1, v13

    const/16 v13, -0x65

    aput v13, v1, v12

    const/16 v12, -0x70

    aput v12, v1, v11

    const/16 v11, -0x7c

    aput v11, v1, v10

    const/16 v10, 0x746a

    aput v10, v1, v9

    const/16 v9, 0x3f74

    aput v9, v1, v8

    const/16 v8, 0x2d3f

    aput v8, v1, v7

    const/16 v7, -0x68d3

    aput v7, v1, v6

    const/16 v6, -0x69

    aput v6, v1, v5

    const/16 v5, -0x13

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_11
    array-length v5, v1

    if-lt v3, v5, :cond_13

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_12
    array-length v5, v1

    if-lt v3, v5, :cond_14

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_9

    :cond_11
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_f

    :cond_12
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_10

    :cond_13
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_11

    :cond_14
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_12

    :catch_2
    move-exception v1

    goto/16 :goto_8

    :catch_3
    move-exception v1

    goto/16 :goto_e

    :catch_4
    move-exception v1

    goto/16 :goto_9

    :catch_5
    move-exception v1

    goto/16 :goto_9
.end method

.method public settingInfo(Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;Ljava/lang/String;)V
    .locals 11

    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v3, 0x0

    :try_start_0
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x6

    new-array v1, v0, [I

    const/4 v0, 0x5

    const/16 v2, 0x7005

    aput v2, v1, v0

    const/16 v0, -0x6de7

    aput v0, v1, v10

    const/16 v0, -0xd

    aput v0, v1, v9

    const/16 v0, -0x15

    aput v0, v1, v8

    const/16 v0, -0x50

    aput v0, v1, v7

    const/16 v0, -0x7e

    aput v0, v1, v3

    const/4 v0, 0x6

    new-array v0, v0, [I

    const/4 v2, 0x5

    const/16 v4, 0x7077

    aput v4, v0, v2

    const/16 v2, -0x6d90

    aput v2, v0, v10

    const/16 v2, -0x6e

    aput v2, v0, v9

    const/16 v2, -0x45

    aput v2, v0, v8

    const/16 v2, -0x22

    aput v2, v0, v7

    const/16 v2, -0x29

    aput v2, v0, v3

    move v2, v3

    :goto_0
    array-length v4, v0

    if-lt v2, v4, :cond_1

    array-length v0, v1

    new-array v0, v0, [C

    move v2, v3

    :goto_1
    array-length v4, v0

    if-lt v2, v4, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    :try_start_1
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, v8}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$string;->disconnect_accessory:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$string;->s_will_be_disconnected:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v7, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getDeviceName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelable(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$13;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$13;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelButtonListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnCancelButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v5

    const/4 v0, 0x6

    new-array v1, v0, [I

    const/4 v0, 0x5

    const/16 v2, -0x30

    aput v2, v1, v0

    const/16 v0, 0x475e

    aput v0, v1, v10

    const/16 v0, -0x9da

    aput v0, v1, v9

    const/16 v0, -0x7a

    aput v0, v1, v8

    const/16 v0, -0x6bda

    aput v0, v1, v7

    const/16 v0, -0x3f

    aput v0, v1, v3

    const/4 v0, 0x6

    new-array v0, v0, [I

    const/4 v2, 0x5

    const/16 v6, -0x5e

    aput v6, v0, v2

    const/16 v2, 0x4737

    aput v2, v0, v10

    const/16 v2, -0x9b9

    aput v2, v0, v9

    const/16 v2, -0xa

    aput v2, v0, v8

    const/16 v2, -0x6bb8

    aput v2, v0, v7

    const/16 v2, -0x6c

    aput v2, v0, v3

    move v2, v3

    :goto_2
    array-length v6, v0

    if-lt v2, v6, :cond_3

    array-length v0, v1

    new-array v0, v0, [C

    :goto_3
    array-length v2, v0

    if-lt v3, v2, :cond_4

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v5, v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    :cond_0
    :goto_4
    return-void

    :cond_1
    aget v4, v0, v2

    aget v5, v1, v2

    xor-int/2addr v4, v5

    aput v4, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    :cond_2
    aget v4, v1, v2

    int-to-char v4, v4

    aput-char v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1

    :cond_3
    aget v6, v0, v2

    aget v7, v1, v2

    xor-int/2addr v6, v7

    aput v6, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_4
    aget v2, v1, v3

    int-to-char v2, v2

    aput-char v2, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :catch_0
    move-exception v0

    goto :goto_4

    :catch_1
    move-exception v0

    goto :goto_4
.end method

.method protected startSearch()V
    .locals 5

    const/16 v4, 0x8

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mScannedDevicesList:Ljava/util/ArrayList;

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mScannedDevicesList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->deviceMap:Ljava/util/concurrent/ConcurrentHashMap;

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->deviceMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mScannedlist:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->scanbutton:Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$string;->stop:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->nodevices:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v4}, Landroid/widget/FrameLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mScannedlist:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mScannedDevicesList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->spinner:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->scanningLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    :cond_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public startServiceIfNeeded(Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mContext:Landroid/content/Context;

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->startServiceIfNeeded()V

    return-void
.end method

.method protected updateScanningFragment(IZLjava/util/ArrayList;II)V
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZ",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;II)V"
        }
    .end annotation

    const/4 v1, 0x3

    new-array v4, v1, [J

    const/4 v1, 0x2

    const-wide/16 v2, 0x3

    aput-wide v2, v4, v1

    const/4 v1, 0x0

    array-length v2, v4

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v4, v2

    long-to-int v2, v2

    if-gtz v2, :cond_0

    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, v1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    const/4 v3, 0x0

    move/from16 v0, p1

    int-to-long v1, v0

    const/16 v5, 0x20

    shl-long/2addr v1, v5

    const/16 v5, 0x20

    ushr-long v5, v1, v5

    aget-wide v1, v4, v3

    const-wide/16 v7, 0x0

    cmp-long v7, v1, v7

    if-eqz v7, :cond_1

    const-wide v7, -0x3ffd6c73623f3161L    # -2.322045547911173

    xor-long/2addr v1, v7

    :cond_1
    const/16 v7, 0x20

    ushr-long/2addr v1, v7

    const/16 v7, 0x20

    shl-long/2addr v1, v7

    xor-long/2addr v1, v5

    const-wide v5, -0x3ffd6c73623f3161L    # -2.322045547911173

    xor-long/2addr v1, v5

    aput-wide v1, v4, v3

    const/4 v1, 0x1

    array-length v2, v4

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v4, v2

    long-to-int v2, v2

    if-lt v1, v2, :cond_2

    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, v1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_2
    const/4 v3, 0x0

    move/from16 v0, p4

    int-to-long v1, v0

    const/16 v5, 0x20

    shl-long v5, v1, v5

    aget-wide v1, v4, v3

    const-wide/16 v7, 0x0

    cmp-long v7, v1, v7

    if-eqz v7, :cond_3

    const-wide v7, -0x3ffd6c73623f3161L    # -2.322045547911173

    xor-long/2addr v1, v7

    :cond_3
    const/16 v7, 0x20

    shl-long/2addr v1, v7

    const/16 v7, 0x20

    ushr-long/2addr v1, v7

    xor-long/2addr v1, v5

    const-wide v5, -0x3ffd6c73623f3161L    # -2.322045547911173

    xor-long/2addr v1, v5

    aput-wide v1, v4, v3

    const/4 v1, 0x2

    array-length v2, v4

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v4, v2

    long-to-int v2, v2

    if-lt v1, v2, :cond_4

    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, v1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_4
    const/4 v3, 0x1

    move/from16 v0, p5

    int-to-long v1, v0

    const/16 v5, 0x20

    shl-long/2addr v1, v5

    const/16 v5, 0x20

    ushr-long v5, v1, v5

    aget-wide v1, v4, v3

    const-wide/16 v7, 0x0

    cmp-long v7, v1, v7

    if-eqz v7, :cond_5

    const-wide v7, -0x3ffd6c73623f3161L    # -2.322045547911173

    xor-long/2addr v1, v7

    :cond_5
    const/16 v7, 0x20

    ushr-long/2addr v1, v7

    const/16 v7, 0x20

    shl-long/2addr v1, v7

    xor-long/2addr v1, v5

    const-wide v5, -0x3ffd6c73623f3161L    # -2.322045547911173

    xor-long/2addr v1, v5

    aput-wide v1, v4, v3

    const/4 v1, 0x1

    sput-boolean v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mUpdateScanStatus:Z

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mIsNoSupportScan:Z

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->IsAutoScanRequired:Z

    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, -0x5e

    aput v13, v2, v12

    const/16 v12, -0x7e8c

    aput v12, v2, v11

    const/16 v11, -0x18

    aput v11, v2, v10

    const/16 v10, -0x37

    aput v10, v2, v9

    const/16 v9, -0x7a

    aput v9, v2, v8

    const/16 v8, -0x3f

    aput v8, v2, v7

    const/16 v7, -0x59

    aput v7, v2, v6

    const/16 v6, -0x5a

    aput v6, v2, v5

    const/16 v5, 0x6262

    aput v5, v2, v3

    const/16 v3, 0x240e

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, -0x39

    aput v14, v1, v13

    const/16 v13, -0x7ee9

    aput v13, v1, v12

    const/16 v12, -0x7f

    aput v12, v1, v11

    const/16 v11, -0x41

    aput v11, v1, v10

    const/16 v10, -0xc

    aput v10, v1, v9

    const/16 v9, -0x5c

    aput v9, v1, v8

    const/16 v8, -0xc

    aput v8, v1, v7

    const/16 v7, -0x3c

    aput v7, v1, v6

    const/16 v6, 0x620b

    aput v6, v1, v5

    const/16 v5, 0x2462

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v5, v1

    if-lt v3, v5, :cond_6

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v5, v1

    if-lt v3, v5, :cond_7

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->lp()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->isScanning:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mInfoContentIds:[I

    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_2

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mInfoContentIds:[I

    array-length v1, v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_c

    iget-object v3, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mInfoContentIds:[I

    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    const/4 v2, 0x2

    if-gt v1, v2, :cond_a

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, -0x38

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, -0x6

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v5, v1

    if-lt v3, v5, :cond_8

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v5, v1

    if-lt v3, v5, :cond_9

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_6
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_7
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_8
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_9
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_a
    const/4 v1, 0x1

    aget-wide v1, v4, v1

    const-wide/16 v5, 0x0

    cmp-long v5, v1, v5

    if-eqz v5, :cond_b

    const-wide v5, -0x3ffd6c73623f3161L    # -2.322045547911173

    xor-long/2addr v1, v5

    :cond_b
    const/16 v5, 0x20

    shl-long/2addr v1, v5

    const/16 v5, 0x20

    shr-long/2addr v1, v5

    long-to-int v1, v1

    aget v1, v3, v1

    iput v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->contentId:I

    :cond_c
    :goto_4
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->isScanning:Z

    if-eqz v1, :cond_11

    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    const/4 v2, 0x2

    if-gt v1, v2, :cond_f

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, -0x46

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, -0x78

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_5
    array-length v5, v1

    if-lt v3, v5, :cond_d

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_6
    array-length v5, v1

    if-lt v3, v5, :cond_e

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_d
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    :cond_e
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    :cond_f
    const/4 v1, 0x1

    aget-wide v1, v4, v1

    const-wide/16 v5, 0x0

    cmp-long v3, v1, v5

    if-eqz v3, :cond_10

    const-wide v5, -0x3ffd6c73623f3161L    # -2.322045547911173

    xor-long/2addr v1, v5

    :cond_10
    const/16 v3, 0x20

    shl-long/2addr v1, v3

    const/16 v3, 0x20

    shr-long/2addr v1, v3

    long-to-int v1, v1

    if-nez v1, :cond_11

    :goto_7
    return-void

    :cond_11
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->isScanning:Z

    if-nez v1, :cond_16

    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    const/4 v2, 0x2

    if-gt v1, v2, :cond_14

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, -0x1df8

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, -0x1dc6

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_8
    array-length v5, v1

    if-lt v3, v5, :cond_12

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_9
    array-length v5, v1

    if-lt v3, v5, :cond_13

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_12
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_8

    :cond_13
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_9

    :cond_14
    const/4 v1, 0x1

    aget-wide v1, v4, v1

    const-wide/16 v5, 0x0

    cmp-long v3, v1, v5

    if-eqz v3, :cond_15

    const-wide v5, -0x3ffd6c73623f3161L    # -2.322045547911173

    xor-long/2addr v1, v5

    :cond_15
    const/16 v3, 0x20

    shl-long/2addr v1, v3

    const/16 v3, 0x20

    shr-long/2addr v1, v3

    long-to-int v1, v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_17

    :cond_16
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->stopSearch()V

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->stopDiscoveringSensorDevices()V

    :cond_17
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->isFragmetRunning:Z

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->dialog:Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;

    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_3

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->dialog:Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_18

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->dialog:Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;->cancel()V

    :cond_18
    :goto_a
    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    if-gtz v1, :cond_1b

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, 0x4351

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, 0x4361

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_b
    array-length v5, v1

    if-lt v3, v5, :cond_19

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_c
    array-length v5, v1

    if-lt v3, v5, :cond_1a

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_19
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_b

    :cond_1a
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_c

    :cond_1b
    const/4 v1, 0x0

    aget-wide v1, v4, v1

    const-wide/16 v5, 0x0

    cmp-long v3, v1, v5

    if-eqz v3, :cond_1c

    const-wide v5, -0x3ffd6c73623f3161L    # -2.322045547911173

    xor-long/2addr v1, v5

    :cond_1c
    const/16 v3, 0x20

    shl-long/2addr v1, v3

    const/16 v3, 0x20

    shr-long/2addr v1, v3

    long-to-int v1, v1

    iput v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mDataType:I

    move/from16 v0, p2

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->showCompanionDevices:Z

    move-object/from16 v0, p3

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mDeviceTypes:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->handler:Landroid/os/Handler;

    :try_start_2
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_4

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->handler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    :goto_d
    :try_start_3
    invoke-virtual/range {p3 .. p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_0

    invoke-virtual/range {p3 .. p3}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_2a

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mIsNoSupportScan:Z

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    :try_start_4
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_4
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_4} :catch_5

    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    const/4 v2, 0x1

    if-gt v1, v2, :cond_1f

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, -0xfe2

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, -0xfd1

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_e
    array-length v5, v1

    if-lt v3, v5, :cond_1d

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_f
    array-length v5, v1

    if-lt v3, v5, :cond_1e

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_1d
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_e

    :cond_1e
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_f

    :cond_1f
    const/4 v1, 0x0

    aget-wide v1, v4, v1

    const-wide/16 v5, 0x0

    cmp-long v3, v1, v5

    if-eqz v3, :cond_20

    const-wide v5, -0x3ffd6c73623f3161L    # -2.322045547911173

    xor-long/2addr v1, v5

    :cond_20
    const/16 v3, 0x20

    shr-long/2addr v1, v3

    long-to-int v1, v1

    if-eqz v1, :cond_29

    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    const/4 v2, 0x1

    if-gt v1, v2, :cond_23

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, -0x1c

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, -0x2b

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_10
    array-length v5, v1

    if-lt v3, v5, :cond_21

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_11
    array-length v5, v1

    if-lt v3, v5, :cond_22

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_21
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_10

    :cond_22
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_11

    :cond_23
    const/4 v1, 0x0

    aget-wide v1, v4, v1

    const-wide/16 v5, 0x0

    cmp-long v3, v1, v5

    if-eqz v3, :cond_24

    const-wide v5, -0x3ffd6c73623f3161L    # -2.322045547911173

    xor-long/2addr v1, v5

    :cond_24
    const/16 v3, 0x20

    shr-long/2addr v1, v3

    long-to-int v1, v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_29

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    new-instance v5, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$11;

    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    const/4 v2, 0x1

    if-gt v1, v2, :cond_27

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, -0x4d89

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, -0x4dba

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_12
    array-length v5, v1

    if-lt v3, v5, :cond_25

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_13
    array-length v5, v1

    if-lt v3, v5, :cond_26

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_25
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_12

    :cond_26
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_13

    :cond_27
    const/4 v1, 0x0

    aget-wide v1, v4, v1

    const-wide/16 v6, 0x0

    cmp-long v6, v1, v6

    if-eqz v6, :cond_28

    const-wide v6, -0x3ffd6c73623f3161L    # -2.322045547911173

    xor-long/2addr v1, v6

    :cond_28
    const/16 v6, 0x20

    shr-long/2addr v1, v6

    long-to-int v1, v1

    invoke-direct {v5, p0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$11;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;I)V

    invoke-virtual {v3, v5}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    :cond_29
    :goto_14
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    :try_start_5
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_5
    .catch Ljava/lang/NullPointerException; {:try_start_5 .. :try_end_5} :catch_6

    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    const/4 v2, 0x1

    if-gt v1, v2, :cond_2d

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x2

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, 0x33

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_15
    array-length v5, v1

    if-lt v3, v5, :cond_2b

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_16
    array-length v5, v1

    if-lt v3, v5, :cond_2c

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :catch_0
    move-exception v1

    :cond_2a
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->setScanningViewsVisibility(Z)V

    goto :goto_14

    :cond_2b
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_15

    :cond_2c
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_16

    :cond_2d
    const/4 v1, 0x0

    aget-wide v1, v4, v1

    const-wide/16 v5, 0x0

    cmp-long v3, v1, v5

    if-eqz v3, :cond_2e

    const-wide v5, -0x3ffd6c73623f3161L    # -2.322045547911173

    xor-long/2addr v1, v5

    :cond_2e
    const/16 v3, 0x20

    shr-long/2addr v1, v3

    long-to-int v1, v1

    if-eqz v1, :cond_37

    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    const/4 v2, 0x1

    if-gt v1, v2, :cond_31

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, -0x2d

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, -0x1e

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_17
    array-length v5, v1

    if-lt v3, v5, :cond_2f

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_18
    array-length v5, v1

    if-lt v3, v5, :cond_30

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_2f
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_17

    :cond_30
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_18

    :cond_31
    const/4 v1, 0x0

    aget-wide v1, v4, v1

    const-wide/16 v5, 0x0

    cmp-long v3, v1, v5

    if-eqz v3, :cond_32

    const-wide v5, -0x3ffd6c73623f3161L    # -2.322045547911173

    xor-long/2addr v1, v5

    :cond_32
    const/16 v3, 0x20

    shr-long/2addr v1, v3

    long-to-int v1, v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_37

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mIsNoSupportScan:Z

    if-nez v1, :cond_37

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    new-instance v5, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$12;

    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    const/4 v2, 0x1

    if-gt v1, v2, :cond_35

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, 0x4b16

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, 0x4b27

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_19
    array-length v5, v1

    if-lt v3, v5, :cond_33

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1a
    array-length v5, v1

    if-lt v3, v5, :cond_34

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_33
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_19

    :cond_34
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1a

    :cond_35
    const/4 v1, 0x0

    aget-wide v1, v4, v1

    const-wide/16 v6, 0x0

    cmp-long v4, v1, v6

    if-eqz v4, :cond_36

    const-wide v6, -0x3ffd6c73623f3161L    # -2.322045547911173

    xor-long/2addr v1, v6

    :cond_36
    const/16 v4, 0x20

    shr-long/2addr v1, v4

    long-to-int v1, v1

    invoke-direct {v5, p0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$12;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;I)V

    invoke-virtual {v3, v5}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    :cond_37
    :goto_1b
    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mScanningFragmentController:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;

    :try_start_6
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_6
    .catch Ljava/lang/NullPointerException; {:try_start_6 .. :try_end_6} :catch_1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->requestPairedDevices()V

    :goto_1c
    const/4 v1, 0x0

    sput-boolean v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mUpdateScanStatus:Z

    goto/16 :goto_7

    :catch_1
    move-exception v1

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->startServiceIfNeeded()V

    goto :goto_1c

    :catch_2
    move-exception v1

    goto/16 :goto_4

    :catch_3
    move-exception v1

    goto/16 :goto_a

    :catch_4
    move-exception v1

    goto/16 :goto_d

    :catch_5
    move-exception v1

    goto/16 :goto_14

    :catch_6
    move-exception v1

    goto :goto_1b
.end method
