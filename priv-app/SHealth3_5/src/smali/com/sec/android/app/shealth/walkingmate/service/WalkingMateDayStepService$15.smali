.class Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$15;
.super Ljava/lang/Object;
.source "WalkingMateDayStepService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->StartProcessAfterServiceStart()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;)V
    .locals 0

    .prologue
    .line 1769
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$15;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1772
    const-string v1, "PEDOSTART"

    const-string/jumbo v2, "pedometer start"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1773
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->isRun()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1774
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$15;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    const-string/jumbo v2, "notification"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 1775
    .local v0, "notiManager":Landroid/app/NotificationManager;
    if-eqz v0, :cond_0

    .line 1776
    const/16 v1, 0x42d

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 1782
    .end local v0    # "notiManager":Landroid/app/NotificationManager;
    :cond_0
    :goto_0
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->isStartWalking()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1783
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->isRun()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1784
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$15;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    invoke-virtual {v1, v3}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->onStop(Z)V

    .line 1803
    :cond_1
    :goto_1
    const-string v1, "PEDOSTART"

    const-string/jumbo v2, "pedometer end"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1804
    return-void

    .line 1779
    :cond_2
    const-string v1, "PEDOSTART"

    const-string/jumbo v2, "service start fails"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1786
    :cond_3
    const-string v1, "PEDOSTART"

    const-string/jumbo v2, "service start fails"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1789
    :cond_4
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->isRun()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1790
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$15;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    # invokes: Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->initialize()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->access$2400(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;)V

    .line 1791
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->setFirstStart(Z)V

    .line 1792
    invoke-static {v3}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->setStartWalking(Z)V

    .line 1793
    invoke-static {v3}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->setNotificationONOFF(Z)V

    .line 1794
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$15;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->onStart()V

    .line 1795
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$15;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->updateWidgets(Landroid/content/Context;)V

    .line 1799
    :goto_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$15;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    iget-object v1, v1, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mHandler:Landroid/os/Handler;

    if-eqz v1, :cond_1

    .line 1800
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$15;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    iget-object v1, v1, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$15;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    iget-object v2, v2, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mToastRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_1

    .line 1797
    :cond_5
    const-string v1, "PEDOSTART"

    const-string v2, "Service is null"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method
