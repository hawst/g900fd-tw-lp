.class Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$LogHandler;
.super Landroid/os/Handler;
.source "LogManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "LogHandler"
.end annotation


# static fields
.field public static final ACTIVITY_PAUSED:I = 0xa

.field public static final ACTIVITY_RESUMED:I = 0x9

.field public static final CHECK_PROFILE:I = 0xb

.field public static final DATA_VALIDATION_ERROR:I = 0xc

.field public static final DISABLE_BA:I = -0x2

.field public static final ENABLE_BA:I = -0x1

.field public static final INIT_GA:I = 0x0

.field public static final INSERT_LOG:I = 0x1

.field public static final PREPARE_SENDING_LOGS:I = 0x2

.field public static final SPP_BIND:I = 0x4

.field public static final SPP_SEND:I = 0x5

.field public static final SPP_SERVICE_CONNECTED:I = 0x7

.field public static final SPP_SERVICE_DISCONNECTED:I = 0x8

.field public static final SPP_UNREGISTER:I = 0x6

.field public static final START_SENDING_LOGS:I = 0x3


# direct methods
.method constructor <init>(Landroid/os/Looper;)V
    .locals 0
    .param p1, "loop"    # Landroid/os/Looper;

    .prologue
    .line 1103
    invoke-direct {p0, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 1104
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 21
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 1108
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->isARTWithKitkat()Z

    move-result v17

    if-eqz v17, :cond_1

    .line 1276
    :cond_0
    :goto_0
    return-void

    .line 1113
    :cond_1
    const/4 v3, 0x0

    .line 1114
    .local v3, "bundle":Landroid/os/Bundle;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->getInstance()Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;

    move-result-object v14

    .line 1116
    .local v14, "manager":Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;
    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v17, v0

    packed-switch v17, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 1125
    :pswitch_1
    const-string v17, "LogManager"

    const-string v18, "Disable BA."

    invoke-static/range {v17 .. v18}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1126
    const/16 v17, 0x0

    # setter for: Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->sIsSupportBa:Z
    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->access$202(Z)Z

    goto :goto_0

    .line 1118
    :pswitch_2
    # invokes: Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->initGa()V
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->access$000()V

    goto :goto_0

    .line 1121
    :pswitch_3
    const-string v17, "LogManager"

    const-string v18, "Enable BA."

    invoke-static/range {v17 .. v18}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1122
    # invokes: Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->initBa()V
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->access$100()V

    goto :goto_0

    .line 1129
    :pswitch_4
    iget-object v0, v14, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mPref:Landroid/content/SharedPreferences;

    move-object/from16 v17, v0

    const-string v18, "LogSentDate"

    const-wide/16 v19, -0x1

    invoke-interface/range {v17 .. v20}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v12

    .line 1130
    .local v12, "lastDate":J
    const-wide/16 v17, -0x1

    cmp-long v17, v12, v17

    if-nez v17, :cond_3

    .line 1131
    iget-object v0, v14, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mPref:Landroid/content/SharedPreferences;

    move-object/from16 v17, v0

    invoke-interface/range {v17 .. v17}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v17

    const-string v18, "LogSentDate"

    const/16 v19, 0x1

    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->getMidnightTimeBefore(I)J

    move-result-wide v19

    invoke-interface/range {v17 .. v20}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1138
    :cond_2
    :goto_1
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v3

    .line 1139
    if-eqz v3, :cond_0

    .line 1142
    const/16 v17, 0x0

    move/from16 v0, v17

    # invokes: Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->appendLog(ZLandroid/os/Bundle;)V
    invoke-static {v0, v3}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->access$500(ZLandroid/os/Bundle;)V

    goto :goto_0

    .line 1132
    :cond_3
    const/16 v17, 0x1

    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->getMidnightTimeBefore(I)J

    move-result-wide v17

    cmp-long v17, v12, v17

    if-eqz v17, :cond_2

    .line 1133
    iget-object v0, v14, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mPref:Landroid/content/SharedPreferences;

    move-object/from16 v17, v0

    invoke-interface/range {v17 .. v17}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v17

    const-string v18, "LogSentDate"

    const/16 v19, 0x1

    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->getMidnightTimeBefore(I)J

    move-result-wide v19

    invoke-interface/range {v17 .. v20}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1134
    # invokes: Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->performDailyTask()V
    invoke-static {v14}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->access$300(Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;)V

    .line 1135
    # invokes: Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->startSendLog()V
    invoke-static {v14}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->access$400(Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;)V

    goto :goto_1

    .line 1146
    .end local v12    # "lastDate":J
    :pswitch_5
    const-string v17, "LogManager"

    const-string v18, "Before sending log, check samsung account."

    invoke-static/range {v17 .. v18}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1148
    iget-object v0, v14, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->isDeviceSignInSamsungAccount(Landroid/content/Context;)Z

    move-result v17

    if-eqz v17, :cond_4

    .line 1149
    const-string v17, "LogManager"

    const-string v18, "Samsung account exist, request user id."

    invoke-static/range {v17 .. v18}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1151
    # invokes: Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->getSamsungAccountUserId()Z
    invoke-static {v14}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->access$600(Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;)Z

    move-result v17

    if-eqz v17, :cond_4

    .line 1152
    const-string v17, "LogManager"

    const-string v18, "Request samsung account, waiting response."

    invoke-static/range {v17 .. v18}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1157
    :cond_4
    const/16 v17, 0x0

    move-object/from16 v0, v17

    # setter for: Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mUserId:Ljava/lang/String;
    invoke-static {v14, v0}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->access$702(Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;Ljava/lang/String;)Ljava/lang/String;

    .line 1158
    # getter for: Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mHandler:Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$LogHandler;
    invoke-static {v14}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->access$800(Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;)Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$LogHandler;

    move-result-object v17

    const/16 v18, 0x3

    invoke-virtual/range {v17 .. v18}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$LogHandler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 1163
    :pswitch_6
    const-string v17, "LogManager"

    const-string v18, "For start of sending logs, bind spp service"

    invoke-static/range {v17 .. v18}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1164
    # getter for: Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->isUploaderRunning:Z
    invoke-static {v14}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->access$900(Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;)Z

    move-result v17

    if-eqz v17, :cond_0

    .line 1165
    # invokes: Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->registSPPReceiver()V
    invoke-static {v14}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->access$1000(Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;)V

    .line 1166
    # invokes: Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->register()V
    invoke-static {v14}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->access$1100(Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;)V

    goto/16 :goto_0

    .line 1171
    :pswitch_7
    invoke-virtual {v14}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->sppBinding()V

    goto/16 :goto_0

    .line 1174
    :pswitch_8
    const-string v17, "LogManager"

    const-string v18, "SPP_SERVICE_CONNECTED : Start sending logs"

    invoke-static/range {v17 .. v18}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1175
    # getter for: Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mSPPBinder:Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;
    invoke-static {v14}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->access$1200(Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;)Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;

    move-result-object v17

    if-eqz v17, :cond_0

    .line 1176
    invoke-virtual {v14}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->sendLogsViaSpp()V

    .line 1177
    invoke-virtual {v14}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->sppUnbinding()V

    .line 1178
    const/16 v17, 0x0

    move/from16 v0, v17

    # setter for: Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->isUploaderRunning:Z
    invoke-static {v14, v0}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->access$902(Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;Z)Z

    goto/16 :goto_0

    .line 1183
    :pswitch_9
    const-string v17, "LogManager"

    const-string v18, "SPP_SERVICE_DISCONNECTED"

    invoke-static/range {v17 .. v18}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1184
    invoke-virtual {v14}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->unregistSPPReceiver()V

    .line 1185
    const/16 v17, 0x0

    move/from16 v0, v17

    # setter for: Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->isUploaderRunning:Z
    invoke-static {v14, v0}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->access$902(Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;Z)Z

    goto/16 :goto_0

    .line 1188
    :pswitch_a
    # getter for: Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mIsResponseReceived:Z
    invoke-static {v14}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->access$1300(Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;)Z

    move-result v17

    if-nez v17, :cond_5

    .line 1189
    const-string v17, "LogManager"

    const-string v18, "No response from SPP, so end the operation."

    invoke-static/range {v17 .. v18}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 1190
    invoke-virtual {v14}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->unregistSPPReceiver()V

    .line 1191
    const/16 v17, 0x0

    move/from16 v0, v17

    # setter for: Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mIsResponseReceived:Z
    invoke-static {v14, v0}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->access$1302(Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;Z)Z

    .line 1193
    :cond_5
    const/16 v17, 0x0

    move/from16 v0, v17

    # setter for: Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->isUploaderRunning:Z
    invoke-static {v14, v0}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->access$902(Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;Z)Z

    goto/16 :goto_0

    .line 1196
    :pswitch_b
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v3

    .line 1197
    if-eqz v3, :cond_0

    .line 1201
    const-string v17, "activity"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    # setter for: Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mActivityName:Ljava/lang/String;
    invoke-static {v14, v0}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->access$1402(Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;Ljava/lang/String;)Ljava/lang/String;

    .line 1202
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v17

    move-wide/from16 v0, v17

    # setter for: Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mActivityStartTime:J
    invoke-static {v14, v0, v1}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->access$1502(Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;J)J

    goto/16 :goto_0

    .line 1206
    :pswitch_c
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v3

    .line 1207
    if-eqz v3, :cond_0

    .line 1211
    # getter for: Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mActivityName:Ljava/lang/String;
    invoke-static {v14}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->access$1400(Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;)Ljava/lang/String;

    move-result-object v17

    if-eqz v17, :cond_0

    .line 1215
    # getter for: Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mActivityName:Ljava/lang/String;
    invoke-static {v14}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->access$1400(Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;)Ljava/lang/String;

    move-result-object v17

    const-string v18, "activity"

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_0

    .line 1219
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v17

    # getter for: Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mActivityStartTime:J
    invoke-static {v14}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->access$1500(Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;)J

    move-result-wide v19

    sub-long v6, v17, v19

    .line 1221
    .local v6, "duration":J
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 1222
    .local v4, "bundle2":Landroid/os/Bundle;
    const-string v17, "app_id"

    const-string v18, "com.sec.android.app.shealth"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1223
    const-string v17, "feature"

    const-string v18, "UAPP"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1224
    const-string v17, "extra"

    # getter for: Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mActivityName:Ljava/lang/String;
    invoke-static {v14}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->access$1400(Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1225
    const-string/jumbo v17, "value"

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1227
    const/16 v17, 0x1

    move/from16 v0, v17

    # invokes: Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->appendLog(ZLandroid/os/Bundle;)V
    invoke-static {v0, v4}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->access$500(ZLandroid/os/Bundle;)V

    goto/16 :goto_0

    .line 1232
    .end local v4    # "bundle2":Landroid/os/Bundle;
    .end local v6    # "duration":J
    :pswitch_d
    # getter for: Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->isProfileSet:Z
    invoke-static {v14}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->access$1600(Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;)Z

    move-result v17

    if-eqz v17, :cond_6

    .line 1233
    const-string v17, "LogManager"

    const-string v18, "Profile was already set."

    invoke-static/range {v17 .. v18}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1235
    :cond_6
    const-string v17, "LogManager"

    const-string v18, "Check profile."

    invoke-static/range {v17 .. v18}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1237
    :try_start_0
    new-instance v16, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    iget-object v0, v14, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    invoke-direct/range {v16 .. v17}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    .line 1239
    .local v16, "shealthProfile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getGender()I

    move-result v10

    .line 1240
    .local v10, "gender":I
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getBirthDate()Ljava/lang/String;

    move-result-object v2

    .line 1242
    .local v2, "birthday":Ljava/lang/String;
    invoke-static {v10, v2}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->setProfileItems(ILjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 1244
    .end local v2    # "birthday":Ljava/lang/String;
    .end local v10    # "gender":I
    .end local v16    # "shealthProfile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    :catch_0
    move-exception v8

    .line 1245
    .local v8, "e":Ljava/lang/Exception;
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 1250
    .end local v8    # "e":Ljava/lang/Exception;
    :pswitch_e
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v3

    .line 1251
    if-eqz v3, :cond_0

    .line 1255
    const-string v17, "dataType"

    const-string v18, ""

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1256
    .local v5, "dataType":Ljava/lang/String;
    const-string v17, "isFixed"

    const/16 v18, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v11

    .line 1260
    .local v11, "isFixed":Z
    if-eqz v11, :cond_7

    .line 1261
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "DataErrorFixed_"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    .line 1266
    .local v15, "prefKey":Ljava/lang/String;
    :goto_2
    iget-object v0, v14, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mPref:Landroid/content/SharedPreferences;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-interface {v0, v15, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v9

    .line 1267
    .local v9, "errorCount":I
    add-int/lit8 v9, v9, 0x1

    .line 1269
    const-string v17, "LogManager"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Increase error count. dataType=["

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "] count=["

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "]"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1271
    iget-object v0, v14, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mPref:Landroid/content/SharedPreferences;

    move-object/from16 v17, v0

    invoke-interface/range {v17 .. v17}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-interface {v0, v15, v9}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto/16 :goto_0

    .line 1263
    .end local v9    # "errorCount":I
    .end local v15    # "prefKey":Ljava/lang/String;
    :cond_7
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "DataErrorIgnored_"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    .restart local v15    # "prefKey":Ljava/lang/String;
    goto :goto_2

    .line 1116
    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_8
        :pswitch_9
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_a
    .end packed-switch
.end method
