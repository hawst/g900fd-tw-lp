.class public Lcom/sec/android/app/shealth/home/AddFavoritesActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "AddFavoritesActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/home/AddFavoritesActivity$SpinnerCustomAdapter;,
        Lcom/sec/android/app/shealth/home/AddFavoritesActivity$RegistryContentObserver;
    }
.end annotation


# static fields
.field private static final FITNESS:Ljava/lang/String; = "Fitness"

.field private static final LOG_TAG:Ljava/lang/String;

.field private static final MAX_FAVORITES_COUNT:I = 0x10

.field private static final THIRDPARTY_APP_TYPE:I = 0x2


# instance fields
.field private favCount:I

.field private mCareAdapter:Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter;

.field private mCareGridView:Lcom/sec/android/app/shealth/home/favorite/EditFavoritesGridView;

.field private mFavoritesCount:I

.field private mFitnessAdapter:Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter;

.field private mFitnessGridView:Lcom/sec/android/app/shealth/home/favorite/EditFavoritesGridView;

.field private mNoAppLayout:Landroid/widget/LinearLayout;

.field private mPartnersList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/home/data/IconData;",
            ">;"
        }
    .end annotation
.end field

.field private mPartnersSection:Landroid/widget/LinearLayout;

.field private mPluginlist:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;",
            ">;"
        }
    .end annotation
.end field

.field private mSHealthList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/home/data/IconData;",
            ">;"
        }
    .end annotation
.end field

.field private mSHealthSection:Landroid/widget/LinearLayout;

.field private mSelectDataAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mSelectList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mSelectSpinner:Landroid/widget/Spinner;

.field private registryContentObserver:Lcom/sec/android/app/shealth/home/AddFavoritesActivity$RegistryContentObserver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58
    const-class v0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->getLogTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 56
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    .line 62
    iput-object v0, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mPartnersSection:Landroid/widget/LinearLayout;

    .line 63
    iput-object v0, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mCareGridView:Lcom/sec/android/app/shealth/home/favorite/EditFavoritesGridView;

    .line 64
    iput-object v0, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mPartnersList:Ljava/util/ArrayList;

    .line 65
    iput-object v0, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mCareAdapter:Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter;

    .line 66
    iput-object v0, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mPluginlist:Ljava/util/ArrayList;

    .line 67
    iput-object v0, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mNoAppLayout:Landroid/widget/LinearLayout;

    .line 71
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mSelectList:Ljava/util/List;

    .line 75
    iput v1, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mFavoritesCount:I

    .line 76
    iput v1, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->favCount:I

    .line 460
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mSHealthList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    .prologue
    .line 56
    iget v0, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mFavoritesCount:I

    return v0
.end method

.method static synthetic access$1000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->LOG_TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/AddFavoritesActivity;
    .param p1, "x1"    # I

    .prologue
    .line 56
    iput p1, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mFavoritesCount:I

    return p1
.end method

.method static synthetic access$108(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    .prologue
    .line 56
    iget v0, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mFavoritesCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mFavoritesCount:I

    return v0
.end method

.method static synthetic access$110(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    .prologue
    .line 56
    iget v0, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mFavoritesCount:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mFavoritesCount:I

    return v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;)Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mFitnessAdapter:Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->setAdapter()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->setActionBarSpinner()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    .prologue
    .line 56
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->refreshFocusables()V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mPartnersList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;)Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mCareAdapter:Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    .prologue
    .line 56
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->refreshFocusables()V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;)Lcom/sec/android/app/shealth/home/AddFavoritesActivity$RegistryContentObserver;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->registryContentObserver:Lcom/sec/android/app/shealth/home/AddFavoritesActivity$RegistryContentObserver;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mPluginlist:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/AddFavoritesActivity;
    .param p1, "x1"    # Ljava/util/ArrayList;
    .param p2, "x2"    # Ljava/util/ArrayList;

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->setFavoritesStatus(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    return-void
.end method

.method private addSelectSpinner()V
    .locals 4

    .prologue
    .line 507
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mSelectSpinner:Landroid/widget/Spinner;

    if-nez v0, :cond_0

    .line 509
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030003

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mSelectSpinner:Landroid/widget/Spinner;

    .line 510
    new-instance v0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$SpinnerCustomAdapter;

    const v1, 0x7f03021a

    const v2, 0x7f08080a

    iget-object v3, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mSelectList:Ljava/util/List;

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$SpinnerCustomAdapter;-><init>(Landroid/content/Context;IILjava/util/List;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mSelectDataAdapter:Landroid/widget/ArrayAdapter;

    .line 511
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mSelectDataAdapter:Landroid/widget/ArrayAdapter;

    const v1, 0x7f030219

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 513
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mSelectSpinner:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mSelectDataAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 514
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mSelectSpinner:Landroid/widget/Spinner;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addSpinnerView(Landroid/widget/Spinner;)V

    .line 516
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mSelectSpinner:Landroid/widget/Spinner;

    new-instance v1, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$4;-><init>(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 589
    :cond_0
    return-void
.end method

.method private checkAppCount()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 431
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mSHealthList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 433
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mSHealthSection:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 440
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mPartnersList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 442
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mPartnersSection:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 449
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mSHealthList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mPartnersList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 451
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mNoAppLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 457
    :goto_2
    return-void

    .line 437
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mSHealthSection:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 446
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mPartnersSection:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_1

    .line 455
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mNoAppLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_2
.end method

.method private initView()V
    .locals 6

    .prologue
    const v3, 0x7f090cdd

    const v5, 0x7f0901fd

    const v4, 0x7f080167

    .line 96
    const v2, 0x7f080340

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mSHealthSection:Landroid/widget/LinearLayout;

    .line 97
    const v2, 0x7f080341

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/home/favorite/EditFavoritesGridView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mFitnessGridView:Lcom/sec/android/app/shealth/home/favorite/EditFavoritesGridView;

    .line 99
    const v2, 0x7f080342

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mPartnersSection:Landroid/widget/LinearLayout;

    .line 100
    const v2, 0x7f080343

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/home/favorite/EditFavoritesGridView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mCareGridView:Lcom/sec/android/app/shealth/home/favorite/EditFavoritesGridView;

    .line 102
    const v2, 0x7f080344

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mNoAppLayout:Landroid/widget/LinearLayout;

    .line 103
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mSHealthSection:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 104
    .local v1, "fitnessText":Landroid/widget/TextView;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 105
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->setContentDescription(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mPartnersSection:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 108
    .local v0, "careText":Landroid/widget/TextView;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09002a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 109
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090cde

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v2, v3}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->setContentDescription(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->setAdapter()V

    .line 112
    new-instance v2, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$RegistryContentObserver;

    const/4 v3, 0x0

    invoke-direct {v2, v3, p0}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$RegistryContentObserver;-><init>(Landroid/os/Handler;Lcom/sec/android/app/shealth/home/AddFavoritesActivity;)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->registryContentObserver:Lcom/sec/android/app/shealth/home/AddFavoritesActivity$RegistryContentObserver;

    .line 113
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/_private/BaseContract$AppRegistry;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->registryContentObserver:Lcom/sec/android/app/shealth/home/AddFavoritesActivity$RegistryContentObserver;

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 114
    return-void
.end method

.method private setActionBarSpinner()V
    .locals 13

    .prologue
    const v12, 0x7f090073

    const v11, 0x7f090071

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 246
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 247
    .local v4, "selectTempList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v3, 0x0

    .line 248
    .local v3, "selectCount":I
    iget-object v6, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mSHealthList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 249
    .local v2, "sHealthPluginCount":I
    iget-object v6, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mPartnersList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 250
    .local v1, "partnerPluginCount":I
    add-int v5, v2, v1

    .line 254
    .local v5, "totalCount":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_1

    .line 256
    iget-object v6, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mSHealthList:Ljava/util/ArrayList;

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/shealth/home/data/IconData;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/home/data/IconData;->getIsSelected()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 258
    add-int/lit8 v3, v3, 0x1

    .line 254
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 262
    :cond_1
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_3

    .line 264
    iget-object v6, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mPartnersList:Ljava/util/ArrayList;

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/shealth/home/data/IconData;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/home/data/IconData;->getIsSelected()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 266
    add-int/lit8 v3, v3, 0x1

    .line 262
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 270
    :cond_3
    const v6, 0x7f090074

    new-array v7, v10, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-virtual {p0, v6, v7}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 272
    if-nez v3, :cond_5

    .line 274
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 275
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v6

    invoke-virtual {v6, v9, v9}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarButtonVisiblity(ZI)V

    .line 289
    :cond_4
    :goto_2
    iget-object v6, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mSelectList:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->clear()V

    .line 290
    iget-object v6, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mSelectList:Ljava/util/List;

    invoke-interface {v6, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 291
    iget-object v6, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mSelectDataAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v6}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    .line 292
    iget-object v6, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mSelectSpinner:Landroid/widget/Spinner;

    invoke-virtual {v6, v9}, Landroid/widget/Spinner;->setSelection(I)V

    .line 294
    if-nez v5, :cond_7

    .line 296
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v6

    invoke-virtual {v6, v9}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setSpinnerViewEnabled(Z)V

    .line 305
    :goto_3
    return-void

    .line 278
    :cond_5
    if-ne v3, v5, :cond_6

    .line 280
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 281
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v6

    invoke-virtual {v6, v10, v9}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarButtonVisiblity(ZI)V

    goto :goto_2

    .line 283
    :cond_6
    if-ge v3, v5, :cond_4

    .line 285
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 286
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 287
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v6

    invoke-virtual {v6, v10, v9}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarButtonVisiblity(ZI)V

    goto :goto_2

    .line 300
    :cond_7
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v6

    invoke-virtual {v6, v10}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setSpinnerViewEnabled(Z)V

    goto :goto_3
.end method

.method private setAdapter()V
    .locals 6

    .prologue
    const v5, 0x7f0700e3

    const v4, 0x7f0201e9

    .line 134
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mSHealthList:Ljava/util/ArrayList;

    .line 135
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mPartnersList:Ljava/util/ArrayList;

    .line 137
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->setNotFavoriteApp()V

    .line 139
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mPluginlist:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 141
    new-instance v0, Lcom/sec/android/app/shealth/home/data/IconData;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/home/data/IconData;-><init>()V

    .line 142
    .local v0, "data":Lcom/sec/android/app/shealth/home/data/IconData;
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mPluginlist:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-object v2, v2, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->appName:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/home/data/IconData;->setAppName(Ljava/lang/String;)V

    .line 143
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mPluginlist:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-object v2, v2, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->displayPlugInIcons:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/home/data/IconData;->setAppDisplayPlugInIcons(Ljava/lang/String;)V

    .line 144
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mPluginlist:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-object v2, v2, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->packagename:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/home/data/IconData;->setPackageName(Ljava/lang/String;)V

    .line 145
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mPluginlist:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget v2, v2, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->pluginId:I

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/home/data/IconData;->setAppPluginId(I)V

    .line 146
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mPluginlist:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget v2, v2, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->appType:I

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/home/data/IconData;->setAppType(I)V

    .line 148
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mPluginlist:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget v2, v2, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->appType:I

    const/4 v3, 0x2

    if-eq v2, v3, :cond_0

    .line 150
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mSHealthList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 139
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 154
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mPartnersList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 157
    .end local v0    # "data":Lcom/sec/android/app/shealth/home/data/IconData;
    :cond_1
    new-instance v2, Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter;

    iget-object v3, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mSHealthList:Ljava/util/ArrayList;

    invoke-direct {v2, p0, v3, v5}, Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter;-><init>(Landroid/content/Context;Ljava/util/ArrayList;I)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mFitnessAdapter:Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter;

    .line 158
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mFitnessGridView:Lcom/sec/android/app/shealth/home/favorite/EditFavoritesGridView;

    iget-object v3, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mFitnessAdapter:Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/home/favorite/EditFavoritesGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 159
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mFitnessGridView:Lcom/sec/android/app/shealth/home/favorite/EditFavoritesGridView;

    invoke-virtual {v2, v4}, Lcom/sec/android/app/shealth/home/favorite/EditFavoritesGridView;->setSelector(I)V

    .line 161
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mFitnessGridView:Lcom/sec/android/app/shealth/home/favorite/EditFavoritesGridView;

    new-instance v3, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$1;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$1;-><init>(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/home/favorite/EditFavoritesGridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 203
    new-instance v2, Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter;

    iget-object v3, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mPartnersList:Ljava/util/ArrayList;

    invoke-direct {v2, p0, v3, v5}, Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter;-><init>(Landroid/content/Context;Ljava/util/ArrayList;I)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mCareAdapter:Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter;

    .line 204
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mCareGridView:Lcom/sec/android/app/shealth/home/favorite/EditFavoritesGridView;

    iget-object v3, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mCareAdapter:Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/home/favorite/EditFavoritesGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 205
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mCareGridView:Lcom/sec/android/app/shealth/home/favorite/EditFavoritesGridView;

    invoke-virtual {v2, v4}, Lcom/sec/android/app/shealth/home/favorite/EditFavoritesGridView;->setSelector(I)V

    .line 207
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mCareGridView:Lcom/sec/android/app/shealth/home/favorite/EditFavoritesGridView;

    new-instance v3, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$2;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$2;-><init>(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/home/favorite/EditFavoritesGridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 241
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->checkAppCount()V

    .line 242
    return-void
.end method

.method private setFavoritesStatus(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/home/data/IconData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 344
    .local p1, "totalList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;>;"
    .local p2, "arrtList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/home/data/IconData;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-eqz v1, :cond_1

    .line 346
    const/4 v0, 0x0

    .local v0, "cnt":I
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 348
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-le v1, v0, :cond_0

    .line 350
    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/home/data/IconData;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/home/data/IconData;->getIsSelected()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 352
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/home/data/IconData;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/home/data/IconData;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/home/data/IconData;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/home/data/IconData;->getAppPluginId()I

    move-result v1

    invoke-static {v2, v3, v4, v1}, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryDbManagerWithProvider;->updateAppFavoriteStatus(Landroid/content/Context;Ljava/lang/String;II)I

    .line 353
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/home/data/IconData;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/home/data/IconData;->getAppPluginId()I

    move-result v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/home/data/IconData;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/home/data/IconData;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->favCount:I

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->setOrder(Ljava/lang/String;I)V

    .line 354
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "com.sec.android.app.shealth"

    const-string v4, "HF01"

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/home/data/IconData;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/home/data/IconData;->getAppName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v3, v4, v1}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 355
    iget v1, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->favCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->favCount:I

    .line 346
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 360
    .end local v0    # "cnt":I
    :cond_1
    iget v1, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->favCount:I

    invoke-static {v1}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->setTotalFavoriteCount(I)V

    .line 361
    return-void
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 317
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    .line 320
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setTitleActionBarVisibility(Z)V

    .line 321
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitleTextVisibility(Z)V

    .line 323
    new-instance v1, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$3;-><init>(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;)V

    .line 336
    .local v1, "doneClickListner":Landroid/view/View$OnClickListener;
    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    const v2, 0x7f090044

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;-><init>(Landroid/view/View$OnClickListener;I)V

    .line 337
    .local v0, "doneButtonBuilder":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;
    invoke-virtual {v0, v5}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->setISOKCancelType(Z)Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    .line 338
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->create()Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 340
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 81
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 83
    const v0, 0x7f0300b1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->setContentView(I)V

    .line 85
    invoke-static {}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->getTotalFavoriteCount()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->favCount:I

    .line 86
    iget v0, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->favCount:I

    iput v0, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mFavoritesCount:I

    .line 88
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->initView()V

    .line 89
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->addSelectSpinner()V

    .line 90
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->setActionBarSpinner()V

    .line 92
    return-void
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 373
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->registryContentObserver:Lcom/sec/android/app/shealth/home/AddFavoritesActivity$RegistryContentObserver;

    if-eqz v0, :cond_0

    .line 375
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->registryContentObserver:Lcom/sec/android/app/shealth/home/AddFavoritesActivity$RegistryContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 376
    iput-object v2, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->registryContentObserver:Lcom/sec/android/app/shealth/home/AddFavoritesActivity$RegistryContentObserver;

    .line 378
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mSelectSpinner:Landroid/widget/Spinner;

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 379
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onDestroy()V

    .line 380
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 385
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->registryContentObserver:Lcom/sec/android/app/shealth/home/AddFavoritesActivity$RegistryContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 386
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onPause()V

    .line 387
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 310
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    .line 311
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 366
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/_private/BaseContract$AppRegistry;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->registryContentObserver:Lcom/sec/android/app/shealth/home/AddFavoritesActivity$RegistryContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 367
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onResume()V

    .line 368
    return-void
.end method

.method public setNotFavoriteApp()V
    .locals 5

    .prologue
    .line 118
    const/4 v3, 0x1

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryDbManagerWithProvider;->getAllAppRegistryDatas(ZLandroid/content/Context;)Ljava/util/List;

    move-result-object v1

    .line 119
    .local v1, "pluginlist":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mPluginlist:Ljava/util/ArrayList;

    .line 120
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    .line 122
    .local v2, "temp":Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;
    iget v3, v2, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->isFavorite:I

    if-nez v3, :cond_0

    .line 124
    iget-object v3, v2, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->actions:Ljava/lang/String;

    if-eqz v3, :cond_1

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->isUseCigna()Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, v2, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->actions:Ljava/lang/String;

    const-string v4, "com.sec.shealth.action.COACH"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 126
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mPluginlist:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 129
    .end local v2    # "temp":Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;
    :cond_2
    return-void
.end method
