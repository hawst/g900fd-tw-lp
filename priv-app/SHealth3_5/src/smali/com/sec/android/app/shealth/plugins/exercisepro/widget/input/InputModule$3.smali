.class Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$3;
.super Ljava/lang/Object;
.source "InputModule.java"

# interfaces
.implements Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$OnValueChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->registerListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)V
    .locals 0

    .prologue
    .line 362
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$3;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onValueChanged(F)V
    .locals 3
    .param p1, "value"    # F

    .prologue
    .line 366
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$3;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    const/high16 v2, 0x41200000    # 10.0f

    mul-float/2addr v0, v2

    float-to-int v2, v0

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$3;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->isDecimalNumber()Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->access$500(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->setNewValue(IZ)V
    invoke-static {v1, v2, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->access$600(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;IZ)V

    .line 367
    return-void

    .line 366
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
