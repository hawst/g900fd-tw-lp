.class public Lcom/sec/android/app/shealth/framework/repository/DatabaseUpgradeHelper;
.super Lsamsung/database/sqlite/SecSQLiteOpenHelper;
.source "DatabaseUpgradeHelper.java"


# static fields
.field private static final DATABASE_VERSION:I = 0x5

.field public static final DBNAME:Ljava/lang/String; = "shealth_to_upgrade.db"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/sec/android/app/shealth/framework/repository/DatabaseUpgradeHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/repository/DatabaseUpgradeHelper;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 25
    const-string/jumbo v0, "shealth_to_upgrade.db"

    const/4 v1, 0x0

    const/4 v2, 0x5

    invoke-direct {p0, p1, v0, v1, v2}, Lsamsung/database/sqlite/SecSQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Lsamsung/database/sqlite/SecSQLiteDatabase$CursorFactory;I)V

    .line 26
    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/repository/DatabaseUpgradeHelper;->mContext:Landroid/content/Context;

    .line 27
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/DatabaseUpgradeHelper;->TAG:Ljava/lang/String;

    const-string v1, "DatabaseUpgradeHelper shealth_to_upgrade.db"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 28
    return-void
.end method


# virtual methods
.method public onCreate(Lsamsung/database/sqlite/SecSQLiteDatabase;)V
    .locals 2
    .param p1, "arg0"    # Lsamsung/database/sqlite/SecSQLiteDatabase;

    .prologue
    .line 34
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/DatabaseUpgradeHelper;->TAG:Ljava/lang/String;

    const-string v1, "There is no databse file to upgrade"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 35
    return-void
.end method

.method public onUpgrade(Lsamsung/database/sqlite/SecSQLiteDatabase;II)V
    .locals 4
    .param p1, "db"    # Lsamsung/database/sqlite/SecSQLiteDatabase;
    .param p2, "oldversion"    # I
    .param p3, "newversion"    # I

    .prologue
    .line 40
    sget-object v1, Lcom/sec/android/app/shealth/framework/repository/DatabaseUpgradeHelper;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "onUpgrade oldversion = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " newversion "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 43
    new-instance v0, Lcom/sec/android/service/health/cp/datamigration/versionh/UptoHUpgradeManager;

    new-instance v1, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    invoke-direct {v1, p1}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;-><init>(Lsamsung/database/sqlite/SecSQLiteDatabase;)V

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/repository/DatabaseUpgradeHelper;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, v2}, Lcom/sec/android/service/health/cp/datamigration/versionh/UptoHUpgradeManager;-><init>(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;Landroid/content/Context;)V

    .line 44
    .local v0, "upgradeManager":Lcom/sec/android/service/health/cp/datamigration/versionh/UptoHUpgradeManager;
    invoke-virtual {v0, p2, p3}, Lcom/sec/android/service/health/cp/datamigration/versionh/UptoHUpgradeManager;->upgrade(II)Z

    .line 45
    return-void
.end method
