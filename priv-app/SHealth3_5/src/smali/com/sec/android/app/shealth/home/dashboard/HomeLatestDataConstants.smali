.class public Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;
.super Ljava/lang/Object;
.source "HomeLatestDataConstants.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants$SWearableConstants;,
        Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants$ExerciseType;
    }
.end annotation


# static fields
.field public static final AWARD:I = 0x14

.field public static final BLOOD_GLUCOSE:I = 0xa

.field public static final BLOOD_PRESSURE:I = 0xb

.field public static final BODY_FAT:I = 0x12

.field public static final BODY_TEMPERATURE:I = 0xc

.field public static final BURNT_CALORIES:I = 0x61

.field public static final CAREGIVER:I = 0xf

.field public static final CHALLENGE:I = 0xe

.field public static final CIGNA_COACH:I = 0x10

.field public static final DEFAULT_FEATURE:I = 0x62

.field public static final DOSAGE:I = 0xd

.field public static final ECG:I = 0x11

.field public static final EXERCISE_MATE:I = 0x2

.field public static final EXERCISE_PRO:I = 0x1

.field public static final EXERCISE_RESOURCES:Ljava/util/TreeMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final FOOD_TRACKER:I = 0x3

.field public static final HEART_BEAT:I = 0x6

.field public static final HEART_RATE:I = 0x15

.field public static final MAX_ITEMS_NUMBER_WITHOUT_SHORTCUT:I = 0x6

.field public static final MIN_ITEMS_NUMBER_WITHOUT_SHORTCUT:I = 0x0

.field public static final SECTION:I = 0x63

.field public static final SLEEP:I = 0x8

.field public static final SPO2:I = 0x13

.field public static final STRESS:I = 0x7

.field public static final THERMO_HYGROMETER:I = 0x9

.field public static final UV:I = 0x16

.field public static final WALKING_MATE:I = 0x0

.field public static final WATER_INTAKE:I = 0x5

.field public static final WEIGHT:I = 0x4


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const v7, 0x7f020493

    const v6, 0x7f02048c

    const v5, 0x7f020457

    const v4, 0x7f02049b

    const v3, 0x7f020486

    .line 66
    new-instance v0, Ljava/util/TreeMap;

    sget-object v1, Ljava/lang/String;->CASE_INSENSITIVE_ORDER:Ljava/util/Comparator;

    invoke-direct {v0, v1}, Ljava/util/TreeMap;-><init>(Ljava/util/Comparator;)V

    sput-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    .line 68
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Walking"

    const v2, 0x7f0204a3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Fast walking"

    const v2, 0x7f020464

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Running"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Power walking"

    const v2, 0x7f02047e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Walking upstairs"

    const v2, 0x7f0204a5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Walking downstairs"

    const v2, 0x7f0204a4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Hiking"

    const v2, 0x7f02046a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Dancing"

    const v2, 0x7f02045f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Inline Skating"

    const v2, 0x7f020473

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Yoga"

    const v2, 0x7f0204a8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Aerobic"

    const v2, 0x7f02044a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Skipping Rope"

    const v2, 0x7f020475

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Water-skiing"

    const v2, 0x7f0204a6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Cycling"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Hula-hooping"

    const v2, 0x7f02046d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Swimming"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Aquarobics"

    const v2, 0x7f02044b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Press-ups"

    const v2, 0x7f020480

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Sit-ups"

    const v2, 0x7f02048a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Chin-ups"

    const v2, 0x7f02045d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Rock climbing"

    const v2, 0x7f020482

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "T\'ai chi"

    const v2, 0x7f02049e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Hang gliding"

    const v2, 0x7f020469

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Golf"

    const v2, 0x7f020467

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Basketball"

    const v2, 0x7f020456

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Squash"

    const v2, 0x7f020496

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Sports dance"

    const v2, 0x7f02044f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Skiing"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Baseball"

    const v2, 0x7f020455

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Football"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Football, Competitive"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Football, Casual"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Tennis"

    const v2, 0x7f02049f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Badminton"

    const v2, 0x7f02044d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Bowling"

    const v2, 0x7f02045a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Billiards"

    const v2, 0x7f020458

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 104
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Boxing"

    const v2, 0x7f02045b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Handball"

    const v2, 0x7f020468

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Horse riding "

    const v2, 0x7f02046b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Judo"

    const v2, 0x7f020474

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Taekwondo"

    const v2, 0x7f02049d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Softball"

    const v2, 0x7f020494

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Table tennis"

    const v2, 0x7f02049c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 111
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Volleyball"

    const v2, 0x7f0204a2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 112
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Skin/scuba diving"

    const v2, 0x7f02048d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Ice Skating"

    const v2, 0x7f02048b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Step machine"

    const v2, 0x7f020499

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 115
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Exercise bike"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Weight machine"

    const v2, 0x7f0204a7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "General"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 119
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Treadmill"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 120
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Elliptical"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 121
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Bike"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 122
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Rower"

    const v2, 0x7f02045c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 123
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Climber"

    const v2, 0x7f020482

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Nordic skier"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Pedometer"

    const v2, 0x7f0204a3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 131
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Walking"

    const v2, 0x7f0204a3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Slow Walking"

    const v2, 0x7f02048f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 133
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Normal Walking"

    const v2, 0x7f0204a3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 134
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Fast Walking"

    const v2, 0x7f020464

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Walking upstairs"

    const v2, 0x7f0204a5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 136
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Walking downstairs"

    const v2, 0x7f0204a4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Running"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 140
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Running, General"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 141
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Running, 4 mph"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 142
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Running, 5 mph"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 143
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Running, 6 mph"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 144
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Running, 7 mph"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 145
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Running, 8 mph"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 146
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Running, 9 mph"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 147
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Running, 10 mph"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 148
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Running, 11 mph"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 149
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Running, 12 mph"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 150
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Running, 13 mph"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 151
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Running, 14 mph"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 154
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "General"

    const v2, 0x7f0204a3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 155
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Hiking"

    const v2, 0x7f02046a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 156
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Ballet"

    const v2, 0x7f02044e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 157
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Dancing"

    const v2, 0x7f02045f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 158
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Inline skating, slow"

    const v2, 0x7f020473

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 159
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Inline skating, normal"

    const v2, 0x7f020473

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 160
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Inline skating, fast"

    const v2, 0x7f020473

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 161
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Stretching, mild"

    const v2, 0x7f0204a0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 162
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Pilates"

    const v2, 0x7f02047c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 163
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Yoga"

    const v2, 0x7f0204a8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 164
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Aerobic, Low impact"

    const v2, 0x7f02044a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 165
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Aerobic, HighImpact"

    const v2, 0x7f02044a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 166
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Skipping (120~160/min)"

    const v2, 0x7f020475

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 167
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Skipping (100~120/min)"

    const v2, 0x7f020475

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 168
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Skipping (~100/min)"

    const v2, 0x7f020475

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 169
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Hula-hooping"

    const v2, 0x7f02046d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 170
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Rock climbing, High Difficulty"

    const v2, 0x7f020482

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 171
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Rock climbing, Low to Moderate Difficulty"

    const v2, 0x7f020482

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 172
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Tai chi, General"

    const v2, 0x7f02049e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 173
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Tai chi, Light Effort"

    const v2, 0x7f02049e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 174
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Hangliding"

    const v2, 0x7f020469

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 175
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Pistol shooting"

    const v2, 0x7f02047d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 176
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Archery, non-hunting "

    const v2, 0x7f02044c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 177
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Field hockey "

    const v2, 0x7f020465

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 178
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Ice hockey, general"

    const v2, 0x7f020470

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 179
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Hockey, ice, competitive"

    const v2, 0x7f020470

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 180
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Rugby, union, team, competitive"

    const v2, 0x7f020485

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 181
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Rugby, touch, non-competitive"

    const v2, 0x7f020485

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 182
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Golf"

    const v2, 0x7f020467

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 183
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Basketball, general"

    const v2, 0x7f020456

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 184
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Basketball, game"

    const v2, 0x7f020456

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 185
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Squash, general"

    const v2, 0x7f020496

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 186
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Ballroom dance, fast"

    const v2, 0x7f02044f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 187
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Ballroom dance, slow"

    const v2, 0x7f02044f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 188
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Baseball"

    const v2, 0x7f020455

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 189
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Football"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 190
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Football, Competitive"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 191
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Football, Casual"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 192
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Tennis, general"

    const v2, 0x7f02049f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 193
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Tennis, singles"

    const v2, 0x7f02049f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 194
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Tennis, doubles"

    const v2, 0x7f02049f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 195
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Badminton, competitive"

    const v2, 0x7f02044d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 196
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Badminton, social singles and doubles"

    const v2, 0x7f02044d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 197
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Billiard"

    const v2, 0x7f020458

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 198
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Boxing, in ring"

    const v2, 0x7f02045b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 199
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Boxing, punching bag"

    const v2, 0x7f02045b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 200
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Boxing, sparring"

    const v2, 0x7f02045b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 201
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Handball, general"

    const v2, 0x7f020468

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 202
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Handball, team"

    const v2, 0x7f020468

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 203
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Horseback riding "

    const v2, 0x7f02046b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 204
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Martial arts, moderate pace"

    const v2, 0x7f02049d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 205
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Martial arts, slower pace"

    const v2, 0x7f02049d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 206
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Softball, general "

    const v2, 0x7f020494

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 207
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Softball, practice\u00a0"

    const v2, 0x7f020494

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Softball, pitching"

    const v2, 0x7f020494

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 209
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Table tennis"

    const v2, 0x7f02049c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 210
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Table Tennis"

    const v2, 0x7f02049c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 211
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Volleyball, competitive"

    const v2, 0x7f0204a2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 212
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Volleyball, general"

    const v2, 0x7f0204a2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 213
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Step machine"

    const v2, 0x7f020499

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 214
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Weight machine"

    const v2, 0x7f0204a7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Bowling"

    const v2, 0x7f02045a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 218
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "WaterActivities"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 219
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Water Activities"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 220
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Water-Skiing"

    const v2, 0x7f0204a6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 221
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Free-style Swimming, Fast"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 222
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Free-style Swimming, Slow"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 223
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Backstroke, Training"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 224
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Backstroke, Recreational"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 225
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Breaststroke, Training"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 226
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Breaststroke, Recreational"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 227
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Butterfly Stroke Swimming"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 228
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Aquarobics"

    const v2, 0x7f02044b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 229
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Canoeing, rowing"

    const v2, 0x7f02045c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 230
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Sailing, yachting, for leisure"

    const v2, 0x7f020487

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 231
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Skindiving, fast"

    const v2, 0x7f02048d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 232
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Skindiving, moderate"

    const v2, 0x7f02048d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 233
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Skindiving, scuba diving, general, rowing"

    const v2, 0x7f02048d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 234
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Snorkeling, for leisure"

    const v2, 0x7f020491

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 237
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Cycling"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 238
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Bicycling"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 239
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Bicycling, Slow"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 240
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Bicycling, Moderate"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 241
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Bicycling, Fast"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 242
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Bicycling, Very Fast"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 243
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Stationary bicycle, very light to light effort (30-50 watts)"

    const v2, 0x7f020498

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 244
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Stationary bicycle, moderate to vigorous effort (90-100 watts)"

    const v2, 0x7f020498

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 245
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Stationary bicycle, vigorous effort (101-160 watts)"

    const v2, 0x7f020498

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 246
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Stationary bicycle, vigorous effort"

    const v2, 0x7f020498

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 247
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Stationary bicycle, 201-270 watts, very vigorous effort"

    const v2, 0x7f020498

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 248
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Stationary bicycle, 51-89 watts, light-to-moderate effort"

    const v2, 0x7f020498

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 251
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Winter Activities"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 252
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Skiing, Light"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 253
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Skiing, Moderate"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 254
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Skiing, Vigorous"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 255
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Snowboarding"

    const v2, 0x7f020492

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 256
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Snowboarding, Light"

    const v2, 0x7f020492

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 257
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Snowboarding, Moderate"

    const v2, 0x7f020492

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 258
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Ice dancing"

    const v2, 0x7f02046f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 259
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Ice skating, slow"

    const v2, 0x7f02048b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 260
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Ice skating, general"

    const v2, 0x7f02048b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 261
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Ice skating, rapidly"

    const v2, 0x7f02048b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 262
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Speed skating, competitive"

    const v2, 0x7f02048b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 265
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Strength"

    const v2, 0x7f0204a7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 266
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Strength Workouts"

    const v2, 0x7f0204a7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 267
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Strength Workout"

    const v2, 0x7f0204a7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 268
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Stength Workout"

    const v2, 0x7f0204a7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 269
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Push-up"

    const v2, 0x7f020480

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 270
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Pull-up"

    const v2, 0x7f02045d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 271
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Sit-up"

    const v2, 0x7f02048a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 272
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Dumbbell benchpress"

    const v2, 0x7f020460

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 273
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Barbell benchpress"

    const v2, 0x7f020450

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 274
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Flat bench dumbbell flye"

    const v2, 0x7f020466

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 275
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Incline barbell press"

    const v2, 0x7f020471

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 276
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Dumbbell lateral raise"

    const v2, 0x7f020462

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 277
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Smith machine upright row"

    const v2, 0x7f020490

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 278
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Dumbbell overhead press"

    const v2, 0x7f020463

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 279
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Barbell bent-over row"

    const v2, 0x7f020451

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 280
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Barbell upright row"

    const v2, 0x7f020454

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 281
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "One-arm dumbbell row"

    const v2, 0x7f02047b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 282
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Lat pulldown"

    const v2, 0x7f020476

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 283
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Barbell curl"

    const v2, 0x7f020452

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 284
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Preacher curl machine"

    const v2, 0x7f02047f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 285
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Incline dumbbell curl"

    const v2, 0x7f020472

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 286
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Lying barbell extension"

    const v2, 0x7f020479

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 287
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Rope pressdown"

    const v2, 0x7f020484

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 288
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Dumbbell kickback"

    const v2, 0x7f020461

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 289
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Leg extension"

    const v2, 0x7f020477

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 290
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Barbell squat"

    const v2, 0x7f020453

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 291
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Leg press"

    const v2, 0x7f020478

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 292
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Seated leg curl"

    const v2, 0x7f020489

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 293
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Romanian deadlift"

    const v2, 0x7f020483

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 294
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Lying leg curl"

    const v2, 0x7f02047a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 295
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Seated calf raise"

    const v2, 0x7f020488

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 296
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Standing calf raise"

    const v2, 0x7f020497

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 297
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Crunch"

    const v2, 0x7f02045e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 298
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Reverse crunch"

    const v2, 0x7f020481

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 299
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    return-void
.end method
