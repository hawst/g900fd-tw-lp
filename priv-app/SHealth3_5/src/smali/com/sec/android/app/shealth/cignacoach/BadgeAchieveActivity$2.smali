.class Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity$2;
.super Landroid/os/Handler;
.source "BadgeAchieveActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;)V
    .locals 0

    .prologue
    .line 339
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 342
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 344
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 382
    :goto_0
    return-void

    .line 347
    :pswitch_0
    # getter for: Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->access$300()Ljava/lang/String;

    move-result-object v0

    const-string v1, "BadgeAchieveActivity Finish Handle msg: HANDLE_MSG_INIT"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 348
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;

    # invokes: Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->setPostDelay()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->access$400(Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;)V

    goto :goto_0

    .line 353
    :pswitch_1
    # getter for: Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->access$300()Ljava/lang/String;

    move-result-object v0

    const-string v1, "BadgeAchieveActivity Finish Handle msg: HANDLE_MSG_NEXT"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 354
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;

    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v2

    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v3

    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mReceiveBadgeIds:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->access$500(Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/EearnedBadgeCountUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/EearnedBadgeCountUtils;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/cignacoach/util/EearnedBadgeCountUtils;->getRemainBadgeCount()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v2, v3, v0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getBadgeDataById(Landroid/content/Context;I)Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;

    move-result-object v0

    iput-object v0, v1, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mBadgeData:Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;

    .line 355
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/EearnedBadgeCountUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/EearnedBadgeCountUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/util/EearnedBadgeCountUtils;->reduceRemainBadgeCount()V

    .line 357
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;

    # invokes: Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->updateBadgeIcon()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->access$600(Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;)V

    .line 358
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;

    # invokes: Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->updateCompleteHeaderView()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->access$700(Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;)V

    .line 360
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;

    # invokes: Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->setPostDelay()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->access$400(Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;)V

    goto :goto_0

    .line 365
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mFinishDelayHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->access$200(Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 366
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mFinishDelayHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->access$200(Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 367
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mFinishDelayHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->access$200(Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->access$100(Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 369
    # getter for: Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->access$300()Ljava/lang/String;

    move-result-object v0

    const-string v1, "BadgeAchieveActivity Finish Handle msg: HANDLE_MSG_FINISH"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 370
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mHowShowScoreScreen:I
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->access$800(Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;)I

    move-result v0

    const/16 v1, 0x2b67

    if-ne v0, v1, :cond_1

    .line 371
    # getter for: Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->access$300()Ljava/lang/String;

    move-result-object v0

    const-string v1, "REQUEST_NEW_SCORE_SCREEN_AFTER_BADGE_SCREEN"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 372
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;

    # invokes: Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->startCignaCoachActivity()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->access$900(Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;)V

    .line 377
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->finish()V

    goto/16 :goto_0

    .line 373
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mHowShowScoreScreen:I
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->access$800(Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;)I

    move-result v0

    const/16 v1, 0x56ce

    if-ne v0, v1, :cond_0

    .line 374
    # getter for: Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->access$300()Ljava/lang/String;

    move-result-object v0

    const-string v1, "REQUEST_SET_RESULT_AFTER_BADGE_SCREEN"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 375
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->setResult(I)V

    goto :goto_1

    .line 344
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
