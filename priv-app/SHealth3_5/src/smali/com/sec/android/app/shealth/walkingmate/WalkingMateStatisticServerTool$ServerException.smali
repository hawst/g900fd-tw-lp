.class public Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ServerException;
.super Ljava/lang/Exception;
.source "WalkingMateStatisticServerTool.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ServerException"
.end annotation


# instance fields
.field private statusCode:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "reasonPhrase"    # Ljava/lang/String;

    .prologue
    .line 687
    invoke-direct {p0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 688
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .param p1, "reasonPhrase"    # Ljava/lang/String;
    .param p2, "statusCode"    # I

    .prologue
    .line 691
    invoke-direct {p0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 692
    iput p2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ServerException;->statusCode:I

    .line 693
    return-void
.end method


# virtual methods
.method public getStatusCode()I
    .locals 1

    .prologue
    .line 696
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ServerException;->statusCode:I

    return v0
.end method
