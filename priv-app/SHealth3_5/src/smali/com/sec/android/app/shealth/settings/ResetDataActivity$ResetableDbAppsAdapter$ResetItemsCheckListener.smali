.class Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter$ResetItemsCheckListener;
.super Ljava/lang/Object;
.source "ResetDataActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ResetItemsCheckListener"
.end annotation


# instance fields
.field mPosition:I

.field final synthetic this$1:Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter;I)V
    .locals 0
    .param p2, "position"    # I

    .prologue
    .line 442
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter$ResetItemsCheckListener;->this$1:Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 443
    iput p2, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter$ResetItemsCheckListener;->mPosition:I

    .line 444
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 448
    const v1, 0x7f0808ad

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 449
    .local v0, "clickedCheckBoxItem":Landroid/widget/CheckBox;
    if-eqz v0, :cond_0

    .line 450
    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 451
    invoke-virtual {v0, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 452
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter$ResetItemsCheckListener;->this$1:Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter;

    iget-object v1, v1, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter;->this$0:Lcom/sec/android/app/shealth/settings/ResetDataActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ResetDataActivity;->mcheckedItem:Ljava/util/Map;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->access$300(Lcom/sec/android/app/shealth/settings/ResetDataActivity;)Ljava/util/Map;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter$ResetItemsCheckListener;->mPosition:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 453
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter$ResetItemsCheckListener;->this$1:Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter;

    iget-object v1, v1, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter;->this$0:Lcom/sec/android/app/shealth/settings/ResetDataActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ResetDataActivity;->checkedDbResettableAppListItems:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->access$100(Lcom/sec/android/app/shealth/settings/ResetDataActivity;)Ljava/util/ArrayList;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter$ResetItemsCheckListener;->this$1:Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter;

    # getter for: Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter;->mDbResettableAppList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter;->access$400(Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter;)Ljava/util/ArrayList;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter$ResetItemsCheckListener;->mPosition:I

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 460
    :goto_0
    const/4 v1, 0x0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    invoke-static {p1, v1, v4, v2}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->announceForCheckBoxAccessibility(Landroid/view/View;Ljava/lang/String;ZZ)V

    .line 463
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter$ResetItemsCheckListener;->this$1:Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter;

    iget-object v1, v1, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter;->this$0:Lcom/sec/android/app/shealth/settings/ResetDataActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ResetDataActivity;->checkedDbResettableAppListItems:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->access$100(Lcom/sec/android/app/shealth/settings/ResetDataActivity;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_2

    .line 464
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter$ResetItemsCheckListener;->this$1:Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter;

    iget-object v1, v1, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter;->this$0:Lcom/sec/android/app/shealth/settings/ResetDataActivity;

    # invokes: Lcom/sec/android/app/shealth/settings/ResetDataActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->access$500(Lcom/sec/android/app/shealth/settings/ResetDataActivity;)Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    invoke-virtual {v1, v5, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarButtonVisiblity(ZI)V

    .line 465
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter$ResetItemsCheckListener;->this$1:Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter;

    iget-object v1, v1, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter;->this$0:Lcom/sec/android/app/shealth/settings/ResetDataActivity;

    # invokes: Lcom/sec/android/app/shealth/settings/ResetDataActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->access$600(Lcom/sec/android/app/shealth/settings/ResetDataActivity;)Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getActionBarLayout()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->invalidate()V

    .line 470
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter$ResetItemsCheckListener;->this$1:Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter;

    iget-object v1, v1, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter;->this$0:Lcom/sec/android/app/shealth/settings/ResetDataActivity;

    # invokes: Lcom/sec/android/app/shealth/settings/ResetDataActivity;->refreshFocusables()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->access$900(Lcom/sec/android/app/shealth/settings/ResetDataActivity;)V

    .line 471
    return-void

    .line 456
    :cond_1
    invoke-virtual {v0, v5}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 457
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter$ResetItemsCheckListener;->this$1:Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter;

    iget-object v1, v1, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter;->this$0:Lcom/sec/android/app/shealth/settings/ResetDataActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ResetDataActivity;->mcheckedItem:Ljava/util/Map;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->access$300(Lcom/sec/android/app/shealth/settings/ResetDataActivity;)Ljava/util/Map;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter$ResetItemsCheckListener;->mPosition:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 458
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter$ResetItemsCheckListener;->this$1:Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter;

    iget-object v1, v1, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter;->this$0:Lcom/sec/android/app/shealth/settings/ResetDataActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ResetDataActivity;->checkedDbResettableAppListItems:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->access$100(Lcom/sec/android/app/shealth/settings/ResetDataActivity;)Ljava/util/ArrayList;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter$ResetItemsCheckListener;->this$1:Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter;

    # getter for: Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter;->mDbResettableAppList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter;->access$400(Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter;)Ljava/util/ArrayList;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter$ResetItemsCheckListener;->mPosition:I

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 467
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter$ResetItemsCheckListener;->this$1:Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter;

    iget-object v1, v1, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter;->this$0:Lcom/sec/android/app/shealth/settings/ResetDataActivity;

    # invokes: Lcom/sec/android/app/shealth/settings/ResetDataActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->access$700(Lcom/sec/android/app/shealth/settings/ResetDataActivity;)Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    invoke-virtual {v1, v4, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarButtonVisiblity(ZI)V

    .line 468
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter$ResetItemsCheckListener;->this$1:Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter;

    iget-object v1, v1, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter;->this$0:Lcom/sec/android/app/shealth/settings/ResetDataActivity;

    # invokes: Lcom/sec/android/app/shealth/settings/ResetDataActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->access$800(Lcom/sec/android/app/shealth/settings/ResetDataActivity;)Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getActionBarLayout()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->invalidate()V

    goto :goto_1
.end method
