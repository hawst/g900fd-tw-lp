.class Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$16;
.super Ljava/lang/Object;
.source "DataMigrationHelper.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)V
    .locals 0

    .prologue
    .line 941
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$16;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDialogButtonClick(Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;)V
    .locals 2
    .param p1, "dialogButtonType"    # Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    .prologue
    .line 945
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$16;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;->NONE:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->popupStatus:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->access$502(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;)Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    .line 947
    sget-object v0, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->POSITIVE:Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    if-ne p1, v0, :cond_1

    .line 949
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$16;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->access$300(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "MIGRATION - Type2 Step 5. - OK - Goto Step 6."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 951
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$16;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->doMigrationStartFrom2xto25()V

    .line 958
    :cond_0
    :goto_0
    return-void

    .line 953
    :cond_1
    sget-object v0, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->NEGATIVE:Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    if-ne p1, v0, :cond_0

    .line 955
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$16;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->access$300(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "MIGRATION - Type2 Step 5. - Cancel - Exit app."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 956
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$16;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->exitApplication()V

    goto :goto_0
.end method
