.class public Lcom/sec/android/app/shealth/walkingmate/cocktailwidget/WalkingMateCocktailWidgetReceiver;
.super Landroid/content/BroadcastReceiver;
.source "WalkingMateCocktailWidgetReceiver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/walkingmate/cocktailwidget/WalkingMateCocktailWidgetReceiver$UpdateCocktailWidget;
    }
.end annotation


# static fields
.field private static final STEP_GOAL:Ljava/lang/String; = "STEP_GOAL"

.field private static final SYNC_CHECK:Ljava/lang/String; = "android.intent.action.WEARABLE_DEVICE_SYNC_CHECK"

.field private static final TAG:Ljava/lang/String; = "WalkingMateCocktailWidgetReceiver"

.field private static final UPDATE:Ljava/lang/String; = "com.sec.android.app.shealth.walkingmate.cocktailwidget.UPDATE"

.field private static final UPDATE_CAL:Ljava/lang/String; = "CAL"

.field private static final UPDATE_CAL_UNIT:Ljava/lang/String; = "CAL_UNIT"

.field private static final UPDATE_CURRENT_MODE:Ljava/lang/String; = "UPDATE_CURRENT_MODE"

.field private static final UPDATE_DISTANCE:Ljava/lang/String; = "DISTANCE"

.field private static final UPDATE_DISTANCE_UNIT:Ljava/lang/String; = "DISTANCE_UNIT"

.field private static final UPDATE_STEP:Ljava/lang/String; = "STEP"

.field private static final UPDATE_STEP_UNIT:Ljava/lang/String; = "STEP_UNIT"

.field private static mDecimalFormat:Ljava/text/DecimalFormat;

.field private static mWalkingMateCocktailWidgetReceiver:Lcom/sec/android/app/shealth/walkingmate/cocktailwidget/WalkingMateCocktailWidgetReceiver;


# instance fields
.field private mTotalSteps:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 86
    sget-object v0, Ljava/util/Locale;->UK:Ljava/util/Locale;

    invoke-static {v0}, Ljava/text/NumberFormat;->getNumberInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v0

    check-cast v0, Ljava/text/DecimalFormat;

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/cocktailwidget/WalkingMateCocktailWidgetReceiver;->mDecimalFormat:Ljava/text/DecimalFormat;

    .line 88
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/cocktailwidget/WalkingMateCocktailWidgetReceiver;->mWalkingMateCocktailWidgetReceiver:Lcom/sec/android/app/shealth/walkingmate/cocktailwidget/WalkingMateCocktailWidgetReceiver;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 90
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 93
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/cocktailwidget/WalkingMateCocktailWidgetReceiver;->mTotalSteps:I

    .line 91
    sput-object p0, Lcom/sec/android/app/shealth/walkingmate/cocktailwidget/WalkingMateCocktailWidgetReceiver;->mWalkingMateCocktailWidgetReceiver:Lcom/sec/android/app/shealth/walkingmate/cocktailwidget/WalkingMateCocktailWidgetReceiver;

    .line 92
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/walkingmate/cocktailwidget/WalkingMateCocktailWidgetReceiver;Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/cocktailwidget/WalkingMateCocktailWidgetReceiver;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Landroid/content/Intent;

    .prologue
    .line 70
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/walkingmate/cocktailwidget/WalkingMateCocktailWidgetReceiver;->updateWidget(Landroid/content/Context;Landroid/content/Intent;)V

    return-void
.end method

.method public static getEdgePanelPendingIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 10
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/high16 v9, 0x14010000

    const/4 v8, 0x1

    .line 487
    const/4 v1, 0x0

    .line 489
    .local v1, "intent":Landroid/content/Intent;
    const/4 v0, 0x0

    .line 491
    .local v0, "initializationNeeded":Z
    const-string v6, "WalkingMateCocktailWidgetReceiver"

    const-string v7, "getEdgePanelPendingIntent called"

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 492
    if-eqz p0, :cond_0

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/MemoryStatusUtils;->isNotEnoughMemory()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 493
    :cond_0
    const-string v6, "WalkingMateCocktailWidgetReceiver"

    const-string v7, "getEdgePanelPendingIntent called but Notenough memory or context is null"

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    move-object v2, v1

    .line 536
    .end local v1    # "intent":Landroid/content/Intent;
    .local v2, "intent":Ljava/lang/Object;
    :goto_0
    return-object v2

    .line 496
    .end local v2    # "intent":Ljava/lang/Object;
    .restart local v1    # "intent":Landroid/content/Intent;
    :cond_1
    new-instance v5, Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    invoke-direct {v5, p0}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;-><init>(Landroid/content/Context;)V

    .line 497
    .local v5, "shcm":Lcom/samsung/android/sdk/health/content/ShealthContentManager;
    if-eqz v5, :cond_2

    invoke-virtual {v5}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->isBackUpRestoreInProgress()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 498
    const-string v6, "WalkingMateCocktailWidgetReceiver"

    const-string v7, "disable launch via widget: Backup or Restore is ongoing"

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    move-object v2, v1

    .line 499
    .restart local v2    # "intent":Ljava/lang/Object;
    goto :goto_0

    .line 502
    .end local v2    # "intent":Ljava/lang/Object;
    :cond_2
    invoke-static {p0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isInitializationNeeded(Landroid/content/Context;)Z

    move-result v0

    .line 503
    const/4 v4, 0x0

    .line 504
    .local v4, "migrationState":I
    if-nez v0, :cond_3

    .line 505
    invoke-static {p0}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->getMigrationState(Landroid/content/Context;)I

    move-result v4

    .line 506
    if-eqz v4, :cond_3

    .line 507
    const/4 v0, 0x1

    .line 511
    :cond_3
    sget-boolean v6, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mIsProgressCompleted:Z

    if-eqz v6, :cond_4

    .line 512
    const-string v6, "WalkingMateCocktailWidgetReceiver"

    const-string/jumbo v7, "mIsProgressCompleted is true, go to RestoreActivity"

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 513
    new-instance v3, Landroid/content/Intent;

    const-class v6, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    invoke-direct {v3, p0, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 514
    .local v3, "launcherintent":Landroid/content/Intent;
    invoke-virtual {v3, v9}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-object v2, v1

    .line 515
    .restart local v2    # "intent":Ljava/lang/Object;
    goto :goto_0

    .line 517
    .end local v2    # "intent":Ljava/lang/Object;
    .end local v3    # "launcherintent":Landroid/content/Intent;
    :cond_4
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v6

    if-eqz v6, :cond_5

    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/service/health/keyManager/KeyManager;->isContentProviderAccessible()Z

    move-result v6

    if-eqz v6, :cond_5

    if-eqz v0, :cond_6

    .line 519
    :cond_5
    const-string v6, "WalkingMateCocktailWidgetReceiver"

    const-string v7, "initializationNeeded"

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 520
    new-instance v1, Landroid/content/Intent;

    .end local v1    # "intent":Landroid/content/Intent;
    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 521
    .restart local v1    # "intent":Landroid/content/Intent;
    const-string v6, "com.sec.android.app.shealth"

    invoke-virtual {v1, v6}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 522
    const-string v6, "launchWidget"

    invoke-virtual {v1, v6, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 523
    const-class v6, Lcom/sec/android/app/shealth/SplashScreenActivity;

    invoke-virtual {v1, p0, v6}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 524
    invoke-virtual {v1, v9}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-object v2, v1

    .line 525
    .restart local v2    # "intent":Ljava/lang/Object;
    goto :goto_0

    .line 527
    .end local v2    # "intent":Ljava/lang/Object;
    :cond_6
    invoke-static {}, Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;->getInstance()Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;

    move-result-object v6

    invoke-virtual {v6, v8}, Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;->setInitialized(Z)V

    .line 528
    new-instance v1, Landroid/content/Intent;

    .end local v1    # "intent":Landroid/content/Intent;
    const-string v6, "com.sec.shealth.HomeActivity"

    invoke-direct {v1, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 529
    .restart local v1    # "intent":Landroid/content/Intent;
    const-string v6, "com.sec.android.app.shealth"

    invoke-virtual {v1, v6}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 530
    const-string v6, "launchWidget"

    invoke-virtual {v1, v6, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 531
    const-string/jumbo v6, "widgetActivityAction"

    const-string v7, "com.sec.shealth.action.PEDOMETER"

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 532
    const-string/jumbo v6, "widgetActivityPackage"

    const-string v7, "com.sec.android.app.shealth"

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 533
    invoke-virtual {v1, v9}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 534
    const-string v6, "WalkingMateCocktailWidgetReceiver"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "BroadCastReceived  "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move-object v2, v1

    .line 536
    .restart local v2    # "intent":Ljava/lang/Object;
    goto/16 :goto_0
.end method

.method private setVisibility(IILjava/lang/String;ZZLandroid/widget/RemoteViews;)V
    .locals 8
    .param p1, "totalStep"    # I
    .param p2, "goal"    # I
    .param p3, "mode"    # Ljava/lang/String;
    .param p4, "connected"    # Z
    .param p5, "isSyncing"    # Z
    .param p6, "remoteViews"    # Landroid/widget/RemoteViews;

    .prologue
    const v7, 0x7f080b6d

    const v6, 0x7f080b75

    const v5, 0x7f080b64

    const/4 v4, 0x0

    const/16 v3, 0x8

    .line 204
    const v1, 0x7f080b78

    const/4 v2, 0x4

    invoke-virtual {p6, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 205
    const v1, 0x7f080b65

    invoke-virtual {p6, v1, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 206
    invoke-virtual {p6, v7, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 207
    const v1, 0x7f080b68

    invoke-virtual {p6, v1, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 208
    invoke-virtual {p6, v6, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 209
    const v1, 0x7f080b70

    invoke-virtual {p6, v1, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 210
    invoke-virtual {p6, v6, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 211
    const v1, 0x7f080b76

    invoke-virtual {p6, v1, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 212
    const-string/jumbo v1, "setBackgroundResource"

    const v2, 0x7f020160

    invoke-virtual {p6, v5, v1, v2}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 214
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isInitializationNeeded(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 215
    const v1, 0x7f080b67

    invoke-virtual {p6, v1, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 216
    const v1, 0x7f080b66

    invoke-virtual {p6, v1, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 219
    :cond_0
    const-string v1, "gear"

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 220
    const-string v1, "WalkingMateCocktailWidgetReceiver"

    const-string v2, "gear mode"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->getSynchronizedWearableList(J)Ljava/util/ArrayList;

    move-result-object v0

    .line 224
    .local v0, "deviceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    if-eqz v0, :cond_3

    .line 225
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_1

    .line 226
    invoke-virtual {p6, v7, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 227
    const v1, 0x7f080b68

    invoke-virtual {p6, v1, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 230
    :cond_1
    const-string/jumbo v1, "setBackgroundResource"

    const v2, 0x7f020161

    invoke-virtual {p6, v5, v1, v2}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 231
    if-nez p1, :cond_4

    .line 232
    const v1, 0x7f080b65

    invoke-virtual {p6, v1, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 233
    invoke-virtual {p6, v7, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 234
    invoke-virtual {p6, v6, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 235
    const v1, 0x7f080b68

    invoke-virtual {p6, v1, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 240
    :cond_2
    :goto_0
    if-eqz p4, :cond_6

    .line 241
    const-string v1, "WalkingMateCocktailWidgetReceiver"

    const-string/jumbo v2, "sync enabled"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 242
    const v1, 0x7f080b78

    invoke-virtual {p6, v1, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 244
    if-eqz p5, :cond_5

    .line 245
    const-string v1, "WalkingMateCocktailWidgetReceiver"

    const-string/jumbo v2, "syncing"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 246
    const v1, 0x7f080b79

    invoke-virtual {p6, v1, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 247
    const v1, 0x7f080520

    invoke-virtual {p6, v1, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 279
    .end local v0    # "deviceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :cond_3
    :goto_1
    return-void

    .line 236
    .restart local v0    # "deviceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :cond_4
    if-lt p1, p2, :cond_2

    .line 237
    const-string/jumbo v1, "setBackgroundResource"

    const v2, 0x7f020772

    invoke-virtual {p6, v5, v1, v2}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    goto :goto_0

    .line 249
    :cond_5
    const-string v1, "WalkingMateCocktailWidgetReceiver"

    const-string/jumbo v2, "synced"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 250
    const v1, 0x7f080b79

    invoke-virtual {p6, v1, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 251
    const v1, 0x7f080520

    invoke-virtual {p6, v1, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_1

    .line 254
    :cond_6
    const-string v1, "WalkingMateCocktailWidgetReceiver"

    const-string/jumbo v2, "sync disabled"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 255
    const v1, 0x7f080b78

    const/4 v2, 0x4

    invoke-virtual {p6, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_1

    .line 258
    .end local v0    # "deviceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :cond_7
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isInitializationNeeded(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_8

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->isStartWalking()Z

    move-result v1

    if-nez v1, :cond_8

    .line 259
    const v1, 0x7f080b70

    invoke-virtual {p6, v1, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 260
    invoke-virtual {p6, v6, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 261
    const v1, 0x7f080b76

    invoke-virtual {p6, v1, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 262
    const-string/jumbo v1, "setBackgroundResource"

    const v2, 0x7f020164

    invoke-virtual {p6, v5, v1, v2}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    goto :goto_1

    .line 263
    :cond_8
    const-string/jumbo v1, "normal"

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 264
    if-nez p1, :cond_3

    .line 265
    const v1, 0x7f080b65

    invoke-virtual {p6, v1, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 266
    invoke-virtual {p6, v7, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 267
    invoke-virtual {p6, v6, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_1

    .line 269
    :cond_9
    const-string v1, "healthy"

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 270
    const-string/jumbo v1, "setBackgroundResource"

    const v2, 0x7f020162

    invoke-virtual {p6, v5, v1, v2}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    goto/16 :goto_1

    .line 271
    :cond_a
    const-string v1, "inactive"

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 272
    const-string/jumbo v1, "setBackgroundResource"

    const v2, 0x7f020163

    invoke-virtual {p6, v5, v1, v2}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    goto/16 :goto_1

    .line 273
    :cond_b
    const-string/jumbo v1, "medal"

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 274
    if-eqz p1, :cond_3

    .line 275
    const-string/jumbo v1, "setBackgroundResource"

    const v2, 0x7f02076a

    invoke-virtual {p6, v5, v1, v2}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    goto/16 :goto_1
.end method

.method public static updateCocktailWidget(Landroid/content/Context;)V
    .locals 12
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 433
    new-instance v4, Landroid/content/Intent;

    const-string v9, "com.sec.android.app.shealth.walkingmate.cocktailwidget.UPDATE"

    invoke-direct {v4, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 435
    .local v4, "intent":Landroid/content/Intent;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getTotalStep()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    .line 436
    .local v5, "step":Ljava/lang/String;
    sget-object v9, Lcom/sec/android/app/shealth/walkingmate/cocktailwidget/WalkingMateCocktailWidgetReceiver;->mDecimalFormat:Ljava/text/DecimalFormat;

    const-string v10, "0"

    invoke-virtual {v9, v10}, Ljava/text/DecimalFormat;->applyPattern(Ljava/lang/String;)V

    .line 437
    sget-object v9, Lcom/sec/android/app/shealth/walkingmate/cocktailwidget/WalkingMateCocktailWidgetReceiver;->mDecimalFormat:Ljava/text/DecimalFormat;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v10

    invoke-static {v10}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getCal()F

    move-result v10

    float-to-double v10, v10

    invoke-static {v10, v11}, Ljava/lang/Math;->floor(D)D

    move-result-wide v10

    invoke-virtual {v9, v10, v11}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    .line 438
    .local v1, "cal":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getGoal()I

    move-result v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    .line 440
    .local v6, "stepGoal":Ljava/lang/String;
    const-string v9, "STEP"

    invoke-virtual {v4, v9, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 441
    const-string v9, "STEP_UNIT"

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0907e5

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 442
    const-string v9, "STEP_GOAL"

    invoke-virtual {v4, v9, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 443
    const-string v9, "CAL"

    invoke-virtual {v4, v9, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 444
    const-string v9, "CAL_UNIT"

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0900b9

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 446
    new-instance v7, Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    invoke-direct {v7, p0}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;-><init>(Landroid/content/Context;)V

    .line 447
    .local v7, "unitHelper":Lcom/sec/android/app/shealth/common/utils/UnitHelper;
    invoke-virtual {v7}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->getDistanceUnit()Ljava/lang/String;

    move-result-object v3

    .line 449
    .local v3, "distanceUnit":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getDistance()F

    move-result v0

    .line 451
    .local v0, "actualDistance":F
    const v8, 0x7f0900c7

    .line 452
    .local v8, "unitId":I
    const-string/jumbo v9, "mi"

    invoke-virtual {v3, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 453
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/UnitUtils$UnitConverter;->convertKgIntoMiles(F)F

    move-result v0

    .line 454
    const v8, 0x7f0900cc

    .line 456
    :cond_0
    const/high16 v9, 0x41200000    # 10.0f

    div-float/2addr v0, v9

    .line 457
    float-to-double v9, v0

    invoke-static {v9, v10}, Ljava/lang/Math;->floor(D)D

    move-result-wide v9

    double-to-float v0, v9

    .line 458
    const/high16 v9, 0x42c80000    # 100.0f

    div-float/2addr v0, v9

    .line 459
    sget-object v9, Lcom/sec/android/app/shealth/walkingmate/cocktailwidget/WalkingMateCocktailWidgetReceiver;->mDecimalFormat:Ljava/text/DecimalFormat;

    const-string v10, "0.00"

    invoke-virtual {v9, v10}, Ljava/text/DecimalFormat;->applyPattern(Ljava/lang/String;)V

    .line 460
    sget-object v9, Lcom/sec/android/app/shealth/walkingmate/cocktailwidget/WalkingMateCocktailWidgetReceiver;->mDecimalFormat:Ljava/text/DecimalFormat;

    float-to-double v10, v0

    invoke-virtual {v9, v10, v11}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v2

    .line 461
    .local v2, "distance":Ljava/lang/String;
    const-string v9, "DISTANCE"

    invoke-virtual {v4, v9, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 462
    const-string v9, "DISTANCE_UNIT"

    invoke-virtual {p0, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 463
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->isHealthyStep()Z

    move-result v9

    if-eqz v9, :cond_2

    .line 464
    const-string v9, "UPDATE_CURRENT_MODE"

    const-string v10, "healthy"

    invoke-virtual {v4, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 473
    :goto_0
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v9

    const/16 v10, 0x2719

    if-eq v9, v10, :cond_1

    .line 474
    const-string v9, "UPDATE_CURRENT_MODE"

    const-string v10, "gear"

    invoke-virtual {v4, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 475
    const-string v9, "SYNC"

    invoke-static {p0}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateSummaryUtils;->isWearableConnected(Landroid/content/Context;)Z

    move-result v10

    invoke-virtual {v4, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 476
    const-string v9, "SYNCING"

    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->isSyncing()Z

    move-result v10

    invoke-virtual {v4, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 479
    :cond_1
    sget-object v9, Lcom/sec/android/app/shealth/walkingmate/cocktailwidget/WalkingMateCocktailWidgetReceiver;->mWalkingMateCocktailWidgetReceiver:Lcom/sec/android/app/shealth/walkingmate/cocktailwidget/WalkingMateCocktailWidgetReceiver;

    if-eqz v9, :cond_5

    .line 480
    sget-object v9, Lcom/sec/android/app/shealth/walkingmate/cocktailwidget/WalkingMateCocktailWidgetReceiver;->mWalkingMateCocktailWidgetReceiver:Lcom/sec/android/app/shealth/walkingmate/cocktailwidget/WalkingMateCocktailWidgetReceiver;

    invoke-virtual {v9, p0, v4}, Lcom/sec/android/app/shealth/walkingmate/cocktailwidget/WalkingMateCocktailWidgetReceiver;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    .line 484
    :goto_1
    return-void

    .line 465
    :cond_2
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->isInactivetime()Z

    move-result v9

    if-eqz v9, :cond_3

    .line 466
    const-string v9, "UPDATE_CURRENT_MODE"

    const-string v10, "inactive"

    invoke-virtual {v4, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 467
    :cond_3
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getTotalStep()I

    move-result v9

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v10

    invoke-static {v10}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getGoal()I

    move-result v10

    if-lt v9, v10, :cond_4

    .line 468
    const-string v9, "UPDATE_CURRENT_MODE"

    const-string/jumbo v10, "medal"

    invoke-virtual {v4, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 470
    :cond_4
    const-string v9, "UPDATE_CURRENT_MODE"

    const-string/jumbo v10, "normal"

    invoke-virtual {v4, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 482
    :cond_5
    invoke-virtual {p0, v4}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_1
.end method

.method private declared-synchronized updateWidget(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 38
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 282
    monitor-enter p0

    :try_start_0
    new-instance v9, Landroid/widget/RemoteViews;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const v32, 0x7f030285

    move/from16 v0, v32

    invoke-direct {v9, v3, v0}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 284
    .local v9, "rv":Landroid/widget/RemoteViews;
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v10

    .line 285
    .local v10, "actionName":Ljava/lang/String;
    if-eqz v10, :cond_0

    const-string v3, "com.sec.android.app.shealth.walkingmate.cocktailwidget.UPDATE"

    invoke-virtual {v10, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-nez v3, :cond_1

    .line 404
    :cond_0
    monitor-exit p0

    return-void

    .line 294
    :cond_1
    :try_start_1
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v32, 0x7f0900b9

    move/from16 v0, v32

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 295
    .local v13, "caloriesUnitTxt":Ljava/lang/String;
    const v3, 0x7f0901c3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 296
    .local v17, "distanceUnitTTS":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v32, 0x7f0901cb

    move/from16 v0, v32

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 297
    .local v12, "caloriesUnitTTS":Ljava/lang/String;
    const v16, 0x7f0900c7

    .line 299
    .local v16, "distanceUnitId":I
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->isRun()Z

    move-result v3

    if-nez v3, :cond_3

    .line 300
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v9}, Lcom/sec/android/app/shealth/walkingmate/cocktailwidget/WalkingMateCocktailWidgetReceiver;->displayDataFromDB(Landroid/content/Context;Landroid/widget/RemoteViews;)V

    .line 383
    :goto_0
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/shealth/walkingmate/cocktailwidget/WalkingMateCocktailWidgetReceiver;->getEdgePanelPendingIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v27

    .line 385
    .local v27, "pedoLaunchIntent":Landroid/content/Intent;
    new-instance v25, Landroid/content/Intent;

    const-class v3, Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver;

    move-object/from16 v0, v25

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 386
    .local v25, "intentSync":Landroid/content/Intent;
    const-string v3, "android.intent.action.WEARABLE_DEVICE_SYNC_CHECK"

    move-object/from16 v0, v25

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 387
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const/16 v32, 0x1ca8

    const/high16 v33, 0x8000000

    move/from16 v0, v32

    move-object/from16 v1, v25

    move/from16 v2, v33

    invoke-static {v3, v0, v1, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v29

    .line 388
    .local v29, "pendingIntentSync":Landroid/app/PendingIntent;
    if-eqz v27, :cond_2

    .line 389
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const/16 v32, 0x1ca8

    const/high16 v33, 0x8000000

    move/from16 v0, v32

    move-object/from16 v1, v27

    move/from16 v2, v33

    invoke-static {v3, v0, v1, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v28

    .line 390
    .local v28, "pendingIntent":Landroid/app/PendingIntent;
    const v3, 0x7f080b64

    move-object/from16 v0, v28

    invoke-virtual {v9, v3, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 391
    const v3, 0x7f080b6d

    move-object/from16 v0, v28

    invoke-virtual {v9, v3, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 392
    const v3, 0x7f080b68

    move-object/from16 v0, v28

    invoke-virtual {v9, v3, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 393
    const v3, 0x7f080b65

    move-object/from16 v0, v28

    invoke-virtual {v9, v3, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 395
    .end local v28    # "pendingIntent":Landroid/app/PendingIntent;
    :cond_2
    const v3, 0x7f080b78

    move-object/from16 v0, v29

    invoke-virtual {v9, v3, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 397
    invoke-static/range {p1 .. p1}, Lcom/samsung/android/sdk/look/cocktailbar/SlookCocktailManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/look/cocktailbar/SlookCocktailManager;

    move-result-object v26

    .line 398
    .local v26, "manager":Lcom/samsung/android/sdk/look/cocktailbar/SlookCocktailManager;
    new-instance v3, Lcom/samsung/android/sdk/look/cocktailbar/SlookCocktailManager$CocktailInfo$Builder;

    invoke-direct {v3, v9}, Lcom/samsung/android/sdk/look/cocktailbar/SlookCocktailManager$CocktailInfo$Builder;-><init>(Landroid/widget/RemoteViews;)V

    invoke-virtual {v3}, Lcom/samsung/android/sdk/look/cocktailbar/SlookCocktailManager$CocktailInfo$Builder;->build()Lcom/samsung/android/sdk/look/cocktailbar/SlookCocktailManager$CocktailInfo;

    move-result-object v23

    .line 399
    .local v23, "info":Lcom/samsung/android/sdk/look/cocktailbar/SlookCocktailManager$CocktailInfo;
    new-instance v3, Landroid/content/ComponentName;

    const-class v32, Lcom/sec/android/app/shealth/walkingmate/cocktailwidget/WalkingMateCocktailWidgetProvider;

    move-object/from16 v0, p1

    move-object/from16 v1, v32

    invoke-direct {v3, v0, v1}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/look/cocktailbar/SlookCocktailManager;->getCocktailIds(Landroid/content/ComponentName;)[I

    move-result-object v22

    .line 401
    .local v22, "ids":[I
    const/16 v21, 0x0

    .local v21, "i":I
    :goto_1
    move-object/from16 v0, v22

    array-length v3, v0

    move/from16 v0, v21

    if-ge v0, v3, :cond_0

    .line 402
    aget v3, v22, v21

    move-object/from16 v0, v26

    move-object/from16 v1, v23

    invoke-virtual {v0, v3, v1}, Lcom/samsung/android/sdk/look/cocktailbar/SlookCocktailManager;->updateCocktail(ILcom/samsung/android/sdk/look/cocktailbar/SlookCocktailManager$CocktailInfo;)V

    .line 401
    add-int/lit8 v21, v21, 0x1

    goto :goto_1

    .line 302
    .end local v21    # "i":I
    .end local v22    # "ids":[I
    .end local v23    # "info":Lcom/samsung/android/sdk/look/cocktailbar/SlookCocktailManager$CocktailInfo;
    .end local v25    # "intentSync":Landroid/content/Intent;
    .end local v26    # "manager":Lcom/samsung/android/sdk/look/cocktailbar/SlookCocktailManager;
    .end local v27    # "pedoLaunchIntent":Landroid/content/Intent;
    .end local v29    # "pendingIntentSync":Landroid/app/PendingIntent;
    :cond_3
    const-string v3, "STEP"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    .line 303
    .local v30, "stepsTxt":Ljava/lang/String;
    const-string v3, "STEP_UNIT"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v31

    .line 304
    .local v31, "stepsUnitTxt":Ljava/lang/String;
    const-string v3, "DISTANCE"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 305
    .local v15, "distanceTxt":Ljava/lang/String;
    const-string v3, "DISTANCE_UNIT"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 307
    .local v18, "distanceUnitTxt":Ljava/lang/String;
    const-string/jumbo v3, "mi"

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 308
    const v16, 0x7f0900cc

    .line 309
    const v3, 0x7f0900cb

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 311
    :cond_4
    const-string v3, "CAL"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 312
    .local v11, "caloriesTxt":Ljava/lang/String;
    const-string v3, "CAL_UNIT"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 313
    const-string v3, "STEP_GOAL"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 315
    .local v20, "goal":Ljava/lang/String;
    const-string v3, "WalkingMateCocktailWidgetReceiver"

    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    const-string v33, "Update: "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v32

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, " "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v32

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, " "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v32

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, " "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v32

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, " "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v32

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, " "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v32

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v32

    invoke-static {v3, v0}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 316
    const v3, 0x7f080b6e

    move-object/from16 v0, v30

    invoke-virtual {v9, v3, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 318
    const v3, 0x7f09008a

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v24

    .line 319
    .local v24, "integrated_gear_step":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v24

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v32, ": "

    move-object/from16 v0, v32

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    .line 320
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v24

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v30

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    .line 322
    const v3, 0x7f080b69

    move-object/from16 v0, v24

    invoke-virtual {v9, v3, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 323
    const v3, 0x7f080b6f

    move-object/from16 v0, v31

    invoke-virtual {v9, v3, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 324
    const v3, 0x7f080b71

    invoke-virtual {v9, v3, v15}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 325
    const v3, 0x7f080b72

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v32

    invoke-virtual {v9, v3, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 326
    const v3, 0x7f080b73

    invoke-virtual {v9, v3, v11}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 327
    const v3, 0x7f080b74

    invoke-virtual {v9, v3, v13}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 329
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isInitializationNeeded(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_8

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->isStartWalking()Z

    move-result v3

    if-nez v3, :cond_8

    .line 331
    const v3, 0x7f080b6d

    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v32

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v32

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v33

    const v34, 0x7f090bf3

    invoke-virtual/range {v33 .. v34}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v33

    const/16 v34, 0x1

    move/from16 v0, v34

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v34, v0

    const/16 v35, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v36

    const v37, 0x7f090020

    invoke-virtual/range {v36 .. v37}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v36

    aput-object v36, v34, v35

    invoke-static/range {v33 .. v34}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v33

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v32

    invoke-virtual {v9, v3, v0}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    .line 339
    :goto_2
    const-string v3, "UPDATE_CURRENT_MODE"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 340
    .local v6, "currentMode":Ljava/lang/String;
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 341
    .local v5, "nGoal":I
    invoke-static/range {v30 .. v30}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 342
    .local v4, "nStep":I
    const/4 v8, 0x0

    .line 343
    .local v8, "syncing":Z
    const/4 v7, 0x0

    .line 344
    .local v7, "connected":Z
    const-string v3, "gear"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 345
    const-string v3, "WalkingMateCocktailWidgetReceiver"

    const-string v32, "gear mode"

    move-object/from16 v0, v32

    invoke-static {v3, v0}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 346
    const-string v3, "SYNC"

    const/16 v32, 0x0

    move-object/from16 v0, p2

    move/from16 v1, v32

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 347
    const/4 v7, 0x1

    .line 349
    :cond_5
    const-string v3, "SYNCING"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    const-string v3, "SYNCING"

    const/16 v32, 0x0

    move-object/from16 v0, p2

    move/from16 v1, v32

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 350
    const/4 v8, 0x1

    .line 354
    :cond_6
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v32

    invoke-static/range {v32 .. v33}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->getSynchronizedWearableList(J)Ljava/util/ArrayList;

    move-result-object v14

    .line 356
    .local v14, "deviceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    if-eqz v14, :cond_7

    .line 357
    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/16 v32, 0x1

    move/from16 v0, v32

    if-le v3, v0, :cond_7

    .line 358
    const-string v19, ""

    .line 359
    .local v19, "first_gear_step":Ljava/lang/String;
    const/4 v3, 0x0

    invoke-virtual {v14, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 377
    :goto_3
    :pswitch_0
    const v3, 0x7f080b6b

    move-object/from16 v0, v19

    invoke-virtual {v9, v3, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .end local v19    # "first_gear_step":Ljava/lang/String;
    :cond_7
    move-object/from16 v3, p0

    .line 380
    invoke-direct/range {v3 .. v9}, Lcom/sec/android/app/shealth/walkingmate/cocktailwidget/WalkingMateCocktailWidgetReceiver;->setVisibility(IILjava/lang/String;ZZLandroid/widget/RemoteViews;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 282
    .end local v4    # "nStep":I
    .end local v5    # "nGoal":I
    .end local v6    # "currentMode":Ljava/lang/String;
    .end local v7    # "connected":Z
    .end local v8    # "syncing":Z
    .end local v9    # "rv":Landroid/widget/RemoteViews;
    .end local v10    # "actionName":Ljava/lang/String;
    .end local v11    # "caloriesTxt":Ljava/lang/String;
    .end local v12    # "caloriesUnitTTS":Ljava/lang/String;
    .end local v13    # "caloriesUnitTxt":Ljava/lang/String;
    .end local v14    # "deviceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v15    # "distanceTxt":Ljava/lang/String;
    .end local v16    # "distanceUnitId":I
    .end local v17    # "distanceUnitTTS":Ljava/lang/String;
    .end local v18    # "distanceUnitTxt":Ljava/lang/String;
    .end local v20    # "goal":Ljava/lang/String;
    .end local v24    # "integrated_gear_step":Ljava/lang/String;
    .end local v30    # "stepsTxt":Ljava/lang/String;
    .end local v31    # "stepsUnitTxt":Ljava/lang/String;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 334
    .restart local v9    # "rv":Landroid/widget/RemoteViews;
    .restart local v10    # "actionName":Ljava/lang/String;
    .restart local v11    # "caloriesTxt":Ljava/lang/String;
    .restart local v12    # "caloriesUnitTTS":Ljava/lang/String;
    .restart local v13    # "caloriesUnitTxt":Ljava/lang/String;
    .restart local v15    # "distanceTxt":Ljava/lang/String;
    .restart local v16    # "distanceUnitId":I
    .restart local v17    # "distanceUnitTTS":Ljava/lang/String;
    .restart local v18    # "distanceUnitTxt":Ljava/lang/String;
    .restart local v20    # "goal":Ljava/lang/String;
    .restart local v24    # "integrated_gear_step":Ljava/lang/String;
    .restart local v30    # "stepsTxt":Ljava/lang/String;
    .restart local v31    # "stepsUnitTxt":Ljava/lang/String;
    :cond_8
    const v3, 0x7f080b6d

    :try_start_2
    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v32

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v32

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v32

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v32

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v32

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v32

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v33

    const v34, 0x7f090bf3

    invoke-virtual/range {v33 .. v34}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v33

    const/16 v34, 0x1

    move/from16 v0, v34

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v34, v0

    const/16 v35, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v36

    const v37, 0x7f090020

    invoke-virtual/range {v36 .. v37}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v36

    aput-object v36, v34, v35

    invoke-static/range {v33 .. v34}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v33

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v32

    invoke-virtual {v9, v3, v0}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 362
    .restart local v4    # "nStep":I
    .restart local v5    # "nGoal":I
    .restart local v6    # "currentMode":Ljava/lang/String;
    .restart local v7    # "connected":Z
    .restart local v8    # "syncing":Z
    .restart local v14    # "deviceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .restart local v19    # "first_gear_step":Ljava/lang/String;
    :pswitch_1
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v32, 0x7f090bcd

    move/from16 v0, v32

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v32

    const/4 v3, 0x1

    new-array v0, v3, [Ljava/lang/Object;

    move-object/from16 v33, v0

    const/16 v34, 0x0

    const/4 v3, 0x0

    invoke-virtual {v14, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v35

    move-wide/from16 v0, v35

    invoke-static {v3, v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->getTotalStepsFromDeviceType(IJ)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v33, v34

    invoke-static/range {v32 .. v33}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    .line 363
    goto/16 :goto_3

    .line 365
    :pswitch_2
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v32, 0x7f090bcc

    move/from16 v0, v32

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v32

    const/4 v3, 0x1

    new-array v0, v3, [Ljava/lang/Object;

    move-object/from16 v33, v0

    const/16 v34, 0x0

    const/4 v3, 0x0

    invoke-virtual {v14, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v35

    move-wide/from16 v0, v35

    invoke-static {v3, v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->getTotalStepsFromDeviceType(IJ)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v33, v34

    invoke-static/range {v32 .. v33}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    .line 366
    goto/16 :goto_3

    .line 368
    :pswitch_3
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v32, 0x7f090bce

    move/from16 v0, v32

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v32

    const/4 v3, 0x1

    new-array v0, v3, [Ljava/lang/Object;

    move-object/from16 v33, v0

    const/16 v34, 0x0

    const/4 v3, 0x0

    invoke-virtual {v14, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v35

    move-wide/from16 v0, v35

    invoke-static {v3, v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->getTotalStepsFromDeviceType(IJ)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v33, v34

    invoke-static/range {v32 .. v33}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    .line 369
    goto/16 :goto_3

    .line 371
    :pswitch_4
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v32, 0x7f090bca

    move/from16 v0, v32

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v32

    const/4 v3, 0x1

    new-array v0, v3, [Ljava/lang/Object;

    move-object/from16 v33, v0

    const/16 v34, 0x0

    const/4 v3, 0x0

    invoke-virtual {v14, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v35

    move-wide/from16 v0, v35

    invoke-static {v3, v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->getTotalStepsFromDeviceType(IJ)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v33, v34

    invoke-static/range {v32 .. v33}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    .line 372
    goto/16 :goto_3

    .line 374
    :pswitch_5
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v32, 0x7f090bcb

    move/from16 v0, v32

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v32

    const/4 v3, 0x1

    new-array v0, v3, [Ljava/lang/Object;

    move-object/from16 v33, v0

    const/16 v34, 0x0

    const/4 v3, 0x0

    invoke-virtual {v14, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v35

    move-wide/from16 v0, v35

    invoke-static {v3, v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->getTotalStepsFromDeviceType(IJ)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v33, v34

    invoke-static/range {v32 .. v33}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v19

    goto/16 :goto_3

    .line 359
    nop

    :pswitch_data_0
    .packed-switch 0x2723
        :pswitch_3
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method


# virtual methods
.method public displayDataFromDB(Landroid/content/Context;Landroid/widget/RemoteViews;)V
    .locals 26
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "rv"    # Landroid/widget/RemoteViews;

    .prologue
    .line 95
    const-string v2, "displayDataFromDB"

    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->E(Ljava/lang/String;)V

    .line 96
    if-nez p1, :cond_0

    .line 97
    const-string v2, "There is no context."

    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;)V

    .line 201
    :goto_0
    return-void

    .line 100
    :cond_0
    if-nez p2, :cond_1

    .line 101
    const-string v2, "There is no remote view."

    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;)V

    goto :goto_0

    .line 105
    :cond_1
    const/16 v23, 0x0

    .line 106
    .local v23, "totalSteps":I
    const/4 v14, 0x0

    .line 107
    .local v14, "distance":F
    const/4 v8, 0x0

    .line 108
    .local v8, "calories":F
    const/4 v12, 0x0

    .line 110
    .local v12, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v13

    .line 111
    .local v13, "deviceType":I
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    .line 112
    .local v20, "query":Ljava/lang/StringBuilder;
    const-string v2, " SELECT "

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 113
    const-string v2, " SUM( "

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    const-string/jumbo v2, "total_step) AS TOTAL_STEP, "

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 115
    const-string v2, " SUM( "

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 116
    const-string v2, "distance) AS TOTAL_DISTANCE, "

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 117
    const-string v2, " SUM( "

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 118
    const-string v2, "calorie) AS TOTAL_CALORIE, "

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 119
    const-string v2, " FROM "

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 120
    const-string/jumbo v2, "walk_info"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 121
    const-string v2, " WHERE "

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 122
    const-string/jumbo v2, "sync_status != 170004"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 123
    const-string v2, " AND "

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 124
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "start_time >= "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 125
    const-string v2, " AND "

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 126
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "start_time < "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 127
    const-string v2, " AND "

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 128
    const-string/jumbo v2, "total_step > 0"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 129
    const-string v2, " AND "

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 130
    invoke-static {v13}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->getQueryConditionByDeviceType(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 132
    :try_start_1
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 133
    const-string v2, "Cursor is valid."

    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->E(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 138
    :goto_1
    if-eqz v12, :cond_7

    .line 139
    :try_start_2
    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_3

    .line 140
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    .line 141
    const-string v2, "TOTAL_STEP"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v23

    .line 142
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/shealth/walkingmate/cocktailwidget/WalkingMateCocktailWidgetReceiver;->mTotalSteps:I

    move/from16 v0, v23

    if-le v0, v2, :cond_2

    .line 143
    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/shealth/walkingmate/cocktailwidget/WalkingMateCocktailWidgetReceiver;->mTotalSteps:I

    .line 145
    :cond_2
    const-string v2, "TOTAL_DISTANCE"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v14

    .line 146
    const-string v2, "TOTAL_CALORIE"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v8

    .line 148
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "displayDataFromDB total step:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v23

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " distance:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " calories:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->E(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 157
    :cond_3
    :goto_2
    if-eqz v12, :cond_4

    .line 158
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 162
    :cond_4
    new-instance v24, Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    move-object/from16 v0, v24

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;-><init>(Landroid/content/Context;)V

    .line 163
    .local v24, "unitSettingHelper":Lcom/sec/android/app/shealth/common/utils/UnitHelper;
    invoke-virtual/range {v24 .. v24}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->getDistanceUnit()Ljava/lang/String;

    move-result-object v18

    .line 164
    .local v18, "distanceUnitTxt":Ljava/lang/String;
    const v16, 0x7f0900c7

    .line 166
    .local v16, "distanceUnitId":I
    const v2, 0x7f0901c3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 167
    .local v17, "distanceUnitTTS":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0901cb

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 168
    .local v10, "caloriesUnitTTS":Ljava/lang/String;
    const-string/jumbo v2, "mi"

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 169
    invoke-static {v14}, Lcom/sec/android/app/shealth/walkingmate/utils/UnitUtils$UnitConverter;->convertKgIntoMiles(F)F

    move-result v14

    .line 170
    const v16, 0x7f0900cc

    .line 171
    const v2, 0x7f0900cb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 174
    :cond_5
    const/high16 v2, 0x41200000    # 10.0f

    div-float/2addr v14, v2

    .line 175
    float-to-double v2, v14

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-float v14, v2

    .line 176
    const/high16 v2, 0x42c80000    # 100.0f

    div-float/2addr v14, v2

    .line 178
    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v21

    .line 179
    .local v21, "stepsTxt":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0907e5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v22

    .line 180
    .local v22, "stepsUnitTxt":Ljava/lang/String;
    sget-object v2, Lcom/sec/android/app/shealth/walkingmate/cocktailwidget/WalkingMateCocktailWidgetReceiver;->mDecimalFormat:Ljava/text/DecimalFormat;

    const-string v3, "0.00"

    invoke-virtual {v2, v3}, Ljava/text/DecimalFormat;->applyPattern(Ljava/lang/String;)V

    .line 181
    sget-object v2, Lcom/sec/android/app/shealth/walkingmate/cocktailwidget/WalkingMateCocktailWidgetReceiver;->mDecimalFormat:Ljava/text/DecimalFormat;

    float-to-double v3, v14

    invoke-virtual {v2, v3, v4}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v15

    .line 182
    .local v15, "distanceTxt":Ljava/lang/String;
    sget-object v2, Lcom/sec/android/app/shealth/walkingmate/cocktailwidget/WalkingMateCocktailWidgetReceiver;->mDecimalFormat:Ljava/text/DecimalFormat;

    const-string v3, "0"

    invoke-virtual {v2, v3}, Ljava/text/DecimalFormat;->applyPattern(Ljava/lang/String;)V

    .line 183
    sget-object v2, Lcom/sec/android/app/shealth/walkingmate/cocktailwidget/WalkingMateCocktailWidgetReceiver;->mDecimalFormat:Ljava/text/DecimalFormat;

    float-to-double v3, v8

    invoke-static {v3, v4}, Ljava/lang/Math;->floor(D)D

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v9

    .line 184
    .local v9, "caloriesTxt":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0900b9

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 185
    .local v11, "caloriesUnitTxt":Ljava/lang/String;
    const v2, 0x7f080b6e

    move-object/from16 v0, p2

    move-object/from16 v1, v21

    invoke-virtual {v0, v2, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 186
    const v2, 0x7f080b6f

    move-object/from16 v0, p2

    move-object/from16 v1, v22

    invoke-virtual {v0, v2, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 187
    const v2, 0x7f080b71

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v15}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 188
    const v2, 0x7f080b72

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 189
    const v2, 0x7f080b73

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v9}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 190
    const v2, 0x7f080b74

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v11}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 192
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isInitializationNeeded(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_8

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->isStartWalking()Z

    move-result v2

    if-nez v2, :cond_8

    .line 194
    const v2, 0x7f080b6d

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v21

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v22

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090bf3

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v25, 0x7f090020

    move/from16 v0, v25

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 134
    .end local v9    # "caloriesTxt":Ljava/lang/String;
    .end local v10    # "caloriesUnitTTS":Ljava/lang/String;
    .end local v11    # "caloriesUnitTxt":Ljava/lang/String;
    .end local v15    # "distanceTxt":Ljava/lang/String;
    .end local v16    # "distanceUnitId":I
    .end local v17    # "distanceUnitTTS":Ljava/lang/String;
    .end local v18    # "distanceUnitTxt":Ljava/lang/String;
    .end local v21    # "stepsTxt":Ljava/lang/String;
    .end local v22    # "stepsUnitTxt":Ljava/lang/String;
    .end local v24    # "unitSettingHelper":Lcom/sec/android/app/shealth/common/utils/UnitHelper;
    :catch_0
    move-exception v19

    .line 135
    .local v19, "e":Ljava/lang/Exception;
    :try_start_3
    invoke-virtual/range {v19 .. v19}, Ljava/lang/Exception;->printStackTrace()V

    .line 136
    const-string v2, "Cursor error"

    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_1

    .line 157
    .end local v13    # "deviceType":I
    .end local v19    # "e":Ljava/lang/Exception;
    .end local v20    # "query":Ljava/lang/StringBuilder;
    :catchall_0
    move-exception v2

    if-eqz v12, :cond_6

    .line 158
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    :cond_6
    throw v2

    .line 151
    .restart local v13    # "deviceType":I
    .restart local v20    # "query":Ljava/lang/StringBuilder;
    :cond_7
    :try_start_4
    const-string v2, "loadLatestData() - No data"

    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 152
    const/16 v23, 0x0

    .line 153
    const/4 v14, 0x0

    .line 154
    const/4 v8, 0x0

    goto/16 :goto_2

    .line 197
    .restart local v9    # "caloriesTxt":Ljava/lang/String;
    .restart local v10    # "caloriesUnitTTS":Ljava/lang/String;
    .restart local v11    # "caloriesUnitTxt":Ljava/lang/String;
    .restart local v15    # "distanceTxt":Ljava/lang/String;
    .restart local v16    # "distanceUnitId":I
    .restart local v17    # "distanceUnitTTS":Ljava/lang/String;
    .restart local v18    # "distanceUnitTxt":Ljava/lang/String;
    .restart local v21    # "stepsTxt":Ljava/lang/String;
    .restart local v22    # "stepsUnitTxt":Ljava/lang/String;
    .restart local v24    # "unitSettingHelper":Lcom/sec/android/app/shealth/common/utils/UnitHelper;
    :cond_8
    const v2, 0x7f080b6d

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v21

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v22

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090bf3

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v25, 0x7f090020

    move/from16 v0, v25

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    goto/16 :goto_0
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 421
    const-string v2, "WalkingMateCocktailWidgetReceiver"

    const-string v3, "Receive intent"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 423
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string v3, "com.sec.feature.cocktailbar"

    invoke-virtual {v2, v3}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    .line 424
    .local v0, "isSupported":Z
    if-eqz v0, :cond_0

    .line 425
    new-instance v1, Lcom/sec/android/app/shealth/walkingmate/cocktailwidget/WalkingMateCocktailWidgetReceiver$UpdateCocktailWidget;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, p0, v2, p2}, Lcom/sec/android/app/shealth/walkingmate/cocktailwidget/WalkingMateCocktailWidgetReceiver$UpdateCocktailWidget;-><init>(Lcom/sec/android/app/shealth/walkingmate/cocktailwidget/WalkingMateCocktailWidgetReceiver;Landroid/content/Context;Landroid/content/Intent;)V

    .line 426
    .local v1, "updateCocktailWidget":Lcom/sec/android/app/shealth/walkingmate/cocktailwidget/WalkingMateCocktailWidgetReceiver$UpdateCocktailWidget;
    new-instance v2, Ljava/lang/Thread;

    invoke-direct {v2, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    .line 428
    .end local v1    # "updateCocktailWidget":Lcom/sec/android/app/shealth/walkingmate/cocktailwidget/WalkingMateCocktailWidgetReceiver$UpdateCocktailWidget;
    :cond_0
    return-void
.end method
