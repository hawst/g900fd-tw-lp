.class Lcom/sec/android/app/shealth/stress/StressSummaryFragment$19;
.super Ljava/lang/Object;
.source "StressSummaryFragment.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->stressLightningLeftAnitmaion()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)V
    .locals 0

    .prologue
    .line 1771
    iput-object p1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$19;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 4
    .param p1, "animator"    # Landroid/animation/ValueAnimator;

    .prologue
    .line 1775
    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$19;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondCenterIconBigLeftLightning:Landroid/widget/ImageView;
    invoke-static {v2}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$4400(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/widget/ImageView;

    move-result-object v3

    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$19;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mIsGreen:Z
    invoke-static {v2}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$4300(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Z

    move-result v2

    if-eqz v2, :cond_0

    const v2, 0x7f020779

    :goto_0
    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1778
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 1780
    .local v1, "value":Ljava/lang/Integer;
    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$19;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondCenterIconBigLeftLightning:Landroid/widget/ImageView;
    invoke-static {v2}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$4400(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/ClipDrawable;

    .line 1781
    .local v0, "drawable":Landroid/graphics/drawable/ClipDrawable;
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/ClipDrawable;->setLevel(I)Z

    .line 1782
    return-void

    .line 1775
    .end local v0    # "drawable":Landroid/graphics/drawable/ClipDrawable;
    .end local v1    # "value":Ljava/lang/Integer;
    :cond_0
    const v2, 0x7f02077b

    goto :goto_0
.end method
