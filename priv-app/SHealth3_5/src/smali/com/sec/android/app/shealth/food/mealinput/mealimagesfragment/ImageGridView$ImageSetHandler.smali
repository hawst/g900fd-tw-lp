.class Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView$ImageSetHandler;
.super Landroid/os/Handler;
.source "ImageGridView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ImageSetHandler"
.end annotation


# instance fields
.field private mImageGridView:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/view/ViewGroup;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;)V
    .locals 1
    .param p1, "mImageGridView"    # Landroid/view/ViewGroup;

    .prologue
    .line 355
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 356
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView$ImageSetHandler;->mImageGridView:Ljava/lang/ref/WeakReference;

    .line 357
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 362
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 364
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView$ImageSetHandler;->mImageGridView:Ljava/lang/ref/WeakReference;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView$ImageSetHandler;->mImageGridView:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 366
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView$ImageSetHandler;->mImageGridView:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iget v2, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 368
    .local v0, "imageView":Landroid/widget/ImageButton;
    if-eqz v0, :cond_0

    .line 370
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 385
    .end local v0    # "imageView":Landroid/widget/ImageButton;
    :goto_0
    return-void

    .line 374
    .restart local v0    # "imageView":Landroid/widget/ImageButton;
    :cond_0
    # getter for: Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->access$200()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Fail to find ImageView. View was detached"

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/common/utils/Logging;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 376
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    .line 382
    .end local v0    # "imageView":Landroid/widget/ImageButton;
    :cond_1
    # getter for: Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->access$200()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Fail to find content View. View was detached"

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/common/utils/Logging;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
