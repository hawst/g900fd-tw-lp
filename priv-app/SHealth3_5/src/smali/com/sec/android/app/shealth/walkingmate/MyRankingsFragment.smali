.class public Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;
.super Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;
.source "MyRankingsFragment.java"


# static fields
.field private static final BROADCAST_MESSAGE:I = 0xd

.field private static final CLIENT_ID:Ljava/lang/String; = "1y90e30264"

.field private static final CLIENT_SECRET:Ljava/lang/String; = "80E7ECD9D301CB7888C73703639302E5"

.field private static final REGISTER_NEW_SAMSUNG_TOKEN_MESSAGE:I = 0x3

.field private static final TAG:Ljava/lang/String;

.field private static final UPDATE_MESSAGE_EPIC_FAIL:I = 0x21

.field private static final UPDATE_MESSAGE_SUCCESS:I = 0x17


# instance fields
.field private agePlaceImage:Landroid/widget/ImageView;

.field private agePlaceNumber:Landroid/widget/TextView;

.field private agePlaceRanking:Landroid/widget/TextView;

.field private ageSeparator:Landroid/widget/TextView;

.field private ageSlider:Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;

.field private basicResponse:Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;

.field private byAgeResponse:Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;

.field private distanceUnit:Ljava/lang/String;

.field private headerTextDistance:Landroid/widget/TextView;

.field private headerTextStep:Landroid/widget/TextView;

.field private headerTextView:Landroid/widget/TextView;

.field private loadingDialog:Lcom/sec/android/app/shealth/walkingmate/utils/Utils$LoadingDialog;

.field private mActivity:Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;

.field private mNoNetworkLayout:Landroid/widget/RelativeLayout;

.field private mOnNetworkLayout:Landroid/widget/LinearLayout;

.field private mRequestResult:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

.field private mScrollView:Landroid/widget/ScrollView;

.field private mUpdatedTextView:Landroid/widget/TextView;

.field private mWalk_my_zero:Landroid/widget/RelativeLayout;

.field private mZeroUpdatedTextView:Landroid/widget/TextView;

.field private rankingUpdaterTask:Lcom/sec/android/app/shealth/walkingmate/loaders/MyRankingsUpdater;

.field private twoDForm:Ljava/text/DecimalFormat;

.field private unitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

.field private updateHandler:Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;

.field private updateToast:Landroid/widget/Toast;

.field private worldPlaceImage:Landroid/widget/ImageView;

.field private worldPlaceNumber:Landroid/widget/TextView;

.field private worldPlaceRanking:Landroid/widget/TextView;

.field private worldSeparator:Landroid/widget/TextView;

.field private worldSlider:Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 66
    const-class v0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;-><init>()V

    .line 73
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;->DEFAULT:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->mRequestResult:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    .line 111
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "#.##"

    new-instance v2, Ljava/text/DecimalFormatSymbols;

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v2, v3}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    invoke-direct {v0, v1, v2}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->twoDForm:Ljava/text/DecimalFormat;

    .line 123
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment$1;-><init>(Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->updateHandler:Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->sendRequestForNewUserToken()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->sendRequestForUserToken()V

    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;Ljava/lang/String;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Z

    .prologue
    .line 65
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->loadOfflineData(Ljava/lang/String;Z)V

    return-void
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->mWalk_my_zero:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;)Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->mRequestResult:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    return-object v0
.end method

.method static synthetic access$1202(Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;)Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->mRequestResult:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    return-object p1
.end method

.method static synthetic access$1302(Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;)Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->basicResponse:Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;

    return-object p1
.end method

.method static synthetic access$1402(Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;)Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->byAgeResponse:Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;

    return-object p1
.end method

.method static synthetic access$1500(Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->saveTheLatestReceivedData()V

    return-void
.end method

.method static synthetic access$1602(Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;Lcom/sec/android/app/shealth/walkingmate/loaders/MyRankingsUpdater;)Lcom/sec/android/app/shealth/walkingmate/loaders/MyRankingsUpdater;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/walkingmate/loaders/MyRankingsUpdater;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->rankingUpdaterTask:Lcom/sec/android/app/shealth/walkingmate/loaders/MyRankingsUpdater;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;)Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->updateHandler:Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->updateViewsWithLatestValues()V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;

    .prologue
    .line 65
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->refreshFragmentFocusables()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->loadLastSuccessfullyReceivedData()V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->showUpdateFail()V

    return-void
.end method

.method static synthetic access$700()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->cancelUpdateTask()V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;Landroid/content/Intent;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;
    .param p1, "x1"    # Landroid/content/Intent;
    .param p2, "x2"    # Z

    .prologue
    .line 65
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->requestUpdateData(Landroid/content/Intent;Z)V

    return-void
.end method

.method private cancelUpdateTask()V
    .locals 2

    .prologue
    .line 732
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->rankingUpdaterTask:Lcom/sec/android/app/shealth/walkingmate/loaders/MyRankingsUpdater;

    if-eqz v0, :cond_0

    .line 733
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->rankingUpdaterTask:Lcom/sec/android/app/shealth/walkingmate/loaders/MyRankingsUpdater;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/loaders/MyRankingsUpdater;->cancel(Z)Z

    .line 734
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->rankingUpdaterTask:Lcom/sec/android/app/shealth/walkingmate/loaders/MyRankingsUpdater;

    .line 736
    :cond_0
    return-void
.end method

.method private getCheatingFormattedString(IF)Ljava/lang/String;
    .locals 10
    .param p1, "baseStringId"    # I
    .param p2, "percentValue"    # F

    .prologue
    const/4 v9, 0x0

    const/4 v7, 0x1

    .line 716
    float-to-int v3, p2

    .line 717
    .local v3, "intPart":I
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    .line 718
    .local v4, "intPartString":Ljava/lang/String;
    int-to-float v5, v3

    sub-float v5, p2, v5

    invoke-static {v5, v7}, Lcom/sec/android/app/shealth/common/utils/FormatUtils;->roundValueToPrecision(FI)F

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v2

    .line 719
    .local v2, "floatPartString":Ljava/lang/String;
    const/4 v5, 0x3

    invoke-virtual {v2, v7, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 721
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->isArabLocale()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 722
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 726
    .local v0, "awesomeString":Ljava/lang/String;
    :goto_0
    invoke-virtual {v0, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v6

    add-int v1, v5, v6

    .line 727
    .local v1, "endIndexOfIntPart":I
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v9, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 728
    return-object v0

    .line 724
    .end local v0    # "awesomeString":Ljava/lang/String;
    .end local v1    # "endIndexOfIntPart":I
    :cond_0
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "awesomeString":Ljava/lang/String;
    goto :goto_0
.end method

.method private getRankingString(I)Ljava/lang/String;
    .locals 8
    .param p1, "ranking"    # I

    .prologue
    const v7, 0x7f090b7a

    const v5, 0x7f090b79

    const v3, 0x7f090b78

    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 570
    const/4 v1, 0x0

    .line 571
    .local v1, "result":Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->getRankingString(I)I

    move-result v0

    .line 573
    .local v0, "rankStrType":I
    packed-switch v0, :pswitch_data_0

    .line 599
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->isArabLocale()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 600
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const v3, 0x7f090b7b

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 606
    :goto_0
    return-object v1

    .line 575
    :pswitch_0
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->isArabLocale()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 576
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 578
    :cond_0
    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 580
    goto :goto_0

    .line 583
    :pswitch_1
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->isArabLocale()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 584
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 586
    :cond_1
    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 588
    goto :goto_0

    .line 591
    :pswitch_2
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->isArabLocale()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 592
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 594
    :cond_2
    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 596
    goto :goto_0

    .line 602
    :cond_3
    const v2, 0x7f090b7b

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 573
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private init(Landroid/view/View;)V
    .locals 8
    .param p1, "contentView"    # Landroid/view/View;

    .prologue
    const/4 v7, 0x0

    .line 363
    const v3, 0x7f080bdc

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;

    iput-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->worldSlider:Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;

    .line 364
    const v3, 0x7f080bde

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;

    iput-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->ageSlider:Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;

    .line 366
    const v3, 0x7f080bd8

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->headerTextStep:Landroid/widget/TextView;

    .line 367
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->headerTextStep:Landroid/widget/TextView;

    const-string v4, "0"

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 368
    const v3, 0x7f080bd9

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->headerTextDistance:Landroid/widget/TextView;

    .line 370
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->distanceUnit:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 371
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " (0.00 "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const v4, 0x7f0900cb

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 372
    .local v2, "mileStr":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " (0.00 "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const v4, 0x7f0900c7

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 373
    .local v1, "kmStr":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->distanceUnit:Ljava/lang/String;

    const-string/jumbo v4, "mi"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    move-object v0, v2

    .line 375
    .local v0, "defaultDistanceStr":Ljava/lang/String;
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->headerTextDistance:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 378
    .end local v0    # "defaultDistanceStr":Ljava/lang/String;
    .end local v1    # "kmStr":Ljava/lang/String;
    .end local v2    # "mileStr":Ljava/lang/String;
    :cond_0
    const v3, 0x7f080bda

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->headerTextView:Landroid/widget/TextView;

    .line 380
    const v3, 0x7f080b83

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->mUpdatedTextView:Landroid/widget/TextView;

    .line 381
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->mUpdatedTextView:Landroid/widget/TextView;

    const-string v4, ""

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 383
    const v3, 0x7f080be0

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->worldPlaceNumber:Landroid/widget/TextView;

    .line 384
    const v3, 0x7f080be1

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->worldPlaceRanking:Landroid/widget/TextView;

    .line 385
    const v3, 0x7f080bdb

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->worldSeparator:Landroid/widget/TextView;

    .line 386
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->worldPlaceNumber:Landroid/widget/TextView;

    const v4, 0x7f090b7b

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 388
    const v3, 0x7f080bd6

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->agePlaceNumber:Landroid/widget/TextView;

    .line 389
    const v3, 0x7f080bd7

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->agePlaceRanking:Landroid/widget/TextView;

    .line 390
    const v3, 0x7f080bdd

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->ageSeparator:Landroid/widget/TextView;

    .line 392
    const v3, 0x7f080bdf

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->worldPlaceImage:Landroid/widget/ImageView;

    .line 393
    const v3, 0x7f080bd5

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->agePlaceImage:Landroid/widget/ImageView;

    .line 394
    const v3, 0x7f0803a8

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ScrollView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->mScrollView:Landroid/widget/ScrollView;

    .line 395
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->mScrollView:Landroid/widget/ScrollView;

    new-instance v4, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment$4;

    invoke-direct {v4, p0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment$4;-><init>(Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;)V

    invoke-virtual {v3, v4}, Landroid/widget/ScrollView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 403
    return-void

    .restart local v1    # "kmStr":Ljava/lang/String;
    .restart local v2    # "mileStr":Ljava/lang/String;
    :cond_1
    move-object v0, v1

    .line 373
    goto/16 :goto_0
.end method

.method private initSamsungAccountInfo()V
    .locals 2

    .prologue
    .line 439
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/loaders/WalkingMateRankingHelper;->isAvailable()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 440
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/loaders/WalkingMateRankingHelper;->getSamsungAccountInfoIntent()Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->requestUpdateData(Landroid/content/Intent;Z)V

    .line 444
    :cond_0
    :goto_0
    return-void

    .line 441
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->updateHandler:Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;

    if-eqz v0, :cond_0

    .line 442
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->updateHandler:Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;

    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method private isNetworkConnted()Z
    .locals 9

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 200
    const/4 v0, 0x0

    .line 201
    .local v0, "isMobileAvailable":Z
    const/4 v1, 0x0

    .line 202
    .local v1, "isWifiAvailable":Z
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    const-string v8, "connectivity"

    invoke-virtual {v7, v8}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/ConnectivityManager;

    .line 203
    .local v2, "manager":Landroid/net/ConnectivityManager;
    invoke-virtual {v2, v5}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v3

    .line 204
    .local v3, "mobile":Landroid/net/NetworkInfo;
    invoke-virtual {v2, v6}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v4

    .line 206
    .local v4, "wifi":Landroid/net/NetworkInfo;
    if-eqz v3, :cond_0

    .line 207
    invoke-virtual {v3}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    .line 209
    :cond_0
    if-eqz v4, :cond_1

    .line 210
    invoke-virtual {v4}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v1

    .line 213
    :cond_1
    if-nez v0, :cond_2

    if-eqz v1, :cond_3

    :cond_2
    move v5, v6

    .line 216
    :cond_3
    return v5
.end method

.method private loadLastSuccessfullyReceivedData()V
    .locals 1

    .prologue
    .line 787
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;->RESTORE_LOCAL:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->mRequestResult:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    .line 788
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->updateToast:Landroid/widget/Toast;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->updateToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    .line 789
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->loadSavedBasicResponse()V

    .line 790
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->loadSavedByAgeResponse()V

    .line 791
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->updateViewsWithLatestValues()V

    .line 792
    return-void
.end method

.method private loadOfflineData(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "msg"    # Ljava/lang/String;
    .param p2, "showMsg"    # Z

    .prologue
    .line 349
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->dismissLoadingDialog()V

    .line 350
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->isMyRankingDataAvailable()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 351
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->loadLastSuccessfullyReceivedData()V

    .line 360
    :cond_0
    :goto_0
    return-void

    .line 353
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 355
    if-eqz p2, :cond_2

    .line 356
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 358
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_0
.end method

.method private loadSavedBasicResponse()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 800
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getMyRankingsBasicResponse()Ljava/lang/String;

    move-result-object v1

    .line 801
    .local v1, "jsonStr":Ljava/lang/String;
    new-instance v2, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;

    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v3}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;-><init>(Lorg/json/JSONObject;)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->basicResponse:Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ServerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 809
    .end local v1    # "jsonStr":Ljava/lang/String;
    :goto_0
    return-void

    .line 802
    :catch_0
    move-exception v0

    .line 803
    .local v0, "catchMeIfYouCan":Lorg/json/JSONException;
    iput-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->basicResponse:Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;

    .line 804
    sget-object v2, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->TAG:Ljava/lang/String;

    invoke-static {v2, v0}, Lcom/sec/android/app/shealth/common/utils/Utils;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 805
    .end local v0    # "catchMeIfYouCan":Lorg/json/JSONException;
    :catch_1
    move-exception v0

    .line 806
    .local v0, "catchMeIfYouCan":Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ServerException;
    iput-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->basicResponse:Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;

    .line 807
    sget-object v2, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->TAG:Ljava/lang/String;

    invoke-static {v2, v0}, Lcom/sec/android/app/shealth/common/utils/Utils;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private loadSavedByAgeResponse()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 817
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getMyRankingsByAgeResponse()Ljava/lang/String;

    move-result-object v1

    .line 818
    .local v1, "jsonStr":Ljava/lang/String;
    new-instance v2, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;

    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v3}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;-><init>(Lorg/json/JSONObject;)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->byAgeResponse:Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ServerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 826
    .end local v1    # "jsonStr":Ljava/lang/String;
    :goto_0
    return-void

    .line 819
    :catch_0
    move-exception v0

    .line 820
    .local v0, "catchMeIfYouCan":Lorg/json/JSONException;
    iput-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->byAgeResponse:Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;

    .line 821
    sget-object v2, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->TAG:Ljava/lang/String;

    invoke-static {v2, v0}, Lcom/sec/android/app/shealth/common/utils/Utils;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 822
    .end local v0    # "catchMeIfYouCan":Lorg/json/JSONException;
    :catch_1
    move-exception v0

    .line 823
    .local v0, "catchMeIfYouCan":Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ServerException;
    iput-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->byAgeResponse:Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;

    .line 824
    sget-object v2, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->TAG:Ljava/lang/String;

    invoke-static {v2, v0}, Lcom/sec/android/app/shealth/common/utils/Utils;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static newInstance(I)Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;
    .locals 1
    .param p0, "rowId"    # I

    .prologue
    .line 119
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;-><init>()V

    .line 120
    .local v0, "f":Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;
    return-object v0
.end method

.method private requestUpdateData(Landroid/content/Intent;Z)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "isLatestIntent"    # Z

    .prologue
    const/4 v1, 0x0

    .line 453
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->rankingUpdaterTask:Lcom/sec/android/app/shealth/walkingmate/loaders/MyRankingsUpdater;

    if-nez v0, :cond_0

    .line 454
    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->basicResponse:Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;

    .line 455
    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->byAgeResponse:Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;

    .line 456
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment$6;-><init>(Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->rankingUpdaterTask:Lcom/sec/android/app/shealth/walkingmate/loaders/MyRankingsUpdater;

    .line 483
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "requestUpdateData"

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 484
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->rankingUpdaterTask:Lcom/sec/android/app/shealth/walkingmate/loaders/MyRankingsUpdater;

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x1

    new-array v2, v2, [Landroid/content/Intent;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/walkingmate/loaders/MyRankingsUpdater;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 486
    :cond_0
    return-void
.end method

.method private saveTheLatestReceivedData()V
    .locals 1

    .prologue
    .line 775
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->basicResponse:Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->byAgeResponse:Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;

    if-eqz v0, :cond_0

    .line 776
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->setMyRankingDataAvailable(Z)V

    .line 777
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->basicResponse:Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;->getJsonObject()Lorg/json/JSONObject;

    move-result-object v0

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->setMyRankingsBasicResponse(Ljava/lang/String;)V

    .line 778
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->byAgeResponse:Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;->getJsonObject()Lorg/json/JSONObject;

    move-result-object v0

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->setMyRankingsByAgeResponse(Ljava/lang/String;)V

    .line 780
    :cond_0
    return-void
.end method

.method private sendRequestForNewUserToken()V
    .locals 4

    .prologue
    .line 290
    sget-object v2, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "sendRequestForNewUserToken - enter"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 292
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    if-nez v2, :cond_0

    .line 293
    sget-object v2, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "sendRequestForNewUserToken - exit, cause : activity is null"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 343
    :goto_0
    return-void

    .line 297
    :cond_0
    invoke-static {}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->getInstance()Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    move-result-object v1

    .line 299
    .local v1, "userTokenManager":Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;
    :try_start_0
    new-instance v2, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment$3;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment$3;-><init>(Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->sendRequestForNewUserToken(Lcom/sec/android/service/health/samsungaccount/ISamsungUserTokenListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 342
    :goto_1
    sget-object v2, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "sendRequestForNewUserToken - exit"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 339
    :catch_0
    move-exception v0

    .line 340
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method private sendRequestForUserToken()V
    .locals 6

    .prologue
    .line 238
    sget-object v2, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "sendRequestForUserToken - enter"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 239
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    if-nez v2, :cond_0

    .line 240
    sget-object v2, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "sendRequestForUserToken - exit, cause : activity is null"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 287
    :goto_0
    return-void

    .line 244
    :cond_0
    invoke-static {}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->getInstance()Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    move-result-object v1

    .line 246
    .local v1, "userTokenManager":Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "1y90e30264"

    const-string v4, "80E7ECD9D301CB7888C73703639302E5"

    new-instance v5, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment$2;

    invoke-direct {v5, p0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment$2;-><init>(Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;)V

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->sendRequestForUserToken(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/service/health/samsungaccount/ISamsungUserTokenListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 286
    :goto_1
    sget-object v2, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "sendRequestForUserToken - exit"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 283
    :catch_0
    move-exception v0

    .line 284
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method private setDistanceValueAndUnitText(IF)Ljava/lang/String;
    .locals 8
    .param p1, "step"    # I
    .param p2, "value"    # F

    .prologue
    const/high16 v7, 0x42c80000    # 100.0f

    .line 844
    const/4 v1, 0x0

    .line 846
    .local v1, "distance":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 848
    .local v4, "res":Landroid/content/res/Resources;
    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->distanceUnit:Ljava/lang/String;

    const-string/jumbo v6, "mi"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 850
    const v5, 0x3a22e36f

    mul-float/2addr v5, p2

    mul-float/2addr v5, v7

    float-to-int v5, v5

    int-to-float v5, v5

    div-float v0, v5, v7

    .line 851
    .local v0, "convert_distance":F
    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->twoDForm:Ljava/text/DecimalFormat;

    float-to-double v6, v0

    invoke-virtual {v5, v6, v7}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v3

    .line 853
    .local v3, "newValue":F
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f0900cb

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 859
    .end local v3    # "newValue":F
    :goto_0
    return-object v1

    .line 855
    .end local v0    # "convert_distance":F
    :cond_0
    const/high16 v5, 0x41200000    # 10.0f

    div-float v5, p2, v5

    float-to-int v5, v5

    int-to-float v5, v5

    div-float v0, v5, v7

    .line 856
    .restart local v0    # "convert_distance":F
    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->twoDForm:Ljava/text/DecimalFormat;

    float-to-double v6, v0

    invoke-virtual {v5, v6, v7}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v2

    .line 857
    .local v2, "kmValue":F
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f0900c7

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private showLoadingDialog()V
    .locals 4

    .prologue
    .line 421
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->dismissLoadingDialog()V

    .line 422
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/utils/Utils$LoadingDialog;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils$LoadingDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->loadingDialog:Lcom/sec/android/app/shealth/walkingmate/utils/Utils$LoadingDialog;

    .line 423
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->loadingDialog:Lcom/sec/android/app/shealth/walkingmate/utils/Utils$LoadingDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils$LoadingDialog;->show()V

    .line 425
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->updateHandler:Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;

    if-eqz v0, :cond_0

    .line 426
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->updateHandler:Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;

    const/16 v1, 0x21

    const-wide/16 v2, 0x7530

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 428
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->loadingDialog:Lcom/sec/android/app/shealth/walkingmate/utils/Utils$LoadingDialog;

    new-instance v1, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment$5;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment$5;-><init>(Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils$LoadingDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 436
    return-void
.end method

.method private showUpdateFail()V
    .locals 5

    .prologue
    const v4, 0x7f090b75

    .line 490
    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "mRequestResult : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->mRequestResult:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 491
    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const v2, 0x7f0900d9

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment$8;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment$8;-><init>(Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment$7;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment$7;-><init>(Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnDismissListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    .line 509
    .local v0, "dialogBuilder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->mRequestResult:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    sget-object v2, Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;->CONNECTION_ERROR:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    if-ne v1, v2, :cond_0

    .line 510
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090bd3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 518
    :goto_0
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string/jumbo v3, "vskrutie pokazalo: pacient spal"

    invoke-virtual {v2, v1, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 519
    return-void

    .line 511
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->mRequestResult:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    sget-object v2, Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;->SERVER_ERROR:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    if-ne v1, v2, :cond_1

    .line 512
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    goto :goto_0

    .line 513
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->mRequestResult:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    sget-object v2, Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;->FAILED_TO_GET_TOKEN:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    if-ne v1, v2, :cond_2

    .line 514
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090ba5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    goto :goto_0

    .line 516
    :cond_2
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    goto :goto_0
.end method

.method private updateBasicResponseInfo()V
    .locals 17

    .prologue
    .line 610
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-nez v1, :cond_1

    .line 662
    :cond_0
    :goto_0
    return-void

    .line 611
    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->basicResponse:Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;

    if-eqz v1, :cond_0

    .line 613
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    .line 614
    .local v12, "res":Landroid/content/res/Resources;
    if-nez v12, :cond_2

    .line 615
    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "res is Null"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 619
    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->basicResponse:Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;->getRanking()I

    move-result v10

    .line 621
    .local v10, "rank":I
    if-nez v10, :cond_3

    .line 622
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->worldSeparator:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 623
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->worldPlaceNumber:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 624
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->worldPlaceRanking:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 625
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->worldSlider:Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->setVisibility(I)V

    .line 626
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->worldPlaceImage:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 628
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->mActivity:Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->displayLastUpdatedTime(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v13

    .line 629
    .local v13, "str":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->mZeroUpdatedTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 630
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->mWalk_my_zero:Landroid/widget/RelativeLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 631
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->mScrollView:Landroid/widget/ScrollView;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/widget/ScrollView;->scrollTo(II)V

    .line 632
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->mScrollView:Landroid/widget/ScrollView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ScrollView;->setScrollBarSize(I)V

    goto :goto_0

    .line 636
    .end local v13    # "str":Ljava/lang/String;
    :cond_3
    const/4 v1, 0x1

    :try_start_0
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->saveLastUpdatedTime(Z)V

    .line 637
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->mActivity:Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->displayLastUpdatedTime(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v13

    .line 638
    .restart local v13    # "str":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->mUpdatedTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 639
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->basicResponse:Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;->getTotalUserNum()I

    move-result v15

    .line 642
    .local v15, "totalUserCnt":I
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->isArabLocale()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 643
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const v2, 0x7f090b8a

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    .line 647
    .local v14, "totalUser":Ljava/lang/String;
    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f090b85

    invoke-virtual {v12, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 648
    .local v16, "worldStr":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->worldSeparator:Landroid/widget/TextView;

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 649
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->worldSeparator:Landroid/widget/TextView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setSelected(Z)V

    .line 650
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->worldSeparator:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0901fd

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 651
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->basicResponse:Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;->getRanking()I

    move-result v1

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->getRankingString(I)Ljava/lang/String;

    move-result-object v11

    .line 652
    .local v11, "rankStr":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->worldPlaceNumber:Landroid/widget/TextView;

    invoke-virtual {v1, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 653
    const v1, 0x7f090b88

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->basicResponse:Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;->getPercentile()D

    move-result-wide v2

    double-to-float v2, v2

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->getCheatingFormattedString(IF)Ljava/lang/String;

    move-result-object v9

    .line 654
    .local v9, "percentStr":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->worldPlaceRanking:Landroid/widget/TextView;

    invoke-virtual {v1, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 655
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->worldSlider:Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->basicResponse:Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;->getStep()I

    move-result v2

    int-to-long v2, v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->basicResponse:Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;->getAvgStep()I

    move-result v4

    int-to-long v4, v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->basicResponse:Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;->getBestStep()I

    move-result v6

    int-to-long v6, v6

    invoke-virtual/range {v1 .. v7}, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->setValues(JJJ)V

    .line 656
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->worldSlider:Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->animateMe()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 657
    .end local v9    # "percentStr":Ljava/lang/String;
    .end local v11    # "rankStr":Ljava/lang/String;
    .end local v13    # "str":Ljava/lang/String;
    .end local v14    # "totalUser":Ljava/lang/String;
    .end local v15    # "totalUserCnt":I
    .end local v16    # "worldStr":Ljava/lang/String;
    :catch_0
    move-exception v8

    .line 658
    .local v8, "e":Ljava/lang/NullPointerException;
    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "updateBasicResponseInfo basicResponse is Null"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 645
    .end local v8    # "e":Ljava/lang/NullPointerException;
    .restart local v13    # "str":Ljava/lang/String;
    .restart local v15    # "totalUserCnt":I
    :cond_4
    const v1, 0x7f090b8a

    :try_start_1
    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v14

    .restart local v14    # "totalUser":Ljava/lang/String;
    goto/16 :goto_1
.end method

.method private updateByAgeResponseInfo()V
    .locals 11

    .prologue
    .line 667
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_1

    .line 709
    :cond_0
    :goto_0
    return-void

    .line 668
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->byAgeResponse:Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;

    if-eqz v0, :cond_0

    .line 670
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->byAgeResponse:Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;->getRanking()I

    move-result v7

    .line 671
    .local v7, "ageRank":I
    if-nez v7, :cond_2

    .line 672
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->ageSeparator:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 673
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->agePlaceNumber:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 674
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->agePlaceRanking:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 675
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->ageSlider:Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->setVisibility(I)V

    .line 676
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->agePlaceImage:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 705
    .end local v7    # "ageRank":I
    :catch_0
    move-exception v8

    .line 706
    .local v8, "e":Ljava/lang/NullPointerException;
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "updateByAgeResponseInfo byAgeResponse is Null"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 679
    .end local v8    # "e":Ljava/lang/NullPointerException;
    .restart local v7    # "ageRank":I
    :cond_2
    :try_start_1
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    .line 680
    .local v9, "res":Landroid/content/res/Resources;
    if-eqz v9, :cond_0

    .line 684
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->isArabLocale()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 685
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const v1, 0x7f090b8a

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->byAgeResponse:Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;->getTotalUserNum()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    .line 690
    .local v10, "totalAgeGroupUser":Ljava/lang/String;
    :goto_1
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->isUrduLocale()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 691
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->ageSeparator:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f090b86

    invoke-virtual {v9, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->byAgeResponse:Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;->getAgeRange()I

    move-result v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/utils/AgeUtils;->getAgeGroup(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\u200f("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 697
    :goto_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->ageSeparator:Landroid/widget/TextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setSelected(Z)V

    .line 698
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->ageSeparator:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f090b86

    invoke-virtual {v9, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->byAgeResponse:Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;->getAgeRange()I

    move-result v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/utils/AgeUtils;->getAgeGroup(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const v2, 0x7f0901fd

    invoke-virtual {v9, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 700
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->agePlaceNumber:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->byAgeResponse:Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;->getRanking()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->getRankingString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 701
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->agePlaceRanking:Landroid/widget/TextView;

    const v1, 0x7f090b88

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->byAgeResponse:Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;->getPercentile()D

    move-result-wide v2

    double-to-float v2, v2

    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->getCheatingFormattedString(IF)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 702
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->ageSlider:Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->byAgeResponse:Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;->getStep()I

    move-result v1

    int-to-long v1, v1

    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->byAgeResponse:Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;->getAvgStep()I

    move-result v3

    int-to-long v3, v3

    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->byAgeResponse:Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;->getBestStep()I

    move-result v5

    int-to-long v5, v5

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->setValues(JJJ)V

    .line 703
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->ageSlider:Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->animateMe()V

    goto/16 :goto_0

    .line 687
    .end local v10    # "totalAgeGroupUser":Ljava/lang/String;
    :cond_3
    const v0, 0x7f090b8a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->byAgeResponse:Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;->getTotalUserNum()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    .restart local v10    # "totalAgeGroupUser":Ljava/lang/String;
    goto/16 :goto_1

    .line 694
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->ageSeparator:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f090b86

    invoke-virtual {v9, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->byAgeResponse:Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;->getAgeRange()I

    move-result v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/utils/AgeUtils;->getAgeGroup(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_2
.end method

.method private updateHeaderText()V
    .locals 22

    .prologue
    .line 529
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->basicResponse:Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;->getStep()I

    move-result v3

    .line 530
    .local v3, "basicStep":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->basicResponse:Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;->getCreateTime()Ljava/lang/String;

    move-result-object v4

    .line 533
    .local v4, "createTime":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->isArabLocale()Z

    move-result v17

    if-eqz v17, :cond_1

    .line 534
    sget-object v17, Ljava/util/Locale;->US:Ljava/util/Locale;

    const v18, 0x7f090b8d

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v18

    const/16 v19, 0x1

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    aput-object v21, v19, v20

    invoke-static/range {v17 .. v19}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    .line 538
    .local v14, "stepTxt":Ljava/lang/String;
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->headerTextStep:Landroid/widget/TextView;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 540
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->basicResponse:Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;->getDistance()I

    move-result v17

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v3, v1}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->setDistanceValueAndUnitText(IF)Ljava/lang/String;

    move-result-object v7

    .line 541
    .local v7, "distance":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->headerTextDistance:Landroid/widget/TextView;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 544
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v17

    if-nez v17, :cond_0

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v17

    const/16 v18, 0x8

    move/from16 v0, v17

    move/from16 v1, v18

    if-lt v0, v1, :cond_0

    .line 545
    const-string v17, "00000000000000000"

    move-object/from16 v0, v17

    invoke-static {v4, v0}, Landroid/text/TextUtils;->indexOf(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)I

    move-result v17

    const/16 v18, -0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_2

    .line 546
    const-string v13, ""

    .line 561
    .local v13, "sinceDate":Ljava/lang/String;
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->headerTextView:Landroid/widget/TextView;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 567
    .end local v3    # "basicStep":I
    .end local v4    # "createTime":Ljava/lang/String;
    .end local v7    # "distance":Ljava/lang/String;
    .end local v13    # "sinceDate":Ljava/lang/String;
    .end local v14    # "stepTxt":Ljava/lang/String;
    :cond_0
    :goto_2
    return-void

    .line 536
    .restart local v3    # "basicStep":I
    .restart local v4    # "createTime":Ljava/lang/String;
    :cond_1
    const v17, 0x7f090b8d

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v17

    const/16 v18, 0x1

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    aput-object v20, v18, v19

    invoke-static/range {v17 .. v18}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    .restart local v14    # "stepTxt":Ljava/lang/String;
    goto :goto_0

    .line 548
    .restart local v7    # "distance":Ljava/lang/String;
    :cond_2
    const/16 v17, 0x0

    const/16 v18, 0x4

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v4, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v16

    .line 549
    .local v16, "year":I
    const/16 v17, 0x4

    const/16 v18, 0x6

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v4, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v11

    .line 550
    .local v11, "month":I
    const/16 v17, 0x6

    const/16 v18, 0x8

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v4, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    .line 551
    .local v6, "day":I
    add-int/lit8 v17, v11, -0x1

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-static {v0, v1, v6}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance(III)Ljava/util/GregorianCalendar;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v9

    .line 553
    .local v9, "millisec":J
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v15

    .line 554
    .local v15, "tz":Ljava/util/TimeZone;
    invoke-virtual {v15, v9, v10}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v12

    .line 555
    .local v12, "offset":I
    sget-object v17, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->TAG:Ljava/lang/String;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Milli Sec : "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ", TimeZone: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v15}, Ljava/util/TimeZone;->getDisplayName()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ", offset: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 556
    int-to-long v0, v12

    move-wide/from16 v17, v0

    add-long v9, v9, v17

    .line 557
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v17

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v18

    const-string v19, "/"

    invoke-static/range {v17 .. v19}, Lcom/sec/android/app/shealth/common/utils/DateFormatHelper;->getSystemDateFormat(Landroid/content/Context;Ljava/lang/Long;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 559
    .local v5, "date":Ljava/lang/String;
    const v17, 0x7f090b8e

    const/16 v18, 0x1

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    aput-object v5, v18, v19

    move-object/from16 v0, p0

    move/from16 v1, v17

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v13

    .restart local v13    # "sinceDate":Ljava/lang/String;
    goto/16 :goto_1

    .line 563
    .end local v3    # "basicStep":I
    .end local v4    # "createTime":Ljava/lang/String;
    .end local v5    # "date":Ljava/lang/String;
    .end local v6    # "day":I
    .end local v7    # "distance":Ljava/lang/String;
    .end local v9    # "millisec":J
    .end local v11    # "month":I
    .end local v12    # "offset":I
    .end local v13    # "sinceDate":Ljava/lang/String;
    .end local v14    # "stepTxt":Ljava/lang/String;
    .end local v15    # "tz":Ljava/util/TimeZone;
    .end local v16    # "year":I
    :catch_0
    move-exception v8

    .line 564
    .local v8, "e":Ljava/lang/NullPointerException;
    sget-object v17, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v18, "updateHeaderText basicResponse is Null"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2
.end method

.method private updateValues()V
    .locals 2

    .prologue
    .line 406
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->updateHeaderText()V

    .line 407
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->mRequestResult:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;->DEFAULT:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    if-eq v0, v1, :cond_2

    .line 408
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->mRequestResult:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;->SUCCESS:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->mRequestResult:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;->RESTORE_LOCAL:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    if-ne v0, v1, :cond_1

    .line 409
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->updateViewsWithLatestValues()V

    .line 415
    :cond_1
    :goto_0
    return-void

    .line 412
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->showLoadingDialog()V

    .line 413
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->initSamsungAccountInfo()V

    goto :goto_0
.end method

.method private updateViewsWithLatestValues()V
    .locals 0

    .prologue
    .line 522
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->updateHeaderText()V

    .line 523
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->updateBasicResponseInfo()V

    .line 524
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->updateByAgeResponseInfo()V

    .line 525
    return-void
.end method


# virtual methods
.method public dismissLoadingDialog()V
    .locals 1

    .prologue
    .line 761
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->loadingDialog:Lcom/sec/android/app/shealth/walkingmate/utils/Utils$LoadingDialog;

    if-eqz v0, :cond_1

    .line 763
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->loadingDialog:Lcom/sec/android/app/shealth/walkingmate/utils/Utils$LoadingDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils$LoadingDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 764
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->loadingDialog:Lcom/sec/android/app/shealth/walkingmate/utils/Utils$LoadingDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils$LoadingDialog;->dismiss()V

    .line 766
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->loadingDialog:Lcom/sec/android/app/shealth/walkingmate/utils/Utils$LoadingDialog;

    .line 768
    :cond_1
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 0
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 840
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 841
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 740
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onAttach(Landroid/app/Activity;)V

    .line 741
    check-cast p1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;

    .end local p1    # "activity":Landroid/app/Activity;
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->mActivity:Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;

    .line 743
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->updateHandler:Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;

    if-eqz v0, :cond_0

    .line 744
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->updateHandler:Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;->resume()V

    .line 745
    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 830
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 831
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 159
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onCreate(Landroid/os/Bundle;)V

    .line 160
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->setRetainInstance(Z)V

    .line 161
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x0

    .line 165
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const v4, 0x7f0302a3

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 166
    .local v0, "content":Landroid/view/View;
    const v3, 0x7f080b82

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->mOnNetworkLayout:Landroid/widget/LinearLayout;

    .line 167
    const v3, 0x7f080b85

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->mNoNetworkLayout:Landroid/widget/RelativeLayout;

    .line 168
    const v3, 0x7f080be2

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->mWalk_my_zero:Landroid/widget/RelativeLayout;

    .line 170
    const v3, 0x7f080be7

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 171
    .local v2, "txt":Landroid/widget/TextView;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090bab

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 172
    .local v1, "str_pls_wait":Ljava/lang/String;
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 174
    const v3, 0x7f080be3

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->mZeroUpdatedTextView:Landroid/widget/TextView;

    .line 176
    new-instance v3, Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->unitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    .line 177
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->unitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->getDistanceUnit()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->distanceUnit:Ljava/lang/String;

    .line 178
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->distanceUnit:Ljava/lang/String;

    if-nez v3, :cond_0

    .line 180
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0900c7

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->distanceUnit:Ljava/lang/String;

    .line 182
    :cond_0
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->init(Landroid/view/View;)V

    .line 184
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->isNetworkConnted()Z

    move-result v3

    if-nez v3, :cond_1

    .line 185
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->mNoNetworkLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 186
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->mOnNetworkLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 187
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->removeShareViaButton()V

    .line 196
    :goto_0
    return-object v0

    .line 190
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->mNoNetworkLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v7}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 191
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->mOnNetworkLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 192
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->addShareViaButton()V

    .line 193
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->updateValues()V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 233
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->cancelUpdateTask()V

    .line 234
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onDestroy()V

    .line 235
    return-void
.end method

.method public onDetach()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 749
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onDetach()V

    .line 751
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->updateHandler:Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;

    if-eqz v0, :cond_0

    .line 753
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->updateHandler:Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;->pause()V

    .line 754
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->updateHandler:Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 755
    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->updateHandler:Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;

    .line 757
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->dismissLoadingDialog()V

    .line 758
    return-void
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 228
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onPause()V

    .line 229
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 222
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onResume()V

    .line 223
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->unitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->getDistanceUnit()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->distanceUnit:Ljava/lang/String;

    .line 224
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 835
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 836
    return-void
.end method
