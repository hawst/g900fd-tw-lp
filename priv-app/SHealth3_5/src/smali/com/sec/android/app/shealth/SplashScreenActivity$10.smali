.class Lcom/sec/android/app/shealth/SplashScreenActivity$10;
.super Ljava/lang/Object;
.source "SplashScreenActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/SplashScreenActivity;->getContentInitializationListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/SplashScreenActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/SplashScreenActivity;)V
    .locals 0

    .prologue
    .line 905
    iput-object p1, p0, Lcom/sec/android/app/shealth/SplashScreenActivity$10;->this$0:Lcom/sec/android/app/shealth/SplashScreenActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onContentInitialization(Landroid/view/View;Landroid/app/Activity;Landroid/app/Dialog;Landroid/os/Bundle;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V
    .locals 4
    .param p1, "content"    # Landroid/view/View;
    .param p2, "parentActivity"    # Landroid/app/Activity;
    .param p3, "dialog"    # Landroid/app/Dialog;
    .param p4, "savedInstanceState"    # Landroid/os/Bundle;
    .param p5, "okButtonHandler"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;

    .prologue
    .line 908
    const v2, 0x7f080040

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 910
    .local v1, "text":Landroid/widget/TextView;
    iget-object v3, p0, Lcom/sec/android/app/shealth/SplashScreenActivity$10;->this$0:Lcom/sec/android/app/shealth/SplashScreenActivity;

    const v2, 0x7f08031b

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    # setter for: Lcom/sec/android/app/shealth/SplashScreenActivity;->doNotShowCheckBox:Landroid/widget/CheckBox;
    invoke-static {v3, v2}, Lcom/sec/android/app/shealth/SplashScreenActivity;->access$1002(Lcom/sec/android/app/shealth/SplashScreenActivity;Landroid/widget/CheckBox;)Landroid/widget/CheckBox;

    .line 911
    const v2, 0x7f08031c

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 912
    .local v0, "doNotShowText":Landroid/widget/TextView;
    const v2, 0x7f09078c

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 914
    iget-object v2, p0, Lcom/sec/android/app/shealth/SplashScreenActivity$10;->this$0:Lcom/sec/android/app/shealth/SplashScreenActivity;

    # getter for: Lcom/sec/android/app/shealth/SplashScreenActivity;->doNotShowCheckBox:Landroid/widget/CheckBox;
    invoke-static {v2}, Lcom/sec/android/app/shealth/SplashScreenActivity;->access$1000(Lcom/sec/android/app/shealth/SplashScreenActivity;)Landroid/widget/CheckBox;

    move-result-object v2

    new-instance v3, Lcom/sec/android/app/shealth/SplashScreenActivity$10$1;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/SplashScreenActivity$10$1;-><init>(Lcom/sec/android/app/shealth/SplashScreenActivity$10;)V

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 920
    new-instance v2, Lcom/sec/android/app/shealth/SplashScreenActivity$10$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/SplashScreenActivity$10$2;-><init>(Lcom/sec/android/app/shealth/SplashScreenActivity$10;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 931
    iget-object v2, p0, Lcom/sec/android/app/shealth/SplashScreenActivity$10;->this$0:Lcom/sec/android/app/shealth/SplashScreenActivity;

    # getter for: Lcom/sec/android/app/shealth/SplashScreenActivity;->popupMode:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/SplashScreenActivity;->access$1100(Lcom/sec/android/app/shealth/SplashScreenActivity;)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 941
    :goto_0
    :pswitch_0
    return-void

    .line 933
    :pswitch_1
    const v2, 0x7f09079c

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 936
    :pswitch_2
    const v2, 0x7f0907a3

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 931
    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
