.class public final Lcom/sec/android/app/shealth/framework/repository/common/DBConstants$BLOOD_SUGAR_TYPE;
.super Ljava/lang/Object;
.source "DBConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/framework/repository/common/DBConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BLOOD_SUGAR_TYPE"
.end annotation


# static fields
.field public static final AFTER_MEAL:I = 0x1

.field public static final FASTING:I = 0x0

.field public static final ID:I = 0x3ee

.field public static final MG_UNIT:Ljava/lang/String; = "mg/dL"

.field public static final MMOL_UNIT:Ljava/lang/String; = "mmol/L"

.field public static final ROW_ID:I = 0x7d6


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
