.class public Lcom/sec/android/app/shealth/weight/WeightCalendarActivity;
.super Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;
.source "WeightCalendarActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected getCalendarIconId()I
    .locals 1

    .prologue
    .line 54
    const v0, 0x7f020036

    return v0
.end method

.method protected getDao()Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime;
    .locals 2

    .prologue
    .line 49
    new-instance v0, Lcom/sec/android/app/shealth/weight/weightdao/WeightDaoImplDb;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/WeightCalendarActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/weight/weightdao/WeightDaoImplDb;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method protected getDaysStatuses(JJ)Ljava/util/TreeMap;
    .locals 8
    .param p1, "startMonthTime"    # J
    .param p3, "endMonthTime"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ)",
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 61
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    .line 63
    .local v0, "calendarGoalMap":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/Long;Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;>;"
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/WeightCalendarActivity;->getDao()Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime;

    move-result-object v1

    .line 65
    .local v1, "dao":Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime;
    invoke-interface {v1, p1, p2, p3, p4}, Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime;->getDataForPeriod(JJ)Ljava/util/List;

    move-result-object v5

    .line 67
    .local v5, "monthDataList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;>;"
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;

    .line 69
    .local v2, "dayData":Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;
    new-instance v4, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;

    invoke-direct {v4}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;-><init>()V

    .line 70
    .local v4, "info":Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/WeightCalendarActivity;->getCalendarIconId()I

    move-result v6

    invoke-virtual {v4, v6}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;->setMedalResourceId(I)V

    .line 72
    invoke-virtual {p0, v2, p3, p4}, Lcom/sec/android/app/shealth/weight/WeightCalendarActivity;->isGoalReached(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;J)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 73
    const/4 v6, 0x1

    invoke-virtual {v4, v6}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;->setGoalAchieved(Z)V

    .line 74
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/WeightCalendarActivity;->getMedalId()I

    move-result v6

    invoke-virtual {v4, v6}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;->setMedalResourceId(I)V

    .line 77
    :cond_0
    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/weight/WeightCalendarActivity;->getSampleTime(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;)J

    move-result-wide v6

    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v0, v6, v4}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 82
    .end local v2    # "dayData":Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;
    .end local v4    # "info":Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;
    :cond_1
    return-object v0
.end method

.method protected getMedalId()I
    .locals 1

    .prologue
    .line 95
    const/4 v0, 0x0

    return v0
.end method

.method protected getSampleTime(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;)J
    .locals 3
    .param p1, "entryData"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;

    .prologue
    .line 43
    move-object v0, p1

    check-cast v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    .line 44
    .local v0, "dayData":Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getSampleTime()J

    move-result-wide v1

    return-wide v1
.end method

.method protected isGoalReached(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;J)Z
    .locals 1
    .param p1, "entryData"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;
    .param p2, "endMonthTime"    # J

    .prologue
    .line 88
    const/4 v0, 0x0

    return v0
.end method
