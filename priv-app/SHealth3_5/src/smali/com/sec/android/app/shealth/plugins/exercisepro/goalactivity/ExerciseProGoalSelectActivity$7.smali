.class Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity$7;
.super Ljava/lang/Object;
.source "ExerciseProGoalSelectActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnCancelButtonClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;->showMaxHrPopup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;)V
    .locals 0

    .prologue
    .line 232
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancelButtonClick(Landroid/app/Activity;)V
    .locals 3
    .param p1, "parentActivity"    # Landroid/app/Activity;

    .prologue
    const/4 v2, 0x0

    .line 236
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isNeedMaxHeartrateConfirm()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 237
    invoke-static {v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setNeedMaxHeartrateConfirm(Z)V

    .line 239
    :cond_0
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setIsRealTimeMaxHRAutoUpdate(Z)V

    .line 240
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;

    const-class v1, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;->startNextActivity(Ljava/lang/Class;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;->access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;Ljava/lang/Class;)V

    .line 241
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;->setAllButtonsWereClicked(Z)V
    invoke-static {v0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;->access$500(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;Z)V

    .line 242
    return-void
.end method
