.class public Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;
.super Ljava/lang/Object;
.source "SpO2DeviceConnector.java"


# static fields
.field private static final MEASURING_TIMEOUT:I = 0x7530

.field private static TAG:Ljava/lang/String;

.field private static mInstance:Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;


# instance fields
.field private IsDeviceStarted:Z

.field public isRecord:Z

.field private mContext:Landroid/content/Context;

.field private mCountDownTimer:Landroid/os/CountDownTimer;

.field mDataListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;

.field private mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

.field private mEventListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;

.field private mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

.field mServiceConnectionListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ServiceConnectionListener;

.field private mSpO2SensorListener:Lcom/sec/android/app/shealth/spo2/SpO2SensorListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 6

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->IsDeviceStarted:Z

    .line 38
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->mContext:Landroid/content/Context;

    .line 42
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->isRecord:Z

    .line 360
    new-instance v0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector$2;-><init>(Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->mServiceConnectionListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ServiceConnectionListener;

    .line 431
    new-instance v0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector$4;

    const-wide/16 v2, 0x7530

    const-wide/16 v4, 0x3e8

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector$4;-><init>(Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;JJ)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->mCountDownTimer:Landroid/os/CountDownTimer;

    .line 61
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;
    .param p1, "x1"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    .prologue
    .line 25
    iput-object p1, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    .prologue
    .line 25
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->IsDeviceStarted:Z

    return v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;
    .param p1, "x1"    # Z

    .prologue
    .line 25
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->IsDeviceStarted:Z

    return p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->startDevice()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;)Lcom/sec/android/app/shealth/spo2/SpO2SensorListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->mSpO2SensorListener:Lcom/sec/android/app/shealth/spo2/SpO2SensorListener;

    return-object v0
.end method

.method public static getInstance()Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;
    .locals 2

    .prologue
    .line 45
    sget-object v0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->mInstance:Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    if-nez v0, :cond_0

    .line 46
    new-instance v0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->mInstance:Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    .line 48
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->mInstance:Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    iget-object v0, v0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->mEventListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;

    if-nez v0, :cond_1

    .line 49
    sget-object v0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->mInstance:Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    sget-object v1, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->mInstance:Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->getEventListener()Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->mEventListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;

    .line 51
    :cond_1
    sget-object v0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->mInstance:Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    iget-object v0, v0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->mDataListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;

    if-nez v0, :cond_2

    .line 52
    sget-object v0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->mInstance:Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    sget-object v1, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->mInstance:Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->getDataListener()Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->mDataListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;

    .line 55
    :cond_2
    sget-object v0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->mInstance:Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    return-object v0
.end method

.method private initSensor()V
    .locals 0

    .prologue
    .line 151
    return-void
.end method

.method private scanDevice()V
    .locals 10

    .prologue
    const/16 v9, 0x272a

    const/4 v8, 0x4

    .line 71
    sget-object v4, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "scanDevice"

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    const/4 v5, 0x4

    const/16 v6, 0x272a

    const/16 v7, 0x11

    invoke-virtual {v4, v5, v6, v7}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->getConnectedDeviceList(III)Ljava/util/List;

    move-result-object v0

    .line 77
    .local v0, "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    if-eqz v0, :cond_4

    .line 78
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 79
    sget-object v4, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->TAG:Ljava/lang/String;

    const-string v5, "deviceList == isEmpty"

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    iget-object v4, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->mDataListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;

    const/4 v5, 0x0

    const/16 v6, 0x103

    invoke-interface {v4, v5, v6}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;->onStopped(II)V

    .line 116
    .end local v0    # "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    :cond_0
    :goto_0
    return-void

    .line 82
    .restart local v0    # "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    :cond_1
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    .line 83
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    .line 84
    .local v1, "dvc":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getConnectivityType()I

    move-result v4

    if-ne v4, v8, :cond_2

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getType()I

    move-result v4

    if-ne v4, v9, :cond_2

    .line 86
    iput-object v1, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    .line 87
    iget-object v4, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    iget-object v5, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->mEventListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->join(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;)V

    .line 88
    sget-object v4, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "scanDevice() device: found- "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getType()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getConnectivityType()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getProtocol()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_0 .. :try_end_0} :catch_5

    goto :goto_1

    .line 101
    .end local v0    # "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    .end local v1    # "dvc":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    .end local v3    # "i$":Ljava/util/Iterator;
    :catch_0
    move-exception v2

    .line 102
    .local v2, "e":Landroid/os/RemoteException;
    invoke-virtual {v2}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 90
    .end local v2    # "e":Landroid/os/RemoteException;
    .restart local v0    # "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    .restart local v1    # "dvc":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    .restart local v3    # "i$":Ljava/util/Iterator;
    :cond_2
    :try_start_1
    sget-object v4, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "scanDevice() device: else - "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getType()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getConnectivityType()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getProtocol()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_1 .. :try_end_1} :catch_5

    goto/16 :goto_1

    .line 103
    .end local v0    # "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    .end local v1    # "dvc":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    .end local v3    # "i$":Ljava/util/Iterator;
    :catch_1
    move-exception v2

    .line 104
    .local v2, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v2}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto/16 :goto_0

    .line 93
    .end local v2    # "e":Ljava/lang/IllegalArgumentException;
    .restart local v0    # "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    .restart local v3    # "i$":Ljava/util/Iterator;
    :cond_3
    :try_start_2
    iget-object v4, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    if-nez v4, :cond_0

    .line 94
    iget-object v4, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->mDataListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;

    const/4 v5, 0x0

    const/16 v6, 0x103

    invoke-interface {v4, v5, v6}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;->onStopped(II)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_2 .. :try_end_2} :catch_5

    goto/16 :goto_0

    .line 105
    .end local v0    # "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    .end local v3    # "i$":Ljava/util/Iterator;
    :catch_2
    move-exception v2

    .line 106
    .local v2, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto/16 :goto_0

    .line 98
    .end local v2    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    .restart local v0    # "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    :cond_4
    :try_start_3
    sget-object v4, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->TAG:Ljava/lang/String;

    const-string v5, "deviceList == null"

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    iget-object v4, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->mDataListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;

    const/4 v5, 0x0

    const/16 v6, 0x103

    invoke-interface {v4, v5, v6}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;->onStopped(II)V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_4
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_3 .. :try_end_3} :catch_5

    goto/16 :goto_0

    .line 107
    .end local v0    # "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    :catch_3
    move-exception v2

    .line 108
    .local v2, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;
    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;->printStackTrace()V

    goto/16 :goto_0

    .line 109
    .end local v2    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;
    :catch_4
    move-exception v2

    .line 111
    .local v2, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v2}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto/16 :goto_0

    .line 112
    .end local v2    # "e":Ljava/lang/IllegalStateException;
    :catch_5
    move-exception v2

    .line 114
    .local v2, "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;->printStackTrace()V

    goto/16 :goto_0
.end method

.method private startDevice()V
    .locals 4

    .prologue
    .line 154
    sget-object v1, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->TAG:Ljava/lang/String;

    const-string v2, "Start"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 157
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    if-nez v1, :cond_0

    .line 158
    sget-object v1, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->TAG:Ljava/lang/String;

    const-string v2, "SHealthDeviceFinder is null"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->mServiceConnectionListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ServiceConnectionListener;

    invoke-direct {v1, v2, v3}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ServiceConnectionListener;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    .line 190
    :goto_0
    return-void

    .line 161
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    if-nez v1, :cond_1

    .line 162
    invoke-direct {p0}, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->scanDevice()V

    goto :goto_0

    .line 165
    :cond_1
    :try_start_0
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->IsDeviceStarted:Z

    if-nez v1, :cond_2

    .line 166
    sget-object v1, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "startReceivingData_1"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->mDataListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->startReceivingData(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;)V

    .line 168
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->isRecord:Z

    if-eqz v1, :cond_2

    .line 169
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->startRecord()V

    .line 172
    :cond_2
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->IsDeviceStarted:Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_0 .. :try_end_0} :catch_5

    goto :goto_0

    .line 173
    :catch_0
    move-exception v0

    .line 174
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 175
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 176
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 177
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .line 178
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    .line 179
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_3
    move-exception v0

    .line 180
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto :goto_0

    .line 181
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    :catch_4
    move-exception v0

    .line 183
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException;->printStackTrace()V

    goto :goto_0

    .line 184
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException;
    :catch_5
    move-exception v0

    .line 186
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;->printStackTrace()V

    goto :goto_0
.end method

.method private stopDevice()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 232
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    if-eqz v2, :cond_0

    .line 234
    :try_start_0
    sget-object v2, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "stopReceivingData_1"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 235
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->stopReceivingData()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_0 .. :try_end_0} :catch_4

    .line 251
    :goto_0
    :try_start_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->leave()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_1 .. :try_end_1} :catch_6
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_8

    .line 262
    :goto_1
    iput-object v4, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    .line 266
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    if-eqz v2, :cond_1

    .line 268
    :try_start_2
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->stopScan()V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_9
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_2 .. :try_end_2} :catch_a
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_b

    .line 278
    :goto_2
    :try_start_3
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->close()V
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_c
    .catch Ljava/util/ConcurrentModificationException; {:try_start_3 .. :try_end_3} :catch_d

    .line 284
    :goto_3
    iput-object v4, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    .line 286
    :cond_1
    return-void

    .line 236
    :catch_0
    move-exception v1

    .line 237
    .local v1, "e1":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 238
    .end local v1    # "e1":Landroid/os/RemoteException;
    :catch_1
    move-exception v1

    .line 239
    .local v1, "e1":Ljava/lang/IllegalStateException;
    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    .line 240
    .end local v1    # "e1":Ljava/lang/IllegalStateException;
    :catch_2
    move-exception v1

    .line 241
    .local v1, "e1":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto :goto_0

    .line 242
    .end local v1    # "e1":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    :catch_3
    move-exception v0

    .line 243
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0

    .line 244
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :catch_4
    move-exception v0

    .line 246
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;->printStackTrace()V

    goto :goto_0

    .line 252
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    :catch_5
    move-exception v0

    .line 253
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1

    .line 254
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_6
    move-exception v0

    .line 255
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto :goto_1

    .line 256
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    :catch_7
    move-exception v0

    .line 257
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_1

    .line 258
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_8
    move-exception v0

    .line 259
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_1

    .line 269
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :catch_9
    move-exception v1

    .line 270
    .local v1, "e1":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_2

    .line 271
    .end local v1    # "e1":Landroid/os/RemoteException;
    :catch_a
    move-exception v1

    .line 272
    .local v1, "e1":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto :goto_2

    .line 273
    .end local v1    # "e1":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    :catch_b
    move-exception v0

    .line 274
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_2

    .line 279
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_c
    move-exception v0

    .line 280
    .restart local v0    # "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_3

    .line 281
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_d
    move-exception v0

    .line 282
    .local v0, "e":Ljava/util/ConcurrentModificationException;
    invoke-virtual {v0}, Ljava/util/ConcurrentModificationException;->printStackTrace()V

    goto :goto_3
.end method


# virtual methods
.method public clearListener()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 225
    iput-object v0, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->mSpO2SensorListener:Lcom/sec/android/app/shealth/spo2/SpO2SensorListener;

    .line 226
    iput-object v0, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->mEventListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;

    .line 227
    iput-object v0, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->mDataListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;

    .line 228
    return-void
.end method

.method getDataListener()Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;
    .locals 1

    .prologue
    .line 399
    new-instance v0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector$3;-><init>(Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;)V

    return-object v0
.end method

.method getEventListener()Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;
    .locals 1

    .prologue
    .line 290
    new-instance v0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector$1;-><init>(Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;)V

    return-object v0
.end method

.method public isStarted()Z
    .locals 1

    .prologue
    .line 66
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->IsDeviceStarted:Z

    return v0
.end method

.method public startCountDownTimer()V
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->mCountDownTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->start()Landroid/os/CountDownTimer;

    .line 131
    return-void
.end method

.method public startMeasuring(Landroid/content/Context;Lcom/sec/android/app/shealth/spo2/SpO2SensorListener;Z)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "spO2SensorListener"    # Lcom/sec/android/app/shealth/spo2/SpO2SensorListener;
    .param p3, "isRecordON"    # Z

    .prologue
    .line 120
    iput-object p1, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->mContext:Landroid/content/Context;

    .line 121
    if-eqz p2, :cond_0

    .line 122
    iput-object p2, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->mSpO2SensorListener:Lcom/sec/android/app/shealth/spo2/SpO2SensorListener;

    .line 124
    :cond_0
    iput-boolean p3, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->isRecord:Z

    .line 125
    invoke-direct {p0}, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->startDevice()V

    .line 126
    invoke-direct {p0}, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->initSensor()V

    .line 127
    return-void
.end method

.method public startRecord()V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 195
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->record()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_0 .. :try_end_0} :catch_5

    .line 215
    :goto_0
    return-void

    .line 196
    :catch_0
    move-exception v0

    .line 198
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 199
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 201
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 202
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .line 204
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    .line 205
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_3
    move-exception v0

    .line 207
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto :goto_0

    .line 208
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    :catch_4
    move-exception v0

    .line 210
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;->printStackTrace()V

    goto :goto_0

    .line 211
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    :catch_5
    move-exception v0

    .line 213
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;->printStackTrace()V

    goto :goto_0
.end method

.method public stopCountDownTimer()V
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->mCountDownTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 134
    return-void
.end method

.method public stopMeasuring()V
    .locals 2

    .prologue
    .line 218
    sget-object v0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "stopMeasuring"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 219
    invoke-direct {p0}, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->stopDevice()V

    .line 221
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->IsDeviceStarted:Z

    .line 222
    return-void
.end method
