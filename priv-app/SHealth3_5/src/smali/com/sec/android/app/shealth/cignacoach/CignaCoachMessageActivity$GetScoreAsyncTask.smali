.class Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity$GetScoreAsyncTask;
.super Landroid/os/AsyncTask;
.source "CignaCoachMessageActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "GetScoreAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;",
        "Ljava/lang/Void;",
        "Lcom/cigna/coach/apiobjects/Scores;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;)V
    .locals 0

    .prologue
    .line 375
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity$GetScoreAsyncTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;)Lcom/cigna/coach/apiobjects/Scores;
    .locals 2
    .param p1, "params"    # [Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    .prologue
    .line 380
    const/4 v1, 0x0

    aget-object v0, p1, v1

    .line 382
    .local v0, "category":Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getScores(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;)Lcom/cigna/coach/apiobjects/Scores;

    move-result-object v1

    return-object v1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 375
    check-cast p1, [Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity$GetScoreAsyncTask;->doInBackground([Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;)Lcom/cigna/coach/apiobjects/Scores;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/cigna/coach/apiobjects/Scores;)V
    .locals 2
    .param p1, "scores"    # Lcom/cigna/coach/apiobjects/Scores;

    .prologue
    .line 387
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 389
    if-nez p1, :cond_0

    .line 390
    # getter for: Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GetScoreAsyncTask Result is null"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 398
    :goto_0
    return-void

    .line 392
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity$GetScoreAsyncTask;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 393
    # getter for: Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GetScoreAsyncTask Canceled"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 397
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity$GetScoreAsyncTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;

    invoke-virtual {p1}, Lcom/cigna/coach/apiobjects/Scores;->getCategoryInfo()Ljava/util/Hashtable;

    move-result-object v1

    # invokes: Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->updateScore(Ljava/util/Hashtable;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->access$200(Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;Ljava/util/Hashtable;)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 375
    check-cast p1, Lcom/cigna/coach/apiobjects/Scores;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity$GetScoreAsyncTask;->onPostExecute(Lcom/cigna/coach/apiobjects/Scores;)V

    return-void
.end method
