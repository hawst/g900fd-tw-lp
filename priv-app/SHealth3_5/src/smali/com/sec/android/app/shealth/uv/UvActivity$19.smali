.class Lcom/sec/android/app/shealth/uv/UvActivity$19;
.super Ljava/lang/Object;
.source "UvActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/uv/UvActivity;->showNoSensorInformationDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/uv/UvActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/uv/UvActivity;)V
    .locals 0

    .prologue
    .line 843
    iput-object p1, p0, Lcom/sec/android/app/shealth/uv/UvActivity$19;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onContentInitialization(Landroid/view/View;Landroid/app/Activity;Landroid/app/Dialog;Landroid/os/Bundle;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V
    .locals 3
    .param p1, "content"    # Landroid/view/View;
    .param p2, "parentActivity"    # Landroid/app/Activity;
    .param p3, "dialog"    # Landroid/app/Dialog;
    .param p4, "savedInstanceState"    # Landroid/os/Bundle;
    .param p5, "okButtonHandler"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;

    .prologue
    .line 849
    const/4 v0, 0x0

    .line 850
    .local v0, "isChecked":Z
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity$19;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    # getter for: Lcom/sec/android/app/shealth/uv/UvActivity;->doNotShowCheckBox:Landroid/widget/CheckBox;
    invoke-static {v1}, Lcom/sec/android/app/shealth/uv/UvActivity;->access$1200(Lcom/sec/android/app/shealth/uv/UvActivity;)Landroid/widget/CheckBox;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 851
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity$19;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    # getter for: Lcom/sec/android/app/shealth/uv/UvActivity;->doNotShowCheckBox:Landroid/widget/CheckBox;
    invoke-static {v1}, Lcom/sec/android/app/shealth/uv/UvActivity;->access$1200(Lcom/sec/android/app/shealth/uv/UvActivity;)Landroid/widget/CheckBox;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    .line 853
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvActivity$19;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    const v1, 0x7f080afa

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    # setter for: Lcom/sec/android/app/shealth/uv/UvActivity;->doNotShowCheckBox:Landroid/widget/CheckBox;
    invoke-static {v2, v1}, Lcom/sec/android/app/shealth/uv/UvActivity;->access$1202(Lcom/sec/android/app/shealth/uv/UvActivity;Landroid/widget/CheckBox;)Landroid/widget/CheckBox;

    .line 854
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvActivity$19;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    const v1, 0x7f080581

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    # setter for: Lcom/sec/android/app/shealth/uv/UvActivity;->mShowAgainNoSensorCheckLayout:Landroid/widget/RelativeLayout;
    invoke-static {v2, v1}, Lcom/sec/android/app/shealth/uv/UvActivity;->access$2802(Lcom/sec/android/app/shealth/uv/UvActivity;Landroid/widget/RelativeLayout;)Landroid/widget/RelativeLayout;

    .line 855
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity$19;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    # getter for: Lcom/sec/android/app/shealth/uv/UvActivity;->doNotShowCheckBox:Landroid/widget/CheckBox;
    invoke-static {v1}, Lcom/sec/android/app/shealth/uv/UvActivity;->access$1200(Lcom/sec/android/app/shealth/uv/UvActivity;)Landroid/widget/CheckBox;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 856
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity$19;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    # getter for: Lcom/sec/android/app/shealth/uv/UvActivity;->doNotShowCheckBox:Landroid/widget/CheckBox;
    invoke-static {v1}, Lcom/sec/android/app/shealth/uv/UvActivity;->access$1200(Lcom/sec/android/app/shealth/uv/UvActivity;)Landroid/widget/CheckBox;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvActivity$19;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    iget-object v2, v2, Lcom/sec/android/app/shealth/uv/UvActivity;->againOnCheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 857
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity$19;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    # getter for: Lcom/sec/android/app/shealth/uv/UvActivity;->mShowAgainNoSensorCheckLayout:Landroid/widget/RelativeLayout;
    invoke-static {v1}, Lcom/sec/android/app/shealth/uv/UvActivity;->access$2800(Lcom/sec/android/app/shealth/uv/UvActivity;)Landroid/widget/RelativeLayout;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvActivity$19;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    iget-object v2, v2, Lcom/sec/android/app/shealth/uv/UvActivity;->mCheckboxLayoutClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 858
    return-void
.end method
