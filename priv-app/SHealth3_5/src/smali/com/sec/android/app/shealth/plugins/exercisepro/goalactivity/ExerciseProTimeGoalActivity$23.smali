.class Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity$23;
.super Ljava/lang/Object;
.source "ExerciseProTimeGoalActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;)V
    .locals 0

    .prologue
    .line 1082
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity$23;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDialogButtonClick(Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;)V
    .locals 5
    .param p1, "dialogButtonType"    # Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    .prologue
    const/4 v4, 0x0

    .line 1086
    sget-object v1, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->POSITIVE:Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    if-ne v1, p1, :cond_1

    .line 1089
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity$23;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;->goalValue:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;->access$1400(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;)I

    move-result v1

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;->DURATION_MIN_ADVICE_GOAL:I
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;->access$1500()I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 1090
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity$23;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;->hourEditText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;->access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;)Landroid/widget/EditText;

    move-result-object v1

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "0"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1091
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity$23;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;->minutesEditText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;->access$600(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;)Landroid/widget/EditText;

    move-result-object v1

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;->DURATION_MIN_ADVICE_GOAL:I
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;->access$1500()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1094
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity$23;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;->minutesEditText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;->access$600(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->requestFocus()Z

    .line 1095
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity$23;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;->minutesEditText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;->access$600(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;)Landroid/widget/EditText;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity$23;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;->minutesEditText:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;->access$600(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    invoke-virtual {v1, v4, v2}, Landroid/widget/EditText;->setSelection(II)V

    .line 1096
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "Value being set...!!!"

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1108
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity$23;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "dialog_exer"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/DialogFragment;

    .line 1109
    .local v0, "dialog_exer":Landroid/support/v4/app/DialogFragment;
    if-eqz v0, :cond_1

    .line 1111
    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    .line 1114
    .end local v0    # "dialog_exer":Landroid/support/v4/app/DialogFragment;
    :cond_1
    return-void

    .line 1097
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity$23;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;->goalValue:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;->access$1400(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;)I

    move-result v1

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;->DURATION_MAX_ADVICE_GOAL:I
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;->access$1600()I

    move-result v2

    if-le v1, v2, :cond_0

    .line 1098
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity$23;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;->hourEditText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;->access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;)Landroid/widget/EditText;

    move-result-object v1

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;->DURATION_MAX_ADVICE_GOAL:I
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;->access$1600()I

    move-result v2

    div-int/lit8 v2, v2, 0x3c

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1099
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity$23;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;->minutesEditText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;->access$600(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;)Landroid/widget/EditText;

    move-result-object v1

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "0"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1100
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity$23;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;->minutesEditText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;->access$600(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->requestFocus()Z

    .line 1101
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity$23;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;->minutesEditText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;->access$600(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;)Landroid/widget/EditText;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity$23;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;->minutesEditText:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;->access$600(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    invoke-virtual {v1, v4, v2}, Landroid/widget/EditText;->setSelection(II)V

    goto :goto_0
.end method
