.class public abstract Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;
.super Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;
.source "CommonDaoWithDateTimeImplDb.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;",
        ">",
        "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb",
        "<TT;>;",
        "Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime",
        "<TT;>;"
    }
.end annotation


# static fields
.field private static final GROUP_BY_TIME_MACROS_FORMAT:Ljava/lang/String; = "strftime(\"%s\",([%s]/1000),\'unixepoch\',\'localtime\')"

.field private static final TIME_FORMAT_DAY:Ljava/lang/String; = "%d-%m-%Y"

.field private static final TIME_FORMAT_HOUR:Ljava/lang/String; = "%H-%d-%m-%Y"

.field private static final TIME_FORMAT_MONTH:Ljava/lang/String; = "%m-%Y"

.field private static final TIME_FORMAT_YEAR:Ljava/lang/String; = "%Y"


# instance fields
.field private final mIdColumnName:Ljava/lang/String;

.field private mIdColumnNameCheckedStatus:Z

.field protected final mKeyValueColumnName:Ljava/lang/String;

.field private mKeyValueColumnNameCheckedStatus:Z

.field private final mTimeColumnName:Ljava/lang/String;

.field private mTimeColumnNameCheckedStatus:Z


# direct methods
.method protected constructor <init>(Landroid/net/Uri;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "timeColumnNameForCheckRules"    # Ljava/lang/String;
    .param p4, "idColumnName"    # Ljava/lang/String;
    .param p5, "keyValueColumnName"    # Ljava/lang/String;

    .prologue
    .local p0, "this":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;, "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb<TT;>;"
    const/4 v0, 0x0

    .line 66
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;-><init>(Landroid/net/Uri;Landroid/content/Context;)V

    .line 43
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->mIdColumnNameCheckedStatus:Z

    .line 45
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->mTimeColumnNameCheckedStatus:Z

    .line 47
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->mKeyValueColumnNameCheckedStatus:Z

    .line 67
    iput-object p3, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->mTimeColumnName:Ljava/lang/String;

    .line 68
    iput-object p4, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->mIdColumnName:Ljava/lang/String;

    .line 69
    iput-object p5, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->mKeyValueColumnName:Ljava/lang/String;

    .line 70
    return-void
.end method


# virtual methods
.method public deleteAllDataForDay(J)I
    .locals 4
    .param p1, "timeInDay"    # J

    .prologue
    .line 81
    .local p0, "this":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;, "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb<TT;>;"
    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v0

    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v2

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->deleteAllDataForPeriod(JJ)I

    move-result v0

    return v0
.end method

.method public deleteAllDataForPeriod(JJ)I
    .locals 5
    .param p1, "periodStart"    # J
    .param p3, "periodEnd"    # J

    .prologue
    .line 74
    .local p0, "this":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;, "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb<TT;>;"
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->getPeriodRangeFilter(JJ)Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->getCommonMethodsFilter()Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;->AND(Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;)Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;

    move-result-object v0

    .line 76
    .local v0, "filter":Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->uri:Landroid/net/Uri;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;->getSQLCode()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    return v1
.end method

.method public getAggregationFunctionResultForPeriod(JJLcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime$SqlAggregateFunction;)F
    .locals 8
    .param p1, "periodStart"    # J
    .param p3, "periodEnd"    # J
    .param p5, "function"    # Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime$SqlAggregateFunction;

    .prologue
    .line 249
    .local p0, "this":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;, "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb<TT;>;"
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->getKeyValueColumnName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->getCommonMethodsFilter()Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;

    move-result-object v7

    move-object v0, p0

    move-wide v1, p1

    move-wide v3, p3

    move-object v5, p5

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->getAggregationFunctionResultForPeriod(JJLcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime$SqlAggregateFunction;Ljava/lang/String;Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;)F

    move-result v0

    return v0
.end method

.method protected getAggregationFunctionResultForPeriod(JJLcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime$SqlAggregateFunction;Ljava/lang/String;Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;)F
    .locals 8
    .param p1, "periodStart"    # J
    .param p3, "periodEnd"    # J
    .param p5, "function"    # Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime$SqlAggregateFunction;
    .param p6, "keyColumnName"    # Ljava/lang/String;
    .param p7, "filter"    # Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;

    .prologue
    .local p0, "this":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;, "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb<TT;>;"
    const/4 v7, 0x1

    const/4 v5, 0x0

    .line 264
    if-eqz p6, :cond_0

    invoke-virtual {p6}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 265
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "keyValueColumnName must contain valid column name!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 267
    :cond_1
    if-nez p5, :cond_2

    .line 268
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "function shoudn\'t be null!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 270
    :cond_2
    new-array v2, v7, [Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p5}, Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime$SqlAggregateFunction;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v5

    .line 271
    .local v2, "projection":[Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->getTimeColumnName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " >= ? "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->getTimeColumnName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " <= ? "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 273
    .local v3, "selection":Ljava/lang/String;
    if-eqz p7, :cond_3

    .line 274
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p7}, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;->getSQLCode()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 277
    :cond_3
    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-static {p3, p4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v7

    .line 279
    .local v4, "args":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 281
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->uri:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 282
    if-eqz v6, :cond_5

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 283
    const/4 v0, 0x0

    aget-object v0, v2, v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getFloat(I)F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 289
    if-eqz v6, :cond_4

    .line 290
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_4
    :goto_0
    return v0

    .line 285
    :cond_5
    const/4 v0, 0x0

    .line 289
    if-eqz v6, :cond_4

    .line 290
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 289
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_6

    .line 290
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_6
    throw v0
.end method

.method public getDataById(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;
    .locals 1
    .param p1, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)TT;"
        }
    .end annotation

    .prologue
    .line 161
    .local p0, "this":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;, "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb<TT;>;"
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->getCommonMethodsFilter()Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->getDataById(JLcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;)Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;

    move-result-object v0

    return-object v0
.end method

.method protected getDataById(JLcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;)Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;
    .locals 7
    .param p1, "id"    # J
    .param p3, "filter"    # Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 169
    .local p0, "this":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;, "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb<TT;>;"
    new-instance v2, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;

    new-instance v3, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->getIdColumnName()Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    const-string v6, "="

    invoke-direct {v3, v4, v5, v6}, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v2, v3}, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;-><init>(Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;)V

    invoke-virtual {v2, p3}, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;->AND(Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;)Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;

    move-result-object v0

    .line 171
    .local v0, "filterQueryBuilder":Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->getAllDatas(Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;)Ljava/util/List;

    move-result-object v1

    .line 172
    .local v1, "result":Ljava/util/List;, "Ljava/util/List<TT;>;"
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 173
    :cond_0
    const/4 v2, 0x0

    .line 177
    :goto_0
    return-object v2

    .line 174
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_2

    .line 175
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "We recieve two items by unique ID, check implementation!"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 177
    :cond_2
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;

    goto :goto_0
.end method

.method public final getDataForDay(J)Ljava/util/List;
    .locals 5
    .param p1, "timeInDay"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 117
    .local p0, "this":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;, "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb<TT;>;"
    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v1

    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v3

    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->getDataForPeriod(JJ)Ljava/util/List;

    move-result-object v0

    .line 118
    .local v0, "result":Ljava/util/List;, "Ljava/util/List<TT;>;"
    if-nez v0, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .end local v0    # "result":Ljava/util/List;, "Ljava/util/List<TT;>;"
    :cond_0
    return-object v0
.end method

.method protected final getDataForDay(JLcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;)Ljava/util/List;
    .locals 8
    .param p1, "timeInDay"    # J
    .param p3, "filter"    # Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;",
            ")",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 128
    .local p0, "this":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;, "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb<TT;>;"
    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v1

    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v3

    const/4 v6, 0x0

    move-object v0, p0

    move-object v5, p3

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->getDataForPeriod(JJLcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;)Ljava/util/List;

    move-result-object v7

    .line 129
    .local v7, "result":Ljava/util/List;, "Ljava/util/List<TT;>;"
    if-nez v7, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v7

    .end local v7    # "result":Ljava/util/List;, "Ljava/util/List<TT;>;"
    :cond_0
    return-object v7
.end method

.method public getDataForPeriod(JJ)Ljava/util/List;
    .locals 7
    .param p1, "periodStart"    # J
    .param p3, "periodEnd"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ)",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 134
    .local p0, "this":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;, "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb<TT;>;"
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->getCommonMethodsFilter()Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;

    move-result-object v5

    const/4 v6, 0x0

    move-object v0, p0

    move-wide v1, p1

    move-wide v3, p3

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->getDataForPeriod(JJLcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected getDataForPeriod(JJLcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;)Ljava/util/List;
    .locals 7
    .param p1, "periodStart"    # J
    .param p3, "periodEnd"    # J
    .param p5, "filter"    # Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;
    .param p6, "sortOrder"    # Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;",
            "Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;",
            ")",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;, "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb<TT;>;"
    const/4 v2, 0x0

    .line 151
    new-instance v1, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;

    new-instance v3, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->getTimeColumnName()Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    const-string v6, ">="

    invoke-direct {v3, v4, v5, v6}, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v1, v3}, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;-><init>(Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;)V

    new-instance v3, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->getTimeColumnName()Ljava/lang/String;

    move-result-object v4

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    const-string v6, "<="

    invoke-direct {v3, v4, v5, v6}, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;->AND(Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;)Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;->AND(Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;)Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;

    move-result-object v0

    .line 155
    .local v0, "filterBuilder":Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;->getSQLCode()Ljava/lang/String;

    move-result-object v3

    if-nez p6, :cond_0

    move-object v1, v2

    :goto_0
    invoke-virtual {p0, v2, v3, v2, v1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->getAllDatas([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    return-object v1

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->getTimeColumnName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p6}, Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getDataForPeriod(JJLcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;)Ljava/util/List;
    .locals 7
    .param p1, "periodStart"    # J
    .param p3, "periodEnd"    # J
    .param p5, "sortOrder"    # Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;",
            ")",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 139
    .local p0, "this":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;, "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb<TT;>;"
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->getCommonMethodsFilter()Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;

    move-result-object v5

    move-object v0, p0

    move-wide v1, p1

    move-wide v3, p3

    move-object v6, p5

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->getDataForPeriod(JJLcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getDataForPeriodsWithItAggregation(JJLcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime$SqlAggregateFunction;)Ljava/util/Map;
    .locals 9
    .param p1, "periodStart"    # J
    .param p3, "periodEnd"    # J
    .param p5, "periodType"    # Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;
    .param p6, "function"    # Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime$SqlAggregateFunction;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;",
            "Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime$SqlAggregateFunction;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 296
    .local p0, "this":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;, "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb<TT;>;"
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->getKeyValueColumnName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->getCommonMethodsFilter()Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;

    move-result-object v8

    move-object v0, p0

    move-wide v1, p1

    move-wide v3, p3

    move-object v5, p5

    move-object v6, p6

    invoke-virtual/range {v0 .. v8}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->getDataForPeriodsWithItAggregation(JJLcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime$SqlAggregateFunction;Ljava/lang/String;Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method protected getDataForPeriodsWithItAggregation(JJLcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime$SqlAggregateFunction;Ljava/lang/String;Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;)Ljava/util/Map;
    .locals 13
    .param p1, "periodStart"    # J
    .param p3, "periodEnd"    # J
    .param p5, "periodType"    # Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;
    .param p6, "function"    # Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime$SqlAggregateFunction;
    .param p7, "keyValueColumnName"    # Ljava/lang/String;
    .param p8, "filter"    # Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;",
            "Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime$SqlAggregateFunction;",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 314
    .local p0, "this":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;, "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb<TT;>;"
    if-eqz p7, :cond_0

    invoke-virtual/range {p7 .. p7}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 315
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "keyValueColumnName must contain valid column name!"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 317
    :cond_1
    if-nez p6, :cond_2

    .line 318
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "function shoudn\'t be null!"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 320
    :cond_2
    new-instance v10, Ljava/util/Hashtable;

    invoke-direct {v10}, Ljava/util/Hashtable;-><init>()V

    .line 321
    .local v10, "result":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Long;Ljava/lang/Float;>;"
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p6 .. p6}, Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime$SqlAggregateFunction;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p7

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 322
    .local v7, "aggregationField":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->getTimeColumnName()Ljava/lang/String;

    move-result-object v12

    .line 323
    .local v12, "timeColumnName":Ljava/lang/String;
    const/4 v1, 0x2

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object v12, v3, v1

    const/4 v1, 0x1

    aput-object v7, v3, v1

    .line 324
    .local v3, "projection":[Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " >= ? "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " AND "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " <= ? "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-nez p8, :cond_6

    const-string v1, ""

    :goto_0
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") GROUP BY ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p5

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->getPeriodGroupByValue(Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 329
    .local v4, "select":Ljava/lang/String;
    const/4 v1, 0x2

    new-array v5, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v1

    const/4 v1, 0x1

    invoke-static/range {p3 .. p4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v1

    .line 330
    .local v5, "arg":[Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->uri:Landroid/net/Uri;

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 332
    .local v8, "cursor":Landroid/database/Cursor;
    if-eqz v8, :cond_4

    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 334
    :cond_3
    invoke-interface {v8, v12}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    .line 335
    .local v11, "time":Ljava/lang/Long;
    invoke-interface {v8, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    .line 336
    .local v9, "funcResult":Ljava/lang/Float;
    invoke-interface {v10, v11, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 337
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_3

    .line 341
    .end local v9    # "funcResult":Ljava/lang/Float;
    .end local v11    # "time":Ljava/lang/Long;
    :cond_4
    if-eqz v8, :cond_5

    .line 342
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_5
    return-object v10

    .line 324
    .end local v4    # "select":Ljava/lang/String;
    .end local v5    # "arg":[Ljava/lang/String;
    .end local v8    # "cursor":Landroid/database/Cursor;
    :cond_6
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " AND ("

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual/range {p8 .. p8}, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;->getSQLCode()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, ")"

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 341
    .restart local v4    # "select":Ljava/lang/String;
    .restart local v5    # "arg":[Ljava/lang/String;
    .restart local v8    # "cursor":Landroid/database/Cursor;
    :catchall_0
    move-exception v1

    if-eqz v8, :cond_7

    .line 342
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_7
    throw v1
.end method

.method protected getIdColumnName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 217
    .local p0, "this":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;, "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb<TT;>;"
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->mIdColumnNameCheckedStatus:Z

    if-nez v0, :cond_2

    .line 218
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->mIdColumnName:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->mIdColumnName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 219
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "This operation requires idColumnName to be set!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 221
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->mIdColumnNameCheckedStatus:Z

    .line 223
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->mIdColumnName:Ljava/lang/String;

    return-object v0
.end method

.method protected getKeyValueColumnName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 202
    .local p0, "this":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;, "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb<TT;>;"
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->mKeyValueColumnNameCheckedStatus:Z

    if-nez v0, :cond_2

    .line 203
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->mKeyValueColumnName:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->mKeyValueColumnName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 204
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "This operation requires keyValueColumnName to be set!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 206
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->mKeyValueColumnNameCheckedStatus:Z

    .line 208
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->mKeyValueColumnName:Ljava/lang/String;

    return-object v0
.end method

.method public getLastCreatedData()Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 467
    .local p0, "this":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;, "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb<TT;>;"
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->getCommonMethodsFilter()Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;

    move-result-object v0

    const-string v1, "create_time"

    sget-object v2, Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;->DESC:Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->getLastDataWithFilter(Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;Ljava/lang/String;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;)Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;

    move-result-object v0

    return-object v0
.end method

.method public getLastDataBeforeTime(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;
    .locals 6
    .param p1, "beforeTime"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)TT;"
        }
    .end annotation

    .prologue
    .line 472
    .local p0, "this":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;, "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb<TT;>;"
    new-instance v1, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;

    new-instance v2, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->getTimeColumnName()Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    const-string v5, "<="

    invoke-direct {v2, v3, v4, v5}, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v1, v2}, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;-><init>(Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;)V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->getCommonMethodsFilter()Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;->AND(Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;)Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;

    move-result-object v0

    .line 477
    .local v0, "filterQueryBuilder":Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->getTimeColumnName()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;->DESC:Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->getLastDataWithFilter(Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;Ljava/lang/String;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;)Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;

    move-result-object v1

    return-object v1
.end method

.method public getLastDataForDay(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;
    .locals 1
    .param p1, "timeInDay"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)TT;"
        }
    .end annotation

    .prologue
    .line 86
    .local p0, "this":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;, "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb<TT;>;"
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->getCommonMethodsFilter()Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->getLastDataForDay(JLcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;)Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;

    move-result-object v0

    return-object v0
.end method

.method protected getLastDataForDay(JLcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;)Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;
    .locals 7
    .param p1, "timeInDay"    # J
    .param p3, "filter"    # Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;",
            ")TT;"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;, "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb<TT;>;"
    const/4 v6, 0x0

    .line 95
    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v2

    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v4

    invoke-virtual {p0, v2, v3, v4, v5}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->getPeriodRangeFilter(JJ)Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;

    move-result-object v0

    .line 96
    .local v0, "filterBuilder":Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;
    invoke-virtual {v0, p3}, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;->AND(Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;)Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->getCommonMethodsFilter()Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;->AND(Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;)Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;

    .line 97
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->getTimeColumnName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " DESC, "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "update_time"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " DESC "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 99
    .local v1, "sortingRule":Ljava/lang/String;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;->getSQLCode()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v6, v2, v6, v1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->getFirstItemFromQuery([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;

    move-result-object v2

    return-object v2
.end method

.method protected getLastDataWithFilter(Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;Ljava/lang/String;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;)Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;
    .locals 4
    .param p1, "filter"    # Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;
    .param p2, "columnForSort"    # Ljava/lang/String;
    .param p3, "sortOrder"    # Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;",
            ")TT;"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;, "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb<TT;>;"
    const/4 v2, 0x0

    .line 452
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    if-nez p3, :cond_1

    .line 453
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "columnForSort and sortOrder should be initialized!"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 455
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 456
    .local v0, "sortRule":Ljava/lang/String;
    if-nez p1, :cond_2

    move-object v1, v2

    :goto_0
    invoke-virtual {p0, v2, v1, v2, v0}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->getFirstItemFromQuery([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;

    move-result-object v1

    return-object v1

    :cond_2
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;->getSQLCode()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method protected getLastDataWithFilter(Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;)Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;
    .locals 4
    .param p1, "filter"    # Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;",
            ")TT;"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;, "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb<TT;>;"
    const/4 v2, 0x0

    .line 439
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->getTimeColumnName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " DESC, "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, "update_time"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " DESC "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 440
    .local v0, "sortOrder":Ljava/lang/String;
    if-nez p1, :cond_0

    move-object v1, v2

    :goto_0
    invoke-virtual {p0, v2, v1, v2, v0}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->getFirstItemFromQuery([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;

    move-result-object v1

    return-object v1

    :cond_0
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;->getSQLCode()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getLastUpdatedData()Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 462
    .local p0, "this":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;, "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb<TT;>;"
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->getCommonMethodsFilter()Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;

    move-result-object v0

    const-string/jumbo v1, "update_time"

    sget-object v2, Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;->DESC:Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->getLastDataWithFilter(Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;Ljava/lang/String;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;)Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;

    move-result-object v0

    return-object v0
.end method

.method public getNearestDateWithDataAfter(J)Ljava/lang/Long;
    .locals 6
    .param p1, "timeAfter"    # J

    .prologue
    .line 394
    .local p0, "this":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;, "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb<TT;>;"
    new-instance v0, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;

    new-instance v2, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->getTimeColumnName()Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    const-string v5, ">"

    invoke-direct {v2, v3, v4, v5}, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, v2}, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;-><init>(Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;)V

    .line 396
    .local v0, "filter":Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->getTimeColumnName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ASC"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 397
    .local v1, "sortOrder":Ljava/lang/String;
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->getTimeOfDataWithFilterAndSortOrder(Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    return-object v2
.end method

.method public getNearestDateWithDataBefore(J)Ljava/lang/Long;
    .locals 6
    .param p1, "timeBefore"    # J

    .prologue
    .line 386
    .local p0, "this":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;, "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb<TT;>;"
    new-instance v0, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;

    new-instance v2, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->getTimeColumnName()Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    const-string v5, "<"

    invoke-direct {v2, v3, v4, v5}, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, v2}, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;-><init>(Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;)V

    .line 388
    .local v0, "filter":Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->getTimeColumnName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " DESC"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 389
    .local v1, "sortOrder":Ljava/lang/String;
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->getTimeOfDataWithFilterAndSortOrder(Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    return-object v2
.end method

.method public getNextDayWithData(J)Ljava/lang/Long;
    .locals 2
    .param p1, "timeInDay"    # J

    .prologue
    .line 407
    .local p0, "this":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;, "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb<TT;>;"
    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->getNearestDateWithDataAfter(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method protected getPeriodCursor(JJLcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;[Ljava/lang/String;Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;)Landroid/database/Cursor;
    .locals 8
    .param p1, "periodStart"    # J
    .param p3, "periodEnd"    # J
    .param p5, "periodType"    # Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;
    .param p6, "projection"    # [Ljava/lang/String;
    .param p7, "whereClause"    # Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;
    .param p8, "sortOrder"    # Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;

    .prologue
    .line 364
    .local p0, "this":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;, "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb<TT;>;"
    if-nez p6, :cond_0

    .line 365
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Wrong input: projection shouldn\'t be null!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 367
    :cond_0
    if-nez p5, :cond_2

    const-string v6, ""

    .line 368
    .local v6, "groupByRule":Ljava/lang/String;
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->getTimeColumnName()Ljava/lang/String;

    move-result-object v7

    .line 369
    .local v7, "timeColumnName":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " >= ? "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " <= ? "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-nez p7, :cond_3

    const-string v0, ""

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->getCommonMethodsFilter()Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;

    move-result-object v0

    if-eqz v0, :cond_4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " AND "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->getCommonMethodsFilter()Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;->getSQLCode()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 375
    .local v3, "select":Ljava/lang/String;
    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x1

    invoke-static {p3, p4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    .line 376
    .local v4, "arg":[Ljava/lang/String;
    if-nez p8, :cond_1

    .line 377
    sget-object p8, Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;->DESC:Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;

    .line 379
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual/range {p8 .. p8}, Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 381
    .local v5, "sortOrderClause":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->uri:Landroid/net/Uri;

    move-object v2, p6

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0

    .line 367
    .end local v3    # "select":Ljava/lang/String;
    .end local v4    # "arg":[Ljava/lang/String;
    .end local v5    # "sortOrderClause":Ljava/lang/String;
    .end local v6    # "groupByRule":Ljava/lang/String;
    .end local v7    # "timeColumnName":Ljava/lang/String;
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ") GROUP BY ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, p5}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->getPeriodGroupByValue(Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_0

    .line 369
    .restart local v6    # "groupByRule":Ljava/lang/String;
    .restart local v7    # "timeColumnName":Ljava/lang/String;
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " AND ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p7}, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;->getSQLCode()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ") "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_4
    const-string v0, ""

    goto/16 :goto_2
.end method

.method protected final getPeriodGroupByValue(Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;)Ljava/lang/String;
    .locals 5
    .param p1, "periodType"    # Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;

    .prologue
    .line 233
    .local p0, "this":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;, "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb<TT;>;"
    sget-object v1, Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;->DAY:Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;

    if-ne p1, v1, :cond_0

    .line 234
    const-string v0, "%d-%m-%Y"

    .line 244
    .local v0, "timeFormat":Ljava/lang/String;
    :goto_0
    const-string/jumbo v1, "strftime(\"%s\",([%s]/1000),\'unixepoch\',\'localtime\')"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->getTimeColumnName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 235
    .end local v0    # "timeFormat":Ljava/lang/String;
    :cond_0
    sget-object v1, Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;->MONTH:Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;

    if-ne p1, v1, :cond_1

    .line 236
    const-string v0, "%m-%Y"

    .restart local v0    # "timeFormat":Ljava/lang/String;
    goto :goto_0

    .line 237
    .end local v0    # "timeFormat":Ljava/lang/String;
    :cond_1
    sget-object v1, Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;->YEAR:Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;

    if-ne p1, v1, :cond_2

    .line 238
    const-string v0, "%Y"

    .restart local v0    # "timeFormat":Ljava/lang/String;
    goto :goto_0

    .line 239
    .end local v0    # "timeFormat":Ljava/lang/String;
    :cond_2
    sget-object v1, Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;->HOUR:Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;

    if-ne p1, v1, :cond_3

    .line 240
    const-string v0, "%H-%d-%m-%Y"

    .restart local v0    # "timeFormat":Ljava/lang/String;
    goto :goto_0

    .line 242
    .end local v0    # "timeFormat":Ljava/lang/String;
    :cond_3
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "It seems there is no implementation for your period: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method protected getPeriodRangeFilter(JJ)Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;
    .locals 5
    .param p1, "periodStart"    # J
    .param p3, "periodEnd"    # J

    .prologue
    .line 109
    .local p0, "this":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;, "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb<TT;>;"
    new-instance v0, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;

    new-instance v1, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->getTimeColumnName()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    const-string v4, ">="

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;-><init>(Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;)V

    new-instance v1, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->getTimeColumnName()Ljava/lang/String;

    move-result-object v2

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    const-string v4, "<="

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;->AND(Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;)Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;

    move-result-object v0

    return-object v0
.end method

.method public getPreviousDayWithData(J)Ljava/lang/Long;
    .locals 2
    .param p1, "timeInDay"    # J

    .prologue
    .line 402
    .local p0, "this":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;, "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb<TT;>;"
    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->getNearestDateWithDataBefore(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method protected getTimeColumnName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 187
    .local p0, "this":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;, "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb<TT;>;"
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->mTimeColumnNameCheckedStatus:Z

    if-nez v0, :cond_2

    .line 188
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->mTimeColumnName:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->mTimeColumnName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 189
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "This operation requires timeColumnName to be set!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 191
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->mTimeColumnNameCheckedStatus:Z

    .line 193
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->mTimeColumnName:Ljava/lang/String;

    return-object v0
.end method

.method protected getTimeOfDataWithFilterAndSortOrder(Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;Ljava/lang/String;)Ljava/lang/Long;
    .locals 8
    .param p1, "filter"    # Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;
    .param p2, "sortOrder"    # Ljava/lang/String;

    .prologue
    .local p0, "this":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;, "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb<TT;>;"
    const/4 v4, 0x0

    .line 417
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->getTimeColumnName()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v0

    .line 418
    .local v2, "projection":[Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->uri:Landroid/net/Uri;

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;->getSQLCode()Ljava/lang/String;

    move-result-object v3

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " LIMIT 1"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 421
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_1

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 422
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;->getTimeColumnName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    .line 426
    if-eqz v6, :cond_0

    .line 427
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_0
    :goto_0
    return-object v4

    .line 426
    :cond_1
    if-eqz v6, :cond_0

    .line 427
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 426
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 427
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method
