.class Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$17;
.super Ljava/lang/Object;
.source "RealtimeHealthService.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)V
    .locals 0

    .prologue
    .line 2533
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$17;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceived(ILcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;Landroid/os/Bundle;)V
    .locals 10
    .param p1, "dataType"    # I
    .param p2, "data"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;
    .param p3, "extra"    # Landroid/os/Bundle;

    .prologue
    .line 2539
    const/16 v7, 0x8

    if-ne p1, v7, :cond_8

    move-object v0, p2

    .line 2541
    check-cast v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Fitness;

    .line 2543
    .local v0, "bikeData":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Fitness;
    iget v2, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Fitness;->cumulativeDistance:F

    .line 2544
    .local v2, "distance":F
    const v7, 0x7f7fffff    # Float.MAX_VALUE

    cmpl-float v7, v2, v7

    if-eqz v7, :cond_2

    .line 2545
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$17;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    iget-object v7, v7, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v7, v7, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_distance_sum:F

    cmpl-float v7, v7, v2

    if-eqz v7, :cond_1

    .line 2546
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$17;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;

    .line 2547
    const/4 v7, 0x2

    float-to-int v8, v2

    int-to-float v8, v8

    invoke-static {v7, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->workoutProgress(IF)V

    .line 2548
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$17;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mListListener:Ljava/util/ArrayList;
    invoke-static {v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Ljava/util/ArrayList;

    move-result-object v8

    monitor-enter v8

    .line 2549
    :try_start_0
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$17;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mListListener:Ljava/util/ArrayList;
    invoke-static {v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2551
    .local v5, "listener":Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;
    const/4 v7, 0x2

    const/4 v9, 0x0

    :try_start_1
    invoke-interface {v5, v7, v2, v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;->onUpdateVaule(IFI)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2553
    :catch_0
    move-exception v3

    .line 2554
    .local v3, "e":Ljava/lang/Exception;
    :try_start_2
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$600()Ljava/lang/String;

    move-result-object v7

    const-string v9, "mBSACDataListener - distance onUpdateVaule->Exception"

    invoke-static {v7, v9}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2557
    .end local v3    # "e":Ljava/lang/Exception;
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v5    # "listener":Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;
    :catchall_0
    move-exception v7

    monitor-exit v8
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v7

    .restart local v4    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_3
    monitor-exit v8
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2559
    .end local v4    # "i$":Ljava/util/Iterator;
    :cond_1
    const-string v7, "mBSACDataListener"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$17;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    iget-object v9, v9, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v9, v9, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_distance_sum:F

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 2560
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$17;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    iget-object v7, v7, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iput v2, v7, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_distance_sum:F

    .line 2561
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$17;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$17;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    iget-object v8, v8, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v8, v8, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_distance_sum:F

    invoke-static {v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->getRoundedValue(F)F

    move-result v8

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->checkTTSOnUpdateVaule(F)V
    invoke-static {v7, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$5400(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;F)V

    .line 2563
    :cond_2
    iget v6, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Fitness;->instantaneousSpeed:F

    .line 2564
    .local v6, "speed":F
    const v7, 0x7f7fffff    # Float.MAX_VALUE

    cmpl-float v7, v6, v7

    if-eqz v7, :cond_4

    .line 2565
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$17;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    iget-object v7, v7, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v7, v7, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_speed:F

    cmpl-float v7, v7, v6

    if-eqz v7, :cond_5

    .line 2566
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$17;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;

    .line 2567
    const/4 v7, 0x3

    float-to-int v8, v6

    int-to-float v8, v8

    invoke-static {v7, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->workoutProgress(IF)V

    .line 2568
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$17;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mListListener:Ljava/util/ArrayList;
    invoke-static {v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Ljava/util/ArrayList;

    move-result-object v8

    monitor-enter v8

    .line 2569
    :try_start_4
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$17;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mListListener:Ljava/util/ArrayList;
    invoke-static {v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .restart local v4    # "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2571
    .restart local v5    # "listener":Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;
    const/4 v7, 0x3

    const/4 v9, 0x0

    :try_start_5
    invoke-interface {v5, v7, v6, v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;->onUpdateVaule(IFI)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_1

    .line 2573
    :catch_1
    move-exception v3

    .line 2574
    .restart local v3    # "e":Ljava/lang/Exception;
    :try_start_6
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$600()Ljava/lang/String;

    move-result-object v7

    const-string v9, "mBSACDataListener - speed onUpdateVaule->Exception"

    invoke-static {v7, v9}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 2577
    .end local v3    # "e":Ljava/lang/Exception;
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v5    # "listener":Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;
    :catchall_1
    move-exception v7

    monitor-exit v8
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v7

    .restart local v4    # "i$":Ljava/util/Iterator;
    :cond_3
    :try_start_7
    monitor-exit v8
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 2582
    .end local v4    # "i$":Ljava/util/Iterator;
    :goto_2
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$17;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    iget-object v7, v7, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    invoke-virtual {v7, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->setSpeed(F)V

    .line 2584
    :cond_4
    iget v1, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Fitness;->cadencePerMinute:F

    .line 2585
    .local v1, "cadence":F
    const v7, 0x7f7fffff    # Float.MAX_VALUE

    cmpl-float v7, v1, v7

    if-eqz v7, :cond_8

    .line 2586
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$17;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    iget-object v7, v7, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v7, v7, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_cadence:F

    cmpl-float v7, v7, v1

    if-eqz v7, :cond_7

    .line 2587
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$17;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;

    .line 2588
    const/16 v7, 0x12

    float-to-int v8, v1

    int-to-float v8, v8

    invoke-static {v7, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->workoutProgress(IF)V

    .line 2589
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$17;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mListListener:Ljava/util/ArrayList;
    invoke-static {v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Ljava/util/ArrayList;

    move-result-object v8

    monitor-enter v8

    .line 2590
    :try_start_8
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$17;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mListListener:Ljava/util/ArrayList;
    invoke-static {v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .restart local v4    # "i$":Ljava/util/Iterator;
    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 2592
    .restart local v5    # "listener":Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;
    const/16 v7, 0x12

    const/4 v9, 0x0

    :try_start_9
    invoke-interface {v5, v7, v1, v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;->onUpdateVaule(IFI)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    goto :goto_3

    .line 2594
    :catch_2
    move-exception v3

    .line 2595
    .restart local v3    # "e":Ljava/lang/Exception;
    :try_start_a
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$600()Ljava/lang/String;

    move-result-object v7

    const-string v9, "mBSACDataListener - cadence onUpdateVaule->Exception"

    invoke-static {v7, v9}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 2598
    .end local v3    # "e":Ljava/lang/Exception;
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v5    # "listener":Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;
    :catchall_2
    move-exception v7

    monitor-exit v8
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    throw v7

    .line 2579
    .end local v1    # "cadence":F
    :cond_5
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$17;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;

    .line 2580
    const/4 v7, 0x3

    float-to-int v8, v6

    int-to-float v8, v8

    invoke-static {v7, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->workoutProgress(IF)V

    goto :goto_2

    .line 2598
    .restart local v1    # "cadence":F
    .restart local v4    # "i$":Ljava/util/Iterator;
    :cond_6
    :try_start_b
    monitor-exit v8
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    .line 2600
    .end local v4    # "i$":Ljava/util/Iterator;
    :cond_7
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$17;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    iget-object v7, v7, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    invoke-virtual {v7, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->setCadence(F)V

    .line 2603
    .end local v0    # "bikeData":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Fitness;
    .end local v1    # "cadence":F
    .end local v2    # "distance":F
    .end local v6    # "speed":F
    :cond_8
    return-void
.end method

.method public onReceived(I[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;[Landroid/os/Bundle;)V
    .locals 0
    .param p1, "dataType"    # I
    .param p2, "data"    # [Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;
    .param p3, "extra"    # [Landroid/os/Bundle;

    .prologue
    .line 2630
    return-void
.end method

.method public onStarted(II)V
    .locals 4
    .param p1, "dataType"    # I
    .param p2, "error"    # I

    .prologue
    .line 2608
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$17;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mBSACDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$2500(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$17;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mExerciseId:J
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$500(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->record(J)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_0 .. :try_end_0} :catch_5

    .line 2622
    :goto_0
    return-void

    .line 2609
    :catch_0
    move-exception v0

    .line 2610
    .local v0, "e":Landroid/os/RemoteException;
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$600()Ljava/lang/String;

    move-result-object v1

    const-string v2, "mBSACDataListener - onStarted->RemoteException"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2611
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 2612
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$600()Ljava/lang/String;

    move-result-object v1

    const-string v2, "mBSACDataListener - onStarted->IllegalArgumentException"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2613
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .line 2614
    .local v0, "e":Ljava/lang/IllegalStateException;
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$600()Ljava/lang/String;

    move-result-object v1

    const-string v2, "mBSACDataListener - onStarted->IllegalStateException"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2615
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_3
    move-exception v0

    .line 2616
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$600()Ljava/lang/String;

    move-result-object v1

    const-string v2, "mBSACDataListener - onStarted->ShealthSensorServiceNotBoundException"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2617
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    :catch_4
    move-exception v0

    .line 2618
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$600()Ljava/lang/String;

    move-result-object v1

    const-string v2, "mBSACDataListener - onStarted->ShealthSensorNotSupportedException"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2619
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;
    :catch_5
    move-exception v0

    .line 2620
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$600()Ljava/lang/String;

    move-result-object v1

    const-string v2, "mBSACDataListener - onStarted->SHealthSensorInternalErrorException"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onStopped(II)V
    .locals 0
    .param p1, "dataType"    # I
    .param p2, "error"    # I

    .prologue
    .line 2626
    return-void
.end method
