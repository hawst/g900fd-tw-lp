.class final enum Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$GoalState;
.super Ljava/lang/Enum;
.source "WeightWithGoalFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "GoalState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$GoalState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$GoalState;

.field public static final enum IN_PROGRESS:Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$GoalState;

.field public static final enum LOOSED_GOAL:Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$GoalState;

.field public static final enum REACHED_GOAL_EARLY:Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$GoalState;

.field public static final enum REACHED_GOAL_IN_TIME:Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$GoalState;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 88
    new-instance v0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$GoalState;

    const-string v1, "REACHED_GOAL_IN_TIME"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$GoalState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$GoalState;->REACHED_GOAL_IN_TIME:Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$GoalState;

    .line 89
    new-instance v0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$GoalState;

    const-string v1, "REACHED_GOAL_EARLY"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$GoalState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$GoalState;->REACHED_GOAL_EARLY:Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$GoalState;

    .line 90
    new-instance v0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$GoalState;

    const-string v1, "LOOSED_GOAL"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$GoalState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$GoalState;->LOOSED_GOAL:Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$GoalState;

    .line 91
    new-instance v0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$GoalState;

    const-string v1, "IN_PROGRESS"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$GoalState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$GoalState;->IN_PROGRESS:Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$GoalState;

    .line 87
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$GoalState;

    sget-object v1, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$GoalState;->REACHED_GOAL_IN_TIME:Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$GoalState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$GoalState;->REACHED_GOAL_EARLY:Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$GoalState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$GoalState;->LOOSED_GOAL:Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$GoalState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$GoalState;->IN_PROGRESS:Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$GoalState;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$GoalState;->$VALUES:[Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$GoalState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 87
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$GoalState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 87
    const-class v0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$GoalState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$GoalState;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$GoalState;
    .locals 1

    .prologue
    .line 87
    sget-object v0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$GoalState;->$VALUES:[Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$GoalState;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$GoalState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$GoalState;

    return-object v0
.end method
