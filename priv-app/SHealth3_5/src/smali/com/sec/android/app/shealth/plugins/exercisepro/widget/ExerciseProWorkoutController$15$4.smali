.class Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15$4;
.super Ljava/lang/Object;
.source "ExerciseProWorkoutController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;->onChannelStateChanged(Ljava/lang/String;II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;

.field final synthetic val$state:I

.field final synthetic val$uid:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 1058
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15$4;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;

    iput p2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15$4;->val$state:I

    iput-object p3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15$4;->val$uid:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 1061
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15$4;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->updateIndoorInProgressPopup()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)V

    .line 1062
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15$4;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->mHealthListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$IHealthControllerListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$600(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$IHealthControllerListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1063
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15$4;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->mHealthListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$IHealthControllerListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$600(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$IHealthControllerListener;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15$4;->val$state:I

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15$4;->val$uid:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$IHealthControllerListener;->onChannelStateChanged(ILjava/lang/String;)V

    .line 1065
    :cond_0
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15$4;->val$state:I

    packed-switch v0, :pswitch_data_0

    .line 1091
    :goto_0
    :pswitch_0
    return-void

    .line 1070
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15$4;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15$4;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;

    iget-object v1, v1, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->mRealtimeService:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getCurWorkoutResult()Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    move-result-object v1

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->startDetailActivity(ILcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;)V
    invoke-static {v0, v3, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$2200(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;ILcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;)V

    .line 1071
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15$4;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->mRealtimeService:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->initCurWorkoutResult(I)V

    .line 1072
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15$4$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15$4$1;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15$4;)V

    const-wide/16 v2, 0x5dc

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 1087
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15$4;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->mContext:Landroid/app/Activity;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$800(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15$4;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;

    iget-object v1, v1, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->mContext:Landroid/app/Activity;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$800(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f090702

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1088
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15$4;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    const/4 v1, 0x1

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->updateIndoorLayoutText(Z)Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$2400(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;Z)Z

    goto :goto_0

    .line 1065
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
