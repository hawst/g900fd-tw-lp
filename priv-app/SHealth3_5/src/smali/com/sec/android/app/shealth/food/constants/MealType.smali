.class public final enum Lcom/sec/android/app/shealth/food/constants/MealType;
.super Ljava/lang/Enum;
.source "MealType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/food/constants/MealType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/food/constants/MealType;

.field public static final enum BREAKFAST:Lcom/sec/android/app/shealth/food/constants/MealType;

.field public static final enum DINNER:Lcom/sec/android/app/shealth/food/constants/MealType;

.field public static final enum LUNCH:Lcom/sec/android/app/shealth/food/constants/MealType;

.field public static final enum NOT_DEFINED:Lcom/sec/android/app/shealth/food/constants/MealType;

.field public static final NOT_DEFINED_TEXT_ID:I = -0x1

.field public static final enum OTHER:Lcom/sec/android/app/shealth/food/constants/MealType;


# instance fields
.field public final mealTypeId:I

.field public final textResourceId:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, -0x1

    .line 24
    new-instance v0, Lcom/sec/android/app/shealth/food/constants/MealType;

    const-string v1, "BREAKFAST"

    const v2, 0x186a1

    const v3, 0x7f090920

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/sec/android/app/shealth/food/constants/MealType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sec/android/app/shealth/food/constants/MealType;->BREAKFAST:Lcom/sec/android/app/shealth/food/constants/MealType;

    .line 25
    new-instance v0, Lcom/sec/android/app/shealth/food/constants/MealType;

    const-string v1, "LUNCH"

    const v2, 0x186a2

    const v3, 0x7f090921

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/sec/android/app/shealth/food/constants/MealType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sec/android/app/shealth/food/constants/MealType;->LUNCH:Lcom/sec/android/app/shealth/food/constants/MealType;

    .line 26
    new-instance v0, Lcom/sec/android/app/shealth/food/constants/MealType;

    const-string v1, "DINNER"

    const v2, 0x186a3

    const v3, 0x7f090922

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/sec/android/app/shealth/food/constants/MealType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sec/android/app/shealth/food/constants/MealType;->DINNER:Lcom/sec/android/app/shealth/food/constants/MealType;

    .line 27
    new-instance v0, Lcom/sec/android/app/shealth/food/constants/MealType;

    const-string v1, "OTHER"

    const v2, 0x186a4

    const v3, 0x7f090923

    invoke-direct {v0, v1, v8, v2, v3}, Lcom/sec/android/app/shealth/food/constants/MealType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sec/android/app/shealth/food/constants/MealType;->OTHER:Lcom/sec/android/app/shealth/food/constants/MealType;

    .line 28
    new-instance v0, Lcom/sec/android/app/shealth/food/constants/MealType;

    const-string v1, "NOT_DEFINED"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v2, v4, v4}, Lcom/sec/android/app/shealth/food/constants/MealType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sec/android/app/shealth/food/constants/MealType;->NOT_DEFINED:Lcom/sec/android/app/shealth/food/constants/MealType;

    .line 23
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/sec/android/app/shealth/food/constants/MealType;

    sget-object v1, Lcom/sec/android/app/shealth/food/constants/MealType;->BREAKFAST:Lcom/sec/android/app/shealth/food/constants/MealType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/shealth/food/constants/MealType;->LUNCH:Lcom/sec/android/app/shealth/food/constants/MealType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/shealth/food/constants/MealType;->DINNER:Lcom/sec/android/app/shealth/food/constants/MealType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sec/android/app/shealth/food/constants/MealType;->OTHER:Lcom/sec/android/app/shealth/food/constants/MealType;

    aput-object v1, v0, v8

    const/4 v1, 0x4

    sget-object v2, Lcom/sec/android/app/shealth/food/constants/MealType;->NOT_DEFINED:Lcom/sec/android/app/shealth/food/constants/MealType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/shealth/food/constants/MealType;->$VALUES:[Lcom/sec/android/app/shealth/food/constants/MealType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3, "mealTypeId"    # I
    .param p4, "textResourceId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 43
    iput p3, p0, Lcom/sec/android/app/shealth/food/constants/MealType;->mealTypeId:I

    .line 44
    iput p4, p0, Lcom/sec/android/app/shealth/food/constants/MealType;->textResourceId:I

    .line 45
    return-void
.end method

.method public static getMealTypeByMealTypeId(I)Lcom/sec/android/app/shealth/food/constants/MealType;
    .locals 7
    .param p0, "mealTypeId"    # I

    .prologue
    .line 52
    invoke-static {}, Lcom/sec/android/app/shealth/food/constants/MealType;->values()[Lcom/sec/android/app/shealth/food/constants/MealType;

    move-result-object v0

    .local v0, "arr$":[Lcom/sec/android/app/shealth/food/constants/MealType;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 53
    .local v3, "mealType":Lcom/sec/android/app/shealth/food/constants/MealType;
    iget v4, v3, Lcom/sec/android/app/shealth/food/constants/MealType;->mealTypeId:I

    if-ne p0, v4, :cond_0

    .line 54
    return-object v3

    .line 52
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 57
    .end local v3    # "mealType":Lcom/sec/android/app/shealth/food/constants/MealType;
    :cond_1
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Wrong meal type id : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/food/constants/MealType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 23
    const-class v0, Lcom/sec/android/app/shealth/food/constants/MealType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/constants/MealType;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/food/constants/MealType;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/sec/android/app/shealth/food/constants/MealType;->$VALUES:[Lcom/sec/android/app/shealth/food/constants/MealType;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/food/constants/MealType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/food/constants/MealType;

    return-object v0
.end method
