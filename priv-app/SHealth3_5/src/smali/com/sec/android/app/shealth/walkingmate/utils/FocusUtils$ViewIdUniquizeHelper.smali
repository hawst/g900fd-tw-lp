.class public Lcom/sec/android/app/shealth/walkingmate/utils/FocusUtils$ViewIdUniquizeHelper;
.super Ljava/lang/Object;
.source "FocusUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/walkingmate/utils/FocusUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ViewIdUniquizeHelper"
.end annotation


# instance fields
.field currentMaxViewId:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 313
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 314
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/FocusUtils$ViewIdUniquizeHelper;->currentMaxViewId:I

    return-void
.end method


# virtual methods
.method public getCurrentMaxViewId()I
    .locals 1

    .prologue
    .line 317
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/FocusUtils$ViewIdUniquizeHelper;->currentMaxViewId:I

    return v0
.end method

.method public setCurrentMaxViewId(I)Z
    .locals 1
    .param p1, "currentMaxViewId"    # I

    .prologue
    .line 321
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/FocusUtils$ViewIdUniquizeHelper;->currentMaxViewId:I

    if-ge p1, v0, :cond_0

    .line 322
    const/4 v0, 0x0

    .line 324
    :goto_0
    return v0

    .line 323
    :cond_0
    iput p1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/FocusUtils$ViewIdUniquizeHelper;->currentMaxViewId:I

    .line 324
    const/4 v0, 0x1

    goto :goto_0
.end method
