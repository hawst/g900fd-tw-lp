.class Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$ExerciseImageScaleCopyTask;
.super Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ImageScaleCopyAsyncTask;
.source "ExerciseProActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ExerciseImageScaleCopyTask"
.end annotation


# instance fields
.field private context:Landroid/content/Context;

.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "inputImagePath"    # Ljava/lang/String;
    .param p4, "outputImagePath"    # Ljava/lang/String;

    .prologue
    .line 1561
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$ExerciseImageScaleCopyTask;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    .line 1562
    invoke-direct {p0, p3, p4}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ImageScaleCopyAsyncTask;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1563
    iput-object p2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$ExerciseImageScaleCopyTask;->context:Landroid/content/Context;

    .line 1564
    return-void
.end method


# virtual methods
.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 5
    .param p1, "isSuccessful"    # Ljava/lang/Boolean;

    .prologue
    .line 1568
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ImageScaleCopyAsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 1569
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1570
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$ExerciseImageScaleCopyTask;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "file://"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$ExerciseImageScaleCopyTask;->getOutputImagePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 1572
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$ExerciseImageScaleCopyTask;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$ExerciseImageScaleCopyTask;->getOutputImagePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeExerciseId()J

    move-result-wide v2

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->saveInfoAndUpdateViewGrid(Ljava/lang/String;J)V
    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->access$1500(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;Ljava/lang/String;J)V

    .line 1573
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$ExerciseImageScaleCopyTask;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->refreshLastTakenPhoto(Landroid/content/Context;)V

    .line 1575
    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 1558
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$ExerciseImageScaleCopyTask;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method
