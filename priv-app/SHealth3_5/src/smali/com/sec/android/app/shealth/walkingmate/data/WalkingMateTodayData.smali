.class public Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;
.super Ljava/lang/Object;
.source "WalkingMateTodayData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData$OnPedoMeterSensorDataReceiveListener;
    }
.end annotation


# static fields
.field private static TAG:Ljava/lang/String;

.field private static fullNotificationNeeded:Z

.field private static halfNotificationNeeded:Z

.field private static isFirstStart:Z

.field private static isNotUsedForWeek:Z

.field private static mAcheivedNotify:Z

.field static mContext:Landroid/content/Context;

.field private static mHalfNotify:Z

.field public static mInactiveNotify:Z

.field private static mIsRefreshingNotiState:Z

.field private static mLastNotiType:I

.field private static mNewRecordNotify:Z

.field private static mNotUsedNotify:Z

.field static mWalkingMateTodayData:Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;


# instance fields
.field private final BESTVALUE:I

.field private final GOALNOTI:I

.field private final HALFNOTI:I

.field private final NOTUSEDNOTI:I

.field private commonQuery:Ljava/lang/StringBuilder;

.field private volatile lastUpdateTime:J

.field private lastUpdateType:I

.field mBestValue:I

.field mCal:F

.field mDistance:F

.field private mGoalValue:I

.field private final mListener:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData$OnPedoMeterSensorDataReceiveListener;",
            ">;"
        }
    .end annotation
.end field

.field mTotalStep:I

.field mUpdateTime:J

.field private mWalkingMateDayStepService:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 34
    const-string v0, "WalkingMateTodayData"

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->TAG:Ljava/lang/String;

    .line 42
    sput-boolean v2, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->isFirstStart:Z

    .line 48
    sput-boolean v2, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->halfNotificationNeeded:Z

    .line 49
    sput-boolean v2, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->fullNotificationNeeded:Z

    .line 51
    const/4 v0, -0x1

    sput v0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mLastNotiType:I

    .line 52
    sput-boolean v1, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->isNotUsedForWeek:Z

    .line 54
    sput-boolean v1, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mAcheivedNotify:Z

    .line 55
    sput-boolean v1, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mNotUsedNotify:Z

    .line 56
    sput-boolean v1, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mNewRecordNotify:Z

    .line 57
    sput-boolean v1, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mIsRefreshingNotiState:Z

    .line 58
    sput-boolean v1, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mHalfNotify:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const-wide/16 v1, 0x0

    const/4 v0, 0x0

    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mDistance:F

    .line 31
    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mCal:F

    .line 32
    iput-wide v1, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mUpdateTime:J

    .line 33
    const/16 v0, 0x2710

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mGoalValue:I

    .line 39
    iput-wide v1, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->lastUpdateTime:J

    .line 40
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->lastUpdateType:I

    .line 44
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->BESTVALUE:I

    .line 45
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->HALFNOTI:I

    .line 46
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->GOALNOTI:I

    .line 47
    const/16 v0, 0x8

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->NOTUSEDNOTI:I

    .line 80
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->commonQuery:Ljava/lang/StringBuilder;

    .line 81
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->commonQuery:Ljava/lang/StringBuilder;

    const-string v1, "SELECT "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 82
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->commonQuery:Ljava/lang/StringBuilder;

    const-string v1, "SUM(total_step) AS total_step"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 84
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->commonQuery:Ljava/lang/StringBuilder;

    const-string v1, " , "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 85
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->commonQuery:Ljava/lang/StringBuilder;

    const-string v1, "SUM(distance) AS distance"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 87
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->commonQuery:Ljava/lang/StringBuilder;

    const-string v1, " , "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 88
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->commonQuery:Ljava/lang/StringBuilder;

    const-string v1, "SUM(calorie) AS calorie"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 90
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->commonQuery:Ljava/lang/StringBuilder;

    const-string v1, " , "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 91
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->commonQuery:Ljava/lang/StringBuilder;

    const-string v1, "MAX(update_time) AS update_time"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 93
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->commonQuery:Ljava/lang/StringBuilder;

    const-string v1, " FROM "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 94
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->commonQuery:Ljava/lang/StringBuilder;

    const-string/jumbo v1, "walk_info"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 95
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->commonQuery:Ljava/lang/StringBuilder;

    const-string v1, " WHERE "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 96
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->commonQuery:Ljava/lang/StringBuilder;

    const-string/jumbo v1, "sync_status != 170004"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 98
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->commonQuery:Ljava/lang/StringBuilder;

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 99
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->commonQuery:Ljava/lang/StringBuilder;

    const-string/jumbo v1, "total_step > 0 "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 101
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->commonQuery:Ljava/lang/StringBuilder;

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 102
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mListener:Ljava/util/ArrayList;

    .line 104
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mWalkingMateDayStepService:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    return-object v0
.end method

.method private checkUnitSetting()V
    .locals 6

    .prologue
    .line 535
    const/4 v1, 0x0

    .line 539
    .local v1, "mShealthProfile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    :try_start_0
    new-instance v2, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v2, v4}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v1    # "mShealthProfile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    .local v2, "mShealthProfile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    move-object v1, v2

    .line 546
    .end local v2    # "mShealthProfile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    .restart local v1    # "mShealthProfile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    :goto_0
    new-instance v3, Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;-><init>(Landroid/content/Context;)V

    .line 547
    .local v3, "unitSettingHelper":Lcom/sec/android/app/shealth/common/utils/UnitHelper;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getDistanceUnit()I

    move-result v4

    const v5, 0x29811

    if-ne v4, v5, :cond_0

    .line 548
    const-string v4, "km"

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->putDistanceUnit(Ljava/lang/String;)V

    .line 551
    :goto_1
    return-void

    .line 541
    .end local v3    # "unitSettingHelper":Lcom/sec/android/app/shealth/common/utils/UnitHelper;
    :catch_0
    move-exception v0

    .line 543
    .local v0, "ex":Ljava/lang/IllegalArgumentException;
    sget-object v4, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->TAG:Ljava/lang/String;

    const-string v5, "There is exception occured while loading profile!!"

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 544
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 550
    .end local v0    # "ex":Ljava/lang/IllegalArgumentException;
    .restart local v3    # "unitSettingHelper":Lcom/sec/android/app/shealth/common/utils/UnitHelper;
    :cond_0
    const-string/jumbo v4, "mi"

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->putDistanceUnit(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private duplicationCheck(Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData$OnPedoMeterSensorDataReceiveListener;)Z
    .locals 4
    .param p1, "listener"    # Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData$OnPedoMeterSensorDataReceiveListener;

    .prologue
    .line 150
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mListener:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData$OnPedoMeterSensorDataReceiveListener;

    .line 151
    .local v1, "li":Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData$OnPedoMeterSensorDataReceiveListener;
    if-ne v1, p1, :cond_0

    .line 152
    const-string v2, "KJKJ"

    const-string v3, "duplication occurs"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    const/4 v2, 0x1

    .line 156
    .end local v1    # "li":Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData$OnPedoMeterSensorDataReceiveListener;
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;
    .locals 4
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    .line 107
    const-class v1, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mWalkingMateTodayData:Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    if-nez v0, :cond_0

    .line 108
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mWalkingMateTodayData:Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    .line 109
    sput-object p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mContext:Landroid/content/Context;

    .line 112
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "KeyManager WalkingMateTodayData ======= "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/service/health/keyManager/KeyManager;->isContentProviderAccessible()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/keyManager/KeyManager;->isContentProviderAccessible()Z

    move-result v0

    if-nez v0, :cond_1

    .line 115
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mWalkingMateTodayData:Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 128
    :goto_0
    monitor-exit v1

    return-object v0

    .line 118
    :cond_1
    :try_start_1
    sget-boolean v0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->isFirstStart:Z

    if-eqz v0, :cond_2

    .line 119
    const-string v0, "PEDOSTART"

    const-string v2, "first Start"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->isFirstStart:Z

    .line 121
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mWalkingMateTodayData:Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->refreshBestStep()V

    .line 122
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mWalkingMateTodayData:Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->refreshTodayGoalFromDB()V

    .line 123
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mWalkingMateTodayData:Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->updateFromDB()V

    .line 124
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->isNotUsedForAWeek()Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->isNotUsedForWeek:Z

    .line 128
    :cond_2
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mWalkingMateTodayData:Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 107
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private isWalkingStarted()Z
    .locals 4

    .prologue
    .line 342
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->isInitializationNeeded()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 343
    const/4 v0, 0x0

    .line 348
    :goto_0
    return v0

    .line 346
    :cond_0
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->isStartWalking()Z

    move-result v0

    .line 347
    .local v0, "startWalking":Z
    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isStarted = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private updateWidget()V
    .locals 2

    .prologue
    .line 484
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mWalkingMateDayStepService:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    if-nez v0, :cond_0

    .line 485
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->TAG:Ljava/lang/String;

    const-string v1, "There is no WalkingMateDayStepService."

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 504
    :goto_0
    return-void

    .line 489
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "updateWidget updateWidgets"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 491
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData$1;-><init>(Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method


# virtual methods
.method public accumulateData(IFFJ)V
    .locals 5
    .param p1, "totalSteps"    # I
    .param p2, "cal"    # F
    .param p3, "distance"    # F
    .param p4, "updateTime"    # J

    .prologue
    const/16 v4, 0x2719

    .line 354
    invoke-virtual {p0, p4, p5}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->dateChangeCheck(J)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 355
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->updateFromDB()V

    .line 361
    :cond_0
    iput-wide p4, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mUpdateTime:J

    .line 362
    iget v1, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mTotalStep:I

    add-int/2addr v1, p1

    iput v1, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mTotalStep:I

    .line 363
    iget v1, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mCal:F

    add-float/2addr v1, p2

    iput v1, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mCal:F

    .line 364
    iget v1, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mDistance:F

    add-float/2addr v1, p3

    iput v1, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mDistance:F

    .line 365
    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "accumulateData() - mTotalStep : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mTotalStep:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mCal : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mCal:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mDistance : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mDistance:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", deviceType:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 376
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v1

    if-ne v1, v4, :cond_2

    if-lez p1, :cond_2

    .line 377
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    move-result-object v0

    .line 378
    .local v0, "monitor":Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->isInactivetime()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 379
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->setRefreshingNotiState(Z)V

    .line 380
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->unsetInactiveMonitor()V

    .line 381
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->setInactiveMonitor(I)V

    .line 382
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->setRefreshingNotiState(Z)V

    .line 384
    :cond_1
    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->sendResponse(I)V

    .line 385
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->checkNotiStatus()V

    .line 386
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->updateWidget()V

    .line 389
    .end local v0    # "monitor":Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;
    :cond_2
    return-void
.end method

.method public declared-synchronized checkNotiStatus()V
    .locals 5

    .prologue
    .line 279
    monitor-enter p0

    :try_start_0
    sget-object v2, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mBestValue = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mBestValue:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 280
    sget-object v2, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "mGoalValue = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mGoalValue:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 281
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mWalkingMateDayStepService:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    if-nez v2, :cond_1

    .line 282
    sget-object v2, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "service is null"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 339
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 286
    :cond_1
    const/4 v1, 0x0

    .line 288
    .local v1, "notiType":I
    :try_start_1
    sget-boolean v0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mHalfNotify:Z

    .line 290
    .local v0, "backupHalfNotify":Z
    iget v2, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mTotalStep:I

    iget v3, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mGoalValue:I

    div-int/lit8 v3, v3, 0x2

    if-lt v2, v3, :cond_2

    .line 291
    sget-object v2, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->TAG:Ljava/lang/String;

    const-string v3, "Half Noti"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 292
    or-int/lit8 v1, v1, 0x2

    .line 293
    sget-boolean v2, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->halfNotificationNeeded:Z

    sput-boolean v2, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mHalfNotify:Z

    .line 296
    :cond_2
    iget v2, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mTotalStep:I

    iget v3, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mGoalValue:I

    if-lt v2, v3, :cond_3

    .line 297
    sget-object v2, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->TAG:Ljava/lang/String;

    const-string v3, "Goal achieved"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 298
    or-int/lit8 v1, v1, 0x4

    .line 299
    sget-boolean v2, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->fullNotificationNeeded:Z

    sput-boolean v2, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mAcheivedNotify:Z

    .line 302
    :cond_3
    iget v2, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mTotalStep:I

    iget v3, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mBestValue:I

    if-lt v2, v3, :cond_4

    .line 303
    sget-object v2, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->TAG:Ljava/lang/String;

    const-string v3, "A new Record"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 304
    iget v2, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mTotalStep:I

    iput v2, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mBestValue:I

    .line 305
    or-int/lit8 v1, v1, 0x1

    .line 306
    const/4 v2, 0x1

    sput-boolean v2, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mNewRecordNotify:Z

    .line 309
    :cond_4
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->isWalkingStarted()Z

    move-result v2

    if-nez v2, :cond_5

    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->isNotUsedForAWeek()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 310
    const/16 v1, 0x8

    .line 311
    const/4 v2, 0x1

    sput-boolean v2, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mNotUsedNotify:Z

    .line 314
    :cond_5
    sget-object v2, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "notiType = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 315
    sget-object v2, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "mLastNotiType = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget v4, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mLastNotiType:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 317
    sget-boolean v2, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mIsRefreshingNotiState:Z

    if-nez v2, :cond_8

    .line 319
    if-eqz v0, :cond_6

    sget-boolean v2, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mHalfNotify:Z

    if-nez v2, :cond_6

    .line 320
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mWalkingMateDayStepService:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    if-eqz v2, :cond_6

    .line 321
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mWalkingMateDayStepService:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->stopNotification()V

    .line 325
    :cond_6
    sget v2, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mLastNotiType:I

    if-eq v1, v2, :cond_0

    .line 326
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mWalkingMateDayStepService:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    if-eqz v2, :cond_7

    .line 327
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mWalkingMateDayStepService:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->checkNotification()V

    .line 329
    :cond_7
    sput v1, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mLastNotiType:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 279
    .end local v0    # "backupHalfNotify":Z
    .end local v1    # "notiType":I
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 332
    .restart local v0    # "backupHalfNotify":Z
    .restart local v1    # "notiType":I
    :cond_8
    const/16 v2, 0x8

    if-ne v1, v2, :cond_9

    .line 333
    :try_start_2
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mWalkingMateDayStepService:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    if-eqz v2, :cond_9

    .line 334
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mWalkingMateDayStepService:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->checkNotification()V

    .line 337
    :cond_9
    sput v1, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mLastNotiType:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method

.method public clearListener()V
    .locals 1

    .prologue
    .line 507
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mListener:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 508
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mListener:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 510
    :cond_0
    return-void
.end method

.method public declared-synchronized dateChangeCheck(J)Z
    .locals 4
    .param p1, "updateTime"    # J

    .prologue
    .line 271
    monitor-enter p0

    :try_start_0
    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v0

    iget-wide v2, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mUpdateTime:J

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 272
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->resetTodayData()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 273
    const/4 v0, 0x1

    .line 275
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 271
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public dateChanged()V
    .locals 4

    .prologue
    .line 581
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mUpdateTime:J

    .line 582
    return-void
.end method

.method public getBestValue()I
    .locals 1

    .prologue
    .line 463
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mBestValue:I

    return v0
.end method

.method public getCal()F
    .locals 1

    .prologue
    .line 451
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mCal:F

    return v0
.end method

.method public getDistance()F
    .locals 1

    .prologue
    .line 455
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mDistance:F

    return v0
.end method

.method public getGoal()I
    .locals 1

    .prologue
    .line 459
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mGoalValue:I

    return v0
.end method

.method public getLastUpdateTime()J
    .locals 2

    .prologue
    .line 475
    iget-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->lastUpdateTime:J

    return-wide v0
.end method

.method public getLastUpdateType()I
    .locals 1

    .prologue
    .line 467
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->lastUpdateType:I

    return v0
.end method

.method public getTotalStep()I
    .locals 1

    .prologue
    .line 447
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mTotalStep:I

    return v0
.end method

.method public isAcheivedNotify()Z
    .locals 1

    .prologue
    .line 556
    sget-boolean v0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mAcheivedNotify:Z

    return v0
.end method

.method public isHalfNotify()Z
    .locals 1

    .prologue
    .line 576
    sget-boolean v0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mHalfNotify:Z

    return v0
.end method

.method public isNewRecordNotify()Z
    .locals 1

    .prologue
    .line 564
    sget-boolean v0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mNewRecordNotify:Z

    return v0
.end method

.method public isNotUsedNotify()Z
    .locals 1

    .prologue
    .line 560
    sget-boolean v0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mNotUsedNotify:Z

    return v0
.end method

.method public isRefreshingNotiState()Z
    .locals 1

    .prologue
    .line 568
    sget-boolean v0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mIsRefreshingNotiState:Z

    return v0
.end method

.method public declared-synchronized refreshBestStep()V
    .locals 1

    .prologue
    .line 265
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->getBestRecordInfo()Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;->getSteps()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mBestValue:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 268
    monitor-exit p0

    return-void

    .line 265
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized refreshGoal()V
    .locals 3

    .prologue
    .line 259
    monitor-enter p0

    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->loadGoalAndSearch(J)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mGoalValue:I

    .line 260
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "updated goal = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mGoalValue:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 262
    monitor-exit p0

    return-void

    .line 259
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized refreshTodayGoalFromDB()V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 242
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/service/health/keyManager/KeyManager;->isContentProviderAccessible()Z

    move-result v2

    if-nez v2, :cond_0

    .line 243
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->TAG:Ljava/lang/String;

    const-string v1, "DB is locked."

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 253
    :goto_0
    monitor-exit p0

    return-void

    .line 246
    :cond_0
    :try_start_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->loadGoalAndSearch(J)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mGoalValue:I

    .line 247
    sget-object v2, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "updated goal = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mGoalValue:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 248
    sget-boolean v2, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mIsRefreshingNotiState:Z

    if-nez v2, :cond_1

    .line 249
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->resetNotificationStatus()V

    .line 251
    :cond_1
    iget v2, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mTotalStep:I

    iget v3, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mGoalValue:I

    div-int/lit8 v3, v3, 0x2

    if-ge v2, v3, :cond_2

    move v2, v0

    :goto_1
    sput-boolean v2, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->halfNotificationNeeded:Z

    .line 252
    iget v2, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mTotalStep:I

    iget v3, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mGoalValue:I

    if-ge v2, v3, :cond_3

    :goto_2
    sput-boolean v0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->fullNotificationNeeded:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 242
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    move v2, v1

    .line 251
    goto :goto_1

    :cond_3
    move v0, v1

    .line 252
    goto :goto_2
.end method

.method public declared-synchronized registerListener(Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData$OnPedoMeterSensorDataReceiveListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData$OnPedoMeterSensorDataReceiveListener;

    .prologue
    .line 160
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "registerListener ~~listener = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 161
    if-nez p1, :cond_1

    .line 171
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 164
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mListener:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 165
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mListener:Ljava/util/ArrayList;

    monitor-enter v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 166
    :try_start_2
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->duplicationCheck(Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData$OnPedoMeterSensorDataReceiveListener;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 167
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mListener:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 169
    :cond_2
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 160
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized resetNotificationStatus()V
    .locals 1

    .prologue
    .line 62
    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mIsRefreshingNotiState:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    .line 77
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 66
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    sput-boolean v0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mNewRecordNotify:Z

    .line 67
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mAcheivedNotify:Z

    .line 68
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mHalfNotify:Z

    .line 69
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mNotUsedNotify:Z

    .line 70
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mNotUsedNotify:Z

    .line 71
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mInactiveNotify:Z

    .line 72
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mIsRefreshingNotiState:Z

    .line 73
    const/4 v0, -0x1

    sput v0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mLastNotiType:I

    .line 74
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mWalkingMateDayStepService:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    if-eqz v0, :cond_0

    .line 75
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mWalkingMateDayStepService:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->stopNotification()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 62
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized resetTodayData()V
    .locals 3

    .prologue
    .line 513
    monitor-enter p0

    const/4 v2, 0x0

    :try_start_0
    iput v2, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mDistance:F

    .line 514
    const/4 v2, 0x0

    iput v2, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mCal:F

    .line 515
    const/4 v2, 0x0

    iput v2, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mTotalStep:I

    .line 516
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->refreshBestStep()V

    .line 517
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->refreshTodayGoalFromDB()V

    .line 518
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->resetData()V

    .line 521
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mWalkingMateDayStepService:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->isSensorHubSupported()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 522
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mWalkingMateDayStepService:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->isWalkingStarted()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 523
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mWalkingMateDayStepService:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->getExerciseId()J

    move-result-wide v0

    .line 524
    .local v0, "exerciseId":J
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->recreateExerciseID(J)V

    .line 531
    .end local v0    # "exerciseId":J
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->checkUnitSetting()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 532
    monitor-exit p0

    return-void

    .line 513
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized sendModeChange(Ljava/lang/String;)V
    .locals 5
    .param p1, "walkingMode"    # Ljava/lang/String;

    .prologue
    .line 218
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mListener:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mListener:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 219
    :cond_0
    sget-object v2, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "mListener is null or empty : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mListener:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 227
    :goto_0
    monitor-exit p0

    return-void

    .line 222
    :cond_1
    :try_start_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mListener:Ljava/util/ArrayList;

    monitor-enter v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 223
    :try_start_2
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mListener:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData$OnPedoMeterSensorDataReceiveListener;

    .line 224
    .local v1, "listener":Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData$OnPedoMeterSensorDataReceiveListener;
    invoke-interface {v1, p1}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData$OnPedoMeterSensorDataReceiveListener;->onWalkingModeChanged(Ljava/lang/String;)V

    goto :goto_1

    .line 226
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "listener":Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData$OnPedoMeterSensorDataReceiveListener;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 218
    :catchall_1
    move-exception v2

    monitor-exit p0

    throw v2

    .line 226
    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_2
    :try_start_4
    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0
.end method

.method public declared-synchronized sendResponse(I)V
    .locals 18
    .param p1, "deviceType"    # I

    .prologue
    .line 186
    monitor-enter p0

    const/4 v15, 0x0

    .line 187
    .local v15, "NOT_USED":I
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mListener:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mListener:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 188
    :cond_0
    sget-object v2, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "mListener is null or empty : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mListener:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 215
    :goto_0
    monitor-exit p0

    return-void

    .line 193
    :cond_1
    :try_start_1
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getLastUpdateType()I

    move-result v2

    move/from16 v0, p1

    if-eq v2, v0, :cond_2

    .line 194
    sget-object v2, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Device type is changed to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 195
    const-wide/16 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->setLastUpdateTime(J)V

    .line 199
    :cond_2
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->lastUpdateTime:J

    const-wide/16 v4, 0x10

    add-long/2addr v2, v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-lez v2, :cond_3

    .line 200
    const-string v2, "OPTIMIZE-CALLBACK"

    const-string/jumbo v3, "skip...."

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 186
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 205
    :cond_3
    :try_start_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->setLastUpdateTime(J)V

    .line 206
    invoke-virtual/range {p0 .. p1}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->setLastUpdateType(I)V

    .line 208
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mListener:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    monitor-enter v17
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 209
    :try_start_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mListener:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v16

    .local v16, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData$OnPedoMeterSensorDataReceiveListener;

    .line 210
    .local v1, "listener":Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData$OnPedoMeterSensorDataReceiveListener;
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mDistance:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mCal:F

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mTotalStep:I

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget-wide v13, v0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mUpdateTime:J

    move/from16 v12, p1

    invoke-interface/range {v1 .. v14}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData$OnPedoMeterSensorDataReceiveListener;->onResponseReceived(FFFIIIIIIIIJ)V

    goto :goto_1

    .line 214
    .end local v1    # "listener":Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData$OnPedoMeterSensorDataReceiveListener;
    .end local v16    # "i$":Ljava/util/Iterator;
    :catchall_1
    move-exception v2

    monitor-exit v17
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    throw v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .restart local v16    # "i$":Ljava/util/Iterator;
    :cond_4
    :try_start_5
    monitor-exit v17
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto/16 :goto_0
.end method

.method public declared-synchronized sendResponseForSync(II)V
    .locals 4
    .param p1, "error"    # I
    .param p2, "deviceType"    # I

    .prologue
    .line 230
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mListener:Ljava/util/ArrayList;

    if-eqz v2, :cond_1

    .line 231
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mListener:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 232
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mListener:Ljava/util/ArrayList;

    monitor-enter v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 233
    :try_start_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mListener:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData$OnPedoMeterSensorDataReceiveListener;

    .line 234
    .local v1, "listener":Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData$OnPedoMeterSensorDataReceiveListener;
    invoke-interface {v1, p1, p2}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData$OnPedoMeterSensorDataReceiveListener;->onWearableSyncComplete(II)V

    goto :goto_0

    .line 236
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "listener":Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData$OnPedoMeterSensorDataReceiveListener;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 230
    :catchall_1
    move-exception v2

    monitor-exit p0

    throw v2

    .line 236
    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 239
    .end local v0    # "i$":Ljava/util/Iterator;
    :cond_1
    monitor-exit p0

    return-void
.end method

.method public setDayStepService(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;)V
    .locals 2
    .param p1, "_dayStepService"    # Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    .prologue
    .line 132
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "setDayStepService"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mWalkingMateDayStepService:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    .line 134
    return-void
.end method

.method public declared-synchronized setLastUpdateTime(J)V
    .locals 1
    .param p1, "_time"    # J

    .prologue
    .line 479
    monitor-enter p0

    :try_start_0
    iput-wide p1, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->lastUpdateTime:J

    .line 480
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->updateWidget()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 481
    monitor-exit p0

    return-void

    .line 479
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setLastUpdateType(I)V
    .locals 0
    .param p1, "_type"    # I

    .prologue
    .line 471
    iput p1, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->lastUpdateType:I

    .line 472
    return-void
.end method

.method public declared-synchronized setRefreshingNotiState(Z)V
    .locals 1
    .param p1, "_status"    # Z

    .prologue
    .line 572
    monitor-enter p0

    :try_start_0
    sput-boolean p1, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mIsRefreshingNotiState:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 573
    monitor-exit p0

    return-void

    .line 572
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized unregisterListener(Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData$OnPedoMeterSensorDataReceiveListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData$OnPedoMeterSensorDataReceiveListener;

    .prologue
    .line 174
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "unregisterListener ~~listener = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 175
    if-nez p1, :cond_1

    .line 183
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 178
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mListener:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 179
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mListener:Ljava/util/ArrayList;

    monitor-enter v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 180
    :try_start_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mListener:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 181
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 174
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public updateData(IFFJ)V
    .locals 4
    .param p1, "totalSteps"    # I
    .param p2, "cal"    # F
    .param p3, "distance"    # F
    .param p4, "updateTime"    # J

    .prologue
    const/16 v3, 0x2719

    .line 394
    invoke-virtual {p0, p4, p5}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->dateChangeCheck(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 395
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->updateFromDB()V

    .line 398
    :cond_0
    iput-wide p4, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mUpdateTime:J

    .line 399
    iput p1, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mTotalStep:I

    .line 400
    iput p2, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mCal:F

    .line 401
    iput p3, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mDistance:F

    .line 402
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "updateData() - mTotalStep : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mTotalStep:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mCal : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mCal:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mDistance : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mDistance:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 405
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v0

    if-ne v0, v3, :cond_1

    .line 406
    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->sendResponse(I)V

    .line 407
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->checkNotiStatus()V

    .line 408
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->updateWidget()V

    .line 410
    :cond_1
    return-void
.end method

.method public updateFromDB()V
    .locals 1

    .prologue
    .line 413
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->updateFromDB(Z)V

    .line 414
    return-void
.end method

.method public declared-synchronized updateFromDB(Z)V
    .locals 9
    .param p1, "widgetUpdated"    # Z

    .prologue
    .line 417
    monitor-enter p0

    :try_start_0
    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "update from DB"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 418
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/service/health/keyManager/KeyManager;->isContentProviderAccessible()Z

    move-result v1

    if-nez v1, :cond_1

    .line 419
    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->TAG:Ljava/lang/String;

    const-string v2, "DB is locked."

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 444
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 422
    :cond_1
    :try_start_1
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtilsData;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtilsData;-><init>(IFFJ)V

    .line 424
    .local v0, "data":Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtilsData;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 425
    .local v6, "currentTime":J
    invoke-virtual {p0, v6, v7}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->dateChangeCheck(J)Z

    .line 426
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v8

    .line 427
    .local v8, "deviceType":I
    invoke-static {v6, v7, v8}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->getTotalValuesFromAllDevice(JI)Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtilsData;

    move-result-object v0

    .line 429
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtilsData;->getTotalSteps()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mTotalStep:I

    .line 430
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtilsData;->getCalories()F

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mCal:F

    .line 431
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtilsData;->getDistance()F

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mDistance:F

    .line 432
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtilsData;->getUpdateTime()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mUpdateTime:J

    .line 434
    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "updateFromDB() - deviceType="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "mTotalStep="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mTotalStep:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mCal="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mCal:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mDistance="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mDistance:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mUpdateTime="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v3, p0, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->mUpdateTime:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 439
    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->sendResponse(I)V

    .line 440
    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    .line 441
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->refreshGoal()V

    .line 442
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->updateWidget()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 417
    .end local v0    # "data":Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtilsData;
    .end local v6    # "currentTime":J
    .end local v8    # "deviceType":I
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method
