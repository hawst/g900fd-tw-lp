.class public Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;
.super Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;
.source "AMapExerciseProStatusFragment.java"

# interfaces
.implements Landroid/location/GpsStatus$Listener;
.implements Landroid/view/View$OnClickListener;
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/OnSaveChosenItemListener;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ValidFragment"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$AMapLocationSource;,
        Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$MediainfoReceiver;,
        Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$ExerciseImageScaleCopyTask;,
        Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$GoalValueTask;,
        Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$DropDownClickListener;,
        Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$REALTIME_DATA;
    }
.end annotation


# static fields
.field public static final ACTION_FILTER_MEDIA_Next_442:Ljava/lang/String; = "com.samsung.musicplus.musicservicecommand.next"

.field public static final ACTION_FILTER_MEDIA_PREVIOUS_442:Ljava/lang/String; = "com.samsung.musicplus.musicservicecommand.previous"

.field public static final ACTION_FILTER_MEDIA_PREVIOUS_KK:Ljava/lang/String; = "com.sec.android.app.music.musicservicecommand.previous"

.field private static final DEFAULT_PERIOD:I = -0x1

.field private static final MAX_COACH_INDEX:I = 0x1

.field private static final POLYLINE_DEFAULT_COLOR:I

.field private static POLYLINE_DEFAULT_WIDTH:I = 0x0

.field static final SPACE:Ljava/lang/String; = " "

.field private static final TAG:Ljava/lang/String;

.field private static artist:Ljava/lang/String;

.field private static isMusicPlaying:Z

.field private static temp_db_value:J

.field private static track:Ljava/lang/String;


# instance fields
.field private alignment:I

.field private big_extra_value:Landroid/widget/TextView;

.field private big_layout:Landroid/widget/RelativeLayout;

.field private big_value:Landroid/widget/TextView;

.field cardViewDataTitleTextView:Landroid/widget/TextView;

.field cyclingView:Landroid/view/View;

.field private final dataTextViewIDs:[I

.field private dispDataTypes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field firstbubbleView:Landroid/widget/ImageView;

.field fourthbubbleView:Landroid/widget/ImageView;

.field hikingView:Landroid/view/View;

.field private internalViewIdUniquizeHelper:Lcom/sec/android/app/shealth/common/utils/FocusUtils$ViewIdUniquizeHelper;

.field private isVisualGuideLayoutVisible:Z

.field private keepMusicText:Z

.field private mAMapListener:Lcom/amap/api/maps2d/LocationSource$OnLocationChangedListener;

.field private mAccWeatherButtonListener:Landroid/view/View$OnClickListener;

.field private mAccuWeather:Landroid/view/View;

.field private mActivity:Landroid/app/Activity;

.field private mActivityTextCycling:Landroid/widget/TextView;

.field private mActivityTextHiking:Landroid/widget/TextView;

.field private mActivityTextRunning:Landroid/widget/TextView;

.field private mActivityTextWaliking:Landroid/widget/TextView;

.field private mActivityType:Landroid/view/View;

.field private mActivityTypeClickListener:Landroid/view/View$OnClickListener;

.field private mAudioCheckbox:Landroid/widget/CheckBox;

.field private mAvgSpeed:F

.field mBackGroundHandler:Landroid/os/Handler;

.field private mBtnMusic_next:Landroid/widget/ImageButton;

.field private mBtnMusic_paly:Landroid/widget/ImageButton;

.field private mBtnMusic_previous:Landroid/widget/ImageButton;

.field private mBundle:Landroid/os/Bundle;

.field private mCanHideHRMData:Z

.field private mCardNoLayoutText:Landroid/widget/TextView;

.field private mCardView:Landroid/view/View;

.field private mCardViewDataLayout:Landroid/widget/LinearLayout;

.field private mCardViewDetailTxt1:Landroid/widget/TextView;

.field private mCardViewDetailTxt2:Landroid/widget/TextView;

.field private mCardViewDetailTxt3:Landroid/widget/TextView;

.field private mCardViewDistTxt:Landroid/widget/TextView;

.field private mCardViewDurTxt:Landroid/widget/TextView;

.field private mCardViewGoalImg:Landroid/widget/ImageView;

.field private mCardViewNoDataLayout:Landroid/widget/LinearLayout;

.field private mCardViewPaceTxt:Landroid/widget/TextView;

.field private mCircleProgress:Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;

.field private mCoachStatusLayout:Landroid/widget/LinearLayout;

.field private final mContentView:[Landroid/view/View;

.field mControllerListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$IHealthControllerListener;

.field private final mDataIcon:[Landroid/widget/ImageView;

.field private mDatas_layout:Landroid/view/View;

.field private mDrawables:[Landroid/graphics/drawable/Drawable;

.field private mDropDownList:Landroid/view/View;

.field private mGoalAchived:Landroid/widget/ImageView;

.field private mGoalTypeValue:I

.field protected mGoalTypeView:Landroid/view/View;

.field private mGoalValue:I

.field private mHeight:I

.field private mHeightSpec:I

.field private mImageCaptureUri:Landroid/net/Uri;

.field protected mIndoorOrOutdoor:Landroid/view/View;

.field private mKcalGoal:I

.field private mKcalSummaryLayout:Landroid/widget/LinearLayout;

.field private mLandscape:Z

.field private mLastLocation:Landroid/location/Location;

.field private mLastQueryTime:J

.field private mListPopupIndoorOrOutdoor:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

.field private mLocationHandler:Landroid/os/Handler;

.field private mLocationRunnable:Ljava/lang/Runnable;

.field public mMap:Lcom/amap/api/maps2d/AMap;

.field private mMapOperation:Landroid/view/View;

.field private mMapView:Lcom/amap/api/maps2d/MapView;

.field private mMaxElevation:D

.field private mMediaInfoReceiver:Landroid/content/BroadcastReceiver;

.field private mMusicButtonListener:Landroid/view/View$OnClickListener;

.field private mMusicController:Landroid/view/View;

.field private mMusicPlayerLayout:Landroid/widget/LinearLayout;

.field private mMusicState:Landroid/view/View;

.field private mNextIntent:Landroid/content/Intent;

.field mOnAudioGuideClickListener:Landroid/view/View$OnClickListener;

.field private mOperatelayout:Landroid/widget/RelativeLayout;

.field private mPolyIndex:I

.field mPolyLineOption:Lcom/amap/api/maps2d/model/PolylineOptions;

.field private mPrevIntent:Landroid/content/Intent;

.field private final mScrollTextViews:[Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;

.field private mStatusLayout:Landroid/widget/RelativeLayout;

.field private mSyncObj:Ljava/lang/Object;

.field private mTEenginlevel:I

.field private mTvAccuWeather:Landroid/widget/TextView;

.field private mUiSettings:Lcom/amap/api/maps2d/UiSettings;

.field private mView:Landroid/view/View;

.field private mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

.field private mZoomLevel:F

.field private musicDummyView:Landroid/view/View;

.field private musicSubTextView:Landroid/widget/TextView;

.field private musicTitleTextView:Landroid/widget/TextView;

.field private onResume:Z

.field private popup:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

.field runningView:Landroid/view/View;

.field secondbubbleView:Landroid/widget/ImageView;

.field public startingPolylines:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/amap/api/maps2d/model/Polyline;",
            ">;"
        }
    .end annotation
.end field

.field thirdbubbleView:Landroid/widget/ImageView;

.field public tmpPolylines:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/amap/api/maps2d/model/Polyline;",
            ">;"
        }
    .end annotation
.end field

.field walkingView:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 134
    const-class v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->TAG:Ljava/lang/String;

    .line 138
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->temp_db_value:J

    .line 177
    sput-boolean v3, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->isMusicPlaying:Z

    .line 180
    const-string v0, ""

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->track:Ljava/lang/String;

    .line 181
    const-string v0, ""

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->artist:Ljava/lang/String;

    .line 193
    const/16 v0, 0x9

    sput v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->POLYLINE_DEFAULT_WIDTH:I

    .line 194
    const/16 v0, 0x80

    const/16 v1, 0x85

    const/16 v2, 0x2c

    invoke-static {v0, v3, v1, v2}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    sput v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->POLYLINE_DEFAULT_COLOR:I

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 261
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;-><init>()V

    .line 148
    iput v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mHeight:I

    .line 149
    const/4 v0, -0x2

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mHeightSpec:I

    .line 151
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mLandscape:Z

    .line 159
    iput v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mKcalGoal:I

    .line 160
    const v0, 0x20001

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->alignment:I

    .line 161
    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mPolyLineOption:Lcom/amap/api/maps2d/model/PolylineOptions;

    .line 162
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mLastQueryTime:J

    .line 164
    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mMediaInfoReceiver:Landroid/content/BroadcastReceiver;

    .line 167
    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mKcalSummaryLayout:Landroid/widget/LinearLayout;

    .line 173
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mSyncObj:Ljava/lang/Object;

    .line 174
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCanHideHRMData:Z

    .line 178
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->keepMusicText:Z

    iput-boolean v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->onResume:Z

    .line 191
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->tmpPolylines:Ljava/util/List;

    .line 192
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->startingPolylines:Ljava/util/List;

    .line 195
    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCircleProgress:Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;

    .line 206
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/FocusUtils$ViewIdUniquizeHelper;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/common/utils/FocusUtils$ViewIdUniquizeHelper;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->internalViewIdUniquizeHelper:Lcom/sec/android/app/shealth/common/utils/FocusUtils$ViewIdUniquizeHelper;

    .line 208
    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mAudioCheckbox:Landroid/widget/CheckBox;

    .line 212
    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mImageCaptureUri:Landroid/net/Uri;

    .line 218
    new-array v0, v4, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dataTextViewIDs:[I

    .line 222
    new-array v0, v4, [Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mScrollTextViews:[Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;

    .line 223
    new-array v0, v4, [Landroid/view/View;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mContentView:[Landroid/view/View;

    .line 224
    new-array v0, v4, [Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mDataIcon:[Landroid/widget/ImageView;

    .line 234
    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCardView:Landroid/view/View;

    .line 235
    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCardViewNoDataLayout:Landroid/widget/LinearLayout;

    .line 236
    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCardViewDataLayout:Landroid/widget/LinearLayout;

    .line 256
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->isVisualGuideLayoutVisible:Z

    .line 604
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$4;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivityTypeClickListener:Landroid/view/View$OnClickListener;

    .line 884
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$5;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mMusicButtonListener:Landroid/view/View$OnClickListener;

    .line 923
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$6;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mAccWeatherButtonListener:Landroid/view/View$OnClickListener;

    .line 1041
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$8;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$8;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mOnAudioGuideClickListener:Landroid/view/View$OnClickListener;

    .line 1368
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$9;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$9;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mControllerListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$IHealthControllerListener;

    .line 3667
    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$13;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$13;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mBackGroundHandler:Landroid/os/Handler;

    .line 3949
    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mLocationHandler:Landroid/os/Handler;

    .line 3950
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$16;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$16;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mLocationRunnable:Ljava/lang/Runnable;

    .line 262
    return-void

    .line 218
    nop

    :array_0
    .array-data 4
        0x7f08070c
        0x7f08070d
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 264
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;-><init>()V

    .line 148
    iput v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mHeight:I

    .line 149
    const/4 v0, -0x2

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mHeightSpec:I

    .line 151
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mLandscape:Z

    .line 159
    iput v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mKcalGoal:I

    .line 160
    const v0, 0x20001

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->alignment:I

    .line 161
    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mPolyLineOption:Lcom/amap/api/maps2d/model/PolylineOptions;

    .line 162
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mLastQueryTime:J

    .line 164
    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mMediaInfoReceiver:Landroid/content/BroadcastReceiver;

    .line 167
    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mKcalSummaryLayout:Landroid/widget/LinearLayout;

    .line 173
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mSyncObj:Ljava/lang/Object;

    .line 174
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCanHideHRMData:Z

    .line 178
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->keepMusicText:Z

    iput-boolean v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->onResume:Z

    .line 191
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->tmpPolylines:Ljava/util/List;

    .line 192
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->startingPolylines:Ljava/util/List;

    .line 195
    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCircleProgress:Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;

    .line 206
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/FocusUtils$ViewIdUniquizeHelper;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/common/utils/FocusUtils$ViewIdUniquizeHelper;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->internalViewIdUniquizeHelper:Lcom/sec/android/app/shealth/common/utils/FocusUtils$ViewIdUniquizeHelper;

    .line 208
    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mAudioCheckbox:Landroid/widget/CheckBox;

    .line 212
    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mImageCaptureUri:Landroid/net/Uri;

    .line 218
    new-array v0, v4, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dataTextViewIDs:[I

    .line 222
    new-array v0, v4, [Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mScrollTextViews:[Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;

    .line 223
    new-array v0, v4, [Landroid/view/View;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mContentView:[Landroid/view/View;

    .line 224
    new-array v0, v4, [Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mDataIcon:[Landroid/widget/ImageView;

    .line 234
    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCardView:Landroid/view/View;

    .line 235
    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCardViewNoDataLayout:Landroid/widget/LinearLayout;

    .line 236
    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCardViewDataLayout:Landroid/widget/LinearLayout;

    .line 256
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->isVisualGuideLayoutVisible:Z

    .line 604
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$4;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivityTypeClickListener:Landroid/view/View$OnClickListener;

    .line 884
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$5;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mMusicButtonListener:Landroid/view/View$OnClickListener;

    .line 923
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$6;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mAccWeatherButtonListener:Landroid/view/View$OnClickListener;

    .line 1041
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$8;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$8;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mOnAudioGuideClickListener:Landroid/view/View$OnClickListener;

    .line 1368
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$9;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$9;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mControllerListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$IHealthControllerListener;

    .line 3667
    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$13;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$13;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mBackGroundHandler:Landroid/os/Handler;

    .line 3949
    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mLocationHandler:Landroid/os/Handler;

    .line 3950
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$16;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$16;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mLocationRunnable:Ljava/lang/Runnable;

    .line 265
    return-void

    .line 218
    nop

    :array_0
    .array-data 4
        0x7f08070c
        0x7f08070d
    .end array-data
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 132
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    .prologue
    .line 132
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->loadConfigure()V

    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    .prologue
    .line 132
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mGoalTypeValue:I

    return v0
.end method

.method static synthetic access$1002(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;
    .param p1, "x1"    # I

    .prologue
    .line 132
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mGoalTypeValue:I

    return p1
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    .prologue
    .line 132
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mGoalValue:I

    return v0
.end method

.method static synthetic access$1102(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;
    .param p1, "x1"    # I

    .prologue
    .line 132
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mGoalValue:I

    return p1
.end method

.method static synthetic access$1200(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    .prologue
    .line 132
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mTEenginlevel:I

    return v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;III)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;
    .param p1, "x1"    # I
    .param p2, "x2"    # I
    .param p3, "x3"    # I

    .prologue
    .line 132
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->setGoalValue(III)V

    return-void
.end method

.method static synthetic access$1400(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;
    .param p1, "x1"    # I

    .prologue
    .line 132
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispActivityTypeData(I)V

    return-void
.end method

.method static synthetic access$1500(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;IZ)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;
    .param p1, "x1"    # I
    .param p2, "x2"    # Z

    .prologue
    .line 132
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispGoalTypeData(IZ)V

    return-void
.end method

.method static synthetic access$1600(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)Landroid/content/Intent;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mPrevIntent:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic access$1700()Z
    .locals 1

    .prologue
    .line 132
    sget-boolean v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->isMusicPlaying:Z

    return v0
.end method

.method static synthetic access$1702(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 132
    sput-boolean p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->isMusicPlaying:Z

    return p0
.end method

.method static synthetic access$1800(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)Landroid/content/Intent;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mNextIntent:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mTvAccuWeather:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    .prologue
    .line 132
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->setBigTextSize()V

    return-void
.end method

.method static synthetic access$2000(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    .prologue
    .line 132
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->keepMusicText:Z

    return v0
.end method

.method static synthetic access$2002(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 132
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->keepMusicText:Z

    return p1
.end method

.method static synthetic access$2100(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    .prologue
    .line 132
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->updateAudioGuideButton()V

    return-void
.end method

.method static synthetic access$2200(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mAudioCheckbox:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;
    .param p1, "x1"    # I

    .prologue
    .line 132
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->updateDataText(I)V

    return-void
.end method

.method static synthetic access$2400(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    .prologue
    .line 132
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->setDefaultBackground()V

    return-void
.end method

.method static synthetic access$2500(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    .prologue
    .line 132
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->updateDataText()V

    return-void
.end method

.method static synthetic access$2600(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;J)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;
    .param p1, "x1"    # J

    .prologue
    .line 132
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->lapClockDispUpdate(J)V

    return-void
.end method

.method static synthetic access$2700(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 132
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->enableGoal(Z)V

    return-void
.end method

.method static synthetic access$2800(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    .prologue
    .line 132
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->updateIndoorlayout()V

    return-void
.end method

.method static synthetic access$2900(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 132
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->updateModeState(Z)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)Lcom/sec/android/app/shealth/common/commonui/ListPopup;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mListPopupIndoorOrOutdoor:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    return-object v0
.end method

.method static synthetic access$3100(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    .prologue
    .line 132
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCanHideHRMData:Z

    return v0
.end method

.method static synthetic access$3102(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 132
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCanHideHRMData:Z

    return p1
.end method

.method static synthetic access$3200(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    .prologue
    .line 132
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->hideHRMData()V

    return-void
.end method

.method static synthetic access$3300(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    .prologue
    .line 132
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispHRMData()V

    return-void
.end method

.method static synthetic access$3400(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)Landroid/location/Location;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mLastLocation:Landroid/location/Location;

    return-object v0
.end method

.method static synthetic access$3402(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;Landroid/location/Location;)Landroid/location/Location;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;
    .param p1, "x1"    # Landroid/location/Location;

    .prologue
    .line 132
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mLastLocation:Landroid/location/Location;

    return-object p1
.end method

.method static synthetic access$3500(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)Lcom/amap/api/maps2d/LocationSource$OnLocationChangedListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mAMapListener:Lcom/amap/api/maps2d/LocationSource$OnLocationChangedListener;

    return-object v0
.end method

.method static synthetic access$3502(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;Lcom/amap/api/maps2d/LocationSource$OnLocationChangedListener;)Lcom/amap/api/maps2d/LocationSource$OnLocationChangedListener;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;
    .param p1, "x1"    # Lcom/amap/api/maps2d/LocationSource$OnLocationChangedListener;

    .prologue
    .line 132
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mAMapListener:Lcom/amap/api/maps2d/LocationSource$OnLocationChangedListener;

    return-object p1
.end method

.method static synthetic access$3600(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;Landroid/location/Location;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;
    .param p1, "x1"    # Landroid/location/Location;
    .param p2, "x2"    # I

    .prologue
    .line 132
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->playMap(Landroid/location/Location;I)V

    return-void
.end method

.method static synthetic access$3700(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    .prologue
    .line 132
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->isVisualGuideLayoutVisible:Z

    return v0
.end method

.method static synthetic access$3702(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 132
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->isVisualGuideLayoutVisible:Z

    return p1
.end method

.method static synthetic access$3800(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    .prologue
    .line 132
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->reSizeDataLayout()V

    return-void
.end method

.method static synthetic access$3900(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 132
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->startAnimationFromLockMode(Z)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)Landroid/app/Activity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$4000(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)Lcom/amap/api/maps2d/MapView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mMapView:Lcom/amap/api/maps2d/MapView;

    return-object v0
.end method

.method static synthetic access$4100(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 132
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->setUpMapIfNeeded(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$4200(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mLocationHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$4202(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;Landroid/os/Handler;)Landroid/os/Handler;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;
    .param p1, "x1"    # Landroid/os/Handler;

    .prologue
    .line 132
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mLocationHandler:Landroid/os/Handler;

    return-object p1
.end method

.method static synthetic access$4300(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mLocationRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$4400(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->musicDummyView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$4500(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mMusicPlayerLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$4600(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->musicTitleTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$4700(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mMapOperation:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$4900(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;
    .param p1, "x1"    # I

    .prologue
    .line 132
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->setWorkoutModeState(I)V

    return-void
.end method

.method static synthetic access$5000(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)Lcom/sec/android/app/shealth/common/commonui/ListPopup;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->popup:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    return-object v0
.end method

.method static synthetic access$5002(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;Lcom/sec/android/app/shealth/common/commonui/ListPopup;)Lcom/sec/android/app/shealth/common/commonui/ListPopup;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    .prologue
    .line 132
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->popup:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    return-object p1
.end method

.method static synthetic access$5100(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    .prologue
    .line 132
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mLandscape:Z

    return v0
.end method

.method static synthetic access$5200(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    .prologue
    .line 132
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->alignment:I

    return v0
.end method

.method static synthetic access$5202(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;
    .param p1, "x1"    # I

    .prologue
    .line 132
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->alignment:I

    return p1
.end method

.method static synthetic access$5400(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$5600(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;ILandroid/view/View;Lcom/sec/android/app/shealth/common/commonui/ListPopup;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;
    .param p1, "x1"    # I
    .param p2, "x2"    # Landroid/view/View;
    .param p3, "x3"    # Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    .prologue
    .line 132
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getXOffset(ILandroid/view/View;Lcom/sec/android/app/shealth/common/commonui/ListPopup;)I

    move-result v0

    return v0
.end method

.method static synthetic access$5700(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;ILandroid/view/View;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;
    .param p1, "x1"    # I
    .param p2, "x2"    # Landroid/view/View;

    .prologue
    .line 132
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getYOffset(ILandroid/view/View;)I

    move-result v0

    return v0
.end method

.method static synthetic access$5800(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    .prologue
    .line 132
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mHeightSpec:I

    return v0
.end method

.method static synthetic access$5900(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    .prologue
    .line 132
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mHeight:I

    return v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    .prologue
    .line 132
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mZoomLevel:F

    return v0
.end method

.method static synthetic access$6000(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    .prologue
    .line 132
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->updatedGoalTextByTaskPostExecute()V

    return-void
.end method

.method static synthetic access$602(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;
    .param p1, "x1"    # F

    .prologue
    .line 132
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mZoomLevel:F

    return p1
.end method

.method static synthetic access$6100(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;Ljava/lang/String;J)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # J

    .prologue
    .line 132
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->saveInfoAndUpdateViewGrid(Ljava/lang/String;J)V

    return-void
.end method

.method static synthetic access$6200()Ljava/lang/String;
    .locals 1

    .prologue
    .line 132
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->track:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$6202(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 132
    sput-object p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->track:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$6300()Ljava/lang/String;
    .locals 1

    .prologue
    .line 132
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->artist:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$6302(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 132
    sput-object p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->artist:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$6400(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    .prologue
    .line 132
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->onResume:Z

    return v0
.end method

.method static synthetic access$6402(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 132
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->onResume:Z

    return p1
.end method

.method static synthetic access$6500(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;ZLjava/lang/String;Ljava/lang/String;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;
    .param p1, "x1"    # Z
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Ljava/lang/String;
    .param p4, "x4"    # Z

    .prologue
    .line 132
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->updateMusicControllerLayout(ZLjava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method

.method static synthetic access$6600(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mSyncObj:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$6700(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)[Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mDrawables:[Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method static synthetic access$6702(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;[Landroid/graphics/drawable/Drawable;)[Landroid/graphics/drawable/Drawable;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;
    .param p1, "x1"    # [Landroid/graphics/drawable/Drawable;

    .prologue
    .line 132
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mDrawables:[Landroid/graphics/drawable/Drawable;

    return-object p1
.end method

.method static synthetic access$6800(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mDatas_layout:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    .prologue
    .line 132
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->updateModeText()V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    .prologue
    .line 132
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->updateActivityType()V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    .prologue
    .line 132
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->updateCardView()V

    return-void
.end method

.method private checkGoalAchieved(J)V
    .locals 24
    .param p1, "exerciseId"    # J

    .prologue
    .line 2985
    const/16 v17, 0x0

    .line 2986
    .local v17, "goalSubtype":I
    const/16 v18, 0x0

    .line 2987
    .local v18, "goalValue":I
    const-wide/16 v21, -0x1

    .line 2988
    .local v21, "starttime":J
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "select exercise.[start_time] as TIME from exercise where exercise.[_id]="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-wide/from16 v0, p1

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 2989
    .local v5, "exerciseQuery":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v23

    .line 2991
    .local v23, "subTypeCursor":Landroid/database/Cursor;
    if-eqz v23, :cond_1

    .line 2992
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->moveToFirst()Z

    .line 2993
    const-string v2, "TIME"

    move-object/from16 v0, v23

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v23

    invoke-interface {v0, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2994
    const-string v2, "TIME"

    move-object/from16 v0, v23

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v23

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v21

    .line 2995
    :cond_0
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->close()V

    .line 2997
    :cond_1
    const-wide/16 v2, 0x0

    cmp-long v2, v21, v2

    if-gez v2, :cond_3

    .line 2998
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCardViewGoalImg:Landroid/widget/ImageView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 3053
    :cond_2
    :goto_0
    return-void

    .line 3001
    :cond_3
    const-string v20, "40002,40007,40008,40009,40010"

    .line 3004
    .local v20, "inValues":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "select goal.[goal_subtype] as GTYPE, goal.[value]  as GVAL from goal where  goal.[update_time]<"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-wide/from16 v0, v21

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  and goal.[goal_type] in ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")  order by  goal.[update_time] desc limit 1 "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 3007
    .local v9, "goalValueQuery":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    sget-object v7, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v8, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v6 .. v11}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v19

    .line 3009
    .local v19, "goalValueCursor":Landroid/database/Cursor;
    if-eqz v19, :cond_6

    .line 3010
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_5

    .line 3011
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->moveToFirst()Z

    .line 3012
    const-string v2, "GVAL"

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_4

    .line 3013
    const-string v2, "GVAL"

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v18

    .line 3014
    :cond_4
    const-string v2, "GTYPE"

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_5

    .line 3015
    const-string v2, "GTYPE"

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v17

    .line 3017
    :cond_5
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    .line 3020
    :cond_6
    if-lez v17, :cond_7

    if-gtz v18, :cond_8

    .line 3021
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCardViewGoalImg:Landroid/widget/ImageView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 3024
    :cond_8
    const/4 v13, 0x0

    .line 3025
    .local v13, "selectionClause":Ljava/lang/String;
    packed-switch v17, :pswitch_data_0

    .line 3041
    :goto_1
    if-eqz v13, :cond_2

    .line 3042
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    sget-object v11, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v12, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-virtual/range {v10 .. v15}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v16

    .line 3044
    .local v16, "cursor":Landroid/database/Cursor;
    if-eqz v16, :cond_a

    .line 3045
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_9

    .line 3046
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCardViewGoalImg:Landroid/widget/ImageView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 3047
    :cond_9
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 3027
    .end local v16    # "cursor":Landroid/database/Cursor;
    :pswitch_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "select exercise.[_id] from exercise where exercise.[_id]="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-wide/from16 v0, p1

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " and exercise.[distance]>="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 3029
    goto :goto_1

    .line 3031
    :pswitch_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "select exercise.[_id] from exercise where exercise.[_id]="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-wide/from16 v0, p1

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " and exercise.[total_calorie]>="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 3033
    goto :goto_1

    .line 3035
    :pswitch_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "select exercise.[_id] from exercise where exercise.[_id]="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-wide/from16 v0, p1

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " and exercise.[duration_min]>="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    goto/16 :goto_1

    .line 3049
    .restart local v16    # "cursor":Landroid/database/Cursor;
    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCardViewGoalImg:Landroid/widget/ImageView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 3025
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private checkInChinaArea()Z
    .locals 13

    .prologue
    const/16 v12, 0x1c6

    const/4 v11, 0x5

    const/4 v7, 0x1

    .line 3896
    const/4 v0, 0x0

    .line 3899
    .local v0, "isChinaArea":Z
    const-string v8, "PAP"

    const-string/jumbo v9, "ro.csc.sales_code"

    invoke-static {v9}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 3946
    :goto_0
    return v7

    .line 3902
    :cond_0
    const-string v8, "gsm.operator.iso-country"

    const-string v9, ""

    invoke-static {v8, v9}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 3904
    .local v1, "isoCountryCode":Ljava/lang/String;
    sget-object v8, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "isoCountryCode : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 3905
    const-string v8, "cn"

    invoke-virtual {v8, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 3906
    const/4 v0, 0x1

    .line 3909
    :cond_1
    const-string v8, "gsm.operator.numeric"

    const-string v9, ""

    invoke-static {v8, v9}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 3912
    .local v3, "opNumSIM1":Ljava/lang/String;
    const/4 v4, 0x0

    .local v4, "opNumSIM1Code":I
    const/4 v5, 0x0

    .line 3915
    .local v5, "opNumSIM2Code":I
    :try_start_0
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v8

    if-lt v8, v11, :cond_2

    .line 3916
    const-string v8, ","

    invoke-virtual {v3, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 3918
    .local v6, "opNumSIMCodes":[Ljava/lang/String;
    array-length v8, v6

    if-ne v8, v7, :cond_5

    .line 3919
    const/4 v7, 0x0

    aget-object v7, v6, v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    if-lt v7, v11, :cond_2

    .line 3920
    const/4 v7, 0x0

    aget-object v7, v6, v7

    const/4 v8, 0x0

    const/4 v9, 0x3

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    .line 3939
    .end local v6    # "opNumSIMCodes":[Ljava/lang/String;
    :cond_2
    :goto_1
    sget-object v7, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "opNumSIM1Code : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " opNumSIM2Code : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 3941
    const/16 v7, 0x1cc

    if-eq v4, v7, :cond_3

    const/16 v7, 0x1c7

    if-eq v4, v7, :cond_3

    if-eq v4, v12, :cond_3

    const/16 v7, 0x1cc

    if-eq v5, v7, :cond_3

    const/16 v7, 0x1c7

    if-eq v5, v7, :cond_3

    if-ne v5, v12, :cond_4

    .line 3943
    :cond_3
    const/4 v0, 0x1

    :cond_4
    move v7, v0

    .line 3946
    goto/16 :goto_0

    .line 3923
    .restart local v6    # "opNumSIMCodes":[Ljava/lang/String;
    :cond_5
    :try_start_1
    array-length v7, v6

    const/4 v8, 0x2

    if-ne v7, v8, :cond_2

    .line 3924
    const/4 v7, 0x0

    aget-object v7, v6, v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    if-lt v7, v11, :cond_6

    .line 3925
    const/4 v7, 0x0

    aget-object v7, v6, v7

    const/4 v8, 0x0

    const/4 v9, 0x3

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 3927
    :cond_6
    const/4 v7, 0x1

    aget-object v7, v6, v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    if-lt v7, v11, :cond_2

    .line 3928
    const/4 v7, 0x1

    aget-object v7, v6, v7

    const/4 v8, 0x0

    const/4 v9, 0x3

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v5

    goto :goto_1

    .line 3935
    .end local v6    # "opNumSIMCodes":[Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 3936
    .local v2, "nfe":Ljava/lang/NumberFormatException;
    sget-object v7, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->TAG:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/NumberFormatException;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public static checkPreviousRecordValue(J)Ljava/lang/String;
    .locals 13
    .param p0, "second"    # J

    .prologue
    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 1961
    sget-object v8, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "checkPreviousRecordValue: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1963
    invoke-static {p0, p1}, Ljava/lang/Math;->abs(J)J

    move-result-wide p0

    .line 1964
    const-wide/16 v2, 0x3c

    .line 1965
    .local v2, "MINUTE":J
    const-wide/16 v8, 0x3c

    mul-long v0, v8, v2

    .line 1966
    .local v0, "HOUR":J
    rem-long/2addr p0, v0

    .line 1967
    div-long v4, p0, v2

    .line 1968
    .local v4, "minute":J
    rem-long/2addr p0, v2

    .line 1969
    const-string v8, "%02d"

    new-array v9, v12, [Ljava/lang/Object;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v9, v11

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 1970
    .local v6, "minuteText":Ljava/lang/String;
    const-string v8, "%02d"

    new-array v9, v12, [Ljava/lang/Object;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v9, v11

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 1972
    .local v7, "secondText":Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ":"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    return-object v8
.end method

.method private convertToProperUnit(Ljava/lang/String;I)Ljava/lang/String;
    .locals 9
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "type"    # I

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 2337
    if-ne p2, v6, :cond_4

    .line 2338
    :try_start_0
    const-string v5, ":"

    invoke-virtual {p1, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 2339
    .local v1, "splitedDuration":[Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getWorkoutState()I

    move-result v3

    .line 2340
    .local v3, "state":I
    array-length v5, v1

    if-ne v5, v7, :cond_2

    .line 2341
    const/16 v5, 0x7d1

    if-ne v3, v5, :cond_1

    .line 2342
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v6, 0x0

    aget-object v6, v1, v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0901ce

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f09020b

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x1

    aget-object v6, v1, v6

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    add-int/lit8 v6, v6, 0x2

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f090216

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object p1

    .end local v1    # "splitedDuration":[Ljava/lang/String;
    .end local v3    # "state":I
    :cond_0
    :goto_0
    move-object v4, p1

    .line 2383
    .end local p1    # "value":Ljava/lang/String;
    .local v4, "value":Ljava/lang/String;
    :goto_1
    return-object v4

    .line 2347
    .end local v4    # "value":Ljava/lang/String;
    .restart local v1    # "splitedDuration":[Ljava/lang/String;
    .restart local v3    # "state":I
    .restart local p1    # "value":Ljava/lang/String;
    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v6, 0x0

    aget-object v6, v1, v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0901ce

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f09020b

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x1

    aget-object v6, v1, v6

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f090216

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 2351
    :cond_2
    array-length v5, v1

    if-le v5, v7, :cond_0

    .line 2352
    const/16 v5, 0x7d1

    if-ne v3, v5, :cond_3

    .line 2353
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v6, 0x0

    aget-object v6, v1, v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0901cc

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f09020b

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x1

    aget-object v6, v1, v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0901ce

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f09020b

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x2

    aget-object v6, v1, v6

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    add-int/lit8 v6, v6, 0x2

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f090216

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object p1

    goto/16 :goto_0

    .line 2359
    :cond_3
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v6, 0x0

    aget-object v6, v1, v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0901cc

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f09020b

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x1

    aget-object v6, v1, v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0901ce

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f09020b

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x2

    aget-object v6, v1, v6

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f090216

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object p1

    goto/16 :goto_0

    .line 2365
    .end local v1    # "splitedDuration":[Ljava/lang/String;
    .end local v3    # "state":I
    :cond_4
    const/16 v5, 0x13

    if-ne p2, v5, :cond_0

    .line 2366
    const-string v5, "\'"

    invoke-virtual {p1, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2367
    const-string v5, "\'"

    invoke-virtual {p1, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 2368
    .local v2, "splittedStrings":[Ljava/lang/String;
    array-length v5, v2

    if-le v5, v6, :cond_0

    .line 2369
    const/4 v5, 0x1

    aget-object v5, v2, v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 2370
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v6, 0x0

    aget-object v6, v2, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0901c0

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x1

    aget-object v6, v2, v6

    const-string v7, "\""

    invoke-virtual {v6, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    aget-object v6, v6, v7

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0901cd

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    const/4 v7, 0x1

    aget-object v7, v2, v7

    const-string v8, "\""

    invoke-virtual {v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x1

    aget-object v7, v7, v8

    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->convertToProperUnitsText(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto/16 :goto_0

    .line 2374
    :cond_5
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v6, 0x0

    aget-object v6, v2, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0901c0

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x1

    aget-object v6, v2, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0901cd

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p1

    goto/16 :goto_0

    .line 2380
    .end local v2    # "splittedStrings":[Ljava/lang/String;
    :catch_0
    move-exception v0

    .local v0, "nfe":Ljava/lang/NumberFormatException;
    move-object v4, p1

    .line 2381
    .end local p1    # "value":Ljava/lang/String;
    .restart local v4    # "value":Ljava/lang/String;
    goto/16 :goto_1
.end method

.method private dispActivityTypeData(I)V
    .locals 6
    .param p1, "activityType"    # I

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 3308
    packed-switch p1, :pswitch_data_0

    .line 3333
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 3334
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 3335
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    const/16 v1, 0x13

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 3340
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->saveRealtimeDispDataStatus(Ljava/util/ArrayList;)V

    .line 3341
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->saveRealtimeDispDataMap(Ljava/util/ArrayList;)V

    .line 3342
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->saveRealtimeDispDataBackupTypeForHRM(I)V

    .line 3343
    return-void

    .line 3311
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 3312
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 3313
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 3317
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 3318
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->isAvailableBarometer(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3319
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 3322
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 3321
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 3326
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 3327
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 3328
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 3308
    nop

    :pswitch_data_0
    .packed-switch 0x4652
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method private dispGoalTypeData(IZ)V
    .locals 7
    .param p1, "gaolType"    # I
    .param p2, "set"    # Z

    .prologue
    const/4 v6, 0x3

    const/16 v5, 0x13

    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x2

    .line 3346
    packed-switch p1, :pswitch_data_0

    .line 3414
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->saveRealtimeDispDataStatus(Ljava/util/ArrayList;)V

    .line 3415
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->saveRealtimeDispDataMap(Ljava/util/ArrayList;)V

    .line 3416
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->saveRealtimeDispDataBackupTypeForHRM(I)V

    .line 3417
    :goto_1
    return-void

    .line 3348
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getActivityType()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispActivityTypeData(I)V

    goto :goto_1

    .line 3351
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    const/4 v1, 0x7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 3352
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getActivityType()I

    move-result v0

    const/16 v1, 0x4653

    if-ne v0, v1, :cond_1

    .line 3353
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 3354
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 3355
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getActivityType()I

    move-result v0

    const/16 v1, 0x4652

    if-ne v0, v1, :cond_2

    .line 3356
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 3357
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 3358
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getActivityType()I

    move-result v0

    const/16 v1, 0x4654

    if-ne v0, v1, :cond_3

    .line 3359
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 3360
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 3361
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getActivityType()I

    move-result v0

    const/16 v1, 0x4655

    if-ne v0, v1, :cond_0

    .line 3362
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 3363
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 3367
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    const/4 v1, 0x7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 3368
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getActivityType()I

    move-result v0

    const/16 v1, 0x4653

    if-ne v0, v1, :cond_4

    .line 3369
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 3370
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 3371
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getActivityType()I

    move-result v0

    const/16 v1, 0x4652

    if-ne v0, v1, :cond_5

    .line 3372
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 3373
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 3374
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getActivityType()I

    move-result v0

    const/16 v1, 0x4654

    if-ne v0, v1, :cond_6

    .line 3375
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 3376
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 3377
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getActivityType()I

    move-result v0

    const/16 v1, 0x4655

    if-ne v0, v1, :cond_0

    .line 3378
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 3379
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->isAvailableBarometer(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 3380
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 3382
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 3388
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    const/4 v1, 0x7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 3389
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getActivityType()I

    move-result v0

    const/16 v1, 0x4653

    if-ne v0, v1, :cond_8

    .line 3390
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 3391
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 3392
    :cond_8
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getActivityType()I

    move-result v0

    const/16 v1, 0x4652

    if-ne v0, v1, :cond_9

    .line 3393
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 3394
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 3395
    :cond_9
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getActivityType()I

    move-result v0

    const/16 v1, 0x4654

    if-ne v0, v1, :cond_a

    .line 3396
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 3397
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 3398
    :cond_a
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getActivityType()I

    move-result v0

    const/16 v1, 0x4655

    if-ne v0, v1, :cond_0

    .line 3399
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 3400
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->isAvailableBarometer(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 3401
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 3403
    :cond_b
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 3408
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    const/16 v1, 0xe

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 3409
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 3410
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 3346
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private dispHRMData()V
    .locals 5

    .prologue
    const/4 v4, 0x5

    const/4 v3, 0x2

    .line 3420
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    if-nez v1, :cond_1

    .line 3433
    :cond_0
    :goto_0
    return-void

    .line 3423
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 3424
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 3425
    const/4 v1, 0x3

    if-lt v0, v1, :cond_0

    .line 3423
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 3430
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->saveRealtimeDispDataBackupTypeForHRM(I)V

    .line 3431
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 3432
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    invoke-static {v1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->saveRealtimeDispDataStatus(Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method private enableGoal(Z)V
    .locals 4
    .param p1, "enabled"    # Z

    .prologue
    const/4 v3, 0x1

    .line 3142
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mGoalTypeView:Landroid/view/View;

    const v2, 0x7f080813

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 3143
    .local v0, "tv":Landroid/widget/TextView;
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setSelected(Z)V

    .line 3144
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mGoalTypeValue:I

    const/4 v2, 0x5

    if-ne v1, v2, :cond_0

    if-eqz p1, :cond_1

    .line 3145
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mGoalTypeView:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setEnabled(Z)V

    .line 3149
    :goto_0
    return-void

    .line 3147
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mGoalTypeView:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0
.end method

.method private getDataText(IILcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$REALTIME_DATA;)Z
    .locals 22
    .param p1, "dispidx"    # I
    .param p2, "type"    # I
    .param p3, "data"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$REALTIME_DATA;

    .prologue
    .line 1783
    const/4 v13, 0x0

    .line 1784
    .local v13, "update":Z
    const-string v17, ""

    .local v17, "valueStr":Ljava/lang/String;
    const-string v12, ""

    .local v12, "unitStr":Ljava/lang/String;
    const-string v5, ""

    .local v5, "extraValueStr":Ljava/lang/String;
    const-string v4, ""

    .line 1785
    .local v4, "extraUnitStr":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    if-eqz v18, :cond_0

    if-nez p3, :cond_1

    :cond_0
    move v14, v13

    .line 1943
    .end local v13    # "update":Z
    .local v14, "update":I
    :goto_0
    return v14

    .line 1788
    .end local v14    # "update":I
    .restart local v13    # "update":Z
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/Integer;

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Integer;->intValue()I

    move-result v18

    const/16 v19, 0x7

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_8

    .line 1789
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeGoalType()I

    move-result v7

    .line 1790
    .local v7, "goalType":I
    invoke-static {v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->getDataTypeByGoalType(I)I

    move-result v3

    .line 1791
    .local v3, "dataType":I
    const/16 v18, 0x7

    move/from16 v0, p2

    move/from16 v1, v18

    if-ne v0, v1, :cond_2

    .line 1792
    const/4 v13, 0x1

    .line 1793
    const/16 v18, 0x11

    move/from16 v0, v18

    if-ne v3, v0, :cond_3

    .line 1794
    sget v18, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->diffdbduration:F

    move/from16 v0, v18

    float-to-long v0, v0

    move-wide/from16 v18, v0

    sput-wide v18, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->temp_db_value:J

    .line 1796
    sget-wide v18, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->temp_db_value:J

    invoke-static/range {v18 .. v19}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->checkPreviousRecordValue(J)Ljava/lang/String;

    move-result-object v17

    .line 1797
    const-string v12, ""

    .line 1940
    .end local v3    # "dataType":I
    .end local v7    # "goalType":I
    :cond_2
    :goto_1
    move-object/from16 v0, v17

    move-object/from16 v1, p3

    iput-object v0, v1, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$REALTIME_DATA;->value:Ljava/lang/String;

    .line 1941
    move-object/from16 v0, p3

    iput-object v12, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$REALTIME_DATA;->unit:Ljava/lang/String;

    .line 1942
    move-object/from16 v0, p3

    iput-object v5, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$REALTIME_DATA;->extraValue:Ljava/lang/String;

    move v14, v13

    .line 1943
    .restart local v14    # "update":I
    goto :goto_0

    .line 1799
    .end local v14    # "update":I
    .restart local v3    # "dataType":I
    .restart local v7    # "goalType":I
    :cond_3
    const/16 v18, -0x1

    move/from16 v0, v18

    if-ne v3, v0, :cond_4

    .line 1800
    const-string v17, ""

    .line 1801
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v18

    const v19, 0x7f090a02

    invoke-virtual/range {v18 .. v19}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v12

    goto :goto_1

    .line 1802
    :cond_4
    const/16 v18, 0x1

    move/from16 v0, v18

    if-ne v3, v0, :cond_5

    .line 1803
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getDataSet()Ljava/util/Map;

    move-result-object v18

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    invoke-interface/range {v18 .. v19}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/Long;

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Long;->longValue()J

    move-result-wide v18

    invoke-static/range {v18 .. v19}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->getRemainSecToGoal(J)J

    move-result-wide v15

    .line 1804
    .local v15, "val":J
    invoke-static/range {v15 .. v16}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkTimeValue(J)Ljava/lang/String;

    move-result-object v17

    .line 1805
    const-string v12, ""

    .line 1806
    goto :goto_1

    .end local v15    # "val":J
    :cond_5
    const/16 v18, 0x2

    move/from16 v0, v18

    if-ne v3, v0, :cond_7

    .line 1807
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->loadUnitsFromSharedPreferences()V

    .line 1808
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getDataSet()Ljava/util/Map;

    move-result-object v18

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    invoke-interface/range {v18 .. v19}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/Float;

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Float;->floatValue()F

    move-result v18

    move/from16 v0, v18

    invoke-static {v3, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->getRemainValueToGoal(IF)F

    move-result v15

    .line 1811
    .local v15, "val":F
    sget-object v18, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/CommonCache;->infoDistanceUnit:Ljava/lang/String;

    const-string v19, "km"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_6

    .line 1812
    float-to-long v0, v15

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    long-to-double v0, v0

    move-wide/from16 v18, v0

    invoke-static/range {v18 .. v19}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getMilesFromMetersWhileWorkingOut(D)Ljava/lang/String;

    move-result-object v17

    .line 1816
    :goto_2
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v18

    invoke-static {v15}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-static {v3, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkUnit(ILjava/lang/Float;)I

    move-result v19

    invoke-static/range {v18 .. v19}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getDataUnitByType(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v12

    .line 1819
    goto/16 :goto_1

    .line 1814
    :cond_6
    invoke-static {v15}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-static {v3, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkValue(ILjava/lang/Float;)Ljava/lang/String;

    move-result-object v17

    goto :goto_2

    .line 1820
    .end local v15    # "val":F
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getDataSet()Ljava/util/Map;

    move-result-object v18

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    invoke-interface/range {v18 .. v19}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/Float;

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Float;->floatValue()F

    move-result v18

    move/from16 v0, v18

    invoke-static {v3, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->getRemainValueToGoal(IF)F

    move-result v15

    .line 1822
    .restart local v15    # "val":F
    invoke-static {v15}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-static {v3, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkValue(ILjava/lang/Float;)Ljava/lang/String;

    move-result-object v17

    .line 1823
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v18

    invoke-static {v15}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-static {v3, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkUnit(ILjava/lang/Float;)I

    move-result v19

    invoke-static/range {v18 .. v19}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getDataUnitByType(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v12

    goto/16 :goto_1

    .line 1828
    .end local v3    # "dataType":I
    .end local v7    # "goalType":I
    .end local v15    # "val":F
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/Integer;

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Integer;->intValue()I

    move-result v18

    const/16 v19, 0xe

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_e

    .line 1829
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeGoalType()I

    move-result v7

    .line 1830
    .restart local v7    # "goalType":I
    invoke-static {v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->getDataTypeByGoalType(I)I

    move-result v3

    .line 1831
    .restart local v3    # "dataType":I
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeGoalValue()I

    move-result v8

    .line 1832
    .local v8, "goalValue":I
    const/16 v18, 0xe

    move/from16 v0, p2

    move/from16 v1, v18

    if-eq v0, v1, :cond_9

    const/16 v18, 0x18

    move/from16 v0, p2

    move/from16 v1, v18

    if-ne v0, v1, :cond_2

    .line 1838
    :cond_9
    const/4 v13, 0x1

    .line 1839
    const/16 v18, -0x1

    move/from16 v0, v18

    if-ne v3, v0, :cond_a

    .line 1840
    const-string v17, ""

    .line 1841
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v18

    const v19, 0x7f090a02

    invoke-virtual/range {v18 .. v19}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v12

    goto/16 :goto_1

    .line 1843
    :cond_a
    const/16 v18, 0x5

    move/from16 v0, v18

    if-ne v7, v0, :cond_c

    .line 1844
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeTrainingLevel()I

    move-result v10

    .line 1845
    .local v10, "level":I
    const/4 v11, 0x0

    .line 1846
    .local v11, "progressInPercentage":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getDataSet()Ljava/util/Map;

    move-result-object v18

    const/16 v19, 0x18

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    invoke-interface/range {v18 .. v19}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    if-eqz v18, :cond_b

    .line 1847
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getDataSet()Ljava/util/Map;

    move-result-object v18

    const/16 v19, 0x18

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    invoke-interface/range {v18 .. v19}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/Float;

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Float;->floatValue()F

    move-result v11

    .line 1850
    :cond_b
    const/16 v18, 0x18

    invoke-static {v11}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkValue(ILjava/lang/Float;)Ljava/lang/String;

    move-result-object v17

    .line 1851
    const-string v12, "%"

    .line 1852
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-static {v0, v10}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getEffectGoalStringByType(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v5

    .line 1853
    goto/16 :goto_1

    .end local v10    # "level":I
    .end local v11    # "progressInPercentage":F
    :cond_c
    const/16 v18, 0x1

    move/from16 v0, v18

    if-ne v3, v0, :cond_d

    .line 1854
    mul-int/lit8 v18, v8, 0x3c

    move/from16 v0, v18

    int-to-long v0, v0

    move-wide/from16 v18, v0

    invoke-static/range {v18 .. v19}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkTimeValue(J)Ljava/lang/String;

    move-result-object v17

    .line 1855
    const-string v12, ""

    goto/16 :goto_1

    .line 1857
    :cond_d
    int-to-float v0, v8

    move/from16 v18, v0

    invoke-static/range {v18 .. v18}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-static {v3, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkValue(ILjava/lang/Float;)Ljava/lang/String;

    move-result-object v17

    .line 1858
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v18

    int-to-float v0, v8

    move/from16 v19, v0

    invoke-static/range {v19 .. v19}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-static {v3, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkUnit(ILjava/lang/Float;)I

    move-result v19

    invoke-static/range {v18 .. v19}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getDataUnitByType(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v12

    goto/16 :goto_1

    .line 1863
    .end local v3    # "dataType":I
    .end local v7    # "goalType":I
    .end local v8    # "goalValue":I
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/Integer;

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Integer;->intValue()I

    move-result v18

    move/from16 v0, p2

    move/from16 v1, v18

    if-ne v0, v1, :cond_19

    .line 1864
    const/4 v13, 0x1

    .line 1865
    const/16 v18, 0x1

    move/from16 v0, p2

    move/from16 v1, v18

    if-ne v0, v1, :cond_f

    .line 1866
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getDataSet()Ljava/util/Map;

    move-result-object v18

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    invoke-interface/range {v18 .. v19}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/Long;

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Long;->longValue()J

    move-result-wide v18

    invoke-static/range {v18 .. v19}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkTimeValue(J)Ljava/lang/String;

    move-result-object v17

    .line 1867
    const-string v12, ""

    goto/16 :goto_1

    .line 1869
    :cond_f
    const/16 v18, 0x5

    move/from16 v0, p2

    move/from16 v1, v18

    if-ne v0, v1, :cond_12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getDataSet()Ljava/util/Map;

    move-result-object v18

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    invoke-interface/range {v18 .. v19}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/Float;

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Float;->floatValue()F

    move-result v18

    const/16 v19, 0x0

    cmpl-float v18, v18, v19

    if-nez v18, :cond_12

    .line 1870
    const-string v17, "--"

    .line 1910
    :cond_10
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    move-object/from16 v18, v0

    if-eqz v18, :cond_11

    .line 1911
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getDataSet()Ljava/util/Map;

    move-result-object v18

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/Float;

    move/from16 v0, p2

    move-object/from16 v1, v18

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkUnit(ILjava/lang/Float;)I

    move-result v18

    move-object/from16 v0, v19

    move/from16 v1, v18

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getDataUnitByType(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v12

    .line 1914
    :cond_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->big_value:Landroid/widget/TextView;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v18

    const-string v19, "\'"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v18

    if-eqz v18, :cond_2

    .line 1915
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->big_value:Landroid/widget/TextView;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->big_value:Landroid/widget/TextView;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v19

    const/16 v20, 0x13

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move/from16 v2, v20

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->convertToProperUnit(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 1871
    :cond_12
    const/16 v18, 0x6

    move/from16 v0, p2

    move/from16 v1, v18

    if-ne v0, v1, :cond_13

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getDataSet()Ljava/util/Map;

    move-result-object v18

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    invoke-interface/range {v18 .. v19}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/Float;

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Float;->floatValue()F

    move-result v18

    const/16 v19, 0x0

    cmpl-float v18, v18, v19

    if-nez v18, :cond_13

    .line 1872
    const-string v17, "0"

    goto/16 :goto_3

    .line 1873
    :cond_13
    const/16 v18, 0x12

    move/from16 v0, p2

    move/from16 v1, v18

    if-ne v0, v1, :cond_14

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getDataSet()Ljava/util/Map;

    move-result-object v18

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    invoke-interface/range {v18 .. v19}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/Float;

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Float;->floatValue()F

    move-result v18

    const/16 v19, 0x0

    cmpl-float v18, v18, v19

    if-nez v18, :cond_14

    .line 1874
    const-string v17, "0"

    goto/16 :goto_3

    .line 1876
    :cond_14
    const/16 v18, 0x13

    move/from16 v0, p2

    move/from16 v1, v18

    if-ne v0, v1, :cond_15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getDataSet()Ljava/util/Map;

    move-result-object v18

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    invoke-interface/range {v18 .. v19}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/Float;

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Float;->floatValue()F

    move-result v18

    const/16 v19, 0x0

    cmpl-float v18, v18, v19

    if-nez v18, :cond_15

    .line 1877
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    const v19, 0x7f090acd

    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v17

    goto/16 :goto_3

    .line 1879
    :cond_15
    const/16 v18, 0x17

    move/from16 v0, p2

    move/from16 v1, v18

    if-ne v0, v1, :cond_17

    .line 1880
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeGoalType()I

    move-result v7

    .line 1881
    .restart local v7    # "goalType":I
    const/16 v18, 0x5

    move/from16 v0, v18

    if-ne v7, v0, :cond_10

    .line 1882
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeTrainingLevel()I

    move-result v10

    .line 1883
    .restart local v10    # "level":I
    const-string v6, ""

    .line 1884
    .local v6, "goal":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getDataSet()Ljava/util/Map;

    move-result-object v18

    const/16 v19, 0x17

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    invoke-interface/range {v18 .. v19}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    if-eqz v18, :cond_16

    .line 1885
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-static {v0, v10}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getEffectGoalStringByType(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v6

    .line 1887
    :cond_16
    move-object/from16 v17, v6

    .line 1888
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mGoalValue:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v19

    const v20, 0x7f0900eb

    invoke-virtual/range {v19 .. v20}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_3

    .line 1897
    .end local v6    # "goal":Ljava/lang/String;
    .end local v7    # "goalType":I
    .end local v10    # "level":I
    :cond_17
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    move-object/from16 v18, v0

    if-eqz v18, :cond_10

    .line 1898
    const/16 v18, 0x2

    move/from16 v0, p2

    move/from16 v1, v18

    if-ne v0, v1, :cond_18

    .line 1900
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getDataSet()Ljava/util/Map;

    move-result-object v18

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    invoke-interface/range {v18 .. v19}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/Float;

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Float;->floatValue()F

    move-result v15

    .line 1901
    .restart local v15    # "val":F
    const/high16 v18, 0x41200000    # 10.0f

    div-float v18, v15, v18

    move/from16 v0, v18

    float-to-int v0, v0

    move/from16 v18, v0

    mul-int/lit8 v18, v18, 0xa

    move/from16 v0, v18

    int-to-float v9, v0

    .line 1902
    .local v9, "ivalue":F
    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v18

    move/from16 v0, p2

    move-object/from16 v1, v18

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkValue(ILjava/lang/Float;)Ljava/lang/String;

    move-result-object v17

    .line 1903
    goto/16 :goto_3

    .line 1905
    .end local v9    # "ivalue":F
    .end local v15    # "val":F
    :cond_18
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getDataSet()Ljava/util/Map;

    move-result-object v18

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    invoke-interface/range {v18 .. v19}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/Float;

    move/from16 v0, p2

    move-object/from16 v1, v18

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkValue(ILjava/lang/Float;)Ljava/lang/String;

    move-result-object v17

    goto/16 :goto_3

    .line 1919
    :cond_19
    const/16 v18, 0x3

    move/from16 v0, p2

    move/from16 v1, v18

    if-ne v0, v1, :cond_2

    .line 1920
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/Integer;

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Integer;->intValue()I

    move-result v18

    const/16 v19, 0x13

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_2

    .line 1921
    const/4 v13, 0x1

    .line 1922
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    move-object/from16 v18, v0

    if-eqz v18, :cond_1b

    .line 1923
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getDataSet()Ljava/util/Map;

    move-result-object v18

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    invoke-interface/range {v18 .. v19}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/Float;

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Float;->floatValue()F

    move-result v18

    const/16 v19, 0x0

    cmpg-float v18, v18, v19

    if-gtz v18, :cond_1a

    .line 1924
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    const v19, 0x7f090acd

    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 1929
    :goto_4
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v19

    const/16 v20, 0x13

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getDataSet()Ljava/util/Map;

    move-result-object v18

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/Float;

    move/from16 v0, v20

    move-object/from16 v1, v18

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkUnit(ILjava/lang/Float;)I

    move-result v18

    move-object/from16 v0, v19

    move/from16 v1, v18

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getDataUnitByType(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v12

    goto/16 :goto_1

    .line 1926
    :cond_1a
    const/16 v19, 0x13

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getDataSet()Ljava/util/Map;

    move-result-object v18

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/Float;

    move/from16 v0, v19

    move-object/from16 v1, v18

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkValue(ILjava/lang/Float;)Ljava/lang/String;

    move-result-object v17

    goto :goto_4

    .line 1934
    :cond_1b
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    const v19, 0x7f090acd

    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 1935
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v18

    const/16 v19, 0x13

    const/16 v20, 0x0

    invoke-static/range {v20 .. v20}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkUnit(ILjava/lang/Float;)I

    move-result v19

    invoke-static/range {v18 .. v19}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getDataUnitByType(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v12

    goto/16 :goto_1
.end method

.method private getMapMode()I
    .locals 9

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x1

    const/4 v8, -0x1

    .line 792
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v5

    const-string/jumbo v6, "mapMode"

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 793
    .local v2, "viewMode":Landroid/content/SharedPreferences;
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 794
    .local v0, "ed":Landroid/content/SharedPreferences$Editor;
    const-string/jumbo v5, "mapMode"

    invoke-interface {v2, v5, v8}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 795
    .local v1, "savedViewMode":I
    sget-object v5, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->TAG:Ljava/lang/String;

    const-string v6, "getMapMode()"

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 797
    if-ne v1, v8, :cond_1

    .line 798
    const-string/jumbo v4, "mapMode"

    invoke-interface {v0, v4, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 799
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move v1, v3

    .line 804
    .end local v1    # "savedViewMode":I
    :cond_0
    :goto_0
    return v1

    .line 801
    .restart local v1    # "savedViewMode":I
    :cond_1
    if-le v1, v4, :cond_0

    move v1, v4

    .line 802
    goto :goto_0
.end method

.method private getPaceValue(FZ)Ljava/lang/String;
    .locals 10
    .param p1, "time"    # F
    .param p2, "isAvg"    # Z

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 2940
    const-string v4, "--\'--\""

    .line 2941
    .local v4, "value":Ljava/lang/String;
    const/4 v5, 0x0

    cmpg-float v5, p1, v5

    if-gez v5, :cond_0

    move-object v5, v4

    .line 2950
    :goto_0
    return-object v5

    .line 2943
    :cond_0
    const v5, 0x45bb8000    # 6000.0f

    cmpl-float v5, p1, v5

    if-ltz v5, :cond_1

    .line 2944
    const-string v5, "--\'--\""

    goto :goto_0

    .line 2946
    :cond_1
    const-wide/16 v0, 0x3c

    .line 2947
    .local v0, "MINUTE":J
    const-string v5, "%d"

    new-array v6, v9, [Ljava/lang/Object;

    long-to-float v7, v0

    div-float v7, p1, v7

    float-to-int v7, v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2948
    .local v2, "minuteText":Ljava/lang/String;
    const-string v5, "%02d"

    new-array v6, v9, [Ljava/lang/Object;

    long-to-float v7, v0

    rem-float v7, p1, v7

    float-to-int v7, v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2949
    .local v3, "secondText":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v5, v4

    .line 2950
    goto :goto_0
.end method

.method private getXOffset(ILandroid/view/View;Lcom/sec/android/app/shealth/common/commonui/ListPopup;)I
    .locals 4
    .param p1, "alignment"    # I
    .param p2, "caller"    # Landroid/view/View;
    .param p3, "popup"    # Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    .prologue
    .line 2548
    const/4 v0, 0x0

    .line 2549
    .local v0, "xoffset":I
    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    :cond_0
    move v1, v0

    .line 2561
    .end local v0    # "xoffset":I
    .local v1, "xoffset":I
    :goto_0
    return v1

    .line 2551
    .end local v1    # "xoffset":I
    .restart local v0    # "xoffset":I
    :cond_1
    and-int/lit8 v2, p1, 0xf

    packed-switch v2, :pswitch_data_0

    .line 2559
    :pswitch_0
    invoke-virtual {p3}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->getWidth()I

    move-result v2

    invoke-virtual {p2}, Landroid/view/View;->getWidth()I

    move-result v3

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    rsub-int/lit8 v0, v2, 0x0

    :goto_1
    move v1, v0

    .line 2561
    .end local v0    # "xoffset":I
    .restart local v1    # "xoffset":I
    goto :goto_0

    .line 2553
    .end local v1    # "xoffset":I
    .restart local v0    # "xoffset":I
    :pswitch_1
    const/4 v0, 0x0

    .line 2554
    goto :goto_1

    .line 2556
    :pswitch_2
    invoke-virtual {p2}, Landroid/view/View;->getWidth()I

    move-result v0

    .line 2557
    goto :goto_1

    .line 2551
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private getXOffset(ILandroid/view/View;Lcom/sec/android/app/shealth/common/commonui/ListPopup;I)I
    .locals 4
    .param p1, "alignment"    # I
    .param p2, "caller"    # Landroid/view/View;
    .param p3, "popup"    # Lcom/sec/android/app/shealth/common/commonui/ListPopup;
    .param p4, "padding"    # I

    .prologue
    .line 2565
    const/4 v0, 0x0

    .line 2566
    .local v0, "xoffset":I
    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    :cond_0
    move v1, v0

    .line 2578
    .end local v0    # "xoffset":I
    .local v1, "xoffset":I
    :goto_0
    return v1

    .line 2568
    .end local v1    # "xoffset":I
    .restart local v0    # "xoffset":I
    :cond_1
    and-int/lit8 v2, p1, 0xf

    packed-switch v2, :pswitch_data_0

    .line 2576
    :pswitch_0
    invoke-virtual {p3}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->getWidth()I

    move-result v2

    invoke-virtual {p2}, Landroid/view/View;->getWidth()I

    move-result v3

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    div-int/lit8 v3, p4, 0x2

    add-int/2addr v2, v3

    rsub-int/lit8 v0, v2, 0x0

    :goto_1
    move v1, v0

    .line 2578
    .end local v0    # "xoffset":I
    .restart local v1    # "xoffset":I
    goto :goto_0

    .line 2570
    .end local v1    # "xoffset":I
    .restart local v0    # "xoffset":I
    :pswitch_1
    const/4 v0, 0x0

    .line 2571
    goto :goto_1

    .line 2573
    :pswitch_2
    invoke-virtual {p2}, Landroid/view/View;->getWidth()I

    move-result v0

    .line 2574
    goto :goto_1

    .line 2568
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private getYOffset(ILandroid/view/View;)I
    .locals 6
    .param p1, "alignment"    # I
    .param p2, "caller"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x1

    .line 2582
    const/4 v1, 0x0

    .line 2584
    .local v1, "yoffset":I
    if-nez p2, :cond_0

    move v2, v1

    .line 2603
    .end local v1    # "yoffset":I
    .local v2, "yoffset":I
    :goto_0
    return v2

    .line 2587
    .end local v2    # "yoffset":I
    .restart local v1    # "yoffset":I
    :cond_0
    const/high16 v3, 0xf0000

    and-int/2addr v3, p1

    packed-switch v3, :pswitch_data_0

    .line 2598
    const/4 v3, -0x2

    iput v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mHeightSpec:I

    .line 2599
    if-eqz p2, :cond_1

    .line 2600
    invoke-virtual {p2}, Landroid/view/View;->getHeight()I

    move-result v3

    rsub-int/lit8 v1, v3, 0x0

    :cond_1
    :goto_1
    move v2, v1

    .line 2603
    .end local v1    # "yoffset":I
    .restart local v2    # "yoffset":I
    goto :goto_0

    .line 2589
    .end local v2    # "yoffset":I
    .restart local v1    # "yoffset":I
    :pswitch_0
    const/4 v3, 0x2

    new-array v0, v3, [I

    .line 2590
    .local v0, "location":[I
    if-eqz p2, :cond_2

    .line 2591
    invoke-virtual {p2, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 2592
    aget v3, v0, v5

    rsub-int/lit8 v3, v3, 0x0

    invoke-virtual {p2}, Landroid/view/View;->getHeight()I

    move-result v4

    sub-int v1, v3, v4

    .line 2594
    :cond_2
    aget v3, v0, v5

    iput v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mHeight:I

    .line 2595
    const/4 v3, 0x0

    iput v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mHeightSpec:I

    goto :goto_1

    .line 2587
    nop

    :pswitch_data_0
    .packed-switch 0x10000
        :pswitch_0
    .end packed-switch
.end method

.method private hideHRMData()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 3436
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    if-nez v1, :cond_1

    .line 3444
    :cond_0
    :goto_0
    return-void

    .line 3439
    :cond_1
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeDispDataBackupTypeForHRM()I

    move-result v0

    .line 3440
    .local v0, "backupDispType":I
    if-ltz v0, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, 0x5

    if-ne v1, v2, :cond_0

    .line 3442
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 3443
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    invoke-static {v1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->saveRealtimeDispDataStatus(Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method private initCardViewLayout()V
    .locals 2

    .prologue
    .line 565
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mOperatelayout:Landroid/widget/RelativeLayout;

    const v1, 0x7f08078a

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCardView:Landroid/view/View;

    .line 567
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCardView:Landroid/view/View;

    const v1, 0x7f080725

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCardViewNoDataLayout:Landroid/widget/LinearLayout;

    .line 568
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCardView:Landroid/view/View;

    const v1, 0x7f080728

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCardViewDataLayout:Landroid/widget/LinearLayout;

    .line 570
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCardView:Landroid/view/View;

    const v1, 0x7f08072c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCardViewDistTxt:Landroid/widget/TextView;

    .line 572
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCardView:Landroid/view/View;

    const v1, 0x7f08072e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCardViewDurTxt:Landroid/widget/TextView;

    .line 573
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCardView:Landroid/view/View;

    const v1, 0x7f080730

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCardViewPaceTxt:Landroid/widget/TextView;

    .line 576
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCardView:Landroid/view/View;

    const v1, 0x7f080727

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCardNoLayoutText:Landroid/widget/TextView;

    .line 577
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCardView:Landroid/view/View;

    const v1, 0x7f080729

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCardViewGoalImg:Landroid/widget/ImageView;

    .line 579
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCardView:Landroid/view/View;

    const v1, 0x7f08072d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCardViewDetailTxt1:Landroid/widget/TextView;

    .line 580
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCardView:Landroid/view/View;

    const v1, 0x7f08072f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCardViewDetailTxt2:Landroid/widget/TextView;

    .line 581
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCardView:Landroid/view/View;

    const v1, 0x7f080731

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCardViewDetailTxt3:Landroid/widget/TextView;

    .line 583
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCardView:Landroid/view/View;

    const v1, 0x7f08072a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->cardViewDataTitleTextView:Landroid/widget/TextView;

    .line 584
    return-void
.end method

.method private initLayout()V
    .locals 15

    .prologue
    const v14, 0x7f09020a

    const v13, 0x7f080812

    const/4 v12, 0x0

    const v10, 0x7f080786

    const v11, 0x7f09020b

    .line 395
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mView:Landroid/view/View;

    const v8, 0x7f08071f

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mDatas_layout:Landroid/view/View;

    .line 396
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mView:Landroid/view/View;

    const v8, 0x7f08071a

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/LinearLayout;

    iput-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCoachStatusLayout:Landroid/widget/LinearLayout;

    .line 397
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mView:Landroid/view/View;

    const v8, 0x7f08071d

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/RelativeLayout;

    iput-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mOperatelayout:Landroid/widget/RelativeLayout;

    .line 400
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mOperatelayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v7, v10}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    const v8, 0x7f08082f

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivityTextRunning:Landroid/widget/TextView;

    .line 401
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mOperatelayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v7, v10}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    const v8, 0x7f080831

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivityTextWaliking:Landroid/widget/TextView;

    .line 402
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mOperatelayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v7, v10}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    const v8, 0x7f080833

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivityTextCycling:Landroid/widget/TextView;

    .line 403
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mOperatelayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v7, v10}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    const v8, 0x7f080835

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivityTextHiking:Landroid/widget/TextView;

    .line 405
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mView:Landroid/view/View;

    const v8, 0x7f08071c

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/LinearLayout;

    iput-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mKcalSummaryLayout:Landroid/widget/LinearLayout;

    .line 406
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mKcalSummaryLayout:Landroid/widget/LinearLayout;

    const v8, 0x7f0802a6

    invoke-virtual {v7, v8}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;

    iput-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCircleProgress:Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;

    .line 407
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCircleProgress:Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;

    const/16 v8, 0x64

    invoke-virtual {v7, v8}, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->setMax(I)V

    .line 408
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mView:Landroid/view/View;

    const v8, 0x7f080775

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    iput-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mGoalAchived:Landroid/widget/ImageView;

    .line 409
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mView:Landroid/view/View;

    const v8, 0x7f08071e

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/RelativeLayout;

    iput-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mStatusLayout:Landroid/widget/RelativeLayout;

    .line 410
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mView:Landroid/view/View;

    const v8, 0x7f080720

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/LinearLayout;

    iput-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mMusicPlayerLayout:Landroid/widget/LinearLayout;

    .line 412
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dataTextViewIDs:[I

    array-length v7, v7

    if-ge v1, v7, :cond_0

    .line 413
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mContentView:[Landroid/view/View;

    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mView:Landroid/view/View;

    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dataTextViewIDs:[I

    aget v9, v9, v1

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    aput-object v8, v7, v1

    .line 414
    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mScrollTextViews:[Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;

    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mContentView:[Landroid/view/View;

    aget-object v7, v7, v1

    const v9, 0x7f0806e1

    invoke-virtual {v7, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;

    aput-object v7, v8, v1

    .line 415
    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mDataIcon:[Landroid/widget/ImageView;

    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mContentView:[Landroid/view/View;

    aget-object v7, v7, v1

    const v9, 0x7f0806df

    invoke-virtual {v7, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    aput-object v7, v8, v1

    .line 412
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 418
    :cond_0
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->isLockMode()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 419
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mMusicPlayerLayout:Landroid/widget/LinearLayout;

    const v8, 0x7f08077e

    invoke-virtual {v7, v8}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/view/View;->setVisibility(I)V

    .line 422
    :cond_1
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v7}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/Window;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v7

    invoke-interface {v7}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/Display;->getHeight()I

    move-result v5

    .line 423
    .local v5, "screenHeight":I
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v7}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/Window;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v7

    invoke-interface {v7}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/Display;->getWidth()I

    move-result v6

    .line 424
    .local v6, "screenWidth":I
    if-le v6, v5, :cond_2

    .line 425
    const/4 v7, 0x1

    iput-boolean v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mLandscape:Z

    .line 427
    :cond_2
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mView:Landroid/view/View;

    const v8, 0x7f080704

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mDropDownList:Landroid/view/View;

    .line 428
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mView:Landroid/view/View;

    const v8, 0x7f080709

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->big_value:Landroid/widget/TextView;

    .line 429
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mView:Landroid/view/View;

    const v8, 0x7f08070b

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->big_extra_value:Landroid/widget/TextView;

    .line 430
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mView:Landroid/view/View;

    const v8, 0x7f080707

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/RelativeLayout;

    iput-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->big_layout:Landroid/widget/RelativeLayout;

    .line 432
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mView:Landroid/view/View;

    const v8, 0x7f080787

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mIndoorOrOutdoor:Landroid/view/View;

    .line 433
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mIndoorOrOutdoor:Landroid/view/View;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/view/View;->setVisibility(I)V

    .line 434
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mIndoorOrOutdoor:Landroid/view/View;

    invoke-virtual {v7, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 436
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mView:Landroid/view/View;

    invoke-virtual {v7, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivityType:Landroid/view/View;

    .line 437
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivityType:Landroid/view/View;

    const v8, 0x7f08082e

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->runningView:Landroid/view/View;

    .line 438
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->runningView:Landroid/view/View;

    const/16 v8, 0x4653

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 439
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivityType:Landroid/view/View;

    const v8, 0x7f080830

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->walkingView:Landroid/view/View;

    .line 440
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->walkingView:Landroid/view/View;

    const/16 v8, 0x4652

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 441
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivityType:Landroid/view/View;

    const v8, 0x7f080832

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->cyclingView:Landroid/view/View;

    .line 442
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->cyclingView:Landroid/view/View;

    const/16 v8, 0x4654

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 443
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivityType:Landroid/view/View;

    const v8, 0x7f080834

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->hikingView:Landroid/view/View;

    .line 444
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->hikingView:Landroid/view/View;

    const/16 v8, 0x4655

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 446
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->runningView:Landroid/view/View;

    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivityTypeClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v7, v8}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 447
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->walkingView:Landroid/view/View;

    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivityTypeClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v7, v8}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 448
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->cyclingView:Landroid/view/View;

    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivityTypeClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v7, v8}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 449
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->hikingView:Landroid/view/View;

    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivityTypeClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v7, v8}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 453
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->runningView:Landroid/view/View;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0909d3

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f090867

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 457
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v7

    iget-object v7, v7, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v7}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    .line 458
    .local v3, "lang":Ljava/lang/String;
    const-string v0, "de"

    .line 459
    .local v0, "german":Ljava/lang/String;
    const-string v7, "de"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 460
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->walkingView:Landroid/view/View;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f090785

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f090868

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 470
    :goto_1
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->cyclingView:Landroid/view/View;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0909d0

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f090869

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 474
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->hikingView:Landroid/view/View;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0909d1

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f09086a

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 479
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mView:Landroid/view/View;

    const v8, 0x7f080789

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mGoalTypeView:Landroid/view/View;

    .line 480
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mGoalTypeView:Landroid/view/View;

    const v8, 0x7f080816

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    iput-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->firstbubbleView:Landroid/widget/ImageView;

    .line 481
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mGoalTypeView:Landroid/view/View;

    const v8, 0x7f080817

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    iput-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->secondbubbleView:Landroid/widget/ImageView;

    .line 482
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mGoalTypeView:Landroid/view/View;

    const v8, 0x7f080818

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    iput-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->thirdbubbleView:Landroid/widget/ImageView;

    .line 483
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mGoalTypeView:Landroid/view/View;

    const v8, 0x7f08081a

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    iput-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->fourthbubbleView:Landroid/widget/ImageView;

    .line 486
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mGoalTypeView:Landroid/view/View;

    new-instance v8, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$2;

    invoke-direct {v8, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$2;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)V

    invoke-virtual {v7, v8}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 499
    const-string v7, "de"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 500
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mGoalTypeView:Landroid/view/View;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f090786

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9, v14}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 515
    :goto_2
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mGoalTypeView:Landroid/view/View;

    invoke-virtual {v7, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 516
    .local v2, "iv":Landroid/widget/ImageView;
    const v7, 0x7f02027b

    invoke-static {v2, v7}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->setImageResource(Landroid/widget/ImageView;I)V

    .line 519
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mView:Landroid/view/View;

    const v8, 0x7f08078b

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mMusicState:Landroid/view/View;

    .line 520
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mMusicState:Landroid/view/View;

    invoke-virtual {v7, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 521
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mMusicState:Landroid/view/View;

    invoke-virtual {v7, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .end local v2    # "iv":Landroid/widget/ImageView;
    check-cast v2, Landroid/widget/ImageView;

    .line 522
    .restart local v2    # "iv":Landroid/widget/ImageView;
    const v7, 0x7f020400

    invoke-static {v2, v7}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->setImageResource(Landroid/widget/ImageView;I)V

    .line 523
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mView:Landroid/view/View;

    const v8, 0x7f08078b

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 524
    .local v4, "musicPlayer":Landroid/view/View;
    new-instance v7, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$3;

    invoke-direct {v7, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$3;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)V

    invoke-virtual {v4, v7}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 541
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0901ee

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8, v14}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 547
    invoke-virtual {v4, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .end local v2    # "iv":Landroid/widget/ImageView;
    check-cast v2, Landroid/widget/ImageView;

    .line 548
    .restart local v2    # "iv":Landroid/widget/ImageView;
    const v7, 0x7f020189

    invoke-static {v2, v7}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->setImageResource(Landroid/widget/ImageView;I)V

    .line 550
    const-string v7, ""

    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->updateMusicPlayerText(Ljava/lang/String;)V

    .line 551
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->initMusicControllLayout()V

    .line 552
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->initCardViewLayout()V

    .line 555
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivityTextRunning:Landroid/widget/TextView;

    const-string/jumbo v8, "sec-roboto-light"

    invoke-static {v8, v12}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 556
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivityTextWaliking:Landroid/widget/TextView;

    const-string/jumbo v8, "sec-roboto-light"

    invoke-static {v8, v12}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 557
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivityTextCycling:Landroid/widget/TextView;

    const-string/jumbo v8, "sec-roboto-light"

    invoke-static {v8, v12}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 558
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivityTextHiking:Landroid/widget/TextView;

    const-string/jumbo v8, "sec-roboto-light"

    invoke-static {v8, v12}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 560
    return-void

    .line 464
    .end local v2    # "iv":Landroid/widget/ImageView;
    .end local v4    # "musicPlayer":Landroid/view/View;
    :cond_3
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->walkingView:Landroid/view/View;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0909d2

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f090868

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 506
    :cond_4
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mGoalTypeView:Landroid/view/View;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f090ab0

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9, v14}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_2
.end method

.method private initMusicControllLayout()V
    .locals 4

    .prologue
    const v3, 0x7f090a9b

    const/4 v2, 0x1

    .line 650
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mView:Landroid/view/View;

    const v1, 0x7f08077b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mMusicController:Landroid/view/View;

    .line 651
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mView:Landroid/view/View;

    const v1, 0x7f080780

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mBtnMusic_paly:Landroid/widget/ImageButton;

    .line 652
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mView:Landroid/view/View;

    const v1, 0x7f08077f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mBtnMusic_previous:Landroid/widget/ImageButton;

    .line 653
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mView:Landroid/view/View;

    const v1, 0x7f080781

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mBtnMusic_next:Landroid/widget/ImageButton;

    .line 654
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mView:Landroid/view/View;

    const v1, 0x7f080783

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->musicTitleTextView:Landroid/widget/TextView;

    .line 655
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mView:Landroid/view/View;

    const v1, 0x7f080784

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->musicSubTextView:Landroid/widget/TextView;

    .line 656
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mView:Landroid/view/View;

    const v1, 0x7f080785

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->musicDummyView:Landroid/view/View;

    .line 657
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->musicDummyView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 658
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->musicTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setSelected(Z)V

    .line 659
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->musicSubTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setSelected(Z)V

    .line 660
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->musicTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 661
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->musicSubTextView:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 662
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mBtnMusic_paly:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mMusicButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 663
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mBtnMusic_previous:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mMusicButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 664
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mBtnMusic_next:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mMusicButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 665
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mBtnMusic_paly:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 666
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mBtnMusic_previous:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 667
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mBtnMusic_next:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 668
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mView:Landroid/view/View;

    const v1, 0x7f08077c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mAccuWeather:Landroid/view/View;

    .line 669
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mView:Landroid/view/View;

    const v1, 0x7f08077d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mTvAccuWeather:Landroid/widget/TextView;

    .line 671
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->isLockMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 672
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->musicTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 673
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mMusicPlayerLayout:Landroid/widget/LinearLayout;

    const v1, 0x7f080782

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 679
    :cond_0
    sget-boolean v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->isMusicPlaying:Z

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->play_setContentDescription(Z)V

    .line 683
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getPserviceType()I

    move-result v0

    sget v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->PSERVICE_ACCUWEATHER:I

    if-ne v0, v1, :cond_1

    .line 689
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mTvAccuWeather:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mAccWeatherButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 690
    return-void

    .line 686
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mAccuWeather:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private isVikalDevice()Z
    .locals 2

    .prologue
    .line 3988
    const-string v0, "SM-G9092"

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "SM-G9098"

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private lapClockDispUpdate(J)V
    .locals 2
    .param p1, "second"    # J

    .prologue
    .line 3160
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivity:Landroid/app/Activity;

    if-nez v0, :cond_0

    .line 3169
    :goto_0
    return-void

    .line 3163
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivity:Landroid/app/Activity;

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$12;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$12;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private loadConfigure()V
    .locals 5

    .prologue
    const/4 v4, 0x5

    .line 1054
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeGoalType()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mGoalTypeValue:I

    .line 1056
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeDispDataStatus()Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    .line 1057
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->checkSupportedHRM()Z

    move-result v2

    if-nez v2, :cond_2

    .line 1058
    const/4 v0, 0x0

    .local v0, "index":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 1059
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v2, v4, :cond_1

    .line 1060
    const/4 v1, 0x1

    .local v1, "type":I
    :goto_1
    if-ge v1, v4, :cond_1

    .line 1061
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1062
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1060
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1058
    .end local v1    # "type":I
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1068
    .end local v0    # "index":I
    :cond_2
    return-void
.end method

.method private playMap(Landroid/location/Location;I)V
    .locals 5
    .param p1, "newLocation"    # Landroid/location/Location;
    .param p2, "mode"    # I

    .prologue
    .line 810
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mMap:Lcom/amap/api/maps2d/AMap;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    if-nez v1, :cond_1

    .line 819
    :cond_0
    :goto_0
    return-void

    .line 812
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mDatas_layout:Landroid/view/View;

    const v2, 0x7f080701

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 813
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "playMap: lat="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", lng="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 815
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getWorkoutState()I

    move-result v0

    .line 816
    .local v0, "state":I
    const/16 v1, 0x7d1

    if-ne v0, v1, :cond_0

    .line 817
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->playMapFromCacheDB()V

    goto :goto_0
.end method

.method private playMapFromCacheDB()V
    .locals 23

    .prologue
    .line 822
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getWorkoutState()I

    move-result v17

    .line 823
    .local v17, "state":I
    const/16 v4, 0x7d0

    move/from16 v0, v17

    if-eq v0, v4, :cond_1

    .line 825
    const-string v9, "create_time ASC"

    .line 826
    .local v9, "sortOder":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "exercise__id = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeExerciseId()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " AND "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "create_time"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ">"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-wide v5, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mLastQueryTime:J

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 828
    .local v7, "selection":Ljava/lang/String;
    const/4 v12, 0x0

    .line 830
    .local v12, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/samsung/android/sdk/health/content/ShealthContract$LocationData;->CONTENT_URI:Landroid/net/Uri;

    const/4 v6, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 832
    invoke-static {v12}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getMapPathDB(Landroid/database/Cursor;)Ljava/util/ArrayList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v10

    .line 834
    .local v10, "cacheLocationItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;>;"
    if-eqz v12, :cond_0

    .line 835
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 837
    :cond_0
    if-nez v10, :cond_3

    .line 838
    sget-object v4, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "playMapFromCacheDB, No Data!!"

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 882
    .end local v7    # "selection":Ljava/lang/String;
    .end local v9    # "sortOder":Ljava/lang/String;
    .end local v10    # "cacheLocationItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;>;"
    .end local v12    # "cursor":Landroid/database/Cursor;
    :cond_1
    :goto_0
    return-void

    .line 834
    .restart local v7    # "selection":Ljava/lang/String;
    .restart local v9    # "sortOder":Ljava/lang/String;
    .restart local v12    # "cursor":Landroid/database/Cursor;
    :catchall_0
    move-exception v4

    if-eqz v12, :cond_2

    .line 835
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v4

    .line 842
    .restart local v10    # "cacheLocationItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;>;"
    :cond_3
    sget-object v4, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "playMapFromCacheDB cacheLocationItems.size : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 843
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v5, 0x1

    if-le v4, v5, :cond_6

    .line 844
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mMap:Lcom/amap/api/maps2d/AMap;

    invoke-virtual {v4}, Lcom/amap/api/maps2d/AMap;->clear()V

    .line 845
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->setMyLocationStyle()V

    .line 847
    const/4 v15, 0x0

    .local v15, "i":I
    :goto_1
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ge v15, v4, :cond_5

    .line 848
    invoke-virtual {v10, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;

    .line 849
    .local v16, "startPath":Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;
    add-int/lit8 v4, v15, 0x1

    invoke-virtual {v10, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;

    .line 850
    .local v14, "endPath":Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;
    add-int/lit8 v4, v15, 0x1

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    if-ne v4, v5, :cond_4

    .line 851
    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->getCreateTime()J

    move-result-wide v4

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mLastQueryTime:J

    .line 853
    :cond_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mPolyLineOption:Lcom/amap/api/maps2d/model/PolylineOptions;

    const/4 v5, 0x2

    new-array v5, v5, [Lcom/amap/api/maps2d/model/LatLng;

    const/4 v6, 0x0

    new-instance v8, Lcom/amap/api/maps2d/model/LatLng;

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->getLatitude()D

    move-result-wide v19

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->getLongitude()D

    move-result-wide v21

    move-wide/from16 v0, v19

    move-wide/from16 v2, v21

    invoke-direct {v8, v0, v1, v2, v3}, Lcom/amap/api/maps2d/model/LatLng;-><init>(DD)V

    aput-object v8, v5, v6

    const/4 v6, 0x1

    new-instance v8, Lcom/amap/api/maps2d/model/LatLng;

    invoke-virtual {v14}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->getLatitude()D

    move-result-wide v19

    invoke-virtual {v14}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->getLongitude()D

    move-result-wide v21

    move-wide/from16 v0, v19

    move-wide/from16 v2, v21

    invoke-direct {v8, v0, v1, v2, v3}, Lcom/amap/api/maps2d/model/LatLng;-><init>(DD)V

    aput-object v8, v5, v6

    invoke-virtual {v4, v5}, Lcom/amap/api/maps2d/model/PolylineOptions;->add([Lcom/amap/api/maps2d/model/LatLng;)Lcom/amap/api/maps2d/model/PolylineOptions;

    .line 847
    add-int/lit8 v15, v15, 0x1

    goto :goto_1

    .line 856
    .end local v14    # "endPath":Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;
    .end local v16    # "startPath":Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;
    :cond_5
    sget-object v4, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "playMapFromCacheDB PolylineOptions size = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", mPolyIndex : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mPolyIndex:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 858
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mPolyLineOption:Lcom/amap/api/maps2d/model/PolylineOptions;

    sget v5, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->POLYLINE_DEFAULT_COLOR:I

    invoke-virtual {v4, v5}, Lcom/amap/api/maps2d/model/PolylineOptions;->color(I)Lcom/amap/api/maps2d/model/PolylineOptions;

    .line 859
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mPolyLineOption:Lcom/amap/api/maps2d/model/PolylineOptions;

    sget v5, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->POLYLINE_DEFAULT_WIDTH:I

    int-to-float v5, v5

    invoke-virtual {v4, v5}, Lcom/amap/api/maps2d/model/PolylineOptions;->width(F)Lcom/amap/api/maps2d/model/PolylineOptions;

    .line 860
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mMap:Lcom/amap/api/maps2d/AMap;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mPolyLineOption:Lcom/amap/api/maps2d/model/PolylineOptions;

    invoke-virtual {v4, v5}, Lcom/amap/api/maps2d/AMap;->addPolyline(Lcom/amap/api/maps2d/model/PolylineOptions;)Lcom/amap/api/maps2d/model/Polyline;

    .line 863
    .end local v15    # "i":I
    :cond_6
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_1

    .line 864
    new-instance v18, Landroid/location/Location;

    const-string v4, "TEMPLOC"

    move-object/from16 v0, v18

    invoke-direct {v0, v4}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 865
    .local v18, "tmpLocation":Landroid/location/Location;
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v10, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->getLatitude()D

    move-result-wide v4

    move-object/from16 v0, v18

    invoke-virtual {v0, v4, v5}, Landroid/location/Location;->setLatitude(D)V

    .line 866
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v10, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->getLongitude()D

    move-result-wide v4

    move-object/from16 v0, v18

    invoke-virtual {v0, v4, v5}, Landroid/location/Location;->setLongitude(D)V

    .line 868
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v10, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->getCreateTime()J

    move-result-wide v4

    move-object/from16 v0, v18

    invoke-virtual {v0, v4, v5}, Landroid/location/Location;->setTime(J)V

    .line 869
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v10, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->getAccuracy()F

    move-result v4

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Landroid/location/Location;->setAccuracy(F)V

    .line 870
    const/4 v4, 0x0

    sput-boolean v4, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->isRestart:Z

    .line 872
    new-instance v11, Lcom/amap/api/maps2d/model/LatLng;

    invoke-virtual/range {v18 .. v18}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    invoke-virtual/range {v18 .. v18}, Landroid/location/Location;->getLongitude()D

    move-result-wide v19

    move-wide/from16 v0, v19

    invoke-direct {v11, v4, v5, v0, v1}, Lcom/amap/api/maps2d/model/LatLng;-><init>(DD)V

    .line 874
    .local v11, "currentLatLng":Lcom/amap/api/maps2d/model/LatLng;
    :try_start_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mMap:Lcom/amap/api/maps2d/AMap;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mZoomLevel:F

    invoke-static {v11, v5}, Lcom/amap/api/maps2d/CameraUpdateFactory;->newLatLngZoom(Lcom/amap/api/maps2d/model/LatLng;F)Lcom/amap/api/maps2d/CameraUpdate;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/amap/api/maps2d/AMap;->animateCamera(Lcom/amap/api/maps2d/CameraUpdate;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 876
    :catch_0
    move-exception v13

    .line 877
    .local v13, "e":Ljava/lang/Exception;
    sget-object v4, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->TAG:Ljava/lang/String;

    const-string v5, "animateCamera issue"

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 878
    invoke-virtual {v13}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0
.end method

.method private reSizeDataLayout()V
    .locals 8

    .prologue
    const v7, 0x7f0a051b

    const v6, 0x7f080703

    const/high16 v5, 0x423c0000    # 47.0f

    const/high16 v4, 0x41880000    # 17.0f

    .line 1646
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->isLockMode()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1647
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->musicDummyView:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1652
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getIsVisualGuideShown()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1653
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mView:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1654
    .local v1, "view":Landroid/view/View;
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1655
    .local v0, "params2":Landroid/widget/LinearLayout$LayoutParams;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v5}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->convertDpToPx(Landroid/content/Context;F)F

    move-result v2

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    add-float/2addr v2, v3

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->convertDpToPx(Landroid/content/Context;F)F

    move-result v3

    add-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 1658
    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1673
    :goto_1
    return-void

    .line 1649
    .end local v0    # "params2":Landroid/widget/LinearLayout$LayoutParams;
    .end local v1    # "view":Landroid/view/View;
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->musicDummyView:Landroid/view/View;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 1660
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mView:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1661
    .restart local v1    # "view":Landroid/view/View;
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1666
    .restart local v0    # "params2":Landroid/widget/LinearLayout$LayoutParams;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v5}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->convertDpToPx(Landroid/content/Context;F)F

    move-result v2

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    add-float/2addr v2, v3

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->convertDpToPx(Landroid/content/Context;F)F

    move-result v3

    add-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 1669
    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_1
.end method

.method private registerReceiver()V
    .locals 4

    .prologue
    .line 3623
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 3626
    .local v1, "itentFilter":Landroid/content/IntentFilter;
    const-string v2, "com.android.music.playstatechanged"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 3627
    const-string v2, "com.android.music.metachanged"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 3628
    const-string v2, "com.sec.android.music.musicservicecommnad.mediainfo"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 3630
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 3631
    .local v0, "clearFilter":Landroid/content/IntentFilter;
    const-string v2, "android.intent.action.PACKAGE_DATA_CLEARED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 3632
    const-string/jumbo v2, "package"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 3634
    new-instance v2, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$MediainfoReceiver;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$MediainfoReceiver;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mMediaInfoReceiver:Landroid/content/BroadcastReceiver;

    .line 3635
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mMediaInfoReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 3636
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mMediaInfoReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 3637
    return-void
.end method

.method private saveInfoAndUpdateViewGrid(Ljava/lang/String;J)V
    .locals 5
    .param p1, "picturePath"    # Ljava/lang/String;
    .param p2, "exerciseId"    # J

    .prologue
    .line 3288
    new-instance v1, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;

    invoke-direct {v1, p1}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;-><init>(Ljava/lang/String;)V

    .line 3289
    .local v1, "photoData":Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mLastLocation:Landroid/location/Location;

    if-eqz v2, :cond_1

    .line 3290
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mLastLocation:Landroid/location/Location;

    invoke-virtual {v2}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    double-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mLastLocation:Landroid/location/Location;

    invoke-virtual {v3}, Landroid/location/Location;->getLongitude()D

    move-result-wide v3

    double-to-float v3, v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->setLocate(FF)V

    .line 3298
    :cond_0
    :goto_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v1, v3}, Lcom/sec/android/app/shealth/plugins/common/utils/ExercisePhotoUtils;->insertTempitem(Landroid/content/Context;Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;Z)J

    .line 3299
    return-void

    .line 3293
    :cond_1
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->getFilePath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/common/utils/ExercisePhotoUtils;->getLocateMetaInfo(Ljava/lang/String;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    move-result-object v0

    .line 3294
    .local v0, "location":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;
    if-eqz v0, :cond_0

    .line 3295
    iget-wide v2, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->latitude:D

    double-to-float v2, v2

    iget-wide v3, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->longitude:D

    double-to-float v3, v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->setLocate(FF)V

    goto :goto_0
.end method

.method private setBigTextSize()V
    .locals 3

    .prologue
    .line 1976
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 1977
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v1, 0xe

    if-ne v0, v1, :cond_0

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeGoalType()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 1979
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->big_value:Landroid/widget/TextView;

    const/4 v1, 0x1

    const/high16 v2, 0x420c0000    # 35.0f

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1982
    :cond_0
    return-void
.end method

.method private setDefaultBackground()V
    .locals 2

    .prologue
    .line 3722
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivity:Landroid/app/Activity;

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$14;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$14;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 3751
    return-void
.end method

.method private setDropDownContentDescription(Landroid/view/View;I)V
    .locals 8
    .param p1, "view"    # Landroid/view/View;
    .param p2, "type"    # I

    .prologue
    const v7, 0x7f09020b

    .line 3791
    if-eqz p1, :cond_0

    .line 3792
    const v4, 0x7f0806e1

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;

    .line 3793
    .local v2, "tvScroll":Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;
    const v4, 0x7f0806e4

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 3794
    .local v0, "dataValue":Landroid/widget/TextView;
    const v4, 0x7f0806de

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 3795
    .local v1, "dropDown":Landroid/view/View;
    const-string v3, " "

    .line 3796
    .local v3, "unitText":Ljava/lang/String;
    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 3797
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4, p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->convertToProperUnit(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    .line 3798
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0901ec

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, v3}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->convertToProperUnitsText(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 3807
    .end local v0    # "dataValue":Landroid/widget/TextView;
    .end local v1    # "dropDown":Landroid/view/View;
    .end local v2    # "tvScroll":Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;
    .end local v3    # "unitText":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method private setGoalValue(III)V
    .locals 3
    .param p1, "type"    # I
    .param p2, "value"    # I
    .param p3, "level"    # I

    .prologue
    .line 3495
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeGoalType()I

    move-result v0

    .line 3496
    .local v0, "oldType":I
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeGoalValue()I

    move-result v1

    .line 3497
    .local v1, "oldValue":I
    const/4 v2, 0x5

    if-ne p1, v2, :cond_0

    .line 3498
    invoke-static {p3}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->saveRealtimeTrainingLevel(I)V

    .line 3499
    if-ltz p3, :cond_0

    .line 3500
    iput p3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mTEenginlevel:I

    .line 3502
    :cond_0
    if-ne p1, v0, :cond_1

    if-ne p2, v1, :cond_1

    .line 3508
    :goto_0
    return-void

    .line 3504
    :cond_1
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mGoalTypeValue:I

    .line 3505
    iput p2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mGoalValue:I

    .line 3506
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->saveBackupRealtimeGoalType(II)V

    .line 3507
    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->saveRealtimeGoalType(II)V

    goto :goto_0
.end method

.method private setMyLocationStyle()V
    .locals 3

    .prologue
    .line 3976
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$AMapLocationSource;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$AMapLocationSource;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$1;)V

    .line 3977
    .local v0, "mAMapLocationSource":Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$AMapLocationSource;
    new-instance v1, Lcom/amap/api/maps2d/model/MyLocationStyle;

    invoke-direct {v1}, Lcom/amap/api/maps2d/model/MyLocationStyle;-><init>()V

    .line 3979
    .local v1, "mAMapLocationStyle":Lcom/amap/api/maps2d/model/MyLocationStyle;
    const v2, 0x7f0203f1

    invoke-static {v2}, Lcom/amap/api/maps2d/model/BitmapDescriptorFactory;->fromResource(I)Lcom/amap/api/maps2d/model/BitmapDescriptor;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/amap/api/maps2d/model/MyLocationStyle;->myLocationIcon(Lcom/amap/api/maps2d/model/BitmapDescriptor;)Lcom/amap/api/maps2d/model/MyLocationStyle;

    .line 3981
    const/high16 v2, -0x1000000

    invoke-virtual {v1, v2}, Lcom/amap/api/maps2d/model/MyLocationStyle;->strokeColor(I)Lcom/amap/api/maps2d/model/MyLocationStyle;

    .line 3982
    const v2, 0x3dcccccd    # 0.1f

    invoke-virtual {v1, v2}, Lcom/amap/api/maps2d/model/MyLocationStyle;->strokeWidth(F)Lcom/amap/api/maps2d/model/MyLocationStyle;

    .line 3983
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mMap:Lcom/amap/api/maps2d/AMap;

    invoke-virtual {v2, v1}, Lcom/amap/api/maps2d/AMap;->setMyLocationStyle(Lcom/amap/api/maps2d/model/MyLocationStyle;)V

    .line 3984
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mMap:Lcom/amap/api/maps2d/AMap;

    invoke-virtual {v2, v0}, Lcom/amap/api/maps2d/AMap;->setLocationSource(Lcom/amap/api/maps2d/LocationSource;)V

    .line 3985
    return-void
.end method

.method private setRealtimeDataFromStatic(J)V
    .locals 6
    .param p1, "ExerciseId"    # J

    .prologue
    const/4 v5, 0x0

    const-wide/16 v3, 0x0

    .line 2954
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2, p1, p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getWorkoutStatics(Landroid/content/Context;ZJ)Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils$WorkoutStastics;

    move-result-object v0

    .line 2955
    .local v0, "stastics":Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils$WorkoutStastics;
    iget v1, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils$WorkoutStastics;->avgSpeed:F

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mAvgSpeed:F

    .line 2956
    iget-wide v1, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils$WorkoutStastics;->maxAltitude:D

    iput-wide v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mMaxElevation:D

    .line 2957
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mAvgSpeed:F

    cmpg-float v1, v1, v5

    if-gez v1, :cond_0

    .line 2958
    iput v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mAvgSpeed:F

    .line 2959
    :cond_0
    iget-wide v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mMaxElevation:D

    cmpg-double v1, v1, v3

    if-gez v1, :cond_1

    .line 2960
    iput-wide v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mMaxElevation:D

    .line 2961
    :cond_1
    return-void
.end method

.method private setUpMap()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 745
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeMapZoomLevel()F

    move-result v4

    iput v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mZoomLevel:F

    .line 746
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getMapMode()I

    move-result v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mMap:Lcom/amap/api/maps2d/AMap;

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->setAMapType(ILcom/amap/api/maps2d/AMap;)Lcom/amap/api/maps2d/AMap;

    .line 748
    :try_start_0
    sget-object v4, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mPrevLocation:Landroid/location/Location;

    if-eqz v4, :cond_2

    .line 749
    sget-object v4, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "setUpMap, RealtimeHealthService.mPrevLocation is NOT NULL"

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 751
    new-instance v3, Lcom/amap/api/maps2d/model/LatLng;

    sget-object v4, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mPreviousLatLng:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v4, v4, Lcom/google/android/gms/maps/model/LatLng;->latitude:D

    sget-object v6, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mPreviousLatLng:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v6, v6, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/amap/api/maps2d/model/LatLng;-><init>(DD)V

    .line 752
    .local v3, "mPreviousLatLng":Lcom/amap/api/maps2d/model/LatLng;
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mMap:Lcom/amap/api/maps2d/AMap;

    iget v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mZoomLevel:F

    invoke-static {v3, v5}, Lcom/amap/api/maps2d/CameraUpdateFactory;->newLatLngZoom(Lcom/amap/api/maps2d/model/LatLng;F)Lcom/amap/api/maps2d/CameraUpdate;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/amap/api/maps2d/AMap;->moveCamera(Lcom/amap/api/maps2d/CameraUpdate;)V

    .line 753
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mAMapListener:Lcom/amap/api/maps2d/LocationSource$OnLocationChangedListener;

    if-eqz v4, :cond_0

    .line 754
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mAMapListener:Lcom/amap/api/maps2d/LocationSource$OnLocationChangedListener;

    sget-object v5, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mPrevLocation:Landroid/location/Location;

    invoke-interface {v4, v5}, Lcom/amap/api/maps2d/LocationSource$OnLocationChangedListener;->onLocationChanged(Landroid/location/Location;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 765
    .end local v3    # "mPreviousLatLng":Lcom/amap/api/maps2d/model/LatLng;
    :cond_0
    :goto_0
    sget-object v4, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "setUpMap()"

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 766
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->setMyLocationStyle()V

    .line 767
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mMap:Lcom/amap/api/maps2d/AMap;

    invoke-virtual {v4, v9}, Lcom/amap/api/maps2d/AMap;->setMyLocationEnabled(Z)V

    .line 768
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mMap:Lcom/amap/api/maps2d/AMap;

    invoke-virtual {v4}, Lcom/amap/api/maps2d/AMap;->getUiSettings()Lcom/amap/api/maps2d/UiSettings;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mUiSettings:Lcom/amap/api/maps2d/UiSettings;

    .line 769
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mUiSettings:Lcom/amap/api/maps2d/UiSettings;

    invoke-virtual {v4, v8}, Lcom/amap/api/maps2d/UiSettings;->setMyLocationButtonEnabled(Z)V

    .line 770
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mUiSettings:Lcom/amap/api/maps2d/UiSettings;

    invoke-virtual {v4, v8}, Lcom/amap/api/maps2d/UiSettings;->setZoomControlsEnabled(Z)V

    .line 771
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mUiSettings:Lcom/amap/api/maps2d/UiSettings;

    invoke-virtual {v4, v8}, Lcom/amap/api/maps2d/UiSettings;->setCompassEnabled(Z)V

    .line 772
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mUiSettings:Lcom/amap/api/maps2d/UiSettings;

    invoke-virtual {v4, v8}, Lcom/amap/api/maps2d/UiSettings;->setAllGesturesEnabled(Z)V

    .line 773
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mUiSettings:Lcom/amap/api/maps2d/UiSettings;

    invoke-virtual {v4, v9}, Lcom/amap/api/maps2d/UiSettings;->setCompassEnabled(Z)V

    .line 776
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getLastLocation(Landroid/content/Context;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 777
    new-instance v0, Lcom/amap/api/maps2d/model/LatLngBounds$Builder;

    invoke-direct {v0}, Lcom/amap/api/maps2d/model/LatLngBounds$Builder;-><init>()V

    .line 778
    .local v0, "builder":Lcom/amap/api/maps2d/model/LatLngBounds$Builder;
    new-instance v2, Lcom/amap/api/maps2d/model/LatLng;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getLastLocation(Landroid/content/Context;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v4

    iget-wide v4, v4, Lcom/google/android/gms/maps/model/LatLng;->latitude:D

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getLastLocation(Landroid/content/Context;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v6

    iget-wide v6, v6, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    invoke-direct {v2, v4, v5, v6, v7}, Lcom/amap/api/maps2d/model/LatLng;-><init>(DD)V

    .line 779
    .local v2, "lat":Lcom/amap/api/maps2d/model/LatLng;
    invoke-virtual {v0, v2}, Lcom/amap/api/maps2d/model/LatLngBounds$Builder;->include(Lcom/amap/api/maps2d/model/LatLng;)Lcom/amap/api/maps2d/model/LatLngBounds$Builder;

    .line 781
    :try_start_1
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mMap:Lcom/amap/api/maps2d/AMap;

    invoke-virtual {v0}, Lcom/amap/api/maps2d/model/LatLngBounds$Builder;->build()Lcom/amap/api/maps2d/model/LatLngBounds;

    move-result-object v5

    const/16 v6, 0x168

    const/16 v7, 0x220

    const/4 v8, 0x3

    invoke-static {v5, v6, v7, v8}, Lcom/amap/api/maps2d/CameraUpdateFactory;->newLatLngBounds(Lcom/amap/api/maps2d/model/LatLngBounds;III)Lcom/amap/api/maps2d/CameraUpdate;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/amap/api/maps2d/AMap;->moveCamera(Lcom/amap/api/maps2d/CameraUpdate;)V

    .line 782
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mMap:Lcom/amap/api/maps2d/AMap;

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeMapZoomLevel()F

    move-result v5

    invoke-static {v5}, Lcom/amap/api/maps2d/CameraUpdateFactory;->zoomTo(F)Lcom/amap/api/maps2d/CameraUpdate;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/amap/api/maps2d/AMap;->moveCamera(Lcom/amap/api/maps2d/CameraUpdate;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 789
    .end local v0    # "builder":Lcom/amap/api/maps2d/model/LatLngBounds$Builder;
    .end local v2    # "lat":Lcom/amap/api/maps2d/model/LatLng;
    :cond_1
    :goto_1
    return-void

    .line 758
    :cond_2
    :try_start_2
    sget-object v4, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "setUpMap, RealtimeHealthService.mPrevLocation is NULL!"

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0

    .line 762
    :catch_0
    move-exception v1

    .line 763
    .local v1, "e":Ljava/lang/Exception;
    sget-object v4, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "moveCamera issue"

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 784
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v0    # "builder":Lcom/amap/api/maps2d/model/LatLngBounds$Builder;
    .restart local v2    # "lat":Lcom/amap/api/maps2d/model/LatLng;
    :catch_1
    move-exception v1

    .line 785
    .restart local v1    # "e":Ljava/lang/Exception;
    sget-object v4, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "moveCamera issue"

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 786
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method private setUpMapIfNeeded(Landroid/view/View;)V
    .locals 3
    .param p1, "contentView"    # Landroid/view/View;

    .prologue
    .line 728
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "setUpMapIfNeeded()"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 729
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mMapView:Lcom/amap/api/maps2d/MapView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mMap:Lcom/amap/api/maps2d/AMap;

    if-nez v1, :cond_0

    .line 732
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mMapView:Lcom/amap/api/maps2d/MapView;

    invoke-virtual {v1}, Lcom/amap/api/maps2d/MapView;->getMap()Lcom/amap/api/maps2d/AMap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mMap:Lcom/amap/api/maps2d/AMap;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 739
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mMap:Lcom/amap/api/maps2d/AMap;

    if-eqz v1, :cond_1

    .line 740
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->setUpMap()V

    .line 742
    :cond_1
    return-void

    .line 733
    :catch_0
    move-exception v0

    .line 734
    .local v0, "ex":Ljava/lang/NullPointerException;
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->TAG:Ljava/lang/String;

    const-string v2, "Exception occured in getMap() api call!!"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 735
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method private setWorkoutModeState(I)V
    .locals 5
    .param p1, "state"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1677
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->updateDataLayout()V

    .line 1679
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivity:Landroid/app/Activity;

    check-cast v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->isDrawerMenuVisible()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1680
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivity:Landroid/app/Activity;

    check-cast v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getActionBarTitleId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 1684
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getActionBarTitleId()I

    move-result v0

    const v1, 0x7f090021

    if-ne v0, v1, :cond_2

    .line 1685
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivity:Landroid/app/Activity;

    check-cast v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->setActionbarDescription(Z)V

    .line 1691
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->updateAudioGuideButton()V

    .line 1692
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->setDefaultBackground()V

    .line 1694
    packed-switch p1, :pswitch_data_0

    .line 1706
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mMap:Lcom/amap/api/maps2d/AMap;

    if-eqz v0, :cond_1

    .line 1707
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mMap:Lcom/amap/api/maps2d/AMap;

    invoke-virtual {v0}, Lcom/amap/api/maps2d/AMap;->clear()V

    .line 1708
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->setMyLocationStyle()V

    .line 1710
    :cond_1
    invoke-direct {p0, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->enableGoal(Z)V

    .line 1712
    const-wide/16 v0, 0x0

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->lapClockDispUpdate(J)V

    .line 1713
    invoke-direct {p0, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->updateModeState(Z)V

    .line 1715
    :goto_1
    return-void

    .line 1688
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivity:Landroid/app/Activity;

    check-cast v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->setActionbarDescription(Z)V

    .line 1689
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivity:Landroid/app/Activity;

    check-cast v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getActionBarLayout()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f080306

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09020f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1696
    :pswitch_0
    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->enableGoal(Z)V

    .line 1697
    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->updateModeState(Z)V

    goto :goto_1

    .line 1701
    :pswitch_1
    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->enableGoal(Z)V

    .line 1702
    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->updateModeState(Z)V

    goto :goto_1

    .line 1694
    :pswitch_data_0
    .packed-switch 0x7d1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private startAnimationFromLockMode(Z)V
    .locals 4
    .param p1, "islock"    # Z

    .prologue
    .line 1604
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mMusicPlayerLayout:Landroid/widget/LinearLayout;

    if-nez v2, :cond_0

    .line 1643
    :goto_0
    return-void

    .line 1607
    :cond_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f04001d

    invoke-static {v2, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    .line 1608
    .local v1, "fadeOutanimation":Landroid/view/animation/Animation;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f04001c

    invoke-static {v2, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 1609
    .local v0, "fadeInanimation":Landroid/view/animation/Animation;
    new-instance v2, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$10;

    invoke-direct {v2, p0, p1, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$10;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;ZLandroid/view/animation/Animation;)V

    invoke-virtual {v1, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1641
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mMusicPlayerLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->clearAnimation()V

    .line 1642
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mMusicPlayerLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->setAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method private unregisterReceivers()V
    .locals 2

    .prologue
    .line 3640
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mMediaInfoReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 3641
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mMediaInfoReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 3642
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mMediaInfoReceiver:Landroid/content/BroadcastReceiver;

    .line 3644
    :cond_0
    return-void
.end method

.method private updateActivityType()V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2619
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->runningView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 2620
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->walkingView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 2621
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->cyclingView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 2622
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->hikingView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 2623
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->firstbubbleView:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2624
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->secondbubbleView:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2625
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->thirdbubbleView:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2626
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->fourthbubbleView:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2628
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getActivityType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 2646
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mGoalTypeView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    .line 2648
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->updateGoalText()V

    .line 2650
    return-void

    .line 2630
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->runningView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setSelected(Z)V

    .line 2631
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->firstbubbleView:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 2634
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->walkingView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setSelected(Z)V

    .line 2635
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->secondbubbleView:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 2638
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->cyclingView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setSelected(Z)V

    .line 2639
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->thirdbubbleView:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 2642
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->hikingView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setSelected(Z)V

    .line 2643
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->fourthbubbleView:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 2628
    nop

    :pswitch_data_0
    .packed-switch 0x4652
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private updateAudioGuideButton()V
    .locals 11

    .prologue
    const/16 v10, 0x7d0

    const/4 v9, 0x2

    const v8, 0x7f09020b

    const v6, 0x7f08070e

    const/4 v5, 0x0

    .line 936
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealTimeAudioGuideItemIndex()I

    move-result v4

    if-nez v4, :cond_2

    const/4 v1, 0x1

    .line 940
    .local v1, "isVisible":Z
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getCurrentMode()I

    move-result v4

    if-ne v4, v9, :cond_4

    .line 941
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->isIndoorRecording()Z

    move-result v4

    if-nez v4, :cond_3

    .line 942
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mOperatelayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v4, v6}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 953
    .local v0, "audioGuide":Landroid/view/View;
    :goto_1
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mAudioCheckbox:Landroid/widget/CheckBox;

    if-eqz v4, :cond_0

    .line 954
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mAudioCheckbox:Landroid/widget/CheckBox;

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 957
    :cond_0
    if-eqz v1, :cond_6

    .line 958
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f090a79

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f09022a

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f09022b

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 977
    :goto_2
    const v4, 0x7f080724

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/CheckBox;

    iput-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mAudioCheckbox:Landroid/widget/CheckBox;

    .line 978
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mAudioCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v4, v5}, Landroid/widget/CheckBox;->setClickable(Z)V

    .line 979
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mAudioCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v4, v5}, Landroid/widget/CheckBox;->setFocusable(Z)V

    .line 980
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mAudioCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v4, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 981
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mAudioCheckbox:Landroid/widget/CheckBox;

    new-instance v6, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$7;

    invoke-direct {v6, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$7;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)V

    invoke-virtual {v4, v6}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 1003
    const v4, 0x7f080723

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 1004
    .local v3, "tv":Landroid/widget/TextView;
    const v4, 0x7f080722

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 1005
    .local v2, "iv":Landroid/widget/ImageView;
    const v4, 0x7f090a79

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 1006
    if-nez v1, :cond_a

    .line 1007
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getCurrentMode()I

    move-result v4

    if-ne v4, v9, :cond_8

    .line 1008
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->isIndoorRecording()Z

    move-result v4

    if-nez v4, :cond_7

    .line 1009
    const v4, 0x7f020179

    invoke-static {v2, v4}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->setImageResource(Landroid/widget/ImageView;I)V

    .line 1035
    :goto_3
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mOnAudioGuideClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1036
    sget v4, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->SOUND_MODE:I

    if-nez v4, :cond_1

    .line 1037
    invoke-virtual {v0, v5}, Landroid/view/View;->setSoundEffectsEnabled(Z)V

    .line 1039
    :cond_1
    return-void

    .end local v0    # "audioGuide":Landroid/view/View;
    .end local v1    # "isVisible":Z
    .end local v2    # "iv":Landroid/widget/ImageView;
    .end local v3    # "tv":Landroid/widget/TextView;
    :cond_2
    move v1, v5

    .line 936
    goto/16 :goto_0

    .line 944
    .restart local v1    # "isVisible":Z
    :cond_3
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mStatusLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v4, v6}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .restart local v0    # "audioGuide":Landroid/view/View;
    goto/16 :goto_1

    .line 947
    .end local v0    # "audioGuide":Landroid/view/View;
    :cond_4
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getWorkoutState()I

    move-result v4

    if-ne v4, v10, :cond_5

    .line 948
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mOperatelayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v4, v6}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .restart local v0    # "audioGuide":Landroid/view/View;
    goto/16 :goto_1

    .line 950
    .end local v0    # "audioGuide":Landroid/view/View;
    :cond_5
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mStatusLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v4, v6}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .restart local v0    # "audioGuide":Landroid/view/View;
    goto/16 :goto_1

    .line 967
    :cond_6
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f090a79

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f09022a

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f09022c

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 1011
    .restart local v2    # "iv":Landroid/widget/ImageView;
    .restart local v3    # "tv":Landroid/widget/TextView;
    :cond_7
    const v4, 0x7f02017a

    invoke-static {v2, v4}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->setImageResource(Landroid/widget/ImageView;I)V

    goto/16 :goto_3

    .line 1014
    :cond_8
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getWorkoutState()I

    move-result v4

    if-ne v4, v10, :cond_9

    .line 1015
    const v4, 0x7f020179

    invoke-static {v2, v4}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->setImageResource(Landroid/widget/ImageView;I)V

    goto/16 :goto_3

    .line 1017
    :cond_9
    const v4, 0x7f02017a

    invoke-static {v2, v4}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->setImageResource(Landroid/widget/ImageView;I)V

    goto/16 :goto_3

    .line 1021
    :cond_a
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getCurrentMode()I

    move-result v4

    if-ne v4, v9, :cond_c

    .line 1022
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->isIndoorRecording()Z

    move-result v4

    if-nez v4, :cond_b

    .line 1023
    const v4, 0x7f02017b

    invoke-static {v2, v4}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->setImageResource(Landroid/widget/ImageView;I)V

    goto/16 :goto_3

    .line 1025
    :cond_b
    const v4, 0x7f02017c

    invoke-static {v2, v4}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->setImageResource(Landroid/widget/ImageView;I)V

    goto/16 :goto_3

    .line 1028
    :cond_c
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getWorkoutState()I

    move-result v4

    if-ne v4, v10, :cond_d

    .line 1029
    const v4, 0x7f02017b

    invoke-static {v2, v4}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->setImageResource(Landroid/widget/ImageView;I)V

    goto/16 :goto_3

    .line 1031
    :cond_d
    const v4, 0x7f02017c

    invoke-static {v2, v4}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->setImageResource(Landroid/widget/ImageView;I)V

    goto/16 :goto_3
.end method

.method private updateCardView()V
    .locals 42

    .prologue
    .line 2653
    const/4 v6, 0x0

    .line 2654
    .local v6, "selectionClause":Ljava/lang/String;
    const/4 v15, 0x0

    .line 2655
    .local v15, "cursor":Landroid/database/Cursor;
    const/16 v16, 0x0

    .line 2656
    .local v16, "deviceCursor":Landroid/database/Cursor;
    const-wide/16 v26, -0x1

    .line 2659
    .local v26, "exerciseId":J
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getActivityType()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 2734
    :goto_0
    if-eqz v6, :cond_a

    .line 2735
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v15

    .line 2737
    if-eqz v15, :cond_1a

    .line 2738
    invoke-interface {v15}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-lez v3, :cond_19

    .line 2739
    const/16 v22, 0x0

    .local v22, "duration":I
    const/4 v14, 0x0

    .line 2740
    .local v14, "calorie":I
    const/16 v19, 0x0

    .line 2741
    .local v19, "distance":F
    const-wide/16 v38, 0x0

    .local v38, "starttime":J
    const-wide/16 v24, 0x0

    .local v24, "endtime":J
    const-wide/16 v20, 0x0

    .line 2742
    .local v20, "dur":J
    invoke-interface {v15}, Landroid/database/Cursor;->moveToFirst()Z

    .line 2743
    const-string v3, "DIST"

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v15, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-nez v3, :cond_0

    .line 2744
    const-string v3, "DIST"

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v19

    .line 2746
    :cond_0
    const-string v3, "DUR"

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v15, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-nez v3, :cond_1

    .line 2747
    const-string v3, "DUR"

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v22

    .line 2749
    :cond_1
    const-string v3, "STIME"

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v15, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-nez v3, :cond_2

    .line 2750
    const-string v3, "STIME"

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v38

    .line 2752
    :cond_2
    const-string v3, "ETIME"

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v15, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-nez v3, :cond_3

    .line 2753
    const-string v3, "ETIME"

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v24

    .line 2755
    :cond_3
    const-string v3, "CAL"

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v15, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-nez v3, :cond_4

    .line 2756
    const-string v3, "CAL"

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    .line 2758
    :cond_4
    const-string v3, "_id"

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v15, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-nez v3, :cond_5

    .line 2759
    const-string v3, "_id"

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v26

    .line 2763
    :cond_5
    move-object/from16 v0, p0

    move-wide/from16 v1, v26

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->setRealtimeDataFromStatic(J)V

    .line 2764
    const/16 v17, -0x1

    .line 2765
    .local v17, "deviceId":I
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "exercise__id = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, v26

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 2766
    .local v10, "selectionClauseDEVICE":Ljava/lang/String;
    const/4 v3, 0x1

    new-array v9, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "user_device__id"

    aput-object v4, v9, v3

    .line 2768
    .local v9, "mSelectionArgsDEVICE":[Ljava/lang/String;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    sget-object v8, Lcom/samsung/android/sdk/health/content/ShealthContract$ExerciseDeviceInfo;->CONTENT_URI:Landroid/net/Uri;

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual/range {v7 .. v12}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v16

    .line 2769
    if-eqz v16, :cond_6

    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-lez v3, :cond_6

    .line 2770
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToFirst()Z

    .line 2771
    const-string/jumbo v3, "user_device__id"

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 2772
    .local v18, "deviceType":Ljava/lang/String;
    if-eqz v18, :cond_6

    const-string v3, "_"

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    if-lez v3, :cond_6

    .line 2773
    const-string v3, "_"

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aget-object v3, v3, v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v17

    .line 2778
    .end local v18    # "deviceType":Ljava/lang/String;
    :cond_6
    if-eqz v16, :cond_7

    .line 2779
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    .line 2781
    :cond_7
    sparse-switch v17, :sswitch_data_0

    .line 2791
    :goto_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCardViewNoDataLayout:Landroid/widget/LinearLayout;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2792
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCardViewDataLayout:Landroid/widget/LinearLayout;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2794
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getActivityType()I

    move-result v3

    const/16 v4, 0x4655

    if-ne v3, v4, :cond_e

    .line 2795
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->isAvailableBarometer(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 2796
    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mMaxElevation:D

    const-wide/16 v7, 0x0

    cmpl-double v3, v3, v7

    if-nez v3, :cond_c

    .line 2797
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const/4 v4, 0x6

    move-object/from16 v0, p0

    iget-wide v7, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mMaxElevation:D

    double-to-float v5, v7

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkUnit(ILjava/lang/Float;)I

    move-result v4

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getDataUnitByType(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v40

    .line 2799
    .local v40, "unit":Ljava/lang/String;
    const-string v33, "0"

    .line 2812
    .local v33, "maxStr":Ljava/lang/String;
    :goto_2
    invoke-static/range {v33 .. v33}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v33

    .line 2813
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCardViewDistTxt:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v33

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v40

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2832
    .end local v33    # "maxStr":Ljava/lang/String;
    .end local v40    # "unit":Ljava/lang/String;
    :goto_3
    sub-long v20, v24, v38

    .line 2833
    const-wide/16 v3, 0x0

    cmp-long v3, v20, v3

    if-gez v3, :cond_8

    .line 2834
    const-wide/16 v20, 0x0

    .line 2837
    :cond_8
    const-wide/16 v3, 0x3e8

    div-long v3, v20, v3

    long-to-int v3, v3

    rem-int/lit8 v37, v3, 0x3c

    .line 2838
    .local v37, "seconds":I
    const-wide/32 v3, 0xea60

    div-long v3, v20, v3

    const-wide/16 v7, 0x3c

    rem-long/2addr v3, v7

    long-to-int v0, v3

    move/from16 v36, v0

    .line 2839
    .local v36, "minutes":I
    const-wide/32 v3, 0x36ee80

    div-long v3, v20, v3

    const-wide/16 v7, 0x18

    rem-long/2addr v3, v7

    long-to-int v0, v3

    move/from16 v28, v0

    .line 2840
    .local v28, "hours":I
    const-string v23, ""

    .line 2842
    .local v23, "durationValue":Ljava/lang/String;
    const-wide/16 v3, 0x3e8

    div-long v3, v20, v3

    const-wide/16 v7, 0xe10

    cmp-long v3, v3, v7

    if-gez v3, :cond_10

    .line 2843
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "%02d"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static/range {v36 .. v36}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "%02d"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static/range {v37 .. v37}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    .line 2848
    :goto_4
    invoke-static/range {v23 .. v23}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    .line 2849
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCardViewDurTxt:Landroid/widget/TextView;

    move-object/from16 v0, v23

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2850
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCardViewDurTxt:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    move-wide/from16 v0, v20

    invoke-static {v0, v1, v4}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->SecToContentDescription(JLandroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2852
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getActivityType()I

    move-result v3

    const/16 v4, 0x4653

    if-ne v3, v4, :cond_14

    .line 2854
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getDistanceUnitFromSharedPreferences()Ljava/lang/String;

    move-result-object v3

    const-string v4, "km"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    .line 2857
    .local v31, "kmUnit":Z
    if-eqz v31, :cond_12

    .line 2858
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mAvgSpeed:F

    const/high16 v4, 0x447a0000    # 1000.0f

    div-float/2addr v3, v4

    const v4, 0x3dcccccd    # 0.1f

    cmpl-float v3, v3, v4

    if-ltz v3, :cond_11

    .line 2859
    const/high16 v3, 0x45610000    # 3600.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mAvgSpeed:F

    const/high16 v5, 0x447a0000    # 1000.0f

    div-float/2addr v4, v5

    div-float/2addr v3, v4

    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getPaceValue(FZ)Ljava/lang/String;

    move-result-object v41

    .line 2863
    .local v41, "value":Ljava/lang/String;
    :goto_5
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const v4, 0x7f0900c7

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v40

    .line 2872
    .restart local v40    # "unit":Ljava/lang/String;
    :goto_6
    invoke-static/range {v41 .. v41}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v41

    .line 2873
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCardViewPaceTxt:Landroid/widget/TextView;

    move-object/from16 v0, v41

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2921
    .end local v9    # "mSelectionArgsDEVICE":[Ljava/lang/String;
    .end local v10    # "selectionClauseDEVICE":Ljava/lang/String;
    .end local v14    # "calorie":I
    .end local v17    # "deviceId":I
    .end local v19    # "distance":F
    .end local v20    # "dur":J
    .end local v22    # "duration":I
    .end local v23    # "durationValue":Ljava/lang/String;
    .end local v24    # "endtime":J
    .end local v28    # "hours":I
    .end local v31    # "kmUnit":Z
    .end local v36    # "minutes":I
    .end local v37    # "seconds":I
    .end local v38    # "starttime":J
    .end local v40    # "unit":Ljava/lang/String;
    .end local v41    # "value":Ljava/lang/String;
    :cond_9
    :goto_7
    if-eqz v15, :cond_a

    .line 2922
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    .line 2930
    :cond_a
    :goto_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivityTextRunning:Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0909d3

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2931
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivityTextWaliking:Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0909d2

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2932
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivityTextCycling:Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0909d0

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2933
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivityTextHiking:Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0909d1

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2935
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCardView:Landroid/view/View;

    const v4, 0x7f080726

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090fc0

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2937
    return-void

    .line 2661
    :pswitch_0
    const-string/jumbo v6, "select exercise.[_id] as _id,exercise.[distance] as DIST,exercise.[start_time] as STIME,exercise.[end_time] as ETIME,exercise.[duration_min] as DUR, exercise.[total_calorie] as CAL from exercise where exercise.[exercise_info__id]= 18003 order by exercise.[end_time] desc limit 1"

    .line 2663
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCardNoLayoutText:Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    const v5, 0x7f090f39

    invoke-virtual {v4, v5}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2664
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCardViewDetailTxt1:Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    const v5, 0x7f090132

    invoke-virtual {v4, v5}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2665
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCardViewDetailTxt2:Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    const v5, 0x7f090a1b

    invoke-virtual {v4, v5}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2666
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCardViewDetailTxt3:Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    const v5, 0x7f090b54

    invoke-virtual {v4, v5}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2667
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivityTextRunning:Landroid/widget/TextView;

    const-string/jumbo v4, "sec-roboto-light"

    const/4 v5, 0x1

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 2668
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivityTextRunning:Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f07011d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2669
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivityTextWaliking:Landroid/widget/TextView;

    const-string/jumbo v4, "sec-roboto-light"

    const/4 v5, 0x0

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 2670
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivityTextWaliking:Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0700e3

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2671
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivityTextCycling:Landroid/widget/TextView;

    const-string/jumbo v4, "sec-roboto-light"

    const/4 v5, 0x0

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 2672
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivityTextCycling:Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0700e3

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2673
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivityTextHiking:Landroid/widget/TextView;

    const-string/jumbo v4, "sec-roboto-light"

    const/4 v5, 0x0

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 2674
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivityTextHiking:Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0700e3

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2675
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->cardViewDataTitleTextView:Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090fb1

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 2679
    :pswitch_1
    const-string/jumbo v6, "select exercise.[_id] as _id,exercise.[distance] as DIST,exercise.[start_time] as STIME,exercise.[end_time] as ETIME,exercise.[duration_min] as DUR, exercise.[total_calorie] as CAL from exercise where exercise.[exercise_info__id]= 18002 order by exercise.[end_time] desc limit 1"

    .line 2681
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCardNoLayoutText:Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    const v5, 0x7f090f3a

    invoke-virtual {v4, v5}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2682
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCardViewDetailTxt1:Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    const v5, 0x7f090132

    invoke-virtual {v4, v5}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2683
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCardViewDetailTxt2:Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    const v5, 0x7f090a1b

    invoke-virtual {v4, v5}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2684
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCardViewDetailTxt3:Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    const v5, 0x7f090b56

    invoke-virtual {v4, v5}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2685
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivityTextRunning:Landroid/widget/TextView;

    const-string/jumbo v4, "sec-roboto-light"

    const/4 v5, 0x0

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 2686
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivityTextRunning:Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0700e3

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2687
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivityTextWaliking:Landroid/widget/TextView;

    const-string/jumbo v4, "sec-roboto-light"

    const/4 v5, 0x1

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 2688
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivityTextWaliking:Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f07011d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2689
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivityTextCycling:Landroid/widget/TextView;

    const-string/jumbo v4, "sec-roboto-light"

    const/4 v5, 0x0

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 2690
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivityTextCycling:Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0700e3

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2691
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivityTextHiking:Landroid/widget/TextView;

    const-string/jumbo v4, "sec-roboto-light"

    const/4 v5, 0x0

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 2692
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivityTextHiking:Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0700e3

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2693
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->cardViewDataTitleTextView:Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090fb2

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 2698
    :pswitch_2
    const-string/jumbo v6, "select exercise.[_id] as _id,exercise.[distance] as DIST,exercise.[start_time] as STIME,exercise.[end_time] as ETIME,exercise.[duration_min] as DUR, exercise.[total_calorie] as CAL from exercise where exercise.[exercise_info__id]= 18004 order by exercise.[end_time] desc limit 1"

    .line 2700
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCardNoLayoutText:Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    const v5, 0x7f090f3b

    invoke-virtual {v4, v5}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2701
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCardViewDetailTxt1:Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    const v5, 0x7f090132

    invoke-virtual {v4, v5}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2702
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCardViewDetailTxt2:Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    const v5, 0x7f090a1b

    invoke-virtual {v4, v5}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2703
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCardViewDetailTxt3:Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    const v5, 0x7f090b55

    invoke-virtual {v4, v5}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2704
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivityTextRunning:Landroid/widget/TextView;

    const-string/jumbo v4, "sec-roboto-light"

    const/4 v5, 0x0

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 2705
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivityTextRunning:Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0700e3

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2706
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivityTextWaliking:Landroid/widget/TextView;

    const-string/jumbo v4, "sec-roboto-light"

    const/4 v5, 0x0

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 2707
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivityTextWaliking:Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0700e3

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2708
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivityTextCycling:Landroid/widget/TextView;

    const-string/jumbo v4, "sec-roboto-light"

    const/4 v5, 0x1

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 2709
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivityTextCycling:Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f07011d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2710
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivityTextHiking:Landroid/widget/TextView;

    const-string/jumbo v4, "sec-roboto-light"

    const/4 v5, 0x0

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 2711
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivityTextHiking:Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0700e3

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2712
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->cardViewDataTitleTextView:Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090fb3

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 2716
    :pswitch_3
    const-string/jumbo v6, "select exercise.[_id] as _id,exercise.[distance] as DIST,exercise.[start_time] as STIME,exercise.[end_time] as ETIME,exercise.[duration_min] as DUR, exercise.[total_calorie] as CAL from exercise where exercise.[exercise_info__id]= 18005 order by exercise.[end_time] desc limit 1"

    .line 2718
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCardNoLayoutText:Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    const v5, 0x7f090f3c

    invoke-virtual {v4, v5}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2719
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCardViewDetailTxt1:Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    const v5, 0x7f090f58

    invoke-virtual {v4, v5}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2720
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCardViewDetailTxt2:Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    const v5, 0x7f090a1b

    invoke-virtual {v4, v5}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2721
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCardViewDetailTxt3:Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    const v5, 0x7f090b56

    invoke-virtual {v4, v5}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2722
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivityTextRunning:Landroid/widget/TextView;

    const-string/jumbo v4, "sec-roboto-light"

    const/4 v5, 0x0

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 2723
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivityTextRunning:Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0700e3

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2724
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivityTextWaliking:Landroid/widget/TextView;

    const-string/jumbo v4, "sec-roboto-light"

    const/4 v5, 0x0

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 2725
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivityTextWaliking:Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0700e3

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2726
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivityTextCycling:Landroid/widget/TextView;

    const-string/jumbo v4, "sec-roboto-light"

    const/4 v5, 0x0

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 2727
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivityTextCycling:Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0700e3

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2728
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivityTextHiking:Landroid/widget/TextView;

    const-string/jumbo v4, "sec-roboto-light"

    const/4 v5, 0x1

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 2729
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivityTextHiking:Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f07011d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2730
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->cardViewDataTitleTextView:Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090fb4

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 2778
    .restart local v9    # "mSelectionArgsDEVICE":[Ljava/lang/String;
    .restart local v10    # "selectionClauseDEVICE":Ljava/lang/String;
    .restart local v14    # "calorie":I
    .restart local v17    # "deviceId":I
    .restart local v19    # "distance":F
    .restart local v20    # "dur":J
    .restart local v22    # "duration":I
    .restart local v24    # "endtime":J
    .restart local v38    # "starttime":J
    :catchall_0
    move-exception v3

    if-eqz v16, :cond_b

    .line 2779
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    :cond_b
    throw v3

    .line 2787
    :sswitch_0
    move-object/from16 v0, p0

    move-wide/from16 v1, v26

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->updateExerciseFromActivityDB(J)V

    goto/16 :goto_1

    .line 2801
    :cond_c
    const/4 v3, 0x6

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mMaxElevation:D

    double-to-float v4, v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkValue(ILjava/lang/Float;)Ljava/lang/String;

    move-result-object v41

    .line 2802
    .restart local v41    # "value":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const/4 v4, 0x6

    move-object/from16 v0, p0

    iget-wide v7, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mMaxElevation:D

    double-to-float v5, v7

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkUnit(ILjava/lang/Float;)I

    move-result v4

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getDataUnitByType(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v40

    .line 2804
    .restart local v40    # "unit":Ljava/lang/String;
    move-object/from16 v33, v41

    .restart local v33    # "maxStr":Ljava/lang/String;
    goto/16 :goto_2

    .line 2807
    .end local v33    # "maxStr":Ljava/lang/String;
    .end local v40    # "unit":Ljava/lang/String;
    .end local v41    # "value":Ljava/lang/String;
    :cond_d
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const/4 v4, 0x6

    move-object/from16 v0, p0

    iget-wide v7, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mMaxElevation:D

    double-to-float v5, v7

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkUnit(ILjava/lang/Float;)I

    move-result v4

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getDataUnitByType(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v40

    .line 2810
    .restart local v40    # "unit":Ljava/lang/String;
    const-string v33, "--"

    .restart local v33    # "maxStr":Ljava/lang/String;
    goto/16 :goto_2

    .line 2816
    .end local v33    # "maxStr":Ljava/lang/String;
    .end local v40    # "unit":Ljava/lang/String;
    :cond_e
    const/4 v3, 0x0

    cmpg-float v3, v19, v3

    if-gez v3, :cond_f

    .line 2817
    const/16 v19, 0x0

    .line 2825
    :cond_f
    const/4 v3, 0x2

    invoke-static/range {v19 .. v19}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    const/4 v5, 0x1

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkValue(ILjava/lang/Float;Z)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v32

    .line 2826
    .local v32, "l_distText":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCardViewDistTxt:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v32

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    const/4 v7, 0x2

    invoke-static/range {v19 .. v19}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkUnit(ILjava/lang/Float;)I

    move-result v7

    invoke-static {v5, v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getDataUnitByType(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 2846
    .end local v32    # "l_distText":Ljava/lang/String;
    .restart local v23    # "durationValue":Ljava/lang/String;
    .restart local v28    # "hours":I
    .restart local v36    # "minutes":I
    .restart local v37    # "seconds":I
    :cond_10
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "%02d"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "%02d"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static/range {v36 .. v36}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "%02d"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static/range {v37 .. v37}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    goto/16 :goto_4

    .line 2861
    .restart local v31    # "kmUnit":Z
    :cond_11
    const-string v41, "--\'--\""

    .restart local v41    # "value":Ljava/lang/String;
    goto/16 :goto_5

    .line 2865
    .end local v41    # "value":Ljava/lang/String;
    :cond_12
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mAvgSpeed:F

    const/high16 v4, 0x447a0000    # 1000.0f

    div-float/2addr v3, v4

    const v4, 0x3dcccccd    # 0.1f

    cmpl-float v3, v3, v4

    if-ltz v3, :cond_13

    .line 2866
    const/high16 v3, 0x45610000    # 3600.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mAvgSpeed:F

    const v5, 0x3a22e36f

    mul-float/2addr v4, v5

    div-float/2addr v3, v4

    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getPaceValue(FZ)Ljava/lang/String;

    move-result-object v41

    .line 2870
    .restart local v41    # "value":Ljava/lang/String;
    :goto_9
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const v4, 0x7f0900cc

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v40

    .restart local v40    # "unit":Ljava/lang/String;
    goto/16 :goto_6

    .line 2868
    .end local v40    # "unit":Ljava/lang/String;
    .end local v41    # "value":Ljava/lang/String;
    :cond_13
    const-string v41, "--\'--\""

    .restart local v41    # "value":Ljava/lang/String;
    goto :goto_9

    .line 2875
    .end local v31    # "kmUnit":Z
    .end local v41    # "value":Ljava/lang/String;
    :cond_14
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getActivityType()I

    move-result v3

    const/16 v4, 0x4652

    if-eq v3, v4, :cond_15

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getActivityType()I

    move-result v3

    const/16 v4, 0x4655

    if-ne v3, v4, :cond_17

    .line 2877
    :cond_15
    if-gez v14, :cond_16

    .line 2878
    const/4 v14, 0x0

    .line 2880
    :cond_16
    invoke-static {v14}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 2881
    .local v13, "calValue":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCardViewPaceTxt:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    const v7, 0x7f0900b9

    invoke-virtual {v5, v7}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_7

    .line 2884
    .end local v13    # "calValue":Ljava/lang/String;
    :cond_17
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getActivityType()I

    move-result v3

    const/16 v4, 0x4654

    if-ne v3, v4, :cond_9

    .line 2886
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getDistanceUnitFromSharedPreferences()Ljava/lang/String;

    move-result-object v3

    const-string v4, "km"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    .line 2888
    .restart local v31    # "kmUnit":Z
    if-eqz v31, :cond_18

    .line 2889
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mAvgSpeed:F

    float-to-double v3, v3

    const-wide v7, 0x408f400000000000L    # 1000.0

    div-double v29, v3, v7

    .line 2890
    .local v29, "km":D
    const-wide/high16 v3, 0x4024000000000000L    # 10.0

    mul-double v3, v3, v29

    double-to-int v3, v3

    int-to-double v3, v3

    const-wide/high16 v7, 0x4024000000000000L    # 10.0

    div-double v29, v3, v7

    .line 2891
    new-instance v3, Ljava/text/DecimalFormat;

    const-string v4, "0.0"

    invoke-direct {v3, v4}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, v29

    invoke-virtual {v3, v0, v1}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v41

    .line 2892
    .restart local v41    # "value":Ljava/lang/String;
    const v3, 0x7f09080f

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getString(I)Ljava/lang/String;

    move-result-object v40

    .line 2907
    .end local v29    # "km":D
    .restart local v40    # "unit":Ljava/lang/String;
    :goto_a
    invoke-static/range {v41 .. v41}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v41

    .line 2908
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCardViewPaceTxt:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v41

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v40

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_7

    .line 2894
    .end local v40    # "unit":Ljava/lang/String;
    .end local v41    # "value":Ljava/lang/String;
    :cond_18
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mAvgSpeed:F

    float-to-double v3, v3

    const-wide v7, 0x3f445c6dea1359a4L    # 6.2137E-4

    mul-double v34, v3, v7

    .line 2895
    .local v34, "mi":D
    const-wide/high16 v3, 0x4024000000000000L    # 10.0

    mul-double v3, v3, v34

    double-to-int v3, v3

    int-to-double v3, v3

    const-wide/high16 v7, 0x4024000000000000L    # 10.0

    div-double v34, v3, v7

    .line 2896
    new-instance v3, Ljava/text/DecimalFormat;

    const-string v4, "0.0"

    invoke-direct {v3, v4}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, v34

    invoke-virtual {v3, v0, v1}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v41

    .line 2897
    .restart local v41    # "value":Ljava/lang/String;
    const v3, 0x7f090810

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getString(I)Ljava/lang/String;

    move-result-object v40

    .restart local v40    # "unit":Ljava/lang/String;
    goto :goto_a

    .line 2917
    .end local v9    # "mSelectionArgsDEVICE":[Ljava/lang/String;
    .end local v10    # "selectionClauseDEVICE":Ljava/lang/String;
    .end local v14    # "calorie":I
    .end local v17    # "deviceId":I
    .end local v19    # "distance":F
    .end local v20    # "dur":J
    .end local v22    # "duration":I
    .end local v23    # "durationValue":Ljava/lang/String;
    .end local v24    # "endtime":J
    .end local v28    # "hours":I
    .end local v31    # "kmUnit":Z
    .end local v34    # "mi":D
    .end local v36    # "minutes":I
    .end local v37    # "seconds":I
    .end local v38    # "starttime":J
    .end local v40    # "unit":Ljava/lang/String;
    .end local v41    # "value":Ljava/lang/String;
    :cond_19
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCardViewNoDataLayout:Landroid/widget/LinearLayout;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2918
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCardViewDataLayout:Landroid/widget/LinearLayout;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_7

    .line 2925
    :cond_1a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCardViewNoDataLayout:Landroid/widget/LinearLayout;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2926
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCardViewDataLayout:Landroid/widget/LinearLayout;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_8

    .line 2659
    nop

    :pswitch_data_0
    .packed-switch 0x4652
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 2781
    :sswitch_data_0
    .sparse-switch
        0x2723 -> :sswitch_0
        0x2724 -> :sswitch_0
        0x2726 -> :sswitch_0
        0x2728 -> :sswitch_0
        0x272e -> :sswitch_0
    .end sparse-switch
.end method

.method private updateDataLayout()V
    .locals 2

    .prologue
    .line 3484
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    if-nez v0, :cond_0

    .line 3492
    :goto_0
    return-void

    .line 3486
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getCurrentMode()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 3487
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->updateOutdoorLayout()V

    .line 3491
    :goto_1
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->updateDataText()V

    goto :goto_0

    .line 3489
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->updateIndoorlayout()V

    goto :goto_1
.end method

.method private updateDataText()V
    .locals 23

    .prologue
    .line 1071
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->isAdded()Z

    move-result v19

    if-nez v19, :cond_1

    .line 1179
    :cond_0
    return-void

    .line 1075
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mStatusLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v19, v0

    if-eqz v19, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    if-eqz v19, :cond_0

    .line 1077
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mStatusLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v19, v0

    const v20, 0x7f080706

    invoke-virtual/range {v19 .. v20}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 1079
    .local v5, "bigTv":Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Integer;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 1080
    .local v6, "bigType":I
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-static {v0, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getDataStringByType(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v4

    .line 1081
    .local v4, "bigText":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->big_value:Landroid/widget/TextView;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1082
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->big_value:Landroid/widget/TextView;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1083
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->big_extra_value:Landroid/widget/TextView;

    move-object/from16 v19, v0

    const/16 v20, 0x8

    invoke-virtual/range {v19 .. v20}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1085
    new-instance v14, Landroid/widget/RelativeLayout$LayoutParams;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->big_value:Landroid/widget/TextView;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-direct {v14, v0}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1087
    .local v14, "params":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v19, 0x13

    move/from16 v0, v19

    if-ne v6, v0, :cond_2

    .line 1088
    const/16 v19, 0x0

    move/from16 v0, v19

    iput v0, v14, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 1093
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->big_value:Landroid/widget/TextView;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v14}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1095
    const/16 v19, 0x7

    move/from16 v0, v19

    if-ne v6, v0, :cond_6

    .line 1096
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeGoalType()I

    move-result v19

    const/16 v20, 0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_5

    .line 1097
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v19

    const/16 v20, 0xe

    invoke-static/range {v19 .. v20}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getBigDataTypeIconByType(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    .line 1098
    .local v8, "drawable_big":Landroid/graphics/drawable/Drawable;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->big_value:Landroid/widget/TextView;

    move-object/from16 v19, v0

    const/16 v20, 0x8

    invoke-virtual/range {v19 .. v20}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1107
    :goto_0
    const/16 v19, 0xe

    move/from16 v0, v19

    if-ne v6, v0, :cond_4

    .line 1108
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeGoalType()I

    move-result v19

    const/16 v20, 0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_3

    .line 1109
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->big_value:Landroid/widget/TextView;

    move-object/from16 v19, v0

    const/16 v20, 0x8

    invoke-virtual/range {v19 .. v20}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1111
    :cond_3
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeGoalType()I

    move-result v9

    .line 1112
    .local v9, "goalType":I
    const/16 v19, 0x5

    move/from16 v0, v19

    if-ne v9, v0, :cond_4

    .line 1113
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->big_extra_value:Landroid/widget/TextView;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1116
    .end local v9    # "goalType":I
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mView:Landroid/view/View;

    move-object/from16 v19, v0

    const v20, 0x7f080705

    invoke-virtual/range {v19 .. v20}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/ImageView;

    .line 1117
    .local v13, "imageview_big":Landroid/widget/ImageView;
    invoke-static {v13, v8}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->setImageDrawable(Landroid/widget/ImageView;Landroid/graphics/drawable/Drawable;)V

    .line 1118
    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1119
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->updateDataText(I)V

    .line 1139
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mDropDownList:Landroid/view/View;

    move-object/from16 v19, v0

    new-instance v20, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$DropDownClickListener;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mDropDownList:Landroid/view/View;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    move-object/from16 v2, v21

    move/from16 v3, v22

    invoke-direct {v0, v1, v2, v3, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$DropDownClickListener;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;Landroid/view/View;II)V

    invoke-virtual/range {v19 .. v20}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1141
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mAccuWeather:Landroid/view/View;

    move-object/from16 v19, v0

    const/16 v20, 0x4

    invoke-virtual/range {v19 .. v20}, Landroid/view/View;->setVisibility(I)V

    .line 1143
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dataTextViewIDs:[I

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    array-length v0, v0

    move/from16 v19, v0

    move/from16 v0, v19

    if-ge v10, v0, :cond_0

    .line 1144
    add-int/lit8 v11, v10, 0x1

    .line 1145
    .local v11, "idx":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Integer;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Integer;->intValue()I

    move-result v17

    .line 1146
    .local v17, "type":I
    sget-object v19, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->TAG:Ljava/lang/String;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v21, "updateDataText dataType : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1147
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mView:Landroid/view/View;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dataTextViewIDs:[I

    move-object/from16 v20, v0

    aget v20, v20, v10

    invoke-virtual/range {v19 .. v20}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v18

    .line 1148
    .local v18, "view":Landroid/view/View;
    new-instance v19, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$DropDownClickListener;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    move-object/from16 v2, v18

    move/from16 v3, v17

    invoke-direct {v0, v1, v2, v11, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$DropDownClickListener;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;Landroid/view/View;II)V

    invoke-virtual/range {v18 .. v19}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1152
    const/16 v19, 0x7

    move/from16 v0, v17

    move/from16 v1, v19

    if-ne v0, v1, :cond_8

    .line 1153
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeGoalType()I

    move-result v19

    const/16 v20, 0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_7

    .line 1154
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v19

    const/16 v20, 0xe

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getDataTypeIconByType(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    .line 1163
    .local v7, "drawable":Landroid/graphics/drawable/Drawable;
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mDataIcon:[Landroid/widget/ImageView;

    move-object/from16 v19, v0

    aget-object v12, v19, v10

    .line 1164
    .local v12, "imageview":Landroid/widget/ImageView;
    invoke-static {v12, v7}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->setImageDrawable(Landroid/widget/ImageView;Landroid/graphics/drawable/Drawable;)V

    .line 1169
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v19

    move-object/from16 v0, v19

    move/from16 v1, v17

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getDataStringByType(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v15

    .line 1170
    .local v15, "text":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mScrollTextViews:[Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;

    move-object/from16 v19, v0

    aget-object v16, v19, v10

    .line 1172
    .local v16, "tvScroll":Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;
    invoke-static {v15}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1173
    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;->invalidate()V

    .line 1175
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->updateDataText(I)V

    .line 1177
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->setDropDownContentDescription(Landroid/view/View;I)V

    .line 1143
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_1

    .line 1100
    .end local v7    # "drawable":Landroid/graphics/drawable/Drawable;
    .end local v8    # "drawable_big":Landroid/graphics/drawable/Drawable;
    .end local v10    # "i":I
    .end local v11    # "idx":I
    .end local v12    # "imageview":Landroid/widget/ImageView;
    .end local v13    # "imageview_big":Landroid/widget/ImageView;
    .end local v15    # "text":Ljava/lang/String;
    .end local v16    # "tvScroll":Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;
    .end local v17    # "type":I
    .end local v18    # "view":Landroid/view/View;
    :cond_5
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v19

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeGoalType()I

    move-result v20

    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->getDataTypeByGoalType(I)I

    move-result v20

    invoke-static/range {v19 .. v20}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getBigDataTypeIconByType(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    .restart local v8    # "drawable_big":Landroid/graphics/drawable/Drawable;
    goto/16 :goto_0

    .line 1104
    .end local v8    # "drawable_big":Landroid/graphics/drawable/Drawable;
    :cond_6
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-static {v0, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getBigDataTypeIconByType(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    .restart local v8    # "drawable_big":Landroid/graphics/drawable/Drawable;
    goto/16 :goto_0

    .line 1156
    .restart local v10    # "i":I
    .restart local v11    # "idx":I
    .restart local v13    # "imageview_big":Landroid/widget/ImageView;
    .restart local v17    # "type":I
    .restart local v18    # "view":Landroid/view/View;
    :cond_7
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v19

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeGoalType()I

    move-result v20

    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->getDataTypeByGoalType(I)I

    move-result v20

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getDataTypeIconByType(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    .restart local v7    # "drawable":Landroid/graphics/drawable/Drawable;
    goto :goto_2

    .line 1160
    .end local v7    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_8
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v19

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getDataTypeIconByType(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    .restart local v7    # "drawable":Landroid/graphics/drawable/Drawable;
    goto/16 :goto_2
.end method

.method private updateDataText(I)V
    .locals 24
    .param p1, "type"    # I

    .prologue
    .line 2008
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->isAdded()Z

    move-result v20

    if-eqz v20, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    if-nez v20, :cond_1

    .line 2257
    :cond_0
    return-void

    .line 2011
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mStatusLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v20, v0

    const v21, 0x7f080706

    invoke-virtual/range {v20 .. v21}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 2012
    .local v6, "bigTv":Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mView:Landroid/view/View;

    move-object/from16 v20, v0

    const v21, 0x7f080708

    invoke-virtual/range {v20 .. v21}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    .line 2015
    .local v3, "bigContentLayout":Landroid/widget/RelativeLayout;
    new-instance v10, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$REALTIME_DATA;

    const/16 v20, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v10, v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$REALTIME_DATA;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$1;)V

    .line 2035
    .local v10, "data":Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$REALTIME_DATA;
    const/16 v20, 0x3

    move/from16 v0, p1

    move/from16 v1, v20

    if-ne v0, v1, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/lang/Integer;

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Integer;->intValue()I

    move-result v20

    const/16 v21, 0x13

    move/from16 v0, v20

    move/from16 v1, v21

    if-eq v0, v1, :cond_4

    .line 2036
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dataTextViewIDs:[I

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    if-ge v12, v0, :cond_4

    .line 2037
    add-int/lit8 v13, v12, 0x1

    .line 2038
    .local v13, "idx":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/lang/Integer;

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Integer;->intValue()I

    move-result v20

    const/16 v21, 0x13

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_3

    .line 2039
    new-instance v10, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$REALTIME_DATA;

    .end local v10    # "data":Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$REALTIME_DATA;
    const/16 v20, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v10, v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$REALTIME_DATA;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$1;)V

    .line 2040
    .restart local v10    # "data":Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$REALTIME_DATA;
    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v0, v13, v1, v10}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getDataText(IILcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$REALTIME_DATA;)Z

    move-result v20

    if-eqz v20, :cond_3

    .line 2041
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mView:Landroid/view/View;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dataTextViewIDs:[I

    move-object/from16 v21, v0

    aget v21, v21, v12

    invoke-virtual/range {v20 .. v21}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v19

    .line 2043
    .local v19, "view":Landroid/view/View;
    if-eqz v19, :cond_3

    .line 2044
    const v20, 0x7f0806e4

    invoke-virtual/range {v19 .. v20}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;

    .line 2045
    .local v16, "tvScroll":Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;
    if-eqz v16, :cond_2

    .line 2046
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, v10, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$REALTIME_DATA;->value:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    iget-object v0, v10, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$REALTIME_DATA;->unit:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v16

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2047
    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;->invalidate()V

    .line 2050
    :cond_2
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->setDropDownContentDescription(Landroid/view/View;I)V

    .line 2036
    .end local v16    # "tvScroll":Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;
    .end local v19    # "view":Landroid/view/View;
    :cond_3
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_0

    .line 2057
    .end local v12    # "i":I
    .end local v13    # "idx":I
    :cond_4
    const/4 v12, 0x0

    .restart local v12    # "i":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->size()I

    move-result v20

    move/from16 v0, v20

    if-ge v12, v0, :cond_0

    .line 2058
    new-instance v10, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$REALTIME_DATA;

    .end local v10    # "data":Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$REALTIME_DATA;
    const/16 v20, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v10, v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$REALTIME_DATA;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$1;)V

    .line 2059
    .restart local v10    # "data":Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$REALTIME_DATA;
    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v0, v12, v1, v10}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getDataText(IILcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$REALTIME_DATA;)Z

    move-result v20

    if-eqz v20, :cond_6

    .line 2060
    if-nez v12, :cond_1d

    .line 2063
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v20

    move-object/from16 v0, v20

    iget v15, v0, Landroid/util/DisplayMetrics;->density:F

    .line 2065
    .local v15, "scale":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->big_layout:Landroid/widget/RelativeLayout;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    .line 2066
    .local v5, "bigLayoutParams":Landroid/view/ViewGroup$LayoutParams;
    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    .line 2067
    .local v4, "bigContentLayoutParams":Landroid/view/ViewGroup$LayoutParams;
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->isVikalDevice()Z

    move-result v20

    if-nez v20, :cond_7

    const/16 v20, 0x44

    :goto_2
    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    mul-float v20, v20, v15

    const/high16 v21, 0x3f000000    # 0.5f

    add-float v20, v20, v21

    move/from16 v0, v20

    float-to-int v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    iput v0, v5, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2068
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->big_layout:Landroid/widget/RelativeLayout;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2070
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->big_value:Landroid/widget/TextView;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    .line 2071
    .local v8, "bigValueParams":Landroid/view/ViewGroup$LayoutParams;
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->isVikalDevice()Z

    move-result v20

    if-nez v20, :cond_8

    const/16 v20, 0x44

    :goto_3
    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    mul-float v20, v20, v15

    const/high16 v21, 0x3f000000    # 0.5f

    add-float v20, v20, v21

    move/from16 v0, v20

    float-to-int v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    iput v0, v8, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2072
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->big_value:Landroid/widget/TextView;

    move-object/from16 v21, v0

    const/16 v22, 0x1

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->isVikalDevice()Z

    move-result v20

    if-nez v20, :cond_9

    const/16 v20, 0x42

    :goto_4
    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 2073
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->big_value:Landroid/widget/TextView;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2075
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mView:Landroid/view/View;

    move-object/from16 v20, v0

    const v21, 0x7f08070a

    invoke-virtual/range {v20 .. v21}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/TextView;

    .line 2076
    .local v18, "tv_unit":Landroid/widget/TextView;
    invoke-virtual/range {v18 .. v18}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2077
    .local v7, "bigUnitParams":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->isVikalDevice()Z

    move-result v20

    if-nez v20, :cond_a

    const/16 v20, 0x38

    :goto_5
    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    mul-float v20, v20, v15

    const/high16 v21, 0x3f000000    # 0.5f

    add-float v20, v20, v21

    move/from16 v0, v20

    float-to-int v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    iput v0, v7, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 2078
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->isVikalDevice()Z

    move-result v20

    if-nez v20, :cond_5

    .line 2079
    const/high16 v20, 0x41e00000    # 28.0f

    mul-float v20, v20, v15

    const/high16 v21, 0x3f000000    # 0.5f

    add-float v20, v20, v21

    move/from16 v0, v20

    float-to-int v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    iput v0, v7, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 2080
    :cond_5
    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2081
    const/16 v21, 0x1

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->isVikalDevice()Z

    move-result v20

    if-nez v20, :cond_b

    const/16 v20, 0x1b

    :goto_6
    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, v18

    move/from16 v1, v21

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 2083
    iget-object v0, v10, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$REALTIME_DATA;->unit:Ljava/lang/String;

    move-object/from16 v20, v0

    if-eqz v20, :cond_c

    iget-object v0, v10, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$REALTIME_DATA;->unit:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->isEmpty()Z

    move-result v20

    if-nez v20, :cond_c

    .line 2084
    const/16 v20, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2088
    :goto_7
    const/16 v20, 0x1

    move/from16 v0, p1

    move/from16 v1, v20

    if-ne v0, v1, :cond_e

    .line 2089
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->big_value:Landroid/widget/TextView;

    move-object/from16 v21, v0

    const/16 v22, 0x1

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->isVikalDevice()Z

    move-result v20

    if-nez v20, :cond_d

    const/16 v20, 0x46

    :goto_8
    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 2090
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->big_value:Landroid/widget/TextView;

    move-object/from16 v20, v0

    iget-object v0, v10, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$REALTIME_DATA;->value:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2091
    iget-object v0, v10, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$REALTIME_DATA;->unit:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2092
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->big_value:Landroid/widget/TextView;

    move-object/from16 v20, v0

    iget-object v0, v10, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$REALTIME_DATA;->value:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->convertToProperUnit(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2094
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v20

    iget-object v0, v10, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$REALTIME_DATA;->unit:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-static/range {v20 .. v21}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->convertToProperUnitsText(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2096
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, v10, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$REALTIME_DATA;->value:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->convertToProperUnit(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v21

    iget-object v0, v10, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$REALTIME_DATA;->unit:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-static/range {v21 .. v22}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->convertToProperUnitsText(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v3, v0}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2099
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mDropDownList:Landroid/view/View;

    move-object/from16 v20, v0

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v22

    invoke-interface/range {v22 .. v22}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    const v23, 0x7f09020b

    invoke-virtual/range {v22 .. v23}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    const v23, 0x7f0901ec

    invoke-virtual/range {v22 .. v23}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2057
    .end local v4    # "bigContentLayoutParams":Landroid/view/ViewGroup$LayoutParams;
    .end local v5    # "bigLayoutParams":Landroid/view/ViewGroup$LayoutParams;
    .end local v7    # "bigUnitParams":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v8    # "bigValueParams":Landroid/view/ViewGroup$LayoutParams;
    .end local v15    # "scale":F
    .end local v18    # "tv_unit":Landroid/widget/TextView;
    :cond_6
    :goto_9
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_1

    .line 2067
    .restart local v4    # "bigContentLayoutParams":Landroid/view/ViewGroup$LayoutParams;
    .restart local v5    # "bigLayoutParams":Landroid/view/ViewGroup$LayoutParams;
    .restart local v15    # "scale":F
    :cond_7
    const/16 v20, 0x2a

    goto/16 :goto_2

    .line 2071
    .restart local v8    # "bigValueParams":Landroid/view/ViewGroup$LayoutParams;
    :cond_8
    const/16 v20, 0x2a

    goto/16 :goto_3

    .line 2072
    :cond_9
    const/16 v20, 0x28

    goto/16 :goto_4

    .line 2077
    .restart local v7    # "bigUnitParams":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v18    # "tv_unit":Landroid/widget/TextView;
    :cond_a
    const/16 v20, 0x2a

    goto/16 :goto_5

    .line 2081
    :cond_b
    const/16 v20, 0x11

    goto/16 :goto_6

    .line 2086
    :cond_c
    const/16 v20, 0x8

    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_7

    .line 2089
    :cond_d
    const/16 v20, 0x28

    goto/16 :goto_8

    .line 2104
    :cond_e
    const/16 v20, 0xe

    move/from16 v0, p1

    move/from16 v1, v20

    if-eq v0, v1, :cond_f

    const/16 v20, 0x18

    move/from16 v0, p1

    move/from16 v1, v20

    if-ne v0, v1, :cond_18

    .line 2105
    :cond_f
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeGoalType()I

    move-result v20

    const/16 v21, 0x5

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_16

    .line 2107
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->isVikalDevice()Z

    move-result v20

    if-nez v20, :cond_11

    const/16 v20, 0x58

    :goto_a
    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    mul-float v20, v20, v15

    const/high16 v21, 0x3f000000    # 0.5f

    add-float v20, v20, v21

    move/from16 v0, v20

    float-to-int v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    iput v0, v5, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2108
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->big_layout:Landroid/widget/RelativeLayout;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2110
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->isVikalDevice()Z

    move-result v20

    if-eqz v20, :cond_10

    .line 2111
    const/high16 v20, 0x41d00000    # 26.0f

    mul-float v20, v20, v15

    const/high16 v21, 0x3f000000    # 0.5f

    add-float v20, v20, v21

    move/from16 v0, v20

    float-to-int v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    iput v0, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2112
    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2115
    :cond_10
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->isVikalDevice()Z

    move-result v20

    if-nez v20, :cond_12

    const/16 v20, 0x44

    :goto_b
    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    mul-float v20, v20, v15

    const/high16 v21, 0x3f000000    # 0.5f

    add-float v20, v20, v21

    move/from16 v0, v20

    float-to-int v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    iput v0, v8, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2116
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->big_value:Landroid/widget/TextView;

    move-object/from16 v21, v0

    const/16 v22, 0x1

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->isVikalDevice()Z

    move-result v20

    if-nez v20, :cond_13

    const/16 v20, 0x3c

    :goto_c
    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 2117
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->big_value:Landroid/widget/TextView;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2119
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->isVikalDevice()Z

    move-result v20

    if-nez v20, :cond_14

    const/16 v20, 0x28

    :goto_d
    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    mul-float v20, v20, v15

    const/high16 v21, 0x3f000000    # 0.5f

    add-float v20, v20, v21

    move/from16 v0, v20

    float-to-int v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    iput v0, v7, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 2120
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->isVikalDevice()Z

    move-result v20

    if-nez v20, :cond_15

    const/16 v20, 0x18

    :goto_e
    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    mul-float v20, v20, v15

    const/high16 v21, 0x3f000000    # 0.5f

    add-float v20, v20, v21

    move/from16 v0, v20

    float-to-int v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    iput v0, v7, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 2121
    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2123
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->big_extra_value:Landroid/widget/TextView;

    move-object/from16 v20, v0

    iget-object v0, v10, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$REALTIME_DATA;->extraValue:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2124
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->big_extra_value:Landroid/widget/TextView;

    move-object/from16 v20, v0

    const-string v21, ""

    invoke-virtual/range {v20 .. v21}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2129
    :goto_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->big_value:Landroid/widget/TextView;

    move-object/from16 v20, v0

    iget-object v0, v10, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$REALTIME_DATA;->value:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2130
    iget-object v0, v10, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$REALTIME_DATA;->unit:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2131
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v20

    iget-object v0, v10, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$REALTIME_DATA;->unit:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-static/range {v20 .. v21}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->convertToProperUnitsText(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2132
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->big_value:Landroid/widget/TextView;

    move-object/from16 v20, v0

    const-string v21, ""

    invoke-virtual/range {v20 .. v21}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2134
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, v10, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$REALTIME_DATA;->value:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v21

    iget-object v0, v10, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$REALTIME_DATA;->unit:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-static/range {v21 .. v22}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->convertToProperUnitsText(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v3, v0}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2138
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mDropDownList:Landroid/view/View;

    move-object/from16 v20, v0

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v22

    invoke-interface/range {v22 .. v22}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    const v23, 0x7f09020b

    invoke-virtual/range {v22 .. v23}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    const v23, 0x7f0901ec

    invoke-virtual/range {v22 .. v23}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_9

    .line 2107
    :cond_11
    const/16 v20, 0x2c

    goto/16 :goto_a

    .line 2115
    :cond_12
    const/16 v20, 0x1a

    goto/16 :goto_b

    .line 2116
    :cond_13
    const/16 v20, 0x18

    goto/16 :goto_c

    .line 2119
    :cond_14
    const/16 v20, 0x1a

    goto/16 :goto_d

    .line 2120
    :cond_15
    const/16 v20, 0x4

    goto/16 :goto_e

    .line 2127
    :cond_16
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->big_value:Landroid/widget/TextView;

    move-object/from16 v21, v0

    const/16 v22, 0x1

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->isVikalDevice()Z

    move-result v20

    if-nez v20, :cond_17

    const/16 v20, 0x46

    :goto_10
    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    goto/16 :goto_f

    :cond_17
    const/16 v20, 0x28

    goto :goto_10

    .line 2143
    :cond_18
    const/16 v20, 0x17

    move/from16 v0, p1

    move/from16 v1, v20

    if-ne v0, v1, :cond_19

    .line 2144
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->big_value:Landroid/widget/TextView;

    move-object/from16 v20, v0

    const/16 v21, 0x1

    const/high16 v22, 0x420c0000    # 35.0f

    invoke-virtual/range {v20 .. v22}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 2145
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->big_value:Landroid/widget/TextView;

    move-object/from16 v20, v0

    const/16 v21, 0x11

    invoke-virtual/range {v20 .. v21}, Landroid/widget/TextView;->setGravity(I)V

    .line 2146
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->big_value:Landroid/widget/TextView;

    move-object/from16 v20, v0

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, v10, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$REALTIME_DATA;->value:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " / "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    iget-object v0, v10, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$REALTIME_DATA;->extraValue:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2147
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->big_value:Landroid/widget/TextView;

    move-object/from16 v20, v0

    const-string v21, ""

    invoke-virtual/range {v20 .. v21}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2148
    const-string v20, ""

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2150
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, v10, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$REALTIME_DATA;->value:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " / "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    iget-object v0, v10, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$REALTIME_DATA;->extraValue:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v3, v0}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2152
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mDropDownList:Landroid/view/View;

    move-object/from16 v20, v0

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v22

    invoke-interface/range {v22 .. v22}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    const v23, 0x7f09020b

    invoke-virtual/range {v22 .. v23}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    const v23, 0x7f0901ec

    invoke-virtual/range {v22 .. v23}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_9

    .line 2157
    :cond_19
    const/16 v20, 0x3

    move/from16 v0, p1

    move/from16 v1, v20

    if-ne v0, v1, :cond_1b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/lang/Integer;

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Integer;->intValue()I

    move-result v20

    const/16 v21, 0x13

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_1b

    .line 2159
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->big_value:Landroid/widget/TextView;

    move-object/from16 v21, v0

    const/16 v22, 0x1

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->isVikalDevice()Z

    move-result v20

    if-nez v20, :cond_1a

    const/16 v20, 0x46

    :goto_11
    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 2160
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->big_value:Landroid/widget/TextView;

    move-object/from16 v20, v0

    iget-object v0, v10, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$REALTIME_DATA;->value:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2162
    iget-object v0, v10, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$REALTIME_DATA;->unit:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2163
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->big_value:Landroid/widget/TextView;

    move-object/from16 v20, v0

    iget-object v0, v10, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$REALTIME_DATA;->value:Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x13

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->convertToProperUnit(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2165
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v20

    iget-object v0, v10, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$REALTIME_DATA;->unit:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-static/range {v20 .. v21}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->convertToProperUnitsText(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2167
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v20

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, v10, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$REALTIME_DATA;->value:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    iget-object v0, v10, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$REALTIME_DATA;->unit:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    const/16 v22, 0x13

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->convertToProperUnit(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->convertToProperUnitsText(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v3, v0}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2169
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mDropDownList:Landroid/view/View;

    move-object/from16 v20, v0

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v22

    invoke-interface/range {v22 .. v22}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    const v23, 0x7f09020b

    invoke-virtual/range {v22 .. v23}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    const v23, 0x7f0901ec

    invoke-virtual/range {v22 .. v23}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_9

    .line 2159
    :cond_1a
    const/16 v20, 0x28

    goto/16 :goto_11

    .line 2176
    :cond_1b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->big_value:Landroid/widget/TextView;

    move-object/from16 v21, v0

    const/16 v22, 0x1

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->isVikalDevice()Z

    move-result v20

    if-nez v20, :cond_1c

    const/16 v20, 0x46

    :goto_12
    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 2177
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->big_value:Landroid/widget/TextView;

    move-object/from16 v20, v0

    iget-object v0, v10, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$REALTIME_DATA;->value:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2178
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->big_value:Landroid/widget/TextView;

    move-object/from16 v20, v0

    const-string v21, ""

    invoke-virtual/range {v20 .. v21}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2179
    iget-object v0, v10, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$REALTIME_DATA;->unit:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2180
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v20

    iget-object v0, v10, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$REALTIME_DATA;->unit:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-static/range {v20 .. v21}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->convertToProperUnitsText(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2182
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, v10, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$REALTIME_DATA;->value:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->convertToProperUnit(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v21

    iget-object v0, v10, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$REALTIME_DATA;->unit:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-static/range {v21 .. v22}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->convertToProperUnitsText(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v3, v0}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2186
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mDropDownList:Landroid/view/View;

    move-object/from16 v20, v0

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v22

    invoke-interface/range {v22 .. v22}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    const v23, 0x7f09020b

    invoke-virtual/range {v22 .. v23}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    const v23, 0x7f0901ec

    invoke-virtual/range {v22 .. v23}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_9

    .line 2176
    :cond_1c
    const/16 v20, 0x28

    goto/16 :goto_12

    .line 2195
    .end local v4    # "bigContentLayoutParams":Landroid/view/ViewGroup$LayoutParams;
    .end local v5    # "bigLayoutParams":Landroid/view/ViewGroup$LayoutParams;
    .end local v7    # "bigUnitParams":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v8    # "bigValueParams":Landroid/view/ViewGroup$LayoutParams;
    .end local v15    # "scale":F
    .end local v18    # "tv_unit":Landroid/widget/TextView;
    :cond_1d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dataTextViewIDs:[I

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    if-gt v12, v0, :cond_0

    .line 2197
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mView:Landroid/view/View;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dataTextViewIDs:[I

    move-object/from16 v21, v0

    add-int/lit8 v22, v12, -0x1

    aget v21, v21, v22

    invoke-virtual/range {v20 .. v21}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v19

    .line 2198
    .restart local v19    # "view":Landroid/view/View;
    if-eqz v19, :cond_6

    .line 2200
    const v20, 0x7f0806e4

    invoke-virtual/range {v19 .. v20}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;

    .line 2201
    .restart local v16    # "tvScroll":Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;
    const v20, 0x7f0806e1

    invoke-virtual/range {v19 .. v20}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;

    .line 2202
    .local v17, "tvScrollType":Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;
    if-eqz v16, :cond_6

    if-eqz v17, :cond_6

    .line 2203
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/lang/Integer;

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Integer;->intValue()I

    move-result v20

    const/16 v21, 0x13

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_1f

    .line 2204
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, v10, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$REALTIME_DATA;->value:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    iget-object v0, v10, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$REALTIME_DATA;->unit:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v16

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2250
    :cond_1e
    :goto_13
    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;->invalidate()V

    .line 2251
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->setDropDownContentDescription(Landroid/view/View;I)V

    goto/16 :goto_9

    .line 2206
    :cond_1f
    const v20, 0x7f0806e0

    invoke-virtual/range {v19 .. v20}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/HorizontalUnlimitedLayout;

    .line 2210
    .local v11, "dataTypeLayout":Lcom/sec/android/app/shealth/plugins/exercisepro/widget/HorizontalUnlimitedLayout;
    if-eqz v11, :cond_1e

    .line 2231
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivity:Landroid/app/Activity;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v20

    move-object/from16 v0, v20

    iget v9, v0, Landroid/util/DisplayMetrics;->density:F

    .line 2232
    .local v9, "d":F
    new-instance v14, Landroid/widget/RelativeLayout$LayoutParams;

    const/high16 v20, 0x43070000    # 135.0f

    mul-float v20, v20, v9

    move/from16 v0, v20

    float-to-int v0, v0

    move/from16 v20, v0

    const/16 v21, -0x2

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-direct {v14, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2234
    .local v14, "lp":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v20, 0x1

    const v21, 0x7f0806df

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v14, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 2235
    const/16 v20, 0xf

    move/from16 v0, v20

    invoke-virtual {v14, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2236
    invoke-virtual {v11, v14}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/HorizontalUnlimitedLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2243
    const-string v20, "%2"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, v10, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$REALTIME_DATA;->value:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    iget-object v0, v10, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$REALTIME_DATA;->unit:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 2244
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, v10, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$REALTIME_DATA;->value:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    iget-object v0, v10, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$REALTIME_DATA;->unit:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v16

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_13
.end method

.method private updateExerciseFromActivityDB(J)V
    .locals 10
    .param p1, "ExerciseId"    # J

    .prologue
    const/4 v9, 0x0

    const-wide/16 v7, 0x0

    .line 2963
    const/4 v6, 0x0

    .line 2965
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v2, 0x0

    .line 2966
    .local v2, "projection":[Ljava/lang/String;
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "exercise__id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2967
    .local v3, "selectionClause":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$ExerciseActivity;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 2968
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 2969
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 2970
    const-string/jumbo v0, "mean_speed_per_hour"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mAvgSpeed:F

    .line 2971
    const-string/jumbo v0, "max_altitude"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    float-to-double v0, v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mMaxElevation:D

    .line 2972
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mAvgSpeed:F

    cmpg-float v0, v0, v9

    if-gez v0, :cond_0

    .line 2973
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mAvgSpeed:F

    .line 2974
    :cond_0
    iget-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mMaxElevation:D

    cmpg-double v0, v0, v7

    if-gez v0, :cond_1

    .line 2975
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mMaxElevation:D
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2978
    :cond_1
    if-eqz v6, :cond_2

    .line 2979
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 2981
    :cond_2
    return-void

    .line 2978
    .end local v3    # "selectionClause":Ljava/lang/String;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    .line 2979
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method private updateGoalText()V
    .locals 7

    .prologue
    const/4 v6, 0x5

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 3058
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getActivityType()I

    move-result v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeActivityGoalType(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mGoalTypeValue:I

    .line 3059
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mGoalTypeValue:I

    if-ne v1, v6, :cond_0

    .line 3060
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getActivityType()I

    move-result v2

    const/4 v3, 0x7

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getActivityTypeGoalValue(Landroid/content/Context;II)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mGoalValue:I

    .line 3066
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mGoalTypeView:Landroid/view/View;

    const v2, 0x7f080813

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 3067
    .local v0, "tv":Landroid/widget/TextView;
    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setSelected(Z)V

    .line 3068
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mGoalTypeValue:I

    if-ne v1, v6, :cond_1

    .line 3069
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getActivityType()I

    move-result v2

    invoke-static {v1, v2, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getActivityTypeGoalValue(Landroid/content/Context;II)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mTEenginlevel:I

    .line 3072
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mGoalTypeValue:I

    iget v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mTEenginlevel:I

    iget v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mGoalValue:I

    invoke-static {v1, v2, v3, v4, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getGoalStringByType(Landroid/content/Context;IIIZ)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3084
    :goto_1
    return-void

    .line 3063
    .end local v0    # "tv":Landroid/widget/TextView;
    :cond_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getActivityType()I

    move-result v2

    iget v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mGoalTypeValue:I

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getActivityTypeGoalValue(Landroid/content/Context;II)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mGoalValue:I

    goto :goto_0

    .line 3076
    .restart local v0    # "tv":Landroid/widget/TextView;
    :cond_1
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mGoalTypeValue:I

    const/4 v2, 0x6

    if-ne v1, v2, :cond_2

    .line 3077
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mGoalTypeValue:I

    iget v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mGoalValue:I

    invoke-static {v1, v2, v4, v3, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getGoalStringByType(Landroid/content/Context;IIIZ)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 3079
    :cond_2
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mGoalTypeValue:I

    iget v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mGoalValue:I

    invoke-static {v1, v2, v4, v3, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getGoalStringByType(Landroid/content/Context;IIIZ)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method private updateGoalTextByTask()V
    .locals 5

    .prologue
    const/4 v2, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 3087
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getActivityType()I

    move-result v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeActivityGoalType(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mGoalTypeValue:I

    .line 3088
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mGoalTypeValue:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 3089
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$GoalValueTask;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$GoalValueTask;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;Landroid/content/Context;)V

    new-array v1, v2, [Ljava/lang/Integer;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getActivityType()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$GoalValueTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 3095
    :goto_0
    return-void

    .line 3092
    :cond_0
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$GoalValueTask;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$GoalValueTask;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;Landroid/content/Context;)V

    new-array v1, v2, [Ljava/lang/Integer;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getActivityType()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mGoalTypeValue:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$GoalValueTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method private updateIndoorlayout()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 3469
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->isIndoorRecording()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3470
    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->updateModeState(Z)V

    .line 3471
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCoachStatusLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 3472
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mStatusLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 3479
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->reSizeDataLayout()V

    .line 3480
    return-void

    .line 3475
    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->updateModeState(Z)V

    .line 3476
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCoachStatusLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 3477
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mStatusLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method private updateModeState(Z)V
    .locals 2
    .param p1, "enable"    # Z

    .prologue
    .line 1774
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mIndoorOrOutdoor:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 1775
    if-eqz p1, :cond_0

    .line 1776
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mIndoorOrOutdoor:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1780
    :goto_0
    return-void

    .line 1778
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mIndoorOrOutdoor:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method private updateModeText()V
    .locals 4

    .prologue
    .line 3152
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mIndoorOrOutdoor:Landroid/view/View;

    const v3, 0x7f080813

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 3153
    .local v1, "tv":Landroid/widget/TextView;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getCurrentMode()I

    move-result v3

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getModeStringByType(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3154
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mIndoorOrOutdoor:Landroid/view/View;

    const v3, 0x7f080812

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 3155
    .local v0, "iv":Landroid/widget/ImageView;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getCurrentMode()I

    move-result v3

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getModeIconByType(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->setImageDrawable(Landroid/widget/ImageView;Landroid/graphics/drawable/Drawable;)V

    .line 3157
    return-void
.end method

.method private updateMusicControllerLayout(ZLjava/lang/String;Ljava/lang/String;Z)V
    .locals 3
    .param p1, "isPlay"    # Z
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "artist"    # Ljava/lang/String;
    .param p4, "isInit"    # Z

    .prologue
    const v1, 0x7f090a9b

    .line 3647
    if-nez p4, :cond_2

    .line 3648
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 3649
    sget-boolean v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->isMusicPlaying:Z

    if-nez v0, :cond_0

    .line 3650
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->musicTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 3658
    :cond_0
    :goto_0
    if-eqz p1, :cond_3

    .line 3659
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mBtnMusic_paly:Landroid/widget/ImageButton;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020270

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->setImageDrawable(Landroid/widget/ImageView;Landroid/graphics/drawable/Drawable;)V

    .line 3665
    :goto_1
    return-void

    .line 3652
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->musicTitleTextView:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " / "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 3655
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->musicTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 3662
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mBtnMusic_paly:Landroid/widget/ImageButton;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020271

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->setImageDrawable(Landroid/widget/ImageView;Landroid/graphics/drawable/Drawable;)V

    goto :goto_1
.end method

.method private updateOutdoorLayout()V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 3447
    const/4 v0, 0x0

    .line 3449
    .local v0, "state":I
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getWorkoutState()I

    move-result v0

    .line 3450
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getSwitchButton()Landroid/view/View;

    move-result-object v1

    .line 3451
    .local v1, "stichButton":Landroid/view/View;
    packed-switch v0, :pswitch_data_0

    .line 3465
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->refreshFragmentFocusables()V

    .line 3466
    return-void

    .line 3454
    :pswitch_0
    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 3455
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCoachStatusLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 3456
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mStatusLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 3457
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->reSizeDataLayout()V

    goto :goto_0

    .line 3460
    :pswitch_1
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 3461
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCoachStatusLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 3462
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mStatusLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0

    .line 3451
    :pswitch_data_0
    .packed-switch 0x7d0
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private updatedGoalTextByTaskPostExecute()V
    .locals 6

    .prologue
    const/4 v3, 0x5

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 3098
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mGoalTypeView:Landroid/view/View;

    const v2, 0x7f080813

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 3099
    .local v0, "tv":Landroid/widget/TextView;
    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setSelected(Z)V

    .line 3100
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mGoalTypeValue:I

    if-ne v1, v3, :cond_0

    .line 3101
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getActivityType()I

    move-result v2

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getActivityTypeGoalValue(Landroid/content/Context;II)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mTEenginlevel:I

    .line 3104
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mGoalTypeValue:I

    iget v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mTEenginlevel:I

    iget v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mGoalValue:I

    invoke-static {v1, v2, v3, v4, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getGoalStringByType(Landroid/content/Context;IIIZ)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3116
    :goto_0
    return-void

    .line 3108
    :cond_0
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mGoalTypeValue:I

    const/4 v2, 0x6

    if-ne v1, v2, :cond_1

    .line 3109
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mGoalTypeValue:I

    iget v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mGoalValue:I

    invoke-static {v1, v2, v4, v3, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getGoalStringByType(Landroid/content/Context;IIIZ)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 3111
    :cond_1
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mGoalTypeValue:I

    iget v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mGoalValue:I

    invoke-static {v1, v2, v4, v3, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getGoalStringByType(Landroid/content/Context;IIIZ)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public addLocationToPicture(Ljava/lang/String;)Z
    .locals 7
    .param p1, "picturePath"    # Ljava/lang/String;

    .prologue
    .line 3826
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "file://"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 3828
    new-instance v1, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;

    invoke-direct {v1, p1}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;-><init>(Ljava/lang/String;)V

    .line 3829
    .local v1, "photoData":Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mLastLocation:Landroid/location/Location;

    if-eqz v2, :cond_1

    .line 3830
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mLastLocation:Landroid/location/Location;

    invoke-virtual {v2}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    double-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mLastLocation:Landroid/location/Location;

    invoke-virtual {v3}, Landroid/location/Location;->getLongitude()D

    move-result-wide v3

    double-to-float v3, v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->setLocate(FF)V

    .line 3839
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mLastLocation:Landroid/location/Location;

    if-eqz v2, :cond_2

    .line 3841
    const/4 v2, 0x1

    .line 3843
    :goto_1
    return v2

    .line 3833
    :cond_1
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->getFilePath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/common/utils/ExercisePhotoUtils;->getLocateMetaInfo(Ljava/lang/String;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    move-result-object v0

    .line 3834
    .local v0, "location":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;
    if-eqz v0, :cond_0

    .line 3835
    iget-wide v2, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->latitude:D

    double-to-float v2, v2

    iget-wide v3, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->longitude:D

    double-to-float v3, v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->setLocate(FF)V

    goto :goto_0

    .line 3843
    .end local v0    # "location":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;
    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public forceWorkoutStop()V
    .locals 1

    .prologue
    .line 3764
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->forceWorkoutStop()V

    .line 3765
    return-void
.end method

.method public getActionBarTitle()I
    .locals 1

    .prologue
    .line 3847
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    if-eqz v0, :cond_0

    .line 3848
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getActionBarTitleId()I

    move-result v0

    .line 3850
    :goto_0
    return v0

    :cond_0
    const v0, 0x7f090021

    goto :goto_0
.end method

.method public getDataTypeIconByType(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "type"    # I

    .prologue
    const v5, 0x7f020182

    const v4, 0x7f020181

    const v3, 0x7f020188

    const v2, 0x7f020187

    const/16 v1, 0x7d0

    .line 1182
    packed-switch p2, :pswitch_data_0

    .line 1246
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1184
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getWorkoutState()I

    move-result v0

    if-ne v0, v1, :cond_0

    .line 1185
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 1187
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 1190
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getWorkoutState()I

    move-result v0

    if-ne v0, v1, :cond_1

    .line 1191
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02017f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 1193
    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020180

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 1196
    :pswitch_3
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02018a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 1198
    :pswitch_4
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020178

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 1200
    :pswitch_5
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getWorkoutState()I

    move-result v0

    if-ne v0, v1, :cond_2

    .line 1201
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02017d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 1203
    :cond_2
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02017e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 1206
    :pswitch_6
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getWorkoutState()I

    move-result v0

    if-ne v0, v1, :cond_3

    .line 1207
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto/16 :goto_0

    .line 1209
    :cond_3
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto/16 :goto_0

    .line 1212
    :pswitch_7
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getWorkoutState()I

    move-result v0

    if-ne v0, v1, :cond_4

    .line 1213
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto/16 :goto_0

    .line 1215
    :cond_4
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto/16 :goto_0

    .line 1218
    :pswitch_8
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getWorkoutState()I

    move-result v0

    if-ne v0, v1, :cond_5

    .line 1219
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto/16 :goto_0

    .line 1221
    :cond_5
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto/16 :goto_0

    .line 1225
    :pswitch_9
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getWorkoutState()I

    move-result v0

    if-ne v0, v1, :cond_6

    .line 1226
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020183

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto/16 :goto_0

    .line 1228
    :cond_6
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020184

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto/16 :goto_0

    .line 1231
    :pswitch_a
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getWorkoutState()I

    move-result v0

    if-ne v0, v1, :cond_7

    .line 1232
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020185

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto/16 :goto_0

    .line 1234
    :cond_7
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020186

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto/16 :goto_0

    .line 1237
    :pswitch_b
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getWorkoutState()I

    move-result v0

    if-ne v0, v1, :cond_8

    .line 1238
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto/16 :goto_0

    .line 1240
    :cond_8
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto/16 :goto_0

    .line 1243
    :pswitch_c
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02067f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto/16 :goto_0

    .line 1182
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_9
        :pswitch_a
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_b
        :pswitch_c
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_8
    .end packed-switch
.end method

.method public final getWorkoutController()Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;
    .locals 1

    .prologue
    .line 3718
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    return-object v0
.end method

.method public insertHRMdata(I)V
    .locals 1
    .param p1, "hrmValue"    # I

    .prologue
    .line 3822
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->insertHRMData(I)V

    .line 3823
    return-void
.end method

.method public isHRMConnected()Z
    .locals 1

    .prologue
    .line 1597
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    if-eqz v0, :cond_0

    .line 1598
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->isHRMConnected()Z

    move-result v0

    .line 1600
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isWorkoutMinIntervalOver()Z
    .locals 1

    .prologue
    .line 3810
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->isWorkoutMinIntervalOver()Z

    move-result v0

    return v0
.end method

.method public locationOKPressed()V
    .locals 1

    .prologue
    .line 3970
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    if-eqz v0, :cond_0

    .line 3971
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->locationPopupOk()V

    .line 3973
    :cond_0
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 10
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v2, 0x1

    const/4 v1, -0x1

    .line 3200
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivity:Landroid/app/Activity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    if-nez p2, :cond_1

    .line 3261
    :cond_0
    :goto_0
    return-void

    .line 3204
    :cond_1
    const/16 v0, 0x7b

    if-ne p1, v0, :cond_2

    .line 3205
    if-ne p2, v1, :cond_2

    .line 3206
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->updateAcessaryText(Z)V

    goto :goto_0

    .line 3211
    :cond_2
    const/16 v0, 0x7c

    if-ne p1, v0, :cond_6

    .line 3214
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    if-nez v0, :cond_3

    .line 3215
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->TAG:Ljava/lang/String;

    const-string v1, "dispDataTypes is not initialized yet."

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 3219
    :cond_3
    if-ne p2, v1, :cond_0

    .line 3220
    const/4 v8, -0x1

    .line 3221
    .local v8, "type":I
    const/4 v9, -0x1

    .line 3222
    .local v9, "value":I
    const/4 v7, -0x1

    .line 3223
    .local v7, "level":I
    if-eqz p3, :cond_4

    .line 3224
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getActivityType()I

    move-result v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeActivityGoalType(I)I

    move-result v8

    .line 3225
    const-string/jumbo v0, "value"

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v9

    .line 3226
    const-string v0, "level"

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    .line 3227
    invoke-direct {p0, v8, v9, v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->setGoalValue(III)V

    .line 3229
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mAudioCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 3230
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->keepMusicText:Z

    .line 3231
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->updateGoal()V

    .line 3232
    invoke-direct {p0, v8, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispGoalTypeData(IZ)V

    .line 3233
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->updateGoalText()V

    goto :goto_0

    .line 3238
    .end local v7    # "level":I
    .end local v8    # "type":I
    .end local v9    # "value":I
    :cond_6
    const/16 v0, 0x82

    if-ne p1, v0, :cond_0

    .line 3239
    if-eqz p2, :cond_0

    .line 3242
    const/4 v3, 0x0

    .line 3244
    .local v3, "picturePath":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mImageCaptureUri:Landroid/net/Uri;

    if-eqz v0, :cond_7

    .line 3247
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mImageCaptureUri:Landroid/net/Uri;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->getImagePathByUri(Landroid/net/Uri;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 3256
    :goto_1
    if-eqz v3, :cond_0

    .line 3257
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "file://"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v1, v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 3258
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$ExerciseImageScaleCopyTask;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->getPathToImage()Ljava/lang/String;

    move-result-object v4

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getRealtimeService()Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getRealtimeExerciseId()J

    move-result-wide v5

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$ExerciseImageScaleCopyTask;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;J)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$ExerciseImageScaleCopyTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0

    .line 3249
    :cond_7
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mLastTakePhoto:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 3252
    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mLastTakePhoto:Ljava/lang/String;

    .line 3253
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mLastTakePhoto:Ljava/lang/String;

    goto :goto_1
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 269
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onAttach(Landroid/app/Activity;)V

    .line 270
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivity:Landroid/app/Activity;

    .line 271
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 1360
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivity:Landroid/app/Activity;

    if-nez v0, :cond_1

    .line 1366
    :cond_0
    :goto_0
    return-void

    .line 1363
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->onBackPressed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1364
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 10
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v8, 0x1

    .line 1720
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    .line 1721
    .local v3, "id":I
    invoke-virtual {p1, v8}, Landroid/view/View;->setFocusable(Z)V

    .line 1722
    packed-switch v3, :pswitch_data_0

    .line 1771
    :goto_0
    :pswitch_0
    return-void

    .line 1725
    :pswitch_1
    new-instance v4, Landroid/content/Intent;

    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivity:Landroid/app/Activity;

    const-class v9, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;

    invoke-direct {v4, v8, v9}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1726
    .local v4, "intent2":Landroid/content/Intent;
    const/16 v8, 0x7c

    invoke-virtual {p0, v4, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 1729
    .end local v4    # "intent2":Landroid/content/Intent;
    :pswitch_2
    new-array v8, v8, [Landroid/view/View;

    const/4 v9, 0x0

    aput-object p1, v8, v9

    invoke-static {v8}, Lcom/sec/android/app/shealth/common/utils/Utils;->temporarilyDisableClick([Landroid/view/View;)V

    .line 1730
    const/4 v8, 0x2

    new-array v0, v8, [I

    fill-array-data v0, :array_0

    .line 1733
    .local v0, "array":[I
    array-length v8, v0

    new-array v5, v8, [Ljava/lang/String;

    .line 1734
    .local v5, "items":[Ljava/lang/String;
    new-instance v7, Ljava/util/HashMap;

    array-length v8, v0

    invoke-direct {v7, v8}, Ljava/util/HashMap;-><init>(I)V

    .line 1735
    .local v7, "typesMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    const/4 v1, 0x0

    .line 1736
    .local v1, "curIdx":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    array-length v8, v0

    if-ge v2, v8, :cond_1

    .line 1737
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v8

    aget v9, v0, v2

    invoke-static {v8, v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getModeStringByType(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v6

    .line 1738
    .local v6, "name":Ljava/lang/String;
    aput-object v6, v5, v2

    .line 1739
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aget v9, v0, v2

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v7, v8, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1740
    aget v8, v0, v2

    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getCurrentMode()I

    move-result v9

    if-ne v8, v9, :cond_0

    .line 1741
    move v1, v2

    .line 1736
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1747
    .end local v6    # "name":Ljava/lang/String;
    :cond_1
    new-instance v8, Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v9

    invoke-direct {v8, v9, v5, p1}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;-><init>(Landroid/content/Context;[Ljava/lang/String;Landroid/view/View;)V

    iput-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mListPopupIndoorOrOutdoor:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    .line 1748
    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mListPopupIndoorOrOutdoor:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    invoke-virtual {v8, v1}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->setCurrentItem(I)V

    .line 1749
    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mListPopupIndoorOrOutdoor:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    new-instance v9, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$11;

    invoke-direct {v9, p0, v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$11;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;Ljava/util/Map;)V

    invoke-virtual {v8, v9}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->setOnItemClickedListener(Lcom/sec/android/app/shealth/common/commonui/ListPopup$OnItemClickedListener;)V

    .line 1765
    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mListPopupIndoorOrOutdoor:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->show()V

    goto :goto_0

    .line 1768
    .end local v0    # "array":[I
    .end local v1    # "curIdx":I
    .end local v2    # "i":I
    .end local v5    # "items":[Ljava/lang/String;
    .end local v7    # "typesMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    :pswitch_3
    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivity:Landroid/app/Activity;

    check-cast v8, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->startMapActivity()V

    goto :goto_0

    .line 1722
    :pswitch_data_0
    .packed-switch 0x7f080787
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch

    .line 1730
    :array_0
    .array-data 4
        0x1
        0x2
    .end array-data
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/4 v2, 0x1

    .line 2608
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 2610
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v2, :cond_1

    .line 2611
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mLandscape:Z

    .line 2615
    :cond_0
    :goto_0
    return-void

    .line 2612
    :cond_1
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 2613
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mLandscape:Z

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 275
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onCreate(Landroid/os/Bundle;)V

    .line 276
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 277
    .local v0, "config":Landroid/content/res/Configuration;
    iget v2, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 278
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mLandscape:Z

    .line 282
    :goto_0
    new-instance v2, Lcom/amap/api/maps2d/model/PolylineOptions;

    invoke-direct {v2}, Lcom/amap/api/maps2d/model/PolylineOptions;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mPolyLineOption:Lcom/amap/api/maps2d/model/PolylineOptions;

    .line 283
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivity:Landroid/app/Activity;

    check-cast v2, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    invoke-virtual {v2, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->setOnSaveChosenItemListener(Lcom/sec/android/app/shealth/common/commonui/dialog/OnSaveChosenItemListener;)V

    .line 284
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->registerReceiver()V

    .line 285
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.android.app.music.musicservicecommand.checkplaystatus"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 286
    .local v1, "intent":Landroid/content/Intent;
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 287
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 289
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->checkInChinaArea()Z

    move-result v2

    if-nez v2, :cond_0

    .line 290
    const/high16 v2, 0x40e00000    # 7.0f

    iput v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mZoomLevel:F

    .line 291
    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mZoomLevel:F

    invoke-static {v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->saveRealtimeMapZoomLevel(F)V

    .line 293
    :cond_0
    return-void

    .line 280
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_1
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mLandscape:Z

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 9
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v8, 0x0

    const/high16 v7, 0x10000000

    const/4 v6, 0x0

    .line 298
    iput-object p3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mBundle:Landroid/os/Bundle;

    .line 299
    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getLayoutInflater(Landroid/os/Bundle;)Landroid/view/LayoutInflater;

    move-result-object v4

    const v5, 0x7f03019a

    invoke-virtual {v4, v5, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mView:Landroid/view/View;

    .line 301
    new-instance v4, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivity:Landroid/app/Activity;

    invoke-direct {v4, v5, p3}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;-><init>(Landroid/app/Activity;Landroid/os/Bundle;)V

    iput-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    .line 302
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mView:Landroid/view/View;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->setMainView(Landroid/view/View;)V

    .line 303
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mControllerListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$IHealthControllerListener;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->setHealthControllerListener(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$IHealthControllerListener;)V

    .line 304
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->setActionBarOverlayMode(Z)V

    .line 305
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v4, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->setAmapStatusFragment(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)V

    .line 306
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->initLayout()V

    .line 308
    new-instance v4, Landroid/content/Intent;

    const-string v5, "com.sec.android.app.music.musicservicecommand.prev"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mPrevIntent:Landroid/content/Intent;

    .line 309
    new-instance v4, Landroid/content/Intent;

    const-string v5, "com.sec.android.app.music.musicservicecommand.next"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mNextIntent:Landroid/content/Intent;

    .line 310
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mPrevIntent:Landroid/content/Intent;

    invoke-virtual {v4, v7}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 311
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mNextIntent:Landroid/content/Intent;

    invoke-virtual {v4, v7}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 312
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mPrevIntent:Landroid/content/Intent;

    invoke-virtual {v4, v5, v6}, Landroid/content/pm/PackageManager;->queryBroadcastReceivers(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v2

    .line 313
    .local v2, "activities":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    if-eqz v2, :cond_0

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    if-gtz v4, :cond_1

    .line 314
    :cond_0
    sget-object v4, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->TAG:Ljava/lang/String;

    const-string v5, "Inside if condition. Intent is changed"

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 315
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mPrevIntent:Landroid/content/Intent;

    const-string v5, "com.sec.android.app.music.musicservicecommand.previous"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 317
    :cond_1
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mPrevIntent:Landroid/content/Intent;

    invoke-virtual {v4, v5, v6}, Landroid/content/pm/PackageManager;->queryBroadcastReceivers(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 318
    .local v0, "act":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    if-gtz v4, :cond_3

    .line 319
    :cond_2
    sget-object v4, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->TAG:Ljava/lang/String;

    const-string v5, "Inside second if condition. changed intent is useless"

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 320
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mPrevIntent:Landroid/content/Intent;

    const-string v5, "com.samsung.musicplus.musicservicecommand.previous"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 322
    :cond_3
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mNextIntent:Landroid/content/Intent;

    invoke-virtual {v4, v5, v6}, Landroid/content/pm/PackageManager;->queryBroadcastReceivers(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    .line 323
    .local v1, "actNext":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    if-eqz v1, :cond_4

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    if-gtz v4, :cond_5

    .line 324
    :cond_4
    sget-object v4, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->TAG:Ljava/lang/String;

    const-string v5, "Inside next if condition. changed intent is useless"

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 325
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mNextIntent:Landroid/content/Intent;

    const-string v5, "com.samsung.musicplus.musicservicecommand.next"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 328
    :cond_5
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->isVikalDevice()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 329
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mPrevIntent:Landroid/content/Intent;

    const-string v5, "com.sec.android.app.music.musicservicecommand.previous"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 330
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mNextIntent:Landroid/content/Intent;

    const-string v5, "com.sec.android.app.music.musicservicecommand.next"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 334
    :cond_6
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->updateCardView()V

    .line 337
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mView:Landroid/view/View;

    const v5, 0x7f080700

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/amap/api/maps2d/MapView;

    iput-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mMapView:Lcom/amap/api/maps2d/MapView;

    .line 338
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mMapView:Lcom/amap/api/maps2d/MapView;

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mBundle:Landroid/os/Bundle;

    invoke-virtual {v4, v5}, Lcom/amap/api/maps2d/MapView;->onCreate(Landroid/os/Bundle;)V

    .line 340
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivity:Landroid/app/Activity;

    invoke-static {v4}, Lcom/amap/api/maps2d/MapsInitializer;->initialize(Landroid/content/Context;)V

    .line 341
    sget-object v4, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "onCreateView()"

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 347
    :goto_0
    new-instance v4, Landroid/os/Handler;

    invoke-direct {v4}, Landroid/os/Handler;-><init>()V

    new-instance v5, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$1;

    invoke-direct {v5, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$1;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)V

    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 382
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    return-object v4

    .line 342
    :catch_0
    move-exception v3

    .line 344
    .local v3, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->TAG:Ljava/lang/String;

    const-string v5, "AMapServicesNotAvailable Exceptionon in CreateView()"

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1346
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->unregisterReceivers()V

    .line 1347
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mMapView:Lcom/amap/api/maps2d/MapView;

    if-eqz v0, :cond_0

    .line 1348
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mMapView:Lcom/amap/api/maps2d/MapView;

    invoke-virtual {v0}, Lcom/amap/api/maps2d/MapView;->onDestroy()V

    .line 1350
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    if-eqz v0, :cond_1

    .line 1351
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->onDestroy()V

    .line 1352
    :cond_1
    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mLocationHandler:Landroid/os/Handler;

    .line 1353
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onDestroy()V

    .line 1354
    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mBtnMusic_paly:Landroid/widget/ImageButton;

    .line 1355
    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mBtnMusic_previous:Landroid/widget/ImageButton;

    .line 1356
    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mBtnMusic_next:Landroid/widget/ImageButton;

    .line 1357
    return-void
.end method

.method public onGpsStatusChanged(I)V
    .locals 0
    .param p1, "arg0"    # I

    .prologue
    .line 3761
    return-void
.end method

.method public onPause()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1252
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->onPause()V

    .line 1253
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->setHealthControllerListener(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$IHealthControllerListener;)V

    .line 1254
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mMapView:Lcom/amap/api/maps2d/MapView;

    if-eqz v0, :cond_0

    .line 1255
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mMapView:Lcom/amap/api/maps2d/MapView;

    invoke-virtual {v0}, Lcom/amap/api/maps2d/MapView;->onPause()V

    .line 1256
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mPolyIndex:I

    .line 1257
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mLastQueryTime:J

    .line 1258
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mPolyLineOption:Lcom/amap/api/maps2d/model/PolylineOptions;

    if-eqz v0, :cond_1

    .line 1259
    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mPolyLineOption:Lcom/amap/api/maps2d/model/PolylineOptions;

    .line 1260
    new-instance v0, Lcom/amap/api/maps2d/model/PolylineOptions;

    invoke-direct {v0}, Lcom/amap/api/maps2d/model/PolylineOptions;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mPolyLineOption:Lcom/amap/api/maps2d/model/PolylineOptions;

    .line 1264
    :cond_1
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onPause()V

    .line 1265
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    const/16 v2, 0x7d0

    const/4 v3, 0x1

    .line 1269
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    if-nez v0, :cond_0

    .line 1325
    :goto_0
    return-void

    .line 1272
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->popup:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    if-eqz v0, :cond_1

    .line 1273
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->popup:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->dismiss()V

    .line 1274
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->updateAudioGuideButton()V

    .line 1275
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onResume()V

    .line 1276
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mMapView:Lcom/amap/api/maps2d/MapView;

    if-eqz v0, :cond_2

    .line 1277
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mMapView:Lcom/amap/api/maps2d/MapView;

    invoke-virtual {v0}, Lcom/amap/api/maps2d/MapView;->onResume()V

    .line 1278
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getWorkoutState()I

    move-result v0

    if-eq v0, v2, :cond_2

    .line 1279
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mView:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->setUpMapIfNeeded(Landroid/view/View;)V

    .line 1282
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->loadConfigure()V

    .line 1283
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getHealthControllerListener()Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$IHealthControllerListener;

    move-result-object v0

    if-nez v0, :cond_3

    .line 1284
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mControllerListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$IHealthControllerListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->setHealthControllerListener(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$IHealthControllerListener;)V

    .line 1285
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getRealtimeService()Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getWorkoutState()I

    move-result v0

    const/16 v1, 0x7d1

    if-ne v0, v1, :cond_4

    .line 1287
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getRealtimeService()Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getHRMStatus()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_6

    .line 1288
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispHRMData()V

    .line 1294
    :cond_4
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getWorkoutState()I

    move-result v0

    if-ne v0, v2, :cond_7

    .line 1295
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->updateSleepUI()V

    .line 1303
    :goto_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->onResume()V

    .line 1305
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivity:Landroid/app/Activity;

    check-cast v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->isDrawerMenuVisible()Z

    move-result v0

    if-nez v0, :cond_5

    .line 1306
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivity:Landroid/app/Activity;

    check-cast v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->FroceRecreateActionbar()V

    .line 1307
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivity:Landroid/app/Activity;

    check-cast v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getActionBarTitleId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 1309
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getActionBarTitleId()I

    move-result v0

    const v1, 0x7f090021

    if-ne v0, v1, :cond_8

    .line 1310
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivity:Landroid/app/Activity;

    check-cast v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->setActionbarDescription(Z)V

    .line 1317
    :cond_5
    :goto_3
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->keepMusicText:Z

    if-nez v0, :cond_9

    .line 1318
    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->updateMusicPlayerText(Ljava/lang/String;)V

    .line 1321
    :goto_4
    sget-boolean v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->isMusicPlaying:Z

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->play_setContentDescription(Z)V

    .line 1323
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivity:Landroid/app/Activity;

    check-cast v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getActionBarLayout()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f080306

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09020e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1324
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mTvAccuWeather:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    goto/16 :goto_0

    .line 1290
    :cond_6
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCanHideHRMData:Z

    if-eqz v0, :cond_4

    .line 1291
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->hideHRMData()V

    goto/16 :goto_1

    .line 1298
    :cond_7
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->playMapFromCacheDB()V

    .line 1299
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->updateUI()V

    .line 1300
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->updateDataText()V

    goto/16 :goto_2

    .line 1313
    :cond_8
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivity:Landroid/app/Activity;

    check-cast v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->setActionbarDescription(Z)V

    goto :goto_3

    .line 1320
    :cond_9
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->onResume:Z

    goto :goto_4
.end method

.method public onSave(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    const/4 v0, 0x1

    .line 3512
    if-le p1, v0, :cond_0

    .line 3513
    const/4 p1, 0x1

    .line 3514
    :cond_0
    invoke-static {p1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->saveCoachingSetgoalSelect(I)V

    .line 3515
    if-nez p1, :cond_1

    .line 3516
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setFirstbeatTrainingGoal(I)V

    .line 3519
    :goto_0
    return-void

    .line 3518
    :cond_1
    invoke-static {v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setFirstbeatTrainingGoal(I)V

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 3303
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 3304
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 387
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onStart()V

    .line 388
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->updateUI()V

    .line 389
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivity:Landroid/app/Activity;

    check-cast v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getWorkoutState()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->reCreateActionbar(I)V

    .line 390
    return-void
.end method

.method public pauseWorkoutOnBackPress()V
    .locals 1

    .prologue
    .line 3818
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->pauseWorkoutOnBackPress()V

    .line 3819
    return-void
.end method

.method public play_setContentDescription(Z)V
    .locals 8
    .param p1, "isplay"    # Z

    .prologue
    const v7, 0x7f090f35

    const v6, 0x7f090f34

    const v5, 0x7f090f33

    const v4, 0x7f090a9b

    const/4 v3, 0x0

    .line 700
    if-eqz p1, :cond_3

    .line 701
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mBtnMusic_paly:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    .line 702
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mBtnMusic_paly:Landroid/widget/ImageButton;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 704
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mBtnMusic_paly:Landroid/widget/ImageButton;

    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;->TYPE_TOOL_TIP:Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->setHoverLayout(Landroid/view/View;Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;Ljava/lang/String;Ljava/lang/String;)V

    .line 713
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mBtnMusic_previous:Landroid/widget/ImageButton;

    if-eqz v0, :cond_1

    .line 714
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mBtnMusic_previous:Landroid/widget/ImageButton;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 716
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mBtnMusic_previous:Landroid/widget/ImageButton;

    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;->TYPE_TOOL_TIP:Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->setHoverPopupListener(Landroid/view/View;Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;Ljava/lang/String;Ljava/lang/String;)V

    .line 719
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mBtnMusic_next:Landroid/widget/ImageButton;

    if-eqz v0, :cond_2

    .line 720
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mBtnMusic_next:Landroid/widget/ImageButton;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 722
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mBtnMusic_next:Landroid/widget/ImageButton;

    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;->TYPE_TOOL_TIP:Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->setHoverPopupListener(Landroid/view/View;Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;Ljava/lang/String;Ljava/lang/String;)V

    .line 725
    :cond_2
    return-void

    .line 707
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mBtnMusic_paly:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    .line 708
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mBtnMusic_paly:Landroid/widget/ImageButton;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 710
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mBtnMusic_paly:Landroid/widget/ImageButton;

    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;->TYPE_TOOL_TIP:Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->setHoverLayout(Landroid/view/View;Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setHoverLayout(Landroid/view/View;Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;
    .param p2, "type"    # Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;
    .param p3, "mainText"    # Ljava/lang/String;
    .param p4, "subText"    # Ljava/lang/String;

    .prologue
    .line 3858
    invoke-virtual {p1}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v3

    if-nez v3, :cond_0

    .line 3879
    :goto_0
    return-void

    .line 3860
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 3861
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v3, 0x7f03013a

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 3862
    .local v1, "layout":Landroid/view/View;
    const v3, 0x7f0208b2

    invoke-virtual {v1, v3}, Landroid/view/View;->setBackgroundResource(I)V

    .line 3864
    const v3, 0x7f080528

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 3865
    .local v2, "text1":Landroid/widget/TextView;
    invoke-virtual {p1, p3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 3866
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, Landroid/view/View;->setHoverPopupType(I)V

    .line 3867
    invoke-virtual {p1}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/HoverPopupWindow;->setFHGuideLineEnabled(Z)V

    .line 3868
    invoke-virtual {p1}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v3

    new-instance v4, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$15;

    invoke-direct {v4, p0, v2, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$15;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;Landroid/widget/TextView;Landroid/view/View;)V

    invoke-virtual {v3, v4}, Landroid/widget/HoverPopupWindow;->setHoverPopupListener(Landroid/widget/HoverPopupWindow$HoverPopupListener;)V

    goto :goto_0
.end method

.method public setImageCaptureUri(Landroid/net/Uri;)V
    .locals 0
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 3854
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mImageCaptureUri:Landroid/net/Uri;

    .line 3855
    return-void
.end method

.method public setKcalGoalText(I)V
    .locals 9
    .param p1, "goal"    # I

    .prologue
    const v8, 0x7f0900b9

    const/4 v7, 0x2

    const/4 v6, 0x0

    .line 3772
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 3773
    .local v1, "goalText":Landroid/text/SpannableStringBuilder;
    new-instance v0, Landroid/text/SpannableString;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f09006b

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v4}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 3774
    .local v0, "Goal":Landroid/text/SpannableString;
    new-instance v4, Landroid/text/style/StyleSpan;

    invoke-direct {v4, v7}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v0}, Landroid/text/SpannableString;->length()I

    move-result v5

    invoke-virtual {v0, v4, v6, v5, v6}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 3775
    new-instance v3, Landroid/text/SpannableString;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 3776
    .local v3, "value":Landroid/text/SpannableString;
    new-instance v4, Landroid/text/style/StyleSpan;

    const/4 v5, 0x3

    invoke-direct {v4, v5}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v3}, Landroid/text/SpannableString;->length()I

    move-result v5

    invoke-virtual {v3, v4, v6, v5, v6}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 3777
    new-instance v2, Landroid/text/SpannableString;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 3778
    .local v2, "units":Landroid/text/SpannableString;
    new-instance v4, Landroid/text/style/StyleSpan;

    invoke-direct {v4, v7}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v2}, Landroid/text/SpannableString;->length()I

    move-result v5

    invoke-virtual {v2, v4, v6, v5, v6}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 3779
    invoke-virtual {v1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 3780
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mKcalSummaryLayout:Landroid/widget/LinearLayout;

    const v5, 0x7f080777

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3782
    return-void
.end method

.method public showMinWorkoutIntervalPopup()V
    .locals 1

    .prologue
    .line 3814
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->showMinWorkoutIntervalPopup()V

    .line 3815
    return-void
.end method

.method public startActivityForResult(Landroid/content/Intent;I)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "requestCode"    # I

    .prologue
    .line 3195
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0, p1, p2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 3196
    return-void
.end method

.method public updateKcalDataLayout(F)V
    .locals 4
    .param p1, "totalCalorie"    # F

    .prologue
    const v3, 0x7f080776

    .line 593
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mKcalSummaryLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Math;->round(F)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 594
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mKcalSummaryLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Math;->round(F)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090955

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 597
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mGoalAchived:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 598
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCircleProgress:Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->setVisibility(I)V

    .line 599
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCircleProgress:Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;

    const v1, 0x7f02067e

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->setBackgroundResource(I)V

    .line 601
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mKcalGoal:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->setKcalGoalText(I)V

    .line 602
    return-void
.end method

.method public updateMusicPlayerText(Ljava/lang/String;)V
    .locals 10
    .param p1, "Title"    # Ljava/lang/String;

    .prologue
    const v9, 0x7f090a1d

    const v8, 0x7f09020a

    const v7, 0x7f0901ee

    const v6, 0x7f09020b

    .line 1986
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mView:Landroid/view/View;

    const v3, 0x7f08078b

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1987
    .local v0, "musicPlayer":Landroid/view/View;
    const v2, 0x7f080813

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 1988
    .local v1, "tv":Landroid/widget/TextView;
    sget-boolean v2, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->isMusicPlaying:Z

    if-nez v2, :cond_1

    .line 1989
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1990
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2005
    :cond_0
    :goto_0
    return-void

    .line 1995
    :cond_1
    const-string v2, ""

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1997
    const-string v2, "%s / %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object p1, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1998
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method

.method public updateSleepUI()V
    .locals 8

    .prologue
    const v5, 0x9c42

    const/4 v4, -0x1

    .line 1328
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/hcp/GoalUtils;->getLatestGoalData(Landroid/content/Context;Ljava/lang/Integer;)Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    move-result-object v0

    .line 1330
    .local v0, "mExerciseGoalData":Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;
    if-nez v0, :cond_0

    .line 1331
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    .end local v0    # "mExerciseGoalData":Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;
    const/high16 v2, 0x44250000    # 660.0f

    invoke-direct {v0, v2, v5, v4, v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;-><init>(FIII)V

    .line 1335
    .restart local v0    # "mExerciseGoalData":Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;
    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->getValue()F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mKcalGoal:I

    .line 1336
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mCircleProgress:Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->setProgress(I)V

    .line 1337
    new-instance v1, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;-><init>(Landroid/content/Context;)V

    .line 1338
    .local v1, "mergeCal":Lcom/sec/android/app/shealth/common/utils/MergeCalorie;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v4

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;->getMergedCalorieGetEarlier(JJ)F

    move-result v2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->updateKcalDataLayout(F)V

    .line 1340
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->updateCardView()V

    .line 1341
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->updateGoalText()V

    .line 1342
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->updateActivityType()V

    .line 1343
    return-void
.end method

.method public updateUI()V
    .locals 4

    .prologue
    const/4 v3, 0x7

    const/4 v2, 0x1

    .line 3182
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3183
    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->updateDataText(I)V

    .line 3185
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3186
    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->updateDataText(I)V

    .line 3190
    :cond_1
    return-void
.end method

.method public workoutResume()V
    .locals 1

    .prologue
    .line 3768
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->workoutResume()V

    .line 3769
    return-void
.end method
