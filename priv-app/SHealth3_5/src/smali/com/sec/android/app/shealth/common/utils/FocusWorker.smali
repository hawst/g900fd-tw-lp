.class public Lcom/sec/android/app/shealth/common/utils/FocusWorker;
.super Ljava/lang/Object;
.source "FocusWorker.java"


# instance fields
.field private UNSPECIFIED_VIEW_ID:I

.field private mContext:Landroid/content/Context;

.field private mDuplicateViewIdCount:I

.field private mIsMenuAdded:Z

.field private specialViewList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private viewIdCunter:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    const/16 v0, -0x3e8

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/FocusWorker;->viewIdCunter:I

    .line 25
    const/16 v0, 0x270f

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/FocusWorker;->mDuplicateViewIdCount:I

    .line 26
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/FocusWorker;->specialViewList:Ljava/util/List;

    .line 27
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/FocusWorker;->UNSPECIFIED_VIEW_ID:I

    .line 30
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/common/utils/FocusWorker;->mIsMenuAdded:Z

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    const/16 v0, -0x3e8

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/FocusWorker;->viewIdCunter:I

    .line 25
    const/16 v0, 0x270f

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/FocusWorker;->mDuplicateViewIdCount:I

    .line 26
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/FocusWorker;->specialViewList:Ljava/util/List;

    .line 27
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/FocusWorker;->UNSPECIFIED_VIEW_ID:I

    .line 30
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/common/utils/FocusWorker;->mIsMenuAdded:Z

    .line 36
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/utils/FocusWorker;->mContext:Landroid/content/Context;

    .line 37
    return-void
.end method

.method private addAllFocusableChildrenToList(Landroid/view/ViewGroup;Ljava/util/List;Z)V
    .locals 1
    .param p1, "viewGroup"    # Landroid/view/ViewGroup;
    .param p3, "dynamicIdsRequired"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 105
    .local p2, "focusableViews":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    if-nez p1, :cond_1

    .line 126
    :cond_0
    :goto_0
    return-void

    .line 109
    :cond_1
    invoke-direct {p0, p2, p1}, Lcom/sec/android/app/shealth/common/utils/FocusWorker;->isViewGroupAlreadyExist(Ljava/util/List;Landroid/view/ViewGroup;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 113
    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->setMotionEventSplittingEnabled(Z)V

    .line 114
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/common/utils/FocusWorker;->isSpecialView(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    if-nez v0, :cond_3

    .line 116
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/FocusWorker;->specialViewList:Ljava/util/List;

    if-nez v0, :cond_2

    .line 118
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/FocusWorker;->specialViewList:Ljava/util/List;

    .line 120
    :cond_2
    invoke-interface {p2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 121
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/FocusWorker;->specialViewList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 125
    :cond_3
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/common/utils/FocusWorker;->addChildViews(Landroid/view/ViewGroup;Ljava/util/List;Z)V

    goto :goto_0
.end method

.method private addChildViews(Landroid/view/ViewGroup;Ljava/util/List;Z)V
    .locals 19
    .param p1, "viewGroup"    # Landroid/view/ViewGroup;
    .param p3, "dynamicIdsRequired"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 131
    .local p2, "focusableViews":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    const/4 v11, 0x1

    .line 132
    .local v11, "isDeviceMenuPresent":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/common/utils/FocusWorker;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    if-eqz v17, :cond_0

    .line 133
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/common/utils/FocusWorker;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Landroid/view/ViewConfiguration;->hasPermanentMenuKey()Z

    move-result v11

    .line 136
    :cond_0
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    invoke-virtual/range {p1 .. p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v17

    move/from16 v0, v17

    if-ge v9, v0, :cond_5

    .line 137
    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v15

    .line 138
    .local v15, "view":Landroid/view/View;
    if-eqz v15, :cond_4

    .line 140
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1, v15}, Lcom/sec/android/app/shealth/common/utils/FocusWorker;->isViewAlreadyExist(Ljava/util/List;Landroid/view/View;)Z

    move-result v17

    if-nez v17, :cond_1

    invoke-virtual {v15}, Landroid/view/View;->isFocusable()Z

    move-result v17

    if-eqz v17, :cond_1

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/sec/android/app/shealth/common/utils/FocusWorker;->isSpecialView(Landroid/view/View;)Z

    move-result v17

    if-nez v17, :cond_1

    .line 143
    move-object/from16 v0, p2

    invoke-interface {v0, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 150
    :cond_1
    invoke-virtual {v15}, Landroid/view/View;->getVisibility()I

    move-result v17

    if-nez v17, :cond_3

    .line 151
    instance-of v0, v15, Lcom/sec/android/app/shealth/common/utils/FocusUtils$VisibleChildrenProvider;

    move/from16 v17, v0

    if-eqz v17, :cond_2

    move-object/from16 v17, v15

    .line 152
    check-cast v17, Lcom/sec/android/app/shealth/common/utils/FocusUtils$VisibleChildrenProvider;

    invoke-interface/range {v17 .. v17}, Lcom/sec/android/app/shealth/common/utils/FocusUtils$VisibleChildrenProvider;->getVisibleChildren()Ljava/util/List;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_3

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Landroid/view/View;

    .line 155
    .local v16, "visibleChild":Landroid/view/View;
    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 157
    .end local v10    # "i$":Ljava/util/Iterator;
    .end local v16    # "visibleChild":Landroid/view/View;
    :cond_2
    instance-of v0, v15, Landroid/view/ViewGroup;

    move/from16 v17, v0

    if-eqz v17, :cond_3

    move-object/from16 v17, v15

    .line 158
    check-cast v17, Landroid/view/ViewGroup;

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, p2

    move/from16 v3, p3

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/FocusWorker;->addAllFocusableChildrenToList(Landroid/view/ViewGroup;Ljava/util/List;Z)V

    .line 167
    :cond_3
    :try_start_0
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/common/utils/FocusWorker;->mIsMenuAdded:Z

    move/from16 v17, v0

    if-nez v17, :cond_4

    if-nez v11, :cond_4

    invoke-virtual {v15}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v17

    if-eqz v17, :cond_4

    invoke-virtual {v15}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v17

    if-eqz v17, :cond_4

    invoke-virtual {v15}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v17

    const-string v18, "ActionBarView"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v17

    if-eqz v17, :cond_4

    .line 174
    invoke-virtual {v15}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v14

    check-cast v14, Landroid/view/View;

    .line 175
    .local v14, "parentView":Landroid/view/View;
    invoke-virtual {v14}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v4

    .line 179
    .local v4, "absActionBarViewClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v17, "mActionMenuPresenter"

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v6

    .line 181
    .local v6, "actionMenuPresenterField":Ljava/lang/reflect/Field;
    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v6, v0}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 184
    invoke-virtual {v6, v14}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    .line 186
    .local v5, "actionMenuPresenter":Ljava/lang/Object;
    if-eqz v5, :cond_4

    .line 189
    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v17

    const-string/jumbo v18, "mOverflowButton"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v13

    .line 193
    .local v13, "overflowButtonField":Ljava/lang/reflect/Field;
    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v13, v0}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 195
    invoke-virtual {v13, v5}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    .line 197
    .local v12, "overflowButton":Ljava/lang/Object;
    if-eqz v12, :cond_4

    .line 200
    move-object v0, v12

    check-cast v0, Landroid/view/View;

    move-object v7, v0

    .line 208
    .local v7, "actionMenuView":Landroid/view/View;
    invoke-virtual {v7}, Landroid/view/View;->getVisibility()I

    move-result v17

    if-nez v17, :cond_4

    invoke-virtual {v7}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v17

    if-eqz v17, :cond_4

    .line 211
    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/shealth/common/utils/FocusWorker;->mIsMenuAdded:Z

    .line 212
    move-object/from16 v0, p2

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    .line 136
    .end local v4    # "absActionBarViewClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v5    # "actionMenuPresenter":Ljava/lang/Object;
    .end local v6    # "actionMenuPresenterField":Ljava/lang/reflect/Field;
    .end local v7    # "actionMenuView":Landroid/view/View;
    .end local v12    # "overflowButton":Ljava/lang/Object;
    .end local v13    # "overflowButtonField":Ljava/lang/reflect/Field;
    .end local v14    # "parentView":Landroid/view/View;
    :cond_4
    :goto_2
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_0

    .line 220
    :catch_0
    move-exception v8

    .line 221
    .local v8, "ex":Ljava/lang/NoSuchFieldException;
    const-string v17, "FocusWorker"

    move-object/from16 v0, v17

    invoke-static {v0, v8}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 223
    .end local v8    # "ex":Ljava/lang/NoSuchFieldException;
    :catch_1
    move-exception v8

    .line 224
    .local v8, "ex":Ljava/lang/IllegalArgumentException;
    const-string v17, "FocusWorker"

    move-object/from16 v0, v17

    invoke-static {v0, v8}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 225
    .end local v8    # "ex":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v8

    .line 226
    .local v8, "ex":Ljava/lang/IllegalAccessException;
    const-string v17, "FocusWorker"

    move-object/from16 v0, v17

    invoke-static {v0, v8}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 230
    .end local v8    # "ex":Ljava/lang/IllegalAccessException;
    :catch_3
    move-exception v8

    .line 231
    .local v8, "ex":Ljava/lang/Exception;
    const-string v17, "FocusWorker"

    move-object/from16 v0, v17

    invoke-static {v0, v8}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 235
    .end local v8    # "ex":Ljava/lang/Exception;
    .end local v15    # "view":Landroid/view/View;
    :cond_5
    return-void
.end method

.method private getFocusbleViews([Landroid/view/View;)Ljava/util/List;
    .locals 7
    .param p1, "allViews"    # [Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Landroid/view/View;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 453
    new-instance v5, Ljava/util/LinkedList;

    invoke-direct {v5}, Ljava/util/LinkedList;-><init>()V

    .line 454
    .local v5, "tempFocusables":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    move-object v0, p1

    .local v0, "arr$":[Landroid/view/View;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    .line 456
    .local v1, "content":Landroid/view/View;
    instance-of v6, v1, Landroid/view/ViewGroup;

    if-eqz v6, :cond_0

    .line 458
    check-cast v1, Landroid/view/ViewGroup;

    .end local v1    # "content":Landroid/view/View;
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/common/utils/FocusWorker;->getAllFocusableChildViews(Landroid/view/ViewGroup;)Ljava/util/List;

    move-result-object v4

    .line 459
    .local v4, "listOfFocusbleChildViews":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    invoke-interface {v5, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 454
    .end local v4    # "listOfFocusbleChildViews":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 463
    .restart local v1    # "content":Landroid/view/View;
    :cond_0
    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 467
    .end local v1    # "content":Landroid/view/View;
    :cond_1
    return-object v5
.end method

.method private init(Ljava/util/List;ZZ)V
    .locals 0
    .param p2, "isClearLeftAndRightDirectionalRequired"    # Z
    .param p3, "isClearUpAndDownDirectionalRequired"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;ZZ)V"
        }
    .end annotation

    .prologue
    .line 256
    .local p1, "focusables":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/common/utils/FocusWorker;->setViewIdForDynamicallyAddedViews(Ljava/util/List;)V

    .line 257
    invoke-direct {p0, p1, p3}, Lcom/sec/android/app/shealth/common/utils/FocusWorker;->intializeFocusUpAndDownDirectionsForViews(Ljava/util/List;Z)V

    .line 258
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/common/utils/FocusWorker;->intializeFocusLeftAndRightDirectionsForViews(Ljava/util/List;Z)V

    .line 259
    return-void
.end method

.method private intializeFocusLeftAndRightDirectionsForViews(Ljava/util/List;Z)V
    .locals 4
    .param p2, "isClearLeftRightRequired"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 274
    .local p1, "focusables":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 276
    .local v1, "view":Landroid/view/View;
    if-eqz p2, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v2

    .line 277
    .local v2, "viewId":I
    :goto_1
    invoke-virtual {v1, v2}, Landroid/view/View;->setNextFocusRightId(I)V

    .line 278
    invoke-virtual {v1, v2}, Landroid/view/View;->setNextFocusLeftId(I)V

    .line 279
    invoke-virtual {v1, v2}, Landroid/view/View;->setNextFocusForwardId(I)V

    goto :goto_0

    .line 276
    .end local v2    # "viewId":I
    :cond_0
    iget v2, p0, Lcom/sec/android/app/shealth/common/utils/FocusWorker;->UNSPECIFIED_VIEW_ID:I

    goto :goto_1

    .line 283
    .end local v1    # "view":Landroid/view/View;
    :cond_1
    return-void
.end method

.method private intializeFocusUpAndDownDirectionsForViews(Ljava/util/List;Z)V
    .locals 4
    .param p2, "isClearUpDownRequired"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 263
    .local p1, "focusables":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 265
    .local v1, "view":Landroid/view/View;
    if-eqz p2, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v2

    .line 266
    .local v2, "viewId":I
    :goto_1
    invoke-virtual {v1, v2}, Landroid/view/View;->setNextFocusUpId(I)V

    .line 267
    invoke-virtual {v1, v2}, Landroid/view/View;->setNextFocusDownId(I)V

    goto :goto_0

    .line 265
    .end local v2    # "viewId":I
    :cond_0
    iget v2, p0, Lcom/sec/android/app/shealth/common/utils/FocusWorker;->UNSPECIFIED_VIEW_ID:I

    goto :goto_1

    .line 270
    .end local v1    # "view":Landroid/view/View;
    :cond_1
    return-void
.end method

.method private isSpecialView(Landroid/view/View;)Z
    .locals 1
    .param p1, "viewGroup"    # Landroid/view/View;

    .prologue
    .line 246
    instance-of v0, p1, Landroid/widget/ListView;

    if-nez v0, :cond_0

    instance-of v0, p1, Landroid/widget/GridView;

    if-nez v0, :cond_0

    instance-of v0, p1, Landroid/widget/ScrollView;

    if-nez v0, :cond_0

    instance-of v0, p1, Landroid/widget/NumberPicker;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isViewAlreadyExist(Ljava/util/List;Landroid/view/View;)Z
    .locals 1
    .param p2, "view"    # Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;",
            "Landroid/view/View;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 240
    .local p1, "focusableViews":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    invoke-interface {p1, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private isViewGroupAlreadyExist(Ljava/util/List;Landroid/view/ViewGroup;)Z
    .locals 1
    .param p2, "viewGroup"    # Landroid/view/ViewGroup;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;",
            "Landroid/view/ViewGroup;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 251
    .local p1, "focusableViews":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    invoke-interface {p1, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private isViewIdAlreadyAvailable(Landroid/view/View;Ljava/util/List;)Z
    .locals 5
    .param p1, "view"    # Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p2, "focusables":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    const/4 v3, 0x0

    .line 344
    const/4 v1, 0x0

    .line 345
    .local v1, "position":I
    if-nez p2, :cond_0

    move v2, v3

    .line 362
    :goto_0
    return v2

    .line 349
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 351
    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-ne p1, v2, :cond_2

    .line 353
    move v1, v0

    .line 357
    :cond_1
    const/4 v0, 0x0

    :goto_2
    if-ge v0, v1, :cond_4

    .line 359
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v4

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v2

    if-ne v4, v2, :cond_3

    .line 360
    const/4 v2, 0x1

    goto :goto_0

    .line 349
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 357
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    move v2, v3

    .line 362
    goto :goto_0
.end method

.method private processSpecialViews(Ljava/util/List;Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "specialViewList":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    .local p2, "focusableList":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    const/high16 v7, 0x40000

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 368
    if-nez p1, :cond_1

    .line 421
    :cond_0
    return-void

    .line 372
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 375
    .local v1, "focusable":Landroid/view/View;
    instance-of v4, v1, Landroid/widget/ListView;

    if-nez v4, :cond_3

    instance-of v4, v1, Landroid/widget/NumberPicker;

    if-eqz v4, :cond_4

    .line 377
    :cond_3
    invoke-virtual {v1, v6}, Landroid/view/View;->setFocusable(Z)V

    .line 380
    :cond_4
    instance-of v4, v1, Landroid/widget/ListView;

    if-eqz v4, :cond_5

    move-object v3, v1

    .line 382
    check-cast v3, Landroid/widget/ListView;

    .line 383
    .local v3, "listView":Landroid/widget/ListView;
    invoke-virtual {v3, v6}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 386
    .end local v3    # "listView":Landroid/widget/ListView;
    :cond_5
    instance-of v4, v1, Landroid/widget/GridView;

    if-eqz v4, :cond_6

    move-object v4, v1

    .line 388
    check-cast v4, Landroid/view/ViewGroup;

    invoke-virtual {v4, v7}, Landroid/view/ViewGroup;->setDescendantFocusability(I)V

    .line 389
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .local v0, "childList":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    move-object v4, v1

    .line 390
    check-cast v4, Landroid/view/ViewGroup;

    invoke-direct {p0, v4, v0, v5}, Lcom/sec/android/app/shealth/common/utils/FocusWorker;->addChildViews(Landroid/view/ViewGroup;Ljava/util/List;Z)V

    .line 391
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_0

    .line 395
    invoke-direct {p0, v0, v6, v5}, Lcom/sec/android/app/shealth/common/utils/FocusWorker;->init(Ljava/util/List;ZZ)V

    .line 396
    invoke-static {v0, v6, v5}, Lcom/sec/android/app/shealth/common/utils/FocusUtils;->process(Ljava/util/List;ZZ)V

    move-object v4, v1

    .line 398
    check-cast v4, Landroid/view/ViewGroup;

    invoke-static {v4, v0}, Lcom/sec/android/app/shealth/common/utils/FocusUtils;->delegateDirectionsForSpecialView(Landroid/view/ViewGroup;Ljava/util/List;)V

    .line 400
    .end local v0    # "childList":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    :cond_6
    instance-of v4, v1, Landroid/widget/ScrollView;

    if-eqz v4, :cond_7

    move-object v4, v1

    .line 402
    check-cast v4, Landroid/view/ViewGroup;

    invoke-virtual {v4, v7}, Landroid/view/ViewGroup;->setDescendantFocusability(I)V

    .line 403
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .restart local v0    # "childList":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    move-object v4, v1

    .line 404
    check-cast v4, Landroid/view/ViewGroup;

    invoke-direct {p0, v4, v0, v5}, Lcom/sec/android/app/shealth/common/utils/FocusWorker;->addChildViews(Landroid/view/ViewGroup;Ljava/util/List;Z)V

    .line 405
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_0

    .line 409
    invoke-direct {p0, v0, v6, v6}, Lcom/sec/android/app/shealth/common/utils/FocusWorker;->init(Ljava/util/List;ZZ)V

    .line 410
    invoke-static {v0, v5, v5}, Lcom/sec/android/app/shealth/common/utils/FocusUtils;->process(Ljava/util/List;ZZ)V

    move-object v4, v1

    .line 411
    check-cast v4, Landroid/view/ViewGroup;

    invoke-static {v4, v0}, Lcom/sec/android/app/shealth/common/utils/FocusUtils;->delegateDirectionsForSpecialView(Landroid/view/ViewGroup;Ljava/util/List;)V

    .line 412
    invoke-direct {p0, p2, v1, v0}, Lcom/sec/android/app/shealth/common/utils/FocusWorker;->resetDirectionForNonSpecialViews(Ljava/util/List;Landroid/view/View;Ljava/util/List;)V

    .line 413
    invoke-direct {p0, v0, p2}, Lcom/sec/android/app/shealth/common/utils/FocusWorker;->processSpecialViews(Ljava/util/List;Ljava/util/List;)V

    .line 416
    .end local v0    # "childList":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    :cond_7
    instance-of v4, v1, Landroid/widget/NumberPicker;

    if-eqz v4, :cond_2

    .line 418
    check-cast v1, Landroid/view/ViewGroup;

    .end local v1    # "focusable":Landroid/view/View;
    invoke-virtual {v1, v7}, Landroid/view/ViewGroup;->setDescendantFocusability(I)V

    goto :goto_0
.end method

.method private resetDirectionForNonSpecialViews(Ljava/util/List;Landroid/view/View;Ljava/util/List;)V
    .locals 4
    .param p2, "focusable"    # Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;",
            "Landroid/view/View;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 426
    .local p1, "focusableList":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    .local p3, "childList":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_2

    .line 428
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    if-ne v3, p2, :cond_1

    .line 430
    if-eqz v0, :cond_0

    .line 432
    add-int/lit8 v3, v0, -0x1

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 433
    .local v1, "viewLeftToSpecialView":Landroid/view/View;
    if-eqz p3, :cond_0

    .line 435
    const/4 v3, 0x0

    invoke-interface {p3, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getId()I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/view/View;->setNextFocusRightId(I)V

    .line 438
    .end local v1    # "viewLeftToSpecialView":Landroid/view/View;
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-eq v0, v3, :cond_1

    .line 440
    add-int/lit8 v3, v0, 0x1

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 441
    .local v2, "viewRightToSpecialView":Landroid/view/View;
    if-eqz p3, :cond_1

    .line 443
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {p3, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getId()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setNextFocusLeftId(I)V

    .line 426
    .end local v2    # "viewRightToSpecialView":Landroid/view/View;
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 449
    :cond_2
    return-void
.end method

.method private resetviewIdCounter()V
    .locals 1

    .prologue
    .line 74
    const/16 v0, -0x3e8

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/FocusWorker;->viewIdCunter:I

    .line 75
    return-void
.end method

.method private setViewIdForDynamicallyAddedViewGroups(Landroid/view/View;)V
    .locals 5
    .param p1, "vg"    # Landroid/view/View;

    .prologue
    .line 321
    move-object v2, p1

    check-cast v2, Landroid/view/ViewGroup;

    .line 323
    .local v2, "viewGroup":Landroid/view/ViewGroup;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    if-ge v0, v3, :cond_2

    .line 325
    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 326
    .local v1, "view":Landroid/view/View;
    instance-of v3, v1, Landroid/view/ViewGroup;

    if-eqz v3, :cond_1

    .line 328
    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/common/utils/FocusWorker;->setViewIdForDynamicallyAddedViewGroups(Landroid/view/View;)V

    .line 323
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 333
    :cond_1
    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_0

    .line 335
    iget v3, p0, Lcom/sec/android/app/shealth/common/utils/FocusWorker;->viewIdCunter:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lcom/sec/android/app/shealth/common/utils/FocusWorker;->viewIdCunter:I

    invoke-virtual {v1, v3}, Landroid/view/View;->setId(I)V

    goto :goto_1

    .line 340
    .end local v1    # "view":Landroid/view/View;
    :cond_2
    return-void
.end method

.method private setViewIdForDynamicallyAddedViews(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "focusables":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    const/4 v4, -0x1

    .line 287
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 290
    .local v1, "view":Landroid/view/View;
    instance-of v2, v1, Landroid/view/ViewGroup;

    if-eqz v2, :cond_3

    .line 292
    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v2

    if-ne v2, v4, :cond_1

    .line 294
    iget v2, p0, Lcom/sec/android/app/shealth/common/utils/FocusWorker;->viewIdCunter:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/sec/android/app/shealth/common/utils/FocusWorker;->viewIdCunter:I

    invoke-virtual {v1, v2}, Landroid/view/View;->setId(I)V

    .line 296
    :cond_1
    invoke-direct {p0, v1, p1}, Lcom/sec/android/app/shealth/common/utils/FocusWorker;->isViewIdAlreadyAvailable(Landroid/view/View;Ljava/util/List;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 298
    iget v2, p0, Lcom/sec/android/app/shealth/common/utils/FocusWorker;->mDuplicateViewIdCount:I

    add-int/lit8 v3, v2, -0x1

    iput v3, p0, Lcom/sec/android/app/shealth/common/utils/FocusWorker;->mDuplicateViewIdCount:I

    invoke-virtual {v1, v2}, Landroid/view/View;->setId(I)V

    .line 300
    :cond_2
    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/common/utils/FocusWorker;->setViewIdForDynamicallyAddedViewGroups(Landroid/view/View;)V

    goto :goto_0

    .line 305
    :cond_3
    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v2

    if-ne v2, v4, :cond_4

    .line 307
    iget v2, p0, Lcom/sec/android/app/shealth/common/utils/FocusWorker;->viewIdCunter:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/sec/android/app/shealth/common/utils/FocusWorker;->viewIdCunter:I

    invoke-virtual {v1, v2}, Landroid/view/View;->setId(I)V

    .line 309
    :cond_4
    invoke-direct {p0, v1, p1}, Lcom/sec/android/app/shealth/common/utils/FocusWorker;->isViewIdAlreadyAvailable(Landroid/view/View;Ljava/util/List;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 311
    iget v2, p0, Lcom/sec/android/app/shealth/common/utils/FocusWorker;->mDuplicateViewIdCount:I

    add-int/lit8 v3, v2, -0x1

    iput v3, p0, Lcom/sec/android/app/shealth/common/utils/FocusWorker;->mDuplicateViewIdCount:I

    invoke-virtual {v1, v2}, Landroid/view/View;->setId(I)V

    goto :goto_0

    .line 317
    .end local v1    # "view":Landroid/view/View;
    :cond_5
    return-void
.end method


# virtual methods
.method public getAllFocusableChildViews(Landroid/view/ViewGroup;)Ljava/util/List;
    .locals 2
    .param p1, "viewGroup"    # Landroid/view/ViewGroup;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 95
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 97
    .local v0, "focusableChildren":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/app/shealth/common/utils/FocusWorker;->addAllFocusableChildrenToList(Landroid/view/ViewGroup;Ljava/util/List;Z)V

    .line 99
    return-object v0
.end method

.method public refreshFocusables(Landroid/view/View;)V
    .locals 4
    .param p1, "content"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 45
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/utils/FocusWorker;->resetviewIdCounter()V

    .line 47
    instance-of v1, p1, Landroid/view/ViewGroup;

    if-eqz v1, :cond_0

    .line 49
    check-cast p1, Landroid/view/ViewGroup;

    .end local p1    # "content":Landroid/view/View;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/common/utils/FocusWorker;->getAllFocusableChildViews(Landroid/view/ViewGroup;)Ljava/util/List;

    move-result-object v0

    .line 50
    .local v0, "focusables":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    invoke-direct {p0, v0, v3, v3}, Lcom/sec/android/app/shealth/common/utils/FocusWorker;->init(Ljava/util/List;ZZ)V

    .line 51
    invoke-static {v0, v2, v2}, Lcom/sec/android/app/shealth/common/utils/FocusUtils;->process(Ljava/util/List;ZZ)V

    .line 52
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/utils/FocusWorker;->specialViewList:Ljava/util/List;

    invoke-direct {p0, v1, v0}, Lcom/sec/android/app/shealth/common/utils/FocusWorker;->processSpecialViews(Ljava/util/List;Ljava/util/List;)V

    .line 54
    .end local v0    # "focusables":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    :cond_0
    return-void
.end method

.method public varargs refreshFocusables([Landroid/view/View;)V
    .locals 3
    .param p1, "allViews"    # [Landroid/view/View;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 62
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/utils/FocusWorker;->resetviewIdCounter()V

    .line 63
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/common/utils/FocusWorker;->mIsMenuAdded:Z

    .line 64
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/common/utils/FocusWorker;->getFocusbleViews([Landroid/view/View;)Ljava/util/List;

    move-result-object v0

    .line 65
    .local v0, "focusables":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    invoke-direct {p0, v0, v2, v2}, Lcom/sec/android/app/shealth/common/utils/FocusWorker;->init(Ljava/util/List;ZZ)V

    .line 66
    invoke-static {v0, v1, v1}, Lcom/sec/android/app/shealth/common/utils/FocusUtils;->process(Ljava/util/List;ZZ)V

    .line 67
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/utils/FocusWorker;->specialViewList:Ljava/util/List;

    invoke-direct {p0, v1, v0}, Lcom/sec/android/app/shealth/common/utils/FocusWorker;->processSpecialViews(Ljava/util/List;Ljava/util/List;)V

    .line 69
    return-void
.end method
