.class public final Lcom/sec/android/app/shealth/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final ChildHeader:I = 0x7f08016c

.field public static final ChildHeader_f:I = 0x7f08093d

.field public static final ContentView1:I = 0x7f080604

.field public static final ContentView2:I = 0x7f080605

.field public static final ContentView3:I = 0x7f080611

.field public static final ContentView4:I = 0x7f080612

.field public static final HR_text_1:I = 0x7f08075e

.field public static final HR_text_2:I = 0x7f08075f

.field public static final HR_text_3:I = 0x7f08076d

.field public static final LL:I = 0x7f080932

.field public static final Nodevices:I = 0x7f0808d4

.field public static final Scanning_layout:I = 0x7f0808d1

.field public static final aaoubt_bmi_msg_id:I = 0x7f0805ff

.field public static final ab_action_title_holder:I = 0x7f080301

.field public static final ab_btn_parent:I = 0x7f08030d

.field public static final about:I = 0x7f0808f6

.field public static final about_bmi_msg:I = 0x7f0805fe

.field public static final about_bmi_title:I = 0x7f0805fd

.field public static final about_bmi_title1:I = 0x7f080602

.field public static final about_bmi_title2:I = 0x7f080603

.field public static final about_cigna_image:I = 0x7f0800b9

.field public static final about_cigna_scrollview:I = 0x7f0800b8

.field public static final about_cigna_text:I = 0x7f0800ba

.field public static final about_dialog_text:I = 0x7f080014

.field public static final about_title:I = 0x7f080015

.field public static final above_layout:I = 0x7f0803c4

.field public static final accessary_connected_text:I = 0x7f0805ee

.field public static final accessary_connection_icon:I = 0x7f08032c

.field public static final accessary_connection_textview:I = 0x7f080b28

.field public static final accessary_name:I = 0x7f08067b

.field public static final accessory_divider:I = 0x7f0808eb

.field public static final accessory_image:I = 0x7f08034e

.field public static final accessory_list_item:I = 0x7f080098

.field public static final accessory_pair_wizard:I = 0x7f0802e4

.field public static final accessory_pair_wizard_view1:I = 0x7f0802e5

.field public static final accessory_pair_wizard_view2:I = 0x7f0802e6

.field public static final accessory_pair_wizard_view3:I = 0x7f0802e7

.field public static final accessory_pane:I = 0x7f0808e9

.field public static final account_desc_pager:I = 0x7f080621

.field public static final account_desc_pager_footer:I = 0x7f080623

.field public static final account_info_layout:I = 0x7f0808b7

.field public static final account_see_more:I = 0x7f0808c2

.field public static final account_setup:I = 0x7f0808e1

.field public static final accounts:I = 0x7f0808e2

.field public static final accu_weather_layout:I = 0x7f08077c

.field public static final accu_weather_textview:I = 0x7f08077d

.field public static final achievement_text:I = 0x7f080bee

.field public static final actionBarCancelDone:I = 0x7f080311

.field public static final actionBarMain:I = 0x7f0802ff

.field public static final actionBarParent:I = 0x7f0802fe

.field public static final actionBarShareLayout:I = 0x7f08030f

.field public static final actionBarSpinner:I = 0x7f080024

.field public static final actionBardShareTag:I = 0x7f080310

.field public static final actionContentBarLayout:I = 0x7f080300

.field public static final action_bar_button_cancel:I = 0x7f080a6d

.field public static final action_bar_button_done:I = 0x7f080a6e

.field public static final action_bar_tab_one_layout:I = 0x7f080bf8

.field public static final action_bar_tab_two_layout:I = 0x7f080bf7

.field public static final action_bar_title_icon:I = 0x7f080308

.field public static final action_select_button_layout:I = 0x7f080307

.field public static final action_settings:I = 0x7f080caa

.field public static final action_up_button:I = 0x7f080306

.field public static final action_up_button_layout:I = 0x7f080305

.field public static final actionbar_switch:I = 0x7f080025

.field public static final active_msg_root_view:I = 0x7f080b4a

.field public static final activity_level_header:I = 0x7f080863

.field public static final activity_tracker_layout:I = 0x7f080c35

.field public static final activity_tracker_radio_button:I = 0x7f080c37

.field public static final activity_tracker_title:I = 0x7f080c36

.field public static final add_button:I = 0x7f0800b1

.field public static final add_food_scroll_view:I = 0x7f080387

.field public static final add_icon_image:I = 0x7f080515

.field public static final add_photo_layout:I = 0x7f0803dd

.field public static final add_to_my_meals:I = 0x7f080c97

.field public static final additional_info:I = 0x7f080c0f

.field public static final additional_item:I = 0x7f080b81

.field public static final adjust:I = 0x7f080cae

.field public static final age:I = 0x7f080c03

.field public static final age_image:I = 0x7f080bd5

.field public static final age_place_number:I = 0x7f080bd6

.field public static final age_place_ranking:I = 0x7f080bd7

.field public static final age_separator:I = 0x7f080bdd

.field public static final age_slider:I = 0x7f080bde

.field public static final alert_text:I = 0x7f080040

.field public static final alert_title_container:I = 0x7f080032

.field public static final american_heart_association:I = 0x7f08076f

.field public static final analysed_by:I = 0x7f08082a

.field public static final anim_image:I = 0x7f080a85

.field public static final animated_view:I = 0x7f08036b

.field public static final animation_dialog_heartrate0:I = 0x7f080559

.field public static final animation_dialog_heartrate1:I = 0x7f08055a

.field public static final animation_dialog_heartrate_err0:I = 0x7f080555

.field public static final animation_dialog_heartrate_err1:I = 0x7f080556

.field public static final animation_dialog_stress_err0:I = 0x7f0809d1

.field public static final animation_dialog_stress_err1:I = 0x7f0809d2

.field public static final animation_dialog_stress_info0:I = 0x7f0809d6

.field public static final animation_dialog_stress_info1:I = 0x7f0809d7

.field public static final animation_dialog_uv_info0:I = 0x7f080af2

.field public static final animation_dialog_uv_info1:I = 0x7f080af3

.field public static final app_icon:I = 0x7f080338

.field public static final arrow_image:I = 0x7f0806e2

.field public static final assessment_4button_layout:I = 0x7f0800c5

.field public static final assessment_btn_next:I = 0x7f0800e3

.field public static final assessment_first:I = 0x7f0800c6

.field public static final assessment_first_tracker_says:I = 0x7f0800c7

.field public static final assessment_first_tv_num:I = 0x7f0800c8

.field public static final assessment_first_tv_str:I = 0x7f0800c9

.field public static final assessment_fourth:I = 0x7f0800d2

.field public static final assessment_fourth_tracker_says:I = 0x7f0800d3

.field public static final assessment_fourth_tv_num:I = 0x7f0800d4

.field public static final assessment_fourth_tv_str:I = 0x7f0800d5

.field public static final assessment_header_height:I = 0x7f0800df

.field public static final assessment_header_iv_background_image:I = 0x7f0800da

.field public static final assessment_header_scroll:I = 0x7f0800c1

.field public static final assessment_header_weight:I = 0x7f0800e1

.field public static final assessment_layout:I = 0x7f0800bb

.field public static final assessment_reference_header:I = 0x7f0800c2

.field public static final assessment_reference_header_bg:I = 0x7f0800c0

.field public static final assessment_reference_header_iv_background_image:I = 0x7f0800bf

.field public static final assessment_reference_header_txt_sub_title:I = 0x7f0800c4

.field public static final assessment_reference_header_txt_title:I = 0x7f0800c3

.field public static final assessment_second:I = 0x7f0800ca

.field public static final assessment_second_tracker_says:I = 0x7f0800cb

.field public static final assessment_second_tv_num:I = 0x7f0800cc

.field public static final assessment_second_tv_str:I = 0x7f0800cd

.field public static final assessment_third:I = 0x7f0800ce

.field public static final assessment_third_tracker_says:I = 0x7f0800cf

.field public static final assessment_third_tv_num:I = 0x7f0800d0

.field public static final assessment_third_tv_str:I = 0x7f0800d1

.field public static final assessment_txt_sub_title:I = 0x7f0800dd

.field public static final assessment_txt_title:I = 0x7f0800dc

.field public static final assessment_viewpager:I = 0x7f0800d6

.field public static final assessment_weight_header_scroll:I = 0x7f0800db

.field public static final audio_guide_checkbox:I = 0x7f080724

.field public static final audio_guide_menu:I = 0x7f08070e

.field public static final audio_icon:I = 0x7f080722

.field public static final audio_text:I = 0x7f080723

.field public static final auto_backup_bottom:I = 0x7f080054

.field public static final auto_backup_interval_pane:I = 0x7f080052

.field public static final auto_backup_item_layout:I = 0x7f080055

.field public static final auto_backup_wifi_only_checkbox:I = 0x7f080051

.field public static final auto_backup_wifi_only_text:I = 0x7f080050

.field public static final automaic_layout:I = 0x7f080804

.field public static final average_layout:I = 0x7f0807a8

.field public static final average_text:I = 0x7f0807e3

.field public static final average_value:I = 0x7f0807e4

.field public static final avg_portion:I = 0x7f080769

.field public static final award:I = 0x7f080b7c

.field public static final award_account_back_up_now:I = 0x7f08005f

.field public static final award_backup_data_item_area:I = 0x7f080081

.field public static final award_challenge_more_button:I = 0x7f08007d

.field public static final award_goal_data_layout:I = 0x7f080083

.field public static final award_goal_data_value:I = 0x7f08005a

.field public static final award_goal_icon:I = 0x7f080059

.field public static final award_goal_set_value:I = 0x7f080084

.field public static final award_install_plugin_area:I = 0x7f08007a

.field public static final award_install_plugin_item_area:I = 0x7f08007b

.field public static final award_last_data_sync:I = 0x7f08005e

.field public static final award_medal_area_burn:I = 0x7f080064

.field public static final award_medal_area_exercise:I = 0x7f080067

.field public static final award_medal_area_food:I = 0x7f08006a

.field public static final award_medal_area_pedometer:I = 0x7f080061

.field public static final award_medal_count_burn:I = 0x7f080066

.field public static final award_medal_count_exercise:I = 0x7f080069

.field public static final award_medal_count_food:I = 0x7f08006c

.field public static final award_medal_count_pedometer:I = 0x7f080063

.field public static final award_medal_img_burn:I = 0x7f080065

.field public static final award_medal_img_exercise:I = 0x7f080068

.field public static final award_medal_img_food:I = 0x7f08006b

.field public static final award_medal_img_pedometer:I = 0x7f080062

.field public static final award_medal_item_area:I = 0x7f080074

.field public static final award_menu_information:I = 0x7f080c88

.field public static final award_menu_profile:I = 0x7f080c86

.field public static final award_menu_setting:I = 0x7f080c89

.field public static final award_menu_share_via:I = 0x7f080c87

.field public static final award_month_data_area:I = 0x7f080075

.field public static final award_month_data_item_area:I = 0x7f080076

.field public static final award_on_going_challenge_area:I = 0x7f08007c

.field public static final award_on_going_challenge_item_area:I = 0x7f08007f

.field public static final award_samsung_account:I = 0x7f08005d

.field public static final award_set_goal_button:I = 0x7f080082

.field public static final award_summary_scroll_view:I = 0x7f08006d

.field public static final award_today_activity_area:I = 0x7f080077

.field public static final award_today_activity_item_list_area:I = 0x7f080079

.field public static final award_today_activity_line:I = 0x7f080085

.field public static final background_view:I = 0x7f080523

.field public static final backup_data_header:I = 0x7f080080

.field public static final backup_interval_list:I = 0x7f080053

.field public static final backup_layout_header:I = 0x7f08004e

.field public static final backup_layout_header_2:I = 0x7f080086

.field public static final backup_text:I = 0x7f080016

.field public static final badge_achieve_1button_area:I = 0x7f0800e8

.field public static final badge_achieve_2button_area:I = 0x7f0800ea

.field public static final badge_achieve_button_left:I = 0x7f0800eb

.field public static final badge_achieve_button_ok:I = 0x7f0800e9

.field public static final badge_achieve_button_right:I = 0x7f0800ec

.field public static final badge_achieve_description:I = 0x7f0800e7

.field public static final badge_achieve_icon:I = 0x7f0800e6

.field public static final badge_achieve_image_layout:I = 0x7f0800e5

.field public static final badge_achieve_layout:I = 0x7f0800e4

.field public static final badge_list_empty_view:I = 0x7f0800ee

.field public static final badge_list_item_badge_achieve_date:I = 0x7f0800f3

.field public static final badge_list_item_badge_achieve_time:I = 0x7f0800f4

.field public static final badge_list_item_badge_description:I = 0x7f0800f2

.field public static final badge_list_item_badge_name:I = 0x7f0800f1

.field public static final badge_list_item_icon:I = 0x7f0800f0

.field public static final badge_list_item_icon_layout:I = 0x7f0800ef

.field public static final badge_lv_list:I = 0x7f0800ed

.field public static final balance_state_text:I = 0x7f080a86

.field public static final basic:I = 0x7f0808df

.field public static final basic_information_header:I = 0x7f080845

.field public static final be_active_and_healthy:I = 0x7f080b66

.field public static final beat_my_previous_record:I = 0x7f08075c

.field public static final below_list_item_text:I = 0x7f0807fe

.field public static final best_record_layout:I = 0x7f0807ab

.field public static final best_record_title:I = 0x7f0807e6

.field public static final best_record_value:I = 0x7f0807e7

.field public static final bg_activity:I = 0x7f08008f

.field public static final bg_image:I = 0x7f08064e

.field public static final big_content_layout:I = 0x7f080708

.field public static final big_data_icon:I = 0x7f080705

.field public static final big_data_layout:I = 0x7f080703

.field public static final big_data_type:I = 0x7f080706

.field public static final big_extra_value:I = 0x7f08070b

.field public static final big_layout:I = 0x7f080707

.field public static final big_unit:I = 0x7f08070a

.field public static final big_value:I = 0x7f080709

.field public static final birthday_header:I = 0x7f08084e

.field public static final black_layer:I = 0x7f080718

.field public static final blank_dashboard:I = 0x7f0804f6

.field public static final blank_label:I = 0x7f080038

.field public static final block_a_label:I = 0x7f08041c

.field public static final block_a_value:I = 0x7f08041d

.field public static final block_b_label:I = 0x7f08041f

.field public static final block_b_value:I = 0x7f080420

.field public static final blood_glucose_check_box:I = 0x7f080378

.field public static final blood_glucose_line:I = 0x7f080379

.field public static final blood_glucose_radio_group:I = 0x7f08003d

.field public static final blood_glucose_radio_group_layout:I = 0x7f08004c

.field public static final blood_glucose_switch:I = 0x7f08003c

.field public static final blood_glucose_title:I = 0x7f080377

.field public static final blood_pressure_check_box:I = 0x7f08037c

.field public static final blood_pressure_line:I = 0x7f08037d

.field public static final blood_pressure_title:I = 0x7f08037b

.field public static final bmi_check_first:I = 0x7f080095

.field public static final bmi_check_space:I = 0x7f080094

.field public static final bmi_info:I = 0x7f080c78

.field public static final bmi_notice3_id:I = 0x7f080601

.field public static final bmi_notice_id:I = 0x7f080600

.field public static final body_information_header:I = 0x7f08085f

.field public static final bottom:I = 0x7f080012

.field public static final bottom_button:I = 0x7f0807b0

.field public static final bottom_container:I = 0x7f0804a8

.field public static final bottom_grid_view:I = 0x7f080343

.field public static final bottom_layout_for_next_button:I = 0x7f0802eb

.field public static final bottom_section:I = 0x7f080342

.field public static final bpm_third_layout:I = 0x7f0809c6

.field public static final bpm_third_layout_down:I = 0x7f0809c9

.field public static final bt_dialog_ok:I = 0x7f08056a

.field public static final bt_summary_discard:I = 0x7f0805e4

.field public static final bt_summary_second_discard:I = 0x7f0809b7

.field public static final bt_summary_second_retry:I = 0x7f0805c0

.field public static final bt_summary_second_start:I = 0x7f080aff

.field public static final bt_summary_third_discard:I = 0x7f080a03

.field public static final bt_summary_third_start:I = 0x7f080a04

.field public static final btnCamera:I = 0x7f0806f1

.field public static final btnFrom:I = 0x7f080bac

.field public static final btnGallery:I = 0x7f0806f3

.field public static final btnTo:I = 0x7f080bad

.field public static final btn_account_sign_in:I = 0x7f0808c5

.field public static final btn_backup_now:I = 0x7f080089

.field public static final btn_cancel:I = 0x7f0800ad

.field public static final btn_create_tag:I = 0x7f080544

.field public static final btn_date:I = 0x7f0803a4

.field public static final btn_date_from:I = 0x7f0803f9

.field public static final btn_date_to:I = 0x7f0803fa

.field public static final btn_delete_data:I = 0x7f080329

.field public static final btn_dish_minus:I = 0x7f0803ee

.field public static final btn_exercise_mate_graph:I = 0x7f080987

.field public static final btn_exercise_summary:I = 0x7f080356

.field public static final btn_facebook:I = 0x7f0807d2

.field public static final btn_heartrate_summary:I = 0x7f080596

.field public static final btn_more_detail:I = 0x7f0807d4

.field public static final btn_next_period:I = 0x7f0800ab

.field public static final btn_opensource:I = 0x7f08001b

.field public static final btn_previous_period:I = 0x7f0800a8

.field public static final btn_restore_now:I = 0x7f0808b1

.field public static final btn_sleepmonitor_summary:I = 0x7f080942

.field public static final btn_spo2_summary:I = 0x7f0809a4

.field public static final btn_tgh_summary:I = 0x7f080a72

.field public static final btn_time:I = 0x7f0803a5

.field public static final btn_twitter:I = 0x7f0807d3

.field public static final btn_update_weight:I = 0x7f080c83

.field public static final btn_uv_summary:I = 0x7f080b05

.field public static final btn_view_eat_meal:I = 0x7f080404

.field public static final btn_view_edit_meal:I = 0x7f080405

.field public static final btn_view_not_eat_meal:I = 0x7f080406

.field public static final btn_view_nutrition_info:I = 0x7f0803f5

.field public static final btn_walking_graph:I = 0x7f080c25

.field public static final btn_walking_summary:I = 0x7f080b9d

.field public static final btn_weight_graph:I = 0x7f080c84

.field public static final btn_weight_summary:I = 0x7f0809e7

.field public static final btns_margin:I = 0x7f0800b2

.field public static final btns_margin2:I = 0x7f0800b0

.field public static final btns_margin_bottom:I = 0x7f0800b6

.field public static final burn_input_module:I = 0x7f08049b

.field public static final burned_calorie_value:I = 0x7f080c6e

.field public static final burnt_inputmodule_tv_title:I = 0x7f080498

.field public static final buttonLayout1:I = 0x7f080ad3

.field public static final buttonLayout2:I = 0x7f080ad8

.field public static final buttonLayout3:I = 0x7f080add

.field public static final buttonLayout4:I = 0x7f080ae2

.field public static final buttonLayout5:I = 0x7f080ae7

.field public static final buttonLayout6:I = 0x7f080aec

.field public static final button_icon:I = 0x7f080313

.field public static final button_icon_sync:I = 0x7f080c26

.field public static final button_text:I = 0x7f080314

.field public static final button_text_sync:I = 0x7f080c27

.field public static final buttons_layout:I = 0x7f0804d0

.field public static final calendar_button_button:I = 0x7f0800a3

.field public static final calendar_button_button1:I = 0x7f08009e

.field public static final calendar_button_medal:I = 0x7f08009d

.field public static final calendar_button_medal_stub:I = 0x7f0802b3

.field public static final calendar_button_text:I = 0x7f0800a4

.field public static final calendar_button_text1:I = 0x7f08009f

.field public static final calendar_currentmonth:I = 0x7f0800a9

.field public static final calendar_currentyear:I = 0x7f0800aa

.field public static final calendar_date:I = 0x7f0800a6

.field public static final calendar_pager:I = 0x7f0800ac

.field public static final calendar_root_layout:I = 0x7f0800a5

.field public static final calendar_today:I = 0x7f0800a7

.field public static final calorie_below_layout:I = 0x7f0804b5

.field public static final calories:I = 0x7f080428

.field public static final calories_goal:I = 0x7f080750

.field public static final calories_goal_divider:I = 0x7f080755

.field public static final calories_goal_hint:I = 0x7f080753

.field public static final calories_goal_hint_rtl:I = 0x7f080754

.field public static final calories_goal_text:I = 0x7f080751

.field public static final calories_goal_text_rtl:I = 0x7f080752

.field public static final calories_static:I = 0x7f080427

.field public static final calories_text:I = 0x7f0803b1

.field public static final camera_button:I = 0x7f080447

.field public static final camera_live_view:I = 0x7f080399

.field public static final camera_view:I = 0x7f08039e

.field public static final cancel:I = 0x7f0801da

.field public static final cancel_button:I = 0x7f0800af

.field public static final cancel_ok_buttons_container:I = 0x7f080034

.field public static final cancel_ok_buttons_layout:I = 0x7f0800ae

.field public static final cancel_ok_buttons_layout_bottom:I = 0x7f0800b4

.field public static final cancel_unaligned_goal_description:I = 0x7f080232

.field public static final cancel_unaligned_goal_message:I = 0x7f080231

.field public static final cancel_view:I = 0x7f08046f

.field public static final carb_prot_fat:I = 0x7f08047c

.field public static final card_number_1:I = 0x7f080616

.field public static final card_number_2:I = 0x7f080617

.field public static final card_number_3:I = 0x7f080618

.field public static final card_profile:I = 0x7f080619

.field public static final card_view_data_layout:I = 0x7f080728

.field public static final card_view_data_title:I = 0x7f08072a

.field public static final card_view_default_title:I = 0x7f080726

.field public static final card_view_detail1:I = 0x7f08072d

.field public static final card_view_detail2:I = 0x7f08072f

.field public static final card_view_detail3:I = 0x7f080731

.field public static final card_view_distance_val:I = 0x7f08072c

.field public static final card_view_duration_val:I = 0x7f08072e

.field public static final card_view_goal_img:I = 0x7f080729

.field public static final card_view_layout:I = 0x7f08078a

.field public static final card_view_no_data_layout:I = 0x7f080725

.field public static final card_view_no_record_text:I = 0x7f080727

.field public static final card_view_pace_val:I = 0x7f080730

.field public static final category_logo:I = 0x7f08044d

.field public static final category_txt:I = 0x7f0800f5

.field public static final cb_dialog_backup_restore:I = 0x7f080683

.field public static final cb_dialog_backup_restore_coach:I = 0x7f0802b7

.field public static final cb_dialog_show_again:I = 0x7f0804ef

.field public static final cb_dialog_show_again_coach:I = 0x7f0802ba

.field public static final cb_dialog_show_again_txt:I = 0x7f080557

.field public static final cb_favorite:I = 0x7f08033e

.field public static final cb_log_child_body:I = 0x7f08058d

.field public static final cb_no_sensor_dialog_show_again:I = 0x7f080afa

.field public static final cb_pp:I = 0x7f0806b8

.field public static final cb_tag:I = 0x7f0805f8

.field public static final cb_tag_body:I = 0x7f0805fb

.field public static final center:I = 0x7f080010

.field public static final center_view:I = 0x7f0807e5

.field public static final challenger_left:I = 0x7f080bfb

.field public static final challenger_right:I = 0x7f080bfd

.field public static final challengers_separator:I = 0x7f080bfc

.field public static final change_period_day_btn:I = 0x7f0804d2

.field public static final change_period_hour_btn:I = 0x7f0804d1

.field public static final change_period_month_btn:I = 0x7f0804d3

.field public static final change_pin_code:I = 0x7f0808db

.field public static final change_pin_top_text:I = 0x7f0808dc

.field public static final chart_button:I = 0x7f0804ab

.field public static final chart_current_date_txt:I = 0x7f0801e9

.field public static final chart_current_score_txt:I = 0x7f0801ea

.field public static final chart_legend:I = 0x7f080ba5

.field public static final checkAgree:I = 0x7f080285

.field public static final checkAgreeLayout:I = 0x7f080284

.field public static final checkAgreeLayoutRight:I = 0x7f080287

.field public static final checkAgreeLayout_PP:I = 0x7f080a14

.field public static final checkAgreeLayout_Terms:I = 0x7f080a11

.field public static final checkAgreeRight:I = 0x7f080288

.field public static final checkAgree_PP:I = 0x7f080a15

.field public static final checkAgree_Terms:I = 0x7f080a12

.field public static final checkBox1:I = 0x7f08004a

.field public static final checkDoNotShow:I = 0x7f08031b

.field public static final checkDoNotShowLayout:I = 0x7f08031a

.field public static final check_bmi_line:I = 0x7f080096

.field public static final check_box:I = 0x7f08041a

.field public static final check_box_all:I = 0x7f08054d

.field public static final check_box_all_container:I = 0x7f08054c

.field public static final check_box_container:I = 0x7f080419

.field public static final check_box_item:I = 0x7f080172

.field public static final check_box_layout:I = 0x7f080721

.field public static final check_box_save_in_my_food:I = 0x7f080394

.field public static final check_box_save_in_my_food_divider:I = 0x7f080395

.field public static final check_box_save_in_my_food_layout:I = 0x7f080392

.field public static final check_boxes_container:I = 0x7f08040d

.field public static final check_confirm_backup:I = 0x7f0808b3

.field public static final check_enable_data:I = 0x7f080319

.field public static final check_enable_text:I = 0x7f08041b

.field public static final checked_button_left_clickable:I = 0x7f0803a2

.field public static final checked_button_right_clickable:I = 0x7f0803a3

.field public static final child_checkbox:I = 0x7f080162

.field public static final cholesterol:I = 0x7f080434

.field public static final cholesterol_percent:I = 0x7f080435

.field public static final cigna:I = 0x7f080b40

.field public static final cigna_about_coach:I = 0x7f080c93

.field public static final cigna_assessment_controller_layout:I = 0x7f0800d7

.field public static final cigna_assessment_header_indicator_area:I = 0x7f0800bd

.field public static final cigna_assessment_header_indicator_icon:I = 0x7f0800d9

.field public static final cigna_assessment_reference_header_layout:I = 0x7f0800bc

.field public static final cigna_assessment_top_content_area:I = 0x7f0800be

.field public static final cigna_assessment_weight_header_layout:I = 0x7f0800d8

.field public static final cigna_cinnecting_third_party_deivce_and_program_txt_id:I = 0x7f080291

.field public static final cigna_cinnecting_third_party_device_and_program_title_id:I = 0x7f080290

.field public static final cigna_coach_first_time_msg:I = 0x7f080176

.field public static final cigna_coach_guide_description_txt:I = 0x7f0801ae

.field public static final cigna_coach_guide_txt:I = 0x7f0801ad

.field public static final cigna_coach_message_gauge_view:I = 0x7f0800f8

.field public static final cigna_coach_step1_description_txt:I = 0x7f0801b2

.field public static final cigna_coach_step1_name_txt:I = 0x7f0801b1

.field public static final cigna_coach_step1_txt:I = 0x7f0801b0

.field public static final cigna_coach_step2_description_txt:I = 0x7f0801b6

.field public static final cigna_coach_step2_name_txt:I = 0x7f0801b5

.field public static final cigna_coach_step2_txt:I = 0x7f0801b4

.field public static final cigna_coach_step3_description_txt:I = 0x7f0801ba

.field public static final cigna_coach_step3_name_txt:I = 0x7f0801b9

.field public static final cigna_coach_step3_txt:I = 0x7f0801b8

.field public static final cigna_coach_step4_description_txt:I = 0x7f0801be

.field public static final cigna_coach_step4_name_txt:I = 0x7f0801bd

.field public static final cigna_coach_step4_txt:I = 0x7f0801bc

.field public static final cigna_current_goal_change_fragment_btn:I = 0x7f080124

.field public static final cigna_current_goal_coach_message:I = 0x7f080133

.field public static final cigna_current_goal_header_view:I = 0x7f080132

.field public static final cigna_disclaimer_of_warranty_title_id:I = 0x7f080296

.field public static final cigna_disclaimer_of_warranty_txt2_id:I = 0x7f080298

.field public static final cigna_disclaimer_of_warranty_txt_id:I = 0x7f080297

.field public static final cigna_edit_weight_container:I = 0x7f0802a1

.field public static final cigna_edit_weight_text:I = 0x7f0802a4

.field public static final cigna_editable_container:I = 0x7f0801a6

.field public static final cigna_first_time_loading_layout:I = 0x7f080173

.field public static final cigna_first_time_message_layout:I = 0x7f080174

.field public static final cigna_first_time_no_score_msg:I = 0x7f080192

.field public static final cigna_gender_image:I = 0x7f0800de

.field public static final cigna_generales_title_id:I = 0x7f08029c

.field public static final cigna_generales_txt_1_id:I = 0x7f08029d

.field public static final cigna_generales_txt_2_id:I = 0x7f08029e

.field public static final cigna_height_edit:I = 0x7f0801a7

.field public static final cigna_height_edit2:I = 0x7f0801a9

.field public static final cigna_height_inputmodule_dropdownlist:I = 0x7f0801a5

.field public static final cigna_height_view:I = 0x7f0800e0

.field public static final cigna_hello_txt:I = 0x7f080175

.field public static final cigna_icon:I = 0x7f080b41

.field public static final cigna_intro_scree_btn:I = 0x7f0801c0

.field public static final cigna_learn_more:I = 0x7f080c92

.field public static final cigna_libaray:I = 0x7f080c91

.field public static final cigna_library_base_list_empty_txt_message:I = 0x7f0801c4

.field public static final cigna_library_base_list_group_txt_category:I = 0x7f0801c5

.field public static final cigna_library_base_list_item_image_icon:I = 0x7f0801c6

.field public static final cigna_library_base_list_item_txt_subInfomation:I = 0x7f0801c8

.field public static final cigna_library_base_list_item_txt_title:I = 0x7f0801c7

.field public static final cigna_library_base_listview:I = 0x7f0801c3

.field public static final cigna_library_sort_by:I = 0x7f080ca6

.field public static final cigna_lifestyle_comparison_layout:I = 0x7f0801eb

.field public static final cigna_lifestyle_get_your_score:I = 0x7f0801f9

.field public static final cigna_lifestyle_score_bottom_btn_layout:I = 0x7f0801e4

.field public static final cigna_lifestyle_score_bottom_left_btn_layout:I = 0x7f0801df

.field public static final cigna_lifestyle_score_bottom_right_btn_layout:I = 0x7f0801e1

.field public static final cigna_lifestyle_score_message:I = 0x7f0801fa

.field public static final cigna_lifestyle_score_title_txt:I = 0x7f0801fc

.field public static final cigna_lifestyle_score_txt:I = 0x7f0801fd

.field public static final cigna_logList:I = 0x7f0801fe

.field public static final cigna_logo:I = 0x7f080b43

.field public static final cigna_main_change_fragment_btn:I = 0x7f0801ff

.field public static final cigna_main_set_goal_btn_layout:I = 0x7f080122

.field public static final cigna_mission:I = 0x7f080b42

.field public static final cigna_no_score_category_txt:I = 0x7f080205

.field public static final cigna_no_score_exercise:I = 0x7f080179

.field public static final cigna_no_score_exercise_icon_bg:I = 0x7f08017c

.field public static final cigna_no_score_exercise_icon_layout:I = 0x7f08017b

.field public static final cigna_no_score_exercise_layout:I = 0x7f08017a

.field public static final cigna_no_score_exercise_txt:I = 0x7f08017d

.field public static final cigna_no_score_food:I = 0x7f08017e

.field public static final cigna_no_score_food_icon_bg:I = 0x7f080181

.field public static final cigna_no_score_food_icon_layout:I = 0x7f080180

.field public static final cigna_no_score_food_layout:I = 0x7f08017f

.field public static final cigna_no_score_food_txt:I = 0x7f080182

.field public static final cigna_no_score_icon_bg:I = 0x7f080203

.field public static final cigna_no_score_icon_img:I = 0x7f080204

.field public static final cigna_no_score_icon_layout:I = 0x7f080202

.field public static final cigna_no_score_layout:I = 0x7f080200

.field public static final cigna_no_score_sleep:I = 0x7f080183

.field public static final cigna_no_score_sleep_icon_bg:I = 0x7f080186

.field public static final cigna_no_score_sleep_icon_layout:I = 0x7f080185

.field public static final cigna_no_score_sleep_layout:I = 0x7f080184

.field public static final cigna_no_score_sleep_txt:I = 0x7f080187

.field public static final cigna_no_score_stress:I = 0x7f080188

.field public static final cigna_no_score_stress_icon_bg:I = 0x7f08018b

.field public static final cigna_no_score_stress_icon_layout:I = 0x7f08018a

.field public static final cigna_no_score_stress_layout:I = 0x7f080189

.field public static final cigna_no_score_stress_txt:I = 0x7f08018c

.field public static final cigna_no_score_top_margin_view:I = 0x7f08027a

.field public static final cigna_no_score_view:I = 0x7f080201

.field public static final cigna_no_score_weight:I = 0x7f08018d

.field public static final cigna_no_score_weight_icon_bg:I = 0x7f080190

.field public static final cigna_no_score_weight_icon_layout:I = 0x7f08018f

.field public static final cigna_no_score_weight_layout:I = 0x7f08018e

.field public static final cigna_no_score_weight_txt:I = 0x7f080191

.field public static final cigna_parties_title_id:I = 0x7f080299

.field public static final cigna_parties_txt_1_id:I = 0x7f08029a

.field public static final cigna_parties_txt_2_id:I = 0x7f08029b

.field public static final cigna_reassess_lifestyle:I = 0x7f080c95

.field public static final cigna_right_and_licences_title_id:I = 0x7f080292

.field public static final cigna_right_and_licences_txt_1_id:I = 0x7f080293

.field public static final cigna_right_and_licences_txt_2_id:I = 0x7f080294

.field public static final cigna_right_and_licences_txt_3_id:I = 0x7f080295

.field public static final cigna_score_animation_icon_bg_img:I = 0x7f080207

.field public static final cigna_score_animation_icon_img:I = 0x7f080208

.field public static final cigna_score_animation_layout:I = 0x7f080206

.field public static final cigna_score_category_txt:I = 0x7f08020b

.field public static final cigna_score_exercise_view:I = 0x7f08027b

.field public static final cigna_score_food_view:I = 0x7f08027c

.field public static final cigna_score_icon_img:I = 0x7f08020a

.field public static final cigna_score_layout:I = 0x7f080209

.field public static final cigna_score_score_txt:I = 0x7f08020c

.field public static final cigna_score_sleep_view:I = 0x7f08027d

.field public static final cigna_score_stress_view:I = 0x7f08027e

.field public static final cigna_score_weight_view:I = 0x7f08027f

.field public static final cigna_settings:I = 0x7f080c94

.field public static final cigna_start_la_score:I = 0x7f080178

.field public static final cigna_suggest_no_goal_btn_layout:I = 0x7f08024e

.field public static final cigna_suggest_reassess_btn:I = 0x7f08024f

.field public static final cigna_suggested_detail_header_starting_txt:I = 0x7f080277

.field public static final cigna_summary_bottom_txt:I = 0x7f080123

.field public static final cigna_summary_header_assess_btn:I = 0x7f0801e6

.field public static final cigna_summary_header_get_your_score_area:I = 0x7f0801e5

.field public static final cigna_summary_header_message_area:I = 0x7f0801f5

.field public static final cigna_summary_header_score_area:I = 0x7f0801fb

.field public static final cigna_summary_header_view:I = 0x7f080279

.field public static final cigna_use_of_coach_txt_1_id:I = 0x7f08028c

.field public static final cigna_use_of_coach_txt_2_id:I = 0x7f08028d

.field public static final cigna_use_of_coach_txt_3_id:I = 0x7f08028e

.field public static final cigna_use_of_coach_txt_4_id:I = 0x7f08028f

.field public static final cigna_view_badges:I = 0x7f080c8f

.field public static final cigna_view_history:I = 0x7f080c8e

.field public static final cigna_weight_dropdownlist:I = 0x7f0802a3

.field public static final cigna_weight_view:I = 0x7f0800e2

.field public static final cigna_widget_cigna_logo:I = 0x7f0802ac

.field public static final cigna_widget_get_your_score_txt:I = 0x7f0802a8

.field public static final cigna_widget_header_score_area:I = 0x7f0802a7

.field public static final cigna_widget_info_layout:I = 0x7f0802a9

.field public static final cigna_widget_initial_txt:I = 0x7f0802af

.field public static final cigna_widget_mission_info_txt:I = 0x7f0802aa

.field public static final cigna_widget_out_of_txt:I = 0x7f0802b2

.field public static final cigna_widget_plus_btn:I = 0x7f0802ab

.field public static final cigna_widget_root_view:I = 0x7f0802a5

.field public static final cigna_widget_score_txt:I = 0x7f0802b1

.field public static final cigna_widget_socre_layout:I = 0x7f0802b0

.field public static final cigna_widget_title_sub_txt:I = 0x7f0802ae

.field public static final cigna_widget_title_txt:I = 0x7f0802ad

.field public static final cinga_get_started_layout:I = 0x7f0801bf

.field public static final cinga_use_of_coach_title_id:I = 0x7f08028b

.field public static final circle_progress_goal_achived:I = 0x7f080775

.field public static final circle_progress_image:I = 0x7f0802a6

.field public static final circle_view_stub:I = 0x7f0803b7

.field public static final clickRemove:I = 0x7f08000a

.field public static final coach_engage_message:I = 0x7f080226

.field public static final coaching_layout:I = 0x7f08071b

.field public static final coaching_setgoal_checkbox:I = 0x7f080810

.field public static final coaching_status_layout:I = 0x7f08071a

.field public static final comfort_level_discard_text:I = 0x7f0802cc

.field public static final comfort_level_popup_text:I = 0x7f0802c9

.field public static final comfort_zone_setting_main_container:I = 0x7f0802cd

.field public static final comfortzone_setting_humidity:I = 0x7f0802d4

.field public static final comfortzone_setting_humidity_container:I = 0x7f0802d5

.field public static final comfortzone_setting_temperature:I = 0x7f0802ce

.field public static final comfortzone_setting_temperature_container:I = 0x7f0802cf

.field public static final commentTV:I = 0x7f080969

.field public static final company_name:I = 0x7f0802f0

.field public static final compatible_accessories:I = 0x7f0808ec

.field public static final compatible_device_connect:I = 0x7f0802de

.field public static final compatible_device_detail_layout:I = 0x7f0802ef

.field public static final complete_badge_divider:I = 0x7f08011c

.field public static final complete_badge_iv_icon:I = 0x7f080119

.field public static final complete_badge_txt_subTitle:I = 0x7f08011a

.field public static final complete_badge_txt_title:I = 0x7f08011b

.field public static final complete_header_txt_exclamations:I = 0x7f08011d

.field public static final complete_header_txt_header_title:I = 0x7f08011e

.field public static final complete_header_txt_msg:I = 0x7f08011f

.field public static final complete_header_txt_progress_msg_1:I = 0x7f080120

.field public static final complete_header_txt_progress_msg_2:I = 0x7f080121

.field public static final connect_layout:I = 0x7f080821

.field public static final connect_status:I = 0x7f080822

.field public static final connecting_contents_id:I = 0x7f080a1d

.field public static final connecting_id:I = 0x7f080a1c

.field public static final connection_status:I = 0x7f080986

.field public static final connectivity_image:I = 0x7f08009a

.field public static final consent:I = 0x7f0808f3

.field public static final container:I = 0x7f080632

.field public static final container_view_nutrition_info:I = 0x7f0803f4

.field public static final contentParentLayout:I = 0x7f080043

.field public static final contentParentLayout_total:I = 0x7f080035

.field public static final contentParentLayout_value:I = 0x7f080036

.field public static final content_container:I = 0x7f080033

.field public static final content_devider:I = 0x7f080bae

.field public static final content_frame:I = 0x7f080091

.field public static final contentui_dialog_cancel_bottom:I = 0x7f0800b5

.field public static final contentui_dialog_ok_bottom:I = 0x7f0800b7

.field public static final contentui_list_description:I = 0x7f080046

.field public static final contentui_list_layout:I = 0x7f080045

.field public static final controllView:I = 0x7f080bbd

.field public static final controll_view:I = 0x7f0807e9

.field public static final controller_layout:I = 0x7f0807e8

.field public static final copyright_text:I = 0x7f080017

.field public static final count_view:I = 0x7f0806fd

.field public static final current_goal_no_data:I = 0x7f080215

.field public static final current_mission_detail_details_description1:I = 0x7f080137

.field public static final current_mission_detail_details_description2:I = 0x7f080138

.field public static final current_mission_detail_header_view:I = 0x7f080135

.field public static final current_mission_detail_scrollview:I = 0x7f080134

.field public static final current_mission_detail_tip_text_detail:I = 0x7f080148

.field public static final current_mission_detail_tip_text_title:I = 0x7f080147

.field public static final current_mission_detail_tracking_check_txt:I = 0x7f08014c

.field public static final current_mission_detail_tracking_day_of_week_1:I = 0x7f08014f

.field public static final current_mission_detail_tracking_day_of_week_2:I = 0x7f080150

.field public static final current_mission_detail_tracking_day_of_week_3:I = 0x7f080151

.field public static final current_mission_detail_tracking_day_of_week_4:I = 0x7f080152

.field public static final current_mission_detail_tracking_day_of_week_5:I = 0x7f080153

.field public static final current_mission_detail_tracking_day_of_week_6:I = 0x7f080154

.field public static final current_mission_detail_tracking_day_of_week_7:I = 0x7f080155

.field public static final current_mission_detail_tracking_day_txt:I = 0x7f080149

.field public static final current_mission_detail_tracking_disable:I = 0x7f08014d

.field public static final current_mission_detail_tracking_layout:I = 0x7f08014a

.field public static final current_mission_detail_tracking_mode_layout:I = 0x7f08014b

.field public static final current_mission_detail_viewpager:I = 0x7f080146

.field public static final current_mission_header_again_btn:I = 0x7f080140

.field public static final current_mission_header_again_layout:I = 0x7f08013f

.field public static final current_mission_header_complete_date_txt:I = 0x7f08013d

.field public static final current_mission_header_extra_txt:I = 0x7f08013c

.field public static final current_mission_header_icon_img:I = 0x7f080139

.field public static final current_mission_header_title_txt:I = 0x7f08013a

.field public static final current_mission_header_tracker_info_txt:I = 0x7f08013e

.field public static final current_mission_header_xdayleft_txt:I = 0x7f08013b

.field public static final current_mission_tips_layout:I = 0x7f080144

.field public static final current_mission_tracking_btn:I = 0x7f080157

.field public static final current_mission_tracking_btn_layout:I = 0x7f080156

.field public static final current_weight:I = 0x7f080c67

.field public static final custom_detail_state_bar:I = 0x7f080762

.field public static final custom_detail_text_bar:I = 0x7f080763

.field public static final custom_graph_state_bar:I = 0x7f0809e9

.field public static final custom_hr_state_bar:I = 0x7f080507

.field public static final custom_input_state_bar:I = 0x7f0809d0

.field public static final custom_log_state_bar:I = 0x7f0809e6

.field public static final custom_spo2_state_bar:I = 0x7f080508

.field public static final custom_state_chinese_summary_bar:I = 0x7f080b0f

.field public static final custom_state_summary_bar:I = 0x7f0805da

.field public static final custom_state_summary_bar_type2:I = 0x7f0805eb

.field public static final custom_state_summary_small_bar:I = 0x7f0809c8

.field public static final custom_state_summary_small_bar_graph:I = 0x7f0809a8

.field public static final custom_state_summary_small_bar_middle:I = 0x7f0809ba

.field public static final custom_stress_state_bar:I = 0x7f080504

.field public static final custom_summary_bar:I = 0x7f080b0e

.field public static final custom_two_info_view:I = 0x7f0801a3

.field public static final custom_view_holder:I = 0x7f08030b

.field public static final customheader_article_tip_bg_effect:I = 0x7f08015a

.field public static final customheader_background_layout:I = 0x7f080158

.field public static final customheader_iv_background_image:I = 0x7f080159

.field public static final daily_values:I = 0x7f08042b

.field public static final daily_values_info:I = 0x7f080444

.field public static final dash_board_circleview_icon:I = 0x7f080502

.field public static final dash_board_item_date:I = 0x7f080510

.field public static final dash_board_item_icon:I = 0x7f080503

.field public static final dash_board_item_title:I = 0x7f080505

.field public static final dash_board_item_title_second:I = 0x7f080506

.field public static final dash_board_item_value:I = 0x7f080509

.field public static final dash_board_item_value_1:I = 0x7f08050c

.field public static final dash_board_item_value_2:I = 0x7f08050e

.field public static final dash_board_item_value_image1:I = 0x7f08050b

.field public static final dash_board_item_value_image2:I = 0x7f08050d

.field public static final dash_board_item_value_layout:I = 0x7f08050a

.field public static final dash_board_item_value_stress:I = 0x7f08050f

.field public static final dash_board_item_wearable_icon:I = 0x7f080511

.field public static final dashboard_scroll_view:I = 0x7f0804f3

.field public static final data_collected_progress_percent:I = 0x7f080a01

.field public static final data_icon:I = 0x7f0806df

.field public static final data_layout:I = 0x7f080702

.field public static final data_layout_effect_bg:I = 0x7f080701

.field public static final data_pane:I = 0x7f0808ed

.field public static final data_staus_layout:I = 0x7f08071e

.field public static final data_text1:I = 0x7f08070c

.field public static final data_text2:I = 0x7f08070d

.field public static final data_type:I = 0x7f0806e1

.field public static final data_type_layout:I = 0x7f0806e0

.field public static final data_value:I = 0x7f0806e4

.field public static final data_value_layout:I = 0x7f0806e3

.field public static final datas_layout:I = 0x7f08071f

.field public static final date:I = 0x7f08003a

.field public static final date_and_time:I = 0x7f0806e5

.field public static final date_bar:I = 0x7f0804cf

.field public static final date_divider:I = 0x7f080631

.field public static final date_layout:I = 0x7f08031e

.field public static final date_selector:I = 0x7f080a07

.field public static final date_selector_divider:I = 0x7f080324

.field public static final date_switcher:I = 0x7f080321

.field public static final date_time:I = 0x7f080358

.field public static final date_time_buttons:I = 0x7f0803e8

.field public static final date_view:I = 0x7f080a8b

.field public static final dateandtime:I = 0x7f08004b

.field public static final datepicker:I = 0x7f080325

.field public static final day_motion_less:I = 0x7f080972

.field public static final day_percent_text:I = 0x7f080954

.field public static final day_progressBar:I = 0x7f080973

.field public static final day_sleep_area:I = 0x7f080953

.field public static final days_of_week_container:I = 0x7f0800a0

.field public static final db_resetable_app_list:I = 0x7f0808ab

.field public static final delete:I = 0x7f080c8d

.field public static final delete_img:I = 0x7f0804f1

.field public static final desc_text:I = 0x7f080779

.field public static final description_layout:I = 0x7f0802e9

.field public static final detail_map:I = 0x7f080700

.field public static final detail_map_viewmode_btn:I = 0x7f080717

.field public static final detail_text:I = 0x7f0807c3

.field public static final detail_text_add1:I = 0x7f0807c4

.field public static final detail_text_add10:I = 0x7f080896

.field public static final detail_text_add11:I = 0x7f080897

.field public static final detail_text_add12:I = 0x7f080898

.field public static final detail_text_add13:I = 0x7f080899

.field public static final detail_text_add14:I = 0x7f08089a

.field public static final detail_text_add2:I = 0x7f0807c5

.field public static final detail_text_add3:I = 0x7f08088f

.field public static final detail_text_add4:I = 0x7f080890

.field public static final detail_text_add5:I = 0x7f080891

.field public static final detail_text_add6:I = 0x7f080892

.field public static final detail_text_add7:I = 0x7f080893

.field public static final detail_text_add8:I = 0x7f080894

.field public static final detail_text_add9:I = 0x7f080895

.field public static final details_category_txt:I = 0x7f080136

.field public static final details_container:I = 0x7f080738

.field public static final detected_devices_header:I = 0x7f0808cf

.field public static final device:I = 0x7f0802f2

.field public static final device_connected_status:I = 0x7f080020

.field public static final device_desc:I = 0x7f0802f3

.field public static final device_im:I = 0x7f0802f5

.field public static final device_im_layout:I = 0x7f0802f4

.field public static final device_image:I = 0x7f080099

.field public static final device_image_name:I = 0x7f08001d

.field public static final device_list:I = 0x7f08032a

.field public static final device_name:I = 0x7f08001f

.field public static final device_name_layout:I = 0x7f08001e

.field public static final device_sensor_layout:I = 0x7f080c29

.field public static final device_sensor_radio_button:I = 0x7f080c2b

.field public static final device_sensor_title:I = 0x7f080c2a

.field public static final device_type_text:I = 0x7f0808d0

.field public static final device_web:I = 0x7f0802f6

.field public static final deviceinfo_image:I = 0x7f080350

.field public static final dialog_content:I = 0x7f080bfa

.field public static final dialog_text_1:I = 0x7f080687

.field public static final dialog_text_2:I = 0x7f080688

.field public static final dialog_text_3:I = 0x7f080689

.field public static final dialog_top_text:I = 0x7f08032f

.field public static final dietary_fiber:I = 0x7f08043d

.field public static final dietary_fiber_percent:I = 0x7f08043e

.field public static final discard_start_button_layout:I = 0x7f0805e3

.field public static final disclaimer_of_warranty_text_1_id:I = 0x7f080a23

.field public static final disclaimer_of_warranty_text_2_id:I = 0x7f080a24

.field public static final disclaimer_of_warranty_title_id:I = 0x7f080a22

.field public static final dish_cal_text_view:I = 0x7f0803ed

.field public static final dish_name_text_view:I = 0x7f0803ec

.field public static final dish_nutrients_text_view:I = 0x7f0803f2

.field public static final distance:I = 0x7f080c05

.field public static final distance_goal:I = 0x7f080746

.field public static final distance_goal_hint:I = 0x7f080749

.field public static final distance_goal_hint_rtl:I = 0x7f08074a

.field public static final distance_goal_text:I = 0x7f080747

.field public static final distance_goal_text_rtl:I = 0x7f080748

.field public static final divders_parent:I = 0x7f08002b

.field public static final divider:I = 0x7f080129

.field public static final divider1:I = 0x7f08002c

.field public static final divider2:I = 0x7f08002d

.field public static final divider3:I = 0x7f08002e

.field public static final divider4:I = 0x7f08002f

.field public static final divider_backup_interval:I = 0x7f080058

.field public static final divider_total_kcal:I = 0x7f0804a5

.field public static final do_not_show_again:I = 0x7f0807d8

.field public static final dot_block_a_b:I = 0x7f08041e

.field public static final double_line_text:I = 0x7f0804b4

.field public static final double_line_view:I = 0x7f0804b3

.field public static final double_prime:I = 0x7f0801aa

.field public static final drawer_layout:I = 0x7f080090

.field public static final drawer_menu_layout:I = 0x7f080339

.field public static final dropBubLayout:I = 0x7f080814

.field public static final drop_arrow_img:I = 0x7f0807d6

.field public static final drop_down_button:I = 0x7f080424

.field public static final drop_firstBub:I = 0x7f080816

.field public static final drop_firstBubLayout:I = 0x7f080815

.field public static final drop_fourthBub:I = 0x7f08081a

.field public static final drop_icon:I = 0x7f080812

.field public static final drop_secondBub:I = 0x7f080817

.field public static final drop_text:I = 0x7f080813

.field public static final drop_thirdBub:I = 0x7f080818

.field public static final dropdownbox:I = 0x7f0806de

.field public static final dropdownlist:I = 0x7f080704

.field public static final dtv1:I = 0x7f080a68

.field public static final dummyImageView:I = 0x7f080819

.field public static final dummy_edit:I = 0x7f0807e1

.field public static final dummy_focus:I = 0x7f0807c8

.field public static final duration:I = 0x7f0806e6

.field public static final dv_ascent_and_descent:I = 0x7f0806eb

.field public static final dv_averagehr_and_maximunhr:I = 0x7f0806ec

.field public static final dv_averagepace_and_maximumpace:I = 0x7f0806e9

.field public static final dv_averagespd_and_maximumspd:I = 0x7f0806e8

.field public static final dv_distance_and_calories:I = 0x7f0806e7

.field public static final dv_lowelevation_and_highelevation:I = 0x7f0806ea

.field public static final dv_traningeffect_and_recoverytime:I = 0x7f0806ed

.field public static final dynamic_grid:I = 0x7f080030

.field public static final dynamic_grid_wobble_tag:I = 0x7f080013

.field public static final easy_icon:I = 0x7f080b2b

.field public static final easy_icon_dim:I = 0x7f080b2c

.field public static final easy_icon_healthy:I = 0x7f080b2d

.field public static final easy_icon_inactive:I = 0x7f080b2e

.field public static final easy_icon_medal:I = 0x7f080b2f

.field public static final easy_root_view:I = 0x7f080b2a

.field public static final easy_steps_value:I = 0x7f080b30

.field public static final edit_button:I = 0x7f0805f4

.field public static final edit_text:I = 0x7f080807

.field public static final edit_text_calorie_per_serving:I = 0x7f080389

.field public static final edit_text_name_of_food:I = 0x7f080397

.field public static final empty:I = 0x7f080009

.field public static final empty_backup_data:I = 0x7f08005c

.field public static final empty_favorites:I = 0x7f080514

.field public static final empty_layout:I = 0x7f080303

.field public static final engagin_select_button_area:I = 0x7f080227

.field public static final engaging_fifth:I = 0x7f080230

.field public static final engaging_first:I = 0x7f080228

.field public static final engaging_first_tv_str:I = 0x7f080229

.field public static final engaging_fourth:I = 0x7f08022e

.field public static final engaging_fourth_tv_str:I = 0x7f08022f

.field public static final engaging_header_area:I = 0x7f080222

.field public static final engaging_header_content_area:I = 0x7f080223

.field public static final engaging_header_icon:I = 0x7f080224

.field public static final engaging_second:I = 0x7f08022a

.field public static final engaging_second_tv_str:I = 0x7f08022b

.field public static final engaging_third:I = 0x7f08022c

.field public static final engaging_third_tv_str:I = 0x7f08022d

.field public static final enter_calorie_text:I = 0x7f0804a1

.field public static final et_nutrient_title_value_layout:I = 0x7f080316

.field public static final et_nutrient_value:I = 0x7f080317

.field public static final exeercisepro_info_text:I = 0x7f0806f0

.field public static final exercise_calory:I = 0x7f0804ec

.field public static final exercise_custom_state_summary_bar:I = 0x7f080770

.field public static final exercise_info_layout:I = 0x7f0807cb

.field public static final exercise_list:I = 0x7f0804eb

.field public static final exercise_pro_goal_input_module:I = 0x7f080740

.field public static final exercise_pro_goal_title_text:I = 0x7f0807e2

.field public static final exercise_pro_hrm_main_null:I = 0x7f08075d

.field public static final exercise_route_image:I = 0x7f080714

.field public static final exercise_share_content_frame:I = 0x7f080797

.field public static final exercise_share_content_title_frame:I = 0x7f080798

.field public static final exercise_share_main_layout:I = 0x7f080796

.field public static final exercise_state_bar_high:I = 0x7f0807a0

.field public static final exercise_state_bar_low:I = 0x7f08079f

.field public static final exercise_summary_bar:I = 0x7f08079e

.field public static final exercise_summary_polygon:I = 0x7f08079d

.field public static final expand_button:I = 0x7f0804e4

.field public static final expandable_button_right:I = 0x7f080128

.field public static final expandable_child_layout:I = 0x7f080125

.field public static final expandable_cpi_progress:I = 0x7f080127

.field public static final expandable_delete_checkbox:I = 0x7f080164

.field public static final expandable_group_layout:I = 0x7f08012c

.field public static final expandable_image_arrow:I = 0x7f080131

.field public static final expandable_image_icon:I = 0x7f08012d

.field public static final expandable_listView:I = 0x7f08065a

.field public static final expandable_txt_complete:I = 0x7f080165

.field public static final expandable_txt_complete_mission_count:I = 0x7f08012f

.field public static final expandable_txt_date:I = 0x7f080166

.field public static final expandable_txt_info:I = 0x7f08012e

.field public static final expandable_txt_min_frequency:I = 0x7f080130

.field public static final expandable_txt_title:I = 0x7f080126

.field public static final export_data_divider:I = 0x7f0808ef

.field public static final export_data_id:I = 0x7f0808f0

.field public static final exporting_path:I = 0x7f080375

.field public static final extra_text:I = 0x7f08031d

.field public static final fail:I = 0x7f080bc0

.field public static final fastest_speed:I = 0x7f080715

.field public static final fastest_title:I = 0x7f080713

.field public static final favorite_button:I = 0x7f0804aa

.field public static final favorites_list_empty_view:I = 0x7f0801ca

.field public static final favorites_listview:I = 0x7f0801c9

.field public static final female_icon:I = 0x7f08084c

.field public static final female_layout:I = 0x7f08084b

.field public static final female_text:I = 0x7f08084d

.field public static final fill:I = 0x7f08000f

.field public static final firstSyncedGearDevice:I = 0x7f080c12

.field public static final first_box:I = 0x7f0807d9

.field public static final first_card:I = 0x7f080345

.field public static final first_exercise_layout:I = 0x7f080355

.field public static final first_icon:I = 0x7f080b86

.field public static final first_info:I = 0x7f08035a

.field public static final first_info_area:I = 0x7f080b9f

.field public static final first_info_box:I = 0x7f080ba0

.field public static final first_info_data:I = 0x7f08035c

.field public static final first_info_data_unit:I = 0x7f08035d

.field public static final first_info_title:I = 0x7f08035b

.field public static final first_info_unit:I = 0x7f080ba1

.field public static final first_legend_mark_layout:I = 0x7f080644

.field public static final first_sub_tab:I = 0x7f0802f9

.field public static final first_sub_tab_rd_btn:I = 0x7f0802fa

.field public static final first_text:I = 0x7f080528

.field public static final first_time_btn_start:I = 0x7f080177

.field public static final firstbeat:I = 0x7f0807bf

.field public static final firstbeat_layout:I = 0x7f0807be

.field public static final firstbeat_logo:I = 0x7f0807c0

.field public static final firstbeat_logo_img:I = 0x7f0807ba

.field public static final firstbeat_logo_text:I = 0x7f08082d

.field public static final firt_button:I = 0x7f080823

.field public static final five_button_header_scroll:I = 0x7f080225

.field public static final fl_dialog_video:I = 0x7f080558

.field public static final fl_summary_first_layout:I = 0x7f0809f6

.field public static final fl_summary_fourth_graph_layout:I = 0x7f0809a0

.field public static final fl_summary_second_center_pulse_layout:I = 0x7f0805c2

.field public static final fl_summary_second_graph_layout:I = 0x7f080595

.field public static final flingRemove:I = 0x7f08000b

.field public static final floor:I = 0x7f080c04

.field public static final focus_fake_view:I = 0x7f08045a

.field public static final food_add_btn:I = 0x7f0803db

.field public static final food_expandable_fragment_header:I = 0x7f080452

.field public static final food_expandable_list_item_check_box:I = 0x7f0803ab

.field public static final food_expandable_list_item_expand_favorite_layout:I = 0x7f0803ac

.field public static final food_expandable_list_item_expand_indicator:I = 0x7f0803ad

.field public static final food_expandable_list_item_favorite_button:I = 0x7f0803af

.field public static final food_favourite_icon_wrapper:I = 0x7f0803ae

.field public static final food_graph_information_area_amount_view:I = 0x7f0803bc

.field public static final food_graph_information_area_item_second_container_subtitle:I = 0x7f0803be

.field public static final food_graph_information_area_item_subtitle:I = 0x7f0803bb

.field public static final food_info_text:I = 0x7f0803dc

.field public static final food_linearLayout1:I = 0x7f0808f8

.field public static final food_linearLayout2:I = 0x7f0808fc

.field public static final food_linearLayout3:I = 0x7f080900

.field public static final food_meal_item_view_layout_text_area:I = 0x7f0803eb

.field public static final food_meal_time_date_title:I = 0x7f080415

.field public static final food_meal_time_date_value:I = 0x7f080416

.field public static final food_name:I = 0x7f080474

.field public static final food_pick_add_food_button:I = 0x7f080461

.field public static final food_pick_barcode_add_food_divider:I = 0x7f080460

.field public static final food_pick_categories_list:I = 0x7f08044c

.field public static final food_pick_category_content:I = 0x7f08044f

.field public static final food_pick_category_header:I = 0x7f08044e

.field public static final food_pick_category_name_text:I = 0x7f080450

.field public static final food_pick_check_box:I = 0x7f0803d0

.field public static final food_pick_favorite:I = 0x7f0803d1

.field public static final food_pick_fragment_container:I = 0x7f080464

.field public static final food_pick_frequent_list:I = 0x7f080451

.field public static final food_pick_my_food_fragment_child_list_item_calories_field:I = 0x7f080458

.field public static final food_pick_my_food_fragment_child_list_item_caption_field:I = 0x7f080457

.field public static final food_pick_my_food_meals_list_view:I = 0x7f080456

.field public static final food_pick_no_frequent_layout:I = 0x7f080459

.field public static final food_pick_no_search_result:I = 0x7f080467

.field public static final food_pick_no_search_result_bottom_container:I = 0x7f080468

.field public static final food_pick_root_fragment_container:I = 0x7f08044a

.field public static final food_pick_search_barcode_button:I = 0x7f08045f

.field public static final food_pick_search_edit_text:I = 0x7f08045c

.field public static final food_pick_search_fragment:I = 0x7f08044b

.field public static final food_pick_search_line_layout:I = 0x7f08045b

.field public static final food_pick_search_tab:I = 0x7f08046c

.field public static final food_pick_search_tab_host:I = 0x7f080463

.field public static final food_pick_search_voice_barcode_divider:I = 0x7f08045e

.field public static final food_pick_search_voice_button:I = 0x7f08045d

.field public static final food_pick_searched_items:I = 0x7f080466

.field public static final food_pick_searched_items_holder:I = 0x7f080465

.field public static final food_saved_to_my_food_input:I = 0x7f080398

.field public static final food_set_goal_recommended_kcal_text:I = 0x7f080495

.field public static final food_tracker_zoom_in_popup:I = 0x7f08052a

.field public static final footer:I = 0x7f080c58

.field public static final footer_indicator_1:I = 0x7f080624

.field public static final footer_indicator_2:I = 0x7f080625

.field public static final footer_indicator_3:I = 0x7f080626

.field public static final forcuse:I = 0x7f0807b1

.field public static final foreground_image_view:I = 0x7f08063c

.field public static final four_cont:I = 0x7f080920

.field public static final four_info:I = 0x7f080369

.field public static final fourth_lev1_radio_button:I = 0x7f080857

.field public static final fourth_lev1_sub_title:I = 0x7f080856

.field public static final fourth_lev1_title:I = 0x7f080855

.field public static final fourth_lev2_radio_button:I = 0x7f08085c

.field public static final fourth_lev2_sub_title:I = 0x7f08085b

.field public static final fourth_lev2_title:I = 0x7f08085a

.field public static final fourth_lev_1:I = 0x7f080853

.field public static final fourth_lev_2:I = 0x7f080858

.field public static final fragment:I = 0x7f080417

.field public static final fragment_container:I = 0x7f080bf9

.field public static final fragment_content:I = 0x7f0806f4

.field public static final fragment_layout:I = 0x7f0801e3

.field public static final fragment_type_switch:I = 0x7f080794

.field public static final fringe_layout:I = 0x7f080031

.field public static final fristbeat_text:I = 0x7f080811

.field public static final from:I = 0x7f080610

.field public static final from_camera:I = 0x7f0803df

.field public static final from_gallery:I = 0x7f0803e1

.field public static final full_width_popup_btn_cancel:I = 0x7f080196

.field public static final full_width_popup_btn_layout:I = 0x7f080195

.field public static final full_width_popup_btn_ok:I = 0x7f080197

.field public static final full_width_popup_content_container:I = 0x7f080194

.field public static final full_width_popup_txt_title:I = 0x7f080193

.field public static final gallery_button:I = 0x7f080448

.field public static final gallery_slider:I = 0x7f0803b3

.field public static final gallery_view_pager:I = 0x7f0803b2

.field public static final gearDevices:I = 0x7f080c11

.field public static final gear_details_title:I = 0x7f080bce

.field public static final gear_gear_layout:I = 0x7f080c2f

.field public static final gear_gear_radio_button:I = 0x7f080c31

.field public static final gear_gear_title:I = 0x7f080c30

.field public static final gear_log_details:I = 0x7f080bcc

.field public static final gender_image:I = 0x7f080860

.field public static final gender_photo:I = 0x7f080071

.field public static final general_pane:I = 0x7f0808de

.field public static final general_view_container:I = 0x7f08036d

.field public static final goal:I = 0x7f080c99

.field public static final goal_complete_1button_area:I = 0x7f08019c

.field public static final goal_complete_2button_area:I = 0x7f08019e

.field public static final goal_complete_badge_layout:I = 0x7f080199

.field public static final goal_complete_button_left:I = 0x7f08019f

.field public static final goal_complete_button_ok:I = 0x7f08019d

.field public static final goal_complete_button_right:I = 0x7f0801a0

.field public static final goal_complete_layout:I = 0x7f080198

.field public static final goal_date:I = 0x7f080c61

.field public static final goal_description:I = 0x7f08019b

.field public static final goal_label:I = 0x7f080c5d

.field public static final goal_mission_expandable_list:I = 0x7f080163

.field public static final goal_view_expandable_list:I = 0x7f08012a

.field public static final goal_view_list_empty_view:I = 0x7f08012b

.field public static final goal_view_no_data:I = 0x7f080216

.field public static final goal_view_no_data_text:I = 0x7f080217

.field public static final gone_visual_guidance:I = 0x7f080791

.field public static final goto_gear_manager_button:I = 0x7f0802ee

.field public static final gps_hrm_info:I = 0x7f080829

.field public static final gps_signal_icon:I = 0x7f080827

.field public static final gps_signal_status:I = 0x7f080828

.field public static final gradation_view:I = 0x7f08063b

.field public static final graph_info:I = 0x7f0803b4

.field public static final graph_information_area_amount_layout:I = 0x7f0804cd

.field public static final graph_information_area_amount_view:I = 0x7f0803b6

.field public static final graph_information_area_date_value:I = 0x7f0803b5

.field public static final graph_information_area_for_hour_first_container:I = 0x7f0803ba

.field public static final graph_information_area_for_hour_second_container:I = 0x7f0803bd

.field public static final graph_information_area_item_subtitle:I = 0x7f0804ca

.field public static final graph_information_area_item_title_area:I = 0x7f0804cb

.field public static final graph_information_area_item_title_view_avg_label:I = 0x7f0804cc

.field public static final graph_information_area_unit_or_avg_view:I = 0x7f0803b8

.field public static final graph_information_area_unit_view:I = 0x7f0803b9

.field public static final graph_information_area_unit_view_avg_label:I = 0x7f0804ce

.field public static final graph_information_item_container:I = 0x7f0804c9

.field public static final graph_legend:I = 0x7f080353

.field public static final graph_triangular_switch_fragment_button:I = 0x7f080646

.field public static final grid:I = 0x7f0800a2

.field public static final gridInfoPreview:I = 0x7f0804d4

.field public static final grid_layout_for_images:I = 0x7f0803c2

.field public static final gridview:I = 0x7f0808f7

.field public static final group_expand_button:I = 0x7f080168

.field public static final guidance_arrow:I = 0x7f08082b

.field public static final guidance_text:I = 0x7f08082c

.field public static final guide1:I = 0x7f0802ea

.field public static final gv_tag_icon_list:I = 0x7f080639

.field public static final header:I = 0x7f080c7c

.field public static final header_container:I = 0x7f080bcd

.field public static final header_layout:I = 0x7f080802

.field public static final header_text:I = 0x7f080803

.field public static final health_care_summary_content_left_bar:I = 0x7f0804d6

.field public static final health_care_summary_content_right_bar:I = 0x7f0804d7

.field public static final healthy:I = 0x7f080b33

.field public static final heart_button:I = 0x7f080b62

.field public static final heart_rate_check_box:I = 0x7f080380

.field public static final heart_rate_line:I = 0x7f080381

.field public static final heart_rate_title:I = 0x7f08037f

.field public static final height_root_center:I = 0x7f0801a4

.field public static final height_view:I = 0x7f080861

.field public static final help:I = 0x7f080c8a

.field public static final help_coach_guidance:I = 0x7f0801ac

.field public static final help_coach_step1:I = 0x7f0801af

.field public static final help_coach_step2:I = 0x7f0801b3

.field public static final help_coach_step3:I = 0x7f0801b7

.field public static final help_coach_step4:I = 0x7f0801bb

.field public static final help_detail_txt_1:I = 0x7f080930

.field public static final help_detail_txt_2:I = 0x7f080931

.field public static final help_list_hyphen_1_2_1:I = 0x7f080938

.field public static final help_list_item_1:I = 0x7f0808fa

.field public static final help_list_item_1_1:I = 0x7f0804e0

.field public static final help_list_item_1_2:I = 0x7f080926

.field public static final help_list_item_1_2_1:I = 0x7f080937

.field public static final help_list_item_1_3:I = 0x7f080909

.field public static final help_list_item_1_4:I = 0x7f08090b

.field public static final help_list_item_1_5:I = 0x7f08090e

.field public static final help_list_item_2:I = 0x7f0808fe

.field public static final help_list_item_3:I = 0x7f080902

.field public static final help_list_item_4:I = 0x7f08091f

.field public static final help_list_item_5:I = 0x7f080923

.field public static final help_list_item_number_1:I = 0x7f0808f9

.field public static final help_list_item_number_1_1:I = 0x7f0804df

.field public static final help_list_item_number_1_1_forrtl:I = 0x7f08092c

.field public static final help_list_item_number_1_2:I = 0x7f080925

.field public static final help_list_item_number_1_2_forrtl:I = 0x7f08092e

.field public static final help_list_item_number_1_3:I = 0x7f080908

.field public static final help_list_item_number_1_4:I = 0x7f08090a

.field public static final help_list_item_number_1_5:I = 0x7f08090d

.field public static final help_list_item_number_1_forrtl:I = 0x7f0808fb

.field public static final help_list_item_number_2:I = 0x7f0808fd

.field public static final help_list_item_number_2_forrtl:I = 0x7f0808ff

.field public static final help_list_item_number_3:I = 0x7f080901

.field public static final help_list_item_number_3_forrtl:I = 0x7f080903

.field public static final help_list_item_number_4:I = 0x7f08091e

.field public static final help_list_item_number_5:I = 0x7f080922

.field public static final help_scrollview:I = 0x7f0801ab

.field public static final high_hum:I = 0x7f0802bd

.field public static final high_percentile:I = 0x7f080500

.field public static final history_view_expandable_list:I = 0x7f0801a1

.field public static final history_view_list_empty_view:I = 0x7f0801a2

.field public static final holoCircularProgressBar1:I = 0x7f0809fc

.field public static final home_backuprestore:I = 0x7f080c9f

.field public static final home_connect_wearable_device:I = 0x7f0804ed

.field public static final home_edit_favorites:I = 0x7f080c9d

.field public static final home_latest_data_area:I = 0x7f0804f4

.field public static final home_latest_data_item_area:I = 0x7f0804f5

.field public static final home_set_background:I = 0x7f080c9e

.field public static final home_setting:I = 0x7f080ca0

.field public static final hour_day_month_view:I = 0x7f080b9e

.field public static final hour_edit_text_layout:I = 0x7f0807a2

.field public static final hoverGridChild:I = 0x7f080526

.field public static final hr:I = 0x7f080b3e

.field public static final hr_bpm:I = 0x7f080760

.field public static final hr_bpm_non_medical:I = 0x7f080772

.field public static final hr_bpm_unit:I = 0x7f080761

.field public static final hr_bpm_unit_non_medical:I = 0x7f080773

.field public static final hr_button:I = 0x7f080b56

.field public static final hr_high_index:I = 0x7f080766

.field public static final hr_icon:I = 0x7f080b3f

.field public static final hr_low_index:I = 0x7f080764

.field public static final hrm_device_connected_status:I = 0x7f0805ed

.field public static final hrm_discard_button:I = 0x7f0805d0

.field public static final hrm_error_do_not_move_text:I = 0x7f08099d

.field public static final hrm_error_finger_motion:I = 0x7f080553

.field public static final hrm_error_finger_pressure:I = 0x7f080552

.field public static final hrm_error_first_message:I = 0x7f080551

.field public static final hrm_first_measure_warning_2_text:I = 0x7f08099b

.field public static final hrm_first_measure_warning_3_text:I = 0x7f08099c

.field public static final hrm_frame:I = 0x7f0806fe

.field public static final hrm_graph_avg_bpm_text:I = 0x7f0805a2

.field public static final hrm_graph_avg_text:I = 0x7f08059c

.field public static final hrm_graph_avg_value:I = 0x7f0805a1

.field public static final hrm_graph_info_day_month:I = 0x7f0805a5

.field public static final hrm_graph_info_hours:I = 0x7f0805a4

.field public static final hrm_graph_max_bpm_text:I = 0x7f08059e

.field public static final hrm_graph_max_text:I = 0x7f08059a

.field public static final hrm_graph_max_value:I = 0x7f08059d

.field public static final hrm_graph_min_bpm_text:I = 0x7f0805a0

.field public static final hrm_graph_min_text:I = 0x7f08059b

.field public static final hrm_graph_min_value:I = 0x7f08059f

.field public static final hrm_heart_rate_guide_for_flip_cover:I = 0x7f080554

.field public static final hrm_info:I = 0x7f0805a7

.field public static final hrm_information_1:I = 0x7f08055b

.field public static final hrm_information_1_text:I = 0x7f08055c

.field public static final hrm_information_2:I = 0x7f08055d

.field public static final hrm_information_2_text:I = 0x7f08055e

.field public static final hrm_information_3:I = 0x7f08055f

.field public static final hrm_information_3_text:I = 0x7f080560

.field public static final hrm_information_4:I = 0x7f080561

.field public static final hrm_information_4_text:I = 0x7f080562

.field public static final hrm_information_5:I = 0x7f080563

.field public static final hrm_information_5_text:I = 0x7f080564

.field public static final hrm_information_fifth:I = 0x7f080578

.field public static final hrm_information_fifth_rtl:I = 0x7f08057a

.field public static final hrm_information_first:I = 0x7f08056c

.field public static final hrm_information_first_rtl:I = 0x7f08056e

.field public static final hrm_information_fourth:I = 0x7f080575

.field public static final hrm_information_fourth_rtl:I = 0x7f080577

.field public static final hrm_information_second:I = 0x7f08056f

.field public static final hrm_information_second_rtl:I = 0x7f080571

.field public static final hrm_information_sixth:I = 0x7f08057b

.field public static final hrm_information_sixth_rtl:I = 0x7f08057d

.field public static final hrm_information_third:I = 0x7f080572

.field public static final hrm_information_third_rtl:I = 0x7f080574

.field public static final hrm_layout:I = 0x7f0806ff

.field public static final hrm_linearLayout1:I = 0x7f0804de

.field public static final hrm_linearLayout2:I = 0x7f080919

.field public static final hrm_linearLayout3:I = 0x7f08091a

.field public static final hrm_linearLayout4:I = 0x7f08091c

.field public static final hrm_no_sensor_dialog_show_again:I = 0x7f080582

.field public static final hrm_no_sensor_information_text_1:I = 0x7f08057f

.field public static final hrm_no_sensor_information_text_2:I = 0x7f080580

.field public static final hrm_previous_tag_icon:I = 0x7f0805e8

.field public static final hrm_result_layout_medical:I = 0x7f08076e

.field public static final hrm_result_layout_non_medical:I = 0x7f080771

.field public static final hrm_scover__error_titletext:I = 0x7f0805af

.field public static final hrm_scover_error_dialog_ok:I = 0x7f0805b2

.field public static final hrm_scover_error_title_close:I = 0x7f0805b0

.field public static final hrm_scover_title_close:I = 0x7f0805ce

.field public static final hrm_scover_title_close_layout:I = 0x7f0805cd

.field public static final hrm_scover_title_layout:I = 0x7f0805ae

.field public static final hrm_scover_titletext:I = 0x7f0805cf

.field public static final hrm_start_button:I = 0x7f0805f0

.field public static final hrm_tag_btm_divider:I = 0x7f080541

.field public static final hrm_text_1:I = 0x7f08056d

.field public static final hrm_text_2:I = 0x7f080570

.field public static final hrm_text_3:I = 0x7f080573

.field public static final hrm_text_4:I = 0x7f080576

.field public static final hrm_text_5:I = 0x7f080579

.field public static final hrm_text_6:I = 0x7f08057c

.field public static final humidity_from:I = 0x7f0802d6

.field public static final humidity_range:I = 0x7f0802cb

.field public static final humidity_to:I = 0x7f0802d8

.field public static final huminity_unit_first:I = 0x7f0802d7

.field public static final hybrid:I = 0x7f080004

.field public static final hyphen:I = 0x7f080929

.field public static final ib_summary_third_graph:I = 0x7f0805ef

.field public static final icon:I = 0x7f080525

.field public static final icon_boundary:I = 0x7f080635

.field public static final icon_container:I = 0x7f080501

.field public static final icon_dim:I = 0x7f080b59

.field public static final icon_download_image:I = 0x7f08080c

.field public static final icon_healthy:I = 0x7f080b5a

.field public static final icon_image:I = 0x7f0804f0

.field public static final icon_img_view:I = 0x7f080b7f

.field public static final icon_inactive:I = 0x7f080b5b

.field public static final icon_layout:I = 0x7f080b7e

.field public static final icon_medal:I = 0x7f080b5c

.field public static final icon_name:I = 0x7f0804f2

.field public static final icon_paused:I = 0x7f080b58

.field public static final icons_visibility_layout:I = 0x7f08016f

.field public static final il_finished_sunprotectiontime:I = 0x7f080b14

.field public static final imageView1:I = 0x7f080351

.field public static final imageView_status:I = 0x7f08096c

.field public static final image_colon:I = 0x7f0807a4

.field public static final image_container:I = 0x7f08033b

.field public static final image_delete:I = 0x7f0807de

.field public static final image_delete_button:I = 0x7f0803c1

.field public static final image_device_layout:I = 0x7f0802df

.field public static final image_device_view1:I = 0x7f0802e1

.field public static final image_device_view2:I = 0x7f0802e2

.field public static final image_device_view3:I = 0x7f0802e3

.field public static final image_dialog_spo2:I = 0x7f080998

.field public static final image_error_spo2:I = 0x7f080997

.field public static final image_grid_view:I = 0x7f0803da

.field public static final image_holder_01:I = 0x7f0803c7

.field public static final image_holder_02:I = 0x7f0803c8

.field public static final image_holder_03:I = 0x7f0803c9

.field public static final image_holder_04:I = 0x7f0803ca

.field public static final image_holder_05:I = 0x7f0803cb

.field public static final image_holder_06:I = 0x7f0803cc

.field public static final image_holder_07:I = 0x7f0803cd

.field public static final image_holder_08:I = 0x7f0807df

.field public static final image_holder_09:I = 0x7f0807e0

.field public static final image_holder_big:I = 0x7f0803c5

.field public static final image_holder_small_01:I = 0x7f0803c6

.field public static final image_layout:I = 0x7f080838

.field public static final image_panel_container:I = 0x7f080480

.field public static final image_resorce:I = 0x7f0807dd

.field public static final image_resource:I = 0x7f0803c0

.field public static final images_flipper:I = 0x7f0803ce

.field public static final imgExerciseType:I = 0x7f08034c

.field public static final imgGoal:I = 0x7f080349

.field public static final imgIcon:I = 0x7f080bd0

.field public static final imgTimeIcon:I = 0x7f08034a

.field public static final img_device:I = 0x7f0802e0

.field public static final img_logo:I = 0x7f0808b5

.field public static final img_sync:I = 0x7f0808b8

.field public static final inactive_set_time:I = 0x7f080bea

.field public static final inactive_set_time_txt:I = 0x7f080beb

.field public static final inactive_time:I = 0x7f080bf0

.field public static final inactive_time_check_select:I = 0x7f080bf3

.field public static final inactive_time_configuration_value:I = 0x7f080bf2

.field public static final inactive_time_enable:I = 0x7f080bf4

.field public static final inactive_time_select:I = 0x7f080bf1

.field public static final inactive_time_set_time_always:I = 0x7f080baa

.field public static final inactive_time_set_time_always_check:I = 0x7f080bab

.field public static final inactive_time_set_time_setting:I = 0x7f080ba9

.field public static final include1:I = 0x7f080b04

.field public static final indoor_img:I = 0x7f0807d7

.field public static final indoor_info:I = 0x7f08081e

.field public static final indoor_info_btn:I = 0x7f08081f

.field public static final indoor_layout:I = 0x7f080793

.field public static final indoor_status:I = 0x7f080820

.field public static final info:I = 0x7f080cad

.field public static final info_left_count_txt:I = 0x7f08015d

.field public static final info_left_layout:I = 0x7f08015c

.field public static final info_left_title_txt:I = 0x7f08015e

.field public static final info_right_count_txt:I = 0x7f080160

.field public static final info_right_layout:I = 0x7f08015f

.field public static final info_right_title_txt:I = 0x7f080161

.field public static final information:I = 0x7f08036a

.field public static final informationAreaParent:I = 0x7f080357

.field public static final informationAreaParent_fitness:I = 0x7f080368

.field public static final information_container:I = 0x7f08036c

.field public static final information_icon:I = 0x7f080bb0

.field public static final information_layout:I = 0x7f080c28

.field public static final init_blood_glucose:I = 0x7f080376

.field public static final init_blood_pressure:I = 0x7f08037a

.field public static final init_heart_rate:I = 0x7f08037e

.field public static final init_item:I = 0x7f0808ac

.field public static final init_password_text:I = 0x7f08062c

.field public static final init_password_text_2:I = 0x7f08062d

.field public static final init_profile_card_bottom:I = 0x7f08089c

.field public static final init_profile_card_top:I = 0x7f08085e

.field public static final init_samsungaccount_base_layout:I = 0x7f080620

.field public static final init_set_password:I = 0x7f08062e

.field public static final init_stress:I = 0x7f080382

.field public static final initial_profile_background:I = 0x7f08061b

.field public static final initial_profile_intro_bottom_layout:I = 0x7f08061c

.field public static final initial_profile_intro_center_layout:I = 0x7f08061d

.field public static final initial_profile_intro_content:I = 0x7f080628

.field public static final initial_profile_intro_layout:I = 0x7f08061e

.field public static final initial_profile_password_bottom_layout:I = 0x7f08062f

.field public static final initial_profile_samsung_account_bottom_layout:I = 0x7f080627

.field public static final initial_profile_samsung_account_image:I = 0x7f0808bf

.field public static final input_activity_root_layout:I = 0x7f080630

.field public static final input_module_calorie_burn:I = 0x7f080c7e

.field public static final input_module_calorie_intake:I = 0x7f080c7d

.field public static final input_module_diastolic:I = 0x7f080a89

.field public static final input_module_layout:I = 0x7f080499

.field public static final input_module_systolic:I = 0x7f080a88

.field public static final input_type:I = 0x7f08073b

.field public static final input_type_pane:I = 0x7f08073a

.field public static final inputmodule_btn_decrease:I = 0x7f08063a

.field public static final inputmodule_btn_increase:I = 0x7f08063d

.field public static final inputmodule_et_value:I = 0x7f080472

.field public static final inputmodule_et_value_parent:I = 0x7f080471

.field public static final inputmodule_horizontal_top_layout:I = 0x7f08063e

.field public static final inputmodule_parent_layout:I = 0x7f08049c

.field public static final inputmodule_tv_title:I = 0x7f080641

.field public static final inputmodule_tv_unit:I = 0x7f080473

.field public static final intake_input_module:I = 0x7f08049a

.field public static final intake_inputmodule_tv_title:I = 0x7f080497

.field public static final intake_value:I = 0x7f080c6c

.field public static final intro_next_layout:I = 0x7f08062a

.field public static final is_paired_image:I = 0x7f08009b

.field public static final item_body:I = 0x7f0807fc

.field public static final item_calorie:I = 0x7f0807cf

.field public static final item_container:I = 0x7f0804e7

.field public static final item_container_scroll_view:I = 0x7f0804e2

.field public static final item_date:I = 0x7f080bd1

.field public static final item_distance:I = 0x7f0807ce

.field public static final item_dual_left:I = 0x7f0807ea

.field public static final item_dual_right:I = 0x7f0807f2

.field public static final item_duration:I = 0x7f0807cd

.field public static final item_icon:I = 0x7f0807f8

.field public static final item_icon_left:I = 0x7f0807eb

.field public static final item_icon_right:I = 0x7f0807f3

.field public static final item_name:I = 0x7f080bcf

.field public static final item_suggest_header:I = 0x7f080265

.field public static final item_time_and_date:I = 0x7f0807cc

.field public static final item_value:I = 0x7f080bd3

.field public static final iv_finished_sunprotection:I = 0x7f080b12

.field public static final iv_finished_sunprotectiontext:I = 0x7f080b13

.field public static final iv_finished_sunprotectiontime_hr:I = 0x7f080b16

.field public static final iv_finished_sunprotectiontime_hrval:I = 0x7f080b15

.field public static final iv_finished_sunprotectiontime_min:I = 0x7f080b18

.field public static final iv_finished_sunprotectiontime_minval:I = 0x7f080b17

.field public static final iv_icon:I = 0x7f08033c

.field public static final iv_information_maximum:I = 0x7f080b07

.field public static final iv_information_state:I = 0x7f080b09

.field public static final iv_information_state_image:I = 0x7f080b08

.field public static final iv_lifestyle_arrow_icon:I = 0x7f0801ec

.field public static final iv_lifestyle_cap:I = 0x7f0801e7

.field public static final iv_log_child_accessory:I = 0x7f080591

.field public static final iv_log_child_image:I = 0x7f080afb

.field public static final iv_log_child_memo:I = 0x7f080592

.field public static final iv_log_child_memo_detal_view:I = 0x7f080990

.field public static final iv_log_group_expand:I = 0x7f080586

.field public static final iv_measuring_progress_linear:I = 0x7f0805d1

.field public static final iv_measuring_progress_view:I = 0x7f0805d2

.field public static final iv_measuring_progress_view1:I = 0x7f0809fd

.field public static final iv_measuring_progress_view_fail:I = 0x7f0805d3

.field public static final iv_ready_checkbox_spf:I = 0x7f080b1f

.field public static final iv_ready_spfchecktext:I = 0x7f080b1d

.field public static final iv_ready_spfseektext:I = 0x7f080b1a

.field public static final iv_ready_text_spftext:I = 0x7f080b1c

.field public static final iv_summary_second_center_icon:I = 0x7f0805c1

.field public static final iv_summary_second_center_icon_big:I = 0x7f0805c8

.field public static final iv_summary_second_center_icon_big_anim:I = 0x7f0809c2

.field public static final iv_summary_second_center_icon_big_animator:I = 0x7f080b20

.field public static final iv_summary_second_center_icon_big_left_lightning:I = 0x7f080a00

.field public static final iv_summary_second_center_icon_big_right_lightning:I = 0x7f0809ff

.field public static final iv_summary_second_center_icon_finish:I = 0x7f0809fe

.field public static final iv_summary_second_center_icon_noskin:I = 0x7f080b21

.field public static final iv_summary_second_center_pulse_view_green_icon:I = 0x7f0805c5

.field public static final iv_summary_second_center_pulse_view_icon:I = 0x7f0805c4

.field public static final iv_summary_second_center_pulse_view_mask:I = 0x7f0805c7

.field public static final iv_summary_second_center_pulse_view_orange_icon:I = 0x7f0805c6

.field public static final iv_summary_second_center_text_finish:I = 0x7f080b11

.field public static final iv_summary_second_icon:I = 0x7f0805bb

.field public static final iv_summary_second_pulse_icon:I = 0x7f0809c0

.field public static final iv_summary_wearable_second_center_no_data_text:I = 0x7f080b24

.field public static final iv_summary_wearable_second_no_data_icon:I = 0x7f080b23

.field public static final iv_tag_icon:I = 0x7f0805dd

.field public static final iv_tag_icon_logdetail:I = 0x7f0805a8

.field public static final iv_tag_image:I = 0x7f0805f9

.field public static final kcal_summary_layout:I = 0x7f08071c

.field public static final korean_popup_button:I = 0x7f080018

.field public static final label:I = 0x7f080060

.field public static final label_container_left:I = 0x7f0804da

.field public static final label_container_right:I = 0x7f0804dd

.field public static final last_gear_type:I = 0x7f080b3c

.field public static final last_profile_birthday:I = 0x7f08083c

.field public static final last_profile_bmi_view:I = 0x7f0808a1

.field public static final last_profile_gender:I = 0x7f0808a0

.field public static final last_profile_gender_image:I = 0x7f08083b

.field public static final last_profile_height:I = 0x7f08083e

.field public static final last_profile_height_unit:I = 0x7f08083f

.field public static final last_profile_image:I = 0x7f08089e

.field public static final last_profile_image_private:I = 0x7f080839

.field public static final last_profile_image_shadow:I = 0x7f08089f

.field public static final last_profile_lev_image:I = 0x7f080843

.field public static final last_profile_lev_text:I = 0x7f080844

.field public static final last_profile_name:I = 0x7f08083a

.field public static final last_profile_weight:I = 0x7f080841

.field public static final last_profile_weight_unit:I = 0x7f080842

.field public static final last_sync:I = 0x7f080985

.field public static final layout_auto_backup:I = 0x7f080087

.field public static final layout_auto_backup_wifi_only:I = 0x7f08004f

.field public static final layout_for_gear_manager_button:I = 0x7f0802ed

.field public static final layout_information:I = 0x7f0807b3

.field public static final layout_left_arrow:I = 0x7f08031f

.field public static final layout_right_arrow:I = 0x7f080322

.field public static final layout_root:I = 0x7f08008e

.field public static final layout_teamHeader:I = 0x7f080169

.field public static final layout_text:I = 0x7f080386

.field public static final leader_list_item_layout:I = 0x7f080b7a

.field public static final left:I = 0x7f080006

.field public static final left_arrow:I = 0x7f080320

.field public static final left_center_temp:I = 0x7f0802c0

.field public static final left_clickable:I = 0x7f0807c6

.field public static final left_drawer:I = 0x7f080092

.field public static final left_temp:I = 0x7f0802bf

.field public static final left_text:I = 0x7f08067f

.field public static final legen_frame_container:I = 0x7f080642

.field public static final legend_container:I = 0x7f080371

.field public static final legend_mark_container:I = 0x7f080643

.field public static final lev1:I = 0x7f080606

.field public static final lev1_cal:I = 0x7f080607

.field public static final lev1_image:I = 0x7f080866

.field public static final lev1_image_linear:I = 0x7f080865

.field public static final lev1_radio_button:I = 0x7f08086a

.field public static final lev1_sub_title:I = 0x7f080869

.field public static final lev1_text:I = 0x7f080867

.field public static final lev1_title:I = 0x7f080868

.field public static final lev1_title_linear:I = 0x7f080854

.field public static final lev2:I = 0x7f080608

.field public static final lev2_cal:I = 0x7f080609

.field public static final lev2_image:I = 0x7f08086d

.field public static final lev2_image_linear:I = 0x7f08086c

.field public static final lev2_radio_button:I = 0x7f080871

.field public static final lev2_sub_title:I = 0x7f080870

.field public static final lev2_text:I = 0x7f08086e

.field public static final lev2_title:I = 0x7f08086f

.field public static final lev2_title_linear:I = 0x7f080859

.field public static final lev3:I = 0x7f08060a

.field public static final lev3_cal:I = 0x7f08060b

.field public static final lev3_image:I = 0x7f080874

.field public static final lev3_image_linear:I = 0x7f080873

.field public static final lev3_radio_button:I = 0x7f080879

.field public static final lev3_sub_title:I = 0x7f080878

.field public static final lev3_text:I = 0x7f080875

.field public static final lev3_title:I = 0x7f080877

.field public static final lev3_title_linear:I = 0x7f080876

.field public static final lev4:I = 0x7f08060c

.field public static final lev4_cal:I = 0x7f08060d

.field public static final lev4_image:I = 0x7f08087c

.field public static final lev4_image_linear:I = 0x7f08087b

.field public static final lev4_radio_button:I = 0x7f080881

.field public static final lev4_sub_title:I = 0x7f080880

.field public static final lev4_text:I = 0x7f08087d

.field public static final lev4_title:I = 0x7f08087f

.field public static final lev4_title_linear:I = 0x7f08087e

.field public static final lev5:I = 0x7f08060e

.field public static final lev5_cal:I = 0x7f08060f

.field public static final lev5_image:I = 0x7f080884

.field public static final lev5_image_linear:I = 0x7f080883

.field public static final lev5_radio_button:I = 0x7f080889

.field public static final lev5_sub_title:I = 0x7f080888

.field public static final lev5_text:I = 0x7f080885

.field public static final lev5_title:I = 0x7f080887

.field public static final lev5_title_linear:I = 0x7f080886

.field public static final lev_1:I = 0x7f080864

.field public static final lev_2:I = 0x7f08086b

.field public static final lev_3:I = 0x7f080872

.field public static final lev_4:I = 0x7f08087a

.field public static final lev_5:I = 0x7f080882

.field public static final library_empty_area:I = 0x7f0801cc

.field public static final library_fragment_layout:I = 0x7f0801cb

.field public static final library_home_article_tip_bg_effect:I = 0x7f0801ce

.field public static final library_home_article_tip_iv_background:I = 0x7f0801cd

.field public static final library_home_article_tip_txt_title:I = 0x7f0801cf

.field public static final library_home_group_featured:I = 0x7f0801d0

.field public static final library_home_indicator_viewpager:I = 0x7f0801d1

.field public static final library_home_listview:I = 0x7f0801d2

.field public static final library_home_search_field_et_search:I = 0x7f0801d6

.field public static final library_home_search_group_results:I = 0x7f0801d3

.field public static final library_home_search_listview:I = 0x7f0801d4

.field public static final license_nextbtn:I = 0x7f0802ec

.field public static final life_style_description:I = 0x7f0800f9

.field public static final life_style_divider:I = 0x7f0800f7

.field public static final life_style_progressbar:I = 0x7f08010f

.field public static final life_style_progressbar_green:I = 0x7f080112

.field public static final life_style_progressbar_layout:I = 0x7f08010e

.field public static final life_style_progressbar_red:I = 0x7f080110

.field public static final life_style_progressbar_score:I = 0x7f080114

.field public static final life_style_progressbar_score0:I = 0x7f080118

.field public static final life_style_progressbar_score100:I = 0x7f080115

.field public static final life_style_progressbar_score30:I = 0x7f080117

.field public static final life_style_progressbar_score70:I = 0x7f080116

.field public static final life_style_progressbar_yellow:I = 0x7f080111

.field public static final life_style_reassess_description:I = 0x7f0801f4

.field public static final life_style_text_description1:I = 0x7f0800fa

.field public static final life_style_text_description_0:I = 0x7f0800fb

.field public static final life_style_text_description_1:I = 0x7f0800ff

.field public static final life_style_text_description_1_normal_left:I = 0x7f0800fd

.field public static final life_style_text_description_1_normal_right:I = 0x7f080101

.field public static final life_style_text_description_1_rtl_left:I = 0x7f0800fe

.field public static final life_style_text_description_1_rtl_right:I = 0x7f080100

.field public static final life_style_text_description_2:I = 0x7f080105

.field public static final life_style_text_description_2_normal_left:I = 0x7f080103

.field public static final life_style_text_description_2_normal_right:I = 0x7f080107

.field public static final life_style_text_description_2_rtl_left:I = 0x7f080104

.field public static final life_style_text_description_2_rtl_right:I = 0x7f080106

.field public static final life_style_text_description_3:I = 0x7f08010b

.field public static final life_style_text_description_3_normal_left:I = 0x7f080109

.field public static final life_style_text_description_3_normal_right:I = 0x7f08010d

.field public static final life_style_text_description_3_rtl_left:I = 0x7f08010a

.field public static final life_style_text_description_3_rtl_right:I = 0x7f08010c

.field public static final life_style_text_title1:I = 0x7f0800fc

.field public static final life_style_text_title2:I = 0x7f080102

.field public static final life_style_text_title3:I = 0x7f080108

.field public static final lifestyle_category_score_view:I = 0x7f0800f6

.field public static final lifestyle_circle_layout:I = 0x7f0801f7

.field public static final lifestyle_circle_progress_image:I = 0x7f0801f8

.field public static final lifestyle_no_remain_start:I = 0x7f0801f2

.field public static final lifestyle_no_score_remain_category1:I = 0x7f0801f0

.field public static final lifestyle_no_score_remain_category2:I = 0x7f0801f1

.field public static final lifestyle_one_score_reassess_now:I = 0x7f0801e0

.field public static final lifestyle_one_score_set_goal:I = 0x7f0801e2

.field public static final lifestyle_score_view:I = 0x7f0801f3

.field public static final line_divider:I = 0x7f080b03

.field public static final linear:I = 0x7f080be4

.field public static final linear2:I = 0x7f080be6

.field public static final linearLayout1:I = 0x7f08092b

.field public static final linearLayout2:I = 0x7f08092d

.field public static final linear_burn:I = 0x7f080c6d

.field public static final linear_intake:I = 0x7f080c6b

.field public static final linear_txt_fourth:I = 0x7f0805ba

.field public static final linear_txt_one:I = 0x7f0805b6

.field public static final linear_txt_three:I = 0x7f0805b9

.field public static final linear_txt_two:I = 0x7f0805b8

.field public static final list:I = 0x7f080093

.field public static final listItemParent:I = 0x7f0808aa

.field public static final listParent:I = 0x7f08032d

.field public static final list_bottom_divider:I = 0x7f080594

.field public static final list_in_dialog:I = 0x7f080331

.field public static final list_item_additional_text:I = 0x7f080800

.field public static final list_item_additional_unit:I = 0x7f0807ff

.field public static final list_item_divider:I = 0x7f0803d2

.field public static final list_item_divider1:I = 0x7f080940

.field public static final list_item_header_arrow:I = 0x7f0803a0

.field public static final list_item_header_title:I = 0x7f0803a1

.field public static final list_item_loading_progress_bar:I = 0x7f0803d3

.field public static final list_item_text:I = 0x7f08033a

.field public static final list_item_text_right:I = 0x7f0807fd

.field public static final list_of_devices:I = 0x7f08032b

.field public static final list_split_line:I = 0x7f08007e

.field public static final list_split_line_left:I = 0x7f0807ef

.field public static final list_split_line_right:I = 0x7f0807f7

.field public static final list_title:I = 0x7f080647

.field public static final list_top_divider:I = 0x7f08058b

.field public static final list_view:I = 0x7f080801

.field public static final listitem_layout:I = 0x7f080337

.field public static final listview:I = 0x7f080047

.field public static final listview_row_id:I = 0x7f0805f7

.field public static final ll_bpm_layout:I = 0x7f080530

.field public static final ll_center_profile_view:I = 0x7f08089b

.field public static final ll_dialog_check:I = 0x7f0804ee

.field public static final ll_dialog_check_backup_restore:I = 0x7f080682

.field public static final ll_dialog_check_backup_restore_coach:I = 0x7f0802b6

.field public static final ll_dialog_check_coach:I = 0x7f0802b9

.field public static final ll_dialog_ok:I = 0x7f080569

.field public static final ll_exercise_summary_polygon:I = 0x7f08079c

.field public static final ll_hrm_summry_date_curr:I = 0x7f0805e1

.field public static final ll_log_all:I = 0x7f080587

.field public static final ll_log_child_header:I = 0x7f080588

.field public static final ll_log_child_icon:I = 0x7f080590

.field public static final ll_nosensor_dialog_check:I = 0x7f080581

.field public static final ll_restore_now:I = 0x7f0808b0

.field public static final ll_spo2_legend:I = 0x7f0809a1

.field public static final ll_spo2_summary_polygon:I = 0x7f080516

.field public static final ll_stm_summary_polygon:I = 0x7f0804f8

.field public static final ll_summary_first:I = 0x7f0809b5

.field public static final ll_summary_first_ready_layout:I = 0x7f0809f7

.field public static final ll_summary_first_state_layout:I = 0x7f0809fa

.field public static final ll_summary_prev_hrm:I = 0x7f0805e7

.field public static final ll_summary_second_center_pulse_view_layout:I = 0x7f0805c3

.field public static final ll_summary_third_comment:I = 0x7f080b0c

.field public static final ll_summary_third_previous:I = 0x7f0805e5

.field public static final ll_summary_third_previousdate:I = 0x7f080a05

.field public static final ll_summary_third_startdiscard:I = 0x7f080a02

.field public static final ll_summry_border:I = 0x7f0805e6

.field public static final ll_uv_summry_date_curr:I = 0x7f080b01

.field public static final ln_bpm_log_input:I = 0x7f08053b

.field public static final ln_container:I = 0x7f080634

.field public static final ln_heart_rate:I = 0x7f0805bd

.field public static final ln_heart_tag:I = 0x7f0805dc

.field public static final ln_heart_tag_parent:I = 0x7f0805db

.field public static final ln_state_bar_center:I = 0x7f0805d9

.field public static final ln_tag_avg:I = 0x7f080652

.field public static final ln_tag_icon_2:I = 0x7f080638

.field public static final ln_tag_log_detail:I = 0x7f080534

.field public static final ln_tag_log_detail1:I = 0x7f08053e

.field public static final ln_tag_max:I = 0x7f080655

.field public static final ln_tag_max_min_avg:I = 0x7f080651

.field public static final loader:I = 0x7f080bbf

.field public static final loading_img:I = 0x7f08064b

.field public static final loading_popup_background:I = 0x7f08064a

.field public static final loading_popup_txt:I = 0x7f080686

.field public static final lock_button:I = 0x7f080825

.field public static final lock_root_view:I = 0x7f080bc1

.field public static final log_bar:I = 0x7f08099e

.field public static final log_child_body_left_space:I = 0x7f0809e5

.field public static final log_list_accessory_image:I = 0x7f08066b

.field public static final log_list_bottom_text:I = 0x7f080668

.field public static final log_list_center_text:I = 0x7f080667

.field public static final log_list_data_row:I = 0x7f080661

.field public static final log_list_data_row_medal_image:I = 0x7f08066a

.field public static final log_list_delete_check_box:I = 0x7f080663

.field public static final log_list_expand_button:I = 0x7f080374

.field public static final log_list_group_header_left_text:I = 0x7f080372

.field public static final log_list_icons_container:I = 0x7f080669

.field public static final log_list_item_divider:I = 0x7f080662

.field public static final log_list_left_text:I = 0x7f080666

.field public static final log_list_memo_image:I = 0x7f08066c

.field public static final log_list_right_groupe_text:I = 0x7f080373

.field public static final log_list_right_text:I = 0x7f08066e

.field public static final log_list_row_left_image:I = 0x7f080664

.field public static final log_list_row_left_text_container:I = 0x7f080665

.field public static final log_list_team_header:I = 0x7f08065c

.field public static final log_list_team_header_divider:I = 0x7f080660

.field public static final log_list_team_header_left_text_view:I = 0x7f08065d

.field public static final log_list_team_header_medal_image:I = 0x7f08065e

.field public static final log_list_team_header_right_text_view:I = 0x7f08065f

.field public static final log_list_top_right_text:I = 0x7f08066d

.field public static final logo_img:I = 0x7f08061f

.field public static final lose_progress_text_view:I = 0x7f080c6f

.field public static final lost_weight_text_view:I = 0x7f080c63

.field public static final low_high_mark:I = 0x7f0804fe

.field public static final low_hum:I = 0x7f0802be

.field public static final low_percentile:I = 0x7f0804ff

.field public static final lower_lv:I = 0x7f0805f5

.field public static final lv_ChildHeader:I = 0x7f08058c

.field public static final lv_add_tag_view:I = 0x7f080542

.field public static final lv_custom_tag_list:I = 0x7f08054b

.field public static final lv_emotion_tag_list:I = 0x7f080548

.field public static final lv_more_tag_view:I = 0x7f080545

.field public static final lv_tag_cancel:I = 0x7f0805f3

.field public static final lv_tag_custom_list:I = 0x7f08052c

.field public static final lv_tag_default_list:I = 0x7f08052b

.field public static final lv_tag_list:I = 0x7f080567

.field public static final ly_action_bar_button:I = 0x7f080302

.field public static final ly_action_btn_holder:I = 0x7f08030e

.field public static final ly_action_btn_holder_canceldone:I = 0x7f080312

.field public static final ly_content_area:I = 0x7f080baf

.field public static final ly_motionless:I = 0x7f08095b

.field public static final ly_sleeptime:I = 0x7f08095e

.field public static final ly_sub_action_title_holder:I = 0x7f080304

.field public static final ly_summary_first:I = 0x7f0805d4

.field public static final ly_totalsleep:I = 0x7f080958

.field public static final ly_wakeup_time:I = 0x7f080961

.field public static final lyt_hr_layout:I = 0x7f0809ac

.field public static final lyt_spo2_layout:I = 0x7f0809a6

.field public static final main:I = 0x7f08096a

.field public static final main_container:I = 0x7f0804a7

.field public static final main_container_breakfast_item:I = 0x7f0804ac

.field public static final main_container_dinner_item:I = 0x7f0804ae

.field public static final main_container_lunch_item:I = 0x7f0804ad

.field public static final main_container_snacks_item:I = 0x7f0804af

.field public static final main_frame:I = 0x7f08078e

.field public static final main_graph:I = 0x7f0802c4

.field public static final main_item:I = 0x7f08001c

.field public static final main_layout:I = 0x7f0803c3

.field public static final maintain_weight:I = 0x7f080c68

.field public static final male_icon:I = 0x7f080849

.field public static final male_layout:I = 0x7f080848

.field public static final male_text:I = 0x7f08084a

.field public static final manual:I = 0x7f08001a

.field public static final manual_webview:I = 0x7f08066f

.field public static final manually_input_layout:I = 0x7f080806

.field public static final manually_layout:I = 0x7f080805

.field public static final manufacturer_name:I = 0x7f08009c

.field public static final map:I = 0x7f0806f5

.field public static final map_data_layout:I = 0x7f0806f8

.field public static final map_data_text1:I = 0x7f0806f7

.field public static final map_data_text2:I = 0x7f0806f9

.field public static final map_frame:I = 0x7f0807ca

.field public static final map_info_layout:I = 0x7f080710

.field public static final map_mode_btn:I = 0x7f0806fb

.field public static final map_play_btn:I = 0x7f0806fc

.field public static final map_viewmode_btn:I = 0x7f0806fa

.field public static final marking_img:I = 0x7f080113

.field public static final max_value:I = 0x7f0807bd

.field public static final maxhr:I = 0x7f080c96

.field public static final may_not_available:I = 0x7f0802f7

.field public static final meal_activity_fragment_container:I = 0x7f0803d5

.field public static final meal_activity_meal_items_fragment_container:I = 0x7f0803a9

.field public static final meal_activity_meal_photos_fragment_container:I = 0x7f0803d8

.field public static final meal_activity_time_container:I = 0x7f0803d6

.field public static final meal_activity_top_fragment_container:I = 0x7f0803d7

.field public static final meal_add_button:I = 0x7f0804b8

.field public static final meal_breakfast_row:I = 0x7f08049d

.field public static final meal_dinner_row:I = 0x7f08049f

.field public static final meal_fakeView:I = 0x7f0803d9

.field public static final meal_input_top:I = 0x7f0803e7

.field public static final meal_item_image:I = 0x7f0804b0

.field public static final meal_item_view_image_divider:I = 0x7f0803e0

.field public static final meal_items_container:I = 0x7f0803f3

.field public static final meal_k_calories:I = 0x7f0804b6

.field public static final meal_k_calories_unit:I = 0x7f0804b7

.field public static final meal_lunch_row:I = 0x7f08049e

.field public static final meal_memo_input:I = 0x7f0803e2

.field public static final meal_name_text:I = 0x7f0803b0

.field public static final meal_plan_add_meal_fragment:I = 0x7f080400

.field public static final meal_plan_check_box1:I = 0x7f08040e

.field public static final meal_plan_check_box2:I = 0x7f08040f

.field public static final meal_plan_check_box3:I = 0x7f080410

.field public static final meal_plan_check_box4:I = 0x7f080411

.field public static final meal_plan_check_box5:I = 0x7f080412

.field public static final meal_plan_check_box6:I = 0x7f080413

.field public static final meal_plan_check_box7:I = 0x7f080414

.field public static final meal_plan_creator_fragment_container:I = 0x7f0803f8

.field public static final meal_plan_list_view:I = 0x7f0803fd

.field public static final meal_plan_meal_item_fragment:I = 0x7f0803fb

.field public static final meal_plan_meal_type_text:I = 0x7f0803fe

.field public static final meal_plan_period_range:I = 0x7f0803ff

.field public static final meal_plan_preview_meal_fragment:I = 0x7f080403

.field public static final meal_plan_selected_meal_type_radio_group:I = 0x7f080407

.field public static final meal_plan_set_period_fragment:I = 0x7f0803fc

.field public static final meal_snack_row:I = 0x7f0804a0

.field public static final meal_type:I = 0x7f0803ea

.field public static final meal_type_container:I = 0x7f080490

.field public static final meal_type_drop_down:I = 0x7f080492

.field public static final meal_type_drop_down_ll:I = 0x7f080491

.field public static final meal_type_ll:I = 0x7f0803e9

.field public static final meal_view_button:I = 0x7f0804b1

.field public static final measurables_list:I = 0x7f080a8d

.field public static final measure_items_container:I = 0x7f08073c

.field public static final measuring_circle_view:I = 0x7f0805c9

.field public static final medal_image:I = 0x7f080c5e

.field public static final memo_divider:I = 0x7f080c3b

.field public static final memo_edit_view:I = 0x7f08073d

.field public static final memo_edit_view_gone:I = 0x7f0807d0

.field public static final memo_icon_view:I = 0x7f0803e4

.field public static final memo_image:I = 0x7f080170

.field public static final memo_input_view:I = 0x7f0803e3

.field public static final memo_layout:I = 0x7f080538

.field public static final memo_text_view:I = 0x7f0803e5

.field public static final memo_title:I = 0x7f0803e6

.field public static final memo_view:I = 0x7f080633

.field public static final memo_view_input:I = 0x7f080637

.field public static final middleLayout:I = 0x7f080afe

.field public static final middle_layout:I = 0x7f08032e

.field public static final middle_line:I = 0x7f0807f1

.field public static final middle_line_left:I = 0x7f0807f0

.field public static final min_value:I = 0x7f0807bc

.field public static final minutes_edit_text_layout:I = 0x7f0807a5

.field public static final mission_complete_1button_area:I = 0x7f080210

.field public static final mission_complete_2button_area:I = 0x7f080212

.field public static final mission_complete_badge_icon:I = 0x7f08019a

.field public static final mission_complete_badge_layout:I = 0x7f08020e

.field public static final mission_complete_button_left:I = 0x7f080213

.field public static final mission_complete_button_ok:I = 0x7f080211

.field public static final mission_complete_button_right:I = 0x7f080214

.field public static final mission_complete_layout:I = 0x7f08020d

.field public static final mission_description:I = 0x7f08020f

.field public static final mode_check_box:I = 0x7f0808ad

.field public static final mode_title:I = 0x7f0808ae

.field public static final module_title_layout:I = 0x7f080496

.field public static final month_divider:I = 0x7f0800a1

.field public static final month_motionless_sleep_portion:I = 0x7f080984

.field public static final month_progressBar:I = 0x7f080982

.field public static final month_total_sleep:I = 0x7f080983

.field public static final monthly_avg_motionless:I = 0x7f08097d

.field public static final more_info:I = 0x7f080907

.field public static final more_info_1:I = 0x7f080918

.field public static final more_info_2:I = 0x7f08091b

.field public static final more_info_3:I = 0x7f08091d

.field public static final more_info_expand_button:I = 0x7f0807c2

.field public static final more_option_item_text:I = 0x7f08089d

.field public static final mot_hour_str:I = 0x7f080947

.field public static final mot_hour_text:I = 0x7f080946

.field public static final mot_minute_str:I = 0x7f080949

.field public static final mot_minute_text:I = 0x7f080948

.field public static final motionless:I = 0x7f08095c

.field public static final motionless_data_area:I = 0x7f08097c

.field public static final motionless_month_average:I = 0x7f080981

.field public static final motionless_sleep_area:I = 0x7f080944

.field public static final motionless_sleep_portion:I = 0x7f080975

.field public static final motionless_sleep_title:I = 0x7f080945

.field public static final multi_item_checkbox:I = 0x7f080672

.field public static final multi_item_list:I = 0x7f080671

.field public static final multi_item_text:I = 0x7f080673

.field public static final music_artist:I = 0x7f080784

.field public static final music_controller:I = 0x7f08077b

.field public static final music_dummy_view:I = 0x7f080785

.field public static final music_play:I = 0x7f080780

.field public static final music_player_layout:I = 0x7f080720

.field public static final music_rewind:I = 0x7f08077f

.field public static final music_text_layout:I = 0x7f080782

.field public static final music_title:I = 0x7f080783

.field public static final music_unwind:I = 0x7f080781

.field public static final my_accessories:I = 0x7f0808ea

.field public static final my_food:I = 0x7f080c9b

.field public static final my_food_add_my_food_plus:I = 0x7f080453

.field public static final my_food_fragment_header:I = 0x7f080469

.field public static final my_food_fragment_header_text_view:I = 0x7f080455

.field public static final my_food_select_all_delete_checkbox:I = 0x7f080454

.field public static final my_goals_header:I = 0x7f080078

.field public static final my_ranking_container:I = 0x7f080b7b

.field public static final my_ranking_header_text:I = 0x7f080bda

.field public static final my_ranking_total_distance:I = 0x7f080bd9

.field public static final my_ranking_total_step:I = 0x7f080bd8

.field public static final mygoals_connect_wearable_device:I = 0x7f080674

.field public static final mypage_account_list:I = 0x7f08005b

.field public static final mypage_birthday:I = 0x7f080072

.field public static final mypage_photo:I = 0x7f080070

.field public static final mypage_photo_area:I = 0x7f08006e

.field public static final name:I = 0x7f080c02

.field public static final next_btn:I = 0x7f080613

.field public static final next_button:I = 0x7f080c7b

.field public static final next_button_container:I = 0x7f08039c

.field public static final next_image:I = 0x7f080614

.field public static final next_text:I = 0x7f08039d

.field public static final no_data:I = 0x7f080359

.field public static final no_data_layout:I = 0x7f0803f7

.field public static final no_data_message:I = 0x7f08065b

.field public static final no_data_root_layout:I = 0x7f080418

.field public static final no_data_view:I = 0x7f08054f

.field public static final no_devices_image:I = 0x7f0808d5

.field public static final no_devices_text:I = 0x7f0808d6

.field public static final no_goal_hint:I = 0x7f080744

.field public static final no_goal_hint_rtl:I = 0x7f080745

.field public static final no_goal_text:I = 0x7f080742

.field public static final no_goal_text_rtl:I = 0x7f080743

.field public static final no_goals:I = 0x7f080741

.field public static final no_gps_data_noti_text:I = 0x7f08070f

.field public static final no_item_layout:I = 0x7f080344

.field public static final no_network_layout:I = 0x7f080b85

.field public static final no_score_coach_message:I = 0x7f0801ef

.field public static final nodata_container:I = 0x7f08036e

.field public static final nodata_image:I = 0x7f08036f

.field public static final nodata_text:I = 0x7f080370

.field public static final non_sensor_iv_summary_second_icon:I = 0x7f0805d6

.field public static final non_sensor_no_data:I = 0x7f0805d7

.field public static final non_sensor_sync_guide:I = 0x7f0805e0

.field public static final none:I = 0x7f080000

.field public static final normal:I = 0x7f080001

.field public static final normal_range:I = 0x7f080008

.field public static final notesCommentLayout:I = 0x7f080967

.field public static final notes_textview:I = 0x7f080968

.field public static final noti_checkbox:I = 0x7f0808e8

.field public static final notice_popup_button:I = 0x7f080019

.field public static final notification_btn_left:I = 0x7f08021e

.field public static final notification_btn_right:I = 0x7f08021f

.field public static final notification_desc_message:I = 0x7f08021c

.field public static final notification_head_message:I = 0x7f08021b

.field public static final notification_image:I = 0x7f08021a

.field public static final notification_ok:I = 0x7f080221

.field public static final notification_one_btn_layout:I = 0x7f080220

.field public static final notification_settings:I = 0x7f0808e6

.field public static final notification_text:I = 0x7f0808e7

.field public static final notification_two_btn_layout:I = 0x7f08021d

.field public static final nutrient_carbo_text:I = 0x7f0804c4

.field public static final nutrient_carbo_value:I = 0x7f0804c5

.field public static final nutrient_container:I = 0x7f0804c1

.field public static final nutrient_fat_text:I = 0x7f0804c2

.field public static final nutrient_fat_value:I = 0x7f0804c3

.field public static final nutrient_protein_text:I = 0x7f0804c6

.field public static final nutrient_protein_value:I = 0x7f0804c7

.field public static final nutrient_title:I = 0x7f080315

.field public static final nutrient_unit:I = 0x7f080318

.field public static final nutrient_view_carbo:I = 0x7f08038d

.field public static final nutrient_view_fat:I = 0x7f08038c

.field public static final nutrient_view_protein:I = 0x7f08038e

.field public static final nutrients_container:I = 0x7f08038b

.field public static final nutrition_blocks_view:I = 0x7f080443

.field public static final nutrition_info_root_layout:I = 0x7f08042a

.field public static final nutrition_list:I = 0x7f080445

.field public static final ok_button:I = 0x7f0800b3

.field public static final onDown:I = 0x7f08000c

.field public static final onLongPress:I = 0x7f08000e

.field public static final onMove:I = 0x7f08000d

.field public static final on_network_layout:I = 0x7f080b82

.field public static final one_cont:I = 0x7f080904

.field public static final opensouce_contents:I = 0x7f080678

.field public static final opensource_license:I = 0x7f080675

.field public static final opensource_scrollView:I = 0x7f080676

.field public static final operate_activity_type:I = 0x7f080786

.field public static final operate_layout:I = 0x7f08071d

.field public static final operate_map:I = 0x7f08078c

.field public static final operate_text1:I = 0x7f080787

.field public static final operate_text2:I = 0x7f080788

.field public static final operate_text3:I = 0x7f080789

.field public static final operate_text4:I = 0x7f08078b

.field public static final otherSyncedGearDevice:I = 0x7f080c14

.field public static final page_selector:I = 0x7f0803cf

.field public static final paired_device_layout:I = 0x7f0808cb

.field public static final paired_devices_divider:I = 0x7f080023

.field public static final paired_list_container:I = 0x7f0808cd

.field public static final paired_list_layout:I = 0x7f0808ca

.field public static final paired_type_text:I = 0x7f0808cc

.field public static final parentLayout:I = 0x7f08064d

.field public static final parent_background:I = 0x7f0803bf

.field public static final password_img:I = 0x7f08062b

.field public static final pause:I = 0x7f080cb4

.field public static final paused:I = 0x7f080c10

.field public static final pbtn1:I = 0x7f080a6b

.field public static final pdtv1:I = 0x7f080a69

.field public static final pedo_calory:I = 0x7f0804ea

.field public static final pedo_list:I = 0x7f0804e8

.field public static final pedo_txt_1:I = 0x7f08092a

.field public static final pedo_txt_3:I = 0x7f080924

.field public static final pedometer_button:I = 0x7f080b61

.field public static final pedometer_default_option:I = 0x7f080bed

.field public static final pedometer_default_option_check:I = 0x7f080bef

.field public static final pedometer_image:I = 0x7f08034f

.field public static final pedometer_inactive_time_set_time_content:I = 0x7f080ba8

.field public static final pedometer_notification_setting_content:I = 0x7f080ba7

.field public static final pedoscoreboard_button_textview:I = 0x7f080c24

.field public static final percent:I = 0x7f0802bc

.field public static final percent_icon:I = 0x7f08094b

.field public static final percent_icon_close:I = 0x7f080955

.field public static final percent_text:I = 0x7f08094a

.field public static final percent_text_view:I = 0x7f080c65

.field public static final personal_pane:I = 0x7f0808bb

.field public static final pet1:I = 0x7f080a6a

.field public static final photo:I = 0x7f080c01

.field public static final photo_add_butten_layout:I = 0x7f0806ef

.field public static final photo_control:I = 0x7f080739

.field public static final photo_control_layout:I = 0x7f080737

.field public static final photo_header:I = 0x7f0803de

.field public static final photo_horizontal_scroll_view:I = 0x7f080719

.field public static final photo_item_layout:I = 0x7f080809

.field public static final photo_scroll_view:I = 0x7f080808

.field public static final photo_title:I = 0x7f0806ee

.field public static final pick_item:I = 0x7f0803aa

.field public static final picture:I = 0x7f080446

.field public static final piece_of_pie_diagram:I = 0x7f0804c8

.field public static final pin_code_edit_text:I = 0x7f080650

.field public static final pin_code_title:I = 0x7f08067e

.field public static final plain_heart_button:I = 0x7f080b47

.field public static final plain_pedometer_button:I = 0x7f080b46

.field public static final plain_root_view:I = 0x7f080b45

.field public static final plain_widget_full_layout:I = 0x7f080b44

.field public static final play_layout:I = 0x7f08077e

.field public static final policy_bottom:I = 0x7f0806b9

.field public static final policy_scrollView2:I = 0x7f08068c

.field public static final pop_up_textTitle:I = 0x7f080042

.field public static final pop_up_title_layout:I = 0x7f080041

.field public static final popup_button_ok:I = 0x7f080b8f

.field public static final popup_content:I = 0x7f080b8c

.field public static final popup_content_layout:I = 0x7f080b8b

.field public static final popup_range_text:I = 0x7f080b8d

.field public static final popup_range_value:I = 0x7f080b8e

.field public static final popup_title:I = 0x7f080b8a

.field public static final portion_max_value:I = 0x7f080478

.field public static final portion_min_value:I = 0x7f080477

.field public static final portion_number:I = 0x7f080475

.field public static final portion_size_handler_container:I = 0x7f080479

.field public static final portion_size_handler_view_container:I = 0x7f080470

.field public static final portion_size_input_module:I = 0x7f08047a

.field public static final portion_size_scale_view:I = 0x7f080476

.field public static final portion_size_slider_view:I = 0x7f0803a6

.field public static final pp_australia_subject_1_title_id:I = 0x7f0806bf

.field public static final pp_australia_subject_2_content_id:I = 0x7f0806c2

.field public static final pp_australia_subject_2_title_id:I = 0x7f0806c1

.field public static final pp_australia_subject_3_content_1_id:I = 0x7f0806c4

.field public static final pp_australia_subject_3_content_2_id:I = 0x7f0806c5

.field public static final pp_australia_subject_3_title_id:I = 0x7f0806c3

.field public static final pp_australia_subject_4_content_id:I = 0x7f0806c7

.field public static final pp_australia_subject_4_title_id:I = 0x7f0806c6

.field public static final pp_australia_subject_5_content_1_id:I = 0x7f0806c9

.field public static final pp_australia_subject_5_content_2_id:I = 0x7f0806ca

.field public static final pp_australia_subject_5_title_id:I = 0x7f0806c8

.field public static final pp_australia_subject_6_content_1_id:I = 0x7f0806cc

.field public static final pp_australia_subject_6_content_2_id:I = 0x7f0806cd

.field public static final pp_australia_subject_6_title_id:I = 0x7f0806cb

.field public static final pp_australia_subject_7_content_1_id:I = 0x7f0806cf

.field public static final pp_australia_subject_7_content_2_id:I = 0x7f0806d0

.field public static final pp_australia_subject_7_content_3_id:I = 0x7f0806d1

.field public static final pp_australia_subject_7_content_4_id:I = 0x7f0806d2

.field public static final pp_australia_subject_7_title_id:I = 0x7f0806ce

.field public static final pp_australia_subject_content_1_id:I = 0x7f0806c0

.field public static final pp_australia_supplment_content_1_id:I = 0x7f0806bb

.field public static final pp_australia_supplment_content_2_id:I = 0x7f0806bc

.field public static final pp_australia_supplment_content_3_id:I = 0x7f0806bd

.field public static final pp_australia_supplment_content_4_id:I = 0x7f0806be

.field public static final pp_australia_supplment_title_id:I = 0x7f0806ba

.field public static final pp_base_layout:I = 0x7f08068b

.field public static final pp_kor_sub_1_content_1:I = 0x7f0806d5

.field public static final pp_kor_sub_1_content_2:I = 0x7f0806d6

.field public static final pp_kor_sub_1_title:I = 0x7f0806d4

.field public static final pp_kor_sub_2_content_1:I = 0x7f0806d8

.field public static final pp_kor_sub_2_content_2:I = 0x7f0806d9

.field public static final pp_kor_sub_2_content_3:I = 0x7f0806da

.field public static final pp_kor_sub_2_title:I = 0x7f0806d7

.field public static final pp_kor_sub_3_content_1:I = 0x7f0806dc

.field public static final pp_kor_sub_3_content_2:I = 0x7f0806dd

.field public static final pp_kor_sub_3_title:I = 0x7f0806db

.field public static final pp_kor_title:I = 0x7f0806d3

.field public static final pressure_summary_view:I = 0x7f080a8a

.field public static final preview_chooser:I = 0x7f080524

.field public static final previous_button:I = 0x7f080c7a

.field public static final previous_button_container:I = 0x7f08039a

.field public static final previous_next_buttons:I = 0x7f0807b2

.field public static final previous_text:I = 0x7f08039b

.field public static final primary_display_about_speed:I = 0x7f080ca8

.field public static final primary_text:I = 0x7f080648

.field public static final prime:I = 0x7f0801a8

.field public static final print:I = 0x7f080ca7

.field public static final privacy_and_policy:I = 0x7f0808f5

.field public static final privacy_au:I = 0x7f080a13

.field public static final privacy_divider:I = 0x7f0808e0

.field public static final privacy_kor:I = 0x7f08069c

.field public static final privacy_kor_sub_1_content_1:I = 0x7f08069e

.field public static final privacy_kor_sub_1_content_2:I = 0x7f08069f

.field public static final privacy_kor_sub_1_title:I = 0x7f08069d

.field public static final privacy_kor_sub_2_content_1:I = 0x7f0806a1

.field public static final privacy_kor_sub_2_content_2:I = 0x7f0806a2

.field public static final privacy_kor_sub_2_content_3:I = 0x7f0806a3

.field public static final privacy_kor_sub_2_title:I = 0x7f0806a0

.field public static final privacy_kor_sub_3_content_1:I = 0x7f0806a5

.field public static final privacy_kor_sub_3_content_2:I = 0x7f0806a6

.field public static final privacy_kor_sub_3_title:I = 0x7f0806a4

.field public static final privacy_open:I = 0x7f08068d

.field public static final privacy_policy_deletion_1:I = 0x7f0806ad

.field public static final privacy_policy_deletion_2:I = 0x7f0806ae

.field public static final privacy_policy_deletion_3:I = 0x7f0806af

.field public static final privacy_policy_deletion_4:I = 0x7f0806b0

.field public static final privacy_policy_deletion_open_1:I = 0x7f080693

.field public static final privacy_policy_deletion_open_2:I = 0x7f080694

.field public static final privacy_policy_deletion_open_3:I = 0x7f080695

.field public static final privacy_policy_deletion_open_4:I = 0x7f080696

.field public static final privacy_policy_divider:I = 0x7f0808f4

.field public static final privacy_policy_paragraph_1_id:I = 0x7f0806a9

.field public static final privacy_policy_paragraph_1_open_id:I = 0x7f08068f

.field public static final privacy_policy_paragraph_2_id:I = 0x7f0806aa

.field public static final privacy_policy_paragraph_2_open_id:I = 0x7f080690

.field public static final privacy_policy_paragraph_3_2_id:I = 0x7f0806ac

.field public static final privacy_policy_paragraph_3_2_open_id:I = 0x7f080692

.field public static final privacy_policy_paragraph_3_id:I = 0x7f0806ab

.field public static final privacy_policy_paragraph_3_open_id:I = 0x7f080691

.field public static final privacy_policy_paragraph_4_id:I = 0x7f0806b2

.field public static final privacy_policy_paragraph_4_open_id:I = 0x7f080698

.field public static final privacy_policy_paragraph_5_id:I = 0x7f0806b5

.field public static final privacy_policy_paragraph_5_open_id:I = 0x7f08069b

.field public static final privacy_policy_paragraph_6_id:I = 0x7f0806b1

.field public static final privacy_policy_paragraph_6_open_id:I = 0x7f080697

.field public static final privacy_policy_title_id:I = 0x7f0806a8

.field public static final privacy_policy_title_open_id:I = 0x7f08068e

.field public static final profile_body_dialog_text:I = 0x7f080836

.field public static final profile_body_dialog_unit_text:I = 0x7f080837

.field public static final profile_card_first:I = 0x7f080346

.field public static final profile_card_second:I = 0x7f080347

.field public static final profile_card_third:I = 0x7f080348

.field public static final profile_card_top:I = 0x7f080615

.field public static final profile_edit_weight_container:I = 0x7f0808a7

.field public static final profile_edit_weight_text:I = 0x7f0808a8

.field public static final profile_editable_container:I = 0x7f08088a

.field public static final profile_email:I = 0x7f080336

.field public static final profile_height_edit:I = 0x7f08088b

.field public static final profile_height_edit2:I = 0x7f08088d

.field public static final profile_height_edit_feet:I = 0x7f08088c

.field public static final profile_height_inputmodule_dropdownlist:I = 0x7f08088e

.field public static final profile_height_parent:I = 0x7f08083d

.field public static final profile_image:I = 0x7f080333

.field public static final profile_image_layout:I = 0x7f080332

.field public static final profile_image_man:I = 0x7f080846

.field public static final profile_information:I = 0x7f080cab

.field public static final profile_lev:I = 0x7f080073

.field public static final profile_name:I = 0x7f080335

.field public static final profile_pressed_view:I = 0x7f080847

.field public static final profile_share_via:I = 0x7f080cac

.field public static final profile_spinner_item_layout:I = 0x7f0808a3

.field public static final profile_spinner_item_txt:I = 0x7f0808a4

.field public static final profile_spinner_main_layout:I = 0x7f0808a5

.field public static final profile_spinner_main_txt:I = 0x7f0808a6

.field public static final profile_text_layout:I = 0x7f080334

.field public static final profile_updated_last:I = 0x7f0808a2

.field public static final profile_weight_dropdownlist:I = 0x7f0808a9

.field public static final profile_weight_parent:I = 0x7f080840

.field public static final progress:I = 0x7f080b32

.field public static final progressBar:I = 0x7f080767

.field public static final progressBar1:I = 0x7f0808d2

.field public static final progress_bar:I = 0x7f080b3d

.field public static final progress_bar_left:I = 0x7f0804d8

.field public static final progress_bar_right:I = 0x7f0804db

.field public static final progress_icon:I = 0x7f080b31

.field public static final progress_layout:I = 0x7f080b91

.field public static final progress_title_left:I = 0x7f0804d9

.field public static final progress_title_right:I = 0x7f0804dc

.field public static final progressbar_average:I = 0x7f080b96

.field public static final progressbar_average_layout:I = 0x7f080b95

.field public static final progressbar_backup:I = 0x7f08008b

.field public static final progressbar_best:I = 0x7f080b99

.field public static final progressbar_best_layout:I = 0x7f080b98

.field public static final progressbar_you:I = 0x7f080b93

.field public static final progressbar_you_layout:I = 0x7f080b92

.field public static final proprietary_rights_and_licenses_text_1_id:I = 0x7f080a1f

.field public static final proprietary_rights_and_licenses_text_2_id:I = 0x7f080a20

.field public static final proprietary_rights_and_licenses_text_3_id:I = 0x7f080a21

.field public static final proprietary_rights_and_licenses_title_id:I = 0x7f080a1e

.field public static final protein:I = 0x7f080441

.field public static final provider_footer:I = 0x7f080422

.field public static final pulse_layout:I = 0x7f0809bf

.field public static final quick_input_image_panel:I = 0x7f080493

.field public static final quick_input_large_meal_icon:I = 0x7f08048d

.field public static final quick_input_large_meal_label_text:I = 0x7f08048f

.field public static final quick_input_large_meal_layout:I = 0x7f08048c

.field public static final quick_input_large_meal_value_text:I = 0x7f08048e

.field public static final quick_input_medium_meal_icon:I = 0x7f080489

.field public static final quick_input_medium_meal_label_text:I = 0x7f08048b

.field public static final quick_input_medium_meal_layout:I = 0x7f080488

.field public static final quick_input_medium_meal_value_text:I = 0x7f08048a

.field public static final quick_input_popup_total_calorie:I = 0x7f08047f

.field public static final quick_input_skipped_meal_icon:I = 0x7f080482

.field public static final quick_input_skipped_meal_label_text:I = 0x7f080483

.field public static final quick_input_skipped_meal_layout:I = 0x7f080481

.field public static final quick_input_small_meal_icon:I = 0x7f080485

.field public static final quick_input_small_meal_label_text:I = 0x7f080487

.field public static final quick_input_small_meal_layout:I = 0x7f080484

.field public static final quick_input_small_meal_value_text:I = 0x7f080486

.field public static final quickinput_button:I = 0x7f0804a9

.field public static final quickinput_calorie_value:I = 0x7f0804a2

.field public static final quickinput_calorie_value_edit:I = 0x7f08047e

.field public static final quickinput_calorie_value_edit_container:I = 0x7f08047d

.field public static final radio_button:I = 0x7f080778

.field public static final radio_button_breakfast:I = 0x7f080408

.field public static final radio_button_dinner:I = 0x7f08040a

.field public static final radio_button_lunch:I = 0x7f080409

.field public static final radio_button_manual:I = 0x7f08077a

.field public static final radio_button_snacks:I = 0x7f08040b

.field public static final range_popup:I = 0x7f080b89

.field public static final range_zone:I = 0x7f0802da

.field public static final ranking:I = 0x7f080bff

.field public static final rb_after_meal:I = 0x7f08003e

.field public static final rb_fasting:I = 0x7f08003f

.field public static final rb_skintype_1:I = 0x7f080ad4

.field public static final rb_skintype_2:I = 0x7f080ad9

.field public static final rb_skintype_3:I = 0x7f080ade

.field public static final rb_skintype_4:I = 0x7f080ae3

.field public static final rb_skintype_5:I = 0x7f080ae8

.field public static final rb_skintype_6:I = 0x7f080aed

.field public static final rb_tag_selected:I = 0x7f0805f6

.field public static final rdb_backup_interval:I = 0x7f080057

.field public static final realtime_container:I = 0x7f08078f

.field public static final realtime_hour_edittext:I = 0x7f0807a3

.field public static final realtime_map_dataLayout:I = 0x7f0806f6

.field public static final realtime_minutes_edittext:I = 0x7f0807a6

.field public static final reassessment_description_scroll:I = 0x7f08023a

.field public static final reassessment_diverder_view:I = 0x7f080239

.field public static final reassessment_less_than_btn_no:I = 0x7f080236

.field public static final reassessment_less_than_btn_yes:I = 0x7f080237

.field public static final recent_meals:I = 0x7f0807fb

.field public static final recommend_text:I = 0x7f0807af

.field public static final recommend_text_layout:I = 0x7f0807ae

.field public static final recommended_value:I = 0x7f080c6a

.field public static final reengaging_btn:I = 0x7f080243

.field public static final reengaging_btn_cancel:I = 0x7f080240

.field public static final reengaging_btn_ok:I = 0x7f080241

.field public static final reengaging_one_btn_layout:I = 0x7f080242

.field public static final reengaging_two_btn_layout:I = 0x7f08023f

.field public static final rel_lay_data:I = 0x7f080049

.field public static final rename:I = 0x7f08067a

.field public static final rename_layout:I = 0x7f080679

.field public static final reset:I = 0x7f0802db

.field public static final reset_data:I = 0x7f0808ee

.field public static final restore_failed:I = 0x7f0802b5

.field public static final restore_layout_header:I = 0x7f0808af

.field public static final right:I = 0x7f080007

.field public static final right_arrow:I = 0x7f080323

.field public static final right_center_temp:I = 0x7f0802c1

.field public static final right_clickable:I = 0x7f0807c7

.field public static final right_padding:I = 0x7f08094c

.field public static final right_temp:I = 0x7f0802c2

.field public static final right_text:I = 0x7f080680

.field public static final rl_checkbox_container:I = 0x7f08084f

.field public static final rl_confirm_backup:I = 0x7f0808b2

.field public static final rl_custom_listview:I = 0x7f08054a

.field public static final rl_emotion_listview:I = 0x7f080547

.field public static final rl_finish_topLayout:I = 0x7f080b10

.field public static final rl_finish_wearable_no_data_topLayout:I = 0x7f080b22

.field public static final rl_listview:I = 0x7f080566

.field public static final rl_non_sensor_sync_guide:I = 0x7f0805df

.field public static final rl_non_sensor_ui:I = 0x7f0805d5

.field public static final rl_pp_layout:I = 0x7f0806b7

.field public static final rl_spf_checkbox_layout:I = 0x7f080b1e

.field public static final rl_summary_second_rt_graph_layout:I = 0x7f0805bc

.field public static final rl_tag_add:I = 0x7f0805f1

.field public static final root:I = 0x7f080048

.field public static final root_frame_layout:I = 0x7f080bf6

.field public static final root_nutrition_item_view:I = 0x7f080423

.field public static final root_view:I = 0x7f080b49

.field public static final row_view_id_tag_key:I = 0x7f080005

.field public static final samsung_account_signin:I = 0x7f080622

.field public static final samsung_gear2_layout:I = 0x7f080c32

.field public static final samsung_gear2_radio_button:I = 0x7f080c34

.field public static final samsung_gear2_title:I = 0x7f080c33

.field public static final samsung_gear_fit_layout:I = 0x7f080c2c

.field public static final samsung_gear_fit_radio_button:I = 0x7f080c2e

.field public static final samsung_gear_fit_title:I = 0x7f080c2d

.field public static final satellite:I = 0x7f080002

.field public static final saturated_fat:I = 0x7f080430

.field public static final saturated_fat_percent:I = 0x7f080431

.field public static final scanButton:I = 0x7f0808d8

.field public static final scanButtonLayout:I = 0x7f0808c7

.field public static final scancontainer:I = 0x7f0802fd

.field public static final scanlistlayout:I = 0x7f0808ce

.field public static final scannedlist:I = 0x7f0808d7

.field public static final scanning_header_text:I = 0x7f0808c8

.field public static final scanning_txt:I = 0x7f0808d3

.field public static final score_board_button:I = 0x7f080c23

.field public static final score_coach_message:I = 0x7f0801f6

.field public static final scover_dialog_show_again:I = 0x7f0805b1

.field public static final screenshot_layout:I = 0x7f0807c9

.field public static final scrollView:I = 0x7f08057e

.field public static final scroll_account_text:I = 0x7f0808c0

.field public static final scroll_view:I = 0x7f0803a8

.field public static final scroller:I = 0x7f080512

.field public static final scrollview:I = 0x7f0808c6

.field public static final search_edit_text:I = 0x7f0801d7

.field public static final search_food_in_google:I = 0x7f08046a

.field public static final search_hint_text:I = 0x7f0801d9

.field public static final search_icon:I = 0x7f0801d8

.field public static final search_list_empty_view:I = 0x7f0801d5

.field public static final search_logo:I = 0x7f08046b

.field public static final secondSyncedGearDevice:I = 0x7f080c13

.field public static final second_box:I = 0x7f0807da

.field public static final second_button:I = 0x7f080824

.field public static final second_card:I = 0x7f08085d

.field public static final second_icon:I = 0x7f080b87

.field public static final second_info:I = 0x7f08035e

.field public static final second_info_area:I = 0x7f080ba2

.field public static final second_info_avg:I = 0x7f080363

.field public static final second_info_box:I = 0x7f080ba3

.field public static final second_info_data:I = 0x7f080360

.field public static final second_info_data_unit:I = 0x7f080362

.field public static final second_info_title:I = 0x7f08035f

.field public static final second_info_unit:I = 0x7f080ba4

.field public static final second_info_unit_box:I = 0x7f080361

.field public static final second_legend_mark_layout:I = 0x7f080645

.field public static final second_sub_tab:I = 0x7f0802fb

.field public static final second_sub_tab_rd_btn:I = 0x7f0802fc

.field public static final second_text:I = 0x7f080529

.field public static final secondary_text:I = 0x7f080649

.field public static final section_header_text:I = 0x7f0801c2

.field public static final security:I = 0x7f0808e3

.field public static final security_check_box_container:I = 0x7f0808d9

.field public static final select:I = 0x7f080c8b

.field public static final select_all_checkbox:I = 0x7f080245

.field public static final select_all_layout:I = 0x7f080244

.field public static final select_device_to_view:I = 0x7f080928

.field public static final selected_item_root:I = 0x7f08046d

.field public static final selected_panel:I = 0x7f080462

.field public static final selected_panel_content_layout:I = 0x7f080494

.field public static final server_check_loading_img:I = 0x7f080685

.field public static final serving_size:I = 0x7f080426

.field public static final set_calories_scroll_view:I = 0x7f0805a6

.field public static final set_challenge_button:I = 0x7f080c71

.field public static final set_pin_code_check_box:I = 0x7f0808da

.field public static final set_quick_input:I = 0x7f080c9a

.field public static final set_time_setting:I = 0x7f080bec

.field public static final setgoal:I = 0x7f080cb1

.field public static final setting:I = 0x7f080022

.field public static final setting_layout:I = 0x7f080021

.field public static final settings:I = 0x7f080ca4

.field public static final settings_main_root_view:I = 0x7f0808dd

.field public static final settings_menu:I = 0x7f080c9c

.field public static final sfview:I = 0x7f08073f

.field public static final share:I = 0x7f080ca9

.field public static final shareViaHomeLayout:I = 0x7f080026

.field public static final share_content_frame:I = 0x7f080027

.field public static final share_result_layout:I = 0x7f0807d1

.field public static final share_via:I = 0x7f080c90

.field public static final share_view_date:I = 0x7f08079b

.field public static final share_view_main_title:I = 0x7f080799

.field public static final share_view_main_title2:I = 0x7f08079a

.field public static final sharevia:I = 0x7f080c98

.field public static final shealthIconImg:I = 0x7f0802e8

.field public static final shealth_heartrate_tip_4_1:I = 0x7f080911

.field public static final shealth_heartrate_tip_4_2:I = 0x7f080913

.field public static final shealth_hrm_toast_message_wrap:I = 0x7f0805cc

.field public static final shortcut_area:I = 0x7f0804f7

.field public static final shortcut_container:I = 0x7f080513

.field public static final should_lose_weight_text_view:I = 0x7f080c64

.field public static final show_new_score:I = 0x7f080233

.field public static final signal_status_layout:I = 0x7f080826

.field public static final single_line_text:I = 0x7f0804b2

.field public static final skinType:I = 0x7f080caf

.field public static final skinType1:I = 0x7f080ac6

.field public static final skinType1_parent:I = 0x7f080ac5

.field public static final skinType2:I = 0x7f080ac8

.field public static final skinType2_parent:I = 0x7f080ac7

.field public static final skinType3:I = 0x7f080aca

.field public static final skinType3_parent:I = 0x7f080ac9

.field public static final skinType4:I = 0x7f080acc

.field public static final skinType4_parent:I = 0x7f080acb

.field public static final skinType5:I = 0x7f080ace

.field public static final skinType5_parent:I = 0x7f080acd

.field public static final skinType6:I = 0x7f080ad0

.field public static final skinType6_parent:I = 0x7f080acf

.field public static final skinscroll_parent:I = 0x7f080ac2

.field public static final sleep_average_data_view:I = 0x7f08097f

.field public static final sleep_average_no_data_view:I = 0x7f08097e

.field public static final sleep_chart_explanatory_notes:I = 0x7f080941

.field public static final sleep_dialog_checkbox_parent:I = 0x7f080988

.field public static final sleep_green_layout_top:I = 0x7f08096b

.field public static final sleep_linearLayout1:I = 0x7f080939

.field public static final sleep_linearLayout2:I = 0x7f08093a

.field public static final sleep_log_rating_bar:I = 0x7f080965

.field public static final sleep_log_rating_field:I = 0x7f080964

.field public static final sleep_rating_bar_instance_one:I = 0x7f080978

.field public static final sleep_rating_bar_instance_two:I = 0x7f08097b

.field public static final sleep_rating_divider:I = 0x7f080966

.field public static final sleep_time_hour_value:I = 0x7f080980

.field public static final sleep_tipboxtv:I = 0x7f08093b

.field public static final sleep_total_data_view:I = 0x7f080970

.field public static final sleep_total_no_data_view:I = 0x7f08096f

.field public static final sleeptime:I = 0x7f08095f

.field public static final slider:I = 0x7f0807bb

.field public static final slowest_speed:I = 0x7f080716

.field public static final slowest_title:I = 0x7f080712

.field public static final sodium:I = 0x7f080437

.field public static final sodium_percent:I = 0x7f080438

.field public static final sonstiges_german_id:I = 0x7f080a27

.field public static final sonstiges_text_german_id:I = 0x7f080a28

.field public static final space_divider:I = 0x7f0806f2

.field public static final special_pp_id:I = 0x7f0806b3

.field public static final special_pp_open_id:I = 0x7f080699

.field public static final special_pp_text1_id:I = 0x7f0806b4

.field public static final special_pp_text1_open_id:I = 0x7f08069a

.field public static final special_t_and_c_id:I = 0x7f080a25

.field public static final special_t_and_c_text1_id:I = 0x7f080a26

.field public static final spfText:I = 0x7f080af0

.field public static final spf_container:I = 0x7f080b19

.field public static final spf_icon:I = 0x7f080b1b

.field public static final spf_layout:I = 0x7f080a96

.field public static final spf_layout_divider:I = 0x7f080a95

.field public static final spf_picker:I = 0x7f080af1

.field public static final spinner_child_layout:I = 0x7f080246

.field public static final spinner_group_layout:I = 0x7f080248

.field public static final spinner_item_txt:I = 0x7f080247

.field public static final spinner_view_holder:I = 0x7f08030c

.field public static final split_line:I = 0x7f080bd4

.field public static final split_line_1:I = 0x7f08042c

.field public static final split_line_2:I = 0x7f08042f

.field public static final split_line_3:I = 0x7f080433

.field public static final split_line_4:I = 0x7f080436

.field public static final split_line_5:I = 0x7f080439

.field public static final split_line_6:I = 0x7f08043c

.field public static final split_line_7:I = 0x7f080440

.field public static final split_line_8:I = 0x7f080442

.field public static final split_line_9:I = 0x7f080421

.field public static final split_line_above_daily_value:I = 0x7f080429

.field public static final spo2_first_measure_warning_1_text:I = 0x7f08099a

.field public static final spo2_graph_avg:I = 0x7f080517

.field public static final spo2_info_3:I = 0x7f0809b2

.field public static final spo2_info_4:I = 0x7f0809b3

.field public static final spo2_input_log_bar:I = 0x7f080993

.field public static final spo2_log_detail_bar:I = 0x7f08098a

.field public static final spo2_recommended_low_messages_1_message_1:I = 0x7f0809b1

.field public static final spo2_small_bar:I = 0x7f080519

.field public static final spo2_small_polygon:I = 0x7f080518

.field public static final spo2_third_layout:I = 0x7f0809c3

.field public static final spo2_warning:I = 0x7f080999

.field public static final start:I = 0x7f080cb3

.field public static final state_bar_high:I = 0x7f0804fd

.field public static final state_bar_indicator:I = 0x7f0804fb

.field public static final state_bar_indicator_type2:I = 0x7f080936

.field public static final state_bar_low:I = 0x7f0804fc

.field public static final status_bar2:I = 0x7f0805b4

.field public static final status_bar_high:I = 0x7f08098c

.field public static final status_bar_highText:I = 0x7f0809be

.field public static final status_bar_high_input:I = 0x7f080995

.field public static final status_bar_low:I = 0x7f08098b

.field public static final status_bar_lowText:I = 0x7f0809bb

.field public static final status_bar_low_input:I = 0x7f080994

.field public static final status_bar_middle1:I = 0x7f0809bc

.field public static final status_bar_middle2:I = 0x7f0809bd

.field public static final status_barbase1:I = 0x7f0805ad

.field public static final status_barbase2:I = 0x7f0805b3

.field public static final status_barbase3:I = 0x7f0805b5

.field public static final status_icon:I = 0x7f080a84

.field public static final step:I = 0x7f080b34

.field public static final step_goal:I = 0x7f080927

.field public static final step_info:I = 0x7f080b37

.field public static final steps:I = 0x7f080b57

.field public static final steps_goal:I = 0x7f080b53

.field public static final steps_small_icon:I = 0x7f080bc2

.field public static final steps_small_icon_dim:I = 0x7f080bc7

.field public static final steps_small_icon_healthy:I = 0x7f080bc4

.field public static final steps_small_icon_inactive:I = 0x7f080bc5

.field public static final steps_small_icon_medal:I = 0x7f080bc6

.field public static final steps_small_icon_paused:I = 0x7f080bc3

.field public static final steps_small_value:I = 0x7f080bc8

.field public static final steps_small_value_k:I = 0x7f080bca

.field public static final steps_small_value_k_paused:I = 0x7f080bcb

.field public static final steps_small_value_paused:I = 0x7f080bc9

.field public static final steps_unit:I = 0x7f080b51

.field public static final steps_value:I = 0x7f080b5d

.field public static final steps_value_1:I = 0x7f080b4d

.field public static final steps_value_1_1:I = 0x7f080b4e

.field public static final steps_value_2:I = 0x7f080b4f

.field public static final steps_value_2_1:I = 0x7f080b50

.field public static final steps_value_k:I = 0x7f080b5f

.field public static final steps_value_k_paused:I = 0x7f080b60

.field public static final steps_value_layout:I = 0x7f080b4c

.field public static final steps_value_paused:I = 0x7f080b5e

.field public static final steps_value_root_layout:I = 0x7f080b4b

.field public static final stm_100:I = 0x7f08051d

.field public static final stm_70:I = 0x7f08051a

.field public static final stm_90:I = 0x7f08051b

.field public static final stm_95:I = 0x7f08051c

.field public static final stm_bar_index:I = 0x7f08076a

.field public static final stm_bar_index_high:I = 0x7f08076c

.field public static final stm_bar_index_low:I = 0x7f08076b

.field public static final stm_detail_bar:I = 0x7f0809ea

.field public static final stm_detail_polygon:I = 0x7f080765

.field public static final stm_discard_button:I = 0x7f0809f5

.field public static final stm_error_finger_motion:I = 0x7f0809d5

.field public static final stm_error_finger_pressure:I = 0x7f0809d4

.field public static final stm_error_first_message:I = 0x7f0809d3

.field public static final stm_gradient_first:I = 0x7f0809ef

.field public static final stm_gradient_last:I = 0x7f0809f0

.field public static final stm_graph_avg:I = 0x7f0809ec

.field public static final stm_graph_bar:I = 0x7f0809ee

.field public static final stm_graph_info:I = 0x7f0809eb

.field public static final stm_graph_polygon:I = 0x7f0809ed

.field public static final stm_information_1:I = 0x7f0809d8

.field public static final stm_information_1_text:I = 0x7f0809d9

.field public static final stm_information_2:I = 0x7f0809da

.field public static final stm_information_2_text:I = 0x7f0809db

.field public static final stm_information_3:I = 0x7f0809dc

.field public static final stm_information_3_text:I = 0x7f0809dd

.field public static final stm_information_4:I = 0x7f0809de

.field public static final stm_information_4_text:I = 0x7f0809df

.field public static final stm_information_5:I = 0x7f0809e0

.field public static final stm_information_5_text:I = 0x7f0809e1

.field public static final stm_information_6:I = 0x7f0809e2

.field public static final stm_information_6_text:I = 0x7f0809e3

.field public static final stm_log_bar:I = 0x7f08051f

.field public static final stm_log_polygon:I = 0x7f08051e

.field public static final stm_small_bar:I = 0x7f0809f4

.field public static final stm_small_polygon:I = 0x7f0809f3

.field public static final stm_start_button:I = 0x7f080a06

.field public static final stm_summary_bar:I = 0x7f0804fa

.field public static final stm_summary_polygon:I = 0x7f0804f9

.field public static final stm_summary_polygon_type2:I = 0x7f080935

.field public static final stm_textView_high_summary:I = 0x7f0809f2

.field public static final stm_textView_low_summary:I = 0x7f0809f1

.field public static final stress_check_box:I = 0x7f080384

.field public static final stress_line:I = 0x7f080385

.field public static final stress_test_score:I = 0x7f0809fb

.field public static final stress_title:I = 0x7f080383

.field public static final stroke:I = 0x7f0802c7

.field public static final stroke_no_anime:I = 0x7f0802c8

.field public static final sub_title:I = 0x7f0804e5

.field public static final sub_title_text:I = 0x7f0803d4

.field public static final subtabs_rd_grp:I = 0x7f0802f8

.field public static final sugars:I = 0x7f08043f

.field public static final suggest_horizontal_divider:I = 0x7f08026c

.field public static final suggest_horizontal_divider_main:I = 0x7f0801c1

.field public static final suggest_list_empty_view:I = 0x7f080252

.field public static final suggest_no_data:I = 0x7f080249

.field public static final suggest_no_data_description:I = 0x7f08024c

.field public static final suggest_no_data_description_2:I = 0x7f08024d

.field public static final suggest_no_data_text:I = 0x7f08024b

.field public static final suggest_no_data_text_extra:I = 0x7f08024a

.field public static final suggested_bottom_image:I = 0x7f08015b

.field public static final suggested_btn_cancel:I = 0x7f080257

.field public static final suggested_btn_ok:I = 0x7f080258

.field public static final suggested_contents_container:I = 0x7f080256

.field public static final suggested_detail_contents_goal_txt_description:I = 0x7f08025d

.field public static final suggested_detail_contents_mission_txt_tracking_instructions:I = 0x7f080264

.field public static final suggested_detail_contents_mission_txt_what_to_do:I = 0x7f080261

.field public static final suggested_detail_contents_mission_txt_why_do_it:I = 0x7f080263

.field public static final suggested_detail_mission_btn_startdate:I = 0x7f080278

.field public static final suggested_goal_min_frequency_txt:I = 0x7f08026b

.field public static final suggested_goal_why_do_it:I = 0x7f08025c

.field public static final suggested_header_view:I = 0x7f080255

.field public static final suggested_list:I = 0x7f080251

.field public static final suggested_list_item_container:I = 0x7f080266

.field public static final suggested_list_item_icon:I = 0x7f080268

.field public static final suggested_list_item_icon_area:I = 0x7f080267

.field public static final suggested_list_item_txt_extraString:I = 0x7f08026a

.field public static final suggested_list_item_txt_title:I = 0x7f080269

.field public static final suggested_list_layout:I = 0x7f080250

.field public static final suggested_mission_detail_category_icon:I = 0x7f080271

.field public static final suggested_mission_detail_normal_header:I = 0x7f08025f

.field public static final suggested_mission_what_to_do_txt:I = 0x7f080260

.field public static final suggested_mission_why_do_it_txt:I = 0x7f080262

.field public static final suggested_more:I = 0x7f080253

.field public static final suggested_more_btn:I = 0x7f080254

.field public static final suggested_spinner_frequency:I = 0x7f080274

.field public static final suggested_spinner_frequency_layout:I = 0x7f080273

.field public static final suggested_top_area:I = 0x7f08026d

.field public static final suggested_top_coach_message:I = 0x7f080270

.field public static final suggested_top_content:I = 0x7f08026f

.field public static final suggested_top_title:I = 0x7f08026e

.field public static final suggested_top_view:I = 0x7f08025e

.field public static final suggested_txt_frequency:I = 0x7f080276

.field public static final suggested_txt_frequency_layout:I = 0x7f080275

.field public static final suggested_txt_title:I = 0x7f080272

.field public static final suggestion_text:I = 0x7f080bfe

.field public static final summary_center_view:I = 0x7f080c4a

.field public static final summary_center_view_bmi:I = 0x7f080c81

.field public static final summary_center_view_bmi_label:I = 0x7f080c82

.field public static final summary_center_view_bmi_layout:I = 0x7f080c43

.field public static final summary_center_view_bmi_value:I = 0x7f080c44

.field public static final summary_center_view_bmr_layout:I = 0x7f080c52

.field public static final summary_center_view_body_age_layout:I = 0x7f080c4f

.field public static final summary_center_view_body_fat_layout:I = 0x7f080c40

.field public static final summary_center_view_height:I = 0x7f080c4c

.field public static final summary_center_view_height_layout:I = 0x7f080c4b

.field public static final summary_center_view_height_unit:I = 0x7f080c4e

.field public static final summary_center_view_height_value:I = 0x7f080c4d

.field public static final summary_center_view_skeletal_muscle_layout:I = 0x7f080c3d

.field public static final summary_center_view_visceral_fat_layout:I = 0x7f080c55

.field public static final summary_center_view_visceral_fat_unit:I = 0x7f080c57

.field public static final summary_center_view_visceral_fat_value:I = 0x7f080c56

.field public static final summary_container:I = 0x7f0803f0

.field public static final summary_crown_icon_new_record:I = 0x7f080c06

.field public static final summary_food_intake_calorie_view:I = 0x7f0804bf

.field public static final summary_food_txt_goal_value:I = 0x7f0804c0

.field public static final summary_frag_container:I = 0x7f080a08

.field public static final summary_goal_value_layout:I = 0x7f080c0d

.field public static final summary_icon_combined:I = 0x7f080c08

.field public static final summary_icon_default:I = 0x7f0804bc

.field public static final summary_icon_gear:I = 0x7f080c09

.field public static final summary_icon_layout:I = 0x7f0804bb

.field public static final summary_icon_medal:I = 0x7f0804bd

.field public static final summary_icon_paused:I = 0x7f080c0a

.field public static final summary_progress:I = 0x7f0804ba

.field public static final summary_progress_layout:I = 0x7f0804b9

.field public static final summary_second_center_icon_big_container:I = 0x7f0809c1

.field public static final summary_second_icon_layout:I = 0x7f0805d8

.field public static final summary_top_view:I = 0x7f080c3c

.field public static final summary_view_bmr_unit:I = 0x7f080c54

.field public static final summary_view_bmr_value:I = 0x7f080c53

.field public static final summary_view_body_age_unit:I = 0x7f080c51

.field public static final summary_view_body_age_value:I = 0x7f080c50

.field public static final summary_view_body_fat_unit:I = 0x7f080c42

.field public static final summary_view_body_fat_value:I = 0x7f080c41

.field public static final summary_view_content_balance_statistics_text:I = 0x7f080a83

.field public static final summary_view_content_balance_text:I = 0x7f080a82

.field public static final summary_view_content_icon:I = 0x7f080a7e

.field public static final summary_view_content_layout:I = 0x7f080a7c

.field public static final summary_view_content_text:I = 0x7f080a7f

.field public static final summary_view_content_title_text:I = 0x7f080a81

.field public static final summary_view_content_value_layout:I = 0x7f080a7d

.field public static final summary_view_divider:I = 0x7f0803ef

.field public static final summary_view_divider_line:I = 0x7f080c80

.field public static final summary_view_skeletal_muscle_unit:I = 0x7f080c3f

.field public static final summary_view_skeletal_muscle_value:I = 0x7f080c3e

.field public static final summary_walkforlife_container_current:I = 0x7f0804be

.field public static final summary_walkforlife_container_status:I = 0x7f080c15

.field public static final summary_walkforlife_container_status_distance:I = 0x7f080c16

.field public static final summary_walkforlife_container_status_distance_top:I = 0x7f080c17

.field public static final summary_walkforlife_container_status_kcal:I = 0x7f080c1b

.field public static final summary_walkforlife_container_status_kcal_top:I = 0x7f080c1c

.field public static final summary_walkforlife_k:I = 0x7f080c0c

.field public static final summary_walkforlife_new_record_txt_view:I = 0x7f080c07

.field public static final summary_walkforlife_steps_count:I = 0x7f080c0b

.field public static final summary_walkforlife_txt_goal:I = 0x7f080c0e

.field public static final summary_walkforlife_txt_status_distance:I = 0x7f080c1a

.field public static final summary_walkforlife_txt_status_distance_value:I = 0x7f080c18

.field public static final summary_walkforlife_txt_status_kcal:I = 0x7f080c1e

.field public static final summary_walkforlife_txt_status_kcal_value:I = 0x7f080c1d

.field public static final summary_walkforlife_txt_status_km:I = 0x7f080c19

.field public static final summary_walkforlife_txt_status_wastedcalories:I = 0x7f080c1f

.field public static final summary_walkforlife_update_layout:I = 0x7f080c20

.field public static final summary_walkforlife_update_time_value:I = 0x7f080c21

.field public static final summary_weight_icon:I = 0x7f080c46

.field public static final summary_weight_indicator_view:I = 0x7f080c45

.field public static final summary_weight_range_state_value:I = 0x7f080c47

.field public static final summer:I = 0x7f0802c5

.field public static final sunProtection_layout:I = 0x7f080a9b

.field public static final sunProtection_layout_divider:I = 0x7f080a9a

.field public static final sv_devicelayout:I = 0x7f0802dd

.field public static final sync_animation_view:I = 0x7f080c22

.field public static final sync_button:I = 0x7f080521

.field public static final sync_click_layout:I = 0x7f080b52

.field public static final sync_progress:I = 0x7f080520

.field public static final sync_progress_bar:I = 0x7f080522

.field public static final sync_time:I = 0x7f080b3b

.field public static final synced_device:I = 0x7f080957

.field public static final synced_device_layout:I = 0x7f080956

.field public static final tab_1th_layout:I = 0x7f080733

.field public static final tab_2th_layout:I = 0x7f080735

.field public static final tab_3th_layout:I = 0x7f080736

.field public static final tab_map:I = 0x7f080734

.field public static final table:I = 0x7f0802b4

.field public static final tabs_layout:I = 0x7f080732

.field public static final tagIndex:I = 0x7f080597

.field public static final tag_information:I = 0x7f080ca3

.field public static final tag_list_Header:I = 0x7f0805fa

.field public static final tag_listview:I = 0x7f08054e

.field public static final tag_view_by:I = 0x7f080ca1

.field public static final tagging_edit:I = 0x7f080ca2

.field public static final tagging_select:I = 0x7f080ca5

.field public static final take_photo:I = 0x7f08039f

.field public static final tap_to_get_started:I = 0x7f080b67

.field public static final tc_australia_part_1:I = 0x7f080a2a

.field public static final tc_australia_part_2:I = 0x7f080a3e

.field public static final tc_australia_part_3:I = 0x7f080a51

.field public static final tc_australia_start_content_1_id:I = 0x7f080a2b

.field public static final tc_australia_start_content_2_id:I = 0x7f080a2c

.field public static final tc_australia_start_content_3_id:I = 0x7f080a2d

.field public static final tc_australia_start_content_4_id:I = 0x7f080a2e

.field public static final tc_australia_subject_10_content_1_id:I = 0x7f080a5f

.field public static final tc_australia_subject_10_content_2_id:I = 0x7f080a60

.field public static final tc_australia_subject_10_content_3_id:I = 0x7f080a61

.field public static final tc_australia_subject_10_content_4_id:I = 0x7f080a62

.field public static final tc_australia_subject_10_content_5_id:I = 0x7f080a63

.field public static final tc_australia_subject_10_content_6_id:I = 0x7f080a64

.field public static final tc_australia_subject_10_title_id:I = 0x7f080a5e

.field public static final tc_australia_subject_11_content_1_id:I = 0x7f080a66

.field public static final tc_australia_subject_11_title_id:I = 0x7f080a65

.field public static final tc_australia_subject_12_content_1_id:I = 0x7f080a67

.field public static final tc_australia_subject_1_content_1_id:I = 0x7f080a36

.field public static final tc_australia_subject_1_content_2_id:I = 0x7f080a37

.field public static final tc_australia_subject_1_content_3_id:I = 0x7f080a38

.field public static final tc_australia_subject_1_title_id:I = 0x7f080a35

.field public static final tc_australia_subject_2_content_1_id:I = 0x7f080a3a

.field public static final tc_australia_subject_2_content_2_id:I = 0x7f080a3b

.field public static final tc_australia_subject_2_title_id:I = 0x7f080a39

.field public static final tc_australia_subject_3_content_1_id:I = 0x7f080a3d

.field public static final tc_australia_subject_3_title_id:I = 0x7f080a3c

.field public static final tc_australia_subject_4_content_1_id:I = 0x7f080a40

.field public static final tc_australia_subject_4_content_2_id:I = 0x7f080a41

.field public static final tc_australia_subject_4_content_3_id:I = 0x7f080a42

.field public static final tc_australia_subject_4_content_4_id:I = 0x7f080a43

.field public static final tc_australia_subject_4_content_5_id:I = 0x7f080a44

.field public static final tc_australia_subject_4_title_id:I = 0x7f080a3f

.field public static final tc_australia_subject_5_a_content_1_id:I = 0x7f080a46

.field public static final tc_australia_subject_5_a_content_2_id:I = 0x7f080a47

.field public static final tc_australia_subject_5_a_content_3_id:I = 0x7f080a48

.field public static final tc_australia_subject_5_a_content_4_id:I = 0x7f080a49

.field public static final tc_australia_subject_5_a_content_5_id:I = 0x7f080a4a

.field public static final tc_australia_subject_5_a_title_id:I = 0x7f080a45

.field public static final tc_australia_subject_5_b_content_1_id:I = 0x7f080a4c

.field public static final tc_australia_subject_5_b_content_2_id:I = 0x7f080a4d

.field public static final tc_australia_subject_5_b_content_3_id:I = 0x7f080a4e

.field public static final tc_australia_subject_5_b_content_4_id:I = 0x7f080a4f

.field public static final tc_australia_subject_5_b_content_5_id:I = 0x7f080a50

.field public static final tc_australia_subject_5_b_title_id:I = 0x7f080a4b

.field public static final tc_australia_subject_6_content_1_id:I = 0x7f080a53

.field public static final tc_australia_subject_6_content_2_id:I = 0x7f080a54

.field public static final tc_australia_subject_6_title_id:I = 0x7f080a52

.field public static final tc_australia_subject_7_content_1_id:I = 0x7f080a56

.field public static final tc_australia_subject_7_content_2_id:I = 0x7f080a57

.field public static final tc_australia_subject_7_title_id:I = 0x7f080a55

.field public static final tc_australia_subject_8_content_1_id:I = 0x7f080a59

.field public static final tc_australia_subject_8_content_2_id:I = 0x7f080a5a

.field public static final tc_australia_subject_8_content_3_id:I = 0x7f080a5b

.field public static final tc_australia_subject_8_title_id:I = 0x7f080a58

.field public static final tc_australia_subject_9_content_1_id:I = 0x7f080a5d

.field public static final tc_australia_subject_9_title_id:I = 0x7f080a5c

.field public static final tc_australia_use_of_shealth_content_1_id:I = 0x7f080a30

.field public static final tc_australia_use_of_shealth_content_2_id:I = 0x7f080a31

.field public static final tc_australia_use_of_shealth_content_3_id:I = 0x7f080a32

.field public static final tc_australia_use_of_shealth_content_4_id:I = 0x7f080a33

.field public static final tc_australia_use_of_shealth_content_5_id:I = 0x7f080a34

.field public static final tc_australia_use_of_shealth_title_id:I = 0x7f080a2f

.field public static final temp:I = 0x7f0802c3

.field public static final temperature_from:I = 0x7f0802d0

.field public static final temperature_range:I = 0x7f0802ca

.field public static final temperature_to:I = 0x7f0802d2

.field public static final temperature_unit_first:I = 0x7f0802d1

.field public static final temperature_unit_second:I = 0x7f0802d3

.field public static final temperature_unit_second_1:I = 0x7f0802d9

.field public static final term_bottom:I = 0x7f08028a

.field public static final term_kor:I = 0x7f0806b6

.field public static final term_layout:I = 0x7f080282

.field public static final term_open:I = 0x7f0806a7

.field public static final term_scrollView:I = 0x7f080281

.field public static final terms:I = 0x7f080283

.field public static final terms_and_pp_with_frame:I = 0x7f080a0d

.field public static final terms_base_layout:I = 0x7f080280

.field public static final terms_end:I = 0x7f080a29

.field public static final terms_in_initial_with_pp:I = 0x7f080a10

.field public static final terms_in_initial_with_pp_au:I = 0x7f080a0f

.field public static final terms_in_initial_with_pp_kor:I = 0x7f080a0e

.field public static final terms_in_settings:I = 0x7f080a0c

.field public static final terms_in_settings_au:I = 0x7f080a0b

.field public static final terms_in_settings_kor:I = 0x7f080a0a

.field public static final terms_pain:I = 0x7f0808f1

.field public static final terms_without_frame:I = 0x7f080a09

.field public static final terrain:I = 0x7f080003

.field public static final text:I = 0x7f08080a

.field public static final textAgree:I = 0x7f080286

.field public static final textAgreeRight:I = 0x7f080289

.field public static final textContent:I = 0x7f080044

.field public static final textDoNotShow:I = 0x7f08031c

.field public static final textParent:I = 0x7f080a6c

.field public static final textView1:I = 0x7f08080e

.field public static final textView2:I = 0x7f08080f

.field public static final textView_awaketime:I = 0x7f080963

.field public static final textView_sleepqualty:I = 0x7f08095d

.field public static final textView_sleeptime:I = 0x7f080960

.field public static final textView_totalsleep:I = 0x7f08095a

.field public static final text_area:I = 0x7f080bf5

.field public static final text_average:I = 0x7f0807a9

.field public static final text_average_value:I = 0x7f0807aa

.field public static final text_below_pin_code:I = 0x7f08064f

.field public static final text_best_record:I = 0x7f0807ac

.field public static final text_best_record_value:I = 0x7f0807ad

.field public static final text_bot:I = 0x7f080b80

.field public static final text_confirm_backup:I = 0x7f0808b4

.field public static final text_date:I = 0x7f0807f9

.field public static final text_date_left:I = 0x7f0807ed

.field public static final text_date_right:I = 0x7f0807f5

.field public static final text_icon:I = 0x7f080636

.field public static final text_list_divider:I = 0x7f080330

.field public static final text_my_ranking:I = 0x7f080b7d

.field public static final text_no_current_goal:I = 0x7f080218

.field public static final text_no_data:I = 0x7f080219

.field public static final text_planed_workout:I = 0x7f08080d

.field public static final text_planed_workout_layout:I = 0x7f08080b

.field public static final text_popup_hint:I = 0x7f08068a

.field public static final text_value:I = 0x7f0807fa

.field public static final text_value_left:I = 0x7f0807ee

.field public static final text_value_right:I = 0x7f0807f6

.field public static final text_view:I = 0x7f08046e

.field public static final text_view_calorie_per_serving:I = 0x7f080388

.field public static final text_view_kcal:I = 0x7f08038a

.field public static final text_view_name_of_food:I = 0x7f080396

.field public static final text_view_preview_days_of_week:I = 0x7f080402

.field public static final text_view_preview_period_range:I = 0x7f080401

.field public static final text_view_save_in_my_food:I = 0x7f080393

.field public static final texts:I = 0x7f080527

.field public static final textview_average:I = 0x7f080b97

.field public static final textview_best:I = 0x7f080b9a

.field public static final textview_you:I = 0x7f080b94

.field public static final tgh_humidity:I = 0x7f080a71

.field public static final tgh_information:I = 0x7f080a75

.field public static final tgh_information_avg_humidity_text:I = 0x7f080a7b

.field public static final tgh_information_avg_text:I = 0x7f080a77

.field public static final tgh_information_date:I = 0x7f080a73

.field public static final tgh_information_header:I = 0x7f080a74

.field public static final tgh_information_humidity:I = 0x7f080a79

.field public static final tgh_information_humidity_header:I = 0x7f080a78

.field public static final tgh_information_humidity_text:I = 0x7f080a7a

.field public static final tgh_information_percent_text:I = 0x7f080a76

.field public static final tgh_temperature:I = 0x7f080a70

.field public static final third_box:I = 0x7f0807db

.field public static final third_card:I = 0x7f080852

.field public static final third_info:I = 0x7f080364

.field public static final third_info_data:I = 0x7f080366

.field public static final third_info_data_unit:I = 0x7f080367

.field public static final third_info_title:I = 0x7f080365

.field public static final third_party_img:I = 0x7f08033d

.field public static final third_text:I = 0x7f0807dc

.field public static final three_cont:I = 0x7f080906

.field public static final three_views:I = 0x7f08072b

.field public static final time:I = 0x7f08003b

.field public static final time_filter:I = 0x7f080b84

.field public static final time_goal:I = 0x7f08074b

.field public static final time_goal_hint:I = 0x7f08074e

.field public static final time_goal_hint_rtl:I = 0x7f08074f

.field public static final time_goal_text:I = 0x7f08074c

.field public static final time_goal_text_rtl:I = 0x7f08074d

.field public static final time_layout:I = 0x7f0807a1

.field public static final time_of_sleep_instance_one:I = 0x7f080977

.field public static final time_of_sleep_instance_two:I = 0x7f08097a

.field public static final time_of_sleep_layout_instance_one:I = 0x7f080976

.field public static final time_of_sleep_layout_instance_two:I = 0x7f080979

.field public static final time_record_layout:I = 0x7f0807a7

.field public static final time_text:I = 0x7f080943

.field public static final time_without_activity:I = 0x7f080be8

.field public static final time_without_activity_txt:I = 0x7f080be9

.field public static final timepicker:I = 0x7f080a8c

.field public static final tip_article_detail_iv_background:I = 0x7f0801dd

.field public static final tip_article_detail_iv_background_layout:I = 0x7f0801dc

.field public static final tip_article_detail_webview_layout:I = 0x7f0801de

.field public static final tip_article_root:I = 0x7f0801db

.field public static final tip_box_1:I = 0x7f0804e1

.field public static final tip_box_1_tv:I = 0x7f080921

.field public static final tip_box_2:I = 0x7f08090c

.field public static final tip_box_3:I = 0x7f08090f

.field public static final tip_dot_1:I = 0x7f080910

.field public static final tip_dot_2:I = 0x7f080912

.field public static final tip_dot_3:I = 0x7f080914

.field public static final tip_dot_4:I = 0x7f080915

.field public static final tip_dot_5:I = 0x7f080916

.field public static final tip_dot_6:I = 0x7f080917

.field public static final tip_dot_7:I = 0x7f080934

.field public static final tipboxtv:I = 0x7f08092f

.field public static final tips_category_txt:I = 0x7f080145

.field public static final tips_expandable_group_header_layout:I = 0x7f080142

.field public static final tips_expandable_img_arrow:I = 0x7f080143

.field public static final tips_expandable_txt_title:I = 0x7f080141

.field public static final title:I = 0x7f08004d

.field public static final title_button:I = 0x7f0804e3

.field public static final title_devider:I = 0x7f0804e6

.field public static final title_layout:I = 0x7f080b90

.field public static final title_left:I = 0x7f0807ec

.field public static final title_name:I = 0x7f080425

.field public static final title_repeat:I = 0x7f08040c

.field public static final title_right:I = 0x7f0807f4

.field public static final title_text:I = 0x7f0802dc

.field public static final top:I = 0x7f080011

.field public static final top_container:I = 0x7f0804a6

.field public static final top_grid_view:I = 0x7f080341

.field public static final top_layout:I = 0x7f080774

.field public static final top_section:I = 0x7f080340

.field public static final total_calories_pan:I = 0x7f0804a3

.field public static final total_carbohydrate:I = 0x7f08043a

.field public static final total_carbohydrate_percent:I = 0x7f08043b

.field public static final total_data_area:I = 0x7f08096d

.field public static final total_fat:I = 0x7f08042d

.field public static final total_fat_percent:I = 0x7f08042e

.field public static final total_food:I = 0x7f0804a4

.field public static final total_hour_str:I = 0x7f080950

.field public static final total_hour_text:I = 0x7f08094f

.field public static final total_kcal:I = 0x7f0803f1

.field public static final total_minute_str:I = 0x7f080952

.field public static final total_minute_text:I = 0x7f080951

.field public static final total_portion:I = 0x7f080768

.field public static final total_sleep_area:I = 0x7f08094d

.field public static final total_sleep_hour_value:I = 0x7f080971

.field public static final total_sleep_label:I = 0x7f08096e

.field public static final total_sleep_portion:I = 0x7f080974

.field public static final total_sleep_title:I = 0x7f08094e

.field public static final totalsleep:I = 0x7f080959

.field public static final touch_lock_layout:I = 0x7f080795

.field public static final tracker_data_loading_iv_icon:I = 0x7f08029f

.field public static final tracker_data_loading_progress:I = 0x7f0802a0

.field public static final trackingview_category_txt:I = 0x7f08014e

.field public static final training_coach_distance_text:I = 0x7f080bd2

.field public static final training_effect_goal:I = 0x7f080757

.field public static final training_effect_goal_hint:I = 0x7f08075a

.field public static final training_effect_goal_hint_rtl:I = 0x7f08075b

.field public static final training_effect_goal_layout:I = 0x7f080756

.field public static final training_effect_goal_text:I = 0x7f080758

.field public static final training_effect_goal_text_rtl:I = 0x7f080759

.field public static final training_effect_guide_item:I = 0x7f0807c1

.field public static final training_effect_heading:I = 0x7f0807b4

.field public static final training_effect_level_content:I = 0x7f0807b8

.field public static final training_effect_level_sub_title:I = 0x7f0807b7

.field public static final training_effect_level_title:I = 0x7f0807b5

.field public static final training_effect_level_value:I = 0x7f0807b6

.field public static final training_effect_select_button:I = 0x7f0807b9

.field public static final trans_fat:I = 0x7f080432

.field public static final trophy:I = 0x7f080c00

.field public static final try_challenge_new_text_view:I = 0x7f080c70

.field public static final try_operate3:I = 0x7f08078d

.field public static final ttip_box_4:I = 0x7f080933

.field public static final tvActionTitle:I = 0x7f080309

.field public static final tvActionTitle2:I = 0x7f08030a

.field public static final tvShareViewDate:I = 0x7f08002a

.field public static final tvShareViewHomeTitle:I = 0x7f080028

.field public static final tvShareViewOtherTitle:I = 0x7f080029

.field public static final tv_actionbar_item:I = 0x7f080a6f

.field public static final tv_bmi_value:I = 0x7f080097

.field public static final tv_bpm_txt_logdetail:I = 0x7f0805aa

.field public static final tv_create_tag:I = 0x7f080543

.field public static final tv_dialog_clinical:I = 0x7f080565

.field public static final tv_dialog_title:I = 0x7f080568

.field public static final tv_dialog_title_content:I = 0x7f080abc

.field public static final tv_heartrate_infomation_bpm:I = 0x7f080598

.field public static final tv_heartrate_infomation_bpm_text:I = 0x7f080599

.field public static final tv_heartrate_infomation_date:I = 0x7f0805a3

.field public static final tv_heartrate_log_input_bpm:I = 0x7f08053c

.field public static final tv_heartrate_log_input_time_date:I = 0x7f08053a

.field public static final tv_hrm_summry_date_curr:I = 0x7f0805e2

.field public static final tv_icon_name:I = 0x7f08033f

.field public static final tv_infomation_date:I = 0x7f0809e8

.field public static final tv_infomation_time:I = 0x7f080b06

.field public static final tv_inputTag:I = 0x7f080546

.field public static final tv_inputTag_custom:I = 0x7f080549

.field public static final tv_lifestyle_comparison_score:I = 0x7f0801ed

.field public static final tv_lifestyle_comparison_str:I = 0x7f0801ee

.field public static final tv_lifestyle_sub_msg:I = 0x7f0801e8

.field public static final tv_log_child_body_bottom_left:I = 0x7f08058f

.field public static final tv_log_child_body_right:I = 0x7f080593

.field public static final tv_log_child_body_safe_time_bottom_left:I = 0x7f080afd

.field public static final tv_log_child_body_spf_bottom_left:I = 0x7f080afc

.field public static final tv_log_child_body_top_left:I = 0x7f08058e

.field public static final tv_log_child_body_top_left_o2:I = 0x7f08099f

.field public static final tv_log_child_body_top_left_tag:I = 0x7f0804e9

.field public static final tv_log_child_header_layout:I = 0x7f0809e4

.field public static final tv_log_child_header_left:I = 0x7f080589

.field public static final tv_log_child_header_right:I = 0x7f08058a

.field public static final tv_log_detail_accessory_type:I = 0x7f08052e

.field public static final tv_log_detail_bpm:I = 0x7f080531

.field public static final tv_log_detail_bpm_range:I = 0x7f080532

.field public static final tv_log_detail_comment:I = 0x7f080539

.field public static final tv_log_detail_comment_head:I = 0x7f080537

.field public static final tv_log_detail_notes:I = 0x7f08098f

.field public static final tv_log_detail_pulse:I = 0x7f08098d

.field public static final tv_log_detail_spo2:I = 0x7f080989

.field public static final tv_log_detail_time:I = 0x7f080a91

.field public static final tv_log_detail_time_date:I = 0x7f08052f

.field public static final tv_log_detail_uv_icon:I = 0x7f080a92

.field public static final tv_log_detail_uv_index:I = 0x7f080a93

.field public static final tv_log_detail_uv_spf_icon:I = 0x7f080a97

.field public static final tv_log_detail_uv_spf_title:I = 0x7f080a98

.field public static final tv_log_detail_uv_spf_value:I = 0x7f080a99

.field public static final tv_log_detail_uv_state:I = 0x7f080a94

.field public static final tv_log_detail_uv_sun_protection_time_title:I = 0x7f080a9c

.field public static final tv_log_detail_uv_sun_protection_time_value:I = 0x7f080a9d

.field public static final tv_log_detail_weight:I = 0x7f080c3a

.field public static final tv_log_detail_weight_date_layout:I = 0x7f080c38

.field public static final tv_log_detail_weight_layout:I = 0x7f080c39

.field public static final tv_log_group_first:I = 0x7f080583

.field public static final tv_log_group_second:I = 0x7f080584

.field public static final tv_log_group_third:I = 0x7f080585

.field public static final tv_log_input_bpm_range:I = 0x7f08053d

.field public static final tv_log_input_time:I = 0x7f080a9e

.field public static final tv_log_input_time_date:I = 0x7f0809cf

.field public static final tv_log_input_uv_icon:I = 0x7f080a9f

.field public static final tv_log_last_line:I = 0x7f08098e

.field public static final tv_progress_title:I = 0x7f08008a

.field public static final tv_reassessment_coach_description_first:I = 0x7f08023b

.field public static final tv_reassessment_coach_description_second:I = 0x7f08023c

.field public static final tv_reassessment_coach_message:I = 0x7f080238

.field public static final tv_reassessment_goal_warning_description:I = 0x7f080235

.field public static final tv_reassessment_goal_warning_message:I = 0x7f080234

.field public static final tv_reengaging_description:I = 0x7f08023e

.field public static final tv_reengaging_head_message:I = 0x7f08023d

.field public static final tv_scanning_info:I = 0x7f0808c9

.field public static final tv_shealth:I = 0x7f080b38

.field public static final tv_skintype_1_message:I = 0x7f080ad5

.field public static final tv_skintype_1_message_desc:I = 0x7f080ad6

.field public static final tv_skintype_2_message:I = 0x7f080ada

.field public static final tv_skintype_2_message_desc:I = 0x7f080adb

.field public static final tv_skintype_3_message:I = 0x7f080adf

.field public static final tv_skintype_3_message_desc:I = 0x7f080ae0

.field public static final tv_skintype_4_message:I = 0x7f080ae4

.field public static final tv_skintype_4_message_desc:I = 0x7f080ae5

.field public static final tv_skintype_5_message:I = 0x7f080ae9

.field public static final tv_skintype_5_message_desc:I = 0x7f080aea

.field public static final tv_skintype_6_message:I = 0x7f080aee

.field public static final tv_skintype_6_message_desc:I = 0x7f080aef

.field public static final tv_spo2_infomation_avg_text:I = 0x7f0809b0

.field public static final tv_spo2_infomation_bpm:I = 0x7f0809ae

.field public static final tv_spo2_infomation_bpm_header:I = 0x7f0809ad

.field public static final tv_spo2_infomation_bpm_text:I = 0x7f0809af

.field public static final tv_spo2_infomation_date:I = 0x7f0809a5

.field public static final tv_spo2_infomation_percent_text:I = 0x7f0809aa

.field public static final tv_spo2_infomation_spo2:I = 0x7f0809a9

.field public static final tv_spo2_infomation_spo2_avg_text:I = 0x7f0809ab

.field public static final tv_spo2_infomation_spo2_header:I = 0x7f0809a7

.field public static final tv_spo2_legend_pulse:I = 0x7f0809a3

.field public static final tv_spo2_legend_spo2:I = 0x7f0809a2

.field public static final tv_spo2_log_input_pulse:I = 0x7f080996

.field public static final tv_spo2_log_input_spo2:I = 0x7f080992

.field public static final tv_spo2_log_input_time_date:I = 0x7f080991

.field public static final tv_spo2_summary_discard:I = 0x7f0809b4

.field public static final tv_spo2_summary_start:I = 0x7f0809ce

.field public static final tv_suggested_detail_contents_goal_frequency:I = 0x7f08025a

.field public static final tv_suggested_detail_contents_goal_name:I = 0x7f080259

.field public static final tv_suggested_detail_contents_goal_question_response:I = 0x7f08025b

.field public static final tv_summary_first_message:I = 0x7f0805cb

.field public static final tv_summary_first_ready_message:I = 0x7f0809f9

.field public static final tv_summary_first_ready_status:I = 0x7f0809f8

.field public static final tv_summary_first_spo2_value_for_test:I = 0x7f0809b6

.field public static final tv_summary_first_status:I = 0x7f0805ca

.field public static final tv_summary_second_bpm:I = 0x7f0805be

.field public static final tv_summary_second_bpm_unit:I = 0x7f0805bf

.field public static final tv_summary_second_o2:I = 0x7f0809b8

.field public static final tv_summary_second_o2_unit:I = 0x7f0809b9

.field public static final tv_summary_third_bpm:I = 0x7f0805e9

.field public static final tv_summary_third_bpm_down:I = 0x7f0809cb

.field public static final tv_summary_third_bpm_unit:I = 0x7f0805ea

.field public static final tv_summary_third_bpm_unit_down:I = 0x7f0809cc

.field public static final tv_summary_third_comment:I = 0x7f080b0d

.field public static final tv_summary_third_date:I = 0x7f0805ec

.field public static final tv_summary_third_o2:I = 0x7f0809c4

.field public static final tv_summary_third_o2_unit:I = 0x7f0809c5

.field public static final tv_summary_third_pulse_icon:I = 0x7f0809c7

.field public static final tv_summary_third_pulse_icon_down:I = 0x7f0809ca

.field public static final tv_summary_third_time:I = 0x7f0809cd

.field public static final tv_summary_third_uv_comment:I = 0x7f080b26

.field public static final tv_summary_third_uv_image:I = 0x7f080b25

.field public static final tv_tag:I = 0x7f080535

.field public static final tv_tag_avg_txt:I = 0x7f080653

.field public static final tv_tag_avg_val:I = 0x7f080654

.field public static final tv_tag_body_left:I = 0x7f0805fc

.field public static final tv_tag_header:I = 0x7f0805f2

.field public static final tv_tag_icon:I = 0x7f08053f

.field public static final tv_tag_icon_select:I = 0x7f080540

.field public static final tv_tag_max_txt:I = 0x7f080656

.field public static final tv_tag_max_val:I = 0x7f080657

.field public static final tv_tag_min_txt:I = 0x7f080658

.field public static final tv_tag_min_val:I = 0x7f080659

.field public static final tv_tag_text:I = 0x7f0805de

.field public static final tv_tag_txt_logdetail:I = 0x7f0805a9

.field public static final tv_tap_start:I = 0x7f080b39

.field public static final tv_total_calories:I = 0x7f0803a7

.field public static final tv_uv_no_data_wearable:I = 0x7f080b00

.field public static final tv_uv_summry_date_curr:I = 0x7f080b02

.field public static final tw_backing_in_mb:I = 0x7f08008d

.field public static final tw_backing_up_percents:I = 0x7f08008c

.field public static final tw_backup:I = 0x7f0808bc

.field public static final tw_delete_data:I = 0x7f0808be

.field public static final tw_delete_data_text:I = 0x7f080328

.field public static final tw_delete_data_text_subtitle:I = 0x7f080327

.field public static final tw_restore:I = 0x7f0808bd

.field public static final tw_restore_text_layout:I = 0x7f080326

.field public static final tw_samsung_account_address_value:I = 0x7f0808b9

.field public static final tw_samsung_last_sync_time:I = 0x7f0808ba

.field public static final two_cont:I = 0x7f080905

.field public static final txtGroupExerciseInfo:I = 0x7f08034d

.field public static final txtGroupLeft:I = 0x7f080167

.field public static final txtGroupRight:I = 0x7f08093c

.field public static final txtGroupRowLeft:I = 0x7f08016d

.field public static final txtGroupRowLeftBelow:I = 0x7f08093f

.field public static final txtGroupRowLeftMiddle:I = 0x7f08093e

.field public static final txtGroupRowPoint:I = 0x7f08016e

.field public static final txtGroupRowRight:I = 0x7f080171

.field public static final txtGroupRowRightDuration:I = 0x7f080352

.field public static final txtTeamHeader:I = 0x7f08016a

.field public static final txtTeamKCal:I = 0x7f08034b

.field public static final txtTeamWeight:I = 0x7f08016b

.field public static final txtTopRightGoalKcal:I = 0x7f080777

.field public static final txtTopRightKcal:I = 0x7f080776

.field public static final txtUserName:I = 0x7f080711

.field public static final txt_backup_interval:I = 0x7f080056

.field public static final txt_below:I = 0x7f0808c3

.field public static final txt_cycling:I = 0x7f080833

.field public static final txt_dialog_backup_restore:I = 0x7f080684

.field public static final txt_dialog_backup_restore_coach:I = 0x7f0802b8

.field public static final txt_dialog_show_again:I = 0x7f080681

.field public static final txt_dialog_show_again_coach:I = 0x7f0802bb

.field public static final txt_hiking:I = 0x7f080835

.field public static final txt_notice_food:I = 0x7f080677

.field public static final txt_one:I = 0x7f0805b7

.field public static final txt_ranking_updated:I = 0x7f080b83

.field public static final txt_receiving:I = 0x7f08064c

.field public static final txt_running:I = 0x7f08082f

.field public static final txt_samsung_account_1:I = 0x7f0808c1

.field public static final txt_samsung_account_2:I = 0x7f0808c4

.field public static final txt_samsung_account_description:I = 0x7f0808b6

.field public static final txt_view_no_ranking_data:I = 0x7f080be7

.field public static final txt_walking:I = 0x7f080831

.field public static final txt_zero_ranking_updated:I = 0x7f080be3

.field public static final type_cycling:I = 0x7f080832

.field public static final type_hiking:I = 0x7f080834

.field public static final type_running:I = 0x7f08082e

.field public static final type_walking:I = 0x7f080830

.field public static final unit:I = 0x7f0807d5

.field public static final unit_dropdown_button:I = 0x7f08047b

.field public static final unit_icon:I = 0x7f080a80

.field public static final unit_settings_text:I = 0x7f0808e5

.field public static final unit_setup:I = 0x7f0808e4

.field public static final unit_setup_height_layout:I = 0x7f080a8e

.field public static final unit_setup_text:I = 0x7f080a8f

.field public static final unit_setup_value:I = 0x7f080a90

.field public static final unlock_button:I = 0x7f08081d

.field public static final unlock_pressed_layout:I = 0x7f08081b

.field public static final unlock_progress:I = 0x7f08081c

.field public static final unpair:I = 0x7f08067d

.field public static final unpair_layout:I = 0x7f08067c

.field public static final update:I = 0x7f0808f2

.field public static final update_layout:I = 0x7f080b54

.field public static final update_time:I = 0x7f080b55

.field public static final upper_divider_1:I = 0x7f080088

.field public static final upper_layout:I = 0x7f0802f1

.field public static final use_of_s_health_text_1_id:I = 0x7f080a17

.field public static final use_of_s_health_text_2_id:I = 0x7f080a18

.field public static final use_of_s_health_text_3_id:I = 0x7f080a19

.field public static final use_of_s_health_text_4_id:I = 0x7f080a1a

.field public static final use_of_s_health_text_5_id:I = 0x7f080a1b

.field public static final use_of_s_health_title_id:I = 0x7f080a16

.field public static final use_ranking_checkbox:I = 0x7f080850

.field public static final use_ranking_text:I = 0x7f080851

.field public static final user_private:I = 0x7f08006f

.field public static final uvSkinOption1:I = 0x7f080ad2

.field public static final uvSkinOption2:I = 0x7f080ad7

.field public static final uvSkinOption3:I = 0x7f080adc

.field public static final uvSkinOption4:I = 0x7f080ae1

.field public static final uvSkinOption5:I = 0x7f080ae6

.field public static final uvSkinOption6:I = 0x7f080aeb

.field public static final uv_bt_summary_second_discard_text:I = 0x7f080b0a

.field public static final uv_bt_summary_second_start_text:I = 0x7f080b29

.field public static final uv_device_connected_status:I = 0x7f080b27

.field public static final uv_error_first_message:I = 0x7f080ab8

.field public static final uv_error_motion:I = 0x7f080aba

.field public static final uv_error_move_talk:I = 0x7f080ab9

.field public static final uv_information_text_1:I = 0x7f080af4

.field public static final uv_information_text_2:I = 0x7f080af5

.field public static final uv_information_text_3:I = 0x7f080af6

.field public static final uv_information_text_4:I = 0x7f080af7

.field public static final uv_no_sensor_information_text_1:I = 0x7f080af8

.field public static final uv_no_sensor_information_text_2:I = 0x7f080af9

.field public static final uv_recommended_container:I = 0x7f080abb

.field public static final uv_recommended_extreme_layout:I = 0x7f080ac1

.field public static final uv_recommended_extreme_messages_5_message_1:I = 0x7f080aab

.field public static final uv_recommended_extreme_messages_5_message_2:I = 0x7f080aac

.field public static final uv_recommended_extreme_messages_5_message_3:I = 0x7f080aad

.field public static final uv_recommended_extreme_messages_5_message_4:I = 0x7f080aae

.field public static final uv_recommended_extreme_title:I = 0x7f080aaa

.field public static final uv_recommended_high_layout:I = 0x7f080abf

.field public static final uv_recommended_high_messages_3_message_1:I = 0x7f080aa1

.field public static final uv_recommended_high_messages_3_message_2:I = 0x7f080aa2

.field public static final uv_recommended_high_messages_3_message_3:I = 0x7f080aa3

.field public static final uv_recommended_high_messages_3_message_4:I = 0x7f080aa4

.field public static final uv_recommended_high_title:I = 0x7f080aa0

.field public static final uv_recommended_low_layout:I = 0x7f080abd

.field public static final uv_recommended_low_messages_1_message_1:I = 0x7f080ab5

.field public static final uv_recommended_low_messages_1_message_2:I = 0x7f080ab6

.field public static final uv_recommended_low_messages_1_message_3:I = 0x7f080ab7

.field public static final uv_recommended_low_title:I = 0x7f080ab4

.field public static final uv_recommended_moderate_layout:I = 0x7f080abe

.field public static final uv_recommended_moderate_messages_2_message_1:I = 0x7f080ab0

.field public static final uv_recommended_moderate_messages_2_message_2:I = 0x7f080ab1

.field public static final uv_recommended_moderate_messages_2_message_3:I = 0x7f080ab2

.field public static final uv_recommended_moderate_messages_2_message_4:I = 0x7f080ab3

.field public static final uv_recommended_moderate_title:I = 0x7f080aaf

.field public static final uv_recommended_veryhigh_layout:I = 0x7f080ac0

.field public static final uv_recommended_veryhigh_messages_4_message_1:I = 0x7f080aa6

.field public static final uv_recommended_veryhigh_messages_4_message_2:I = 0x7f080aa7

.field public static final uv_recommended_veryhigh_messages_4_message_3:I = 0x7f080aa8

.field public static final uv_recommended_veryhigh_messages_4_message_4:I = 0x7f080aa9

.field public static final uv_recommended_veryhigh_title:I = 0x7f080aa5

.field public static final uv_sensor_type:I = 0x7f080cb0

.field public static final uv_skintype_main_info:I = 0x7f080ac3

.field public static final uv_skintype_question1:I = 0x7f080ac4

.field public static final uv_skintype_question2:I = 0x7f080ad1

.field public static final uv_summary_top_parent:I = 0x7f080b0b

.field public static final v_tag_margin:I = 0x7f08052d

.field public static final value:I = 0x7f080037

.field public static final value_container:I = 0x7f080c62

.field public static final value_symbol:I = 0x7f080039

.field public static final video_dialog_heartrate:I = 0x7f08056b

.field public static final view_another_fied_container:I = 0x7f08038f

.field public static final view_another_field:I = 0x7f080390

.field public static final view_another_field_bottom_divider:I = 0x7f080391

.field public static final view_by:I = 0x7f080c8c

.field public static final view_for_bottomspace:I = 0x7f08073e

.field public static final view_nutrition_info_divider:I = 0x7f0803f6

.field public static final viewstepcount:I = 0x7f080cb2

.field public static final visual_guidance:I = 0x7f080790

.field public static final voice_button:I = 0x7f080449

.field public static final vv_dialog_movie:I = 0x7f080550

.field public static final vw_bpm_empty_view:I = 0x7f0805ac

.field public static final vw_bpm_fill_view:I = 0x7f0805ab

.field public static final vw_comment_head:I = 0x7f080536

.field public static final vw_tag_head:I = 0x7f080533

.field public static final wakeup_time:I = 0x7f080962

.field public static final walk_average_layout:I = 0x7f080bb4

.field public static final walk_average_text:I = 0x7f080bb5

.field public static final walk_average_value:I = 0x7f080bb6

.field public static final walk_best_record_layout:I = 0x7f080bb8

.field public static final walk_best_record_title:I = 0x7f080bb9

.field public static final walk_best_record_value:I = 0x7f080bba

.field public static final walk_center_view:I = 0x7f080bb7

.field public static final walk_cocktailwidget_details:I = 0x7f080b70

.field public static final walk_cocktailwidget_inactive:I = 0x7f080b76

.field public static final walk_cocktailwidget_inactive_time:I = 0x7f080b77

.field public static final walk_cocktailwidget_init:I = 0x7f080b65

.field public static final walk_cocktailwidget_integrated_first_gear_step:I = 0x7f080b6b

.field public static final walk_cocktailwidget_integrated_gear:I = 0x7f080b68

.field public static final walk_cocktailwidget_integrated_gear_first_step_unit:I = 0x7f080b6c

.field public static final walk_cocktailwidget_integrated_gear_step:I = 0x7f080b69

.field public static final walk_cocktailwidget_integrated_gear_step_unit:I = 0x7f080b6a

.field public static final walk_cocktailwidget_normal:I = 0x7f080b6d

.field public static final walk_cocktailwidget_paused:I = 0x7f080b75

.field public static final walk_cocktailwidget_walk_mate_cal:I = 0x7f080b73

.field public static final walk_cocktailwidget_walk_mate_cal_unit:I = 0x7f080b74

.field public static final walk_cocktailwidget_walk_mate_distance:I = 0x7f080b71

.field public static final walk_cocktailwidget_walk_mate_distance_unit:I = 0x7f080b72

.field public static final walk_cocktailwidget_walk_mate_icon:I = 0x7f080b64

.field public static final walk_cocktailwidget_walk_mate_logo:I = 0x7f080b63

.field public static final walk_cocktailwidget_walk_mate_step:I = 0x7f080b6e

.field public static final walk_cocktailwidget_walk_mate_step_unit:I = 0x7f080b6f

.field public static final walk_cocktailwidget_walk_mate_sync:I = 0x7f080b78

.field public static final walk_cocktailwidget_walk_mate_sync_icon:I = 0x7f080b79

.field public static final walk_controller_layout:I = 0x7f080bbb

.field public static final walk_graph_bottom_view:I = 0x7f080b9b

.field public static final walk_graph_legend:I = 0x7f080b9c

.field public static final walk_inputmodule_btn_decrease:I = 0x7f080bbc

.field public static final walk_inputmodule_btn_increase:I = 0x7f080bbe

.field public static final walk_inputmodule_et_value:I = 0x7f080bb2

.field public static final walk_inputmodule_tv_unit:I = 0x7f080bb3

.field public static final walk_my_zero:I = 0x7f080be2

.field public static final walk_piece_of_pie_diagram:I = 0x7f080ba6

.field public static final walking_goal_input_module:I = 0x7f080b88

.field public static final walking_goal_title_text:I = 0x7f080bb1

.field public static final warning_txt:I = 0x7f080a87

.field public static final wearable:I = 0x7f080b3a

.field public static final webview:I = 0x7f080670

.field public static final weight_graph_bottom_view:I = 0x7f080354

.field public static final weight_indicator_linear:I = 0x7f080c85

.field public static final weight_input_module:I = 0x7f080c5c

.field public static final weight_input_screen_info_text_top:I = 0x7f080640

.field public static final weight_input_screen_weight_text:I = 0x7f08063f

.field public static final weight_progress_bar:I = 0x7f080c66

.field public static final weight_root_center:I = 0x7f0802a2

.field public static final weight_set_goal:I = 0x7f080c69

.field public static final weight_summary_view_goal:I = 0x7f080c74

.field public static final weight_summary_view_goal_left:I = 0x7f080c49

.field public static final weight_summary_view_indicator_body_image:I = 0x7f080c76

.field public static final weight_summary_view_indicator_dials:I = 0x7f080c72

.field public static final weight_summary_view_range:I = 0x7f080c73

.field public static final weight_summary_view_update_time:I = 0x7f080c48

.field public static final weight_summary_view_weight_value:I = 0x7f080c77

.field public static final weight_summary_view_window:I = 0x7f080c75

.field public static final weight_unit:I = 0x7f080c60

.field public static final weight_value:I = 0x7f080c5f

.field public static final weight_view:I = 0x7f080862

.field public static final welcome_text:I = 0x7f080629

.field public static final welcomepage_s_health:I = 0x7f08061a

.field public static final wgt_log_detail_memo:I = 0x7f080c79

.field public static final wgt_set_goal_calorie_recommend_description:I = 0x7f080c7f

.field public static final wgt_set_goal_target_date_btn:I = 0x7f080c5b

.field public static final wgt_set_goal_target_date_layout:I = 0x7f080c59

.field public static final wgt_set_goal_target_date_tv:I = 0x7f080c5a

.field public static final widget_full_layout:I = 0x7f080b48

.field public static final winter:I = 0x7f0802c6

.field public static final workout_bottom_layout:I = 0x7f080792

.field public static final world_image:I = 0x7f080bdf

.field public static final world_image_1:I = 0x7f080be5

.field public static final world_place_number:I = 0x7f080be0

.field public static final world_place_ranking:I = 0x7f080be1

.field public static final world_separator:I = 0x7f080bdb

.field public static final world_slider:I = 0x7f080bdc

.field public static final zero_step:I = 0x7f080b36

.field public static final zero_step_top:I = 0x7f080b35

.field public static final zoom_in_icon:I = 0x7f0804d5


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8355
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
