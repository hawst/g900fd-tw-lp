.class public Lcom/sec/android/app/shealth/cignacoach/CignaBadgeListActivity;
.super Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;
.source "CignaBadgeListActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mBadgeListAdapter:Lcom/sec/android/app/shealth/cignacoach/CignaBadgeListAdapter;

.field private mBadgeListView:Landroid/widget/ListView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/CignaBadgeListActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/CignaBadgeListActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/cignacoach/CignaBadgeListActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/CignaBadgeListActivity;

    .prologue
    .line 35
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaBadgeListActivity;->prepareShareView()V

    return-void
.end method

.method private getBadgeDataList(Landroid/content/Context;)Ljava/util/ArrayList;
    .locals 16
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 124
    invoke-static {}, Lcom/cigna/coach/factory/CoachFactory;->getInstance()Lcom/cigna/coach/factory/CoachFactory;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/cigna/coach/factory/CoachFactory;->getLifeStyle(Landroid/content/Context;)Lcom/cigna/coach/interfaces/ILifeStyle;

    move-result-object v13

    .line 127
    .local v13, "ils":Lcom/cigna/coach/interfaces/ILifeStyle;
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 130
    .local v11, "dataList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;>;"
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getUserId()Ljava/lang/String;

    move-result-object v15

    .line 131
    .local v15, "userId":Ljava/lang/String;
    invoke-interface {v13, v15}, Lcom/cigna/coach/interfaces/ILifeStyle;->getBadgeCount(Ljava/lang/String;)Lcom/cigna/coach/apiobjects/BadgeInfo;

    move-result-object v9

    .line 133
    .local v9, "badgeInfo":Lcom/cigna/coach/apiobjects/BadgeInfo;
    invoke-virtual {v9}, Lcom/cigna/coach/apiobjects/BadgeInfo;->getBadges()Ljava/util/List;

    move-result-object v10

    .line 134
    .local v10, "badgesList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Badge;>;"
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/sec/android/app/shealth/cignacoach/CignaBadgeListActivity;->sortBadgeListByTime(Ljava/util/List;)V

    .line 135
    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    .line 136
    .local v14, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/cigna/coach/apiobjects/Badge;>;"
    :goto_0
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 137
    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/cigna/coach/apiobjects/Badge;

    .line 139
    .local v8, "badge":Lcom/cigna/coach/apiobjects/Badge;
    invoke-virtual {v8}, Lcom/cigna/coach/apiobjects/Badge;->getBadgeEarnedDate()Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    move-object/from16 v0, p0

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/cignacoach/util/DateFormatUtil;->getMonthLen3DateFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v5

    .line 140
    .local v5, "achieveDate":Ljava/lang/String;
    invoke-virtual {v8}, Lcom/cigna/coach/apiobjects/Badge;->getBadgeEarnedDate()Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    move-object/from16 v0, p0

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/cignacoach/util/DateFormatUtil;->getTimeFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v6

    .line 141
    .local v6, "achieveTime":Ljava/lang/String;
    new-instance v1, Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;

    invoke-virtual {v8}, Lcom/cigna/coach/apiobjects/Badge;->getBadgeId()I

    move-result v2

    invoke-virtual {v8}, Lcom/cigna/coach/apiobjects/Badge;->getBadge()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v8}, Lcom/cigna/coach/apiobjects/Badge;->getBadgeDescription()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v8}, Lcom/cigna/coach/apiobjects/Badge;->getBadgeImage()Ljava/lang/String;

    move-result-object v7

    invoke-direct/range {v1 .. v7}, Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v11, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 148
    .end local v5    # "achieveDate":Ljava/lang/String;
    .end local v6    # "achieveTime":Ljava/lang/String;
    .end local v8    # "badge":Lcom/cigna/coach/apiobjects/Badge;
    .end local v9    # "badgeInfo":Lcom/cigna/coach/apiobjects/BadgeInfo;
    .end local v10    # "badgesList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Badge;>;"
    .end local v14    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/cigna/coach/apiobjects/Badge;>;"
    .end local v15    # "userId":Ljava/lang/String;
    :catch_0
    move-exception v12

    .line 149
    .local v12, "e":Ljava/lang/Throwable;
    invoke-virtual {v12}, Ljava/lang/Throwable;->printStackTrace()V

    .line 152
    .end local v12    # "e":Ljava/lang/Throwable;
    :cond_0
    return-object v11
.end method

.method private sortBadgeListByTime(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/Badge;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 166
    .local p1, "badgeList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Badge;>;"
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/CignaBadgeListActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/cignacoach/CignaBadgeListActivity$2;-><init>(Lcom/sec/android/app/shealth/cignacoach/CignaBadgeListActivity;)V

    .line 173
    .local v0, "dateComparator":Ljava/util/Comparator;, "Ljava/util/Comparator<Lcom/cigna/coach/apiobjects/Badge;>;"
    invoke-static {p1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 174
    return-void
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 6

    .prologue
    const v4, 0x7f090033

    .line 64
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/CignaBadgeListActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/cignacoach/CignaBadgeListActivity$1;-><init>(Lcom/sec/android/app/shealth/cignacoach/CignaBadgeListActivity;)V

    .line 79
    .local v0, "actionBarButtonListener":Landroid/view/View$OnClickListener;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaBadgeListActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    const v3, 0x7f090c5d

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 81
    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    const v2, 0x7f0207cf

    invoke-direct {v1, v2, v0}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;-><init>(ILandroid/view/View$OnClickListener;)V

    .line 82
    .local v1, "shareViaButton":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;
    const v2, 0x7f020022

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->setBackgroundResID(I)Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    .line 83
    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->setContentDescription(I)Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    .line 84
    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->setHoverTextResID(I)Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    .line 85
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaBadgeListActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const/4 v4, 0x0

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->create()Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 86
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaBadgeListActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->makeAllActionBarButtonsInvisible()V

    .line 88
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->customizeActionBar()V

    .line 89
    return-void
.end method

.method public getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;
    .locals 1

    .prologue
    .line 101
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 43
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 45
    const v2, 0x7f03002b

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/cignacoach/CignaBadgeListActivity;->setContentView(I)V

    .line 48
    const v2, 0x7f0800ee

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/cignacoach/CignaBadgeListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 49
    .local v0, "emptyView":Landroid/view/View;
    const v2, 0x7f080219

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 50
    .local v1, "noBadgeTextView":Landroid/widget/TextView;
    const v2, 0x7f09032c

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 52
    new-instance v2, Lcom/sec/android/app/shealth/cignacoach/CignaBadgeListAdapter;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/cignacoach/CignaBadgeListActivity;->getBadgeDataList(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Lcom/sec/android/app/shealth/cignacoach/CignaBadgeListAdapter;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/CignaBadgeListActivity;->mBadgeListAdapter:Lcom/sec/android/app/shealth/cignacoach/CignaBadgeListAdapter;

    .line 53
    const v2, 0x7f0800ed

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/cignacoach/CignaBadgeListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ListView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/CignaBadgeListActivity;->mBadgeListView:Landroid/widget/ListView;

    .line 54
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/CignaBadgeListActivity;->mBadgeListView:Landroid/widget/ListView;

    invoke-virtual {v2, v0}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 56
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/CignaBadgeListActivity;->mBadgeListView:Landroid/widget/ListView;

    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/CignaBadgeListActivity;->mBadgeListAdapter:Lcom/sec/android/app/shealth/cignacoach/CignaBadgeListAdapter;

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 57
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/CignaBadgeListActivity;->mBadgeListView:Landroid/widget/ListView;

    invoke-virtual {v2, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 59
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5
    .param p2, "arg1"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 107
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/CignaBadgeListActivity;->mBadgeListAdapter:Lcom/sec/android/app/shealth/cignacoach/CignaBadgeListAdapter;

    if-nez v3, :cond_1

    .line 120
    :cond_0
    :goto_0
    return-void

    .line 108
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/CignaBadgeListActivity;->mBadgeListAdapter:Lcom/sec/android/app/shealth/cignacoach/CignaBadgeListAdapter;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/cignacoach/CignaBadgeListAdapter;->getCount()I

    move-result v3

    if-le v3, p3, :cond_0

    .line 109
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/CignaBadgeListActivity;->mBadgeListAdapter:Lcom/sec/android/app/shealth/cignacoach/CignaBadgeListAdapter;

    invoke-virtual {v3, p3}, Lcom/sec/android/app/shealth/cignacoach/CignaBadgeListAdapter;->getItem(I)Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;

    move-result-object v2

    .line 110
    .local v2, "selectedItem":Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;
    if-eqz v2, :cond_0

    .line 112
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 113
    .local v0, "badgeId":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;->getBadgeId()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 115
    new-instance v1, Landroid/content/Intent;

    const-class v3, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;

    invoke-direct {v1, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 116
    .local v1, "intent":Landroid/content/Intent;
    const/high16 v3, 0x24000000

    invoke-virtual {v1, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 117
    const-string v3, "EXTRA_NAME_BADGE_VIEW_MODE"

    const/16 v4, 0xd05

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 118
    const-string v3, "EXTRA_NAME_BADGE_IDS"

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 119
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/CignaBadgeListActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 94
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onResume()V

    .line 95
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaBadgeListActivity;->mBadgeListAdapter:Lcom/sec/android/app/shealth/cignacoach/CignaBadgeListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/CignaBadgeListAdapter;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 96
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaBadgeListActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->makeAllActionBarButtonsVisible()V

    .line 98
    :cond_0
    return-void
.end method
