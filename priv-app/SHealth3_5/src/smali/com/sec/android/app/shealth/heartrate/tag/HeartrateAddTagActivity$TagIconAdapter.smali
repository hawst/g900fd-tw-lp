.class public Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$TagIconAdapter;
.super Landroid/widget/BaseAdapter;
.source "HeartrateAddTagActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "TagIconAdapter"
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mTagIconIds:[Ljava/lang/Integer;

.field final synthetic this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;Landroid/content/Context;)V
    .locals 3
    .param p2, "c"    # Landroid/content/Context;

    .prologue
    .line 396
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$TagIconAdapter;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 358
    const/16 v0, 0x21

    new-array v0, v0, [Ljava/lang/Integer;

    const/4 v1, 0x0

    const/16 v2, 0x55f0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const/16 v2, 0x571d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const/16 v2, 0x5657

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const/16 v2, 0x5655

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const/16 v2, 0x56b9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const/16 v2, 0x56ba

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const/16 v2, 0x56bb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const/16 v2, 0x56bc

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const/16 v2, 0x56bd

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const/16 v2, 0x56be

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const/16 v2, 0x571e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const/16 v2, 0x571f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const/16 v2, 0x5720

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const/16 v2, 0x5721

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const/16 v2, 0x5722

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const/16 v2, 0x5728

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const/16 v2, 0x5725

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const/16 v2, 0x5726

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const/16 v2, 0x5729

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const/16 v2, 0x5727

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const/16 v2, 0x5781

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const/16 v2, 0x5782

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const/16 v2, 0x5783

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const/16 v2, 0x5784

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const/16 v2, 0x5785

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const/16 v2, 0x5786

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const/16 v2, 0x5787

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const/16 v2, 0x5788

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const/16 v2, 0x5789

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const/16 v2, 0x578a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const/16 v2, 0x57e5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const/16 v2, 0x57e6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const/16 v2, 0x57e7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$TagIconAdapter;->mTagIconIds:[Ljava/lang/Integer;

    .line 397
    iput-object p2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$TagIconAdapter;->mContext:Landroid/content/Context;

    .line 398
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 401
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$TagIconAdapter;->mTagIconIds:[Ljava/lang/Integer;

    array-length v0, v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 405
    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 409
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$TagIconAdapter;->mTagIconIds:[Ljava/lang/Integer;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 415
    if-nez p2, :cond_0

    .line 416
    new-instance v1, Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$TagIconAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 421
    .local v1, "imageView":Landroid/widget/ImageView;
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$TagIconAdapter;->mTagIconIds:[Ljava/lang/Integer;

    aget-object v2, v2, p1

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils;->getIconResourceId(I)I

    move-result v0

    .line 422
    .local v0, "iconResourceId":I
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 423
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$TagIconAdapter;->mTagIconIds:[Ljava/lang/Integer;

    aget-object v2, v2, p1

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 424
    if-nez p1, :cond_1

    .line 425
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$TagIconAdapter;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;

    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$TagIconAdapter;->mTagIconIds:[Ljava/lang/Integer;

    aget-object v4, v4, p1

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-static {v4}, Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils;->getIconNameId(I)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$TagIconAdapter;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;

    const v4, 0x7f091109

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 429
    :goto_1
    return-object v1

    .end local v0    # "iconResourceId":I
    .end local v1    # "imageView":Landroid/widget/ImageView;
    :cond_0
    move-object v1, p2

    .line 418
    check-cast v1, Landroid/widget/ImageView;

    .restart local v1    # "imageView":Landroid/widget/ImageView;
    goto :goto_0

    .line 427
    .restart local v0    # "iconResourceId":I
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$TagIconAdapter;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;

    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$TagIconAdapter;->mTagIconIds:[Ljava/lang/Integer;

    aget-object v4, v4, p1

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-static {v4}, Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils;->getIconNameId(I)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$TagIconAdapter;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;

    const v4, 0x7f09110a

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method
