.class Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1$1;
.super Ljava/lang/Object;
.source "WeightPreloadSummaryFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/weight/service/WeightService$WeightDataReceivedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1;->onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1;)V
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1$1;->this$1:Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceived()V
    .locals 2

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1$1;->this$1:Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1;

    iget-object v0, v0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1;->this$0:Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 119
    # getter for: Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "getActivity() is null"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    :goto_0
    return-void

    .line 122
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1$1;->this$1:Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1;

    iget-object v0, v0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1;->this$0:Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1$1$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1$1$1;-><init>(Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1$1;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public showErrorPopUp()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 143
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1$1;->this$1:Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1;

    iget-object v2, v2, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1;->this$0:Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v0, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 144
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelable(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 145
    const v2, 0x7f090bae

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 146
    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1$1;->this$1:Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1;

    iget-object v2, v2, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1;->this$0:Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0900d9

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 147
    const v2, 0x7f090047

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 149
    new-instance v2, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1$1$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1$1$2;-><init>(Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1$1;)V

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 160
    new-instance v2, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1$1$3;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1$1$3;-><init>(Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1$1;)V

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnDismissListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 176
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    .line 177
    .local v1, "errorDialog":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1$1;->this$1:Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1;

    iget-object v2, v2, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1;->this$0:Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 178
    return-void
.end method

.method public updateStateView(I)V
    .locals 1
    .param p1, "state"    # I

    .prologue
    .line 137
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1$1;->this$1:Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1;

    iget-object v0, v0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1;->this$0:Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mDeviceFinder:Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;
    invoke-static {v0}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->access$400(Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;)Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 138
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1$1;->this$1:Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1;

    iget-object v0, v0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1;->this$0:Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mDeviceFinder:Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;
    invoke-static {v0}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->access$400(Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;)Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;->updateDeviceConnectionStatus()V

    .line 139
    :cond_0
    return-void
.end method
