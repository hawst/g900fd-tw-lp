.class Lcom/sec/android/app/shealth/settings/PinCodeActivity$SipAndRecentKeyReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PinCodeActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/settings/PinCodeActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SipAndRecentKeyReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/PinCodeActivity;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/settings/PinCodeActivity;)V
    .locals 0

    .prologue
    .line 194
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity$SipAndRecentKeyReceiver;->this$0:Lcom/sec/android/app/shealth/settings/PinCodeActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/settings/PinCodeActivity;Lcom/sec/android/app/shealth/settings/PinCodeActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/settings/PinCodeActivity;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/settings/PinCodeActivity$1;

    .prologue
    .line 194
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/settings/PinCodeActivity$SipAndRecentKeyReceiver;-><init>(Lcom/sec/android/app/shealth/settings/PinCodeActivity;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "arg0"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x1

    .line 199
    if-nez p2, :cond_1

    .line 217
    :cond_0
    :goto_0
    return-void

    .line 203
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ResponseAxT9Info"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity$SipAndRecentKeyReceiver;->this$0:Lcom/sec/android/app/shealth/settings/PinCodeActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mIsRecentKeyPressed:Z
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->access$200(Lcom/sec/android/app/shealth/settings/PinCodeActivity;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 205
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity$SipAndRecentKeyReceiver;->this$0:Lcom/sec/android/app/shealth/settings/PinCodeActivity;

    const-string v2, "AxT9IME.isVisibleWindow"

    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    # setter for: Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mIsKeyBoardShown:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->access$302(Lcom/sec/android/app/shealth/settings/PinCodeActivity;Z)Z

    goto :goto_0

    .line 208
    :cond_2
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 210
    const-string/jumbo v1, "reason"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 211
    .local v0, "reason":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string/jumbo v1, "recentapps"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 214
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity$SipAndRecentKeyReceiver;->this$0:Lcom/sec/android/app/shealth/settings/PinCodeActivity;

    # setter for: Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mIsRecentKeyPressed:Z
    invoke-static {v1, v3}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->access$202(Lcom/sec/android/app/shealth/settings/PinCodeActivity;Z)Z

    goto :goto_0
.end method
