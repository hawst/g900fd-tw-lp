.class public Lcom/sec/android/app/shealth/cignacoach/CurrentGoalDataManager;
.super Ljava/lang/Object;
.source "CurrentGoalDataManager.java"


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static mInstance:Lcom/sec/android/app/shealth/cignacoach/CurrentGoalDataManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalDataManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalDataManager;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/shealth/cignacoach/CurrentGoalDataManager;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalDataManager;->mInstance:Lcom/sec/android/app/shealth/cignacoach/CurrentGoalDataManager;

    if-nez v0, :cond_0

    .line 37
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalDataManager;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalDataManager;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalDataManager;->mInstance:Lcom/sec/android/app/shealth/cignacoach/CurrentGoalDataManager;

    .line 39
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalDataManager;->mInstance:Lcom/sec/android/app/shealth/cignacoach/CurrentGoalDataManager;

    return-object v0
.end method


# virtual methods
.method public getCoachMessage(Lcom/cigna/coach/apiobjects/CoachResponse;)Ljava/lang/String;
    .locals 5
    .param p1, "cr"    # Lcom/cigna/coach/apiobjects/CoachResponse;

    .prologue
    .line 364
    invoke-virtual {p1}, Lcom/cigna/coach/apiobjects/CoachResponse;->getCoachMsgList()Ljava/util/List;

    move-result-object v1

    .line 366
    .local v1, "cmList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 367
    .local v2, "icm":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 368
    sget-object v3, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalDataManager;->TAG:Ljava/lang/String;

    const-string v4, "Coach Message Type - None Defined"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 382
    :cond_0
    const/4 v3, 0x0

    :goto_0
    return-object v3

    .line 370
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 371
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/apiobjects/CoachMessage;

    .line 378
    .local v0, "cm":Lcom/cigna/coach/apiobjects/CoachMessage;
    invoke-virtual {v0}, Lcom/cigna/coach/apiobjects/CoachMessage;->getMessageDescription()Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method public getCompleteGoalMissionStatus()Lcom/cigna/coach/apiobjects/GoalsMissionsHistory;
    .locals 7

    .prologue
    .line 68
    :try_start_0
    invoke-static {}, Lcom/cigna/coach/factory/CoachFactory;->getInstance()Lcom/cigna/coach/factory/CoachFactory;

    move-result-object v0

    .line 69
    .local v0, "cf":Lcom/cigna/coach/factory/CoachFactory;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/cigna/coach/factory/CoachFactory;->getGoalsMissions(Landroid/content/Context;)Lcom/cigna/coach/interfaces/IGoalsMissions;

    move-result-object v4

    .line 70
    .local v4, "lgm":Lcom/cigna/coach/interfaces/IGoalsMissions;
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getUserId()Ljava/lang/String;

    move-result-object v5

    .line 72
    .local v5, "userId":Ljava/lang/String;
    invoke-interface {v4, v5}, Lcom/cigna/coach/interfaces/IGoalsMissions;->getUserCompletedGoalMissions(Ljava/lang/String;)Lcom/cigna/coach/apiobjects/CoachResponse;

    move-result-object v1

    .line 74
    .local v1, "cr":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/GoalsMissionsHistory;>;"
    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/CoachResponse;->getObject()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/cigna/coach/apiobjects/GoalsMissionsHistory;
    :try_end_0
    .catch Lcom/cigna/coach/exceptions/CoachException; {:try_start_0 .. :try_end_0} :catch_0

    .line 76
    .local v3, "gmh":Lcom/cigna/coach/apiobjects/GoalsMissionsHistory;
    if-eqz v3, :cond_0

    .line 84
    .end local v0    # "cf":Lcom/cigna/coach/factory/CoachFactory;
    .end local v1    # "cr":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/GoalsMissionsHistory;>;"
    .end local v3    # "gmh":Lcom/cigna/coach/apiobjects/GoalsMissionsHistory;
    .end local v4    # "lgm":Lcom/cigna/coach/interfaces/IGoalsMissions;
    .end local v5    # "userId":Ljava/lang/String;
    :goto_0
    return-object v3

    .line 79
    :catch_0
    move-exception v2

    .line 81
    .local v2, "e":Lcom/cigna/coach/exceptions/CoachException;
    invoke-virtual {v2}, Lcom/cigna/coach/exceptions/CoachException;->printStackTrace()V

    .line 84
    .end local v2    # "e":Lcom/cigna/coach/exceptions/CoachException;
    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public getCurrentGoalCount()I
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 45
    :try_start_0
    invoke-static {}, Lcom/cigna/coach/factory/CoachFactory;->getInstance()Lcom/cigna/coach/factory/CoachFactory;

    move-result-object v0

    .line 46
    .local v0, "cf":Lcom/cigna/coach/factory/CoachFactory;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v0, v7}, Lcom/cigna/coach/factory/CoachFactory;->getGoalsMissions(Landroid/content/Context;)Lcom/cigna/coach/interfaces/IGoalsMissions;

    move-result-object v4

    .line 47
    .local v4, "lgm":Lcom/cigna/coach/interfaces/IGoalsMissions;
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getUserId()Ljava/lang/String;

    move-result-object v5

    .line 49
    .local v5, "userId":Ljava/lang/String;
    const/4 v7, 0x0

    invoke-interface {v4, v5, v7}, Lcom/cigna/coach/interfaces/IGoalsMissions;->getUserSelectedGoalMissions(Ljava/lang/String;Z)Lcom/cigna/coach/apiobjects/CoachResponse;

    move-result-object v1

    .line 51
    .local v1, "cr":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;>;"
    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/CoachResponse;->getObject()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    .line 53
    .local v3, "gimiList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;"
    if-nez v3, :cond_0

    .line 63
    .end local v0    # "cf":Lcom/cigna/coach/factory/CoachFactory;
    .end local v1    # "cr":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;>;"
    .end local v3    # "gimiList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;"
    .end local v4    # "lgm":Lcom/cigna/coach/interfaces/IGoalsMissions;
    .end local v5    # "userId":Ljava/lang/String;
    :goto_0
    return v6

    .line 56
    .restart local v0    # "cf":Lcom/cigna/coach/factory/CoachFactory;
    .restart local v1    # "cr":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;>;"
    .restart local v3    # "gimiList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;"
    .restart local v4    # "lgm":Lcom/cigna/coach/interfaces/IGoalsMissions;
    .restart local v5    # "userId":Ljava/lang/String;
    :cond_0
    invoke-interface {v3}, Ljava/util/List;->size()I
    :try_end_0
    .catch Lcom/cigna/coach/exceptions/CoachException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v6

    goto :goto_0

    .line 58
    .end local v0    # "cf":Lcom/cigna/coach/factory/CoachFactory;
    .end local v1    # "cr":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;>;"
    .end local v3    # "gimiList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;"
    .end local v4    # "lgm":Lcom/cigna/coach/interfaces/IGoalsMissions;
    .end local v5    # "userId":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 60
    .local v2, "e":Lcom/cigna/coach/exceptions/CoachException;
    invoke-virtual {v2}, Lcom/cigna/coach/exceptions/CoachException;->printStackTrace()V

    goto :goto_0
.end method

.method public getCurrentGoalData(Ljava/util/ArrayList;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 23
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;",
            ">;>;)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 89
    .local p1, "goalDataList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;>;"
    .local p2, "missionDataList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/util/ArrayList<Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;>;>;"
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 92
    .local v15, "goalProgressMsgs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :try_start_0
    invoke-static {}, Lcom/cigna/coach/factory/CoachFactory;->getInstance()Lcom/cigna/coach/factory/CoachFactory;

    move-result-object v8

    .line 93
    .local v8, "cf":Lcom/cigna/coach/factory/CoachFactory;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v8, v3}, Lcom/cigna/coach/factory/CoachFactory;->getGoalsMissions(Landroid/content/Context;)Lcom/cigna/coach/interfaces/IGoalsMissions;

    move-result-object v18

    .line 94
    .local v18, "lgm":Lcom/cigna/coach/interfaces/IGoalsMissions;
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getUserId()Ljava/lang/String;

    move-result-object v22

    .line 96
    .local v22, "userId":Ljava/lang/String;
    const/4 v3, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, v22

    invoke-interface {v0, v1, v3}, Lcom/cigna/coach/interfaces/IGoalsMissions;->getUserSelectedGoalMissions(Ljava/lang/String;Z)Lcom/cigna/coach/apiobjects/CoachResponse;

    move-result-object v9

    .line 98
    .local v9, "cr":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;>;"
    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalDataManager;->getCoachMessage(Lcom/cigna/coach/apiobjects/CoachResponse;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v15, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 100
    invoke-virtual {v9}, Lcom/cigna/coach/apiobjects/CoachResponse;->getObject()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/util/List;

    .line 102
    .local v12, "gimiList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;"
    invoke-interface {v12}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v16

    .line 104
    .local v16, "igimi":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;"
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 106
    sget-object v3, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalDataManager;->TAG:Ljava/lang/String;

    const-string v4, "Goal/Mission List - None Defined"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    .end local v8    # "cf":Lcom/cigna/coach/factory/CoachFactory;
    .end local v9    # "cr":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;>;"
    .end local v12    # "gimiList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;"
    .end local v16    # "igimi":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;"
    .end local v18    # "lgm":Lcom/cigna/coach/interfaces/IGoalsMissions;
    .end local v22    # "userId":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v15

    .line 110
    .restart local v8    # "cf":Lcom/cigna/coach/factory/CoachFactory;
    .restart local v9    # "cr":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;>;"
    .restart local v12    # "gimiList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;"
    .restart local v16    # "igimi":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;"
    .restart local v18    # "lgm":Lcom/cigna/coach/interfaces/IGoalsMissions;
    .restart local v22    # "userId":Ljava/lang/String;
    :cond_1
    :goto_1
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 112
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;

    .line 114
    .local v11, "gimi":Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;
    invoke-virtual {v11}, Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;->getCoachMessage()Lcom/cigna/coach/apiobjects/CoachMessage;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 115
    invoke-virtual {v11}, Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;->getCoachMessage()Lcom/cigna/coach/apiobjects/CoachMessage;

    move-result-object v3

    invoke-virtual {v3}, Lcom/cigna/coach/apiobjects/CoachMessage;->getMessageDescription()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_6

    .line 116
    invoke-virtual {v11}, Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;->getCoachMessage()Lcom/cigna/coach/apiobjects/CoachMessage;

    move-result-object v3

    invoke-virtual {v3}, Lcom/cigna/coach/apiobjects/CoachMessage;->getMessageDescription()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v15, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 124
    :cond_2
    :goto_2
    const/16 v20, 0x0

    .line 125
    .local v20, "missionCompleteCount":I
    const/16 v19, 0x0

    .line 126
    .local v19, "minFrequecy":I
    instance-of v3, v11, Lcom/cigna/coach/dataobjects/UserGoalData;

    if-eqz v3, :cond_3

    .line 127
    move-object v0, v11

    check-cast v0, Lcom/cigna/coach/dataobjects/UserGoalData;

    move-object v3, v0

    invoke-virtual {v3}, Lcom/cigna/coach/dataobjects/UserGoalData;->getNumMissionsCompleted()I

    move-result v20

    .line 128
    move-object v0, v11

    check-cast v0, Lcom/cigna/coach/dataobjects/UserGoalData;

    move-object v3, v0

    invoke-virtual {v3}, Lcom/cigna/coach/dataobjects/UserGoalData;->getMinMissionFrequencyNumber()I

    move-result v19

    .line 132
    :cond_3
    if-eqz p1, :cond_4

    .line 133
    new-instance v2, Lcom/sec/android/app/shealth/cignacoach/data/GoalData;

    invoke-virtual {v11}, Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;->getGoalId()I

    move-result v3

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v11}, Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;->getCategoryType()Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->getCurrentGoalIconIdByCategory(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v11}, Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;->getGoal()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v11}, Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;->getGoalFrequency()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v11}, Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;->getCategoryType()Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-result-object v7

    invoke-direct/range {v2 .. v7}, Lcom/sec/android/app/shealth/cignacoach/data/GoalData;-><init>(ILandroid/graphics/drawable/Drawable;Ljava/lang/String;Ljava/lang/String;Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)V

    .line 136
    .local v2, "goalData":Lcom/sec/android/app/shealth/cignacoach/data/GoalData;
    move/from16 v0, v20

    invoke-virtual {v2, v0}, Lcom/sec/android/app/shealth/cignacoach/data/GoalData;->setMissionCompleteCount(I)V

    .line 137
    move/from16 v0, v19

    invoke-virtual {v2, v0}, Lcom/sec/android/app/shealth/cignacoach/data/GoalData;->setMinFrequency(I)V

    .line 138
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 141
    .end local v2    # "goalData":Lcom/sec/android/app/shealth/cignacoach/data/GoalData;
    :cond_4
    invoke-virtual {v11}, Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;->getGoalMissionDetailInfoList()Ljava/util/List;

    move-result-object v14

    .line 143
    .local v14, "gmdiList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;>;"
    invoke-interface {v14}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v17

    .line 145
    .local v17, "igmdi":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;>;"
    new-instance v21, Ljava/util/ArrayList;

    invoke-direct/range {v21 .. v21}, Ljava/util/ArrayList;-><init>()V

    .line 147
    .local v21, "missionData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;>;"
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_7

    .line 149
    sget-object v3, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalDataManager;->TAG:Ljava/lang/String;

    const-string v4, "Mission List - None Defined"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 165
    :cond_5
    if-eqz p2, :cond_1

    .line 166
    move-object/from16 v0, p2

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/cigna/coach/exceptions/CoachException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    .line 170
    .end local v8    # "cf":Lcom/cigna/coach/factory/CoachFactory;
    .end local v9    # "cr":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;>;"
    .end local v11    # "gimi":Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;
    .end local v12    # "gimiList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;"
    .end local v14    # "gmdiList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;>;"
    .end local v16    # "igimi":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;"
    .end local v17    # "igmdi":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;>;"
    .end local v18    # "lgm":Lcom/cigna/coach/interfaces/IGoalsMissions;
    .end local v19    # "minFrequecy":I
    .end local v20    # "missionCompleteCount":I
    .end local v21    # "missionData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;>;"
    .end local v22    # "userId":Ljava/lang/String;
    :catch_0
    move-exception v10

    .line 172
    .local v10, "e":Lcom/cigna/coach/exceptions/CoachException;
    invoke-virtual {v10}, Lcom/cigna/coach/exceptions/CoachException;->printStackTrace()V

    goto/16 :goto_0

    .line 118
    .end local v10    # "e":Lcom/cigna/coach/exceptions/CoachException;
    .restart local v8    # "cf":Lcom/cigna/coach/factory/CoachFactory;
    .restart local v9    # "cr":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;>;"
    .restart local v11    # "gimi":Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;
    .restart local v12    # "gimiList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;"
    .restart local v16    # "igimi":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;"
    .restart local v18    # "lgm":Lcom/cigna/coach/interfaces/IGoalsMissions;
    .restart local v22    # "userId":Ljava/lang/String;
    :cond_6
    :try_start_1
    const-string v3, ""

    invoke-virtual {v15, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 153
    .restart local v14    # "gmdiList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;>;"
    .restart local v17    # "igmdi":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;>;"
    .restart local v19    # "minFrequecy":I
    .restart local v20    # "missionCompleteCount":I
    .restart local v21    # "missionData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;>;"
    :cond_7
    if-eqz p2, :cond_5

    .line 155
    :goto_3
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 157
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;

    .line 159
    .local v13, "gmdi":Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;
    invoke-static {v13}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->valueOf(Lcom/cigna/coach/apiobjects/GoalMissionInfo;)Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    move-result-object v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lcom/cigna/coach/exceptions/CoachException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3
.end method

.method public getCurrentGoalMissionCount()[I
    .locals 13

    .prologue
    .line 390
    const/4 v10, 0x2

    new-array v6, v10, [I

    .line 393
    .local v6, "goalMissionCount":[I
    :try_start_0
    invoke-static {}, Lcom/cigna/coach/factory/CoachFactory;->getInstance()Lcom/cigna/coach/factory/CoachFactory;

    move-result-object v0

    .line 394
    .local v0, "cf":Lcom/cigna/coach/factory/CoachFactory;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v10

    invoke-virtual {v0, v10}, Lcom/cigna/coach/factory/CoachFactory;->getGoalsMissions(Landroid/content/Context;)Lcom/cigna/coach/interfaces/IGoalsMissions;

    move-result-object v8

    .line 395
    .local v8, "lgm":Lcom/cigna/coach/interfaces/IGoalsMissions;
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getUserId()Ljava/lang/String;

    move-result-object v9

    .line 397
    .local v9, "userId":Ljava/lang/String;
    const/4 v10, 0x0

    invoke-interface {v8, v9, v10}, Lcom/cigna/coach/interfaces/IGoalsMissions;->getUserSelectedGoalMissions(Ljava/lang/String;Z)Lcom/cigna/coach/apiobjects/CoachResponse;

    move-result-object v1

    .line 399
    .local v1, "cr":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;>;"
    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/CoachResponse;->getObject()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    .line 401
    .local v4, "gimiList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;"
    if-eqz v4, :cond_0

    .line 402
    const/4 v10, 0x0

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v11

    aput v11, v6, v10

    .line 404
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .line 406
    .local v7, "igimi":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;"
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-nez v10, :cond_1

    .line 408
    sget-object v10, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalDataManager;->TAG:Ljava/lang/String;

    const-string v11, "Goal/Mission List - None Defined"

    invoke-static {v10, v11}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 431
    .end local v0    # "cf":Lcom/cigna/coach/factory/CoachFactory;
    .end local v1    # "cr":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;>;"
    .end local v4    # "gimiList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;"
    .end local v7    # "igimi":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;"
    .end local v8    # "lgm":Lcom/cigna/coach/interfaces/IGoalsMissions;
    .end local v9    # "userId":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v6

    .line 412
    .restart local v0    # "cf":Lcom/cigna/coach/factory/CoachFactory;
    .restart local v1    # "cr":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;>;"
    .restart local v4    # "gimiList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;"
    .restart local v7    # "igimi":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;"
    .restart local v8    # "lgm":Lcom/cigna/coach/interfaces/IGoalsMissions;
    .restart local v9    # "userId":Ljava/lang/String;
    :cond_1
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    .line 414
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;

    .line 416
    .local v3, "gimi":Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;
    invoke-virtual {v3}, Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;->getGoalMissionDetailInfoList()Ljava/util/List;

    move-result-object v5

    .line 418
    .local v5, "gmdiList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;>;"
    if-eqz v5, :cond_1

    .line 419
    const/4 v10, 0x1

    aget v11, v6, v10

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v12

    add-int/2addr v11, v12

    aput v11, v6, v10
    :try_end_0
    .catch Lcom/cigna/coach/exceptions/CoachException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 424
    .end local v0    # "cf":Lcom/cigna/coach/factory/CoachFactory;
    .end local v1    # "cr":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;>;"
    .end local v3    # "gimi":Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;
    .end local v4    # "gimiList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;"
    .end local v5    # "gmdiList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;>;"
    .end local v7    # "igimi":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;"
    .end local v8    # "lgm":Lcom/cigna/coach/interfaces/IGoalsMissions;
    .end local v9    # "userId":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 425
    .local v2, "e":Lcom/cigna/coach/exceptions/CoachException;
    invoke-virtual {v2}, Lcom/cigna/coach/exceptions/CoachException;->printStackTrace()V

    goto :goto_0
.end method

.method public getCurrentMissionData(Ljava/util/ArrayList;)Ljava/lang/String;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 180
    .local p1, "missionDataList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;>;"
    const/4 v1, 0x0

    .line 183
    .local v1, "coachMessage":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/cigna/coach/factory/CoachFactory;->getInstance()Lcom/cigna/coach/factory/CoachFactory;

    move-result-object v0

    .line 184
    .local v0, "cf":Lcom/cigna/coach/factory/CoachFactory;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v12

    invoke-virtual {v0, v12}, Lcom/cigna/coach/factory/CoachFactory;->getGoalsMissions(Landroid/content/Context;)Lcom/cigna/coach/interfaces/IGoalsMissions;

    move-result-object v10

    .line 185
    .local v10, "lgm":Lcom/cigna/coach/interfaces/IGoalsMissions;
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v12

    invoke-virtual {v12}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getUserId()Ljava/lang/String;

    move-result-object v11

    .line 187
    .local v11, "userId":Ljava/lang/String;
    const/4 v12, 0x0

    invoke-interface {v10, v11, v12}, Lcom/cigna/coach/interfaces/IGoalsMissions;->getUserSelectedGoalMissions(Ljava/lang/String;Z)Lcom/cigna/coach/apiobjects/CoachResponse;

    move-result-object v2

    .line 189
    .local v2, "cr":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;>;"
    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalDataManager;->getCoachMessage(Lcom/cigna/coach/apiobjects/CoachResponse;)Ljava/lang/String;

    move-result-object v1

    .line 191
    invoke-virtual {v2}, Lcom/cigna/coach/apiobjects/CoachResponse;->getObject()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/List;

    .line 193
    .local v5, "gimiList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;"
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .line 195
    .local v8, "igimi":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;"
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-nez v12, :cond_1

    .line 197
    sget-object v12, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalDataManager;->TAG:Ljava/lang/String;

    const-string v13, "Goal/Mission List - None Defined"

    invoke-static {v12, v13}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    .end local v0    # "cf":Lcom/cigna/coach/factory/CoachFactory;
    .end local v2    # "cr":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;>;"
    .end local v5    # "gimiList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;"
    .end local v8    # "igimi":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;"
    .end local v10    # "lgm":Lcom/cigna/coach/interfaces/IGoalsMissions;
    .end local v11    # "userId":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v1

    .line 201
    .restart local v0    # "cf":Lcom/cigna/coach/factory/CoachFactory;
    .restart local v2    # "cr":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;>;"
    .restart local v5    # "gimiList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;"
    .restart local v8    # "igimi":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;"
    .restart local v10    # "lgm":Lcom/cigna/coach/interfaces/IGoalsMissions;
    .restart local v11    # "userId":Ljava/lang/String;
    :cond_1
    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_0

    .line 203
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;

    .line 205
    .local v4, "gimi":Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;
    invoke-virtual {v4}, Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;->getGoalMissionDetailInfoList()Ljava/util/List;

    move-result-object v7

    .line 207
    .local v7, "gmdiList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;>;"
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .line 209
    .local v9, "igmdi":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;>;"
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-nez v12, :cond_2

    .line 211
    sget-object v12, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalDataManager;->TAG:Ljava/lang/String;

    const-string v13, "Mission List - None Defined"

    invoke-static {v12, v13}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lcom/cigna/coach/exceptions/CoachException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 224
    .end local v0    # "cf":Lcom/cigna/coach/factory/CoachFactory;
    .end local v2    # "cr":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;>;"
    .end local v4    # "gimi":Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;
    .end local v5    # "gimiList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;"
    .end local v7    # "gmdiList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;>;"
    .end local v8    # "igimi":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;"
    .end local v9    # "igmdi":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;>;"
    .end local v10    # "lgm":Lcom/cigna/coach/interfaces/IGoalsMissions;
    .end local v11    # "userId":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 226
    .local v3, "e":Lcom/cigna/coach/exceptions/CoachException;
    invoke-virtual {v3}, Lcom/cigna/coach/exceptions/CoachException;->printStackTrace()V

    goto :goto_0

    .line 215
    .end local v3    # "e":Lcom/cigna/coach/exceptions/CoachException;
    .restart local v0    # "cf":Lcom/cigna/coach/factory/CoachFactory;
    .restart local v2    # "cr":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;>;"
    .restart local v4    # "gimi":Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;
    .restart local v5    # "gimiList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;"
    .restart local v7    # "gmdiList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;>;"
    .restart local v8    # "igimi":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;"
    .restart local v9    # "igmdi":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;>;"
    .restart local v10    # "lgm":Lcom/cigna/coach/interfaces/IGoalsMissions;
    .restart local v11    # "userId":Ljava/lang/String;
    :cond_2
    :goto_2
    :try_start_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_1

    .line 217
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;

    .line 219
    .local v6, "gmdi":Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;
    invoke-static {v6}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->valueOf(Lcom/cigna/coach/apiobjects/GoalMissionInfo;)Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    move-result-object v12

    invoke-virtual {p1, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lcom/cigna/coach/exceptions/CoachException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method

.method public getGoalMissionHistory(Ljava/util/ArrayList;Ljava/util/ArrayList;)Lcom/sec/android/app/shealth/cignacoach/data/CompleteGoalMissionInfoData;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;",
            ">;>;)",
            "Lcom/sec/android/app/shealth/cignacoach/data/CompleteGoalMissionInfoData;"
        }
    .end annotation

    .prologue
    .line 316
    .local p1, "goalDataList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;>;"
    .local p2, "missionDataList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/util/ArrayList<Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;>;>;"
    const/4 v3, 0x0

    .line 319
    .local v3, "completeGoalMissionInfoData":Lcom/sec/android/app/shealth/cignacoach/data/CompleteGoalMissionInfoData;
    :try_start_0
    invoke-static {}, Lcom/cigna/coach/factory/CoachFactory;->getInstance()Lcom/cigna/coach/factory/CoachFactory;

    move-result-object v2

    .line 320
    .local v2, "cf":Lcom/cigna/coach/factory/CoachFactory;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Lcom/cigna/coach/factory/CoachFactory;->getGoalsMissions(Landroid/content/Context;)Lcom/cigna/coach/interfaces/IGoalsMissions;

    move-result-object v14

    .line 321
    .local v14, "lgm":Lcom/cigna/coach/interfaces/IGoalsMissions;
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getUserId()Ljava/lang/String;

    move-result-object v16

    .line 323
    .local v16, "userId":Ljava/lang/String;
    move-object/from16 v0, v16

    invoke-interface {v14, v0}, Lcom/cigna/coach/interfaces/IGoalsMissions;->getUserCompletedGoalMissions(Ljava/lang/String;)Lcom/cigna/coach/apiobjects/CoachResponse;

    move-result-object v5

    .line 325
    .local v5, "cr":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/GoalsMissionsHistory;>;"
    invoke-virtual {v5}, Lcom/cigna/coach/apiobjects/CoachResponse;->getObject()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/cigna/coach/apiobjects/GoalsMissionsHistory;

    .line 328
    .local v9, "gmh":Lcom/cigna/coach/apiobjects/GoalsMissionsHistory;
    new-instance v4, Lcom/sec/android/app/shealth/cignacoach/data/CompleteGoalMissionInfoData;

    invoke-direct {v4}, Lcom/sec/android/app/shealth/cignacoach/data/CompleteGoalMissionInfoData;-><init>()V
    :try_end_0
    .catch Lcom/cigna/coach/exceptions/CoachException; {:try_start_0 .. :try_end_0} :catch_1

    .line 329
    .end local v3    # "completeGoalMissionInfoData":Lcom/sec/android/app/shealth/cignacoach/data/CompleteGoalMissionInfoData;
    .local v4, "completeGoalMissionInfoData":Lcom/sec/android/app/shealth/cignacoach/data/CompleteGoalMissionInfoData;
    :try_start_1
    invoke-virtual {v9}, Lcom/cigna/coach/apiobjects/GoalsMissionsHistory;->getCompletedGoalsCount()I

    move-result v17

    move/from16 v0, v17

    invoke-virtual {v4, v0}, Lcom/sec/android/app/shealth/cignacoach/data/CompleteGoalMissionInfoData;->setGoalCompeleteCount(I)V

    .line 330
    invoke-virtual {v9}, Lcom/cigna/coach/apiobjects/GoalsMissionsHistory;->getCompletedMissionsCount()I

    move-result v17

    move/from16 v0, v17

    invoke-virtual {v4, v0}, Lcom/sec/android/app/shealth/cignacoach/data/CompleteGoalMissionInfoData;->setMissionCompleteCount(I)V

    .line 332
    invoke-virtual {v9}, Lcom/cigna/coach/apiobjects/GoalsMissionsHistory;->getGoalInfoMissionsInfos()Ljava/util/List;

    move-result-object v10

    .line 334
    .local v10, "goalInfoMissionInfoList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;"
    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_1

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;

    .line 337
    .local v7, "gimi":Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;
    invoke-static {v7}, Lcom/sec/android/app/shealth/cignacoach/data/GoalData;->valueOf(Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;)Lcom/sec/android/app/shealth/cignacoach/data/GoalData;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 339
    invoke-virtual {v7}, Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;->getGoalMissionDetailInfoList()Ljava/util/List;

    move-result-object v11

    .line 341
    .local v11, "goalMissionDetailInfoList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;>;"
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 343
    .local v15, "missionData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;>;"
    if-eqz v11, :cond_0

    .line 345
    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    .local v13, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_0

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;

    .line 348
    .local v8, "gmdi":Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;
    invoke-static {v8}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->valueOf(Lcom/cigna/coach/apiobjects/GoalMissionInfo;)Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lcom/cigna/coach/exceptions/CoachException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 355
    .end local v7    # "gimi":Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;
    .end local v8    # "gmdi":Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;
    .end local v10    # "goalInfoMissionInfoList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;"
    .end local v11    # "goalMissionDetailInfoList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;>;"
    .end local v13    # "i$":Ljava/util/Iterator;
    .end local v15    # "missionData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;>;"
    :catch_0
    move-exception v6

    move-object v3, v4

    .line 357
    .end local v2    # "cf":Lcom/cigna/coach/factory/CoachFactory;
    .end local v4    # "completeGoalMissionInfoData":Lcom/sec/android/app/shealth/cignacoach/data/CompleteGoalMissionInfoData;
    .end local v5    # "cr":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/GoalsMissionsHistory;>;"
    .end local v9    # "gmh":Lcom/cigna/coach/apiobjects/GoalsMissionsHistory;
    .end local v14    # "lgm":Lcom/cigna/coach/interfaces/IGoalsMissions;
    .end local v16    # "userId":Ljava/lang/String;
    .restart local v3    # "completeGoalMissionInfoData":Lcom/sec/android/app/shealth/cignacoach/data/CompleteGoalMissionInfoData;
    .local v6, "e":Lcom/cigna/coach/exceptions/CoachException;
    :goto_2
    invoke-virtual {v6}, Lcom/cigna/coach/exceptions/CoachException;->printStackTrace()V

    .line 360
    .end local v6    # "e":Lcom/cigna/coach/exceptions/CoachException;
    :goto_3
    return-object v3

    .line 352
    .end local v3    # "completeGoalMissionInfoData":Lcom/sec/android/app/shealth/cignacoach/data/CompleteGoalMissionInfoData;
    .restart local v2    # "cf":Lcom/cigna/coach/factory/CoachFactory;
    .restart local v4    # "completeGoalMissionInfoData":Lcom/sec/android/app/shealth/cignacoach/data/CompleteGoalMissionInfoData;
    .restart local v5    # "cr":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/GoalsMissionsHistory;>;"
    .restart local v7    # "gimi":Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;
    .restart local v9    # "gmh":Lcom/cigna/coach/apiobjects/GoalsMissionsHistory;
    .restart local v10    # "goalInfoMissionInfoList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;"
    .restart local v11    # "goalMissionDetailInfoList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;>;"
    .restart local v14    # "lgm":Lcom/cigna/coach/interfaces/IGoalsMissions;
    .restart local v15    # "missionData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;>;"
    .restart local v16    # "userId":Ljava/lang/String;
    :cond_0
    :try_start_2
    move-object/from16 v0, p2

    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lcom/cigna/coach/exceptions/CoachException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .end local v7    # "gimi":Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;
    .end local v11    # "goalMissionDetailInfoList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;>;"
    .end local v15    # "missionData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;>;"
    :cond_1
    move-object v3, v4

    .line 358
    .end local v4    # "completeGoalMissionInfoData":Lcom/sec/android/app/shealth/cignacoach/data/CompleteGoalMissionInfoData;
    .restart local v3    # "completeGoalMissionInfoData":Lcom/sec/android/app/shealth/cignacoach/data/CompleteGoalMissionInfoData;
    goto :goto_3

    .line 355
    .end local v2    # "cf":Lcom/cigna/coach/factory/CoachFactory;
    .end local v5    # "cr":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/GoalsMissionsHistory;>;"
    .end local v9    # "gmh":Lcom/cigna/coach/apiobjects/GoalsMissionsHistory;
    .end local v10    # "goalInfoMissionInfoList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;"
    .end local v14    # "lgm":Lcom/cigna/coach/interfaces/IGoalsMissions;
    .end local v16    # "userId":Ljava/lang/String;
    :catch_1
    move-exception v6

    goto :goto_2
.end method

.method public getUserGoalDataByGoalID(I)Lcom/sec/android/app/shealth/cignacoach/data/GoalData;
    .locals 10
    .param p1, "goalID"    # I

    .prologue
    .line 234
    const/4 v2, 0x0

    .line 237
    .local v2, "currentGoalData":Lcom/sec/android/app/shealth/cignacoach/data/GoalData;
    :try_start_0
    invoke-static {}, Lcom/cigna/coach/factory/CoachFactory;->getInstance()Lcom/cigna/coach/factory/CoachFactory;

    move-result-object v0

    .line 238
    .local v0, "cf":Lcom/cigna/coach/factory/CoachFactory;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v0, v9}, Lcom/cigna/coach/factory/CoachFactory;->getGoalsMissions(Landroid/content/Context;)Lcom/cigna/coach/interfaces/IGoalsMissions;

    move-result-object v7

    .line 239
    .local v7, "lgm":Lcom/cigna/coach/interfaces/IGoalsMissions;
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getUserId()Ljava/lang/String;

    move-result-object v8

    .line 241
    .local v8, "userId":Ljava/lang/String;
    const/4 v9, 0x0

    invoke-interface {v7, v8, v9}, Lcom/cigna/coach/interfaces/IGoalsMissions;->getUserSelectedGoalMissions(Ljava/lang/String;Z)Lcom/cigna/coach/apiobjects/CoachResponse;

    move-result-object v1

    .line 243
    .local v1, "cr":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;>;"
    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/CoachResponse;->getObject()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/List;

    .line 245
    .local v5, "gimiList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;"
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .line 247
    .local v6, "igimi":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;"
    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 248
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;

    .line 250
    .local v4, "gimi":Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;
    invoke-virtual {v4}, Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;->getGoalId()I

    move-result v9

    if-ne p1, v9, :cond_0

    .line 251
    invoke-static {v4}, Lcom/sec/android/app/shealth/cignacoach/data/GoalData;->valueOf(Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;)Lcom/sec/android/app/shealth/cignacoach/data/GoalData;
    :try_end_0
    .catch Lcom/cigna/coach/exceptions/CoachException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    .line 255
    .end local v0    # "cf":Lcom/cigna/coach/factory/CoachFactory;
    .end local v1    # "cr":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;>;"
    .end local v4    # "gimi":Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;
    .end local v5    # "gimiList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;"
    .end local v6    # "igimi":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;"
    .end local v7    # "lgm":Lcom/cigna/coach/interfaces/IGoalsMissions;
    .end local v8    # "userId":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 256
    .local v3, "e":Lcom/cigna/coach/exceptions/CoachException;
    invoke-virtual {v3}, Lcom/cigna/coach/exceptions/CoachException;->printStackTrace()V

    .line 259
    .end local v3    # "e":Lcom/cigna/coach/exceptions/CoachException;
    :cond_1
    return-object v2
.end method
