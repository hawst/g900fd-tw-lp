.class public Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;
.super Landroid/view/View;
.source "ProfileSecondView.java"


# static fields
.field private static final HEIGHT_DIALOG:Ljava/lang/String; = "height_dialog"

.field private static final WEIGHT_DIALOG:Ljava/lang/String; = "weight_dialog"


# instance fields
.field private dialogInterface:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFocusChangeInterface;

.field private mContext:Landroid/content/Context;

.field private mGenderImage:Landroid/widget/ImageView;

.field private mProfileHeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

.field private mProfileWeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

.field private mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

.field private mView:Landroid/view/View;

.field private mWeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/sdk/health/content/ShealthProfile;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "shealthProfile"    # Lcom/samsung/android/sdk/health/content/ShealthProfile;

    .prologue
    const/4 v1, 0x0

    .line 68
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 56
    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mContext:Landroid/content/Context;

    .line 57
    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mView:Landroid/view/View;

    .line 61
    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mGenderImage:Landroid/widget/ImageView;

    .line 62
    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    .line 259
    new-instance v0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView$2;-><init>(Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->dialogInterface:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFocusChangeInterface;

    .line 69
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mContext:Landroid/content/Context;

    .line 70
    iput-object p2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    .line 71
    const v0, 0x7f0301e7

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mView:Landroid/view/View;

    .line 72
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->initLayout()V

    .line 73
    invoke-virtual {p2}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getGender()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->setGenderImage(I)V

    .line 75
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;)Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mProfileWeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->showRangeDialog(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;)Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mProfileHeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    return-object v0
.end method

.method private checkEditFocus()Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 232
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    .line 233
    .local v0, "v":Landroid/view/View;
    instance-of v1, v0, Landroid/widget/EditText;

    if-eqz v1, :cond_0

    .line 235
    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 256
    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1

    .line 238
    :sswitch_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mProfileWeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->checkValueText()Z

    move-result v1

    if-nez v1, :cond_0

    .line 239
    invoke-virtual {v0}, Landroid/view/View;->clearFocus()V

    move v1, v2

    .line 240
    goto :goto_0

    .line 248
    :sswitch_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mProfileHeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->checkValueText()Z

    move-result v1

    if-nez v1, :cond_0

    .line 249
    invoke-virtual {v0}, Landroid/view/View;->clearFocus()V

    move v1, v2

    .line 250
    goto :goto_0

    .line 235
    :sswitch_data_0
    .sparse-switch
        0x7f08088b -> :sswitch_1
        0x7f08088d -> :sswitch_1
        0x7f0808a8 -> :sswitch_0
    .end sparse-switch
.end method

.method private initLayout()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 117
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    if-nez v1, :cond_0

    .line 118
    new-instance v1, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    .line 120
    :cond_0
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;-><init>(Landroid/content/Context;)V

    .line 122
    .local v0, "unitHelper":Lcom/sec/android/app/shealth/common/utils/UnitHelper;
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mView:Landroid/view/View;

    const v2, 0x7f080860

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mGenderImage:Landroid/widget/ImageView;

    .line 124
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mView:Landroid/view/View;

    const v2, 0x7f080861

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mProfileHeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    .line 125
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mView:Landroid/view/View;

    const v2, 0x7f080862

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mProfileWeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    .line 127
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mProfileHeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->dialogInterface:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFocusChangeInterface;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->setOnProfileDialogListener(Lcom/sec/android/app/shealth/settings/userprofile/ProfileFocusChangeInterface;)V

    .line 128
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mProfileWeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->dialogInterface:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFocusChangeInterface;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->setOnProfileDialogListener(Lcom/sec/android/app/shealth/settings/userprofile/ProfileFocusChangeInterface;)V

    .line 130
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mView:Landroid/view/View;

    const v2, 0x7f08085f

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090818

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09020b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0901fd

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 132
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->getHeightUnit()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->getHeightUnit()Ljava/lang/String;

    move-result-object v1

    const-string v2, "cm"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 133
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mProfileHeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    invoke-virtual {v1, v5}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->setListValue(I)V

    .line 134
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mProfileHeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    const v2, 0x249f1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->setHeightUnit(I)V

    .line 140
    :goto_0
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->getWeightUnit()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->getWeightUnit()Ljava/lang/String;

    move-result-object v1

    const-string v2, "kg"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 141
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mProfileWeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    invoke-virtual {v1, v5}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->setDropDownListValue(I)V

    .line 142
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mProfileWeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    const v2, 0x1fbd1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->setWeightUnit(I)V

    .line 148
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mProfileWeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    const v2, 0x7f0808a8

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mWeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    .line 149
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mWeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    new-instance v2, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView$1;-><init>(Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 166
    return-void

    .line 136
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mProfileHeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    invoke-virtual {v1, v6}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->setListValue(I)V

    .line 137
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mProfileHeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    const v2, 0x249f2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->setHeightUnit(I)V

    goto :goto_0

    .line 144
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mProfileWeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    invoke-virtual {v1, v6}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->setDropDownListValue(I)V

    .line 145
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mProfileWeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    const v2, 0x1fbd2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->setWeightUnit(I)V

    goto :goto_1
.end method

.method private setDataProfileValue()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 168
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getGender()I

    move-result v0

    if-eq v0, v3, :cond_0

    .line 169
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getGender()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->setGenderImage(I)V

    .line 171
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getHeight()F

    move-result v0

    invoke-static {v0, v4}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getHeightUnit()I

    move-result v0

    if-eq v0, v3, :cond_1

    .line 172
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mProfileHeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getHeight()F

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getHeightUnit()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->setHeightProfileData(FI)V

    .line 174
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getWeight()F

    move-result v0

    invoke-static {v0, v4}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getWeightUnit()I

    move-result v0

    if-eq v0, v3, :cond_2

    .line 175
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mProfileWeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getWeight()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->setWeightPreviousValue(F)V

    .line 176
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mProfileWeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getWeight()F

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getWeightUnit()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->setWeightProfileData(FI)V

    .line 178
    :cond_2
    return-void
.end method

.method private showRangeDialog(Landroid/view/View;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const v4, 0x7f0301e3

    .line 267
    if-nez p1, :cond_0

    .line 269
    const-string v2, "ProfileSecondView"

    const-string v3, "View is NULL"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 352
    :goto_0
    return-void

    .line 272
    :cond_0
    move-object v1, p1

    .line 273
    .local v1, "v":Landroid/view/View;
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 274
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090ae2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 275
    const v2, 0x7f090047

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 276
    new-instance v2, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView$3;

    invoke-direct {v2, p0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView$3;-><init>(Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;Landroid/view/View;)V

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 285
    new-instance v2, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView$4;

    invoke-direct {v2, p0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView$4;-><init>(Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;Landroid/view/View;)V

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnDismissListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 292
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 349
    const/4 v0, 0x0

    goto :goto_0

    .line 296
    :sswitch_0
    new-instance v2, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView$5;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView$5;-><init>(Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;)V

    invoke-virtual {v0, v4, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(ILcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 320
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v3

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v4, "height_dialog"

    invoke-virtual {v3, v2, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 323
    :sswitch_1
    new-instance v2, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView$6;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView$6;-><init>(Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;)V

    invoke-virtual {v0, v4, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(ILcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 346
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v3

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string/jumbo v4, "weight_dialog"

    invoke-virtual {v3, v2, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 292
    :sswitch_data_0
    .sparse-switch
        0x7f08088b -> :sswitch_0
        0x7f08088d -> :sswitch_0
        0x7f0808a8 -> :sswitch_1
    .end sparse-switch
.end method


# virtual methods
.method public getView()Landroid/view/View;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mView:Landroid/view/View;

    return-object v0
.end method

.method public hideEditKeyboard()V
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mProfileHeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->hideHeightKeyboard()V

    .line 181
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mProfileWeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->hideWeightKeyboard()V

    .line 182
    return-void
.end method

.method public isChanged()Z
    .locals 2

    .prologue
    .line 90
    const/4 v0, 0x0

    .line 91
    .local v0, "changed":Z
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mProfileHeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->checkChangeValue()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mProfileWeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->checkChangeValue()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mProfileHeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->checkChangeUnit()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mProfileWeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->checkChangeUnit()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 93
    :cond_0
    const/4 v0, 0x1

    .line 95
    :cond_1
    return v0
.end method

.method public saveProfile()Z
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 186
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->checkEditFocus()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 227
    :goto_0
    return v4

    .line 190
    :cond_0
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    if-nez v5, :cond_1

    .line 191
    new-instance v5, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    .line 195
    :cond_1
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mProfileHeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->getListValue()I

    move-result v5

    if-nez v5, :cond_4

    const v1, 0x249f1

    .line 196
    .local v1, "heightUnit":I
    :goto_1
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mProfileHeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->setHeightValue()V

    .line 197
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mProfileHeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->getHeightValue()F

    move-result v6

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setHeight(F)V

    .line 198
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v5, v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setHeightUnit(I)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 205
    :try_start_1
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mProfileWeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->getDropDownListValue()I

    move-result v5

    if-nez v5, :cond_5

    const v3, 0x1fbd1

    .line 206
    .local v3, "weightUnit":I
    :goto_2
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mProfileWeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->setWeightValue()V

    .line 207
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mProfileWeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->getWeight()F

    move-result v6

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setWeight(F)V

    .line 208
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v5, v3}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setWeightUnit(I)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    .line 215
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getTemperatureUnit()I

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getTemperatureUnit()I

    move-result v4

    const/4 v5, -0x1

    if-ne v4, v5, :cond_3

    .line 217
    :cond_2
    new-instance v2, Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v2, v4}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;-><init>(Landroid/content/Context;)V

    .line 218
    .local v2, "unitSettingHelper":Lcom/sec/android/app/shealth/common/utils/UnitHelper;
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->getTemperatureUnit()Ljava/lang/String;

    move-result-object v4

    const-string v5, "F"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 219
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    const v5, 0x27102

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setTemperatureUnit(I)V

    .line 225
    .end local v2    # "unitSettingHelper":Lcom/sec/android/app/shealth/common/utils/UnitHelper;
    :cond_3
    :goto_3
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mProfileHeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->getHeightUnit()I

    move-result v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mProfileWeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->getWeightUnit()I

    move-result v5

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->saveUnitforUnitHelper(II)V

    .line 227
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 195
    .end local v1    # "heightUnit":I
    .end local v3    # "weightUnit":I
    :cond_4
    const v1, 0x249f2

    goto :goto_1

    .line 200
    :catch_0
    move-exception v0

    .line 201
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v5, "ProfileSecondView"

    const-string v6, "Height range Error"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 205
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    .restart local v1    # "heightUnit":I
    :cond_5
    const v3, 0x1fbd2

    goto :goto_2

    .line 209
    :catch_1
    move-exception v0

    .line 210
    .restart local v0    # "e":Ljava/lang/IllegalArgumentException;
    const-string v5, "ProfileSecondView"

    const-string v6, "Weight range Error"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 221
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    .restart local v2    # "unitSettingHelper":Lcom/sec/android/app/shealth/common/utils/UnitHelper;
    .restart local v3    # "weightUnit":I
    :cond_6
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    const v5, 0x27101

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setTemperatureUnit(I)V

    goto :goto_3
.end method

.method public setDefaultValue(I)V
    .locals 5
    .param p1, "gender"    # I

    .prologue
    const/4 v4, -0x1

    const/high16 v3, 0x42820000    # 65.0f

    const/high16 v2, 0x425c0000    # 55.0f

    const/4 v1, 0x0

    .line 99
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getHeight()F

    move-result v0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getHeightUnit()I

    move-result v0

    if-eq v0, v4, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getWeight()F

    move-result v0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getWeightUnit()I

    move-result v0

    if-eq v0, v4, :cond_0

    .line 102
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->setDataProfileValue()V

    .line 114
    :goto_0
    return-void

    .line 104
    :cond_0
    const v0, 0x2e635

    if-ne p1, v0, :cond_1

    .line 105
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mProfileWeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->setWeightPreviousValue(F)V

    .line 106
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mProfileHeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    const/high16 v1, 0x432a0000    # 170.0f

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->setValue(F)V

    .line 107
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mProfileWeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->setValue(F)V

    goto :goto_0

    .line 109
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mProfileWeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->setWeightPreviousValue(F)V

    .line 110
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mProfileHeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    const/high16 v1, 0x43200000    # 160.0f

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->setValue(F)V

    .line 111
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mProfileWeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->setValue(F)V

    goto :goto_0
.end method

.method public setGenderImage(I)V
    .locals 2
    .param p1, "gender"    # I

    .prologue
    .line 82
    const v0, 0x2e635

    if-ne p1, v0, :cond_0

    .line 83
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mGenderImage:Landroid/widget/ImageView;

    const v1, 0x7f0205eb

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 87
    :goto_0
    return-void

    .line 85
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mGenderImage:Landroid/widget/ImageView;

    const v1, 0x7f0205db

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0
.end method
