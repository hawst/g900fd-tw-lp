.class public Lcom/sec/android/app/shealth/uv/custom/UvGIFView;
.super Landroid/view/View;
.source "UvGIFView.java"


# static fields
.field private static final DEFAULT_MOVIEW_DURATION:I = 0x3e8


# instance fields
.field private final MOVIE_TAG:Ljava/lang/String;

.field private mCurrentAnimationTime:I

.field private mLeft:F

.field private mMeasuredMovieHeight:I

.field private mMeasuredMovieWidth:I

.field private mMovie:Landroid/graphics/Movie;

.field private mMovieResourceId:I

.field private mMovieStart:J

.field private volatile mPaused:Z

.field private volatile mRestart:Z

.field private mScale:F

.field private mTop:F

.field private mVisible:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 54
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 55
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 58
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 59
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v2, 0x0

    .line 62
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 28
    iput v2, p0, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->mCurrentAnimationTime:I

    .line 47
    const-string/jumbo v1, "movie"

    iput-object v1, p0, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->MOVIE_TAG:Ljava/lang/String;

    .line 49
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->mPaused:Z

    .line 50
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->mRestart:Z

    .line 51
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->mVisible:Z

    .line 66
    :try_start_0
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->setViewAttributes(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 73
    :goto_0
    return-void

    .line 68
    :catch_0
    move-exception v0

    .line 71
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method private drawMovieFrame(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 299
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->mMovie:Landroid/graphics/Movie;

    iget v1, p0, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->mCurrentAnimationTime:I

    invoke-virtual {v0, v1}, Landroid/graphics/Movie;->setTime(I)Z

    .line 301
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->save(I)I

    .line 302
    iget v0, p0, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->mScale:F

    iget v1, p0, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->mScale:F

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->scale(FF)V

    .line 303
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->mMovie:Landroid/graphics/Movie;

    iget v1, p0, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->mLeft:F

    iget v2, p0, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->mScale:F

    div-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->mTop:F

    iget v3, p0, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->mScale:F

    div-float/2addr v2, v3

    invoke-virtual {v0, p1, v1, v2}, Landroid/graphics/Movie;->draw(Landroid/graphics/Canvas;FF)V

    .line 304
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 305
    return-void
.end method

.method private invalidateView()V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 265
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->mVisible:Z

    if-eqz v0, :cond_0

    .line 266
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_1

    .line 267
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->postInvalidateOnAnimation()V

    .line 272
    :cond_0
    :goto_0
    return-void

    .line 269
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->invalidate()V

    goto :goto_0
.end method

.method private setViewAttributes(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 82
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 83
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->setLayerType(ILandroid/graphics/Paint;)V

    .line 117
    :cond_0
    return-void
.end method

.method private updateAnimationTime()V
    .locals 7

    .prologue
    .line 278
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    .line 280
    .local v1, "now":J
    iget-wide v3, p0, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->mMovieStart:J

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-nez v3, :cond_0

    .line 281
    iput-wide v1, p0, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->mMovieStart:J

    .line 284
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->mMovie:Landroid/graphics/Movie;

    invoke-virtual {v3}, Landroid/graphics/Movie;->duration()I

    move-result v0

    .line 286
    .local v0, "dur":I
    if-nez v0, :cond_1

    .line 287
    const/16 v0, 0x3e8

    .line 290
    :cond_1
    iget-wide v3, p0, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->mMovieStart:J

    sub-long v3, v1, v3

    int-to-long v5, v0

    rem-long/2addr v3, v5

    long-to-int v3, v3

    iput v3, p0, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->mCurrentAnimationTime:I

    .line 291
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 336
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->mMovie:Landroid/graphics/Movie;

    .line 337
    return-void
.end method

.method public getMovie()Landroid/graphics/Movie;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->mMovie:Landroid/graphics/Movie;

    return-object v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v1, 0x0

    .line 238
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->mMovie:Landroid/graphics/Movie;

    if-eqz v0, :cond_0

    .line 240
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->mRestart:Z

    if-eqz v0, :cond_1

    .line 241
    iput v1, p0, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->mCurrentAnimationTime:I

    .line 242
    invoke-direct {p0}, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->invalidateView()V

    .line 243
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->mRestart:Z

    .line 255
    :cond_0
    :goto_0
    return-void

    .line 247
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->mPaused:Z

    if-nez v0, :cond_2

    .line 248
    invoke-direct {p0}, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->updateAnimationTime()V

    .line 249
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->drawMovieFrame(Landroid/graphics/Canvas;)V

    .line 250
    invoke-direct {p0}, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->invalidateView()V

    goto :goto_0

    .line 252
    :cond_2
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->drawMovieFrame(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 3
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    .line 225
    invoke-super/range {p0 .. p5}, Landroid/view/View;->onLayout(ZIIII)V

    .line 230
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->getWidth()I

    move-result v0

    iget v1, p0, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->mMeasuredMovieWidth:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    div-float/2addr v0, v2

    iput v0, p0, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->mLeft:F

    .line 231
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->getHeight()I

    move-result v0

    iget v1, p0, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->mMeasuredMovieHeight:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    div-float/2addr v0, v2

    iput v0, p0, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->mTop:F

    .line 233
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->mVisible:Z

    .line 234
    return-void

    .line 233
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 10
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 175
    iget-object v8, p0, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->mMovie:Landroid/graphics/Movie;

    if-eqz v8, :cond_2

    .line 176
    iget-object v8, p0, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->mMovie:Landroid/graphics/Movie;

    invoke-virtual {v8}, Landroid/graphics/Movie;->width()I

    move-result v5

    .line 177
    .local v5, "movieWidth":I
    iget-object v8, p0, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->mMovie:Landroid/graphics/Movie;

    invoke-virtual {v8}, Landroid/graphics/Movie;->height()I

    move-result v4

    .line 182
    .local v4, "movieHeight":I
    const/high16 v6, 0x3f800000    # 1.0f

    .line 183
    .local v6, "scaleH":F
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    .line 185
    .local v3, "measureModeWidth":I
    if-eqz v3, :cond_0

    .line 186
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 187
    .local v1, "maximumWidth":I
    if-le v5, v1, :cond_0

    .line 188
    int-to-float v8, v5

    int-to-float v9, v1

    div-float v6, v8, v9

    .line 195
    .end local v1    # "maximumWidth":I
    :cond_0
    const/high16 v7, 0x3f800000    # 1.0f

    .line 196
    .local v7, "scaleW":F
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    .line 198
    .local v2, "measureModeHeight":I
    if-eqz v2, :cond_1

    .line 199
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 200
    .local v0, "maximumHeight":I
    if-le v4, v0, :cond_1

    .line 201
    int-to-float v8, v4

    int-to-float v9, v0

    div-float v7, v8, v9

    .line 208
    .end local v0    # "maximumHeight":I
    :cond_1
    const/high16 v8, 0x3f800000    # 1.0f

    invoke-static {v6, v7}, Ljava/lang/Math;->max(FF)F

    move-result v9

    div-float/2addr v8, v9

    iput v8, p0, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->mScale:F

    .line 210
    int-to-float v8, v5

    iget v9, p0, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->mScale:F

    mul-float/2addr v8, v9

    float-to-int v8, v8

    iput v8, p0, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->mMeasuredMovieWidth:I

    .line 211
    int-to-float v8, v4

    iget v9, p0, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->mScale:F

    mul-float/2addr v8, v9

    float-to-int v8, v8

    iput v8, p0, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->mMeasuredMovieHeight:I

    .line 213
    iget v8, p0, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->mMeasuredMovieWidth:I

    iget v9, p0, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->mMeasuredMovieHeight:I

    invoke-virtual {p0, v8, v9}, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->setMeasuredDimension(II)V

    .line 221
    .end local v2    # "measureModeHeight":I
    .end local v3    # "measureModeWidth":I
    .end local v4    # "movieHeight":I
    .end local v5    # "movieWidth":I
    .end local v6    # "scaleH":F
    .end local v7    # "scaleW":F
    :goto_0
    return-void

    .line 219
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->getSuggestedMinimumWidth()I

    move-result v8

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->getSuggestedMinimumHeight()I

    move-result v9

    invoke-virtual {p0, v8, v9}, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->setMeasuredDimension(II)V

    goto :goto_0
.end method

.method public onScreenStateChanged(I)V
    .locals 1
    .param p1, "screenState"    # I
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 310
    invoke-super {p0, p1}, Landroid/view/View;->onScreenStateChanged(I)V

    .line 311
    if-ne p1, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->mVisible:Z

    .line 312
    invoke-direct {p0}, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->invalidateView()V

    .line 313
    return-void

    .line 311
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onVisibilityChanged(Landroid/view/View;I)V
    .locals 1
    .param p1, "changedView"    # Landroid/view/View;
    .param p2, "visibility"    # I
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 318
    invoke-super {p0, p1, p2}, Landroid/view/View;->onVisibilityChanged(Landroid/view/View;I)V

    .line 319
    if-nez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->mVisible:Z

    .line 320
    invoke-direct {p0}, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->invalidateView()V

    .line 321
    return-void

    .line 319
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onWindowVisibilityChanged(I)V
    .locals 1
    .param p1, "visibility"    # I

    .prologue
    .line 325
    invoke-super {p0, p1}, Landroid/view/View;->onWindowVisibilityChanged(I)V

    .line 326
    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->mVisible:Z

    .line 327
    invoke-direct {p0}, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->invalidateView()V

    .line 328
    return-void

    .line 326
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setMovie(Landroid/graphics/Movie;)V
    .locals 0
    .param p1, "movie"    # Landroid/graphics/Movie;

    .prologue
    .line 151
    iput-object p1, p0, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->mMovie:Landroid/graphics/Movie;

    .line 152
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->requestLayout()V

    .line 153
    return-void
.end method

.method public setMovieResource(I)V
    .locals 4
    .param p1, "movieResId"    # I

    .prologue
    .line 124
    iput p1, p0, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->mMovieResourceId:I

    .line 125
    const/4 v1, 0x0

    .line 128
    .local v1, "in":Ljava/io/InputStream;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->mMovieResourceId:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v1

    .line 129
    invoke-static {v1}, Landroid/graphics/Movie;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Movie;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->mMovie:Landroid/graphics/Movie;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 136
    :try_start_1
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 143
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->requestLayout()V

    .line 144
    return-void

    .line 138
    :catch_0
    move-exception v0

    .line 140
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 134
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v2

    .line 136
    :try_start_2
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 141
    :goto_1
    throw v2

    .line 138
    :catch_1
    move-exception v0

    .line 140
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method

.method public setMovieTime(I)V
    .locals 0
    .param p1, "time"    # I

    .prologue
    .line 168
    iput p1, p0, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->mCurrentAnimationTime:I

    .line 169
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->invalidate()V

    .line 170
    return-void
.end method
