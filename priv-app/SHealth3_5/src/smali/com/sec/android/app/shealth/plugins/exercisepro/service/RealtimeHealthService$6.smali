.class Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$6;
.super Landroid/os/Handler;
.source "RealtimeHealthService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)V
    .locals 0

    .prologue
    .line 1117
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$6;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method private checkTopActivityAndNotification()V
    .locals 2

    .prologue
    .line 1243
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$6;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getHRMStatus()I

    move-result v0

    if-eqz v0, :cond_0

    .line 1244
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$6;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->stopHRM()V

    .line 1246
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$6;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->notifyExerciseStopToWorkForLife()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$1700(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)V

    .line 1247
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;

    .line 1248
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f090a85

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->play(Ljava/lang/String;)Z

    .line 1250
    const/16 v0, 0xc8

    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->closeNotification(I)V

    .line 1251
    return-void
.end method

.method private showNotiAndSetSynchronized(J)V
    .locals 6
    .param p1, "exerciseId"    # J

    .prologue
    .line 1228
    const/16 v3, 0x42

    const-wide/16 v4, 0x0

    invoke-static {v3, v4, v5, p1, p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->showNotification(IJJ)Landroid/app/Notification;

    .line 1229
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$6;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mListListener:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Ljava/util/ArrayList;

    move-result-object v4

    monitor-enter v4

    .line 1230
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$6;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mListListener:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1232
    .local v2, "listener":Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;
    :try_start_1
    invoke-interface {v2, p1, p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;->onWorkoutSaved(J)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1234
    :catch_0
    move-exception v0

    .line 1235
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 1238
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "listener":Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3

    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1239
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 12
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/16 v11, 0xc8

    const/16 v10, 0x7d0

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 1120
    iget v6, p1, Landroid/os/Message;->what:I

    sparse-switch v6, :sswitch_data_0

    .line 1225
    :cond_0
    :goto_0
    :sswitch_0
    return-void

    .line 1122
    :sswitch_1
    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$6;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getHRMStatus()I

    move-result v6

    if-nez v6, :cond_0

    .line 1123
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$6;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    iget-object v6, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v6, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v7, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->startHRM(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)V

    goto :goto_0

    .line 1126
    :sswitch_2
    sput-boolean v9, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->fFirstConnected:Z

    .line 1127
    invoke-static {v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->setRecording(Z)V

    .line 1128
    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$6;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mWorkoutState:I
    invoke-static {v6, v10}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$702(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;I)I

    .line 1129
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;

    .line 1130
    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$6;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mExerciseId:J
    invoke-static {v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$500(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)J

    move-result-wide v6

    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$6;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    iget-object v8, v8, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v8, v8, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_distance_sum:F

    float-to-double v8, v8

    invoke-static {v6, v7, v8, v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->goalEnd(JD)V

    .line 1133
    invoke-static {v11}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->closeNotification(I)V

    goto :goto_0

    .line 1136
    :sswitch_3
    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$6;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mWorkoutState:I
    invoke-static {v6, v10}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$702(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;I)I

    .line 1138
    invoke-static {v11}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->closeNotification(I)V

    .line 1139
    const/16 v6, 0x11

    const/4 v7, 0x0

    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->showNotification(ILcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;)Landroid/app/Notification;

    goto :goto_0

    .line 1142
    :sswitch_4
    sget-boolean v6, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->fFirstConnected:Z

    if-eqz v6, :cond_1

    .line 1143
    sput-boolean v8, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->fFirstConnected:Z

    .line 1144
    invoke-static {v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->setRecording(Z)V

    .line 1145
    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$6;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    const/16 v7, 0x7d1

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mWorkoutState:I
    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$702(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;I)I

    .line 1146
    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$6;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    new-instance v7, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    const/4 v8, 0x2

    invoke-direct {v7, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;-><init>(I)V

    iput-object v7, v6, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    .line 1147
    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$6;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->notifyExerciseStartToWorkForLife()V
    invoke-static {v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$1400(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)V

    .line 1149
    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$6;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;

    .line 1150
    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$6;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    iget-object v6, v6, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->listenerGoal:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils$GoalListener;

    invoke-static {v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->registerListener(Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils$GoalListener;)V

    .line 1151
    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$6;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->setGoal(Z)Z
    invoke-static {v6, v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$1500(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;Z)Z

    move-result v6

    if-eqz v6, :cond_2

    const v0, 0x7f090a88

    .line 1152
    .local v0, "audioGuideId":I
    :goto_1
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;

    .line 1153
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->play(Ljava/lang/String;)Z

    .line 1155
    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$6;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mConnectedTime:Landroid/text/format/Time;
    invoke-static {v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$1600(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Landroid/text/format/Time;

    move-result-object v6

    invoke-virtual {v6}, Landroid/text/format/Time;->setToNow()V

    .line 1158
    .end local v0    # "audioGuideId":I
    :cond_1
    invoke-static {v11}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->closeNotification(I)V

    .line 1159
    const/16 v6, 0x21

    const/4 v7, 0x0

    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->showNotification(ILcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;)Landroid/app/Notification;

    goto/16 :goto_0

    .line 1151
    :cond_2
    const v0, 0x7f090a87

    goto :goto_1

    .line 1162
    :sswitch_5
    invoke-static {v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->setRecording(Z)V

    .line 1163
    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$6;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mWorkoutState:I
    invoke-static {v6, v10}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$702(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;I)I

    .line 1164
    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$6;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    iget-object v6, v6, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    const/4 v7, -0x1

    iput v7, v6, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->paceGuide:I

    .line 1166
    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$6;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    invoke-virtual {v6, v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->stopFE(Z)V

    .line 1167
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$6;->checkTopActivityAndNotification()V

    .line 1169
    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$6;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->doSaveExercise()V

    .line 1170
    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$6;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mExerciseId:J
    invoke-static {v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$500(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)J

    move-result-wide v2

    .line 1171
    .local v2, "exerciseId":J
    invoke-direct {p0, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$6;->showNotiAndSetSynchronized(J)V

    goto/16 :goto_0

    .line 1178
    .end local v2    # "exerciseId":J
    :sswitch_6
    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$6;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->startFE()V

    goto/16 :goto_0

    .line 1181
    :sswitch_7
    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$6;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    iget-object v6, v6, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iput v7, v6, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_heartrate:F

    .line 1182
    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$6;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->notiLastValue()V

    .line 1183
    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$6;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;

    .line 1184
    const/4 v6, 0x5

    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->workoutProgress(IF)V

    .line 1185
    const/16 v6, 0xc

    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->workoutProgress(IF)V

    goto/16 :goto_0

    .line 1188
    :sswitch_8
    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$6;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    iget-object v6, v6, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iput v7, v6, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_heartrate:F

    .line 1189
    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$6;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->notiLastValue()V

    .line 1190
    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$6;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;

    .line 1191
    const/4 v6, 0x5

    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->workoutProgress(IF)V

    .line 1192
    const/16 v6, 0xc

    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->workoutProgress(IF)V

    goto/16 :goto_0

    .line 1195
    :sswitch_9
    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$6;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    iget-object v6, v6, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iput v7, v6, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_heartrate:F

    goto/16 :goto_0

    .line 1200
    :sswitch_a
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$600()Ljava/lang/String;

    move-result-object v6

    const-string v7, "FE_NOTI_USER_CANCEL"

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1201
    invoke-static {v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->setRecording(Z)V

    .line 1202
    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$6;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mWorkoutState:I
    invoke-static {v6, v10}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$702(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;I)I

    .line 1203
    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$6;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    iget-object v6, v6, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    const/4 v7, -0x1

    iput v7, v6, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->paceGuide:I

    .line 1204
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$6;->checkTopActivityAndNotification()V

    .line 1206
    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$6;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->doSaveExercise()V

    .line 1207
    const/16 v6, 0x42

    const-wide/16 v7, 0x0

    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$6;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mExerciseId:J
    invoke-static {v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$500(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)J

    move-result-wide v9

    invoke-static {v6, v7, v8, v9, v10}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->showNotification(IJJ)Landroid/app/Notification;

    .line 1208
    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$6;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mListListener:Ljava/util/ArrayList;
    invoke-static {v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Ljava/util/ArrayList;

    move-result-object v7

    monitor-enter v7

    .line 1209
    :try_start_0
    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$6;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mListListener:Ljava/util/ArrayList;
    invoke-static {v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1211
    .local v5, "listener":Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;
    :try_start_1
    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$6;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mExerciseId:J
    invoke-static {v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$500(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)J

    move-result-wide v8

    invoke-interface {v5, v8, v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;->onWorkoutSaved(J)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 1213
    :catch_0
    move-exception v1

    .line 1214
    .local v1, "e":Ljava/lang/Exception;
    :try_start_2
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$600()Ljava/lang/String;

    move-result-object v6

    const-string v8, "FE_NOTI_USER_CANCEL : AUTO_SAVE -> Exception"

    invoke-static {v6, v8}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 1217
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v5    # "listener":Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;
    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v6

    .restart local v4    # "i$":Ljava/util/Iterator;
    :cond_3
    :try_start_3
    monitor-exit v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 1120
    :sswitch_data_0
    .sparse-switch
        0x3e8 -> :sswitch_7
        0x3e9 -> :sswitch_8
        0x3ea -> :sswitch_9
        0x3ed -> :sswitch_0
        0x3ee -> :sswitch_2
        0x3ef -> :sswitch_3
        0x3f0 -> :sswitch_4
        0x3f1 -> :sswitch_5
        0x3f2 -> :sswitch_6
        0x3f3 -> :sswitch_a
        0xfa4 -> :sswitch_1
    .end sparse-switch
.end method
