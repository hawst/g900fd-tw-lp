.class public Lcom/sec/android/app/shealth/food/mask/BitmapProvider;
.super Ljava/lang/Object;
.source "BitmapProvider.java"


# instance fields
.field private mCache:Landroid/support/v4/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v2

    const-wide/16 v4, 0x8

    div-long v0, v2, v4

    .line 34
    .local v0, "memory":J
    new-instance v2, Lcom/sec/android/app/shealth/food/mask/BitmapProvider$1;

    long-to-int v3, v0

    invoke-direct {v2, p0, v3}, Lcom/sec/android/app/shealth/food/mask/BitmapProvider$1;-><init>(Lcom/sec/android/app/shealth/food/mask/BitmapProvider;I)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/food/mask/BitmapProvider;->mCache:Landroid/support/v4/util/LruCache;

    .line 41
    return-void
.end method


# virtual methods
.method public clearLruCache()V
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mask/BitmapProvider;->mCache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v0}, Landroid/support/v4/util/LruCache;->evictAll()V

    .line 81
    return-void
.end method

.method public getImage(Ljava/lang/String;II)Landroid/graphics/Bitmap;
    .locals 5
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "requiredWidth"    # I
    .param p3, "requiredHeight"    # I

    .prologue
    const/4 v3, 0x0

    .line 53
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 54
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_1

    move-object v0, v3

    .line 72
    :cond_0
    :goto_0
    return-object v0

    .line 57
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/mask/BitmapProvider;->mCache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v4, p1}, Landroid/support/v4/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 58
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_2

    .line 59
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    if-gt p2, v4, :cond_2

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    if-le p3, v4, :cond_0

    .line 63
    :cond_2
    const/4 v4, 0x1

    invoke-static {p1, p3, p2, v4}, Lcom/sec/android/app/shealth/common/utils/BitmapUtil;->getCroppedAndScaledByCenterBitmap(Ljava/lang/String;IIZ)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 64
    .local v2, "result":Landroid/graphics/Bitmap;
    if-eqz v2, :cond_3

    .line 66
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mask/BitmapProvider;->mCache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v3, p1, v2}, Landroid/support/v4/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v0, v2

    .line 68
    goto :goto_0

    :cond_3
    move-object v0, v3

    .line 72
    goto :goto_0
.end method
