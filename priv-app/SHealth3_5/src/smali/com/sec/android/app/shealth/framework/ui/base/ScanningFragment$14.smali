.class Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$14;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->actionFoundScannedDevice(Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

.field final synthetic val$device:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$14;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    iput-object p2, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$14;->val$device:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 36

    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, -0x6f

    aput v12, v2, v11

    const/16 v11, -0xf

    aput v11, v2, v10

    const/16 v10, 0x6439

    aput v10, v2, v9

    const/16 v9, -0x1aee

    aput v9, v2, v8

    const/16 v8, -0x69

    aput v8, v2, v7

    const/16 v7, 0x2029

    aput v7, v2, v6

    const/16 v6, 0x5873

    aput v6, v2, v5

    const/16 v5, -0x29c6

    aput v5, v2, v4

    const/16 v4, -0x41

    aput v4, v2, v3

    const/16 v3, -0x38c1

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, -0xc

    aput v13, v1, v12

    const/16 v12, -0x6e

    aput v12, v1, v11

    const/16 v11, 0x6450

    aput v11, v1, v10

    const/16 v10, -0x1a9c

    aput v10, v1, v9

    const/16 v9, -0x1b

    aput v9, v1, v8

    const/16 v8, 0x204c

    aput v8, v1, v7

    const/16 v7, 0x5820

    aput v7, v1, v6

    const/16 v6, -0x29a8

    aput v6, v1, v5

    const/16 v5, -0x2a

    aput v5, v1, v4

    const/16 v4, -0x38ad

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v4, v1

    if-lt v3, v4, :cond_2

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v4, v1

    if-lt v3, v4, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    const/16 v1, 0x1f

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, 0x1d

    const/16 v33, 0x1e

    const/16 v34, -0x24f8

    aput v34, v2, v33

    const/16 v33, -0xd

    aput v33, v2, v32

    const/16 v32, -0x4

    aput v32, v2, v31

    const/16 v31, -0x4d9e

    aput v31, v2, v30

    const/16 v30, -0x40

    aput v30, v2, v29

    const/16 v29, -0x30

    aput v29, v2, v28

    const/16 v28, 0x631d

    aput v28, v2, v27

    const/16 v27, 0x3406

    aput v27, v2, v26

    const/16 v26, 0x557

    aput v26, v2, v25

    const/16 v25, -0x4694

    aput v25, v2, v24

    const/16 v24, -0x31

    aput v24, v2, v23

    const/16 v23, -0x10

    aput v23, v2, v22

    const/16 v22, -0xd

    aput v22, v2, v21

    const/16 v21, -0x27f9

    aput v21, v2, v20

    const/16 v20, -0x43

    aput v20, v2, v19

    const/16 v19, -0x7a

    aput v19, v2, v18

    const/16 v18, -0x37

    aput v18, v2, v17

    const/16 v17, -0x37

    aput v17, v2, v16

    const/16 v16, -0x12

    aput v16, v2, v15

    const/16 v15, -0x2c1

    aput v15, v2, v14

    const/16 v14, -0x67

    aput v14, v2, v13

    const/16 v13, -0x46

    aput v13, v2, v12

    const/16 v12, 0x1074

    aput v12, v2, v11

    const/16 v11, 0x2c7f

    aput v11, v2, v10

    const/16 v10, 0x626a

    aput v10, v2, v9

    const/16 v9, 0x2c0c

    aput v9, v2, v8

    const/16 v8, -0x58bd

    aput v8, v2, v7

    const/16 v7, -0x32

    aput v7, v2, v6

    const/16 v6, -0x2e

    aput v6, v2, v5

    const/16 v5, 0x782d

    aput v5, v2, v3

    const/16 v3, -0x7be7

    aput v3, v2, v1

    const/16 v1, 0x1f

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, 0x1d

    const/16 v34, 0x1e

    const/16 v35, -0x24df

    aput v35, v1, v34

    const/16 v34, -0x25

    aput v34, v1, v33

    const/16 v33, -0x6e

    aput v33, v1, v32

    const/16 v32, -0x4de9

    aput v32, v1, v31

    const/16 v31, -0x4e

    aput v31, v1, v30

    const/16 v30, -0x12

    aput v30, v1, v29

    const/16 v29, 0x6330

    aput v29, v1, v28

    const/16 v28, 0x3463

    aput v28, v1, v27

    const/16 v27, 0x534

    aput v27, v1, v26

    const/16 v26, -0x46fb

    aput v26, v1, v25

    const/16 v25, -0x47

    aput v25, v1, v24

    const/16 v24, -0x6b

    aput v24, v1, v23

    const/16 v23, -0x49

    aput v23, v1, v22

    const/16 v22, -0x279d

    aput v22, v1, v21

    const/16 v21, -0x28

    aput v21, v1, v20

    const/16 v20, -0x18

    aput v20, v1, v19

    const/16 v19, -0x59

    aput v19, v1, v18

    const/16 v18, -0x58

    aput v18, v1, v17

    const/16 v17, -0x73

    aput v17, v1, v16

    const/16 v16, -0x294

    aput v16, v1, v15

    const/4 v15, -0x3

    aput v15, v1, v14

    const/16 v14, -0x2c

    aput v14, v1, v13

    const/16 v13, 0x1001

    aput v13, v1, v12

    const/16 v12, 0x2c10

    aput v12, v1, v11

    const/16 v11, 0x622c

    aput v11, v1, v10

    const/16 v10, 0x2c62

    aput v10, v1, v9

    const/16 v9, -0x58d4

    aput v9, v1, v8

    const/16 v8, -0x59

    aput v8, v1, v7

    const/16 v7, -0x5a

    aput v7, v1, v6

    const/16 v6, 0x784e

    aput v6, v1, v5

    const/16 v5, -0x7b88

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v5, v1

    if-lt v3, v5, :cond_4

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v5, v1

    if-lt v3, v5, :cond_5

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$14;->val$device:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getDeviceName()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$14;->val$device:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getDeviceId()Ljava/lang/String;

    move-result-object v5

    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, -0x75e9

    aput v14, v2, v13

    const/16 v13, -0x17

    aput v13, v2, v12

    const/16 v12, -0xb

    aput v12, v2, v11

    const/4 v11, -0x6

    aput v11, v2, v10

    const/16 v10, -0x52be

    aput v10, v2, v9

    const/16 v9, -0x38

    aput v9, v2, v8

    const/16 v8, -0x72

    aput v8, v2, v7

    const/16 v7, -0x56

    aput v7, v2, v6

    const/16 v6, -0x70c2

    aput v6, v2, v3

    const/16 v3, -0x1d

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, -0x758e

    aput v15, v1, v14

    const/16 v14, -0x76

    aput v14, v1, v13

    const/16 v13, -0x64

    aput v13, v1, v12

    const/16 v12, -0x74

    aput v12, v1, v11

    const/16 v11, -0x52d0

    aput v11, v1, v10

    const/16 v10, -0x53

    aput v10, v1, v9

    const/16 v9, -0x23

    aput v9, v1, v8

    const/16 v8, -0x38

    aput v8, v1, v7

    const/16 v7, -0x70a9

    aput v7, v1, v6

    const/16 v6, -0x71

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_4
    array-length v6, v1

    if-lt v3, v6, :cond_6

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_5
    array-length v6, v1

    if-lt v3, v6, :cond_7

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x1b

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/4 v12, 0x6

    const/4 v13, 0x7

    const/16 v14, 0x8

    const/16 v15, 0x9

    const/16 v16, 0xa

    const/16 v17, 0xb

    const/16 v18, 0xc

    const/16 v19, 0xd

    const/16 v20, 0xe

    const/16 v21, 0xf

    const/16 v22, 0x10

    const/16 v23, 0x11

    const/16 v24, 0x12

    const/16 v25, 0x13

    const/16 v26, 0x14

    const/16 v27, 0x15

    const/16 v28, 0x16

    const/16 v29, 0x17

    const/16 v30, 0x18

    const/16 v31, 0x19

    const/16 v32, 0x1a

    const/16 v33, 0x5232

    aput v33, v2, v32

    const/16 v32, 0x537

    aput v32, v2, v31

    const/16 v31, 0x5b68

    aput v31, v2, v30

    const/16 v30, 0x363a

    aput v30, v2, v29

    const/16 v29, 0x5b58

    aput v29, v2, v28

    const/16 v28, 0xa7b

    aput v28, v2, v27

    const/16 v27, 0x6857

    aput v27, v2, v26

    const/16 v26, 0x4b0d

    aput v26, v2, v25

    const/16 v25, 0x2428

    aput v25, v2, v24

    const/16 v24, 0x214d

    aput v24, v2, v23

    const/16 v23, 0x2457

    aput v23, v2, v22

    const/16 v22, -0x17bf

    aput v22, v2, v21

    const/16 v21, -0x54

    aput v21, v2, v20

    const/16 v20, 0x1d3f

    aput v20, v2, v19

    const/16 v19, 0x2e5f

    aput v19, v2, v18

    const/16 v18, 0x494a

    aput v18, v2, v17

    const/16 v17, 0x7f27

    aput v17, v2, v16

    const/16 v16, 0x710a

    aput v16, v2, v15

    const/16 v15, 0x621e

    aput v15, v2, v14

    const/16 v14, -0x79dc

    aput v14, v2, v13

    const/16 v13, -0x18

    aput v13, v2, v12

    const/16 v12, -0x42b4

    aput v12, v2, v11

    const/16 v11, -0x2c

    aput v11, v2, v10

    const/16 v10, -0x5d

    aput v10, v2, v9

    const/16 v9, -0x53

    aput v9, v2, v8

    const/16 v8, -0x52

    aput v8, v2, v3

    const/16 v3, 0xe12

    aput v3, v2, v1

    const/16 v1, 0x1b

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x2

    const/4 v10, 0x3

    const/4 v11, 0x4

    const/4 v12, 0x5

    const/4 v13, 0x6

    const/4 v14, 0x7

    const/16 v15, 0x8

    const/16 v16, 0x9

    const/16 v17, 0xa

    const/16 v18, 0xb

    const/16 v19, 0xc

    const/16 v20, 0xd

    const/16 v21, 0xe

    const/16 v22, 0xf

    const/16 v23, 0x10

    const/16 v24, 0x11

    const/16 v25, 0x12

    const/16 v26, 0x13

    const/16 v27, 0x14

    const/16 v28, 0x15

    const/16 v29, 0x16

    const/16 v30, 0x17

    const/16 v31, 0x18

    const/16 v32, 0x19

    const/16 v33, 0x1a

    const/16 v34, 0x520f

    aput v34, v1, v33

    const/16 v33, 0x552

    aput v33, v1, v32

    const/16 v32, 0x5b05

    aput v32, v1, v31

    const/16 v31, 0x365b

    aput v31, v1, v30

    const/16 v30, 0x5b36

    aput v30, v1, v29

    const/16 v29, 0xa5b

    aput v29, v1, v28

    const/16 v28, 0x680a

    aput v28, v1, v27

    const/16 v27, 0x4b68

    aput v27, v1, v26

    const/16 v26, 0x244b

    aput v26, v1, v25

    const/16 v25, 0x2124

    aput v25, v1, v24

    const/16 v24, 0x2421

    aput v24, v1, v23

    const/16 v23, -0x17dc

    aput v23, v1, v22

    const/16 v22, -0x18

    aput v22, v1, v21

    const/16 v21, 0x1d6b

    aput v21, v1, v20

    const/16 v20, 0x2e1d

    aput v20, v1, v19

    const/16 v19, 0x492e

    aput v19, v1, v18

    const/16 v18, 0x7f49

    aput v18, v1, v17

    const/16 v17, 0x717f

    aput v17, v1, v16

    const/16 v16, 0x6271

    aput v16, v1, v15

    const/16 v15, -0x799e

    aput v15, v1, v14

    const/16 v14, -0x7a

    aput v14, v1, v13

    const/16 v13, -0x42dd

    aput v13, v1, v12

    const/16 v12, -0x43

    aput v12, v1, v11

    const/16 v11, -0x29

    aput v11, v1, v10

    const/16 v10, -0x32

    aput v10, v1, v9

    const/16 v9, -0x31

    aput v9, v1, v8

    const/16 v8, 0xe49

    aput v8, v1, v3

    const/4 v3, 0x0

    :goto_6
    array-length v8, v1

    if-lt v3, v8, :cond_8

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_7
    array-length v8, v1

    if-lt v3, v8, :cond_9

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v1, 0x4

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, -0x3

    aput v10, v2, v9

    const/16 v9, -0x9

    aput v9, v2, v8

    const/16 v8, 0x5c1f

    aput v8, v2, v3

    const/16 v3, -0x4684

    aput v3, v2, v1

    const/4 v1, 0x4

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x2

    const/4 v10, 0x3

    const/16 v11, -0x40

    aput v11, v1, v10

    const/16 v10, -0x6d

    aput v10, v1, v9

    const/16 v9, 0x5c56

    aput v9, v1, v8

    const/16 v8, -0x46a4

    aput v8, v1, v3

    const/4 v3, 0x0

    :goto_8
    array-length v8, v1

    if-lt v3, v8, :cond_a

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_9
    array-length v8, v1

    if-lt v3, v8, :cond_b

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v6, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$14;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->deviceMap:Ljava/util/concurrent/ConcurrentHashMap;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$1700(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$14;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->deviceMap:Ljava/util/concurrent/ConcurrentHashMap;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$1700(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$14;->val$device:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v1, v5, v2}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$14;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->spinner:Landroid/widget/ProgressBar;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$4800(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Landroid/widget/ProgressBar;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$14;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->scanningLayout:Landroid/widget/RelativeLayout;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$4900(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Landroid/widget/RelativeLayout;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$14;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mScannedlist:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$1100(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Landroid/widget/LinearLayout;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :cond_0
    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/base/PairOfDeviceIdAndName;

    invoke-direct {v1, v5, v4}, Lcom/sec/android/app/shealth/framework/ui/base/PairOfDeviceIdAndName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$14;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mScannedDevicesList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$1600(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$14;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mScannedDevicesList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$1600(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$14;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # invokes: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getScannedView(Lcom/sec/android/app/shealth/framework/ui/base/PairOfDeviceIdAndName;)Landroid/view/View;
    invoke-static {v2, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$5000(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Lcom/sec/android/app/shealth/framework/ui/base/PairOfDeviceIdAndName;)Landroid/view/View;

    move-result-object v1

    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$14;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mScannedlist:Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$1100(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Landroid/widget/LinearLayout;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    :cond_1
    :goto_a
    return-void

    :cond_2
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_3
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_4
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    :cond_5
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3

    :cond_6
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_4

    :cond_7
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_5

    :cond_8
    aget v8, v1, v3

    aget v9, v2, v3

    xor-int/2addr v8, v9

    aput v8, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_6

    :cond_9
    aget v8, v2, v3

    int-to-char v8, v8

    aput-char v8, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_7

    :cond_a
    aget v8, v1, v3

    aget v9, v2, v3

    xor-int/2addr v8, v9

    aput v8, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_8

    :cond_b
    aget v8, v2, v3

    int-to-char v8, v8

    aput-char v8, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_9

    :catch_0
    move-exception v1

    goto :goto_a
.end method
