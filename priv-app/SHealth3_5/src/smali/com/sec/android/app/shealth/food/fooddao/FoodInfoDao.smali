.class public interface abstract Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;
.super Ljava/lang/Object;
.source "FoodInfoDao.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commondao/CommonDao;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/sec/android/app/shealth/common/commondao/CommonDao",
        "<",
        "Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;",
        ">;"
    }
.end annotation


# virtual methods
.method public abstract getFoodInfoFrequencyByMeal(JJI)Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJI)",
            "Ljava/util/Map",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getFoodInfoListByMealId(J)Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/Set",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;",
            ">;"
        }
    .end annotation
.end method
