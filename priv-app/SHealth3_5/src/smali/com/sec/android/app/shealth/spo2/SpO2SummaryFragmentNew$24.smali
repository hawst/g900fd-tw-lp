.class Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$24;
.super Ljava/lang/Object;
.source "SpO2SummaryFragmentNew.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->showErrorDialog(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)V
    .locals 0

    .prologue
    .line 1678
    iput-object p1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$24;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/app/Activity;)V
    .locals 2
    .param p1, "parentActivity"    # Landroid/app/Activity;

    .prologue
    const/4 v1, 0x0

    .line 1682
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$24;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->timer:Ljava/util/Timer;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$4800(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Ljava/util/Timer;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1683
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$24;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->timer:Ljava/util/Timer;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$4800(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Ljava/util/Timer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 1686
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$24;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->errorAnimation:Landroid/view/animation/AnimationSet;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$4700(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Landroid/view/animation/AnimationSet;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1687
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$24;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->errorAnimation:Landroid/view/animation/AnimationSet;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$4700(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Landroid/view/animation/AnimationSet;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/animation/AnimationSet;->cancel()V

    .line 1688
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$24;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # setter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->errorAnimation:Landroid/view/animation/AnimationSet;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$4702(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;Landroid/view/animation/AnimationSet;)Landroid/view/animation/AnimationSet;

    .line 1691
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$24;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    iget-object v0, v0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->sHealthAlertDialogbuild:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_2

    .line 1692
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$24;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    iput-object v1, v0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->sHealthAlertDialogbuild:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 1694
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$24;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondCenterIconBigAnimator:Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$3500(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->clearAnimation()V

    .line 1695
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$24;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondCenterIconBigAnimator:Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$3500(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->setVisibility(I)V

    .line 1696
    return-void
.end method
