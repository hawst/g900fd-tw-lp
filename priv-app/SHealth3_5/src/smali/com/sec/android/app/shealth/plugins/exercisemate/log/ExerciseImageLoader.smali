.class public Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoader;
.super Ljava/lang/Object;
.source "ExerciseImageLoader.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private ExerciseId:J

.field private mContext:Landroid/content/Context;

.field private mIloadImageReturn:Lcom/sec/android/app/shealth/plugins/exercisemate/log/IloadImageReturn;

.field private mPhotoDatas:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;",
            ">;"
        }
    .end annotation
.end field

.field private maskHeight:I

.field private maskWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoader;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoader;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/shealth/plugins/exercisemate/log/IloadImageReturn;)V
    .locals 1
    .param p1, "mContext"    # Landroid/content/Context;
    .param p2, "imageReturn"    # Lcom/sec/android/app/shealth/plugins/exercisemate/log/IloadImageReturn;

    .prologue
    const/4 v0, 0x0

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoader;->maskWidth:I

    .line 34
    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoader;->maskHeight:I

    .line 38
    iput-object p2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoader;->mIloadImageReturn:Lcom/sec/android/app/shealth/plugins/exercisemate/log/IloadImageReturn;

    .line 39
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoader;->mPhotoDatas:Ljava/util/ArrayList;

    .line 40
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoader;->mContext:Landroid/content/Context;

    .line 41
    return-void
.end method

.method private BitmapColorChange(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    .locals 10
    .param p1, "Image"    # Landroid/graphics/Bitmap;
    .param p2, "ChangeColor"    # I

    .prologue
    const/4 v2, 0x0

    .line 249
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 250
    .local v3, "width":I
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 251
    .local v7, "height":I
    mul-int v0, v3, v7

    new-array v1, v0, [I

    .local v1, "maskPixels":[I
    move-object v0, p1

    move v4, v2

    move v5, v2

    move v6, v3

    .line 252
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 253
    const/4 v9, 0x0

    .local v9, "x":I
    :goto_0
    array-length v0, v1

    if-ge v9, v0, :cond_1

    .line 254
    aget v0, v1, v9

    invoke-static {v0}, Landroid/graphics/Color;->red(I)I

    move-result v0

    const/16 v2, 0xff

    if-ne v0, v2, :cond_0

    aget v0, v1, v9

    invoke-static {v0}, Landroid/graphics/Color;->green(I)I

    move-result v0

    const/16 v2, 0xe6

    if-ge v0, v2, :cond_0

    .line 255
    aput p2, v1, v9

    .line 253
    :cond_0
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 258
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v0

    invoke-static {v1, v3, v7, v0}, Landroid/graphics/Bitmap;->createBitmap([IIILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 259
    .local v8, "Image2":Landroid/graphics/Bitmap;
    return-object v8
.end method

.method private LoaderMask(I)Landroid/graphics/Bitmap;
    .locals 3
    .param p1, "size"    # I

    .prologue
    .line 186
    const/4 v1, 0x0

    .line 187
    .local v1, "MaskId":I
    packed-switch p1, :pswitch_data_0

    .line 201
    const v1, 0x7f02064a

    .line 204
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoader;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v2, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 205
    .local v0, "MaskD":Landroid/graphics/Bitmap;
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoader;->maskWidth:I

    .line 206
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoader;->maskHeight:I

    .line 207
    return-object v0

    .line 189
    .end local v0    # "MaskD":Landroid/graphics/Bitmap;
    :pswitch_0
    const v1, 0x7f020647

    .line 190
    goto :goto_0

    .line 192
    :pswitch_1
    const v1, 0x7f020648

    .line 193
    goto :goto_0

    .line 195
    :pswitch_2
    const v1, 0x7f020649

    .line 196
    goto :goto_0

    .line 198
    :pswitch_3
    const v1, 0x7f02064a

    .line 199
    goto :goto_0

    .line 187
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static calculateInSampleSize(Landroid/graphics/BitmapFactory$Options;II)I
    .locals 6
    .param p0, "options"    # Landroid/graphics/BitmapFactory$Options;
    .param p1, "reqWidth"    # I
    .param p2, "reqHeight"    # I

    .prologue
    .line 229
    iget v2, p0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 230
    .local v2, "height":I
    iget v4, p0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 231
    .local v4, "width":I
    const/4 v3, 0x1

    .line 233
    .local v3, "inSampleSize":I
    if-gt v2, p2, :cond_0

    if-le v4, p1, :cond_1

    .line 235
    :cond_0
    div-int/lit8 v0, v2, 0x2

    .line 236
    .local v0, "halfHeight":I
    div-int/lit8 v1, v4, 0x2

    .line 240
    .local v1, "halfWidth":I
    :goto_0
    div-int v5, v0, v3

    if-le v5, p2, :cond_2

    div-int v5, v1, v3

    if-le v5, p1, :cond_2

    .line 241
    mul-int/lit8 v3, v3, 0x2

    goto :goto_0

    .line 244
    .end local v0    # "halfHeight":I
    .end local v1    # "halfWidth":I
    :cond_1
    const/4 v3, 0x4

    .line 245
    :cond_2
    return v3
.end method


# virtual methods
.method public LoadBitmap(Ljava/util/List;)Landroid/graphics/Bitmap;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;",
            ">;)",
            "Landroid/graphics/Bitmap;"
        }
    .end annotation

    .prologue
    .line 94
    .local p1, "PhotoDatas":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;>;"
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x4

    if-le v3, v4, :cond_1

    const/16 v18, 0x4

    .line 95
    .local v18, "size":I
    :goto_0
    new-instance v13, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v13}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 96
    .local v13, "options":Landroid/graphics/BitmapFactory$Options;
    const/4 v3, 0x1

    iput-boolean v3, v13, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 97
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoader;->maskWidth:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoader;->maskHeight:I

    invoke-static {v13, v3, v4}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoader;->calculateInSampleSize(Landroid/graphics/BitmapFactory$Options;II)I

    move-result v3

    iput v3, v13, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 98
    const/4 v3, 0x0

    iput-boolean v3, v13, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 99
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoader;->maskWidth:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoader;->maskHeight:I

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v15

    .line 100
    .local v15, "resultBitmap":Landroid/graphics/Bitmap;
    new-instance v16, Landroid/graphics/Canvas;

    move-object/from16 v0, v16

    invoke-direct {v0, v15}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 101
    .local v16, "resultCanvas":Landroid/graphics/Canvas;
    const/4 v12, 0x0

    .local v12, "indexCase":I
    :goto_1
    move/from16 v0, v18

    if-ge v12, v0, :cond_4

    .line 102
    move-object/from16 v0, p1

    invoke-interface {v0, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;

    .line 103
    .local v14, "photoData":Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;
    if-nez v14, :cond_2

    .line 101
    :cond_0
    :goto_2
    add-int/lit8 v12, v12, 0x1

    goto :goto_1

    .line 94
    .end local v12    # "indexCase":I
    .end local v13    # "options":Landroid/graphics/BitmapFactory$Options;
    .end local v14    # "photoData":Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;
    .end local v15    # "resultBitmap":Landroid/graphics/Bitmap;
    .end local v16    # "resultCanvas":Landroid/graphics/Canvas;
    .end local v18    # "size":I
    :cond_1
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v18

    goto :goto_0

    .line 105
    .restart local v12    # "indexCase":I
    .restart local v13    # "options":Landroid/graphics/BitmapFactory$Options;
    .restart local v14    # "photoData":Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;
    .restart local v15    # "resultBitmap":Landroid/graphics/Bitmap;
    .restart local v16    # "resultCanvas":Landroid/graphics/Canvas;
    .restart local v18    # "size":I
    :cond_2
    invoke-virtual {v14}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->getFilePath()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v13}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 106
    .local v2, "LoadImage":Landroid/graphics/Bitmap;
    if-eqz v2, :cond_0

    .line 108
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoader;->maskWidth:I

    int-to-float v3, v3

    const/high16 v4, 0x40000000    # 2.0f

    mul-float/2addr v4, v3

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    if-le v3, v5, :cond_3

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    :goto_3
    int-to-float v3, v3

    div-float v17, v4, v3

    .line 111
    .local v17, "scale":F
    :try_start_0
    new-instance v7, Landroid/graphics/Matrix;

    invoke-direct {v7}, Landroid/graphics/Matrix;-><init>()V

    .line 112
    .local v7, "matrix":Landroid/graphics/Matrix;
    new-instance v11, Landroid/media/ExifInterface;

    invoke-virtual {v14}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->getFilePath()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v11, v3}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    .line 113
    .local v11, "exif":Landroid/media/ExifInterface;
    move/from16 v0, v17

    move/from16 v1, v17

    invoke-virtual {v7, v0, v1}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 114
    const-string v3, "Orientation"

    const/4 v4, 0x1

    invoke-virtual {v11, v3, v4}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result v9

    .line 115
    .local v9, "ExifOri":I
    packed-switch v9, :pswitch_data_0

    .line 127
    :goto_4
    :pswitch_0
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    const/4 v8, 0x1

    invoke-static/range {v2 .. v8}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 133
    .end local v7    # "matrix":Landroid/graphics/Matrix;
    .end local v9    # "ExifOri":I
    .end local v11    # "exif":Landroid/media/ExifInterface;
    :goto_5
    packed-switch v12, :pswitch_data_1

    goto :goto_2

    .line 135
    :pswitch_1
    packed-switch v18, :pswitch_data_2

    goto :goto_2

    .line 137
    :pswitch_2
    const/4 v3, 0x0

    const/4 v4, 0x0

    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_2

    .line 108
    .end local v17    # "scale":F
    :cond_3
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    goto :goto_3

    .line 117
    .restart local v7    # "matrix":Landroid/graphics/Matrix;
    .restart local v9    # "ExifOri":I
    .restart local v11    # "exif":Landroid/media/ExifInterface;
    .restart local v17    # "scale":F
    :pswitch_3
    const/high16 v3, 0x42b40000    # 90.0f

    :try_start_1
    invoke-virtual {v7, v3}, Landroid/graphics/Matrix;->postRotate(F)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_4

    .line 129
    .end local v7    # "matrix":Landroid/graphics/Matrix;
    .end local v9    # "ExifOri":I
    .end local v11    # "exif":Landroid/media/ExifInterface;
    :catch_0
    move-exception v10

    .line 130
    .local v10, "e":Ljava/io/IOException;
    invoke-virtual {v10}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 120
    .end local v10    # "e":Ljava/io/IOException;
    .restart local v7    # "matrix":Landroid/graphics/Matrix;
    .restart local v9    # "ExifOri":I
    .restart local v11    # "exif":Landroid/media/ExifInterface;
    :pswitch_4
    const/high16 v3, 0x43340000    # 180.0f

    :try_start_2
    invoke-virtual {v7, v3}, Landroid/graphics/Matrix;->postRotate(F)Z

    goto :goto_4

    .line 124
    :pswitch_5
    const/high16 v3, 0x43870000    # 270.0f

    invoke-virtual {v7, v3}, Landroid/graphics/Matrix;->postRotate(F)Z
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_4

    .line 140
    .end local v7    # "matrix":Landroid/graphics/Matrix;
    .end local v9    # "ExifOri":I
    .end local v11    # "exif":Landroid/media/ExifInterface;
    :pswitch_6
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoader;->maskWidth:I

    neg-int v4, v4

    int-to-float v4, v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_2

    .line 143
    :pswitch_7
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoader;->maskWidth:I

    neg-int v4, v4

    int-to-float v4, v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_2

    .line 146
    :pswitch_8
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoader;->maskWidth:I

    neg-int v3, v3

    int-to-float v3, v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoader;->maskWidth:I

    neg-int v4, v4

    int-to-float v4, v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_2

    .line 152
    :pswitch_9
    packed-switch v18, :pswitch_data_3

    goto/16 :goto_2

    .line 155
    :pswitch_a
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoader;->maskWidth:I

    int-to-float v4, v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_2

    .line 158
    :pswitch_b
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoader;->maskWidth:I

    neg-int v3, v3

    int-to-float v3, v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoader;->maskWidth:I

    int-to-float v4, v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_2

    .line 161
    :pswitch_c
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoader;->maskWidth:I

    int-to-float v3, v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoader;->maskWidth:I

    neg-int v4, v4

    int-to-float v4, v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_2

    .line 167
    :pswitch_d
    packed-switch v18, :pswitch_data_4

    goto/16 :goto_2

    .line 169
    :pswitch_e
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoader;->maskWidth:I

    int-to-float v3, v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoader;->maskWidth:I

    int-to-float v4, v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_2

    .line 172
    :pswitch_f
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoader;->maskWidth:I

    int-to-float v4, v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_2

    .line 178
    :pswitch_10
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoader;->maskWidth:I

    int-to-float v3, v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoader;->maskHeight:I

    int-to-float v4, v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_2

    .line 182
    .end local v2    # "LoadImage":Landroid/graphics/Bitmap;
    .end local v14    # "photoData":Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;
    .end local v17    # "scale":F
    :cond_4
    return-object v15

    .line 115
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_5
    .end packed-switch

    .line 133
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_9
        :pswitch_d
        :pswitch_10
    .end packed-switch

    .line 135
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_2
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch

    .line 152
    :pswitch_data_3
    .packed-switch 0x2
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch

    .line 167
    :pswitch_data_4
    .packed-switch 0x3
        :pswitch_e
        :pswitch_f
    .end packed-switch
.end method

.method protected execute(J)V
    .locals 11
    .param p1, "ExerciseId"    # J

    .prologue
    const/4 v10, 0x0

    .line 45
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoader;->updatePhotoData(J)V

    .line 46
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoader;->mPhotoDatas:Ljava/util/ArrayList;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoader;->mPhotoDatas:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-nez v7, :cond_1

    .line 75
    :cond_0
    :goto_0
    return-void

    .line 50
    :cond_1
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoader;->mPhotoDatas:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    invoke-direct {p0, v7}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoader;->LoaderMask(I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 51
    .local v1, "Mask":Landroid/graphics/Bitmap;
    const/4 v7, 0x0

    invoke-direct {p0, v1, v7}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoader;->BitmapColorChange(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 52
    .local v2, "Mask2":Landroid/graphics/Bitmap;
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoader;->mPhotoDatas:Ljava/util/ArrayList;

    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoader;->LoadBitmap(Ljava/util/List;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 54
    .local v0, "LoadImage":Landroid/graphics/Bitmap;
    iget v7, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoader;->maskWidth:I

    iget v8, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoader;->maskHeight:I

    sget-object v9, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v7, v8, v9}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 55
    .local v6, "resultBitmap":Landroid/graphics/Bitmap;
    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Landroid/graphics/Bitmap;->setHasAlpha(Z)V

    .line 56
    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    .line 57
    .local v5, "paint":Landroid/graphics/Paint;
    new-instance v7, Landroid/graphics/PorterDuffXfermode;

    sget-object v8, Landroid/graphics/PorterDuff$Mode;->DST_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v7, v8}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v5, v7}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 58
    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v6}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 60
    .local v3, "canvas":Landroid/graphics/Canvas;
    invoke-virtual {v3}, Landroid/graphics/Canvas;->save()I

    .line 61
    new-instance v7, Landroid/graphics/Paint;

    invoke-direct {v7}, Landroid/graphics/Paint;-><init>()V

    invoke-virtual {v3, v0, v10, v10, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 62
    invoke-virtual {v3, v1, v10, v10, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 63
    invoke-virtual {v3}, Landroid/graphics/Canvas;->restore()V

    .line 64
    new-instance v7, Landroid/graphics/Paint;

    invoke-direct {v7}, Landroid/graphics/Paint;-><init>()V

    invoke-virtual {v3, v2, v10, v10, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 66
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 67
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 68
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 69
    new-instance v4, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoader;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-direct {v4, v7, v6}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 70
    .local v4, "drawable":Landroid/graphics/drawable/Drawable;
    if-eqz v4, :cond_2

    .line 71
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoader;->mIloadImageReturn:Lcom/sec/android/app/shealth/plugins/exercisemate/log/IloadImageReturn;

    invoke-interface {v7, p1, p2, v4}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/IloadImageReturn;->onReturnLoadImage(JLandroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 73
    :cond_2
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoader;->mIloadImageReturn:Lcom/sec/android/app/shealth/plugins/exercisemate/log/IloadImageReturn;

    invoke-interface {v7, p1, p2}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/IloadImageReturn;->onNotReturnLoadImage(J)V

    goto :goto_0
.end method

.method public updatePhotoData(J)V
    .locals 7
    .param p1, "rowId"    # J

    .prologue
    .line 78
    const/4 v6, 0x0

    .line 81
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "exercise__id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 85
    .local v3, "selection":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoader;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$ExercisePhoto;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 86
    invoke-static {v6}, Lcom/sec/android/app/shealth/plugins/common/utils/ExercisePhotoUtils;->getExercisePhotoDataList(Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoader;->mPhotoDatas:Ljava/util/ArrayList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 88
    if-eqz v6, :cond_0

    .line 89
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 91
    :cond_0
    return-void

    .line 88
    .end local v3    # "selection":Ljava/lang/String;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_1

    .line 89
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0
.end method
