.class public Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;
.super Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment;
.source "WeightSetGoalCaloriesFragment.java"


# static fields
.field public static final GOAL_DATA_KEY:Ljava/lang/String; = "goal_data_key"

.field private static final OUT_OF_RANGE_DIALOG:Ljava/lang/String; = "OUT_OF_RANGE_DIALOG"

.field private static final sExerciseGoalDataKey:Ljava/lang/String; = "sExerciseGoalDataKey"

.field private static final sMealGoalDataKey:Ljava/lang/String; = "sMealGoalDataKey"

.field private static final sStubPeriod:I = -0x1


# instance fields
.field private mBurnCalorieInput:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

.field private mExerciseGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

.field private mGoalDao:Lcom/sec/android/app/shealth/common/commondao/goaldao/GoalDao;

.field private mGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

.field private mIntakeCalorieInput:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

.field private mMealGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->isInputValid()Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->saveData()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->showAlertDialog()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->showCancelDialog()V

    return-void
.end method

.method private initFooterButtons(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 165
    const v3, 0x7f080c7b

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 166
    .local v0, "doneButton":Landroid/widget/LinearLayout;
    const v3, 0x7f080c7a

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 167
    .local v2, "prevButton":Landroid/widget/LinearLayout;
    const v3, 0x7f08039d

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 169
    .local v1, "doneButtonText":Landroid/widget/TextView;
    const v3, 0x7f090044

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    .line 170
    new-instance v3, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment$2;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment$2;-><init>(Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;)V

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 186
    new-instance v3, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment$3;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment$3;-><init>(Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;)V

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 194
    return-void
.end method

.method private initGoalData(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v4, 0x9c4b

    const v2, 0x9c43

    const/4 v3, -0x1

    .line 88
    if-eqz p1, :cond_2

    const-string/jumbo v0, "sMealGoalDataKey"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 89
    const-string/jumbo v0, "sMealGoalDataKey"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->mMealGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    .line 99
    :cond_0
    :goto_0
    if-eqz p1, :cond_3

    const-string/jumbo v0, "sExerciseGoalDataKey"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 100
    const-string/jumbo v0, "sExerciseGoalDataKey"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->mExerciseGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    .line 109
    :cond_1
    :goto_1
    return-void

    .line 92
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/hcp/GoalUtils;->getLatestGoalData(Landroid/content/Context;Ljava/lang/Integer;)Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->mMealGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    .line 93
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->mMealGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    if-nez v0, :cond_0

    .line 94
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/NetCalorieUtils;->getRecommendedCalorieIntake()I

    move-result v1

    int-to-float v1, v1

    invoke-direct {v0, v1, v2, v3, v3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;-><init>(FIII)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->mMealGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    goto :goto_0

    .line 102
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/hcp/GoalUtils;->getLatestGoalData(Landroid/content/Context;Ljava/lang/Integer;)Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->mExerciseGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    .line 103
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->mExerciseGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    if-nez v0, :cond_1

    .line 104
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/NetCalorieUtils;->getRecommendedCalorieBurn()D

    move-result-wide v1

    double-to-float v1, v1

    invoke-direct {v0, v1, v4, v3, v3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;-><init>(FIII)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->mExerciseGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    goto :goto_1
.end method

.method private initInputModules(Landroid/view/View;)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const v5, 0x7f0d005a

    const v4, 0x7f0900b9

    .line 148
    const v0, 0x7f080c7d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->mIntakeCalorieInput:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    .line 149
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->mIntakeCalorieInput:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0056

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d0058

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;->setInputRange(FF)V

    .line 151
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->mIntakeCalorieInput:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;->setMoveDistance(F)V

    .line 152
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->mIntakeCalorieInput:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090953

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;->setTitle(Ljava/lang/CharSequence;)V

    .line 153
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->mIntakeCalorieInput:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->mMealGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->getValue()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;->setValue(F)V

    .line 154
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->mIntakeCalorieInput:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;->setUnit(Ljava/lang/CharSequence;)V

    .line 155
    const v0, 0x7f080c7e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->mBurnCalorieInput:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    .line 156
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->mBurnCalorieInput:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0057

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d0059

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;->setInputRange(FF)V

    .line 158
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->mBurnCalorieInput:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;->setMoveDistance(F)V

    .line 159
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->mBurnCalorieInput:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090954

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;->setTitle(Ljava/lang/CharSequence;)V

    .line 160
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->mBurnCalorieInput:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->mExerciseGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->getValue()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;->setValue(F)V

    .line 161
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->mBurnCalorieInput:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;->setUnit(Ljava/lang/CharSequence;)V

    .line 162
    return-void
.end method

.method private initTextViews(Landroid/view/View;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 235
    const v1, 0x7f080c7f

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 236
    .local v0, "goalCalorieRecommendDescription":Landroid/widget/TextView;
    const v1, 0x7f090950

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/NetCalorieUtils;->getNetCalorie()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 237
    return-void
.end method

.method private initUIComponents(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 142
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->initInputModules(Landroid/view/View;)V

    .line 143
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->initFooterButtons(Landroid/view/View;)V

    .line 144
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->initTextViews(Landroid/view/View;)V

    .line 145
    return-void
.end method

.method private insertGoal(Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;)V
    .locals 1
    .param p1, "data"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    .prologue
    .line 229
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->mGoalDao:Lcom/sec/android/app/shealth/common/commondao/goaldao/GoalDao;

    invoke-interface {v0, p1}, Lcom/sec/android/app/shealth/common/commondao/goaldao/GoalDao;->insertData(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;)J

    .line 230
    return-void
.end method

.method private isInputValid()Z
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->mIntakeCalorieInput:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;->isOutOfRange()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->mBurnCalorieInput:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;->isOutOfRange()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private saveData()V
    .locals 11

    .prologue
    .line 211
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 212
    .local v0, "createTime":J
    iget-object v7, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->mGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->getSetTime()J

    move-result-wide v5

    .line 213
    .local v5, "targetDate":J
    cmp-long v7, v0, v5

    if-lez v7, :cond_0

    .line 214
    move-wide v0, v5

    .line 216
    :cond_0
    sub-long v7, v5, v0

    const-wide/16 v9, 0x3e8

    div-long v2, v7, v9

    .line 217
    .local v2, "diff":J
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Long;->intValue()I

    move-result v4

    .line 218
    .local v4, "period":I
    const-string v7, "WeightSetGoalCaloriesFragment"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "create time: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", targetDate: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", diff: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", period: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 219
    iget-object v7, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->mGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    invoke-virtual {v7, v0, v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->setSetTime(J)V

    .line 220
    iget-object v7, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->mGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    invoke-virtual {v7, v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->setPeriod(I)V

    .line 221
    new-instance v7, Lcom/sec/android/app/shealth/common/commondao/goaldao/GoalDaoImplDb;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/sec/android/app/shealth/common/commondao/goaldao/GoalDaoImplDb;-><init>(Landroid/content/Context;)V

    iget-object v8, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->mGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    invoke-virtual {v7, v8}, Lcom/sec/android/app/shealth/common/commondao/goaldao/GoalDaoImplDb;->insertOrUpdateData(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;)J

    .line 222
    iget-object v7, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->mExerciseGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    iget-object v8, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->mBurnCalorieInput:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;->getValue()Ljava/lang/Float;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F

    move-result v8

    invoke-virtual {v7, v8}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->setValue(F)V

    .line 223
    iget-object v7, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->mExerciseGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    invoke-direct {p0, v7}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->insertGoal(Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;)V

    .line 224
    iget-object v7, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->mMealGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    iget-object v8, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->mIntakeCalorieInput:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;->getValue()Ljava/lang/Float;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F

    move-result v8

    invoke-virtual {v7, v8}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->setValue(F)V

    .line 225
    iget-object v7, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->mMealGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    invoke-direct {p0, v7}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->insertGoal(Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;)V

    .line 226
    return-void
.end method

.method private setFocusHandleListener(Landroid/view/View;)V
    .locals 2
    .param p1, "rootView"    # Landroid/view/View;

    .prologue
    .line 117
    const v0, 0x7f0805a6

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment$1;-><init>(Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 128
    return-void
.end method

.method private showAlertDialog()V
    .locals 8

    .prologue
    const v7, 0x1869f

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 254
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f0909bc

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const v2, 0x7f0909bd

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    const/4 v4, 0x2

    const/16 v5, 0xa

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 259
    .local v0, "outOfRangeText":Ljava/lang/String;
    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v6}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setType(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f090b60

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "OUT_OF_RANGE_DIALOG"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 263
    return-void
.end method

.method private showCancelDialog()V
    .locals 3

    .prologue
    .line 197
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090048

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090ccd

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment$4;-><init>(Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 208
    return-void
.end method


# virtual methods
.method protected customizeActionBar(Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;)V
    .locals 1
    .param p1, "actionBarHelper"    # Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    .prologue
    .line 137
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment;->customizeActionBar(Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;)V

    .line 138
    const v0, 0x7f090031

    invoke-virtual {p1, v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 139
    return-void
.end method

.method protected getFragmentType()Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment$GoalFragment;
    .locals 1

    .prologue
    .line 132
    sget-object v0, Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment$GoalFragment;->ADD_NEW_GOAL_CALORIES:Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment$GoalFragment;

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 76
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "goal_data_key"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    iput-object v1, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->mGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    .line 77
    new-instance v1, Lcom/sec/android/app/shealth/common/commondao/goaldao/GoalDaoImplDb;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/sec/android/app/shealth/common/commondao/goaldao/GoalDaoImplDb;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->mGoalDao:Lcom/sec/android/app/shealth/common/commondao/goaldao/GoalDao;

    .line 78
    const v1, 0x7f0302c4

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 79
    .local v0, "view":Landroid/view/View;
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->setFocusHandleListener(Landroid/view/View;)V

    .line 80
    invoke-direct {p0, p3}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->initGoalData(Landroid/os/Bundle;)V

    .line 82
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->initUIComponents(Landroid/view/View;)V

    .line 83
    return-object v0
.end method
