.class Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$DateChangeReceiver;
.super Landroid/content/BroadcastReceiver;
.source "FoodTrackerBaseFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DateChangeReceiver"
.end annotation


# instance fields
.field private lastTime:J

.field final synthetic this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;)V
    .locals 2

    .prologue
    .line 582
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$DateChangeReceiver;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 583
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$DateChangeReceiver;->lastTime:J

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$1;

    .prologue
    .line 582
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$DateChangeReceiver;-><init>(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 587
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 588
    .local v0, "action":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v4, "android.intent.action.TIME_TICK"

    invoke-virtual {v4, v0}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "android.intent.action.TIME_SET"

    invoke-virtual {v4, v0}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "android.intent.action.TIMEZONE_CHANGED"

    invoke-virtual {v4, v0}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 604
    :goto_0
    return-void

    .line 593
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    .line 594
    .local v1, "inTime":J
    new-instance v3, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Ljava/util/Date;-><init>(J)V

    .line 595
    .local v3, "newTimeDate":Ljava/util/Date;
    iget-wide v4, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$DateChangeReceiver;->lastTime:J

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v4

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v6

    cmp-long v4, v4, v6

    if-eqz v4, :cond_1

    .line 597
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$DateChangeReceiver;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    # invokes: Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;
    invoke-static {v4}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->access$300(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;)Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setSystemDate(Ljava/util/Date;)V

    .line 598
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$DateChangeReceiver;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    invoke-virtual {v4, v3}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->onDateChanged(Ljava/util/Date;)V

    .line 599
    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$DateChangeReceiver;->lastTime:J

    .line 602
    :cond_1
    # getter for: Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->access$400()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "TIME action = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", duration = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long/2addr v6, v1

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
