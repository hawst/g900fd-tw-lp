.class public Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProMapViewMapFragment;
.super Lcom/google/android/gms/maps/SupportMapFragment;
.source "ExerciseProMapViewMapFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProMapViewMapFragment$MapWrapper;
    }
.end annotation


# instance fields
.field mOriginalView:Landroid/view/View;

.field mapWrapper:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProMapViewMapFragment$MapWrapper;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/google/android/gms/maps/SupportMapFragment;-><init>()V

    .line 34
    return-void
.end method


# virtual methods
.method public getView()Landroid/view/View;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProMapViewMapFragment;->mOriginalView:Landroid/view/View;

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 22
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/maps/SupportMapFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProMapViewMapFragment;->mOriginalView:Landroid/view/View;

    .line 23
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProMapViewMapFragment$MapWrapper;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProMapViewMapFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProMapViewMapFragment$MapWrapper;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProMapViewMapFragment;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProMapViewMapFragment;->mapWrapper:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProMapViewMapFragment$MapWrapper;

    .line 24
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProMapViewMapFragment;->mapWrapper:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProMapViewMapFragment$MapWrapper;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProMapViewMapFragment;->mOriginalView:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProMapViewMapFragment$MapWrapper;->addView(Landroid/view/View;)V

    .line 25
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProMapViewMapFragment;->mapWrapper:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProMapViewMapFragment$MapWrapper;

    return-object v0
.end method
