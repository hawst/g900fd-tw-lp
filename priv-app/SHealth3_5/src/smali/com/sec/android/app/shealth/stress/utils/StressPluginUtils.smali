.class public Lcom/sec/android/app/shealth/stress/utils/StressPluginUtils;
.super Ljava/lang/Object;
.source "StressPluginUtils.java"


# static fields
.field private static StressPluginUtils:Lcom/sec/android/app/shealth/stress/utils/StressPluginUtils;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/shealth/stress/utils/StressPluginUtils;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/sec/android/app/shealth/stress/utils/StressPluginUtils;->StressPluginUtils:Lcom/sec/android/app/shealth/stress/utils/StressPluginUtils;

    if-nez v0, :cond_0

    .line 16
    new-instance v0, Lcom/sec/android/app/shealth/stress/utils/StressPluginUtils;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/stress/utils/StressPluginUtils;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/stress/utils/StressPluginUtils;->StressPluginUtils:Lcom/sec/android/app/shealth/stress/utils/StressPluginUtils;

    .line 18
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/stress/utils/StressPluginUtils;->StressPluginUtils:Lcom/sec/android/app/shealth/stress/utils/StressPluginUtils;

    return-object v0
.end method


# virtual methods
.method public getLatestDataTimestamp(Ljava/lang/String;J)J
    .locals 9
    .param p1, "format"    # Ljava/lang/String;
    .param p2, "dateTime"    # J

    .prologue
    .line 30
    const-wide/16 v7, 0x0

    .line 31
    .local v7, "obtainedTime":J
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "select sample_time from stress where strftime(\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\",("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "sample_time"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/1000),\'unixepoch\', \'localtime\') = strftime(\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\",("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/1000),\'unixepoch\', \'localtime\') order by "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "sample_time"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " desc limit 1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 34
    .local v3, "query":Ljava/lang/String;
    const/4 v6, 0x0

    .line 37
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 38
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 39
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 40
    const-string/jumbo v0, "sample_time"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v7

    .line 45
    :cond_0
    if-eqz v6, :cond_1

    .line 46
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 48
    :cond_1
    return-wide v7

    .line 45
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 46
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method
