.class public Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private mBtnBackground:I

.field private mContentDesc:I

.field private mHoverTextResId:I

.field private mISOKCancelType:Z

.field private mIconRes:Landroid/graphics/drawable/Drawable;

.field private mIconResId:I

.field private mTextResId:I

.field private mlistener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(ILandroid/view/View$OnClickListener;)V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->mIconResId:I

    iput-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->mIconRes:Landroid/graphics/drawable/Drawable;

    iput v0, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->mTextResId:I

    iput v0, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->mHoverTextResId:I

    iput v0, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->mBtnBackground:I

    iput-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->mlistener:Landroid/view/View$OnClickListener;

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->mISOKCancelType:Z

    iput v0, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->mContentDesc:I

    iput p1, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->mIconResId:I

    iput-object p2, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->mlistener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method public constructor <init>(Landroid/view/View$OnClickListener;I)V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->mIconResId:I

    iput-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->mIconRes:Landroid/graphics/drawable/Drawable;

    iput v0, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->mTextResId:I

    iput v0, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->mHoverTextResId:I

    iput v0, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->mBtnBackground:I

    iput-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->mlistener:Landroid/view/View$OnClickListener;

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->mISOKCancelType:Z

    iput v0, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->mContentDesc:I

    iput p2, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->mTextResId:I

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->mlistener:Landroid/view/View$OnClickListener;

    return-void
.end method


# virtual methods
.method public create()Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;
    .locals 2

    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>()V

    iget v1, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->mHoverTextResId:I

    iput v1, v0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->mHoverTextResId:I

    iget v1, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->mIconResId:I

    iput v1, v0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->mIconResId:I

    iget v1, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->mTextResId:I

    iput v1, v0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->mTextResId:I

    iget v1, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->mBtnBackground:I

    iput v1, v0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->mBtnBackground:I

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->mISOKCancelType:Z

    iput-boolean v1, v0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->mISOKCancelType:Z

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->mlistener:Landroid/view/View$OnClickListener;

    iput-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->mlistener:Landroid/view/View$OnClickListener;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->mIconRes:Landroid/graphics/drawable/Drawable;

    iput-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->mIconRes:Landroid/graphics/drawable/Drawable;

    iget v1, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->mContentDesc:I

    iput v1, v0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->mContentDesc:I

    return-object v0
.end method

.method public setBackgroundResID(I)Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;
    .locals 0

    iput p1, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->mBtnBackground:I

    return-object p0
.end method

.method public setContentDescription(I)Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;
    .locals 0

    iput p1, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->mContentDesc:I

    return-object p0
.end method

.method public setHoverTextResID(I)Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;
    .locals 0

    iput p1, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->mHoverTextResId:I

    return-object p0
.end method

.method public setISOKCancelType(Z)Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;
    .locals 0

    iput-boolean p1, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->mISOKCancelType:Z

    return-object p0
.end method

.method public setIconResDrawable(Landroid/graphics/drawable/Drawable;)Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->mIconRes:Landroid/graphics/drawable/Drawable;

    return-object p0
.end method

.method public setTextResID(I)Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;
    .locals 0

    iput p1, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->mTextResId:I

    return-object p0
.end method
