.class Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$14;
.super Ljava/lang/Object;
.source "ExerciseProActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->resetAllDataConfirm()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;)V
    .locals 0

    .prologue
    .line 1448
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$14;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onOKButtonClick(Landroid/app/Activity;Landroid/view/View;Landroid/app/Dialog;)V
    .locals 9
    .param p1, "parentActivity"    # Landroid/app/Activity;
    .param p2, "content"    # Landroid/view/View;
    .param p3, "dialog"    # Landroid/app/Dialog;

    .prologue
    const-wide/16 v7, 0x1

    .line 1453
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseUtilsEx;->deleteExerciseDataForDay(Landroid/content/Context;J)I

    .line 1455
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$14;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f09094a

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 1457
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$14;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    iget-object v2, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mStatusFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    if-eqz v2, :cond_0

    .line 1458
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$14;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    iget-object v2, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mStatusFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->onResume()V

    .line 1459
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$14;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    iget-object v2, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mAmapStatusFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    if-eqz v2, :cond_1

    .line 1460
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$14;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    iget-object v2, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mAmapStatusFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->onResume()V

    .line 1462
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$14;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    iget v2, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mState:I

    const/16 v3, 0x3e9

    if-ne v2, v3, :cond_2

    .line 1463
    new-instance v1, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;-><init>(Landroid/content/Context;)V

    .line 1464
    .local v1, "mergeCal":Lcom/sec/android/app/shealth/common/utils/MergeCalorie;
    sget-boolean v2, Lcom/sec/android/app/shealth/plugins/exercisepro/constants/ConfigurationConstants;->CHINA_AMAP_LOCATION_ON:Z

    if-eqz v2, :cond_3

    .line 1465
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$14;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    iget-object v2, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mAmapStatusFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    if-eqz v2, :cond_2

    .line 1466
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$14;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    iget-object v2, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mAmapStatusFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-static {v5, v6}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v5

    add-long/2addr v5, v7

    invoke-virtual {v1, v3, v4, v5, v6}, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;->getMergedCalorieGetEarlier(JJ)F

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->updateKcalDataLayout(F)V

    .line 1482
    .end local v1    # "mergeCal":Lcom/sec/android/app/shealth/common/utils/MergeCalorie;
    :cond_2
    :goto_0
    return-void

    .line 1471
    .restart local v1    # "mergeCal":Lcom/sec/android/app/shealth/common/utils/MergeCalorie;
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$14;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    iget-object v2, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mStatusFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    if-eqz v2, :cond_2

    .line 1472
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$14;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    iget-object v2, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mStatusFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-static {v5, v6}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v5

    add-long/2addr v5, v7

    invoke-virtual {v1, v3, v4, v5, v6}, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;->getMergedCalorieGetEarlier(JJ)F

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->updateKcalDataLayout(F)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1479
    .end local v1    # "mergeCal":Lcom/sec/android/app/shealth/common/utils/MergeCalorie;
    :catch_0
    move-exception v0

    .line 1480
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
