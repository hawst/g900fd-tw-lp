.class Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity$2;
.super Ljava/lang/Object;
.source "FoodPickActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;->customizeActionBar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;)V
    .locals 0

    .prologue
    .line 130
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity$2;->this$0:Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 134
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity$2;->this$0:Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;->access$000(Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;)Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/shealth/food/app/UserActionLog;->DONE_PICK_FOOD_BY_ACTION_BAR_BUTTON:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;->logUserAction(Lcom/sec/android/app/shealth/food/app/UserActionLog;)V

    .line 135
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity$2;->this$0:Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;->mOnDoneClickListener:Lcom/sec/android/app/shealth/food/foodpick/search/ActionBarDoneCancelAccessor$OnActionBarClick;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;->access$200(Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;)Lcom/sec/android/app/shealth/food/foodpick/search/ActionBarDoneCancelAccessor$OnActionBarClick;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 136
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity$2;->this$0:Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;->mOnDoneClickListener:Lcom/sec/android/app/shealth/food/foodpick/search/ActionBarDoneCancelAccessor$OnActionBarClick;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;->access$200(Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;)Lcom/sec/android/app/shealth/food/foodpick/search/ActionBarDoneCancelAccessor$OnActionBarClick;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/shealth/food/foodpick/search/ActionBarDoneCancelAccessor$OnActionBarClick;->onClick()V

    .line 140
    :goto_0
    return-void

    .line 138
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity$2;->this$0:Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;->finish()V

    goto :goto_0
.end method
