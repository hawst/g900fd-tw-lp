.class Lcom/sec/android/app/shealth/uv/UvSummaryFragment$13;
.super Landroid/os/CountDownTimer;
.source "UvSummaryFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/uv/UvSummaryFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;JJ)V
    .locals 0
    .param p2, "x0"    # J
    .param p4, "x1"    # J

    .prologue
    .line 1467
    iput-object p1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$13;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    invoke-direct {p0, p2, p3, p4, p5}, Landroid/os/CountDownTimer;-><init>(JJ)V

    return-void
.end method


# virtual methods
.method public onFinish()V
    .locals 2

    .prologue
    .line 1475
    sget-object v0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->TAG:Ljava/lang/String;

    const-string v1, " no sensor data returned, mMeasuringTimer crossed 10 seconds"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1477
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$13;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->showFailurePopup()V

    .line 1479
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$13;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->stopSensor()V

    .line 1480
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$13;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$13;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->getStateReady()Lcom/sec/android/app/shealth/uv/state/UvState;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->setState(Lcom/sec/android/app/shealth/uv/state/UvState;)V

    .line 1481
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$13;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->getStateReady()Lcom/sec/android/app/shealth/uv/state/UvState;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/uv/state/UvState;->updateViewWithAnimation(Z)V

    .line 1482
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$13;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iget-object v0, v0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mMeasuringTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 1484
    return-void
.end method

.method public onTick(J)V
    .locals 0
    .param p1, "millisUntilFinished"    # J

    .prologue
    .line 1471
    return-void
.end method
