.class public Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;
.super Landroid/view/View;
.source "HoloCircleSeekBar.java"


# static fields
.field private static final GOAL_COLOR:I = -0x8200

.field private static final OUTER_GREY:I = -0x171718

.field private static final SHADE_COLOR_ONE:I = -0xa260fa

.field private static final SHADE_COLOR_TWO:I = -0xff5d95

.field private static final STATE_PARENT:Ljava/lang/String; = "parent"

.field private static final STATE_PREVIOUS_ANI_VAL:Ljava/lang/String; = "mCurrentAniValue"

.field private static final STATE_RADIAN:Ljava/lang/String; = "radian"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private CIRCLE_DEFAULT:I

.field private OFFSET7:F

.field private OFFSET7_1:F

.field private OFFSET7_2:F

.field private isTimerTaskRunning:Z

.field private final mAnimationIntervalTime:I

.field private mArcFinishAngle:I

.field private mBackupValue:I

.field private mCirclePaintBackground:Landroid/graphics/Paint;

.field private mCirclePaintDefaults:Landroid/graphics/Paint;

.field private mCirclePaintGradient:Landroid/graphics/Paint;

.field private mContext:Landroid/content/Context;

.field private mCurrentAniValue:I

.field private mCurrentValue:I

.field private mGoalColor:I

.field private mInnerCircleRectangle:Landroid/graphics/RectF;

.field private mIsGoalExceeded:Z

.field private mIsGradient:Z

.field private mMax:I

.field private mOuterCircleRectangle:Landroid/graphics/RectF;

.field private mRadian:F

.field private mTimerTask:Ljava/util/TimerTask;

.field private mTranslationOffset:F

.field private mViewWidth:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    const/high16 v2, 0x40e00000    # 7.0f

    .line 92
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 37
    const v0, -0x8c46f1

    iput v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->CIRCLE_DEFAULT:I

    .line 49
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mIsGradient:Z

    .line 51
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->convertDpToPx(Landroid/content/Context;F)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->OFFSET7:F

    .line 52
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    const/high16 v1, 0x40800000    # 4.0f

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->convertDpToPx(Landroid/content/Context;F)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->OFFSET7_1:F

    .line 53
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->convertDpToPx(Landroid/content/Context;F)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->OFFSET7_2:F

    .line 59
    const/16 v0, 0xf

    iput v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mAnimationIntervalTime:I

    .line 66
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mMax:I

    .line 68
    const/16 v0, 0x168

    iput v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mArcFinishAngle:I

    .line 93
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mContext:Landroid/content/Context;

    .line 94
    const/4 v0, 0x0

    invoke-direct {p0, v0, v3}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->init(Landroid/util/AttributeSet;I)V

    .line 95
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v3, 0x0

    const/high16 v2, 0x40e00000    # 7.0f

    .line 98
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    const v0, -0x8c46f1

    iput v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->CIRCLE_DEFAULT:I

    .line 49
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mIsGradient:Z

    .line 51
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->convertDpToPx(Landroid/content/Context;F)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->OFFSET7:F

    .line 52
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    const/high16 v1, 0x40800000    # 4.0f

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->convertDpToPx(Landroid/content/Context;F)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->OFFSET7_1:F

    .line 53
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->convertDpToPx(Landroid/content/Context;F)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->OFFSET7_2:F

    .line 59
    const/16 v0, 0xf

    iput v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mAnimationIntervalTime:I

    .line 66
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mMax:I

    .line 68
    const/16 v0, 0x168

    iput v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mArcFinishAngle:I

    .line 99
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mContext:Landroid/content/Context;

    .line 100
    invoke-direct {p0, p2, v3}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->init(Landroid/util/AttributeSet;I)V

    .line 101
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/high16 v2, 0x40e00000    # 7.0f

    .line 104
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 37
    const v0, -0x8c46f1

    iput v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->CIRCLE_DEFAULT:I

    .line 49
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mIsGradient:Z

    .line 51
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->convertDpToPx(Landroid/content/Context;F)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->OFFSET7:F

    .line 52
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    const/high16 v1, 0x40800000    # 4.0f

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->convertDpToPx(Landroid/content/Context;F)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->OFFSET7_1:F

    .line 53
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->convertDpToPx(Landroid/content/Context;F)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->OFFSET7_2:F

    .line 59
    const/16 v0, 0xf

    iput v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mAnimationIntervalTime:I

    .line 66
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mMax:I

    .line 68
    const/16 v0, 0x168

    iput v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mArcFinishAngle:I

    .line 105
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mContext:Landroid/content/Context;

    .line 106
    invoke-direct {p0, p2, p3}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->init(Landroid/util/AttributeSet;I)V

    .line 107
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->doUiChanged()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->isTimerTaskRunning:Z

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;

    .prologue
    .line 26
    iget v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mCurrentAniValue:I

    return v0
.end method

.method static synthetic access$212(Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;I)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;
    .param p1, "x1"    # I

    .prologue
    .line 26
    iget v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mCurrentAniValue:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mCurrentAniValue:I

    return v0
.end method

.method static synthetic access$220(Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;I)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;
    .param p1, "x1"    # I

    .prologue
    .line 26
    iget v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mCurrentAniValue:I

    sub-int/2addr v0, p1

    iput v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mCurrentAniValue:I

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;

    .prologue
    .line 26
    iget v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mCurrentValue:I

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;

    .prologue
    .line 26
    iget v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mArcFinishAngle:I

    return v0
.end method

.method static synthetic access$402(Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;
    .param p1, "x1"    # I

    .prologue
    .line 26
    iput p1, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mArcFinishAngle:I

    return p1
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;

    .prologue
    .line 26
    iget v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mMax:I

    return v0
.end method

.method static synthetic access$602(Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;
    .param p1, "x1"    # F

    .prologue
    .line 26
    iput p1, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mRadian:F

    return p1
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;I)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;
    .param p1, "x1"    # I

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->calculateRadiansFromAngle(I)F

    move-result v0

    return v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;

    .prologue
    .line 26
    iget v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mBackupValue:I

    return v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->stopTask()V

    return-void
.end method

.method private applyOffset(FF)V
    .locals 7
    .param p1, "viewWidth"    # F
    .param p2, "viewHeight"    # F

    .prologue
    const/4 v6, 0x1

    const/high16 v5, 0x40000000    # 2.0f

    .line 184
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mInnerCircleRectangle:Landroid/graphics/RectF;

    .line 185
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mInnerCircleRectangle:Landroid/graphics/RectF;

    iget v1, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->OFFSET7:F

    mul-float/2addr v1, v5

    iget v2, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->OFFSET7:F

    mul-float/2addr v2, v5

    iget v3, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->OFFSET7:F

    mul-float/2addr v3, v5

    sub-float v3, p1, v3

    iget v4, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->OFFSET7:F

    mul-float/2addr v4, v5

    sub-float v4, p2, v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 186
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mOuterCircleRectangle:Landroid/graphics/RectF;

    .line 187
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mOuterCircleRectangle:Landroid/graphics/RectF;

    iget v1, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->OFFSET7:F

    iget v2, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->OFFSET7:F

    iget v3, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->OFFSET7:F

    sub-float v3, p1, v3

    iget v4, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->OFFSET7:F

    sub-float v4, p2, v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 189
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v6}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mCirclePaintBackground:Landroid/graphics/Paint;

    .line 190
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mCirclePaintBackground:Landroid/graphics/Paint;

    const v1, -0x171718

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 191
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mCirclePaintBackground:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 192
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mCirclePaintBackground:Landroid/graphics/Paint;

    iget v1, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->OFFSET7:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 194
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v6}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mCirclePaintDefaults:Landroid/graphics/Paint;

    .line 195
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mCirclePaintDefaults:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 196
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mCirclePaintDefaults:Landroid/graphics/Paint;

    iget v1, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->OFFSET7:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 197
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mCirclePaintDefaults:Landroid/graphics/Paint;

    iget v1, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->CIRCLE_DEFAULT:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 199
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v6}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mCirclePaintGradient:Landroid/graphics/Paint;

    .line 200
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mCirclePaintGradient:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 201
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mCirclePaintGradient:Landroid/graphics/Paint;

    iget v1, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->OFFSET7:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 202
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mCirclePaintGradient:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/SweepGradient;

    div-float v2, p1, v5

    div-float v3, p2, v5

    const v4, -0xa260fa

    const v5, -0xff5d95

    invoke-direct {v1, v2, v3, v4, v5}, Landroid/graphics/SweepGradient;-><init>(FFII)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 204
    return-void
.end method

.method private calculateFinishAngle(F)I
    .locals 7
    .param p1, "angle"    # F

    .prologue
    const/4 v6, 0x0

    .line 227
    const/4 v0, 0x0

    .line 228
    .local v0, "radians":I
    cmpl-float v2, p1, v6

    if-eqz v2, :cond_1

    .line 229
    float-to-double v2, p1

    const-wide v4, 0x401921fb54442d18L    # 6.283185307179586

    div-double/2addr v2, v4

    double-to-float v1, v2

    .line 230
    .local v1, "unit":F
    cmpg-float v2, v1, v6

    if-gez v2, :cond_0

    .line 231
    const/high16 v2, 0x3f800000    # 1.0f

    add-float/2addr v1, v2

    .line 233
    :cond_0
    const/high16 v2, 0x43b40000    # 360.0f

    mul-float/2addr v2, v1

    const/high16 v3, 0x43870000    # 270.0f

    sub-float/2addr v2, v3

    float-to-int v0, v2

    .line 234
    if-gez v0, :cond_1

    .line 235
    add-int/lit16 v0, v0, 0x168

    .line 237
    .end local v1    # "unit":F
    :cond_1
    return v0
.end method

.method private calculateRadiansFromAngle(I)F
    .locals 4
    .param p1, "radians"    # I

    .prologue
    .line 241
    add-int/lit16 v0, p1, 0x10e

    int-to-double v0, v0

    const-wide v2, 0x401921fb54442d18L    # 6.283185307179586

    mul-double/2addr v0, v2

    const-wide v2, 0x4076800000000000L    # 360.0

    div-double/2addr v0, v2

    double-to-float v0, v0

    return v0
.end method

.method public static convertDpToPx(Landroid/content/Context;F)F
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "dp"    # F

    .prologue
    .line 85
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 86
    .local v0, "displayMetrics":Landroid/util/DisplayMetrics;
    const/4 v1, 0x1

    invoke-static {v1, p1, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    return v1
.end method

.method private doTimerTask()V
    .locals 1

    .prologue
    .line 333
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->doTimerTask(Z)V

    .line 334
    return-void
.end method

.method private doTimerTask(Z)V
    .locals 6
    .param p1, "forceAnimate"    # Z

    .prologue
    .line 338
    if-eqz p1, :cond_0

    .line 339
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mCurrentAniValue:I

    .line 344
    :cond_0
    iget v1, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mCurrentAniValue:I

    iget v2, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mCurrentValue:I

    if-ne v1, v2, :cond_1

    iget v1, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mCurrentValue:I

    iget v2, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mMax:I

    if-ne v1, v2, :cond_3

    .line 345
    :cond_1
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    .line 347
    .local v0, "t":Ljava/util/Timer;
    const/4 v1, -0x1

    iput v1, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mBackupValue:I

    .line 348
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->isTimerTaskRunning:Z

    .line 349
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mTimerTask:Ljava/util/TimerTask;

    if-eqz v1, :cond_2

    .line 350
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mTimerTask:Ljava/util/TimerTask;

    invoke-virtual {v1}, Ljava/util/TimerTask;->cancel()Z

    .line 351
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mTimerTask:Ljava/util/TimerTask;

    .line 353
    :cond_2
    new-instance v1, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar$1;-><init>(Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mTimerTask:Ljava/util/TimerTask;

    .line 359
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mTimerTask:Ljava/util/TimerTask;

    const-wide/16 v2, 0x64

    const-wide/16 v4, 0xf

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 361
    .end local v0    # "t":Ljava/util/Timer;
    :cond_3
    return-void
.end method

.method private doUiChanged()V
    .locals 2

    .prologue
    .line 365
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    new-instance v1, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar$2;-><init>(Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 392
    return-void
.end method

.method private init(Landroid/util/AttributeSet;I)V
    .locals 5
    .param p1, "attrs"    # Landroid/util/AttributeSet;
    .param p2, "defStyle"    # I

    .prologue
    const/4 v4, 0x0

    .line 161
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/shealth/R$styleable;->workout_HoloCircleSeekBar:[I

    invoke-virtual {v1, p1, v2, p2, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 164
    .local v0, "typedArray":Landroid/content/res/TypedArray;
    const/16 v1, 0x1d

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a051c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mViewWidth:F

    .line 166
    const/16 v1, 0x1e

    const-string v2, "#ff7e00"

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mGoalColor:I

    .line 168
    sget-object v1, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "w : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->getWidth()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", h : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->getHeight()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 170
    iget v1, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mViewWidth:F

    iget v2, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mViewWidth:F

    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->applyOffset(FF)V

    .line 171
    iput v4, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mArcFinishAngle:I

    .line 172
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mRadian:F

    .line 175
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 177
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->invalidate()V

    .line 180
    return-void
.end method

.method private stopTask()V
    .locals 1

    .prologue
    .line 395
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->isTimerTaskRunning:Z

    .line 396
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mTimerTask:Ljava/util/TimerTask;

    if-eqz v0, :cond_0

    .line 397
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mTimerTask:Ljava/util/TimerTask;

    invoke-virtual {v0}, Ljava/util/TimerTask;->cancel()Z

    .line 398
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mTimerTask:Ljava/util/TimerTask;

    .line 400
    :cond_0
    return-void
.end method


# virtual methods
.method public animateProgress()V
    .locals 1

    .prologue
    .line 292
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->doTimerTask(Z)V

    .line 293
    return-void
.end method

.method public changeCircleWidth(Z)V
    .locals 2
    .param p1, "makeItThin"    # Z

    .prologue
    .line 117
    if-eqz p1, :cond_0

    iget v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->OFFSET7_1:F

    :goto_0
    iput v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->OFFSET7:F

    .line 120
    iget v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mViewWidth:F

    iget v1, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mViewWidth:F

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->applyOffset(FF)V

    .line 122
    return-void

    .line 117
    :cond_0
    iget v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->OFFSET7_2:F

    goto :goto_0
.end method

.method public getProgress()I
    .locals 1

    .prologue
    .line 285
    iget v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mCurrentValue:I

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 7
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/16 v6, 0x168

    const/4 v4, 0x0

    const/high16 v3, 0x43b40000    # 360.0f

    const/4 v2, 0x0

    .line 212
    iget v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mTranslationOffset:F

    iget v1, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mTranslationOffset:F

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 215
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mOuterCircleRectangle:Landroid/graphics/RectF;

    iget-object v5, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mCirclePaintBackground:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 218
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mIsGoalExceeded:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mArcFinishAngle:I

    if-lt v0, v6, :cond_1

    .line 219
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mCirclePaintDefaults:Landroid/graphics/Paint;

    iget v1, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mGoalColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 223
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mOuterCircleRectangle:Landroid/graphics/RectF;

    iget v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mArcFinishAngle:I

    if-ge v0, v6, :cond_0

    iget v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mArcFinishAngle:I

    int-to-float v3, v0

    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mIsGradient:Z

    if-eqz v0, :cond_2

    iget-object v5, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mCirclePaintGradient:Landroid/graphics/Paint;

    :goto_1
    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 224
    return-void

    .line 221
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mCirclePaintDefaults:Landroid/graphics/Paint;

    iget v1, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->CIRCLE_DEFAULT:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_0

    .line 223
    :cond_2
    iget-object v5, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mCirclePaintDefaults:Landroid/graphics/Paint;

    goto :goto_1
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 4
    .param p1, "state"    # Landroid/os/Parcelable;

    .prologue
    .line 317
    move-object v1, p1

    check-cast v1, Landroid/os/Bundle;

    .line 319
    .local v1, "savedState":Landroid/os/Bundle;
    const-string/jumbo v3, "parent"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    .line 322
    .local v2, "superState":Landroid/os/Parcelable;
    const-string/jumbo v3, "radian"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v3

    iput v3, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mRadian:F

    .line 323
    iget v3, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mRadian:F

    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->calculateFinishAngle(F)I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mArcFinishAngle:I

    .line 325
    const-string v3, "mCurrentAniValue"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 326
    .local v0, "lastAniVal":Ljava/lang/String;
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mCurrentAniValue:I

    .line 328
    invoke-super {p0, v2}, Landroid/view/View;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 329
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 5

    .prologue
    .line 303
    invoke-super {p0}, Landroid/view/View;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    .line 305
    .local v2, "superState":Landroid/os/Parcelable;
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 306
    .local v1, "state":Landroid/os/Bundle;
    const-string/jumbo v3, "parent"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 307
    const-string/jumbo v3, "radian"

    iget v4, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mRadian:F

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 309
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget v4, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mCurrentAniValue:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 310
    .local v0, "lastAniVal":Ljava/lang/String;
    const-string v3, "mCurrentAniValue"

    invoke-virtual {v1, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 312
    return-object v1
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 298
    const/4 v0, 0x0

    return v0
.end method

.method public setColor(I)V
    .locals 2
    .param p1, "color"    # I

    .prologue
    .line 130
    iput p1, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->CIRCLE_DEFAULT:I

    .line 131
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mCirclePaintDefaults:Landroid/graphics/Paint;

    iget v1, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->CIRCLE_DEFAULT:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 132
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->invalidate()V

    .line 133
    return-void
.end method

.method public setGoalAchieved(Z)V
    .locals 2
    .param p1, "isGoalAchieved"    # Z

    .prologue
    .line 154
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mCirclePaintDefaults:Landroid/graphics/Paint;

    if-eqz p1, :cond_0

    iget v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mGoalColor:I

    :goto_0
    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 155
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->invalidate()V

    .line 157
    return-void

    .line 154
    :cond_0
    iget v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->CIRCLE_DEFAULT:I

    goto :goto_0
.end method

.method public setGoalColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 139
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 140
    const v0, -0x8200

    iput v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mGoalColor:I

    .line 144
    :goto_0
    return-void

    .line 142
    :cond_0
    iput p1, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mGoalColor:I

    goto :goto_0
.end method

.method public setGradient(Z)V
    .locals 0
    .param p1, "setValue"    # Z

    .prologue
    .line 413
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mIsGradient:Z

    .line 414
    return-void
.end method

.method public setProgress(I)V
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 250
    const/16 v0, 0x6e

    if-le p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mIsGoalExceeded:Z

    .line 251
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->isTimerTaskRunning:Z

    if-eqz v0, :cond_1

    .line 252
    iput p1, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mBackupValue:I

    .line 258
    :goto_1
    return-void

    .line 250
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 255
    :cond_1
    int-to-float v0, p1

    iget v1, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mMax:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    const/high16 v1, 0x43b40000    # 360.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mArcFinishAngle:I

    .line 256
    iget v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mArcFinishAngle:I

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->calculateRadiansFromAngle(I)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mRadian:F

    .line 257
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->invalidate()V

    goto :goto_1
.end method

.method public setProgressWithAnimation(F)V
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 266
    const/high16 v0, 0x42dc0000    # 110.0f

    cmpl-float v0, p1, v0

    if-lez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mIsGoalExceeded:Z

    .line 267
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->isTimerTaskRunning:Z

    if-eqz v0, :cond_0

    .line 268
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->stopTask()V

    .line 270
    :cond_0
    float-to-int v0, p1

    iput v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mCurrentValue:I

    .line 272
    iget v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mCurrentValue:I

    iget v1, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mMax:I

    if-le v0, v1, :cond_1

    .line 273
    iget v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mMax:I

    iput v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mCurrentValue:I

    .line 275
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->doTimerTask()V

    .line 276
    return-void

    .line 266
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
