.class abstract Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;
.super Landroid/widget/FrameLayout;
.source "InputModule.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$OnTextChangedListener;,
        Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$TYPE;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "Input Module"

.field private static final transparentDrawable:Landroid/graphics/drawable/ColorDrawable;


# instance fields
.field LastVaule:I

.field private actualMinValue:I

.field private backupString:Ljava/lang/String;

.field private filterText:Z

.field private isOccupiedControllerTab:Z

.field private mContext:Landroid/content/Context;

.field private mController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView;

.field private mCurrentScaleValue:I

.field private mCurrentValue:I

.field private mDecButton:Landroid/widget/ImageButton;

.field protected mDummyEdit:Landroid/widget/EditText;

.field private mIncButton:Landroid/widget/ImageButton;

.field private mIncDecTouchListener:Landroid/view/View$OnTouchListener;

.field private mInputFilters:[Landroid/text/InputFilter;

.field private mInputModuleTextWatcher:Landroid/text/TextWatcher;

.field private mKeyListener:Landroid/view/View$OnKeyListener;

.field private mMaxInputRange:I

.field private mMaxNormalRange:I

.field private mMaxValueLength:I

.field private mMinInputRange:I

.field private mMinNormalRange:I

.field private mMoveDistance:I

.field private final mRegularExpression:Landroid/text/InputFilter;

.field private mSystemNumberSeparator:Ljava/lang/String;

.field private mTextChangedListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$OnTextChangedListener;

.field private mUnitTextView:Landroid/widget/TextView;

.field protected mValueEditText:Landroid/widget/EditText;

.field private needCursorLastPosition:Z

.field private type:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 62
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->transparentDrawable:Landroid/graphics/drawable/ColorDrawable;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 124
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 63
    new-instance v0, Ljava/text/DecimalFormat;

    invoke-direct {v0}, Ljava/text/DecimalFormat;-><init>()V

    invoke-virtual {v0}, Ljava/text/DecimalFormat;->getDecimalFormatSymbols()Ljava/text/DecimalFormatSymbols;

    move-result-object v0

    invoke-virtual {v0}, Ljava/text/DecimalFormatSymbols;->getDecimalSeparator()C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mSystemNumberSeparator:Ljava/lang/String;

    .line 78
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->needCursorLastPosition:Z

    .line 79
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->isOccupiedControllerTab:Z

    .line 81
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->filterText:Z

    .line 405
    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->LastVaule:I

    .line 495
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$6;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mRegularExpression:Landroid/text/InputFilter;

    .line 571
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$7;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mInputModuleTextWatcher:Landroid/text/TextWatcher;

    .line 763
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$8;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$8;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mKeyListener:Landroid/view/View$OnKeyListener;

    .line 778
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$9;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$9;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mIncDecTouchListener:Landroid/view/View$OnTouchListener;

    .line 125
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mContext:Landroid/content/Context;

    .line 126
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->initialize(Landroid/content/Context;)V

    .line 127
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    .line 130
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 63
    new-instance v0, Ljava/text/DecimalFormat;

    invoke-direct {v0}, Ljava/text/DecimalFormat;-><init>()V

    invoke-virtual {v0}, Ljava/text/DecimalFormat;->getDecimalFormatSymbols()Ljava/text/DecimalFormatSymbols;

    move-result-object v0

    invoke-virtual {v0}, Ljava/text/DecimalFormatSymbols;->getDecimalSeparator()C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mSystemNumberSeparator:Ljava/lang/String;

    .line 78
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->needCursorLastPosition:Z

    .line 79
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->isOccupiedControllerTab:Z

    .line 81
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->filterText:Z

    .line 405
    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->LastVaule:I

    .line 495
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$6;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mRegularExpression:Landroid/text/InputFilter;

    .line 571
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$7;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mInputModuleTextWatcher:Landroid/text/TextWatcher;

    .line 763
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$8;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$8;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mKeyListener:Landroid/view/View$OnKeyListener;

    .line 778
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$9;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$9;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mIncDecTouchListener:Landroid/view/View$OnTouchListener;

    .line 131
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mContext:Landroid/content/Context;

    .line 132
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->initialize(Landroid/content/Context;)V

    .line 133
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v1, 0x0

    .line 136
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 63
    new-instance v0, Ljava/text/DecimalFormat;

    invoke-direct {v0}, Ljava/text/DecimalFormat;-><init>()V

    invoke-virtual {v0}, Ljava/text/DecimalFormat;->getDecimalFormatSymbols()Ljava/text/DecimalFormatSymbols;

    move-result-object v0

    invoke-virtual {v0}, Ljava/text/DecimalFormatSymbols;->getDecimalSeparator()C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mSystemNumberSeparator:Ljava/lang/String;

    .line 78
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->needCursorLastPosition:Z

    .line 79
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->isOccupiedControllerTab:Z

    .line 81
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->filterText:Z

    .line 405
    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->LastVaule:I

    .line 495
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$6;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mRegularExpression:Landroid/text/InputFilter;

    .line 571
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$7;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mInputModuleTextWatcher:Landroid/text/TextWatcher;

    .line 763
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$8;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$8;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mKeyListener:Landroid/view/View$OnKeyListener;

    .line 778
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$9;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$9;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mIncDecTouchListener:Landroid/view/View$OnTouchListener;

    .line 137
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mContext:Landroid/content/Context;

    .line 138
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->initialize(Landroid/content/Context;)V

    .line 139
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    .prologue
    .line 55
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->isOccupiedControllerTab:Z

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    .prologue
    .line 55
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mMaxInputRange:I

    return v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;IZZ)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;
    .param p1, "x1"    # I
    .param p2, "x2"    # Z
    .param p3, "x3"    # Z

    .prologue
    .line 55
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->setNewValue(IZZ)V

    return-void
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    .prologue
    .line 55
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mMinInputRange:I

    return v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$OnTextChangedListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mTextChangedListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$OnTextChangedListener;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->backupString:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1302(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->backupString:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1400(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->isNumeric(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1502(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;
    .param p1, "x1"    # I

    .prologue
    .line 55
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mCurrentValue:I

    return p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    .prologue
    .line 55
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->filterText:Z

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    .prologue
    .line 55
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->actualMinValue:I

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    .prologue
    .line 55
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->type:I

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->isDecimalNumber()Z

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;IZ)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;
    .param p1, "x1"    # I
    .param p2, "x2"    # Z

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->setNewValue(IZ)V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mSystemNumberSeparator:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    .prologue
    .line 55
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mMaxValueLength:I

    return v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    .prologue
    .line 55
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->needCursorLastPosition:Z

    return v0
.end method

.method static synthetic access$902(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;
    .param p1, "x1"    # Z

    .prologue
    .line 55
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->needCursorLastPosition:Z

    return p1
.end method

.method private initialize(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 142
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->getRootLayoutId()I

    move-result v1

    invoke-static {p1, v1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 144
    .local v0, "rootView":Landroid/view/View;
    const v1, 0x7f080473

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mUnitTextView:Landroid/widget/TextView;

    .line 145
    const v1, 0x7f08063d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mIncButton:Landroid/widget/ImageButton;

    .line 146
    const v1, 0x7f08063a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mDecButton:Landroid/widget/ImageButton;

    .line 147
    const v1, 0x7f080472

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    .line 148
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0702b1

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setTextColor(I)V

    .line 149
    const v1, 0x7f0807e1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mDummyEdit:Landroid/widget/EditText;

    .line 150
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->getControllerView(Landroid/view/View;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView;

    .line 151
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->initAdditionalViews(Landroid/view/View;)V

    .line 152
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->registerListener()V

    .line 153
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->moveFocusFromValueEditText()V

    .line 154
    return-void
.end method

.method private isDecimalNumber()Z
    .locals 1

    .prologue
    .line 450
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mMaxInputRange:I

    rem-int/lit8 v0, v0, 0xa

    if-nez v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mMaxNormalRange:I

    rem-int/lit8 v0, v0, 0xa

    if-nez v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mMinInputRange:I

    rem-int/lit8 v0, v0, 0xa

    if-nez v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mMinNormalRange:I

    rem-int/lit8 v0, v0, 0xa

    if-nez v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mMoveDistance:I

    rem-int/lit8 v0, v0, 0xa

    if-eqz v0, :cond_1

    .line 452
    :cond_0
    const/4 v0, 0x1

    .line 454
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isNumeric(Ljava/lang/String;)Z
    .locals 3
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 872
    :try_start_0
    invoke-static {p1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 876
    const/4 v1, 0x1

    :goto_0
    return v1

    .line 873
    :catch_0
    move-exception v0

    .line 874
    .local v0, "nfe":Ljava/lang/NumberFormatException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private registerListener()V
    .locals 2

    .prologue
    .line 282
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mInputModuleTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 283
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$1;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 295
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$2;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 362
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView;

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$3;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView;->setOnValueChangedListener(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$OnValueChangedListener;)V

    .line 370
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView;

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$4;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView;->setOnControllerTapListener(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$OnControllerTapListener;)V

    .line 379
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mDecButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mIncDecTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 380
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mIncButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mIncDecTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 381
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mDecButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 382
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mIncButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 383
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->setInputFilter()V

    .line 384
    return-void
.end method

.method private setEnableDecBtn(Z)V
    .locals 2
    .param p1, "isEnable"    # Z

    .prologue
    .line 257
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mDecButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 258
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mDecButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 260
    if-eqz p1, :cond_0

    .line 261
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mDecButton:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setNextFocusDownId(I)V

    .line 262
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mIncButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mDecButton:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setNextFocusLeftId(I)V

    .line 264
    :cond_0
    return-void
.end method

.method private setEnableIncBtn(Z)V
    .locals 2
    .param p1, "isEnable"    # Z

    .prologue
    .line 249
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mIncButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 250
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mIncButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 251
    if-eqz p1, :cond_0

    .line 252
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mDecButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mIncButton:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setNextFocusRightId(I)V

    .line 254
    :cond_0
    return-void
.end method

.method private setInputFilter()V
    .locals 5

    .prologue
    .line 458
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mInputFilters:[Landroid/text/InputFilter;

    if-nez v1, :cond_0

    .line 459
    const/4 v1, 0x2

    new-array v1, v1, [Landroid/text/InputFilter;

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mInputFilters:[Landroid/text/InputFilter;

    .line 461
    :cond_0
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mMaxInputRange:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 462
    .local v0, "maxRangeString":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mMaxValueLength:I

    .line 464
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mInputFilters:[Landroid/text/InputFilter;

    const/4 v2, 0x0

    new-instance v3, Landroid/text/InputFilter$LengthFilter;

    iget v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mMaxValueLength:I

    invoke-direct {v3, v4}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v3, v1, v2

    .line 465
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mInputFilters:[Landroid/text/InputFilter;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mRegularExpression:Landroid/text/InputFilter;

    aput-object v3, v1, v2

    .line 466
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mInputFilters:[Landroid/text/InputFilter;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 467
    return-void
.end method

.method private setInputType()V
    .locals 2

    .prologue
    .line 470
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->isDecimalNumber()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 471
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$5;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$5;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setKeyListener(Landroid/text/method/KeyListener;)V

    .line 489
    :goto_0
    return-void

    .line 487
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setInputType(I)V

    goto :goto_0
.end method

.method private setNewValue(IZ)V
    .locals 1
    .param p1, "newValue"    # I
    .param p2, "mustShowSparator"    # Z

    .prologue
    .line 408
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->setNewValue(IZZ)V

    .line 409
    return-void
.end method

.method private setNewValue(IZZ)V
    .locals 7
    .param p1, "newValue"    # I
    .param p2, "mustShowSparator"    # Z
    .param p3, "withSound"    # Z

    .prologue
    const/4 v6, 0x0

    const/4 v4, 0x1

    .line 413
    iput-boolean v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->needCursorLastPosition:Z

    .line 414
    int-to-float v1, p1

    const/high16 v2, 0x41200000    # 10.0f

    div-float v0, v1, v2

    .line 426
    .local v0, "floatNewValue":F
    rem-int/lit8 v1, p1, 0xa

    if-nez v1, :cond_2

    .line 427
    if-eqz p2, :cond_1

    .line 428
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "%.1f"

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 436
    :goto_0
    if-eqz p3, :cond_3

    sget v1, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->SOUND_MODE:I

    if-nez v1, :cond_3

    .line 439
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->LastVaule:I

    if-eq v1, p1, :cond_0

    .line 440
    sget v1, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->SOUND_GRADUATION_CONTROL:I

    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->playSound(I)V

    .line 441
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->LastVaule:I

    .line 447
    :cond_0
    :goto_1
    return-void

    .line 430
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "%d"

    new-array v4, v4, [Ljava/lang/Object;

    float-to-int v5, v0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 433
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "%.1f"

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 444
    :cond_3
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->LastVaule:I

    goto :goto_1
.end method

.method private showAbnormalGoalWarningPopupCalorie()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 993
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090f51

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/16 v6, 0xa

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    const/4 v6, 0x1

    const/16 v7, 0x2706

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 995
    .local v3, "rangeString":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0900b9

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 996
    .local v2, "kCal":Ljava/lang/String;
    invoke-virtual {v3, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 997
    const-string v4, "kcal"

    invoke-virtual {v3, v4, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 999
    :cond_0
    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mContext:Landroid/content/Context;

    invoke-direct {v1, v4, v8}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 1000
    .local v1, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090f54

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "\n\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1011
    .local v0, "alertTextString":Ljava/lang/StringBuilder;
    const v4, 0x7f090b60

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v4

    const/high16 v5, 0x20000

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->addFlags(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v4

    new-instance v5, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$11;

    invoke-direct {v5, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$11;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)V

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1029
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v5

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mContext:Landroid/content/Context;

    check-cast v4, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    const-string v6, "Input Module"

    invoke-virtual {v5, v4, v6}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 1030
    return-void
.end method

.method private showAbnormalGoalWarningPopupDistance()V
    .locals 14

    .prologue
    const-wide/32 v12, 0xf3e58

    const-wide/16 v10, 0x64

    const/4 v9, 0x3

    const/4 v8, 0x0

    .line 935
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getDistanceUnitFromSharedPreferences()Ljava/lang/String;

    move-result-object v4

    const-string v5, "km"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    .line 937
    .local v3, "isUnitKm":Z
    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mContext:Landroid/content/Context;

    invoke-direct {v1, v4, v8}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 938
    .local v1, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    if-eqz v3, :cond_0

    const v4, 0x7f090f6b

    :goto_0
    invoke-virtual {v5, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 940
    .local v2, "distType":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090f50

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "\n\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v4, 0x2

    new-array v6, v4, [Ljava/lang/Object;

    if-eqz v3, :cond_1

    invoke-static {v10, v11}, Lcom/sec/android/app/shealth/common/utils/UnitUtils$UnitConverter;->getKmFromMeter(J)Ljava/lang/String;

    move-result-object v4

    :goto_1
    aput-object v4, v6, v8

    const/4 v7, 0x1

    if-eqz v3, :cond_2

    invoke-static {v12, v13}, Lcom/sec/android/app/shealth/common/utils/UnitUtils$UnitConverter;->getKmFromMeter(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    :goto_2
    aput-object v4, v6, v7

    invoke-static {v2, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 949
    .local v0, "alertTextString":Ljava/lang/StringBuilder;
    const v4, 0x7f090b60

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v4

    const/high16 v5, 0x20000

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->addFlags(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v4

    new-instance v5, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$10;

    invoke-direct {v5, p0, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$10;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;Z)V

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 973
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v5

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mContext:Landroid/content/Context;

    check-cast v4, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    const-string v6, "Input Module"

    invoke-virtual {v5, v4, v6}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 974
    return-void

    .line 938
    .end local v0    # "alertTextString":Ljava/lang/StringBuilder;
    .end local v2    # "distType":Ljava/lang/String;
    :cond_0
    const v4, 0x7f090f6c

    goto :goto_0

    .line 940
    .restart local v2    # "distType":Ljava/lang/String;
    :cond_1
    invoke-static {v10, v11}, Lcom/sec/android/app/shealth/common/utils/UnitUtils$UnitConverter;->getMilesFromMeters(J)Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    :cond_2
    invoke-static {v12, v13}, Lcom/sec/android/app/shealth/common/utils/UnitUtils$UnitConverter;->getMilesFromMeters(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    goto :goto_2
.end method


# virtual methods
.method public dispatchKeyEventPreIme(Landroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 918
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    const/4 v3, 0x4

    if-ne v2, v3, :cond_0

    .line 919
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "input_method"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 920
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->isInputMethodShown()Z

    move-result v1

    .line 921
    .local v1, "isActive":Z
    if-eqz v1, :cond_0

    .line 922
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->hideEdittextBackground()V

    .line 923
    const/4 v2, 0x1

    .line 926
    .end local v0    # "imm":Landroid/view/inputmethod/InputMethodManager;
    .end local v1    # "isActive":Z
    :goto_0
    return v2

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchKeyEventPreIme(Landroid/view/KeyEvent;)Z

    move-result v2

    goto :goto_0
.end method

.method protected abstract getControllerView(Landroid/view/View;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView;
.end method

.method public getKmFromMeterByLocale(J)Ljava/lang/String;
    .locals 8
    .param p1, "distance"    # J

    .prologue
    .line 977
    long-to-double v4, p1

    const-wide v6, 0x408f400000000000L    # 1000.0

    div-double v2, v4, v6

    .line 978
    .local v2, "km":D
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v4, "0.00"

    new-instance v5, Ljava/text/DecimalFormatSymbols;

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v5, v6}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    invoke-direct {v0, v4, v5}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    .line 979
    .local v0, "decimalFormat":Ljava/text/DecimalFormat;
    invoke-virtual {v0, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    .line 981
    .local v1, "goal":Ljava/lang/String;
    return-object v1
.end method

.method public getMilesFromMetersByLocale(J)Ljava/lang/String;
    .locals 8
    .param p1, "distance"    # J

    .prologue
    .line 985
    long-to-double v4, p1

    const-wide v6, 0x3f445c6dea1359a4L    # 6.2137E-4

    mul-double v2, v4, v6

    .line 986
    .local v2, "mi":D
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v4, "0.00"

    new-instance v5, Ljava/text/DecimalFormatSymbols;

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v5, v6}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    invoke-direct {v0, v4, v5}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    .line 987
    .local v0, "decimalFormat":Ljava/text/DecimalFormat;
    invoke-virtual {v0, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    .line 989
    .local v1, "goal":Ljava/lang/String;
    return-object v1
.end method

.method protected abstract getRootLayoutId()I
.end method

.method public getValue()F
    .locals 2

    .prologue
    .line 220
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mCurrentValue:I

    int-to-float v0, v0

    const/high16 v1, 0x41200000    # 10.0f

    div-float/2addr v0, v1

    return v0
.end method

.method public hideEdittextBackground()V
    .locals 3

    .prologue
    .line 880
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->hasFocus()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 881
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 883
    .local v0, "mTempString":Ljava/lang/String;
    const-string v1, ""

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 884
    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mCurrentScaleValue:I

    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->isDecimalNumber()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-direct {p0, v2, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->setNewValue(IZ)V

    .line 914
    .end local v0    # "mTempString":Ljava/lang/String;
    :cond_0
    :goto_1
    return-void

    .line 884
    .restart local v0    # "mTempString":Ljava/lang/String;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 887
    :cond_2
    const-string v1, ","

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mSystemNumberSeparator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mSystemNumberSeparator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 888
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mSystemNumberSeparator:Ljava/lang/String;

    const-string v2, "."

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 911
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->moveFocusFromValueEditText()V

    goto :goto_1
.end method

.method public hideKeyboard()V
    .locals 3

    .prologue
    .line 746
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 748
    return-void
.end method

.method protected abstract initAdditionalViews(Landroid/view/View;)V
.end method

.method public isValueEmpty()Z
    .locals 2

    .prologue
    .line 224
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected moveFocusFromValueEditText()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 390
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 391
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFocusableInTouchMode(Z)V

    .line 392
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 393
    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->setEnableIncBtn(Z)V

    .line 394
    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->setEnableDecBtn(Z)V

    .line 396
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->clearFocus()V

    .line 397
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->hideKeyboard()V

    .line 398
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 399
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setFocusableInTouchMode(Z)V

    .line 400
    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->setEnableIncBtn(Z)V

    .line 401
    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->setEnableDecBtn(Z)V

    .line 403
    :cond_0
    return-void
.end method

.method protected onTouchModeEditTextColor(Z)V
    .locals 4
    .param p1, "isTaped"    # Z

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 267
    if-eqz p1, :cond_0

    .line 268
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setCursorVisible(Z)V

    .line 269
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->isOccupiedControllerTab:Z

    .line 270
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    const v1, -0xc25400

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setTextColor(I)V

    .line 272
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setFocusableInTouchMode(Z)V

    .line 279
    :goto_0
    return-void

    .line 274
    :cond_0
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->isOccupiedControllerTab:Z

    .line 275
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setTextColor(I)V

    .line 277
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setFocusableInTouchMode(Z)V

    goto :goto_0
.end method

.method public setEditTextSize(I)V
    .locals 3
    .param p1, "size"    # I

    .prologue
    .line 157
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    const/16 v2, 0x10b

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setMaxWidth(I)V

    .line 158
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->setSingleLine()V

    .line 159
    const/4 v1, 0x1

    new-array v0, v1, [Landroid/text/InputFilter;

    .line 160
    .local v0, "filterArray":[Landroid/text/InputFilter;
    const/4 v1, 0x0

    new-instance v2, Landroid/text/InputFilter$LengthFilter;

    invoke-direct {v2, p1}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v2, v0, v1

    .line 161
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 162
    return-void
.end method

.method public setEdittextChars()V
    .locals 0

    .prologue
    .line 493
    return-void
.end method

.method public setEnabled(Z)V
    .locals 4
    .param p1, "isEnable"    # Z

    .prologue
    .line 228
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setEnabled(Z)V

    .line 229
    move v1, p1

    .local v1, "enableIncBtn":Z
    move v0, p1

    .line 231
    .local v0, "enableDecBtn":Z
    if-eqz p1, :cond_0

    .line 232
    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mCurrentValue:I

    iget v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mMaxInputRange:I

    if-lt v2, v3, :cond_1

    .line 233
    const/4 v1, 0x0

    .line 239
    :cond_0
    :goto_0
    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->setEnableIncBtn(Z)V

    .line 240
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->setEnableDecBtn(Z)V

    .line 242
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    invoke-virtual {v2, p1}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 243
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    invoke-virtual {v2, p1}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 244
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    invoke-virtual {v2, p1}, Landroid/widget/EditText;->setFocusableInTouchMode(Z)V

    .line 245
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView;

    invoke-virtual {v2, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView;->setEnabled(Z)V

    .line 246
    return-void

    .line 234
    :cond_1
    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mCurrentValue:I

    iget v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mMinInputRange:I

    if-gt v2, v3, :cond_0

    .line 235
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setFilterText(Z)V
    .locals 0
    .param p1, "filterText"    # Z

    .prologue
    .line 568
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->filterText:Z

    .line 569
    return-void
.end method

.method public setIncDecEnabled(IZ)V
    .locals 3
    .param p1, "changedNumber"    # I
    .param p2, "skipUpdate"    # Z

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 699
    if-nez p2, :cond_1

    .line 700
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mCurrentValue:I

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mCurrentScaleValue:I

    .line 701
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 702
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mMaxInputRange:I

    if-lt p1, v0, :cond_2

    .line 703
    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->setEnableIncBtn(Z)V

    .line 704
    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->setEnableDecBtn(Z)V

    .line 718
    :cond_0
    :goto_0
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->setValueToController(I)V

    .line 721
    :cond_1
    return-void

    .line 706
    :cond_2
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->actualMinValue:I

    if-gt p1, v0, :cond_3

    .line 707
    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->setEnableIncBtn(Z)V

    .line 708
    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->setEnableDecBtn(Z)V

    goto :goto_0

    .line 711
    :cond_3
    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->setEnableIncBtn(Z)V

    .line 712
    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->setEnableDecBtn(Z)V

    goto :goto_0
.end method

.method public setInputRange(FF)V
    .locals 2
    .param p1, "minRange"    # F
    .param p2, "maxRange"    # F

    .prologue
    const/high16 v1, 0x41200000    # 10.0f

    .line 173
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->filterText:Z

    if-eqz v0, :cond_0

    .line 174
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mMinInputRange:I

    .line 178
    :goto_0
    mul-float v0, p1, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->actualMinValue:I

    .line 179
    mul-float v0, p2, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mMaxInputRange:I

    .line 181
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->setInputFilter()V

    .line 182
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->setInputType()V

    .line 183
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView;->setInputRange(FF)V

    .line 184
    return-void

    .line 176
    :cond_0
    mul-float v0, p1, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mMinInputRange:I

    goto :goto_0
.end method

.method public setMoveDistance(F)V
    .locals 1
    .param p1, "distance"    # F

    .prologue
    .line 202
    const/high16 v0, 0x41200000    # 10.0f

    mul-float/2addr v0, p1

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mMoveDistance:I

    .line 204
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->setInputType()V

    .line 205
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView;->setInterval(F)V

    .line 206
    return-void
.end method

.method public setNormalRange(FF)V
    .locals 2
    .param p1, "minNormalRange"    # F
    .param p2, "maxNormalRange"    # F

    .prologue
    const/high16 v1, 0x41200000    # 10.0f

    .line 191
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->filterText:Z

    if-eqz v0, :cond_0

    .line 192
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mMinNormalRange:I

    .line 195
    :goto_0
    mul-float v0, p2, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mMaxNormalRange:I

    .line 197
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->setInputType()V

    .line 198
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView;->setNormalRange(FF)V

    .line 199
    return-void

    .line 194
    :cond_0
    mul-float v0, p1, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mMinNormalRange:I

    goto :goto_0
.end method

.method public setType(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 930
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->type:I

    .line 931
    return-void
.end method

.method public setUnit(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1, "unit"    # Ljava/lang/CharSequence;

    .prologue
    .line 165
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mUnitTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 166
    return-void
.end method

.method public setValue(F)V
    .locals 1
    .param p1, "value"    # F

    .prologue
    .line 209
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->setValue(FZ)V

    .line 210
    return-void
.end method

.method public setValue(FZ)V
    .locals 3
    .param p1, "value"    # F
    .param p2, "withSound"    # Z

    .prologue
    .line 213
    const-string v0, "Input Module"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setValue : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 214
    const/high16 v0, 0x41200000    # 10.0f

    mul-float/2addr v0, p1

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mCurrentValue:I

    .line 215
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mCurrentValue:I

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mCurrentScaleValue:I

    .line 216
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mCurrentValue:I

    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->isDecimalNumber()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v1, v0, p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->setNewValue(IZZ)V

    .line 217
    return-void

    .line 216
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setValueToController(I)V
    .locals 6
    .param p1, "changedNumber"    # I

    .prologue
    const v5, 0x7f090244

    const v4, 0x7f090243

    const/high16 v3, 0x41200000    # 10.0f

    .line 725
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView;

    if-eqz v0, :cond_0

    .line 726
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->filterText:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->actualMinValue:I

    if-ge p1, v0, :cond_1

    .line 728
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView;

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->actualMinValue:I

    int-to-float v1, v1

    div-float/2addr v1, v3

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView;->setValue(F)V

    .line 729
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->actualMinValue:I

    int-to-float v2, v2

    div-float/2addr v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 737
    :cond_0
    :goto_0
    return-void

    .line 733
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView;

    int-to-float v1, p1

    div-float/2addr v1, v3

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView;->setValue(F)V

    .line 734
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    int-to-float v2, p1

    div-float/2addr v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected valueIncOrDec(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 751
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mIncButton:Landroid/widget/ImageButton;

    invoke-virtual {v3}, Landroid/widget/ImageButton;->getId()I

    move-result v3

    if-ne v2, v3, :cond_2

    .line 752
    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mCurrentScaleValue:I

    iget v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mMoveDistance:I

    add-int/2addr v2, v3

    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->isDecimalNumber()Z

    move-result v3

    if-eqz v3, :cond_1

    :goto_0
    invoke-direct {p0, v2, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->setNewValue(IZ)V

    .line 760
    :cond_0
    :goto_1
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mCurrentScaleValue:I

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mCurrentValue:I

    .line 761
    return-void

    :cond_1
    move v0, v1

    .line 752
    goto :goto_0

    .line 753
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mDecButton:Landroid/widget/ImageButton;

    invoke-virtual {v3}, Landroid/widget/ImageButton;->getId()I

    move-result v3

    if-ne v2, v3, :cond_0

    .line 754
    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mMinInputRange:I

    const/16 v3, 0x2710

    if-ne v2, v3, :cond_4

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mCurrentScaleValue:I

    const/16 v3, 0x2af8

    if-gt v2, v3, :cond_4

    .line 755
    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mMinInputRange:I

    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->isDecimalNumber()Z

    move-result v3

    if-eqz v3, :cond_3

    :goto_2
    invoke-direct {p0, v2, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->setNewValue(IZ)V

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2

    .line 757
    :cond_4
    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mCurrentScaleValue:I

    iget v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mMoveDistance:I

    sub-int/2addr v2, v3

    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->isDecimalNumber()Z

    move-result v3

    if-eqz v3, :cond_5

    :goto_3
    invoke-direct {p0, v2, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->setNewValue(IZ)V

    goto :goto_1

    :cond_5
    move v0, v1

    goto :goto_3
.end method
