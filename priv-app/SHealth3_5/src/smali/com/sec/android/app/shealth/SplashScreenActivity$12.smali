.class Lcom/sec/android/app/shealth/SplashScreenActivity$12;
.super Ljava/lang/Object;
.source "SplashScreenActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/SplashScreenActivity;->getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/SplashScreenActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/SplashScreenActivity;)V
    .locals 0

    .prologue
    .line 978
    iput-object p1, p0, Lcom/sec/android/app/shealth/SplashScreenActivity$12;->this$0:Lcom/sec/android/app/shealth/SplashScreenActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDialogButtonClick(Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;)V
    .locals 3
    .param p1, "dialogButtonType"    # Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    .prologue
    const/4 v1, 0x1

    .line 981
    sget-object v0, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->POSITIVE:Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    if-ne p1, v0, :cond_5

    .line 982
    iget-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity$12;->this$0:Lcom/sec/android/app/shealth/SplashScreenActivity;

    # getter for: Lcom/sec/android/app/shealth/SplashScreenActivity;->popupMode:I
    invoke-static {v0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->access$1100(Lcom/sec/android/app/shealth/SplashScreenActivity;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1016
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 984
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity$12;->this$0:Lcom/sec/android/app/shealth/SplashScreenActivity;

    # getter for: Lcom/sec/android/app/shealth/SplashScreenActivity;->doNotShowCheckBox:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->access$1000(Lcom/sec/android/app/shealth/SplashScreenActivity;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 985
    iget-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity$12;->this$0:Lcom/sec/android/app/shealth/SplashScreenActivity;

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setLaunchMobilePopupWasShown(Landroid/content/Context;Z)V

    .line 987
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity$12;->this$0:Lcom/sec/android/app/shealth/SplashScreenActivity;

    # getter for: Lcom/sec/android/app/shealth/SplashScreenActivity;->networkUtils:Lcom/sec/android/app/shealth/common/utils/NetworkUtils;
    invoke-static {v0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->access$1200(Lcom/sec/android/app/shealth/SplashScreenActivity;)Lcom/sec/android/app/shealth/common/utils/NetworkUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;->isWifiEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity$12;->this$0:Lcom/sec/android/app/shealth/SplashScreenActivity;

    invoke-static {v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isLaunchWifiPopupWasShown(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 989
    iget-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity$12;->this$0:Lcom/sec/android/app/shealth/SplashScreenActivity;

    const/16 v1, 0x8

    # setter for: Lcom/sec/android/app/shealth/SplashScreenActivity;->popupMode:I
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/SplashScreenActivity;->access$1102(Lcom/sec/android/app/shealth/SplashScreenActivity;I)I

    .line 990
    iget-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity$12;->this$0:Lcom/sec/android/app/shealth/SplashScreenActivity;

    # getter for: Lcom/sec/android/app/shealth/SplashScreenActivity;->connectionWifiBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-static {v0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->access$1300(Lcom/sec/android/app/shealth/SplashScreenActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/SplashScreenActivity$12;->this$0:Lcom/sec/android/app/shealth/SplashScreenActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/SplashScreenActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "data_connection_dialog"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 992
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity$12;->this$0:Lcom/sec/android/app/shealth/SplashScreenActivity;

    # setter for: Lcom/sec/android/app/shealth/SplashScreenActivity;->isPopupOkDone:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/SplashScreenActivity;->access$1402(Lcom/sec/android/app/shealth/SplashScreenActivity;Z)Z

    .line 993
    iget-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity$12;->this$0:Lcom/sec/android/app/shealth/SplashScreenActivity;

    # invokes: Lcom/sec/android/app/shealth/SplashScreenActivity;->launchHomeActivity()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->access$1500(Lcom/sec/android/app/shealth/SplashScreenActivity;)V

    goto :goto_0

    .line 998
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity$12;->this$0:Lcom/sec/android/app/shealth/SplashScreenActivity;

    # getter for: Lcom/sec/android/app/shealth/SplashScreenActivity;->doNotShowCheckBox:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->access$1000(Lcom/sec/android/app/shealth/SplashScreenActivity;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 999
    iget-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity$12;->this$0:Lcom/sec/android/app/shealth/SplashScreenActivity;

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setLaunchWifiPopupWasShown(Landroid/content/Context;Z)V

    .line 1001
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity$12;->this$0:Lcom/sec/android/app/shealth/SplashScreenActivity;

    # getter for: Lcom/sec/android/app/shealth/SplashScreenActivity;->networkUtils:Lcom/sec/android/app/shealth/common/utils/NetworkUtils;
    invoke-static {v0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->access$1200(Lcom/sec/android/app/shealth/SplashScreenActivity;)Lcom/sec/android/app/shealth/common/utils/NetworkUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;->isWifiEnabled()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity$12;->this$0:Lcom/sec/android/app/shealth/SplashScreenActivity;

    # getter for: Lcom/sec/android/app/shealth/SplashScreenActivity;->networkUtils:Lcom/sec/android/app/shealth/common/utils/NetworkUtils;
    invoke-static {v0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->access$1200(Lcom/sec/android/app/shealth/SplashScreenActivity;)Lcom/sec/android/app/shealth/common/utils/NetworkUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;->isMobileDataEnabled()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity$12;->this$0:Lcom/sec/android/app/shealth/SplashScreenActivity;

    invoke-static {v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isLaunchMobilePopupWasShown(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1003
    iget-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity$12;->this$0:Lcom/sec/android/app/shealth/SplashScreenActivity;

    const/4 v1, 0x6

    # setter for: Lcom/sec/android/app/shealth/SplashScreenActivity;->popupMode:I
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/SplashScreenActivity;->access$1102(Lcom/sec/android/app/shealth/SplashScreenActivity;I)I

    .line 1004
    iget-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity$12;->this$0:Lcom/sec/android/app/shealth/SplashScreenActivity;

    # getter for: Lcom/sec/android/app/shealth/SplashScreenActivity;->connectionBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-static {v0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->access$1600(Lcom/sec/android/app/shealth/SplashScreenActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/SplashScreenActivity$12;->this$0:Lcom/sec/android/app/shealth/SplashScreenActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/SplashScreenActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "data_connection_dialog"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1006
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity$12;->this$0:Lcom/sec/android/app/shealth/SplashScreenActivity;

    # setter for: Lcom/sec/android/app/shealth/SplashScreenActivity;->isPopupOkDone:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/SplashScreenActivity;->access$1402(Lcom/sec/android/app/shealth/SplashScreenActivity;Z)Z

    .line 1007
    iget-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity$12;->this$0:Lcom/sec/android/app/shealth/SplashScreenActivity;

    # invokes: Lcom/sec/android/app/shealth/SplashScreenActivity;->launchHomeActivity()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->access$1500(Lcom/sec/android/app/shealth/SplashScreenActivity;)V

    goto/16 :goto_0

    .line 1013
    :cond_5
    sget-object v0, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->NEGATIVE:Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    if-ne p1, v0, :cond_0

    .line 1014
    iget-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity$12;->this$0:Lcom/sec/android/app/shealth/SplashScreenActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->finish()V

    goto/16 :goto_0

    .line 982
    nop

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
