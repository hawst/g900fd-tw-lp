.class public Lcom/sec/android/app/shealth/common/commondao/userdevicedao/UserDeviceDaoImplDb;
.super Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;
.source "UserDeviceDaoImplDb.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commondao/userdevicedao/UserDeviceDao;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb",
        "<",
        "Lcom/sec/android/app/shealth/common/utils/hcp/data/UserDeviceData;",
        ">;",
        "Lcom/sec/android/app/shealth/common/commondao/userdevicedao/UserDeviceDao;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 37
    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$UserDevice;->CONTENT_URI:Landroid/net/Uri;

    const-string v3, "create_time"

    const-string v4, "_id"

    const-string v5, "device_id"

    move-object v0, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;-><init>(Landroid/net/Uri;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    return-void
.end method


# virtual methods
.method protected bridge synthetic getDataContentValue(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;)Landroid/content/ContentValues;
    .locals 1
    .param p1, "x0"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;

    .prologue
    .line 30
    check-cast p1, Lcom/sec/android/app/shealth/common/utils/hcp/data/UserDeviceData;

    .end local p1    # "x0":Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/common/commondao/userdevicedao/UserDeviceDaoImplDb;->getDataContentValue(Lcom/sec/android/app/shealth/common/utils/hcp/data/UserDeviceData;)Landroid/content/ContentValues;

    move-result-object v0

    return-object v0
.end method

.method protected getDataContentValue(Lcom/sec/android/app/shealth/common/utils/hcp/data/UserDeviceData;)Landroid/content/ContentValues;
    .locals 3
    .param p1, "item"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/UserDeviceData;

    .prologue
    .line 43
    new-instance v0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;

    invoke-direct {v0, p1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;-><init>(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;)V

    .line 44
    .local v0, "values":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;
    const-string v1, "custom_name"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/UserDeviceData;->getCustomName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    const-string v1, "device_id"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/UserDeviceData;->getDeviceId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    const-string v1, "device_type"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/UserDeviceData;->getDeviceType()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 47
    const-string/jumbo v1, "model"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/UserDeviceData;->getModel()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    const-string/jumbo v1, "manufacture"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/UserDeviceData;->getManufacture()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    const-string v1, "connectivity_type"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/UserDeviceData;->getConnectivityType()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 50
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->getContentValues()Landroid/content/ContentValues;

    move-result-object v1

    return-object v1
.end method

.method protected bridge synthetic getItemFromRow(Landroid/database/Cursor;)Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;
    .locals 1
    .param p1, "x0"    # Landroid/database/Cursor;

    .prologue
    .line 30
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/common/commondao/userdevicedao/UserDeviceDaoImplDb;->getItemFromRow(Landroid/database/Cursor;)Lcom/sec/android/app/shealth/common/utils/hcp/data/UserDeviceData;

    move-result-object v0

    return-object v0
.end method

.method protected getItemFromRow(Landroid/database/Cursor;)Lcom/sec/android/app/shealth/common/utils/hcp/data/UserDeviceData;
    .locals 18
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 55
    new-instance v17, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;-><init>(Landroid/database/Cursor;)V

    .line 56
    .local v17, "getter":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;
    new-instance v2, Lcom/sec/android/app/shealth/common/utils/hcp/data/UserDeviceData;

    const-string v3, "device_id"

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "device_type"

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getInt(Ljava/lang/String;)I

    move-result v4

    const-string v5, "custom_name"

    move-object/from16 v0, v17

    invoke-virtual {v0, v5}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "model"

    move-object/from16 v0, v17

    invoke-virtual {v0, v6}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "manufacture"

    move-object/from16 v0, v17

    invoke-virtual {v0, v7}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "connectivity_type"

    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getInt(Ljava/lang/String;)I

    move-result v8

    const-string v9, "_id"

    move-object/from16 v0, v17

    invoke-virtual {v0, v9}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getLong(Ljava/lang/String;)J

    move-result-wide v9

    const-string v11, "create_time"

    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getLong(Ljava/lang/String;)J

    move-result-wide v11

    const-string/jumbo v13, "update_time"

    move-object/from16 v0, v17

    invoke-virtual {v0, v13}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getLong(Ljava/lang/String;)J

    move-result-wide v13

    const-string/jumbo v15, "time_zone"

    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getInt(Ljava/lang/String;)I

    move-result v15

    const-string v16, "daylight_saving"

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getInt(Ljava/lang/String;)I

    move-result v16

    invoke-direct/range {v2 .. v16}, Lcom/sec/android/app/shealth/common/utils/hcp/data/UserDeviceData;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;IJJJII)V

    return-object v2
.end method
