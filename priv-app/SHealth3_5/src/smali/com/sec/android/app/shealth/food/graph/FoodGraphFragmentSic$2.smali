.class Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic$2;
.super Ljava/lang/Object;
.source "FoodGraphFragmentSic.java"

# interfaces
.implements Lcom/samsung/android/sdk/chart/style/SchartEventIconListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->initSeriesStyle(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# static fields
.field static final MEDAL_ICON:I = 0x0

.field static final NO_ICON:I = -0x1


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;)V
    .locals 0

    .prologue
    .line 335
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic$2;->this$0:Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public OnGetBitmapIndexStringXY(I[Ljava/lang/String;[F)[I
    .locals 3
    .param p1, "arg0"    # I
    .param p2, "xarr"    # [Ljava/lang/String;
    .param p3, "yarr"    # [F

    .prologue
    .line 343
    array-length v2, p2

    if-lez v2, :cond_0

    .line 344
    array-length v2, p2

    new-array v1, v2, [I

    .line 345
    .local v1, "ret":[I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p2

    if-ge v0, v2, :cond_1

    .line 346
    const/4 v2, -0x1

    aput v2, v1, v0

    .line 345
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 350
    .end local v0    # "i":I
    .end local v1    # "ret":[I
    :cond_0
    const/4 v1, 0x0

    :cond_1
    return-object v1
.end method

.method public OnGetBitmapIndexTimeXY(I[J[F)[I
    .locals 8
    .param p1, "arg0"    # I
    .param p2, "xarr"    # [J
    .param p3, "yarr"    # [F

    .prologue
    .line 356
    array-length v4, p2

    if-lez v4, :cond_3

    .line 359
    array-length v4, p2

    new-array v3, v4, [I

    .line 361
    .local v3, "ret":[I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v4, p2

    if-ge v1, v4, :cond_4

    .line 364
    const/4 v0, 0x0

    .line 365
    .local v0, "goal":F
    const/4 v2, 0x0

    .line 367
    .local v2, "isGoalAcheived":Z
    # getter for: Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->mGoalDataMap:Ljava/util/Map;
    invoke-static {}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->access$100()Ljava/util/Map;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Map;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 369
    # invokes: Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->createGoalMap()V
    invoke-static {}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->access$200()V

    .line 372
    :cond_0
    # getter for: Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->mGoalDataMap:Ljava/util/Map;
    invoke-static {}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->access$100()Ljava/util/Map;

    move-result-object v4

    aget-wide v5, p2, v1

    invoke-static {v5, v6}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 375
    aget v5, p3, v1

    # getter for: Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->mGoalDataMap:Ljava/util/Map;
    invoke-static {}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->access$100()Ljava/util/Map;

    move-result-object v4

    aget-wide v6, p2, v1

    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v4, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    invoke-static {v5, v4}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->isCaloriesIntakeGoalAchieved(FF)Z

    move-result v2

    .line 402
    :goto_1
    if-eqz v2, :cond_2

    .line 404
    const/4 v4, 0x0

    aput v4, v3, v1

    .line 361
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 395
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic$2;->this$0:Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    aget-wide v5, p2, v1

    invoke-static {v5, v6}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v5

    invoke-static {v5, v6}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v5

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->getGoalCaloriesByDate(Landroid/content/Context;J)F

    move-result v0

    .line 397
    aget v4, p3, v1

    invoke-static {v4, v0}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->isCaloriesIntakeGoalAchieved(FF)Z

    move-result v2

    goto :goto_1

    .line 408
    :cond_2
    const/4 v4, -0x1

    aput v4, v3, v1

    goto :goto_2

    .line 413
    .end local v0    # "goal":F
    .end local v1    # "i":I
    .end local v2    # "isGoalAcheived":Z
    .end local v3    # "ret":[I
    :cond_3
    const/4 v3, 0x0

    :cond_4
    return-object v3
.end method
