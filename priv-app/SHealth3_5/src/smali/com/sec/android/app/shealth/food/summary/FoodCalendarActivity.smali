.class public Lcom/sec/android/app/shealth/food/summary/FoodCalendarActivity;
.super Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;
.source "FoodCalendarActivity.java"


# instance fields
.field private mDefaultGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

.field private mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected getDaysStatuses(JJ)Ljava/util/TreeMap;
    .locals 25
    .param p1, "startMonthTime"    # J
    .param p3, "endMonthTime"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ)",
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 54
    new-instance v21, Ljava/util/TreeMap;

    invoke-direct/range {v21 .. v21}, Ljava/util/TreeMap;-><init>()V

    .line 55
    .local v21, "result":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/Long;Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;>;"
    new-instance v13, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;

    invoke-direct {v13}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;-><init>()V

    .line 56
    .local v13, "dayWithMedal":Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;
    const v22, 0x7f020605

    move/from16 v0, v22

    invoke-virtual {v13, v0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;->setMedalResourceId(I)V

    .line 57
    const/16 v22, 0x1

    move/from16 v0, v22

    invoke-virtual {v13, v0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;->setGoalAchieved(Z)V

    .line 58
    new-instance v12, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;

    invoke-direct {v12}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;-><init>()V

    .line 59
    .local v12, "dayWithMeasurements":Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;
    const v22, 0x7f020033

    move/from16 v0, v22

    invoke-virtual {v12, v0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;->setMedalResourceId(I)V

    .line 60
    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-virtual {v12, v0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;->setGoalAchieved(Z)V

    .line 61
    new-instance v22, Lcom/sec/android/app/shealth/food/fooddao/MealDaoImplDb;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/food/fooddao/MealDaoImplDb;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v22

    move-wide/from16 v1, p1

    move-wide/from16 v3, p3

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/android/app/shealth/food/fooddao/MealDaoImplDb;->getDataForPeriod(JJ)Ljava/util/List;

    move-result-object v20

    .line 63
    .local v20, "mealDataForPeriod":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;>;"
    new-instance v14, Landroid/util/LongSparseArray;

    invoke-direct {v14}, Landroid/util/LongSparseArray;-><init>()V

    .line 64
    .local v14, "daysCalories":Landroid/util/LongSparseArray;, "Landroid/util/LongSparseArray<Ljava/lang/Float;>;"
    invoke-interface/range {v20 .. v20}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v17

    .local v17, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-eqz v22, :cond_1

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    .line 65
    .local v19, "mealData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getMealTime()J

    move-result-wide v22

    invoke-static/range {v22 .. v23}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v22

    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    .line 66
    .local v10, "dayStart":Ljava/lang/Long;
    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v22

    move-wide/from16 v0, v22

    invoke-virtual {v14, v0, v1}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v22

    if-nez v22, :cond_0

    const/4 v5, 0x0

    .line 67
    .local v5, "baseCalories":F
    :goto_1
    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getKcal()F

    move-result v22

    add-float v6, v5, v22

    .line 68
    .local v6, "calories":F
    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v22

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v24

    move-wide/from16 v0, v22

    move-object/from16 v2, v24

    invoke-virtual {v14, v0, v1, v2}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    goto :goto_0

    .line 66
    .end local v5    # "baseCalories":F
    .end local v6    # "calories":F
    :cond_0
    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v22

    move-wide/from16 v0, v22

    invoke-virtual {v14, v0, v1}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Ljava/lang/Float;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Float;->floatValue()F

    move-result v5

    goto :goto_1

    .line 71
    .end local v10    # "dayStart":Ljava/lang/Long;
    .end local v19    # "mealData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    :cond_1
    const v22, 0x9c43

    move/from16 v0, v22

    move-wide/from16 v1, p1

    move-wide/from16 v3, p3

    invoke-static {v0, v1, v2, v3, v4}, Lcom/sec/android/app/shealth/common/utils/hcp/GoalUtils;->getGoalValueForPeriod(IJJ)Landroid/util/LongSparseArray;

    move-result-object v18

    .line 74
    .local v18, "mGoalDataMap":Landroid/util/LongSparseArray;, "Landroid/util/LongSparseArray<Ljava/lang/Float;>;"
    const/16 v16, 0x0

    .local v16, "i":I
    :goto_2
    invoke-virtual {v14}, Landroid/util/LongSparseArray;->size()I

    move-result v22

    move/from16 v0, v16

    move/from16 v1, v22

    if-ge v0, v1, :cond_4

    .line 75
    move/from16 v0, v16

    invoke-virtual {v14, v0}, Landroid/util/LongSparseArray;->keyAt(I)J

    move-result-wide v10

    .line 76
    .local v10, "dayStart":J
    invoke-static {v10, v11}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v8

    .line 77
    .local v8, "dayEnd":J
    move/from16 v0, v16

    invoke-virtual {v14, v0}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Ljava/lang/Float;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Float;->floatValue()F

    move-result v7

    .line 80
    .local v7, "dayCalories":F
    const/4 v15, 0x0

    .line 81
    .local v15, "goalValue":F
    move-object/from16 v0, v18

    invoke-virtual {v0, v8, v9}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v22

    if-eqz v22, :cond_2

    .line 83
    move-object/from16 v0, v18

    invoke-virtual {v0, v8, v9}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Ljava/lang/Float;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Float;->floatValue()F

    move-result v15

    .line 89
    :goto_3
    invoke-static {v7, v15}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->isCaloriesIntakeGoalAchieved(FF)Z

    move-result v22

    if-eqz v22, :cond_3

    .line 90
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v22

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v0, v1, v13}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    :goto_4
    add-int/lit8 v16, v16, 0x1

    goto :goto_2

    .line 87
    :cond_2
    invoke-static {v8, v9}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v22

    move-object/from16 v0, p0

    move-wide/from16 v1, v22

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->getGoalCaloriesByDate(Landroid/content/Context;J)F

    move-result v15

    goto :goto_3

    .line 92
    :cond_3
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v22

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v0, v1, v12}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4

    .line 96
    .end local v7    # "dayCalories":F
    .end local v8    # "dayEnd":J
    .end local v10    # "dayStart":J
    .end local v15    # "goalValue":F
    :cond_4
    return-object v21
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 47
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->onCreate(Landroid/os/Bundle;)V

    .line 48
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodCalendarActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLoggerHolder;

    invoke-interface {v0}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLoggerHolder;->getUserActionLogger()Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodCalendarActivity;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    .line 49
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodCalendarActivity;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    sget-object v1, Lcom/sec/android/app/shealth/food/app/UserActionLog;->LAUNCH_CALENDAR:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;->logUserAction(Lcom/sec/android/app/shealth/food/app/UserActionLog;)V

    .line 50
    return-void
.end method
