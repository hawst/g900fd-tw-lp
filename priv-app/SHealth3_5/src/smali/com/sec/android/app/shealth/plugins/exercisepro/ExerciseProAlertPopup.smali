.class public Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProAlertPopup;
.super Landroid/support/v4/app/FragmentActivity;
.source "ExerciseProAlertPopup.java"


# instance fields
.field private dataType:I

.field private rowId:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProAlertPopup;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProAlertPopup;

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProAlertPopup;->startDetailActivity()V

    return-void
.end method

.method private showExceedRecordingTimePopup()V
    .locals 4

    .prologue
    .line 36
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 37
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    const v1, 0x7f0900e3

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f090a44

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProAlertPopup;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    const/high16 v2, 0x20000

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->addFlags(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProAlertPopup$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProAlertPopup$2;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProAlertPopup;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProAlertPopup$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProAlertPopup$1;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProAlertPopup;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnDismissListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 53
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProAlertPopup;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string/jumbo v3, "realtime_exceed_recording_time"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 54
    return-void
.end method

.method private startDetailActivity()V
    .locals 0

    .prologue
    .line 67
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 23
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 25
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProAlertPopup;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 26
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "DURATION_MAX_REACHED"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 27
    const-string v1, "data_type"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProAlertPopup;->dataType:I

    .line 28
    const-string/jumbo v1, "pick_result"

    const-wide/16 v2, -0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProAlertPopup;->rowId:J

    .line 29
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProAlertPopup;->showExceedRecordingTimePopup()V

    .line 33
    :goto_0
    return-void

    .line 31
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProAlertPopup;->finish()V

    goto :goto_0
.end method
