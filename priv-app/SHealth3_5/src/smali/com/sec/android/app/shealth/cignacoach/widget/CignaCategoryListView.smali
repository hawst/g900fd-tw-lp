.class public Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;
.super Landroid/widget/ListView;
.source "CignaCategoryListView.java"

# interfaces
.implements Lcom/sec/android/app/shealth/cignacoach/widget/CignaCaterogyListAdapter$MorePagesListener;


# static fields
.field public static final TAG:Ljava/lang/String;


# instance fields
.field footerViewAttached:Z

.field listFooter:Landroid/view/View;

.field private mHeaderView:Landroid/view/View;

.field private mHeaderViewHeight:I

.field private mHeaderViewVisible:Z

.field private mHeaderViewWidth:I

.field private mListAdapter:Lcom/sec/android/app/shealth/cignacoach/widget/CignaCaterogyListAdapter;

.field private mMoreLayout:Landroid/widget/LinearLayout;

.field private mPreventScrollWhenListUpdating:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 79
    invoke-direct {p0, p1}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    .line 25
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;->footerViewAttached:Z

    .line 35
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;->mPreventScrollWhenListUpdating:Z

    .line 80
    const/16 v0, 0x3e8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;->setScrollBarDefaultDelayBeforeFade(I)V

    .line 81
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v0, 0x0

    .line 84
    invoke-direct {p0, p1, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 25
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;->footerViewAttached:Z

    .line 35
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;->mPreventScrollWhenListUpdating:Z

    .line 85
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v0, 0x0

    .line 88
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 25
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;->footerViewAttached:Z

    .line 35
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;->mPreventScrollWhenListUpdating:Z

    .line 89
    return-void
.end method


# virtual methods
.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 71
    invoke-super {p0, p1}, Landroid/widget/ListView;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 72
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;->mHeaderViewVisible:Z

    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;->mHeaderView:Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;->getDrawingTime()J

    move-result-wide v1

    invoke-virtual {p0, p1, v0, v1, v2}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    .line 75
    :cond_0
    return-void
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 179
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;->mPreventScrollWhenListUpdating:Z

    if-eqz v0, :cond_0

    .line 180
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CignaCategoryListView updating, key event prenvent: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;->mPreventScrollWhenListUpdating:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    const/4 v0, 0x1

    .line 183
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/ListView;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 169
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;->mPreventScrollWhenListUpdating:Z

    if-eqz v0, :cond_0

    .line 170
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CignaCategoryListView updating, touch event: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;->mPreventScrollWhenListUpdating:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    const/4 v0, 0x1

    .line 173
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/ListView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public dispatchWindowFocusChanged(Z)V
    .locals 3
    .param p1, "focus"    # Z

    .prologue
    .line 189
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;->mPreventScrollWhenListUpdating:Z

    if-eqz v0, :cond_0

    .line 190
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CignaCategoryListView updating, focus change prenvent: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;->mPreventScrollWhenListUpdating:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 194
    :goto_0
    return-void

    .line 193
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/ListView;->dispatchWindowFocusChanged(Z)V

    goto :goto_0
.end method

.method public bridge synthetic getAdapter()Landroid/widget/Adapter;
    .locals 1

    .prologue
    .line 21
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;->getAdapter()Lcom/sec/android/app/shealth/cignacoach/widget/CignaCaterogyListAdapter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getAdapter()Landroid/widget/ListAdapter;
    .locals 1

    .prologue
    .line 21
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;->getAdapter()Lcom/sec/android/app/shealth/cignacoach/widget/CignaCaterogyListAdapter;

    move-result-object v0

    return-object v0
.end method

.method public getAdapter()Lcom/sec/android/app/shealth/cignacoach/widget/CignaCaterogyListAdapter;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;->mListAdapter:Lcom/sec/android/app/shealth/cignacoach/widget/CignaCaterogyListAdapter;

    return-object v0
.end method

.method public getLoadingView()Landroid/view/View;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;->listFooter:Landroid/view/View;

    return-object v0
.end method

.method public noMorePages()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 128
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;->listFooter:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 129
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;->listFooter:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;->removeFooterView(Landroid/view/View;)Z

    .line 131
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;->mListAdapter:Lcom/sec/android/app/shealth/cignacoach/widget/CignaCaterogyListAdapter;

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCaterogyListAdapter;->mLoadComplete:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 132
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;->mMoreLayout:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 133
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;->mListAdapter:Lcom/sec/android/app/shealth/cignacoach/widget/CignaCaterogyListAdapter;

    iput-boolean v2, v0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCaterogyListAdapter;->mLoadComplete:Z

    .line 136
    :cond_0
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;->footerViewAttached:Z

    .line 138
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 4
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    const/4 v3, 0x0

    .line 62
    invoke-super/range {p0 .. p5}, Landroid/widget/ListView;->onLayout(ZIIII)V

    .line 63
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;->mHeaderView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;->mHeaderView:Landroid/view/View;

    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;->mHeaderViewWidth:I

    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;->mHeaderViewHeight:I

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/view/View;->layout(IIII)V

    .line 66
    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 1
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 52
    invoke-super {p0, p1, p2}, Landroid/widget/ListView;->onMeasure(II)V

    .line 53
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;->mHeaderView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 54
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;->mHeaderView:Landroid/view/View;

    invoke-virtual {p0, v0, p1, p2}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;->measureChild(Landroid/view/View;II)V

    .line 55
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;->mHeaderView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;->mHeaderViewWidth:I

    .line 56
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;->mHeaderView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;->mHeaderViewHeight:I

    .line 58
    :cond_0
    return-void
.end method

.method public bridge synthetic setAdapter(Landroid/widget/Adapter;)V
    .locals 0
    .param p1, "x0"    # Landroid/widget/Adapter;

    .prologue
    .line 21
    check-cast p1, Landroid/widget/ListAdapter;

    .end local p1    # "x0":Landroid/widget/Adapter;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .locals 4
    .param p1, "adapter"    # Landroid/widget/ListAdapter;

    .prologue
    const/4 v2, 0x0

    .line 101
    instance-of v1, p1, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCaterogyListAdapter;

    if-nez v1, :cond_0

    .line 102
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-class v3, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " must use adapter of type "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-class v3, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCaterogyListAdapter;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 106
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;->mListAdapter:Lcom/sec/android/app/shealth/cignacoach/widget/CignaCaterogyListAdapter;

    if-eqz v1, :cond_1

    .line 107
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;->mListAdapter:Lcom/sec/android/app/shealth/cignacoach/widget/CignaCaterogyListAdapter;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCaterogyListAdapter;->setMorePagesListener(Lcom/sec/android/app/shealth/cignacoach/widget/CignaCaterogyListAdapter$MorePagesListener;)V

    .line 108
    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    :cond_1
    move-object v1, p1

    .line 111
    check-cast v1, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCaterogyListAdapter;

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;->mListAdapter:Lcom/sec/android/app/shealth/cignacoach/widget/CignaCaterogyListAdapter;

    move-object v1, p1

    .line 112
    check-cast v1, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCaterogyListAdapter;

    invoke-virtual {v1, p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCaterogyListAdapter;->setMorePagesListener(Lcom/sec/android/app/shealth/cignacoach/widget/CignaCaterogyListAdapter$MorePagesListener;)V

    move-object v1, p1

    .line 113
    check-cast v1, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCaterogyListAdapter;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 115
    new-instance v0, Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 116
    .local v0, "dummy":Landroid/view/View;
    invoke-super {p0, v0}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;)V

    .line 117
    invoke-super {p0, p1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 118
    invoke-super {p0, v0}, Landroid/widget/ListView;->removeFooterView(Landroid/view/View;)Z

    .line 119
    return-void
.end method

.method public setHeaderView(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 38
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;->mHeaderView:Landroid/view/View;

    .line 40
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;->mHeaderView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 41
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;->setFadingEdgeLength(I)V

    .line 43
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;->requestLayout()V

    .line 44
    return-void
.end method

.method public setLoadingView(Landroid/view/View;)V
    .locals 0
    .param p1, "listFooter"    # Landroid/view/View;

    .prologue
    .line 92
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;->listFooter:Landroid/view/View;

    .line 93
    return-void
.end method

.method public setMoreLayout(Landroid/widget/LinearLayout;)V
    .locals 0
    .param p1, "moreLayout"    # Landroid/widget/LinearLayout;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;->mMoreLayout:Landroid/widget/LinearLayout;

    .line 48
    return-void
.end method

.method public setPreventScroll(Z)V
    .locals 0
    .param p1, "preventScroll"    # Z

    .prologue
    .line 164
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;->mPreventScrollWhenListUpdating:Z

    .line 165
    return-void
.end method
