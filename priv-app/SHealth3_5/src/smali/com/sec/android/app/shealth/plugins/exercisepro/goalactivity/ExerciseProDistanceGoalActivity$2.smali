.class Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity$2;
.super Ljava/lang/Object;
.source "ExerciseProDistanceGoalActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->customizeActionBar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;)V
    .locals 0

    .prologue
    .line 153
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 156
    move-object v0, p1

    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;

    .line 158
    .local v0, "ab":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;->getActionButtonIndex()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 171
    :goto_0
    return-void

    .line 160
    :pswitch_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->isDestroyed()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->distanceInputModule:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->access$100(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 161
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;

    const-string v3, "input_method"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    .line 162
    .local v1, "imm":Landroid/view/inputmethod/InputMethodManager;
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->distanceInputModule:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->access$100(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;->getValueEditText()Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 165
    .end local v1    # "imm":Landroid/view/inputmethod/InputMethodManager;
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->checkChangesAndExit()V
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;)V

    goto :goto_0

    .line 168
    :pswitch_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->checkDataAndSave()V
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;)V

    goto :goto_0

    .line 158
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
