.class Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$WearableSyncEventListener;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$SyncEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "WearableSyncEventListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$WearableSyncEventListener;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$WearableSyncEventListener;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)V

    return-void
.end method


# virtual methods
.method public onSyncDeviceJoined()V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$WearableSyncEventListener;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    iget-object v0, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->handler:Landroid/os/Handler;

    const/16 v1, 0x6f

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$WearableSyncEventListener;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->handler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public onSyncDeviceLeft()V
    .locals 0

    return-void
.end method

.method public onSyncStopped(ILcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;)V
    .locals 13

    const-wide/16 v11, 0x0

    const-wide v9, -0x3ebccf113c3cfbafL    # -2515421.5293889423

    const/4 v8, 0x1

    const/16 v7, 0x20

    const/4 v3, 0x0

    const/4 v0, 0x2

    new-array v2, v0, [J

    const-wide/16 v0, 0x1

    aput-wide v0, v2, v8

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_0

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    int-to-long v0, p1

    shl-long/2addr v0, v7

    ushr-long v4, v0, v7

    aget-wide v0, v2, v3

    cmp-long v6, v0, v11

    if-eqz v6, :cond_1

    xor-long/2addr v0, v9

    :cond_1
    ushr-long/2addr v0, v7

    shl-long/2addr v0, v7

    xor-long/2addr v0, v4

    xor-long/2addr v0, v9

    aput-wide v0, v2, v3

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_4

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-array v1, v8, [I

    const/16 v0, -0x24

    aput v0, v1, v3

    new-array v0, v8, [I

    const/16 v2, -0x14

    aput v2, v0, v3

    move v2, v3

    :goto_0
    array-length v5, v0

    if-lt v2, v5, :cond_2

    array-length v0, v1

    new-array v0, v0, [C

    :goto_1
    array-length v2, v0

    if-lt v3, v2, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_2
    aget v5, v0, v2

    aget v6, v1, v2

    xor-int/2addr v5, v6

    aput v5, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    aget v2, v1, v3

    int-to-char v2, v2

    aput-char v2, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_4
    aget-wide v0, v2, v3

    cmp-long v4, v0, v11

    if-eqz v4, :cond_5

    xor-long/2addr v0, v9

    :cond_5
    shl-long/2addr v0, v7

    shr-long/2addr v0, v7

    long-to-int v0, v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$WearableSyncEventListener;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    iget-object v0, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->handler:Landroid/os/Handler;

    const/16 v1, 0x73

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    iput-object p2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$WearableSyncEventListener;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->handler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :goto_2
    return-void

    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$WearableSyncEventListener;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    iget-object v0, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->handler:Landroid/os/Handler;

    const/16 v1, 0x70

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v4

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_9

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-array v1, v8, [I

    const/16 v0, 0x1113

    aput v0, v1, v3

    new-array v0, v8, [I

    const/16 v2, 0x1123

    aput v2, v0, v3

    move v2, v3

    :goto_3
    array-length v5, v0

    if-lt v2, v5, :cond_7

    array-length v0, v1

    new-array v0, v0, [C

    :goto_4
    array-length v2, v0

    if-lt v3, v2, :cond_8

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_7
    aget v5, v0, v2

    aget v6, v1, v2

    xor-int/2addr v5, v6

    aput v5, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_8
    aget v2, v1, v3

    int-to-char v2, v2

    aput-char v2, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    :cond_9
    aget-wide v0, v2, v3

    cmp-long v2, v0, v11

    if-eqz v2, :cond_a

    xor-long/2addr v0, v9

    :cond_a
    shl-long/2addr v0, v7

    shr-long/2addr v0, v7

    long-to-int v0, v0

    iput v0, v4, Landroid/os/Message;->arg1:I

    iput-object p2, v4, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$WearableSyncEventListener;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    iget-object v0, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_2
.end method
