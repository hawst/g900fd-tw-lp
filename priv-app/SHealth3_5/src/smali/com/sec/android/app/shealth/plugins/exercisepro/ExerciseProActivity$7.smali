.class Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$7;
.super Ljava/lang/Object;
.source "ExerciseProActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;)V
    .locals 0

    .prologue
    .line 897
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 901
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mExerciseProHRMFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->access$700(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;)Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    move-result-object v1

    if-nez v1, :cond_0

    .line 902
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    new-instance v2, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    iget-object v3, v3, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mExerciseProActivity:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    invoke-direct {v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;-><init>(Landroid/app/Activity;)V

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mExerciseProHRMFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->access$702(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    .line 903
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mExerciseProHRMFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->access$700(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;)Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    iget-object v2, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mFragmentHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->setFragmentHandler(Landroid/os/Handler;)V

    .line 905
    if-eqz p1, :cond_0

    .line 906
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "heartrate_warning_checked"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_1

    .line 907
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->showInfomationDialog()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->access$800(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;)V

    .line 919
    :cond_0
    :goto_0
    return-void

    .line 909
    :cond_1
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 910
    .local v0, "msg":Landroid/os/Message;
    const/4 v1, 0x1

    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 911
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mExerciseProHRMFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->access$700(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;)Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    move-result-object v2

    const-string v3, "ExerciseProHRMFragment"

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->addTransparentFragment(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;Ljava/lang/String;)V
    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->access$900(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;Ljava/lang/String;)V

    .line 913
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    iget-object v1, v1, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mFragmentHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 914
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    iget-object v1, v1, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mFragmentHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0
.end method
