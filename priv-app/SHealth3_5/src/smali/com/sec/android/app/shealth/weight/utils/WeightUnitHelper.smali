.class public Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;
.super Ljava/lang/Object;
.source "WeightUnitHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;,
        Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;
    }
.end annotation


# instance fields
.field private mUnitHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    invoke-direct {v0, p1}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->mUnitHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    .line 46
    return-void
.end method

.method public static convertInchToCm(F)F
    .locals 1
    .param p0, "valueInInches"    # F

    .prologue
    .line 243
    invoke-static {p0}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->convertInchToCm(F)F

    move-result v0

    return v0
.end method

.method public static convertKgToLb(F)F
    .locals 1
    .param p0, "valueInKg"    # F

    .prologue
    .line 232
    invoke-static {p0}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->convertKgToLb(F)F

    move-result v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->roundToTenth(F)F

    move-result v0

    return v0
.end method

.method public static convertKgToUnit(FLcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;)F
    .locals 1
    .param p0, "valueInKg"    # F
    .param p1, "unit"    # Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    .prologue
    .line 203
    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->convertKgToUnit(FLcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;I)F

    move-result v0

    return v0
.end method

.method public static convertKgToUnit(FLcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;I)F
    .locals 2
    .param p0, "valueInKg"    # F
    .param p1, "unit"    # Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;
    .param p2, "numberOfDigits"    # I

    .prologue
    .line 217
    move v0, p0

    .line 218
    .local v0, "outputValue":F
    sget-object v1, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;->KG_UNIT:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    if-eq p1, v1, :cond_0

    .line 219
    invoke-static {p0}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->convertKgToLb(F)F

    move-result v0

    .line 221
    :cond_0
    invoke-static {v0, p2}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->roundToDigit(FI)F

    move-result v1

    return v1
.end method

.method public static convertUnitToKg(FLcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;)F
    .locals 1
    .param p0, "value"    # F
    .param p1, "unit"    # Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    .prologue
    .line 187
    sget-object v0, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;->KG_UNIT:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    if-eq p1, v0, :cond_0

    .line 188
    invoke-static {p0}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->convertLbToKg(F)F

    move-result p0

    .line 190
    .end local p0    # "value":F
    :cond_0
    return p0
.end method

.method private getHeightTypeByName(Ljava/lang/String;)Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;
    .locals 5
    .param p1, "unitName"    # Ljava/lang/String;

    .prologue
    .line 151
    invoke-static {}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;->values()[Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;

    move-result-object v0

    .local v0, "arr$":[Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    .line 152
    .local v1, "heightType":Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;
    # getter for: Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;->mUntranslatableName:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;->access$100(Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 157
    .end local v1    # "heightType":Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;
    :goto_1
    return-object v1

    .line 151
    .restart local v1    # "heightType":Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 156
    .end local v1    # "heightType":Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;
    :cond_1
    sget-object v4, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;->CM_UNIT:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->putHeightUnit(Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;)V

    .line 157
    sget-object v1, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;->CM_UNIT:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;

    goto :goto_1
.end method

.method public static getLatestDataTimestamp(Ljava/lang/String;J)J
    .locals 7
    .param p0, "format"    # Ljava/lang/String;
    .param p1, "dateTime"    # J

    .prologue
    .line 255
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "select sample_time from weight where strftime(\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\",("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "sample_time"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/1000),\'unixepoch\', \'localtime\') = strftime(\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\",("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/1000),\'unixepoch\', \'localtime\') order by "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "sample_time"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " desc limit 1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 258
    .local v3, "query":Ljava/lang/String;
    const/4 v6, 0x0

    .line 261
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 262
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 264
    const-string/jumbo v0, "sample_time"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 269
    if-eqz v6, :cond_0

    .line 270
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 272
    :cond_0
    :goto_0
    return-wide v0

    .line 269
    :cond_1
    if-eqz v6, :cond_2

    .line 270
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 272
    :cond_2
    const-wide/16 v0, 0x0

    goto :goto_0

    .line 269
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    .line 270
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method private getWeightTypeByName(Ljava/lang/String;)Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;
    .locals 5
    .param p1, "unitName"    # Ljava/lang/String;

    .prologue
    .line 132
    invoke-static {}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;->values()[Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    move-result-object v0

    .local v0, "arr$":[Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 133
    .local v3, "weightType":Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;
    # getter for: Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;->mUntranslatableName:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;->access$000(Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 138
    .end local v3    # "weightType":Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;
    :goto_1
    return-object v3

    .line 132
    .restart local v3    # "weightType":Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 137
    .end local v3    # "weightType":Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;
    :cond_1
    sget-object v4, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;->KG_UNIT:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->putWeightUnit(Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;)V

    .line 138
    sget-object v3, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;->KG_UNIT:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    goto :goto_1
.end method


# virtual methods
.method public getHeightUnit()Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->mUnitHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->getHeightUnit()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->getHeightTypeByName(Ljava/lang/String;)Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;

    move-result-object v0

    return-object v0
.end method

.method public getWeightUnit()Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->mUnitHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->getWeightUnit()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->getWeightTypeByName(Ljava/lang/String;)Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    move-result-object v0

    return-object v0
.end method

.method public putHeightUnit(Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;)V
    .locals 2
    .param p1, "heightUnit"    # Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;

    .prologue
    .line 175
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->mUnitHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    # getter for: Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;->mUntranslatableName:Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;->access$100(Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->putHeightunit(Ljava/lang/String;)V

    .line 176
    return-void
.end method

.method public putWeightUnit(Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;)V
    .locals 2
    .param p1, "weightUnit"    # Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->mUnitHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    # getter for: Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;->mUntranslatableName:Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;->access$000(Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->putweightUnit(Ljava/lang/String;)V

    .line 167
    return-void
.end method
