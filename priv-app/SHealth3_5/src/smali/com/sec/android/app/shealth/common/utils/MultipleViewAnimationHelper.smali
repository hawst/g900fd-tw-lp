.class public Lcom/sec/android/app/shealth/common/utils/MultipleViewAnimationHelper;
.super Ljava/lang/Object;
.source "MultipleViewAnimationHelper.java"


# instance fields
.field private mAnimation:Landroid/view/animation/Animation;

.field private mViewSet:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Collection;Landroid/view/animation/Animation;)V
    .locals 2
    .param p2, "animation"    # Landroid/view/animation/Animation;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Landroid/view/View;",
            ">;",
            "Landroid/view/animation/Animation;",
            ")V"
        }
    .end annotation

    .prologue
    .line 27
    .local p1, "viewSet":Ljava/util/Collection;, "Ljava/util/Collection<Landroid/view/View;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 29
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument\'s can\'t be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 31
    :cond_1
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/utils/MultipleViewAnimationHelper;->mViewSet:Ljava/util/Collection;

    .line 32
    iput-object p2, p0, Lcom/sec/android/app/shealth/common/utils/MultipleViewAnimationHelper;->mAnimation:Landroid/view/animation/Animation;

    .line 33
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 3

    .prologue
    .line 49
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/utils/MultipleViewAnimationHelper;->mViewSet:Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 50
    .local v1, "view":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->clearAnimation()V

    goto :goto_0

    .line 52
    .end local v1    # "view":Landroid/view/View;
    :cond_0
    return-void
.end method

.method public start()V
    .locals 3

    .prologue
    .line 39
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/utils/MultipleViewAnimationHelper;->mViewSet:Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 40
    .local v1, "view":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->clearAnimation()V

    .line 41
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/utils/MultipleViewAnimationHelper;->mAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v1, v2}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 43
    .end local v1    # "view":Landroid/view/View;
    :cond_0
    return-void
.end method
