.class public Lcom/sec/android/app/shealth/UpdateCheckActivity;
.super Landroid/support/v4/app/FragmentActivity;
.source "UpdateCheckActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IBackPressControllerProvider;
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/UpdateCheckActivity$3;,
        Lcom/sec/android/app/shealth/UpdateCheckActivity$UpdateServiceBackPressController;,
        Lcom/sec/android/app/shealth/UpdateCheckActivity$UpdateServiceDialogButtonController;
    }
.end annotation


# static fields
.field private static final UPDATE_APP:Ljava/lang/String; = "update app"


# instance fields
.field private intent:Landroid/content/Intent;

.field private final mBackPressMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/shealth/common/commonui/dialog/IBackPressController;",
            ">;"
        }
    .end annotation
.end field

.field private final mDialogControllerMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;",
            ">;"
        }
    .end annotation
.end field

.field private mIsForceUpdate:Z

.field private mIsFromInitialSettings:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 40
    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    .line 45
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/UpdateCheckActivity;->mIsForceUpdate:Z

    .line 46
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/UpdateCheckActivity;->mIsFromInitialSettings:Z

    .line 52
    new-instance v0, Lcom/sec/android/app/shealth/UpdateCheckActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/UpdateCheckActivity$1;-><init>(Lcom/sec/android/app/shealth/UpdateCheckActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/UpdateCheckActivity;->mDialogControllerMap:Ljava/util/Map;

    .line 64
    new-instance v0, Lcom/sec/android/app/shealth/UpdateCheckActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/UpdateCheckActivity$2;-><init>(Lcom/sec/android/app/shealth/UpdateCheckActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/UpdateCheckActivity;->mBackPressMap:Ljava/util/Map;

    .line 197
    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/UpdateCheckActivity;)Landroid/content/Intent;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/UpdateCheckActivity;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/sec/android/app/shealth/UpdateCheckActivity;->getIntentForSHealthUpdate()Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/UpdateCheckActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/UpdateCheckActivity;

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/UpdateCheckActivity;->mIsForceUpdate:Z

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/UpdateCheckActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/UpdateCheckActivity;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/sec/android/app/shealth/UpdateCheckActivity;->destoryApp()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/UpdateCheckActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/UpdateCheckActivity;

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/UpdateCheckActivity;->mIsFromInitialSettings:Z

    return v0
.end method

.method private destoryApp()V
    .locals 3

    .prologue
    .line 165
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/UpdateCheckActivity;->mIsFromInitialSettings:Z

    if-eqz v1, :cond_0

    .line 166
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 170
    .local v0, "intent":Landroid/content/Intent;
    :goto_0
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 171
    const/high16 v1, 0x20000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 172
    const-string v1, "Restart_after_update"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 173
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/UpdateCheckActivity;->startActivity(Landroid/content/Intent;)V

    .line 174
    return-void

    .line 168
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/home/HomeActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .restart local v0    # "intent":Landroid/content/Intent;
    goto :goto_0
.end method

.method private getIntentForSHealthUpdate()Landroid/content/Intent;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 177
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 178
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.sec.android.app.samsungapps"

    const-string v2, "com.sec.android.app.samsungapps.Main"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 179
    const-string v1, "directcall"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 180
    const-string v1, "CallerType"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 181
    const-string v1, "GUID"

    const-string v2, "com.sec.android.app.shealth"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 182
    const v1, 0x14000020

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 183
    return-object v0
.end method


# virtual methods
.method public getBackPressController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IBackPressController;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 189
    iget-object v0, p0, Lcom/sec/android/app/shealth/UpdateCheckActivity;->mBackPressMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/commonui/dialog/IBackPressController;

    return-object v0
.end method

.method public getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 109
    iget-object v0, p0, Lcom/sec/android/app/shealth/UpdateCheckActivity;->mDialogControllerMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v4, 0x7f0908b2

    const/4 v3, 0x0

    .line 75
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 77
    const v1, 0x7f030018

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/UpdateCheckActivity;->setContentView(I)V

    .line 79
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/UpdateCheckActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/UpdateCheckActivity;->intent:Landroid/content/Intent;

    .line 81
    iget-object v1, p0, Lcom/sec/android/app/shealth/UpdateCheckActivity;->intent:Landroid/content/Intent;

    const-string v2, "FORCE_UPDATE"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 82
    iget-object v1, p0, Lcom/sec/android/app/shealth/UpdateCheckActivity;->intent:Landroid/content/Intent;

    const-string v2, "FORCE_UPDATE"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/UpdateCheckActivity;->mIsForceUpdate:Z

    .line 84
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/UpdateCheckActivity;->intent:Landroid/content/Intent;

    const-string v2, "FROM_INITIAL_SETTINGS"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 85
    iget-object v1, p0, Lcom/sec/android/app/shealth/UpdateCheckActivity;->intent:Landroid/content/Intent;

    const-string v2, "FROM_INITIAL_SETTINGS"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/UpdateCheckActivity;->mIsFromInitialSettings:Z

    .line 88
    :cond_1
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const/4 v1, 0x2

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 89
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    const v1, 0x7f090019

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 90
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/UpdateCheckActivity;->mIsForceUpdate:Z

    if-eqz v1, :cond_2

    .line 91
    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/UpdateCheckActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 92
    const v1, 0x7f090048

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 93
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setDoNotDismissOnBackPressed(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 99
    :goto_0
    const v1, 0x7f090056

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 101
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/UpdateCheckActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string/jumbo v3, "update app"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 103
    return-void

    .line 95
    :cond_2
    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/UpdateCheckActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 96
    const v1, 0x7f09004c

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 97
    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setDoNotDismissOnBackPressed(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    goto :goto_0
.end method
