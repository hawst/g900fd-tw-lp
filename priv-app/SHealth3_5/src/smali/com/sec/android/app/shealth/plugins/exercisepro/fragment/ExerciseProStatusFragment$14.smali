.class Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$14;
.super Ljava/lang/Object;
.source "ExerciseProStatusFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->setDefaultBackground()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)V
    .locals 0

    .prologue
    .line 3683
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$14;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const v3, 0x7f080701

    const/16 v2, 0x8

    .line 3686
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$14;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$300(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getCurrentMode()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 3687
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$14;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$300(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getWorkoutState()I

    move-result v0

    const/16 v1, 0x7d0

    if-eq v0, v1, :cond_1

    .line 3688
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$14;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->mMapView:Lcom/google/android/gms/maps/MapView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$3900(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)Lcom/google/android/gms/maps/MapView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/maps/MapView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 3689
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$14;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$14;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->mView:Landroid/view/View;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$4600(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)Landroid/view/View;

    move-result-object v1

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->setUpMapIfNeeded(Landroid/view/View;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$4000(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;Landroid/view/View;)V

    .line 3691
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$14;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->mMapView:Lcom/google/android/gms/maps/MapView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$3900(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)Lcom/google/android/gms/maps/MapView;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/android/gms/maps/MapView;->setVisibility(I)V

    .line 3692
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$14;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->mDatas_layout:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$6500(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 3702
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$14;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->mDatas_layout:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$6500(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)Landroid/view/View;

    move-result-object v0

    const v1, -0xa0a0b

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 3703
    return-void

    .line 3694
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$14;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->mMapView:Lcom/google/android/gms/maps/MapView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$3900(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)Lcom/google/android/gms/maps/MapView;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/gms/maps/MapView;->setVisibility(I)V

    .line 3695
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$14;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->mDatas_layout:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$6500(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 3699
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$14;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->mMapView:Lcom/google/android/gms/maps/MapView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$3900(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)Lcom/google/android/gms/maps/MapView;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/gms/maps/MapView;->setVisibility(I)V

    .line 3700
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$14;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->mDatas_layout:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$6500(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method
