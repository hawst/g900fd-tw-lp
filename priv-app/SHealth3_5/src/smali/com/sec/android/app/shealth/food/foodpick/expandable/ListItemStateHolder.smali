.class Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItemStateHolder;
.super Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;
.source "ListItemStateHolder.java"


# instance fields
.field private mIsExpanded:Z


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;)V
    .locals 0
    .param p1, "category"    # Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;-><init>()V

    .line 24
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItemStateHolder;->initFromInstance(Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;)V

    .line 25
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 37
    instance-of v0, p1, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItemStateHolder;

    if-nez v0, :cond_0

    move v0, v1

    .line 43
    :goto_0
    return v0

    .line 40
    :cond_0
    iget-boolean v2, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItemStateHolder;->mIsExpanded:Z

    move-object v0, p1

    check-cast v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItemStateHolder;

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItemStateHolder;->mIsExpanded:Z

    if-eq v2, v0, :cond_1

    move v0, v1

    .line 41
    goto :goto_0

    .line 43
    :cond_1
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public isExpanded()Z
    .locals 1

    .prologue
    .line 28
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItemStateHolder;->mIsExpanded:Z

    return v0
.end method

.method public setExpanded(Z)V
    .locals 0
    .param p1, "isExpanded"    # Z

    .prologue
    .line 32
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItemStateHolder;->mIsExpanded:Z

    .line 33
    return-void
.end method
