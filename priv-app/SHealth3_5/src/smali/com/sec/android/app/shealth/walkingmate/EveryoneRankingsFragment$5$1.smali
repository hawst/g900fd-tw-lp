.class Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$5$1;
.super Ljava/lang/Object;
.source "EveryoneRankingsFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/ListPopup$OnItemClickedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$5;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$5;

.field final synthetic val$popup:Lcom/sec/android/app/shealth/common/commonui/ListPopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$5;Lcom/sec/android/app/shealth/common/commonui/ListPopup;)V
    .locals 0

    .prologue
    .line 381
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$5$1;->this$1:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$5;

    iput-object p2, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$5$1;->val$popup:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClicked(ILjava/lang/String;Landroid/widget/PopupWindow;)V
    .locals 5
    .param p1, "itemIndex"    # I
    .param p2, "itemContent"    # Ljava/lang/String;
    .param p3, "popupWindow"    # Landroid/widget/PopupWindow;

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 386
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$5$1;->this$1:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$5;

    iget-object v1, v1, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$5;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mUpdateTextView:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$800(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)Landroid/widget/TextView;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 387
    if-nez p1, :cond_1

    .line 388
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$5$1;->this$1:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$5;

    iget-object v1, v1, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$5;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mTV_dropdown_day_seleter:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$2500(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)Landroid/widget/TextView;

    move-result-object v1

    const v2, 0x7f090b9f

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 389
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$5$1;->this$1:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$5;

    iget-object v1, v1, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$5;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->period_select_idx:I
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$1902(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;I)I

    .line 400
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$5$1;->this$1:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$5;

    iget-object v1, v1, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$5;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mTV_dropdown_day_seleter:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$2500(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)Landroid/widget/TextView;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$5$1;->this$1:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$5;

    iget-object v3, v3, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$5;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mActivity:Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;
    invoke-static {v3}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$700(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$5$1;->this$1:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$5;

    iget-object v4, v4, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$5;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mTV_dropdown_day_seleter:Landroid/widget/TextView;
    invoke-static {v4}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$2500(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->convertToProperUnitsText(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0901ec

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 403
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$5$1;->this$1:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$5;

    iget-object v1, v1, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$5;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mTimeFilter:Landroid/view/View;
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$500(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 404
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$5$1;->this$1:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$5;

    iget-object v1, v1, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$5;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # invokes: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->showLoadingDialog()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$2600(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)V

    .line 405
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$5$1;->this$1:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$5;

    iget-object v1, v1, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$5;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # invokes: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->updateValues()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$2700(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)V

    .line 406
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$5$1;->val$popup:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->dismiss()V

    .line 407
    return-void

    .line 391
    :cond_1
    if-ne p1, v3, :cond_2

    .line 392
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$5$1;->this$1:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$5;

    iget-object v1, v1, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$5;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mTV_dropdown_day_seleter:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$2500(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)Landroid/widget/TextView;

    move-result-object v1

    const v2, 0x7f090b9e

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 393
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$5$1;->this$1:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$5;

    iget-object v1, v1, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$5;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # setter for: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->period_select_idx:I
    invoke-static {v1, v3}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$1902(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;I)I

    goto/16 :goto_0

    .line 395
    :cond_2
    if-ne p1, v4, :cond_0

    .line 396
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$5$1;->this$1:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$5;

    iget-object v2, v2, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$5;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090b9a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$5$1;->this$1:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$5;

    iget-object v2, v2, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$5;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090b9b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 397
    .local v0, "temp":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$5$1;->this$1:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$5;

    iget-object v1, v1, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$5;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mTV_dropdown_day_seleter:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$2500(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 398
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$5$1;->this$1:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$5;

    iget-object v1, v1, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$5;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # setter for: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->period_select_idx:I
    invoke-static {v1, v4}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$1902(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;I)I

    goto/16 :goto_0
.end method
