.class public Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;
.super Ljava/lang/Object;
.source "SharedPrefManager.java"


# static fields
.field private static final OBJ_LOCK:Ljava/lang/Object;

.field private static final SHAREDPREF_FILE_BACKUP_RESTORE:Ljava/lang/String; = "com.sec.android.app.shealth_preferences_backup_restore"

.field private static final TAG:Ljava/lang/String;

.field private static instance:Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;


# instance fields
.field private mBackupPref:Landroid/content/SharedPreferences;

.field private mPref:Landroid/content/SharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;->instance:Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;

    .line 16
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;->OBJ_LOCK:Ljava/lang/Object;

    .line 21
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;->mPref:Landroid/content/SharedPreferences;

    .line 34
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;->instance:Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;

    return-object v0
.end method


# virtual methods
.method public getBoolean(Ljava/lang/String;Z)Z
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "defValue"    # Z

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;->mPref:Landroid/content/SharedPreferences;

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getFloat(Ljava/lang/String;F)F
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "defValue"    # F

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;->mPref:Landroid/content/SharedPreferences;

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v0

    return v0
.end method

.method public getInt(Ljava/lang/String;I)I
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "defValue"    # I

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;->mPref:Landroid/content/SharedPreferences;

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "defValue"    # Ljava/lang/String;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;->mPref:Landroid/content/SharedPreferences;

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public initialize(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 26
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;->mPref:Landroid/content/SharedPreferences;

    .line 27
    const-string v0, "com.sec.android.app.shealth_preferences_backup_restore"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;->mBackupPref:Landroid/content/SharedPreferences;

    .line 28
    return-void
.end method

.method public setValue(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 5
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/Object;

    .prologue
    .line 103
    sget-object v2, Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;->OBJ_LOCK:Ljava/lang/Object;

    monitor-enter v2

    .line 105
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;->mPref:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 106
    .local v0, "prefsEdit":Landroid/content/SharedPreferences$Editor;
    instance-of v1, p2, Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 108
    check-cast p2, Ljava/lang/Boolean;

    .end local p2    # "value":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 126
    :cond_0
    :goto_0
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 127
    monitor-exit v2

    .line 128
    const/4 v1, 0x1

    return v1

    .line 110
    .restart local p2    # "value":Ljava/lang/Object;
    :cond_1
    instance-of v1, p2, Ljava/lang/Float;

    if-eqz v1, :cond_2

    .line 112
    check-cast p2, Ljava/lang/Float;

    .end local p2    # "value":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 127
    .end local v0    # "prefsEdit":Landroid/content/SharedPreferences$Editor;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 114
    .restart local v0    # "prefsEdit":Landroid/content/SharedPreferences$Editor;
    .restart local p2    # "value":Ljava/lang/Object;
    :cond_2
    :try_start_1
    instance-of v1, p2, Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 116
    check-cast p2, Ljava/lang/Integer;

    .end local p2    # "value":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 118
    .restart local p2    # "value":Ljava/lang/Object;
    :cond_3
    instance-of v1, p2, Ljava/lang/Long;

    if-eqz v1, :cond_4

    .line 120
    check-cast p2, Ljava/lang/Long;

    .end local p2    # "value":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-interface {v0, p1, v3, v4}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 122
    .restart local p2    # "value":Ljava/lang/Object;
    :cond_4
    instance-of v1, p2, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 124
    check-cast p2, Ljava/lang/String;

    .end local p2    # "value":Ljava/lang/Object;
    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method
