.class public Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView;
.super Landroid/widget/FrameLayout;
.source "DartVaderSpaceShipView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$1;,
        Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$HideAnimation;,
        Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderUpdater;,
        Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;
    }
.end annotation


# static fields
.field private static final DELAY:J = 0x82L

.field private static final NO_INDEX:I = -0x1

.field public static final STATES_COUNT:I = 0x5

.field private static final TRACK_DELAY:J = 0x19aL


# instance fields
.field private spaceCrafts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/widget/ImageView;",
            ">;"
        }
    .end annotation
.end field

.field private state:Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;

.field private updater:Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderUpdater;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 38
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 32
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;->NO_STATE:Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView;->state:Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;

    .line 35
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView;->updater:Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderUpdater;

    .line 39
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView;->init()V

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;->NO_STATE:Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView;->state:Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;

    .line 35
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView;->updater:Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderUpdater;

    .line 44
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView;->init()V

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 48
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;->NO_STATE:Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView;->state:Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;

    .line 35
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView;->updater:Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderUpdater;

    .line 49
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView;->init()V

    .line 50
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView;)Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView;->state:Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView;Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;)Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;

    .prologue
    .line 19
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView;->state:Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView;II)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView;->updateState(II)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView;->spaceCrafts:Ljava/util/List;

    return-object v0
.end method

.method private addSpaceCraft(I)V
    .locals 3
    .param p1, "src"    # I

    .prologue
    const/4 v2, -0x1

    .line 77
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 78
    .local v0, "spaceCraft":Landroid/widget/ImageView;
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 79
    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 80
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 81
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView;->addView(Landroid/view/View;)V

    .line 82
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView;->spaceCrafts:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 83
    return-void
.end method

.method private init()V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView;->initSpaceCrafts()V

    .line 57
    return-void
.end method

.method private initSpaceCrafts()V
    .locals 2

    .prologue
    .line 63
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView;->spaceCrafts:Ljava/util/List;

    .line 64
    const v0, 0x7f0203df

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView;->addSpaceCraft(I)V

    .line 65
    const v0, 0x7f0203e0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView;->addSpaceCraft(I)V

    .line 66
    const v0, 0x7f0203e1

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView;->addSpaceCraft(I)V

    .line 67
    const v0, 0x7f0203e2

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView;->addSpaceCraft(I)V

    .line 68
    const v0, 0x7f0203e3

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView;->addSpaceCraft(I)V

    .line 69
    return-void
.end method

.method private updateState(II)V
    .locals 3
    .param p1, "prevIndex"    # I
    .param p2, "curIndex"    # I

    .prologue
    const/4 v2, -0x1

    .line 107
    if-eq p1, v2, :cond_0

    .line 108
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView;->spaceCrafts:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    new-instance v1, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$HideAnimation;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$HideAnimation;-><init>(Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView;I)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 109
    :cond_0
    if-eq p2, v2, :cond_1

    .line 110
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView;->spaceCrafts:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 112
    :cond_1
    return-void
.end method


# virtual methods
.method public setState(Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;)V
    .locals 3
    .param p1, "dartVaderState"    # Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;

    .prologue
    .line 91
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView;->state:Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;

    if-eq v0, p1, :cond_1

    .line 92
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView;->updater:Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderUpdater;

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView;->updater:Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderUpdater;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderUpdater;->cancel(Z)Z

    .line 95
    :cond_0
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderUpdater;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderUpdater;-><init>(Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView;Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView;->updater:Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderUpdater;

    .line 96
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView;->updater:Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderUpdater;

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderUpdater;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 98
    :cond_1
    return-void
.end method
