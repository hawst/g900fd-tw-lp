.class Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity$3;
.super Ljava/lang/Object;
.source "FoodPickActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/OnSaveChosenItemListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;->getOnSaveChosenItemListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/OnSaveChosenItemListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;)V
    .locals 0

    .prologue
    .line 265
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity$3;->this$0:Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSave(I)V
    .locals 6
    .param p1, "index"    # I

    .prologue
    const/4 v5, 0x1

    .line 268
    if-ne p1, v5, :cond_0

    .line 269
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity$3;->this$0:Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;

    # invokes: Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;->showOkCancelDeleteMealDialog()V
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;->access$300(Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;)V

    .line 278
    :goto_0
    return-void

    .line 271
    :cond_0
    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity$3;->this$0:Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 272
    .local v0, "intent":Landroid/content/Intent;
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 273
    .local v1, "params":Landroid/os/Bundle;
    const-string v2, "MY_FOOD_INFO_DATA_ID"

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity$3;->this$0:Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;->mSelectedPanelHolder:Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;
    invoke-static {v3}, Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;->access$400(Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;)Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;->getId()J

    move-result-wide v3

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 274
    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 275
    const-string v2, "SHOULD_ALWAYS_SAVE_TO_MY_FOOD"

    invoke-virtual {v0, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 276
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity$3;->this$0:Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;

    const/16 v3, 0x4d2

    invoke-virtual {v2, v0, v3}, Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method
