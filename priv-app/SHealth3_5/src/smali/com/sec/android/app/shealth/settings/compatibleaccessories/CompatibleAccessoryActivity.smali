.class public Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "CompatibleAccessoryActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field allAppData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;",
            ">;"
        }
    .end annotation
.end field

.field appList:Landroid/widget/LinearLayout;

.field mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const-class v0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryActivity;->allAppData:Ljava/util/List;

    return-void
.end method

.method private isBloodGlucoseSupported()Z
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 86
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryDbManagerWithProvider;->getAllAppRegistryDatas(ZLandroid/content/Context;)Ljava/util/List;

    move-result-object v0

    .line 87
    .local v0, "enabledPlugIns":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 88
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-object v2, v2, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->appName:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0901ae

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v2, v3

    .line 92
    :goto_1
    return v2

    .line 87
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 92
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 2

    .prologue
    .line 97
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    .line 98
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const v1, 0x7f0907e7

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 99
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 9
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 104
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 106
    .local v1, "id":Ljava/lang/String;
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 108
    .local v2, "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090021

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_0

    .line 110
    const-string v3, "compatible_list_app_name"

    invoke-virtual {v2, v3, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 111
    new-array v0, v8, [I

    fill-array-data v0, :array_0

    .line 114
    .local v0, "deviceTypes":[I
    const-string v3, "compatible_list_type"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[I)Landroid/content/Intent;

    .line 132
    .end local v0    # "deviceTypes":[I
    :goto_0
    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryActivity;->startActivity(Landroid/content/Intent;)V

    .line 133
    return-void

    .line 117
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09002c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_1

    .line 119
    new-array v0, v6, [I

    const/16 v3, 0x2712

    aput v3, v0, v5

    .line 120
    .restart local v0    # "deviceTypes":[I
    const-string v3, "compatible_list_app_name"

    const/4 v4, 0x3

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 121
    const-string v3, "compatible_list_type"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[I)Landroid/content/Intent;

    goto :goto_0

    .line 123
    .end local v0    # "deviceTypes":[I
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0901ae

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_2

    .line 124
    new-array v0, v6, [I

    const/16 v3, 0x2714

    aput v3, v0, v5

    .line 125
    .restart local v0    # "deviceTypes":[I
    const-string v3, "compatible_list_app_name"

    invoke-virtual {v2, v3, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 126
    const-string v3, "compatible_list_type"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[I)Landroid/content/Intent;

    goto :goto_0

    .line 130
    .end local v0    # "deviceTypes":[I
    :cond_2
    const-string v3, "compatible_list_app_name"

    invoke-virtual {v2, v3, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0

    .line 111
    :array_0
    .array-data 4
        0x2718
        0x271d
        0x271b
        0x271f
    .end array-data
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 19
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 54
    invoke-super/range {p0 .. p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 55
    const v4, 0x7f03009b

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryActivity;->setContentView(I)V

    .line 56
    const v4, 0x7f0802dd

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryActivity;->appList:Landroid/widget/LinearLayout;

    .line 57
    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    const-string v2, "com.sec.android.app.shealth"

    const v4, 0x7f090021

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "true"

    const-string v5, "3.0.0.0014"

    const-string v6, "Functions"

    const/4 v7, 0x1

    const/4 v8, 0x1

    const/4 v9, 0x0

    const-wide/16 v10, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-direct/range {v1 .. v13}, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;JZI)V

    .line 58
    .local v1, "exercise":Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;
    new-instance v2, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    const-string v3, "com.sec.android.app.shealth"

    const v4, 0x7f09002c

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "true"

    const-string v6, "3.0.0.0014"

    const-string v7, "Functions"

    const/4 v8, 0x1

    const/4 v9, 0x1

    const/4 v10, 0x0

    const-wide/16 v11, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-direct/range {v2 .. v14}, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;JZI)V

    .line 59
    .local v2, "weight":Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryActivity;->allAppData:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 60
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryActivity;->allAppData:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 61
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryActivity;->isBloodGlucoseSupported()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 62
    new-instance v3, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    const-string v4, "com.sec.android.app.shealth"

    const v5, 0x7f0901ae

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "true"

    const-string v7, "3.0.0.0014"

    const-string v8, "Functions"

    const/4 v9, 0x1

    const/4 v10, 0x1

    const/4 v11, 0x0

    const-wide/16 v12, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-direct/range {v3 .. v15}, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;JZI)V

    .line 63
    .local v3, "bg":Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryActivity;->allAppData:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 68
    .end local v3    # "bg":Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;
    :cond_0
    const/16 v17, 0x0

    .local v17, "i":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryActivity;->allAppData:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    move/from16 v0, v17

    if-ge v0, v4, :cond_2

    .line 70
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryActivity;->allAppData:Ljava/util/List;

    move/from16 v0, v17

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-boolean v4, v4, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->hasAccessory:Z

    if-eqz v4, :cond_1

    .line 72
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "layout_inflater"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Landroid/view/LayoutInflater;

    .line 73
    .local v18, "inflater":Landroid/view/LayoutInflater;
    const v4, 0x7f030177

    const/4 v5, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/LinearLayout;

    .line 75
    .local v16, "appItem":Landroid/widget/LinearLayout;
    const v4, 0x7f08033a

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryActivity;->allAppData:Ljava/util/List;

    move/from16 v0, v17

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-object v5, v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->appName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 76
    const v4, 0x7f08033a

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryActivity;->allAppData:Ljava/util/List;

    move/from16 v0, v17

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-object v5, v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->appName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 77
    const v4, 0x7f08033a

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    move-object/from16 v0, p0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 79
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryActivity;->appList:Landroid/widget/LinearLayout;

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 68
    .end local v16    # "appItem":Landroid/widget/LinearLayout;
    .end local v18    # "inflater":Landroid/view/LayoutInflater;
    :cond_1
    add-int/lit8 v17, v17, 0x1

    goto/16 :goto_0

    .line 82
    :cond_2
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 137
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onResume()V

    .line 138
    return-void
.end method
