.class public Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil;
.super Ljava/lang/Object;
.source "ShareUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$SnsType;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    return-void
.end method

.method public static createChartShareView(Landroid/app/Activity;Landroid/graphics/Bitmap;)V
    .locals 15
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "mainContent"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v14, 0x1

    const/4 v13, -0x1

    .line 302
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/CoreUtil;->getAppContext()Landroid/content/Context;

    move-result-object v4

    .line 304
    .local v4, "context":Landroid/content/Context;
    const/4 v1, 0x0

    .line 305
    .local v1, "actionBarHeight":I
    new-instance v10, Landroid/util/TypedValue;

    invoke-direct {v10}, Landroid/util/TypedValue;-><init>()V

    .line 306
    .local v10, "tv":Landroid/util/TypedValue;
    invoke-virtual {v4}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v11

    const v12, 0x10102eb

    invoke-virtual {v11, v12, v10, v14}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 307
    iget v11, v10, Landroid/util/TypedValue;->data:I

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/TypedValue;->complexToDimensionPixelSize(ILandroid/util/DisplayMetrics;)I

    move-result v1

    .line 309
    :cond_0
    const/4 v9, 0x0

    .line 312
    .local v9, "rootLayout":Landroid/view/ViewGroup;
    const v11, 0x1020002

    :try_start_0
    invoke-virtual {p0, v11}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v11

    invoke-virtual {v11}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v11

    invoke-interface {v11}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v11

    move-object v0, v11

    check-cast v0, Landroid/view/ViewGroup;

    move-object v9, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 319
    :goto_0
    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v3, v13, v13}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 322
    .local v3, "contentLayoutParam":Landroid/widget/FrameLayout$LayoutParams;
    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v11

    const v12, 0x7f030005

    const/4 v13, 0x0

    invoke-virtual {v11, v12, v13}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v8

    .line 323
    .local v8, "mainView":Landroid/view/View;
    invoke-virtual {v9, v8, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 325
    const v11, 0x7f080027

    invoke-virtual {v8, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout;

    .line 326
    .local v2, "contentLayout":Landroid/widget/FrameLayout;
    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Landroid/widget/FrameLayout$LayoutParams;

    .line 327
    .local v7, "lp":Landroid/widget/FrameLayout$LayoutParams;
    iput v1, v7, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 328
    invoke-virtual {v2, v7}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 330
    const v11, 0x7f080028

    invoke-virtual {v8, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    const v12, 0x7f0900de

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setText(I)V

    .line 332
    new-instance v5, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-direct {v5, v11, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 333
    .local v5, "drawable":Landroid/graphics/drawable/BitmapDrawable;
    invoke-virtual {v5, v14}, Landroid/graphics/drawable/BitmapDrawable;->setAntiAlias(Z)V

    .line 334
    invoke-virtual {v2, v5}, Landroid/widget/FrameLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 335
    const/4 v11, 0x4

    invoke-virtual {v8, v11}, Landroid/view/View;->setVisibility(I)V

    .line 337
    new-instance v11, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$3;

    invoke-direct {v11, v8, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$3;-><init>(Landroid/view/View;Landroid/app/Activity;)V

    invoke-virtual {v8, v11}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 348
    return-void

    .line 314
    .end local v2    # "contentLayout":Landroid/widget/FrameLayout;
    .end local v3    # "contentLayoutParam":Landroid/widget/FrameLayout$LayoutParams;
    .end local v5    # "drawable":Landroid/graphics/drawable/BitmapDrawable;
    .end local v7    # "lp":Landroid/widget/FrameLayout$LayoutParams;
    .end local v8    # "mainView":Landroid/view/View;
    :catch_0
    move-exception v6

    .line 316
    .local v6, "e":Ljava/lang/Exception;
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static createShareView(Landroid/app/Activity;Landroid/graphics/Bitmap;IJILjava/util/List;)V
    .locals 20
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "mainContent"    # Landroid/graphics/Bitmap;
    .param p2, "titleId"    # I
    .param p3, "exerciseDate"    # J
    .param p5, "customHeight"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Landroid/graphics/Bitmap;",
            "IJI",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 39
    .local p6, "extraImageList":Ljava/util/List;, "Ljava/util/List<Landroid/net/Uri;>;"
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/CoreUtil;->getAppContext()Landroid/content/Context;

    move-result-object v7

    .line 41
    .local v7, "context":Landroid/content/Context;
    const/4 v4, 0x0

    .line 42
    .local v4, "actionBarHeight":I
    new-instance v13, Landroid/util/TypedValue;

    invoke-direct {v13}, Landroid/util/TypedValue;-><init>()V

    .line 43
    .local v13, "tv":Landroid/util/TypedValue;
    invoke-virtual {v7}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v17

    const v18, 0x10102eb

    const/16 v19, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v13, v2}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    move-result v17

    if-eqz v17, :cond_0

    .line 44
    iget v0, v13, Landroid/util/TypedValue;->data:I

    move/from16 v17, v0

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/TypedValue;->complexToDimensionPixelSize(ILandroid/util/DisplayMetrics;)I

    move-result v4

    .line 47
    :cond_0
    const/4 v12, 0x0

    .line 50
    .local v12, "rootLayout":Landroid/view/ViewGroup;
    const v17, 0x1020002

    :try_start_0
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v17

    move-object/from16 v0, v17

    check-cast v0, Landroid/view/ViewGroup;

    move-object v12, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 57
    :goto_0
    new-instance v6, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v17, -0x1

    const/16 v18, -0x1

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-direct {v6, v0, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 60
    .local v6, "contentLayoutParam":Landroid/widget/FrameLayout$LayoutParams;
    invoke-static {v7}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v17

    const v18, 0x7f0301b2

    const/16 v19, 0x0

    invoke-virtual/range {v17 .. v19}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v11

    .line 61
    .local v11, "mainView":Landroid/view/View;
    invoke-virtual {v12, v11, v6}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 75
    const v17, 0x7f080799

    move/from16 v0, v17

    invoke-virtual {v11, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/TextView;

    .line 76
    .local v15, "viewMainTitle":Landroid/widget/TextView;
    const v17, 0x7f0900de

    move/from16 v0, v17

    invoke-virtual {v15, v0}, Landroid/widget/TextView;->setText(I)V

    .line 78
    const v17, 0x7f08079a

    move/from16 v0, v17

    invoke-virtual {v11, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/TextView;

    .line 79
    .local v16, "viewMainTitle2":Landroid/widget/TextView;
    if-eqz p2, :cond_1

    .line 80
    move-object/from16 v0, v16

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 81
    :cond_1
    const v17, 0x7f08079b

    move/from16 v0, v17

    invoke-virtual {v11, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/TextView;

    .line 83
    .local v14, "viewDate":Landroid/widget/TextView;
    const-wide/16 v17, -0x1

    cmp-long v17, p3, v17

    if-nez v17, :cond_2

    .line 84
    const-string v17, ""

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 88
    :goto_1
    const v17, 0x7f080797

    move/from16 v0, v17

    invoke-virtual {v11, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/FrameLayout;

    .line 89
    .local v5, "contentLayout":Landroid/widget/FrameLayout;
    invoke-virtual {v5}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v10

    check-cast v10, Landroid/widget/FrameLayout$LayoutParams;

    .line 90
    .local v10, "lp":Landroid/widget/FrameLayout$LayoutParams;
    iput v4, v10, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 91
    invoke-virtual {v5, v10}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 93
    new-instance v8, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-direct {v8, v0, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 94
    .local v8, "drawable":Landroid/graphics/drawable/BitmapDrawable;
    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v8, v0}, Landroid/graphics/drawable/BitmapDrawable;->setAntiAlias(Z)V

    .line 95
    invoke-virtual {v5, v8}, Landroid/widget/FrameLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 97
    const/16 v17, 0x4

    move/from16 v0, v17

    invoke-virtual {v11, v0}, Landroid/view/View;->setVisibility(I)V

    .line 99
    new-instance v17, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$1;

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    move-object/from16 v2, p6

    move-object/from16 v3, p0

    invoke-direct {v0, v11, v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$1;-><init>(Landroid/view/View;Landroid/graphics/Bitmap;Ljava/util/List;Landroid/app/Activity;)V

    move-object/from16 v0, v17

    invoke-virtual {v11, v0}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 121
    return-void

    .line 52
    .end local v5    # "contentLayout":Landroid/widget/FrameLayout;
    .end local v6    # "contentLayoutParam":Landroid/widget/FrameLayout$LayoutParams;
    .end local v8    # "drawable":Landroid/graphics/drawable/BitmapDrawable;
    .end local v10    # "lp":Landroid/widget/FrameLayout$LayoutParams;
    .end local v11    # "mainView":Landroid/view/View;
    .end local v14    # "viewDate":Landroid/widget/TextView;
    .end local v15    # "viewMainTitle":Landroid/widget/TextView;
    .end local v16    # "viewMainTitle2":Landroid/widget/TextView;
    :catch_0
    move-exception v9

    .line 54
    .local v9, "e":Ljava/lang/Exception;
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 86
    .end local v9    # "e":Ljava/lang/Exception;
    .restart local v6    # "contentLayoutParam":Landroid/widget/FrameLayout$LayoutParams;
    .restart local v11    # "mainView":Landroid/view/View;
    .restart local v14    # "viewDate":Landroid/widget/TextView;
    .restart local v15    # "viewMainTitle":Landroid/widget/TextView;
    .restart local v16    # "viewMainTitle2":Landroid/widget/TextView;
    :cond_2
    invoke-static/range {p3 .. p4}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getDateInAndroidFormat(J)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public static createShareViewForSNS(Landroid/app/Activity;Landroid/graphics/Bitmap;IJILjava/util/List;Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$SnsType;)V
    .locals 23
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "mainContent"    # Landroid/graphics/Bitmap;
    .param p2, "titleId"    # I
    .param p3, "exerciseDate"    # J
    .param p5, "customHeight"    # I
    .param p7, "type"    # Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$SnsType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Landroid/graphics/Bitmap;",
            "IJI",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$SnsType;",
            ")V"
        }
    .end annotation

    .prologue
    .line 198
    .local p6, "extraImageList":Ljava/util/List;, "Ljava/util/List<Landroid/net/Uri;>;"
    if-nez p1, :cond_0

    .line 199
    new-instance v18, Landroid/content/Intent;

    const-string v2, "android.intent.action.SEND_MULTIPLE"

    move-object/from16 v0, v18

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 200
    .local v18, "sendingIntent":Landroid/content/Intent;
    const-string v2, "image/jpeg"

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 201
    const-string v2, "android.intent.extra.STREAM"

    check-cast p6, Ljava/util/ArrayList;

    .end local p6    # "extraImageList":Ljava/util/List;, "Ljava/util/List<Landroid/net/Uri;>;"
    move-object/from16 v0, v18

    move-object/from16 v1, p6

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 203
    const-string/jumbo v8, "theme"

    .line 204
    .local v8, "THEME_CHOOSER":Ljava/lang/String;
    const/4 v9, 0x2

    .line 206
    .local v9, "THEME_DEVICE_DEFAULT_LIGHT":I
    move-object/from16 v0, v18

    invoke-virtual {v0, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 207
    const-string v2, "com.facebook.katana"

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 208
    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f090033

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-static {v0, v2}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 298
    .end local v8    # "THEME_CHOOSER":Ljava/lang/String;
    .end local v9    # "THEME_DEVICE_DEFAULT_LIGHT":I
    .end local v18    # "sendingIntent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 211
    .restart local p6    # "extraImageList":Ljava/util/List;, "Ljava/util/List<Landroid/net/Uri;>;"
    :cond_0
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/CoreUtil;->getAppContext()Landroid/content/Context;

    move-result-object v13

    .line 213
    .local v13, "context":Landroid/content/Context;
    const/4 v10, 0x0

    .line 214
    .local v10, "actionBarHeight":I
    new-instance v19, Landroid/util/TypedValue;

    invoke-direct/range {v19 .. v19}, Landroid/util/TypedValue;-><init>()V

    .line 215
    .local v19, "tv":Landroid/util/TypedValue;
    invoke-virtual {v13}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    const v4, 0x10102eb

    const/4 v5, 0x1

    move-object/from16 v0, v19

    invoke-virtual {v2, v4, v0, v5}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 216
    move-object/from16 v0, v19

    iget v2, v0, Landroid/util/TypedValue;->data:I

    invoke-virtual {v13}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/TypedValue;->complexToDimensionPixelSize(ILandroid/util/DisplayMetrics;)I

    move-result v10

    .line 219
    :cond_1
    const/16 v17, 0x0

    .line 222
    .local v17, "rootLayout":Landroid/view/ViewGroup;
    const v2, 0x1020002

    :try_start_0
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Landroid/view/ViewGroup;

    move-object/from16 v17, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 229
    :goto_1
    new-instance v12, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v4, -0x1

    invoke-direct {v12, v2, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 232
    .local v12, "contentLayoutParam":Landroid/widget/FrameLayout$LayoutParams;
    invoke-static {v13}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v4, 0x7f0301b2

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 233
    .local v3, "mainView":Landroid/view/View;
    move-object/from16 v0, v17

    invoke-virtual {v0, v3, v12}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 235
    const v2, 0x7f080799

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v21

    check-cast v21, Landroid/widget/TextView;

    .line 236
    .local v21, "viewMainTitle":Landroid/widget/TextView;
    const v2, 0x7f0900de

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 238
    const v2, 0x7f08079a

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/TextView;

    .line 239
    .local v22, "viewMainTitle2":Landroid/widget/TextView;
    move-object/from16 v0, v22

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 240
    const v2, 0x7f08079b

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v20

    check-cast v20, Landroid/widget/TextView;

    .line 241
    .local v20, "viewDate":Landroid/widget/TextView;
    invoke-static/range {p3 .. p4}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getDateInAndroidFormat(J)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 243
    const v2, 0x7f080797

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/FrameLayout;

    .line 244
    .local v11, "contentLayout":Landroid/widget/FrameLayout;
    invoke-virtual {v11}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v16

    check-cast v16, Landroid/widget/FrameLayout$LayoutParams;

    .line 245
    .local v16, "lp":Landroid/widget/FrameLayout$LayoutParams;
    move-object/from16 v0, v16

    iput v10, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 246
    move-object/from16 v0, v16

    invoke-virtual {v11, v0}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 248
    new-instance v14, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v13}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-direct {v14, v2, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 249
    .local v14, "drawable":Landroid/graphics/drawable/BitmapDrawable;
    const/4 v2, 0x1

    invoke-virtual {v14, v2}, Landroid/graphics/drawable/BitmapDrawable;->setAntiAlias(Z)V

    .line 250
    invoke-virtual {v11, v14}, Landroid/widget/FrameLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 252
    const/4 v2, 0x4

    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    .line 253
    new-instance v2, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$2;

    move-object/from16 v4, p1

    move-object/from16 v5, p0

    move-object/from16 v6, p7

    move-object/from16 v7, p6

    invoke-direct/range {v2 .. v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$2;-><init>(Landroid/view/View;Landroid/graphics/Bitmap;Landroid/app/Activity;Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$SnsType;Ljava/util/List;)V

    invoke-virtual {v3, v2}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0

    .line 224
    .end local v3    # "mainView":Landroid/view/View;
    .end local v11    # "contentLayout":Landroid/widget/FrameLayout;
    .end local v12    # "contentLayoutParam":Landroid/widget/FrameLayout$LayoutParams;
    .end local v14    # "drawable":Landroid/graphics/drawable/BitmapDrawable;
    .end local v16    # "lp":Landroid/widget/FrameLayout$LayoutParams;
    .end local v20    # "viewDate":Landroid/widget/TextView;
    .end local v21    # "viewMainTitle":Landroid/widget/TextView;
    .end local v22    # "viewMainTitle2":Landroid/widget/TextView;
    :catch_0
    move-exception v15

    .line 226
    .local v15, "e":Ljava/lang/Exception;
    invoke-virtual {v15}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_1
.end method

.method public static getScreenshot(Landroid/view/View;)Landroid/graphics/Bitmap;
    .locals 3
    .param p0, "view"    # Landroid/view/View;

    .prologue
    .line 352
    const/4 v0, 0x0

    .line 354
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    const/4 v2, 0x1

    :try_start_0
    invoke-virtual {p0, v2}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 355
    invoke-virtual {p0}, Landroid/view/View;->buildDrawingCache()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 361
    invoke-virtual {p0}, Landroid/view/View;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 362
    invoke-virtual {p0}, Landroid/view/View;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-static {v2}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 365
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->destroyDrawingCache()V

    .line 366
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    move-object v2, v0

    .line 368
    :goto_0
    return-object v2

    .line 356
    :catch_0
    move-exception v1

    .line 357
    .local v1, "ex":Ljava/lang/RuntimeException;
    invoke-virtual {v1}, Ljava/lang/RuntimeException;->printStackTrace()V

    .line 358
    const/4 v2, 0x0

    goto :goto_0
.end method
