.class public Lcom/sec/android/app/shealth/DBUtil;
.super Ljava/lang/Object;
.source "DBUtil.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static clearApplicationData(Landroid/content/Context;)V
    .locals 12
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 899
    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v3

    .line 900
    .local v3, "cache":Ljava/io/File;
    invoke-virtual {p0}, Landroid/content/Context;->getExternalCacheDir()Ljava/io/File;

    move-result-object v5

    .line 902
    .local v5, "externalCache":Ljava/io/File;
    if-nez v3, :cond_1

    .line 926
    :cond_0
    :goto_0
    return-void

    .line 905
    :cond_1
    new-instance v0, Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v0, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 908
    .local v0, "appDir":Ljava/io/File;
    :try_start_0
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_4

    .line 909
    invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v9

    .line 910
    .local v9, "subDir":[Ljava/lang/String;
    move-object v2, v9

    .local v2, "arr$":[Ljava/lang/String;
    array-length v8, v2

    .local v8, "len$":I
    const/4 v7, 0x0

    .local v7, "i$":I
    :goto_1
    if-ge v7, v8, :cond_3

    aget-object v6, v2, v7

    .line 911
    .local v6, "folderName":Ljava/lang/String;
    const-string v10, "lib"

    invoke-virtual {v6, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_2

    .line 912
    new-instance v10, Ljava/io/File;

    invoke-direct {v10, v0, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {v10}, Lcom/sec/android/app/shealth/DBUtil;->deleteDir(Ljava/io/File;)Z

    .line 910
    :cond_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 915
    .end local v6    # "folderName":Ljava/lang/String;
    :cond_3
    const-string v10, "TAG"

    const-string v11, "clearApplicationData"

    invoke-static {v10, v11}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 917
    .end local v2    # "arr$":[Ljava/lang/String;
    .end local v7    # "i$":I
    .end local v8    # "len$":I
    .end local v9    # "subDir":[Ljava/lang/String;
    :cond_4
    if-eqz v5, :cond_0

    .line 920
    new-instance v1, Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v1, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 921
    .end local v0    # "appDir":Ljava/io/File;
    .local v1, "appDir":Ljava/io/File;
    :try_start_1
    invoke-static {v1}, Lcom/sec/android/app/shealth/DBUtil;->deleteDir(Ljava/io/File;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-object v0, v1

    .line 925
    .end local v1    # "appDir":Ljava/io/File;
    .restart local v0    # "appDir":Ljava/io/File;
    goto :goto_0

    .line 923
    :catch_0
    move-exception v4

    .line 924
    .local v4, "e":Ljava/lang/Exception;
    :goto_2
    const-string/jumbo v10, "tag"

    invoke-virtual {v4}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 923
    .end local v0    # "appDir":Ljava/io/File;
    .end local v4    # "e":Ljava/lang/Exception;
    .restart local v1    # "appDir":Ljava/io/File;
    :catch_1
    move-exception v4

    move-object v0, v1

    .end local v1    # "appDir":Ljava/io/File;
    .restart local v0    # "appDir":Ljava/io/File;
    goto :goto_2
.end method

.method private static deleteDir(Ljava/io/File;)Z
    .locals 5
    .param p0, "dir"    # Ljava/io/File;

    .prologue
    const/4 v2, 0x0

    .line 929
    if-nez p0, :cond_1

    .line 940
    :cond_0
    :goto_0
    return v2

    .line 932
    :cond_1
    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 933
    invoke-virtual {p0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v1

    .line 934
    .local v1, "subDirectory":[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v3, v1

    if-ge v0, v3, :cond_2

    .line 935
    new-instance v3, Ljava/io/File;

    aget-object v4, v1, v0

    invoke-direct {v3, p0, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {v3}, Lcom/sec/android/app/shealth/DBUtil;->deleteDir(Ljava/io/File;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 934
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 940
    .end local v0    # "i":I
    .end local v1    # "subDirectory":[Ljava/lang/String;
    :cond_2
    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    move-result v2

    goto :goto_0
.end method
