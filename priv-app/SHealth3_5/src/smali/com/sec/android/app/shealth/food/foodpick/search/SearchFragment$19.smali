.class Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$19;
.super Ljava/lang/Object;
.source "SearchFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->showSearchResult(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter$OnItemClickListener",
        "<",
        "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)V
    .locals 0

    .prologue
    .line 1008
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$19;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;)V
    .locals 2
    .param p1, "listItem"    # Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;

    .prologue
    .line 1011
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$19;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->access$1800(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/shealth/food/app/UserActionLog;->SELECT_FOOD_AMONG_THE_SEARCH_RESULT:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;->logUserAction(Lcom/sec/android/app/shealth/food/app/UserActionLog;)V

    .line 1012
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$19;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;->getFoodInfoItem()Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->addFoodItem(Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;)V

    .line 1013
    return-void
.end method

.method public bridge synthetic onItemClick(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 1008
    check-cast p1, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$19;->onItemClick(Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;)V

    return-void
.end method
