.class public Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;
.super Landroid/widget/RelativeLayout;
.source "GraphInformationItem.java"


# static fields
.field private static final DEFAULT_VALUE_FORMAT:Ljava/lang/String; = "%.0f"

.field private static final DEFAULT_VALUE_SEPARATOR:Ljava/lang/String; = "/"


# instance fields
.field private mSeriesId:I

.field private mTitleArea:Landroid/widget/LinearLayout;

.field private mTitleAvgView:Landroid/widget/TextView;

.field private mTitleView:Landroid/widget/TextView;

.field private mUnitAvgView:Landroid/widget/TextView;

.field private mUnitView:Landroid/widget/TextView;

.field private mValueFormat:Ljava/lang/String;

.field private mValueView:Landroid/widget/TextView;

.field private mValuesCount:I

.field private mValuesSeparator:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "seriesId"    # I

    .prologue
    const/4 v2, 0x1

    .line 57
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 39
    iput v2, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->mValuesCount:I

    .line 40
    const-string v1, "/"

    iput-object v1, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->mValuesSeparator:Ljava/lang/String;

    .line 41
    const-string v1, "%.0f"

    iput-object v1, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->mValueFormat:Ljava/lang/String;

    .line 59
    const-string v1, "layout_inflater"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 60
    .local v0, "layoutInflater":Landroid/view/LayoutInflater;
    sget v1, Lcom/sec/android/app/shealth/logutils/R$layout;->graph_information_area_item_view:I

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 62
    sget v1, Lcom/sec/android/app/shealth/logutils/R$id;->graph_information_area_item_title_area:I

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->mTitleArea:Landroid/widget/LinearLayout;

    .line 63
    sget v1, Lcom/sec/android/app/shealth/logutils/R$id;->graph_information_area_item_subtitle:I

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->mTitleView:Landroid/widget/TextView;

    .line 64
    sget v1, Lcom/sec/android/app/shealth/logutils/R$id;->graph_information_area_item_title_view_avg_label:I

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->mTitleAvgView:Landroid/widget/TextView;

    .line 65
    sget v1, Lcom/sec/android/app/shealth/logutils/R$id;->graph_information_area_amount_view:I

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->mValueView:Landroid/widget/TextView;

    .line 66
    sget v1, Lcom/sec/android/app/shealth/logutils/R$id;->graph_information_area_unit_view:I

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->mUnitView:Landroid/widget/TextView;

    .line 67
    sget v1, Lcom/sec/android/app/shealth/logutils/R$id;->graph_information_area_unit_view_avg_label:I

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->mUnitAvgView:Landroid/widget/TextView;

    .line 69
    iget-object v1, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->mTitleAvgView:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/sec/android/app/shealth/logutils/R$string;->average_short:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 70
    iget-object v1, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->mUnitAvgView:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/sec/android/app/shealth/logutils/R$string;->average_short:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 72
    iput p2, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->mSeriesId:I

    .line 73
    return-void
.end method

.method private isAverageLabelVisible()Z
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->mTitleAvgView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->mUnitAvgView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getSeriesId()I
    .locals 1

    .prologue
    .line 240
    iget v0, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->mSeriesId:I

    return v0
.end method

.method public getValuesCount()I
    .locals 1

    .prologue
    .line 231
    iget v0, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->mValuesCount:I

    return v0
.end method

.method public hideAverageLabel()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 149
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->mTitleAvgView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 150
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->mUnitAvgView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 151
    return-void
.end method

.method public hideTitle()V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 125
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->mTitleArea:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 126
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->mValueView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/logutils/R$dimen;->graph_information_area_amount_view_without_label_text_size:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v3, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 128
    invoke-direct {p0}, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->isAverageLabelVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 129
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->mTitleAvgView:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 130
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->mUnitAvgView:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 132
    :cond_0
    return-void
.end method

.method public setAverageLabelVisible(Z)V
    .locals 0
    .param p1, "visible"    # Z

    .prologue
    .line 159
    if-eqz p1, :cond_0

    .line 160
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->showAverageLabel()V

    .line 164
    :goto_0
    return-void

    .line 162
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->hideAverageLabel()V

    goto :goto_0
.end method

.method public setColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 220
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->mValueView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 221
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->mUnitView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 222
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->mUnitAvgView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 223
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 201
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 202
    return-void
.end method

.method public setUnit(Ljava/lang/String;)V
    .locals 2
    .param p1, "unit"    # Ljava/lang/String;

    .prologue
    .line 210
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->mUnitView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 211
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->mUnitView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->mContext:Landroid/content/Context;

    invoke-static {v1, p1}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->convertToProperUnitsText(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 212
    return-void
.end method

.method public setValueFormat(Ljava/lang/String;)V
    .locals 0
    .param p1, "format"    # Ljava/lang/String;

    .prologue
    .line 192
    iput-object p1, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->mValueFormat:Ljava/lang/String;

    .line 193
    return-void
.end method

.method public setValuesCount(I)V
    .locals 0
    .param p1, "count"    # I

    .prologue
    .line 174
    iput p1, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->mValuesCount:I

    .line 175
    return-void
.end method

.method public setValuesSeparator(Ljava/lang/String;)V
    .locals 0
    .param p1, "separator"    # Ljava/lang/String;

    .prologue
    .line 183
    iput-object p1, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->mValuesSeparator:Ljava/lang/String;

    .line 184
    return-void
.end method

.method public showAverageLabel()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 138
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->mTitleArea:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 139
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->mTitleAvgView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 143
    :goto_0
    return-void

    .line 141
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->mUnitAvgView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public update(D)V
    .locals 7
    .param p1, "value"    # D

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 81
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v1, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    .line 82
    .local v0, "language":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v1, "ar"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 83
    iget-object v1, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->mValueView:Landroid/widget/TextView;

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    iget-object v3, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->mValueFormat:Ljava/lang/String;

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 86
    :goto_0
    return-void

    .line 85
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->mValueView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->mValueFormat:Ljava/lang/String;

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public update(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Double;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 94
    .local p1, "values":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Double;>;"
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    iget v3, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->mValuesCount:I

    if-ge v2, v3, :cond_0

    .line 95
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Wrong values count: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " instead of "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->mValuesCount:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 98
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 99
    .local v1, "val":Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->mValuesCount:I

    if-ge v0, v2, :cond_1

    .line 100
    iget-object v2, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->mValueFormat:Ljava/lang/String;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 101
    iget-object v2, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->mValuesSeparator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 99
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 103
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 105
    iget-object v2, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->mValueView:Landroid/widget/TextView;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 106
    return-void
.end method
