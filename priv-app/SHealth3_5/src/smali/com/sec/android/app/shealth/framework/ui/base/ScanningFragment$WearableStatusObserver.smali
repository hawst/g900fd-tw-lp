.class public Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$WearableStatusObserver;
.super Landroid/database/ContentObserver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "WearableStatusObserver"
.end annotation


# static fields
.field private static SETTING_WEARABLE_ID:Ljava/lang/String;


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;


# direct methods
.method static constructor <clinit>()V
    .locals 50

    const/16 v0, 0x2f

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x4

    const/4 v6, 0x5

    const/4 v7, 0x6

    const/4 v8, 0x7

    const/16 v9, 0x8

    const/16 v10, 0x9

    const/16 v11, 0xa

    const/16 v12, 0xb

    const/16 v13, 0xc

    const/16 v14, 0xd

    const/16 v15, 0xe

    const/16 v16, 0xf

    const/16 v17, 0x10

    const/16 v18, 0x11

    const/16 v19, 0x12

    const/16 v20, 0x13

    const/16 v21, 0x14

    const/16 v22, 0x15

    const/16 v23, 0x16

    const/16 v24, 0x17

    const/16 v25, 0x18

    const/16 v26, 0x19

    const/16 v27, 0x1a

    const/16 v28, 0x1b

    const/16 v29, 0x1c

    const/16 v30, 0x1d

    const/16 v31, 0x1e

    const/16 v32, 0x1f

    const/16 v33, 0x20

    const/16 v34, 0x21

    const/16 v35, 0x22

    const/16 v36, 0x23

    const/16 v37, 0x24

    const/16 v38, 0x25

    const/16 v39, 0x26

    const/16 v40, 0x27

    const/16 v41, 0x28

    const/16 v42, 0x29

    const/16 v43, 0x2a

    const/16 v44, 0x2b

    const/16 v45, 0x2c

    const/16 v46, 0x2d

    const/16 v47, 0x2e

    const/16 v48, -0x3

    aput v48, v1, v47

    const/16 v47, -0x30

    aput v47, v1, v46

    const/16 v46, -0x4d8c

    aput v46, v1, v45

    const/16 v45, -0x29

    aput v45, v1, v44

    const/16 v44, -0x64

    aput v44, v1, v43

    const/16 v43, -0x6cfd

    aput v43, v1, v42

    const/16 v42, -0xe

    aput v42, v1, v41

    const/16 v41, -0x57

    aput v41, v1, v40

    const/16 v40, -0x15bd

    aput v40, v1, v39

    const/16 v39, -0x71

    aput v39, v1, v38

    const/16 v38, -0x12ce

    aput v38, v1, v37

    const/16 v37, -0x4e

    aput v37, v1, v36

    const/16 v36, -0x49f7

    aput v36, v1, v35

    const/16 v35, -0x2d

    aput v35, v1, v34

    const/16 v34, -0xf

    aput v34, v1, v33

    const/16 v33, 0x3730

    aput v33, v1, v32

    const/16 v32, 0x252

    aput v32, v1, v31

    const/16 v31, 0x76c

    aput v31, v1, v30

    const/16 v30, -0x7497

    aput v30, v1, v29

    const/16 v29, -0x1c

    aput v29, v1, v28

    const/16 v28, -0x23

    aput v28, v1, v27

    const/16 v27, -0x76

    aput v27, v1, v26

    const/16 v26, 0x1731

    aput v26, v1, v25

    const/16 v25, 0x7572

    aput v25, v1, v24

    const/16 v24, -0x3dff

    aput v24, v1, v23

    const/16 v23, -0x4f

    aput v23, v1, v22

    const/16 v22, -0x77ef

    aput v22, v1, v21

    const/16 v21, -0x5

    aput v21, v1, v20

    const/16 v20, -0x26f9

    aput v20, v1, v19

    const/16 v19, -0x56

    aput v19, v1, v18

    const/16 v18, 0x1773

    aput v18, v1, v17

    const/16 v17, -0x1087

    aput v17, v1, v16

    const/16 v16, -0x7a

    aput v16, v1, v15

    const/16 v15, -0x1e

    aput v15, v1, v14

    const/16 v14, -0x1e8b

    aput v14, v1, v13

    const/16 v13, -0x7c

    aput v13, v1, v12

    const/16 v12, -0x34

    aput v12, v1, v11

    const/16 v11, -0x42da

    aput v11, v1, v10

    const/16 v10, -0x6e

    aput v10, v1, v9

    const/16 v9, -0x7b6

    aput v9, v1, v8

    const/16 v8, -0x74

    aput v8, v1, v7

    const/16 v7, 0x1c02

    aput v7, v1, v6

    const/16 v6, -0x6487

    aput v6, v1, v5

    const/16 v5, -0x11

    aput v5, v1, v4

    const/16 v4, -0x6c

    aput v4, v1, v3

    const/16 v3, 0x4b5b

    aput v3, v1, v2

    const/16 v2, 0x5528

    aput v2, v1, v0

    const/16 v0, 0x2f

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, 0x10

    const/16 v19, 0x11

    const/16 v20, 0x12

    const/16 v21, 0x13

    const/16 v22, 0x14

    const/16 v23, 0x15

    const/16 v24, 0x16

    const/16 v25, 0x17

    const/16 v26, 0x18

    const/16 v27, 0x19

    const/16 v28, 0x1a

    const/16 v29, 0x1b

    const/16 v30, 0x1c

    const/16 v31, 0x1d

    const/16 v32, 0x1e

    const/16 v33, 0x1f

    const/16 v34, 0x20

    const/16 v35, 0x21

    const/16 v36, 0x22

    const/16 v37, 0x23

    const/16 v38, 0x24

    const/16 v39, 0x25

    const/16 v40, 0x26

    const/16 v41, 0x27

    const/16 v42, 0x28

    const/16 v43, 0x29

    const/16 v44, 0x2a

    const/16 v45, 0x2b

    const/16 v46, 0x2c

    const/16 v47, 0x2d

    const/16 v48, 0x2e

    const/16 v49, -0x67

    aput v49, v0, v48

    const/16 v48, -0x47

    aput v48, v0, v47

    const/16 v47, -0x4dd5

    aput v47, v0, v46

    const/16 v46, -0x4e

    aput v46, v0, v45

    const/16 v45, -0x10

    aput v45, v0, v44

    const/16 v44, -0x6c9f

    aput v44, v0, v43

    const/16 v43, -0x6d

    aput v43, v0, v42

    const/16 v42, -0x25

    aput v42, v0, v41

    const/16 v41, -0x15de

    aput v41, v0, v40

    const/16 v40, -0x16

    aput v40, v0, v39

    const/16 v39, -0x12bb

    aput v39, v0, v38

    const/16 v38, -0x13

    aput v38, v0, v37

    const/16 v37, -0x4993

    aput v37, v0, v36

    const/16 v36, -0x4a

    aput v36, v0, v35

    const/16 v35, -0x7b

    aput v35, v0, v34

    const/16 v34, 0x3753

    aput v34, v0, v33

    const/16 v33, 0x237

    aput v33, v0, v32

    const/16 v32, 0x702

    aput v32, v0, v31

    const/16 v31, -0x74f9

    aput v31, v0, v30

    const/16 v30, -0x75

    aput v30, v0, v29

    const/16 v29, -0x42

    aput v29, v0, v28

    const/16 v28, -0x5b

    aput v28, v0, v27

    const/16 v27, 0x175c

    aput v27, v0, v26

    const/16 v26, 0x7517

    aput v26, v0, v25

    const/16 v25, -0x3d8b

    aput v25, v0, v24

    const/16 v24, -0x3e

    aput v24, v0, v23

    const/16 v23, -0x7798

    aput v23, v0, v22

    const/16 v22, -0x78

    aput v22, v0, v21

    const/16 v21, -0x26d8

    aput v21, v0, v20

    const/16 v20, -0x27

    aput v20, v0, v19

    const/16 v19, 0x1714

    aput v19, v0, v18

    const/16 v18, -0x10e9

    aput v18, v0, v17

    const/16 v17, -0x11

    aput v17, v0, v16

    const/16 v16, -0x6a

    aput v16, v0, v15

    const/16 v15, -0x1eff

    aput v15, v0, v14

    const/16 v14, -0x1f

    aput v14, v0, v13

    const/16 v13, -0x41

    aput v13, v0, v12

    const/16 v12, -0x42f7

    aput v12, v0, v11

    const/16 v11, -0x43

    aput v11, v0, v10

    const/16 v10, -0x790

    aput v10, v0, v9

    const/4 v9, -0x8

    aput v9, v0, v8

    const/16 v8, 0x1c6c

    aput v8, v0, v7

    const/16 v7, -0x64e4

    aput v7, v0, v6

    const/16 v6, -0x65

    aput v6, v0, v5

    const/4 v5, -0x6

    aput v5, v0, v4

    const/16 v4, 0x4b34

    aput v4, v0, v3

    const/16 v3, 0x554b

    aput v3, v0, v2

    const/4 v2, 0x0

    :goto_0
    array-length v3, v0

    if-lt v2, v3, :cond_0

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_1
    array-length v3, v0

    if-lt v2, v3, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$WearableStatusObserver;->SETTING_WEARABLE_ID:Ljava/lang/String;

    return-void

    :cond_0
    aget v3, v0, v2

    aget v4, v1, v2

    xor-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    aget v3, v1, v2

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public constructor <init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Landroid/os/Handler;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$WearableStatusObserver;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 38

    const/16 v1, 0x16

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, 0x10

    const/16 v19, 0x11

    const/16 v20, 0x12

    const/16 v21, 0x13

    const/16 v22, 0x14

    const/16 v23, 0x15

    const/16 v24, -0x3995

    aput v24, v2, v23

    const/16 v23, -0x5d

    aput v23, v2, v22

    const/16 v22, -0x1986

    aput v22, v2, v21

    const/16 v21, -0x6c

    aput v21, v2, v20

    const/16 v20, -0x72ee

    aput v20, v2, v19

    const/16 v19, -0x2

    aput v19, v2, v18

    const/16 v18, -0x7d

    aput v18, v2, v17

    const/16 v17, -0x50b0

    aput v17, v2, v16

    const/16 v16, -0x24

    aput v16, v2, v15

    const/16 v15, 0x6c67

    aput v15, v2, v14

    const/16 v14, -0x1e8

    aput v14, v2, v13

    const/16 v13, -0x61

    aput v13, v2, v12

    const/16 v12, 0x578

    aput v12, v2, v11

    const/16 v11, 0x6f56

    aput v11, v2, v10

    const/16 v10, 0x700a

    aput v10, v2, v9

    const/16 v9, 0x571c

    aput v9, v2, v8

    const/16 v8, 0x635

    aput v8, v2, v7

    const/16 v7, -0x3c99

    aput v7, v2, v6

    const/16 v6, -0x4f

    aput v6, v2, v5

    const/16 v5, -0x2fd3

    aput v5, v2, v4

    const/16 v4, -0x4b

    aput v4, v2, v3

    const/16 v3, -0x1ec5

    aput v3, v2, v1

    const/16 v1, 0x16

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, -0x39e7

    aput v25, v1, v24

    const/16 v24, -0x3a

    aput v24, v1, v23

    const/16 v23, -0x19f4

    aput v23, v1, v22

    const/16 v22, -0x1a

    aput v22, v1, v21

    const/16 v21, -0x7289

    aput v21, v1, v20

    const/16 v20, -0x73

    aput v20, v1, v19

    const/16 v19, -0x1f

    aput v19, v1, v18

    const/16 v18, -0x50e1

    aput v18, v1, v17

    const/16 v17, -0x51

    aput v17, v1, v16

    const/16 v16, 0x6c12

    aput v16, v1, v15

    const/16 v15, -0x194

    aput v15, v1, v14

    const/4 v14, -0x2

    aput v14, v1, v13

    const/16 v13, 0x50c

    aput v13, v1, v12

    const/16 v12, 0x6f05

    aput v12, v1, v11

    const/16 v11, 0x706f

    aput v11, v1, v10

    const/16 v10, 0x5770

    aput v10, v1, v9

    const/16 v9, 0x657

    aput v9, v1, v8

    const/16 v8, -0x3cfa

    aput v8, v1, v7

    const/16 v7, -0x3d

    aput v7, v1, v6

    const/16 v6, -0x2fb4

    aput v6, v1, v5

    const/16 v5, -0x30

    aput v5, v1, v4

    const/16 v4, -0x1e94

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v4, v1

    if-lt v3, v4, :cond_0

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v4, v1

    if-lt v3, v4, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x20

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, 0x1d

    const/16 v34, 0x1e

    const/16 v35, 0x1f

    const/16 v36, 0x361c

    aput v36, v2, v35

    const/16 v35, -0x7bad

    aput v35, v2, v34

    const/16 v34, -0x1d

    aput v34, v2, v33

    const/16 v33, -0x16

    aput v33, v2, v32

    const/16 v32, -0x7a

    aput v32, v2, v31

    const/16 v31, -0x3d

    aput v31, v2, v30

    const/16 v30, -0x57

    aput v30, v2, v29

    const/16 v29, -0x5b

    aput v29, v2, v28

    const/16 v28, -0x2983

    aput v28, v2, v27

    const/16 v27, -0x8

    aput v27, v2, v26

    const/16 v26, 0x1c62

    aput v26, v2, v25

    const/16 v25, -0x687

    aput v25, v2, v24

    const/16 v24, -0x71

    aput v24, v2, v23

    const/16 v23, -0x38bf

    aput v23, v2, v22

    const/16 v22, -0x5e

    aput v22, v2, v21

    const/16 v21, 0x4b5a

    aput v21, v2, v20

    const/16 v20, 0x2c29

    aput v20, v2, v19

    const/16 v19, -0x109d

    aput v19, v2, v18

    const/16 v18, -0x64

    aput v18, v2, v17

    const/16 v17, 0x4f07

    aput v17, v2, v16

    const/16 v16, 0xe3b

    aput v16, v2, v15

    const/16 v15, -0x2791

    aput v15, v2, v14

    const/16 v14, -0x54

    aput v14, v2, v13

    const/16 v13, 0x604e

    aput v13, v2, v12

    const/16 v12, -0x1cfb

    aput v12, v2, v11

    const/16 v11, -0x71

    aput v11, v2, v10

    const/16 v10, 0x2e03

    aput v10, v2, v9

    const/16 v9, 0x344f

    aput v9, v2, v8

    const/16 v8, -0x4fba

    aput v8, v2, v7

    const/16 v7, -0x2f

    aput v7, v2, v6

    const/16 v6, -0x79

    aput v6, v2, v3

    const/16 v3, 0x5e3f

    aput v3, v2, v1

    const/16 v1, 0x20

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, 0x11

    const/16 v23, 0x12

    const/16 v24, 0x13

    const/16 v25, 0x14

    const/16 v26, 0x15

    const/16 v27, 0x16

    const/16 v28, 0x17

    const/16 v29, 0x18

    const/16 v30, 0x19

    const/16 v31, 0x1a

    const/16 v32, 0x1b

    const/16 v33, 0x1c

    const/16 v34, 0x1d

    const/16 v35, 0x1e

    const/16 v36, 0x1f

    const/16 v37, 0x3634

    aput v37, v1, v36

    const/16 v36, -0x7bca

    aput v36, v1, v35

    const/16 v35, -0x7c

    aput v35, v1, v34

    const/16 v34, -0x7c

    aput v34, v1, v33

    const/16 v33, -0x19

    aput v33, v1, v32

    const/16 v32, -0x55

    aput v32, v1, v31

    const/16 v31, -0x16

    aput v31, v1, v30

    const/16 v30, -0x35

    aput v30, v1, v29

    const/16 v29, -0x29ee

    aput v29, v1, v28

    const/16 v28, -0x2a

    aput v28, v1, v27

    const/16 v27, 0x1c10

    aput v27, v1, v26

    const/16 v26, -0x6e4

    aput v26, v1, v25

    const/16 v25, -0x7

    aput v25, v1, v24

    const/16 v24, -0x38cd

    aput v24, v1, v23

    const/16 v23, -0x39

    aput v23, v1, v22

    const/16 v22, 0x4b29

    aput v22, v1, v21

    const/16 v21, 0x2c4b

    aput v21, v1, v20

    const/16 v20, -0x10d4

    aput v20, v1, v19

    const/16 v19, -0x11

    aput v19, v1, v18

    const/16 v18, 0x4f72

    aput v18, v1, v17

    const/16 v17, 0xe4f

    aput v17, v1, v16

    const/16 v16, -0x27f2

    aput v16, v1, v15

    const/16 v15, -0x28

    aput v15, v1, v14

    const/16 v14, 0x601d

    aput v14, v1, v13

    const/16 v13, -0x1ca0

    aput v13, v1, v12

    const/16 v12, -0x1d

    aput v12, v1, v11

    const/16 v11, 0x2e61

    aput v11, v1, v10

    const/16 v10, 0x342e

    aput v10, v1, v9

    const/16 v9, -0x4fcc

    aput v9, v1, v8

    const/16 v8, -0x50

    aput v8, v1, v7

    const/16 v7, -0x1e

    aput v7, v1, v6

    const/16 v6, 0x5e68

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v6, v1

    if-lt v3, v6, :cond_2

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v6, v1

    if-lt v3, v6, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move/from16 v0, p1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, 0x5c0f

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v6, 0x5c26

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_4
    array-length v6, v1

    if-lt v3, v6, :cond_4

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_5
    array-length v6, v1

    if-lt v3, v6, :cond_5

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super/range {p0 .. p1}, Landroid/database/ContentObserver;->onChange(Z)V

    return-void

    :cond_0
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_1
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_2
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_3
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_4
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    :cond_5
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_5
.end method

.method public onChange(ZLandroid/net/Uri;)V
    .locals 51

    :try_start_0
    invoke-virtual/range {p2 .. p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    const/16 v1, 0x2f

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, 0x10

    const/16 v19, 0x11

    const/16 v20, 0x12

    const/16 v21, 0x13

    const/16 v22, 0x14

    const/16 v23, 0x15

    const/16 v24, 0x16

    const/16 v25, 0x17

    const/16 v26, 0x18

    const/16 v27, 0x19

    const/16 v28, 0x1a

    const/16 v29, 0x1b

    const/16 v30, 0x1c

    const/16 v31, 0x1d

    const/16 v32, 0x1e

    const/16 v33, 0x1f

    const/16 v34, 0x20

    const/16 v35, 0x21

    const/16 v36, 0x22

    const/16 v37, 0x23

    const/16 v38, 0x24

    const/16 v39, 0x25

    const/16 v40, 0x26

    const/16 v41, 0x27

    const/16 v42, 0x28

    const/16 v43, 0x29

    const/16 v44, 0x2a

    const/16 v45, 0x2b

    const/16 v46, 0x2c

    const/16 v47, 0x2d

    const/16 v48, 0x2e

    const/16 v49, -0x2e

    aput v49, v2, v48

    const/16 v48, -0x60

    aput v48, v2, v47

    const/16 v47, -0x11fc

    aput v47, v2, v46

    const/16 v46, -0x75

    aput v46, v2, v45

    const/16 v45, -0x15

    aput v45, v2, v44

    const/16 v44, -0x1aa9

    aput v44, v2, v43

    const/16 v43, -0x7c

    aput v43, v2, v42

    const/16 v42, -0x11

    aput v42, v2, v41

    const/16 v41, -0x4a

    aput v41, v2, v40

    const/16 v40, 0x251a

    aput v40, v2, v39

    const/16 v39, -0x38ae

    aput v39, v2, v38

    const/16 v38, -0x68

    aput v38, v2, v37

    const/16 v37, 0x4e2a

    aput v37, v2, v36

    const/16 v36, -0x57d5

    aput v36, v2, v35

    const/16 v35, -0x24

    aput v35, v2, v34

    const/16 v34, 0x6f13

    aput v34, v2, v33

    const/16 v33, 0xa

    aput v33, v2, v32

    const/16 v32, -0x1192

    aput v32, v2, v31

    const/16 v31, -0x80

    aput v31, v2, v30

    const/16 v30, 0x6929

    aput v30, v2, v29

    const/16 v29, 0x380a

    aput v29, v2, v28

    const/16 v28, 0x17

    aput v28, v2, v27

    const/16 v27, -0x6893

    aput v27, v2, v26

    const/16 v26, -0xe

    aput v26, v2, v25

    const/16 v25, -0x59

    aput v25, v2, v24

    const/16 v24, -0x1f

    aput v24, v2, v23

    const/16 v23, -0x6

    aput v23, v2, v22

    const/16 v22, 0x30d

    aput v22, v2, v21

    const/16 v21, -0x28d4

    aput v21, v2, v20

    const/16 v20, -0x5c

    aput v20, v2, v19

    const/16 v19, 0x7b63

    aput v19, v2, v18

    const/16 v18, 0x2415

    aput v18, v2, v17

    const/16 v17, 0x4e4d

    aput v17, v2, v16

    const/16 v16, -0x52c6

    aput v16, v2, v15

    const/16 v15, -0x27

    aput v15, v2, v14

    const/16 v14, -0x50eb

    aput v14, v2, v13

    const/16 v13, -0x24

    aput v13, v2, v12

    const/16 v12, -0x4a

    aput v12, v2, v11

    const/16 v11, 0x5954

    aput v11, v2, v10

    const/16 v10, 0x6e63

    aput v10, v2, v9

    const/16 v9, -0x61e6

    aput v9, v2, v8

    const/16 v8, -0x10

    aput v8, v2, v7

    const/16 v7, -0x3f

    aput v7, v2, v6

    const/16 v6, 0x705a

    aput v6, v2, v5

    const/16 v5, -0x6fe2

    aput v5, v2, v4

    const/4 v4, -0x1

    aput v4, v2, v3

    const/16 v3, -0x5c81

    aput v3, v2, v1

    const/16 v1, 0x2f

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, 0x1d

    const/16 v33, 0x1e

    const/16 v34, 0x1f

    const/16 v35, 0x20

    const/16 v36, 0x21

    const/16 v37, 0x22

    const/16 v38, 0x23

    const/16 v39, 0x24

    const/16 v40, 0x25

    const/16 v41, 0x26

    const/16 v42, 0x27

    const/16 v43, 0x28

    const/16 v44, 0x29

    const/16 v45, 0x2a

    const/16 v46, 0x2b

    const/16 v47, 0x2c

    const/16 v48, 0x2d

    const/16 v49, 0x2e

    const/16 v50, -0x4a

    aput v50, v1, v49

    const/16 v49, -0x37

    aput v49, v1, v48

    const/16 v48, -0x11a5

    aput v48, v1, v47

    const/16 v47, -0x12

    aput v47, v1, v46

    const/16 v46, -0x79

    aput v46, v1, v45

    const/16 v45, -0x1acb

    aput v45, v1, v44

    const/16 v44, -0x1b

    aput v44, v1, v43

    const/16 v43, -0x63

    aput v43, v1, v42

    const/16 v42, -0x29

    aput v42, v1, v41

    const/16 v41, 0x257f

    aput v41, v1, v40

    const/16 v40, -0x38db

    aput v40, v1, v39

    const/16 v39, -0x39

    aput v39, v1, v38

    const/16 v38, 0x4e4e

    aput v38, v1, v37

    const/16 v37, -0x57b2

    aput v37, v1, v36

    const/16 v36, -0x58

    aput v36, v1, v35

    const/16 v35, 0x6f70

    aput v35, v1, v34

    const/16 v34, 0x6f

    aput v34, v1, v33

    const/16 v33, -0x1200

    aput v33, v1, v32

    const/16 v32, -0x12

    aput v32, v1, v31

    const/16 v31, 0x6946

    aput v31, v1, v30

    const/16 v30, 0x3869

    aput v30, v1, v29

    const/16 v29, 0x38

    aput v29, v1, v28

    const/16 v28, -0x6900

    aput v28, v1, v27

    const/16 v27, -0x69

    aput v27, v1, v26

    const/16 v26, -0x2d

    aput v26, v1, v25

    const/16 v25, -0x6e

    aput v25, v1, v24

    const/16 v24, -0x7d

    aput v24, v1, v23

    const/16 v23, 0x37e

    aput v23, v1, v22

    const/16 v22, -0x28fd

    aput v22, v1, v21

    const/16 v21, -0x29

    aput v21, v1, v20

    const/16 v20, 0x7b04

    aput v20, v1, v19

    const/16 v19, 0x247b

    aput v19, v1, v18

    const/16 v18, 0x4e24

    aput v18, v1, v17

    const/16 v17, -0x52b2

    aput v17, v1, v16

    const/16 v16, -0x53

    aput v16, v1, v15

    const/16 v15, -0x5090

    aput v15, v1, v14

    const/16 v14, -0x51

    aput v14, v1, v13

    const/16 v13, -0x67

    aput v13, v1, v12

    const/16 v12, 0x597b

    aput v12, v1, v11

    const/16 v11, 0x6e59

    aput v11, v1, v10

    const/16 v10, -0x6192

    aput v10, v1, v9

    const/16 v9, -0x62

    aput v9, v1, v8

    const/16 v8, -0x5c

    aput v8, v1, v7

    const/16 v7, 0x702e

    aput v7, v1, v6

    const/16 v6, -0x6f90

    aput v6, v1, v5

    const/16 v5, -0x70

    aput v5, v1, v4

    const/16 v4, -0x5ce4

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v4, v1

    if-lt v3, v4, :cond_1

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v4, v1

    if-lt v3, v4, :cond_2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual/range {p2 .. p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0x16

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, 0x10

    const/16 v19, 0x11

    const/16 v20, 0x12

    const/16 v21, 0x13

    const/16 v22, 0x14

    const/16 v23, 0x15

    const/16 v24, -0x36

    aput v24, v2, v23

    const/16 v23, -0x75

    aput v23, v2, v22

    const/16 v22, 0x3729

    aput v22, v2, v21

    const/16 v21, -0x10bb

    aput v21, v2, v20

    const/16 v20, -0x76

    aput v20, v2, v19

    const/16 v19, -0x2aa5

    aput v19, v2, v18

    const/16 v18, -0x49

    aput v18, v2, v17

    const/16 v17, -0x2595

    aput v17, v2, v16

    const/16 v16, -0x57

    aput v16, v2, v15

    const/16 v15, -0x4aa

    aput v15, v2, v14

    const/16 v14, -0x71

    aput v14, v2, v13

    const/16 v13, -0x11

    aput v13, v2, v12

    const/16 v12, -0x40

    aput v12, v2, v11

    const/16 v11, 0x6307

    aput v11, v2, v10

    const/16 v10, 0x2306

    aput v10, v2, v9

    const/16 v9, -0x5db1

    aput v9, v2, v8

    const/16 v8, -0x40

    aput v8, v2, v7

    const/16 v7, -0x118e

    aput v7, v2, v6

    const/16 v6, -0x64

    aput v6, v2, v5

    const/16 v5, 0xc15

    aput v5, v2, v4

    const/16 v4, -0x1597

    aput v4, v2, v3

    const/16 v3, -0x43

    aput v3, v2, v1

    const/16 v1, 0x16

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, -0x48

    aput v25, v1, v24

    const/16 v24, -0x12

    aput v24, v1, v23

    const/16 v23, 0x375f

    aput v23, v1, v22

    const/16 v22, -0x10c9

    aput v22, v1, v21

    const/16 v21, -0x11

    aput v21, v1, v20

    const/16 v20, -0x2ad8

    aput v20, v1, v19

    const/16 v19, -0x2b

    aput v19, v1, v18

    const/16 v18, -0x25dc

    aput v18, v1, v17

    const/16 v17, -0x26

    aput v17, v1, v16

    const/16 v16, -0x4dd

    aput v16, v1, v15

    const/4 v15, -0x5

    aput v15, v1, v14

    const/16 v14, -0x72

    aput v14, v1, v13

    const/16 v13, -0x4c

    aput v13, v1, v12

    const/16 v12, 0x6354

    aput v12, v1, v11

    const/16 v11, 0x2363

    aput v11, v1, v10

    const/16 v10, -0x5ddd

    aput v10, v1, v9

    const/16 v9, -0x5e

    aput v9, v1, v8

    const/16 v8, -0x11ed

    aput v8, v1, v7

    const/16 v7, -0x12

    aput v7, v1, v6

    const/16 v6, 0xc74

    aput v6, v1, v5

    const/16 v5, -0x15f4

    aput v5, v1, v4

    const/16 v4, -0x16

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v4, v1

    if-lt v3, v4, :cond_3

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v4, v1

    if-lt v3, v4, :cond_4

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x20

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, 0x1d

    const/16 v34, 0x1e

    const/16 v35, 0x1f

    const/16 v36, 0x1e48

    aput v36, v2, v35

    const/16 v35, 0x627b

    aput v35, v2, v34

    const/16 v34, -0x3efb

    aput v34, v2, v33

    const/16 v33, -0x51

    aput v33, v2, v32

    const/16 v32, 0x3d7d

    aput v32, v2, v31

    const/16 v31, -0x9ab

    aput v31, v2, v30

    const/16 v30, -0x4b

    aput v30, v2, v29

    const/16 v29, -0x33

    aput v29, v2, v28

    const/16 v28, 0x5161

    aput v28, v2, v27

    const/16 v27, -0x4981

    aput v27, v2, v26

    const/16 v26, -0x3c

    aput v26, v2, v25

    const/16 v25, -0xf

    aput v25, v2, v24

    const/16 v24, -0x1e1

    aput v24, v2, v23

    const/16 v23, -0x74

    aput v23, v2, v22

    const/16 v22, 0x7962

    aput v22, v2, v21

    const/16 v21, 0xf0a

    aput v21, v2, v20

    const/16 v20, -0x5193

    aput v20, v2, v19

    const/16 v19, -0x1f

    aput v19, v2, v18

    const/16 v18, -0x659b

    aput v18, v2, v17

    const/16 v17, -0x11

    aput v17, v2, v16

    const/16 v16, -0x6

    aput v16, v2, v15

    const/16 v15, -0x76db

    aput v15, v2, v14

    const/4 v14, -0x3

    aput v14, v2, v13

    const/16 v13, 0x736e

    aput v13, v2, v12

    const/16 v12, 0x4316

    aput v12, v2, v11

    const/16 v11, 0x172f

    aput v11, v2, v10

    const/16 v10, 0x4c75

    aput v10, v2, v9

    const/16 v9, 0x712d

    aput v9, v2, v8

    const/16 v8, 0x6803

    aput v8, v2, v7

    const/16 v7, -0x7df7

    aput v7, v2, v6

    const/16 v6, -0x19

    aput v6, v2, v3

    const/16 v3, -0x6f

    aput v3, v2, v1

    const/16 v1, 0x20

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, 0x11

    const/16 v23, 0x12

    const/16 v24, 0x13

    const/16 v25, 0x14

    const/16 v26, 0x15

    const/16 v27, 0x16

    const/16 v28, 0x17

    const/16 v29, 0x18

    const/16 v30, 0x19

    const/16 v31, 0x1a

    const/16 v32, 0x1b

    const/16 v33, 0x1c

    const/16 v34, 0x1d

    const/16 v35, 0x1e

    const/16 v36, 0x1f

    const/16 v37, 0x1e60

    aput v37, v1, v36

    const/16 v36, 0x621e

    aput v36, v1, v35

    const/16 v35, -0x3e9e

    aput v35, v1, v34

    const/16 v34, -0x3f

    aput v34, v1, v33

    const/16 v33, 0x3d1c

    aput v33, v1, v32

    const/16 v32, -0x9c3

    aput v32, v1, v31

    const/16 v31, -0xa

    aput v31, v1, v30

    const/16 v30, -0x5d

    aput v30, v1, v29

    const/16 v29, 0x510e

    aput v29, v1, v28

    const/16 v28, -0x49af

    aput v28, v1, v27

    const/16 v27, -0x4a

    aput v27, v1, v26

    const/16 v26, -0x6c

    aput v26, v1, v25

    const/16 v25, -0x197

    aput v25, v1, v24

    const/16 v24, -0x2

    aput v24, v1, v23

    const/16 v23, 0x7907

    aput v23, v1, v22

    const/16 v22, 0xf79

    aput v22, v1, v21

    const/16 v21, -0x51f1

    aput v21, v1, v20

    const/16 v20, -0x52

    aput v20, v1, v19

    const/16 v19, -0x65ea

    aput v19, v1, v18

    const/16 v18, -0x66

    aput v18, v1, v17

    const/16 v17, -0x72

    aput v17, v1, v16

    const/16 v16, -0x76bc

    aput v16, v1, v15

    const/16 v15, -0x77

    aput v15, v1, v14

    const/16 v14, 0x733d

    aput v14, v1, v13

    const/16 v13, 0x4373

    aput v13, v1, v12

    const/16 v12, 0x1743

    aput v12, v1, v11

    const/16 v11, 0x4c17

    aput v11, v1, v10

    const/16 v10, 0x714c

    aput v10, v1, v9

    const/16 v9, 0x6871

    aput v9, v1, v8

    const/16 v8, -0x7d98

    aput v8, v1, v7

    const/16 v7, -0x7e

    aput v7, v1, v6

    const/16 v6, -0x3a

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_4
    array-length v6, v1

    if-lt v3, v6, :cond_5

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_5
    array-length v6, v1

    if-lt v3, v6, :cond_6

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move/from16 v0, p1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const/16 v1, 0x8

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x3644

    aput v12, v2, v11

    const/16 v11, -0x35f5

    aput v11, v2, v10

    const/16 v10, -0x16

    aput v10, v2, v9

    const/16 v9, -0x1c

    aput v9, v2, v8

    const/16 v8, 0x4d3b

    aput v8, v2, v7

    const/16 v7, -0x50e8

    aput v7, v2, v6

    const/16 v6, -0x71

    aput v6, v2, v3

    const/16 v3, -0x2cfb

    aput v3, v2, v1

    const/16 v1, 0x8

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x3664

    aput v13, v1, v12

    const/16 v12, -0x35ca

    aput v12, v1, v11

    const/16 v11, -0x36

    aput v11, v1, v10

    const/16 v10, -0x53

    aput v10, v1, v9

    const/16 v9, 0x4d69

    aput v9, v1, v8

    const/16 v8, -0x50b3

    aput v8, v1, v7

    const/16 v7, -0x51

    aput v7, v1, v6

    const/16 v6, -0x2cd4

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_6
    array-length v6, v1

    if-lt v3, v6, :cond_7

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_7
    array-length v6, v1

    if-lt v3, v6, :cond_8

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual/range {p2 .. p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$WearableStatusObserver;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->handler:Landroid/os/Handler;

    const/16 v2, 0x72

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$WearableStatusObserver;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    iget-object v2, v2, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    :goto_8
    invoke-super/range {p0 .. p1}, Landroid/database/ContentObserver;->onChange(Z)V

    return-void

    :cond_1
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_2
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_3
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    :cond_4
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3

    :cond_5
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_4

    :cond_6
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_5

    :cond_7
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_6

    :cond_8
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_7

    :catch_0
    move-exception v1

    goto :goto_8
.end method
