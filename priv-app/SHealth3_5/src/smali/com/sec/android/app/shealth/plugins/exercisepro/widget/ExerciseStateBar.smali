.class public Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateBar;
.super Landroid/view/View;
.source "ExerciseStateBar.java"


# instance fields
.field colors:[I

.field private mBitmap:Landroid/graphics/Bitmap;

.field private mCanvas:Landroid/graphics/Canvas;

.field private mGradient:Landroid/graphics/LinearGradient;

.field private mPaint:Landroid/graphics/Paint;

.field private mStateColor:I

.field private mStateColorPoint:I

.field public positions:[F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const v6, 0x7f070196

    const v5, 0x7f070195

    const/4 v4, 0x5

    .line 32
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 22
    new-array v0, v4, [I

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateBar;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateBar;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateBar;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070197

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateBar;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateBar;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    aput v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateBar;->colors:[I

    .line 29
    new-array v0, v4, [F

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateBar;->positions:[F

    .line 33
    return-void

    .line 29
    :array_0
    .array-data 4
        0x0
        0x3e851eb8    # 0.26f
        0x3f028f5c    # 0.51f
        0x3f428f5c    # 0.76f
        0x3f800000    # 1.0f
    .end array-data
.end method


# virtual methods
.method public getStateBarColor()I
    .locals 1

    .prologue
    .line 60
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateBar;->mStateColor:I

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v3, 0x0

    .line 49
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateBar;->mStateColorPoint:I

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateBar;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 50
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateBar;->mStateColorPoint:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateBar;->mStateColorPoint:I

    .line 52
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateBar;->mBitmap:Landroid/graphics/Bitmap;

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateBar;->mStateColorPoint:I

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateBar;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateBar;->mStateColor:I

    .line 53
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateBar;->mBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateBar;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v3, v3, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 54
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 8
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    const/4 v1, 0x0

    .line 37
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    .line 38
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateBar;->mPaint:Landroid/graphics/Paint;

    .line 39
    new-instance v0, Landroid/graphics/LinearGradient;

    int-to-float v3, p1

    int-to-float v4, p2

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateBar;->colors:[I

    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateBar;->positions:[F

    sget-object v7, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    move v2, v1

    invoke-direct/range {v0 .. v7}, Landroid/graphics/LinearGradient;-><init>(FFFF[I[FLandroid/graphics/Shader$TileMode;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateBar;->mGradient:Landroid/graphics/LinearGradient;

    .line 40
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, p2, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateBar;->mBitmap:Landroid/graphics/Bitmap;

    .line 41
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateBar;->mBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateBar;->mCanvas:Landroid/graphics/Canvas;

    .line 43
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateBar;->mPaint:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateBar;->mGradient:Landroid/graphics/LinearGradient;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 44
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateBar;->mCanvas:Landroid/graphics/Canvas;

    int-to-float v3, p1

    int-to-float v4, p2

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateBar;->mPaint:Landroid/graphics/Paint;

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 45
    return-void
.end method

.method public setStateBarColor(I)V
    .locals 0
    .param p1, "point"    # I

    .prologue
    .line 57
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateBar;->mStateColorPoint:I

    .line 58
    return-void
.end method
