.class public final enum Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;
.super Ljava/lang/Enum;
.source "IRestoreListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/backup/server/IRestoreListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "RestoreStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;

.field public static final enum DONE:Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;

.field public static final enum ONGOING:Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;

.field public static final enum ON_WIFI_TO_CONTINUE:Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;

.field public static final enum STARTED:Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;

.field public static final enum STOPPED:Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 11
    new-instance v0, Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;

    const-string v1, "STARTED"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;->STARTED:Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;

    .line 16
    new-instance v0, Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;

    const-string v1, "ONGOING"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;->ONGOING:Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;

    .line 21
    new-instance v0, Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;

    const-string v1, "DONE"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;->DONE:Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;

    .line 26
    new-instance v0, Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;

    const-string v1, "STOPPED"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;->STOPPED:Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;

    .line 30
    new-instance v0, Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;

    const-string v1, "ON_WIFI_TO_CONTINUE"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;->ON_WIFI_TO_CONTINUE:Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;

    .line 6
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;

    sget-object v1, Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;->STARTED:Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;->ONGOING:Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;->DONE:Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;->STOPPED:Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;->ON_WIFI_TO_CONTINUE:Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;

    aput-object v1, v0, v6

    sput-object v0, Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;->$VALUES:[Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 6
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 6
    const-class v0, Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;
    .locals 1

    .prologue
    .line 6
    sget-object v0, Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;->$VALUES:[Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;

    return-object v0
.end method
