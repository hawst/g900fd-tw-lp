.class Lcom/sec/android/app/shealth/stress/StressSummaryFragment$16;
.super Ljava/lang/Object;
.source "StressSummaryFragment.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->animate(Landroid/widget/ImageView;Landroid/widget/ImageView;[I[Ljava/lang/String;IZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

.field final synthetic val$imageView1:Landroid/widget/ImageView;

.field final synthetic val$imageView2:Landroid/widget/ImageView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;Landroid/widget/ImageView;Landroid/widget/ImageView;)V
    .locals 0

    .prologue
    .line 1689
    iput-object p1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$16;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    iput-object p2, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$16;->val$imageView2:Landroid/widget/ImageView;

    iput-object p3, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$16;->val$imageView1:Landroid/widget/ImageView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    const/4 v1, 0x0

    .line 1691
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$16;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    iget-object v0, v0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->animationFadeIn:Landroid/view/animation/AnimationSet;

    if-ne p1, v0, :cond_1

    .line 1692
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$16;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->isImage1Found:Z

    if-eqz v0, :cond_0

    .line 1693
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$16;->val$imageView2:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1695
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$16;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->isImage2Found:Z

    if-eqz v0, :cond_1

    .line 1696
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$16;->val$imageView1:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1699
    :cond_1
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 1702
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 1705
    return-void
.end method
