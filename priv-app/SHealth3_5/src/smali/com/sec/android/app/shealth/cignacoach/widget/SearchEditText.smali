.class public Lcom/sec/android/app/shealth/cignacoach/widget/SearchEditText;
.super Landroid/widget/FrameLayout;
.source "SearchEditText.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private editText:Landroid/widget/EditText;

.field private hintText:Landroid/widget/TextView;

.field private mSearchIcon:Landroid/widget/ImageView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/widget/SearchEditText;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/widget/SearchEditText;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 28
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/widget/SearchEditText;->init(Landroid/content/Context;)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/widget/SearchEditText;->init(Landroid/content/Context;)V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 37
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 38
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/widget/SearchEditText;->init(Landroid/content/Context;)V

    .line 39
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/cignacoach/widget/SearchEditText;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/widget/SearchEditText;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/SearchEditText;->editText:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/cignacoach/widget/SearchEditText;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/widget/SearchEditText;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/SearchEditText;->hintText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/cignacoach/widget/SearchEditText;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/widget/SearchEditText;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/SearchEditText;->mSearchIcon:Landroid/widget/ImageView;

    return-object v0
.end method

.method private init(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 42
    const v1, 0x7f030061

    invoke-static {p1, v1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 44
    const v1, 0x7f0801d7

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/SearchEditText;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/SearchEditText;->editText:Landroid/widget/EditText;

    .line 45
    const v1, 0x7f0801d9

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/SearchEditText;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/SearchEditText;->hintText:Landroid/widget/TextView;

    .line 46
    const v1, 0x7f0801d8

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/SearchEditText;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/SearchEditText;->mSearchIcon:Landroid/widget/ImageView;

    .line 47
    const v1, 0x7f0801da

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/SearchEditText;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 48
    .local v0, "cancelIcon":Landroid/view/View;
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 49
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 50
    new-instance v1, Lcom/sec/android/app/shealth/cignacoach/widget/SearchEditText$1;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/shealth/cignacoach/widget/SearchEditText$1;-><init>(Lcom/sec/android/app/shealth/cignacoach/widget/SearchEditText;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 60
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/SearchEditText;->editText:Landroid/widget/EditText;

    new-instance v2, Lcom/sec/android/app/shealth/cignacoach/widget/SearchEditText$2;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/SearchEditText$2;-><init>(Lcom/sec/android/app/shealth/cignacoach/widget/SearchEditText;Landroid/view/View;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 81
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/SearchEditText;->editText:Landroid/widget/EditText;

    new-instance v2, Lcom/sec/android/app/shealth/cignacoach/widget/SearchEditText$3;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/cignacoach/widget/SearchEditText$3;-><init>(Lcom/sec/android/app/shealth/cignacoach/widget/SearchEditText;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 92
    return-void
.end method


# virtual methods
.method public addTextChangedListener(Landroid/text/TextWatcher;)V
    .locals 1
    .param p1, "textWatcher"    # Landroid/text/TextWatcher;

    .prologue
    .line 115
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/SearchEditText;->editText:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 116
    return-void
.end method

.method public getEditText()Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/SearchEditText;->editText:Landroid/widget/EditText;

    return-object v0
.end method

.method public getText()Landroid/text/Editable;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/SearchEditText;->editText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    return-object v0
.end method

.method public setHint(I)V
    .locals 1
    .param p1, "resId"    # I

    .prologue
    .line 111
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/SearchEditText;->hintText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 112
    return-void
.end method

.method public setHint(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/CharSequence;

    .prologue
    .line 107
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/SearchEditText;->hintText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 108
    return-void
.end method

.method public setImeOptions(I)V
    .locals 1
    .param p1, "imeOptions"    # I

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/SearchEditText;->editText:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setImeOptions(I)V

    .line 122
    return-void
.end method

.method public setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V
    .locals 1
    .param p1, "onEditorActionListener"    # Landroid/widget/TextView$OnEditorActionListener;

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/SearchEditText;->editText:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 126
    return-void
.end method

.method public setText(I)V
    .locals 1
    .param p1, "resId"    # I

    .prologue
    .line 103
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/SearchEditText;->editText:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(I)V

    .line 104
    return-void
.end method

.method public setText(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/CharSequence;

    .prologue
    .line 95
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/SearchEditText;->editText:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 96
    return-void
.end method
