.class Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$5;
.super Ljava/lang/Object;
.source "SleepMonitorSummaryFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->mAlertDialog_GMLanch()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)V
    .locals 0

    .prologue
    .line 1129
    iput-object p1, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$5;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onOKButtonClick(Landroid/app/Activity;Landroid/view/View;Landroid/app/Dialog;)V
    .locals 6
    .param p1, "parentActivity"    # Landroid/app/Activity;
    .param p2, "content"    # Landroid/view/View;
    .param p3, "dialog"    # Landroid/app/Dialog;

    .prologue
    const/4 v5, 0x0

    .line 1134
    iget-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$5;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    iput-boolean v5, v3, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->isSync:Z

    .line 1136
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$5;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 1137
    .local v2, "pm":Landroid/content/pm/PackageManager;
    const-string v3, "com.samsung.android.app.watchmanager"

    invoke-virtual {v2, v3}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1139
    .local v0, "LaunchIntent":Landroid/content/Intent;
    if-eqz v0, :cond_0

    .line 1140
    iget-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$5;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V

    .line 1150
    .end local v0    # "LaunchIntent":Landroid/content/Intent;
    .end local v2    # "pm":Landroid/content/pm/PackageManager;
    :goto_0
    return-void

    .line 1142
    .restart local v0    # "LaunchIntent":Landroid/content/Intent;
    .restart local v2    # "pm":Landroid/content/pm/PackageManager;
    :cond_0
    new-instance v3, Landroid/content/ActivityNotFoundException;

    invoke-direct {v3}, Landroid/content/ActivityNotFoundException;-><init>()V

    throw v3
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1143
    .end local v0    # "LaunchIntent":Landroid/content/Intent;
    .end local v2    # "pm":Landroid/content/pm/PackageManager;
    :catch_0
    move-exception v1

    .line 1144
    .local v1, "e":Landroid/content/ActivityNotFoundException;
    iget-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$5;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const-string v4, "Application is not available on your device !!!"

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
