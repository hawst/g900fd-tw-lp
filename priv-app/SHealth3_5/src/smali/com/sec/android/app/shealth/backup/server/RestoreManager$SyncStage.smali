.class final enum Lcom/sec/android/app/shealth/backup/server/RestoreManager$SyncStage;
.super Ljava/lang/Enum;
.source "RestoreManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/backup/server/RestoreManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "SyncStage"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/backup/server/RestoreManager$SyncStage;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/backup/server/RestoreManager$SyncStage;

.field public static final enum DATABASE:Lcom/sec/android/app/shealth/backup/server/RestoreManager$SyncStage;

.field public static final enum IMAGE:Lcom/sec/android/app/shealth/backup/server/RestoreManager$SyncStage;

.field public static final enum PREFERENCE:Lcom/sec/android/app/shealth/backup/server/RestoreManager$SyncStage;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 85
    new-instance v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager$SyncStage;

    const-string v1, "DATABASE"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/backup/server/RestoreManager$SyncStage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager$SyncStage;->DATABASE:Lcom/sec/android/app/shealth/backup/server/RestoreManager$SyncStage;

    .line 87
    new-instance v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager$SyncStage;

    const-string v1, "IMAGE"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/shealth/backup/server/RestoreManager$SyncStage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager$SyncStage;->IMAGE:Lcom/sec/android/app/shealth/backup/server/RestoreManager$SyncStage;

    .line 89
    new-instance v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager$SyncStage;

    const-string v1, "PREFERENCE"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/shealth/backup/server/RestoreManager$SyncStage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager$SyncStage;->PREFERENCE:Lcom/sec/android/app/shealth/backup/server/RestoreManager$SyncStage;

    .line 82
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/android/app/shealth/backup/server/RestoreManager$SyncStage;

    sget-object v1, Lcom/sec/android/app/shealth/backup/server/RestoreManager$SyncStage;->DATABASE:Lcom/sec/android/app/shealth/backup/server/RestoreManager$SyncStage;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/shealth/backup/server/RestoreManager$SyncStage;->IMAGE:Lcom/sec/android/app/shealth/backup/server/RestoreManager$SyncStage;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/shealth/backup/server/RestoreManager$SyncStage;->PREFERENCE:Lcom/sec/android/app/shealth/backup/server/RestoreManager$SyncStage;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager$SyncStage;->$VALUES:[Lcom/sec/android/app/shealth/backup/server/RestoreManager$SyncStage;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 82
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/backup/server/RestoreManager$SyncStage;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 82
    const-class v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager$SyncStage;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager$SyncStage;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/backup/server/RestoreManager$SyncStage;
    .locals 1

    .prologue
    .line 82
    sget-object v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager$SyncStage;->$VALUES:[Lcom/sec/android/app/shealth/backup/server/RestoreManager$SyncStage;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/backup/server/RestoreManager$SyncStage;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/backup/server/RestoreManager$SyncStage;

    return-object v0
.end method
