.class Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$20;
.super Ljava/lang/Object;
.source "SearchFragment.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->updateScrollListener(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

.field final synthetic val$result:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;)V
    .locals 0

    .prologue
    .line 1051
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$20;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    iput-object p2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$20;->val$result:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 5
    .param p1, "absListView"    # Landroid/widget/AbsListView;
    .param p2, "firstVisibleItem"    # I
    .param p3, "visibleItemCount"    # I
    .param p4, "totalItemCount"    # I

    .prologue
    .line 1060
    add-int v0, p2, p3

    .line 1061
    .local v0, "lastItem":I
    if-ne v0, p4, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$20;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mLoadMoreTask:Lcom/sec/android/app/shealth/food/foodpick/loader/Task;
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->access$2700(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)Lcom/sec/android/app/shealth/food/foodpick/loader/Task;

    move-result-object v2

    if-nez v2, :cond_0

    .line 1062
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$20;->val$result:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$20;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mFoodSearchApi:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;
    invoke-static {v3}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->access$2300(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;->createNextSearchListRequest(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;

    move-result-object v1

    .line 1063
    .local v1, "nextRequest":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;
    if-nez v1, :cond_1

    .line 1069
    .end local v1    # "nextRequest":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;
    :cond_0
    :goto_0
    return-void

    .line 1066
    .restart local v1    # "nextRequest":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$20;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    new-instance v3, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$LoadMoreFoodInfoDataTaskForSearch;

    iget-object v4, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$20;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    invoke-direct {v3, v4, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$LoadMoreFoodInfoDataTaskForSearch;-><init>(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;)V

    # setter for: Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mLoadMoreTask:Lcom/sec/android/app/shealth/food/foodpick/loader/Task;
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->access$2702(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;Lcom/sec/android/app/shealth/food/foodpick/loader/Task;)Lcom/sec/android/app/shealth/food/foodpick/loader/Task;

    .line 1067
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$20;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mTaskRunner:Lcom/sec/android/app/shealth/food/foodpick/loader/TaskRunner;
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->access$2800(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)Lcom/sec/android/app/shealth/food/foodpick/loader/TaskRunner;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$20;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mLoadMoreTask:Lcom/sec/android/app/shealth/food/foodpick/loader/Task;
    invoke-static {v3}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->access$2700(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)Lcom/sec/android/app/shealth/food/foodpick/loader/Task;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/sec/android/app/shealth/food/foodpick/loader/TaskRunner;->addTask(Lcom/sec/android/app/shealth/food/foodpick/loader/Task;)V

    goto :goto_0
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0
    .param p1, "absListView"    # Landroid/widget/AbsListView;
    .param p2, "i"    # I

    .prologue
    .line 1055
    return-void
.end method
