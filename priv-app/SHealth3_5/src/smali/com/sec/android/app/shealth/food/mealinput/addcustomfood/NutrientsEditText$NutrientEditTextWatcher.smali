.class Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText$NutrientEditTextWatcher;
.super Lcom/sec/android/app/shealth/common/utils/TextWatcherStub;
.source "NutrientsEditText.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NutrientEditTextWatcher"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;)V
    .locals 0

    .prologue
    .line 110
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText$NutrientEditTextWatcher;->this$0:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;

    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/utils/TextWatcherStub;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText$1;

    .prologue
    .line 110
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText$NutrientEditTextWatcher;-><init>(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;)V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 3
    .param p1, "editable"    # Landroid/text/Editable;

    .prologue
    .line 115
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 116
    .local v0, "changeString":Ljava/lang/String;
    # getter for: Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;->mSystemNumberSeparator:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;->access$200()Ljava/lang/String;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 117
    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 118
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText$NutrientEditTextWatcher;->this$0:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;

    const-string v2, "0."

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;->setText(Ljava/lang/CharSequence;)V

    .line 126
    :goto_0
    return-void

    .line 121
    :cond_0
    const-string v1, "0."

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 123
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText$NutrientEditTextWatcher;->this$0:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;->setSelection(I)V

    .line 125
    :cond_1
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/common/utils/TextWatcherStub;->afterTextChanged(Landroid/text/Editable;)V

    goto :goto_0
.end method
