.class public Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "AddCustomFoodActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerProvider;
.implements Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$EmptyFieldListener;
.implements Lcom/sec/android/app/shealth/food/mealinput/IButtonStateChanger;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$13;,
        Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$DiscardChangesDialogButtonController;
    }
.end annotation


# static fields
.field private static final DISCARD_CHANGES_POPUP_TAG:Ljava/lang/String; = "DISCARD_CHANGES_POPUP_TAG"

.field private static final DOT:Ljava/lang/String; = "."

.field public static final RESULT_MY_FOOD_ID:Ljava/lang/String; = "result_my_food_id"

.field private static final sActionBarButtonDoneIndex:I = 0x1

.field private static final sCalories:Ljava/lang/String; = "sCalories"

.field private static final sCarbos:Ljava/lang/String; = "sCarbos"

.field private static final sDefaultNumberSeparator:Ljava/lang/String; = "."

.field private static final sEmptyString:Ljava/lang/String; = ""

.field private static final sFat:Ljava/lang/String; = "sFat"

.field private static final sIsMyFood:Ljava/lang/String; = "sIsMyFood"

.field private static final sIsShouldAlwaysBeSavedToMyFood:Ljava/lang/String; = "sIsShouldAlwaysBeSavedToMyFood"

.field private static final sName:Ljava/lang/String; = "sName"

.field private static final sNoIconResId:I = 0x0

.field private static final sProteins:Ljava/lang/String; = "sProteins"


# instance fields
.field private mCaloriePerServingEditText:Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;

.field private final mDialogControllerMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;",
            ">;"
        }
    .end annotation
.end field

.field private mExtraFoodInfoDao:Lcom/sec/android/app/shealth/food/fooddao/ExtraFoodInfoDao;

.field private mFoodInfoDao:Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;

.field private mFoodNameAlreadyExistToast:Landroid/widget/Toast;

.field private mFoodNameTextWatcher:Lcom/sec/android/app/shealth/common/utils/MemoTextWatcher;

.field private mIncorrectRangeToast:Landroid/widget/Toast;

.field private mInvalidInputToast:Landroid/widget/Toast;

.field private mInvalidSaveToast:Landroid/widget/Toast;

.field private mIsShouldAlwaysBeSavedToMyFood:Z

.field private mNameOfFoodEditText:Landroid/widget/EditText;

.field private mNutrientCarbo:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

.field private mNutrientFat:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

.field private mNutrientProtein:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

.field private mNutrientsContainer:Landroid/widget/LinearLayout;

.field private mOutOfRangeToast:Landroid/widget/Toast;

.field private mPrevCal:Ljava/lang/String;

.field private mPrevCarbohydrate:Ljava/lang/String;

.field private mPrevFat:Ljava/lang/String;

.field private mPrevName:Ljava/lang/String;

.field private mPrevProtein:Ljava/lang/String;

.field private mSaveInMyFoodCheckBox:Landroid/widget/CheckBox;

.field private mSaveInMyFoodLayout:Landroid/view/View;

.field private mScrollView:Landroid/widget/ScrollView;

.field private mSystemNumberSeparator:Ljava/lang/String;

.field private mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

.field private mViewNutrientFieldButton:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 97
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    .line 135
    new-instance v0, Ljava/text/DecimalFormat;

    invoke-direct {v0}, Ljava/text/DecimalFormat;-><init>()V

    invoke-virtual {v0}, Ljava/text/DecimalFormat;->getDecimalFormatSymbols()Ljava/text/DecimalFormatSymbols;

    move-result-object v0

    invoke-virtual {v0}, Ljava/text/DecimalFormatSymbols;->getDecimalSeparator()C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mSystemNumberSeparator:Ljava/lang/String;

    .line 149
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mIsShouldAlwaysBeSavedToMyFood:Z

    .line 180
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mPrevProtein:Ljava/lang/String;

    .line 182
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mPrevCarbohydrate:Ljava/lang/String;

    .line 184
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mPrevFat:Ljava/lang/String;

    .line 191
    new-instance v0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$1;-><init>(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mDialogControllerMap:Ljava/util/Map;

    .line 1178
    new-instance v0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$11;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$11;-><init>(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mFoodNameTextWatcher:Lcom/sec/android/app/shealth/common/utils/MemoTextWatcher;

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;)Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNutrientFat:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;

    .prologue
    .line 97
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->scrollAsPerNameText()V

    return-void
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;

    .prologue
    .line 97
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->isInvalidState()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;

    .prologue
    .line 97
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->isFoodDataChanged()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;

    .prologue
    .line 97
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->onSaveButtonClick()V

    return-void
.end method

.method static synthetic access$1400(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;

    .prologue
    .line 97
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->onCancelButtonClick()V

    return-void
.end method

.method static synthetic access$1500(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;)Landroid/widget/ScrollView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;

    .prologue
    .line 97
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->getScrollView()Landroid/widget/ScrollView;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1600(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;

    .prologue
    .line 97
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->setResultCanceledAndFinish()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;)Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNutrientCarbo:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;)Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNutrientProtein:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;)Landroid/widget/Toast;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mIncorrectRangeToast:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic access$402(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;Landroid/widget/Toast;)Landroid/widget/Toast;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;
    .param p1, "x1"    # Landroid/widget/Toast;

    .prologue
    .line 97
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mIncorrectRangeToast:Landroid/widget/Toast;

    return-object p1
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;)Landroid/widget/Toast;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mInvalidInputToast:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic access$502(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;Landroid/widget/Toast;)Landroid/widget/Toast;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;
    .param p1, "x1"    # Landroid/widget/Toast;

    .prologue
    .line 97
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mInvalidInputToast:Landroid/widget/Toast;

    return-object p1
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;)Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mCaloriePerServingEditText:Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;IFFFZ)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;
    .param p1, "x1"    # I
    .param p2, "x2"    # F
    .param p3, "x3"    # F
    .param p4, "x4"    # F
    .param p5, "x5"    # Z

    .prologue
    .line 97
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->setVisibilityForNutrientFields(IFFFZ)V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;

    .prologue
    .line 97
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->refreshFocusables()V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mSaveInMyFoodCheckBox:Landroid/widget/CheckBox;

    return-object v0
.end method

.method private addEmptyListener()V
    .locals 1

    .prologue
    .line 1268
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mCaloriePerServingEditText:Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;->setEmptyFieldListener(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$EmptyFieldListener;)V

    .line 1269
    return-void
.end method

.method private checkBundleDataFromIntent()V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 481
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 483
    .local v2, "savedBundle":Landroid/os/Bundle;
    if-eqz v2, :cond_5

    .line 486
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->isEditMyFoodMode()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 489
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mFoodInfoDao:Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;

    const-string v5, "MY_FOOD_INFO_DATA_ID"

    invoke-virtual {v2, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v5

    invoke-interface {v4, v5, v6}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;->getDataById(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    .line 493
    .local v1, "foodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    if-eqz v1, :cond_3

    .line 495
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mPrevName:Ljava/lang/String;

    if-nez v4, :cond_0

    .line 496
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getName()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mPrevName:Ljava/lang/String;

    .line 497
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mPrevCal:Ljava/lang/String;

    if-nez v4, :cond_1

    .line 498
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getkCal()F

    move-result v4

    float-to-int v4, v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mPrevCal:Ljava/lang/String;

    .line 500
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNameOfFoodEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 502
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNameOfFoodEditText:Landroid/widget/EditText;

    iget-object v5, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNameOfFoodEditText:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-interface {v5}, Landroid/text/Editable;->length()I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setSelection(I)V

    .line 504
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mCaloriePerServingEditText:Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getkCal()F

    move-result v5

    float-to-int v5, v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;->setText(Ljava/lang/CharSequence;)V

    .line 506
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mExtraFoodInfoDao:Lcom/sec/android/app/shealth/food/fooddao/ExtraFoodInfoDao;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getId()J

    move-result-wide v5

    invoke-interface {v4, v5, v6}, Lcom/sec/android/app/shealth/food/fooddao/ExtraFoodInfoDao;->getExtraByFoodId(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;

    move-result-object v0

    .line 508
    .local v0, "extraFoodInfo":Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;
    if-eqz v0, :cond_2

    .line 510
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->checkExtraInfoAndChangeVisibility(Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;)V

    .line 513
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mSaveInMyFoodCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->isAddedByUser()Z

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 535
    .end local v0    # "extraFoodInfo":Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;
    .end local v1    # "foodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    :cond_3
    :goto_0
    const-string v4, "SHOULD_ALWAYS_SAVE_TO_MY_FOOD"

    invoke-virtual {v2, v4, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    iput-boolean v4, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mIsShouldAlwaysBeSavedToMyFood:Z

    .line 539
    iget-boolean v4, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mIsShouldAlwaysBeSavedToMyFood:Z

    if-nez v4, :cond_4

    const/4 v3, 0x1

    :cond_4
    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->setSaveMyToFoodCheckboxAreaVisibilityState(Z)V

    .line 543
    :cond_5
    return-void

    .line 521
    :cond_6
    const-string v4, "FOOD_NAME_NOT_FOUND"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 524
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mCaloriePerServingEditText:Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;->requestFocus()Z

    .line 528
    :cond_7
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNameOfFoodEditText:Landroid/widget/EditText;

    const-string v5, "FOOD_NAME_NOT_FOUND"

    const-string v6, ""

    invoke-virtual {v2, v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private checkExtraInfoAndChangeVisibility(Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;)V
    .locals 8
    .param p1, "extraFoodInfoData"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x0

    const/4 v1, 0x0

    .line 998
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getProtein()F

    move-result v2

    .line 1000
    .local v2, "protienValue":F
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getCarbohydrate()F

    move-result v3

    .line 1002
    .local v3, "carboValue":F
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getFat()F

    move-result v4

    .line 1004
    .local v4, "fatValue":F
    cmpl-float v0, v2, v6

    if-gtz v0, :cond_0

    cmpl-float v0, v3, v6

    if-gtz v0, :cond_0

    cmpl-float v0, v4, v6

    if-lez v0, :cond_1

    :cond_0
    move-object v0, p0

    move v5, v1

    .line 1006
    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->setVisibilityForNutrientFields(IFFFZ)V

    .line 1013
    cmpl-float v0, v2, v6

    if-lez v0, :cond_2

    .line 1015
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNutrientProtein:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->getNutrientValueView()Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1016
    invoke-static {v2}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mPrevProtein:Ljava/lang/String;

    .line 1017
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNutrientProtein:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->setVisibility(I)V

    .line 1026
    :goto_0
    cmpl-float v0, v3, v6

    if-lez v0, :cond_3

    .line 1028
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNutrientCarbo:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->getNutrientValueView()Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1029
    invoke-static {v3}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mPrevCarbohydrate:Ljava/lang/String;

    .line 1030
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNutrientCarbo:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->setVisibility(I)V

    .line 1039
    :goto_1
    cmpl-float v0, v4, v6

    if-lez v0, :cond_4

    .line 1041
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNutrientFat:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->getNutrientValueView()Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1042
    invoke-static {v4}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mPrevFat:Ljava/lang/String;

    .line 1043
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNutrientFat:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->setVisibility(I)V

    .line 1052
    :cond_1
    :goto_2
    return-void

    .line 1021
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNutrientProtein:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    invoke-virtual {v0, v7}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->setVisibility(I)V

    .line 1022
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mPrevProtein:Ljava/lang/String;

    .line 1023
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mViewNutrientFieldButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    .line 1034
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNutrientCarbo:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    invoke-virtual {v0, v7}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->setVisibility(I)V

    .line 1035
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mPrevCarbohydrate:Ljava/lang/String;

    .line 1036
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mViewNutrientFieldButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_1

    .line 1047
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNutrientFat:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    invoke-virtual {v0, v7}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->setVisibility(I)V

    .line 1048
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mPrevFat:Ljava/lang/String;

    .line 1049
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mViewNutrientFieldButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_2
.end method

.method private checkNutrientsAndInsert(Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;)Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;
    .locals 8
    .param p1, "extraFoodInfoData"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;

    .prologue
    const/4 v6, 0x0

    .line 1056
    iget-object v7, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNutrientProtein:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->getNutrientValueView()Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;->getValueString()Ljava/lang/String;

    move-result-object v4

    .line 1058
    .local v4, "protein":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNutrientCarbo:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->getNutrientValueView()Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;->getValueString()Ljava/lang/String;

    move-result-object v0

    .line 1060
    .local v0, "carbo":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNutrientFat:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->getNutrientValueView()Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;->getValueString()Ljava/lang/String;

    move-result-object v2

    .line 1063
    .local v2, "fat":Ljava/lang/String;
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_0

    const-string v7, "."

    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    :cond_0
    move v5, v6

    .line 1065
    .local v5, "proteinValue":F
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_1

    const-string v7, "."

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    :cond_1
    move v1, v6

    .line 1067
    .local v1, "carboValue":F
    :goto_1
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_2

    const-string v7, "."

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    :cond_2
    move v3, v6

    .line 1069
    .local v3, "fatValue":F
    :goto_2
    cmpl-float v7, v5, v6

    if-nez v7, :cond_6

    cmpl-float v7, v1, v6

    if-nez v7, :cond_6

    cmpl-float v7, v3, v6

    if-nez v7, :cond_6

    .line 1071
    const/4 p1, 0x0

    .line 1101
    .end local p1    # "extraFoodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;
    :goto_3
    return-object p1

    .line 1063
    .end local v1    # "carboValue":F
    .end local v3    # "fatValue":F
    .end local v5    # "proteinValue":F
    .restart local p1    # "extraFoodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;
    :cond_3
    invoke-static {v4}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v5

    goto :goto_0

    .line 1065
    .restart local v5    # "proteinValue":F
    :cond_4
    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    goto :goto_1

    .line 1067
    .restart local v1    # "carboValue":F
    :cond_5
    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v3

    goto :goto_2

    .line 1074
    .restart local v3    # "fatValue":F
    :cond_6
    cmpl-float v7, v5, v6

    if-lez v7, :cond_7

    .line 1076
    invoke-virtual {p1, v5}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setProtein(F)V

    .line 1083
    :goto_4
    cmpl-float v7, v1, v6

    if-lez v7, :cond_8

    .line 1085
    invoke-virtual {p1, v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setCarbohydrate(F)V

    .line 1092
    :goto_5
    cmpl-float v7, v3, v6

    if-lez v7, :cond_9

    .line 1094
    invoke-virtual {p1, v3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setFat(F)V

    goto :goto_3

    .line 1080
    :cond_7
    invoke-virtual {p1, v6}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setProtein(F)V

    goto :goto_4

    .line 1089
    :cond_8
    invoke-virtual {p1, v6}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setCarbohydrate(F)V

    goto :goto_5

    .line 1098
    :cond_9
    invoke-virtual {p1, v6}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setFat(F)V

    goto :goto_3
.end method

.method private findViews()V
    .locals 4

    .prologue
    .line 256
    const v0, 0x7f080394

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mSaveInMyFoodCheckBox:Landroid/widget/CheckBox;

    .line 258
    const v0, 0x7f080392

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mSaveInMyFoodLayout:Landroid/view/View;

    .line 260
    const v0, 0x7f080397

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNameOfFoodEditText:Landroid/widget/EditText;

    .line 262
    const v0, 0x7f080389

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mCaloriePerServingEditText:Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;

    .line 263
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mCaloriePerServingEditText:Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;->setMinValue(I)V

    .line 264
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mCaloriePerServingEditText:Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    const v3, 0x47c34f80    # 99999.0f

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;->setIntervalValues(FFF)V

    .line 266
    const v0, 0x7f08038b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNutrientsContainer:Landroid/widget/LinearLayout;

    .line 268
    const v0, 0x7f08038e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNutrientProtein:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    .line 270
    const v0, 0x7f08038d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNutrientCarbo:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    .line 272
    const v0, 0x7f08038c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNutrientFat:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    .line 274
    const v0, 0x7f080390

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mViewNutrientFieldButton:Landroid/widget/Button;

    .line 276
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNutrientFat:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->getNutrientValueView()Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$2;-><init>(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 303
    return-void
.end method

.method private getScrollView()Landroid/widget/ScrollView;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mScrollView:Landroid/widget/ScrollView;

    if-nez v0, :cond_0

    .line 169
    const v0, 0x7f080387

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mScrollView:Landroid/widget/ScrollView;

    .line 171
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mScrollView:Landroid/widget/ScrollView;

    return-object v0
.end method

.method private initListeners()V
    .locals 2

    .prologue
    .line 452
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mViewNutrientFieldButton:Landroid/widget/Button;

    new-instance v1, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$5;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$5;-><init>(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 475
    return-void
.end method

.method private initTextWatcher()V
    .locals 3

    .prologue
    .line 604
    new-instance v0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$7;-><init>(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;)V

    .line 641
    .local v0, "refreshTextWatcher":Landroid/text/TextWatcher;
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNameOfFoodEditText:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 643
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNameOfFoodEditText:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mFoodNameTextWatcher:Lcom/sec/android/app/shealth/common/utils/MemoTextWatcher;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 645
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mCaloriePerServingEditText:Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 649
    return-void
.end method

.method private initViews(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 308
    if-nez p1, :cond_0

    .line 311
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->checkBundleDataFromIntent()V

    .line 353
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mCaloriePerServingEditText:Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;

    new-array v2, v2, [Lcom/sec/android/app/shealth/food/utils/MinMaxIntegerInputFilter;

    new-instance v4, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$3;

    const/4 v5, 0x0

    const v6, 0x47c34f80    # 99999.0f

    const/4 v7, 0x2

    invoke-direct {v4, p0, v5, v6, v7}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$3;-><init>(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;FFI)V

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;->setFilters([Landroid/text/InputFilter;)V

    .line 401
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mCaloriePerServingEditText:Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;

    new-instance v2, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$4;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$4;-><init>(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 448
    return-void

    .line 317
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNameOfFoodEditText:Landroid/widget/EditText;

    const-string/jumbo v4, "sName"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 319
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mCaloriePerServingEditText:Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;

    const-string/jumbo v4, "sCalories"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;->setText(Ljava/lang/CharSequence;)V

    .line 321
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mSaveInMyFoodCheckBox:Landroid/widget/CheckBox;

    const-string/jumbo v4, "sIsMyFood"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    invoke-virtual {v1, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 323
    const-string/jumbo v1, "sIsShouldAlwaysBeSavedToMyFood"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mIsShouldAlwaysBeSavedToMyFood:Z

    .line 325
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNutrientProtein:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->getNutrientValueView()Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;

    move-result-object v1

    const-string/jumbo v4, "sProteins"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;->setText(Ljava/lang/CharSequence;)V

    .line 327
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNutrientCarbo:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->getNutrientValueView()Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;

    move-result-object v1

    const-string/jumbo v4, "sCarbos"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;->setText(Ljava/lang/CharSequence;)V

    .line 329
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNutrientFat:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->getNutrientValueView()Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;

    move-result-object v1

    const-string/jumbo v4, "sFat"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;->setText(Ljava/lang/CharSequence;)V

    .line 331
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mIsShouldAlwaysBeSavedToMyFood:Z

    if-nez v1, :cond_4

    move v1, v2

    :goto_1
    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->setSaveMyToFoodCheckboxAreaVisibilityState(Z)V

    .line 333
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;-><init>()V

    .line 335
    .local v0, "restoredExtraInfo":Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;
    const-string/jumbo v1, "sProteins"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    const-string/jumbo v1, "sProteins"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 337
    const-string/jumbo v1, "sProteins"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setProtein(F)V

    .line 339
    :cond_1
    const-string/jumbo v1, "sCarbos"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    const-string/jumbo v1, "sCarbos"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 341
    const-string/jumbo v1, "sCarbos"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setCarbohydrate(F)V

    .line 343
    :cond_2
    const-string/jumbo v1, "sFat"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    const-string/jumbo v1, "sFat"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 345
    const-string/jumbo v1, "sFat"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setFat(F)V

    .line 350
    :cond_3
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->checkExtraInfoAndChangeVisibility(Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;)V

    goto/16 :goto_0

    .end local v0    # "restoredExtraInfo":Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;
    :cond_4
    move v1, v3

    .line 331
    goto :goto_1
.end method

.method private isEditMyFoodMode()Z
    .locals 2

    .prologue
    .line 547
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 549
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "MY_FOOD_INFO_DATA_ID"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    .line 552
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isFoodDataChanged()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1198
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->isEditMyFoodMode()Z

    move-result v3

    if-nez v3, :cond_2

    .line 1200
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNameOfFoodEditText:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mCaloriePerServingEditText:Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNutrientProtein:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->isFieldEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNutrientCarbo:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->isFieldEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNutrientFat:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->isFieldEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    move v0, v1

    .line 1202
    .local v0, "isAllFieldsEmpty":Z
    :goto_0
    if-nez v0, :cond_1

    .line 1204
    .end local v0    # "isAllFieldsEmpty":Z
    :goto_1
    return v1

    :cond_0
    move v0, v2

    .line 1200
    goto :goto_0

    .restart local v0    # "isAllFieldsEmpty":Z
    :cond_1
    move v1, v2

    .line 1202
    goto :goto_1

    .line 1204
    .end local v0    # "isAllFieldsEmpty":Z
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNameOfFoodEditText:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mPrevName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mCaloriePerServingEditText:Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mPrevCal:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNutrientProtein:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->getNutrientValueView()Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mPrevProtein:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNutrientCarbo:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->getNutrientValueView()Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mPrevCarbohydrate:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNutrientFat:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->getNutrientValueView()Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mPrevFat:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    :cond_3
    move v2, v1

    :cond_4
    move v1, v2

    goto :goto_1
.end method

.method private isInvalidState()Z
    .locals 3

    .prologue
    const v2, 0x7f090992

    const/4 v0, 0x1

    .line 1323
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mCaloriePerServingEditText:Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/common/utils/Utils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNameOfFoodEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/common/utils/Utils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->isNutritionInfoAdded()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1325
    const v1, 0x7f090991

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->showInvalidSaveToast(I)V

    .line 1326
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->finish()V

    .line 1358
    :goto_0
    return v0

    .line 1329
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNameOfFoodEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/common/utils/Utils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mCaloriePerServingEditText:Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/common/utils/Utils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->isNutritionInfoAdded()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1331
    :cond_1
    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->showInvalidSaveToast(I)V

    .line 1332
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNameOfFoodEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->requestFocus()Z

    .line 1333
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNameOfFoodEditText:Landroid/widget/EditText;

    invoke-static {p0, v1}, Lcom/sec/android/app/shealth/common/utils/SoftInputUtils;->showSoftInput(Landroid/content/Context;Landroid/view/View;)V

    .line 1334
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->scrollAsPerNameText()V

    goto :goto_0

    .line 1337
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mCaloriePerServingEditText:Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/common/utils/Utils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNameOfFoodEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/common/utils/Utils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->isNutritionInfoAdded()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1340
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mCaloriePerServingEditText:Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;->isFocused()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1341
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->processEmptyField()V

    goto :goto_0

    .line 1343
    :cond_4
    const v1, 0x7f090993

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->showInvalidSaveToast(I)V

    .line 1344
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mCaloriePerServingEditText:Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;->requestFocus()Z

    .line 1345
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mCaloriePerServingEditText:Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;

    invoke-static {p0, v1}, Lcom/sec/android/app/shealth/common/utils/SoftInputUtils;->showSoftInput(Landroid/content/Context;Landroid/view/View;)V

    goto :goto_0

    .line 1350
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mCaloriePerServingEditText:Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/common/utils/Utils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNameOfFoodEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/common/utils/Utils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->isNutritionInfoAdded()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1352
    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->showInvalidSaveToast(I)V

    .line 1353
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNameOfFoodEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->requestFocus()Z

    .line 1354
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNameOfFoodEditText:Landroid/widget/EditText;

    invoke-static {p0, v1}, Lcom/sec/android/app/shealth/common/utils/SoftInputUtils;->showSoftInput(Landroid/content/Context;Landroid/view/View;)V

    .line 1355
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->scrollAsPerNameText()V

    goto/16 :goto_0

    .line 1358
    :cond_6
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method private isNutritionInfoAdded()Z
    .locals 1

    .prologue
    .line 1303
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNutrientProtein:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->getNutrientValueView()Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNutrientCarbo:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->getNutrientValueView()Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNutrientFat:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->getNutrientValueView()Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onCancelButtonClick()V
    .locals 3

    .prologue
    .line 824
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->isFoodDataChanged()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 827
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const/4 v1, 0x2

    const v2, 0x7f090081

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;II)V

    const v1, 0x7f090044

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090083

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "DISCARD_CHANGES_POPUP_TAG"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 843
    :goto_0
    return-void

    .line 839
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->setResultCanceledAndFinish()V

    goto :goto_0
.end method

.method private onSaveButtonClick()V
    .locals 14

    .prologue
    const/4 v13, 0x0

    .line 729
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->isEditMyFoodMode()Z

    move-result v10

    if-nez v10, :cond_1

    invoke-static {p0}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->isMyFoodItemsLimitExceed(Landroid/content/Context;)Z

    move-result v10

    if-eqz v10, :cond_1

    iget-object v10, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mSaveInMyFoodCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v10}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v10

    if-eqz v10, :cond_1

    .line 735
    invoke-static {p0}, Lcom/sec/android/app/shealth/common/utils/SoftInputUtils;->hideSoftInput(Landroid/app/Activity;)V

    .line 737
    invoke-static {p0}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->showToastAboutMyFoodItemsLimitExceeded(Landroid/content/Context;)V

    .line 819
    :cond_0
    :goto_0
    return-void

    .line 743
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->isEditMyFoodMode()Z

    move-result v10

    if-eqz v10, :cond_5

    iget-object v10, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mFoodInfoDao:Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->getIntent()Landroid/content/Intent;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v11

    const-string v12, "MY_FOOD_INFO_DATA_ID"

    invoke-virtual {v11, v12}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v11

    invoke-interface {v10, v11, v12}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;->getDataById(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;

    move-result-object v10

    check-cast v10, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    move-object v3, v10

    .line 750
    .local v3, "foodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    :goto_1
    iget-object v10, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mExtraFoodInfoDao:Lcom/sec/android/app/shealth/food/fooddao/ExtraFoodInfoDao;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->getIntent()Landroid/content/Intent;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v11

    const-string v12, "MY_FOOD_INFO_DATA_ID"

    invoke-virtual {v11, v12}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v11

    invoke-interface {v10, v11, v12}, Lcom/sec/android/app/shealth/food/fooddao/ExtraFoodInfoDao;->getExtraByFoodId(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;

    move-result-object v2

    .line 751
    .local v2, "extraFoodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;
    if-nez v2, :cond_2

    .line 753
    new-instance v2, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;

    .end local v2    # "extraFoodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;
    invoke-direct {v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;-><init>()V

    .line 757
    .restart local v2    # "extraFoodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;
    :cond_2
    iget-object v10, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mSaveInMyFoodCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v10}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v10

    if-eqz v10, :cond_6

    .line 760
    invoke-static {p0}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->getMyFoods(Landroid/content/Context;)Ljava/util/List;

    move-result-object v9

    .line 762
    .local v9, "myFoods":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;>;"
    invoke-interface {v9, v3}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 764
    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    .line 767
    .local v1, "data":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getName()Ljava/lang/String;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNameOfFoodEditText:Landroid/widget/EditText;

    invoke-virtual {v11}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 770
    iget-object v10, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNameOfFoodEditText:Landroid/widget/EditText;

    invoke-static {p0, v10}, Lcom/sec/android/app/shealth/common/utils/SoftInputUtils;->showSoftInput(Landroid/content/Context;Landroid/view/View;)V

    .line 772
    iget-object v10, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mFoodNameAlreadyExistToast:Landroid/widget/Toast;

    if-nez v10, :cond_4

    .line 775
    const v10, 0x7f09098f

    const/4 v11, 0x1

    invoke-static {p0, v10, v11}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v10

    iput-object v10, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mFoodNameAlreadyExistToast:Landroid/widget/Toast;

    .line 779
    :cond_4
    iget-object v10, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mFoodNameAlreadyExistToast:Landroid/widget/Toast;

    invoke-virtual {v10}, Landroid/widget/Toast;->getView()Landroid/view/View;

    move-result-object v10

    invoke-virtual {v10}, Landroid/view/View;->getWindowVisibility()I

    move-result v10

    if-eqz v10, :cond_0

    .line 782
    iget-object v10, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mFoodNameAlreadyExistToast:Landroid/widget/Toast;

    invoke-virtual {v10}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 743
    .end local v1    # "data":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    .end local v2    # "extraFoodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;
    .end local v3    # "foodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v9    # "myFoods":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;>;"
    :cond_5
    new-instance v3, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    invoke-direct {v3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;-><init>()V

    goto :goto_1

    .line 794
    .restart local v2    # "extraFoodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;
    .restart local v3    # "foodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    :cond_6
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    .line 795
    .local v0, "currentFocus":Landroid/view/View;
    if-eqz v0, :cond_7

    instance-of v10, v0, Landroid/widget/EditText;

    if-eqz v10, :cond_7

    .line 797
    const-string v10, "input_method"

    invoke-virtual {p0, v10}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/view/inputmethod/InputMethodManager;

    .line 798
    .local v7, "inputManager":Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {v0}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v10

    invoke-virtual {v7, v10, v13}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 801
    .end local v7    # "inputManager":Landroid/view/inputmethod/InputMethodManager;
    :cond_7
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->getIntent()Landroid/content/Intent;

    move-result-object v8

    .line 803
    .local v8, "intent":Landroid/content/Intent;
    invoke-direct {p0, v3, v2}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->saveFood(Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;)J

    move-result-wide v5

    .line 805
    .local v5, "id":J
    const-string/jumbo v10, "result_my_food_id"

    invoke-virtual {v8, v10, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 807
    const/4 v10, -0x1

    invoke-virtual {p0, v10, v8}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->setResult(ILandroid/content/Intent;)V

    .line 809
    const v10, 0x7f090835

    invoke-virtual {p0, v10}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-static {p0, v10, v13}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v10

    invoke-virtual {v10}, Landroid/widget/Toast;->show()V

    .line 812
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->isEditMyFoodMode()Z

    move-result v10

    if-nez v10, :cond_8

    .line 813
    iget-object v10, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    sget-object v11, Lcom/sec/android/app/shealth/food/app/UserActionLog;->DONE_ADD_MY_FOOD:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    invoke-interface {v10, v11}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;->logUserAction(Lcom/sec/android/app/shealth/food/app/UserActionLog;)V

    .line 817
    :cond_8
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->finish()V

    goto/16 :goto_0
.end method

.method private saveFood(Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;)J
    .locals 3
    .param p1, "foodInfoData"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    .param p2, "extraFoodInfoData"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;

    .prologue
    .line 881
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNameOfFoodEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setName(Ljava/lang/String;)V

    .line 883
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mCaloriePerServingEditText:Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 885
    .local v0, "kcal":I
    int-to-float v1, v0

    invoke-virtual {p1, v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setkCal(F)V

    .line 887
    invoke-direct {p0, p2}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->checkNutrientsAndInsert(Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;)Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;

    move-result-object p2

    .line 889
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mSaveInMyFoodCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 892
    const v1, 0x46cd1

    invoke-virtual {p1, v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setServerSourceType(I)V

    .line 894
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->CHECK_SAVE_IN_MY_FOOD:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    invoke-interface {v1, v2}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;->logUserAction(Lcom/sec/android/app/shealth/food/app/UserActionLog;)V

    .line 906
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mFoodInfoDao:Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;

    invoke-static {p1, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/DataBaseUpdater;->insertOrUpdateData(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;Lcom/sec/android/app/shealth/common/commondao/CommonDao;)V

    .line 908
    if-eqz p2, :cond_0

    .line 910
    const/4 v1, 0x0

    invoke-virtual {p2, v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setGrammInKcal(F)V

    .line 911
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getId()J

    move-result-wide v1

    invoke-virtual {p2, v1, v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setFoodInfoId(J)V

    .line 912
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mExtraFoodInfoDao:Lcom/sec/android/app/shealth/food/fooddao/ExtraFoodInfoDao;

    invoke-static {p2, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/DataBaseUpdater;->insertOrUpdateData(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;Lcom/sec/android/app/shealth/common/commondao/CommonDao;)V

    .line 915
    :cond_0
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getId()J

    move-result-wide v1

    return-wide v1

    .line 900
    :cond_1
    const/4 v1, -0x1

    invoke-virtual {p1, v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setServerSourceType(I)V

    .line 902
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->UNCHECK_SAVE_IN_MY_FOOD:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    invoke-interface {v1, v2}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;->logUserAction(Lcom/sec/android/app/shealth/food/app/UserActionLog;)V

    goto :goto_0
.end method

.method private scrollAsPerNameText()V
    .locals 2

    .prologue
    .line 1307
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->getScrollView()Landroid/widget/ScrollView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ScrollView;->getScrollY()I

    move-result v0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->getScrollView()Landroid/widget/ScrollView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ScrollView;->getTop()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 1310
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->getScrollView()Landroid/widget/ScrollView;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$12;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$12;-><init>(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->post(Ljava/lang/Runnable;)Z

    .line 1319
    :cond_0
    return-void
.end method

.method private setImeOptions()V
    .locals 7

    .prologue
    const/4 v6, 0x5

    const/4 v5, 0x6

    .line 1216
    const/4 v3, 0x0

    .line 1217
    .local v3, "isDoneSet":Z
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNutrientsContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v4

    if-nez v4, :cond_4

    const/4 v2, 0x1

    .line 1219
    .local v2, "isContainerVisible":Z
    :goto_0
    if-eqz v2, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNutrientProtein:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->getVisibility()I

    move-result v4

    if-nez v4, :cond_0

    .line 1221
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNutrientProtein:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->getNutrientValueView()Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;

    move-result-object v4

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;->setImeOptions(I)V

    .line 1222
    const/4 v3, 0x1

    .line 1224
    :cond_0
    if-eqz v2, :cond_1

    iget-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNutrientCarbo:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->getVisibility()I

    move-result v4

    if-nez v4, :cond_1

    .line 1226
    if-eqz v3, :cond_5

    .line 1228
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNutrientCarbo:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->getNutrientValueView()Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;

    move-result-object v4

    invoke-virtual {v4, v6}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;->setImeOptions(I)V

    .line 1236
    :cond_1
    :goto_1
    if-eqz v2, :cond_2

    iget-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNutrientFat:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->getVisibility()I

    move-result v4

    if-nez v4, :cond_2

    .line 1238
    if-eqz v3, :cond_6

    .line 1240
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNutrientFat:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->getNutrientValueView()Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;

    move-result-object v4

    invoke-virtual {v4, v6}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;->setImeOptions(I)V

    .line 1249
    :cond_2
    :goto_2
    if-eqz v3, :cond_7

    .line 1251
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mCaloriePerServingEditText:Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;

    invoke-virtual {v4, v6}, Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;->setImeOptions(I)V

    .line 1258
    :goto_3
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    .line 1259
    .local v0, "currentFocusView":Landroid/view/View;
    if-eqz v0, :cond_3

    instance-of v4, v0, Landroid/widget/EditText;

    if-eqz v4, :cond_3

    .line 1260
    const-string v4, "input_method"

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    .line 1261
    .local v1, "inputManager":Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {v1, v0}, Landroid/view/inputmethod/InputMethodManager;->restartInput(Landroid/view/View;)V

    .line 1265
    .end local v1    # "inputManager":Landroid/view/inputmethod/InputMethodManager;
    :cond_3
    return-void

    .line 1217
    .end local v0    # "currentFocusView":Landroid/view/View;
    .end local v2    # "isContainerVisible":Z
    :cond_4
    const/4 v2, 0x0

    goto :goto_0

    .line 1232
    .restart local v2    # "isContainerVisible":Z
    :cond_5
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNutrientCarbo:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->getNutrientValueView()Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;

    move-result-object v4

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;->setImeOptions(I)V

    .line 1233
    const/4 v3, 0x1

    goto :goto_1

    .line 1244
    :cond_6
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNutrientFat:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->getNutrientValueView()Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;

    move-result-object v4

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;->setImeOptions(I)V

    .line 1245
    const/4 v3, 0x1

    goto :goto_2

    .line 1255
    :cond_7
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mCaloriePerServingEditText:Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;->setImeOptions(I)V

    goto :goto_3
.end method

.method private setResultCanceledAndFinish()V
    .locals 2

    .prologue
    .line 870
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->setResult(I)V

    .line 872
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    sget-object v1, Lcom/sec/android/app/shealth/food/app/UserActionLog;->CANCEL_ADD_MY_FOOD:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;->logUserAction(Lcom/sec/android/app/shealth/food/app/UserActionLog;)V

    .line 874
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->finish()V

    .line 876
    return-void
.end method

.method private setSaveMyToFoodCheckboxAreaVisibilityState(Z)V
    .locals 4
    .param p1, "saveCheckBoxIsVisible"    # Z

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 559
    if-nez p1, :cond_0

    .line 562
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mSaveInMyFoodCheckBox:Landroid/widget/CheckBox;

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 566
    :cond_0
    if-eqz p1, :cond_1

    .line 569
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mSaveInMyFoodLayout:Landroid/view/View;

    new-instance v3, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$6;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$6;-><init>(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 584
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mSaveInMyFoodLayout:Landroid/view/View;

    if-eqz p1, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 586
    const v0, 0x7f080395

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    if-eqz p1, :cond_3

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 590
    const v0, 0x7f080391

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz p1, :cond_4

    :goto_2
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 592
    return-void

    :cond_2
    move v0, v2

    .line 584
    goto :goto_0

    :cond_3
    move v0, v2

    .line 586
    goto :goto_1

    :cond_4
    move v1, v2

    .line 590
    goto :goto_2
.end method

.method private setVisibilityForNutrientFields(IFFFZ)V
    .locals 6
    .param p1, "visibility"    # I
    .param p2, "protienValue"    # F
    .param p3, "carboValue"    # F
    .param p4, "fatValue"    # F
    .param p5, "onClickViewAnotherField"    # Z

    .prologue
    const v5, 0x7f0900bf

    const v4, 0x7f08038f

    const/4 v3, 0x0

    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 931
    if-nez p1, :cond_3

    .line 934
    cmpl-float v0, p2, v3

    if-lez v0, :cond_2

    cmpl-float v0, p3, v3

    if-lez v0, :cond_2

    cmpl-float v0, p4, v3

    if-lez v0, :cond_2

    .line 936
    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 944
    :cond_0
    :goto_0
    const v0, 0x7f080391

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 946
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNutrientsContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 948
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNutrientFat:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->setVisibility(I)V

    .line 950
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNutrientProtein:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->setVisibility(I)V

    .line 952
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNutrientCarbo:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->setVisibility(I)V

    .line 955
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNutrientProtein:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->getNutrientTitleView()Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f090966

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 957
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNutrientProtein:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->getNutrientUnitView()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(I)V

    .line 959
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNutrientCarbo:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->getNutrientTitleView()Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f090962

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 961
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNutrientCarbo:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->getNutrientUnitView()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(I)V

    .line 963
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNutrientFat:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->getNutrientTitleView()Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f0909cb

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 965
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNutrientFat:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->getNutrientUnitView()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(I)V

    .line 967
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->getScrollView()Landroid/widget/ScrollView;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$10;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$10;-><init>(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->post(Ljava/lang/Runnable;)Z

    .line 988
    :cond_1
    :goto_1
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->setImeOptions()V

    .line 990
    return-void

    .line 937
    :cond_2
    if-eqz p5, :cond_0

    .line 939
    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 980
    :cond_3
    if-ne p1, v2, :cond_1

    .line 982
    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 984
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNutrientsContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_1
.end method

.method private showInvalidSaveToast(I)V
    .locals 3
    .param p1, "messageId"    # I

    .prologue
    .line 1292
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mInvalidSaveToast:Landroid/widget/Toast;

    if-nez v0, :cond_0

    .line 1293
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, ""

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mInvalidSaveToast:Landroid/widget/Toast;

    .line 1296
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mInvalidSaveToast:Landroid/widget/Toast;

    invoke-virtual {v0, p1}, Landroid/widget/Toast;->setText(I)V

    .line 1297
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mInvalidSaveToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1298
    return-void
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 666
    new-instance v3, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$8;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$8;-><init>(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;)V

    .line 696
    .local v3, "actionSaveListener":Landroid/view/View$OnClickListener;
    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v4, 0x7f09004f

    invoke-direct {v1, v6, v4, v3, v7}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IILandroid/view/View$OnClickListener;Z)V

    .line 700
    .local v1, "actionBarButtonSave":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;
    new-instance v2, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$9;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$9;-><init>(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;)V

    .line 713
    .local v2, "actionCancelListener":Landroid/view/View$OnClickListener;
    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v4, 0x7f090048

    invoke-direct {v0, v6, v4, v2, v7}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IILandroid/view/View$OnClickListener;Z)V

    .line 716
    .local v0, "actionBarButtonCancel":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v4

    invoke-virtual {v4, v6}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setHomeButtonVisible(Z)V

    .line 718
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    const/4 v5, 0x2

    new-array v5, v5, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    aput-object v0, v5, v6

    aput-object v1, v5, v7

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 720
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    .line 723
    return-void
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 1368
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    const/16 v3, 0x42

    if-ne v2, v3, :cond_1

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_1

    .line 1370
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    .line 1372
    .local v0, "currentView":Landroid/view/View;
    if-eqz v0, :cond_1

    instance-of v2, v0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    if-eqz v2, :cond_1

    .line 1374
    const/16 v2, 0x82

    invoke-virtual {v0, v2}, Landroid/view/View;->focusSearch(I)Landroid/view/View;

    move-result-object v1

    .line 1375
    .local v1, "nextView":Landroid/view/View;
    if-eqz v1, :cond_0

    .line 1377
    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    .line 1380
    :cond_0
    const/4 v2, 0x1

    .line 1384
    .end local v0    # "currentView":Landroid/view/View;
    .end local v1    # "nextView":Landroid/view/View;
    :goto_0
    return v2

    :cond_1
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v2

    goto :goto_0
.end method

.method public getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 1151
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mDialogControllerMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;

    return-object v0
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 848
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->isInvalidState()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 865
    :goto_0
    return-void

    .line 852
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNameOfFoodEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    if-lez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mCaloriePerServingEditText:Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;->length()I

    move-result v0

    if-lez v0, :cond_2

    .line 854
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->isFoodDataChanged()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 856
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->onSaveButtonClick()V

    goto :goto_0

    .line 859
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->setResultCanceledAndFinish()V

    goto :goto_0

    .line 863
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->setResultCanceledAndFinish()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 204
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 206
    const v1, 0x7f0300bf

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->setContentView(I)V

    .line 208
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 210
    .local v0, "window":Landroid/view/Window;
    if-eqz v0, :cond_0

    .line 213
    const/16 v1, 0x400

    const/16 v2, 0x500

    invoke-virtual {v0, v1, v2}, Landroid/view/Window;->setFlags(II)V

    .line 217
    :cond_0
    new-instance v1, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mFoodInfoDao:Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;

    .line 219
    new-instance v1, Lcom/sec/android/app/shealth/food/fooddao/ExtraFoodInfoDaoImplDb;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/food/fooddao/ExtraFoodInfoDaoImplDb;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mExtraFoodInfoDao:Lcom/sec/android/app/shealth/food/fooddao/ExtraFoodInfoDao;

    .line 221
    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 226
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onPostCreate(Landroid/os/Bundle;)V

    .line 228
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->findViews()V

    .line 230
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->initTextWatcher()V

    .line 232
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->initViews(Landroid/os/Bundle;)V

    .line 234
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->initListeners()V

    .line 238
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLoggerHolder;

    invoke-interface {v0}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLoggerHolder;->getUserActionLogger()Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    .line 240
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->clearScreensViewsList()V

    .line 242
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getButtonContainer()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->addToScreenViewsList(Landroid/view/View;)V

    .line 244
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mContent:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->addToScreenViewsList(Landroid/view/View;)V

    .line 246
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->setImeOptions()V

    .line 248
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->addEmptyListener()V

    .line 251
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 1390
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onResume()V

    .line 1392
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    .line 1393
    .local v0, "currentView":Landroid/view/View;
    invoke-static {p0, v0}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->showSoftInputKeypad(Landroid/content/Context;Landroid/view/View;)V

    .line 1394
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 1108
    const-string/jumbo v0, "sName"

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNameOfFoodEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1110
    const-string/jumbo v0, "sCalories"

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mCaloriePerServingEditText:Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1112
    const-string/jumbo v0, "sProteins"

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNutrientProtein:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->getNutrientValueView()Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mSystemNumberSeparator:Ljava/lang/String;

    const-string v3, "."

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1114
    const-string/jumbo v0, "sCarbos"

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNutrientCarbo:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->getNutrientValueView()Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mSystemNumberSeparator:Ljava/lang/String;

    const-string v3, "."

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1116
    const-string/jumbo v0, "sFat"

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNutrientFat:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->getNutrientValueView()Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mSystemNumberSeparator:Ljava/lang/String;

    const-string v3, "."

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1118
    const-string/jumbo v0, "sIsMyFood"

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mSaveInMyFoodCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1120
    const-string/jumbo v0, "sIsShouldAlwaysBeSavedToMyFood"

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mIsShouldAlwaysBeSavedToMyFood:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1122
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1124
    return-void
.end method

.method public processEmptyField()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1276
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/SoftInputUtils;->isHardKeyBoardPresent(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1277
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mCaloriePerServingEditText:Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/SoftInputUtils;->showSoftInput(Landroid/content/Context;Landroid/view/View;)V

    .line 1280
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mCaloriePerServingEditText:Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;->setText(Ljava/lang/CharSequence;)V

    .line 1281
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mCaloriePerServingEditText:Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;->requestFocus()Z

    .line 1282
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mCaloriePerServingEditText:Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;->selectAll()V

    .line 1283
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mOutOfRangeToast:Landroid/widget/Toast;

    if-nez v0, :cond_1

    .line 1285
    const v0, 0x7f09092d

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    const/4 v2, 0x1

    const v3, 0x1869f

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v4}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mOutOfRangeToast:Landroid/widget/Toast;

    .line 1287
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mOutOfRangeToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1290
    return-void
.end method

.method public setDoneButtonEnabled(Z)V
    .locals 2
    .param p1, "isEnabled"    # Z

    .prologue
    const/4 v1, 0x1

    .line 1130
    if-eqz p1, :cond_0

    .line 1133
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->enableActionBarButton(I)V

    .line 1143
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->refreshFocusables()V

    .line 1145
    return-void

    .line 1139
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->disableActionBarButton(I)V

    goto :goto_0
.end method
