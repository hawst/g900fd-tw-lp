.class Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$UpdateListTask;
.super Landroid/os/AsyncTask;
.source "CurrentGoalFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "UpdateListTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/cigna/coach/apiobjects/Scores;",
        ">;"
    }
.end annotation


# instance fields
.field private mNewCoachMessages:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mNewGoalDataList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;",
            ">;"
        }
    .end annotation
.end field

.field private mNewMissionDataList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;",
            ">;>;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;)V
    .locals 1

    .prologue
    .line 227
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$UpdateListTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 230
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$UpdateListTask;->mNewGoalDataList:Ljava/util/ArrayList;

    .line 231
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$UpdateListTask;->mNewMissionDataList:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Lcom/cigna/coach/apiobjects/Scores;
    .locals 4
    .param p1, "arg0"    # [Ljava/lang/Void;

    .prologue
    .line 235
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$UpdateListTask;->isCancelled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 236
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$UpdateListTask;->mNewGoalDataList:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$UpdateListTask;->mNewMissionDataList:Ljava/util/ArrayList;

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getCurrentGoalListData(Ljava/util/ArrayList;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$UpdateListTask;->mNewCoachMessages:Ljava/util/ArrayList;

    .line 238
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v1

    sget-object v2, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->ALL:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getScores(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;)Lcom/cigna/coach/apiobjects/Scores;

    move-result-object v0

    .line 241
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 227
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$UpdateListTask;->doInBackground([Ljava/lang/Void;)Lcom/cigna/coach/apiobjects/Scores;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/cigna/coach/apiobjects/Scores;)V
    .locals 7
    .param p1, "scores"    # Lcom/cigna/coach/apiobjects/Scores;

    .prologue
    .line 246
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 247
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$UpdateListTask;->isCancelled()Z

    move-result v2

    if-nez v2, :cond_5

    .line 248
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$UpdateListTask;->mNewGoalDataList:Ljava/util/ArrayList;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$UpdateListTask;->mNewMissionDataList:Ljava/util/ArrayList;

    if-eqz v2, :cond_5

    .line 249
    const/4 v0, -0x1

    .line 251
    .local v0, "expandGoalId":I
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$UpdateListTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mGoalDataList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->access$100(Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 252
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$UpdateListTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mGoalDataList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->access$100(Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 254
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$UpdateListTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mMissionDataList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->access$200(Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 255
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$UpdateListTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mMissionDataList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->access$200(Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 257
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$UpdateListTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mCoachMessages:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->access$300(Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 258
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$UpdateListTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mCoachMessages:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->access$300(Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 260
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$UpdateListTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mGoalDataList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->access$100(Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;)Ljava/util/ArrayList;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$UpdateListTask;->mNewGoalDataList:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 261
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$UpdateListTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mMissionDataList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->access$200(Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;)Ljava/util/ArrayList;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$UpdateListTask;->mNewMissionDataList:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 263
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$UpdateListTask;->mNewCoachMessages:Ljava/util/ArrayList;

    if-eqz v2, :cond_3

    .line 264
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$UpdateListTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mCoachMessages:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->access$300(Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;)Ljava/util/ArrayList;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$UpdateListTask;->mNewCoachMessages:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 266
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$UpdateListTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mCurrentExpandedGroupPosition:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->access$400(Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_4

    .line 267
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$UpdateListTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mCurrentExpandedGroupPosition:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->access$400(Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;)I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$UpdateListTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mGoalDataList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->access$100(Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-gt v2, v3, :cond_4

    .line 268
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$UpdateListTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mGoalDataList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->access$100(Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;)Ljava/util/ArrayList;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$UpdateListTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mCurrentExpandedGroupPosition:I
    invoke-static {v3}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->access$400(Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;

    .line 269
    .local v1, "goalData":Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->getId()I

    move-result v0

    .line 273
    .end local v1    # "goalData":Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$UpdateListTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;

    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$UpdateListTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;

    invoke-virtual {v3, v0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->getCurrentGoalProgressMsg(I)Ljava/lang/String;

    move-result-object v3

    # setter for: Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mCurrentGoalProgressMsg:Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->access$502(Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 274
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$UpdateListTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mCignaLifestyleScoreView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;
    invoke-static {v2}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->access$600(Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;)Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;

    move-result-object v3

    invoke-virtual {p1}, Lcom/cigna/coach/apiobjects/Scores;->getOverallScore()I

    move-result v4

    const/4 v5, 0x0

    invoke-virtual {p1}, Lcom/cigna/coach/apiobjects/Scores;->getOutOfMessage()Ljava/lang/String;

    move-result-object v6

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$UpdateListTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mCurrentGoalProgressMsg:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->access$500(Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_6

    const-string v2, ""

    :goto_0
    invoke-virtual {v3, v4, v5, v6, v2}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->setCurrentScoreInfo(ILcom/cigna/coach/interfaces/ILifeStyle$CategoryType;Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$UpdateListTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;

    # invokes: Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->setAdapter()V
    invoke-static {v2}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->access$700(Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;)V

    .line 278
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$UpdateListTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;

    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$UpdateListTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mCurrentExpandedGroupPosition:I
    invoke-static {v3}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->access$400(Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;)I

    move-result v3

    const/4 v4, 0x0

    # invokes: Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->expandGroup(IZ)V
    invoke-static {v2, v3, v4}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->access$800(Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;IZ)V

    .line 281
    .end local v0    # "expandGoalId":I
    :cond_5
    return-void

    .line 274
    .restart local v0    # "expandGoalId":I
    :cond_6
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$UpdateListTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mCurrentGoalProgressMsg:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->access$500(Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 227
    check-cast p1, Lcom/cigna/coach/apiobjects/Scores;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$UpdateListTask;->onPostExecute(Lcom/cigna/coach/apiobjects/Scores;)V

    return-void
.end method
