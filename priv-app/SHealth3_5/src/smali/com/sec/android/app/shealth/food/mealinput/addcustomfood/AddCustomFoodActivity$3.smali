.class Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$3;
.super Lcom/sec/android/app/shealth/food/utils/MinMaxIntegerInputFilter;
.source "AddCustomFoodActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->initViews(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;FFI)V
    .locals 0
    .param p2, "x0"    # F
    .param p3, "x1"    # F
    .param p4, "x2"    # I

    .prologue
    .line 358
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$3;->this$0:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;

    invoke-direct {p0, p2, p3, p4}, Lcom/sec/android/app/shealth/food/utils/MinMaxIntegerInputFilter;-><init>(FFI)V

    return-void
.end method


# virtual methods
.method public showAlertToast()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 364
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$3;->this$0:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mIncorrectRangeToast:Landroid/widget/Toast;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->access$400(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;)Landroid/widget/Toast;

    move-result-object v1

    if-nez v1, :cond_0

    .line 367
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$3;->this$0:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;

    const v2, 0x7f09098d

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    const v4, 0x1869f

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 371
    .local v0, "text":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$3;->this$0:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$3;->this$0:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;

    invoke-static {v2, v0, v6}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v2

    # setter for: Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mIncorrectRangeToast:Landroid/widget/Toast;
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->access$402(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;Landroid/widget/Toast;)Landroid/widget/Toast;

    .line 375
    .end local v0    # "text":Ljava/lang/String;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$3;->this$0:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mIncorrectRangeToast:Landroid/widget/Toast;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->access$400(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowVisibility()I

    move-result v1

    if-eqz v1, :cond_1

    .line 378
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$3;->this$0:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mIncorrectRangeToast:Landroid/widget/Toast;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->access$400(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 382
    :cond_1
    return-void
.end method

.method public showInvalidInputToast()V
    .locals 4

    .prologue
    .line 386
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$3;->this$0:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mInvalidInputToast:Landroid/widget/Toast;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->access$500(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;)Landroid/widget/Toast;

    move-result-object v0

    if-nez v0, :cond_0

    .line 388
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$3;->this$0:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$3;->this$0:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$3;->this$0:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;

    const v3, 0x7f09092e

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v1

    # setter for: Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mInvalidInputToast:Landroid/widget/Toast;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->access$502(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;Landroid/widget/Toast;)Landroid/widget/Toast;

    .line 389
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$3;->this$0:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mInvalidInputToast:Landroid/widget/Toast;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->access$500(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 397
    :goto_0
    return-void

    .line 394
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$3;->this$0:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mInvalidInputToast:Landroid/widget/Toast;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->access$500(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
