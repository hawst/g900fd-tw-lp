.class Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$1;
.super Ljava/lang/Object;
.source "ConnectedDeviceFinder.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;)V
    .locals 0

    .prologue
    .line 136
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$1;->this$0:Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(I)V
    .locals 2
    .param p1, "error"    # I

    .prologue
    .line 140
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$1;->this$0:Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;

    # getter for: Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;->mDeviceConnectionCheckListener:Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceConnectionCheckListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;->access$000(Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;)Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceConnectionCheckListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$1;->this$0:Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;

    # getter for: Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;->mDeviceConnectionCheckListener:Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceConnectionCheckListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;->access$000(Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;)Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceConnectionCheckListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$1;->this$0:Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;

    # invokes: Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;->isDeviceConnected()Z
    invoke-static {v1}, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;->access$100(Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;)Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceConnectionCheckListener;->onDeviceConnectionChecked(Z)V

    .line 143
    :cond_0
    return-void
.end method

.method public onServiceDisconnected(I)V
    .locals 2
    .param p1, "error"    # I

    .prologue
    .line 147
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$1;->this$0:Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;

    # getter for: Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;->mDeviceConnectionCheckListener:Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceConnectionCheckListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;->access$000(Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;)Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceConnectionCheckListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 148
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$1;->this$0:Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;

    # getter for: Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;->mDeviceConnectionCheckListener:Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceConnectionCheckListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;->access$000(Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;)Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceConnectionCheckListener;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceConnectionCheckListener;->onDeviceConnectionChecked(Z)V

    .line 150
    :cond_0
    return-void
.end method
