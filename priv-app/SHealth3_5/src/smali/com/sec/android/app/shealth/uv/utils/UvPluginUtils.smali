.class public Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;
.super Ljava/lang/Object;
.source "UvPluginUtils.java"


# static fields
.field public static final DEFAULTOFFSET:F = 0.8f

.field public static SkinUV:[[I

.field public static final ivt_error:[B

.field public static final ivt_finish:[B

.field public static final ivt_measuring:[B

.field private static uvPluginUtils:Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x5

    .line 28
    new-array v0, v3, [[I

    const/4 v1, 0x0

    new-array v2, v3, [I

    fill-array-data v2, :array_0

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-array v2, v3, [I

    fill-array-data v2, :array_1

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-array v2, v3, [I

    fill-array-data v2, :array_2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    new-array v2, v3, [I

    fill-array-data v2, :array_3

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-array v2, v3, [I

    fill-array-data v2, :array_4

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;->SkinUV:[[I

    .line 40
    const/16 v0, 0x28

    new-array v0, v0, [B

    fill-array-data v0, :array_5

    sput-object v0, Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;->ivt_finish:[B

    .line 51
    const/16 v0, 0x4c

    new-array v0, v0, [B

    fill-array-data v0, :array_6

    sput-object v0, Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;->ivt_measuring:[B

    .line 67
    const/16 v0, 0x44

    new-array v0, v0, [B

    fill-array-data v0, :array_7

    sput-object v0, Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;->ivt_error:[B

    return-void

    .line 28
    nop

    :array_0
    .array-data 4
        0x38
        0x3b
        0x46
        0x5b
        0xa8
    .end array-data

    :array_1
    .array-data 4
        0x18
        0x19
        0x1e
        0x27
        0x48
    .end array-data

    :array_2
    .array-data 4
        0x10
        0x11
        0x14
        0x1a
        0x30
    .end array-data

    :array_3
    .array-data 4
        0xb
        0xc
        0xe
        0x12
        0x22
    .end array-data

    :array_4
    .array-data 4
        0xa
        0xa
        0xc
        0x10
        0x1d
    .end array-data

    .line 40
    :array_5
    .array-data 1
        0x1t
        0x0t
        0x2t
        0x0t
        0x20t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0xct
        0x0t
        -0xft
        -0x20t
        0x1t
        -0x1et
        0x0t
        0x8t
        -0x30t
        0x0t
        0x1et
        -0x2ft
        0x4ct
        -0x1t
        0x30t
        0x0t
        0x0t
        0x3t
        0x0t
        0x0t
        0x0t
        0x0t
        0x20t
        0x1t
        0x0t
        0x0t
        0x4ct
        0x13t
        0x4dt
        -0x20t
    .end array-data

    .line 51
    :array_6
    .array-data 1
        0x1t
        0x0t
        0x2t
        0x0t
        0x44t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x2ft
        0x0t
        -0xft
        -0x20t
        0x1t
        -0x1et
        0x0t
        0x14t
        -0xft
        -0x20t
        0x1t
        -0x1et
        0x1t
        -0x70t
        -0x2ft
        0x39t
        -0xft
        -0x20t
        0x1t
        -0x1et
        0x3t
        0xct
        -0x2ft
        0x30t
        -0xft
        -0x20t
        0x1t
        -0x1et
        0x4t
        -0x78t
        -0x2ft
        0x26t
        -0xft
        -0x20t
        0x1t
        -0x1et
        0x6t
        0x4t
        -0x2ft
        0x1ct
        -0xft
        -0x20t
        0x1t
        -0x1et
        0x7t
        -0x80t
        -0x2ft
        0xft
        -0x1t
        0x30t
        0x0t
        0x0t
        0xet
        0x0t
        0x0t
        0x0t
        0x0t
        0x20t
        0x3t
        0x0t
        0x0t
        0x40t
        0x23t
        0x46t
        -0x40t
        0x0t
    .end array-data

    .line 67
    :array_7
    .array-data 1
        0x1t
        0x0t
        0x3t
        0x0t
        0x3ct
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1dt
        0x0t
        0x25t
        0x0t
        -0xft
        -0x20t
        0x1t
        -0x1et
        0x0t
        0x1t
        -0x30t
        0x0t
        0xat
        -0x2ft
        0x4ct
        -0xft
        -0x20t
        0x2t
        -0x1et
        0x0t
        0xct
        -0x30t
        0x0t
        0x46t
        -0xft
        -0x20t
        0x1t
        -0x1et
        0x0t
        0x4ct
        -0x2ft
        0x66t
        -0x1t
        0x20t
        0x1t
        0x0t
        0x0t
        0x53t
        -0x7dt
        0x57t
        0x41t
        0x30t
        0x3bt
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x20t
        0x7t
        0x0t
        0x0t
        0x1t
        0x1t
        -0x6at
        -0x6et
        0x0t
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;->uvPluginUtils:Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;

    if-nez v0, :cond_0

    .line 23
    new-instance v0, Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;->uvPluginUtils:Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;

    .line 25
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;->uvPluginUtils:Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;

    return-object v0
.end method


# virtual methods
.method public calculateSafetyTime(J)Ljava/lang/String;
    .locals 11
    .param p1, "safetyTime"    # J

    .prologue
    const v4, 0x7f090db5

    const v3, 0x7f090db2

    const-wide/16 v7, 0x3c

    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 135
    const-string v2, ""

    .line 136
    .local v2, "safetyTimeStr":Ljava/lang/String;
    cmp-long v5, p1, v7

    if-lez v5, :cond_3

    .line 138
    const-wide/16 v5, 0x78

    cmp-long v5, p1, v5

    if-gtz v5, :cond_2

    .line 139
    div-long v5, p1, v7

    long-to-int v0, v5

    .line 140
    .local v0, "hour":I
    rem-long v5, p1, v7

    long-to-int v1, v5

    .line 141
    .local v1, "min":I
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    if-ne v0, v9, :cond_0

    const v3, 0x7f090db6

    :cond_0
    new-array v7, v9, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v10

    invoke-virtual {v6, v3, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    if-ne v1, v9, :cond_1

    const v3, 0x7f090db7

    :goto_0
    new-array v4, v9, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v4, v10

    invoke-virtual {v6, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 155
    .end local v0    # "hour":I
    .end local v1    # "min":I
    :goto_1
    return-object v2

    .restart local v0    # "hour":I
    .restart local v1    # "min":I
    :cond_1
    move v3, v4

    .line 141
    goto :goto_0

    .line 146
    .end local v0    # "hour":I
    .end local v1    # "min":I
    :cond_2
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    new-array v5, v9, [Ljava/lang/Object;

    const/4 v6, 0x2

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v10

    invoke-virtual {v4, v3, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 151
    :cond_3
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const-wide/16 v5, 0x1

    cmp-long v5, p1, v5

    if-nez v5, :cond_4

    const v4, 0x7f090db7

    :cond_4
    new-array v5, v9, [Ljava/lang/Object;

    long-to-int v6, p1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v10

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method

.method public calculateSafetyTimeForTalkback(J)Ljava/lang/String;
    .locals 11
    .param p1, "safetyTime"    # J

    .prologue
    const v4, 0x7f090dbe

    const v3, 0x7f090dbb

    const-wide/16 v7, 0x3c

    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 110
    const-string v2, ""

    .line 111
    .local v2, "safetyTimeStr":Ljava/lang/String;
    cmp-long v5, p1, v7

    if-lez v5, :cond_3

    .line 113
    const-wide/16 v5, 0x78

    cmp-long v5, p1, v5

    if-gtz v5, :cond_2

    .line 114
    div-long v5, p1, v7

    long-to-int v0, v5

    .line 115
    .local v0, "hour":I
    rem-long v5, p1, v7

    long-to-int v1, v5

    .line 116
    .local v1, "min":I
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    if-ne v0, v9, :cond_0

    const v3, 0x7f090dbd

    :cond_0
    new-array v7, v9, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v10

    invoke-virtual {v6, v3, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    if-ne v1, v9, :cond_1

    const v3, 0x7f090dbf

    :goto_0
    new-array v4, v9, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v4, v10

    invoke-virtual {v6, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 130
    .end local v0    # "hour":I
    .end local v1    # "min":I
    :goto_1
    return-object v2

    .restart local v0    # "hour":I
    .restart local v1    # "min":I
    :cond_1
    move v3, v4

    .line 116
    goto :goto_0

    .line 121
    .end local v0    # "hour":I
    .end local v1    # "min":I
    :cond_2
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    new-array v5, v9, [Ljava/lang/Object;

    const/4 v6, 0x2

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v10

    invoke-virtual {v4, v3, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 126
    :cond_3
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const-wide/16 v5, 0x1

    cmp-long v5, p1, v5

    if-nez v5, :cond_4

    const v4, 0x7f090dbf

    :cond_4
    new-array v5, v9, [Ljava/lang/Object;

    long-to-int v6, p1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v10

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method

.method public convertDptoPx(ILandroid/content/Context;)I
    .locals 4
    .param p1, "dp"    # I
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 159
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 160
    .local v1, "res":Landroid/content/res/Resources;
    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 161
    .local v0, "metrics":Landroid/util/DisplayMetrics;
    int-to-float v2, p1

    iget v3, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    return v2
.end method

.method public getLatestDataTimestamp(Ljava/lang/String;J)J
    .locals 9
    .param p1, "format"    # Ljava/lang/String;
    .param p2, "dateTime"    # J

    .prologue
    .line 88
    const-wide/16 v7, 0x0

    .line 89
    .local v7, "obtainedTime":J
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "select sample_time from ultraviolet_rays where strftime(\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\",("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "sample_time"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/1000),\'unixepoch\', \'localtime\') = strftime(\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\",("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/1000),\'unixepoch\', \'localtime\') order by "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "sample_time"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " desc limit 1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 92
    .local v3, "query":Ljava/lang/String;
    const/4 v6, 0x0

    .line 95
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 96
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 97
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 98
    const-string/jumbo v0, "sample_time"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v7

    .line 103
    :cond_0
    if-eqz v6, :cond_1

    .line 104
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 106
    :cond_1
    return-wide v7

    .line 103
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 104
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method public getUvIndexString(ZD)I
    .locals 4
    .param p1, "isChinaModel"    # Z
    .param p2, "uvIndex"    # D

    .prologue
    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    .line 166
    if-eqz p1, :cond_0

    const v0, 0x7f091158

    .line 168
    .local v0, "id":I
    :goto_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->isChinaModel()Z

    move-result v1

    if-nez v1, :cond_5

    .line 170
    cmpg-double v1, p2, v2

    if-gtz v1, :cond_1

    .line 171
    const v0, 0x7f090d94

    .line 194
    :goto_1
    return v0

    .line 166
    .end local v0    # "id":I
    :cond_0
    const v0, 0x7f090d94

    goto :goto_0

    .line 172
    .restart local v0    # "id":I
    :cond_1
    const-wide/high16 v1, 0x4014000000000000L    # 5.0

    cmpg-double v1, p2, v1

    if-gtz v1, :cond_2

    .line 173
    const v0, 0x7f090d95

    goto :goto_1

    .line 174
    :cond_2
    const-wide/high16 v1, 0x401c000000000000L    # 7.0

    cmpg-double v1, p2, v1

    if-gtz v1, :cond_3

    .line 175
    const v0, 0x7f090d96

    goto :goto_1

    .line 176
    :cond_3
    const-wide/high16 v1, 0x4024000000000000L    # 10.0

    cmpg-double v1, p2, v1

    if-gtz v1, :cond_4

    .line 177
    const v0, 0x7f090d97

    goto :goto_1

    .line 179
    :cond_4
    const v0, 0x7f090d98

    goto :goto_1

    .line 182
    :cond_5
    cmpg-double v1, p2, v2

    if-gtz v1, :cond_6

    .line 183
    const v0, 0x7f091158

    goto :goto_1

    .line 184
    :cond_6
    const-wide/high16 v1, 0x4010000000000000L    # 4.0

    cmpg-double v1, p2, v1

    if-gtz v1, :cond_7

    .line 185
    const v0, 0x7f091159

    goto :goto_1

    .line 186
    :cond_7
    const-wide/high16 v1, 0x4018000000000000L    # 6.0

    cmpg-double v1, p2, v1

    if-gtz v1, :cond_8

    .line 187
    const v0, 0x7f09115a

    goto :goto_1

    .line 188
    :cond_8
    const-wide/high16 v1, 0x4022000000000000L    # 9.0

    cmpg-double v1, p2, v1

    if-gtz v1, :cond_9

    .line 189
    const v0, 0x7f09115b

    goto :goto_1

    .line 191
    :cond_9
    const v0, 0x7f09115c

    goto :goto_1
.end method
