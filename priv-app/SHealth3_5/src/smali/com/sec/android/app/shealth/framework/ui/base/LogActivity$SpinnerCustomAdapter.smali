.class Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$SpinnerCustomAdapter;
.super Landroid/widget/ArrayAdapter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SpinnerCustomAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;Landroid/content/Context;IILjava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "II",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x2

    new-array v2, v0, [J

    const/4 v0, 0x1

    const-wide/16 v3, 0x2

    aput-wide v3, v2, v0

    const/4 v0, 0x0

    array-length v1, v2

    add-int/lit8 v1, v1, -0x1

    aget-wide v3, v2, v1

    long-to-int v1, v3

    if-gtz v1, :cond_0

    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, v0}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    const/4 v3, 0x0

    int-to-long v0, p3

    const/16 v4, 0x20

    shl-long/2addr v0, v4

    const/16 v4, 0x20

    ushr-long v4, v0, v4

    aget-wide v0, v2, v3

    const-wide/16 v6, 0x0

    cmp-long v6, v0, v6

    if-eqz v6, :cond_1

    const-wide v6, 0x1ce8f39ff4dcad48L

    xor-long/2addr v0, v6

    :cond_1
    const/16 v6, 0x20

    ushr-long/2addr v0, v6

    const/16 v6, 0x20

    shl-long/2addr v0, v6

    xor-long/2addr v0, v4

    const-wide v4, 0x1ce8f39ff4dcad48L

    xor-long/2addr v0, v4

    aput-wide v0, v2, v3

    const/4 v0, 0x1

    array-length v1, v2

    add-int/lit8 v1, v1, -0x1

    aget-wide v3, v2, v1

    long-to-int v1, v3

    if-lt v0, v1, :cond_2

    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, v0}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    const/4 v3, 0x0

    int-to-long v0, p4

    const/16 v4, 0x20

    shl-long v4, v0, v4

    aget-wide v0, v2, v3

    const-wide/16 v6, 0x0

    cmp-long v6, v0, v6

    if-eqz v6, :cond_3

    const-wide v6, 0x1ce8f39ff4dcad48L

    xor-long/2addr v0, v6

    :cond_3
    const/16 v6, 0x20

    shl-long/2addr v0, v6

    const/16 v6, 0x20

    ushr-long/2addr v0, v6

    xor-long/2addr v0, v4

    const-wide v4, 0x1ce8f39ff4dcad48L

    xor-long/2addr v0, v4

    aput-wide v0, v2, v3

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$SpinnerCustomAdapter;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_6

    new-instance v3, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v0, 0x1

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/16 v2, -0x3383

    aput v2, v1, v0

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/16 v4, -0x33b3

    aput v4, v0, v2

    const/4 v2, 0x0

    :goto_0
    array-length v4, v0

    if-lt v2, v4, :cond_4

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_1
    array-length v4, v0

    if-lt v2, v4, :cond_5

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_4
    aget v4, v0, v2

    aget v5, v1, v2

    xor-int/2addr v4, v5

    aput v4, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_5
    aget v4, v1, v2

    int-to-char v4, v4

    aput-char v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_6
    const/4 v0, 0x0

    aget-wide v0, v2, v0

    const-wide/16 v3, 0x0

    cmp-long v3, v0, v3

    if-eqz v3, :cond_7

    const-wide v3, 0x1ce8f39ff4dcad48L

    xor-long/2addr v0, v3

    :cond_7
    const/16 v3, 0x20

    shl-long/2addr v0, v3

    const/16 v3, 0x20

    shr-long/2addr v0, v3

    long-to-int v3, v0

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    const/4 v1, 0x1

    if-gt v0, v1, :cond_a

    new-instance v3, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v0, 0x1

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/16 v2, 0x5315

    aput v2, v1, v0

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/16 v4, 0x5324

    aput v4, v0, v2

    const/4 v2, 0x0

    :goto_2
    array-length v4, v0

    if-lt v2, v4, :cond_8

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_3
    array-length v4, v0

    if-lt v2, v4, :cond_9

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_8
    aget v4, v0, v2

    aget v5, v1, v2

    xor-int/2addr v4, v5

    aput v4, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_9
    aget v4, v1, v2

    int-to-char v4, v4

    aput-char v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_a
    const/4 v0, 0x0

    aget-wide v0, v2, v0

    const-wide/16 v4, 0x0

    cmp-long v2, v0, v4

    if-eqz v2, :cond_b

    const-wide v4, 0x1ce8f39ff4dcad48L

    xor-long/2addr v0, v4

    :cond_b
    const/16 v2, 0x20

    shr-long/2addr v0, v2

    long-to-int v0, v0

    invoke-direct {p0, p2, v3, v0, p5}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;IILjava/util/List;)V

    return-void
.end method


# virtual methods
.method public getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 17

    const/4 v2, 0x2

    new-array v4, v2, [J

    const/4 v2, 0x1

    const-wide/16 v5, 0x1

    aput-wide v5, v4, v2

    const/4 v2, 0x0

    array-length v3, v4

    add-int/lit8 v3, v3, -0x1

    aget-wide v5, v4, v3

    long-to-int v3, v5

    if-gtz v3, :cond_0

    new-instance v3, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v4, Ljava/lang/Integer;

    invoke-direct {v4, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v4}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_0
    const/4 v5, 0x0

    move/from16 v0, p1

    int-to-long v2, v0

    const/16 v6, 0x20

    shl-long/2addr v2, v6

    const/16 v6, 0x20

    ushr-long v6, v2, v6

    aget-wide v2, v4, v5

    const-wide/16 v8, 0x0

    cmp-long v8, v2, v8

    if-eqz v8, :cond_1

    const-wide v8, -0x3206f058235d115fL    # -4.222796283863587E67

    xor-long/2addr v2, v8

    :cond_1
    const/16 v8, 0x20

    ushr-long/2addr v2, v8

    const/16 v8, 0x20

    shl-long/2addr v2, v8

    xor-long/2addr v2, v6

    const-wide v6, -0x3206f058235d115fL    # -4.222796283863587E67

    xor-long/2addr v2, v6

    aput-wide v2, v4, v5

    array-length v2, v4

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v4, v2

    long-to-int v2, v2

    if-gtz v2, :cond_4

    new-instance v5, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v2, 0x1

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/16 v4, 0x5f62

    aput v4, v3, v2

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/16 v6, 0x5f52

    aput v6, v2, v4

    const/4 v4, 0x0

    :goto_0
    array-length v6, v2

    if-lt v4, v6, :cond_2

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_1
    array-length v6, v2

    if-lt v4, v6, :cond_3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v5, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_2
    aget v6, v2, v4

    aget v7, v3, v4

    xor-int/2addr v6, v7

    aput v6, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_3
    aget v6, v3, v4

    int-to-char v6, v6

    aput-char v6, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_4
    const/4 v2, 0x0

    aget-wide v2, v4, v2

    const-wide/16 v5, 0x0

    cmp-long v5, v2, v5

    if-eqz v5, :cond_5

    const-wide v5, -0x3206f058235d115fL    # -4.222796283863587E67

    xor-long/2addr v2, v5

    :cond_5
    const/16 v5, 0x20

    shl-long/2addr v2, v5

    const/16 v5, 0x20

    shr-long/2addr v2, v5

    long-to-int v2, v2

    const/4 v3, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-super {v0, v2, v3, v1}, Landroid/widget/ArrayAdapter;->getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    array-length v2, v4

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v4, v2

    long-to-int v2, v2

    if-gtz v2, :cond_8

    new-instance v5, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v2, 0x1

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/16 v4, -0x16

    aput v4, v3, v2

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/16 v6, -0x26

    aput v6, v2, v4

    const/4 v4, 0x0

    :goto_2
    array-length v6, v2

    if-lt v4, v6, :cond_6

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_3
    array-length v6, v2

    if-lt v4, v6, :cond_7

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v5, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_6
    aget v6, v2, v4

    aget v7, v3, v4

    xor-int/2addr v6, v7

    aput v6, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :cond_7
    aget v6, v3, v4

    int-to-char v6, v6

    aput-char v6, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    :cond_8
    const/4 v2, 0x0

    aget-wide v2, v4, v2

    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-eqz v4, :cond_9

    const-wide v4, -0x3206f058235d115fL    # -4.222796283863587E67

    xor-long/2addr v2, v4

    :cond_9
    const/16 v4, 0x20

    shl-long/2addr v2, v4

    const/16 v4, 0x20

    shr-long/2addr v2, v4

    long-to-int v2, v2

    if-nez v2, :cond_a

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$id;->text:I

    invoke-virtual {v6, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    :try_start_0
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    const/16 v3, 0xa

    new-array v4, v3, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, -0x55c9

    aput v15, v4, v14

    const/16 v14, -0x3d

    aput v14, v4, v13

    const/16 v13, -0xa

    aput v13, v4, v12

    const/16 v12, -0x34

    aput v12, v4, v11

    const/16 v11, -0x5d81

    aput v11, v4, v10

    const/16 v10, -0x71

    aput v10, v4, v9

    const/16 v9, -0x285

    aput v9, v4, v8

    const/16 v8, -0x6d

    aput v8, v4, v7

    const/16 v7, 0xd4f

    aput v7, v4, v5

    const/16 v5, 0x257e

    aput v5, v4, v3

    const/16 v3, 0xa

    new-array v3, v3, [I

    const/4 v5, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/4 v12, 0x6

    const/4 v13, 0x7

    const/16 v14, 0x8

    const/16 v15, 0x9

    const/16 v16, -0x55af

    aput v16, v3, v15

    const/16 v15, -0x56

    aput v15, v3, v14

    const/16 v14, -0x7c

    aput v14, v3, v13

    const/16 v13, -0x57

    aput v13, v3, v12

    const/16 v12, -0x5df4

    aput v12, v3, v11

    const/16 v11, -0x5e

    aput v11, v3, v10

    const/16 v10, -0x2f8

    aput v10, v3, v9

    const/4 v9, -0x3

    aput v9, v3, v8

    const/16 v8, 0xd2e

    aput v8, v3, v7

    const/16 v7, 0x250d

    aput v7, v3, v5

    const/4 v5, 0x0

    :goto_4
    array-length v7, v3

    if-lt v5, v7, :cond_b

    array-length v3, v4

    new-array v3, v3, [C

    const/4 v5, 0x0

    :goto_5
    array-length v7, v3

    if-lt v5, v7, :cond_c

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v3, v4}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    :cond_a
    :goto_6
    return-object v6

    :cond_b
    aget v7, v3, v5

    aget v8, v4, v5

    xor-int/2addr v7, v8

    aput v7, v4, v5

    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    :cond_c
    aget v7, v4, v5

    int-to-char v7, v7

    aput-char v7, v3, v5

    add-int/lit8 v5, v5, 0x1

    goto :goto_5

    :catch_0
    move-exception v2

    goto :goto_6
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 11

    const-wide v9, 0x5cb11e6a21a4bd41L    # 3.185308619652021E138

    const/4 v8, 0x0

    const/16 v7, 0x20

    const/4 v0, 0x2

    new-array v2, v0, [J

    const/4 v0, 0x1

    const-wide/16 v3, 0x1

    aput-wide v3, v2, v0

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_0

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, v8}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    int-to-long v0, p1

    shl-long/2addr v0, v7

    ushr-long v3, v0, v7

    aget-wide v0, v2, v8

    const-wide/16 v5, 0x0

    cmp-long v5, v0, v5

    if-eqz v5, :cond_1

    xor-long/2addr v0, v9

    :cond_1
    ushr-long/2addr v0, v7

    shl-long/2addr v0, v7

    xor-long/2addr v0, v3

    xor-long/2addr v0, v9

    aput-wide v0, v2, v8

    invoke-super {p0, v8, p2, p3}, Landroid/widget/ArrayAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
