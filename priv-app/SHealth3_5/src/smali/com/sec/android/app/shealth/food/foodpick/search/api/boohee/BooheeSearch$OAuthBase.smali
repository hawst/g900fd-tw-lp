.class Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch$OAuthBase;
.super Ljava/lang/Object;
.source "BooheeSearch.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "OAuthBase"
.end annotation


# static fields
.field private static final APP_KEY:Ljava/lang/String; = "app_key"

.field private static final APP_SECRET:Ljava/lang/String; = "app_secret"

.field private static final BARCODE:Ljava/lang/String; = "barcode"

.field private static final CONSUMER_KEY:Ljava/lang/String; = "consumer_key"

.field private static final FOOD_GROUP_ID:Ljava/lang/String; = "ifood_group_id"

.field private static final ID:Ljava/lang/String; = "id"

.field private static final IS_MENU:Ljava/lang/String; = "is_menu"

.field private static final KEYWORD:Ljava/lang/String; = "q"

.field private static final LIMIT:Ljava/lang/String; = "limit"

.field private static final PAGE:Ljava/lang/String; = "page"

.field private static final PER_PAGE:Ljava/lang/String; = "per_page"

.field private static final TYPE:Ljava/lang/String; = "type"

.field private static final ZH_LANG:Ljava/lang/String; = "zh"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 533
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch$1;

    .prologue
    .line 533
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch$OAuthBase;-><init>()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch$OAuthBase;Ljava/lang/String;Ljava/util/AbstractMap;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch$OAuthBase;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/util/AbstractMap;

    .prologue
    .line 533
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch$OAuthBase;->generateConsumerKey(Ljava/lang/String;Ljava/util/AbstractMap;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Ljava/util/Map;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Ljava/util/Map;

    .prologue
    .line 533
    invoke-static {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch$OAuthBase;->createQueryString(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static createQueryString(Ljava/util/Map;)Ljava/lang/String;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 573
    .local p0, "parameters":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v0, 0x0

    .line 574
    .local v0, "cnt":I
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 575
    .local v4, "sb":Ljava/lang/StringBuilder;
    invoke-interface {p0}, Ljava/util/Map;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_1

    .line 576
    const-string v5, "?"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 577
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    .line 578
    .local v2, "entrySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;>;"
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 579
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    # invokes: Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch$Base32Util;->encodeUrl(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch$Base32Util;->access$400(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 580
    add-int/lit8 v0, v0, 0x1

    invoke-interface {p0}, Ljava/util/Map;->size()I

    move-result v5

    if-eq v0, v5, :cond_0

    .line 581
    const-string v5, "&"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 585
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v2    # "entrySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;>;"
    .end local v3    # "i$":Ljava/util/Iterator;
    :cond_1
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5
.end method

.method private generateConsumerKey(Ljava/lang/String;Ljava/util/AbstractMap;)Ljava/lang/String;
    .locals 10
    .param p1, "secretkey"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/AbstractMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 557
    .local p2, "map":Ljava/util/AbstractMap;, "Ljava/util/AbstractMap<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v7, Ljava/util/TreeMap;

    invoke-direct {v7, p2}, Ljava/util/TreeMap;-><init>(Ljava/util/Map;)V

    .line 558
    .local v7, "treeMap":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 560
    .local v5, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {v7}, Ljava/util/TreeMap;->entrySet()Ljava/util/Set;

    move-result-object v2

    .line 561
    .local v2, "entrySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;>;"
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 562
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 564
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_0
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 566
    .local v4, "parameterString":Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    # invokes: Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch$Base32Util;->encodeUrl(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v8}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch$Base32Util;->access$400(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 568
    .local v6, "signatureBaseString":Ljava/lang/String;
    # invokes: Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch$Base32Util;->str2Md5(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch$Base32Util;->access$500(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 569
    .local v0, "consumerKey":Ljava/lang/String;
    return-object v0
.end method
