.class Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$3;
.super Lcom/sec/android/app/shealth/framework/ui/base/OnSingleClickListener;
.source "MealItemsFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;)V
    .locals 0

    .prologue
    .line 229
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$3;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/OnSingleClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onClickAction(Landroid/view/View;)V
    .locals 8
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/16 v6, 0xf

    const/4 v7, 0x1

    .line 232
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$3;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;

    iget-object v2, v2, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealItems:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v2, v6, :cond_1

    .line 233
    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$3;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 234
    .local v0, "intent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$3;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealActivityAction:Lcom/sec/android/app/shealth/food/mealinput/IMealActivityAction;
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->access$300(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;)Lcom/sec/android/app/shealth/food/mealinput/IMealActivityAction;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 235
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$3;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealActivityAction:Lcom/sec/android/app/shealth/food/mealinput/IMealActivityAction;
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->access$300(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;)Lcom/sec/android/app/shealth/food/mealinput/IMealActivityAction;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/shealth/food/mealinput/IMealActivityAction;->prepareBundleData()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 236
    const-string v2, "edit_meal_back_press_save"

    const-string v3, "edit_meal_back_press_save"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 237
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$3;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;

    iget-object v2, v2, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->fragmentMode:Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;

    sget-object v3, Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;->EDIT_MY_MEAL:Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;

    if-ne v2, v3, :cond_0

    .line 238
    const-string v2, "CALLING_ACTIVITY_CLASS"

    const-class v3, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 242
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$3;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;

    # invokes: Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->handleMemoFocus()V
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->access$100(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;)V

    .line 243
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$3;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->startActivity(Landroid/content/Intent;)V

    .line 248
    .end local v0    # "intent":Landroid/content/Intent;
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$3;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->access$200(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;)Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/shealth/food/app/UserActionLog;->TRY_TO_ADD_FOOD_INFORMATION_PLUS:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    invoke-interface {v2, v3}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;->logUserAction(Lcom/sec/android/app/shealth/food/app/UserActionLog;)V

    .line 249
    return-void

    .line 245
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$3;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;

    const v3, 0x7f090925

    new-array v4, v7, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 246
    .local v1, "message":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$3;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2, v1, v7}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
