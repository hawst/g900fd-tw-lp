.class public Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;
.source "FoodTrackerActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IContentInitializatorGetter;
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerProvider;
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/ISaveChosenItemListenerGetter;
.implements Lcom/sec/android/app/shealth/food/summary/FoodTrackerProvider;
.implements Lcom/sec/android/app/shealth/logutils/graph/FragmentSwitchable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$7;,
        Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$SaveMealWithImageAfterScaleCopyTask;,
        Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$SuccessSearchDialogButtonController;,
        Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$FoodTrackerMode;
    }
.end annotation


# static fields
.field private static final FRAGMENT_CONTAINER_VIEW_ID:I = 0x7f080091

.field private static final FRAGMENT_MODE:Ljava/lang/String; = "FRAGMENT_MODE"

.field private static final IMAGE_CAPTURE_URI_KEY:Ljava/lang/String; = "IMAGE_CAPTURE_URI_KEY"

.field private static final LOG_TAG:Ljava/lang/String;

.field private static final RESET_DIALOG_TAG:Ljava/lang/String; = "reset_dialog"

.field private static final SHOW_GRAPH_FRAGMENT_IN_PORTRAIT:Ljava/lang/String; = "SHOW_GRAPH_FRAGMENT_IN_PORTRAIT"

.field private static final TAKE_PHOTO:I = 0x1388

.field private static final TAKE_PHOTO_CODES:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/sec/android/app/shealth/food/constants/MealType;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAKE_PHOTO_FOR_BREAKFAST:I = 0x1389

.field private static final TAKE_PHOTO_FOR_DINNER:I = 0x138b

.field private static final TAKE_PHOTO_FOR_LUNCH:I = 0x138a

.field private static final TAKE_PHOTO_FOR_SNACKS:I = 0x138c


# instance fields
.field private final SUMMARYVIEW_DEFAULT_ACTIONBAR_ICON_COUNT:I

.field private actionBarShareviaButton:Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

.field protected foodTrackerBaseFragment:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

.field private isSaveinstance:Z

.field private mContentObserver:Landroid/database/ContentObserver;

.field private final mDialogControllerMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;",
            ">;"
        }
    .end annotation
.end field

.field private mFoodGraphFragmentSic:Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;

.field private mFoodTrackerMode:Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$FoodTrackerMode;

.field private mImageCaptureUri:Landroid/net/Uri;

.field private mShowFromHomeDashboard:Z

.field private mShowGraphFragmentInPortrait:Z

.field private mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 91
    const-class v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->getLogTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->LOG_TAG:Ljava/lang/String;

    .line 102
    new-instance v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$1;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->TAKE_PHOTO_CODES:Landroid/util/SparseArray;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 89
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;-><init>()V

    .line 111
    new-instance v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$2;-><init>(Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->mDialogControllerMap:Ljava/util/Map;

    .line 119
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->mShowGraphFragmentInPortrait:Z

    .line 122
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->isSaveinstance:Z

    .line 131
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->SUMMARYVIEW_DEFAULT_ACTIONBAR_ICON_COUNT:I

    .line 133
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->mShowFromHomeDashboard:Z

    .line 134
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->mImageCaptureUri:Landroid/net/Uri;

    .line 619
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;)Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->mFoodGraphFragmentSic:Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;

    .prologue
    .line 89
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->prepareShareView()V

    return-void
.end method

.method private getContentObserver()Landroid/database/ContentObserver;
    .locals 2

    .prologue
    .line 605
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->mContentObserver:Landroid/database/ContentObserver;

    if-nez v0, :cond_0

    .line 606
    new-instance v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$6;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$6;-><init>(Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->mContentObserver:Landroid/database/ContentObserver;

    .line 616
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->mContentObserver:Landroid/database/ContentObserver;

    return-object v0
.end method

.method private initFragments()V
    .locals 4

    .prologue
    .line 223
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const v2, 0x7f080091

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 224
    .local v0, "fragment":Landroid/support/v4/app/Fragment;
    if-nez v0, :cond_0

    .line 225
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->createFoodTrackerBaseFragment()Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->foodTrackerBaseFragment:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    .line 226
    new-instance v1, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;

    invoke-direct {v1}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->mFoodGraphFragmentSic:Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;

    .line 238
    .end local v0    # "fragment":Landroid/support/v4/app/Fragment;
    :goto_0
    return-void

    .line 229
    .restart local v0    # "fragment":Landroid/support/v4/app/Fragment;
    :cond_0
    instance-of v1, v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    if-eqz v1, :cond_1

    .line 230
    check-cast v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    .end local v0    # "fragment":Landroid/support/v4/app/Fragment;
    iput-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->foodTrackerBaseFragment:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    .line 231
    new-instance v1, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;

    invoke-direct {v1}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->mFoodGraphFragmentSic:Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;

    goto :goto_0

    .line 232
    .restart local v0    # "fragment":Landroid/support/v4/app/Fragment;
    :cond_1
    instance-of v1, v0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;

    if-eqz v1, :cond_2

    .line 233
    check-cast v0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;

    .end local v0    # "fragment":Landroid/support/v4/app/Fragment;
    iput-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->mFoodGraphFragmentSic:Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;

    .line 234
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->createFoodTrackerBaseFragment()Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->foodTrackerBaseFragment:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    goto :goto_0

    .line 236
    .restart local v0    # "fragment":Landroid/support/v4/app/Fragment;
    :cond_2
    sget-object v1, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Wrong type of fragment is attached :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/common/utils/Logging;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private launchSetQuickInputActivity()V
    .locals 2

    .prologue
    .line 653
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 654
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->startActivity(Landroid/content/Intent;)V

    .line 655
    return-void
.end method

.method private showResetPopup()V
    .locals 3

    .prologue
    .line 461
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const/4 v1, 0x2

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const v1, 0x7f09003b

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090062

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090032

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090048

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string/jumbo v2, "reset_dialog"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 467
    return-void
.end method

.method private startSetGoalActivity()V
    .locals 2

    .prologue
    .line 450
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 451
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->startActivity(Landroid/content/Intent;)V

    .line 452
    return-void
.end method

.method private switchFragmentToGraphSilently()V
    .locals 5

    .prologue
    .line 507
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    sget-object v3, Lcom/sec/android/app/shealth/food/app/UserActionLog;->ENTER_FOOD_CHART_VIEW:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    invoke-interface {v2, v3}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;->logUserAction(Lcom/sec/android/app/shealth/food/app/UserActionLog;)V

    .line 508
    sget-object v2, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$FoodTrackerMode;->GRAPH:Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$FoodTrackerMode;

    iput-object v2, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->mFoodTrackerMode:Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$FoodTrackerMode;

    .line 509
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->isFinishing()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->isSaveinstance:Z

    if-nez v2, :cond_2

    .line 511
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    .line 513
    .local v1, "transaction":Landroid/support/v4/app/FragmentTransaction;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->mFoodGraphFragmentSic:Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 514
    .local v0, "graphFragment":Landroid/support/v4/app/Fragment;
    if-nez v0, :cond_1

    .line 516
    const v2, 0x7f080091

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->mFoodGraphFragmentSic:Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;

    iget-object v4, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->mFoodGraphFragmentSic:Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 519
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->mFoodGraphFragmentSic:Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentTransaction;->show(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 522
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->foodTrackerBaseFragment:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 523
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 525
    .end local v0    # "graphFragment":Landroid/support/v4/app/Fragment;
    .end local v1    # "transaction":Landroid/support/v4/app/FragmentTransaction;
    :cond_2
    return-void
.end method

.method private switchFragmentToSummarySilently()V
    .locals 6

    .prologue
    .line 478
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    sget-object v4, Lcom/sec/android/app/shealth/food/app/UserActionLog;->ENTER_FOOD_SUMMARY_VIEW:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    invoke-interface {v3, v4}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;->logUserAction(Lcom/sec/android/app/shealth/food/app/UserActionLog;)V

    .line 479
    sget-object v3, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$FoodTrackerMode;->BASE:Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$FoodTrackerMode;

    iput-object v3, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->mFoodTrackerMode:Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$FoodTrackerMode;

    .line 481
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 482
    .local v0, "fragmentManager":Landroid/support/v4/app/FragmentManager;
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->foodTrackerBaseFragment:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    .line 483
    .local v1, "summaryFragment":Landroid/support/v4/app/Fragment;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->isFinishing()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-boolean v3, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->isSaveinstance:Z

    if-nez v3, :cond_2

    .line 485
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    .line 486
    .local v2, "transaction":Landroid/support/v4/app/FragmentTransaction;
    if-nez v1, :cond_1

    .line 487
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->createFoodTrackerBaseFragment()Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->foodTrackerBaseFragment:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    .line 488
    const v3, 0x7f080091

    iget-object v4, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->foodTrackerBaseFragment:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    iget-object v5, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->foodTrackerBaseFragment:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v3, v4, v5}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 490
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->mFoodGraphFragmentSic:Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 491
    invoke-virtual {v2}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 495
    .end local v2    # "transaction":Landroid/support/v4/app/FragmentTransaction;
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->deleteShareViaButton()V

    .line 496
    return-void
.end method


# virtual methods
.method public addShareViaButton()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 690
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 692
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getTotalActionBarButtonCount()I

    move-result v0

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->actionBarShareviaButton:Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    if-eqz v0, :cond_0

    .line 693
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    new-array v1, v1, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->actionBarShareviaButton:Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 695
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->invalidateOptionsMenu()V

    .line 697
    :cond_1
    return-void
.end method

.method protected createFoodTrackerBaseFragment()Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;
    .locals 1

    .prologue
    .line 245
    new-instance v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;-><init>()V

    return-object v0
.end method

.method protected customizeActionBar()V
    .locals 6

    .prologue
    .line 250
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->customizeActionBar()V

    .line 252
    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    const v3, 0x7f0207c0

    new-instance v4, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$4;

    invoke-direct {v4, p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$4;-><init>(Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;)V

    invoke-direct {v1, v3, v4}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;-><init>(ILandroid/view/View$OnClickListener;)V

    .line 259
    .local v1, "actionBarLogButtonBuilder":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;
    const v3, 0x7f09005a

    invoke-virtual {v1, v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->setHoverTextResID(I)Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    .line 262
    new-instance v2, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    const v3, 0x7f0207cf

    new-instance v4, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$5;

    invoke-direct {v4, p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$5;-><init>(Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;)V

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;-><init>(ILandroid/view/View$OnClickListener;)V

    .line 271
    .local v2, "actionBarShareviaButtonBuilder":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;
    const v3, 0x7f090033

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->setHoverTextResID(I)Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    .line 272
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->create()Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->actionBarShareviaButton:Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    .line 274
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    .line 275
    .local v0, "actionBarHelper":Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;
    const v3, 0x7f090028

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 276
    const/4 v3, 0x1

    new-array v3, v3, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const/4 v4, 0x0

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->create()Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 278
    return-void
.end method

.method public deleteShareViaButton()V
    .locals 3

    .prologue
    .line 670
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 672
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getTotalActionBarButtonCount()I

    move-result v0

    .line 673
    .local v0, "i":I
    :goto_0
    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 675
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    add-int/lit8 v2, v0, -0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->removeActionButton(I)V

    .line 676
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getTotalActionBarButtonCount()I

    move-result v0

    goto :goto_0

    .line 679
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->invalidateOptionsMenu()V

    .line 682
    .end local v0    # "i":I
    :cond_1
    return-void
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 5
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 702
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v3

    if-nez v3, :cond_0

    .line 703
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    .line 704
    .local v1, "focusedView":Landroid/view/View;
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->foodTrackerBaseFragment:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->foodTrackerBaseFragment:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->isVisible()Z

    move-result v3

    if-eqz v3, :cond_0

    if-eqz v1, :cond_0

    .line 708
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->foodTrackerBaseFragment:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v4

    invoke-virtual {v3, v1, v4}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->assignFocus(Landroid/view/View;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 710
    .local v2, "status":Z
    if-eqz v2, :cond_0

    .line 711
    const/4 v3, 0x1

    .line 720
    .end local v1    # "focusedView":Landroid/view/View;
    .end local v2    # "status":Z
    :goto_0
    return v3

    .line 713
    .restart local v1    # "focusedView":Landroid/view/View;
    :catch_0
    move-exception v0

    .line 714
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "FoodTrackerActivity"

    invoke-static {v3, v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 720
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "focusedView":Landroid/view/View;
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v3

    goto :goto_0
.end method

.method public getContentInitializationListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 661
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->foodTrackerBaseFragment:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getContentInitializationListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;

    move-result-object v0

    return-object v0
.end method

.method public getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
    .locals 2
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 571
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->mDialogControllerMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;

    .line 572
    .local v0, "dialogButtonController":Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
    if-nez v0, :cond_0

    .line 573
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->foodTrackerBaseFragment:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;

    move-result-object v0

    .line 575
    :cond_0
    return-object v0
.end method

.method protected getHelpItem()Ljava/lang/String;
    .locals 1

    .prologue
    .line 457
    const-string v0, "com.sec.shealth.help.action.FOOD"

    return-object v0
.end method

.method public getOnSaveChosenItemListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/OnSaveChosenItemListener;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 566
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->foodTrackerBaseFragment:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getOnSaveChosenItemListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/OnSaveChosenItemListener;

    move-result-object v0

    return-object v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 7
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 538
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 539
    const/4 v5, -0x1

    if-ne p2, v5, :cond_0

    sget-object v5, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->TAKE_PHOTO_CODES:Landroid/util/SparseArray;

    invoke-virtual {v5, p1}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v5

    if-ltz v5, :cond_0

    .line 541
    iget-object v5, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->mImageCaptureUri:Landroid/net/Uri;

    if-eqz v5, :cond_0

    .line 542
    iget-object v5, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->mImageCaptureUri:Landroid/net/Uri;

    invoke-static {v5, p0}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->getImagePathByUri(Landroid/net/Uri;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    .line 543
    .local v4, "photoFilePath":Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/shealth/common/utils/Utils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 545
    new-instance v2, Landroid/content/Intent;

    const-string v5, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    invoke-direct {v2, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 546
    .local v2, "mediaScanIntent":Landroid/content/Intent;
    iget-object v5, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->mImageCaptureUri:Landroid/net/Uri;

    invoke-virtual {v2, v5}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 547
    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 550
    sget-object v5, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->TAKE_PHOTO_CODES:Landroid/util/SparseArray;

    invoke-virtual {v5, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/food/constants/MealType;

    iget v1, v5, Lcom/sec/android/app/shealth/food/constants/MealType;->mealTypeId:I

    .line 551
    .local v1, "mealTypeId":I
    sget-object v5, Lcom/sec/android/app/shealth/food/constants/MealType;->NOT_DEFINED:Lcom/sec/android/app/shealth/food/constants/MealType;

    iget v5, v5, Lcom/sec/android/app/shealth/food/constants/MealType;->mealTypeId:I

    if-ne v1, v5, :cond_1

    .line 552
    iget-object v5, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->foodTrackerBaseFragment:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    const-class v6, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;

    invoke-virtual {v5, p0, v6}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->createIntentForQuickInput(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 553
    .local v0, "intent":Landroid/content/Intent;
    const-string v5, "FOOD_TRACKER_BASE_DATA_FROM_CAMERA"

    invoke-virtual {v0, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 554
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->startActivity(Landroid/content/Intent;)V

    .line 562
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "mealTypeId":I
    .end local v2    # "mediaScanIntent":Landroid/content/Intent;
    .end local v4    # "photoFilePath":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 556
    .restart local v1    # "mealTypeId":I
    .restart local v2    # "mediaScanIntent":Landroid/content/Intent;
    .restart local v4    # "photoFilePath":Ljava/lang/String;
    :cond_1
    invoke-static {}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->getPathToImage()Ljava/lang/String;

    move-result-object v3

    .line 557
    .local v3, "newPath":Ljava/lang/String;
    new-instance v5, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$SaveMealWithImageAfterScaleCopyTask;

    invoke-direct {v5, p0, v4, v3, v1}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$SaveMealWithImageAfterScaleCopyTask;-><init>(Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;Ljava/lang/String;Ljava/lang/String;I)V

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Void;

    invoke-virtual {v5, v6}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$SaveMealWithImageAfterScaleCopyTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 282
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->mShowFromHomeDashboard:Z

    if-eqz v0, :cond_0

    .line 283
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onBackPressed()V

    .line 293
    :goto_0
    return-void

    .line 287
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->mFoodGraphFragmentSic:Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 289
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->switchFragmentToSummary()V

    goto :goto_0

    .line 292
    :cond_1
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x0

    .line 139
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 140
    sget-boolean v4, Lcom/sec/android/app/shealth/SHealthApplication;->isFoodInfoCleanUpRequired:Z

    if-eqz v4, :cond_0

    .line 142
    new-instance v4, Ljava/lang/Thread;

    new-instance v5, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$3;

    invoke-direct {v5, p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$3;-><init>(Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;)V

    invoke-direct {v4, v5}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 152
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->getApplication()Landroid/app/Application;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLoggerHolder;

    invoke-interface {v4}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLoggerHolder;->getUserActionLogger()Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    .line 153
    iput-boolean v6, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->isSaveinstance:Z

    .line 154
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->initFragments()V

    .line 155
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string/jumbo v5, "show_graph_fragment"

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    iput-boolean v4, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->mShowFromHomeDashboard:Z

    .line 156
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->getApplication()Landroid/app/Application;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/shealth/food/app/IFoodConfigurationHolder;

    invoke-interface {v4}, Lcom/sec/android/app/shealth/food/app/IFoodConfigurationHolder;->getConfiguration()Lcom/sec/android/app/shealth/food/app/IFoodConfiguration;

    move-result-object v0

    .line 157
    .local v0, "foodConfiguration":Lcom/sec/android/app/shealth/food/app/IFoodConfiguration;
    if-eqz p1, :cond_3

    const-string v4, "FRAGMENT_MODE"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 158
    const-string v4, "FRAGMENT_MODE"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 159
    .local v2, "fragmentModeIndex":I
    invoke-static {}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$FoodTrackerMode;->values()[Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$FoodTrackerMode;

    move-result-object v4

    aget-object v1, v4, v2

    .line 160
    .local v1, "foodTrackerMode":Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$FoodTrackerMode;
    sget-object v4, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$FoodTrackerMode;->GRAPH:Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$FoodTrackerMode;

    if-ne v1, v4, :cond_2

    .line 161
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->switchFragmentToGraphSilently()V

    .line 191
    .end local v1    # "foodTrackerMode":Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$FoodTrackerMode;
    .end local v2    # "fragmentModeIndex":I
    :goto_0
    if-eqz p1, :cond_1

    const-string v4, "IMAGE_CAPTURE_URI_KEY"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "IMAGE_CAPTURE_URI_KEY"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 192
    const-string v4, "IMAGE_CAPTURE_URI_KEY"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 193
    .local v3, "uriString":Ljava/lang/String;
    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->mImageCaptureUri:Landroid/net/Uri;

    .line 196
    .end local v3    # "uriString":Ljava/lang/String;
    :cond_1
    return-void

    .line 163
    .restart local v1    # "foodTrackerMode":Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$FoodTrackerMode;
    .restart local v2    # "fragmentModeIndex":I
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->switchFragmentToSummarySilently()V

    goto :goto_0

    .line 166
    .end local v1    # "foodTrackerMode":Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$FoodTrackerMode;
    .end local v2    # "fragmentModeIndex":I
    :cond_3
    invoke-interface {v0}, Lcom/sec/android/app/shealth/food/app/IFoodConfiguration;->isHorizontalViewEnabled()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 167
    if-eqz p1, :cond_4

    .line 168
    const-string v4, "SHOW_GRAPH_FRAGMENT_IN_PORTRAIT"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    iput-boolean v4, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->mShowGraphFragmentInPortrait:Z

    .line 170
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget v4, v4, Landroid/content/res/Configuration;->orientation:I

    if-ne v4, v7, :cond_5

    .line 171
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    sget-object v5, Lcom/sec/android/app/shealth/food/app/UserActionLog;->ROTATE_TO_THE_LANDSCAPE_MODE_IN_SUMMARY_VIEW:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    invoke-interface {v4, v5}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;->logUserAction(Lcom/sec/android/app/shealth/food/app/UserActionLog;)V

    .line 173
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget v4, v4, Landroid/content/res/Configuration;->orientation:I

    if-eq v4, v7, :cond_6

    iget-boolean v4, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->mShowGraphFragmentInPortrait:Z

    if-eqz v4, :cond_7

    .line 175
    :cond_6
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->switchFragmentToGraphSilently()V

    goto :goto_0

    .line 177
    :cond_7
    iget-boolean v4, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->mShowFromHomeDashboard:Z

    if-eqz v4, :cond_8

    .line 178
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->switchFragmentToGraphSilently()V

    goto :goto_0

    .line 180
    :cond_8
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->switchFragmentToSummarySilently()V

    goto :goto_0

    .line 184
    :cond_9
    iget-boolean v4, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->mShowFromHomeDashboard:Z

    if-eqz v4, :cond_a

    .line 185
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->switchFragmentToGraphSilently()V

    goto :goto_0

    .line 187
    :cond_a
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->switchFragmentToSummarySilently()V

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 4
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 303
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->isDrawerMenuShown()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 324
    :goto_0
    return v0

    .line 306
    :cond_0
    sget-object v2, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$7;->$SwitchMap$com$sec$android$app$shealth$food$summary$FoodTrackerActivity$FoodTrackerMode:[I

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->mFoodTrackerMode:Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$FoodTrackerMode;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$FoodTrackerMode;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    :goto_1
    move v0, v1

    .line 324
    goto :goto_0

    .line 309
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    .line 310
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v2, 0x7f100013

    invoke-virtual {v0, v2, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    goto :goto_1

    .line 316
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    goto :goto_1

    .line 306
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onLogSelected()V
    .locals 2

    .prologue
    .line 297
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/food/log/FoodLogListActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 298
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->startActivity(Landroid/content/Intent;)V

    .line 299
    return-void
.end method

.method public onMenuOpened(ILandroid/view/Menu;)Z
    .locals 2
    .param p1, "featureId"    # I
    .param p2, "menu"    # Landroid/view/Menu;

    .prologue
    .line 355
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    sget-object v1, Lcom/sec/android/app/shealth/food/app/UserActionLog;->PRESS_MORE_MENU_BUTTON:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;->logUserAction(Lcom/sec/android/app/shealth/food/app/UserActionLog;)V

    .line 356
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onMenuOpened(ILandroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 6
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 212
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 213
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 214
    const-string v3, "MEAL_ID_THAT_WAS_ADDED"

    const-wide/16 v4, -0x1

    invoke-virtual {p1, v3, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v1

    .line 215
    .local v1, "mealId":J
    new-instance v3, Lcom/sec/android/app/shealth/food/fooddao/MealDaoImplDb;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/food/fooddao/MealDaoImplDb;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v1, v2}, Lcom/sec/android/app/shealth/food/fooddao/MealDaoImplDb;->getDataById(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    .line 216
    .local v0, "mealData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    if-eqz v0, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->foodTrackerBaseFragment:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->isAdded()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 217
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->foodTrackerBaseFragment:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getMealTime()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->initForTime(J)V

    .line 220
    .end local v0    # "mealData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    .end local v1    # "mealId":J
    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v1, 0x1

    .line 361
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 390
    :goto_0
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    :goto_1
    return v1

    .line 363
    :sswitch_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    sget-object v3, Lcom/sec/android/app/shealth/food/app/UserActionLog;->SUMMARY_ENTER_SET_GOAL_MENU:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    invoke-interface {v2, v3}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;->logUserAction(Lcom/sec/android/app/shealth/food/app/UserActionLog;)V

    .line 364
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->startSetGoalActivity()V

    goto :goto_1

    .line 372
    :sswitch_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    sget-object v3, Lcom/sec/android/app/shealth/food/app/UserActionLog;->SUMMARY_ENTER_MY_FOOD_MENU:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    invoke-interface {v2, v3}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;->logUserAction(Lcom/sec/android/app/shealth/food/app/UserActionLog;)V

    .line 373
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->foodTrackerBaseFragment:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    const-class v3, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;

    invoke-virtual {v2, p0, v3}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->createIntentCalculatingMealType(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 374
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 377
    .end local v0    # "intent":Landroid/content/Intent;
    :sswitch_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->SUMMARY_ENTER_SHARE_VIA_MENU:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    invoke-interface {v1, v2}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;->logUserAction(Lcom/sec/android/app/shealth/food/app/UserActionLog;)V

    goto :goto_0

    .line 380
    :sswitch_3
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->SUMMARY_ENTER_HELP_MENU:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    invoke-interface {v1, v2}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;->logUserAction(Lcom/sec/android/app/shealth/food/app/UserActionLog;)V

    .line 381
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    goto :goto_1

    .line 383
    :sswitch_4
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->SUMMARY_ENTER_PRINT_MENU:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    invoke-interface {v1, v2}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;->logUserAction(Lcom/sec/android/app/shealth/food/app/UserActionLog;)V

    goto :goto_0

    .line 386
    :sswitch_5
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->launchSetQuickInputActivity()V

    goto :goto_0

    .line 361
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f080c8a -> :sswitch_3
        0x7f080c98 -> :sswitch_2
        0x7f080c99 -> :sswitch_0
        0x7f080c9a -> :sswitch_5
        0x7f080c9b -> :sswitch_1
        0x7f080ca7 -> :sswitch_4
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 206
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->unregisterContentObserver()V

    .line 207
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onPause()V

    .line 208
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 200
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onResume()V

    .line 201
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->registerContentObserver()V

    .line 202
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 334
    :try_start_0
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->mShowGraphFragmentInPortrait:Z

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->mFoodGraphFragmentSic:Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->isGraphTapped()Z

    move-result v2

    or-int/2addr v1, v2

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->mShowGraphFragmentInPortrait:Z

    .line 335
    const-string v1, "SHOW_GRAPH_FRAGMENT_IN_PORTRAIT"

    iget-boolean v2, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->mShowGraphFragmentInPortrait:Z

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 336
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->mFoodTrackerMode:Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$FoodTrackerMode;

    if-eqz v1, :cond_0

    .line 337
    const-string v1, "FRAGMENT_MODE"

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->mFoodTrackerMode:Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$FoodTrackerMode;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$FoodTrackerMode;->ordinal()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 339
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->mImageCaptureUri:Landroid/net/Uri;

    if-eqz v1, :cond_1

    .line 340
    const-string v1, "IMAGE_CAPTURE_URI_KEY"

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->mImageCaptureUri:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 342
    :cond_1
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->isSaveinstance:Z

    .line 344
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 351
    :goto_0
    return-void

    .line 346
    :catch_0
    move-exception v0

    .line 348
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "FoodTrackerActivity"

    const-string v2, "Problem while executing save instance"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public openGallery(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 444
    .local p1, "imagePaths":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 445
    .local v0, "viewPhotoIntent":Landroid/content/Intent;
    const-string v1, "gallery_image_paths"

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 446
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->startActivity(Landroid/content/Intent;)V

    .line 447
    return-void
.end method

.method protected registerContentObserver()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 593
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Meal;->CONTENT_URI:Landroid/net/Uri;

    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->getContentObserver()Landroid/database/ContentObserver;

    move-result-object v2

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 594
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$MealImage;->CONTENT_URI:Landroid/net/Uri;

    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->getContentObserver()Landroid/database/ContentObserver;

    move-result-object v2

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 595
    return-void
.end method

.method public showFragment(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;)V
    .locals 1
    .param p1, "fragment"    # Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;

    .prologue
    .line 471
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->isAdded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 472
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->showFragment(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;)V

    .line 474
    :cond_0
    return-void
.end method

.method public startCameraActivity(Lcom/sec/android/app/shealth/food/constants/MealType;)V
    .locals 3
    .param p1, "mealType"    # Lcom/sec/android/app/shealth/food/constants/MealType;

    .prologue
    .line 435
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/GalleryUtils;->getThirdPartyURI()Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->mImageCaptureUri:Landroid/net/Uri;

    .line 436
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.media.action.IMAGE_CAPTURE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 437
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "output"

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->mImageCaptureUri:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 438
    const-string/jumbo v1, "return-data"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 439
    sget-object v1, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->TAKE_PHOTO_CODES:Landroid/util/SparseArray;

    sget-object v2, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->TAKE_PHOTO_CODES:Landroid/util/SparseArray;

    invoke-virtual {v2, p1}, Landroid/util/SparseArray;->indexOfValue(Ljava/lang/Object;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 440
    return-void
.end method

.method public switchFragmentToGraph()V
    .locals 2

    .prologue
    .line 531
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->foodTrackerBaseFragment:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->mFoodGraphFragmentSic:Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/logutils/graph/GraphLogUtils;->addBundleToGraphFragment(Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;)V

    .line 532
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->mShowGraphFragmentInPortrait:Z

    .line 533
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->switchFragmentToGraphSilently()V

    .line 534
    return-void
.end method

.method public switchFragmentToSummary()V
    .locals 1

    .prologue
    .line 502
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->mShowGraphFragmentInPortrait:Z

    .line 503
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->switchFragmentToSummarySilently()V

    .line 504
    return-void
.end method

.method protected unregisterContentObserver()V
    .locals 2

    .prologue
    .line 601
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->getContentObserver()Landroid/database/ContentObserver;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 602
    return-void
.end method
