.class public final enum Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;
.super Ljava/lang/Enum;
.source "FragmentMode.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;

.field public static final enum EDIT_MEAL:Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;

.field public static final enum EDIT_MY_MEAL:Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;

.field public static final enum VIEW_MEAL:Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;

.field public static final enum VIEW_PLAN_MEAL:Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;


# instance fields
.field public final addFoodButtonVisibility:I

.field public final isEditable:Z

.field public final isPortionSizeButtonVisible:Z

.field public final nutritionInfoButtonVisibility:I


# direct methods
.method static constructor <clinit>()V
    .locals 14

    .prologue
    const/4 v13, 0x3

    const/4 v12, 0x2

    const/16 v3, 0x8

    const/4 v11, 0x1

    const/4 v2, 0x0

    .line 24
    new-instance v0, Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;

    const-string v1, "VIEW_MEAL"

    move v4, v2

    move v5, v2

    move v6, v2

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;-><init>(Ljava/lang/String;IIZZI)V

    sput-object v0, Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;->VIEW_MEAL:Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;

    .line 26
    new-instance v4, Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;

    const-string v5, "VIEW_PLAN_MEAL"

    move v6, v11

    move v7, v3

    move v8, v2

    move v9, v2

    move v10, v3

    invoke-direct/range {v4 .. v10}, Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;-><init>(Ljava/lang/String;IIZZI)V

    sput-object v4, Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;->VIEW_PLAN_MEAL:Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;

    .line 28
    new-instance v4, Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;

    const-string v5, "EDIT_MEAL"

    move v6, v12

    move v7, v2

    move v8, v11

    move v9, v11

    move v10, v2

    invoke-direct/range {v4 .. v10}, Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;-><init>(Ljava/lang/String;IIZZI)V

    sput-object v4, Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;->EDIT_MEAL:Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;

    .line 30
    new-instance v4, Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;

    const-string v5, "EDIT_MY_MEAL"

    move v6, v13

    move v7, v2

    move v8, v11

    move v9, v2

    move v10, v3

    invoke-direct/range {v4 .. v10}, Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;-><init>(Ljava/lang/String;IIZZI)V

    sput-object v4, Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;->EDIT_MY_MEAL:Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;

    .line 22
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;

    sget-object v1, Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;->VIEW_MEAL:Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;->VIEW_PLAN_MEAL:Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;

    aput-object v1, v0, v11

    sget-object v1, Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;->EDIT_MEAL:Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;

    aput-object v1, v0, v12

    sget-object v1, Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;->EDIT_MY_MEAL:Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;

    aput-object v1, v0, v13

    sput-object v0, Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;->$VALUES:[Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIZZI)V
    .locals 0
    .param p3, "addFoodButtonVisibility"    # I
    .param p4, "isEditable"    # Z
    .param p5, "isPortionSizeButtonVisible"    # Z
    .param p6, "nutritionInfoButtonVisibility"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZZI)V"
        }
    .end annotation

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 47
    iput p3, p0, Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;->addFoodButtonVisibility:I

    .line 48
    iput-boolean p4, p0, Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;->isEditable:Z

    .line 49
    iput-boolean p5, p0, Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;->isPortionSizeButtonVisible:Z

    .line 50
    iput p6, p0, Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;->nutritionInfoButtonVisibility:I

    .line 51
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 22
    const-class v0, Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;->$VALUES:[Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;

    return-object v0
.end method
