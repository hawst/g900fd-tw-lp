.class synthetic Lcom/sec/android/app/shealth/cignacoach/util/DateFormatUtil$1;
.super Ljava/lang/Object;
.source "DateFormatUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/cignacoach/util/DateFormatUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$sec$android$app$shealth$cignacoach$constants$CignaConstants$DateformatType:[I

.field static final synthetic $SwitchMap$com$sec$android$app$shealth$common$utils$calendar$DateFormatType:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 324
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/DateFormatType;->values()[Lcom/sec/android/app/shealth/common/utils/calendar/DateFormatType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/util/DateFormatUtil$1;->$SwitchMap$com$sec$android$app$shealth$common$utils$calendar$DateFormatType:[I

    :try_start_0
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/DateFormatUtil$1;->$SwitchMap$com$sec$android$app$shealth$common$utils$calendar$DateFormatType:[I

    sget-object v1, Lcom/sec/android/app/shealth/common/utils/calendar/DateFormatType;->DDMMYYYY:Lcom/sec/android/app/shealth/common/utils/calendar/DateFormatType;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_5

    :goto_0
    :try_start_1
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/DateFormatUtil$1;->$SwitchMap$com$sec$android$app$shealth$common$utils$calendar$DateFormatType:[I

    sget-object v1, Lcom/sec/android/app/shealth/common/utils/calendar/DateFormatType;->MMDDYYYY:Lcom/sec/android/app/shealth/common/utils/calendar/DateFormatType;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_4

    :goto_1
    :try_start_2
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/DateFormatUtil$1;->$SwitchMap$com$sec$android$app$shealth$common$utils$calendar$DateFormatType:[I

    sget-object v1, Lcom/sec/android/app/shealth/common/utils/calendar/DateFormatType;->YYYYMMDD:Lcom/sec/android/app/shealth/common/utils/calendar/DateFormatType;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_3

    .line 77
    :goto_2
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DateformatType;->values()[Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DateformatType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/util/DateFormatUtil$1;->$SwitchMap$com$sec$android$app$shealth$cignacoach$constants$CignaConstants$DateformatType:[I

    :try_start_3
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/DateFormatUtil$1;->$SwitchMap$com$sec$android$app$shealth$cignacoach$constants$CignaConstants$DateformatType:[I

    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DateformatType;->YEAR:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DateformatType;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_2

    :goto_3
    :try_start_4
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/DateFormatUtil$1;->$SwitchMap$com$sec$android$app$shealth$cignacoach$constants$CignaConstants$DateformatType:[I

    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DateformatType;->MONTH:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DateformatType;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_1

    :goto_4
    :try_start_5
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/DateFormatUtil$1;->$SwitchMap$com$sec$android$app$shealth$cignacoach$constants$CignaConstants$DateformatType:[I

    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DateformatType;->DAY:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DateformatType;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_0

    :goto_5
    return-void

    :catch_0
    move-exception v0

    goto :goto_5

    :catch_1
    move-exception v0

    goto :goto_4

    :catch_2
    move-exception v0

    goto :goto_3

    .line 324
    :catch_3
    move-exception v0

    goto :goto_2

    :catch_4
    move-exception v0

    goto :goto_1

    :catch_5
    move-exception v0

    goto :goto_0
.end method
