.class Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity$DevListAdapter;
.super Landroid/widget/BaseAdapter;
.source "CompatibleAccessoriesListActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DevListAdapter"
.end annotation


# instance fields
.field mAccessoryList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 206
    .local p2, "accessoryList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;>;"
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity$DevListAdapter;->this$0:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 207
    iput-object p2, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity$DevListAdapter;->mAccessoryList:Ljava/util/ArrayList;

    .line 208
    return-void
.end method


# virtual methods
.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 293
    const/4 v0, 0x0

    return v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 213
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity$DevListAdapter;->mAccessoryList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 214
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity$DevListAdapter;->mAccessoryList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 216
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDeviceType(I)I
    .locals 2
    .param p1, "deviceType"    # I

    .prologue
    const v0, 0x7f09018d

    .line 232
    const/16 v1, 0x2712

    if-ne p1, v1, :cond_1

    .line 250
    :cond_0
    :goto_0
    return v0

    .line 235
    :cond_1
    const/16 v1, 0x271b

    if-ne p1, v1, :cond_2

    .line 236
    const v0, 0x7f090bb3

    goto :goto_0

    .line 238
    :cond_2
    const/16 v1, 0x2718

    if-ne p1, v1, :cond_3

    .line 239
    const v0, 0x7f090023

    goto :goto_0

    .line 241
    :cond_3
    const/16 v1, 0x271d

    if-eq p1, v1, :cond_4

    const/16 v1, 0x2724

    if-ne p1, v1, :cond_5

    .line 242
    :cond_4
    const v0, 0x7f090025

    goto :goto_0

    .line 243
    :cond_5
    const/16 v1, 0x2714

    if-ne p1, v1, :cond_6

    .line 244
    const v0, 0x7f090e0f

    goto :goto_0

    .line 246
    :cond_6
    const/16 v1, 0x2713

    if-ne p1, v1, :cond_0

    .line 247
    const v0, 0x7f09018f

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "i"    # I

    .prologue
    .line 222
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity$DevListAdapter;->mAccessoryList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 228
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v7, 0x0

    .line 257
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity$DevListAdapter;->this$0:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    const-string v6, "layout_inflater"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 259
    .local v1, "inflater":Landroid/view/LayoutInflater;
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity$DevListAdapter;->mAccessoryList:Ljava/util/ArrayList;

    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;->getModelName()Ljava/lang/String;

    move-result-object v5

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_0

    .line 260
    const v5, 0x7f03009a

    invoke-virtual {v1, v5, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .end local p2    # "convertView":Landroid/view/View;
    check-cast p2, Landroid/widget/LinearLayout;

    .line 262
    .restart local p2    # "convertView":Landroid/view/View;
    const v5, 0x7f0802dc

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 263
    .local v3, "name":Landroid/widget/TextView;
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity$DevListAdapter;->mAccessoryList:Ljava/util/ArrayList;

    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;->getDeviceType()I

    move-result v5

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity$DevListAdapter;->getDeviceType(I)I

    move-result v4

    .line 264
    .local v4, "nameResourceId":I
    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 265
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity$DevListAdapter;->this$0:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity$DevListAdapter;->this$0:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0901fd

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v5, v6}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->setContentDescription(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    const/4 v5, 0x0

    invoke-virtual {p2, v5}, Landroid/view/View;->setClickable(Z)V

    .line 288
    .end local v4    # "nameResourceId":I
    :goto_0
    return-object p2

    .line 271
    .end local v3    # "name":Landroid/widget/TextView;
    :cond_0
    const v5, 0x7f03001b

    invoke-virtual {v1, v5, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .end local p2    # "convertView":Landroid/view/View;
    check-cast p2, Landroid/widget/LinearLayout;

    .line 273
    .restart local p2    # "convertView":Landroid/view/View;
    const v5, 0x7f08001f

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 274
    .restart local v3    # "name":Landroid/widget/TextView;
    const v5, 0x7f08009c

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 275
    .local v2, "manufacturerName":Landroid/widget/TextView;
    const v5, 0x7f080099

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 277
    .local v0, "devicePicture":Landroid/widget/ImageView;
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity$DevListAdapter;->mAccessoryList:Ljava/util/ArrayList;

    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;->getImagePath()I

    move-result v5

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 278
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity$DevListAdapter;->mAccessoryList:Ljava/util/ArrayList;

    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;

    iget-object v5, v5, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;->manufacturer:Ljava/lang/String;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 280
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity$DevListAdapter;->mAccessoryList:Ljava/util/ArrayList;

    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;->getModelName()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 281
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity$DevListAdapter;->mAccessoryList:Ljava/util/ArrayList;

    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;->getModelName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 284
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity$DevListAdapter;->mAccessoryList:Ljava/util/ArrayList;

    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {p2, v5}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 285
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity$DevListAdapter;->this$0:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;

    iget-object v5, v5, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->onDeviceListItemClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {p2, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public isEnabled(I)Z
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 297
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity$DevListAdapter;->mAccessoryList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;->getModelName()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    .line 299
    const/4 v0, 0x0

    .line 301
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/BaseAdapter;->isEnabled(I)Z

    move-result v0

    goto :goto_0
.end method
