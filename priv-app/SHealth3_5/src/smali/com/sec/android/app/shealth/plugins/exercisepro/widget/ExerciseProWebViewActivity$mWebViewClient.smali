.class Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity$mWebViewClient;
.super Landroid/webkit/WebViewClient;
.source "ExerciseProWebViewActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "mWebViewClient"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;)V
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity$mWebViewClient;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity$1;

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity$mWebViewClient;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;)V

    return-void
.end method


# virtual methods
.method public onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;
    .param p3, "favicon"    # Landroid/graphics/Bitmap;

    .prologue
    .line 55
    invoke-super {p0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 56
    return-void
.end method

.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "errorCode"    # I
    .param p3, "description"    # Ljava/lang/String;
    .param p4, "failingUrl"    # Ljava/lang/String;

    .prologue
    .line 62
    invoke-super {p0, p1, p2, p3, p4}, Landroid/webkit/WebViewClient;->onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V

    .line 63
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity$mWebViewClient;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;->dialog:Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;->access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity$mWebViewClient;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;->dialog:Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;->access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity$mWebViewClient;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;->dialog:Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;->access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;->dismiss()V

    .line 65
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity$mWebViewClient;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;->dialog:Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;->access$202(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;)Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;

    .line 67
    :cond_0
    return-void
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 1
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 48
    invoke-virtual {p1, p2}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 49
    const/4 v0, 0x1

    return v0
.end method
