.class public Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;
.super Landroid/widget/FrameLayout;
.source "ControllerView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;,
        Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;,
        Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$OnValueChangedListener;,
        Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$LayoutType;
    }
.end annotation


# instance fields
.field private mBackgroundImageView:Landroid/widget/ImageView;

.field private mGradationView:Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;

.field private mLayoutHalfMargin:I

.field private mLayoutHeight:I

.field private mLayoutMargin:I

.field private mLayoutType:Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$LayoutType;

.field private mLayoutWidth:I

.field private mOnValueChangedListener:Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$OnValueChangedListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 35
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 28
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$LayoutType;->HORIZONTAL:Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$LayoutType;

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->mLayoutType:Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$LayoutType;

    .line 36
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->initLayout()V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$LayoutType;->HORIZONTAL:Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$LayoutType;

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->mLayoutType:Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$LayoutType;

    .line 41
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->initLayout()V

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 45
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 28
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$LayoutType;->HORIZONTAL:Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$LayoutType;

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->mLayoutType:Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$LayoutType;

    .line 46
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->initLayout()V

    .line 47
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;)Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$LayoutType;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->mLayoutType:Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$LayoutType;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;

    .prologue
    .line 24
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->mLayoutHalfMargin:I

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;)Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$OnValueChangedListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->mOnValueChangedListener:Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$OnValueChangedListener;

    return-object v0
.end method

.method private initLayout()V
    .locals 2

    .prologue
    .line 50
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00f1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->mLayoutMargin:I

    .line 51
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->mLayoutMargin:I

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->mLayoutHalfMargin:I

    .line 53
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;-><init>(Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->mGradationView:Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;

    .line 54
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->mGradationView:Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->addView(Landroid/view/View;)V

    .line 56
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->mBackgroundImageView:Landroid/widget/ImageView;

    .line 57
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->mBackgroundImageView:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->addView(Landroid/view/View;)V

    .line 59
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$1;-><init>(Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->post(Ljava/lang/Runnable;)Z

    .line 66
    return-void
.end method


# virtual methods
.method public getInputRange()V
    .locals 0

    .prologue
    .line 82
    return-void
.end method

.method public getValue()F
    .locals 2

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->mGradationView:Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->getValue()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x41200000    # 10.0f

    div-float/2addr v0, v1

    return v0
.end method

.method public setEnabled(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 90
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setEnabled(Z)V

    .line 91
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->mGradationView:Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->setEnabled(Z)V

    .line 92
    return-void
.end method

.method public setInputRange(FF)V
    .locals 3
    .param p1, "minValue"    # F
    .param p2, "maxValue"    # F

    .prologue
    const/high16 v2, 0x41200000    # 10.0f

    .line 77
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->mGradationView:Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;

    mul-float v1, p1, v2

    float-to-int v1, v1

    mul-float/2addr v2, p2

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->setInputRange(II)V

    .line 78
    return-void
.end method

.method public setInterval(F)V
    .locals 2
    .param p1, "interval"    # F

    .prologue
    .line 95
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->mGradationView:Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;

    const/high16 v1, 0x41200000    # 10.0f

    mul-float/2addr v1, p1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->setInterval(I)V

    .line 96
    return-void
.end method

.method public setNormalRange(FF)V
    .locals 3
    .param p1, "lowBound"    # F
    .param p2, "highBound"    # F

    .prologue
    const/high16 v2, 0x41200000    # 10.0f

    .line 85
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->mGradationView:Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;

    mul-float v1, p1, v2

    float-to-int v1, v1

    mul-float/2addr v2, p2

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->setNormalRange(II)V

    .line 86
    return-void
.end method

.method public setOnValueChangedListener(Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$OnValueChangedListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$OnValueChangedListener;

    .prologue
    .line 126
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->mOnValueChangedListener:Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$OnValueChangedListener;

    .line 127
    return-void
.end method

.method public setOrientation(Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$LayoutType;)V
    .locals 6
    .param p1, "layoutType"    # Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$LayoutType;

    .prologue
    .line 99
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->mLayoutType:Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$LayoutType;

    .line 100
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->mBackgroundImageView:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 101
    .local v0, "backgroundImageViewLayoutParams":Landroid/widget/FrameLayout$LayoutParams;
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->mGradationView:Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout$LayoutParams;

    .line 102
    .local v1, "gradationViewLayoutParams":Landroid/widget/FrameLayout$LayoutParams;
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->mLayoutType:Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$LayoutType;

    sget-object v3, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$LayoutType;->HORIZONTAL:Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$LayoutType;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$LayoutType;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 105
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x43858000    # 267.0f

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->convertDpToPx(Landroid/content/Context;F)F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->mLayoutWidth:I

    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 106
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->getContext()Landroid/content/Context;

    move-result-object v2

    const/high16 v3, 0x42a00000    # 80.0f

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->convertDpToPx(Landroid/content/Context;F)F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->mLayoutHeight:I

    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 115
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->mBackgroundImageView:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 117
    iget v2, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->mLayoutWidth:I

    iget v3, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->mLayoutMargin:I

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 118
    iget v2, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->mLayoutHeight:I

    iget v3, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->mLayoutMargin:I

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 119
    iget v2, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->mLayoutHalfMargin:I

    iget v3, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->mLayoutHalfMargin:I

    iget v4, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->mLayoutHalfMargin:I

    iget v5, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->mLayoutHalfMargin:I

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 120
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->mGradationView:Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 122
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->mGradationView:Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;

    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->mLayoutType:Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$LayoutType;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->setOrientation(Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$LayoutType;)V

    .line 123
    return-void

    .line 110
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00ed

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->mLayoutWidth:I

    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 111
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00ee

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->mLayoutHeight:I

    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 112
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->mBackgroundImageView:Landroid/widget/ImageView;

    const v3, 0x7f0205a8

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public setValue(F)V
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->mGradationView:Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;

    const/high16 v1, 0x41200000    # 10.0f

    mul-float/2addr v1, p1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->setValue(I)V

    .line 70
    return-void
.end method
