.class public Lcom/sec/android/app/shealth/framework/repository/common/DBTables$RunninProTable;
.super Ljava/lang/Object;
.source "DBTables.java"

# interfaces
.implements Landroid/provider/BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/framework/repository/common/DBTables;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "RunninProTable"
.end annotation


# static fields
.field public static final KEY_APPLICATION_NAME:Ljava/lang/String; = "app_name"

.field public static final KEY_CALORIES:Ljava/lang/String; = "calories"

.field public static final KEY_COMMENT:Ljava/lang/String; = "comment"

.field public static final KEY_CREATE_TIME:Ljava/lang/String; = "create_time"

.field public static final KEY_EXERCISE_ID:Ljava/lang/String; = "id_exercise_db"

.field public static final KEY_MIN:Ljava/lang/String; = "min"

.field public static final KEY_PERIOD_DAY_START:Ljava/lang/String; = "day_st"

.field public static final KEY_PERIOD_MONTH_START:Ljava/lang/String; = "month_st"

.field public static final KEY_PERIOD_WEEK_START_MON:Ljava/lang/String; = "week_st_mon"

.field public static final KEY_PERIOD_WEEK_START_SUN:Ljava/lang/String; = "week_st_sun"

.field public static final KEY_PERIOD_YEAR_START:Ljava/lang/String; = "year_st"

.field public static final TABLE_NAME:Ljava/lang/String; = "running_pro"

.field public static final TRIGGER_INSERT_NAME:Ljava/lang/String; = "create_runnin_pro_trigger"

.field public static final TRIGGER_UPDATE_NAME:Ljava/lang/String; = "update_runnin_pro_trigger"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 562
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
