.class Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy$4;
.super Ljava/lang/Object;
.source "PrivacyPolicy.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->initLayout()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;)V
    .locals 0

    .prologue
    .line 220
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy$4;->this$0:Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 225
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy$4;->this$0:Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;

    # getter for: Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->isAgreeCheck:Z
    invoke-static {v4}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->access$100(Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 227
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy$4;->this$0:Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    if-nez v4, :cond_0

    .line 251
    :goto_0
    return-void

    .line 231
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy$4;->this$0:Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;

    # getter for: Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->mSharedPrefsHelper:Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;
    invoke-static {v4}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->access$300(Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;)Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;->setUserCheckPrivacyPolicy(Z)V

    .line 233
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy$4;->this$0:Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;

    # getter for: Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->mSharedPrefsHelper:Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;
    invoke-static {v4}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->access$300(Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;)Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy$4;->this$0:Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/shealth/common/utils/SamsungAccount;->getSamsungAccount(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;->setUserAccountInfo(Ljava/lang/String;)V

    .line 234
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy$4;->this$0:Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setInitializationNeeded(Landroid/content/Context;Z)V

    .line 236
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy$4;->this$0:Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->getMigrationState(Landroid/content/Context;)I

    move-result v1

    .line 238
    .local v1, "migrationState":I
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/service/health/keyManager/KeyManager;->isSecureStorageSupported()Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy$4;->this$0:Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;

    # getter for: Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->mSecuritySettingsHelper:Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;
    invoke-static {v4}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->access$400(Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;)Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy$4;->this$0:Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    const-string/jumbo v6, "security_pin_enabled"

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->getBooleanValueForConfigKey(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 241
    .local v2, "requestPasswordCondition":Z
    :goto_1
    if-eqz v2, :cond_2

    .line 242
    new-instance v0, Landroid/content/Intent;

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy$4;->this$0:Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword;

    invoke-direct {v0, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 243
    .local v0, "intent":Landroid/content/Intent;
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy$4;->this$0:Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;

    invoke-virtual {v3, v0}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .end local v0    # "intent":Landroid/content/Intent;
    .end local v2    # "requestPasswordCondition":Z
    :cond_1
    move v2, v3

    .line 238
    goto :goto_1

    .line 245
    .restart local v2    # "requestPasswordCondition":Z
    :cond_2
    new-instance v0, Landroid/content/Intent;

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy$4;->this$0:Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    invoke-direct {v0, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 246
    .restart local v0    # "intent":Landroid/content/Intent;
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy$4;->this$0:Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;

    invoke-virtual {v3, v0}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 249
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "migrationState":I
    .end local v2    # "requestPasswordCondition":Z
    :cond_3
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy$4;->this$0:Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;

    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy$4;->this$0:Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;

    const v6, 0x7f0908c3

    invoke-virtual {v5, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v3}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0
.end method
