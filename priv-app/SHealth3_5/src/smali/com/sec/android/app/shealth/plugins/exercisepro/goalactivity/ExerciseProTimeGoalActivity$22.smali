.class Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity$22;
.super Lcom/sec/android/app/shealth/common/utils/TextWatcherStub;
.source "ExerciseProTimeGoalActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;)V
    .locals 0

    .prologue
    .line 843
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity$22;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;

    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/utils/TextWatcherStub;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 7
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    const/4 v6, 0x4

    const/4 v4, 0x0

    const/16 v5, 0x3b

    .line 847
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity$22;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;->mMinNumberPicker:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/CustomNumberPicker;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;->access$1300(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;)Lcom/sec/android/app/shealth/plugins/exercisepro/utils/CustomNumberPicker;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/CustomNumberPicker;->setValue(F)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 856
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 857
    .local v0, "currentValue":I
    if-gez v0, :cond_1

    .line 858
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity$22;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;->freezeMinScene()V

    .line 859
    invoke-interface {p1}, Landroid/text/Editable;->clear()V

    .line 860
    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v2}, Landroid/text/Editable;->append(Ljava/lang/CharSequence;)Landroid/text/Editable;

    .line 861
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;->access$1100()Ljava/lang/String;

    move-result-object v2

    const-string v3, "RangeConstants.REALTIME_MIN_MIN"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 893
    .end local v0    # "currentValue":I
    :cond_0
    :goto_0
    return-void

    .line 848
    :catch_0
    move-exception v1

    .line 849
    .local v1, "e":Ljava/lang/NumberFormatException;
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity$22;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;->mMinNumberPicker:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/CustomNumberPicker;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;->access$1300(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;)Lcom/sec/android/app/shealth/plugins/exercisepro/utils/CustomNumberPicker;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/CustomNumberPicker;->setValue(F)V

    .line 851
    invoke-interface {p1}, Landroid/text/Editable;->clear()V

    .line 852
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity$22;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;->minutesEditText:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;->access$600(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;)Landroid/widget/EditText;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 853
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity$22;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;->minutesEditText:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;->access$600(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/widget/EditText;->sendAccessibilityEvent(I)V

    goto :goto_0

    .line 864
    .end local v1    # "e":Ljava/lang/NumberFormatException;
    .restart local v0    # "currentValue":I
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x2

    if-le v2, v3, :cond_2

    .line 865
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity$22;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;->freezeMinScene()V

    .line 867
    const/4 v2, 0x1

    invoke-interface {p1, v4, v2}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    goto :goto_0

    .line 870
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity$22;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;

    iget v2, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;->previousValue:I

    if-ne v2, v5, :cond_3

    .line 871
    if-le v0, v5, :cond_3

    .line 872
    invoke-interface {p1}, Landroid/text/Editable;->clear()V

    .line 873
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity$22;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "0"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    rem-int/lit8 v4, v0, 0xa

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    iput v3, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;->previousValue:I

    .line 874
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "0"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    rem-int/lit8 v3, v0, 0xa

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v2}, Landroid/text/Editable;->append(Ljava/lang/CharSequence;)Landroid/text/Editable;

    .line 875
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "0"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    rem-int/lit8 v3, v0, 0xa

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 878
    :cond_3
    if-le v0, v5, :cond_5

    .line 879
    invoke-interface {p1}, Landroid/text/Editable;->clear()V

    .line 880
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity$22;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;

    iput v5, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;->previousValue:I

    .line 881
    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v2}, Landroid/text/Editable;->append(Ljava/lang/CharSequence;)Landroid/text/Editable;

    .line 889
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity$22;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;->minutesEditText:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;->access$600(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;)Landroid/widget/EditText;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 890
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity$22;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;->minutesEditText:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;->access$600(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/widget/EditText;->sendAccessibilityEvent(I)V

    .line 892
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity$22;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;->checkGoalValue()V
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;->access$1200(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;)V

    goto/16 :goto_0

    .line 886
    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity$22;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;

    iput v0, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;->previousValue:I

    goto :goto_1
.end method
