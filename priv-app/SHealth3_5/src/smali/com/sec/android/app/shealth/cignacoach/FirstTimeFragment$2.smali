.class Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment$2;
.super Ljava/lang/Object;
.source "FirstTimeFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;)V
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 88
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 89
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_0

    .line 90
    instance-of v1, v0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;

    if-eqz v1, :cond_0

    .line 91
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;->mIsScorePresent:Z
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;->access$000(Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;)Z

    move-result v1

    if-eqz v1, :cond_2

    move-object v1, v0

    .line 92
    check-cast v1, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;

    check-cast v0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;

    .end local v0    # "activity":Landroid/app/Activity;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->getCignaSummaryFragment()Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;

    move-result-object v4

    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->isCoachBREnabled(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getCignaRestoreTriggerPopup(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_1

    :goto_0
    invoke-virtual {v1, v4, v3, v3, v2}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->showFragment(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;ZZZ)V

    .line 95
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;

    const v2, 0x7f090029

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/view/View;->announceForAccessibility(Ljava/lang/CharSequence;)V

    .line 96
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "com.sec.android.app.shealth.cignacoach"

    const-string v3, "0018"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    :cond_0
    return-void

    :cond_1
    move v2, v3

    .line 92
    goto :goto_0

    .line 94
    .restart local v0    # "activity":Landroid/app/Activity;
    :cond_2
    check-cast v0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;

    .end local v0    # "activity":Landroid/app/Activity;
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;

    move-result-object v1

    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->isCoachBREnabled(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getCignaRestoreTriggerPopup(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_3

    :goto_2
    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->showFirstTimeNoScoreFragment(Z)V

    goto :goto_1

    :cond_3
    move v2, v3

    goto :goto_2
.end method
