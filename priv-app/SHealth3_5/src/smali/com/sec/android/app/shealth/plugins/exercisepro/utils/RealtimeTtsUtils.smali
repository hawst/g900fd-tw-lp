.class public Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;
.super Landroid/speech/tts/UtteranceProgressListener;
.source "RealtimeTtsUtils.java"

# interfaces
.implements Landroid/speech/tts/TextToSpeech$OnInitListener;


# static fields
.field public static mAction:Ljava/lang/String;

.field private static mContext:Landroid/content/Context;

.field private static mHandler:Landroid/os/Handler;

.field private static mRealtimeTtsUtils:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;

.field public static mSyncObj:Ljava/lang/Object;

.field private static mTTS:Landroid/speech/tts/TextToSpeech;

.field private static mText:Ljava/lang/String;

.field private static mTextQueue:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static mUtterancesPlaying:I

.field private static object:Ljava/lang/Object;


# instance fields
.field private audioFocus:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field private mAm:Landroid/media/AudioManager;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 30
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->mTextQueue:Ljava/util/ArrayList;

    .line 33
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->mUtterancesPlaying:I

    .line 35
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->mHandler:Landroid/os/Handler;

    .line 37
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->object:Ljava/lang/Object;

    .line 164
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->mSyncObj:Ljava/lang/Object;

    .line 165
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->mAction:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 39
    invoke-direct {p0}, Landroid/speech/tts/UtteranceProgressListener;-><init>()V

    .line 184
    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils$4;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->audioFocus:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 40
    sput-object p1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->mContext:Landroid/content/Context;

    .line 41
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->mContext:Landroid/content/Context;

    const-string v2, "audio"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 42
    .local v0, "audioManager":Landroid/media/AudioManager;
    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->mAm:Landroid/media/AudioManager;

    .line 43
    return-void
.end method

.method static synthetic access$000()Landroid/content/Context;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->mText:Ljava/lang/String;

    return-object v0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 46
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->object:Ljava/lang/Object;

    monitor-enter v1

    .line 48
    :try_start_0
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->mRealtimeTtsUtils:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;

    if-nez v0, :cond_0

    .line 49
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->mRealtimeTtsUtils:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;

    .line 51
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->mRealtimeTtsUtils:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;

    monitor-exit v1

    return-object v0

    .line 52
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static play(Ljava/lang/String;)Z
    .locals 2
    .param p0, "text"    # Ljava/lang/String;

    .prologue
    .line 56
    sget v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->mUtterancesPlaying:I

    if-nez v0, :cond_0

    .line 57
    invoke-static {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->play2(Ljava/lang/String;)Z

    move-result v0

    .line 65
    :goto_0
    return v0

    .line 60
    :cond_0
    :goto_1
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->mTextQueue:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x2

    if-lt v0, v1, :cond_1

    .line 61
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->mTextQueue:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_1

    .line 63
    :cond_1
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->mTextQueue:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 65
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static play2(Ljava/lang/String;)Z
    .locals 3
    .param p0, "text"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 70
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->mRealtimeTtsUtils:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;

    if-nez v1, :cond_0

    .line 71
    const-string v1, "RealtimeTtsUtils"

    const-string/jumbo v2, "use after getInstance(context)"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    :goto_0
    return v0

    .line 75
    :cond_0
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealTimeAudioGuideItemIndex()I

    move-result v1

    if-eqz v1, :cond_1

    .line 76
    const-string v1, "RealtimeTtsUtils"

    const-string v2, "audio guide off"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils$1;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 86
    :cond_1
    new-instance v0, Landroid/speech/tts/TextToSpeech;

    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->mContext:Landroid/content/Context;

    sget-object v2, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->mRealtimeTtsUtils:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;

    invoke-direct {v0, v1, v2}, Landroid/speech/tts/TextToSpeech;-><init>(Landroid/content/Context;Landroid/speech/tts/TextToSpeech$OnInitListener;)V

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->mTTS:Landroid/speech/tts/TextToSpeech;

    .line 87
    sput-object p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->mText:Ljava/lang/String;

    .line 89
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private speak(Ljava/util/Locale;)Z
    .locals 8
    .param p1, "locale"    # Ljava/util/Locale;

    .prologue
    const/4 v7, 0x3

    const/4 v3, 0x1

    .line 93
    sget-object v4, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->mTTS:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v4, p1}, Landroid/speech/tts/TextToSpeech;->isLanguageAvailable(Ljava/util/Locale;)I

    move-result v0

    .line 94
    .local v0, "avail":I
    const-string v4, "RealtimeTtsUtils"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "avail["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    packed-switch v0, :pswitch_data_0

    .line 101
    const/4 v3, 0x0

    .line 128
    :cond_0
    :goto_0
    return v3

    .line 104
    :pswitch_0
    const-string v4, "RealtimeTtsUtils"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "playing["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->mText:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    sget-object v4, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->mTTS:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v4, p1}, Landroid/speech/tts/TextToSpeech;->setLanguage(Ljava/util/Locale;)I

    .line 107
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 108
    .local v1, "params":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string/jumbo v4, "streamType"

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    const-string/jumbo v4, "utteranceId"

    const-string v5, "ID"

    invoke-virtual {v1, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    sget-object v4, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->mTTS:Landroid/speech/tts/TextToSpeech;

    sget-object v5, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->mText:Ljava/lang/String;

    invoke-virtual {v4, v5, v3, v1}, Landroid/speech/tts/TextToSpeech;->speak(Ljava/lang/String;ILjava/util/HashMap;)I

    .line 117
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->mAm:Landroid/media/AudioManager;

    if-eqz v4, :cond_0

    .line 119
    sget v4, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->mUtterancesPlaying:I

    if-ge v4, v3, :cond_1

    .line 120
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->mAm:Landroid/media/AudioManager;

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->audioFocus:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v4, v5, v7, v7}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v2

    .line 122
    .local v2, "status":I
    if-nez v2, :cond_1

    .line 123
    const-string v4, "RealtimeTtsUtils"

    const-string/jumbo v5, "speak() audio focus request failed."

    invoke-static {v4, v5}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    .end local v2    # "status":I
    :cond_1
    sget v4, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->mUtterancesPlaying:I

    add-int/lit8 v4, v4, 0x1

    sput v4, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->mUtterancesPlaying:I

    goto :goto_0

    .line 95
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public onDone(Ljava/lang/String;)V
    .locals 5
    .param p1, "utteranceId"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 195
    const-string v1, "RealtimeTtsUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "onUtteranceCompleted(\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    sget v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->mUtterancesPlaying:I

    add-int/lit8 v1, v1, -0x1

    sput v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->mUtterancesPlaying:I

    .line 197
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->mAm:Landroid/media/AudioManager;

    if-nez v1, :cond_1

    .line 217
    :cond_0
    :goto_0
    return-void

    .line 200
    :cond_1
    sget v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->mUtterancesPlaying:I

    const/4 v2, 0x1

    if-ge v1, v2, :cond_0

    .line 201
    sput v4, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->mUtterancesPlaying:I

    .line 202
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->mTextQueue:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 203
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->mTextQueue:Ljava/util/ArrayList;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->play2(Ljava/lang/String;)Z

    goto :goto_0

    .line 206
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->mAm:Landroid/media/AudioManager;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->audioFocus:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 207
    sget-object v2, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->mSyncObj:Ljava/lang/Object;

    monitor-enter v2

    .line 208
    :try_start_0
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->mAction:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 209
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 210
    .local v0, "i":Landroid/content/Intent;
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->mAction:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 211
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 212
    const/4 v1, 0x0

    sput-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->mAction:Ljava/lang/String;

    .line 214
    .end local v0    # "i":Landroid/content/Intent;
    :cond_3
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public onError(Ljava/lang/String;)V
    .locals 0
    .param p1, "utteranceId"    # Ljava/lang/String;

    .prologue
    .line 220
    return-void
.end method

.method public onInit(I)V
    .locals 4
    .param p1, "i"    # I

    .prologue
    .line 134
    const-string v1, "RealtimeTtsUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "ttsStatus["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 135
    if-nez p1, :cond_0

    .line 136
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v0, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 137
    .local v0, "systemLocale":Ljava/util/Locale;
    const-string v1, "RealtimeTtsUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "systemLocale["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->speak(Ljava/util/Locale;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 140
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils$2;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 160
    .end local v0    # "systemLocale":Ljava/util/Locale;
    :cond_0
    :goto_0
    return-void

    .line 148
    .restart local v0    # "systemLocale":Ljava/util/Locale;
    :cond_1
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils$3;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils$3;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 157
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->mTTS:Landroid/speech/tts/TextToSpeech;

    sget-object v2, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->mRealtimeTtsUtils:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;

    invoke-virtual {v1, v2}, Landroid/speech/tts/TextToSpeech;->setOnUtteranceProgressListener(Landroid/speech/tts/UtteranceProgressListener;)I

    goto :goto_0
.end method

.method public onStart(Ljava/lang/String;)V
    .locals 0
    .param p1, "utteranceId"    # Ljava/lang/String;

    .prologue
    .line 223
    return-void
.end method
