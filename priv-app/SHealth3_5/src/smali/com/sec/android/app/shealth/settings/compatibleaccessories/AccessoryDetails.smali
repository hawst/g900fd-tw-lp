.class public Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;
.super Ljava/lang/Object;
.source "AccessoryDetails.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public connectivityStr:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "acsrConnType"
    .end annotation
.end field

.field public description:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "acsrDesc"
    .end annotation
.end field

.field public deviceType:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "acsrType"
    .end annotation
.end field

.field public imagePath:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "acsrImageUrl"
    .end annotation
.end field

.field public logoPath:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "acsrLogoUrl"
    .end annotation
.end field

.field public manufacturer:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "acsrMaker"
    .end annotation
.end field

.field public modelName:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "acsrModel"
    .end annotation
.end field

.field public serviceType:I

.field public syncStatus:Ljava/lang/String;

.field public website:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "acsrWeb"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;IILjava/lang/String;I)V
    .locals 0
    .param p1, "mDeviceType"    # I
    .param p2, "mModelName"    # Ljava/lang/String;
    .param p3, "mManufacturer"    # Ljava/lang/String;
    .param p4, "mLogoPath"    # I
    .param p5, "mImagePath"    # I
    .param p6, "mWebsite"    # Ljava/lang/String;
    .param p7, "mConnectivity"    # I

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    iput p1, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;->deviceType:I

    .line 71
    iput-object p2, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;->modelName:Ljava/lang/String;

    .line 72
    iput-object p3, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;->manufacturer:Ljava/lang/String;

    .line 73
    iput p4, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;->logoPath:I

    .line 74
    iput p5, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;->imagePath:I

    .line 75
    iput-object p6, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;->website:Ljava/lang/String;

    .line 76
    iput p7, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;->connectivityStr:I

    .line 77
    return-void
.end method


# virtual methods
.method public getConnectivity()I
    .locals 1

    .prologue
    .line 104
    iget v0, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;->connectivityStr:I

    return v0
.end method

.method public getDeviceType()I
    .locals 1

    .prologue
    .line 80
    iget v0, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;->deviceType:I

    return v0
.end method

.method public getImagePath()I
    .locals 1

    .prologue
    .line 96
    iget v0, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;->imagePath:I

    return v0
.end method

.method public getLogoPath()I
    .locals 1

    .prologue
    .line 92
    iget v0, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;->logoPath:I

    return v0
.end method

.method public getManufacturer()Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;->manufacturer:Ljava/lang/String;

    return-object v0
.end method

.method public getModelName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;->modelName:Ljava/lang/String;

    return-object v0
.end method

.method public getWebsite()Ljava/lang/String;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;->website:Ljava/lang/String;

    return-object v0
.end method
