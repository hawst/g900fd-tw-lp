.class Lcom/sec/android/app/shealth/settings/ManualActivity$1;
.super Ljava/lang/Object;
.source "ManualActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/ManualActivity;->getContentInitializationListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/ManualActivity;)V
    .locals 0

    .prologue
    .line 84
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$1;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onContentInitialization(Landroid/view/View;Landroid/app/Activity;Landroid/app/Dialog;Landroid/os/Bundle;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V
    .locals 7
    .param p1, "content"    # Landroid/view/View;
    .param p2, "parentActivity"    # Landroid/app/Activity;
    .param p3, "dialog"    # Landroid/app/Dialog;
    .param p4, "savedInstanceState"    # Landroid/os/Bundle;
    .param p5, "okButtonHandler"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x0

    .line 91
    const v3, 0x7f080040

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 92
    .local v2, "text":Landroid/widget/TextView;
    const v3, 0x7f080319

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .local v0, "btnDont":Landroid/view/View;
    move-object v1, v0

    .line 93
    check-cast v1, Landroid/widget/TextView;

    .line 96
    .local v1, "checkText":Landroid/widget/TextView;
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$1;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    const v4, 0x7f08031d

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    # setter for: Lcom/sec/android/app/shealth/settings/ManualActivity;->extraText:Landroid/view/View;
    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/settings/ManualActivity;->access$002(Lcom/sec/android/app/shealth/settings/ManualActivity;Landroid/view/View;)Landroid/view/View;

    .line 98
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$1;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ManualActivity;->popupMode:I
    invoke-static {v3}, Lcom/sec/android/app/shealth/settings/ManualActivity;->access$100(Lcom/sec/android/app/shealth/settings/ManualActivity;)I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 154
    :goto_0
    return-void

    .line 101
    :pswitch_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isChinaModel()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 102
    const v3, 0x7f090792

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 106
    :goto_1
    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 104
    :cond_0
    const v3, 0x7f090793

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    .line 109
    :pswitch_1
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isChinaModel()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 110
    const v3, 0x7f090795

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 114
    :goto_2
    const v3, 0x7f0907a5

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    .line 115
    invoke-virtual {v0, v5}, Landroid/view/View;->setSelected(Z)V

    .line 116
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$1;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    iget-object v3, v3, Lcom/sec/android/app/shealth/settings/ManualActivity;->onCheckBoxClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 112
    :cond_1
    const v3, 0x7f090796

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    goto :goto_2

    .line 119
    :pswitch_2
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isChinaModel()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 120
    const v3, 0x7f090797

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 124
    :goto_3
    const v3, 0x7f090799

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    .line 125
    invoke-virtual {v0, v5}, Landroid/view/View;->setSelected(Z)V

    .line 126
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$1;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    iget-object v3, v3, Lcom/sec/android/app/shealth/settings/ManualActivity;->onCheckBoxClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 122
    :cond_2
    const v3, 0x7f090798

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    goto :goto_3

    .line 129
    :pswitch_3
    const v3, 0x7f09079a

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 130
    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 133
    :pswitch_4
    const v3, 0x7f09079b

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 134
    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 138
    :pswitch_5
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isChinaModel()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 139
    const v3, 0x7f09079c

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 143
    :goto_4
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$1;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    const-string/jumbo v4, "selected"

    invoke-virtual {p4, v4, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    # setter for: Lcom/sec/android/app/shealth/settings/ManualActivity;->checkedforMobile:Z
    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/settings/ManualActivity;->access$202(Lcom/sec/android/app/shealth/settings/ManualActivity;Z)Z

    .line 144
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$1;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ManualActivity;->checkedforMobile:Z
    invoke-static {v3}, Lcom/sec/android/app/shealth/settings/ManualActivity;->access$200(Lcom/sec/android/app/shealth/settings/ManualActivity;)Z

    move-result v3

    invoke-virtual {v0, v3}, Landroid/view/View;->setSelected(Z)V

    .line 145
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$1;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    iget-object v3, v3, Lcom/sec/android/app/shealth/settings/ManualActivity;->onCheckBoxClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 141
    :cond_3
    const v3, 0x7f09079d

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    goto :goto_4

    .line 148
    :pswitch_6
    const v3, 0x7f09079e

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 149
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$1;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    const-string/jumbo v4, "selected"

    invoke-virtual {p4, v4, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    # setter for: Lcom/sec/android/app/shealth/settings/ManualActivity;->checkedforRoaming:Z
    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/settings/ManualActivity;->access$302(Lcom/sec/android/app/shealth/settings/ManualActivity;Z)Z

    .line 150
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$1;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ManualActivity;->checkedforRoaming:Z
    invoke-static {v3}, Lcom/sec/android/app/shealth/settings/ManualActivity;->access$300(Lcom/sec/android/app/shealth/settings/ManualActivity;)Z

    move-result v3

    invoke-virtual {v0, v3}, Landroid/view/View;->setSelected(Z)V

    .line 151
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$1;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    iget-object v3, v3, Lcom/sec/android/app/shealth/settings/ManualActivity;->onCheckBoxClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 98
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
