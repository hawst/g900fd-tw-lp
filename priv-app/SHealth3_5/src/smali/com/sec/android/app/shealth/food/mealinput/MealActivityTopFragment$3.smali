.class Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment$3;
.super Ljava/lang/Object;
.source "MealActivityTopFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/ListPopup$OnItemClickedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->showMealTypeSelectorPopup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;)V
    .locals 0

    .prologue
    .line 148
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment$3;->this$0:Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClicked(ILjava/lang/String;Landroid/widget/PopupWindow;)V
    .locals 4
    .param p1, "itemIndex"    # I
    .param p2, "itemContent"    # Ljava/lang/String;
    .param p3, "popupWindow"    # Landroid/widget/PopupWindow;

    .prologue
    .line 152
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment$3;->this$0:Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mListPopup:Lcom/sec/android/app/shealth/common/commonui/ListPopup;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->access$300(Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;)Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->dismiss()V

    .line 153
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment$3;->this$0:Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mMealTypeSelector:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->access$400(Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 154
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment$3;->this$0:Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mMealTypeSelectorLL:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->access$100(Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment$3;->this$0:Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mMealTypeSelector:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->access$400(Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment$3;->this$0:Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0901ec

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->setContentDescription(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment$3;->this$0:Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->TYPES_MAP:[I
    invoke-static {}, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->access$600()[I

    move-result-object v1

    aget v1, v1, p1

    # setter for: Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mMealType:I
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->access$502(Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;I)I

    .line 156
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment$3;->this$0:Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mListPopup:Lcom/sec/android/app/shealth/common/commonui/ListPopup;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->access$300(Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;)Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->setCurrentItem(I)V

    .line 157
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment$3;->this$0:Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->access$700(Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;)Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/shealth/food/app/UserActionLog;->TRY_TO_CHANGE_MEAL_TYPE:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;->logUserAction(Lcom/sec/android/app/shealth/food/app/UserActionLog;)V

    .line 158
    # getter for: Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->access$800()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "meal type chosen: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    return-void
.end method
