.class public abstract Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/PageSelector;
.super Landroid/widget/LinearLayout;
.source "PageSelector.java"


# static fields
.field private static final LOG_TAG:Ljava/lang/String;

.field public static final NOTHING_SELECTED:I = -0x1


# instance fields
.field private selectedPageIndex:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/PageSelector;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->getLogTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/PageSelector;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 41
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 31
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/PageSelector;->selectedPageIndex:I

    .line 42
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/PageSelector;->init()V

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 31
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/PageSelector;->selectedPageIndex:I

    .line 36
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/PageSelector;->init()V

    .line 37
    return-void
.end method

.method private addPageForced()V
    .locals 4

    .prologue
    .line 125
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/PageSelector;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 126
    .local v0, "pageIndicator":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/PageSelector;->getPageIndicatorImageResource()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 127
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/PageSelector;->getPageIndicatorWidth()I

    move-result v2

    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/PageSelector;->getPageIndicatorHeight()I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 128
    .local v1, "viewParams":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/PageSelector;->getPagesCount()I

    move-result v2

    if-lez v2, :cond_0

    .line 129
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/PageSelector;->getMarginBetweenIndicators()I

    move-result v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 131
    :cond_0
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/PageSelector;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 132
    return-void
.end method

.method private checkStateAfterAddingPages()V
    .locals 2

    .prologue
    .line 111
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/PageSelector;->getPagesCount()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 112
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/PageSelector;->selectPageForced(I)V

    .line 114
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/PageSelector;->checkVisibility()V

    .line 115
    return-void
.end method

.method private checkVisibility()V
    .locals 2

    .prologue
    .line 74
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/PageSelector;->getPagesCount()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/PageSelector;->setVisibility(I)V

    .line 75
    return-void

    .line 74
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getMarginBetweenIndicators()I
    .locals 1

    .prologue
    .line 158
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/PageSelector;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/PageSelector;->getMarginBetweenIndicators(Landroid/content/res/Resources;)I

    move-result v0

    return v0
.end method

.method private getPageIndicatorHeight()I
    .locals 1

    .prologue
    .line 154
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/PageSelector;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/PageSelector;->getPageIndicatorHeight(Landroid/content/res/Resources;)I

    move-result v0

    return v0
.end method

.method private getPageIndicatorWidth()I
    .locals 1

    .prologue
    .line 150
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/PageSelector;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/PageSelector;->getPageIndicatorWidth(Landroid/content/res/Resources;)I

    move-result v0

    return v0
.end method

.method private init()V
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/PageSelector;->setOrientation(I)V

    .line 47
    return-void
.end method

.method private selectPageForced(I)V
    .locals 2
    .param p1, "pageIndex"    # I

    .prologue
    .line 94
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/PageSelector;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 95
    iput p1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/PageSelector;->selectedPageIndex:I

    .line 96
    return-void
.end method


# virtual methods
.method public addPage()V
    .locals 2

    .prologue
    .line 105
    sget-object v0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/PageSelector;->LOG_TAG:Ljava/lang/String;

    const-string v1, "added page"

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/PageSelector;->addPageForced()V

    .line 107
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/PageSelector;->checkStateAfterAddingPages()V

    .line 108
    return-void
.end method

.method public addPages(I)V
    .locals 1
    .param p1, "count"    # I

    .prologue
    .line 118
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p1, :cond_0

    .line 119
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/PageSelector;->addPageForced()V

    .line 118
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 121
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/PageSelector;->checkStateAfterAddingPages()V

    .line 122
    return-void
.end method

.method public deletePage(I)V
    .locals 3
    .param p1, "pageIndex"    # I

    .prologue
    .line 140
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/PageSelector;->getPagesCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 141
    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/PageSelector;->getPagesCount()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 142
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "pageIndex should be >= 0 and < pages count, pageIndex = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " pages count: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/PageSelector;->getPagesCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 144
    :cond_1
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/PageSelector;->removeViewAt(I)V

    .line 145
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/PageSelector;->checkVisibility()V

    .line 147
    :cond_2
    return-void
.end method

.method public fill(II)V
    .locals 4
    .param p1, "pagesCount"    # I
    .param p2, "selectedPage"    # I

    .prologue
    .line 56
    if-gtz p1, :cond_0

    .line 57
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "pagesCount should be > 0, pagesCount = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 59
    :cond_0
    if-ltz p2, :cond_1

    if-lt p2, p1, :cond_2

    .line 60
    :cond_1
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "selectedPageIndex should be >= 0 and < pagesCount, selectedPageIndex = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 62
    :cond_2
    sget-object v1, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/PageSelector;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "added "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " page(s)"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/PageSelector;->getChildCount()I

    move-result v1

    if-lez v1, :cond_3

    .line 64
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/PageSelector;->removeAllViews()V

    .line 66
    :cond_3
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p1, :cond_4

    .line 67
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/PageSelector;->addPageForced()V

    .line 66
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 69
    :cond_4
    invoke-direct {p0, p2}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/PageSelector;->selectPageForced(I)V

    .line 70
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/PageSelector;->checkVisibility()V

    .line 71
    return-void
.end method

.method protected abstract getMarginBetweenIndicators(Landroid/content/res/Resources;)I
.end method

.method protected abstract getPageIndicatorHeight(Landroid/content/res/Resources;)I
.end method

.method protected abstract getPageIndicatorImageResource()I
.end method

.method protected abstract getPageIndicatorWidth(Landroid/content/res/Resources;)I
.end method

.method public getPagesCount()I
    .locals 1

    .prologue
    .line 100
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/PageSelector;->getChildCount()I

    move-result v0

    return v0
.end method

.method public selectPage(I)V
    .locals 3
    .param p1, "pageIndex"    # I

    .prologue
    .line 83
    sget-object v0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/PageSelector;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "selected page "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/PageSelector;->getPagesCount()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 85
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "pageIndex should be >= 0 and < pages count, pageIndex = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", pages count: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/PageSelector;->getPagesCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 87
    :cond_1
    iget v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/PageSelector;->selectedPageIndex:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_2

    iget v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/PageSelector;->selectedPageIndex:I

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/PageSelector;->getPagesCount()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 88
    iget v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/PageSelector;->selectedPageIndex:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/PageSelector;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 90
    :cond_2
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/PageSelector;->selectPageForced(I)V

    .line 91
    return-void
.end method
