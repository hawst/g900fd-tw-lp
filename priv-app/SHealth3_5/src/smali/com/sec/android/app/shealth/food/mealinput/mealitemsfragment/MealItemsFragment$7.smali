.class Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$7;
.super Lcom/sec/android/app/shealth/food/foodpick/loader/ExtraFoodInfoRequestTask;
.source "MealItemsFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->onPortionSizeBtnClick(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;

.field final synthetic val$mealItem:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

.field final synthetic val$mealItemView:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;Landroid/content/Context;Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;)V
    .locals 0
    .param p2, "x0"    # Landroid/content/Context;
    .param p3, "x1"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    .prologue
    .line 471
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$7;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;

    iput-object p4, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$7;->val$mealItem:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    iput-object p5, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$7;->val$mealItemView:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;

    invoke-direct {p0, p2, p3}, Lcom/sec/android/app/shealth/food/foodpick/loader/ExtraFoodInfoRequestTask;-><init>(Landroid/content/Context;Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;)V

    return-void
.end method


# virtual methods
.method public onTaskCompleted(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;)V
    .locals 5
    .param p1, "extraFoodInfoResult"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;

    .prologue
    .line 474
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/loader/ExtraFoodInfoRequestTask;->onTaskCompleted(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;)V

    .line 475
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;->getSearchResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;

    .line 476
    .local v0, "extraFoodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$7;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;

    const v3, 0x7f0900bf

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 477
    .local v1, "gramShortName":Ljava/lang/String;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getServingDescription()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getServingDescription()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$7;->val$mealItem:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->getUnit()I

    move-result v2

    const v3, 0x1d4c1

    if-ne v2, v3, :cond_0

    .line 487
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$7;->val$mealItem:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getDefaultNumber()F

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->setAmount(F)V

    .line 488
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$7;->val$mealItem:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    const v3, 0x1d4c2

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->setUnit(I)V

    .line 490
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$7;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$7;->val$mealItemView:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;

    iget-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$7;->val$mealItem:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    # invokes: Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->showPortionPopup(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;)V
    invoke-static {v2, v3, v4}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->access$900(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;)V

    .line 491
    return-void
.end method

.method public bridge synthetic onTaskCompleted(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;

    .prologue
    .line 471
    check-cast p1, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;

    .end local p1    # "x0":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$7;->onTaskCompleted(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;)V

    return-void
.end method
