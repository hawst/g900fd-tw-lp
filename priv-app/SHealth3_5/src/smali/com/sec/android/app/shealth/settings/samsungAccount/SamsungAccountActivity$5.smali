.class Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity$5;
.super Ljava/lang/Object;
.source "SamsungAccountActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;)V
    .locals 0

    .prologue
    .line 140
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity$5;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 146
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.settings"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 147
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "android.settings.ACCOUNT_SYNC_SETTINGS"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 148
    const-string v1, "account"

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity$5;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/common/utils/SamsungAccount;->getSamsungAccountObject(Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 150
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity$5;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->startActivity(Landroid/content/Intent;)V

    .line 151
    return-void
.end method
