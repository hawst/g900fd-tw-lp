.class Lcom/sec/android/app/shealth/settings/ResetDataActivity$3;
.super Ljava/lang/Object;
.source "ResetDataActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/ResetDataActivity;->resetPreloadedPluginData(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/ResetDataActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/ResetDataActivity;)V
    .locals 0

    .prologue
    .line 216
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/ResetDataActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/16 v3, 0x2719

    .line 219
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v0

    .line 220
    .local v0, "deviceType":I
    const-string v1, "PDEOCHECK"

    const-string v2, "delete WalkInfoExtended data"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 221
    if-ne v0, v3, :cond_0

    .line 222
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->unsetHealthyStep()V

    .line 223
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->unsetActiveTime()V

    .line 226
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/ResetDataActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->clearWalkInfoData(Landroid/content/Context;)I

    .line 227
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/ResetDataActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->clearWalkInfoExtendedData(Landroid/content/Context;)I

    .line 228
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/ResetDataActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->clearWalkingGoalData(Landroid/content/Context;)I

    .line 230
    if-ne v0, v3, :cond_1

    .line 231
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->setHealthyStep()V

    .line 232
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->setActiveTime()V

    .line 235
    :cond_1
    return-void
.end method
