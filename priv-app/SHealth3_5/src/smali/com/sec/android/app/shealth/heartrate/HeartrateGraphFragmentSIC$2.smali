.class Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC$2;
.super Ljava/lang/Object;
.source "HeartrateGraphFragmentSIC.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->initGraphLegendArea()Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;)V
    .locals 0

    .prologue
    .line 319
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC$2;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 331
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC$2;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/support/v4/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 332
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC$2;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;

    # invokes: Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->showSummarayFragment()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->access$100(Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 336
    :goto_0
    return-void

    .line 333
    :catch_0
    move-exception v0

    .line 334
    .local v0, "exception":Ljava/lang/IllegalStateException;
    const-string v1, "HeartrateGraphFragmentSIC"

    const-string/jumbo v2, "onClick Called while destroying Activity"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
