.class public Lcom/sec/android/app/shealth/walkingmate/widget/input/ProHorizontalInputModule;
.super Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;
.source "ProHorizontalInputModule.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/walkingmate/widget/input/ProHorizontalInputModule$OnHorizontalListItemClickListener;
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;-><init>(Landroid/content/Context;)V

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 33
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 34
    return-void
.end method


# virtual methods
.method protected getControllerView(Landroid/view/View;)Lcom/sec/android/app/shealth/walkingmate/widget/input/ControllerView;
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 47
    const v1, 0x7f080bbd

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/walkingmate/widget/input/ControllerView;

    .line 48
    .local v0, "controller":Lcom/sec/android/app/shealth/walkingmate/widget/input/ControllerView;
    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/widget/input/ControllerView$LayoutType;->HORIZONTAL:Lcom/sec/android/app/shealth/walkingmate/widget/input/ControllerView$LayoutType;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/widget/input/ControllerView;->setOrientation(Lcom/sec/android/app/shealth/walkingmate/widget/input/ControllerView$LayoutType;)V

    .line 49
    return-object v0
.end method

.method protected getRootLayoutId()I
    .locals 1

    .prologue
    .line 38
    const v0, 0x7f030299

    return v0
.end method

.method public getValueEditText()Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/ProHorizontalInputModule;->mValueEditText:Landroid/widget/EditText;

    return-object v0
.end method

.method protected initAdditionalViews(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 54
    return-void
.end method

.method public removeEditTextFocus()V
    .locals 0

    .prologue
    .line 57
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/widget/input/ProHorizontalInputModule;->moveFocusFromValueEditText()V

    .line 58
    return-void
.end method
