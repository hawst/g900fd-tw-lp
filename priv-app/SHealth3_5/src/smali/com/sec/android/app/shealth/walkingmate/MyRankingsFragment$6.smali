.class Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment$6;
.super Lcom/sec/android/app/shealth/walkingmate/loaders/MyRankingsUpdater;
.source "MyRankingsFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->requestUpdateData(Landroid/content/Intent;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;)V
    .locals 0

    .prologue
    .line 456
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment$6;->this$0:Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;

    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/loaders/MyRankingsUpdater;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 456
    check-cast p1, Ljava/lang/Void;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment$6;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 2
    .param p1, "aVoid"    # Ljava/lang/Void;

    .prologue
    .line 479
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/walkingmate/loaders/MyRankingsUpdater;->onPostExecute(Ljava/lang/Object;)V

    .line 480
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment$6;->this$0:Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->rankingUpdaterTask:Lcom/sec/android/app/shealth/walkingmate/loaders/MyRankingsUpdater;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->access$1602(Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;Lcom/sec/android/app/shealth/walkingmate/loaders/MyRankingsUpdater;)Lcom/sec/android/app/shealth/walkingmate/loaders/MyRankingsUpdater;

    .line 481
    return-void
.end method

.method public onUpdate(Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;)V
    .locals 3
    .param p1, "responseBasic"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;
    .param p2, "responseByAge"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;
    .param p3, "result"    # Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    .prologue
    .line 459
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment$6;->this$0:Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;

    # setter for: Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->mRequestResult:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;
    invoke-static {v0, p3}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->access$1202(Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;)Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    .line 460
    # getter for: Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->access$700()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "mRequestResult : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment$6;->this$0:Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->mRequestResult:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->access$1200(Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;)Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 462
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment$6;->this$0:Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;

    # setter for: Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->basicResponse:Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;
    invoke-static {v0, p1}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->access$1302(Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;)Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;

    .line 463
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment$6;->this$0:Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;

    # setter for: Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->byAgeResponse:Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;
    invoke-static {v0, p2}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->access$1402(Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;)Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;

    .line 464
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment$6;->this$0:Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->mRequestResult:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->access$1200(Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;)Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;->SUCCESS:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    if-ne v0, v1, :cond_1

    .line 465
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment$6;->this$0:Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;

    # invokes: Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->saveTheLatestReceivedData()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->access$1500(Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;)V

    .line 466
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment$6;->this$0:Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->updateHandler:Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->access$200(Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;)Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 467
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment$6;->this$0:Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->updateHandler:Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->access$200(Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;)Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;

    move-result-object v0

    const/16 v1, 0x17

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;->sendEmptyMessage(I)Z

    .line 475
    :cond_0
    :goto_0
    return-void

    .line 468
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment$6;->this$0:Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->mRequestResult:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->access$1200(Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;)Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;->INVALID_TOKEN:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 469
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment$6;->this$0:Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->updateHandler:Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->access$200(Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;)Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 470
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment$6;->this$0:Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->updateHandler:Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->access$200(Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;)Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 472
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment$6;->this$0:Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->updateHandler:Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->access$200(Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;)Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 473
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment$6;->this$0:Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->updateHandler:Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->access$200(Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;)Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;

    move-result-object v0

    const/16 v1, 0x21

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method
