.class Lcom/sec/android/app/shealth/common/commonui/dialog/MultipleChoicePopupAdapter$1;
.super Ljava/lang/Object;
.source "MultipleChoicePopupAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/common/commonui/dialog/MultipleChoicePopupAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/MultipleChoicePopupAdapter;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/common/commonui/dialog/MultipleChoicePopupAdapter;)V
    .locals 0

    .prologue
    .line 61
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/MultipleChoicePopupAdapter$1;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/MultipleChoicePopupAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 66
    sget v1, Lcom/sec/android/app/shealth/common/commonui/R$id;->multi_item_checkbox:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 68
    .local v0, "checkBox":Landroid/widget/CheckBox;
    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 70
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 74
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/MultipleChoicePopupAdapter$1;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/MultipleChoicePopupAdapter;

    iget-object v1, v1, Lcom/sec/android/app/shealth/common/commonui/dialog/MultipleChoicePopupAdapter;->onDialogListItemClick:Lcom/sec/android/app/shealth/common/commonui/dialog/OnDialogListItemClick;

    if-eqz v1, :cond_0

    .line 75
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/MultipleChoicePopupAdapter$1;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/MultipleChoicePopupAdapter;

    iget-object v1, v1, Lcom/sec/android/app/shealth/common/commonui/dialog/MultipleChoicePopupAdapter;->onDialogListItemClick:Lcom/sec/android/app/shealth/common/commonui/dialog/OnDialogListItemClick;

    invoke-interface {v1, p1}, Lcom/sec/android/app/shealth/common/commonui/dialog/OnDialogListItemClick;->onItemClick(Landroid/view/View;)V

    .line 76
    :cond_0
    return-void

    .line 72
    :cond_1
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0
.end method
