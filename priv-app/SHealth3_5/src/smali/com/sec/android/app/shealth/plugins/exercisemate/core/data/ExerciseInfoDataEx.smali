.class public Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;
.super Ljava/lang/Object;
.source "ExerciseInfoDataEx.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private EXERCISE_INFO__ID:I

.field private createTime:J

.field private delyn:I

.field private favorite:I

.field private id:J

.field private met:F

.field private name:Ljava/lang/String;

.field private pinyin:Ljava/lang/String;

.field private pinyinSort:Ljava/lang/String;

.field private source:I

.field private timeZone:I

.field private updateTime:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 187
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx$1;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const-wide/16 v0, -0x2

    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;->id:J

    .line 21
    iput v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;->EXERCISE_INFO__ID:I

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;->name:Ljava/lang/String;

    .line 25
    const/4 v0, 0x7

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;->favorite:I

    .line 26
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;->pinyin:Ljava/lang/String;

    .line 27
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;->pinyinSort:Ljava/lang/String;

    .line 29
    iput v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;->delyn:I

    .line 76
    return-void
.end method

.method public constructor <init>(JFLjava/lang/String;IILjava/lang/String;Ljava/lang/String;JJI)V
    .locals 4
    .param p1, "id"    # J
    .param p3, "met"    # F
    .param p4, "name"    # Ljava/lang/String;
    .param p5, "source"    # I
    .param p6, "favorite"    # I
    .param p7, "pinyin"    # Ljava/lang/String;
    .param p8, "pinyinSort"    # Ljava/lang/String;
    .param p9, "createTime"    # J
    .param p11, "updateTime"    # J
    .param p13, "timeZone"    # I

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const-wide/16 v1, -0x2

    iput-wide v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;->id:J

    .line 21
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;->EXERCISE_INFO__ID:I

    .line 23
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;->name:Ljava/lang/String;

    .line 25
    const/4 v1, 0x7

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;->favorite:I

    .line 26
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;->pinyin:Ljava/lang/String;

    .line 27
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;->pinyinSort:Ljava/lang/String;

    .line 29
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;->delyn:I

    .line 36
    const-wide/16 v1, -0x2

    cmp-long v1, p1, v1

    if-gez v1, :cond_0

    .line 38
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "id should not be "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 40
    :cond_0
    iput-wide p1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;->id:J

    .line 41
    iput p3, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;->met:F

    .line 42
    iput-object p4, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;->name:Ljava/lang/String;

    .line 43
    iput p5, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;->source:I

    .line 44
    iput p6, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;->favorite:I

    .line 45
    iput-object p7, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;->pinyin:Ljava/lang/String;

    .line 46
    iput-object p8, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;->pinyinSort:Ljava/lang/String;

    .line 47
    iput-wide p9, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;->createTime:J

    .line 48
    iput-wide p11, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;->updateTime:J

    .line 49
    move/from16 v0, p13

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;->timeZone:I

    .line 50
    return-void
.end method

.method public constructor <init>(JIFLjava/lang/String;IILjava/lang/String;Ljava/lang/String;JJI)V
    .locals 15
    .param p1, "id"    # J
    .param p3, "EXERCISE_INFO__ID"    # I
    .param p4, "met"    # F
    .param p5, "name"    # Ljava/lang/String;
    .param p6, "source"    # I
    .param p7, "favorite"    # I
    .param p8, "pinyin"    # Ljava/lang/String;
    .param p9, "pinyinSort"    # Ljava/lang/String;
    .param p10, "createTime"    # J
    .param p12, "updateTime"    # J
    .param p14, "timeZone"    # I

    .prologue
    .line 69
    move-object v1, p0

    move-wide/from16 v2, p1

    move/from16 v4, p4

    move-object/from16 v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-wide/from16 v10, p10

    move-wide/from16 v12, p12

    move/from16 v14, p14

    invoke-direct/range {v1 .. v14}, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;-><init>(JFLjava/lang/String;IILjava/lang/String;Ljava/lang/String;JJI)V

    .line 71
    move/from16 v0, p3

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;->setEXERCISE_INFO__ID(I)V

    .line 72
    return-void
.end method

.method public static clearExerciseData(Landroid/content/Context;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x0

    .line 292
    const/4 v0, 0x0

    .local v0, "deleted":I
    const/4 v1, 0x0

    .line 295
    .local v1, "total_deleted":I
    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$Exercise;->CONTENT_URI:Landroid/net/Uri;

    if-eqz v2, :cond_0

    .line 296
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract$Exercise;->CONTENT_URI:Landroid/net/Uri;

    const-string v4, "exercise_type != 20003"

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 300
    :cond_0
    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$Exercise;->CONTENT_URI:Landroid/net/Uri;

    if-eqz v2, :cond_1

    .line 302
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract$Exercise;->CONTENT_URI:Landroid/net/Uri;

    const-string v4, "exercise_type != 20003"

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 305
    :cond_1
    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$ExerciseInfo;->CONTENT_URI:Landroid/net/Uri;

    if-eqz v2, :cond_2

    .line 306
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract$ExerciseInfo;->CONTENT_URI:Landroid/net/Uri;

    const-string/jumbo v4, "source_type != 0 "

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 313
    :cond_2
    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$Goal;->CONTENT_URI:Landroid/net/Uri;

    if-eqz v2, :cond_3

    .line 314
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract$Goal;->CONTENT_URI:Landroid/net/Uri;

    const-string v4, "goal_type=40002"

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 315
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract$Goal;->CONTENT_URI:Landroid/net/Uri;

    const-string v4, "goal_type=40009"

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 316
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract$Goal;->CONTENT_URI:Landroid/net/Uri;

    const-string v4, "goal_type=40010"

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 317
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract$Goal;->CONTENT_URI:Landroid/net/Uri;

    const-string v4, "goal_type=40007"

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 318
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract$Goal;->CONTENT_URI:Landroid/net/Uri;

    const-string v4, "goal_type=40008"

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 320
    :cond_3
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 205
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 274
    move-object v0, p1

    check-cast v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;

    .line 275
    .local v0, "info":Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;
    if-nez v0, :cond_1

    .line 278
    :cond_0
    :goto_0
    return v1

    :cond_1
    iget-wide v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;->id:J

    iget-wide v4, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;->id:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getEXERCISE_INFO__ID()I
    .locals 1

    .prologue
    .line 267
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;->EXERCISE_INFO__ID:I

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 3

    .prologue
    .line 90
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->keySet()Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;->name:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 91
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;->name:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 93
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;->name:Ljava/lang/String;

    goto :goto_0
.end method

.method public getNameResID()I
    .locals 2

    .prologue
    .line 237
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->keySet()Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;->name:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 238
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 240
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getThumbnailID()I
    .locals 2

    .prologue
    .line 98
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->keySet()Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;->name:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 99
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 115
    :goto_0
    return v0

    .line 103
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;->name:Ljava/lang/String;

    const-string v1, "Archery, non-hunting "

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 104
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Archery, non-hunting "

    invoke-virtual {v0, v1}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    .line 105
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;->name:Ljava/lang/String;

    const-string v1, "Field hockey "

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 106
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Field hockey "

    invoke-virtual {v0, v1}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    .line 107
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;->name:Ljava/lang/String;

    const-string v1, "Horseback riding "

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 108
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Horseback riding "

    invoke-virtual {v0, v1}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    .line 109
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;->name:Ljava/lang/String;

    const-string v1, "Softball, general "

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 110
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    .line 111
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;->name:Ljava/lang/String;

    const-string v1, "Softball, practice"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 112
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Softball, practice\u00a0"

    invoke-virtual {v0, v1}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    .line 115
    :cond_5
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Walking"

    invoke-virtual {v0, v1}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto/16 :goto_0
.end method

.method public setCreateTime(J)V
    .locals 0
    .param p1, "time"    # J

    .prologue
    .line 248
    iput-wide p1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;->createTime:J

    .line 249
    return-void
.end method

.method public setEXERCISE_INFO__ID(I)V
    .locals 0
    .param p1, "eXERCISE_INFO__ID"    # I

    .prologue
    .line 270
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;->EXERCISE_INFO__ID:I

    .line 271
    return-void
.end method

.method public setFavorite(I)V
    .locals 0
    .param p1, "favorite"    # I

    .prologue
    .line 140
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;->favorite:I

    .line 141
    return-void
.end method

.method public setMet(F)V
    .locals 0
    .param p1, "met"    # F

    .prologue
    .line 85
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;->met:F

    .line 86
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 120
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;->name:Ljava/lang/String;

    .line 121
    return-void
.end method

.method public setPinyin(Ljava/lang/String;)V
    .locals 0
    .param p1, "pinyin"    # Ljava/lang/String;

    .prologue
    .line 150
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;->pinyin:Ljava/lang/String;

    .line 151
    return-void
.end method

.method public setPinyinSort(Ljava/lang/String;)V
    .locals 0
    .param p1, "pinyinSort"    # Ljava/lang/String;

    .prologue
    .line 160
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;->pinyinSort:Ljava/lang/String;

    .line 161
    return-void
.end method

.method public setRowId(J)V
    .locals 0
    .param p1, "id"    # J

    .prologue
    .line 164
    iput-wide p1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;->id:J

    .line 165
    return-void
.end method

.method public setSource(I)V
    .locals 0
    .param p1, "source"    # I

    .prologue
    .line 130
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;->source:I

    .line 131
    return-void
.end method

.method public setTimeZone(I)V
    .locals 0
    .param p1, "timezone"    # I

    .prologue
    .line 264
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;->timeZone:I

    .line 265
    return-void
.end method

.method public setUpdateTime(J)V
    .locals 0
    .param p1, "time"    # J

    .prologue
    .line 256
    iput-wide p1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;->updateTime:J

    .line 257
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "parcel"    # Landroid/os/Parcel;
    .param p2, "i"    # I

    .prologue
    .line 174
    iget-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;->id:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 175
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;->EXERCISE_INFO__ID:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 176
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;->met:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 177
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;->name:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 178
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;->source:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 179
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;->favorite:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 180
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;->pinyin:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 181
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;->pinyinSort:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 182
    iget-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;->createTime:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 183
    iget-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;->updateTime:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 184
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;->timeZone:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 185
    return-void
.end method
