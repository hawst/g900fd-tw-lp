.class public Lcom/sec/android/app/shealth/framework/repository/RepositoryInitializationTask;
.super Landroid/os/AsyncTask;
.source "RepositoryInitializationTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/framework/repository/RepositoryInitializationTask$IPostRepositoryInitializationTask;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Exception;",
        ">;"
    }
.end annotation


# static fields
.field public static final DATABASE_COPY_BUFFER:I = 0x1000

.field public static final SHAREDPREF_UPGRADE_FILE:Ljava/lang/String; = "upgrade_prefs"

.field private static final TAG:Ljava/lang/String;

.field public static final UPGRADE_CHECK_KEY:Ljava/lang/String; = "isUpgradeCheckRequired"

.field public static volatile mIsInitialized:Z

.field static mLock:Ljava/lang/Object;


# instance fields
.field private final NEW_VERSION:I

.field private final mContext:Landroid/content/Context;

.field private postRepositoryInitializationTask:Lcom/sec/android/app/shealth/framework/repository/RepositoryInitializationTask$IPostRepositoryInitializationTask;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 32
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "App_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lcom/sec/android/app/shealth/framework/repository/RepositoryInitializationTask;

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/repository/RepositoryInitializationTask;->TAG:Ljava/lang/String;

    .line 43
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/shealth/framework/repository/RepositoryInitializationTask;->mIsInitialized:Z

    .line 44
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/framework/repository/RepositoryInitializationTask;->mLock:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 52
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 36
    const/4 v0, 0x7

    iput v0, p0, Lcom/sec/android/app/shealth/framework/repository/RepositoryInitializationTask;->NEW_VERSION:I

    .line 53
    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/repository/RepositoryInitializationTask;->mContext:Landroid/content/Context;

    .line 54
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/shealth/framework/repository/RepositoryInitializationTask$IPostRepositoryInitializationTask;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "postRepositoryInitializationTask"    # Lcom/sec/android/app/shealth/framework/repository/RepositoryInitializationTask$IPostRepositoryInitializationTask;

    .prologue
    .line 57
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 36
    const/4 v0, 0x7

    iput v0, p0, Lcom/sec/android/app/shealth/framework/repository/RepositoryInitializationTask;->NEW_VERSION:I

    .line 58
    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/repository/RepositoryInitializationTask;->mContext:Landroid/content/Context;

    .line 59
    iput-object p2, p0, Lcom/sec/android/app/shealth/framework/repository/RepositoryInitializationTask;->postRepositoryInitializationTask:Lcom/sec/android/app/shealth/framework/repository/RepositoryInitializationTask$IPostRepositoryInitializationTask;

    .line 60
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Exception;
    .locals 5
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    const/4 v1, 0x0

    .line 65
    sget-object v2, Lcom/sec/android/app/shealth/framework/repository/RepositoryInitializationTask;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 69
    :try_start_0
    sget-boolean v3, Lcom/sec/android/app/shealth/framework/repository/RepositoryInitializationTask;->mIsInitialized:Z

    if-eqz v3, :cond_0

    .line 71
    sget-object v3, Lcom/sec/android/app/shealth/framework/repository/RepositoryInitializationTask;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "mIsInitialized : already initialized. concurrent call resulted in this"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 102
    :try_start_1
    sget-object v3, Lcom/sec/android/app/shealth/framework/repository/RepositoryInitializationTask;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "mIsInitialized : true"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    const/4 v3, 0x1

    sput-boolean v3, Lcom/sec/android/app/shealth/framework/repository/RepositoryInitializationTask;->mIsInitialized:Z

    .line 104
    sget-object v3, Lcom/sec/android/app/shealth/framework/repository/RepositoryInitializationTask;->mLock:Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v0, v1

    .line 108
    :goto_0
    return-object v0

    .line 75
    :cond_0
    :try_start_2
    sget-object v3, Lcom/sec/android/app/shealth/framework/repository/RepositoryInitializationTask;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "mIsInitialized : false"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    const/4 v3, 0x0

    sput-boolean v3, Lcom/sec/android/app/shealth/framework/repository/RepositoryInitializationTask;->mIsInitialized:Z

    .line 80
    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/DBManager;->getInstance()Lcom/sec/android/app/shealth/framework/repository/database/DBManager;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/framework/repository/RepositoryInitializationTask;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/framework/repository/database/DBManager;->initialize(Landroid/content/Context;)V

    .line 81
    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/configuration/SharedPrefManager;->getInstance()Lcom/sec/android/app/shealth/framework/repository/configuration/SharedPrefManager;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/framework/repository/RepositoryInitializationTask;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/framework/repository/configuration/SharedPrefManager;->initialize(Landroid/content/Context;)V

    .line 82
    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/file/FileManager;->getInstance()Lcom/sec/android/app/shealth/framework/repository/file/FileManager;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/framework/repository/RepositoryInitializationTask;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/framework/repository/file/FileManager;->initialize(Landroid/content/Context;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 85
    :try_start_3
    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/DBManager;->getInstance()Lcom/sec/android/app/shealth/framework/repository/database/DBManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/framework/repository/database/DBManager;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    .line 86
    const-string v3, "-----SIC-----"

    const-string v4, "initiliased the repository"

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 102
    :goto_1
    :try_start_4
    sget-object v3, Lcom/sec/android/app/shealth/framework/repository/RepositoryInitializationTask;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "mIsInitialized : true"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    const/4 v3, 0x1

    sput-boolean v3, Lcom/sec/android/app/shealth/framework/repository/RepositoryInitializationTask;->mIsInitialized:Z

    .line 104
    sget-object v3, Lcom/sec/android/app/shealth/framework/repository/RepositoryInitializationTask;->mLock:Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/lang/Object;->notifyAll()V

    .line 106
    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-object v0, v1

    .line 108
    goto :goto_0

    .line 88
    :catch_0
    move-exception v0

    .line 90
    .local v0, "e":Ljava/lang/Exception;
    :try_start_5
    const-string v3, "-----SIC-----"

    const-string v4, "DB not initiliased Exception"

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_1

    .line 94
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    .line 96
    .restart local v0    # "e":Ljava/lang/Exception;
    :try_start_6
    sget-object v1, Lcom/sec/android/app/shealth/framework/repository/RepositoryInitializationTask;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception Occurred during initialization : true"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 102
    :try_start_7
    sget-object v1, Lcom/sec/android/app/shealth/framework/repository/RepositoryInitializationTask;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "mIsInitialized : true"

    invoke-static {v1, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    const/4 v1, 0x1

    sput-boolean v1, Lcom/sec/android/app/shealth/framework/repository/RepositoryInitializationTask;->mIsInitialized:Z

    .line 104
    sget-object v1, Lcom/sec/android/app/shealth/framework/repository/RepositoryInitializationTask;->mLock:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v2

    goto/16 :goto_0

    .line 106
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    throw v1

    .line 102
    :catchall_1
    move-exception v1

    :try_start_8
    sget-object v3, Lcom/sec/android/app/shealth/framework/repository/RepositoryInitializationTask;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "mIsInitialized : true"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    const/4 v3, 0x1

    sput-boolean v3, Lcom/sec/android/app/shealth/framework/repository/RepositoryInitializationTask;->mIsInitialized:Z

    .line 104
    sget-object v3, Lcom/sec/android/app/shealth/framework/repository/RepositoryInitializationTask;->mLock:Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/lang/Object;->notifyAll()V

    throw v1
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 30
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/framework/repository/RepositoryInitializationTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Exception;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Exception;)V
    .locals 1
    .param p1, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 114
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/repository/RepositoryInitializationTask;->postRepositoryInitializationTask:Lcom/sec/android/app/shealth/framework/repository/RepositoryInitializationTask$IPostRepositoryInitializationTask;

    if-eqz v0, :cond_0

    .line 115
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/repository/RepositoryInitializationTask;->postRepositoryInitializationTask:Lcom/sec/android/app/shealth/framework/repository/RepositoryInitializationTask$IPostRepositoryInitializationTask;

    invoke-interface {v0}, Lcom/sec/android/app/shealth/framework/repository/RepositoryInitializationTask$IPostRepositoryInitializationTask;->postInitialized()V

    .line 118
    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 30
    check-cast p1, Ljava/lang/Exception;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/framework/repository/RepositoryInitializationTask;->onPostExecute(Ljava/lang/Exception;)V

    return-void
.end method
