.class Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WrapperScanListener;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ScanListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "WrapperScanListener"
.end annotation


# instance fields
.field private mScanningFrScanListener:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$ScanningFragmentScanListener;

.field private requiredDeviceTypes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$ScanningFragmentScanListener;Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$ScanningFragmentScanListener;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WrapperScanListener;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WrapperScanListener;->mScanningFrScanListener:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$ScanningFragmentScanListener;

    iput-object p3, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WrapperScanListener;->requiredDeviceTypes:Ljava/util/ArrayList;

    return-void
.end method

.method public static hJ0()Ljava/lang/String;
    .locals 67

    const/16 v0, 0x40

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x4

    const/4 v6, 0x5

    const/4 v7, 0x6

    const/4 v8, 0x7

    const/16 v9, 0x8

    const/16 v10, 0x9

    const/16 v11, 0xa

    const/16 v12, 0xb

    const/16 v13, 0xc

    const/16 v14, 0xd

    const/16 v15, 0xe

    const/16 v16, 0xf

    const/16 v17, 0x10

    const/16 v18, 0x11

    const/16 v19, 0x12

    const/16 v20, 0x13

    const/16 v21, 0x14

    const/16 v22, 0x15

    const/16 v23, 0x16

    const/16 v24, 0x17

    const/16 v25, 0x18

    const/16 v26, 0x19

    const/16 v27, 0x1a

    const/16 v28, 0x1b

    const/16 v29, 0x1c

    const/16 v30, 0x1d

    const/16 v31, 0x1e

    const/16 v32, 0x1f

    const/16 v33, 0x20

    const/16 v34, 0x21

    const/16 v35, 0x22

    const/16 v36, 0x23

    const/16 v37, 0x24

    const/16 v38, 0x25

    const/16 v39, 0x26

    const/16 v40, 0x27

    const/16 v41, 0x28

    const/16 v42, 0x29

    const/16 v43, 0x2a

    const/16 v44, 0x2b

    const/16 v45, 0x2c

    const/16 v46, 0x2d

    const/16 v47, 0x2e

    const/16 v48, 0x2f

    const/16 v49, 0x30

    const/16 v50, 0x31

    const/16 v51, 0x32

    const/16 v52, 0x33

    const/16 v53, 0x34

    const/16 v54, 0x35

    const/16 v55, 0x36

    const/16 v56, 0x37

    const/16 v57, 0x38

    const/16 v58, 0x39

    const/16 v59, 0x3a

    const/16 v60, 0x3b

    const/16 v61, 0x3c

    const/16 v62, 0x3d

    const/16 v63, 0x3e

    const/16 v64, 0x3f

    const/16 v65, -0x33c2

    aput v65, v1, v64

    const/16 v64, -0x68

    aput v64, v1, v63

    const/16 v63, -0x41

    aput v63, v1, v62

    const/16 v62, -0x2897

    aput v62, v1, v61

    const/16 v61, -0x62

    aput v61, v1, v60

    const/16 v60, -0x44

    aput v60, v1, v59

    const/16 v59, -0x34f7

    aput v59, v1, v58

    const/16 v58, -0x76

    aput v58, v1, v57

    const/16 v57, -0x5

    aput v57, v1, v56

    const/16 v56, -0x51

    aput v56, v1, v55

    const/16 v55, -0x1aad

    aput v55, v1, v54

    const/16 v54, -0x5c

    aput v54, v1, v53

    const/16 v53, -0x71cd

    aput v53, v1, v52

    const/16 v52, -0x2f

    aput v52, v1, v51

    const/16 v51, -0x78

    aput v51, v1, v50

    const/16 v50, 0x5419

    aput v50, v1, v49

    const/16 v49, 0x7b01

    aput v49, v1, v48

    const/16 v48, 0x5428

    aput v48, v1, v47

    const/16 v47, 0x6f07

    aput v47, v1, v46

    const/16 v46, -0x3dd6

    aput v46, v1, v45

    const/16 v45, -0x70

    aput v45, v1, v44

    const/16 v44, -0x6a

    aput v44, v1, v43

    const/16 v43, 0x597c

    aput v43, v1, v42

    const/16 v42, -0x37ea

    aput v42, v1, v41

    const/16 v41, -0x79

    aput v41, v1, v40

    const/16 v40, -0x78

    aput v40, v1, v39

    const/16 v39, -0x7

    aput v39, v1, v38

    const/16 v38, -0x31

    aput v38, v1, v37

    const/16 v37, 0x3c23

    aput v37, v1, v36

    const/16 v36, -0xfb2

    aput v36, v1, v35

    const/16 v35, -0x7b

    aput v35, v1, v34

    const/16 v34, 0x2f51

    aput v34, v1, v33

    const/16 v33, -0x33a4

    aput v33, v1, v32

    const/16 v32, -0x57

    aput v32, v1, v31

    const/16 v31, -0x2a

    aput v31, v1, v30

    const/16 v30, -0x1cb1

    aput v30, v1, v29

    const/16 v29, -0x79

    aput v29, v1, v28

    const/16 v28, -0xed8

    aput v28, v1, v27

    const/16 v27, -0x62

    aput v27, v1, v26

    const/16 v26, 0x182a

    aput v26, v1, v25

    const/16 v25, -0x3586

    aput v25, v1, v24

    const/16 v24, -0x1c

    aput v24, v1, v23

    const/16 v23, -0x66ef

    aput v23, v1, v22

    const/16 v22, -0xa

    aput v22, v1, v21

    const/16 v21, -0x5a

    aput v21, v1, v20

    const/16 v20, -0x79

    aput v20, v1, v19

    const/16 v19, -0x9

    aput v19, v1, v18

    const/16 v18, 0x3557

    aput v18, v1, v17

    const/16 v17, -0x1ae5

    aput v17, v1, v16

    const/16 v16, -0x73

    aput v16, v1, v15

    const/16 v15, -0x2f

    aput v15, v1, v14

    const/4 v14, -0x8

    aput v14, v1, v13

    const/16 v13, -0x78

    aput v13, v1, v12

    const/16 v12, -0x64

    aput v12, v1, v11

    const/16 v11, -0x28

    aput v11, v1, v10

    const/16 v10, -0x19e2

    aput v10, v1, v9

    const/16 v9, -0x38

    aput v9, v1, v8

    const/16 v8, 0x3b4c

    aput v8, v1, v7

    const/16 v7, -0x56a2

    aput v7, v1, v6

    const/16 v6, -0x26

    aput v6, v1, v5

    const/16 v5, 0x540

    aput v5, v1, v4

    const/16 v4, 0x5768

    aput v4, v1, v3

    const/16 v3, 0x1038

    aput v3, v1, v2

    const/16 v2, 0x3573

    aput v2, v1, v0

    const/16 v0, 0x40

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, 0x10

    const/16 v19, 0x11

    const/16 v20, 0x12

    const/16 v21, 0x13

    const/16 v22, 0x14

    const/16 v23, 0x15

    const/16 v24, 0x16

    const/16 v25, 0x17

    const/16 v26, 0x18

    const/16 v27, 0x19

    const/16 v28, 0x1a

    const/16 v29, 0x1b

    const/16 v30, 0x1c

    const/16 v31, 0x1d

    const/16 v32, 0x1e

    const/16 v33, 0x1f

    const/16 v34, 0x20

    const/16 v35, 0x21

    const/16 v36, 0x22

    const/16 v37, 0x23

    const/16 v38, 0x24

    const/16 v39, 0x25

    const/16 v40, 0x26

    const/16 v41, 0x27

    const/16 v42, 0x28

    const/16 v43, 0x29

    const/16 v44, 0x2a

    const/16 v45, 0x2b

    const/16 v46, 0x2c

    const/16 v47, 0x2d

    const/16 v48, 0x2e

    const/16 v49, 0x2f

    const/16 v50, 0x30

    const/16 v51, 0x31

    const/16 v52, 0x32

    const/16 v53, 0x33

    const/16 v54, 0x34

    const/16 v55, 0x35

    const/16 v56, 0x36

    const/16 v57, 0x37

    const/16 v58, 0x38

    const/16 v59, 0x39

    const/16 v60, 0x3a

    const/16 v61, 0x3b

    const/16 v62, 0x3c

    const/16 v63, 0x3d

    const/16 v64, 0x3e

    const/16 v65, 0x3f

    const/16 v66, -0x3399

    aput v66, v0, v65

    const/16 v65, -0x34

    aput v65, v0, v64

    const/16 v64, -0xa

    aput v64, v0, v63

    const/16 v63, -0x28c1

    aput v63, v0, v62

    const/16 v62, -0x29

    aput v62, v0, v61

    const/16 v61, -0x18

    aput v61, v0, v60

    const/16 v60, -0x34b6

    aput v60, v0, v59

    const/16 v59, -0x35

    aput v59, v0, v58

    const/16 v58, -0x5c

    aput v58, v0, v57

    const/16 v57, -0x1f

    aput v57, v0, v56

    const/16 v56, -0x1ae6

    aput v56, v0, v55

    const/16 v55, -0x1b

    aput v55, v0, v54

    const/16 v54, -0x7182

    aput v54, v0, v53

    const/16 v53, -0x72

    aput v53, v0, v52

    const/16 v52, -0x33

    aput v52, v0, v51

    const/16 v51, 0x544b

    aput v51, v0, v50

    const/16 v50, 0x7b54

    aput v50, v0, v49

    const/16 v49, 0x547b

    aput v49, v0, v48

    const/16 v48, 0x6f54

    aput v48, v0, v47

    const/16 v47, -0x3d91

    aput v47, v0, v46

    const/16 v46, -0x3e

    aput v46, v0, v45

    const/16 v45, -0x3a

    aput v45, v0, v44

    const/16 v44, 0x5938

    aput v44, v0, v43

    const/16 v43, -0x37a7

    aput v43, v0, v42

    const/16 v42, -0x38

    aput v42, v0, v41

    const/16 v41, -0x3c

    aput v41, v0, v40

    const/16 v40, -0x45

    aput v40, v0, v39

    const/16 v39, -0x1f

    aput v39, v0, v38

    const/16 v38, 0x3c46

    aput v38, v0, v37

    const/16 v37, -0xfc4

    aput v37, v0, v36

    const/16 v36, -0x10

    aput v36, v0, v35

    const/16 v35, 0x2f22

    aput v35, v0, v34

    const/16 v34, -0x33d1

    aput v34, v0, v33

    const/16 v33, -0x34

    aput v33, v0, v32

    const/16 v32, -0x5c

    aput v32, v0, v31

    const/16 v31, -0x1cc1

    aput v31, v0, v30

    const/16 v30, -0x1d

    aput v30, v0, v29

    const/16 v29, -0xeb9

    aput v29, v0, v28

    const/16 v28, -0xf

    aput v28, v0, v27

    const/16 v27, 0x1846

    aput v27, v0, v26

    const/16 v26, -0x35e8

    aput v26, v0, v25

    const/16 v25, -0x36

    aput v25, v0, v24

    const/16 v24, -0x6681

    aput v24, v0, v23

    const/16 v23, -0x67

    aput v23, v0, v22

    const/16 v22, -0x31

    aput v22, v0, v21

    const/16 v21, -0xd

    aput v21, v0, v20

    const/16 v20, -0x6c

    aput v20, v0, v19

    const/16 v19, 0x3536

    aput v19, v0, v18

    const/16 v18, -0x1acb

    aput v18, v0, v17

    const/16 v17, -0x1b

    aput v17, v0, v16

    const/16 v16, -0x5b

    aput v16, v0, v15

    const/16 v15, -0x6c

    aput v15, v0, v14

    const/16 v14, -0x17

    aput v14, v0, v13

    const/4 v13, -0x7

    aput v13, v0, v12

    const/16 v12, -0x50

    aput v12, v0, v11

    const/16 v11, -0x1993

    aput v11, v0, v10

    const/16 v10, -0x1a

    aput v10, v0, v9

    const/16 v9, 0x3b2f

    aput v9, v0, v8

    const/16 v8, -0x56c5

    aput v8, v0, v7

    const/16 v7, -0x57

    aput v7, v0, v6

    const/16 v6, 0x56e

    aput v6, v0, v5

    const/16 v5, 0x5705

    aput v5, v0, v4

    const/16 v4, 0x1057

    aput v4, v0, v3

    const/16 v3, 0x3510

    aput v3, v0, v2

    const/4 v2, 0x0

    :goto_0
    array-length v3, v0

    if-lt v2, v3, :cond_0

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_1
    array-length v3, v0

    if-lt v2, v3, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    aget v3, v0, v2

    aget v4, v1, v2

    xor-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    aget v3, v1, v2

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method


# virtual methods
.method public onDeviceFound(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)V
    .locals 42

    const/4 v1, 0x2

    new-array v4, v1, [J

    const/4 v1, 0x1

    const-wide/16 v2, 0x2

    aput-wide v2, v4, v1

    const/16 v1, 0x1a

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, -0xa3

    aput v29, v2, v28

    const/16 v28, -0x66

    aput v28, v2, v27

    const/16 v27, 0x3062

    aput v27, v2, v26

    const/16 v26, -0x36a4

    aput v26, v2, v25

    const/16 v25, -0x5a

    aput v25, v2, v24

    const/16 v24, -0x3b81

    aput v24, v2, v23

    const/16 v23, -0x50

    aput v23, v2, v22

    const/16 v22, 0x2f53

    aput v22, v2, v21

    const/16 v21, 0x2440

    aput v21, v2, v20

    const/16 v20, 0x5d67

    aput v20, v2, v19

    const/16 v19, 0x3129

    aput v19, v2, v18

    const/16 v18, -0x4ca1

    aput v18, v2, v17

    const/16 v17, -0x2a

    aput v17, v2, v16

    const/16 v16, -0x6cf5

    aput v16, v2, v15

    const/16 v15, -0xc

    aput v15, v2, v14

    const/16 v14, -0x6c9c

    aput v14, v2, v13

    const/16 v13, -0x1f

    aput v13, v2, v12

    const/16 v12, -0x26a9

    aput v12, v2, v11

    const/16 v11, -0x42

    aput v11, v2, v10

    const/16 v10, -0x2ec4

    aput v10, v2, v9

    const/16 v9, -0x48

    aput v9, v2, v8

    const/16 v8, -0x3ce5

    aput v8, v2, v7

    const/16 v7, -0x53

    aput v7, v2, v6

    const/16 v6, 0x4c4b

    aput v6, v2, v5

    const/16 v5, -0x77d1

    aput v5, v2, v3

    const/16 v3, -0x25

    aput v3, v2, v1

    const/16 v1, 0x1a

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, -0xd1

    aput v30, v1, v29

    const/16 v29, -0x1

    aput v29, v1, v28

    const/16 v28, 0x300e

    aput v28, v1, v27

    const/16 v27, -0x36d0

    aput v27, v1, v26

    const/16 v26, -0x37

    aput v26, v1, v25

    const/16 v25, -0x3bf3

    aput v25, v1, v24

    const/16 v24, -0x3c

    aput v24, v1, v23

    const/16 v23, 0x2f3d

    aput v23, v1, v22

    const/16 v22, 0x242f

    aput v22, v1, v21

    const/16 v21, 0x5d24

    aput v21, v1, v20

    const/16 v20, 0x315d

    aput v20, v1, v19

    const/16 v19, -0x4ccf

    aput v19, v1, v18

    const/16 v18, -0x4d

    aput v18, v1, v17

    const/16 v17, -0x6c9a

    aput v17, v1, v16

    const/16 v16, -0x6d

    aput v16, v1, v15

    const/16 v15, -0x6cfb

    aput v15, v1, v14

    const/16 v14, -0x6d

    aput v14, v1, v13

    const/16 v13, -0x26ef

    aput v13, v1, v12

    const/16 v12, -0x27

    aput v12, v1, v11

    const/16 v11, -0x2eae

    aput v11, v1, v10

    const/16 v10, -0x2f

    aput v10, v1, v9

    const/16 v9, -0x3c8b

    aput v9, v1, v8

    const/16 v8, -0x3d

    aput v8, v1, v7

    const/16 v7, 0x4c2a

    aput v7, v1, v6

    const/16 v6, -0x77b4

    aput v6, v1, v5

    const/16 v5, -0x78

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v5, v1

    if-lt v3, v5, :cond_0

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v5, v1

    if-lt v3, v5, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x10

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, -0x30

    aput v21, v2, v20

    const/16 v20, -0x58

    aput v20, v2, v19

    const/16 v19, 0x2d53

    aput v19, v2, v18

    const/16 v18, -0x7fbc

    aput v18, v2, v17

    const/16 v17, -0xa

    aput v17, v2, v16

    const/16 v16, -0x16

    aput v16, v2, v15

    const/16 v15, -0x73eb

    aput v15, v2, v14

    const/16 v14, -0x54

    aput v14, v2, v13

    const/16 v13, -0x2a

    aput v13, v2, v12

    const/16 v12, -0x1e

    aput v12, v2, v11

    const/16 v11, -0x18

    aput v11, v2, v10

    const/16 v10, 0x1c1e

    aput v10, v2, v9

    const/16 v9, -0x7994

    aput v9, v2, v8

    const/16 v8, -0xb

    aput v8, v2, v7

    const/4 v7, -0x3

    aput v7, v2, v3

    const/16 v3, -0x72

    aput v3, v2, v1

    const/16 v1, 0x10

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/4 v12, 0x6

    const/4 v13, 0x7

    const/16 v14, 0x8

    const/16 v15, 0x9

    const/16 v16, 0xa

    const/16 v17, 0xb

    const/16 v18, 0xc

    const/16 v19, 0xd

    const/16 v20, 0xe

    const/16 v21, 0xf

    const/16 v22, -0x10

    aput v22, v1, v21

    const/16 v21, -0x33

    aput v21, v1, v20

    const/16 v20, 0x2d30

    aput v20, v1, v19

    const/16 v19, -0x7fd3

    aput v19, v1, v18

    const/16 v18, -0x80

    aput v18, v1, v17

    const/16 v17, -0x71

    aput v17, v1, v16

    const/16 v16, -0x738f

    aput v16, v1, v15

    const/16 v15, -0x74

    aput v15, v1, v14

    const/16 v14, -0x4d

    aput v14, v1, v13

    const/16 v13, -0x6f

    aput v13, v1, v12

    const/16 v12, -0x7a

    aput v12, v1, v11

    const/16 v11, 0x1c71

    aput v11, v1, v10

    const/16 v10, -0x79e4

    aput v10, v1, v9

    const/16 v9, -0x7a

    aput v9, v1, v8

    const/16 v8, -0x68

    aput v8, v1, v7

    const/16 v7, -0x24

    aput v7, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v7, v1

    if-lt v3, v7, :cond_2

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v7, v1

    if-lt v3, v7, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v5, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getConnectivityType()I

    move-result v1

    const/4 v2, 0x0

    array-length v3, v4

    add-int/lit8 v3, v3, -0x1

    aget-wide v5, v4, v3

    long-to-int v3, v5

    if-gtz v3, :cond_4

    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_1
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_2
    aget v7, v1, v3

    aget v8, v2, v3

    xor-int/2addr v7, v8

    aput v7, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_3
    aget v7, v2, v3

    int-to-char v7, v7

    aput-char v7, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_4
    const/4 v3, 0x0

    int-to-long v1, v1

    const/16 v5, 0x20

    shl-long/2addr v1, v5

    const/16 v5, 0x20

    ushr-long v5, v1, v5

    aget-wide v1, v4, v3

    const-wide/16 v7, 0x0

    cmp-long v7, v1, v7

    if-eqz v7, :cond_5

    const-wide v7, -0x442d7f5971a4416bL    # -1.5672262211179677E-20

    xor-long/2addr v1, v7

    :cond_5
    const/16 v7, 0x20

    ushr-long/2addr v1, v7

    const/16 v7, 0x20

    shl-long/2addr v1, v7

    xor-long/2addr v1, v5

    const-wide v5, -0x442d7f5971a4416bL    # -1.5672262211179677E-20

    xor-long/2addr v1, v5

    aput-wide v1, v4, v3

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getType()I

    move-result v1

    const/4 v2, 0x1

    array-length v3, v4

    add-int/lit8 v3, v3, -0x1

    aget-wide v5, v4, v3

    long-to-int v3, v5

    if-lt v2, v3, :cond_6

    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_6
    const/4 v3, 0x0

    int-to-long v1, v1

    const/16 v5, 0x20

    shl-long v5, v1, v5

    aget-wide v1, v4, v3

    const-wide/16 v7, 0x0

    cmp-long v7, v1, v7

    if-eqz v7, :cond_7

    const-wide v7, -0x442d7f5971a4416bL    # -1.5672262211179677E-20

    xor-long/2addr v1, v7

    :cond_7
    const/16 v7, 0x20

    shl-long/2addr v1, v7

    const/16 v7, 0x20

    ushr-long/2addr v1, v7

    xor-long/2addr v1, v5

    const-wide v5, -0x442d7f5971a4416bL    # -1.5672262211179677E-20

    xor-long/2addr v1, v5

    aput-wide v1, v4, v3

    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    const/4 v2, 0x1

    if-gt v1, v2, :cond_a

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, 0x163b

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, 0x160a

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_4
    array-length v5, v1

    if-lt v3, v5, :cond_8

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_5
    array-length v5, v1

    if-lt v3, v5, :cond_9

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_8
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    :cond_9
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    :cond_a
    const/4 v1, 0x0

    aget-wide v1, v4, v1

    const-wide/16 v5, 0x0

    cmp-long v3, v1, v5

    if-eqz v3, :cond_b

    const-wide v5, -0x442d7f5971a4416bL    # -1.5672262211179677E-20

    xor-long/2addr v1, v5

    :cond_b
    const/16 v3, 0x20

    shr-long/2addr v1, v3

    long-to-int v1, v1

    const/16 v2, 0x2712

    if-ne v1, v2, :cond_11

    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    if-gtz v1, :cond_e

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, 0x215f

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, 0x216f

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_6
    array-length v5, v1

    if-lt v3, v5, :cond_c

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_7
    array-length v5, v1

    if-lt v3, v5, :cond_d

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_c
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    :cond_d
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_7

    :cond_e
    const/4 v1, 0x0

    aget-wide v1, v4, v1

    const-wide/16 v5, 0x0

    cmp-long v3, v1, v5

    if-eqz v3, :cond_f

    const-wide v5, -0x442d7f5971a4416bL    # -1.5672262211179677E-20

    xor-long/2addr v1, v5

    :cond_f
    const/16 v3, 0x20

    shl-long/2addr v1, v3

    const/16 v3, 0x20

    shr-long/2addr v1, v3

    long-to-int v1, v1

    const/4 v2, 0x5

    if-ne v1, v2, :cond_11

    :cond_10
    :goto_8
    return-void

    :cond_11
    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    const/4 v2, 0x1

    if-gt v1, v2, :cond_14

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, -0x73c9

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, -0x73fa

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_9
    array-length v5, v1

    if-lt v3, v5, :cond_12

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_a
    array-length v5, v1

    if-lt v3, v5, :cond_13

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_12
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_9

    :cond_13
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_a

    :cond_14
    const/4 v1, 0x0

    aget-wide v1, v4, v1

    const-wide/16 v5, 0x0

    cmp-long v3, v1, v5

    if-eqz v3, :cond_15

    const-wide v5, -0x442d7f5971a4416bL    # -1.5672262211179677E-20

    xor-long/2addr v1, v5

    :cond_15
    const/16 v3, 0x20

    shr-long/2addr v1, v3

    long-to-int v1, v1

    const/16 v2, 0x2713

    if-ne v1, v2, :cond_16

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WrapperScanListener;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;->access$100(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;)Landroid/content/Context;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WrapperScanListener;->hJ0()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryDbManagerWithProvider;->isPluginSupport(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_10

    :cond_16
    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    const/4 v2, 0x1

    if-gt v1, v2, :cond_19

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, 0x797b

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, 0x794a

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_b
    array-length v5, v1

    if-lt v3, v5, :cond_17

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_c
    array-length v5, v1

    if-lt v3, v5, :cond_18

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_17
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_b

    :cond_18
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_c

    :cond_19
    const/4 v1, 0x0

    aget-wide v1, v4, v1

    const-wide/16 v5, 0x0

    cmp-long v3, v1, v5

    if-eqz v3, :cond_1a

    const-wide v5, -0x442d7f5971a4416bL    # -1.5672262211179677E-20

    xor-long/2addr v1, v5

    :cond_1a
    const/16 v3, 0x20

    shr-long/2addr v1, v3

    long-to-int v1, v1

    const/16 v2, 0x2714

    if-ne v1, v2, :cond_1b

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WrapperScanListener;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;->access$100(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;)Landroid/content/Context;

    move-result-object v5

    const/16 v1, 0x24

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, 0x1d

    const/16 v34, 0x1e

    const/16 v35, 0x1f

    const/16 v36, 0x20

    const/16 v37, 0x21

    const/16 v38, 0x22

    const/16 v39, 0x23

    const/16 v40, -0x2f

    aput v40, v2, v39

    const/16 v39, 0x6553

    aput v39, v2, v38

    const/16 v38, -0x2d6

    aput v38, v2, v37

    const/16 v37, -0x42

    aput v37, v2, v36

    const/16 v36, -0x1a

    aput v36, v2, v35

    const/16 v35, -0x1c6

    aput v35, v2, v34

    const/16 v34, -0x47

    aput v34, v2, v33

    const/16 v33, -0x24db

    aput v33, v2, v32

    const/16 v32, -0x61

    aput v32, v2, v31

    const/16 v31, -0x5e

    aput v31, v2, v30

    const/16 v30, -0x2fef

    aput v30, v2, v29

    const/16 v29, -0x64

    aput v29, v2, v28

    const/16 v28, -0x41dd

    aput v28, v2, v27

    const/16 v27, -0x70

    aput v27, v2, v26

    const/16 v26, -0x71

    aput v26, v2, v25

    const/16 v25, -0x58

    aput v25, v2, v24

    const/16 v24, 0xa35

    aput v24, v2, v23

    const/16 v23, 0x227e

    aput v23, v2, v22

    const/16 v22, 0xd41

    aput v22, v2, v21

    const/16 v21, -0x3b94

    aput v21, v2, v20

    const/16 v20, -0x16

    aput v20, v2, v19

    const/16 v19, -0xd

    aput v19, v2, v18

    const/16 v18, -0x3d

    aput v18, v2, v17

    const/16 v17, -0x35

    aput v17, v2, v16

    const/16 v16, -0x25

    aput v16, v2, v15

    const/16 v15, 0x4077

    aput v15, v2, v14

    const/16 v14, -0x55d8

    aput v14, v2, v13

    const/16 v13, -0x27

    aput v13, v2, v12

    const/16 v12, -0x4e8f

    aput v12, v2, v11

    const/16 v11, -0x2e

    aput v11, v2, v10

    const/16 v10, -0x76

    aput v10, v2, v9

    const/16 v9, -0xa

    aput v9, v2, v8

    const/4 v8, -0x4

    aput v8, v2, v7

    const/16 v7, -0x28

    aput v7, v2, v6

    const/16 v6, -0x38d

    aput v6, v2, v3

    const/16 v3, -0x61

    aput v3, v2, v1

    const/16 v1, 0x24

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, 0x11

    const/16 v23, 0x12

    const/16 v24, 0x13

    const/16 v25, 0x14

    const/16 v26, 0x15

    const/16 v27, 0x16

    const/16 v28, 0x17

    const/16 v29, 0x18

    const/16 v30, 0x19

    const/16 v31, 0x1a

    const/16 v32, 0x1b

    const/16 v33, 0x1c

    const/16 v34, 0x1d

    const/16 v35, 0x1e

    const/16 v36, 0x1f

    const/16 v37, 0x20

    const/16 v38, 0x21

    const/16 v39, 0x22

    const/16 v40, 0x23

    const/16 v41, -0x6c

    aput v41, v1, v40

    const/16 v40, 0x6500

    aput v40, v1, v39

    const/16 v39, -0x29b

    aput v39, v1, v38

    const/16 v38, -0x3

    aput v38, v1, v37

    const/16 v37, -0x4d

    aput v37, v1, v36

    const/16 v36, -0x18a

    aput v36, v1, v35

    const/16 v35, -0x2

    aput v35, v1, v34

    const/16 v34, -0x2486

    aput v34, v1, v33

    const/16 v33, -0x25

    aput v33, v1, v32

    const/16 v32, -0x13

    aput v32, v1, v31

    const/16 v31, -0x2fa2

    aput v31, v1, v30

    const/16 v30, -0x30

    aput v30, v1, v29

    const/16 v29, -0x419f

    aput v29, v1, v28

    const/16 v28, -0x42

    aput v28, v1, v27

    const/16 v27, -0x1f

    aput v27, v1, v26

    const/16 v26, -0x39

    aput v26, v1, v25

    const/16 v25, 0xa5c

    aput v25, v1, v24

    const/16 v24, 0x220a

    aput v24, v1, v23

    const/16 v23, 0xd22

    aput v23, v1, v22

    const/16 v22, -0x3bf3

    aput v22, v1, v21

    const/16 v21, -0x3c

    aput v21, v1, v20

    const/16 v20, -0x65

    aput v20, v1, v19

    const/16 v19, -0x49

    aput v19, v1, v18

    const/16 v18, -0x59

    aput v18, v1, v17

    const/16 v17, -0x46

    aput v17, v1, v16

    const/16 v16, 0x4012

    aput v16, v1, v15

    const/16 v15, -0x55c0

    aput v15, v1, v14

    const/16 v14, -0x56

    aput v14, v1, v13

    const/16 v13, -0x4ea1

    aput v13, v1, v12

    const/16 v12, -0x4f

    aput v12, v1, v11

    const/16 v11, -0x11

    aput v11, v1, v10

    const/16 v10, -0x7b

    aput v10, v1, v9

    const/16 v9, -0x2e

    aput v9, v1, v8

    const/16 v8, -0x4b

    aput v8, v1, v7

    const/16 v7, -0x3e4

    aput v7, v1, v6

    const/4 v6, -0x4

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_d
    array-length v6, v1

    if-lt v3, v6, :cond_1c

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_e
    array-length v6, v1

    if-lt v3, v6, :cond_1d

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v5, v1}, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryDbManagerWithProvider;->isPluginSupport(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_10

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/Utils;->isHestiaModel()Z

    move-result v1

    if-nez v1, :cond_10

    :cond_1b
    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    if-gtz v1, :cond_20

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, -0x71

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, -0x41

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_f
    array-length v5, v1

    if-lt v3, v5, :cond_1e

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_10
    array-length v5, v1

    if-lt v3, v5, :cond_1f

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_1c
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_d

    :cond_1d
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_e

    :cond_1e
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_f

    :cond_1f
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_10

    :cond_20
    const/4 v1, 0x0

    aget-wide v1, v4, v1

    const-wide/16 v5, 0x0

    cmp-long v3, v1, v5

    if-eqz v3, :cond_21

    const-wide v5, -0x442d7f5971a4416bL    # -1.5672262211179677E-20

    xor-long/2addr v1, v5

    :cond_21
    const/16 v3, 0x20

    shl-long/2addr v1, v3

    const/16 v3, 0x20

    shr-long/2addr v1, v3

    long-to-int v1, v1

    const/4 v2, 0x6

    if-eq v1, v2, :cond_10

    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    if-gtz v1, :cond_24

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, -0x75

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, -0x45

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_11
    array-length v5, v1

    if-lt v3, v5, :cond_22

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_12
    array-length v5, v1

    if-lt v3, v5, :cond_23

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_22
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_11

    :cond_23
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_12

    :cond_24
    const/4 v1, 0x0

    aget-wide v1, v4, v1

    const-wide/16 v3, 0x0

    cmp-long v3, v1, v3

    if-eqz v3, :cond_25

    const-wide v3, -0x442d7f5971a4416bL    # -1.5672262211179677E-20

    xor-long/2addr v1, v3

    :cond_25
    const/16 v3, 0x20

    shl-long/2addr v1, v3

    const/16 v3, 0x20

    shr-long/2addr v1, v3

    long-to-int v1, v1

    const/4 v2, 0x4

    if-eq v1, v2, :cond_10

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getDataType()Ljava/util/List;

    move-result-object v1

    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getDataType()Ljava/util/List;

    move-result-object v1

    const/16 v2, 0x8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_10

    :goto_13
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WrapperScanListener;->requiredDeviceTypes:Ljava/util/ArrayList;

    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WrapperScanListener;->requiredDeviceTypes:Ljava/util/ArrayList;

    :try_start_2
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WrapperScanListener;->requiredDeviceTypes:Ljava/util/ArrayList;

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getType()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    :goto_14
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getType()I

    move-result v1

    const/16 v2, 0x2718

    if-ne v1, v2, :cond_26

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getDataType()Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_26

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getConnectivityType()I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_26

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getDataType()Ljava/util/List;

    move-result-object v1

    :try_start_3
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_3

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getDataType()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_10

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getSerialNumber()Ljava/lang/String;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getType()I

    move-result v3

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getDataType()Ljava/util/List;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getConnectivityType()I

    move-result v5

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getName()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    move-object/from16 v8, p1

    invoke-direct/range {v1 .. v8}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;-><init>(Ljava/lang/String;IIILjava/lang/String;ZLcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WrapperScanListener;->mScanningFrScanListener:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$ScanningFragmentScanListener;

    invoke-interface {v2, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$ScanningFragmentScanListener;->onDeviceFound(Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;)V

    goto/16 :goto_8

    :cond_26
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getDataType()Ljava/util/List;

    move-result-object v1

    :try_start_4
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_4
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_4} :catch_4

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getDataType()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_10

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-direct {v1, v0, v2}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;-><init>(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;Z)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WrapperScanListener;->mScanningFrScanListener:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$ScanningFragmentScanListener;

    invoke-interface {v2, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$ScanningFragmentScanListener;->onDeviceFound(Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;)V

    goto/16 :goto_8

    :catch_0
    move-exception v1

    goto/16 :goto_13

    :catch_1
    move-exception v1

    goto/16 :goto_14

    :catch_2
    move-exception v1

    goto/16 :goto_8

    :catch_3
    move-exception v1

    goto/16 :goto_8

    :catch_4
    move-exception v1

    goto/16 :goto_8
.end method

.method public onStarted(I)V
    .locals 13

    const-wide/16 v11, 0x0

    const-wide v9, -0x751c266760a16334L    # -3.305018413169694E-256

    const/4 v8, 0x1

    const/16 v7, 0x20

    const/4 v3, 0x0

    const/4 v0, 0x2

    new-array v2, v0, [J

    const-wide/16 v0, 0x1

    aput-wide v0, v2, v8

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_0

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    int-to-long v0, p1

    shl-long/2addr v0, v7

    ushr-long v4, v0, v7

    aget-wide v0, v2, v3

    cmp-long v6, v0, v11

    if-eqz v6, :cond_1

    xor-long/2addr v0, v9

    :cond_1
    ushr-long/2addr v0, v7

    shl-long/2addr v0, v7

    xor-long/2addr v0, v4

    xor-long/2addr v0, v9

    aput-wide v0, v2, v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WrapperScanListener;->mScanningFrScanListener:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$ScanningFragmentScanListener;

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_4

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-array v1, v8, [I

    const/16 v0, -0x308b

    aput v0, v1, v3

    new-array v0, v8, [I

    const/16 v2, -0x30bb

    aput v2, v0, v3

    move v2, v3

    :goto_0
    array-length v5, v0

    if-lt v2, v5, :cond_2

    array-length v0, v1

    new-array v0, v0, [C

    :goto_1
    array-length v2, v0

    if-lt v3, v2, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_2
    aget v5, v0, v2

    aget v6, v1, v2

    xor-int/2addr v5, v6

    aput v5, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    aget v2, v1, v3

    int-to-char v2, v2

    aput-char v2, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_4
    aget-wide v0, v2, v3

    cmp-long v2, v0, v11

    if-eqz v2, :cond_5

    xor-long/2addr v0, v9

    :cond_5
    shl-long/2addr v0, v7

    shr-long/2addr v0, v7

    long-to-int v0, v0

    invoke-interface {v4, v0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$ScanningFragmentScanListener;->onStarted(I)V

    return-void
.end method

.method public onStopped(I)V
    .locals 13

    const-wide/16 v11, 0x0

    const-wide v9, 0x40e17e149d53bc99L    # 35824.64420496784

    const/4 v8, 0x1

    const/16 v7, 0x20

    const/4 v3, 0x0

    const/4 v0, 0x2

    new-array v2, v0, [J

    const-wide/16 v0, 0x1

    aput-wide v0, v2, v8

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_0

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    int-to-long v0, p1

    shl-long/2addr v0, v7

    ushr-long v4, v0, v7

    aget-wide v0, v2, v3

    cmp-long v6, v0, v11

    if-eqz v6, :cond_1

    xor-long/2addr v0, v9

    :cond_1
    ushr-long/2addr v0, v7

    shl-long/2addr v0, v7

    xor-long/2addr v0, v4

    xor-long/2addr v0, v9

    aput-wide v0, v2, v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WrapperScanListener;->mScanningFrScanListener:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$ScanningFragmentScanListener;

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_4

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-array v1, v8, [I

    const/16 v0, 0x2b26

    aput v0, v1, v3

    new-array v0, v8, [I

    const/16 v2, 0x2b16

    aput v2, v0, v3

    move v2, v3

    :goto_0
    array-length v5, v0

    if-lt v2, v5, :cond_2

    array-length v0, v1

    new-array v0, v0, [C

    :goto_1
    array-length v2, v0

    if-lt v3, v2, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_2
    aget v5, v0, v2

    aget v6, v1, v2

    xor-int/2addr v5, v6

    aput v5, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    aget v2, v1, v3

    int-to-char v2, v2

    aput-char v2, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_4
    aget-wide v0, v2, v3

    cmp-long v2, v0, v11

    if-eqz v2, :cond_5

    xor-long/2addr v0, v9

    :cond_5
    shl-long/2addr v0, v7

    shr-long/2addr v0, v7

    long-to-int v0, v0

    invoke-interface {v4, v0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$ScanningFragmentScanListener;->onStopped(I)V

    return-void
.end method
