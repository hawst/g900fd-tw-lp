.class Lcom/sec/android/app/shealth/cignacoach/widget/SingleChoicePopupAdapter$1;
.super Ljava/lang/Object;
.source "SingleChoicePopupAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/cignacoach/widget/SingleChoicePopupAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/widget/SingleChoicePopupAdapter;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/cignacoach/widget/SingleChoicePopupAdapter;)V
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/SingleChoicePopupAdapter$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/SingleChoicePopupAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x1

    .line 64
    const v1, 0x7f080778

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    invoke-virtual {v1, v3}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 65
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "accessibility"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    .line 66
    .local v0, "am":Landroid/view/accessibility/AccessibilityManager;
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 68
    invoke-virtual {p1, v3}, Landroid/view/View;->sendAccessibilityEvent(I)V

    .line 70
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/SingleChoicePopupAdapter$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/SingleChoicePopupAdapter;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/SingleChoicePopupAdapter;->onDialogListItemClick:Lcom/sec/android/app/shealth/common/commonui/dialog/OnDialogListItemClick;
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/widget/SingleChoicePopupAdapter;->access$000(Lcom/sec/android/app/shealth/cignacoach/widget/SingleChoicePopupAdapter;)Lcom/sec/android/app/shealth/common/commonui/dialog/OnDialogListItemClick;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 71
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/SingleChoicePopupAdapter$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/SingleChoicePopupAdapter;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/SingleChoicePopupAdapter;->onDialogListItemClick:Lcom/sec/android/app/shealth/common/commonui/dialog/OnDialogListItemClick;
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/widget/SingleChoicePopupAdapter;->access$100(Lcom/sec/android/app/shealth/cignacoach/widget/SingleChoicePopupAdapter;)Lcom/sec/android/app/shealth/common/commonui/dialog/OnDialogListItemClick;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/sec/android/app/shealth/common/commonui/dialog/OnDialogListItemClick;->onItemClick(Landroid/view/View;)V

    .line 72
    :cond_1
    return-void
.end method
