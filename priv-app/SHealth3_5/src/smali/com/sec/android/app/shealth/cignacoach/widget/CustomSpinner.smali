.class public Lcom/sec/android/app/shealth/cignacoach/widget/CustomSpinner;
.super Landroid/widget/Spinner;
.source "CustomSpinner.java"


# instance fields
.field context:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 11
    invoke-direct {p0, p1}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;)V

    .line 8
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CustomSpinner;->context:Landroid/content/Context;

    .line 12
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CustomSpinner;->context:Landroid/content/Context;

    .line 13
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 8
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CustomSpinner;->context:Landroid/content/Context;

    .line 21
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 16
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 8
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CustomSpinner;->context:Landroid/content/Context;

    .line 17
    return-void
.end method


# virtual methods
.method public onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 25
    invoke-super {p0}, Landroid/widget/Spinner;->onDetachedFromWindow()V

    .line 26
    return-void
.end method
