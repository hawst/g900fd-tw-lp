.class public Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;
.super Ljava/lang/Object;
.source "ExerciseNames.java"


# static fields
.field public static final namesIds:Ljava/util/TreeMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const v7, 0x7f090b04

    const v6, 0x7f090b00

    const v5, 0x7f090aff

    const v4, 0x7f0909d0

    const v3, 0x7f090645

    .line 9
    new-instance v0, Ljava/util/TreeMap;

    sget-object v1, Ljava/lang/String;->CASE_INSENSITIVE_ORDER:Ljava/util/Comparator;

    invoke-direct {v0, v1}, Ljava/util/TreeMap;-><init>(Ljava/util/Comparator;)V

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    .line 12
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Walking"

    const v2, 0x7f0909d2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 13
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Fast walking"

    const v2, 0x7f090b42

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 14
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Running"

    const v2, 0x7f0909d3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 15
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Power walking"

    const v2, 0x7f090b0c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 16
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Walking upstairs"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 17
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Walking downstairs"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 18
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Hiking"

    const v2, 0x7f0909d1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 19
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Dancing"

    const v2, 0x7f090b0d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 20
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Inline Skating"

    const v2, 0x7f090b0e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 21
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Yoga"

    const v2, 0x7f090b01

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 22
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Aerobic"

    const v2, 0x7f090b0f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 23
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Skipping Rope"

    const v2, 0x7f090b10

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 24
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Water-skiing"

    const v2, 0x7f090b02

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 25
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Cycling"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 26
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Hula-hooping"

    const v2, 0x7f090b11

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 27
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Swimming"

    const v2, 0x7f090b12

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 28
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Aquarobics"

    const v2, 0x7f090b03

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Press-ups"

    const v2, 0x7f090b13

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 30
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Sit-ups"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Chin-ups"

    const v2, 0x7f090b14

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Rock climbing"

    const v2, 0x7f090b15

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "T\'ai chi"

    const v2, 0x7f090b16

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Hang gliding"

    const v2, 0x7f090b17

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Golf"

    const v2, 0x7f090b05

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Basketball"

    const v2, 0x7f090b18

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Squash"

    const v2, 0x7f090b19

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Sports dance"

    const v2, 0x7f090b1a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Skiing"

    const v2, 0x7f090b1b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Baseball"

    const v2, 0x7f090b06

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Football"

    const v2, 0x7f090b1c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Football, Competitive"

    const v2, 0x7f090b49

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Football, Casual"

    const v2, 0x7f09062f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Tennis"

    const v2, 0x7f090b1d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Badminton"

    const v2, 0x7f090b1e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Bowling"

    const v2, 0x7f090b07

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Billiards"

    const v2, 0x7f090b1f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Boxing"

    const v2, 0x7f090b20

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Handball"

    const v2, 0x7f090b21

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Horse riding"

    const v2, 0x7f090b08

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Judo"

    const v2, 0x7f090b22

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Taekwondo"

    const v2, 0x7f090b23

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Softball"

    const v2, 0x7f090b24

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Table tennis"

    const v2, 0x7f090b09

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Volleyball"

    const v2, 0x7f090b25

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Skin/scuba diving"

    const v2, 0x7f090b26

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Ice Skating"

    const v2, 0x7f090b27

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Step machine"

    const v2, 0x7f090b0a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Exercise bike"

    const v2, 0x7f090b28

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Weight machine"

    const v2, 0x7f090b0b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "General"

    const v2, 0x7f090b29

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Treadmill"

    const v2, 0x7f090b2a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Elliptical"

    const v2, 0x7f090b2b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Bike"

    const v2, 0x7f090b2c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Rower"

    const v2, 0x7f090b2d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Climber"

    const v2, 0x7f090b2e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Nordic skier"

    const v2, 0x7f090b2f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Pedometer"

    const v2, 0x7f090020

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Walking"

    const v2, 0x7f0909d2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Slow Walking"

    const v2, 0x7f090625

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Normal Walking"

    const v2, 0x7f090b36

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Fast Walking"

    const v2, 0x7f090b42

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Walking upstairs"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Walking downstairs"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Running"

    const v2, 0x7f0909d3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Running, General"

    const v2, 0x7f090b44

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Running, 4 mph"

    const v2, 0x7f0906ed

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Running, 5 mph"

    const v2, 0x7f0906ee

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Running, 6 mph"

    const v2, 0x7f0906ef

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Running, 7 mph"

    const v2, 0x7f0906f0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Running, 8 mph"

    const v2, 0x7f0906f1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Running, 9 mph"

    const v2, 0x7f0906f2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Running, 10 mph"

    const v2, 0x7f0906f3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Running, 11 mph"

    const v2, 0x7f0906f4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Running, 12 mph"

    const v2, 0x7f0906f5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Running, 13 mph"

    const v2, 0x7f0906f6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Running, 14 mph"

    const v2, 0x7f0906f7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "General"

    const v2, 0x7f0907b3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Hiking"

    const v2, 0x7f0909d1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Ballet"

    const v2, 0x7f090662

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Dancing"

    const v2, 0x7f090b0d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Inline skating, slow"

    const v2, 0x7f090664

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Inline skating, normal"

    const v2, 0x7f090b3f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 104
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Inline skating, fast"

    const v2, 0x7f090665

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Stretching, mild"

    const v2, 0x7f090626

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Pilates"

    const v2, 0x7f09068f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Yoga"

    const v2, 0x7f090b01

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Aerobic, Low impact"

    const v2, 0x7f090b4b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Aerobic, HighImpact"

    const v2, 0x7f090690

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Skipping (120~160/min)"

    const v2, 0x7f0906f8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 111
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Skipping (100~120/min)"

    const v2, 0x7f090b10

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 112
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Skipping (~100/min)"

    const v2, 0x7f090691

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Hula-hooping"

    const v2, 0x7f090b11

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Rock climbing, High Difficulty"

    const v2, 0x7f090627

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 115
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Rock climbing, Low to Moderate Difficulty"

    const v2, 0x7f090b43

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Tai chi, General"

    const v2, 0x7f090b33

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Tai chi, Light Effort"

    const v2, 0x7f090628

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 119
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Hangliding"

    const v2, 0x7f090b45

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 120
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Pistol shooting"

    const v2, 0x7f090693

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 121
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Archery, non-hunting "

    const v2, 0x7f090629

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 122
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Field hockey "

    const v2, 0x7f090694

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 123
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Ice hockey, general"

    const v2, 0x7f090695

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Hockey, ice, competitive"

    const v2, 0x7f09062a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Rugby, union, team, competitive"

    const v2, 0x7f09062b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Rugby, touch, non-competitive"

    const v2, 0x7f09062c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 127
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Golf"

    const v2, 0x7f090b05

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Basketball, general"

    const v2, 0x7f090b4d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 129
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Basketball, game"

    const v2, 0x7f09062d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 130
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Squash, general"

    const v2, 0x7f090b3b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 131
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Ballroom dance, fast"

    const v2, 0x7f09062e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Ballroom dance, slow"

    const v2, 0x7f090b48

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 133
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Baseball"

    const v2, 0x7f090b06

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 134
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Soccer, competitive"

    const v2, 0x7f090b49

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Soccer, casual"

    const v2, 0x7f09062f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 136
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Tennis, general"

    const v2, 0x7f090b34

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Tennis, singles"

    const v2, 0x7f090667

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Tennis, doubles"

    const v2, 0x7f090668

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Badminton, competitive"

    const v2, 0x7f090b4c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 140
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Badminton, social singles and doubles"

    const v2, 0x7f090630

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 141
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Billiards"

    const v2, 0x7f090b39

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 142
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Boxing, in ring"

    const v2, 0x7f090b46

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 143
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Boxing, punching bag"

    const v2, 0x7f090669

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 144
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Boxing, sparring"

    const v2, 0x7f09066a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 145
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Handball, general"

    const v2, 0x7f090b4a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 146
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Handball, team"

    const v2, 0x7f09066b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 147
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Horseback riding "

    const v2, 0x7f090b08

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 148
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Martial arts, moderate pace"

    const v2, 0x7f090b40

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 149
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Martial arts, slower pace"

    const v2, 0x7f090b32

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 150
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Softball, general "

    const v2, 0x7f090b3a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 151
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Softball, practice\u00a0"

    const v2, 0x7f09066c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 152
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Softball, pitching"

    const v2, 0x7f09066d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 153
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Table Tennis"

    const v2, 0x7f090b09

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 154
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Volleyball, competitive"

    const v2, 0x7f090b35

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 155
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Volleyball, general"

    const v2, 0x7f09066e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 156
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Step machine"

    const v2, 0x7f090b0a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 157
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Weight machine"

    const v2, 0x7f090b0b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 158
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Bowling"

    const v2, 0x7f090b07

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 161
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Water Activities"

    const v2, 0x7f09066f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 162
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "WaterActivities"

    const v2, 0x7f09066f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 163
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Water-Skiing"

    const v2, 0x7f090b02

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 164
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Free-style Swimming, Fast"

    const v2, 0x7f090670

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 165
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Free-style Swimming, Slow"

    const v2, 0x7f090b3d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 166
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Backstroke, Training"

    const v2, 0x7f090671

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 167
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Backstroke, Recreational"

    const v2, 0x7f090672

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 168
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Breaststroke, Training"

    const v2, 0x7f090673

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 169
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Breaststroke, Recreational"

    const v2, 0x7f090631

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 170
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Butterfly Stroke Swimming"

    const v2, 0x7f090632

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 171
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Aquarobics"

    const v2, 0x7f090b03

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 172
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Canoeing, rowing"

    const v2, 0x7f090674

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 173
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Sailing, yachting, for leisure"

    const v2, 0x7f090633

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 174
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Skindiving, fast"

    const v2, 0x7f090b38

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 175
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Skindiving, moderate"

    const v2, 0x7f090675

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 176
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Skindiving, scuba diving, general, rowing"

    const v2, 0x7f09063e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 177
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Snorkeling, for leisure"

    const v2, 0x7f090634

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 180
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Cycling"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 181
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Bicycling"

    const v2, 0x7f090622

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 182
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Bicycling, Slow"

    const v2, 0x7f090635

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 183
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Bicycling, Moderate"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 184
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Bicycling, Fast"

    const v2, 0x7f090636

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 185
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Bicycling, Very Fast"

    const v2, 0x7f090637

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 186
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Stationary bicycle, very light to light effort (30-50 watts)"

    const v2, 0x7f090b3c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 187
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Stationary bicycle, moderate to vigorous effort (90-100 watts)"

    const v2, 0x7f090638

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 188
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Stationary bicycle, vigorous effort (101-160 watts)"

    const v2, 0x7f090639

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 189
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Stationary bicycle, vigorous effort"

    const v2, 0x7f09063a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 190
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Stationary bicycle, 201-270 watts, very vigorous effort"

    const v2, 0x7f09063b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 191
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Stationary bicycle, 51-89 watts, light-to-moderate effort\u00a0"

    const v2, 0x7f09063c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 194
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Winter Activities"

    const v2, 0x7f090642

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 195
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Skiing, Light"

    const v2, 0x7f090643

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 196
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Skiing, Moderate"

    const v2, 0x7f090b37

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 197
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Skiing, Vigorous"

    const v2, 0x7f09063d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 198
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Snowboarding"

    const v2, 0x7f090b30

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 199
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Snowboarding, Light"

    const v2, 0x7f090643

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 200
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Snowboarding, Moderate"

    const v2, 0x7f090b37

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 201
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Ice dancing"

    const v2, 0x7f090644

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 202
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Ice skating, slow"

    const v2, 0x7f09063f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 203
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Ice skating, general"

    const v2, 0x7f090b3e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 204
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Ice skating, rapidly"

    const v2, 0x7f090640

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 205
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Speed Skating, competitive"

    const v2, 0x7f090641

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Strength Workout"

    const v2, 0x7f090b31

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 209
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Strength"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 210
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Strength Workouts"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 211
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Stength Workout"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 212
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Push-up"

    const v2, 0x7f090646

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 213
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Pull-up"

    const v2, 0x7f090b47

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 214
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Sit-up"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Dumbbell benchpress"

    const v2, 0x7f090647

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 216
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Barbell benchpress"

    const v2, 0x7f090648

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 217
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Flat bench dumbbell flye"

    const v2, 0x7f090649

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 218
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Incline barbell press"

    const v2, 0x7f09064a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 219
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Dumbbell lateral raise"

    const v2, 0x7f09064b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 220
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Smith machine upright row"

    const v2, 0x7f09064c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 221
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Dumbbell overhead press"

    const v2, 0x7f09064d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 222
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Barbell bent-over row"

    const v2, 0x7f09064e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 223
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Barbell upright row"

    const v2, 0x7f09064f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 224
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "One-arm dumbbell row"

    const v2, 0x7f090650

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 225
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Lat pulldown"

    const v2, 0x7f090651

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 226
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Barbell curl"

    const v2, 0x7f090652

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 227
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Preacher curl machine"

    const v2, 0x7f090653

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 228
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Incline dumbbell curl"

    const v2, 0x7f090654

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 229
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Lying barbell extension"

    const v2, 0x7f090655

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 230
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Rope pressdown"

    const v2, 0x7f090656

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 231
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Dumbbell kickback"

    const v2, 0x7f090657

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 232
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Leg extension"

    const v2, 0x7f090658

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 233
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Barbell squat"

    const v2, 0x7f090659

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 234
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Leg press"

    const v2, 0x7f09065a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 235
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Seated leg curl"

    const v2, 0x7f09065b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 236
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Romanian deadlift"

    const v2, 0x7f09065c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 237
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Lying leg curl"

    const v2, 0x7f09065d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 238
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Seated calf raise"

    const v2, 0x7f09065e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 239
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Standing calf raise"

    const v2, 0x7f09065f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 240
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Crunch"

    const v2, 0x7f090660

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 241
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Reverse crunch"

    const v2, 0x7f090661

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 242
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Pull-up ( Chin-ups )"

    const v2, 0x7f090b14

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 243
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Ballroom Dance, Slow"

    const v2, 0x7f090b1a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 244
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Martial arts, Moderate pace"

    const v2, 0x7f090b22

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 245
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Sit-up ( Sit-ups )"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 246
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Push-up ( Press-ups )"

    const v2, 0x7f090646

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 247
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Walking Downstairs"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 248
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Martial arts, Slower pace"

    const v2, 0x7f090b23

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 249
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Walking Upstairs"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 250
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Tai chi"

    const v2, 0x7f090b16

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 251
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Dance sports"

    const v2, 0x7f090b1a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 253
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
