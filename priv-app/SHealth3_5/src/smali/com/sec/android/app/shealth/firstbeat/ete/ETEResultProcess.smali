.class public Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;
.super Ljava/lang/Object;
.source "ETEResultProcess.java"


# static fields
.field protected static mAudioId:I

.field protected static mAudioString:Ljava/lang/String;

.field protected static mPaceId:I

.field private static mRealtimeTEResultProcess:Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;


# instance fields
.field private mUseCase:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mAudioId:I

    .line 11
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mAudioString:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    sget v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->USE_CASE_1:I

    iput v0, p0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mUseCase:I

    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mRealtimeTEResultProcess:Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;

    if-nez v0, :cond_0

    .line 30
    new-instance v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mRealtimeTEResultProcess:Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;

    .line 32
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mRealtimeTEResultProcess:Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;

    return-object v0
.end method


# virtual methods
.method public getAudioId()I
    .locals 1

    .prologue
    .line 15
    sget v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mAudioId:I

    return v0
.end method

.method public getAudioString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mAudioString:Ljava/lang/String;

    return-object v0
.end method

.method public getPaceId()I
    .locals 1

    .prologue
    .line 18
    sget v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mPaceId:I

    return v0
.end method

.method public process(Lfi/firstbeat/ete/ETEresults;)I
    .locals 6
    .param p1, "result"    # Lfi/firstbeat/ete/ETEresults;

    .prologue
    const v2, 0x7f09014e

    const/4 v5, 0x1

    const/4 v4, 0x2

    const/4 v3, 0x0

    .line 41
    sput v3, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mAudioId:I

    .line 42
    sput v3, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mPaceId:I

    .line 43
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mAudioString:Ljava/lang/String;

    .line 45
    iget v0, p1, Lfi/firstbeat/ete/ETEresults;->ETEphraseNumber:I

    packed-switch v0, :pswitch_data_0

    .line 152
    :cond_0
    :goto_0
    :pswitch_0
    return v3

    .line 47
    :pswitch_1
    iget v0, p0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mUseCase:I

    sget v1, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->USE_CASE_1:I

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mUseCase:I

    sget v1, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->USE_CASE_2a:I

    if-ne v0, v1, :cond_0

    .line 48
    :cond_1
    sput v3, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mPaceId:I

    .line 49
    const v0, 0x7f09013f

    sput v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mAudioId:I

    goto :goto_0

    .line 53
    :pswitch_2
    iget v0, p0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mUseCase:I

    sget v1, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->USE_CASE_1:I

    if-ne v0, v1, :cond_0

    .line 54
    sput v3, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mPaceId:I

    .line 55
    const v0, 0x7f090140

    sput v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mAudioId:I

    goto :goto_0

    .line 59
    :pswitch_3
    iget v0, p0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mUseCase:I

    sget v1, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->USE_CASE_1:I

    if-ne v0, v1, :cond_0

    .line 60
    sput v4, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mPaceId:I

    .line 61
    const v0, 0x7f090141

    sput v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mAudioId:I

    goto :goto_0

    .line 65
    :pswitch_4
    iget v0, p0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mUseCase:I

    sget v1, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->USE_CASE_1:I

    if-ne v0, v1, :cond_0

    .line 66
    sput v3, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mPaceId:I

    .line 67
    const v0, 0x7f090142

    sput v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mAudioId:I

    goto :goto_0

    .line 71
    :pswitch_5
    iget v0, p0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mUseCase:I

    sget v1, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->USE_CASE_1:I

    if-ne v0, v1, :cond_0

    .line 72
    sput v4, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mPaceId:I

    .line 73
    const v0, 0x7f090143

    sput v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mAudioId:I

    goto :goto_0

    .line 77
    :pswitch_6
    iget v0, p0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mUseCase:I

    sget v1, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->USE_CASE_1:I

    if-ne v0, v1, :cond_0

    .line 78
    sput v3, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mPaceId:I

    .line 79
    const v0, 0x7f090144

    sput v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mAudioId:I

    goto :goto_0

    .line 83
    :pswitch_7
    iget v0, p0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mUseCase:I

    sget v1, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->USE_CASE_1:I

    if-ne v0, v1, :cond_0

    .line 84
    sput v4, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mPaceId:I

    .line 85
    const v0, 0x7f090145

    sput v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mAudioId:I

    goto :goto_0

    .line 89
    :pswitch_8
    iget v0, p0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mUseCase:I

    sget v1, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->USE_CASE_1:I

    if-ne v0, v1, :cond_0

    .line 90
    sput v3, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mPaceId:I

    .line 91
    const v0, 0x7f090146

    sput v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mAudioId:I

    goto :goto_0

    .line 96
    :pswitch_9
    iget v0, p0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mUseCase:I

    sget v1, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->USE_CASE_1:I

    if-eq v0, v1, :cond_2

    iget v0, p0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mUseCase:I

    sget v1, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->USE_CASE_2a:I

    if-ne v0, v1, :cond_0

    .line 97
    :cond_2
    sput v5, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mPaceId:I

    .line 98
    const v0, 0x7f090147

    sput v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mAudioId:I

    goto/16 :goto_0

    .line 102
    :pswitch_a
    iget v0, p0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mUseCase:I

    sget v1, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->USE_CASE_2a:I

    if-eq v0, v1, :cond_3

    iget v0, p0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mUseCase:I

    sget v1, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->USE_CASE_2b:I

    if-ne v0, v1, :cond_0

    .line 103
    :cond_3
    sput v3, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mPaceId:I

    .line 104
    const v0, 0x7f090148

    sput v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mAudioId:I

    goto/16 :goto_0

    .line 108
    :pswitch_b
    iget v0, p0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mUseCase:I

    sget v1, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->USE_CASE_2a:I

    if-eq v0, v1, :cond_4

    iget v0, p0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mUseCase:I

    sget v1, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->USE_CASE_2b:I

    if-ne v0, v1, :cond_0

    .line 109
    :cond_4
    sput v4, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mPaceId:I

    .line 110
    const v0, 0x7f090149

    sput v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mAudioId:I

    goto/16 :goto_0

    .line 114
    :pswitch_c
    iget v0, p0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mUseCase:I

    sget v1, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->USE_CASE_2a:I

    if-eq v0, v1, :cond_5

    iget v0, p0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mUseCase:I

    sget v1, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->USE_CASE_2b:I

    if-ne v0, v1, :cond_0

    .line 115
    :cond_5
    const/4 v0, 0x3

    sput v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mPaceId:I

    .line 116
    const v0, 0x7f09014a

    sput v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mAudioId:I

    goto/16 :goto_0

    .line 120
    :pswitch_d
    iget v0, p0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mUseCase:I

    sget v1, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->USE_CASE_2a:I

    if-ne v0, v1, :cond_0

    .line 121
    const/4 v0, 0x4

    sput v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mPaceId:I

    .line 122
    const v0, 0x7f09014b

    sput v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mAudioId:I

    goto/16 :goto_0

    .line 126
    :pswitch_e
    iget v0, p0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mUseCase:I

    sget v1, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->USE_CASE_1:I

    if-eq v0, v1, :cond_6

    iget v0, p0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mUseCase:I

    sget v1, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->USE_CASE_2a:I

    if-eq v0, v1, :cond_6

    iget v0, p0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mUseCase:I

    sget v1, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->USE_CASE_2b:I

    if-ne v0, v1, :cond_0

    .line 127
    :cond_6
    sput v3, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mPaceId:I

    .line 128
    const v0, 0x7f09014c

    sput v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mAudioId:I

    goto/16 :goto_0

    .line 132
    :pswitch_f
    iget v0, p0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mUseCase:I

    sget v1, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->USE_CASE_1:I

    if-eq v0, v1, :cond_7

    iget v0, p0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mUseCase:I

    sget v1, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->USE_CASE_2a:I

    if-eq v0, v1, :cond_7

    iget v0, p0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mUseCase:I

    sget v1, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->USE_CASE_2b:I

    if-ne v0, v1, :cond_0

    .line 133
    :cond_7
    sput v3, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mPaceId:I

    .line 134
    const v0, 0x7f09014d

    sput v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mAudioId:I

    goto/16 :goto_0

    .line 138
    :pswitch_10
    iget v0, p0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mUseCase:I

    sget v1, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->USE_CASE_1:I

    if-eq v0, v1, :cond_8

    iget v0, p0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mUseCase:I

    sget v1, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->USE_CASE_2a:I

    if-eq v0, v1, :cond_8

    iget v0, p0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mUseCase:I

    sget v1, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->USE_CASE_2b:I

    if-ne v0, v1, :cond_0

    .line 139
    :cond_8
    sput v3, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mPaceId:I

    .line 140
    sput v2, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mAudioId:I

    .line 141
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mAudioString:Ljava/lang/String;

    goto/16 :goto_0

    .line 145
    :pswitch_11
    iget v0, p0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mUseCase:I

    sget v1, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->USE_CASE_1:I

    if-eq v0, v1, :cond_9

    iget v0, p0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mUseCase:I

    sget v1, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->USE_CASE_2a:I

    if-eq v0, v1, :cond_9

    iget v0, p0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mUseCase:I

    sget v1, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->USE_CASE_2b:I

    if-ne v0, v1, :cond_0

    .line 146
    :cond_9
    sput v5, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mPaceId:I

    .line 147
    const v0, 0x7f09014f

    sput v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mAudioId:I

    goto/16 :goto_0

    .line 45
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_0
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
    .end packed-switch
.end method

.method public setUseCase(I)V
    .locals 0
    .param p1, "useCase"    # I

    .prologue
    .line 36
    iput p1, p0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->mUseCase:I

    .line 37
    return-void
.end method
