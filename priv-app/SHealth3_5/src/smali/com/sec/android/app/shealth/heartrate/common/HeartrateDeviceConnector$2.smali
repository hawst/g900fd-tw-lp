.class Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector$2;
.super Ljava/lang/Object;
.source "HeartrateDeviceConnector.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ServiceConnectionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;)V
    .locals 0

    .prologue
    .line 324
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector$2;->this$0:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(I)V
    .locals 2
    .param p1, "error"    # I

    .prologue
    .line 356
    # getter for: Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->access$200()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Binding is done - Service connected"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 357
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector$2;->this$0:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    # invokes: Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->startDevice()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->access$400(Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;)V

    .line 358
    return-void
.end method

.method public onServiceDisconnected(I)V
    .locals 3
    .param p1, "error"    # I

    .prologue
    .line 327
    # getter for: Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->access$200()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Binding - Service disconnected"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 329
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector$2;->this$0:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    # getter for: Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->access$000(Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 330
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector$2;->this$0:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    # getter for: Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->IsDeviceStarted:Z
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->access$100(Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_3

    move-result v1

    if-eqz v1, :cond_0

    .line 332
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector$2;->this$0:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    # getter for: Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->access$000(Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->stopReceivingData()V

    .line 333
    # getter for: Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->access$200()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "stopReceivingData_2 is called"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_1 .. :try_end_1} :catch_3

    .line 340
    :cond_0
    :goto_0
    :try_start_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector$2;->this$0:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->IsDeviceStarted:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->access$102(Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;Z)Z

    .line 341
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector$2;->this$0:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    # getter for: Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->access$000(Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->leave()V

    .line 342
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector$2;->this$0:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->access$002(Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_2 .. :try_end_2} :catch_3

    .line 350
    :cond_1
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector$2;->this$0:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    # getter for: Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->access$300(Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 351
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector$2;->this$0:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    # getter for: Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->access$300(Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->close()V

    .line 352
    :cond_2
    return-void

    .line 334
    :catch_0
    move-exception v0

    .line 335
    .local v0, "e":Ljava/lang/IllegalStateException;
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_0

    .line 344
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_1
    move-exception v0

    .line 345
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1

    .line 336
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_2
    move-exception v0

    .line 337
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    :try_start_4
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;->printStackTrace()V
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_4 .. :try_end_4} :catch_3

    goto :goto_0

    .line 346
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    :catch_3
    move-exception v0

    .line 347
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto :goto_1
.end method
