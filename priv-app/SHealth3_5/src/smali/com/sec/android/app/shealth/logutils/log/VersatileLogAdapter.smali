.class public Lcom/sec/android/app/shealth/logutils/log/VersatileLogAdapter;
.super Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;
.source "VersatileLogAdapter.java"


# instance fields
.field private mConcreteDelegate:Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;


# direct methods
.method public constructor <init>(Landroid/database/Cursor;Landroid/content/Context;Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;)V
    .locals 0
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "concreteDelegate"    # Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;-><init>(Landroid/database/Cursor;Landroid/content/Context;)V

    .line 41
    iput-object p3, p0, Lcom/sec/android/app/shealth/logutils/log/VersatileLogAdapter;->mConcreteDelegate:Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;

    .line 42
    return-void
.end method

.method private isRowChanged(Landroid/view/View;J)Z
    .locals 3
    .param p1, "rowView"    # Landroid/view/View;
    .param p2, "time"    # J

    .prologue
    .line 115
    sget v1, Lcom/sec/android/app/shealth/logutils/R$id;->row_view_id_tag_key:I

    invoke-virtual {p1, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 116
    .local v0, "originalTime":Ljava/lang/Long;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    cmp-long v1, v1, p2

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected bindChildView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;Z)V
    .locals 9
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cursor"    # Landroid/database/Cursor;
    .param p4, "isLastChild"    # Z

    .prologue
    const/4 v6, 0x0

    .line 88
    move-object v3, p1

    check-cast v3, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;

    .line 89
    .local v3, "rowView":Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v7, "dd/MM/yyyy"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v8

    invoke-direct {v0, v7, v8}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 90
    .local v0, "dateFormatter":Ljava/text/DateFormat;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/log/VersatileLogAdapter;->getColumnNameForCreateTime()Ljava/lang/String;

    move-result-object v7

    invoke-interface {p3, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {p3, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 92
    .local v4, "time":J
    invoke-direct {p0, v3, v4, v5}, Lcom/sec/android/app/shealth/logutils/log/VersatileLogAdapter;->isRowChanged(Landroid/view/View;J)Z

    move-result v2

    .line 93
    .local v2, "isRowChanged":Z
    sget v7, Lcom/sec/android/app/shealth/logutils/R$id;->row_view_id_tag_key:I

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v3, v7, v8}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->setTag(ILjava/lang/Object;)V

    .line 95
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v0, v8}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "_"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/log/VersatileLogAdapter;->getColumnNameForID()Ljava/lang/String;

    move-result-object v8

    invoke-interface {p3, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-interface {p3, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 96
    invoke-virtual {v3}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->getDeleteCheckBox()Landroid/widget/CheckBox;

    move-result-object v1

    .line 97
    .local v1, "deleteCheckBox":Landroid/widget/CheckBox;
    if-eqz v2, :cond_0

    .line 98
    invoke-virtual {v3, v6}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->setHeadGoalVisibility(Z)V

    .line 101
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/log/VersatileLogAdapter;->isMenuDeleteMode()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 103
    invoke-virtual {v1, v6}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 104
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/log/VersatileLogAdapter;->isCheckAll()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/logutils/log/VersatileLogAdapter;->isLogSelected(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/logutils/log/VersatileLogAdapter;->isLogSelected(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    :cond_2
    const/4 v6, 0x1

    :cond_3
    invoke-virtual {v1, v6}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 111
    :goto_0
    iget-object v6, p0, Lcom/sec/android/app/shealth/logutils/log/VersatileLogAdapter;->mConcreteDelegate:Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;

    invoke-virtual {v6, v3, p2, p3, v2}, Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;->bindChildView(Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;Landroid/content/Context;Landroid/database/Cursor;Z)V

    .line 112
    return-void

    .line 107
    :cond_4
    const/16 v7, 0x8

    invoke-virtual {v1, v7}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 108
    invoke-virtual {v1, v6}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 109
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/log/VersatileLogAdapter;->clearAllSelection()V

    goto :goto_0
.end method

.method protected bindGroupView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;Z)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cursor"    # Landroid/database/Cursor;
    .param p4, "isExpanded"    # Z

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/log/VersatileLogAdapter;->mConcreteDelegate:Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;->bindGroupView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;Z)V

    .line 58
    return-void
.end method

.method public clearData()V
    .locals 1

    .prologue
    .line 152
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/logutils/log/VersatileLogAdapter;->mConcreteDelegate:Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;

    .line 153
    return-void
.end method

.method public getChildView(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I
    .param p3, "isLastChild"    # Z
    .param p4, "convertView"    # Landroid/view/View;
    .param p5, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 77
    move-object v4, p4

    .line 78
    .local v4, "child":Landroid/view/View;
    if-eqz v4, :cond_0

    instance-of v0, v4, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;

    if-nez v0, :cond_0

    .line 79
    const-string v0, "VersatileLogAdapter"

    const-string v1, "ConverView is not child instance, make new child view"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    const/4 v4, 0x0

    :cond_0
    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move-object v5, p5

    .line 83
    invoke-super/range {v0 .. v5}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->getChildView(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected getChildrenCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 1
    .param p1, "groupCursor"    # Landroid/database/Cursor;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/log/VersatileLogAdapter;->mConcreteDelegate:Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;->getChildCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method protected getColumnNameForCreateTime()Ljava/lang/String;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/log/VersatileLogAdapter;->mConcreteDelegate:Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;->getCreateTimeColumnName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getColumnNameForID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/log/VersatileLogAdapter;->mConcreteDelegate:Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;->getIdColumnName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getGroupView(IZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p1, "groupPosition"    # I
    .param p2, "isExpanded"    # Z
    .param p3, "convertView"    # Landroid/view/View;
    .param p4, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 63
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/logutils/log/VersatileLogAdapter;->isGroupCollapsed(I)Z

    move-result v0

    invoke-super {p0, p1, v0, p3, p4}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->getGroupView(IZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected getTotalChildCount()I
    .locals 3

    .prologue
    .line 124
    const/4 v0, 0x0

    .line 125
    .local v0, "childCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/log/VersatileLogAdapter;->getGroupCount()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 126
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/logutils/log/VersatileLogAdapter;->getChildrenCount(I)I

    move-result v2

    add-int/2addr v0, v2

    .line 125
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 128
    :cond_0
    return v0
.end method

.method protected newChildView(Landroid/content/Context;Landroid/database/Cursor;ZLandroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cursor"    # Landroid/database/Cursor;
    .param p3, "isLastChild"    # Z
    .param p4, "viewGroup"    # Landroid/view/ViewGroup;

    .prologue
    .line 68
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 69
    .local v0, "inflater":Landroid/view/LayoutInflater;
    iget-object v1, p0, Lcom/sec/android/app/shealth/logutils/log/VersatileLogAdapter;->mConcreteDelegate:Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;->getChildViewId()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p4, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    return-object v1
.end method

.method protected newGroupView(Landroid/content/Context;Landroid/database/Cursor;ZLandroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cursor"    # Landroid/database/Cursor;
    .param p3, "isExpanded"    # Z
    .param p4, "viewGroup"    # Landroid/view/ViewGroup;

    .prologue
    .line 51
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 52
    .local v0, "inflater":Landroid/view/LayoutInflater;
    iget-object v1, p0, Lcom/sec/android/app/shealth/logutils/log/VersatileLogAdapter;->mConcreteDelegate:Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;->getGroupViewId()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p4, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    return-object v1
.end method
