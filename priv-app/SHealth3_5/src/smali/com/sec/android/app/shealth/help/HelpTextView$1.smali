.class Lcom/sec/android/app/shealth/help/HelpTextView$1;
.super Ljava/lang/Object;
.source "HelpTextView.java"

# interfaces
.implements Landroid/text/Html$ImageGetter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/help/HelpTextView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/help/HelpTextView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/help/HelpTextView;)V
    .locals 0

    .prologue
    .line 245
    iput-object p1, p0, Lcom/sec/android/app/shealth/help/HelpTextView$1;->this$0:Lcom/sec/android/app/shealth/help/HelpTextView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 6
    .param p1, "source"    # Ljava/lang/String;

    .prologue
    .line 250
    const/4 v0, 0x0

    .line 252
    .local v0, "commonDrawable":Lcom/sec/android/app/shealth/help/HelpDrawable;
    iget-object v3, p0, Lcom/sec/android/app/shealth/help/HelpTextView$1;->this$0:Lcom/sec/android/app/shealth/help/HelpTextView;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/help/HelpTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const-string v4, "drawable"

    iget-object v5, p0, Lcom/sec/android/app/shealth/help/HelpTextView$1;->this$0:Lcom/sec/android/app/shealth/help/HelpTextView;

    # getter for: Lcom/sec/android/app/shealth/help/HelpTextView;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/android/app/shealth/help/HelpTextView;->access$000(Lcom/sec/android/app/shealth/help/HelpTextView;)Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, p1, v4, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 255
    .local v2, "resID":I
    if-eqz v2, :cond_0

    .line 256
    iget-object v3, p0, Lcom/sec/android/app/shealth/help/HelpTextView$1;->this$0:Lcom/sec/android/app/shealth/help/HelpTextView;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/help/HelpTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/BitmapDrawable;

    .line 258
    .local v1, "drawable":Landroid/graphics/drawable/BitmapDrawable;
    new-instance v0, Lcom/sec/android/app/shealth/help/HelpDrawable;

    .end local v0    # "commonDrawable":Lcom/sec/android/app/shealth/help/HelpDrawable;
    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/help/HelpDrawable;-><init>(Landroid/graphics/drawable/BitmapDrawable;)V

    .line 261
    .end local v1    # "drawable":Landroid/graphics/drawable/BitmapDrawable;
    .restart local v0    # "commonDrawable":Lcom/sec/android/app/shealth/help/HelpDrawable;
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/help/HelpTextView$1;->this$0:Lcom/sec/android/app/shealth/help/HelpTextView;

    # getter for: Lcom/sec/android/app/shealth/help/HelpTextView;->mDrawables:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/app/shealth/help/HelpTextView;->access$100(Lcom/sec/android/app/shealth/help/HelpTextView;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 262
    iget-object v3, p0, Lcom/sec/android/app/shealth/help/HelpTextView$1;->this$0:Lcom/sec/android/app/shealth/help/HelpTextView;

    # invokes: Lcom/sec/android/app/shealth/help/HelpTextView;->invalidateDrawables()V
    invoke-static {v3}, Lcom/sec/android/app/shealth/help/HelpTextView;->access$200(Lcom/sec/android/app/shealth/help/HelpTextView;)V

    .line 264
    return-object v0
.end method
