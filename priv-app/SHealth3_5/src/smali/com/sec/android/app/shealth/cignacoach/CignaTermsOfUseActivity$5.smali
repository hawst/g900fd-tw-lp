.class Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity$5;
.super Ljava/lang/Object;
.source "CignaTermsOfUseActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;)V
    .locals 0

    .prologue
    .line 132
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity$5;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 137
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity$5;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;->isAgreeCheck:Z
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;->access$200(Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 139
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;->setIsAgreeTermsOfUse()V

    .line 140
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity$5;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;->mTermsOfUseValue:F
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;->access$500(Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;)F

    move-result v1

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    .line 141
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity$5;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity$5;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;->mTermsOfUseValue:F
    invoke-static {v2}, Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;->access$500(Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;)F

    move-result v2

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;->setCingaTermsOfUseVersionCode(Landroid/content/Context;F)V

    .line 142
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.shealth.action.COACH"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 143
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity$5;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;->startActivity(Landroid/content/Intent;)V

    .line 144
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity$5;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;->finish()V

    .line 159
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_1
    :goto_0
    return-void

    .line 148
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity$5;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;->mToast1:Landroid/widget/Toast;
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;->access$600(Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;)Landroid/widget/Toast;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity$5;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;->mToast1:Landroid/widget/Toast;
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;->access$600(Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->isShown()Z

    move-result v1

    if-nez v1, :cond_1

    .line 149
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity$5;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;->mToast1:Landroid/widget/Toast;
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;->access$600(Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;)Landroid/widget/Toast;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 150
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity$5;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;->mToast1:Landroid/widget/Toast;
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;->access$600(Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->cancel()V

    .line 152
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity$5;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity$5;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;

    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity$5;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;

    const v4, 0x7f090c44

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v2

    # setter for: Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;->mToast1:Landroid/widget/Toast;
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;->access$602(Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;Landroid/widget/Toast;)Landroid/widget/Toast;

    .line 153
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity$5;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;->mToast1:Landroid/widget/Toast;
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;->access$600(Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
