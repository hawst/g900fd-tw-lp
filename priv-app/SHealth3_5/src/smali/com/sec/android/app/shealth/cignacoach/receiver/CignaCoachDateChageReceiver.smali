.class public Lcom/sec/android/app/shealth/cignacoach/receiver/CignaCoachDateChageReceiver;
.super Landroid/content/BroadcastReceiver;
.source "CignaCoachDateChageReceiver.java"


# static fields
.field private static HANDLE_MIDNIGHT:Ljava/lang/String;

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/receiver/CignaCoachDateChageReceiver;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/receiver/CignaCoachDateChageReceiver;->TAG:Ljava/lang/String;

    .line 17
    const-string v0, "com.sec.android.app.shealth.cignacoach.HANDLE_MIDNIGHT"

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/receiver/CignaCoachDateChageReceiver;->HANDLE_MIDNIGHT:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private startCoachIntentService(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intentAction"    # Ljava/lang/String;

    .prologue
    .line 67
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 68
    .local v0, "service":Landroid/content/Intent;
    const-string v1, "ACTION_NAME"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 69
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 70
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const v5, 0x7f0b0006

    .line 22
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    .line 64
    :cond_0
    :goto_0
    return-void

    .line 26
    :cond_1
    invoke-static {p1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isInitializationNeeded(Landroid/content/Context;)Z

    move-result v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_0

    .line 31
    sget-object v2, Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;->COACH_STARTED_SETTING:Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;

    invoke-static {p1, v2}, Lcom/cigna/coach/utils/SharedPrefUtils;->getCoachSettings(Landroid/content/Context;Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;)Z

    move-result v1

    .line 32
    .local v1, "isCoachStartedAtLeastOnce":Z
    if-eqz v1, :cond_0

    .line 35
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 36
    .local v0, "action":Ljava/lang/String;
    sget-object v2, Lcom/sec/android/app/shealth/cignacoach/receiver/CignaCoachDateChageReceiver;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[CignaCoachDateChageReceiver onReceive] action: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 38
    if-eqz v0, :cond_3

    const-string v2, "com.cigna.mobile.coach.GOAL_COMPLETED"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "com.cigna.mobile.coach.GOAL_CANCELLED"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "com.cigna.mobile.coach.MISSION_COMPLETED"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "com.cigna.mobile.coach.MISSION_FAILED"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 45
    :cond_2
    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/shealth/cignacoach/receiver/CignaCoachDateChageReceiver;->startCoachIntentService(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 47
    :cond_3
    if-eqz v0, :cond_5

    const-string v2, "android.intent.action.TIME_SET"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 48
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/receiver/CignaDaybreakService;->isRun()Z

    move-result v2

    if-nez v2, :cond_4

    .line 49
    sget-object v2, Lcom/sec/android/app/shealth/cignacoach/receiver/CignaCoachDateChageReceiver;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "service stopped"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 51
    :cond_4
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 52
    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/shealth/cignacoach/receiver/CignaCoachDateChageReceiver;->startCoachIntentService(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 55
    :cond_5
    if-eqz v0, :cond_0

    sget-object v2, Lcom/sec/android/app/shealth/cignacoach/receiver/CignaCoachDateChageReceiver;->HANDLE_MIDNIGHT:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 56
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/receiver/CignaDaybreakService;->isRun()Z

    move-result v2

    if-nez v2, :cond_6

    .line 57
    sget-object v2, Lcom/sec/android/app/shealth/cignacoach/receiver/CignaCoachDateChageReceiver;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "service stopped"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 59
    :cond_6
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 60
    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/shealth/cignacoach/receiver/CignaCoachDateChageReceiver;->startCoachIntentService(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
