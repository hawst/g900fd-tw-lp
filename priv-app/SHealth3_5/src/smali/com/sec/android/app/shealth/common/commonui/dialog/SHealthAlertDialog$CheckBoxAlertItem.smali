.class public Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$CheckBoxAlertItem;
.super Ljava/lang/Object;
.source "SHealthAlertDialog.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CheckBoxAlertItem"
.end annotation


# instance fields
.field public checked:I

.field public itemName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1313
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1315
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$CheckBoxAlertItem;->checked:I

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 1320
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "arg1"    # I

    .prologue
    .line 1325
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$CheckBoxAlertItem;->itemName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1326
    iget v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$CheckBoxAlertItem;->checked:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1328
    return-void
.end method
