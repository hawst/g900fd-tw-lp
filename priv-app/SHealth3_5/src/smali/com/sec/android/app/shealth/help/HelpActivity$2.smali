.class Lcom/sec/android/app/shealth/help/HelpActivity$2;
.super Ljava/lang/Object;
.source "HelpActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/help/HelpActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/help/HelpActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/help/HelpActivity;)V
    .locals 0

    .prologue
    .line 137
    iput-object p1, p0, Lcom/sec/android/app/shealth/help/HelpActivity$2;->this$0:Lcom/sec/android/app/shealth/help/HelpActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 141
    iget-object v0, p0, Lcom/sec/android/app/shealth/help/HelpActivity$2;->this$0:Lcom/sec/android/app/shealth/help/HelpActivity;

    # getter for: Lcom/sec/android/app/shealth/help/HelpActivity;->isListViewOpen:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/help/HelpActivity;->access$100(Lcom/sec/android/app/shealth/help/HelpActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Lcom/sec/android/app/shealth/help/HelpActivity$2;->this$0:Lcom/sec/android/app/shealth/help/HelpActivity;

    # setter for: Lcom/sec/android/app/shealth/help/HelpActivity;->isListViewOpen:Z
    invoke-static {v0, v2}, Lcom/sec/android/app/shealth/help/HelpActivity;->access$102(Lcom/sec/android/app/shealth/help/HelpActivity;Z)Z

    .line 143
    iget-object v0, p0, Lcom/sec/android/app/shealth/help/HelpActivity$2;->this$0:Lcom/sec/android/app/shealth/help/HelpActivity;

    # getter for: Lcom/sec/android/app/shealth/help/HelpActivity;->mGuideListContainer:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/app/shealth/help/HelpActivity;->access$200(Lcom/sec/android/app/shealth/help/HelpActivity;)Landroid/widget/LinearLayout;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 144
    iget-object v0, p0, Lcom/sec/android/app/shealth/help/HelpActivity$2;->this$0:Lcom/sec/android/app/shealth/help/HelpActivity;

    # getter for: Lcom/sec/android/app/shealth/help/HelpActivity;->mTitleButton:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/shealth/help/HelpActivity;->access$300(Lcom/sec/android/app/shealth/help/HelpActivity;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setSelected(Z)V

    .line 150
    :goto_0
    return-void

    .line 146
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/help/HelpActivity$2;->this$0:Lcom/sec/android/app/shealth/help/HelpActivity;

    # setter for: Lcom/sec/android/app/shealth/help/HelpActivity;->isListViewOpen:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/help/HelpActivity;->access$102(Lcom/sec/android/app/shealth/help/HelpActivity;Z)Z

    .line 147
    iget-object v0, p0, Lcom/sec/android/app/shealth/help/HelpActivity$2;->this$0:Lcom/sec/android/app/shealth/help/HelpActivity;

    # getter for: Lcom/sec/android/app/shealth/help/HelpActivity;->mGuideListContainer:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/app/shealth/help/HelpActivity;->access$200(Lcom/sec/android/app/shealth/help/HelpActivity;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 148
    iget-object v0, p0, Lcom/sec/android/app/shealth/help/HelpActivity$2;->this$0:Lcom/sec/android/app/shealth/help/HelpActivity;

    # getter for: Lcom/sec/android/app/shealth/help/HelpActivity;->mTitleButton:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/shealth/help/HelpActivity;->access$300(Lcom/sec/android/app/shealth/help/HelpActivity;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setSelected(Z)V

    goto :goto_0
.end method
