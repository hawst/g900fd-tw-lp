.class public Lcom/sec/android/app/shealth/common/utils/ResourceUtil;
.super Ljava/lang/Object;
.source "ResourceUtil.java"


# static fields
.field public static final SHEALTH_RES_APP_PACKAGE:Ljava/lang/String; = "com.sec.android.app.shealth"

.field private static mPackageName:Ljava/lang/String;

.field private static mResourceUtil:Lcom/sec/android/app/shealth/common/utils/ResourceUtil;

.field private static mSharedResource:Landroid/content/res/Resources;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    return-void
.end method

.method public static getInstance(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/app/shealth/common/utils/ResourceUtil;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "appPackageName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/pm/PackageManager$NameNotFoundException;
        }
    .end annotation

    .prologue
    .line 62
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/ResourceUtil;->mResourceUtil:Lcom/sec/android/app/shealth/common/utils/ResourceUtil;

    if-nez v0, :cond_0

    .line 63
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/ResourceUtil;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/common/utils/ResourceUtil;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/ResourceUtil;->mResourceUtil:Lcom/sec/android/app/shealth/common/utils/ResourceUtil;

    .line 65
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/ResourceUtil;->mSharedResource:Landroid/content/res/Resources;

    .line 68
    sput-object p1, Lcom/sec/android/app/shealth/common/utils/ResourceUtil;->mPackageName:Ljava/lang/String;

    .line 69
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/ResourceUtil;->mResourceUtil:Lcom/sec/android/app/shealth/common/utils/ResourceUtil;

    return-object v0
.end method


# virtual methods
.method public getDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 6
    .param p1, "drawableName"    # Ljava/lang/String;

    .prologue
    .line 83
    const/4 v0, 0x0

    .line 85
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    sget-object v3, Lcom/sec/android/app/shealth/common/utils/ResourceUtil;->mSharedResource:Landroid/content/res/Resources;

    const-string v4, "drawable"

    sget-object v5, Lcom/sec/android/app/shealth/common/utils/ResourceUtil;->mPackageName:Ljava/lang/String;

    invoke-virtual {v3, p1, v4, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 88
    .local v2, "resourceId":I
    :try_start_0
    sget-object v3, Lcom/sec/android/app/shealth/common/utils/ResourceUtil;->mSharedResource:Landroid/content/res/Resources;

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 93
    :goto_0
    return-object v0

    .line 89
    :catch_0
    move-exception v1

    .line 90
    .local v1, "nfe":Landroid/content/res/Resources$NotFoundException;
    invoke-virtual {v1}, Landroid/content/res/Resources$NotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method public getDrawable(Ljava/lang/String;I)Landroid/graphics/drawable/Drawable;
    .locals 6
    .param p1, "drawableName"    # Ljava/lang/String;
    .param p2, "density"    # I

    .prologue
    .line 111
    const/4 v0, 0x0

    .line 112
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    sget-object v3, Lcom/sec/android/app/shealth/common/utils/ResourceUtil;->mSharedResource:Landroid/content/res/Resources;

    const-string v4, "drawable"

    sget-object v5, Lcom/sec/android/app/shealth/common/utils/ResourceUtil;->mPackageName:Ljava/lang/String;

    invoke-virtual {v3, p1, v4, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 116
    .local v2, "resourceId":I
    :try_start_0
    sget-object v3, Lcom/sec/android/app/shealth/common/utils/ResourceUtil;->mSharedResource:Landroid/content/res/Resources;

    invoke-virtual {v3, v2, p2}, Landroid/content/res/Resources;->getDrawableForDensity(II)Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 121
    :goto_0
    return-object v0

    .line 117
    :catch_0
    move-exception v1

    .line 118
    .local v1, "nfe":Landroid/content/res/Resources$NotFoundException;
    invoke-virtual {v1}, Landroid/content/res/Resources$NotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method public getSharedResources()Landroid/content/res/Resources;
    .locals 1

    .prologue
    .line 131
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/ResourceUtil;->mSharedResource:Landroid/content/res/Resources;

    return-object v0
.end method
