.class Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment$ResetDialogButtonController;
.super Ljava/lang/Object;
.source "HealthCareSummaryFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ResetDialogButtonController"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;)V
    .locals 0

    .prologue
    .line 285
    iput-object p1, p0, Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment$ResetDialogButtonController;->this$0:Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment$1;

    .prologue
    .line 285
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment$ResetDialogButtonController;-><init>(Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;)V

    return-void
.end method


# virtual methods
.method public onDialogButtonClick(Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;)V
    .locals 3
    .param p1, "dialogButtonType"    # Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    .prologue
    .line 288
    sget-object v0, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->POSITIVE:Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 289
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment$ResetDialogButtonController;->this$0:Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;->deleteAllDataForDay()V

    .line 290
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment$ResetDialogButtonController;->this$0:Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;->clearMainView()V

    .line 291
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment$ResetDialogButtonController;->this$0:Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f09094a

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 293
    :cond_0
    return-void
.end method
