.class Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$22;
.super Ljava/lang/Object;
.source "ExerciseProWorkoutController.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->updateOutdoorLayout()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)V
    .locals 0

    .prologue
    .line 1728
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$22;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const-wide/16 v7, 0x3e8

    const/4 v6, 0x0

    const/4 v4, 0x1

    .line 1733
    sget v0, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->SOUND_MODE:I

    if-nez v0, :cond_0

    .line 1734
    sget v0, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->SOUND_STOP:I

    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->playSound(I)V

    .line 1736
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$22;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->isWorkoutMinIntervalOver()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1737
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$22;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->showMinWorkoutIntervalPopup()V

    .line 1760
    :goto_0
    return-void

    .line 1739
    :cond_1
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->saveUiLockState(Z)V

    .line 1740
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$22;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->curActivityType:I
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$2700(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1758
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$22;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->stopWorkoutAndShowDetail()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$4100(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)V

    goto :goto_0

    .line 1743
    :pswitch_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "com.sec.android.app.shealth.plugins.exercisepro"

    const-string v3, "P133"

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$22;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->dispData:Ljava/util/Map;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$1700(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)Ljava/util/Map;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    mul-long/2addr v4, v7

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v1, v2, v3, v6, v0}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    goto :goto_1

    .line 1746
    :pswitch_1
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "com.sec.android.app.shealth.plugins.exercisepro"

    const-string v3, "P130"

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$22;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->dispData:Ljava/util/Map;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$1700(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)Ljava/util/Map;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    mul-long/2addr v4, v7

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v1, v2, v3, v6, v0}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    goto :goto_1

    .line 1749
    :pswitch_2
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "com.sec.android.app.shealth.plugins.exercisepro"

    const-string v3, "P131"

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$22;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->dispData:Ljava/util/Map;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$1700(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)Ljava/util/Map;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    mul-long/2addr v4, v7

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v1, v2, v3, v6, v0}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    goto :goto_1

    .line 1752
    :pswitch_3
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "com.sec.android.app.shealth.plugins.exercisepro"

    const-string v3, "P132"

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$22;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->dispData:Ljava/util/Map;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$1700(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)Ljava/util/Map;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    mul-long/2addr v4, v7

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v1, v2, v3, v6, v0}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    goto/16 :goto_1

    .line 1740
    nop

    :pswitch_data_0
    .packed-switch 0x4652
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
