.class Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity$6;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity$6;->this$0:Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    const/4 v7, 0x0

    const/4 v6, 0x1

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v0

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity$6;->this$0:Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;

    new-array v3, v6, [Ljava/lang/Long;

    iget-object v4, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity$6;->this$0:Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mPagePeriodType:I
    invoke-static {v4}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->access$200(Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;)I

    move-result v4

    invoke-static {v0, v1, v4}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfCurrentPeriod(JI)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    # invokes: Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->addToPeriodStarts(Ljava/util/List;)V
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->access$300(Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;Ljava/util/List;)V

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity$6;->this$0:Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;

    new-array v3, v6, [Ljava/lang/Long;

    const-wide/32 v4, 0x5265c00

    sub-long/2addr v0, v4

    iget-object v4, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity$6;->this$0:Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mPagePeriodType:I
    invoke-static {v4}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->access$200(Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;)I

    move-result v4

    invoke-static {v0, v1, v4}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfCurrentPeriod(JI)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v3, v7

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    # invokes: Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->addToPeriodStarts(Ljava/util/List;)V
    invoke-static {v2, v0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->access$300(Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;Ljava/util/List;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity$6;->this$0:Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;

    # invokes: Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->addTwoPreviousMonthsToPeriodStarts()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->access$400(Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity$6;->this$0:Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;

    # invokes: Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->addMonthsFromDB()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->access$500(Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity$6;->this$0:Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;

    # invokes: Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->setNewAdapter(Z)V
    invoke-static {v0, v6}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->access$600(Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;Z)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity$6;->this$0:Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;

    # invokes: Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->checkPeriodsAvailable()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->access$700(Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;)V

    return-void
.end method
