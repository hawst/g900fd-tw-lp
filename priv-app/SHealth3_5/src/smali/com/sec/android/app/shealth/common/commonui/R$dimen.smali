.class public final Lcom/sec/android/app/shealth/common/commonui/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/common/commonui/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final _1dp:I = 0x7f0a00ca

.field public static final accessory_connection_icon_size:I = 0x7f0a00ac

.field public static final accessory_connection_view_status_text_left_margin:I = 0x7f0a00ab

.field public static final accessory_connection_view_status_text_size:I = 0x7f0a00aa

.field public static final activity_horizontal_margin:I = 0x7f0a0000

.field public static final activity_vertical_margin:I = 0x7f0a0001

.field public static final constrollerview_gradationbar_text_size:I = 0x7f0a0106

.field public static final controllerview_big_gradationbar_height_horizontal:I = 0x7f0a0100

.field public static final controllerview_big_gradationbar_height_vertical:I = 0x7f0a0103

.field public static final controllerview_gradationbar_horizontal_between_distance:I = 0x7f0a00ff

.field public static final controllerview_gradationbar_vertical_between_distance:I = 0x7f0a00fe

.field public static final controllerview_gradationbar_width:I = 0x7f0a0108

.field public static final controllerview_horizontal_bottomline_height:I = 0x7f0a0107

.field public static final controllerview_horizontal_height:I = 0x7f0a00f0

.field public static final controllerview_horizontal_width:I = 0x7f0a00ef

.field public static final controllerview_margin:I = 0x7f0a00f1

.field public static final controllerview_middle_gradationbar_height_horizontal:I = 0x7f0a0101

.field public static final controllerview_middle_gradationbar_height_vertical:I = 0x7f0a0104

.field public static final controllerview_small_gradationbar_height_horizontal:I = 0x7f0a0102

.field public static final controllerview_small_gradationbar_height_vertical:I = 0x7f0a0105

.field public static final controllerview_vertical_height:I = 0x7f0a00ee

.field public static final controllerview_vertical_width:I = 0x7f0a00ed

.field public static final dialog_bottom_padding:I = 0x7f0a008a

.field public static final dialog_side_padding:I = 0x7f0a008b

.field public static final dialog_title_header_height:I = 0x7f0a008c

.field public static final gradationview_gradationbar_text_size:I = 0x7f0a00f8

.field public static final gradationview_gradationbar_width:I = 0x7f0a00fb

.field public static final gradationview_horizontal_between_gradationbars_distance:I = 0x7f0a00fd

.field public static final gradationview_horizontal_big_gradationbar_height:I = 0x7f0a00f2

.field public static final gradationview_horizontal_bottomline_height:I = 0x7f0a00f9

.field public static final gradationview_horizontal_middle_gradationbar_height:I = 0x7f0a00f3

.field public static final gradationview_horizontal_small_gradationbar_height:I = 0x7f0a00f4

.field public static final gradationview_vertical_between_gradationbars_distance:I = 0x7f0a00fc

.field public static final gradationview_vertical_big_gradationbar_height:I = 0x7f0a00f5

.field public static final gradationview_vertical_bottomline_height:I = 0x7f0a00fa

.field public static final gradationview_vertical_middle_gradationbar_height:I = 0x7f0a00f6

.field public static final gradationview_vertical_small_gradationbar_height:I = 0x7f0a00f7

.field public static final inputmodule_height:I = 0x7f0a012f

.field public static final inputmodule_horizontal_image_button_margin_left:I = 0x7f0a0111

.field public static final inputmodule_horizontal_image_button_margin_right:I = 0x7f0a0110

.field public static final inputmodule_middle_layout_margin_bottom:I = 0x7f0a0112

.field public static final inputmodule_padding_left:I = 0x7f0a012d

.field public static final inputmodule_padding_right:I = 0x7f0a012e

.field public static final inputmodule_pin_margin_left:I = 0x7f0a0130

.field public static final inputmodule_top_layout_edittext_height:I = 0x7f0a010c

.field public static final inputmodule_top_layout_edittext_margin_bottom:I = 0x7f0a010e

.field public static final inputmodule_top_layout_edittext_text_size:I = 0x7f0a010b

.field public static final inputmodule_top_layout_edittext_width:I = 0x7f0a010d

.field public static final inputmodule_top_layout_height:I = 0x7f0a0109

.field public static final inputmodule_top_layout_margin_bottom:I = 0x7f0a010a

.field public static final inputmodule_top_layout_texview_text_size:I = 0x7f0a010f

.field public static final inputmodule_vertical_image_button_margin_bottom:I = 0x7f0a0121

.field public static final inputmodule_vertical_image_button_margin_top:I = 0x7f0a0120

.field public static final inputmodule_vertical_middle_layout_button_margin_top:I = 0x7f0a011e

.field public static final inputmodule_vertical_middle_layout_button_size:I = 0x7f0a011d

.field public static final inputmodule_vertical_middle_layout_controller_view_margin_top:I = 0x7f0a011f

.field public static final inputmodule_vertical_middle_layout_margin_top:I = 0x7f0a011c

.field public static final inputmodule_vertical_top_layout_edittable_height:I = 0x7f0a0118

.field public static final inputmodule_vertical_top_layout_edittable_margin_top:I = 0x7f0a0117

.field public static final inputmodule_vertical_top_layout_edittable_text_size:I = 0x7f0a0119

.field public static final inputmodule_vertical_top_layout_layout_width:I = 0x7f0a0114

.field public static final inputmodule_vertical_top_layout_margin_bottom:I = 0x7f0a0113

.field public static final inputmodule_vertical_top_layout_textview_title_height:I = 0x7f0a0115

.field public static final inputmodule_vertical_top_layout_textview_title_text_size:I = 0x7f0a0116

.field public static final inputmodule_vertical_top_layout_textview_unit_margin_top:I = 0x7f0a011a

.field public static final inputmodule_vertical_top_layout_textview_unit_text_size:I = 0x7f0a011b

.field public static final list_popup_divider_height:I = 0x7f0a00e9

.field public static final list_popup_height:I = 0x7f0a00e7

.field public static final list_popup_item_height:I = 0x7f0a00e6

.field public static final list_popup_text_padding_left:I = 0x7f0a00ea

.field public static final list_popup_text_padding_right:I = 0x7f0a00eb

.field public static final list_popup_text_size:I = 0x7f0a00ec

.field public static final list_popup_width:I = 0x7f0a00e8

.field public static final max_height_listpopup:I = 0x7f0a0094

.field public static final ok_cancel_buttons_double_line_height:I = 0x7f0a0098

.field public static final ok_cancel_buttons_padding_bottom:I = 0x7f0a0097

.field public static final ok_cancel_buttons_padding_side:I = 0x7f0a0096

.field public static final ok_cancel_buttons_padding_top:I = 0x7f0a0095

.field public static final ok_cancel_buttons_single_line_height:I = 0x7f0a007f

.field public static final ok_cancel_buttons_text_size:I = 0x7f0a007a

.field public static final popup_content_padding_bottom:I = 0x7f0a0091

.field public static final popup_content_padding_left:I = 0x7f0a0074

.field public static final popup_content_padding_right:I = 0x7f0a0075

.field public static final popup_content_padding_top:I = 0x7f0a0090

.field public static final popup_datetime_title_padding:I = 0x7f0a0093

.field public static final popup_margin:I = 0x7f0a008d

.field public static final popup_shadow_size:I = 0x7f0a008e

.field public static final popup_text_content_padding:I = 0x7f0a0092

.field public static final popup_title_height:I = 0x7f0a0071

.field public static final popup_width:I = 0x7f0a0070

.field public static final popup_width_content:I = 0x7f0a008f

.field public static final profile_attribute_item_edit_text_container_height:I = 0x7f0a00df

.field public static final progressbar_label_height:I = 0x7f0a012c

.field public static final share_app_icon_size:I = 0x7f0a0080

.field public static final share_app_item_height:I = 0x7f0a0083

.field public static final share_app_item_width:I = 0x7f0a0082

.field public static final share_app_name_text_width:I = 0x7f0a0081

.field public static final share_grid_items_padding:I = 0x7f0a0084

.field public static final share_grid_items_padding_bottom:I = 0x7f0a0088

.field public static final share_grid_items_padding_left:I = 0x7f0a0085

.field public static final share_grid_items_padding_right:I = 0x7f0a0086

.field public static final share_grid_items_padding_top:I = 0x7f0a0087

.field public static final share_grid_items_spacing_vertical:I = 0x7f0a0089

.field public static final summary_pressure_logo_size:I = 0x7f0a00a5

.field public static final summary_screen_accessory_connection_bottom_margin:I = 0x7f0a00e2

.field public static final summary_screen_accessory_connection_left_margin:I = 0x7f0a00e1

.field public static final summary_screen_accessory_connection_text_left_margin:I = 0x7f0a00e0

.field public static final summary_screen_accessory_connection_text_size:I = 0x7f0a00e3

.field public static final summary_screen_accessory_icon_height:I = 0x7f0a00e5

.field public static final summary_screen_accessory_icon_width:I = 0x7f0a00e4

.field public static final summary_view_button_text_size:I = 0x7f0a00af

.field public static final summary_view_content_balance_statistics_text_size:I = 0x7f0a00ba

.field public static final summary_view_content_balance_text_margin_bottom:I = 0x7f0a00b8

.field public static final summary_view_content_balance_text_margin_top:I = 0x7f0a00b7

.field public static final summary_view_content_balance_text_size:I = 0x7f0a00b9

.field public static final summary_view_content_bar_height:I = 0x7f0a00b2

.field public static final summary_view_content_bar_margin:I = 0x7f0a00b3

.field public static final summary_view_content_divider_height:I = 0x7f0a00b5

.field public static final summary_view_content_divider_margin_top:I = 0x7f0a00b4

.field public static final summary_view_content_divider_width:I = 0x7f0a00b6

.field public static final summary_view_content_icon_height:I = 0x7f0a00a1

.field public static final summary_view_content_icon_width:I = 0x7f0a00a2

.field public static final summary_view_content_layout_height:I = 0x7f0a00b0

.field public static final summary_view_content_layout_margin_top:I = 0x7f0a00b1

.field public static final summary_view_content_text_margin_left:I = 0x7f0a00a4

.field public static final summary_view_content_text_size:I = 0x7f0a00a3

.field public static final summary_view_content_title_margin_bottom:I = 0x7f0a00a0

.field public static final summary_view_content_title_margin_top:I = 0x7f0a009f

.field public static final summary_view_content_title_text_height:I = 0x7f0a009e

.field public static final summary_view_content_title_text_size:I = 0x7f0a009d

.field public static final summary_view_content_value_layout_height:I = 0x7f0a009c

.field public static final summary_view_counter_text_size:I = 0x7f0a00cc

.field public static final summary_view_header_balance_state_one_line_text_size:I = 0x7f0a00a7

.field public static final summary_view_header_balance_state_text_height:I = 0x7f0a00a9

.field public static final summary_view_header_balance_state_two_lines_text_size:I = 0x7f0a00a8

.field public static final summary_view_header_height:I = 0x7f0a009b

.field public static final summary_view_header_icon_margin_bottom:I = 0x7f0a009a

.field public static final summary_view_header_icon_margin_top:I = 0x7f0a0099

.field public static final summary_view_header_view_height:I = 0x7f0a00a6

.field public static final summary_view_health_care_counter_view_text_size:I = 0x7f0a00c2

.field public static final summary_view_health_care_counter_view_units_text_size:I = 0x7f0a00c3

.field public static final summary_view_health_care_progress_bar_height:I = 0x7f0a00bc

.field public static final summary_view_health_care_progress_bar_label_side_margin:I = 0x7f0a00c8

.field public static final summary_view_health_care_progress_bar_label_text_size:I = 0x7f0a00c7

.field public static final summary_view_health_care_progress_bar_margin_top:I = 0x7f0a00c1

.field public static final summary_view_health_care_progress_bar_side_margin:I = 0x7f0a00c0

.field public static final summary_view_health_care_progress_bar_slider_height:I = 0x7f0a00be

.field public static final summary_view_health_care_progress_bar_slider_width:I = 0x7f0a00bd

.field public static final summary_view_health_care_progress_bar_text_container_width:I = 0x7f0a00c9

.field public static final summary_view_health_care_progress_bar_title_margin_top:I = 0x7f0a00c5

.field public static final summary_view_health_care_progress_bar_title_text_size:I = 0x7f0a00c6

.field public static final summary_view_health_care_progress_bar_title_width:I = 0x7f0a00c4

.field public static final summary_view_health_care_progress_bar_width:I = 0x7f0a00bf

.field public static final summary_view_health_care_progress_view_container_height:I = 0x7f0a00bb

.field public static final summary_view_single_counter_view_margin_left:I = 0x7f0a00db

.field public static final summary_view_single_degree_line_height:I = 0x7f0a00dd

.field public static final summary_view_single_degree_line_width:I = 0x7f0a00de

.field public static final summary_view_single_indicator_icon_margin_top:I = 0x7f0a00d6

.field public static final summary_view_single_indicator_icon_size:I = 0x7f0a00da

.field public static final summary_view_single_measure_text_label_height:I = 0x7f0a00cf

.field public static final summary_view_single_measure_text_label_margin_left:I = 0x7f0a00ce

.field public static final summary_view_single_measure_text_label_margin_top:I = 0x7f0a00cd

.field public static final summary_view_single_measure_text_label_size:I = 0x7f0a00d0

.field public static final summary_view_single_outside_normal_icon_text_margin:I = 0x7f0a00d2

.field public static final summary_view_single_outside_normal_range_height:I = 0x7f0a00d1

.field public static final summary_view_single_outside_normal_range_text_size:I = 0x7f0a00d4

.field public static final summary_view_single_outside_normal_text_margin:I = 0x7f0a00d3

.field public static final summary_view_single_statistics_from_text_margin:I = 0x7f0a00d5

.field public static final summary_view_single_units_text_size:I = 0x7f0a00dc

.field public static final summary_view_single_value_counter_view_height:I = 0x7f0a00d7

.field public static final summary_view_single_value_counter_view_margin_bottom:I = 0x7f0a00d9

.field public static final summary_view_single_value_counter_view_margin_left:I = 0x7f0a00d8

.field public static final summary_view_slider_width:I = 0x7f0a00cb

.field public static final summary_view_update_button_height:I = 0x7f0a00ae

.field public static final summary_view_update_button_width:I = 0x7f0a00ad

.field public static final vertical_progress_area_margin:I = 0x7f0a0128

.field public static final vertical_progress_area_width:I = 0x7f0a0127

.field public static final vertical_progress_height:I = 0x7f0a0125

.field public static final vertical_progress_labels_area_margin:I = 0x7f0a0123

.field public static final vertical_progress_labels_area_width:I = 0x7f0a0122

.field public static final vertical_progress_mark_height:I = 0x7f0a0126

.field public static final vertical_progress_value_label_height:I = 0x7f0a012a

.field public static final vertical_progress_value_label_text_size:I = 0x7f0a012b

.field public static final vertical_progress_value_label_width:I = 0x7f0a0129

.field public static final vertical_progress_width:I = 0x7f0a0124


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
