.class public final Lcom/sec/android/app/shealth/framework/ui/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/framework/ui/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final EMMMdyyyy:I = 0x7f0901a4

.field public static final EdMMMyyyy:I = 0x7f0901a3

.field public static final EddMMMyyyy:I = 0x7f0901a6

.field public static final Pictures:I = 0x7f09007f

.field public static final accept:I = 0x7f090058

.field public static final action_not_defined_for_this_item:I = 0x7f09018b

.field public static final action_settings:I = 0x7f09001a

.field public static final add:I = 0x7f09003f

.field public static final add_memo:I = 0x7f090128

.field public static final after_meal:I = 0x7f0901b0

.field public static final after_meal_short:I = 0x7f0901b1

.field public static final alert:I = 0x7f0900d8

.field public static final all:I = 0x7f090076

.field public static final all_changes_discard:I = 0x7f090083

.field public static final already_connected_toast:I = 0x7f0900ab

.field public static final am:I = 0x7f090181

.field public static final am_upper:I = 0x7f0900e6

.field public static final and_324_scale_text_1:I = 0x7f0902d5

.field public static final and_324_scale_text_2:I = 0x7f0902d6

.field public static final and_851_bp_text_1:I = 0x7f0902d7

.field public static final and_851_bp_text_2:I = 0x7f0902d8

.field public static final and_ua851_bp_text_1:I = 0x7f0902e9

.field public static final and_ua851_bp_text_2:I = 0x7f0902ea

.field public static final and_uc324_scale_text_1:I = 0x7f0902e6

.field public static final and_uc324_scale_text_2:I = 0x7f0902e7

.field public static final and_uc324_scale_text_3:I = 0x7f0902e8

.field public static final app_name:I = 0x7f090019

.field public static final april:I = 0x7f0900ff

.field public static final april_short:I = 0x7f09010b

.field public static final august:I = 0x7f090103

.field public static final august_short:I = 0x7f09010f

.field public static final auto_backup:I = 0x7f090274

.field public static final average:I = 0x7f09006d

.field public static final average_short:I = 0x7f09006e

.field public static final bg_input_field_image_button_back:I = 0x7f0901a7

.field public static final bg_input_field_image_button_next:I = 0x7f0901a8

.field public static final bg_lifescan:I = 0x7f0902eb

.field public static final birth_date_alert_message:I = 0x7f090119

.field public static final blank_space:I = 0x7f090277

.field public static final blood_glucose:I = 0x7f0901ae

.field public static final blood_glucose_connectivity_text:I = 0x7f090295

.field public static final blood_glucose_meter:I = 0x7f09018e

.field public static final blood_pressure:I = 0x7f0901b4

.field public static final blood_pressure_connectivity_text:I = 0x7f090294

.field public static final blood_pressure_monitor:I = 0x7f09018f

.field public static final bloodglucose:I = 0x7f09026b

.field public static final bloodglucose_unit:I = 0x7f09026d

.field public static final bloodpressure:I = 0x7f09026c

.field public static final bloodpressure_unit:I = 0x7f090270

.field public static final bluetooth_not_supported:I = 0x7f0902a3

.field public static final bluetooth_turnoff_toast:I = 0x7f09008e

.field public static final body_temparature:I = 0x7f09023f

.field public static final bp_data_received_toast:I = 0x7f0902a8

.field public static final bpm:I = 0x7f0900d2

.field public static final bt_and_pressure_1:I = 0x7f0902b9

.field public static final bt_and_pressure_2:I = 0x7f0902ba

.field public static final bt_and_pressure_3:I = 0x7f0902bb

.field public static final bt_and_pressure_4:I = 0x7f0902bc

.field public static final bt_and_scale_1:I = 0x7f0902bd

.field public static final bt_and_scale_2:I = 0x7f0902be

.field public static final bt_and_scale_3:I = 0x7f0902bf

.field public static final bt_and_scale_4:I = 0x7f0902c0

.field public static final bt_omron_pressure_1:I = 0x7f0902b5

.field public static final bt_omron_pressure_2:I = 0x7f0902b8

.field public static final bt_omron_pressure_3:I = 0x7f0902b6

.field public static final bt_omron_weight_1:I = 0x7f0902b7

.field public static final bt_popup_receiving:I = 0x7f0900af

.field public static final calendar:I = 0x7f090069

.field public static final calorie:I = 0x7f0900ba

.field public static final calories:I = 0x7f0900bb

.field public static final camera:I = 0x7f0900df

.field public static final cancel:I = 0x7f090048

.field public static final cannot_build_dialog_without_title_exception:I = 0x7f090197

.field public static final cannot_build_dialog_without_type_exception:I = 0x7f090198

.field public static final category:I = 0x7f090086

.field public static final caution:I = 0x7f0900d7

.field public static final challenge:I = 0x7f090183

.field public static final change_view_mode:I = 0x7f09021d

.field public static final chart_view:I = 0x7f090063

.field public static final chinese_colon:I = 0x7f0900d5

.field public static final cigna:I = 0x7f09002e

.field public static final cigna_coach_total_help:I = 0x7f090202

.field public static final cigna_question:I = 0x7f0901f3

.field public static final close:I = 0x7f090049

.field public static final cm:I = 0x7f0900bc

.field public static final coach:I = 0x7f090029

.field public static final comments:I = 0x7f09007d

.field public static final compatible_product_text_1:I = 0x7f090114

.field public static final connect:I = 0x7f09009f

.field public static final connect_accessary:I = 0x7f090180

.field public static final connect_accessory:I = 0x7f090098

.field public static final connected:I = 0x7f090060

.field public static final connected_device_exist_msg:I = 0x7f09009b

.field public static final connected_to_ps:I = 0x7f090095

.field public static final connecting:I = 0x7f0900a4

.field public static final connecting_same_accessory_type_msg:I = 0x7f09009d

.field public static final connectivity_accessory_name:I = 0x7f09028c

.field public static final content_ui_msg:I = 0x7f090272

.field public static final contentui_multiple_message:I = 0x7f090267

.field public static final contentui_single_message:I = 0x7f090268

.field public static final continue_button_text:I = 0x7f090046

.field public static final daily_average_short:I = 0x7f09006f

.field public static final data_from_s_health:I = 0x7f0900de

.field public static final data_in_server_notification:I = 0x7f090137

.field public static final data_not_in_server_notification:I = 0x7f090138

.field public static final data_received_from_sensor:I = 0x7f09018a

.field public static final data_synced_from:I = 0x7f0900aa

.field public static final date:I = 0x7f090135

.field public static final date_before_birth:I = 0x7f090084

.field public static final day:I = 0x7f090064

.field public static final december:I = 0x7f090107

.field public static final december_short:I = 0x7f090113

.field public static final decilitre_short:I = 0x7f090222

.field public static final decline:I = 0x7f090059

.field public static final default_hrm_text_1:I = 0x7f0902e0

.field public static final default_hrm_text_2:I = 0x7f0902e1

.field public static final delete:I = 0x7f090035

.field public static final deleted_message_all_items:I = 0x7f09011e

.field public static final deleted_message_one_item:I = 0x7f09011c

.field public static final deleted_message_over_two_items:I = 0x7f09011d

.field public static final deleting:I = 0x7f0900b4

.field public static final description_gear_1:I = 0x7f0901ba

.field public static final description_gear_2:I = 0x7f0901bb

.field public static final description_gear_3:I = 0x7f0901bc

.field public static final description_hrm:I = 0x7f0901be

.field public static final description_samsung_hrm:I = 0x7f0901bd

.field public static final deselect_all:I = 0x7f090072

.field public static final details:I = 0x7f09005b

.field public static final detected_accessories:I = 0x7f090091

.field public static final detected_blood_glucose_devices:I = 0x7f090297

.field public static final detected_blood_pressure_devices:I = 0x7f090291

.field public static final device_connectivity:I = 0x7f090184

.field public static final device_name:I = 0x7f0900ad

.field public static final device_null_toast:I = 0x7f0902a4

.field public static final devicename:I = 0x7f09029d

.field public static final dialog_without_content_exception:I = 0x7f090196

.field public static final diastolic:I = 0x7f0901b6

.field public static final different_length_exception:I = 0x7f09019a

.field public static final discard_changes:I = 0x7f090081

.field public static final discard_changes_q:I = 0x7f090082

.field public static final disconnect:I = 0x7f0900a0

.field public static final disconnect_accessory:I = 0x7f090099

.field public static final disconnect_accessory_msg:I = 0x7f09009a

.field public static final distance:I = 0x7f090132

.field public static final do_you_want_t0_connect_q:I = 0x7f0900a9

.field public static final done:I = 0x7f090044

.field public static final double_prime:I = 0x7f0900d4

.field public static final down:I = 0x7f0901aa

.field public static final drawer_menu:I = 0x7f09020e

.field public static final ecg:I = 0x7f09023e

.field public static final edit:I = 0x7f090040

.field public static final edit_memo:I = 0x7f090127

.field public static final end:I = 0x7f090052

.field public static final enter_password:I = 0x7f09011f

.field public static final error:I = 0x7f0900d9

.field public static final exception_join_toast:I = 0x7f0902a7

.field public static final exercise:I = 0x7f090021

.field public static final exercise_acc:I = 0x7f090186

.field public static final expand_list:I = 0x7f09021c

.field public static final facebook_share:I = 0x7f0901f4

.field public static final failed_to_connect_accessory:I = 0x7f09009c

.field public static final fasting:I = 0x7f0901af

.field public static final favorites:I = 0x7f090088

.field public static final favourite:I = 0x7f090087

.field public static final february:I = 0x7f0900fd

.field public static final february_short:I = 0x7f090109

.field public static final firstbeat_coach_feedback_1:I = 0x7f090166

.field public static final firstbeat_coach_feedback_10:I = 0x7f09016f

.field public static final firstbeat_coach_feedback_11:I = 0x7f090170

.field public static final firstbeat_coach_feedback_13:I = 0x7f090171

.field public static final firstbeat_coach_feedback_14:I = 0x7f090172

.field public static final firstbeat_coach_feedback_15:I = 0x7f090173

.field public static final firstbeat_coach_feedback_2:I = 0x7f090167

.field public static final firstbeat_coach_feedback_3:I = 0x7f090168

.field public static final firstbeat_coach_feedback_4:I = 0x7f090169

.field public static final firstbeat_coach_feedback_5:I = 0x7f09016a

.field public static final firstbeat_coach_feedback_6:I = 0x7f09016b

.field public static final firstbeat_coach_feedback_7:I = 0x7f09016c

.field public static final firstbeat_coach_feedback_8:I = 0x7f09016d

.field public static final firstbeat_coach_feedback_9:I = 0x7f09016e

.field public static final firstbeat_coach_fitness_level_1:I = 0x7f090174

.field public static final firstbeat_coach_fitness_level_2:I = 0x7f090175

.field public static final firstbeat_coach_fitness_level_3:I = 0x7f090176

.field public static final firstbeat_coach_fitness_level_4:I = 0x7f090177

.field public static final firstbeat_coach_fitness_level_5:I = 0x7f090178

.field public static final firstbeat_coach_fitness_level_6:I = 0x7f090179

.field public static final firstbeat_coach_fitness_level_7:I = 0x7f09017a

.field public static final firstbeat_coach_longterm_rest:I = 0x7f090151

.field public static final firstbeat_coach_longterm_running:I = 0x7f090150

.field public static final firstbeat_coach_next_workout_1:I = 0x7f09015c

.field public static final firstbeat_coach_next_workout_10:I = 0x7f090165

.field public static final firstbeat_coach_next_workout_2:I = 0x7f09015d

.field public static final firstbeat_coach_next_workout_3:I = 0x7f09015e

.field public static final firstbeat_coach_next_workout_4:I = 0x7f09015f

.field public static final firstbeat_coach_next_workout_5:I = 0x7f090160

.field public static final firstbeat_coach_next_workout_6:I = 0x7f090161

.field public static final firstbeat_coach_next_workout_7:I = 0x7f090162

.field public static final firstbeat_coach_next_workout_8:I = 0x7f090163

.field public static final firstbeat_coach_next_workout_9:I = 0x7f090164

.field public static final firstbeat_coach_next_workout_title_1:I = 0x7f090152

.field public static final firstbeat_coach_next_workout_title_10:I = 0x7f09015b

.field public static final firstbeat_coach_next_workout_title_2:I = 0x7f090153

.field public static final firstbeat_coach_next_workout_title_3:I = 0x7f090154

.field public static final firstbeat_coach_next_workout_title_4:I = 0x7f090155

.field public static final firstbeat_coach_next_workout_title_5:I = 0x7f090156

.field public static final firstbeat_coach_next_workout_title_6:I = 0x7f090157

.field public static final firstbeat_coach_next_workout_title_7:I = 0x7f090158

.field public static final firstbeat_coach_next_workout_title_8:I = 0x7f090159

.field public static final firstbeat_coach_next_workout_title_9:I = 0x7f09015a

.field public static final firstbeat_ete_gudie_335_1:I = 0x7f09013f

.field public static final firstbeat_ete_gudie_335_10:I = 0x7f090144

.field public static final firstbeat_ete_gudie_335_11:I = 0x7f090145

.field public static final firstbeat_ete_gudie_335_14:I = 0x7f090146

.field public static final firstbeat_ete_gudie_335_2:I = 0x7f090140

.field public static final firstbeat_ete_gudie_335_20:I = 0x7f090147

.field public static final firstbeat_ete_gudie_335_22:I = 0x7f090148

.field public static final firstbeat_ete_gudie_335_23:I = 0x7f090149

.field public static final firstbeat_ete_gudie_335_24:I = 0x7f09014a

.field public static final firstbeat_ete_gudie_335_25:I = 0x7f09014b

.field public static final firstbeat_ete_gudie_335_3:I = 0x7f090141

.field public static final firstbeat_ete_gudie_335_30:I = 0x7f09014c

.field public static final firstbeat_ete_gudie_335_31:I = 0x7f09014d

.field public static final firstbeat_ete_gudie_335_32:I = 0x7f09014e

.field public static final firstbeat_ete_gudie_335_33:I = 0x7f09014f

.field public static final firstbeat_ete_gudie_335_6:I = 0x7f090142

.field public static final firstbeat_ete_gudie_335_7:I = 0x7f090143

.field public static final firstbeat_min:I = 0x7f09013a

.field public static final fitness_equipment_text:I = 0x7f0902dd

.field public static final food:I = 0x7f090028

.field public static final forerunner_watch_text:I = 0x7f0902d9

.field public static final forerunner_watch_text_1:I = 0x7f0902da

.field public static final forgot_password:I = 0x7f090121

.field public static final forgot_password_message:I = 0x7f090124

.field public static final forgot_password_message_d:I = 0x7f090123

.field public static final format_date_month_year_period:I = 0x7f0901a0

.field public static final format_month_date:I = 0x7f09019e

.field public static final format_mu_year_month_date:I = 0x7f0901a1

.field public static final format_year:I = 0x7f09019f

.field public static final format_year_month:I = 0x7f09019d

.field public static final format_year_month_date:I = 0x7f0901a2

.field public static final format_year_month_date_day:I = 0x7f09019c

.field public static final frequent:I = 0x7f090085

.field public static final fri:I = 0x7f0900f2

.field public static final fri_short:I = 0x7f0900f9

.field public static final from_s_health:I = 0x7f0900dd

.field public static final ft_inch:I = 0x7f0900bd

.field public static final future_date_alert_message:I = 0x7f090118

.field public static final ga_trackingId:I = 0x7f09001c

.field public static final gallery:I = 0x7f0900e0

.field public static final gear:I = 0x7f090190

.field public static final gear_manager:I = 0x7f090191

.field public static final goal:I = 0x7f09006b

.field public static final goal_achieved_filter_name:I = 0x7f090077

.field public static final goal_not_achieved_filter_name:I = 0x7f090078

.field public static final goal_upper:I = 0x7f09006c

.field public static final grams_short:I = 0x7f0900bf

.field public static final health_care_input_activity_delete_dialog_text:I = 0x7f09028b

.field public static final health_care_summary_view_default_value:I = 0x7f09027e

.field public static final health_care_summary_view_value_default_mask_float:I = 0x7f09027f

.field public static final health_care_summary_view_value_default_mask_integer:I = 0x7f090280

.field public static final health_content_title_received_data:I = 0x7f090266

.field public static final health_content_title_start_recording:I = 0x7f090265

.field public static final health_service:I = 0x7f090239

.field public static final health_service_name:I = 0x7f090273

.field public static final heart_rate:I = 0x7f090022

.field public static final heart_rate_monitor:I = 0x7f090024

.field public static final height:I = 0x7f09012f

.field public static final hello_world:I = 0x7f09001b

.field public static final help:I = 0x7f090039

.field public static final help_download:I = 0x7f090115

.field public static final home:I = 0x7f09002b

.field public static final hour:I = 0x7f090065

.field public static final hour_short:I = 0x7f0900e9

.field public static final hours:I = 0x7f0900e8

.field public static final how_to_use:I = 0x7f09005c

.field public static final hrm_heartrate_accessories:I = 0x7f09017d

.field public static final hrm_select_tag_icon:I = 0x7f0902b4

.field public static final hrm_text_1:I = 0x7f0902db

.field public static final hrm_text_2:I = 0x7f0902dc

.field public static final incorrect_password_5_times:I = 0x7f090125

.field public static final infopia_step_1:I = 0x7f0902c1

.field public static final information:I = 0x7f0900e3

.field public static final input_activity_alert_dialog_message:I = 0x7f090287

.field public static final input_activity_alert_dialog_message_float_range:I = 0x7f090288

.field public static final input_activity_alert_dialog_message_integer_range:I = 0x7f090289

.field public static final inputmodule_title:I = 0x7f090282

.field public static final inputmodule_unit:I = 0x7f090281

.field public static final insert:I = 0x7f090278

.field public static final inserting_dummy_data:I = 0x7f0902ae

.field public static final installing_updates:I = 0x7f090246

.field public static final invalid_input:I = 0x7f090286

.field public static final isens_step_1:I = 0x7f0902c4

.field public static final isens_step_2:I = 0x7f0902c5

.field public static final isens_step_3:I = 0x7f0902c6

.field public static final january:I = 0x7f0900fc

.field public static final january_short:I = 0x7f090108

.field public static final jnj_step_1:I = 0x7f0902c3

.field public static final join_trying_toast:I = 0x7f0902a6

.field public static final joined_successfully_toast:I = 0x7f0902a5

.field public static final july:I = 0x7f090102

.field public static final july_short:I = 0x7f09010e

.field public static final june:I = 0x7f090101

.field public static final june_short:I = 0x7f09010d

.field public static final kcal:I = 0x7f0900b9

.field public static final kg:I = 0x7f0900c0

.field public static final kies_restoration_failed:I = 0x7f090279

.field public static final kilograms:I = 0x7f0900c1

.field public static final km:I = 0x7f0900c7

.field public static final later:I = 0x7f09004c

.field public static final lb:I = 0x7f0900c2

.field public static final leave_toast:I = 0x7f0902ab

.field public static final link:I = 0x7f090043

.field public static final list:I = 0x7f090210

.field public static final list_expanded:I = 0x7f0901ef

.field public static final list_of_devices_supported:I = 0x7f09029b

.field public static final list_unexpanded:I = 0x7f090217

.field public static final litre_short:I = 0x7f090223

.field public static final loading:I = 0x7f0900b3

.field public static final loading_data:I = 0x7f0900b5

.field public static final log:I = 0x7f09005a

.field public static final march:I = 0x7f0900fe

.field public static final march_short:I = 0x7f09010a

.field public static final max_characters_alert:I = 0x7f0900b2

.field public static final may:I = 0x7f090100

.field public static final may_short:I = 0x7f09010c

.field public static final meter:I = 0x7f0900c8

.field public static final meter_short:I = 0x7f0900c9

.field public static final mg:I = 0x7f0900be

.field public static final mgdl:I = 0x7f0900cf

.field public static final mile:I = 0x7f0900ca

.field public static final mile_short:I = 0x7f0900cc

.field public static final miles:I = 0x7f0900cb

.field public static final millimole_short:I = 0x7f09021f

.field public static final min:I = 0x7f0900ea

.field public static final min_short:I = 0x7f0900ed

.field public static final mins:I = 0x7f0900eb

.field public static final minutes:I = 0x7f0900ec

.field public static final ml:I = 0x7f0900c4

.field public static final mmhg:I = 0x7f0900d1

.field public static final mmoll:I = 0x7f0900d0

.field public static final mon:I = 0x7f0900ee

.field public static final mon_short:I = 0x7f0900f5

.field public static final mon_sun:I = 0x7f090134

.field public static final month:I = 0x7f090068

.field public static final more_apps:I = 0x7f09002a

.field public static final more_options:I = 0x7f090030

.field public static final msg_alert_input_rename_device:I = 0x7f0900a2

.field public static final msg_popup_update:I = 0x7f090238

.field public static final msg_unlink_account:I = 0x7f09023a

.field public static final my_accessories:I = 0x7f09008f

.field public static final navigate_up:I = 0x7f09020f

.field public static final next:I = 0x7f090042

.field public static final no_blood_glucose_devices:I = 0x7f090298

.field public static final no_blood_pressure_devices:I = 0x7f090292

.field public static final no_connected_devices:I = 0x7f090094

.field public static final no_data:I = 0x7f09006a

.field public static final no_detected_accessories:I = 0x7f090092

.field public static final no_detected_accessories_with_dot:I = 0x7f090093

.field public static final no_sensor_data:I = 0x7f090187

.field public static final normal:I = 0x7f090061

.field public static final not_connected:I = 0x7f0900ae

.field public static final notes:I = 0x7f09007c

.field public static final notice:I = 0x7f0900e1

.field public static final notices:I = 0x7f0900e2

.field public static final notification_setup_profile_body:I = 0x7f090264

.field public static final notification_setup_profile_title:I = 0x7f090263

.field public static final november:I = 0x7f090106

.field public static final november_short:I = 0x7f090112

.field public static final october:I = 0x7f090105

.field public static final october_short:I = 0x7f090111

.field public static final ok:I = 0x7f090047

.field public static final on_started_toast:I = 0x7f0902a9

.field public static final on_stoped_toast:I = 0x7f0902aa

.field public static final openg_gear_manager:I = 0x7f0900ac

.field public static final option_menu_device_sensor:I = 0x7f09003e

.field public static final option_menu_dummy:I = 0x7f09003d

.field public static final or:I = 0x7f0901ad

.field public static final other:I = 0x7f09002d

.field public static final out_of_range_message:I = 0x7f0900b0

.field public static final outside_normal_range:I = 0x7f0901ab

.field public static final outside_normal_range_statistics_float:I = 0x7f09027a

.field public static final outside_normal_range_statistics_int:I = 0x7f09027b

.field public static final oz:I = 0x7f0900c5

.field public static final pair_confirmation_message:I = 0x7f090188

.field public static final pair_confirmation_message_hrm:I = 0x7f090189

.field public static final paired:I = 0x7f0900a1

.field public static final paired_accessories:I = 0x7f090090

.field public static final paired_accessory_title_heart_rate:I = 0x7f090023

.field public static final paired_blood_glucose_devices:I = 0x7f090296

.field public static final paired_blood_pressure_devices:I = 0x7f090290

.field public static final pairing_device_toast:I = 0x7f0902a1

.field public static final pairing_with:I = 0x7f0902a2

.field public static final password_locked_not_running_health_service:I = 0x7f090261

.field public static final pause:I = 0x7f090053

.field public static final pause_pedometer:I = 0x7f090054

.field public static final pedometer:I = 0x7f090020

.field public static final pedometer_acc:I = 0x7f090185

.field public static final per_serving_size:I = 0x7f09017c

.field public static final period:I = 0x7f0900d6

.field public static final period_automatic:I = 0x7f090129

.field public static final period_one_day:I = 0x7f09012d

.field public static final period_six_hours:I = 0x7f09012b

.field public static final period_three_days:I = 0x7f09012e

.field public static final period_three_hours:I = 0x7f09012a

.field public static final period_twelve_hours:I = 0x7f09012c

.field public static final photo:I = 0x7f09007e

.field public static final pm:I = 0x7f090182

.field public static final pm_upper:I = 0x7f0900e7

.field public static final popup_invalid_date:I = 0x7f090117

.field public static final popup_invalid_input_data:I = 0x7f090116

.field public static final pounds:I = 0x7f0900c3

.field public static final previous:I = 0x7f090041

.field public static final prime:I = 0x7f0900d3

.field public static final print:I = 0x7f090038

.field public static final progressing:I = 0x7f0900b6

.field public static final progressing_restore:I = 0x7f0900b7

.field public static final prompt_Not_tick:I = 0x7f09022c

.field public static final prompt_add:I = 0x7f0901e4

.field public static final prompt_add_picture:I = 0x7f090232

.field public static final prompt_average:I = 0x7f090204

.field public static final prompt_barcode_button:I = 0x7f09023c

.field public static final prompt_beats_per_minute:I = 0x7f090227

.field public static final prompt_bmi_scores:I = 0x7f090201

.field public static final prompt_button:I = 0x7f09020a

.field public static final prompt_cal:I = 0x7f0901d6

.field public static final prompt_calories:I = 0x7f0901c6

.field public static final prompt_camera_button:I = 0x7f0901de

.field public static final prompt_cancel:I = 0x7f09020c

.field public static final prompt_centimeters:I = 0x7f0901bf

.field public static final prompt_check_box:I = 0x7f09022f

.field public static final prompt_checked:I = 0x7f090230

.field public static final prompt_cm:I = 0x7f0901cf

.field public static final prompt_collapse:I = 0x7f09021b

.field public static final prompt_comma:I = 0x7f09020b

.field public static final prompt_continue:I = 0x7f09020d

.field public static final prompt_day_button:I = 0x7f090213

.field public static final prompt_decilitre:I = 0x7f090224

.field public static final prompt_delete:I = 0x7f0901e6

.field public static final prompt_disabled:I = 0x7f09022d

.field public static final prompt_double_tap_to_change:I = 0x7f090200

.field public static final prompt_double_tap_to_close:I = 0x7f090219

.field public static final prompt_double_tap_to_edit:I = 0x7f0901ff

.field public static final prompt_double_tap_to_open:I = 0x7f090218

.field public static final prompt_drag_hold:I = 0x7f090244

.field public static final prompt_drop_down_list:I = 0x7f0901ec

.field public static final prompt_expand:I = 0x7f09021a

.field public static final prompt_favourite_button:I = 0x7f0901df

.field public static final prompt_feet:I = 0x7f0901c0

.field public static final prompt_food_pick_add_food_button:I = 0x7f0901e2

.field public static final prompt_food_pick_search_barcode_button:I = 0x7f0901e0

.field public static final prompt_food_pick_search_voice_button:I = 0x7f0901e1

.field public static final prompt_ft:I = 0x7f0901d0

.field public static final prompt_gallery_button:I = 0x7f0901e8

.field public static final prompt_grams:I = 0x7f090228

.field public static final prompt_h:I = 0x7f0901da

.field public static final prompt_header:I = 0x7f0901fd

.field public static final prompt_hour_button:I = 0x7f090211

.field public static final prompt_hours:I = 0x7f0901cc

.field public static final prompt_hr:I = 0x7f0901db

.field public static final prompt_image:I = 0x7f0901e5

.field public static final prompt_in:I = 0x7f0901dc

.field public static final prompt_inches:I = 0x7f0901cd

.field public static final prompt_kcal:I = 0x7f0901d9

.field public static final prompt_kg:I = 0x7f0901d1

.field public static final prompt_kilocalories:I = 0x7f0901cb

.field public static final prompt_kilograms:I = 0x7f0901c1

.field public static final prompt_kilometers:I = 0x7f0901c3

.field public static final prompt_km:I = 0x7f0901d3

.field public static final prompt_lb:I = 0x7f0901d5

.field public static final prompt_lev_1:I = 0x7f090205

.field public static final prompt_lev_2:I = 0x7f090206

.field public static final prompt_lev_3:I = 0x7f090207

.field public static final prompt_lev_4:I = 0x7f090208

.field public static final prompt_lev_5:I = 0x7f090209

.field public static final prompt_litre:I = 0x7f090225

.field public static final prompt_m:I = 0x7f0901d4

.field public static final prompt_map_view:I = 0x7f09021e

.field public static final prompt_meters:I = 0x7f0901c4

.field public static final prompt_mi:I = 0x7f0901d2

.field public static final prompt_miles:I = 0x7f0901c2

.field public static final prompt_milligrams:I = 0x7f090221

.field public static final prompt_millimeter_mercury:I = 0x7f090226

.field public static final prompt_millimoles:I = 0x7f090220

.field public static final prompt_min:I = 0x7f0901dd

.field public static final prompt_minutes:I = 0x7f0901ce

.field public static final prompt_month_button:I = 0x7f090212

.field public static final prompt_music:I = 0x7f0901ee

.field public static final prompt_mypicture:I = 0x7f0901fe

.field public static final prompt_next:I = 0x7f0901f6

.field public static final prompt_not_checked:I = 0x7f090231

.field public static final prompt_not_selected:I = 0x7f0901f2

.field public static final prompt_open:I = 0x7f090214

.field public static final prompt_ounce:I = 0x7f090229

.field public static final prompt_per:I = 0x7f0901c7

.field public static final prompt_per_hour:I = 0x7f0901c8

.field public static final prompt_per_minute:I = 0x7f0901c9

.field public static final prompt_per_sec:I = 0x7f0901d8

.field public static final prompt_per_second:I = 0x7f0901ca

.field public static final prompt_per_symbol:I = 0x7f0901d7

.field public static final prompt_play:I = 0x7f0901ed

.field public static final prompt_portion_size:I = 0x7f0901e7

.field public static final prompt_pounds:I = 0x7f0901c5

.field public static final prompt_quick_input_button:I = 0x7f09023b

.field public static final prompt_radiobtn:I = 0x7f09022e

.field public static final prompt_score_board:I = 0x7f0901e9

.field public static final prompt_search_edit_text:I = 0x7f0901e3

.field public static final prompt_seconds:I = 0x7f090216

.field public static final prompt_seek_control:I = 0x7f090243

.field public static final prompt_selected:I = 0x7f0901f1

.field public static final prompt_skip:I = 0x7f0901f7

.field public static final prompt_start:I = 0x7f0901ea

.field public static final prompt_step_1:I = 0x7f0901f9

.field public static final prompt_step_2:I = 0x7f0901fa

.field public static final prompt_step_3:I = 0x7f0901fb

.field public static final prompt_step_4:I = 0x7f0901fc

.field public static final prompt_step_d:I = 0x7f0901f8

.field public static final prompt_stop:I = 0x7f0901eb

.field public static final prompt_tab:I = 0x7f090203

.field public static final prompt_tick:I = 0x7f09022b

.field public static final prompt_tick_box:I = 0x7f09022a

.field public static final prompt_today:I = 0x7f090215

.field public static final prompt_toggle:I = 0x7f0901f0

.field public static final prompt_view_picture:I = 0x7f090233

.field public static final ps_disconnected:I = 0x7f090096

.field public static final pulse:I = 0x7f0901b7

.field public static final range:I = 0x7f0900b1

.field public static final received_data:I = 0x7f09009e

.field public static final record_popup_text:I = 0x7f090271

.field public static final remove:I = 0x7f09003a

.field public static final rename:I = 0x7f09008b

.field public static final repeat:I = 0x7f0900e4

.field public static final reset:I = 0x7f090032

.field public static final reset_daily_data:I = 0x7f09003b

.field public static final reset_daily_data_title:I = 0x7f0902ad

.field public static final reset_daily_data_toast_message:I = 0x7f09028a

.field public static final reset_password:I = 0x7f090122

.field public static final reset_popup_text:I = 0x7f090062

.field public static final restore:I = 0x7f090139

.field public static final retry:I = 0x7f09004b

.field public static final s_health:I = 0x7f09001d

.field public static final s_health_main:I = 0x7f09001e

.field public static final s_will_be_disconnected:I = 0x7f090097

.field public static final samsung_activity_tracker:I = 0x7f090194

.field public static final samsung_app_store_disabled:I = 0x7f090237

.field public static final samsung_gear_fit_manager:I = 0x7f090193

.field public static final samsung_gear_manager:I = 0x7f090192

.field public static final samsung_hrm_text:I = 0x7f0902de

.field public static final samsung_scale:I = 0x7f0902ce

.field public static final samsung_scale_1:I = 0x7f0902c7

.field public static final samsung_scale_2:I = 0x7f0902c8

.field public static final samsung_scale_3:I = 0x7f0902c9

.field public static final samsung_scale_4:I = 0x7f0902ca

.field public static final samsung_scale_5:I = 0x7f0902cb

.field public static final samsung_scale_6:I = 0x7f0902cc

.field public static final samsung_scale_7:I = 0x7f0902cd

.field public static final samsung_watch_text:I = 0x7f0902df

.field public static final sat:I = 0x7f0900f3

.field public static final sat_short:I = 0x7f0900fa

.field public static final save:I = 0x7f09004f

.field public static final save_data_confirmation:I = 0x7f0902a0

.field public static final saved:I = 0x7f090080

.field public static final scan:I = 0x7f09004d

.field public static final scanning:I = 0x7f0900a3

.field public static final scanningfragment_failed_to_connect:I = 0x7f090245

.field public static final sdk_device_no_support_toast_message:I = 0x7f090236

.field public static final sdk_no_health_service_toast_message:I = 0x7f090235

.field public static final sdk_unsupport_toast_message:I = 0x7f090234

.field public static final search:I = 0x7f09007b

.field public static final select:I = 0x7f090034

.field public static final select_all:I = 0x7f090071

.field public static final select_password:I = 0x7f090120

.field public static final select_tag:I = 0x7f0902b3

.field public static final selected:I = 0x7f090075

.field public static final selected_format:I = 0x7f090074

.field public static final selected_item_deleted_message:I = 0x7f09011b

.field public static final selected_items_deleted_message:I = 0x7f09011a

.field public static final september:I = 0x7f090104

.field public static final september_short:I = 0x7f090110

.field public static final service_not_connected:I = 0x7f0902ac

.field public static final serving:I = 0x7f09017b

.field public static final set:I = 0x7f09004a

.field public static final set_date:I = 0x7f090079

.field public static final set_goal:I = 0x7f090031

.field public static final set_time:I = 0x7f09007a

.field public static final settings:I = 0x7f090037

.field public static final share_image_storage_error:I = 0x7f09018c

.field public static final share_via:I = 0x7f090033

.field public static final skip:I = 0x7f090045

.field public static final sleep:I = 0x7f09017e

.field public static final sleep_sleep_accessories:I = 0x7f09017f

.field public static final spo2:I = 0x7f090240

.field public static final spo2_tts:I = 0x7f090241

.field public static final start:I = 0x7f090050

.field public static final start_pedometer_util:I = 0x7f090051

.field public static final statistic_from_ada:I = 0x7f0901b2

.field public static final statistic_from_jnc7:I = 0x7f0901b9

.field public static final statistic_from_kda:I = 0x7f0901b3

.field public static final statistic_from_who:I = 0x7f0901b8

.field public static final statistics_ada:I = 0x7f09027c

.field public static final statistics_jnc_7:I = 0x7f09027d

.field public static final steps:I = 0x7f0900c6

.field public static final stop:I = 0x7f09004e

.field public static final stress:I = 0x7f090026

.field public static final stress_accessories:I = 0x7f090027

.field public static final sum:I = 0x7f090070

.field public static final summary_view:I = 0x7f09005f

.field public static final sun:I = 0x7f0900f4

.field public static final sun_sat:I = 0x7f090133

.field public static final sun_short:I = 0x7f0900fb

.field public static final sync_data:I = 0x7f0900a5

.field public static final sync_data_q:I = 0x7f0900a6

.field public static final sync_failed:I = 0x7f0900a7

.field public static final syncing_with_s:I = 0x7f0900a8

.field public static final systolic:I = 0x7f0901b5

.field public static final tanina_scaletype_1_text_1:I = 0x7f0902e2

.field public static final tanina_scaletype_1_text_2:I = 0x7f0902e3

.field public static final tanina_scaletype_2_text_1:I = 0x7f0902e4

.field public static final tanina_scaletype_2_text_2:I = 0x7f0902e5

.field public static final tanina_type_1_scale_text_1:I = 0x7f0902d1

.field public static final tanina_type_1_scale_text_2:I = 0x7f0902d2

.field public static final tanina_type_2_scale_text_1:I = 0x7f0902d3

.field public static final tanina_type_2_scale_text_2:I = 0x7f0902d4

.field public static final tap:I = 0x7f0902d0

.field public static final tap_here_to_unlock_shealth:I = 0x7f090262

.field public static final tap_uninstall:I = 0x7f090276

.field public static final temperature:I = 0x7f090130

.field public static final temperature_c:I = 0x7f0900cd

.field public static final temperature_f:I = 0x7f0900ce

.field public static final tgh_title:I = 0x7f090242

.field public static final thu:I = 0x7f0900f1

.field public static final thu_short:I = 0x7f0900f8

.field public static final time:I = 0x7f090136

.field public static final to_connect_the_accessory:I = 0x7f0902cf

.field public static final today:I = 0x7f09005d

.field public static final total:I = 0x7f09008a

.field public static final training_effect_level_easy:I = 0x7f09013c

.field public static final training_effect_level_hard:I = 0x7f09013e

.field public static final training_effect_level_low:I = 0x7f09013b

.field public static final training_effect_level_moderate:I = 0x7f09013d

.field public static final try_again:I = 0x7f090057

.field public static final tryit_add_device:I = 0x7f09028e

.field public static final tryit_device_added:I = 0x7f09028f

.field public static final tryit_finish_glucose:I = 0x7f090299

.field public static final tryit_finish_pressure:I = 0x7f090293

.field public static final tryit_finish_weight:I = 0x7f09029a

.field public static final tryit_scan:I = 0x7f09028d

.field public static final tue:I = 0x7f0900ef

.field public static final tue_short:I = 0x7f0900f6

.field public static final turning_on_bluetooth:I = 0x7f09008d

.field public static final twitter_share:I = 0x7f0901f5

.field public static final ultra_mini:I = 0x7f09029c

.field public static final unable_backup_during_shealth_running:I = 0x7f0900b8

.field public static final unable_to_print:I = 0x7f0900e5

.field public static final unfavorite:I = 0x7f090089

.field public static final uninstall_fitness_with_gear:I = 0x7f090275

.field public static final unknown_dialog_type_choose_type_exception:I = 0x7f09019b

.field public static final unlock_s_health:I = 0x7f090260

.field public static final unpair:I = 0x7f09008c

.field public static final unselect_all:I = 0x7f090073

.field public static final up:I = 0x7f0901a9

.field public static final update:I = 0x7f090055

.field public static final update_new_version_text:I = 0x7f0900db

.field public static final update_new_version_title:I = 0x7f0900dc

.field public static final update_now:I = 0x7f090056

.field public static final usb_step_2:I = 0x7f0902c2

.field public static final user_manual:I = 0x7f09003c

.field public static final user_name:I = 0x7f09001f

.field public static final uv:I = 0x7f09002f

.field public static final uv_accessories:I = 0x7f09023d

.field public static final value_after_meal:I = 0x7f09026a

.field public static final value_fasting:I = 0x7f090269

.field public static final vertical_progress_counter_default_value:I = 0x7f090285

.field public static final vertical_progressbar_counter_format:I = 0x7f090283

.field public static final vertical_progressbar_legend_format:I = 0x7f090284

.field public static final view_by:I = 0x7f090036

.field public static final walk_sync_notefound:I = 0x7f0902b2

.field public static final walk_sync_notefound_message:I = 0x7f0902b1

.field public static final walk_synctitle:I = 0x7f0902b0

.field public static final warning:I = 0x7f0900da

.field public static final watch:I = 0x7f090025

.field public static final wed:I = 0x7f0900f0

.field public static final wed_short:I = 0x7f0900f7

.field public static final week_format:I = 0x7f090131

.field public static final week_of_day:I = 0x7f090067

.field public static final weighing_scale:I = 0x7f09018d

.field public static final weight:I = 0x7f09002c

.field public static final weight_data_from_sensor_device:I = 0x7f09029e

.field public static final weight_data_receive_message:I = 0x7f09029f

.field public static final weightmachine_unit:I = 0x7f09026e

.field public static final weightmachine_unit_lb:I = 0x7f09026f

.field public static final within_normal_range:I = 0x7f0901ac

.field public static final workout_syncing:I = 0x7f0902af

.field public static final wrong_choose_type_exception:I = 0x7f090199

.field public static final wrong_dialog_type_exception:I = 0x7f090195

.field public static final wrong_password_message:I = 0x7f090126

.field public static final year:I = 0x7f090066

.field public static final yesterday:I = 0x7f09005e

.field public static final yyyyMMMdE:I = 0x7f0901a5


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1487
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
