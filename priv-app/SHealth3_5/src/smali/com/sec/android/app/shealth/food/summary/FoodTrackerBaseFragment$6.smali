.class Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;
.super Ljava/lang/Object;
.source "FoodTrackerBaseFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getContentInitializationListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;)V
    .locals 0

    .prologue
    .line 988
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$1500(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;

    .prologue
    .line 988
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;->showMealTypeSelectorPopup()V

    return-void
.end method

.method private showMealTypeSelectorPopup()V
    .locals 5

    .prologue
    .line 1065
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1097
    :goto_0
    return-void

    .line 1071
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    # invokes: Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->handleSelectedIndexForListPopup()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->access$1400(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;)V

    .line 1073
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    # getter for: Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mStringValuesMealTypes:[Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->access$1300(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;)[Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    # getter for: Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mMealTypeSelectorLL:Landroid/widget/LinearLayout;
    invoke-static {v4}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->access$1200(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;)Landroid/widget/LinearLayout;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;-><init>(Landroid/content/Context;[Ljava/lang/String;Landroid/view/View;)V

    # setter for: Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mListPopup:Lcom/sec/android/app/shealth/common/commonui/ListPopup;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->access$1702(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;Lcom/sec/android/app/shealth/common/commonui/ListPopup;)Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    .line 1074
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    # getter for: Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mListPopup:Lcom/sec/android/app/shealth/common/commonui/ListPopup;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->access$1700(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;)Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    # getter for: Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mSelectedIndexInPopup:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->access$1800(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->setCurrentItem(I)V

    .line 1075
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    # getter for: Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mListPopup:Lcom/sec/android/app/shealth/common/commonui/ListPopup;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->access$1700(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;)Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6$3;-><init>(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->setOnItemClickedListener(Lcom/sec/android/app/shealth/common/commonui/ListPopup$OnItemClickedListener;)V

    .line 1096
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    # getter for: Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mListPopup:Lcom/sec/android/app/shealth/common/commonui/ListPopup;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->access$1700(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;)Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    # getter for: Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mMealTypeSelectorLL:Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->access$1200(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;)Landroid/widget/LinearLayout;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v2

    rsub-int/lit8 v2, v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->show(II)V

    goto :goto_0
.end method


# virtual methods
.method public onContentInitialization(Landroid/view/View;Landroid/app/Activity;Landroid/app/Dialog;Landroid/os/Bundle;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V
    .locals 5
    .param p1, "content"    # Landroid/view/View;
    .param p2, "parentActivity"    # Landroid/app/Activity;
    .param p3, "dialog"    # Landroid/app/Dialog;
    .param p4, "savedInstanceState"    # Landroid/os/Bundle;
    .param p5, "okButtonHandler"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;

    .prologue
    .line 994
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->isAdded()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 999
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    const v1, 0x7f080493

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;

    iput-object v1, v2, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mQuickInputPanel:Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;

    .line 1001
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    iget-object v1, v1, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mQuickInputPanel:Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;

    invoke-static {}, Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider;->getCalories()[I

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->setCalories([I)V

    .line 1002
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    const v1, 0x7f080492

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    # setter for: Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mMealTypeSelector:Landroid/widget/TextView;
    invoke-static {v2, v1}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->access$1102(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 1003
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    const v1, 0x7f080491

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    # setter for: Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mMealTypeSelectorLL:Landroid/widget/LinearLayout;
    invoke-static {v2, v1}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->access$1202(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;

    .line 1008
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    # getter for: Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mMealTypeSelector:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->access$1100(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;)Landroid/widget/TextView;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    # getter for: Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mMealType:I
    invoke-static {v3}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->access$800(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;)I

    move-result v3

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->getMealTypeString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1010
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    # getter for: Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mMealTypeSelectorLL:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->access$1200(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;)Landroid/widget/LinearLayout;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    # getter for: Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mMealTypeSelector:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->access$1100(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0901ec

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->setContentDescription(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    .line 1012
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0e0025

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    # setter for: Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mStringValuesMealTypes:[Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->access$1302(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;[Ljava/lang/String;)[Ljava/lang/String;

    .line 1013
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    # invokes: Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->handleSelectedIndexForListPopup()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->access$1400(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;)V

    .line 1014
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    iget-object v1, v1, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mQuickInputPanel:Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    # getter for: Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mMealType:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->access$800(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->updateCalorieValues(I)V

    .line 1018
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    # getter for: Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mMealTypeSelectorLL:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->access$1200(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;)Landroid/widget/LinearLayout;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6$1;-><init>(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;)V

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1040
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    # getter for: Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mMealTypeSelectorLL:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->access$1200(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 1041
    .local v0, "viewTreeObserver":Landroid/view/ViewTreeObserver;
    invoke-virtual {v0}, Landroid/view/ViewTreeObserver;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1043
    new-instance v1, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6$2;-><init>(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1058
    .end local v0    # "viewTreeObserver":Landroid/view/ViewTreeObserver;
    :cond_0
    return-void
.end method
