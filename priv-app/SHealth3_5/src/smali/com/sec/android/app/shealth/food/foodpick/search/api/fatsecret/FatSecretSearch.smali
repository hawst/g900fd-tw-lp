.class public Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretSearch;
.super Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;
.source "FatSecretSearch.java"


# static fields
.field private static final AUTO_COMPLETE_SEARCH_MIN_LENGTH:I = 0x1

.field private static final GB_REGION:Ljava/lang/String;

.field private static final KEY:Ljava/lang/String; = "628882742fd845a0baf1cacaa1951877"

.field private static final LANG_QUERY_STRING:Ljava/lang/String; = "&lang="

.field private static final LOG_TAG:Ljava/lang/String;

.field private static final NONCE_LOL_OAUTH_VERSION_1_0:Ljava/lang/String; = "&nonce=LOL&oauth_version=1.0"

.field private static final OAUTH_SIGNATURE_METHOD_HMAC_SHA1:Ljava/lang/String; = "&oauth_signature_method=HMAC-SHA1"

.field private static final REGION_QUERY_STRING:Ljava/lang/String; = "&region="

.field private static final SECRET:Ljava/lang/String; = "1a6096fc6c4b4472ac5bdde1f0d33ac4"

.field private static final URL_BASE:Ljava/lang/String; = "http://platform.fatsecret.com/rest/server.api?"

.field private static final WRONG_ID_CODE:Ljava/lang/String; = "106"

.field private static final XML_RELOAD_TRIES_COUNT:I = 0x2


# instance fields
.field private mParser:Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    const-class v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretSearch;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->getLogTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretSearch;->LOG_TAG:Ljava/lang/String;

    .line 51
    sget-object v0, Ljava/util/Locale;->UK:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretSearch;->GB_REGION:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;-><init>(Landroid/content/Context;)V

    .line 65
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;

    invoke-direct {v0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretSearch;->mParser:Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;

    .line 66
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretSearch;->mParser:Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;

    new-instance v1, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretSearch$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretSearch$1;-><init>(Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretSearch;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->setOnFoodParsingCompleteListener(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/NotifyingFoodParser$OnFoodParsingCompleteListener;)V

    .line 73
    return-void
.end method

.method private checkDocumentErrors(Lorg/w3c/dom/Document;)V
    .locals 4
    .param p1, "doc"    # Lorg/w3c/dom/Document;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/ParseException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 159
    const-string v0, "error"

    invoke-interface {p1, v0}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v0

    invoke-interface {v0}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v0

    if-lez v0, :cond_0

    .line 160
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/IllegalInputDataException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "code"

    invoke-interface {p1, v2}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v2

    invoke-interface {v2, v3}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v2

    invoke-interface {v2}, Lorg/w3c/dom/Node;->getTextContent()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "message"

    invoke-interface {p1, v2}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v2

    invoke-interface {v2, v3}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v2

    invoke-interface {v2}, Lorg/w3c/dom/Node;->getTextContent()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/IllegalInputDataException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 163
    :cond_0
    return-void
.end method

.method private getDocumentForURL(Ljava/lang/String;)Lorg/w3c/dom/Document;
    .locals 9
    .param p1, "urlBase"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/apache/http/ParseException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    const/4 v4, 0x0

    .line 144
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/OAuthBase;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/OAuthBase;-><init>()V

    .line 145
    .local v0, "oAuth":Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/OAuthBase;
    new-instance v1, Ljava/net/URL;

    invoke-direct {v1, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 146
    .local v1, "url":Ljava/net/URL;
    new-instance v6, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/OAuthBase$Result;

    invoke-direct {v6}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/OAuthBase$Result;-><init>()V

    .line 147
    .local v6, "result":Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/OAuthBase$Result;
    const-string v2, "628882742fd845a0baf1cacaa1951877"

    const-string v3, "1a6096fc6c4b4472ac5bdde1f0d33ac4"

    move-object v5, v4

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/OAuthBase;->generateSignature(Ljava/net/URL;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/OAuthBase$Result;)V

    .line 148
    invoke-direct {p0, v6}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretSearch;->tryGetDocumentForEncodingResult(Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/OAuthBase$Result;)Lorg/w3c/dom/Document;

    move-result-object v7

    .line 149
    .local v7, "doc":Lorg/w3c/dom/Document;
    const-string v2, "error"

    invoke-interface {v7, v2}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v2

    invoke-interface {v2}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v2

    if-lez v2, :cond_0

    const-string v2, "code"

    invoke-interface {v7, v2}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v2

    invoke-interface {v2, v8}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v2

    invoke-interface {v2}, Lorg/w3c/dom/Node;->getTextContent()Ljava/lang/String;

    move-result-object v2

    const-string v3, "106"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v8, 0x1

    .line 151
    .local v8, "notFoundFoodById":Z
    :cond_0
    if-eqz v8, :cond_1

    .line 155
    :goto_0
    return-object v4

    .line 154
    :cond_1
    invoke-direct {p0, v7}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretSearch;->checkDocumentErrors(Lorg/w3c/dom/Document;)V

    move-object v4, v7

    .line 155
    goto :goto_0
.end method

.method private queryAutoComplete(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Document;
    .locals 3
    .param p1, "request"    # Ljava/lang/String;
    .param p2, "region"    # Ljava/lang/String;
    .param p3, "lang"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/apache/http/ParseException;
        }
    .end annotation

    .prologue
    .line 136
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "http://platform.fatsecret.com/rest/server.api?method=foods.autocomplete&oauth_consumer_key=628882742fd845a0baf1cacaa1951877&oauth_signature_method=HMAC-SHA1&nonce=LOL&oauth_version=1.0&expression="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&region="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&lang="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&max_results="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&flag_default_serving=true"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 140
    .local v0, "urlBase":Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretSearch;->getDocumentForURL(Ljava/lang/String;)Lorg/w3c/dom/Document;

    move-result-object v1

    return-object v1
.end method

.method private queryFoodCategories(Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Document;
    .locals 3
    .param p1, "region"    # Ljava/lang/String;
    .param p2, "lang"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/apache/http/ParseException;
        }
    .end annotation

    .prologue
    .line 198
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "http://platform.fatsecret.com/rest/server.api?method=food_categories.get&oauth_consumer_key=628882742fd845a0baf1cacaa1951877&oauth_signature_method=HMAC-SHA1&nonce=LOL&oauth_version=1.0&region="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&lang="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 201
    .local v0, "urlBase":Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretSearch;->getDocumentForURL(Ljava/lang/String;)Lorg/w3c/dom/Document;

    move-result-object v1

    return-object v1
.end method

.method private queryFoodIdByBarcode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Document;
    .locals 3
    .param p1, "barcode"    # Ljava/lang/String;
    .param p2, "region"    # Ljava/lang/String;
    .param p3, "lang"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/apache/http/ParseException;
        }
    .end annotation

    .prologue
    .line 220
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "http://platform.fatsecret.com/rest/server.api?method=food.find_id_for_barcode&oauth_consumer_key=628882742fd845a0baf1cacaa1951877&oauth_signature_method=HMAC-SHA1&region="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&lang="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&nonce=LOL&oauth_version=1.0"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&barcode="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 224
    .local v0, "urlBase":Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretSearch;->getDocumentForURL(Ljava/lang/String;)Lorg/w3c/dom/Document;

    move-result-object v1

    return-object v1
.end method

.method private queryFoodSubCategories(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Document;
    .locals 3
    .param p1, "foodCategoryId"    # Ljava/lang/String;
    .param p2, "region"    # Ljava/lang/String;
    .param p3, "lang"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/apache/http/ParseException;
        }
    .end annotation

    .prologue
    .line 205
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "http://platform.fatsecret.com/rest/server.api?method=food_sub_categories.get&oauth_consumer_key=628882742fd845a0baf1cacaa1951877&oauth_signature_method=HMAC-SHA1&nonce=LOL&oauth_version=1.0&food_category_id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&region="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&lang="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 208
    .local v0, "urlBase":Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretSearch;->getDocumentForURL(Ljava/lang/String;)Lorg/w3c/dom/Document;

    move-result-object v1

    return-object v1
.end method

.method private queryGetFood(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Document;
    .locals 3
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "region"    # Ljava/lang/String;
    .param p3, "lang"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/apache/http/ParseException;
        }
    .end annotation

    .prologue
    .line 212
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "http://platform.fatsecret.com/rest/server.api?method=food.get&include_sub_categories=true&oauth_consumer_key=628882742fd845a0baf1cacaa1951877&oauth_signature_method=HMAC-SHA1&nonce=LOL&oauth_version=1.0&food_id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&region="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&lang="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&flag_default_serving=true"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 216
    .local v0, "urlBase":Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretSearch;->getDocumentForURL(Ljava/lang/String;)Lorg/w3c/dom/Document;

    move-result-object v1

    return-object v1
.end method

.method private querySearchFood(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Document;
    .locals 3
    .param p1, "request"    # Ljava/lang/String;
    .param p2, "pageNumber"    # I
    .param p3, "perPageNumber"    # I
    .param p4, "region"    # Ljava/lang/String;
    .param p5, "lang"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/apache/http/ParseException;
        }
    .end annotation

    .prologue
    .line 190
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "http://platform.fatsecret.com/rest/server.api?method=foods.search&oauth_consumer_key=628882742fd845a0baf1cacaa1951877&oauth_signature_method=HMAC-SHA1&nonce=LOL&oauth_version=1.0&search_expression="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&page_number="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&region="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&lang="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&flag_default_serving=true"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&max_results="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 194
    .local v0, "urlBase":Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretSearch;->getDocumentForURL(Ljava/lang/String;)Lorg/w3c/dom/Document;

    move-result-object v1

    return-object v1
.end method

.method private tryGetDocumentForEncodingResult(Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/OAuthBase$Result;)Lorg/w3c/dom/Document;
    .locals 9
    .param p1, "result"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/OAuthBase$Result;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 166
    const/4 v3, 0x2

    .line 167
    .local v3, "triesNumber":I
    new-instance v2, Ljava/io/IOException;

    invoke-direct {v2}, Ljava/io/IOException;-><init>()V

    .line 168
    .local v2, "rethrown":Ljava/io/IOException;
    :goto_0
    if-lez v3, :cond_0

    .line 170
    :try_start_0
    iget-object v4, p1, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/OAuthBase$Result;->normalizedUrl:Ljava/lang/String;

    sget-object v5, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/HttpRequestMethod;->POST:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/HttpRequestMethod;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p1, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/OAuthBase$Result;->normalizedRequestParameters:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "&"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "oauth_signature"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p1, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/OAuthBase$Result;->signature:Ljava/lang/String;

    const-string/jumbo v8, "utf-8"

    invoke-static {v7, v8}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-static {v4, v5, v6, v7}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretSearch;->doHttpMethodReq(Ljava/lang/String;Lcom/sec/android/app/shealth/food/foodpick/search/api/base/HttpRequestMethod;Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v1

    .line 177
    .local v1, "response":Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretSearch;->loadXmlDocument(Ljava/lang/String;)Lorg/w3c/dom/Document;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    return-object v4

    .line 178
    .end local v1    # "response":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 179
    .local v0, "ex":Ljava/io/IOException;
    move-object v2, v0

    .line 180
    sget-object v4, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretSearch;->LOG_TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "failed to load xml, try: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/common/utils/Logging;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    sget-object v4, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretSearch;->LOG_TAG:Ljava/lang/String;

    invoke-static {v4, v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 183
    add-int/lit8 v3, v3, -0x1

    goto :goto_0

    .line 185
    .end local v0    # "ex":Ljava/io/IOException;
    :cond_0
    throw v2
.end method


# virtual methods
.method protected checkRegion(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "region"    # Ljava/lang/String;

    .prologue
    .line 229
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;->checkRegion(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 256
    const-string v0, "CA"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 257
    const-string v0, "fr"

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretSearch;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 258
    const-string p1, "FR"

    .line 262
    :cond_0
    return-object p1
.end method

.method public getAutoCompleteMinLength()I
    .locals 1

    .prologue
    .line 132
    const/4 v0, 0x1

    return v0
.end method

.method public getServerSourceType()I
    .locals 1

    .prologue
    .line 77
    const v0, 0x46cd2

    return v0
.end method

.method protected performAutoCompleteSearchRealization(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchRequest;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/AutoCompleteSearchResult;
    .locals 4
    .param p1, "request"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/ParseException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 83
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchRequest;->getStringRequest()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretSearch;->getCountry()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretSearch;->getLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretSearch;->queryAutoComplete(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Document;

    move-result-object v0

    .line 84
    .local v0, "doc":Lorg/w3c/dom/Document;
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretSearch;->mParser:Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->parseAutoCompleteSearch(Lorg/w3c/dom/Document;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/AutoCompleteSearchResult;

    move-result-object v1

    return-object v1
.end method

.method protected performBarcodeSearchRealization(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchRequest;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;
    .locals 7
    .param p1, "request"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/ParseException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 120
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchRequest;->getStringRequest()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretSearch;->getCountry()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretSearch;->getLanguage()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v4, v5, v6}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretSearch;->queryFoodIdByBarcode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Document;

    move-result-object v0

    .line 121
    .local v0, "doc":Lorg/w3c/dom/Document;
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretSearch;->mParser:Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;

    invoke-virtual {v4, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->parseBarcodeSearch(Lorg/w3c/dom/Document;)Ljava/lang/String;

    move-result-object v2

    .line 122
    .local v2, "foodId":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretSearch;->getCountry()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretSearch;->getLanguage()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v2, v4, v5}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretSearch;->queryGetFood(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Document;

    move-result-object v0

    .line 123
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretSearch;->mParser:Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;

    invoke-virtual {v4, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->parseExtraFood(Lorg/w3c/dom/Document;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;

    move-result-object v1

    .line 124
    .local v1, "extraFoodInfoResult":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretSearch;->mParser:Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;

    invoke-virtual {v4, v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->parseFoodInfoFromExtraFoodInfoResult(Lorg/w3c/dom/Document;Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;)Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    move-result-object v3

    .line 125
    .local v3, "foodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretSearch;->getLanguage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setServerLocale(Ljava/lang/String;)V

    .line 126
    new-instance v4, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoRequest;

    invoke-direct {v4, v3}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoRequest;-><init>(Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;)V

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;->setRequest(Ljava/lang/Object;)V

    .line 127
    return-object v1
.end method

.method protected performCategorySearchRealization()Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryResult;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/ParseException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 98
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretSearch;->getCountry()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretSearch;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretSearch;->queryFoodCategories(Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Document;

    move-result-object v0

    .line 99
    .local v0, "doc":Lorg/w3c/dom/Document;
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretSearch;->mParser:Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->parseCategorySearch(Lorg/w3c/dom/Document;)Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryResult;

    move-result-object v1

    return-object v1
.end method

.method protected performExtraFoodInfoRequestRealization(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoRequest;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;
    .locals 6
    .param p1, "extraFoodInfoRequest"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/ParseException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 111
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoRequest;->getFoodInfoData()Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    move-result-object v2

    .line 112
    .local v2, "foodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getServerId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretSearch;->getCountry()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretSearch;->getLanguage()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v3, v4, v5}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretSearch;->queryGetFood(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Document;

    move-result-object v0

    .line 113
    .local v0, "doc":Lorg/w3c/dom/Document;
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretSearch;->mParser:Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;

    invoke-virtual {v3, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->parseExtraFood(Lorg/w3c/dom/Document;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;

    move-result-object v1

    .line 114
    .local v1, "extraFoodInfoResult":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->calculateKcalForFoodInfoData(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;)F

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setkCal(F)V

    .line 115
    return-object v1
.end method

.method protected performFoodSearchRealization(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult;
    .locals 7
    .param p1, "req"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;",
            ")",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/ParseException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 90
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretSearch;->getCountry()Ljava/lang/String;

    move-result-object v4

    .line 91
    .local v4, "region":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretSearch;->getLanguage()Ljava/lang/String;

    move-result-object v5

    .line 92
    .local v5, "lang":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;->getEncodedStringRequest()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;->getPageNumber()I

    move-result v2

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;->getPerPageNumber()I

    move-result v3

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretSearch;->querySearchFood(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Document;

    move-result-object v6

    .line 93
    .local v6, "document":Lorg/w3c/dom/Document;
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretSearch;->mParser:Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;

    invoke-virtual {v0, v6}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->parseFoodSearch(Lorg/w3c/dom/Document;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult;

    move-result-object v0

    return-object v0
.end method

.method protected performSubCategorySearchRealization(Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodSubCategoryRequest;)Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategoryResult;
    .locals 4
    .param p1, "foodSubCategoryRequest"    # Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodSubCategoryRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/ParseException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 105
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodSubCategoryRequest;->getCategory()Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategory;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretSearch;->getCountry()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretSearch;->getLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretSearch;->queryFoodSubCategories(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Document;

    move-result-object v0

    .line 106
    .local v0, "doc":Lorg/w3c/dom/Document;
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretSearch;->mParser:Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->parseSubCategorySearch(Lorg/w3c/dom/Document;)Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategoryResult;

    move-result-object v1

    return-object v1
.end method
