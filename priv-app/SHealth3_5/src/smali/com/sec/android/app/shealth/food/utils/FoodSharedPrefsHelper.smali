.class public Lcom/sec/android/app/shealth/food/utils/FoodSharedPrefsHelper;
.super Lcom/sec/android/app/shealth/common/utils/SharedPreferencesHelper;
.source "FoodSharedPrefsHelper.java"


# static fields
.field private static final FOOD_PICK_SELECTED_FRAGMENT_NAME:Ljava/lang/String; = "FOOD_PICK_SELECTED_FRAGMENT_NAME"

.field private static final FOOD_PICK_SELECTED_FRAGMENT_NAME_DEFAULT:Ljava/lang/String; = "category"

.field private static final FOOD_PICK_SELECTED_GRAPH_VIEW:Ljava/lang/String; = "FOOD_PICK_SELECTED_GRAPH_VIEW"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/common/utils/SharedPreferencesHelper;-><init>(Landroid/content/Context;)V

    .line 50
    return-void
.end method


# virtual methods
.method public getFoodGraphLastView()I
    .locals 2

    .prologue
    .line 87
    const-string v0, "FOOD_PICK_SELECTED_GRAPH_VIEW"

    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->ordinal()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/food/utils/FoodSharedPrefsHelper;->readInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getFoodPickLastFragmentName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 77
    const-string v0, "FOOD_PICK_SELECTED_FRAGMENT_NAME"

    const-string v1, "category"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/food/utils/FoodSharedPrefsHelper;->readString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public putFoodGraphLastView(I)V
    .locals 1
    .param p1, "graphViewOrdinal"    # I

    .prologue
    .line 67
    const-string v0, "FOOD_PICK_SELECTED_GRAPH_VIEW"

    invoke-virtual {p0, v0, p1}, Lcom/sec/android/app/shealth/food/utils/FoodSharedPrefsHelper;->writeInt(Ljava/lang/String;I)V

    .line 69
    return-void
.end method

.method public putFoodPickLastFragmentName(Ljava/lang/String;)V
    .locals 1
    .param p1, "fragmentName"    # Ljava/lang/String;

    .prologue
    .line 58
    const-string v0, "FOOD_PICK_SELECTED_FRAGMENT_NAME"

    invoke-virtual {p0, v0, p1}, Lcom/sec/android/app/shealth/food/utils/FoodSharedPrefsHelper;->writeString(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    return-void
.end method
