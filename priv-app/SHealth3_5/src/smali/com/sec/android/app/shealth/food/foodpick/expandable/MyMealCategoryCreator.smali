.class public Lcom/sec/android/app/shealth/food/foodpick/expandable/MyMealCategoryCreator;
.super Ljava/lang/Object;
.source "MyMealCategoryCreator.java"

# interfaces
.implements Lcom/sec/android/app/shealth/food/foodpick/expandable/IFoodCategoryListItemsCreator;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/expandable/MyMealCategoryCreator;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 62
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/MyMealCategoryCreator$1;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/MyMealCategoryCreator$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/MyMealCategoryCreator;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x0

    return v0
.end method

.method public getFoodListItems(Landroid/content/Context;)Ljava/util/List;
    .locals 16
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 37
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 38
    .local v10, "myMealList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;>;"
    new-instance v6, Lcom/sec/android/app/shealth/food/fooddao/MealDaoMyFoodImplDb;

    move-object/from16 v0, p1

    invoke-direct {v6, v0}, Lcom/sec/android/app/shealth/food/fooddao/MealDaoMyFoodImplDb;-><init>(Landroid/content/Context;)V

    .line 39
    .local v6, "mealDao":Lcom/sec/android/app/shealth/food/fooddao/MealDao;
    new-instance v8, Lcom/sec/android/app/shealth/food/fooddao/MealItemDaoImplDb;

    move-object/from16 v0, p1

    invoke-direct {v8, v0}, Lcom/sec/android/app/shealth/food/fooddao/MealItemDaoImplDb;-><init>(Landroid/content/Context;)V

    .line 40
    .local v8, "mealItemDao":Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;
    new-instance v2, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;

    move-object/from16 v0, p1

    invoke-direct {v2, v0}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;-><init>(Landroid/content/Context;)V

    .line 41
    .local v2, "foodInfoDao":Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;
    new-instance v12, Lcom/sec/android/app/shealth/food/fooddao/MealDaoMyFoodImplDb;

    move-object/from16 v0, p1

    invoke-direct {v12, v0}, Lcom/sec/android/app/shealth/food/fooddao/MealDaoMyFoodImplDb;-><init>(Landroid/content/Context;)V

    invoke-virtual {v12}, Lcom/sec/android/app/shealth/food/fooddao/MealDaoMyFoodImplDb;->getAllDatas()Ljava/util/List;

    move-result-object v11

    .line 42
    .local v11, "myMeals":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;>;"
    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    .line 43
    .local v5, "meal":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    new-instance v1, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableFoodListItem;

    invoke-direct {v1, v5, v6}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableFoodListItem;-><init>(Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;Lcom/sec/android/app/shealth/food/fooddao/MealDao;)V

    .line 44
    .local v1, "expandableItem":Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableFoodListItem;
    invoke-virtual {v5}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getId()J

    move-result-wide v12

    invoke-interface {v8, v12, v13}, Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;->getMealItemsListByMealId(J)Ljava/util/List;

    move-result-object v9

    .line 45
    .local v9, "mealItems":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;>;"
    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    .line 46
    .local v7, "mealItem":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;
    new-instance v13, Lcom/sec/android/app/shealth/food/foodpick/expandable/ChildFoodListItem;

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->getFoodInfoId()J

    move-result-wide v14

    invoke-interface {v2, v14, v15}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;->getDataById(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;

    move-result-object v12

    check-cast v12, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    invoke-direct {v13, v7, v12}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ChildFoodListItem;-><init>(Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;)V

    invoke-virtual {v1, v13}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableFoodListItem;->addChild(Lcom/sec/android/app/shealth/food/foodpick/expandable/ChildFoodListItem;)V

    goto :goto_1

    .line 48
    .end local v7    # "mealItem":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;
    :cond_0
    invoke-interface {v10, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 50
    .end local v1    # "expandableItem":Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableFoodListItem;
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v5    # "meal":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    .end local v9    # "mealItems":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;>;"
    :cond_1
    return-object v10
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "parcel"    # Landroid/os/Parcel;
    .param p2, "i"    # I

    .prologue
    .line 60
    return-void
.end method
