.class public Lcom/sec/android/app/shealth/food/graph/FoodSchartHandlerData;
.super Ljava/lang/Object;
.source "FoodSchartHandlerData.java"


# static fields
.field private static final MEAL_TYPE_SPARSE_ARRAY:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/sec/android/app/shealth/food/constants/MealType;",
            ">;"
        }
    .end annotation
.end field

.field private static final NO_VALUE:F = -1.0E-7f


# instance fields
.field private mCaloriesSum:D

.field private mMaxCaloriesValue:D

.field private mMaxCaloriesValueMealType:Lcom/sec/android/app/shealth/food/constants/MealType;

.field private mMealsCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    new-instance v0, Lcom/sec/android/app/shealth/food/graph/FoodSchartHandlerData$1;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/food/graph/FoodSchartHandlerData$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/food/graph/FoodSchartHandlerData;->MEAL_TYPE_SPARSE_ARRAY:Landroid/util/SparseArray;

    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/sdk/chart/view/SchartHandlerData;)V
    .locals 2
    .param p1, "handlerData"    # Lcom/samsung/android/sdk/chart/view/SchartHandlerData;

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/food/graph/FoodSchartHandlerData;->mMaxCaloriesValue:D

    .line 43
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/graph/FoodSchartHandlerData;->countMaxAndSum(Lcom/samsung/android/sdk/chart/view/SchartHandlerData;)V

    .line 44
    return-void
.end method

.method private countMaxAndSum(Lcom/samsung/android/sdk/chart/view/SchartHandlerData;)V
    .locals 7
    .param p1, "schartHandlerData"    # Lcom/samsung/android/sdk/chart/view/SchartHandlerData;

    .prologue
    .line 47
    const/4 v1, -0x1

    .line 48
    .local v1, "indexMaxValue":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesYValueList()Ljava/util/Vector;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 49
    invoke-virtual {p1}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesYValueList()Ljava/util/Vector;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    iget-wide v4, p0, Lcom/sec/android/app/shealth/food/graph/FoodSchartHandlerData;->mMaxCaloriesValue:D

    cmpl-double v2, v2, v4

    if-ltz v2, :cond_0

    .line 50
    invoke-virtual {p1}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesYValueList()Ljava/util/Vector;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/app/shealth/food/graph/FoodSchartHandlerData;->mMaxCaloriesValue:D

    .line 51
    move v1, v0

    .line 53
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesYValueList()Ljava/util/Vector;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    const-wide v4, -0x4185280d60000000L    # -1.0000000116860974E-7

    cmpl-double v2, v2, v4

    if-lez v2, :cond_1

    .line 54
    iget-wide v3, p0, Lcom/sec/android/app/shealth/food/graph/FoodSchartHandlerData;->mCaloriesSum:D

    invoke-virtual {p1}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesYValueList()Ljava/util/Vector;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v5

    add-double v2, v3, v5

    iput-wide v2, p0, Lcom/sec/android/app/shealth/food/graph/FoodSchartHandlerData;->mCaloriesSum:D

    .line 55
    iget v2, p0, Lcom/sec/android/app/shealth/food/graph/FoodSchartHandlerData;->mMealsCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/sec/android/app/shealth/food/graph/FoodSchartHandlerData;->mMealsCount:I

    .line 48
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 58
    :cond_2
    sget-object v2, Lcom/sec/android/app/shealth/food/graph/FoodSchartHandlerData;->MEAL_TYPE_SPARSE_ARRAY:Landroid/util/SparseArray;

    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/food/constants/MealType;

    iput-object v2, p0, Lcom/sec/android/app/shealth/food/graph/FoodSchartHandlerData;->mMaxCaloriesValueMealType:Lcom/sec/android/app/shealth/food/constants/MealType;

    .line 59
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 7
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 88
    if-ne p0, p1, :cond_1

    .line 107
    :cond_0
    :goto_0
    return v1

    .line 91
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    :cond_2
    move v1, v2

    .line 92
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 94
    check-cast v0, Lcom/sec/android/app/shealth/food/graph/FoodSchartHandlerData;

    .line 95
    .local v0, "that":Lcom/sec/android/app/shealth/food/graph/FoodSchartHandlerData;
    iget-wide v3, v0, Lcom/sec/android/app/shealth/food/graph/FoodSchartHandlerData;->mCaloriesSum:D

    iget-wide v5, p0, Lcom/sec/android/app/shealth/food/graph/FoodSchartHandlerData;->mCaloriesSum:D

    invoke-static {v3, v4, v5, v6}, Ljava/lang/Double;->compare(DD)I

    move-result v3

    if-eqz v3, :cond_4

    move v1, v2

    .line 96
    goto :goto_0

    .line 98
    :cond_4
    iget-wide v3, v0, Lcom/sec/android/app/shealth/food/graph/FoodSchartHandlerData;->mMaxCaloriesValue:D

    iget-wide v5, p0, Lcom/sec/android/app/shealth/food/graph/FoodSchartHandlerData;->mMaxCaloriesValue:D

    invoke-static {v3, v4, v5, v6}, Ljava/lang/Double;->compare(DD)I

    move-result v3

    if-eqz v3, :cond_5

    move v1, v2

    .line 99
    goto :goto_0

    .line 101
    :cond_5
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/graph/FoodSchartHandlerData;->mMaxCaloriesValueMealType:Lcom/sec/android/app/shealth/food/constants/MealType;

    iget-object v4, v0, Lcom/sec/android/app/shealth/food/graph/FoodSchartHandlerData;->mMaxCaloriesValueMealType:Lcom/sec/android/app/shealth/food/constants/MealType;

    if-eq v3, v4, :cond_6

    move v1, v2

    .line 102
    goto :goto_0

    .line 104
    :cond_6
    iget v3, p0, Lcom/sec/android/app/shealth/food/graph/FoodSchartHandlerData;->mMealsCount:I

    iget v4, v0, Lcom/sec/android/app/shealth/food/graph/FoodSchartHandlerData;->mMealsCount:I

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 105
    goto :goto_0
.end method

.method public getCaloriesSum()D
    .locals 2

    .prologue
    .line 83
    iget-wide v0, p0, Lcom/sec/android/app/shealth/food/graph/FoodSchartHandlerData;->mCaloriesSum:D

    return-wide v0
.end method

.method public getMaxCaloriesValue()D
    .locals 2

    .prologue
    .line 72
    iget-wide v0, p0, Lcom/sec/android/app/shealth/food/graph/FoodSchartHandlerData;->mMaxCaloriesValue:D

    return-wide v0
.end method

.method public getMealTypeMaxValue()Lcom/sec/android/app/shealth/food/constants/MealType;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/graph/FoodSchartHandlerData;->mMaxCaloriesValueMealType:Lcom/sec/android/app/shealth/food/constants/MealType;

    return-object v0
.end method

.method public getmMealsCount()I
    .locals 1

    .prologue
    .line 65
    iget v0, p0, Lcom/sec/android/app/shealth/food/graph/FoodSchartHandlerData;->mMealsCount:I

    return v0
.end method

.method public hashCode()I
    .locals 6

    .prologue
    const/16 v5, 0x20

    .line 114
    iget-wide v3, p0, Lcom/sec/android/app/shealth/food/graph/FoodSchartHandlerData;->mCaloriesSum:D

    invoke-static {v3, v4}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v1

    .line 115
    .local v1, "temp":J
    ushr-long v3, v1, v5

    xor-long/2addr v3, v1

    long-to-int v0, v3

    .line 116
    .local v0, "result":I
    iget-wide v3, p0, Lcom/sec/android/app/shealth/food/graph/FoodSchartHandlerData;->mMaxCaloriesValue:D

    invoke-static {v3, v4}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v1

    .line 117
    mul-int/lit8 v3, v0, 0x1f

    ushr-long v4, v1, v5

    xor-long/2addr v4, v1

    long-to-int v4, v4

    add-int v0, v3, v4

    .line 118
    mul-int/lit8 v4, v0, 0x1f

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/graph/FoodSchartHandlerData;->mMaxCaloriesValueMealType:Lcom/sec/android/app/shealth/food/constants/MealType;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/graph/FoodSchartHandlerData;->mMaxCaloriesValueMealType:Lcom/sec/android/app/shealth/food/constants/MealType;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/food/constants/MealType;->hashCode()I

    move-result v3

    :goto_0
    add-int v0, v4, v3

    .line 119
    return v0

    .line 118
    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method
