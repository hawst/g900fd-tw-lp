.class Lcom/sec/android/app/shealth/food/foodpick/search/frequent/OnFoodFavoriteClickListener$1;
.super Lcom/sec/android/app/shealth/food/foodpick/loader/Task;
.source "OnFoodFavoriteClickListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/foodpick/search/frequent/OnFoodFavoriteClickListener;->onFavoriteClick(Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sec/android/app/shealth/food/foodpick/loader/Task",
        "<",
        "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/OnFoodFavoriteClickListener;

.field final synthetic val$listItem:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/search/frequent/OnFoodFavoriteClickListener;Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;)V
    .locals 0

    .prologue
    .line 71
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/OnFoodFavoriteClickListener$1;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/OnFoodFavoriteClickListener;

    iput-object p2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/OnFoodFavoriteClickListener$1;->val$listItem:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;

    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/loader/Task;-><init>()V

    return-void
.end method


# virtual methods
.method protected accompanyWithLoadingDialog()Z
    .locals 1

    .prologue
    .line 74
    const/4 v0, 0x0

    return v0
.end method

.method public doTask()Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 88
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/OnFoodFavoriteClickListener$1;->val$listItem:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;->getFoodInfoItem()Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    move-result-object v0

    .line 89
    .local v0, "currentFoodInfo":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    new-instance v2, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoRequest;

    invoke-direct {v2, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoRequest;-><init>(Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;)V

    .line 92
    .local v2, "request":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoRequest;
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/OnFoodFavoriteClickListener$1;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/OnFoodFavoriteClickListener;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/search/frequent/OnFoodFavoriteClickListener;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/OnFoodFavoriteClickListener;->access$000(Lcom/sec/android/app/shealth/food/foodpick/search/frequent/OnFoodFavoriteClickListener;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getServerSourceType()I

    move-result v4

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchFactory;->getSearchApiBySourceType(Landroid/content/Context;I)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;

    move-result-object v1

    .line 93
    .local v1, "foodSearchApi":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;
    if-nez v1, :cond_0

    .line 95
    new-instance v3, Ljava/lang/AssertionError;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "foodSearchApi is null Bad ServerSourceType :  "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getServerSourceType()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3

    .line 99
    :cond_0
    invoke-interface {v1, v2}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;->performExtraFoodInfoRequest(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoRequest;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;

    move-result-object v3

    return-object v3
.end method

.method public onTaskCompleted(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;)V
    .locals 2
    .param p1, "extraFoodResult"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;

    .prologue
    .line 108
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/OnFoodFavoriteClickListener$1;->val$listItem:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;->saveToDatabase()V

    .line 109
    new-instance v0, Lcom/sec/android/app/shealth/food/fooddao/ExtraFoodInfoDaoImplDb;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/OnFoodFavoriteClickListener$1;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/OnFoodFavoriteClickListener;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/search/frequent/OnFoodFavoriteClickListener;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/OnFoodFavoriteClickListener;->access$000(Lcom/sec/android/app/shealth/food/foodpick/search/frequent/OnFoodFavoriteClickListener;)Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/food/fooddao/ExtraFoodInfoDaoImplDb;-><init>(Landroid/content/Context;)V

    .line 110
    .local v0, "extraFoodInfoDao":Lcom/sec/android/app/shealth/food/fooddao/ExtraFoodInfoDao;
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;->getSearchResult()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/food/fooddao/ExtraFoodInfoDao;->insertOrUpdateData(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;)J

    .line 111
    return-void
.end method

.method public bridge synthetic onTaskCompleted(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;

    .prologue
    .line 71
    check-cast p1, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;

    .end local p1    # "x0":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/OnFoodFavoriteClickListener$1;->onTaskCompleted(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;)V

    return-void
.end method

.method protected withErrorToast()Z
    .locals 1

    .prologue
    .line 115
    const/4 v0, 0x0

    return v0
.end method
