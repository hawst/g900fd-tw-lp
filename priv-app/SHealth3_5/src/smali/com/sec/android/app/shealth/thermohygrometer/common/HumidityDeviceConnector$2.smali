.class Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector$2;
.super Ljava/lang/Object;
.source "HumidityDeviceConnector.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;)V
    .locals 0

    .prologue
    .line 351
    iput-object p1, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector$2;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(I)V
    .locals 2
    .param p1, "error"    # I

    .prologue
    .line 383
    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Binding is done - Service connected"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 384
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector$2;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;

    # invokes: Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->startDevice()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->access$400(Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;)V

    .line 385
    return-void
.end method

.method public onServiceDisconnected(I)V
    .locals 3
    .param p1, "error"    # I

    .prologue
    .line 354
    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Binding - Service disconnected"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 356
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector$2;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;

    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    invoke-static {v1}, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->access$100(Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 357
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector$2;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;

    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->IsDeviceStarted:Z
    invoke-static {v1}, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->access$200(Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_3

    move-result v1

    if-eqz v1, :cond_0

    .line 359
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector$2;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;

    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    invoke-static {v1}, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->access$100(Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->stopReceivingData()V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_1 .. :try_end_1} :catch_3

    .line 367
    :cond_0
    :goto_0
    :try_start_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector$2;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->IsDeviceStarted:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->access$202(Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;Z)Z

    .line 368
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector$2;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;

    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    invoke-static {v1}, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->access$100(Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->leave()V

    .line 369
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector$2;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->access$102(Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_2 .. :try_end_2} :catch_3

    .line 377
    :cond_1
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector$2;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;

    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;
    invoke-static {v1}, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->access$300(Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 378
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector$2;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;

    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;
    invoke-static {v1}, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->access$300(Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->close()V

    .line 379
    :cond_2
    return-void

    .line 360
    :catch_0
    move-exception v0

    .line 361
    .local v0, "e":Ljava/lang/IllegalStateException;
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_0

    .line 371
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_1
    move-exception v0

    .line 372
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1

    .line 362
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_2
    move-exception v0

    .line 364
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    :try_start_4
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;->printStackTrace()V
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_4 .. :try_end_4} :catch_3

    goto :goto_0

    .line 373
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    :catch_3
    move-exception v0

    .line 374
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto :goto_1
.end method
