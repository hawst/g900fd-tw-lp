.class public Lcom/sec/android/app/shealth/framework/repository/common/DBTables$ImagesTable;
.super Ljava/lang/Object;
.source "DBTables.java"

# interfaces
.implements Landroid/provider/BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/framework/repository/common/DBTables;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ImagesTable"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/framework/repository/common/DBTables$ImagesTable$SYNC_KEYS;
    }
.end annotation


# static fields
.field public static final KEY_APPLICATION_NAME:Ljava/lang/String; = "app_name"

.field public static final KEY_FILE_PATH:Ljava/lang/String; = "file_path"

.field public static final KEY_MEASURE_ID:Ljava/lang/String; = "measure_id"

.field public static final KEY_MEASURE_TYPE_ID:Ljava/lang/String; = "measure_type_id"

.field public static final KEY_SYNC_BACKUP:Ljava/lang/String; = "sync_key"

.field public static final TABLE_NAME:Ljava/lang/String; = "images_table"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 540
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 542
    return-void
.end method
