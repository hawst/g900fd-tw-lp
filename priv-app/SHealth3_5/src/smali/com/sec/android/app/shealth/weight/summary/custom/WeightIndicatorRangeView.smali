.class Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;
.super Landroid/view/View;
.source "WeightIndicatorRangeView.java"


# static fields
.field private static final ARC_DRAW_DIFF:I = -0x5a


# instance fields
.field private mBitmap:Landroid/graphics/Bitmap;

.field private mBitmapCanvas:Landroid/graphics/Canvas;

.field private mCutPaint:Landroid/graphics/Paint;

.field private mCutRect:Landroid/graphics/RectF;

.field private mMaxNormalWeightRange:F

.field private mMinNormalWeightRange:F

.field private mNormalRangePaint:Landroid/graphics/Paint;

.field private mRadius:I

.field private mRangeRadius:I

.field private mRotateAngleDiff:F

.field private mStartAngle:F

.field private mSweepNormalAngle:F

.field private mWeightValue:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/high16 v0, -0x40800000    # -1.0f

    .line 53
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 38
    iput v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->mMinNormalWeightRange:F

    .line 39
    iput v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->mMaxNormalWeightRange:F

    .line 44
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->mRadius:I

    .line 54
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->initialize()V

    .line 55
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/high16 v0, -0x40800000    # -1.0f

    .line 58
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 38
    iput v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->mMinNormalWeightRange:F

    .line 39
    iput v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->mMaxNormalWeightRange:F

    .line 44
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->mRadius:I

    .line 59
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->initialize()V

    .line 60
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/high16 v0, -0x40800000    # -1.0f

    .line 63
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 38
    iput v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->mMinNormalWeightRange:F

    .line 39
    iput v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->mMaxNormalWeightRange:F

    .line 44
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->mRadius:I

    .line 64
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->initialize()V

    .line 65
    return-void
.end method

.method private calculateAngles()V
    .locals 3

    .prologue
    const/high16 v2, -0x3d4c0000    # -90.0f

    .line 166
    iget v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->mMinNormalWeightRange:F

    iget v1, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->mWeightValue:F

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorHelper;->getAngleForValue(FF)F

    move-result v0

    neg-float v0, v0

    add-float/2addr v0, v2

    iput v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->mStartAngle:F

    .line 167
    iget v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->mMaxNormalWeightRange:F

    iget v1, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->mWeightValue:F

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorHelper;->getAngleForValue(FF)F

    move-result v0

    neg-float v0, v0

    add-float/2addr v0, v2

    iget v1, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->mStartAngle:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->mSweepNormalAngle:F

    .line 168
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->invalidate()V

    .line 169
    return-void
.end method

.method private drawNormalRange(Landroid/graphics/Canvas;)V
    .locals 7
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v6, 0x0

    const/high16 v5, 0x40000000    # 2.0f

    .line 109
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->isNormalRangeNeeded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 110
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->mNormalRangePaint:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0701bd

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 111
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->getWidth()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->getHeight()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v5

    iget v2, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->mRangeRadius:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->mNormalRangePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 112
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->mBitmapCanvas:Landroid/graphics/Canvas;

    const/high16 v1, -0x1000000

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 113
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->mNormalRangePaint:Landroid/graphics/Paint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 114
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->mBitmapCanvas:Landroid/graphics/Canvas;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->getWidth()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v5

    iget v3, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->mRangeRadius:I

    int-to-float v3, v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->mNormalRangePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 115
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->mNormalRangePaint:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0701be

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 116
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->mBitmapCanvas:Landroid/graphics/Canvas;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->getWidth()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v5

    iget v3, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->mRangeRadius:I

    int-to-float v3, v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->mNormalRangePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 117
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->mBitmapCanvas:Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->mCutRect:Landroid/graphics/RectF;

    iget v2, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->mStartAngle:F

    iget v3, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->mRotateAngleDiff:F

    add-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->mSweepNormalAngle:F

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->mCutPaint:Landroid/graphics/Paint;

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 118
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->mBitmap:Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v6, v6, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 120
    :cond_0
    return-void
.end method

.method private initialize()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 82
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->setVisibility(I)V

    .line 83
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->initializeNormalRangePaint()V

    .line 84
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->mCutRect:Landroid/graphics/RectF;

    .line 85
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v1, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->mBitmap:Landroid/graphics/Bitmap;

    .line 86
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->mBitmapCanvas:Landroid/graphics/Canvas;

    .line 87
    return-void
.end method

.method private initializeNormalRangePaint()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 90
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->mNormalRangePaint:Landroid/graphics/Paint;

    .line 91
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->mNormalRangePaint:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0701bd

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 92
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->mCutPaint:Landroid/graphics/Paint;

    .line 93
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->mCutPaint:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 94
    return-void
.end method

.method private isNormalRangeNeeded()Z
    .locals 2

    .prologue
    const/high16 v1, -0x40800000    # -1.0f

    .line 185
    iget v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->mMinNormalWeightRange:F

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->isEqual(FF)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->mMaxNormalWeightRange:F

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->isEqual(FF)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 0
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 105
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->drawNormalRange(Landroid/graphics/Canvas;)V

    .line 106
    return-void
.end method

.method protected onMeasure(II)V
    .locals 2
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 98
    invoke-super {p0, p1, p2}, Landroid/view/View;->onMeasure(II)V

    .line 99
    iget v1, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->mRadius:I

    mul-int/lit8 v0, v1, 0x2

    .line 100
    .local v0, "size":I
    invoke-virtual {p0, v0, v0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->setMeasuredDimension(II)V

    .line 101
    return-void
.end method

.method public release()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 71
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 72
    iput-object v2, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->mBitmap:Landroid/graphics/Bitmap;

    .line 73
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-le v0, v1, :cond_0

    .line 74
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->mBitmapCanvas:Landroid/graphics/Canvas;

    invoke-virtual {v0}, Landroid/graphics/Canvas;->release()V

    .line 75
    iput-object v2, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->mBitmapCanvas:Landroid/graphics/Canvas;

    .line 78
    :cond_0
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 79
    return-void
.end method

.method setNormalWeightRange(FF)V
    .locals 2
    .param p1, "min"    # F
    .param p2, "max"    # F

    .prologue
    .line 155
    sget-object v0, Lcom/sec/android/app/shealth/weight/common/WeightConstants;->APP_TYPE:Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;

    sget-object v1, Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;->DOWNLOAD_MR:Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 156
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->setVisibility(I)V

    .line 160
    :goto_0
    iput p1, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->mMinNormalWeightRange:F

    .line 161
    iput p2, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->mMaxNormalWeightRange:F

    .line 162
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->calculateAngles()V

    .line 163
    return-void

    .line 158
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->setVisibility(I)V

    goto :goto_0
.end method

.method setRadius(I)V
    .locals 6
    .param p1, "radius"    # I

    .prologue
    const/4 v5, 0x0

    .line 128
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->getVisibility()I

    move-result v2

    const/16 v3, 0x8

    if-ne v2, v3, :cond_0

    .line 146
    :goto_0
    return-void

    .line 131
    :cond_0
    mul-int/lit8 v1, p1, 0x2

    .line 132
    .local v1, "size":I
    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_1

    iget v2, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->mRadius:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_3

    .line 134
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_2

    .line 135
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->release()V

    .line 136
    :cond_2
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->mBitmap:Landroid/graphics/Bitmap;

    .line 137
    new-instance v2, Landroid/graphics/Canvas;

    iget-object v3, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v2, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->mBitmapCanvas:Landroid/graphics/Canvas;

    .line 139
    :cond_3
    iput p1, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->mRadius:I

    .line 140
    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->mCutRect:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 141
    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->mCutRect:Landroid/graphics/RectF;

    int-to-float v3, v1

    int-to-float v4, v1

    invoke-virtual {v2, v5, v5, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 143
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0b1b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0b19

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    sub-int v0, v2, v3

    .line 145
    .local v0, "insetValue":I
    iget v2, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->mRadius:I

    sub-int/2addr v2, v0

    iput v2, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->mRangeRadius:I

    goto :goto_0
.end method

.method setWeightValue(F)V
    .locals 2
    .param p1, "weightValue"    # F

    .prologue
    .line 177
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    .line 182
    :goto_0
    return-void

    .line 180
    :cond_0
    iput p1, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->mWeightValue:F

    .line 181
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->calculateAngles()V

    goto :goto_0
.end method
