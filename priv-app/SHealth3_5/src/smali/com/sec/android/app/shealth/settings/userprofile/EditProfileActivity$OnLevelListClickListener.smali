.class public Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnLevelListClickListener;
.super Ljava/lang/Object;
.source "EditProfileActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "OnLevelListClickListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)V
    .locals 0

    .prologue
    .line 1008
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnLevelListClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 1015
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnLevelListClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    # invokes: Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->checkEditFocus()Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->access$200(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1041
    :goto_0
    return-void

    .line 1020
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 1039
    :goto_1
    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-static {p1, v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->announceForRadioBtnAccessibility(Landroid/view/View;Ljava/lang/String;ZZ)V

    .line 1040
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnLevelListClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnLevelListClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnLevelListClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->activityType:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->access$1700(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)I

    move-result v2

    # invokes: Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->initActivityLevelView(I)I
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->access$1900(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;I)I

    move-result v1

    # setter for: Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->afterActivityType:I
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->access$1802(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;I)I

    goto :goto_0

    .line 1023
    :sswitch_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnLevelListClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    const v1, 0x2bf21

    # setter for: Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->activityType:I
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->access$1702(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;I)I

    goto :goto_1

    .line 1026
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnLevelListClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    const v1, 0x2bf22

    # setter for: Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->activityType:I
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->access$1702(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;I)I

    goto :goto_1

    .line 1029
    :sswitch_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnLevelListClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    const v1, 0x2bf23

    # setter for: Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->activityType:I
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->access$1702(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;I)I

    goto :goto_1

    .line 1032
    :sswitch_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnLevelListClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    const v1, 0x2bf24

    # setter for: Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->activityType:I
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->access$1702(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;I)I

    goto :goto_1

    .line 1035
    :sswitch_4
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnLevelListClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    const v1, 0x2bf25

    # setter for: Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->activityType:I
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->access$1702(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;I)I

    goto :goto_1

    .line 1020
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f080864 -> :sswitch_0
        0x7f08086b -> :sswitch_1
        0x7f080872 -> :sswitch_2
        0x7f08087a -> :sswitch_3
        0x7f080882 -> :sswitch_4
    .end sparse-switch
.end method
