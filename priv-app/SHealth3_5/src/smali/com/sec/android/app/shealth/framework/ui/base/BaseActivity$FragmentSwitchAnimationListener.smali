.class Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$FragmentSwitchAnimationListener;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "FragmentSwitchAnimationListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$FragmentSwitchAnimationListener;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$FragmentSwitchAnimationListener;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mCachePopupBitmap:Landroid/graphics/Bitmap;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->access$1600(Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;)Landroid/graphics/Bitmap;

    move-result-object v0

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$FragmentSwitchAnimationListener;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mCachePopupBitmap:Landroid/graphics/Bitmap;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->access$1600(Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$FragmentSwitchAnimationListener;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mFragmentRootLayout:Landroid/widget/FrameLayout;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->access$1800(Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;)Landroid/widget/FrameLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$FragmentSwitchAnimationListener;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mOldFragmentView:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->access$1700(Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$FragmentSwitchAnimationListener;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mOldFragmentView:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->access$1700(Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;)Landroid/widget/ImageView;

    move-result-object v0

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$FragmentSwitchAnimationListener;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mOldFragmentView:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->access$1700(Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$FragmentSwitchAnimationListener;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mOldFragmentView:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->access$1700(Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    :goto_1
    return-void

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_1

    :catch_2
    move-exception v0

    goto :goto_1
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0

    return-void
.end method
