.class Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$10;
.super Ljava/lang/Object;
.source "ExerciseProWorkoutInfoActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$OnExerciseGridItemListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->updatePhotoGrid()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;)V
    .locals 0

    .prologue
    .line 861
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDeleteButtonClick(Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;)V
    .locals 3
    .param p1, "exerciseData"    # Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;

    .prologue
    .line 865
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mEndMode:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->access$600(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mEditMode:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->access$700(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    if-eqz p1, :cond_1

    .line 866
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->getRowId()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/common/utils/ExercisePhotoUtils;->deleteItem(Landroid/content/Context;J)J

    .line 867
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->updatePhotoGrid()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->access$1000(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;)V

    .line 869
    :cond_1
    return-void
.end method

.method public onStartGallery(I)V
    .locals 1
    .param p1, "pos"    # I

    .prologue
    .line 878
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->startExerciseProPhotoGallery(I)V
    invoke-static {v0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->access$1100(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;I)V

    .line 879
    return-void
.end method
