.class public Lcom/sec/android/app/shealth/weight/WeightActivity;
.super Lcom/sec/android/app/shealth/weight/HealthCareActivity;
.source "WeightActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/HealthCareActivity;-><init>()V

    return-void
.end method

.method private getExtrasForConnectivityActivity()Landroid/os/Bundle;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 126
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 127
    .local v0, "bundle":Landroid/os/Bundle;
    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->DATA_TYPE_KEY:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 128
    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->CONNECTIVITY_TYPE_KEY:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 129
    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->HEADER_TEXT_ID:Ljava/lang/String;

    const v2, 0x7f090ccf

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 130
    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->PAIRED_TEXT_RES_ID_KEY:Ljava/lang/String;

    const v2, 0x7f090cd0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 131
    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->DEVICE_TYPE_TEXT_RES_ID_KEY:Ljava/lang/String;

    const v2, 0x7f090cd1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 132
    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->NO_DEVICES_TEXT_RES_ID_KEY:Ljava/lang/String;

    const v2, 0x7f090cd2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 133
    return-object v0
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 104
    sget-object v1, Lcom/sec/android/app/shealth/weight/common/WeightConstants;->APP_TYPE:Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;

    iget-boolean v1, v1, Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;->isAccessorySupported:Z

    if-nez v1, :cond_0

    .line 105
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/WeightActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/WeightActivity;->getActionBarTitleId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(Ljava/lang/String;)V

    .line 106
    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v1, 0x7f0207c0

    new-instance v2, Lcom/sec/android/app/shealth/weight/WeightActivity$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/weight/WeightActivity$1;-><init>(Lcom/sec/android/app/shealth/weight/WeightActivity;)V

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(ILandroid/view/View$OnClickListener;)V

    .line 112
    .local v0, "logButton":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/WeightActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    new-array v2, v4, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 113
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/WeightActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    .line 117
    .end local v0    # "logButton":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;
    :goto_0
    return-void

    .line 115
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/shealth/weight/HealthCareActivity;->customizeActionBar()V

    goto :goto_0
.end method

.method protected getActionBarTitleId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 89
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/WeightActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09002c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getGraphFragmentClass()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 99
    const-class v0, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;

    return-object v0
.end method

.method protected getHelpItem()Ljava/lang/String;
    .locals 1

    .prologue
    .line 144
    const-string v0, "com.sec.shealth.help.action.WEIGHT"

    return-object v0
.end method

.method protected getSummaryFragmentClass()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 94
    sget-object v0, Lcom/sec/android/app/shealth/weight/common/WeightConstants;->APP_TYPE:Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;

    iget-object v0, v0, Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;->summaryFragmentClass:Ljava/lang/Class;

    return-object v0
.end method

.method public launchInformationActivity()V
    .locals 2

    .prologue
    .line 76
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/weight/WeightInformationFragment;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/weight/WeightActivity;->startActivity(Landroid/content/Intent;)V

    .line 77
    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 138
    invoke-super {p0}, Lcom/sec/android/app/shealth/weight/HealthCareActivity;->onDestroy()V

    .line 139
    return-void
.end method

.method protected onLogSelected()V
    .locals 3

    .prologue
    .line 121
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/WeightActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/shealth/weight/log/WeightLogActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 122
    .local v0, "intent":Landroid/content/Intent;
    const/16 v1, 0x2714

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/weight/WeightActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 123
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 61
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 72
    :goto_0
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/weight/HealthCareActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 63
    :sswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/WeightActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth.weight"

    const-string v2, "WT01"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 66
    :sswitch_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/WeightActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth.weight"

    const-string v2, "WT03"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 69
    :sswitch_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/WeightActivity;->launchInformationActivity()V

    goto :goto_0

    .line 61
    :sswitch_data_0
    .sparse-switch
        0x7f08036a -> :sswitch_2
        0x7f080c8a -> :sswitch_1
        0x7f080c98 -> :sswitch_0
    .end sparse-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 52
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/weight/HealthCareActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    .line 53
    .local v0, "state":Z
    sget-object v1, Lcom/sec/android/app/shealth/weight/common/WeightConstants;->APP_TYPE:Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;

    sget-object v2, Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;->PRELOAD:Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 54
    const v1, 0x7f080c99

    invoke-interface {p1, v1}, Landroid/view/Menu;->removeItem(I)V

    .line 56
    :cond_0
    return v0
.end method

.method protected startConnectivityActivity()V
    .locals 4

    .prologue
    .line 81
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/WeightActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "com.sec.android.app.shealth.weight"

    const-string v3, "WT06"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 83
    .local v0, "intent":Landroid/content/Intent;
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/WeightActivity;->getExtrasForConnectivityActivity()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 84
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/weight/WeightActivity;->startActivity(Landroid/content/Intent;)V

    .line 85
    return-void
.end method
