.class public Lcom/sec/android/app/shealth/widget/CustomFontEditText;
.super Landroid/widget/EditText;
.source "CustomFontEditText.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 33
    invoke-direct {p0, p1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 38
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/widget/CustomFontEditText;->setCustomFont(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 42
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 43
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/widget/CustomFontEditText;->setCustomFont(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 44
    return-void
.end method

.method private setCustomFont(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attributeSet"    # Landroid/util/AttributeSet;

    .prologue
    .line 47
    sget-object v2, Lcom/sec/android/app/shealth/R$styleable;->CustomFontTextView:[I

    invoke-virtual {p1, p2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 48
    .local v1, "typedArray":Landroid/content/res/TypedArray;
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 49
    .local v0, "customFont":Ljava/lang/String;
    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/shealth/widget/CustomFontEditText;->setCustomFont(Landroid/content/Context;Ljava/lang/String;)Z

    .line 50
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 51
    return-void
.end method

.method private setCustomFont(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "asset"    # Ljava/lang/String;

    .prologue
    .line 54
    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/common/utils/FontUtil;->createFont(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    .line 55
    .local v0, "typeface":Landroid/graphics/Typeface;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/widget/CustomFontEditText;->setTypeface(Landroid/graphics/Typeface;)V

    .line 56
    const/4 v1, 0x1

    return v1
.end method
