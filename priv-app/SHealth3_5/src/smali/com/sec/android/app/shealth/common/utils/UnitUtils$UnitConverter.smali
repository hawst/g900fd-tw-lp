.class public Lcom/sec/android/app/shealth/common/utils/UnitUtils$UnitConverter;
.super Ljava/lang/Object;
.source "UnitUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/common/utils/UnitUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UnitConverter"
.end annotation


# static fields
.field private static final GLUCOSE_CONVERT_VALUE:F = 18.0182f


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static convertMgIntoMmol(F)F
    .locals 1
    .param p0, "valueInMg"    # F

    .prologue
    .line 94
    const v0, 0x41902546    # 18.0182f

    div-float v0, p0, v0

    return v0
.end method

.method public static convertMilesToMeters(F)I
    .locals 1
    .param p0, "valueInMiles"    # F

    .prologue
    .line 121
    const v0, 0x3a22e36f

    div-float v0, p0, v0

    float-to-int v0, v0

    return v0
.end method

.method public static convertMmolIntoMg(F)F
    .locals 1
    .param p0, "valueInMmol"    # F

    .prologue
    .line 103
    const v0, 0x41902546    # 18.0182f

    mul-float/2addr v0, p0

    return v0
.end method

.method public static getHour(J)Ljava/lang/String;
    .locals 3
    .param p0, "time"    # J

    .prologue
    .line 60
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v2, "HH"

    invoke-direct {v0, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 61
    .local v0, "sdf":Ljava/text/SimpleDateFormat;
    const-string v2, "GMT"

    invoke-static {v2}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 62
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1, p0, p1}, Ljava/util/Date;-><init>(J)V

    .line 63
    .local v1, "timeInDate":Ljava/util/Date;
    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public static getHourMinute(J)Ljava/lang/String;
    .locals 3
    .param p0, "time"    # J

    .prologue
    .line 36
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v2, "HH:mm"

    invoke-direct {v0, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 37
    .local v0, "sdf":Ljava/text/SimpleDateFormat;
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1, p0, p1}, Ljava/util/Date;-><init>(J)V

    .line 38
    .local v1, "timeInDate":Ljava/util/Date;
    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public static getKmFromMeter(J)Ljava/lang/String;
    .locals 6
    .param p0, "distance"    # J

    .prologue
    .line 111
    long-to-double v2, p0

    const-wide v4, 0x408f400000000000L    # 1000.0

    div-double v0, v2, v4

    .line 112
    .local v0, "km":D
    new-instance v2, Ljava/text/DecimalFormat;

    const-string v3, "0.0"

    invoke-direct {v2, v3}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0, v1}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public static getMilesFromMeters(J)Ljava/lang/String;
    .locals 6
    .param p0, "distance"    # J

    .prologue
    .line 84
    long-to-double v2, p0

    const-wide v4, 0x3f445c6dea1359a4L    # 6.2137E-4

    mul-double v0, v2, v4

    .line 85
    .local v0, "mi":D
    new-instance v2, Ljava/text/DecimalFormat;

    const-string v3, "0.0"

    invoke-direct {v2, v3}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0, v1}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public static getMillisecToTime(JLjava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "time"    # J
    .param p2, "timeFormat"    # Ljava/lang/String;

    .prologue
    .line 23
    new-instance v0, Ljava/text/SimpleDateFormat;

    invoke-direct {v0, p2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 24
    .local v0, "sdf":Ljava/text/SimpleDateFormat;
    const-string v2, "GMT"

    invoke-static {v2}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 25
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1, p0, p1}, Ljava/util/Date;-><init>(J)V

    .line 26
    .local v1, "timeInDate":Ljava/util/Date;
    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public static getMinute(J)Ljava/lang/String;
    .locals 3
    .param p0, "time"    # J

    .prologue
    .line 72
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v2, "mm"

    invoke-direct {v0, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 73
    .local v0, "sdf":Ljava/text/SimpleDateFormat;
    const-string v2, "GMT"

    invoke-static {v2}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 74
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1, p0, p1}, Ljava/util/Date;-><init>(J)V

    .line 75
    .local v1, "timeInDate":Ljava/util/Date;
    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public static getMonthDay(J)Ljava/lang/String;
    .locals 3
    .param p0, "time"    # J

    .prologue
    .line 48
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v2, "MM/dd"

    invoke-direct {v0, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 49
    .local v0, "sdf":Ljava/text/SimpleDateFormat;
    const-string v2, "GMT"

    invoke-static {v2}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 50
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1, p0, p1}, Ljava/util/Date;-><init>(J)V

    .line 51
    .local v1, "timeInDate":Ljava/util/Date;
    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method
