.class Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity$3;
.super Ljava/lang/Object;
.source "HeartrateInputActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/heartrate/common/DialogListItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->startTagChooser()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;)V
    .locals 0

    .prologue
    .line 313
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onListItemClicked(ZIILjava/lang/String;)V
    .locals 6
    .param p1, "hasTag"    # Z
    .param p2, "tagId"    # I
    .param p3, "tagIconId"    # I
    .param p4, "tagName"    # Ljava/lang/String;

    .prologue
    const v4, 0x7f08053e

    const/4 v5, 0x0

    .line 319
    const-string v0, ""

    .line 320
    .local v0, "tag":Ljava/lang/String;
    const/4 v1, 0x0

    .line 321
    .local v1, "tagItem":Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->view:Landroid/view/View;
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->access$200(Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 322
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->view:Landroid/view/View;
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->access$200(Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    # setter for: Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mtagLayout:Landroid/widget/LinearLayout;
    invoke-static {v3, v2}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->access$302(Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;

    .line 323
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mtagLayout:Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->access$300(Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;)Landroid/widget/LinearLayout;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mTagIconListener:Landroid/view/View$OnClickListener;
    invoke-static {v3}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->access$400(Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;)Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 325
    :cond_0
    if-eqz p1, :cond_2

    .line 327
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;

    # setter for: Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mChangedTagId:I
    invoke-static {v2, p2}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->access$502(Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;I)I

    .line 328
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;

    const-string v3, ""

    # setter for: Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mChangedTagName:Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->access$602(Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 337
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mChangedTagId:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->access$500(Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;)I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils;->updateRecentTagPreference(ILandroid/content/Context;)V

    .line 338
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mChangedTagName:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->access$600(Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;)Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 339
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mChangedTagId:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->access$500(Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;)I

    move-result v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils;->getTag(I)Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    move-result-object v1

    .line 340
    if-eqz v1, :cond_1

    iget v2, v1, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mNameId:I

    if-lez v2, :cond_1

    .line 341
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;

    iget v3, v1, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mNameId:I

    # setter for: Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mTagTextId:I
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->access$802(Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;I)I

    .line 351
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->view:Landroid/view/View;
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->access$200(Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 352
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->view:Landroid/view/View;
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->access$200(Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    # setter for: Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mtagLayout:Landroid/widget/LinearLayout;
    invoke-static {v3, v2}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->access$302(Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;

    .line 353
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->view:Landroid/view/View;
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->access$200(Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;)Landroid/view/View;

    move-result-object v2

    const v4, 0x7f080535

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    # setter for: Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mTag:Landroid/widget/TextView;
    invoke-static {v3, v2}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->access$1102(Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 354
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mtagLayout:Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->access$300(Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;)Landroid/widget/LinearLayout;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 355
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mTag:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->access$1100(Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 356
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mTagTextId:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->access$800(Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;)I

    move-result v2

    if-lez v2, :cond_4

    .line 357
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mTag:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->access$1100(Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;)Landroid/widget/TextView;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mTagTextId:I
    invoke-static {v3}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->access$800(Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 368
    :cond_1
    :goto_2
    return-void

    .line 332
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;

    # setter for: Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mChangedTagName:Ljava/lang/String;
    invoke-static {v2, p4}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->access$602(Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 333
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;

    # setter for: Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mChangedTagId:I
    invoke-static {v2, p2}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->access$502(Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;I)I

    .line 334
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;

    # setter for: Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mChangedIconId:I
    invoke-static {v2, p3}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->access$702(Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;I)I

    goto/16 :goto_0

    .line 346
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;

    # setter for: Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mTagTextId:I
    invoke-static {v2, v5}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->access$802(Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;I)I

    .line 347
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;

    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mChangedTagName:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->access$600(Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;)Ljava/lang/String;

    move-result-object v3

    # setter for: Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mTagText:Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->access$902(Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 348
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;

    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mChangedIconId:I
    invoke-static {v3}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->access$700(Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;)I

    move-result v3

    # setter for: Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mTagIconId:I
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->access$1002(Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;I)I

    goto/16 :goto_1

    .line 359
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mChangedTagName:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->access$600(Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_5

    .line 360
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mChangedTagName:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->access$600(Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_1

    .line 361
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mTag:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->access$1100(Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;)Landroid/widget/TextView;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mChangedTagName:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->access$600(Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 365
    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->mTag:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->access$1100(Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;)Landroid/widget/TextView;

    move-result-object v2

    const v3, 0x7f090c0d

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    goto :goto_2
.end method

.method public onOkButtonClicked()V
    .locals 1

    .prologue
    .line 373
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;

    # invokes: Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->launchMoreTagActivity()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;->access$1200(Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;)V

    .line 374
    return-void
.end method
