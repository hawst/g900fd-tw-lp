.class public Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;
.super Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;
.source "EditMyMealActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerProvider;


# static fields
.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private mMealItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;",
            ">;"
        }
    .end annotation
.end field

.field private mMealNameEditText:Landroid/widget/EditText;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const-class v0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->getLogTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 37
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->setDoneButtonState(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;)Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->mealItemsFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 37
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->setDoneButtonState(Z)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->actionDone()V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;

    .prologue
    .line 37
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->actionCancel()V

    return-void
.end method

.method private actionDone()V
    .locals 5

    .prologue
    .line 139
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->mealWasEdited()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 140
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->mealNameChanged()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->getMealName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->doesMyMealNameExistInDb(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 141
    const v2, 0x7f090943

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->showToast(Ljava/lang/String;)Landroid/widget/Toast;

    .line 159
    :goto_0
    return-void

    .line 145
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->mealItemsFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;

    invoke-interface {v2}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;->getDeletedMealItemsList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    .line 146
    .local v1, "mealItemData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->mealItemDao:Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->getId()J

    move-result-wide v3

    invoke-interface {v2, v3, v4}, Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;->deleteDataById(J)Z

    goto :goto_1

    .line 148
    .end local v1    # "mealItemData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->mealItemsFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;

    invoke-interface {v2}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;->getAllMealItems()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    .line 149
    .restart local v1    # "mealItemData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->mealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->setMealId(J)V

    goto :goto_2

    .line 151
    .end local v1    # "mealItemData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->mealItemDao:Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->mealItemsFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;

    invoke-interface {v3}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;->getAllMealItems()Ljava/util/List;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;->insertOrUpdateData(Ljava/util/List;)Ljava/util/List;

    .line 152
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->mealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->getMealName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->setName(Ljava/lang/String;)V

    .line 153
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->mealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->mealItemsFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;

    invoke-interface {v3}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;->getTotalKcal()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->setKcal(F)V

    .line 154
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->mealDaoMyFood:Lcom/sec/android/app/shealth/food/fooddao/MealDaoMyFood;

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->mealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    invoke-interface {v2, v3}, Lcom/sec/android/app/shealth/food/fooddao/MealDaoMyFood;->updateData(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;)I

    .line 155
    const/4 v2, -0x1

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->setResult(I)V

    .line 157
    .end local v0    # "i$":Ljava/util/Iterator;
    :cond_3
    const v2, 0x7f090080

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->showToast(Ljava/lang/String;)Landroid/widget/Toast;

    .line 158
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->finish()V

    goto :goto_0
.end method

.method private getMealName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->mMealNameEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private mealNameChanged()Z
    .locals 2

    .prologue
    .line 166
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->getMealName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->mealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 86
    invoke-super {p0}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->customizeActionBar()V

    .line 87
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    .line 88
    .local v0, "actionBarHelper":Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;
    new-instance v3, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity$2;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity$2;-><init>(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;)V

    .line 95
    .local v3, "onSaveClickListener":Landroid/view/View$OnClickListener;
    new-instance v4, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v5, 0x7f09004f

    invoke-direct {v4, v6, v5, v3, v7}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IILandroid/view/View$OnClickListener;Z)V

    .line 97
    .local v4, "saveButton":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;
    new-instance v2, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity$3;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity$3;-><init>(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;)V

    .line 104
    .local v2, "onCancelClickListener":Landroid/view/View$OnClickListener;
    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v5, 0x7f090048

    invoke-direct {v1, v6, v5, v2, v7}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IILandroid/view/View$OnClickListener;Z)V

    .line 106
    .local v1, "cancelButton":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;
    const/4 v5, 0x2

    new-array v5, v5, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    aput-object v1, v5, v6

    aput-object v4, v5, v7

    invoke-virtual {v0, v5}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 107
    return-void
.end method

.method public getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 209
    sget-object v0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;->ALL_DIALOGS_TAGS:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 210
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->mealItemsFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;

    invoke-interface {v0, p1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;->getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;

    move-result-object v0

    .line 212
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->onDialogNotSupported(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;

    move-result-object v0

    goto :goto_0
.end method

.method protected getLayoutId()I
    .locals 1

    .prologue
    .line 62
    const v0, 0x7f0300cb

    return v0
.end method

.method protected initFragments(Z)V
    .locals 4
    .param p1, "isFirstInit"    # Z

    .prologue
    const v3, 0x7f0803a9

    .line 171
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 172
    .local v0, "fragmentManager":Landroid/support/v4/app/FragmentManager;
    if-eqz p1, :cond_0

    .line 173
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    .line 174
    .local v1, "fragmentTransaction":Landroid/support/v4/app/FragmentTransaction;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->createMealItemFragment()Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;

    move-result-object v2

    .line 175
    .local v2, "mealItemsFragment":Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;
    iput-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->mealItemsFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;

    .line 176
    invoke-virtual {v1, v3, v2}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 177
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 181
    .end local v1    # "fragmentTransaction":Landroid/support/v4/app/FragmentTransaction;
    .end local v2    # "mealItemsFragment":Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;
    :goto_0
    return-void

    .line 179
    :cond_0
    invoke-virtual {v0, v3}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;

    iput-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->mealItemsFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;

    goto :goto_0
.end method

.method protected initMemo()V
    .locals 0

    .prologue
    .line 117
    return-void
.end method

.method protected insertMealDataToFragments()V
    .locals 2

    .prologue
    .line 185
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->mealItemsFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->mMealItems:Ljava/util/List;

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;->addMealItemList(Ljava/util/List;)V

    .line 186
    return-void
.end method

.method protected loadMealDataFromDb(J)V
    .locals 3
    .param p1, "mealId"    # J

    .prologue
    .line 195
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->mealDaoMyFood:Lcom/sec/android/app/shealth/food/fooddao/MealDaoMyFood;

    invoke-interface {v0, p1, p2}, Lcom/sec/android/app/shealth/food/fooddao/MealDaoMyFood;->getDataById(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->mealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    .line 196
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->mealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    if-nez v0, :cond_0

    .line 197
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->mealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    .line 198
    sget-object v0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Can\'t get MealData by id = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->mealItemDao:Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;

    invoke-interface {v0, p1, p2}, Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;->getMealItemsListByMealId(J)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->mMealItems:Ljava/util/List;

    .line 201
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->mMealItems:Ljava/util/List;

    if-nez v0, :cond_1

    .line 202
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->mMealItems:Ljava/util/List;

    .line 203
    sget-object v0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Can\'t get meal items list by id = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    :cond_1
    return-void
.end method

.method protected mealWasEdited()Z
    .locals 5

    .prologue
    .line 133
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->mealItemsFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;

    invoke-interface {v2}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;->getAllMealItems()Ljava/util/List;

    move-result-object v0

    .line 134
    .local v0, "currentMealItems":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;>;"
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->mealItemDao:Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->mealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getId()J

    move-result-wide v3

    invoke-interface {v2, v3, v4}, Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;->getMealItemsListByMealId(J)Ljava/util/List;

    move-result-object v1

    .line 135
    .local v1, "storedMealItems":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;>;"
    invoke-interface {v1, v0}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->mealNameChanged()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 121
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->actionDone()V

    .line 122
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 44
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->onCreate(Landroid/os/Bundle;)V

    .line 45
    const v0, 0x7f080397

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->mMealNameEditText:Landroid/widget/EditText;

    .line 46
    const v0, 0x7f080396

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f09098a

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 47
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->mMealNameEditText:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity$1;-><init>(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 58
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 111
    const/4 v0, 0x0

    return v0
.end method

.method public onMealItemsQuantityChanged(I)V
    .locals 2
    .param p1, "quantity"    # I

    .prologue
    .line 126
    if-lez p1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->mMealNameEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/common/utils/Utils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 128
    .local v0, "doneButtonMustBeEnabled":Z
    :goto_0
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->setDoneButtonState(Z)V

    .line 129
    return-void

    .line 126
    .end local v0    # "doneButtonMustBeEnabled":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 67
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->onPostCreate(Landroid/os/Bundle;)V

    .line 68
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->mealItemsFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;

    sget-object v1, Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;->EDIT_MY_MEAL:Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;->setFragmentMode(Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;)V

    .line 69
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->areFragmentsInitialized(Landroid/os/Bundle;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 74
    :goto_0
    return-void

    .line 72
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->mealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    invoke-static {}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->calculateMealTypeAccordingToCurrentTime()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->setMealType(I)V

    .line 73
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->mMealNameEditText:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->mealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public prepareBundleData()Landroid/os/Bundle;
    .locals 4

    .prologue
    .line 78
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 79
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "MEAL_DATA_HOLDER"

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->mealDataHolder:Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 80
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->mealDataHolder:Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->mealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getMealType()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;->setMealType(J)V

    .line 81
    return-object v0
.end method
