.class Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$2;
.super Ljava/lang/Object;
.source "CignaMainExpandableAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->getGroupView(IZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

.field final synthetic val$goalData:Lcom/sec/android/app/shealth/cignacoach/data/GoalData;

.field final synthetic val$groupPosition:I


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;Lcom/sec/android/app/shealth/cignacoach/data/GoalData;I)V
    .locals 0

    .prologue
    .line 288
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    iput-object p2, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$2;->val$goalData:Lcom/sec/android/app/shealth/cignacoach/data/GoalData;

    iput p3, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$2;->val$groupPosition:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 291
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$2;->val$goalData:Lcom/sec/android/app/shealth/cignacoach/data/GoalData;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/data/GoalData;->isChecked()Z

    move-result v0

    .line 292
    .local v0, "isChecked":Z
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$2;->val$goalData:Lcom/sec/android/app/shealth/cignacoach/data/GoalData;

    if-nez v0, :cond_0

    move v1, v2

    :goto_0
    invoke-virtual {v4, v1}, Lcom/sec/android/app/shealth/cignacoach/data/GoalData;->setChecked(Z)V

    .line 293
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    iget v4, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$2;->val$groupPosition:I

    if-nez v0, :cond_1

    :goto_1
    invoke-virtual {v1, v4, v2}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->allChildItemCheckOfGroup(IZ)V

    .line 294
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->notifyDataSetChanged()V

    .line 295
    return-void

    :cond_0
    move v1, v3

    .line 292
    goto :goto_0

    :cond_1
    move v2, v3

    .line 293
    goto :goto_1
.end method
