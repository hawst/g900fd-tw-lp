.class Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity$1;
.super Ljava/lang/Object;
.source "AwardMedalActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity;)V
    .locals 0

    .prologue
    .line 397
    iput-object p1, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity$1;->this$0:Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/high16 v3, 0x10000000

    .line 402
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 423
    :goto_0
    return-void

    .line 404
    :sswitch_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity$1;->this$0:Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity;

    # getter for: Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity;->access$000(Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity;)Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 405
    .local v0, "logActivityIntent":Landroid/content/Intent;
    invoke-virtual {v0, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 406
    const-string/jumbo v1, "mypageActivity"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 407
    iget-object v1, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity$1;->this$0:Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity;

    # getter for: Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity;->access$000(Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 410
    .end local v0    # "logActivityIntent":Landroid/content/Intent;
    :sswitch_1
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity$1;->this$0:Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity;

    # getter for: Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity;->access$000(Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity;)Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 411
    .restart local v0    # "logActivityIntent":Landroid/content/Intent;
    invoke-virtual {v0, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 412
    iget-object v1, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity$1;->this$0:Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity;

    # getter for: Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity;->access$000(Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 415
    .end local v0    # "logActivityIntent":Landroid/content/Intent;
    :sswitch_2
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity$1;->this$0:Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity;

    # getter for: Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity;->access$000(Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity;)Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/shealth/food/log/FoodLogListActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 416
    .restart local v0    # "logActivityIntent":Landroid/content/Intent;
    invoke-virtual {v0, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 417
    iget-object v1, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity$1;->this$0:Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity;

    # getter for: Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity;->access$000(Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 402
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f080061 -> :sswitch_0
        0x7f080064 -> :sswitch_1
        0x7f08006a -> :sswitch_2
    .end sparse-switch
.end method
