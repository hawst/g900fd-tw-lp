.class public Lcom/sec/android/app/shealth/stress/state/StressStateReady;
.super Lcom/sec/android/app/shealth/stress/state/StressState;
.source "StressStateReady.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/stress/state/StressStateReady$HRUpdateViewTask;
    }
.end annotation


# static fields
.field private static final CURRENT_STATE_TIMEOUT:J = 0x2710L

.field private static final DELAYTIME_CHECK_SENSOR_DETECT:J = 0x96L

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private isInCurrentState:Z

.field private mCurrentStateTimer:Landroid/os/CountDownTimer;

.field private mDetectDelayTimer:Ljava/util/Timer;

.field private mUpdateViewTask:Lcom/sec/android/app/shealth/stress/state/StressStateReady$HRUpdateViewTask;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-class v0, Lcom/sec/android/app/shealth/stress/state/StressStateReady;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/stress/state/StressStateReady;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)V
    .locals 6
    .param p1, "summaryFragment"    # Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    .prologue
    const/4 v0, 0x0

    .line 21
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/stress/state/StressState;-><init>(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)V

    .line 95
    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/state/StressStateReady;->mUpdateViewTask:Lcom/sec/android/app/shealth/stress/state/StressStateReady$HRUpdateViewTask;

    .line 96
    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/state/StressStateReady;->mDetectDelayTimer:Ljava/util/Timer;

    .line 98
    new-instance v0, Lcom/sec/android/app/shealth/stress/state/StressStateReady$1;

    const-wide/16 v2, 0x2710

    const-wide/16 v4, 0x3e8

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/stress/state/StressStateReady$1;-><init>(Lcom/sec/android/app/shealth/stress/state/StressStateReady;JJ)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/state/StressStateReady;->mCurrentStateTimer:Landroid/os/CountDownTimer;

    .line 22
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 13
    sget-object v0, Lcom/sec/android/app/shealth/stress/state/StressStateReady;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/stress/state/StressStateReady;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/state/StressStateReady;

    .prologue
    .line 13
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/stress/state/StressStateReady;->isInCurrentState:Z

    return v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/shealth/stress/state/StressStateReady;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/state/StressStateReady;
    .param p1, "x1"    # Z

    .prologue
    .line 13
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/stress/state/StressStateReady;->isInCurrentState:Z

    return p1
.end method


# virtual methods
.method public clearForNextState()V
    .locals 2

    .prologue
    .line 44
    sget-object v0, Lcom/sec/android/app/shealth/stress/state/StressStateReady;->TAG:Ljava/lang/String;

    const-string v1, "clearForNextState()"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 45
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/stress/state/StressStateReady;->isInCurrentState:Z

    .line 46
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/state/StressStateReady;->mCurrentStateTimer:Landroid/os/CountDownTimer;

    if-eqz v0, :cond_0

    .line 47
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/state/StressStateReady;->mCurrentStateTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 48
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/state/StressStateReady;->mUpdateViewTask:Lcom/sec/android/app/shealth/stress/state/StressStateReady$HRUpdateViewTask;

    if-eqz v0, :cond_1

    .line 49
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/state/StressStateReady;->mUpdateViewTask:Lcom/sec/android/app/shealth/stress/state/StressStateReady$HRUpdateViewTask;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/stress/state/StressStateReady$HRUpdateViewTask;->cancel()Z

    .line 50
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/state/StressStateReady;->mDetectDelayTimer:Ljava/util/Timer;

    if-eqz v0, :cond_2

    .line 51
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/state/StressStateReady;->mDetectDelayTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 52
    :cond_2
    return-void
.end method

.method public reStartStates()V
    .locals 2

    .prologue
    .line 26
    sget-object v0, Lcom/sec/android/app/shealth/stress/state/StressStateReady;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "reStartStates()"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 27
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/state/StressStateReady;->mSummaryFragment:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/state/StressStateReady;->mSummaryFragment:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->getStateReady()Lcom/sec/android/app/shealth/stress/state/StressState;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->setState(Lcom/sec/android/app/shealth/stress/state/StressState;)V

    .line 28
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/stress/state/StressStateReady;->updateViewWithAnimation(Z)V

    .line 29
    return-void
.end method

.method public terminateState()V
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/state/StressStateReady;->mSummaryFragment:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->stopSensor()V

    .line 124
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/state/StressStateReady;->clearForNextState()V

    .line 125
    return-void
.end method

.method public updateNextState(II)V
    .locals 3
    .param p1, "rate"    # I
    .param p2, "interval"    # I

    .prologue
    .line 33
    sget-object v0, Lcom/sec/android/app/shealth/stress/state/StressStateReady;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "updateNextState rate: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 34
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 35
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/state/StressStateReady;->clearForNextState()V

    .line 37
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/state/StressStateReady;->mSummaryFragment:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/state/StressStateReady;->mSummaryFragment:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->getStateMeasuring()Lcom/sec/android/app/shealth/stress/state/StressState;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->setState(Lcom/sec/android/app/shealth/stress/state/StressState;)V

    .line 38
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/state/StressStateReady;->mSummaryFragment:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->getStateMeasuring()Lcom/sec/android/app/shealth/stress/state/StressState;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/stress/state/StressState;->updateViewWithAnimation(Z)V

    .line 40
    :cond_0
    return-void
.end method

.method public updateViewWithAnimation(Z)V
    .locals 4
    .param p1, "hasAnimation"    # Z

    .prologue
    const/4 v2, 0x0

    .line 56
    sget-object v0, Lcom/sec/android/app/shealth/stress/state/StressStateReady;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "updateView()"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 57
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/stress/state/StressStateReady;->isInCurrentState:Z

    .line 58
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/state/StressStateReady;->mSummaryFragment:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->startSensor()V

    .line 59
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/state/StressStateReady;->mCurrentStateTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->start()Landroid/os/CountDownTimer;

    .line 60
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/state/StressStateReady;->mDetectDelayTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 61
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/state/StressStateReady;->mDetectDelayTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 62
    iput-object v2, p0, Lcom/sec/android/app/shealth/stress/state/StressStateReady;->mDetectDelayTimer:Ljava/util/Timer;

    .line 64
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/state/StressStateReady;->mUpdateViewTask:Lcom/sec/android/app/shealth/stress/state/StressStateReady$HRUpdateViewTask;

    if-eqz v0, :cond_1

    .line 65
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/state/StressStateReady;->mUpdateViewTask:Lcom/sec/android/app/shealth/stress/state/StressStateReady$HRUpdateViewTask;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/stress/state/StressStateReady$HRUpdateViewTask;->cancel()Z

    .line 66
    iput-object v2, p0, Lcom/sec/android/app/shealth/stress/state/StressStateReady;->mUpdateViewTask:Lcom/sec/android/app/shealth/stress/state/StressStateReady$HRUpdateViewTask;

    .line 68
    :cond_1
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/state/StressStateReady;->mDetectDelayTimer:Ljava/util/Timer;

    .line 69
    new-instance v0, Lcom/sec/android/app/shealth/stress/state/StressStateReady$HRUpdateViewTask;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/shealth/stress/state/StressStateReady$HRUpdateViewTask;-><init>(Lcom/sec/android/app/shealth/stress/state/StressStateReady;Z)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/state/StressStateReady;->mUpdateViewTask:Lcom/sec/android/app/shealth/stress/state/StressStateReady$HRUpdateViewTask;

    .line 71
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/state/StressStateReady;->mDetectDelayTimer:Ljava/util/Timer;

    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/state/StressStateReady;->mUpdateViewTask:Lcom/sec/android/app/shealth/stress/state/StressStateReady$HRUpdateViewTask;

    const-wide/16 v2, 0x96

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 72
    return-void
.end method
