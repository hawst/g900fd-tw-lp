.class Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$2;
.super Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;
.source "HeartrateSummaryFragmentNew.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->initDeviceFinder()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;Landroid/content/Context;Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceConnectionCheckListener;)V
    .locals 0
    .param p2, "x0"    # Landroid/content/Context;
    .param p3, "x1"    # Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceConnectionCheckListener;

    .prologue
    .line 438
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$2;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    invoke-direct {p0, p2, p3}, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;-><init>(Landroid/content/Context;Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceConnectionCheckListener;)V

    return-void
.end method


# virtual methods
.method protected getDeviceType()Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;
    .locals 9

    .prologue
    .line 441
    const/4 v2, 0x0

    .line 442
    .local v2, "deviceType":Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$2;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    iget-object v5, v5, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->iDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$2;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mServiceConnected:Z
    invoke-static {v5}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$1400(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 444
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$2;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    iget-object v5, v5, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->iDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    const/4 v6, 0x7

    const/16 v7, 0x2711

    const/4 v8, 0x4

    invoke-virtual {v5, v6, v7, v8}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->getConnectedDeviceList(III)Ljava/util/List;

    move-result-object v0

    .line 445
    .local v0, "connectedDeviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    .line 446
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    .line 447
    .local v1, "device":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    if-eqz v2, :cond_1

    .line 481
    .end local v0    # "connectedDeviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    .end local v1    # "device":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    .end local v4    # "i$":Ljava/util/Iterator;
    :cond_0
    :goto_1
    return-object v2

    .line 449
    .restart local v0    # "connectedDeviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    .restart local v1    # "device":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    .restart local v4    # "i$":Ljava/util/Iterator;
    :cond_1
    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getType()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    .line 461
    const/4 v2, 0x0

    goto :goto_0

    .line 452
    :sswitch_0
    sget-object v2, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;->HEART_RATE_GEAR3:Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;

    .line 453
    goto :goto_0

    .line 455
    :sswitch_1
    sget-object v2, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;->HEART_RATE_GEAR2:Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;

    .line 456
    goto :goto_0

    .line 458
    :sswitch_2
    sget-object v2, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;->HEART_RATE_GEARFIT:Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_0 .. :try_end_0} :catch_3

    .line 459
    goto :goto_0

    .line 467
    .end local v0    # "connectedDeviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    .end local v1    # "device":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    .end local v4    # "i$":Ljava/util/Iterator;
    :catch_0
    move-exception v3

    .line 469
    .local v3, "e":Landroid/os/RemoteException;
    invoke-virtual {v3}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1

    .line 470
    .end local v3    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v3

    .line 472
    .local v3, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v3}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_1

    .line 473
    .end local v3    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v3

    .line 475
    .local v3, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto :goto_1

    .line 476
    .end local v3    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    :catch_3
    move-exception v3

    .line 478
    .local v3, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;
    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;->printStackTrace()V

    goto :goto_1

    .line 449
    :sswitch_data_0
    .sparse-switch
        0x2723 -> :sswitch_2
        0x2726 -> :sswitch_1
        0x272e -> :sswitch_0
    .end sparse-switch
.end method
