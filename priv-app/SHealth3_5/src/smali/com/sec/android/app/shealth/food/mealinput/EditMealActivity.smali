.class public Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;
.super Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;
.source "EditMealActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/IMealImagesFragmentAction$IMealImagesQuantityWatcher;


# static fields
.field private static final DATE_BEFORE_BIRTH_POPUP:Ljava/lang/String; = "DATE_BEFORE_BIRTH_POPUP"

.field private static final LOG_TAG:Ljava/lang/String;

.field private static final MEMO_TEXT_BUNDLE_KEY:Ljava/lang/String; = "MEMO_TEXT_BUNDLE_KEY"

.field private static final REPLACE_MEAL_DATA_POPUP:Ljava/lang/String; = "REPLACE_MEAL_DATA_POPUP"


# instance fields
.field private mBroadcastReceiverForClearDataAction:Landroid/content/BroadcastReceiver;

.field private mIsClearDataActionReceived:Z

.field private mIsImageLoading:Z

.field private mIsMemoFocused:Z

.field private mMaxImagesToast:Landroid/widget/Toast;

.field private mMaxMealItemsToast:Landroid/widget/Toast;

.field private mMealActivityTopFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/IMealActivityTopFragmentAction;

.field private mMemoEditText:Landroid/widget/EditText;

.field private mMemoTextWatcher:Lcom/sec/android/app/shealth/common/utils/MemoTextWatcher;

.field private mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 77
    const-class v0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->getLogTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 76
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;-><init>()V

    .line 90
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mIsClearDataActionReceived:Z

    .line 91
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mIsImageLoading:Z

    .line 92
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mIsMemoFocused:Z

    .line 93
    new-instance v0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity$1;-><init>(Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mBroadcastReceiverForClearDataAction:Landroid/content/BroadcastReceiver;

    .line 687
    new-instance v0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity$7;-><init>(Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mMemoTextWatcher:Lcom/sec/android/app/shealth/common/utils/MemoTextWatcher;

    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 76
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mIsClearDataActionReceived:Z

    return p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;

    .prologue
    .line 76
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->actionDone()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;)Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;)Lcom/sec/android/app/shealth/food/mealinput/IMealActivityTopFragmentAction;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mMealActivityTopFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/IMealActivityTopFragmentAction;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;
    .param p1, "x1"    # I

    .prologue
    .line 76
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->cleanMealTypeForTheDay(I)V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;

    .prologue
    .line 76
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->replaceData()J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;J)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;
    .param p1, "x1"    # J

    .prologue
    .line 76
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->onEditAccepted(J)V

    return-void
.end method

.method private actionDone()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 264
    iget-boolean v6, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mIsClearDataActionReceived:Z

    if-eqz v6, :cond_1

    .line 265
    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->setResult(I)V

    .line 266
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->finish()V

    .line 329
    :cond_0
    :goto_0
    return-void

    .line 270
    :cond_1
    const/4 v2, 0x0

    .line 271
    .local v2, "isMealDiscarded":Z
    iget-object v6, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealItemsFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;

    invoke-interface {v6}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;->getAllMealItems()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-nez v6, :cond_2

    iget-object v6, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealImagesFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/IMealImagesFragmentAction;

    invoke-interface {v6}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/IMealImagesFragmentAction;->getAllMealImages()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-nez v6, :cond_2

    iget-object v6, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mMemoEditText:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-nez v6, :cond_2

    .line 272
    const v6, 0x7f090940

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->showToast(Ljava/lang/String;)Landroid/widget/Toast;

    .line 273
    const/4 v2, 0x1

    .line 276
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealWasEdited()Z

    move-result v1

    .line 277
    .local v1, "isMealChanged":Z
    if-eqz v1, :cond_5

    .line 286
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->isDateMoreThanBirthday()Z

    move-result v6

    if-nez v6, :cond_3

    .line 287
    new-instance v6, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const v7, 0x7f090286

    invoke-direct {v6, p0, v8, v7}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;II)V

    const v7, 0x7f090044

    invoke-virtual {v6, v7}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v6

    const v7, 0x7f090084

    invoke-virtual {v6, v7}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v6

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v7

    const-string v8, "DATE_BEFORE_BIRTH_POPUP"

    invoke-virtual {v6, v7, v8}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 293
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->checkCaloriesRangeAndShowPopupAccordingTodayData()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 297
    iget-object v6, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealItemsFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;

    invoke-interface {v6}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;->getAllMealItems()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-nez v6, :cond_4

    iget-object v6, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealImagesFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/IMealImagesFragmentAction;

    invoke-interface {v6}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/IMealImagesFragmentAction;->getAllMealImages()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-nez v6, :cond_4

    iget-object v6, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mMemoEditText:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_4

    .line 298
    const v6, 0x7f09093f

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->showToast(Ljava/lang/String;)Landroid/widget/Toast;

    goto/16 :goto_0

    .line 303
    :cond_4
    iget-boolean v6, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->fromQuickInput:Z

    if-eqz v6, :cond_6

    .line 304
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mergeData()J

    move-result-wide v4

    .line 316
    .local v4, "mealId":J
    :goto_1
    const-wide/16 v6, -0x1

    cmp-long v6, v4, v6

    if-eqz v6, :cond_0

    .line 319
    invoke-direct {p0, v4, v5}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->onEditAccepted(J)V

    .line 320
    if-nez v2, :cond_5

    .line 321
    const v6, 0x7f090835

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->showToast(Ljava/lang/String;)Landroid/widget/Toast;

    .line 325
    .end local v4    # "mealId":J
    :cond_5
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 326
    .local v0, "intent":Landroid/content/Intent;
    const-string v6, "MEAL_WAS_EDITED"

    invoke-virtual {v0, v6, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 327
    const/4 v6, -0x1

    invoke-virtual {p0, v6, v0}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->setResult(ILandroid/content/Intent;)V

    .line 328
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->finish()V

    goto/16 :goto_0

    .line 306
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_6
    iget-object v6, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mMealActivityTopFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/IMealActivityTopFragmentAction;

    invoke-interface {v6}, Lcom/sec/android/app/shealth/food/mealinput/IMealActivityTopFragmentAction;->getMealType()I

    move-result v6

    invoke-direct {p0, v6}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->getMealDataFromDbByTypeForCurrentDay(I)Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    move-result-object v3

    .line 308
    .local v3, "mealDataAccordingMealTypeAndCurrentTime":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    iget-object v6, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getMealType()I

    move-result v6

    iget-object v7, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mMealActivityTopFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/IMealActivityTopFragmentAction;

    invoke-interface {v7}, Lcom/sec/android/app/shealth/food/mealinput/IMealActivityTopFragmentAction;->getMealType()I

    move-result v7

    if-ne v6, v7, :cond_7

    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->isMealDateChanged()Z

    move-result v6

    if-eqz v6, :cond_8

    :cond_7
    if-eqz v3, :cond_8

    .line 310
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->showReplaceMealPopup()V

    goto/16 :goto_0

    .line 313
    :cond_8
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->replaceData()J

    move-result-wide v4

    .restart local v4    # "mealId":J
    goto :goto_1
.end method

.method private cleanMealTypeForTheDay(I)V
    .locals 10
    .param p1, "mealType"    # I

    .prologue
    .line 738
    iget-object v5, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mMealActivityTopFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/IMealActivityTopFragmentAction;

    invoke-interface {v5}, Lcom/sec/android/app/shealth/food/mealinput/IMealActivityTopFragmentAction;->getTimeInMillis()J

    move-result-wide v3

    .line 739
    .local v3, "timeInDataTimePicker":J
    iget-object v5, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealDao:Lcom/sec/android/app/shealth/food/fooddao/MealDao;

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v6

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v8

    invoke-interface {v5, v6, v7, v8, v9}, Lcom/sec/android/app/shealth/food/fooddao/MealDao;->getDataForPeriod(JJ)Ljava/util/List;

    move-result-object v1

    .line 743
    .local v1, "listMealDataByCurrentDay":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    .line 744
    .local v2, "meal":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getMealType()I

    move-result v5

    if-ne v5, p1, :cond_0

    .line 745
    iget-object v5, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealDao:Lcom/sec/android/app/shealth/food/fooddao/MealDao;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getId()J

    move-result-wide v6

    invoke-interface {v5, v6, v7}, Lcom/sec/android/app/shealth/food/fooddao/MealDao;->deleteDataById(J)Z

    goto :goto_0

    .line 748
    .end local v2    # "meal":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    :cond_1
    return-void
.end method

.method private getMealDataFromDbByTypeForCurrentDay(I)Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    .locals 10
    .param p1, "mealType"    # I

    .prologue
    .line 522
    iget-object v5, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mMealActivityTopFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/IMealActivityTopFragmentAction;

    invoke-interface {v5}, Lcom/sec/android/app/shealth/food/mealinput/IMealActivityTopFragmentAction;->getTimeInMillis()J

    move-result-wide v3

    .line 523
    .local v3, "timeInDataTimePicker":J
    iget-object v5, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealDao:Lcom/sec/android/app/shealth/food/fooddao/MealDao;

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v6

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v8

    invoke-interface {v5, v6, v7, v8, v9}, Lcom/sec/android/app/shealth/food/fooddao/MealDao;->getDataForPeriod(JJ)Ljava/util/List;

    move-result-object v1

    .line 527
    .local v1, "listMealDataByCurrentDay":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    .line 528
    .local v2, "meal":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getMealType()I

    move-result v5

    if-ne v5, p1, :cond_0

    .line 532
    .end local v2    # "meal":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    :goto_0
    return-object v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private handleQuicInputChange(Ljava/util/List;J)V
    .locals 6
    .param p2, "mealId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;",
            ">;J)V"
        }
    .end annotation

    .prologue
    .line 476
    .local p1, "mealItems":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;>;"
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealItemsFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;

    check-cast v4, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getQuickInputHashMap()Ljava/util/HashMap;

    move-result-object v3

    .line 477
    .local v3, "quickInputHashMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Ljava/lang/Integer;>;"
    if-eqz v3, :cond_3

    invoke-virtual {v3}, Ljava/util/HashMap;->size()I

    move-result v4

    if-lez v4, :cond_3

    .line 478
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mFoodInfoDao:Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;

    invoke-interface {v4, p2, p3}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;->getFoodInfoListByMealId(J)Ljava/util/Set;

    move-result-object v1

    .line 481
    .local v1, "foodInfoDatas":Ljava/util/Set;, "Ljava/util/Set<Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;>;"
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    .line 483
    .local v0, "foodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getServerSourceType()I

    move-result v4

    invoke-static {v4}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->isQuickInputType(I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 485
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/4 v5, -0x1

    if-ne v4, v5, :cond_1

    .line 487
    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setkCal(F)V

    .line 488
    const v4, 0x46cd7

    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setServerSourceType(I)V

    .line 499
    :goto_1
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mFoodInfoDao:Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;

    invoke-interface {v4, v0}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;->insertOrUpdateData(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;)J

    goto :goto_0

    .line 492
    :cond_1
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 494
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setkCal(F)V

    .line 496
    :cond_2
    const v4, 0x46cd6

    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setServerSourceType(I)V

    goto :goto_1

    .line 506
    .end local v0    # "foodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    .end local v1    # "foodInfoDatas":Ljava/util/Set;, "Ljava/util/Set<Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;>;"
    .end local v2    # "i$":Ljava/util/Iterator;
    :cond_3
    return-void
.end method

.method private isDateMoreThanBirthday()Z
    .locals 6

    .prologue
    .line 509
    new-instance v1, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    .line 510
    .local v1, "profile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 511
    .local v0, "birthDate":Ljava/util/Calendar;
    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getBirthDate()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/common/utils/Utils;->convertDateStringToLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 512
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mMealActivityTopFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/IMealActivityTopFragmentAction;

    invoke-interface {v2}, Lcom/sec/android/app/shealth/food/mealinput/IMealActivityTopFragmentAction;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private isMealDateChanged()Z
    .locals 6

    .prologue
    .line 536
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mMealActivityTopFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/IMealActivityTopFragmentAction;

    invoke-interface {v4}, Lcom/sec/android/app/shealth/food/mealinput/IMealActivityTopFragmentAction;->getTimeInMillis()J

    move-result-wide v2

    .line 537
    .local v2, "timeInTimePicker":J
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getMealTime()J

    move-result-wide v0

    .line 538
    .local v0, "mealTime":J
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v4

    cmp-long v4, v2, v4

    if-ltz v4, :cond_0

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v4

    cmp-long v4, v2, v4

    if-lez v4, :cond_1

    :cond_0
    const/4 v4, 0x1

    :goto_0
    return v4

    :cond_1
    const/4 v4, 0x0

    goto :goto_0
.end method

.method private mergeData()J
    .locals 15

    .prologue
    const-wide/16 v2, -0x1

    const/16 v14, 0xf

    const/16 v13, 0x9

    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 354
    iget-object v7, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mMealActivityTopFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/IMealActivityTopFragmentAction;

    invoke-interface {v7}, Lcom/sec/android/app/shealth/food/mealinput/IMealActivityTopFragmentAction;->getMealType()I

    move-result v7

    invoke-direct {p0, v7}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->getMealDataFromDbByTypeForCurrentDay(I)Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    move-result-object v1

    .line 357
    .local v1, "mealDataAccordingMealTypeAndCurrentTime":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    if-nez v1, :cond_1

    .line 358
    new-instance v1, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    .end local v1    # "mealDataAccordingMealTypeAndCurrentTime":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    invoke-direct {v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;-><init>()V

    .line 359
    .restart local v1    # "mealDataAccordingMealTypeAndCurrentTime":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    const/4 v7, 0x0

    invoke-virtual {v1, v7}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->setKcal(F)V

    .line 381
    :cond_0
    iget-object v7, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mMemoEditText:Landroid/widget/EditText;

    invoke-virtual {v7}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->setComment(Ljava/lang/String;)V

    .line 383
    iget-object v7, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mMealActivityTopFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/IMealActivityTopFragmentAction;

    invoke-interface {v7}, Lcom/sec/android/app/shealth/food/mealinput/IMealActivityTopFragmentAction;->getTimeInMillis()J

    move-result-wide v7

    invoke-virtual {v1, v7, v8}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->setMealTime(J)V

    .line 385
    iget-object v7, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealItemsFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;

    invoke-interface {v7}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;->getTotalKcal()I

    move-result v7

    int-to-float v7, v7

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getKcal()F

    move-result v8

    add-float/2addr v7, v8

    invoke-virtual {v1, v7}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->setKcal(F)V

    .line 388
    iget-object v7, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mMealActivityTopFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/IMealActivityTopFragmentAction;

    invoke-interface {v7}, Lcom/sec/android/app/shealth/food/mealinput/IMealActivityTopFragmentAction;->getMealType()I

    move-result v7

    invoke-virtual {v1, v7}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->setMealType(I)V

    .line 392
    iget-object v7, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealImagesFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/IMealImagesFragmentAction;

    invoke-interface {v7}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/IMealImagesFragmentAction;->getDeletedImagesList()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;

    .line 393
    .local v4, "mealImageData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;
    iget-object v7, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealImageDao:Lcom/sec/android/app/shealth/food/fooddao/MealImageDao;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;->getId()J

    move-result-wide v8

    invoke-interface {v7, v8, v9}, Lcom/sec/android/app/shealth/food/fooddao/MealImageDao;->deleteDataById(J)Z

    goto :goto_0

    .line 360
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v4    # "mealImageData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;
    :cond_1
    iget-object v7, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealItemsFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;

    invoke-interface {v7}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;->getAllMealItems()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealItemDao:Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getId()J

    move-result-wide v9

    invoke-interface {v8, v9, v10}, Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;->getMealItemsListByMealId(J)Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    add-int/2addr v7, v8

    if-le v7, v14, :cond_3

    .line 363
    iget-object v7, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mMaxMealItemsToast:Landroid/widget/Toast;

    if-nez v7, :cond_2

    .line 364
    const v7, 0x7f090925

    new-array v8, v12, [Ljava/lang/Object;

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v11

    invoke-virtual {p0, v7, v8}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->showToast(Ljava/lang/String;)Landroid/widget/Toast;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mMaxMealItemsToast:Landroid/widget/Toast;

    .line 411
    :goto_1
    return-wide v2

    .line 367
    :cond_2
    iget-object v7, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mMaxMealItemsToast:Landroid/widget/Toast;

    invoke-virtual {v7}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 370
    :cond_3
    iget-object v7, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealImagesFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/IMealImagesFragmentAction;

    invoke-interface {v7}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/IMealImagesFragmentAction;->getAllMealImages()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealImageDao:Lcom/sec/android/app/shealth/food/fooddao/MealImageDao;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getId()J

    move-result-wide v9

    invoke-interface {v8, v9, v10}, Lcom/sec/android/app/shealth/food/fooddao/MealImageDao;->getImagesListByMealId(J)Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    add-int/2addr v7, v8

    if-le v7, v13, :cond_0

    .line 373
    iget-object v7, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mMaxImagesToast:Landroid/widget/Toast;

    if-nez v7, :cond_4

    .line 374
    const v7, 0x7f090929

    new-array v8, v12, [Ljava/lang/Object;

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v11

    invoke-virtual {p0, v7, v8}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->showToast(Ljava/lang/String;)Landroid/widget/Toast;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mMaxImagesToast:Landroid/widget/Toast;

    goto :goto_1

    .line 377
    :cond_4
    iget-object v7, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mMaxImagesToast:Landroid/widget/Toast;

    invoke-virtual {v7}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 397
    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_5
    iget-object v7, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealItemsFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;

    invoke-interface {v7}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;->getDeletedMealItemsList()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    .line 398
    .local v6, "mealItemData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;
    iget-object v7, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealItemDao:Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->getId()J

    move-result-wide v8

    invoke-interface {v7, v8, v9}, Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;->deleteDataById(J)Z

    goto :goto_2

    .line 400
    .end local v6    # "mealItemData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;
    :cond_6
    iget-object v7, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealDao:Lcom/sec/android/app/shealth/food/fooddao/MealDao;

    invoke-interface {v7, v1}, Lcom/sec/android/app/shealth/food/fooddao/MealDao;->insertOrUpdateData(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;)J

    .line 401
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getId()J

    move-result-wide v2

    .line 402
    .local v2, "mealId":J
    iget-object v7, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealItemsFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;

    invoke-interface {v7}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;->getAllMealItems()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_7

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    .line 403
    .restart local v6    # "mealItemData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;
    invoke-virtual {v6, v2, v3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->setMealId(J)V

    goto :goto_3

    .line 405
    .end local v6    # "mealItemData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;
    :cond_7
    iget-object v7, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealItemDao:Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;

    iget-object v8, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealItemsFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;

    invoke-interface {v8}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;->getAllMealItems()Ljava/util/List;

    move-result-object v8

    invoke-interface {v7, v8}, Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;->insertOrUpdateData(Ljava/util/List;)Ljava/util/List;

    .line 406
    iget-object v7, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealImagesFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/IMealImagesFragmentAction;

    invoke-interface {v7}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/IMealImagesFragmentAction;->getAllMealImages()Ljava/util/List;

    move-result-object v5

    .line 407
    .local v5, "mealImageDataList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;>;"
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_4
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_8

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;

    .line 408
    .restart local v4    # "mealImageData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;
    invoke-virtual {v4, v2, v3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;->setMealId(J)V

    goto :goto_4

    .line 410
    .end local v4    # "mealImageData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;
    :cond_8
    iget-object v7, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealImageDao:Lcom/sec/android/app/shealth/food/fooddao/MealImageDao;

    invoke-interface {v7, v5}, Lcom/sec/android/app/shealth/food/fooddao/MealImageDao;->insertOrUpdateData(Ljava/util/List;)Ljava/util/List;

    goto/16 :goto_1
.end method

.method private onEditAccepted(J)V
    .locals 4
    .param p1, "mealId"    # J

    .prologue
    const/4 v3, 0x0

    .line 339
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "START_FROM_MEA_VIEW_ACTIVITY"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "STARTED_FROM_FOOD_TRACKER"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    .line 340
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealDataHolder:Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;->setMealId(J)V

    .line 341
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealDataHolder:Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mMealActivityTopFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/IMealActivityTopFragmentAction;

    invoke-interface {v2}, Lcom/sec/android/app/shealth/food/mealinput/IMealActivityTopFragmentAction;->getMealType()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;->setMealType(J)V

    .line 342
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 343
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "MEAL_DATA_HOLDER"

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealDataHolder:Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 344
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->setResult(ILandroid/content/Intent;)V

    .line 351
    :goto_0
    return-void

    .line 346
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 347
    .restart local v0    # "intent":Landroid/content/Intent;
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 348
    const-string v1, "MEAL_ID_THAT_WAS_ADDED"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 349
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private replaceData()J
    .locals 12

    .prologue
    .line 421
    iget-object v8, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getId()J

    move-result-wide v8

    const-wide/16 v10, -0x1

    cmp-long v8, v8, v10

    if-eqz v8, :cond_0

    .line 422
    iget-object v8, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealDao:Lcom/sec/android/app/shealth/food/fooddao/MealDao;

    iget-object v9, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    invoke-virtual {v9}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getId()J

    move-result-wide v9

    invoke-interface {v8, v9, v10}, Lcom/sec/android/app/shealth/food/fooddao/MealDao;->deleteDataById(J)Z

    .line 424
    :cond_0
    iget-object v8, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    iget-object v9, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mMemoEditText:Landroid/widget/EditText;

    invoke-virtual {v9}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->setComment(Ljava/lang/String;)V

    .line 425
    iget-object v8, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    iget-object v9, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mMealActivityTopFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/IMealActivityTopFragmentAction;

    invoke-interface {v9}, Lcom/sec/android/app/shealth/food/mealinput/IMealActivityTopFragmentAction;->getTimeInMillis()J

    move-result-wide v9

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->setMealTime(J)V

    .line 426
    iget-object v8, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    iget-object v9, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealItemsFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;

    invoke-interface {v9}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;->getTotalKcal()I

    move-result v9

    int-to-float v9, v9

    invoke-virtual {v8, v9}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->setKcal(F)V

    .line 427
    iget-object v8, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    iget-object v9, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mMealActivityTopFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/IMealActivityTopFragmentAction;

    invoke-interface {v9}, Lcom/sec/android/app/shealth/food/mealinput/IMealActivityTopFragmentAction;->getMealType()I

    move-result v9

    invoke-virtual {v8, v9}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->setMealType(I)V

    .line 430
    iget-object v8, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealImagesFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/IMealImagesFragmentAction;

    invoke-interface {v8}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/IMealImagesFragmentAction;->getDeletedImagesList()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;

    .line 431
    .local v5, "mealImageData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;
    iget-object v8, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealImageDao:Lcom/sec/android/app/shealth/food/fooddao/MealImageDao;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;->getId()J

    move-result-wide v9

    invoke-interface {v8, v9, v10}, Lcom/sec/android/app/shealth/food/fooddao/MealImageDao;->deleteDataById(J)Z

    goto :goto_0

    .line 435
    .end local v5    # "mealImageData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;
    :cond_1
    iget-object v8, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealItemsFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;

    invoke-interface {v8}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;->getDeletedMealItemsList()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    .line 436
    .local v7, "mealItemData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;
    iget-object v8, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealItemDao:Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->getId()J

    move-result-wide v9

    invoke-interface {v8, v9, v10}, Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;->deleteDataById(J)Z

    goto :goto_1

    .line 438
    .end local v7    # "mealItemData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;
    :cond_2
    iget-object v8, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealDao:Lcom/sec/android/app/shealth/food/fooddao/MealDao;

    iget-object v9, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    invoke-interface {v8, v9}, Lcom/sec/android/app/shealth/food/fooddao/MealDao;->insertOrUpdateData(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;)J

    .line 440
    iget-object v8, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getId()J

    move-result-wide v3

    .line 441
    .local v3, "mealId":J
    iget-object v8, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealItemsFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;

    invoke-interface {v8}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;->getAllMealItems()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    .line 442
    .restart local v7    # "mealItemData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;
    invoke-virtual {v7, v3, v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->setMealId(J)V

    goto :goto_2

    .line 444
    .end local v7    # "mealItemData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;
    :cond_3
    iget-object v8, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealItemDao:Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;

    iget-object v9, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealItemsFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;

    invoke-interface {v9}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;->getAllMealItems()Ljava/util/List;

    move-result-object v9

    invoke-interface {v8, v9}, Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;->insertOrUpdateData(Ljava/util/List;)Ljava/util/List;

    .line 446
    const/4 v2, 0x0

    .line 447
    .local v2, "isNoMealItem":Z
    const/4 v1, 0x0

    .line 449
    .local v1, "isNoMealImageItem":Z
    iget-object v8, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealItemDao:Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;

    invoke-interface {v8, v3, v4}, Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;->getMealItemsListByMealId(J)Ljava/util/List;

    move-result-object v8

    if-eqz v8, :cond_4

    iget-object v8, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealItemDao:Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;

    invoke-interface {v8, v3, v4}, Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;->getMealItemsListByMealId(J)Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    if-nez v8, :cond_5

    .line 451
    :cond_4
    const/4 v2, 0x1

    .line 454
    :cond_5
    iget-object v8, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealImagesFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/IMealImagesFragmentAction;

    invoke-interface {v8}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/IMealImagesFragmentAction;->getAllMealImages()Ljava/util/List;

    move-result-object v6

    .line 455
    .local v6, "mealImageDataList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;>;"
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;

    .line 456
    .restart local v5    # "mealImageData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;
    invoke-virtual {v5, v3, v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;->setMealId(J)V

    goto :goto_3

    .line 458
    .end local v5    # "mealImageData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;
    :cond_6
    iget-object v8, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealImageDao:Lcom/sec/android/app/shealth/food/fooddao/MealImageDao;

    invoke-interface {v8, v6}, Lcom/sec/android/app/shealth/food/fooddao/MealImageDao;->insertOrUpdateData(Ljava/util/List;)Ljava/util/List;

    .line 459
    if-eqz v2, :cond_8

    iget-object v8, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealImageDao:Lcom/sec/android/app/shealth/food/fooddao/MealImageDao;

    invoke-interface {v8, v3, v4}, Lcom/sec/android/app/shealth/food/fooddao/MealImageDao;->getImagesListByMealId(J)Ljava/util/List;

    move-result-object v8

    if-eqz v8, :cond_7

    iget-object v8, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealImageDao:Lcom/sec/android/app/shealth/food/fooddao/MealImageDao;

    invoke-interface {v8, v3, v4}, Lcom/sec/android/app/shealth/food/fooddao/MealImageDao;->getImagesListByMealId(J)Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    if-nez v8, :cond_8

    .line 461
    :cond_7
    const/4 v1, 0x1

    .line 464
    :cond_8
    if-eqz v2, :cond_9

    if-eqz v1, :cond_9

    .line 466
    iget-object v8, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealDao:Lcom/sec/android/app/shealth/food/fooddao/MealDao;

    invoke-interface {v8, v3, v4}, Lcom/sec/android/app/shealth/food/fooddao/MealDao;->deleteDataById(J)Z

    .line 469
    :cond_9
    iget-object v8, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealItemsFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;

    invoke-interface {v8}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;->getAllMealItems()Ljava/util/List;

    move-result-object v8

    invoke-direct {p0, v8, v3, v4}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->handleQuicInputChange(Ljava/util/List;J)V

    .line 470
    return-wide v3
.end method

.method private showReplaceMealPopup()V
    .locals 5

    .prologue
    .line 332
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const/4 v1, 0x2

    const v2, 0x7f09094d

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;II)V

    const v1, 0x7f09094e

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mMealActivityTopFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/IMealActivityTopFragmentAction;

    invoke-interface {v4}, Lcom/sec/android/app/shealth/food/mealinput/IMealActivityTopFragmentAction;->getMealType()I

    move-result v4

    invoke-static {v4}, Lcom/sec/android/app/shealth/food/constants/MealType;->getMealTypeByMealTypeId(I)Lcom/sec/android/app/shealth/food/constants/MealType;

    move-result-object v4

    iget v4, v4, Lcom/sec/android/app/shealth/food/constants/MealType;->textResourceId:I

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "REPLACE_MEAL_DATA_POPUP"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 336
    return-void
.end method


# virtual methods
.method protected checkCaloriesRangeAndShowPopupAccordingTodayData()Z
    .locals 11

    .prologue
    .line 549
    iget-object v6, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mMealActivityTopFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/IMealActivityTopFragmentAction;

    invoke-interface {v6}, Lcom/sec/android/app/shealth/food/mealinput/IMealActivityTopFragmentAction;->getTimeInMillis()J

    move-result-wide v4

    .line 550
    .local v4, "measureTimeInMillis":J
    iget-object v6, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealDao:Lcom/sec/android/app/shealth/food/fooddao/MealDao;

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v7

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v9

    invoke-interface {v6, v7, v8, v9, v10}, Lcom/sec/android/app/shealth/food/fooddao/MealDao;->getDataForPeriod(JJ)Ljava/util/List;

    move-result-object v3

    .line 553
    .local v3, "mealDatas":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;>;"
    const v2, 0x47c34f80    # 99999.0f

    .line 554
    .local v2, "max":F
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    .line 555
    .local v0, "data":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getMealType()I

    move-result v6

    iget-object v7, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getMealType()I

    move-result v7

    if-ne v6, v7, :cond_1

    iget-object v6, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getId()J

    move-result-wide v6

    const-wide/16 v8, -0x1

    cmp-long v6, v6, v8

    if-nez v6, :cond_0

    .line 557
    :cond_1
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getKcal()F

    move-result v6

    sub-float/2addr v2, v6

    goto :goto_0

    .line 560
    .end local v0    # "data":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    :cond_2
    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->checkCaloriesRangeAndShowPopup(F)Z

    move-result v6

    return v6
.end method

.method public cleanImageAndMemo()V
    .locals 2

    .prologue
    .line 708
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mMemoEditText:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 709
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealImagesFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/IMealImagesFragmentAction;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealImagesFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/IMealImagesFragmentAction;

    instance-of v0, v0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;

    if-eqz v0, :cond_0

    .line 710
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealImagesFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/IMealImagesFragmentAction;

    check-cast v0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->deleteAllImages()V

    .line 712
    :cond_0
    return-void
.end method

.method public clearMemoFocus()V
    .locals 3

    .prologue
    .line 719
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mMemoEditText:Landroid/widget/EditText;

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mMemoEditText:Landroid/widget/EditText;

    if-ne v1, v2, :cond_0

    .line 721
    invoke-static {p0}, Lcom/sec/android/app/shealth/common/utils/SoftInputUtils;->hideSoftInput(Landroid/app/Activity;)V

    .line 722
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mMemoEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->clearFocus()V

    .line 723
    const v1, 0x7f0803d9

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 724
    .local v0, "view":Landroid/widget/RelativeLayout;
    if-eqz v0, :cond_0

    .line 726
    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->requestFocus()Z

    .line 730
    .end local v0    # "view":Landroid/widget/RelativeLayout;
    :cond_0
    return-void
.end method

.method protected customizeActionBar()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 591
    invoke-super {p0}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->customizeActionBar()V

    .line 592
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    .line 593
    .local v0, "actionBarHelper":Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;
    new-instance v2, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v3, 0x7f09004f

    new-instance v4, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity$4;

    invoke-direct {v4, p0}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity$4;-><init>(Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;)V

    invoke-direct {v2, v5, v3, v4, v6}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IILandroid/view/View$OnClickListener;Z)V

    .line 602
    .local v2, "save":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;
    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v3, 0x7f090048

    new-instance v4, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity$5;

    invoke-direct {v4, p0}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity$5;-><init>(Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;)V

    invoke-direct {v1, v5, v3, v4, v6}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IILandroid/view/View$OnClickListener;Z)V

    .line 611
    .local v1, "cancel":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    invoke-virtual {v3, v5}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setHomeButtonVisible(Z)V

    .line 612
    const/4 v3, 0x2

    new-array v3, v3, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    aput-object v1, v3, v5

    aput-object v2, v3, v6

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 613
    return-void
.end method

.method public getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 659
    sget-object v0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;->ALL_DIALOGS_TAGS:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 660
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealItemsFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;

    invoke-interface {v0, p1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;->getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;

    move-result-object v0

    .line 679
    :goto_0
    return-object v0

    .line 661
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/food/mealinput/IMealActivityTopFragmentAction;->ALL_DIALOGS_TAGS:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 662
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mMealActivityTopFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/IMealActivityTopFragmentAction;

    invoke-interface {v0, p1}, Lcom/sec/android/app/shealth/food/mealinput/IMealActivityTopFragmentAction;->getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;

    move-result-object v0

    goto :goto_0

    .line 663
    :cond_1
    const-string v0, "REPLACE_MEAL_DATA_POPUP"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 664
    new-instance v0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity$6;-><init>(Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;)V

    goto :goto_0

    .line 676
    :cond_2
    const-string v0, "DATE_BEFORE_BIRTH_POPUP"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 677
    const/4 v0, 0x0

    goto :goto_0

    .line 679
    :cond_3
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;

    move-result-object v0

    goto :goto_0
.end method

.method protected initFragments(Z)V
    .locals 4
    .param p1, "isFirstInit"    # Z

    .prologue
    const v3, 0x7f0803d7

    .line 239
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->initFragments(Z)V

    .line 240
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 241
    .local v0, "fragmentManager":Landroid/support/v4/app/FragmentManager;
    if-eqz p1, :cond_0

    .line 242
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    .line 243
    .local v1, "fragmentTransaction":Landroid/support/v4/app/FragmentTransaction;
    new-instance v2, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;

    invoke-direct {v2}, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;-><init>()V

    .line 244
    .local v2, "topFragment":Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;
    iput-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mMealActivityTopFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/IMealActivityTopFragmentAction;

    .line 245
    invoke-virtual {v1, v3, v2}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 246
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 251
    .end local v1    # "fragmentTransaction":Landroid/support/v4/app/FragmentTransaction;
    .end local v2    # "topFragment":Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;
    :goto_0
    return-void

    .line 248
    :cond_0
    invoke-virtual {v0, v3}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;

    iput-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mMealActivityTopFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/IMealActivityTopFragmentAction;

    goto :goto_0
.end method

.method protected initMemo()V
    .locals 4

    .prologue
    const v1, 0x7f0803e6

    const/4 v3, 0x0

    .line 198
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 200
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0907ab

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 202
    const v0, 0x7f0803e3

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mMemoEditText:Landroid/widget/EditText;

    .line 204
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mMemoEditText:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mMemoTextWatcher:Lcom/sec/android/app/shealth/common/utils/MemoTextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 205
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mMemoEditText:Landroid/widget/EditText;

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setVisibility(I)V

    .line 206
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mMemoEditText:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity$2;-><init>(Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 218
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getComment()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 219
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mMemoEditText:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getComment()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 220
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mMemoEditText:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getComment()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 224
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mMemoEditText:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity$3;-><init>(Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 235
    return-void
.end method

.method protected mealWasEdited()Z
    .locals 15

    .prologue
    const/4 v9, 0x0

    const/4 v10, 0x1

    .line 618
    iget-object v8, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealItemsFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;

    invoke-interface {v8}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;->getAllMealItems()Ljava/util/List;

    move-result-object v1

    .line 619
    .local v1, "currentMealItems":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;>;"
    iget-object v8, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealImagesFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/IMealImagesFragmentAction;

    invoke-interface {v8}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/IMealImagesFragmentAction;->getAllMealImages()Ljava/util/List;

    move-result-object v0

    .line 620
    .local v0, "currentMealImages":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;>;"
    iget-object v8, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getId()J

    move-result-wide v11

    const-wide/16 v13, -0x1

    cmp-long v8, v11, v13

    if-nez v8, :cond_3

    .line 622
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v8

    if-gtz v8, :cond_0

    iget-object v8, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mMemoEditText:Landroid/widget/EditText;

    invoke-virtual {v8}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    if-gtz v8, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v8

    if-lez v8, :cond_2

    :cond_0
    move v8, v10

    :goto_0
    move v10, v8

    .line 645
    :cond_1
    :goto_1
    return v10

    :cond_2
    move v8, v9

    .line 622
    goto :goto_0

    .line 626
    :cond_3
    iget-object v8, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealDataHolder:Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;->getMealId()J

    move-result-wide v3

    .line 627
    .local v3, "mealId":J
    iget-object v8, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealDao:Lcom/sec/android/app/shealth/food/fooddao/MealDao;

    invoke-interface {v8, v3, v4}, Lcom/sec/android/app/shealth/food/fooddao/MealDao;->getDataById(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    .line 628
    .local v5, "storedMealData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    if-eqz v5, :cond_1

    .line 631
    iget-object v8, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealItemDao:Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;

    invoke-interface {v8, v3, v4}, Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;->getMealItemsListByMealId(J)Ljava/util/List;

    move-result-object v7

    .line 632
    .local v7, "storedMealItems":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;>;"
    iget-object v8, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealImageDao:Lcom/sec/android/app/shealth/food/fooddao/MealImageDao;

    invoke-interface {v8, v3, v4}, Lcom/sec/android/app/shealth/food/fooddao/MealImageDao;->getImagesListByMealId(J)Ljava/util/List;

    move-result-object v6

    .line 634
    .local v6, "storedMealImages":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;>;"
    invoke-interface {v7, v1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v6, v0}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 640
    iget-object v8, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealItemsFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;

    check-cast v8, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->hasQuickInputChanged()Z

    move-result v8

    if-nez v8, :cond_1

    .line 644
    iget-object v8, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mMemoEditText:Landroid/widget/EditText;

    invoke-virtual {v8}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 645
    .local v2, "currentMemoString":Ljava/lang/String;
    iget-object v8, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mMealActivityTopFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/IMealActivityTopFragmentAction;

    invoke-interface {v8}, Lcom/sec/android/app/shealth/food/mealinput/IMealActivityTopFragmentAction;->getMealType()I

    move-result v8

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getMealType()I

    move-result v11

    if-ne v8, v11, :cond_4

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getMealTime()J

    move-result-wide v11

    iget-object v8, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mMealActivityTopFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/IMealActivityTopFragmentAction;

    invoke-interface {v8}, Lcom/sec/android/app/shealth/food/mealinput/IMealActivityTopFragmentAction;->getTimeInMillis()J

    move-result-wide v13

    cmp-long v8, v11, v13

    if-nez v8, :cond_4

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getComment()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_5

    :cond_4
    move v9, v10

    :cond_5
    move v10, v9

    goto :goto_1
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 652
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mIsImageLoading:Z

    if-nez v0, :cond_0

    .line 653
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->actionDone()V

    .line 655
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 102
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->onCreate(Landroid/os/Bundle;)V

    .line 105
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->getApplication()Landroid/app/Application;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLoggerHolder;

    invoke-interface {v1}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLoggerHolder;->getUserActionLogger()Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    .line 106
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->ENTER_FOOD_EDIT_VIEW:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    invoke-interface {v1, v2}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;->logUserAction(Lcom/sec/android/app/shealth/food/app/UserActionLog;)V

    .line 107
    if-eqz p1, :cond_0

    .line 108
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mMemoEditText:Landroid/widget/EditText;

    const-string v2, "MEMO_TEXT_BUNDLE_KEY"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 110
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mBroadcastReceiverForClearDataAction:Landroid/content/BroadcastReceiver;

    invoke-static {}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->getIntentFilterForClearDataAction()Landroid/content/IntentFilter;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 112
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 115
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 117
    .local v0, "window":Landroid/view/Window;
    if-eqz v0, :cond_1

    .line 119
    const/16 v1, 0x400

    const/16 v2, 0x500

    invoke-virtual {v0, v1, v2}, Landroid/view/Window;->setFlags(II)V

    .line 124
    :cond_1
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 684
    const/4 v0, 0x0

    return v0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 255
    invoke-super {p0}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->onDestroy()V

    .line 256
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mBroadcastReceiverForClearDataAction:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 257
    return-void
.end method

.method public onMealImageLoading()V
    .locals 1

    .prologue
    .line 576
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->setDoneButtonState(Z)V

    .line 577
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mIsImageLoading:Z

    .line 578
    return-void
.end method

.method public onMealImagesQuantityChanged(I)V
    .locals 2
    .param p1, "quantity"    # I

    .prologue
    const/4 v1, 0x0

    .line 570
    if-gez p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealItemsFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;

    invoke-interface {v0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;->getAllMealItems()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ltz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->setDoneButtonState(Z)V

    .line 571
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mIsImageLoading:Z

    .line 572
    return-void

    :cond_1
    move v0, v1

    .line 570
    goto :goto_0
.end method

.method public onMealItemsQuantityChanged(I)V
    .locals 1
    .param p1, "quantity"    # I

    .prologue
    .line 565
    if-gez p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealImagesFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/IMealImagesFragmentAction;

    invoke-interface {v0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/IMealImagesFragmentAction;->getAllMealImages()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ltz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->setDoneButtonState(Z)V

    .line 566
    return-void

    .line 565
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 190
    invoke-super {p0}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->onPause()V

    .line 191
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mMemoEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->isFocused()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mIsMemoFocused:Z

    .line 194
    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v8, 0xc

    const/16 v7, 0xb

    .line 139
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->onPostCreate(Landroid/os/Bundle;)V

    .line 140
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealItemsFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;

    sget-object v4, Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;->EDIT_MEAL:Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;

    invoke-interface {v3, v4}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;->setFragmentMode(Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;)V

    .line 141
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealImagesFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/IMealImagesFragmentAction;

    sget-object v4, Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;->EDIT_MEAL:Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;

    invoke-interface {v3, v4}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/IMealImagesFragmentAction;->setFragmentMode(Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;)V

    .line 142
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->areFragmentsInitialized(Landroid/os/Bundle;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 165
    :goto_0
    return-void

    .line 145
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mMealActivityTopFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/IMealActivityTopFragmentAction;

    iget-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getMealType()I

    move-result v4

    invoke-interface {v3, v4}, Lcom/sec/android/app/shealth/food/mealinput/IMealActivityTopFragmentAction;->setMealType(I)V

    .line 146
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealDataHolder:Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;->getMealId()J

    move-result-wide v3

    const-wide/high16 v5, -0x8000000000000000L

    cmp-long v3, v3, v5

    if-nez v3, :cond_2

    .line 147
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 148
    .local v1, "calendarForDefineTimeForMeal":Ljava/util/Calendar;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 149
    .local v0, "calendarForCurrentHourAndMinute":Ljava/util/Calendar;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-virtual {v0, v3, v4}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 150
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getMealTime()J

    move-result-wide v3

    invoke-virtual {v1, v3, v4}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 151
    invoke-virtual {v0, v8}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-virtual {v1, v8, v3}, Ljava/util/Calendar;->set(II)V

    .line 152
    invoke-virtual {v0, v7}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-virtual {v1, v7, v3}, Ljava/util/Calendar;->set(II)V

    .line 153
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->setMealTime(J)V

    .line 154
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mMealActivityTopFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/IMealActivityTopFragmentAction;

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-interface {v3, v4, v5}, Lcom/sec/android/app/shealth/food/mealinput/IMealActivityTopFragmentAction;->setTimeInMillis(J)V

    .line 158
    .end local v0    # "calendarForCurrentHourAndMinute":Ljava/util/Calendar;
    .end local v1    # "calendarForDefineTimeForMeal":Ljava/util/Calendar;
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "FOOD_TRACKER_BASE_DATA_FROM_CAMERA"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 159
    .local v2, "imagePath":Ljava/lang/String;
    if-eqz v2, :cond_1

    .line 160
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealImagesFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/IMealImagesFragmentAction;

    invoke-interface {v3, v2}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/IMealImagesFragmentAction;->addAndResizeMealImage(Ljava/lang/String;)V

    .line 162
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->handleReceivedMealItems(Landroid/content/Intent;)V

    goto :goto_0

    .line 156
    .end local v2    # "imagePath":Ljava/lang/String;
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mMealActivityTopFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/IMealActivityTopFragmentAction;

    iget-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getMealTime()J

    move-result-wide v4

    invoke-interface {v3, v4, v5}, Lcom/sec/android/app/shealth/food/mealinput/IMealActivityTopFragmentAction;->setTimeInMillis(J)V

    goto :goto_1
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 169
    invoke-super {p0}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->onResume()V

    .line 170
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mIsMemoFocused:Z

    if-nez v1, :cond_1

    .line 172
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mMemoEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->clearFocus()V

    .line 173
    const v1, 0x7f0803d9

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 174
    .local v0, "view":Landroid/widget/RelativeLayout;
    if-eqz v0, :cond_0

    .line 176
    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->requestFocus()Z

    .line 184
    .end local v0    # "view":Landroid/widget/RelativeLayout;
    :cond_0
    :goto_0
    return-void

    .line 179
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mMemoEditText:Landroid/widget/EditText;

    invoke-static {p0, v1}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->showSoftInputKeypad(Landroid/content/Context;Landroid/view/View;)V

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 703
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 704
    const-string v0, "MEMO_TEXT_BUNDLE_KEY"

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mMemoEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 705
    return-void
.end method

.method public prepareBundleData()Landroid/os/Bundle;
    .locals 4

    .prologue
    .line 582
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 583
    .local v0, "bundle":Landroid/os/Bundle;
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealDataHolder:Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mMealActivityTopFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/IMealActivityTopFragmentAction;

    invoke-interface {v2}, Lcom/sec/android/app/shealth/food/mealinput/IMealActivityTopFragmentAction;->getMealType()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;->setMealType(J)V

    .line 584
    const-string v1, "MEAL_ITEMS_COUNT"

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealItemsFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;

    invoke-interface {v2}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;->getAllMealItems()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 585
    const-string v1, "MEAL_DATA_HOLDER"

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mealDataHolder:Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 586
    return-object v0
.end method
