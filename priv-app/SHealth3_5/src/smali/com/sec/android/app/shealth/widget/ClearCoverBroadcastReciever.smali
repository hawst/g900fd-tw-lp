.class public Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;
.super Landroid/content/BroadcastReceiver;
.source "ClearCoverBroadcastReciever.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever$WFLDataReceivedListener;,
        Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever$UpdateClearCoverView;
    }
.end annotation


# static fields
.field private static final INVALID:I = -0x3

.field private static final TAG:Ljava/lang/String;

.field private static isBound:Z

.field private static mDayStepCounterConnection:Landroid/content/ServiceConnection;

.field private static mDayStepService:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

.field private static mRunningCnt:I

.field private static settingKey:Ljava/lang/String;

.field private static typeExtra:Ljava/lang/String;

.field private static wwflListener:Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever$WFLDataReceivedListener;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 49
    const-class v0, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->TAG:Ljava/lang/String;

    .line 53
    sput-object v1, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->mDayStepService:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    .line 55
    sput-object v1, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->wwflListener:Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever$WFLDataReceivedListener;

    .line 57
    sput-boolean v2, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->isBound:Z

    .line 58
    sput v2, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->mRunningCnt:I

    .line 60
    sput-object v1, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->settingKey:Ljava/lang/String;

    .line 61
    sput-object v1, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->typeExtra:Ljava/lang/String;

    .line 393
    new-instance v0, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever$1;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->mDayStepCounterConnection:Landroid/content/ServiceConnection;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 371
    return-void
.end method

.method static synthetic access$008()I
    .locals 2

    .prologue
    .line 48
    sget v0, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->mRunningCnt:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->mRunningCnt:I

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Landroid/content/Intent;

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->updateClearView(Landroid/content/Context;Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic access$202(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    .prologue
    .line 48
    sput-object p0, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->mDayStepService:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    return-object p0
.end method

.method static synthetic access$300()V
    .locals 0

    .prologue
    .line 48
    invoke-static {}, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->registerListener()V

    return-void
.end method

.method static synthetic access$400()V
    .locals 0

    .prologue
    .line 48
    invoke-static {}, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->unregisterListener()V

    return-void
.end method

.method static synthetic access$502(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 48
    sput-boolean p0, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->isBound:Z

    return p0
.end method

.method public static checkCoverView()V
    .locals 3

    .prologue
    .line 358
    sget-object v1, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->TAG:Ljava/lang/String;

    const-string v2, "checkCoverView"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 359
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 360
    .local v0, "context":Landroid/content/Context;
    sget-object v1, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->TAG:Ljava/lang/String;

    const-string v2, "checkCoverView : clearCoverdayStepChangeListener"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 361
    invoke-static {v0}, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->isCheckedCoverInSettings(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 362
    invoke-static {v0}, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->doBindWalkStepService(Landroid/content/Context;)V

    .line 363
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/service/health/keyManager/KeyManager;->isContentProviderAccessible()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 364
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getTotalStep()I

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->onUpdate(Landroid/content/Context;I)V

    .line 369
    :cond_0
    :goto_0
    return-void

    .line 366
    :cond_1
    const/4 v1, -0x3

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->onUpdate(Landroid/content/Context;I)V

    goto :goto_0
.end method

.method private static doBindWalkStepService(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 201
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->isRun()Z

    move-result v1

    if-nez v1, :cond_0

    .line 202
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 203
    .local v0, "iService":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 204
    sget-object v1, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->TAG:Ljava/lang/String;

    const-string v2, "RealtimeHealthService is started"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 206
    .end local v0    # "iService":Landroid/content/Intent;
    :cond_0
    sget-boolean v1, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->isBound:Z

    if-nez v1, :cond_1

    .line 207
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    sget-object v2, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->mDayStepCounterConnection:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {p0, v1, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v1

    sput-boolean v1, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->isBound:Z

    .line 210
    :cond_1
    return-void
.end method

.method public static isCheckedCoverInSettings(Landroid/content/Context;)Z
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 185
    const/4 v1, 0x0

    .line 187
    .local v1, "isCheckedCoverInSettings":I
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->settingKey:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-static {v4, v5, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 192
    :goto_0
    sget-object v4, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "isCheckedCoverInSettings() : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 193
    if-ne v1, v2, :cond_0

    :goto_1
    return v2

    .line 188
    :catch_0
    move-exception v0

    .line 189
    .local v0, "e":Ljava/lang/Exception;
    const-string v4, "Clear Cover is checked cover in settings"

    invoke-static {v4, v0}, Landroid/util/Log;->secW(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    move v2, v3

    .line 193
    goto :goto_1
.end method

.method public static onUpdate(Landroid/content/Context;I)V
    .locals 14
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "step"    # I

    .prologue
    const v13, 0x7f080b5e

    const v12, 0x7f080525

    const v11, 0x7f080b5d

    const/4 v10, 0x0

    const/16 v9, 0x8

    .line 239
    sget-object v5, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->TAG:Ljava/lang/String;

    const-string/jumbo v6, "onUpdate"

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 241
    int-to-long v1, p1

    .line 243
    .local v1, "steps":J
    invoke-static {p0}, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->isCheckedCoverInSettings(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_f

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v5

    const/16 v6, 0x2719

    if-ne v5, v6, :cond_f

    .line 245
    sget-object v5, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->TAG:Ljava/lang/String;

    const-string v6, "CLEARCOVER_START"

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 246
    invoke-static {v10}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->setWalkMateCoverVisible(Z)V

    .line 247
    new-instance v4, Landroid/widget/RemoteViews;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    const v6, 0x7f030281

    invoke-direct {v4, v5, v6}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 248
    .local v4, "views":Landroid/widget/RemoteViews;
    const v5, 0x7f080b57

    invoke-virtual {v4, v5, v10}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 249
    const v5, 0x7f080b5f

    invoke-virtual {v4, v5, v9}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 250
    const-wide/32 v5, 0x186a0

    cmp-long v5, v1, v5

    if-ltz v5, :cond_3

    .line 251
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getTotalStep()I

    move-result v5

    div-int/lit16 v5, v5, 0x3e8

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    .line 253
    .local v3, "value":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->isStartWalking()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 254
    invoke-virtual {v4, v13, v9}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 255
    const v5, 0x7f080b60

    invoke-virtual {v4, v5, v9}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 256
    invoke-virtual {v4, v11, v10}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 257
    const v5, 0x7f080b5f

    invoke-virtual {v4, v5, v10}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 258
    invoke-virtual {v4, v11, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 278
    .end local v3    # "value":Ljava/lang/String;
    :goto_0
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x13

    if-ne v5, v6, :cond_0

    .line 280
    const/4 v5, 0x1

    const/high16 v6, 0x41f00000    # 30.0f

    invoke-virtual {v4, v11, v5, v6}, Landroid/widget/RemoteViews;->setTextViewTextSize(IIF)V

    .line 282
    :cond_0
    const v6, 0x7f080b57

    const-wide/16 v7, 0x0

    cmp-long v5, v1, v7

    if-gez v5, :cond_7

    const-string v5, ""

    :goto_1
    invoke-virtual {v4, v6, v5}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    .line 283
    const/4 v5, -0x1

    invoke-virtual {v4, v11, v5}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 284
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isChinaModel()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 285
    const-wide/16 v5, 0x2710

    cmp-long v5, v1, v5

    if-ltz v5, :cond_8

    .line 286
    const/4 v5, 0x1

    const/high16 v6, 0x41a00000    # 20.0f

    invoke-virtual {v4, v11, v5, v6}, Landroid/widget/RemoteViews;->setTextViewTextSize(IIF)V

    .line 287
    const/4 v5, 0x1

    const/high16 v6, 0x41a00000    # 20.0f

    invoke-virtual {v4, v13, v5, v6}, Landroid/widget/RemoteViews;->setTextViewTextSize(IIF)V

    .line 293
    :cond_1
    :goto_2
    invoke-static {p0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->isHealthyStep()Z

    move-result v5

    if-eqz v5, :cond_9

    .line 294
    invoke-virtual {v4, v12, v9}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 295
    const v5, 0x7f080b5a

    invoke-virtual {v4, v5, v10}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 296
    const v5, 0x7f080b5b

    invoke-virtual {v4, v5, v9}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 297
    const v5, 0x7f080b5c

    invoke-virtual {v4, v5, v9}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 341
    :goto_3
    const/4 v5, 0x1

    invoke-static {p0, v4, v5}, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->updateClearCoverAppWidget(Landroid/content/Context;Landroid/widget/RemoteViews;Z)V

    .line 355
    :goto_4
    return-void

    .line 260
    .restart local v3    # "value":Ljava/lang/String;
    :cond_2
    invoke-virtual {v4, v13, v10}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 261
    const v5, 0x7f080b60

    invoke-virtual {v4, v5, v10}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 262
    invoke-virtual {v4, v11, v9}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 263
    const v5, 0x7f080b5f

    invoke-virtual {v4, v5, v9}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 264
    invoke-virtual {v4, v13, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto :goto_0

    .line 267
    .end local v3    # "value":Ljava/lang/String;
    :cond_3
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->isStartWalking()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 268
    invoke-virtual {v4, v13, v9}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 269
    invoke-virtual {v4, v11, v10}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 270
    const-wide/16 v5, 0x0

    cmp-long v5, v1, v5

    if-gez v5, :cond_4

    const-string v5, "--"

    :goto_5
    invoke-virtual {v4, v11, v5}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_4
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_5

    .line 272
    :cond_5
    invoke-virtual {v4, v11, v9}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 273
    invoke-virtual {v4, v13, v10}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 274
    const-wide/16 v5, 0x0

    cmp-long v5, v1, v5

    if-gez v5, :cond_6

    const-string v5, "--"

    :goto_6
    invoke-virtual {v4, v13, v5}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_6
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_6

    .line 282
    :cond_7
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0900c6

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_1

    .line 289
    :cond_8
    const/4 v5, 0x1

    const/high16 v6, 0x41d00000    # 26.0f

    invoke-virtual {v4, v11, v5, v6}, Landroid/widget/RemoteViews;->setTextViewTextSize(IIF)V

    .line 290
    const/4 v5, 0x1

    const/high16 v6, 0x41d00000    # 26.0f

    invoke-virtual {v4, v13, v5, v6}, Landroid/widget/RemoteViews;->setTextViewTextSize(IIF)V

    goto/16 :goto_2

    .line 298
    :cond_9
    invoke-static {p0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->isInactivetime()Z

    move-result v5

    if-eqz v5, :cond_a

    .line 299
    invoke-virtual {v4, v12, v9}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 300
    const v5, 0x7f080b5a

    invoke-virtual {v4, v5, v9}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 301
    const v5, 0x7f080b5b

    invoke-virtual {v4, v5, v10}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 302
    const v5, 0x7f080b5c

    invoke-virtual {v4, v5, v9}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto/16 :goto_3

    .line 305
    :cond_a
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/service/health/keyManager/KeyManager;->isContentProviderAccessible()Z

    move-result v5

    if-eqz v5, :cond_d

    .line 306
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getGoal()I

    move-result v0

    .line 307
    .local v0, "goal":I
    int-to-long v5, v0

    cmp-long v5, v1, v5

    if-ltz v5, :cond_b

    .line 308
    invoke-virtual {v4, v12, v9}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 309
    const v5, 0x7f080b5a

    invoke-virtual {v4, v5, v9}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 310
    const v5, 0x7f080b5b

    invoke-virtual {v4, v5, v9}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 311
    const v5, 0x7f080b5c

    invoke-virtual {v4, v5, v10}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto/16 :goto_3

    .line 313
    :cond_b
    const v5, 0x7f080b5a

    invoke-virtual {v4, v5, v9}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 314
    const v5, 0x7f080b5b

    invoke-virtual {v4, v5, v9}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 315
    const v5, 0x7f080b5c

    invoke-virtual {v4, v5, v9}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 316
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->isStartWalking()Z

    move-result v5

    if-eqz v5, :cond_c

    .line 317
    invoke-virtual {v4, v12, v10}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 318
    const v5, 0x7f080b58

    invoke-virtual {v4, v5, v9}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto/16 :goto_3

    .line 320
    :cond_c
    invoke-virtual {v4, v12, v9}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 321
    const v5, 0x7f080b58

    invoke-virtual {v4, v5, v10}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto/16 :goto_3

    .line 326
    .end local v0    # "goal":I
    :cond_d
    invoke-virtual {v4, v12, v10}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 327
    const v5, 0x7f080b5a

    invoke-virtual {v4, v5, v9}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 328
    const v5, 0x7f080b5b

    invoke-virtual {v4, v5, v9}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 329
    const v5, 0x7f080b5c

    invoke-virtual {v4, v5, v9}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 330
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->isStartWalking()Z

    move-result v5

    if-eqz v5, :cond_e

    .line 331
    invoke-virtual {v4, v12, v10}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 332
    const v5, 0x7f080b58

    invoke-virtual {v4, v5, v9}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto/16 :goto_3

    .line 335
    :cond_e
    invoke-virtual {v4, v12, v9}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 336
    const v5, 0x7f080b58

    invoke-virtual {v4, v5, v10}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto/16 :goto_3

    .line 342
    .end local v4    # "views":Landroid/widget/RemoteViews;
    :cond_f
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->isStartWalking()Z

    move-result v5

    if-nez v5, :cond_10

    .line 343
    sget-object v5, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->TAG:Ljava/lang/String;

    const-string v6, "CLEARCOVER_STOP"

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 344
    new-instance v4, Landroid/widget/RemoteViews;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    const v6, 0x7f030281

    invoke-direct {v4, v5, v6}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 345
    .restart local v4    # "views":Landroid/widget/RemoteViews;
    const v5, 0x7f080b57

    const/4 v6, 0x4

    invoke-virtual {v4, v5, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 347
    invoke-static {p0, v4, v10}, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->updateClearCoverAppWidget(Landroid/content/Context;Landroid/widget/RemoteViews;Z)V

    goto/16 :goto_4

    .line 349
    .end local v4    # "views":Landroid/widget/RemoteViews;
    :cond_10
    sget-object v5, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->TAG:Ljava/lang/String;

    const-string v6, "CLEARCOVER_STOP"

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 350
    invoke-static {v10}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->setWalkMateCoverVisible(Z)V

    .line 351
    new-instance v4, Landroid/widget/RemoteViews;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    const v6, 0x7f030281

    invoke-direct {v4, v5, v6}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 353
    .restart local v4    # "views":Landroid/widget/RemoteViews;
    invoke-static {p0, v4, v10}, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->updateClearCoverAppWidget(Landroid/content/Context;Landroid/widget/RemoteViews;Z)V

    goto/16 :goto_4
.end method

.method private static registerListener()V
    .locals 2

    .prologue
    .line 213
    new-instance v0, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever$WFLDataReceivedListener;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever$WFLDataReceivedListener;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->wwflListener:Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever$WFLDataReceivedListener;

    .line 214
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->wwflListener:Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever$WFLDataReceivedListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->registerListener(Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData$OnPedoMeterSensorDataReceiveListener;)V

    .line 215
    return-void
.end method

.method private static unregisterListener()V
    .locals 2

    .prologue
    .line 218
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->wwflListener:Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever$WFLDataReceivedListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->unregisterListener(Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData$OnPedoMeterSensorDataReceiveListener;)V

    .line 219
    return-void
.end method

.method public static updateClearCoverAppWidget(Landroid/content/Context;Landroid/widget/RemoteViews;Z)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "views"    # Landroid/widget/RemoteViews;
    .param p2, "isVisible"    # Z

    .prologue
    .line 223
    sget-object v1, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "updateClearCoverAppWidget "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    invoke-static {p0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isInitializationNeeded(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 226
    sget-object v1, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->TAG:Ljava/lang/String;

    const-string v2, "Update failed due to profile not initialized scenario"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 236
    :goto_0
    return-void

    .line 231
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.cover.REMOTEVIEWS_UPDATE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 232
    .local v0, "intent_clearcover":Landroid/content/Intent;
    const-string/jumbo v1, "visibility"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 233
    const-string/jumbo v1, "type"

    const-string/jumbo v2, "shealth"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 234
    const-string/jumbo v1, "remote"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 235
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private declared-synchronized updateClearView(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v11, 0x1

    .line 64
    monitor-enter p0

    if-eqz p2, :cond_0

    :try_start_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v8

    if-nez v8, :cond_1

    .line 150
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 67
    :cond_1
    :try_start_1
    sget v8, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->mRunningCnt:I

    const/4 v9, 0x2

    if-le v8, v9, :cond_2

    .line 68
    sget v8, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->mRunningCnt:I

    add-int/lit8 v8, v8, -0x1

    sput v8, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->mRunningCnt:I

    .line 69
    sget-object v8, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "updateClearCiew : mRunningCnt = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sget v10, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->mRunningCnt:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 64
    :catchall_0
    move-exception v8

    monitor-exit p0

    throw v8

    .line 73
    :cond_2
    :try_start_2
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    .line 74
    .local v2, "action":Ljava/lang/String;
    const-string v8, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 75
    invoke-static {p1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isInitializationNeeded(Landroid/content/Context;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 77
    sget-object v8, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "updateClearCiew : SharedPreferencesHelper.isInitializationNeeded(context) "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {p1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isInitializationNeeded(Landroid/content/Context;)Z

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 80
    :cond_3
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    sget-object v9, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->settingKey:Ljava/lang/String;

    const/4 v10, 0x0

    invoke-static {v8, v9, v10}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v8

    if-ne v8, v11, :cond_4

    .line 82
    new-instance v5, Landroid/content/Intent;

    const-string v8, "com.sec.android.app.shealth.COVER"

    invoke-direct {v5, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 83
    .local v5, "mIntent":Landroid/content/Intent;
    const-string/jumbo v8, "type"

    sget-object v9, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->typeExtra:Ljava/lang/String;

    invoke-virtual {v5, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 84
    const-string/jumbo v8, "sent_by_shealth"

    const/4 v9, 0x1

    invoke-virtual {v5, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 85
    invoke-virtual {p1, v5}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 149
    .end local v5    # "mIntent":Landroid/content/Intent;
    :cond_4
    :goto_1
    sget v8, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->mRunningCnt:I

    add-int/lit8 v8, v8, -0x1

    sput v8, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->mRunningCnt:I

    goto/16 :goto_0

    .line 88
    :cond_5
    const-string v8, "com.sec.android.app.shealth.COVER"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_9

    .line 91
    const-string/jumbo v8, "type"

    invoke-virtual {p2, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 92
    .local v6, "type":Ljava/lang/String;
    const-string/jumbo v8, "sent_by_shealth"

    const/4 v9, 0x0

    invoke-virtual {p2, v8, v9}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v8

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    .line 94
    .local v3, "bSentBySHealth":Ljava/lang/Boolean;
    sget-object v8, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "bSentBySHealth : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    const-string v8, "WALKMATE"

    invoke-virtual {v8, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 98
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isInitializationNeeded(Landroid/content/Context;)Z

    move-result v8

    if-nez v8, :cond_6

    .line 100
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getTotalStep()I

    move-result v8

    invoke-static {p1, v8}, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->onUpdate(Landroid/content/Context;I)V

    goto :goto_1

    .line 104
    :cond_6
    sget-object v8, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "bSentBySHealth : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    const/4 v0, 0x1

    .local v0, "CHECK":I
    const/4 v1, 0x0

    .line 106
    .local v1, "UNCHECK":I
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    if-nez v8, :cond_4

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    sget-object v9, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->settingKey:Ljava/lang/String;

    const/4 v10, -0x1

    invoke-static {v8, v9, v10}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v8

    if-ne v8, v11, :cond_4

    .line 108
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    sget-object v9, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->settingKey:Ljava/lang/String;

    const/4 v10, 0x0

    invoke-static {v8, v9, v10}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 109
    const/4 v8, 0x1

    invoke-static {v8}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->setWalkMateCoverVisible(Z)V

    .line 110
    sget-object v8, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->TAG:Ljava/lang/String;

    const-string v9, "CLEARCOVER_STOP : SHEALTH_NOT_INIT"

    invoke-static {v8, v9}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    new-instance v7, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v8

    const v9, 0x7f030281

    invoke-direct {v7, v8, v9}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 112
    .local v7, "views":Landroid/widget/RemoteViews;
    const/4 v8, 0x0

    invoke-static {p1, v7, v8}, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->updateClearCoverAppWidget(Landroid/content/Context;Landroid/widget/RemoteViews;Z)V

    .line 114
    new-instance v4, Landroid/content/Intent;

    const-class v8, Lcom/sec/android/app/shealth/SplashScreenActivity;

    invoke-direct {v4, p1, v8}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 115
    .local v4, "intentToRoot":Landroid/content/Intent;
    const/high16 v8, 0x14000000

    invoke-virtual {v4, v8}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 116
    const-string v8, "init_from_scover"

    const/4 v9, 0x1

    invoke-virtual {v4, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 117
    invoke-virtual {p1, v4}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 120
    .end local v0    # "CHECK":I
    .end local v1    # "UNCHECK":I
    .end local v4    # "intentToRoot":Landroid/content/Intent;
    .end local v7    # "views":Landroid/widget/RemoteViews;
    :cond_7
    const-string v8, "WALKMATE_TMR"

    invoke-virtual {v8, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 122
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isInitializationNeeded(Landroid/content/Context;)Z

    move-result v8

    if-nez v8, :cond_8

    .line 124
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getTotalStep()I

    move-result v8

    invoke-static {p1, v8}, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->onUpdate(Landroid/content/Context;I)V

    goto/16 :goto_1

    .line 129
    :cond_8
    const/4 v0, 0x1

    .restart local v0    # "CHECK":I
    const/4 v1, 0x0

    .line 130
    .restart local v1    # "UNCHECK":I
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    if-nez v8, :cond_4

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    sget-object v9, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->settingKey:Ljava/lang/String;

    const/4 v10, -0x1

    invoke-static {v8, v9, v10}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v8

    if-ne v8, v11, :cond_4

    .line 132
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    sget-object v9, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->settingKey:Ljava/lang/String;

    const/4 v10, 0x0

    invoke-static {v8, v9, v10}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 133
    const/4 v8, 0x1

    invoke-static {v8}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->setWalkMateCoverVisible(Z)V

    .line 134
    sget-object v8, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->TAG:Ljava/lang/String;

    const-string v9, "CLEARCOVER_STOP : SHEALTH_NOT_INIT"

    invoke-static {v8, v9}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 135
    new-instance v7, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v8

    const v9, 0x7f030281

    invoke-direct {v7, v8, v9}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 136
    .restart local v7    # "views":Landroid/widget/RemoteViews;
    const/4 v8, 0x0

    invoke-static {p1, v7, v8}, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->updateClearCoverAppWidget(Landroid/content/Context;Landroid/widget/RemoteViews;Z)V

    .line 138
    new-instance v4, Landroid/content/Intent;

    const-class v8, Lcom/sec/android/app/shealth/SplashScreenActivity;

    invoke-direct {v4, p1, v8}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 139
    .restart local v4    # "intentToRoot":Landroid/content/Intent;
    const/high16 v8, 0x14000000

    invoke-virtual {v4, v8}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 140
    const-string v8, "init_from_scover"

    const/4 v9, 0x1

    invoke-virtual {v4, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 141
    invoke-virtual {p1, v4}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 145
    .end local v0    # "CHECK":I
    .end local v1    # "UNCHECK":I
    .end local v3    # "bSentBySHealth":Ljava/lang/Boolean;
    .end local v4    # "intentToRoot":Landroid/content/Intent;
    .end local v6    # "type":Ljava/lang/String;
    .end local v7    # "views":Landroid/widget/RemoteViews;
    :cond_9
    const-string v8, "com.samsung.cover.REQUEST_REMOTEVIEWS"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 146
    sget-object v8, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->TAG:Ljava/lang/String;

    const-string v9, "SCOVER REMOTE VIEW REQUEST"

    invoke-static {v8, v9}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getTotalStep()I

    move-result v8

    invoke-static {p1, v8}, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->onUpdate(Landroid/content/Context;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_1
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 171
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-ge v0, v1, :cond_0

    .line 172
    const-string/jumbo v0, "walk_mate"

    sput-object v0, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->settingKey:Ljava/lang/String;

    .line 173
    const-string v0, "WALKMATE"

    sput-object v0, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->typeExtra:Ljava/lang/String;

    .line 178
    :goto_0
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->isSensorHubSupported()Z

    move-result v0

    if-nez v0, :cond_1

    .line 182
    :goto_1
    return-void

    .line 175
    :cond_0
    const-string v0, "lock_additional_steps"

    sput-object v0, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->settingKey:Ljava/lang/String;

    .line 176
    const-string v0, "WALKMATE_TMR"

    sput-object v0, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->typeExtra:Ljava/lang/String;

    goto :goto_0

    .line 180
    :cond_1
    new-instance v0, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever$UpdateClearCoverView;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1, p2}, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever$UpdateClearCoverView;-><init>(Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;Landroid/content/Context;Landroid/content/Intent;)V

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever$UpdateClearCoverView;->run()V

    .line 181
    sget-object v0, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onRecieve of ClearCoverBroadcastReciever with intent"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
