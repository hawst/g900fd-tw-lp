.class Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$2;
.super Ljava/lang/Object;
.source "ComfortZoneLevelActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->customizeActionBar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;)V
    .locals 0

    .prologue
    .line 128
    iput-object p1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$2;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 134
    instance-of v1, p1, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 136
    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;

    .line 138
    .local v0, "ab":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;->getActionButtonIndex()I

    move-result v1

    if-nez v1, :cond_2

    .line 140
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$2;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    # invokes: Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->isDataChanged()Z
    invoke-static {v1}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->access$100(Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 141
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$2;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    # invokes: Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->showResetPopup()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->access$200(Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;)V

    .line 154
    .end local v0    # "ab":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;
    :cond_0
    :goto_0
    return-void

    .line 143
    .restart local v0    # "ab":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$2;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->finish()V

    goto :goto_0

    .line 147
    :cond_2
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;->getActionButtonIndex()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 149
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "com.sec.android.app.shealth.thermohygrometer"

    const-string v3, "TH01"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$2;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    # invokes: Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->checkRangeAndSave()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->access$300(Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;)V

    goto :goto_0
.end method
