.class public Lcom/sec/android/app/shealth/home/widget/HomeHeartRateStateBar;
.super Landroid/widget/LinearLayout;
.source "HomeHeartRateStateBar.java"


# instance fields
.field private highPercentile:Landroid/widget/TextView;

.field private ivPolygon:Landroid/widget/ImageView;

.field private llBar:Landroid/widget/LinearLayout;

.field private lowPercentile:Landroid/widget/TextView;

.field private mStateBar:Lcom/sec/android/app/shealth/heartrate/barview/StateBar;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    const v0, 0x7f03012f

    invoke-static {p1, v0, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 27
    const v0, 0x7f0804f9

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/home/widget/HomeHeartRateStateBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/widget/HomeHeartRateStateBar;->ivPolygon:Landroid/widget/ImageView;

    .line 28
    const v0, 0x7f0804fa

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/home/widget/HomeHeartRateStateBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/widget/HomeHeartRateStateBar;->llBar:Landroid/widget/LinearLayout;

    .line 29
    const v0, 0x7f0804ff

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/home/widget/HomeHeartRateStateBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/widget/HomeHeartRateStateBar;->lowPercentile:Landroid/widget/TextView;

    .line 30
    const v0, 0x7f080500

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/home/widget/HomeHeartRateStateBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/widget/HomeHeartRateStateBar;->highPercentile:Landroid/widget/TextView;

    .line 31
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/barview/StateBar;

    invoke-direct {v0, p1}, Lcom/sec/android/app/shealth/heartrate/barview/StateBar;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/widget/HomeHeartRateStateBar;->mStateBar:Lcom/sec/android/app/shealth/heartrate/barview/StateBar;

    .line 32
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/HomeHeartRateStateBar;->llBar:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/widget/HomeHeartRateStateBar;->mStateBar:Lcom/sec/android/app/shealth/heartrate/barview/StateBar;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 33
    return-void
.end method

.method private convertDptoPx(F)I
    .locals 4
    .param p1, "dp"    # F

    .prologue
    .line 83
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/widget/HomeHeartRateStateBar;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 84
    .local v2, "res":Landroid/content/res/Resources;
    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 85
    .local v0, "metrics":Landroid/util/DisplayMetrics;
    iget v3, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float v1, p1, v3

    .line 86
    .local v1, "px":F
    float-to-int v3, v1

    return v3
.end method


# virtual methods
.method public getStateColor()I
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/HomeHeartRateStateBar;->mStateBar:Lcom/sec/android/app/shealth/heartrate/barview/StateBar;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/barview/StateBar;->getStateBarColor()I

    move-result v0

    return v0
.end method

.method public moveToPolygon(I[I[I)V
    .locals 9
    .param p1, "bpm"    # I
    .param p2, "avgRange"    # [I
    .param p3, "pecentile"    # [I

    .prologue
    const/high16 v3, 0x40c00000    # 6.0f

    const-wide v4, 0x4045800000000000L    # 43.0

    const v8, 0x7f0204be

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 36
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/widget/HomeHeartRateStateBar;->mStateBar:Lcom/sec/android/app/shealth/heartrate/barview/StateBar;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/heartrate/barview/StateBar;->invalidate()V

    .line 37
    const/4 v0, 0x0

    .line 39
    .local v0, "calculatePosition":F
    aget v2, p2, v6

    if-ge p1, v2, :cond_3

    .line 40
    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/home/widget/HomeHeartRateStateBar;->convertDptoPx(F)I

    move-result v2

    int-to-float v0, v2

    .line 41
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/widget/HomeHeartRateStateBar;->ivPolygon:Landroid/widget/ImageView;

    const v3, 0x7f020436

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 59
    :cond_0
    :goto_0
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x2

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 60
    .local v1, "params":Landroid/widget/LinearLayout$LayoutParams;
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 61
    const/4 v2, 0x3

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 62
    aget v2, p2, v6

    if-lt p1, v2, :cond_1

    aget v2, p2, v7

    if-le p1, v2, :cond_2

    .line 63
    :cond_1
    const/high16 v2, 0x40000000    # 2.0f

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/home/widget/HomeHeartRateStateBar;->convertDptoPx(F)I

    move-result v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    .line 65
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/widget/HomeHeartRateStateBar;->ivPolygon:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 67
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/widget/HomeHeartRateStateBar;->mStateBar:Lcom/sec/android/app/shealth/heartrate/barview/StateBar;

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/heartrate/barview/StateBar;->setStateBarColor(I)V

    .line 68
    return-void

    .line 42
    .end local v1    # "params":Landroid/widget/LinearLayout$LayoutParams;
    :cond_3
    aget v2, p2, v7

    if-le p1, v2, :cond_4

    .line 43
    const/high16 v2, 0x43200000    # 160.0f

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/home/widget/HomeHeartRateStateBar;->convertDptoPx(F)I

    move-result v2

    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/home/widget/HomeHeartRateStateBar;->convertDptoPx(F)I

    move-result v3

    add-int/2addr v2, v3

    int-to-float v0, v2

    .line 44
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/widget/HomeHeartRateStateBar;->ivPolygon:Landroid/widget/ImageView;

    const v3, 0x7f020437

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    .line 45
    :cond_4
    aget v2, p3, v6

    if-ge p1, v2, :cond_5

    .line 46
    aget v2, p3, v6

    aget v3, p2, v6

    sub-int/2addr v2, v3

    int-to-double v2, v2

    div-double v2, v4, v2

    double-to-float v2, v2

    aget v3, p2, v6

    sub-int v3, p1, v3

    int-to-float v3, v3

    mul-float v0, v2, v3

    .line 47
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/home/widget/HomeHeartRateStateBar;->convertDptoPx(F)I

    move-result v2

    int-to-float v0, v2

    .line 48
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/widget/HomeHeartRateStateBar;->ivPolygon:Landroid/widget/ImageView;

    invoke-virtual {v2, v8}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    .line 49
    :cond_5
    aget v2, p3, v7

    if-gt p1, v2, :cond_6

    aget v2, p3, v6

    if-lt p1, v2, :cond_6

    .line 50
    const-wide v2, 0x4055800000000000L    # 86.0

    aget v4, p3, v7

    aget v5, p3, v6

    sub-int/2addr v4, v5

    int-to-double v4, v4

    div-double/2addr v2, v4

    double-to-float v2, v2

    aget v3, p3, v6

    sub-int v3, p1, v3

    int-to-float v3, v3

    mul-float v0, v2, v3

    .line 51
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/home/widget/HomeHeartRateStateBar;->convertDptoPx(F)I

    move-result v2

    const/high16 v3, 0x422c0000    # 43.0f

    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/home/widget/HomeHeartRateStateBar;->convertDptoPx(F)I

    move-result v3

    add-int/2addr v2, v3

    int-to-float v0, v2

    .line 52
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/widget/HomeHeartRateStateBar;->ivPolygon:Landroid/widget/ImageView;

    invoke-virtual {v2, v8}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_0

    .line 53
    :cond_6
    aget v2, p3, v7

    if-le p1, v2, :cond_0

    .line 54
    aget v2, p2, v7

    aget v3, p3, v7

    sub-int/2addr v2, v3

    int-to-double v2, v2

    div-double v2, v4, v2

    double-to-float v2, v2

    aget v3, p3, v7

    sub-int v3, p1, v3

    int-to-float v3, v3

    mul-float v0, v2, v3

    .line 55
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/home/widget/HomeHeartRateStateBar;->convertDptoPx(F)I

    move-result v2

    const/high16 v3, 0x43010000    # 129.0f

    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/home/widget/HomeHeartRateStateBar;->convertDptoPx(F)I

    move-result v3

    add-int/2addr v2, v3

    int-to-float v0, v2

    .line 56
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/widget/HomeHeartRateStateBar;->ivPolygon:Landroid/widget/ImageView;

    invoke-virtual {v2, v8}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_0
.end method

.method public setHighPercentile(Ljava/lang/String;)V
    .locals 1
    .param p1, "tile"    # Ljava/lang/String;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/HomeHeartRateStateBar;->highPercentile:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 76
    return-void
.end method

.method public setLowPercentile(Ljava/lang/String;)V
    .locals 1
    .param p1, "tile"    # Ljava/lang/String;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/HomeHeartRateStateBar;->lowPercentile:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 72
    return-void
.end method
