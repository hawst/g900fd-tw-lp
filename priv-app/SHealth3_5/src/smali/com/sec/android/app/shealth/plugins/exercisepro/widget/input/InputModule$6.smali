.class Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$6;
.super Ljava/lang/Object;
.source "InputModule.java"

# interfaces
.implements Landroid/text/InputFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)V
    .locals 0

    .prologue
    .line 495
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$6;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;
    .locals 9
    .param p1, "source"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "end"    # I
    .param p4, "dest"    # Landroid/text/Spanned;
    .param p5, "dstart"    # I
    .param p6, "dend"    # I

    .prologue
    const/4 v4, 0x0

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 500
    invoke-interface {p4, v8, p5}, Landroid/text/Spanned;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    .line 501
    .local v2, "newString":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {p1, p2, p3}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 502
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {p4}, Landroid/text/Spanned;->length()I

    move-result v6

    invoke-interface {p4, p6, v6}, Landroid/text/Spanned;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 504
    const/4 v3, 0x0

    .line 506
    .local v3, "pattern":Ljava/util/regex/Pattern;
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$6;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mSystemNumberSeparator:Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->access$700(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "."

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 507
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "^([0-9]{1,"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$6;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mMaxValueLength:I
    invoke-static {v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->access$800(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "})(\\.([0-9])?)?$"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;
    :try_end_0
    .catch Ljava/util/regex/PatternSyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 517
    :goto_0
    invoke-virtual {v3, v2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 520
    .local v1, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    if-le v5, v7, :cond_5

    const-string v5, "0"

    invoke-virtual {v2, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    const-string v5, "0."

    invoke-virtual {v2, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_5

    const-string v5, "0,"

    invoke-virtual {v2, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_5

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "."

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_5

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 522
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "0"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 523
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 524
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$6;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->needCursorLastPosition:Z
    invoke-static {v4, v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->access$902(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;Z)Z

    .line 526
    :cond_0
    :try_start_1
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$6;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, ","

    const-string v7, "."

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v5

    const/high16 v6, 0x41200000    # 10.0f

    mul-float/2addr v5, v6

    float-to-int v5, v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->setNewValue(IZZ)V
    invoke-static {v4, v5, v6, v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->access$1000(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;IZZ)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 534
    :cond_1
    const-string v2, ""

    .line 563
    .end local v1    # "matcher":Ljava/util/regex/Matcher;
    .end local v2    # "newString":Ljava/lang/String;
    :cond_2
    :goto_1
    return-object v2

    .line 508
    .restart local v2    # "newString":Ljava/lang/String;
    :cond_3
    :try_start_2
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$6;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mSystemNumberSeparator:Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->access$700(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)Ljava/lang/String;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 509
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "^([0-9]{1,"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$6;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mMaxValueLength:I
    invoke-static {v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->access$800(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "})(,([0-9])?)?$"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;
    :try_end_2
    .catch Ljava/util/regex/PatternSyntaxException; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v3

    goto/16 :goto_0

    :cond_4
    move-object v2, v4

    .line 511
    goto :goto_1

    .line 512
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/util/regex/PatternSyntaxException;
    move-object v2, v4

    .line 514
    goto :goto_1

    .line 528
    .end local v0    # "e":Ljava/util/regex/PatternSyntaxException;
    .restart local v1    # "matcher":Ljava/util/regex/Matcher;
    :catch_1
    move-exception v0

    .line 530
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, ""

    goto :goto_1

    .line 537
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_5
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v5

    if-nez v5, :cond_c

    .line 539
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$6;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mSystemNumberSeparator:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->access$700(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)Ljava/lang/String;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 540
    const-string v4, "."

    invoke-virtual {p1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 541
    const-string v4, ","

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_8

    const-string v4, "."

    invoke-virtual {v2, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    if-eq v4, v5, :cond_6

    const-string v4, "."

    invoke-virtual {v2, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x2

    if-ne v4, v5, :cond_8

    :cond_6
    const-string v4, "."

    invoke-virtual {v2, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_8

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-le v4, v7, :cond_8

    .line 544
    const-string v2, ","

    goto/16 :goto_1

    .line 545
    :cond_7
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 550
    :cond_8
    const-string v4, "."

    invoke-virtual {v2, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_9

    const-string v4, ""

    invoke-virtual {p1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$6;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mSystemNumberSeparator:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->access$700(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 551
    :cond_9
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$6;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->needCursorLastPosition:Z
    invoke-static {v4, v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->access$902(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;Z)Z

    .line 552
    const-string v4, "."

    invoke-virtual {v2, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 554
    const-string v2, "---"

    goto/16 :goto_1

    .line 557
    :cond_a
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$6;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$6;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mMinInputRange:I
    invoke-static {v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->access$1100(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)I

    move-result v5

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->setNewValue(IZZ)V
    invoke-static {v4, v5, v7, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->access$1000(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;IZZ)V

    .line 559
    :cond_b
    const-string v2, ""

    goto/16 :goto_1

    :cond_c
    move-object v2, v4

    .line 563
    goto/16 :goto_1
.end method
