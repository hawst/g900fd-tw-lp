.class final Lcom/sec/android/app/shealth/weight/weightdao/WeightDaoImplDb$1;
.super Ljava/util/HashMap;
.source "WeightDaoImplDb.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/weight/weightdao/WeightDaoImplDb;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashMap",
        "<",
        "Lcom/sec/android/app/shealth/weight/weightdao/WeightDao$WeightContract;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    .line 52
    sget-object v0, Lcom/sec/android/app/shealth/weight/weightdao/WeightDao$WeightContract;->_ID:Lcom/sec/android/app/shealth/weight/weightdao/WeightDao$WeightContract;

    const-string v1, "_id"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/weight/weightdao/WeightDaoImplDb$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    sget-object v0, Lcom/sec/android/app/shealth/weight/weightdao/WeightDao$WeightContract;->COMMENT:Lcom/sec/android/app/shealth/weight/weightdao/WeightDao$WeightContract;

    const-string v1, "comment"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/weight/weightdao/WeightDaoImplDb$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    sget-object v0, Lcom/sec/android/app/shealth/weight/weightdao/WeightDao$WeightContract;->INPUT_SOURCE_TYPE:Lcom/sec/android/app/shealth/weight/weightdao/WeightDao$WeightContract;

    const-string v1, "input_source_type"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/weight/weightdao/WeightDaoImplDb$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    sget-object v0, Lcom/sec/android/app/shealth/weight/weightdao/WeightDao$WeightContract;->SAMPLE_TIME:Lcom/sec/android/app/shealth/weight/weightdao/WeightDao$WeightContract;

    const-string/jumbo v1, "sample_time"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/weight/weightdao/WeightDaoImplDb$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    sget-object v0, Lcom/sec/android/app/shealth/weight/weightdao/WeightDao$WeightContract;->WEIGHT:Lcom/sec/android/app/shealth/weight/weightdao/WeightDao$WeightContract;

    const-string/jumbo v1, "weight"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/weight/weightdao/WeightDaoImplDb$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    sget-object v0, Lcom/sec/android/app/shealth/weight/weightdao/WeightDao$WeightContract;->COUNT_WEIGHT:Lcom/sec/android/app/shealth/weight/weightdao/WeightDao$WeightContract;

    const-string v1, "COUNT(weight)"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/weight/weightdao/WeightDaoImplDb$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    sget-object v0, Lcom/sec/android/app/shealth/weight/weightdao/WeightDao$WeightContract;->SUM_WEIGHT:Lcom/sec/android/app/shealth/weight/weightdao/WeightDao$WeightContract;

    const-string v1, "SUM(weight)"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/weight/weightdao/WeightDaoImplDb$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    sget-object v0, Lcom/sec/android/app/shealth/weight/weightdao/WeightDao$WeightContract;->AVG_WEIGHT:Lcom/sec/android/app/shealth/weight/weightdao/WeightDao$WeightContract;

    const-string v1, "AVG(weight)"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/weight/weightdao/WeightDaoImplDb$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    sget-object v0, Lcom/sec/android/app/shealth/weight/weightdao/WeightDao$WeightContract;->MAX_SAMPLE_TIME:Lcom/sec/android/app/shealth/weight/weightdao/WeightDao$WeightContract;

    const-string v1, "MAX(sample_time)"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/weight/weightdao/WeightDaoImplDb$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    return-void
.end method
