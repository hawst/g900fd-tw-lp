.class public Lcom/sec/android/app/shealth/settings/userprofile/ProfileFourthView;
.super Landroid/view/View;
.source "ProfileFourthView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/settings/userprofile/ProfileFourthView$OnLevelListClickListener;
    }
.end annotation


# instance fields
.field private checkNumber:I

.field private lev_1:Landroid/widget/RelativeLayout;

.field private lev_2:Landroid/widget/RelativeLayout;

.field private mContext:Landroid/content/Context;

.field private mView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/sdk/health/content/ShealthProfile;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "shealthProfile"    # Lcom/samsung/android/sdk/health/content/ShealthProfile;

    .prologue
    const/4 v1, 0x0

    .line 38
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 32
    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFourthView;->mView:Landroid/view/View;

    .line 33
    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFourthView;->mContext:Landroid/content/Context;

    .line 39
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFourthView;->mContext:Landroid/content/Context;

    .line 41
    const v0, 0x7f0301e6

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFourthView;->mView:Landroid/view/View;

    .line 43
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFourthView;->initLayout()V

    .line 44
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/settings/userprofile/ProfileFourthView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/ProfileFourthView;

    .prologue
    .line 30
    iget v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFourthView;->checkNumber:I

    return v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/shealth/settings/userprofile/ProfileFourthView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/ProfileFourthView;
    .param p1, "x1"    # I

    .prologue
    .line 30
    iput p1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFourthView;->checkNumber:I

    return p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/settings/userprofile/ProfileFourthView;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/ProfileFourthView;
    .param p1, "x1"    # I

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFourthView;->initView(I)V

    return-void
.end method

.method private initLayout()V
    .locals 2

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFourthView;->mView:Landroid/view/View;

    const v1, 0x7f080853

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFourthView;->lev_1:Landroid/widget/RelativeLayout;

    .line 53
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFourthView;->mView:Landroid/view/View;

    const v1, 0x7f080858

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFourthView;->lev_2:Landroid/widget/RelativeLayout;

    .line 55
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFourthView;->lev_1:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFourthView$OnLevelListClickListener;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFourthView$OnLevelListClickListener;-><init>(Lcom/sec/android/app/shealth/settings/userprofile/ProfileFourthView;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 56
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFourthView;->lev_2:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFourthView$OnLevelListClickListener;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFourthView$OnLevelListClickListener;-><init>(Lcom/sec/android/app/shealth/settings/userprofile/ProfileFourthView;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 59
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFourthView;->checkNumber:I

    .line 60
    iget v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFourthView;->checkNumber:I

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFourthView;->initView(I)V

    .line 61
    return-void
.end method

.method private initView(I)V
    .locals 13
    .param p1, "lev"    # I

    .prologue
    const v12, 0x7f07011a

    const v11, 0x7f070119

    .line 64
    if-nez p1, :cond_1

    .line 91
    :cond_0
    return-void

    .line 67
    :cond_1
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFourthView;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 72
    .local v1, "packName":Ljava/lang/String;
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    const/4 v8, 0x3

    if-ge v0, v8, :cond_0

    .line 73
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFourthView;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "fourth_lev"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "_title"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const-string v10, "id"

    invoke-virtual {v8, v9, v10, v1}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v5

    .line 74
    .local v5, "resID_title":I
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFourthView;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "fourth_lev"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "_sub_title"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const-string v10, "id"

    invoke-virtual {v8, v9, v10, v1}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    .line 75
    .local v4, "resID_sub_title":I
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFourthView;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "fourth_lev"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "_radio_button"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const-string v10, "id"

    invoke-virtual {v8, v9, v10, v1}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    .line 77
    .local v3, "resID_radio":I
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFourthView;->mView:Landroid/view/View;

    invoke-virtual {v8, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 78
    .local v7, "title":Landroid/widget/TextView;
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFourthView;->mView:Landroid/view/View;

    invoke-virtual {v8, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 79
    .local v6, "sub_title":Landroid/widget/TextView;
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFourthView;->mView:Landroid/view/View;

    invoke-virtual {v8, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RadioButton;

    .line 81
    .local v2, "radio":Landroid/widget/RadioButton;
    if-ne v0, p1, :cond_2

    .line 82
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFourthView;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8, v12}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 83
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFourthView;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8, v12}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 84
    const/4 v8, 0x1

    invoke-virtual {v2, v8}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 72
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    .line 86
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFourthView;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8, v11}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 87
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFourthView;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8, v11}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 88
    const/4 v8, 0x0

    invoke-virtual {v2, v8}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_1
.end method


# virtual methods
.method public getView()Landroid/view/View;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFourthView;->mView:Landroid/view/View;

    return-object v0
.end method
