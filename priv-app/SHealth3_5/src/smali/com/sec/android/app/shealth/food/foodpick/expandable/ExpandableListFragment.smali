.class public Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;
.super Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;
.source "ExpandableListFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/food/foodpick/expandable/IExpandableListFragmentProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment$OnHeaderClickListener;
    }
.end annotation


# static fields
.field private static final CHECKED_LIST_ITEMS:Ljava/lang/String; = "CHECKED_LIST_ITEMS"

.field private static final DEFAULT_DATA_SET_WAS_INVALIDATED:Z = true

.field public static final EMPTY_STRING:Ljava/lang/String; = ""

.field private static final EXPANDABLE_LIST_VIEW:Ljava/lang/String; = "EXPANDABLE_LIST_VIEW"


# instance fields
.field private mFragmentContainer:Lcom/sec/android/app/shealth/food/foodpick/expandable/IExpandableListFragmentContainer;

.field private mFragmentParams:Landroid/os/Bundle;

.field private mHoldersList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;",
            ">;"
        }
    .end annotation
.end field

.field private mMealsList:Landroid/widget/ExpandableListView;

.field private mOnAddMyFoodClickListener:Lcom/sec/android/app/shealth/food/foodpick/expandable/listener/OnAddCustomFoodListener;

.field private mOnClickListener:Lcom/sec/android/app/shealth/food/foodpick/expandable/listener/OnSelectedPanelItemSelectedListener;

.field private mOnHeaderClickListener:Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment$OnHeaderClickListener;

.field private mOnLongClickListener:Lcom/sec/android/app/shealth/food/foodpick/expandable/listener/OnListItemLongClickListener;

.field private mSelectAllCheckbox:Landroid/widget/CheckBox;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;-><init>()V

    .line 354
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->refreshSelectAllCheckBox()Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;)Lcom/sec/android/app/shealth/food/foodpick/expandable/listener/OnListItemLongClickListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->mOnLongClickListener:Lcom/sec/android/app/shealth/food/foodpick/expandable/listener/OnListItemLongClickListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;)Lcom/sec/android/app/shealth/food/foodpick/expandable/listener/OnSelectedPanelItemSelectedListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->mOnClickListener:Lcom/sec/android/app/shealth/food/foodpick/expandable/listener/OnSelectedPanelItemSelectedListener;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->refreshMainScreen(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;)Lcom/sec/android/app/shealth/food/foodpick/expandable/listener/OnAddCustomFoodListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->mOnAddMyFoodClickListener:Lcom/sec/android/app/shealth/food/foodpick/expandable/listener/OnAddCustomFoodListener;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->mSelectAllCheckbox:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;)Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->getFoodListExpandableAdapter()Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;)Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment$OnHeaderClickListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->mOnHeaderClickListener:Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment$OnHeaderClickListener;

    return-object v0
.end method

.method private collapseAllGroups()V
    .locals 3

    .prologue
    .line 281
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->mMealsList:Landroid/widget/ExpandableListView;

    invoke-virtual {v2}, Landroid/widget/ExpandableListView;->getExpandableListAdapter()Landroid/widget/ExpandableListAdapter;

    move-result-object v2

    invoke-interface {v2}, Landroid/widget/ExpandableListAdapter;->getGroupCount()I

    move-result v0

    .line 282
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 283
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->mMealsList:Landroid/widget/ExpandableListView;

    invoke-virtual {v2, v1}, Landroid/widget/ExpandableListView;->collapseGroup(I)Z

    .line 282
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 285
    :cond_0
    return-void
.end method

.method private findViews(Landroid/view/View;)V
    .locals 1
    .param p1, "parent"    # Landroid/view/View;

    .prologue
    .line 105
    const v0, 0x7f080456

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ExpandableListView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->mMealsList:Landroid/widget/ExpandableListView;

    .line 106
    const v0, 0x7f080454

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->mSelectAllCheckbox:Landroid/widget/CheckBox;

    .line 107
    return-void
.end method

.method private getCategoriesMap()Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 152
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    .line 153
    .local v1, "holdersMap":Ljava/util/Map;, "Ljava/util/Map<Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;Ljava/util/List<Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;>;>;"
    const/4 v3, 0x0

    .line 154
    .local v3, "listItems":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;>;"
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->mHoldersList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;

    .line 156
    .local v0, "holder":Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->mFragmentContainer:Lcom/sec/android/app/shealth/food/foodpick/expandable/IExpandableListFragmentContainer;

    if-eqz v4, :cond_1

    .line 157
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->mFragmentContainer:Lcom/sec/android/app/shealth/food/foodpick/expandable/IExpandableListFragmentContainer;

    invoke-interface {v4}, Lcom/sec/android/app/shealth/food/foodpick/expandable/IExpandableListFragmentContainer;->isDeleteMode()Z

    move-result v4

    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->setAreItemsCheckable(Z)V

    .line 160
    :cond_1
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->getFoodCategoryListItemsCreator()Lcom/sec/android/app/shealth/food/foodpick/expandable/IFoodCategoryListItemsCreator;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/sec/android/app/shealth/food/foodpick/expandable/IFoodCategoryListItemsCreator;->getFoodListItems(Landroid/content/Context;)Ljava/util/List;

    move-result-object v3

    .line 161
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 162
    invoke-interface {v1, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 165
    .end local v0    # "holder":Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;
    :cond_2
    return-object v1
.end method

.method private getFoodListExpandableAdapter()Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->mMealsList:Landroid/widget/ExpandableListView;

    invoke-virtual {v0}, Landroid/widget/ExpandableListView;->getExpandableListAdapter()Landroid/widget/ExpandableListAdapter;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;

    return-object v0
.end method

.method private initAdapterListeners(Z)V
    .locals 2
    .param p1, "isDeleteMode"    # Z

    .prologue
    .line 172
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->getFoodListExpandableAdapter()Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;

    move-result-object v0

    .line 173
    .local v0, "expandableAdapter":Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->removeAllListeners()V

    .line 174
    if-eqz p1, :cond_2

    .line 175
    new-instance v1, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment$1;-><init>(Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->addOnItemClickListener(Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$OnItemClickListener;)V

    .line 194
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->mOnClickListener:Lcom/sec/android/app/shealth/food/foodpick/expandable/listener/OnSelectedPanelItemSelectedListener;

    if-eqz v1, :cond_1

    .line 195
    new-instance v1, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment$3;-><init>(Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->addOnItemClickListener(Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$OnItemClickListener;)V

    .line 202
    :cond_1
    new-instance v1, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment$4;-><init>(Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->addOnFavoriteClickListener(Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$OnFavoriteClickListener;)V

    .line 208
    return-void

    .line 183
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->mOnLongClickListener:Lcom/sec/android/app/shealth/food/foodpick/expandable/listener/OnListItemLongClickListener;

    if-eqz v1, :cond_0

    .line 184
    new-instance v1, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment$2;-><init>(Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->setOnItemLongClickListener(Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$OnItemLongClickListener;)V

    goto :goto_0
.end method

.method private refreshListAdapter(Z)V
    .locals 4
    .param p1, "dataSetWasUpdated"    # Z

    .prologue
    .line 128
    if-eqz p1, :cond_4

    .line 129
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->mFragmentParams:Landroid/os/Bundle;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->mFragmentParams:Landroid/os/Bundle;

    const-string v3, "CATEGORY_HOLDERS_LIST"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 130
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->mFragmentParams:Landroid/os/Bundle;

    const-string v3, "CATEGORY_HOLDERS_LIST"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->mHoldersList:Ljava/util/List;

    .line 132
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->mHoldersList:Ljava/util/List;

    if-eqz v2, :cond_2

    .line 133
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->mMealsList:Landroid/widget/ExpandableListView;

    invoke-virtual {v2}, Landroid/widget/ExpandableListView;->getExpandableListAdapter()Landroid/widget/ExpandableListAdapter;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;

    .line 134
    .local v0, "expandableAdapter":Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;
    if-nez v0, :cond_3

    .line 135
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;

    .end local v0    # "expandableAdapter":Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->getCategoriesMap()Ljava/util/Map;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;-><init>(Landroid/content/Context;Ljava/util/Map;)V

    .line 139
    .restart local v0    # "expandableAdapter":Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->mFragmentParams:Landroid/os/Bundle;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->mFragmentParams:Landroid/os/Bundle;

    const-string/jumbo v3, "single_category_title_visible"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 140
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->mFragmentParams:Landroid/os/Bundle;

    const-string/jumbo v3, "single_category_title_visible"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 141
    .local v1, "isSingleCaptionVisible":Z
    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->setIsSingleCategoryCaptionVisible(Z)V

    .line 143
    .end local v1    # "isSingleCaptionVisible":Z
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->mMealsList:Landroid/widget/ExpandableListView;

    invoke-virtual {v2, v0}, Landroid/widget/ExpandableListView;->setAdapter(Landroid/widget/ExpandableListAdapter;)V

    .line 145
    .end local v0    # "expandableAdapter":Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->getView()Landroid/view/View;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->refreshMainScreen(Landroid/view/View;)V

    .line 149
    :goto_1
    return-void

    .line 137
    .restart local v0    # "expandableAdapter":Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->getCategoriesMap()Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->setItemsMap(Ljava/util/Map;)V

    goto :goto_0

    .line 147
    .end local v0    # "expandableAdapter":Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;
    :cond_4
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->getFoodListExpandableAdapter()Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->mFragmentContainer:Lcom/sec/android/app/shealth/food/foodpick/expandable/IExpandableListFragmentContainer;

    invoke-interface {v3}, Lcom/sec/android/app/shealth/food/foodpick/expandable/IExpandableListFragmentContainer;->isDeleteMode()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->setCheckable(Z)V

    goto :goto_1
.end method

.method private refreshMainScreen(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 211
    const v1, 0x7f080459

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 212
    .local v0, "noDataLayout":Landroid/view/View;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->isExpandableListEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 213
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->mMealsList:Landroid/widget/ExpandableListView;

    invoke-virtual {v1, v3}, Landroid/widget/ExpandableListView;->setVisibility(I)V

    .line 214
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 219
    :goto_0
    return-void

    .line 216
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->mMealsList:Landroid/widget/ExpandableListView;

    invoke-virtual {v1, v2}, Landroid/widget/ExpandableListView;->setVisibility(I)V

    .line 217
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private refreshSelectAllCheckBox()Z
    .locals 2

    .prologue
    .line 347
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->mSelectAllCheckbox:Landroid/widget/CheckBox;

    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->getFoodListExpandableAdapter()Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->areAllItemsChecked()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 348
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->mSelectAllCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    return v0
.end method

.method private showSelectAllCheckbox()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 322
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->getView()Landroid/view/View;

    move-result-object v3

    const v4, 0x7f080452

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 323
    .local v2, "headerView":Landroid/view/View;
    const v3, 0x7f080455

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 325
    .local v1, "headerText":Landroid/widget/TextView;
    const v3, 0x7f080453

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 326
    .local v0, "addToMyFood":Landroid/view/View;
    new-instance v3, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment$6;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment$6;-><init>(Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 338
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->mSelectAllCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v3, v5}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 339
    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 340
    const v3, 0x7f090071

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    .line 341
    const v3, 0x7f020879

    invoke-virtual {v2, v3}, Landroid/view/View;->setBackgroundResource(I)V

    .line 342
    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 343
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->refreshSelectAllCheckBox()Z

    .line 344
    return-void
.end method

.method private tryToShowAddMyFoodHeader(Landroid/view/View;)V
    .locals 7
    .param p1, "parent"    # Landroid/view/View;

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x0

    .line 294
    const v3, 0x7f080452

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 295
    .local v2, "headerView":Landroid/view/View;
    const v3, 0x7f080455

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 297
    .local v1, "headerText":Landroid/widget/TextView;
    const v3, 0x7f080453

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 298
    .local v0, "addToMyFood":Landroid/view/View;
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->mFragmentParams:Landroid/os/Bundle;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->mFragmentParams:Landroid/os/Bundle;

    const-string v4, "ADD_CUSTOM_FOOD_LAYOUT_IS_VISIBLE"

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 300
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->mSelectAllCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v3, v6}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 301
    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 302
    const v3, 0x7f090942

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    .line 303
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->mOnAddMyFoodClickListener:Lcom/sec/android/app/shealth/food/foodpick/expandable/listener/OnAddCustomFoodListener;

    if-eqz v3, :cond_0

    .line 304
    new-instance v3, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment$5;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment$5;-><init>(Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 312
    :cond_0
    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 316
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->refreshFragmentFocusables()V

    .line 317
    sget-object v3, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;->TYPE_TOOL_TIP:Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0901e2

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, ""

    invoke-static {v0, v3, v4, v5}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->setHoverPopupListener(Landroid/view/View;Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;Ljava/lang/String;Ljava/lang/String;)V

    .line 319
    return-void

    .line 314
    :cond_1
    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public deleteCheckedItems()Z
    .locals 4

    .prologue
    .line 266
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->getCheckedItems()Ljava/util/Collection;

    move-result-object v0

    .line 267
    .local v0, "checkedList":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;>;"
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;

    .line 268
    .local v2, "listItem":Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->removeFromMyCategory()V

    goto :goto_0

    .line 270
    .end local v2    # "listItem":Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;
    :cond_0
    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    const/4 v3, 0x1

    :goto_1
    return v3

    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public getCheckedItems()Ljava/util/Collection;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 232
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->getFoodListExpandableAdapter()Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;

    move-result-object v0

    .line 233
    .local v0, "adapter":Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;
    if-nez v0, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->getCheckedItems()Ljava/util/Collection;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableCollection(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v1

    goto :goto_0
.end method

.method public getCheckedItemsAmount()I
    .locals 2

    .prologue
    .line 240
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->getFoodListExpandableAdapter()Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;

    move-result-object v0

    .line 241
    .local v0, "adapter":Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->getCheckedItems()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v1

    goto :goto_0
.end method

.method public getExpandableListAdapter()Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;
    .locals 1

    .prologue
    .line 277
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->mMealsList:Landroid/widget/ExpandableListView;

    invoke-virtual {v0}, Landroid/widget/ExpandableListView;->getExpandableListAdapter()Landroid/widget/ExpandableListAdapter;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;

    return-object v0
.end method

.method public isExpandableListEmpty()Z
    .locals 1

    .prologue
    .line 227
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->getFoodListExpandableAdapter()Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->getFoodListExpandableAdapter()Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->isItemsListEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 111
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onAttach(Landroid/app/Activity;)V

    move-object v0, p1

    .line 112
    check-cast v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/IExpandableListFragmentContainer;

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->mFragmentContainer:Lcom/sec/android/app/shealth/food/foodpick/expandable/IExpandableListFragmentContainer;

    .line 113
    instance-of v0, p1, Lcom/sec/android/app/shealth/food/foodpick/expandable/listener/OnSelectedPanelItemSelectedListener;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 114
    check-cast v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/listener/OnSelectedPanelItemSelectedListener;

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->mOnClickListener:Lcom/sec/android/app/shealth/food/foodpick/expandable/listener/OnSelectedPanelItemSelectedListener;

    .line 116
    :cond_0
    instance-of v0, p1, Lcom/sec/android/app/shealth/food/foodpick/expandable/listener/OnListItemLongClickListener;

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 117
    check-cast v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/listener/OnListItemLongClickListener;

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->mOnLongClickListener:Lcom/sec/android/app/shealth/food/foodpick/expandable/listener/OnListItemLongClickListener;

    .line 119
    :cond_1
    instance-of v0, p1, Lcom/sec/android/app/shealth/food/foodpick/expandable/listener/OnAddCustomFoodListener;

    if-eqz v0, :cond_2

    move-object v0, p1

    .line 120
    check-cast v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/listener/OnAddCustomFoodListener;

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->mOnAddMyFoodClickListener:Lcom/sec/android/app/shealth/food/foodpick/expandable/listener/OnAddCustomFoodListener;

    .line 122
    :cond_2
    instance-of v0, p1, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment$OnHeaderClickListener;

    if-eqz v0, :cond_3

    .line 123
    check-cast p1, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment$OnHeaderClickListener;

    .end local p1    # "activity":Landroid/app/Activity;
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->mOnHeaderClickListener:Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment$OnHeaderClickListener;

    .line 125
    :cond_3
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 65
    const v1, 0x7f030106

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 66
    .local v0, "result":Landroid/view/View;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->mFragmentParams:Landroid/os/Bundle;

    .line 67
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->findViews(Landroid/view/View;)V

    .line 68
    return-object v0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 74
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->isAdded()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 75
    const-string v3, "EXPANDABLE_LIST_VIEW"

    iget-object v4, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->mMealsList:Landroid/widget/ExpandableListView;

    invoke-virtual {v4}, Landroid/widget/ExpandableListView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 78
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->mFragmentContainer:Lcom/sec/android/app/shealth/food/foodpick/expandable/IExpandableListFragmentContainer;

    invoke-interface {v3}, Lcom/sec/android/app/shealth/food/foodpick/expandable/IExpandableListFragmentContainer;->isDeleteMode()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 79
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 80
    .local v1, "itemDataHolderList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/food/foodpick/expandable/CheckedListItemDataHolder;>;"
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->getFoodListExpandableAdapter()Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->getCheckedItems()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;

    .line 81
    .local v2, "listItem":Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;
    new-instance v3, Lcom/sec/android/app/shealth/food/foodpick/expandable/CheckedListItemDataHolder;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->getListItemId()J

    move-result-wide v4

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->getListItemType()Lcom/sec/android/app/shealth/food/foodpick/expandable/IListItem$ListItemType;

    move-result-object v6

    invoke-direct {v3, v4, v5, v6}, Lcom/sec/android/app/shealth/food/foodpick/expandable/CheckedListItemDataHolder;-><init>(JLcom/sec/android/app/shealth/food/foodpick/expandable/IListItem$ListItemType;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 84
    .end local v2    # "listItem":Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;
    :cond_1
    const-string v3, "CHECKED_LIST_ITEMS"

    invoke-virtual {p1, v3, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 86
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "itemDataHolderList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/food/foodpick/expandable/CheckedListItemDataHolder;>;"
    :cond_2
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 87
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 91
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 92
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->mFragmentParams:Landroid/os/Bundle;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->updateFragmentState(Landroid/os/Bundle;Z)V

    .line 93
    if-eqz p2, :cond_1

    .line 94
    const-string v0, "CHECKED_LIST_ITEMS"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 95
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->getFoodListExpandableAdapter()Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;

    move-result-object v0

    const-string v1, "CHECKED_LIST_ITEMS"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->restoreCheckedStateForListItems(Ljava/util/List;)V

    .line 98
    :cond_0
    const-string v0, "EXPANDABLE_LIST_VIEW"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 99
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->mMealsList:Landroid/widget/ExpandableListView;

    const-string v1, "EXPANDABLE_LIST_VIEW"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 102
    :cond_1
    return-void
.end method

.method public uncheckAll()V
    .locals 3

    .prologue
    .line 288
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->getCheckedItems()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;

    .line 289
    .local v1, "listItem":Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->setChecked(Z)V

    goto :goto_0

    .line 291
    .end local v1    # "listItem":Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;
    :cond_0
    return-void
.end method

.method public updateFragmentState(Landroid/os/Bundle;Z)V
    .locals 2
    .param p1, "params"    # Landroid/os/Bundle;
    .param p2, "dataSetWasUpdated"    # Z

    .prologue
    .line 246
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->mFragmentParams:Landroid/os/Bundle;

    .line 247
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->mFragmentContainer:Lcom/sec/android/app/shealth/food/foodpick/expandable/IExpandableListFragmentContainer;

    invoke-interface {v1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/IExpandableListFragmentContainer;->isDeleteMode()Z

    move-result v0

    .line 248
    .local v0, "isDeleteMode":Z
    invoke-direct {p0, p2}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->refreshListAdapter(Z)V

    .line 249
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->initAdapterListeners(Z)V

    .line 250
    if-eqz v0, :cond_0

    .line 254
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->collapseAllGroups()V

    .line 261
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->getFoodListExpandableAdapter()Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->notifyDataSetChanged()V

    .line 262
    return-void

    .line 256
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->getView()Landroid/view/View;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->tryToShowAddMyFoodHeader(Landroid/view/View;)V

    .line 257
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->uncheckAll()V

    goto :goto_0
.end method
