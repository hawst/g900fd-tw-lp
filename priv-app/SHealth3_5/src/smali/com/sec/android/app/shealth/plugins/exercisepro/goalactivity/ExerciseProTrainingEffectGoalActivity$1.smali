.class Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity$1;
.super Ljava/lang/Object;
.source "ExerciseProTrainingEffectGoalActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;)V
    .locals 0

    .prologue
    .line 76
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 79
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->mActivityType:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;)I

    move-result v2

    const/4 v3, 0x5

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->mPosition:I
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->access$100()I

    move-result v4

    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->setActivityTypeGoalValue(Landroid/content/Context;III)I

    .line 80
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;

    const-class v2, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 82
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "training_effect_mode"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 83
    const-string/jumbo v1, "pick_type"

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->mActivityType:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 84
    const/high16 v1, 0x24010000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 87
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;

    const/16 v2, 0x7c

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 88
    return-void
.end method
