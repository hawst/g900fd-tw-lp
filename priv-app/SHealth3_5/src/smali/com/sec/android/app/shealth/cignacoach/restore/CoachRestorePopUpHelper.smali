.class public Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;
.super Ljava/lang/Object;
.source "CoachRestorePopUpHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper$CheckForLocalDataTask;
    }
.end annotation


# static fields
.field public static final POPUP_DELAY:I = 0x64

.field private static final TAG:Ljava/lang/String;

.field public static final TAG_TRIGGER_POPUP:Ljava/lang/String; = "COACH_PROMPT_RESTORE"

.field private static dialogButtonType:I

.field private static popupLayoutRID:I

.field private static titleId:I


# instance fields
.field private activity:Landroid/app/Activity;

.field private mBackupRestore:Landroid/widget/CheckBox;

.field private mCheckForLocalDataTask:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper$CheckForLocalDataTask;

.field private mConfirmDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

.field private mDoNotShowAgain:Landroid/widget/CheckBox;

.field private mHandler:Landroid/os/Handler;

.field private mRunnable:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->TAG:Ljava/lang/String;

    .line 36
    const v0, 0x7f030093

    sput v0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->popupLayoutRID:I

    .line 37
    const/4 v0, 0x2

    sput v0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->dialogButtonType:I

    .line 38
    const v0, 0x7f090139

    sput v0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->titleId:I

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->mConfirmDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 50
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->activity:Landroid/app/Activity;

    .line 51
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->mHandler:Landroid/os/Handler;

    .line 52
    return-void
.end method

.method static synthetic access$000()I
    .locals 1

    .prologue
    .line 30
    sget v0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->popupLayoutRID:I

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->mDoNotShowAgain:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;Landroid/widget/CheckBox;)Landroid/widget/CheckBox;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;
    .param p1, "x1"    # Landroid/widget/CheckBox;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->mDoNotShowAgain:Landroid/widget/CheckBox;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->mBackupRestore:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;Landroid/widget/CheckBox;)Landroid/widget/CheckBox;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;
    .param p1, "x1"    # Landroid/widget/CheckBox;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->mBackupRestore:Landroid/widget/CheckBox;

    return-object p1
.end method

.method static synthetic access$300()I
    .locals 1

    .prologue
    .line 30
    sget v0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->titleId:I

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;)Landroid/app/Activity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->activity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$500()I
    .locals 1

    .prologue
    .line 30
    sget v0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->dialogButtonType:I

    return v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->mConfirmDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    return-object v0
.end method

.method static synthetic access$602(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->mConfirmDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    return-object p1
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;
    .param p1, "x1"    # Z

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->showRestoreTriggerPopup(Z)V

    return-void
.end method

.method private dismiss()V
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->mConfirmDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->mConfirmDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 197
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->mConfirmDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->dismissAllowingStateLoss()V

    .line 198
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->mConfirmDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 200
    :cond_0
    return-void
.end method

.method private showRestoreTriggerPopup(Z)V
    .locals 4
    .param p1, "isLocalDataPresent"    # Z

    .prologue
    .line 130
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->dismiss()V

    .line 132
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper$1;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper$1;-><init>(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;Z)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->mRunnable:Ljava/lang/Runnable;

    .line 191
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->mRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 192
    return-void
.end method


# virtual methods
.method public destroyDialog()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 116
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->TAG:Ljava/lang/String;

    const-string v1, "Destroy coach restore dialog"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->mCheckForLocalDataTask:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper$CheckForLocalDataTask;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->mCheckForLocalDataTask:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper$CheckForLocalDataTask;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper$CheckForLocalDataTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->mCheckForLocalDataTask:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper$CheckForLocalDataTask;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper$CheckForLocalDataTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->PENDING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_1

    .line 119
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->mCheckForLocalDataTask:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper$CheckForLocalDataTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper$CheckForLocalDataTask;->cancel(Z)Z

    .line 121
    :cond_1
    iput-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->mDoNotShowAgain:Landroid/widget/CheckBox;

    .line 122
    iput-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->mBackupRestore:Landroid/widget/CheckBox;

    .line 123
    iput-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->mConfirmDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 125
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->mRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 126
    return-void
.end method

.method public dismissRestoreTriggerPopup()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 91
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->TAG:Ljava/lang/String;

    const-string v2, "Dismiss coach restore dialog"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->mRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 95
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->mCheckForLocalDataTask:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper$CheckForLocalDataTask;

    if-eqz v0, :cond_0

    .line 96
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->mCheckForLocalDataTask:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper$CheckForLocalDataTask;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper$CheckForLocalDataTask;->cancel(Z)Z

    .line 98
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->mDoNotShowAgain:Landroid/widget/CheckBox;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->mConfirmDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->mConfirmDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 99
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->mConfirmDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getView()Landroid/view/View;

    move-result-object v0

    const v2, 0x7f0802ba

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->mDoNotShowAgain:Landroid/widget/CheckBox;

    .line 102
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->mDoNotShowAgain:Landroid/widget/CheckBox;

    if-eqz v0, :cond_2

    .line 103
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->activity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->mDoNotShowAgain:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    :goto_0
    invoke-static {v2, v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setCignaRestoreTriggerPopup(Landroid/content/Context;Z)V

    .line 106
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->mConfirmDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->mConfirmDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 107
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->mConfirmDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->dismissAllowingStateLoss()V

    .line 108
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->mConfirmDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 112
    :cond_3
    return-void

    .line 103
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isInitialBackupRequired()Z
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->mBackupRestore:Landroid/widget/CheckBox;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->mBackupRestore:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isShowing()Z
    .locals 2

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->mCheckForLocalDataTask:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper$CheckForLocalDataTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->mCheckForLocalDataTask:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper$CheckForLocalDataTask;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper$CheckForLocalDataTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-eq v0, v1, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->mConfirmDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->mConfirmDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public showDialog()V
    .locals 2

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->TAG:Ljava/lang/String;

    const-string v1, "Skipping show coach restore dilaog, since older method call still under execution"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    :goto_0
    return-void

    .line 66
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->TAG:Ljava/lang/String;

    const-string v1, "Show coach restore dialog"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper$CheckForLocalDataTask;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper$CheckForLocalDataTask;-><init>(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->mCheckForLocalDataTask:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper$CheckForLocalDataTask;

    .line 69
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->mCheckForLocalDataTask:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper$CheckForLocalDataTask;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper$CheckForLocalDataTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method
