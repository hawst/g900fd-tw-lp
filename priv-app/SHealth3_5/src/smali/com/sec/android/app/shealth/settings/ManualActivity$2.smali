.class Lcom/sec/android/app/shealth/settings/ManualActivity$2;
.super Ljava/lang/Object;
.source "ManualActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/settings/ManualActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/ManualActivity;)V
    .locals 0

    .prologue
    .line 199
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    const/4 v2, 0x5

    .line 201
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/shealth/settings/ManualActivity;->popupMode:I
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/ManualActivity;->access$102(Lcom/sec/android/app/shealth/settings/ManualActivity;I)I

    .line 203
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ManualActivity;->networkUtils:Lcom/sec/android/app/shealth/common/utils/NetworkUtils;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/ManualActivity;->access$400(Lcom/sec/android/app/shealth/settings/ManualActivity;)Lcom/sec/android/app/shealth/common/utils/NetworkUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;->isWifiEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 243
    :cond_0
    :goto_0
    return-void

    .line 207
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ManualActivity;->networkUtils:Lcom/sec/android/app/shealth/common/utils/NetworkUtils;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/ManualActivity;->access$400(Lcom/sec/android/app/shealth/settings/ManualActivity;)Lcom/sec/android/app/shealth/common/utils/NetworkUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;->isAirplaneModeEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 208
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/shealth/settings/ManualActivity;->popupMode:I
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/ManualActivity;->access$102(Lcom/sec/android/app/shealth/settings/ManualActivity;I)I

    .line 209
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/ManualActivity;->connectionFailBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/ManualActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "data_connection_dialog"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 210
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ManualActivity;->networkUtils:Lcom/sec/android/app/shealth/common/utils/NetworkUtils;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/ManualActivity;->access$400(Lcom/sec/android/app/shealth/settings/ManualActivity;)Lcom/sec/android/app/shealth/common/utils/NetworkUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;->isSimReady()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 211
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ManualActivity;->networkUtils:Lcom/sec/android/app/shealth/common/utils/NetworkUtils;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/ManualActivity;->access$400(Lcom/sec/android/app/shealth/settings/ManualActivity;)Lcom/sec/android/app/shealth/common/utils/NetworkUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;->isMobileDataEnabled()Z

    move-result v0

    if-nez v0, :cond_3

    .line 212
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    const/4 v1, 0x2

    # setter for: Lcom/sec/android/app/shealth/settings/ManualActivity;->popupMode:I
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/ManualActivity;->access$102(Lcom/sec/android/app/shealth/settings/ManualActivity;I)I

    .line 213
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/ManualActivity;->connectionFailBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/ManualActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "data_connection_dialog"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 214
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ManualActivity;->networkUtils:Lcom/sec/android/app/shealth/common/utils/NetworkUtils;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/ManualActivity;->access$400(Lcom/sec/android/app/shealth/settings/ManualActivity;)Lcom/sec/android/app/shealth/common/utils/NetworkUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;->isRoamingEnabled()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 215
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ManualActivity;->networkUtils:Lcom/sec/android/app/shealth/common/utils/NetworkUtils;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/ManualActivity;->access$400(Lcom/sec/android/app/shealth/settings/ManualActivity;)Lcom/sec/android/app/shealth/common/utils/NetworkUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;->isDataRoamingEnabled()Z

    move-result v0

    if-nez v0, :cond_4

    .line 216
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    const/4 v1, 0x3

    # setter for: Lcom/sec/android/app/shealth/settings/ManualActivity;->popupMode:I
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/ManualActivity;->access$102(Lcom/sec/android/app/shealth/settings/ManualActivity;I)I

    .line 217
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/ManualActivity;->connectionFailBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/ManualActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "data_connection_dialog"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 218
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    invoke-static {v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isManualRoamingPopupWasShown(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 219
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    const/4 v1, 0x7

    # setter for: Lcom/sec/android/app/shealth/settings/ManualActivity;->popupMode:I
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/ManualActivity;->access$102(Lcom/sec/android/app/shealth/settings/ManualActivity;I)I

    .line 220
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/ManualActivity;->connectionBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/ManualActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "data_connection_dialog"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 222
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ManualActivity;->networkUtils:Lcom/sec/android/app/shealth/common/utils/NetworkUtils;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/ManualActivity;->access$400(Lcom/sec/android/app/shealth/settings/ManualActivity;)Lcom/sec/android/app/shealth/common/utils/NetworkUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;->isMoblieDataLimit()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 223
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    const/4 v1, 0x4

    # setter for: Lcom/sec/android/app/shealth/settings/ManualActivity;->popupMode:I
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/ManualActivity;->access$102(Lcom/sec/android/app/shealth/settings/ManualActivity;I)I

    .line 224
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/ManualActivity;->dataLimitBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/ManualActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "data_connection_dialog"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 226
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ManualActivity;->networkUtils:Lcom/sec/android/app/shealth/common/utils/NetworkUtils;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/ManualActivity;->access$400(Lcom/sec/android/app/shealth/settings/ManualActivity;)Lcom/sec/android/app/shealth/common/utils/NetworkUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;->isMobileDataEnabled()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 227
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    invoke-static {v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isManualMobilePopupWasShown(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 228
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    const/4 v1, 0x6

    # setter for: Lcom/sec/android/app/shealth/settings/ManualActivity;->popupMode:I
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/ManualActivity;->access$102(Lcom/sec/android/app/shealth/settings/ManualActivity;I)I

    .line 229
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/ManualActivity;->connectionBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/ManualActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "data_connection_dialog"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 233
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    # setter for: Lcom/sec/android/app/shealth/settings/ManualActivity;->popupMode:I
    invoke-static {v0, v2}, Lcom/sec/android/app/shealth/settings/ManualActivity;->access$102(Lcom/sec/android/app/shealth/settings/ManualActivity;I)I

    .line 234
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/ManualActivity;->outOfServiceBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/ManualActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "data_connection_dialog"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 238
    :cond_8
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    # setter for: Lcom/sec/android/app/shealth/settings/ManualActivity;->popupMode:I
    invoke-static {v0, v2}, Lcom/sec/android/app/shealth/settings/ManualActivity;->access$102(Lcom/sec/android/app/shealth/settings/ManualActivity;I)I

    .line 239
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/ManualActivity;->outOfServiceBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/ManualActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "data_connection_dialog"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
