.class public Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;
.super Landroid/support/v4/app/FragmentActivity;

# interfaces
.implements Lcom/sec/android/app/shealth/framework/ui/calendar/OnPeriodSelectedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity$CalendarFragmentAdapter;
    }
.end annotation


# static fields
.field private static final RESULT_OK:I = 0x64


# instance fields
.field private mBtnNextPeriod:Landroid/widget/Button;

.field private mBtnPrevPeriod:Landroid/widget/Button;

.field private mCalendarCurrentMonth:Landroid/widget/TextView;

.field private mCalendarCurrentYear:Landroid/widget/TextView;

.field private mCalendarTodayButton:Landroid/widget/TextView;

.field private mCancelButton:Landroid/view/View;

.field private mDataType:Ljava/lang/String;

.field private mItemPeriodType:I

.field private mPagePeriodType:I

.field private mPager:Landroid/support/v4/view/ViewPager;

.field private mPeriodStarts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mSelectedTime:J

.field private mTimeColumnName:Ljava/lang/String;

.field private mVisibleDate:Ljava/util/Calendar;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mDataType:Ljava/lang/String;

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mVisibleDate:Ljava/util/Calendar;

    const-string/jumbo v0, "sample_time"

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mTimeColumnName:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->pageSelected(I)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;)Landroid/support/v4/view/ViewPager;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mPager:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mDataType:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mTimeColumnName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;)I
    .locals 1

    iget v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mPagePeriodType:I

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->addToPeriodStarts(Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->addTwoPreviousMonthsToPeriodStarts()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->addMonthsFromDB()V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->setNewAdapter(Z)V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->checkPeriodsAvailable()V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mPeriodStarts:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;)J
    .locals 2

    iget-wide v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mSelectedTime:J

    return-wide v0
.end method

.method private addMonthsFromDB()V
    .locals 9

    const/4 v4, 0x1

    const/4 v2, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mTimeColumnName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " <? "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v4, v4, [Ljava/lang/String;

    const-string v0, ""

    aput-object v0, v4, v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v2

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mDataType:Ljava/lang/String;

    if-eqz v0, :cond_1

    const/4 v6, 0x0

    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mDataType:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v8, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mTimeColumnName:Ljava/lang/String;

    aput-object v8, v2, v5

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mTimeColumnName:Ljava/lang/String;

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, " ASC "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    if-eqz v1, :cond_3

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_3

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mPeriodStarts:Ljava/util/List;

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mTimeColumnName:Ljava/lang/String;

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfMonth(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mTimeColumnName:Ljava/lang/String;

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfMonth(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    :catch_0
    move-exception v0

    :goto_1
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    if-eqz v1, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    :goto_2
    return-void

    :cond_2
    :try_start_3
    invoke-direct {p0, v7}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->addToPeriodStarts(Ljava/util/List;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :cond_3
    if-eqz v1, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_2

    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_3
    if-eqz v1, :cond_4

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    :catchall_1
    move-exception v0

    goto :goto_3

    :catch_1
    move-exception v0

    move-object v1, v6

    goto :goto_1
.end method

.method private addToPeriodStarts(Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mPeriodStarts:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mPeriodStarts:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v2, v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mPeriodStarts:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v1, v4, v6

    if-gez v1, :cond_1

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mPeriodStarts:Ljava/util/List;

    invoke-interface {v1, v2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_0

    :cond_2
    return-void
.end method

.method private addTwoPreviousMonthsToPeriodStarts()V
    .locals 5

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfMonth(J)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    const/4 v0, 0x0

    :goto_0
    const/4 v3, 0x3

    if-ge v0, v3, :cond_0

    const/4 v3, 0x2

    const/4 v4, -0x1

    invoke-virtual {v1, v3, v4}, Ljava/util/Calendar;->add(II)V

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->addToPeriodStarts(Ljava/util/List;)V

    return-void
.end method

.method private checkPeriodsAvailable()V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mBtnPrevPeriod:Landroid/widget/Button;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mBtnPrevPeriod:Landroid/widget/Button;

    iget-object v3, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mBtnPrevPeriod:Landroid/widget/Button;

    invoke-virtual {v3}, Landroid/widget/Button;->isEnabled()Z

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mBtnNextPeriod:Landroid/widget/Button;

    iget-object v3, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v3}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mPeriodStarts:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-eq v3, v4, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mBtnNextPeriod:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mBtnNextPeriod:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->isEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setFocusable(Z)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method private findIndex(J)I
    .locals 5

    const/4 v2, 0x0

    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mPeriodStarts:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mPeriodStarts:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    cmp-long v0, v3, p1

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mPeriodStarts:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    iget v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mPagePeriodType:I

    invoke-static {v3, v4, v0}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfCurrentPeriod(JI)J

    move-result-wide v3

    cmp-long v0, p1, v3

    if-ltz v0, :cond_1

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mPeriodStarts:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v1, v0, :cond_2

    :goto_1
    return v2

    :cond_2
    move v2, v1

    goto :goto_1
.end method

.method private pageSelected(I)V
    .locals 11

    const/4 v10, 0x2

    const/4 v9, 0x1

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->checkPeriodsAvailable()V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mPeriodStarts:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_4

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mPeriodStarts:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mVisibleDate:Ljava/util/Calendar;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v4

    const-string v0, ""

    const-string v1, "ko"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "zh"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "ja"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$string;->year:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    :goto_0
    iget-wide v2, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mSelectedTime:J

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mPeriodStarts:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    iget-wide v7, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mSelectedTime:J

    cmp-long v0, v5, v7

    if-gtz v0, :cond_1

    iget-wide v5, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mSelectedTime:J

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mPeriodStarts:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    iget v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mPagePeriodType:I

    invoke-static {v7, v8, v0}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfCurrentPeriod(JI)J

    move-result-wide v7

    cmp-long v0, v5, v7

    if-lez v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mPeriodStarts:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v5, Lcom/sec/android/app/shealth/framework/ui/R$array;->months:I

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    const-string v2, "ko"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string/jumbo v2, "zh"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "ja"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    :cond_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v9}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mCalendarCurrentMonth:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mCalendarCurrentYear:Landroid/widget/TextView;

    invoke-virtual {v5, v10}, Ljava/util/Calendar;->get(I)I

    move-result v2

    aget-object v0, v0, v2

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_4
    :goto_1
    return-void

    :cond_5
    const-string v1, "ar"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$array;->months_short:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mCalendarCurrentMonth:Landroid/widget/TextView;

    invoke-virtual {v5, v10}, Ljava/util/Calendar;->get(I)I

    move-result v2

    aget-object v0, v0, v2

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mCalendarCurrentMonth:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$style;->regular_normal:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mCalendarCurrentYear:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v5, v9}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mCalendarCurrentYear:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$style;->regular_light:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    goto :goto_1

    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mCalendarCurrentMonth:Landroid/widget/TextView;

    invoke-virtual {v5, v10}, Ljava/util/Calendar;->get(I)I

    move-result v2

    aget-object v0, v0, v2

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mCalendarCurrentMonth:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$style;->regular_normal:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mCalendarCurrentYear:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v5, v9}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mCalendarCurrentYear:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$style;->regular_light:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    goto/16 :goto_1

    :cond_7
    move-object v1, v0

    goto/16 :goto_0
.end method

.method private setNewAdapter(Z)V
    .locals 6

    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity$CalendarFragmentAdapter;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mPeriodStarts:Ljava/util/List;

    iget v4, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mItemPeriodType:I

    move-object v1, p0

    move v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity$CalendarFragmentAdapter;-><init>(Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;Landroid/support/v4/app/FragmentManager;Ljava/util/List;IZ)V

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mPager:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mPeriodStarts:Ljava/util/List;

    iget-wide v2, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mSelectedTime:J

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfMonth(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    return-void
.end method

.method private setRequestFocus(Landroid/view/View;)Z
    .locals 2

    const/4 v1, 0x1

    const-string v0, "audio"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->playSoundEffect(I)V

    invoke-virtual {p1}, Landroid/view/View;->requestFocus()Z

    return v1
.end method


# virtual methods
.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 4

    const/4 v3, 0x5

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v1, 0x15

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mCalendarTodayButton:Landroid/widget/TextView;

    if-eq v1, v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mBtnPrevPeriod:Landroid/widget/Button;

    if-eq v1, v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mBtnNextPeriod:Landroid/widget/Button;

    if-eq v1, v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mCancelButton:Landroid/view/View;

    if-eq v1, v0, :cond_1

    instance-of v0, v1, Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_1

    invoke-virtual {v1}, Landroid/view/View;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v2, "[0-9]+"

    invoke-virtual {v0, v2}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mVisibleDate:Ljava/util/Calendar;

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->getActualMinimum(I)I

    move-result v2

    if-eq v0, v2, :cond_1

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->setRequestFocus(Landroid/view/View;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v1, 0x16

    if-ne v0, v1, :cond_1

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mCalendarTodayButton:Landroid/widget/TextView;

    if-eq v1, v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mBtnPrevPeriod:Landroid/widget/Button;

    if-eq v1, v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mBtnNextPeriod:Landroid/widget/Button;

    if-eq v1, v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mCancelButton:Landroid/view/View;

    if-eq v1, v0, :cond_1

    instance-of v0, v1, Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_1

    invoke-virtual {v1}, Landroid/view/View;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v2, "[0-9]+"

    invoke-virtual {v0, v2}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mVisibleDate:Ljava/util/Calendar;

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->getActualMaximum(I)I

    move-result v2

    if-eq v0, v2, :cond_1

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->setRequestFocus(Landroid/view/View;)Z

    move-result v0

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v1, 0x14

    if-ne v0, v1, :cond_2

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/view/View;->getNextFocusDownId()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v0

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mCancelButton:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->setRequestFocus(Landroid/view/View;)Z

    move-result v0

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v1, 0x13

    if-ne v0, v1, :cond_4

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroid/view/View;->getNextFocusUpId()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v0

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mBtnPrevPeriod:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mBtnPrevPeriod:Landroid/widget/Button;

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->setRequestFocus(Landroid/view/View;)Z

    move-result v0

    goto/16 :goto_0

    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mBtnNextPeriod:Landroid/widget/Button;

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->setRequestFocus(Landroid/view/View;)Z

    move-result v0

    goto/16 :goto_0

    :cond_4
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    goto/16 :goto_0
.end method

.method protected getDaysStatuses(JJ)Ljava/util/TreeMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ)",
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9

    const/4 v8, 0x0

    const/4 v7, 0x3

    const/4 v6, 0x1

    const/4 v5, 0x2

    const/4 v4, 0x0

    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    sget v0, Lcom/sec/android/app/shealth/framework/ui/R$layout;->calenderrefactor:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->setContentView(I)V

    sget v0, Lcom/sec/android/app/shealth/framework/ui/R$id;->btn_previous_period:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mBtnPrevPeriod:Landroid/widget/Button;

    sget v0, Lcom/sec/android/app/shealth/framework/ui/R$id;->btn_next_period:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mBtnNextPeriod:Landroid/widget/Button;

    sget v0, Lcom/sec/android/app/shealth/framework/ui/R$id;->calendar_today:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mCalendarTodayButton:Landroid/widget/TextView;

    sget v0, Lcom/sec/android/app/shealth/framework/ui/R$id;->calendar_currentmonth:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mCalendarCurrentMonth:Landroid/widget/TextView;

    sget v0, Lcom/sec/android/app/shealth/framework/ui/R$id;->calendar_currentyear:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mCalendarCurrentYear:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mCalendarTodayButton:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$string;->prompt_today:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$string;->prompt_comma:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$string;->prompt_button:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    sget v0, Lcom/sec/android/app/shealth/framework/ui/R$id;->calendar_pager:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mPager:Landroid/support/v4/view/ViewPager;

    sget v0, Lcom/sec/android/app/shealth/framework/ui/R$id;->btn_cancel:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mCancelButton:Landroid/view/View;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mCancelButton:Landroid/view/View;

    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;->TYPE_NONE:Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;

    const-string v2, ""

    const-string v3, ""

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->setHoverPopupListener(Landroid/view/View;Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mBtnPrevPeriod:Landroid/widget/Button;

    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;->TYPE_TOOL_TIP:Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$string;->previous:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2, v8}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->setHoverPopupListener(Landroid/view/View;Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mBtnNextPeriod:Landroid/widget/Button;

    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;->TYPE_TOOL_TIP:Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$string;->next:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2, v8}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->setHoverPopupListener(Landroid/view/View;Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "column_name"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "column_name"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mTimeColumnName:Ljava/lang/String;

    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "current_time"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "current_time"

    const-wide/16 v2, -0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mSelectedTime:J

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "data_type"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "data_type"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mDataType:Ljava/lang/String;

    :cond_1
    iget v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mItemPeriodType:I

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mItemPeriodType:I

    if-ne v0, v6, :cond_6

    :cond_2
    iput v5, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mPagePeriodType:I

    :goto_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mPeriodStarts:Ljava/util/List;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mPeriodStarts:Ljava/util/List;

    iget-wide v1, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mSelectedTime:J

    iget v3, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mPagePeriodType:I

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfCurrentPeriod(JI)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v5}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    invoke-direct {p0, v4}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->setNewAdapter(Z)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v6}, Landroid/support/v4/view/ViewPager;->setDrawingCacheEnabled(Z)V

    iget-wide v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mSelectedTime:J

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->findIndex(J)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mPeriodStarts:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mPeriodStarts:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v0, v4}, Landroid/support/v4/view/ViewPager;->setCurrentItem(IZ)V

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->pageSelected(I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v4}, Landroid/support/v4/view/ViewPager;->setFocusable(Z)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mPager:Landroid/support/v4/view/ViewPager;

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity$1;-><init>(Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mBtnPrevPeriod:Landroid/widget/Button;

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity$2;-><init>(Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mBtnNextPeriod:Landroid/widget/Button;

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity$3;-><init>(Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mCancelButton:Landroid/view/View;

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity$4;-><init>(Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mBtnPrevPeriod:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mBtnNextPeriod:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mBtnPrevPeriod:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mBtnNextPeriod:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setFocusable(Z)V

    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mCalendarTodayButton:Landroid/widget/TextView;

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity$5;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity$5;-><init>(Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity$6;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity$6;-><init>(Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void

    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Intent extras not found: BundleKeys.PERIOD_TYPE_KEY and/or BundleKeys.CURRENT_TIME"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    iget v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mItemPeriodType:I

    if-ne v0, v5, :cond_7

    iput v7, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mPagePeriodType:I

    goto/16 :goto_0

    :cond_7
    iget v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mItemPeriodType:I

    if-ne v0, v7, :cond_8

    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mPagePeriodType:I

    goto/16 :goto_0

    :cond_8
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Specified period type is not supported by "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected onPause()V
    .locals 1

    const/4 v0, 0x0

    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onPause()V

    invoke-virtual {p0, v0, v0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->overridePendingTransition(II)V

    return-void
.end method

.method public onPeriodSelected(JIZ)V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "calendar_result"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const/16 v1, 0x64

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->finish()V

    return-void
.end method

.method protected onResume()V
    .locals 1

    const/4 v0, 0x0

    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onResume()V

    invoke-virtual {p0, v0, v0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->overridePendingTransition(II)V

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string/jumbo v0, "period_type"

    iget v1, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mItemPeriodType:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "data_type"

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mDataType:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "current_time"

    iget-wide v1, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mSelectedTime:J

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    return-void
.end method
