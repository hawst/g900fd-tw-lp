.class public Lcom/sec/android/app/shealth/sensor/Spo2Cal;
.super Ljava/lang/Object;
.source "Spo2Cal.java"


# static fields
.field private static final spo2_coef:[F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 6
    const/4 v0, 0x3

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/app/shealth/sensor/Spo2Cal;->spo2_coef:[F

    return-void

    nop

    :array_0
    .array-data 4
        0x40d89cd0
        -0x3db30b78    # -51.2388f
        0x42e6ed42
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static spo2cal(F)F
    .locals 4
    .param p0, "R_value"    # F

    .prologue
    .line 15
    const/4 v0, 0x0

    .line 16
    .local v0, "spo2":F
    const/4 v1, 0x0

    cmpl-float v1, p0, v1

    if-nez v1, :cond_1

    .line 17
    const/4 v0, 0x0

    .line 30
    :cond_0
    :goto_0
    const-string v1, "Spo2Cal"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "R_value+ "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Spo2 = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 31
    return v0

    .line 18
    :cond_1
    const/high16 v1, 0x428a0000    # 69.0f

    cmpl-float v1, p0, v1

    if-lez v1, :cond_2

    .line 19
    move v0, p0

    goto :goto_0

    .line 22
    :cond_2
    sget-object v1, Lcom/sec/android/app/shealth/sensor/Spo2Cal;->spo2_coef:[F

    const/4 v2, 0x0

    aget v1, v1, v2

    mul-float/2addr v1, p0

    mul-float/2addr v1, p0

    sget-object v2, Lcom/sec/android/app/shealth/sensor/Spo2Cal;->spo2_coef:[F

    const/4 v3, 0x1

    aget v2, v2, v3

    mul-float/2addr v2, p0

    add-float/2addr v1, v2

    sget-object v2, Lcom/sec/android/app/shealth/sensor/Spo2Cal;->spo2_coef:[F

    const/4 v3, 0x2

    aget v2, v2, v3

    add-float v0, v1, v2

    .line 26
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v1

    int-to-float v0, v1

    .line 27
    const/high16 v1, 0x428c0000    # 70.0f

    cmpg-float v1, v0, v1

    if-gez v1, :cond_3

    const/high16 v0, 0x428c0000    # 70.0f

    goto :goto_0

    .line 28
    :cond_3
    const/high16 v1, 0x42c80000    # 100.0f

    cmpl-float v1, v0, v1

    if-lez v1, :cond_0

    const/high16 v0, 0x42c80000    # 100.0f

    goto :goto_0
.end method
