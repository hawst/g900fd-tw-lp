.class public interface abstract Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;
.super Ljava/lang/Object;
.source "IMeasureSpecificDbOperations.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Contract::",
        "Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# virtual methods
.method public abstract getContractColumnName(Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TContract;)",
            "Ljava/lang/String;"
        }
    .end annotation
.end method

.method public abstract getContractColumnName(Ljava/util/Set;)Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<TContract;>;)",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getPeriodCursorForChartAndLogList(JJLcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;Ljava/util/Set;Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;)Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;",
            "Ljava/util/Set",
            "<TContract;>;",
            "Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;",
            "Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;",
            ")",
            "Landroid/database/Cursor;"
        }
    .end annotation
.end method

.method public abstract getPeriodCursorForChartAndLogList(JJLcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;[Ljava/lang/String;Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;)Landroid/database/Cursor;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method
