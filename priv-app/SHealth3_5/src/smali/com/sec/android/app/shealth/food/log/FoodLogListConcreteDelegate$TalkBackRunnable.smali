.class Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$TalkBackRunnable;
.super Ljava/lang/Object;
.source "FoodLogListConcreteDelegate.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TalkBackRunnable"
.end annotation


# instance fields
.field cb:Landroid/widget/CompoundButton;

.field isChecked:Z


# direct methods
.method public constructor <init>(Landroid/widget/CompoundButton;Z)V
    .locals 0
    .param p1, "button"    # Landroid/widget/CompoundButton;
    .param p2, "checked"    # Z

    .prologue
    .line 528
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 529
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$TalkBackRunnable;->cb:Landroid/widget/CompoundButton;

    .line 530
    iput-boolean p2, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$TalkBackRunnable;->isChecked:Z

    .line 531
    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 535
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$TalkBackRunnable;->cb:Landroid/widget/CompoundButton;

    if-eqz v0, :cond_0

    .line 536
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$TalkBackRunnable;->cb:Landroid/widget/CompoundButton;

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget-boolean v3, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$TalkBackRunnable;->isChecked:Z

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->announceForCheckBoxAccessibility(Landroid/view/View;Ljava/lang/String;ZZ)V

    .line 537
    :cond_0
    return-void
.end method
