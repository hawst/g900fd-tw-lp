.class public Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;
.super Ljava/lang/Object;
.source "ProfileUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;
    }
.end annotation


# static fields
.field private static final SEND_DISCLOSEYN_MGR:I = 0x64

.field private static final TAG:Ljava/lang/String;

.field public static inputFilter:Landroid/text/InputFilter;

.field private static mResizedBitmap:Landroid/graphics/Bitmap;

.field private static task:Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 89
    const-class v0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->TAG:Ljava/lang/String;

    .line 90
    sput-object v1, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->mResizedBitmap:Landroid/graphics/Bitmap;

    .line 390
    new-instance v0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$1;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->inputFilter:Landroid/text/InputFilter;

    .line 408
    sput-object v1, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->task:Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 418
    return-void
.end method

.method public static BitmapResizePrc(Landroid/content/Context;Landroid/graphics/Bitmap;I)Landroid/graphics/drawable/BitmapDrawable;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "Src"    # Landroid/graphics/Bitmap;
    .param p2, "newHeight"    # I

    .prologue
    const/4 v6, 0x1

    .line 194
    if-nez p1, :cond_0

    .line 195
    const/4 v0, 0x0

    .line 213
    :goto_0
    return-object v0

    .line 196
    :cond_0
    const/high16 v4, 0x43b40000    # 360.0f

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    invoke-static {v6, v4, v5}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v4

    float-to-int v1, v4

    .line 197
    .local v1, "defaultValue":I
    const/4 v0, 0x0

    .line 199
    .local v0, "Result":Landroid/graphics/drawable/BitmapDrawable;
    invoke-static {p1, v1, v1, v6}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object p1

    .line 201
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 202
    .local v3, "width":I
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    .line 203
    .local v2, "height":I
    sget-object v4, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ex resize img height : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " img width : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 204
    const/4 v4, 0x0

    sub-int v5, v2, p2

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    invoke-static {p1, v4, v5, v3, p2}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v4

    sput-object v4, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->mResizedBitmap:Landroid/graphics/Bitmap;

    .line 207
    sget-object v4, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->mResizedBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 208
    sget-object v4, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->mResizedBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    .line 209
    sget-object v4, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "resize img height : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " img width : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 211
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    .end local v0    # "Result":Landroid/graphics/drawable/BitmapDrawable;
    sget-object v4, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->mResizedBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v4}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    .line 213
    .restart local v0    # "Result":Landroid/graphics/drawable/BitmapDrawable;
    goto/16 :goto_0
.end method

.method public static InitexerciseGoalData(Landroid/content/Context;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 158
    invoke-static {}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->netCalorieValue()I

    move-result v3

    int-to-double v3, v3

    invoke-static {}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->getBMR()D

    move-result-wide v5

    sub-double/2addr v3, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->floor(D)D

    move-result-wide v3

    double-to-int v1, v3

    .line 161
    .local v1, "unifiedConsumedCalorieGoal":I
    const/4 v0, 0x0

    .line 163
    .local v0, "DEFAULT_PERIOD":I
    new-instance v2, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    int-to-float v3, v1

    const v4, 0x9c4b

    const/4 v5, -0x1

    invoke-direct {v2, v3, v4, v5, v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;-><init>(FIII)V

    .line 167
    .local v2, "unifiedConsumedCalorieGoaldata":Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;
    invoke-static {p0, v2}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->insertGoal(Landroid/content/Context;Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;)V

    .line 168
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    sget-object v0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method public static convertDecimalFormat(F)Ljava/lang/String;
    .locals 4
    .param p0, "number"    # F

    .prologue
    .line 103
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v2, "#.#"

    invoke-direct {v0, v2}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    .line 104
    .local v0, "decimalFormat":Ljava/text/DecimalFormat;
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/text/DecimalFormat;->setDecimalSeparatorAlwaysShown(Z)V

    .line 105
    new-instance v2, Ljava/text/DecimalFormatSymbols;

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v2, v3}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    invoke-virtual {v0, v2}, Ljava/text/DecimalFormat;->setDecimalFormatSymbols(Ljava/text/DecimalFormatSymbols;)V

    .line 106
    float-to-double v2, p0

    invoke-virtual {v0, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    .line 107
    .local v1, "value":Ljava/lang/String;
    return-object v1
.end method

.method public static getBMR()D
    .locals 2

    .prologue
    .line 111
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/NetCalorieUtils;->getBMR()D

    move-result-wide v0

    return-wide v0
.end method

.method public static getImageFile(Landroid/net/Uri;Landroid/content/ContentResolver;)Ljava/io/File;
    .locals 10
    .param p0, "uri"    # Landroid/net/Uri;
    .param p1, "resolver"    # Landroid/content/ContentResolver;

    .prologue
    const/4 v9, 0x1

    const/4 v3, 0x0

    .line 282
    new-array v2, v9, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_data"

    aput-object v1, v2, v0

    .line 283
    .local v2, "projection":[Ljava/lang/String;
    if-nez p0, :cond_0

    .line 284
    sget-object p0, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 287
    :cond_0
    const-string v5, "date_modified desc"

    move-object v0, p1

    move-object v1, p0

    move-object v4, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 288
    .local v7, "mCursor":Landroid/database/Cursor;
    if-nez v7, :cond_1

    .line 289
    new-instance v3, Ljava/io/File;

    invoke-virtual {p0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 309
    :goto_0
    return-object v3

    .line 290
    :cond_1
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-ge v0, v9, :cond_2

    .line 291
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 292
    const/4 v7, 0x0

    .line 293
    goto :goto_0

    .line 295
    :cond_2
    const-string v0, "_data"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    .line 296
    .local v6, "column_index":I
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 298
    invoke-interface {v7, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 300
    .local v8, "path":Ljava/lang/String;
    if-eqz v7, :cond_3

    .line 301
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 302
    const/4 v7, 0x0

    .line 305
    :cond_3
    invoke-static {v8}, Lcom/sec/android/app/shealth/common/utils/Utils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 306
    sget-object v0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->TAG:Ljava/lang/String;

    const-string v1, "File path is empty"

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 307
    new-instance v3, Ljava/io/File;

    invoke-virtual {p0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 309
    :cond_4
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static getPercentageWidth(FFI)F
    .locals 7
    .param p0, "value"    # F
    .param p1, "maxValue"    # F
    .param p2, "maxWidth"    # I

    .prologue
    .line 341
    new-instance v0, Ljava/math/BigDecimal;

    float-to-double v5, p0

    invoke-direct {v0, v5, v6}, Ljava/math/BigDecimal;-><init>(D)V

    .line 342
    .local v0, "bDecimalData":Ljava/math/BigDecimal;
    new-instance v1, Ljava/math/BigDecimal;

    float-to-double v5, p1

    invoke-direct {v1, v5, v6}, Ljava/math/BigDecimal;-><init>(D)V

    .line 343
    .local v1, "bDecimalMaxData":Ljava/math/BigDecimal;
    new-instance v2, Ljava/math/BigDecimal;

    invoke-direct {v2, p2}, Ljava/math/BigDecimal;-><init>(I)V

    .line 345
    .local v2, "bDecimalWidth":Ljava/math/BigDecimal;
    const/4 v5, 0x2

    sget-object v6, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {v0, v1, v5, v6}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v3

    .line 347
    .local v3, "valueDivideResult":Ljava/math/BigDecimal;
    invoke-virtual {v2, v3}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v4

    .line 349
    .local v4, "widthResult":Ljava/math/BigDecimal;
    invoke-virtual {v4}, Ljava/math/BigDecimal;->floatValue()F

    move-result v5

    return v5
.end method

.method public static getProfileBitmap(Landroid/content/Context;)Landroid/graphics/Bitmap;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 354
    sget-object v7, Lcom/sec/android/app/shealth/common/utils/constants/FilePathsConstants;->USER_PHOTO_FILE_FULL_PATH:Ljava/lang/String;

    if-nez v7, :cond_1

    .line 387
    :cond_0
    :goto_0
    return-object v1

    .line 359
    :cond_1
    new-instance v4, Ljava/io/File;

    sget-object v7, Lcom/sec/android/app/shealth/common/utils/constants/FilePathsConstants;->USER_PHOTO_FILE_FULL_PATH:Ljava/lang/String;

    invoke-direct {v4, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 360
    .local v4, "imgFile":Ljava/io/File;
    const/4 v1, 0x0

    .line 361
    .local v1, "bitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_0

    if-eqz p0, :cond_0

    .line 363
    invoke-static {v4}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v6

    .line 367
    .local v6, "uri":Landroid/net/Uri;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string/jumbo v8, "r"

    invoke-virtual {v7, v6, v8}, Landroid/content/ContentResolver;->openAssetFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v0

    .line 368
    .local v0, "afd":Landroid/content/res/AssetFileDescriptor;
    const/4 v3, 0x0

    .line 369
    .local v3, "fd":Ljava/io/FileDescriptor;
    if-eqz v0, :cond_0

    .line 371
    invoke-virtual {v0}, Landroid/content/res/AssetFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v3

    .line 372
    new-instance v5, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v5}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 373
    .local v5, "opt":Landroid/graphics/BitmapFactory$Options;
    const/4 v7, 0x1

    iput v7, v5, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 374
    if-eqz v3, :cond_2

    .line 375
    const/4 v7, 0x0

    invoke-static {v3, v7, v5}, Landroid/graphics/BitmapFactory;->decodeFileDescriptor(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 376
    :cond_2
    invoke-virtual {v0}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 379
    .end local v0    # "afd":Landroid/content/res/AssetFileDescriptor;
    .end local v3    # "fd":Ljava/io/FileDescriptor;
    .end local v5    # "opt":Landroid/graphics/BitmapFactory$Options;
    :catch_0
    move-exception v2

    .line 380
    .local v2, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v2}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 381
    .end local v2    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v2

    .line 382
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method private static insertGoal(Landroid/content/Context;Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "data"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    .prologue
    .line 171
    new-instance v0, Lcom/sec/android/app/shealth/common/commondao/goaldao/GoalDaoImplDb;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/common/commondao/goaldao/GoalDaoImplDb;-><init>(Landroid/content/Context;)V

    .line 172
    .local v0, "goalDao":Lcom/sec/android/app/shealth/common/commondao/goaldao/GoalDao;
    invoke-interface {v0, p1}, Lcom/sec/android/app/shealth/common/commondao/goaldao/GoalDao;->insertData(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;)J

    .line 173
    return-void
.end method

.method public static netCalorie()Ljava/lang/String;
    .locals 1

    .prologue
    .line 119
    invoke-static {}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->netCalorieValue()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static netCalorieValue()I
    .locals 1

    .prologue
    .line 115
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/NetCalorieUtils;->getNetCalorie()I

    move-result v0

    return v0
.end method

.method public static recycleBitmap()V
    .locals 1

    .prologue
    .line 218
    sget-object v0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->mResizedBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->mResizedBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 219
    sget-object v0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->mResizedBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 220
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->mResizedBitmap:Landroid/graphics/Bitmap;

    .line 223
    :cond_0
    return-void
.end method

.method public static saveUnitforUnitHelper(II)V
    .locals 2
    .param p0, "heightUnit"    # I
    .param p1, "weightUnit"    # I

    .prologue
    .line 176
    if-ltz p0, :cond_0

    if-gez p1, :cond_1

    .line 190
    :cond_0
    :goto_0
    return-void

    .line 179
    :cond_1
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;-><init>(Landroid/content/Context;)V

    .line 180
    .local v0, "unitHelper":Lcom/sec/android/app/shealth/common/utils/UnitHelper;
    const v1, 0x249f1

    if-ne p0, v1, :cond_2

    .line 181
    const-string v1, "cm"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->putHeightunit(Ljava/lang/String;)V

    .line 185
    :goto_1
    const v1, 0x1fbd1

    if-ne p1, v1, :cond_3

    .line 186
    const-string v1, "kg"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->putweightUnit(Ljava/lang/String;)V

    goto :goto_0

    .line 183
    :cond_2
    const-string v1, "ft, inch"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->putHeightunit(Ljava/lang/String;)V

    goto :goto_1

    .line 188
    :cond_3
    const-string v1, "lb"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->putweightUnit(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static setBmiValueLocate(FLjava/lang/String;Landroid/widget/TextView;Landroid/content/res/Resources;)Landroid/widget/TextView;
    .locals 10
    .param p0, "x"    # F
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "textView"    # Landroid/widget/TextView;
    .param p3, "resources"    # Landroid/content/res/Resources;

    .prologue
    const/16 v9, 0x11

    const/high16 v8, 0x40000000    # 2.0f

    .line 316
    invoke-static {p1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    .line 317
    .local v2, "val":F
    const/4 v1, 0x0

    .line 318
    .local v1, "pos":F
    const v4, 0x7f0a0599

    invoke-virtual {p3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 320
    .local v3, "width":I
    float-to-double v4, v2

    const-wide v6, 0x40420ccccccccccdL    # 36.1

    cmpl-double v4, v4, v6

    if-lez v4, :cond_1

    const/high16 v4, 0x42140000    # 37.0f

    cmpl-float v4, v2, v4

    if-eqz v4, :cond_1

    .line 321
    const v4, 0x7f0a0598

    invoke-virtual {p3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 322
    .local v0, "maxWidth":I
    sub-int v4, v0, v3

    int-to-float v1, v4

    .line 323
    const/4 v4, 0x5

    invoke-virtual {p2, v4}, Landroid/widget/TextView;->setGravity(I)V

    .line 334
    .end local v0    # "maxWidth":I
    :cond_0
    :goto_0
    invoke-virtual {p2, v1}, Landroid/widget/TextView;->setX(F)V

    .line 336
    return-object p2

    .line 324
    :cond_1
    float-to-double v4, v2

    const-wide v6, 0x4010666666666666L    # 4.1

    cmpg-double v4, v4, v6

    if-gez v4, :cond_3

    .line 325
    const/4 v4, 0x3

    invoke-virtual {p2, v4}, Landroid/widget/TextView;->setGravity(I)V

    .line 326
    const/high16 v4, 0x3f800000    # 1.0f

    cmpl-float v4, v2, v4

    if-eqz v4, :cond_2

    cmpl-float v4, v2, v8

    if-eqz v4, :cond_2

    const/high16 v4, 0x40400000    # 3.0f

    cmpl-float v4, v2, v4

    if-eqz v4, :cond_2

    const/high16 v4, 0x40800000    # 4.0f

    cmpl-float v4, v2, v4

    if-nez v4, :cond_0

    .line 327
    :cond_2
    int-to-float v4, v3

    div-float/2addr v4, v8

    sub-float v1, p0, v4

    .line 328
    invoke-virtual {p2, v9}, Landroid/widget/TextView;->setGravity(I)V

    goto :goto_0

    .line 331
    :cond_3
    int-to-float v4, v3

    div-float/2addr v4, v8

    sub-float v1, p0, v4

    .line 332
    invoke-virtual {p2, v9}, Landroid/widget/TextView;->setGravity(I)V

    goto :goto_0
.end method

.method public static setCalorieGoal(Landroid/content/Context;)V
    .locals 15
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const v14, 0x9c4b

    const v13, 0x9c43

    const/4 v12, -0x1

    const/4 v11, -0x4

    .line 125
    const/4 v0, 0x0

    .line 127
    .local v0, "DEFAULT_PERIOD":I
    invoke-static {}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->netCalorieValue()I

    move-result v4

    .line 128
    .local v4, "mealGoal":I
    invoke-static {}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->netCalorieValue()I

    move-result v7

    int-to-double v7, v7

    invoke-static {}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->getBMR()D

    move-result-wide v9

    sub-double/2addr v7, v9

    invoke-static {v7, v8}, Ljava/lang/Math;->floor(D)D

    move-result-wide v7

    double-to-int v3, v7

    .line 130
    .local v3, "exerciseGoal":I
    const/4 v1, 0x0

    .line 131
    .local v1, "before_MealGoalDataValue":F
    const/4 v2, 0x0

    .line 133
    .local v2, "before_UnifiedComsumedGoalDataValue":F
    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-static {p0, v7}, Lcom/sec/android/app/shealth/common/utils/hcp/GoalUtils;->getLatestGoalData(Landroid/content/Context;Ljava/lang/Integer;)Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    move-result-object v5

    .line 134
    .local v5, "mealGoalData":Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;
    if-eqz v5, :cond_0

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->getGoalSubType()I

    move-result v7

    if-eq v7, v11, :cond_4

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->getGoalSubType()I

    move-result v7

    if-eqz v7, :cond_4

    .line 135
    :cond_0
    new-instance v5, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    .end local v5    # "mealGoalData":Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;
    int-to-float v7, v4

    invoke-direct {v5, v7, v13, v12, v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;-><init>(FIII)V

    .line 142
    .restart local v5    # "mealGoalData":Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;
    :goto_0
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-static {p0, v7}, Lcom/sec/android/app/shealth/common/utils/hcp/GoalUtils;->getLatestGoalData(Landroid/content/Context;Ljava/lang/Integer;)Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    move-result-object v6

    .line 143
    .local v6, "unifiedConsumedCalorieGoaldata":Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;
    if-eqz v6, :cond_1

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->getGoalSubType()I

    move-result v7

    if-eq v7, v11, :cond_5

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->getGoalSubType()I

    move-result v7

    if-eqz v7, :cond_5

    .line 144
    :cond_1
    new-instance v6, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    .end local v6    # "unifiedConsumedCalorieGoaldata":Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;
    int-to-float v7, v3

    invoke-direct {v6, v7, v14, v12, v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;-><init>(FIII)V

    .line 151
    .restart local v6    # "unifiedConsumedCalorieGoaldata":Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;
    :goto_1
    invoke-virtual {v5}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->getValue()F

    move-result v7

    invoke-static {v7, v1}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->isEqual(FF)Z

    move-result v7

    if-nez v7, :cond_2

    .line 152
    invoke-static {p0, v5}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->insertGoal(Landroid/content/Context;Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;)V

    .line 153
    :cond_2
    invoke-virtual {v6}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->getValue()F

    move-result v7

    invoke-static {v7, v2}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->isEqual(FF)Z

    move-result v7

    if-nez v7, :cond_3

    .line 154
    invoke-static {p0, v6}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->insertGoal(Landroid/content/Context;Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;)V

    .line 155
    :cond_3
    return-void

    .line 139
    .end local v6    # "unifiedConsumedCalorieGoaldata":Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;
    :cond_4
    invoke-virtual {v5}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->getValue()F

    move-result v1

    goto :goto_0

    .line 148
    .restart local v6    # "unifiedConsumedCalorieGoaldata":Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;
    :cond_5
    invoke-virtual {v6}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->getValue()F

    move-result v2

    goto :goto_1
.end method

.method public static setProfileDiscloseYN(Landroid/content/Context;Ljava/lang/Boolean;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "profileDiscloseYN"    # Ljava/lang/Boolean;

    .prologue
    .line 410
    invoke-static {p0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isSendProfileDisclose(Landroid/content/Context;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 416
    :goto_0
    return-void

    .line 413
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->task:Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;

    if-nez v0, :cond_1

    .line 414
    new-instance v0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;-><init>(Landroid/content/Context;Ljava/lang/Boolean;)V

    sput-object v0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->task:Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;

    .line 415
    :cond_1
    sget-object v0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->task:Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;->getUserToken()V

    goto :goto_0
.end method

.method public static setUpdateCodeVersion(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 274
    new-instance v0, Lcom/sec/android/app/shealth/CheckVersionCode;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/CheckVersionCode;-><init>(Landroid/content/Context;)V

    .line 275
    .local v0, "checkCode":Lcom/sec/android/app/shealth/CheckVersionCode;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/CheckVersionCode;->readAssertFile()V

    .line 277
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/CheckVersionCode;->getTermsOfUseUpdateCode()F

    move-result v1

    invoke-static {p0, v1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setTermsOfUseVersionCode(Landroid/content/Context;F)V

    .line 278
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/CheckVersionCode;->getPrivacyUpdateCode()F

    move-result v1

    invoke-static {p0, v1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setPrivacyVersionCode(Landroid/content/Context;F)V

    .line 279
    return-void
.end method

.method public static skipProfileSetup(Landroid/content/Context;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x0

    .line 226
    new-instance v1, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    .line 227
    .local v1, "profile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    new-instance v2, Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;-><init>(Landroid/content/Context;)V

    .line 231
    .local v2, "unitHelper":Lcom/sec/android/app/shealth/common/utils/UnitHelper;
    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getHeightUnit()I

    move-result v3

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getWeightUnit()I

    move-result v4

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->saveUnitforUnitHelper(II)V

    .line 233
    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getDistanceUnit()I

    move-result v3

    const v4, 0x29813

    if-ne v3, v4, :cond_1

    .line 234
    const-string/jumbo v3, "mi"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->putDistanceUnit(Ljava/lang/String;)V

    .line 239
    :goto_0
    :try_start_0
    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->save()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 244
    :goto_1
    invoke-static {p0}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->clearMigrationState(Landroid/content/Context;)V

    .line 245
    invoke-static {p0, v5}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setInitializationNeeded(Landroid/content/Context;Z)V

    .line 246
    invoke-static {p0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setLastUpdated(Landroid/content/Context;)V

    .line 247
    invoke-static {p0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setFirstActivity(Landroid/content/Context;)V

    .line 249
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->StartAfterProfileSetUp()V

    .line 250
    const-string v3, "InitSetProfile"

    const-string/jumbo v4, "start_pedometer"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 253
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->isWalkMateCoverVisible()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 255
    invoke-static {v5}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->setWalkMateCoverVisible(Z)V

    .line 258
    :try_start_1
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x15

    if-lt v3, v4, :cond_2

    .line 259
    const-string v3, "Profile Utils"

    const-string v4, "LOS"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 260
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "lock_additional_steps"

    const/4 v5, 0x1

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 270
    :cond_0
    :goto_2
    invoke-static {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->setUpdateCodeVersion(Landroid/content/Context;)V

    .line 271
    return-void

    .line 236
    :cond_1
    const-string v3, "km"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->putDistanceUnit(Ljava/lang/String;)V

    goto :goto_0

    .line 240
    :catch_0
    move-exception v0

    .line 241
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string/jumbo v3, "skipProfileSetup"

    invoke-static {v3, v0}, Landroid/util/Log;->secW(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 262
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :cond_2
    :try_start_2
    const-string v3, "Profile Utils"

    const-string v4, "<LOS"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "walk_mate"

    const/4 v5, 0x1

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_2

    .line 265
    :catch_1
    move-exception v0

    .line 266
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "Profile Utils"

    invoke-static {v3, v0}, Landroid/util/Log;->secW(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2
.end method
