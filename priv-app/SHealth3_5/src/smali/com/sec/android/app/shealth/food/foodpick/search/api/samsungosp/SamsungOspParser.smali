.class public Lcom/sec/android/app/shealth/food/foodpick/search/api/samsungosp/SamsungOspParser;
.super Lcom/sec/android/app/shealth/food/foodpick/search/api/base/JsonFoodParser;
.source "SamsungOspParser.java"


# static fields
.field private static final CALCIUM:Ljava/lang/String; = "calcium"

.field private static final CALORIE:Ljava/lang/String; = "calorie"

.field private static final CARBOHYDRATE:Ljava/lang/String; = "carbohydrate"

.field private static final CATEGORY_ID:Ljava/lang/String; = "categoryId"

.field private static final CATEGORY_LIST:Ljava/lang/String; = "categoryList"

.field private static final CATEGORY_NAME:Ljava/lang/String; = "categoryName"

.field private static final CHOLESTEROL:Ljava/lang/String; = "cholesterol"

.field private static final DESCRIPTION:Ljava/lang/String; = "desc"

.field private static final EYE_MEASURE:Ljava/lang/String; = "eyeMeasure"

.field private static final FOOD_ID:Ljava/lang/String; = "foodId"

.field private static final FOOD_LIST:Ljava/lang/String; = "foodList"

.field private static final FOOD_NAME:Ljava/lang/String; = "foodName"

.field private static final GRAMM_SHORT:Ljava/lang/String; = "g"

.field private static final IRON:Ljava/lang/String; = "iron"

.field private static final LOG_TAG:Ljava/lang/String;

.field private static final METRIC_SERVING_AMOUNT:Ljava/lang/String; = "metricServingAmount"

.field private static final METRIC_SERVING_UNIT:Ljava/lang/String; = "metricServingUnit"

.field private static final MONOUNSATURATED_FAT:Ljava/lang/String; = "monounsaturatedFat"

.field private static final POLYUNSATURATED_FAT:Ljava/lang/String; = "polyunsaturatedFat"

.field private static final POTASSIUM:Ljava/lang/String; = "potassium"

.field private static final PROTEIN:Ljava/lang/String; = "protein"

.field private static final ROOT_CATEGORY_ID:Ljava/lang/String; = "rootCategoryId"

.field private static final ROOT_CATEGORY_NAME:Ljava/lang/String; = "rootCategoryName"

.field private static final SATURATED_FAT:Ljava/lang/String; = "saturatedFat"

.field private static final SERVING_RES_ID_DEFAULT:I = 0x7f09017b

.field private static final SODIUM:Ljava/lang/String; = "sodium"

.field private static final SUB_CATEGORY_ID:Ljava/lang/String; = "subCategoryId"

.field private static final SUB_CATEGORY_NAME:Ljava/lang/String; = "subCategoryName"

.field private static final SUGAR:Ljava/lang/String; = "sugar"

.field private static final TOTAL_FAT:Ljava/lang/String; = "totalFat"

.field private static final TOTAL_ITEM_COUNT:Ljava/lang/String; = "totalItemCount"

.field private static final TOTAL_SEARCH_CNT:Ljava/lang/String; = "totalSearchCount"

.field private static final TRANS_FAT:Ljava/lang/String; = "transFat"

.field private static final UNIT_COUNT_DEFAULT:I = 0x1

.field private static final VITAMIN_A:Ljava/lang/String; = "vitaminA"

.field private static final VITAMIN_C:Ljava/lang/String; = "vitaminC"


# instance fields
.field private mResources:Landroid/content/res/Resources;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const-class v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/samsungosp/SamsungOspParser;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->getLogTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/samsungosp/SamsungOspParser;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 0
    .param p1, "res"    # Landroid/content/res/Resources;

    .prologue
    .line 87
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/JsonFoodParser;-><init>()V

    .line 88
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/samsungosp/SamsungOspParser;->mResources:Landroid/content/res/Resources;

    .line 89
    return-void
.end method

.method private createDescription(Lorg/json/JSONObject;)Ljava/lang/String;
    .locals 6
    .param p1, "foodJSON"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 262
    const-string v3, "calorie"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v3

    double-to-float v0, v3

    .line 263
    .local v0, "calories":F
    const-string/jumbo v3, "metricServingAmount"

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    invoke-virtual {p1, v3, v4, v5}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Math;->round(D)J

    move-result-wide v3

    long-to-int v1, v3

    .line 264
    .local v1, "metricServingAmount":I
    const-string/jumbo v3, "metricServingUnit"

    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/samsungosp/SamsungOspParser;->getDefaultServing()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 265
    .local v2, "metricServingUnit":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/samsungosp/SamsungOspParser;->mResources:Landroid/content/res/Resources;

    invoke-static {v3, v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->createFoodDescription(Landroid/content/res/Resources;FILjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method private getDefaultServing()Ljava/lang/String;
    .locals 2

    .prologue
    .line 258
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/samsungosp/SamsungOspParser;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f09017b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic parseAutoCompleteSearch(Ljava/lang/Object;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/AutoCompleteSearchResult;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 43
    check-cast p1, Lorg/json/JSONObject;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/samsungosp/SamsungOspParser;->parseAutoCompleteSearch(Lorg/json/JSONObject;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/AutoCompleteSearchResult;

    move-result-object v0

    return-object v0
.end method

.method public parseAutoCompleteSearch(Lorg/json/JSONObject;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/AutoCompleteSearchResult;
    .locals 5
    .param p1, "searchResult"    # Lorg/json/JSONObject;

    .prologue
    .line 93
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/samsungosp/SamsungOspParser;->parseFoodSearch(Lorg/json/JSONObject;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult;

    move-result-object v3

    .line 94
    .local v3, "searchFoodResult":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult;, "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult<Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 95
    .local v1, "hints":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {v3}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult;->getSearchResult()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    .line 96
    .local v0, "foodInfo":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 98
    .end local v0    # "foodInfo":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    :cond_0
    new-instance v4, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/AutoCompleteSearchResult;

    invoke-direct {v4, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/AutoCompleteSearchResult;-><init>(Ljava/util/List;)V

    return-object v4
.end method

.method public bridge synthetic parseBarcodeSearch(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 43
    check-cast p1, Lorg/json/JSONObject;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/samsungosp/SamsungOspParser;->parseBarcodeSearch(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public parseBarcodeSearch(Lorg/json/JSONObject;)Ljava/lang/String;
    .locals 1
    .param p1, "searchResult"    # Lorg/json/JSONObject;

    .prologue
    .line 270
    const-string v0, ""

    return-object v0
.end method

.method public bridge synthetic parseCategorySearch(Ljava/lang/Object;)Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryResult;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 43
    check-cast p1, Lorg/json/JSONObject;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/samsungosp/SamsungOspParser;->parseCategorySearch(Lorg/json/JSONObject;)Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryResult;

    move-result-object v0

    return-object v0
.end method

.method public parseCategorySearch(Lorg/json/JSONObject;)Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryResult;
    .locals 9
    .param p1, "searchResult"    # Lorg/json/JSONObject;

    .prologue
    .line 163
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 165
    .local v0, "categories":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategory;>;"
    :try_start_0
    const-string v5, "categoryList"

    invoke-virtual {p1, v5}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    .line 166
    .local v1, "categoryArray":Lorg/json/JSONArray;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v4, v5, :cond_0

    .line 167
    invoke-virtual {v1, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    .line 168
    .local v2, "categoryJSON":Lorg/json/JSONObject;
    new-instance v5, Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategory;

    const-string v6, "categoryId"

    invoke-virtual {v2, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "categoryName"

    invoke-virtual {v2, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "desc"

    invoke-direct {v5, v6, v7, v8}, Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategory;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 166
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 170
    .end local v1    # "categoryArray":Lorg/json/JSONArray;
    .end local v2    # "categoryJSON":Lorg/json/JSONObject;
    .end local v4    # "i":I
    :catch_0
    move-exception v3

    .line 171
    .local v3, "e":Lorg/json/JSONException;
    sget-object v5, Lcom/sec/android/app/shealth/food/foodpick/search/api/samsungosp/SamsungOspParser;->LOG_TAG:Ljava/lang/String;

    invoke-static {v5, v3}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 173
    .end local v3    # "e":Lorg/json/JSONException;
    :cond_0
    new-instance v5, Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryResult;

    invoke-direct {v5, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryResult;-><init>(Ljava/util/List;)V

    return-object v5
.end method

.method public bridge synthetic parseExtraFood(Ljava/lang/Object;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 43
    check-cast p1, Lorg/json/JSONObject;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/samsungosp/SamsungOspParser;->parseExtraFood(Lorg/json/JSONObject;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;

    move-result-object v0

    return-object v0
.end method

.method public parseExtraFood(Lorg/json/JSONObject;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;
    .locals 11
    .param p1, "searchResult"    # Lorg/json/JSONObject;

    .prologue
    .line 194
    new-instance v1, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;

    invoke-direct {v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;-><init>()V

    .line 196
    .local v1, "extraFoodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;
    :try_start_0
    const-string v8, "foodId"

    invoke-virtual {p1, v8}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v8

    int-to-long v8, v8

    invoke-virtual {v1, v8, v9}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setFoodInfoId(J)V

    .line 197
    const-string/jumbo v8, "metricServingUnit"

    invoke-virtual {p1, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setUnitName(Ljava/lang/String;)V

    .line 198
    iget-object v8, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/samsungosp/SamsungOspParser;->mResources:Landroid/content/res/Resources;

    const v9, 0x7f09017b

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setServingDescription(Ljava/lang/String;)V

    .line 199
    const-string/jumbo v8, "metricServingAmount"

    const-wide/high16 v9, -0x4010000000000000L    # -1.0

    invoke-virtual {p1, v8, v9, v10}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v8

    double-to-float v8, v8

    invoke-virtual {v1, v8}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setDefaultNumber(F)V

    .line 201
    const/4 v8, 0x1

    invoke-virtual {v1, v8}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setUnit(I)V

    .line 202
    const-string v8, "eyeMeasure"

    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/samsungosp/SamsungOspParser;->getDefaultServing()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v8, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setServingDescription(Ljava/lang/String;)V

    .line 203
    const-string/jumbo v8, "totalFat"

    const-wide/high16 v9, -0x4010000000000000L    # -1.0

    invoke-virtual {p1, v8, v9, v10}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v8

    double-to-float v8, v8

    invoke-virtual {v1, v8}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setFat(F)V

    .line 205
    const-string/jumbo v8, "saturatedFat"

    const-wide/high16 v9, -0x4010000000000000L    # -1.0

    invoke-virtual {p1, v8, v9, v10}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v8

    double-to-float v8, v8

    invoke-virtual {v1, v8}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setSaturated(F)V

    .line 207
    const-string/jumbo v8, "polyunsaturatedFat"

    const-wide/high16 v9, -0x4010000000000000L    # -1.0

    invoke-virtual {p1, v8, v9, v10}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v8

    double-to-float v8, v8

    invoke-virtual {v1, v8}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setPolysaturated(F)V

    .line 209
    const-string/jumbo v8, "monounsaturatedFat"

    const-wide/high16 v9, -0x4010000000000000L    # -1.0

    invoke-virtual {p1, v8, v9, v10}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v8

    double-to-float v8, v8

    invoke-virtual {v1, v8}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setMonosaturated(F)V

    .line 211
    const-string/jumbo v8, "transFat"

    const-wide/high16 v9, -0x4010000000000000L    # -1.0

    invoke-virtual {p1, v8, v9, v10}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v8

    double-to-float v8, v8

    invoke-virtual {v1, v8}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setTransFat(F)V

    .line 213
    const-string v8, "carbohydrate"

    const-wide/high16 v9, -0x4010000000000000L    # -1.0

    invoke-virtual {p1, v8, v9, v10}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v8

    double-to-float v8, v8

    invoke-virtual {v1, v8}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setCarbohydrate(F)V

    .line 215
    const-string/jumbo v8, "sugar"

    const-wide/high16 v9, -0x4010000000000000L    # -1.0

    invoke-virtual {p1, v8, v9, v10}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v8

    double-to-float v8, v8

    invoke-virtual {v1, v8}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setSugar(F)V

    .line 217
    const-string/jumbo v8, "protein"

    const-wide/high16 v9, -0x4010000000000000L    # -1.0

    invoke-virtual {p1, v8, v9, v10}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v8

    double-to-float v8, v8

    invoke-virtual {v1, v8}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setProtein(F)V

    .line 219
    const-string v8, "cholesterol"

    const-wide/high16 v9, -0x4010000000000000L    # -1.0

    invoke-virtual {p1, v8, v9, v10}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Math;->round(D)J

    move-result-wide v8

    long-to-int v8, v8

    invoke-virtual {v1, v8}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setCholesterol(I)V

    .line 221
    const-string/jumbo v8, "sodium"

    const-wide/high16 v9, -0x4010000000000000L    # -1.0

    invoke-virtual {p1, v8, v9, v10}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Math;->round(D)J

    move-result-wide v8

    long-to-int v8, v8

    invoke-virtual {v1, v8}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setSodium(I)V

    .line 223
    const-string/jumbo v8, "potassium"

    const-wide/high16 v9, -0x4010000000000000L    # -1.0

    invoke-virtual {p1, v8, v9, v10}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Math;->round(D)J

    move-result-wide v8

    long-to-int v8, v8

    invoke-virtual {v1, v8}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setPotassium(I)V

    .line 225
    const-string/jumbo v8, "vitaminA"

    const-wide/high16 v9, -0x4010000000000000L    # -1.0

    invoke-virtual {p1, v8, v9, v10}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Math;->round(D)J

    move-result-wide v8

    long-to-int v8, v8

    invoke-virtual {v1, v8}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setVitaminA(I)V

    .line 227
    const-string/jumbo v8, "vitaminC"

    const-wide/high16 v9, -0x4010000000000000L    # -1.0

    invoke-virtual {p1, v8, v9, v10}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Math;->round(D)J

    move-result-wide v8

    long-to-int v8, v8

    invoke-virtual {v1, v8}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setVitaminC(I)V

    .line 229
    const-string v8, "calcium"

    const-wide/high16 v9, -0x4010000000000000L    # -1.0

    invoke-virtual {p1, v8, v9, v10}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Math;->round(D)J

    move-result-wide v8

    long-to-int v8, v8

    invoke-virtual {v1, v8}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setCalcium(I)V

    .line 231
    const-string v8, "iron"

    const-wide/high16 v9, -0x4010000000000000L    # -1.0

    invoke-virtual {p1, v8, v9, v10}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Math;->round(D)J

    move-result-wide v8

    long-to-int v8, v8

    invoke-virtual {v1, v8}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setIron(I)V

    .line 234
    const-string/jumbo v8, "metricServingUnit"

    invoke-virtual {p1, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v9, "g"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 235
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getDefaultNumber()F

    move-result v8

    const-string v9, "calorie"

    invoke-virtual {p1, v9}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v9

    double-to-float v9, v9

    div-float v7, v8, v9

    .line 240
    .local v7, "grammInKcal":F
    :goto_0
    invoke-virtual {v1, v7}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setGrammInKcal(F)V

    .line 241
    const-string/jumbo v8, "rootCategoryId"

    const-string v9, ""

    invoke-virtual {p1, v8, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 242
    .local v2, "rootCategoryId":Ljava/lang/String;
    const-string/jumbo v8, "rootCategoryName"

    const-string v9, ""

    invoke-virtual {p1, v8, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 243
    .local v3, "rootCategoryName":Ljava/lang/String;
    const-string/jumbo v8, "subCategoryId"

    const-string v9, ""

    invoke-virtual {p1, v8, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 244
    .local v4, "subCategoryId":Ljava/lang/String;
    const-string/jumbo v8, "subCategoryName"

    const-string v9, ""

    invoke-virtual {p1, v8, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 245
    .local v5, "subCategoryName":Ljava/lang/String;
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;-><init>(Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    .local v0, "result":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;
    const-string v8, "calorie"

    invoke-virtual {p1, v8}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 248
    const-string v8, "calorie"

    invoke-virtual {p1, v8}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Math;->round(D)J

    move-result-wide v8

    long-to-int v8, v8

    invoke-virtual {v0, v8}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;->setCalories(I)V

    .line 254
    :cond_0
    return-object v0

    .line 237
    .end local v0    # "result":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;
    .end local v2    # "rootCategoryId":Ljava/lang/String;
    .end local v3    # "rootCategoryName":Ljava/lang/String;
    .end local v4    # "subCategoryId":Ljava/lang/String;
    .end local v5    # "subCategoryName":Ljava/lang/String;
    .end local v7    # "grammInKcal":F
    :cond_1
    const/high16 v7, 0x3f800000    # 1.0f

    .line 238
    .restart local v7    # "grammInKcal":F
    sget-object v8, Lcom/sec/android/app/shealth/food/foodpick/search/api/samsungosp/SamsungOspParser;->LOG_TAG:Ljava/lang/String;

    const-string v9, "Unknown unit name"

    invoke-static {v8, v9}, Lcom/sec/android/app/shealth/common/utils/Logging;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 250
    .end local v7    # "grammInKcal":F
    :catch_0
    move-exception v6

    .line 251
    .local v6, "e":Lorg/json/JSONException;
    sget-object v8, Lcom/sec/android/app/shealth/food/foodpick/search/api/samsungosp/SamsungOspParser;->LOG_TAG:Ljava/lang/String;

    invoke-static {v8, v6}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 252
    new-instance v8, Lorg/apache/http/ParseException;

    invoke-virtual {v6}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Lorg/apache/http/ParseException;-><init>(Ljava/lang/String;)V

    throw v8
.end method

.method public bridge synthetic parseFoodByCategorySearch(Ljava/lang/Object;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 43
    check-cast p1, Lorg/json/JSONObject;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/samsungosp/SamsungOspParser;->parseFoodByCategorySearch(Lorg/json/JSONObject;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult;

    move-result-object v0

    return-object v0
.end method

.method public parseFoodByCategorySearch(Lorg/json/JSONObject;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult;
    .locals 11
    .param p1, "searchResult"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONObject;",
            ")",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 133
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 136
    .local v5, "foods":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;>;"
    :try_start_0
    const-string/jumbo v8, "totalItemCount"

    invoke-virtual {p1, v8}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v7

    .line 137
    .local v7, "totalSearchCount":I
    const-string v8, "foodList"

    invoke-virtual {p1, v8}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 138
    .local v2, "foodArray":Lorg/json/JSONArray;
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v8

    if-ge v6, v8, :cond_1

    .line 139
    invoke-virtual {v2, v6}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 140
    .local v4, "foodJSON":Lorg/json/JSONObject;
    invoke-virtual {v2, v6}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v8

    const-string v9, "eyeMeasure"

    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 141
    .local v1, "eyeMeasure":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_0

    .line 142
    new-instance v3, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    invoke-direct {v3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;-><init>()V

    .line 143
    .local v3, "foodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    const-string v8, "calorie"

    invoke-virtual {v4, v8}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v8

    double-to-float v8, v8

    invoke-virtual {v3, v8}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setkCal(F)V

    .line 144
    const-string v8, "foodName"

    invoke-virtual {v4, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setName(Ljava/lang/String;)V

    .line 145
    const-string v8, "foodId"

    invoke-virtual {v4, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setServerId(Ljava/lang/String;)V

    .line 146
    const v8, 0x46cd3

    invoke-virtual {v3, v8}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setServerSourceType(I)V

    .line 147
    invoke-direct {p0, v4}, Lcom/sec/android/app/shealth/food/foodpick/search/api/samsungosp/SamsungOspParser;->createDescription(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setDescription(Ljava/lang/String;)V

    .line 148
    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 138
    .end local v3    # "foodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    :goto_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 150
    :cond_0
    sget-object v8, Lcom/sec/android/app/shealth/food/foodpick/search/api/samsungosp/SamsungOspParser;->LOG_TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "foodId"

    invoke-virtual {v4, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ": "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "foodName"

    invoke-virtual {v4, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "has no "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "eyeMeasure"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/app/shealth/common/utils/Logging;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 154
    .end local v1    # "eyeMeasure":Ljava/lang/String;
    .end local v2    # "foodArray":Lorg/json/JSONArray;
    .end local v4    # "foodJSON":Lorg/json/JSONObject;
    .end local v6    # "i":I
    .end local v7    # "totalSearchCount":I
    :catch_0
    move-exception v0

    .line 155
    .local v0, "ex":Lorg/json/JSONException;
    sget-object v8, Lcom/sec/android/app/shealth/food/foodpick/search/api/samsungosp/SamsungOspParser;->LOG_TAG:Ljava/lang/String;

    invoke-static {v8, v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 156
    new-instance v8, Lorg/apache/http/ParseException;

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Lorg/apache/http/ParseException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 158
    .end local v0    # "ex":Lorg/json/JSONException;
    .restart local v2    # "foodArray":Lorg/json/JSONArray;
    .restart local v6    # "i":I
    .restart local v7    # "totalSearchCount":I
    :cond_1
    new-instance v8, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult;

    invoke-direct {v8, v5, v7}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult;-><init>(Ljava/util/List;I)V

    return-object v8
.end method

.method public bridge synthetic parseFoodSearch(Ljava/lang/Object;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 43
    check-cast p1, Lorg/json/JSONObject;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/samsungosp/SamsungOspParser;->parseFoodSearch(Lorg/json/JSONObject;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult;

    move-result-object v0

    return-object v0
.end method

.method public parseFoodSearch(Lorg/json/JSONObject;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult;
    .locals 11
    .param p1, "searchResult"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONObject;",
            ")",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 103
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 106
    .local v5, "foods":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;>;"
    :try_start_0
    const-string/jumbo v8, "totalSearchCount"

    invoke-virtual {p1, v8}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v7

    .line 107
    .local v7, "totalSearchCount":I
    const-string v8, "foodList"

    invoke-virtual {p1, v8}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 108
    .local v2, "foodArray":Lorg/json/JSONArray;
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v8

    if-ge v6, v8, :cond_1

    .line 109
    invoke-virtual {v2, v6}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 110
    .local v4, "foodJSON":Lorg/json/JSONObject;
    invoke-virtual {v2, v6}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v8

    const-string v9, "eyeMeasure"

    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 111
    .local v1, "eyeMeasure":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_0

    .line 112
    new-instance v3, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    invoke-direct {v3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;-><init>()V

    .line 113
    .local v3, "foodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    const-string v8, "calorie"

    invoke-virtual {v4, v8}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v8

    double-to-float v8, v8

    invoke-virtual {v3, v8}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setkCal(F)V

    .line 114
    const-string v8, "foodName"

    invoke-virtual {v4, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setName(Ljava/lang/String;)V

    .line 115
    const-string v8, "foodId"

    invoke-virtual {v4, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setServerId(Ljava/lang/String;)V

    .line 116
    invoke-direct {p0, v4}, Lcom/sec/android/app/shealth/food/foodpick/search/api/samsungosp/SamsungOspParser;->createDescription(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setDescription(Ljava/lang/String;)V

    .line 117
    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/food/foodpick/search/api/samsungosp/SamsungOspParser;->notifyFoodParsingComplete(Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;)V

    .line 118
    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 108
    .end local v3    # "foodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    :goto_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 120
    :cond_0
    sget-object v8, Lcom/sec/android/app/shealth/food/foodpick/search/api/samsungosp/SamsungOspParser;->LOG_TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "foodId"

    invoke-virtual {v4, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ": "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "foodName"

    invoke-virtual {v4, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "has no "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "eyeMeasure"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/app/shealth/common/utils/Logging;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 124
    .end local v1    # "eyeMeasure":Ljava/lang/String;
    .end local v2    # "foodArray":Lorg/json/JSONArray;
    .end local v4    # "foodJSON":Lorg/json/JSONObject;
    .end local v6    # "i":I
    .end local v7    # "totalSearchCount":I
    :catch_0
    move-exception v0

    .line 125
    .local v0, "ex":Lorg/json/JSONException;
    sget-object v8, Lcom/sec/android/app/shealth/food/foodpick/search/api/samsungosp/SamsungOspParser;->LOG_TAG:Ljava/lang/String;

    invoke-static {v8, v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 126
    new-instance v8, Lorg/apache/http/ParseException;

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Lorg/apache/http/ParseException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 128
    .end local v0    # "ex":Lorg/json/JSONException;
    .restart local v2    # "foodArray":Lorg/json/JSONArray;
    .restart local v6    # "i":I
    .restart local v7    # "totalSearchCount":I
    :cond_1
    new-instance v8, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult;

    invoke-direct {v8, v5, v7}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult;-><init>(Ljava/util/List;I)V

    return-object v8
.end method

.method public bridge synthetic parseSubCategorySearch(Ljava/lang/Object;)Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategoryResult;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 43
    check-cast p1, Lorg/json/JSONObject;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/samsungosp/SamsungOspParser;->parseSubCategorySearch(Lorg/json/JSONObject;)Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategoryResult;

    move-result-object v0

    return-object v0
.end method

.method public parseSubCategorySearch(Lorg/json/JSONObject;)Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategoryResult;
    .locals 8
    .param p1, "searchResult"    # Lorg/json/JSONObject;

    .prologue
    .line 178
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 180
    .local v0, "categories":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategory;>;"
    :try_start_0
    const-string v5, "categoryList"

    invoke-virtual {p1, v5}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    .line 181
    .local v1, "categoryArray":Lorg/json/JSONArray;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v4, v5, :cond_0

    .line 182
    invoke-virtual {v1, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    .line 183
    .local v2, "categoryJSON":Lorg/json/JSONObject;
    new-instance v5, Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategory;

    const-string v6, "categoryId"

    invoke-virtual {v2, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "categoryName"

    invoke-virtual {v2, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategory;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 181
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 185
    .end local v1    # "categoryArray":Lorg/json/JSONArray;
    .end local v2    # "categoryJSON":Lorg/json/JSONObject;
    .end local v4    # "i":I
    :catch_0
    move-exception v3

    .line 186
    .local v3, "e":Lorg/json/JSONException;
    sget-object v5, Lcom/sec/android/app/shealth/food/foodpick/search/api/samsungosp/SamsungOspParser;->LOG_TAG:Ljava/lang/String;

    invoke-static {v5, v3}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 188
    .end local v3    # "e":Lorg/json/JSONException;
    :cond_0
    new-instance v5, Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategoryResult;

    invoke-direct {v5, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategoryResult;-><init>(Ljava/util/List;)V

    return-object v5
.end method
