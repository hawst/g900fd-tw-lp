.class public Lcom/sec/android/app/shealth/common/commonui/ListPopup;
.super Landroid/widget/PopupWindow;
.source "ListPopup.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/common/commonui/ListPopup$1;,
        Lcom/sec/android/app/shealth/common/commonui/ListPopup$InternalOnItemClickedListener;,
        Lcom/sec/android/app/shealth/common/commonui/ListPopup$PopupListAdapter;,
        Lcom/sec/android/app/shealth/common/commonui/ListPopup$OnItemClickedListener;
    }
.end annotation


# static fields
.field private static final ADDITIONAL_WIDTH:I = 0x14

.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private caller:Landroid/view/View;

.field protected context:Landroid/content/Context;

.field private listener:Landroid/view/View$OnClickListener;

.field private mItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mListView:Landroid/widget/ListView;

.field private mPopupListAdapter:Lcom/sec/android/app/shealth/common/commonui/ListPopup$PopupListAdapter;

.field private mTextSelection:Ljava/lang/String;

.field private mTextSelectionColorId:I

.field private onItemClickedListener:Lcom/sec/android/app/shealth/common/commonui/ListPopup$OnItemClickedListener;

.field private selectedItemIndex:I

.field private width:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const-class v0, Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->getLogTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/List;Landroid/view/View;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p3, "caller"    # Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .prologue
    .line 72
    .local p2, "items":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Landroid/widget/PopupWindow;-><init>()V

    .line 51
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->selectedItemIndex:I

    .line 73
    iput-object p3, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->caller:Landroid/view/View;

    .line 74
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->mItems:Ljava/util/List;

    .line 75
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->context:Landroid/content/Context;

    .line 76
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/ListPopup$InternalOnItemClickedListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/common/commonui/ListPopup$InternalOnItemClickedListener;-><init>(Lcom/sec/android/app/shealth/common/commonui/ListPopup;Lcom/sec/android/app/shealth/common/commonui/ListPopup$1;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->listener:Landroid/view/View$OnClickListener;

    .line 77
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->init()V

    .line 78
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;[Ljava/lang/String;Landroid/view/View;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "items"    # [Ljava/lang/String;
    .param p3, "caller"    # Landroid/view/View;

    .prologue
    .line 63
    invoke-static {p2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, p1, v0, p3}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;-><init>(Landroid/content/Context;Ljava/util/List;Landroid/view/View;)V

    .line 64
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/common/commonui/ListPopup;)Landroid/view/View$OnClickListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->listener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/common/commonui/ListPopup;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    .prologue
    .line 41
    iget v0, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->selectedItemIndex:I

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/common/commonui/ListPopup;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->mTextSelection:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/common/commonui/ListPopup;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    .prologue
    .line 41
    iget v0, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->mTextSelectionColorId:I

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/common/commonui/ListPopup;)Lcom/sec/android/app/shealth/common/commonui/ListPopup$OnItemClickedListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->onItemClickedListener:Lcom/sec/android/app/shealth/common/commonui/ListPopup$OnItemClickedListener;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/common/commonui/ListPopup;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->mItems:Ljava/util/List;

    return-object v0
.end method

.method private init()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 176
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->getPopupListAdapter()Lcom/sec/android/app/shealth/common/commonui/ListPopup$PopupListAdapter;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->mPopupListAdapter:Lcom/sec/android/app/shealth/common/commonui/ListPopup$PopupListAdapter;

    .line 177
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->context:Landroid/content/Context;

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 178
    .local v0, "inflater":Landroid/view/LayoutInflater;
    new-instance v2, Landroid/widget/ListView;

    iget-object v3, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->context:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->mListView:Landroid/widget/ListView;

    .line 179
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->mListView:Landroid/widget/ListView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->getBackgroundResource()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setBackgroundResource(I)V

    .line 183
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->mListView:Landroid/widget/ListView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->getDividerHeight()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setDividerHeight(I)V

    .line 184
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->mListView:Landroid/widget/ListView;

    invoke-virtual {v2, v5}, Landroid/widget/ListView;->setClickable(Z)V

    .line 185
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->mListView:Landroid/widget/ListView;

    iget-object v3, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->mPopupListAdapter:Lcom/sec/android/app/shealth/common/commonui/ListPopup$PopupListAdapter;

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 186
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->mListView:Landroid/widget/ListView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setFocusable(Z)V

    .line 187
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->mListView:Landroid/widget/ListView;

    invoke-virtual {v2, v5}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 204
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->caller:Landroid/view/View;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->caller:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    if-lez v2, :cond_1

    .line 205
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->caller:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->width:I

    .line 206
    iget v2, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->width:I

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->setWidth(I)V

    .line 211
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->getListPopupItemHeightWithDivider()I

    move-result v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->getDividerHeight()I

    move-result v3

    sub-int/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->mItems:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    mul-int/2addr v2, v3

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->getDividerHeight()I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->mItems:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    mul-int/2addr v3, v4

    add-int v1, v2, v3

    .line 212
    .local v1, "measuredHeight":I
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->isHeightSetToWrapContent()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v1, -0x2

    .end local v1    # "measuredHeight":I
    :cond_0
    :goto_1
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->setHeight(I)V

    .line 214
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->mListView:Landroid/widget/ListView;

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->setContentView(Landroid/view/View;)V

    .line 215
    new-instance v3, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const/4 v2, 0x0

    check-cast v2, Landroid/graphics/Bitmap;

    invoke-direct {v3, v4, v2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 216
    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->setFocusable(Z)V

    .line 217
    return-void

    .line 208
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->setWidth(I)V

    goto :goto_0

    .line 212
    .restart local v1    # "measuredHeight":I
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->getListPopupHeight()I

    move-result v2

    if-lt v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->getListPopupHeight()I

    move-result v1

    goto :goto_1
.end method


# virtual methods
.method protected getBackgroundResource()I
    .locals 1

    .prologue
    .line 129
    sget v0, Lcom/sec/android/app/shealth/common/commonui/R$drawable;->tw_menu_dropdown_panel_holo_light:I

    return v0
.end method

.method protected getDefaultItemColorId()I
    .locals 1

    .prologue
    .line 122
    sget v0, Lcom/sec/android/app/shealth/common/commonui/R$color;->list_popup_item_default_text_color:I

    return v0
.end method

.method protected getDividerDrawable()Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/sec/android/app/shealth/common/commonui/R$drawable;->spinner_divider:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method protected getDividerHeight()I
    .locals 2

    .prologue
    .line 150
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/sec/android/app/shealth/common/commonui/R$dimen;->list_popup_divider_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method protected getLayoutId()I
    .locals 1

    .prologue
    .line 136
    sget v0, Lcom/sec/android/app/shealth/common/commonui/R$layout;->dropdown_list_popup_item:I

    return v0
.end method

.method protected getListPopupHeight()I
    .locals 2

    .prologue
    .line 108
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/sec/android/app/shealth/common/commonui/R$dimen;->list_popup_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method protected getListPopupItemHeightWithDivider()I
    .locals 2

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/sec/android/app/shealth/common/commonui/R$dimen;->list_popup_item_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method protected getPopupListAdapter()Lcom/sec/android/app/shealth/common/commonui/ListPopup$PopupListAdapter;
    .locals 3

    .prologue
    .line 228
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/ListPopup$PopupListAdapter;

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->context:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->mItems:Ljava/util/List;

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/ListPopup$PopupListAdapter;-><init>(Lcom/sec/android/app/shealth/common/commonui/ListPopup;Landroid/content/Context;Ljava/util/List;)V

    return-object v0
.end method

.method protected getSelectedItemColorId()I
    .locals 1

    .prologue
    .line 115
    sget v0, Lcom/sec/android/app/shealth/common/commonui/R$color;->list_popup_item_selected_text_color:I

    return v0
.end method

.method public getSelectedItemIndex()I
    .locals 1

    .prologue
    .line 327
    iget v0, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->selectedItemIndex:I

    return v0
.end method

.method protected getTextViewId()I
    .locals 1

    .prologue
    .line 143
    sget v0, Lcom/sec/android/app/shealth/common/commonui/R$id;->list_item_text:I

    return v0
.end method

.method protected isHeightSetToWrapContent()Z
    .locals 1

    .prologue
    .line 232
    const/4 v0, 0x1

    return v0
.end method

.method public setCurrentItem(I)V
    .locals 0
    .param p1, "itemIndex"    # I

    .prologue
    .line 319
    iput p1, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->selectedItemIndex:I

    .line 320
    return-void
.end method

.method public setNewItems(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 85
    .local p1, "newItems":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->mItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->mItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 87
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->mPopupListAdapter:Lcom/sec/android/app/shealth/common/commonui/ListPopup$PopupListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/ListPopup$PopupListAdapter;->notifyDataSetChanged()V

    .line 88
    return-void
.end method

.method public setOnItemClickedListener(Lcom/sec/android/app/shealth/common/commonui/ListPopup$OnItemClickedListener;)V
    .locals 0
    .param p1, "onItemClickedListener"    # Lcom/sec/android/app/shealth/common/commonui/ListPopup$OnItemClickedListener;

    .prologue
    .line 172
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->onItemClickedListener:Lcom/sec/android/app/shealth/common/commonui/ListPopup$OnItemClickedListener;

    .line 173
    return-void
.end method

.method public setScrollable(Z)V
    .locals 1
    .param p1, "isScrollable"    # Z

    .prologue
    .line 224
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, p1}, Landroid/widget/ListView;->setScrollContainer(Z)V

    .line 225
    return-void
.end method

.method public setTextSelection(Ljava/lang/String;I)V
    .locals 1
    .param p1, "textSelection"    # Ljava/lang/String;
    .param p2, "textSelectionColorId"    # I

    .prologue
    .line 336
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->mTextSelection:Ljava/lang/String;

    .line 337
    iput p2, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->mTextSelectionColorId:I

    .line 338
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->mPopupListAdapter:Lcom/sec/android/app/shealth/common/commonui/ListPopup$PopupListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/ListPopup$PopupListAdapter;->notifyDataSetChanged()V

    .line 339
    return-void
.end method

.method public show()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 296
    invoke-virtual {p0, v0, v0}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->show(II)V

    .line 297
    return-void
.end method

.method public show(II)V
    .locals 1
    .param p1, "xOffset"    # I
    .param p2, "yOffset"    # I

    .prologue
    .line 305
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->mItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 306
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->caller:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 307
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->caller:Landroid/view/View;

    invoke-virtual {p0, v0, p1, p2}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->showAsDropDown(Landroid/view/View;II)V

    .line 312
    :cond_0
    :goto_0
    return-void

    .line 310
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->dismiss()V

    goto :goto_0
.end method
