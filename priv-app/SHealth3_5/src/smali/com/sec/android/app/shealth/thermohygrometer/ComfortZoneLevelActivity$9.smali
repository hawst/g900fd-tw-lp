.class Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$9;
.super Ljava/lang/Object;
.source "ComfortZoneLevelActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->getContentInitializationListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;)V
    .locals 0

    .prologue
    .line 696
    iput-object p1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$9;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onContentInitialization(Landroid/view/View;Landroid/app/Activity;Landroid/app/Dialog;Landroid/os/Bundle;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V
    .locals 7
    .param p1, "content"    # Landroid/view/View;
    .param p2, "parentActivity"    # Landroid/app/Activity;
    .param p3, "dialog"    # Landroid/app/Dialog;
    .param p4, "savedInstanceState"    # Landroid/os/Bundle;
    .param p5, "okButtonHandler"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;

    .prologue
    const v3, 0x7f0900ce

    const v2, 0x7f0900cd

    .line 699
    const v0, 0x7f0802ca

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$9;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->MINIMAL_TEMPETATURE:F
    invoke-static {v4}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->access$500(Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;)F

    move-result v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$9;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const-string v1, "C"

    sget-object v6, Lcom/sec/android/app/shealth/thermohygrometer/utils/TGHCache;->infoTemperatureUnit:Ljava/lang/String;

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v2

    :goto_0
    invoke-virtual {v5, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " ~ "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v4, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$9;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->MAXIMAL_TEMPETATURE:F
    invoke-static {v4}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->access$600(Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;)F

    move-result v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v4, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$9;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const-string v5, "C"

    sget-object v6, Lcom/sec/android/app/shealth/thermohygrometer/utils/TGHCache;->infoTemperatureUnit:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    :goto_1
    invoke-virtual {v4, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 708
    return-void

    :cond_0
    move v1, v3

    .line 699
    goto :goto_0

    :cond_1
    move v2, v3

    goto :goto_1
.end method
