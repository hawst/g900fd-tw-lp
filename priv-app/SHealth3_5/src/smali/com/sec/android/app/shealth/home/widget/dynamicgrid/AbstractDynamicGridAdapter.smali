.class public abstract Lcom/sec/android/app/shealth/home/widget/dynamicgrid/AbstractDynamicGridAdapter;
.super Landroid/widget/BaseAdapter;
.source "AbstractDynamicGridAdapter.java"


# static fields
.field public static final INVALID_ID:I = -0x1


# instance fields
.field private mIdMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 17
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/AbstractDynamicGridAdapter;->mIdMap:Ljava/util/HashMap;

    return-void
.end method


# virtual methods
.method protected addAllStableId(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 60
    .local p1, "items":Ljava/util/List;, "Ljava/util/List<*>;"
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/AbstractDynamicGridAdapter;->getCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/AbstractDynamicGridAdapter;->getItemId(I)J

    move-result-wide v2

    long-to-int v1, v2

    .line 61
    .local v1, "startId":I
    add-int/lit8 v1, v1, 0x1

    .line 62
    move v0, v1

    .local v0, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 63
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/AbstractDynamicGridAdapter;->mIdMap:Ljava/util/HashMap;

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 65
    :cond_0
    return-void
.end method

.method protected addStableId(Ljava/lang/Object;)V
    .locals 3
    .param p1, "item"    # Ljava/lang/Object;

    .prologue
    .line 49
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/AbstractDynamicGridAdapter;->getCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/AbstractDynamicGridAdapter;->getItemId(I)J

    move-result-wide v1

    long-to-int v0, v1

    .line 50
    .local v0, "newId":I
    add-int/lit8 v0, v0, 0x1

    .line 51
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/AbstractDynamicGridAdapter;->mIdMap:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    return-void
.end method

.method protected clearStableIdMap()V
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/AbstractDynamicGridAdapter;->mIdMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 88
    return-void
.end method

.method public abstract getColumnCount()I
.end method

.method public final getItemId(I)J
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 75
    if-ltz p1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/AbstractDynamicGridAdapter;->mIdMap:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->size()I

    move-result v1

    if-lt p1, v1, :cond_1

    .line 76
    :cond_0
    const-wide/16 v1, -0x1

    .line 79
    :goto_0
    return-wide v1

    .line 78
    :cond_1
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/AbstractDynamicGridAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 79
    .local v0, "item":Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/AbstractDynamicGridAdapter;->mIdMap:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-long v1, v1

    goto :goto_0
.end method

.method public final hasStableIds()Z
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x1

    return v0
.end method

.method protected removeStableID(Ljava/lang/Object;)V
    .locals 1
    .param p1, "item"    # Ljava/lang/Object;

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/AbstractDynamicGridAdapter;->mIdMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    return-void
.end method

.method public abstract reorderItems(II)V
.end method
