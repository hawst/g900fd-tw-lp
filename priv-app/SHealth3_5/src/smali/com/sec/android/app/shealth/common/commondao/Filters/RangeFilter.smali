.class public Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;
.super Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;
.source "RangeFilter.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final BETWEEN:Ljava/lang/String; = "BETWEEN"

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;",
            ">;"
        }
    .end annotation
.end field

.field public static final EQUALS:Ljava/lang/String; = "="

.field public static final GREATER_THAN:Ljava/lang/String; = ">"

.field public static final GREATER_THAN_EQUALS:Ljava/lang/String; = ">="

.field public static final IN:Ljava/lang/String; = "IN"

.field public static final IS_NULL:Ljava/lang/String; = "IS NULL"

.field public static final LESS_THAN:Ljava/lang/String; = "<"

.field public static final LESS_THAN_EQUALS:Ljava/lang/String; = "<="

.field public static final NOT_EQUALS:Ljava/lang/String; = "<>"


# instance fields
.field private mNameOfField:Ljava/lang/String;

.field private mRelation:Ljava/lang/String;

.field private mSqlLogicOperators:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mValue:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 120
    new-instance v0, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter$1;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "parcel"    # Landroid/os/Parcel;

    .prologue
    .line 140
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;-><init>()V

    .line 36
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;->mNameOfField:Ljava/lang/String;

    .line 39
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;->mSqlLogicOperators:Ljava/util/Set;

    .line 42
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;->mSqlLogicOperators:Ljava/util/Set;

    const-string v1, "<>"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 43
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;->mSqlLogicOperators:Ljava/util/Set;

    const-string v1, "="

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 44
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;->mSqlLogicOperators:Ljava/util/Set;

    const-string v1, ">"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 45
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;->mSqlLogicOperators:Ljava/util/Set;

    const-string v1, ">="

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 46
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;->mSqlLogicOperators:Ljava/util/Set;

    const-string v1, "<"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 47
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;->mSqlLogicOperators:Ljava/util/Set;

    const-string v1, "<="

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 48
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;->mSqlLogicOperators:Ljava/util/Set;

    const-string v1, "IS NULL"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 49
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;->mSqlLogicOperators:Ljava/util/Set;

    const-string v1, "BETWEEN"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 50
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;->mSqlLogicOperators:Ljava/util/Set;

    const-string v1, "IN"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 141
    invoke-static {}, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;->throwUnsupoprtedParcelableException()V

    .line 142
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V
    .locals 3
    .param p1, "nameOfField"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/Object;
    .param p3, "relation"    # Ljava/lang/String;

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;-><init>()V

    .line 36
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;->mNameOfField:Ljava/lang/String;

    .line 39
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;->mSqlLogicOperators:Ljava/util/Set;

    .line 42
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;->mSqlLogicOperators:Ljava/util/Set;

    const-string v1, "<>"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 43
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;->mSqlLogicOperators:Ljava/util/Set;

    const-string v1, "="

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 44
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;->mSqlLogicOperators:Ljava/util/Set;

    const-string v1, ">"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 45
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;->mSqlLogicOperators:Ljava/util/Set;

    const-string v1, ">="

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 46
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;->mSqlLogicOperators:Ljava/util/Set;

    const-string v1, "<"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 47
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;->mSqlLogicOperators:Ljava/util/Set;

    const-string v1, "<="

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 48
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;->mSqlLogicOperators:Ljava/util/Set;

    const-string v1, "IS NULL"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 49
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;->mSqlLogicOperators:Ljava/util/Set;

    const-string v1, "BETWEEN"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 50
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;->mSqlLogicOperators:Ljava/util/Set;

    const-string v1, "IN"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 59
    if-nez p1, :cond_0

    .line 60
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Name of field should not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 62
    :cond_0
    if-nez p3, :cond_1

    .line 63
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Relation should be just from this list: \'=\', \'<>\', \'<\', \'>\', \'IS_NULL NULL\'"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 65
    :cond_1
    if-nez p2, :cond_2

    const-string v0, "IS NULL"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 66
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Value should not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 68
    :cond_2
    const-string v0, "IS NULL"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    instance-of v0, p2, Ljava/lang/Number;

    if-nez v0, :cond_3

    instance-of v0, p2, Ljava/lang/String;

    if-nez v0, :cond_3

    .line 69
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Value should be just from this list of types: Integer, Double, Float, Byte, Short, String, Long"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 71
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;->mSqlLogicOperators:Ljava/util/Set;

    invoke-interface {v0, p3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 72
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Relation should be just from this list: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;->mSqlLogicOperators:Ljava/util/Set;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 74
    :cond_4
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;->mNameOfField:Ljava/lang/String;

    .line 75
    iput-object p2, p0, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;->mValue:Ljava/lang/Object;

    .line 76
    iput-object p3, p0, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;->mRelation:Ljava/lang/String;

    .line 77
    return-void
.end method

.method static synthetic access$000()V
    .locals 0

    .prologue
    .line 26
    invoke-static {}, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;->throwUnsupoprtedParcelableException()V

    return-void
.end method

.method private static throwUnsupoprtedParcelableException()V
    .locals 3

    .prologue
    .line 150
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-class v2, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " does not support Parcelable interface though implements it"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 137
    const/4 v0, 0x0

    return v0
.end method

.method public getNameOfField()Ljava/lang/String;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;->mNameOfField:Ljava/lang/String;

    return-object v0
.end method

.method public getRelation()Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;->mRelation:Ljava/lang/String;

    return-object v0
.end method

.method public getSQLCode()Ljava/lang/String;
    .locals 4

    .prologue
    .line 107
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;->mRelation:Ljava/lang/String;

    const-string v2, "IS NULL"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 108
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;->mNameOfField:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "IS NULL"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 116
    :goto_0
    return-object v1

    .line 110
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;->mValue:Ljava/lang/Object;

    instance-of v1, v1, Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;->mRelation:Ljava/lang/String;

    const-string v2, "BETWEEN"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;->mRelation:Ljava/lang/String;

    const-string v2, "IN"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 111
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;->mValue:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    const-string v2, "\'"

    const-string v3, "\'\'"

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 112
    .local v0, "valueWithQuotesDoubled":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " UPPER("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;->mNameOfField:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;->mRelation:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " UPPER(\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\') "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 113
    .end local v0    # "valueWithQuotesDoubled":Ljava/lang/String;
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;->mValue:Ljava/lang/Object;

    instance-of v1, v1, Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;->mRelation:Ljava/lang/String;

    const-string v2, "IN"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 114
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;->mNameOfField:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;->mRelation:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;->mValue:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 116
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;->mNameOfField:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;->mRelation:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;->mValue:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0
.end method

.method public getValue()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;->mValue:Ljava/lang/Object;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 146
    invoke-static {}, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;->throwUnsupoprtedParcelableException()V

    .line 147
    return-void
.end method
