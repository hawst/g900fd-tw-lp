.class public Lcom/sec/android/app/shealth/logutils/graph/LegendMark;
.super Ljava/lang/Object;
.source "LegendMark.java"


# instance fields
.field private mDrawableId:I

.field private mLabelStringId:I

.field private mMarkColorId:I


# direct methods
.method constructor <init>(III)V
    .locals 0
    .param p1, "drawableId"    # I
    .param p2, "markColorId"    # I
    .param p3, "labelStringId"    # I

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput p1, p0, Lcom/sec/android/app/shealth/logutils/graph/LegendMark;->mDrawableId:I

    .line 35
    iput p2, p0, Lcom/sec/android/app/shealth/logutils/graph/LegendMark;->mMarkColorId:I

    .line 36
    iput p3, p0, Lcom/sec/android/app/shealth/logutils/graph/LegendMark;->mLabelStringId:I

    .line 37
    return-void
.end method


# virtual methods
.method public createLegendMarkImageView(Landroid/content/Context;)Landroid/widget/ImageView;
    .locals 8
    .param p1, "ctx"    # Landroid/content/Context;

    .prologue
    const/4 v7, 0x0

    .line 73
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/sec/android/app/shealth/logutils/R$dimen;->graph_legend_area_legend_mark_width:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 75
    .local v3, "imageMarkWidth":I
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/sec/android/app/shealth/logutils/R$dimen;->graph_legend_area_legend_mark_height:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 77
    .local v0, "imageMarkHeight":I
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v3, v0}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 79
    .local v2, "imageMarkViewParams":Landroid/widget/LinearLayout$LayoutParams;
    new-instance v1, Landroid/widget/ImageView;

    invoke-direct {v1, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 80
    .local v1, "imageMarkView":Landroid/widget/ImageView;
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/sec/android/app/shealth/logutils/R$dimen;->graph_legend_area_legend_mark_margin_left:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Lcom/sec/android/app/shealth/logutils/R$dimen;->graph_text_mark_view_left_offset:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    invoke-virtual {v2, v4, v7, v5, v7}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 82
    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 83
    iget v4, p0, Lcom/sec/android/app/shealth/logutils/graph/LegendMark;->mMarkColorId:I

    const/4 v5, -0x1

    if-eq v4, v5, :cond_0

    .line 84
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/shealth/logutils/graph/LegendMark;->mMarkColorId:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 88
    :goto_0
    return-object v1

    .line 86
    :cond_0
    iget v4, p0, Lcom/sec/android/app/shealth/logutils/graph/LegendMark;->mDrawableId:I

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public createLegendMarkTextView(Landroid/content/Context;)Landroid/widget/TextView;
    .locals 7
    .param p1, "ctx"    # Landroid/content/Context;

    .prologue
    .line 47
    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 48
    .local v1, "textMarkView":Landroid/widget/TextView;
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Lcom/sec/android/app/shealth/logutils/R$dimen;->graph_legend_area_text_mark_width:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 50
    .local v3, "textMarkWidth":I
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Lcom/sec/android/app/shealth/logutils/R$dimen;->graph_legend_area_legend_mark_text_size:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 52
    .local v4, "textSize":I
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Lcom/sec/android/app/shealth/logutils/R$color;->legend_mark_text_color:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 54
    .local v0, "textColor":I
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v5, -0x2

    invoke-direct {v2, v3, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 56
    .local v2, "textMarkViewParams":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 57
    invoke-virtual {v1}, Landroid/widget/TextView;->setSingleLine()V

    .line 58
    sget-object v5, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 59
    const/4 v5, 0x0

    int-to-float v6, v4

    invoke-virtual {v1, v5, v6}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 60
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 61
    iget v5, p0, Lcom/sec/android/app/shealth/logutils/graph/LegendMark;->mLabelStringId:I

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(I)V

    .line 62
    return-object v1
.end method
