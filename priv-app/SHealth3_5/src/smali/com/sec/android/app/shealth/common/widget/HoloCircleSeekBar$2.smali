.class Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar$2;
.super Ljava/lang/Object;
.source "HoloCircleSeekBar.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->doUiChanged()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;)V
    .locals 0

    .prologue
    .line 365
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/high16 v3, 0x43b40000    # 360.0f

    .line 367
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;

    # getter for: Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->isTimerTaskRunning:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->access$100(Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 390
    :goto_0
    return-void

    .line 369
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;

    # getter for: Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mCurrentAniValue:I
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->access$200(Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;)I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;

    # getter for: Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mCurrentValue:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->access$300(Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;)I

    move-result v1

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    if-lez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;

    # getter for: Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mCurrentAniValue:I
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->access$200(Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;)I

    move-result v0

    if-ltz v0, :cond_2

    .line 370
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;

    # getter for: Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mCurrentAniValue:I
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->access$200(Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;)I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;

    # getter for: Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mCurrentValue:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->access$300(Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;)I

    move-result v1

    if-le v0, v1, :cond_1

    .line 371
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;

    # -= operator for: Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mCurrentAniValue:I
    invoke-static {v0, v2}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->access$220(Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;I)I

    .line 375
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;

    # getter for: Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mCurrentAniValue:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->access$200(Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;)I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;

    # getter for: Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mMax:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->access$500(Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    mul-float/2addr v1, v3

    float-to-int v1, v1

    # setter for: Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mArcFinishAngle:I
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->access$402(Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;I)I

    .line 376
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;

    # getter for: Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mArcFinishAngle:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->access$400(Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;)I

    move-result v2

    # invokes: Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->calculateRadiansFromAngle(I)F
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->access$700(Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;I)F

    move-result v1

    # setter for: Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mRadian:F
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->access$602(Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;F)F

    .line 377
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->invalidate()V

    goto :goto_0

    .line 373
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;

    # += operator for: Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mCurrentAniValue:I
    invoke-static {v0, v2}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->access$212(Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;I)I

    goto :goto_1

    .line 379
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;

    # getter for: Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mBackupValue:I
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->access$800(Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;)I

    move-result v0

    if-ltz v0, :cond_3

    .line 380
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;

    # getter for: Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mBackupValue:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->access$800(Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;)I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;

    # getter for: Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mMax:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->access$500(Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    mul-float/2addr v1, v3

    float-to-int v1, v1

    # setter for: Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mArcFinishAngle:I
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->access$402(Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;I)I

    .line 384
    :goto_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;

    # getter for: Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mArcFinishAngle:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->access$400(Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;)I

    move-result v2

    # invokes: Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->calculateRadiansFromAngle(I)F
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->access$700(Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;I)F

    move-result v1

    # setter for: Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mRadian:F
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->access$602(Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;F)F

    .line 385
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->invalidate()V

    .line 386
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;

    # invokes: Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->stopTask()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->access$900(Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;)V

    goto/16 :goto_0

    .line 382
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;

    # getter for: Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mCurrentValue:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->access$300(Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;)I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;

    # getter for: Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mMax:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->access$500(Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    mul-float/2addr v1, v3

    float-to-int v1, v1

    # setter for: Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->mArcFinishAngle:I
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->access$402(Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;I)I

    goto :goto_2
.end method
