.class public final Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;
.super Ljava/lang/Object;
.source "FoodPluginMealUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils$FocusRunnable;
    }
.end annotation


# static fields
.field public static final GALLERY_PATH:Ljava/lang/String;

.field private static final LOG_TAG:Ljava/lang/String;

.field private static final QUICK_INPUT_PREF:Ljava/lang/String; = "food_quick_input_pref"

.field private static final URI_SHEALTH_SERVICE_PACKAGE:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 84
    const-class v0, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->getLogTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->LOG_TAG:Ljava/lang/String;

    .line 85
    const-string/jumbo v0, "package:com.sec.android.service.health"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->URI_SHEALTH_SERVICE_PACKAGE:Landroid/net/Uri;

    .line 90
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/shealth/common/utils/constants/FilePathsConstants;->FOOD_IMAGES_FOLDER_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->GALLERY_PATH:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    return-void
.end method

.method public static calculateKcalForFoodInfoData(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;)F
    .locals 4
    .param p0, "extraFoodInfo"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;

    .prologue
    const/4 v3, 0x0

    .line 667
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;->hasCalories()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 668
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;->getCalories()I

    move-result v2

    int-to-float v2, v2

    .line 677
    :goto_0
    return v2

    .line 670
    :cond_0
    const/high16 v1, -0x40800000    # -1.0f

    .line 671
    .local v1, "kcalInUnit":F
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;->getSearchResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;

    .line 672
    .local v0, "data":Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getGrammInKcal()F

    move-result v2

    cmpl-float v2, v2, v3

    if-lez v2, :cond_2

    .line 673
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getGrammInKcal()F

    move-result v1

    .line 677
    :cond_1
    :goto_1
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getDefaultNumber()F

    move-result v2

    div-float/2addr v2, v1

    goto :goto_0

    .line 674
    :cond_2
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getOzInKcal()F

    move-result v2

    cmpl-float v2, v2, v3

    if-lez v2, :cond_1

    .line 675
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getOzInKcal()F

    move-result v1

    goto :goto_1
.end method

.method public static calculateMealTypeAccordingToCurrentTime()I
    .locals 6

    .prologue
    .line 429
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v1

    .line 430
    .local v1, "calendar":Ljava/util/Calendar;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 431
    const/16 v4, 0xb

    invoke-virtual {v1, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    int-to-float v2, v4

    .line 432
    .local v2, "hour":F
    const/16 v4, 0xc

    invoke-virtual {v1, v4}, Ljava/util/Calendar;->get(I)I

    move-result v3

    .line 433
    .local v3, "minute":I
    int-to-float v4, v3

    const/high16 v5, 0x42700000    # 60.0f

    div-float/2addr v4, v5

    add-float/2addr v2, v4

    .line 434
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/Utils$BoundaryChecker;

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-direct {v0, v4}, Lcom/sec/android/app/shealth/common/utils/Utils$BoundaryChecker;-><init>(Ljava/lang/Comparable;)V

    .line 436
    .local v0, "boundaryChecker":Lcom/sec/android/app/shealth/common/utils/Utils$BoundaryChecker;, "Lcom/sec/android/app/shealth/common/utils/Utils$BoundaryChecker<Ljava/lang/Float;>;"
    const/high16 v4, 0x40c00000    # 6.0f

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    const/high16 v5, 0x41100000    # 9.0f

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Lcom/sec/android/app/shealth/common/utils/Utils$BoundaryChecker;->isValueInBounds(Ljava/lang/Comparable;Ljava/lang/Comparable;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 437
    const v4, 0x186a1

    .line 443
    :goto_0
    return v4

    .line 438
    :cond_0
    const/high16 v4, 0x41300000    # 11.0f

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    const/high16 v5, 0x41600000    # 14.0f

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Lcom/sec/android/app/shealth/common/utils/Utils$BoundaryChecker;->isValueInBounds(Ljava/lang/Comparable;Ljava/lang/Comparable;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 439
    const v4, 0x186a2

    goto :goto_0

    .line 440
    :cond_1
    const/high16 v4, 0x41880000    # 17.0f

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    const/high16 v5, 0x41a00000    # 20.0f

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Lcom/sec/android/app/shealth/common/utils/Utils$BoundaryChecker;->isValueInBounds(Ljava/lang/Comparable;Ljava/lang/Comparable;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 441
    const v4, 0x186a3

    goto :goto_0

    .line 443
    :cond_2
    const v4, 0x186a4

    goto :goto_0
.end method

.method public static convertFloatKcalValueToInt(F)I
    .locals 1
    .param p0, "calories"    # F

    .prologue
    .line 172
    float-to-int v0, p0

    return v0
.end method

.method public static convertToSingleDecimal(F)F
    .locals 4
    .param p0, "calories"    # F

    .prologue
    const/high16 v3, 0x41200000    # 10.0f

    .line 182
    mul-float v1, p0, v3

    float-to-double v1, v1

    invoke-static {v1, v2}, Ljava/lang/Math;->floor(D)D

    move-result-wide v1

    double-to-float v0, v1

    .line 183
    .local v0, "total":F
    div-float/2addr v0, v3

    .line 184
    return v0
.end method

.method public static deleteAllDataFromDB(Landroid/content/Context;)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 402
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 403
    .local v0, "allDao":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/commondao/CommonDao;>;"
    new-instance v6, Lcom/sec/android/app/shealth/food/fooddao/MealDaoMyFoodImplDb;

    invoke-direct {v6, p0}, Lcom/sec/android/app/shealth/food/fooddao/MealDaoMyFoodImplDb;-><init>(Landroid/content/Context;)V

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 404
    new-instance v6, Lcom/sec/android/app/shealth/food/fooddao/MealDaoImplDb;

    invoke-direct {v6, p0}, Lcom/sec/android/app/shealth/food/fooddao/MealDaoImplDb;-><init>(Landroid/content/Context;)V

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 405
    new-instance v6, Lcom/sec/android/app/shealth/food/fooddao/MealImageDaoImplDb;

    invoke-direct {v6, p0}, Lcom/sec/android/app/shealth/food/fooddao/MealImageDaoImplDb;-><init>(Landroid/content/Context;)V

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 406
    new-instance v6, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;

    invoke-direct {v6, p0}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;-><init>(Landroid/content/Context;)V

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 407
    new-instance v6, Lcom/sec/android/app/shealth/food/fooddao/MealItemDaoImplDb;

    invoke-direct {v6, p0}, Lcom/sec/android/app/shealth/food/fooddao/MealItemDaoImplDb;-><init>(Landroid/content/Context;)V

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 408
    new-instance v6, Lcom/sec/android/app/shealth/food/fooddao/ExtraFoodInfoDaoImplDb;

    invoke-direct {v6, p0}, Lcom/sec/android/app/shealth/food/fooddao/ExtraFoodInfoDaoImplDb;-><init>(Landroid/content/Context;)V

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 409
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/common/commondao/CommonDao;

    .line 410
    .local v1, "dao":Lcom/sec/android/app/shealth/common/commondao/CommonDao;
    invoke-interface {v1}, Lcom/sec/android/app/shealth/common/commondao/CommonDao;->deleteAll()I

    goto :goto_0

    .line 412
    .end local v1    # "dao":Lcom/sec/android/app/shealth/common/commondao/CommonDao;
    :cond_0
    new-instance v3, Lcom/sec/android/app/shealth/common/commondao/goaldao/GoalDaoImplDb;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/common/commondao/goaldao/GoalDaoImplDb;-><init>(Landroid/content/Context;)V

    .line 413
    .local v3, "goalDao":Lcom/sec/android/app/shealth/common/commondao/goaldao/GoalDao;
    const v6, 0x9c43

    invoke-interface {v3, v6}, Lcom/sec/android/app/shealth/common/commondao/goaldao/GoalDao;->deleteAllByGoalType(I)I

    .line 414
    const-string v6, "food_quick_input_pref"

    const/4 v7, 0x0

    invoke-virtual {p0, v6, v7}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v5

    .line 415
    .local v5, "prefs":Landroid/content/SharedPreferences;
    if-eqz v5, :cond_1

    .line 417
    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 418
    .local v2, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    .line 419
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 421
    .end local v2    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_1
    return-void
.end method

.method public static doesMealHaveMealItems(Landroid/content/Context;J)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "mealId"    # J

    .prologue
    .line 795
    new-instance v0, Lcom/sec/android/app/shealth/food/fooddao/MealItemDaoImplDb;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/food/fooddao/MealItemDaoImplDb;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/shealth/food/fooddao/MealItemDaoImplDb;->getMealItemsListByMealId(J)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getCaloriesString(Landroid/content/res/Resources;F)Ljava/lang/String;
    .locals 3
    .param p0, "resources"    # Landroid/content/res/Resources;
    .param p1, "calories"    # F

    .prologue
    const/4 v2, 0x0

    .line 457
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/sec/android/app/shealth/food/foodpick/ConditionChecker;->must([Ljava/lang/Object;)Lcom/sec/android/app/shealth/food/foodpick/ConditionChecker$MustChecker;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/sec/android/app/shealth/food/foodpick/ConditionChecker$MustChecker;->beGreaterThen(I)V

    .line 458
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    float-to-int v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v1, 0x7f0900b9

    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getCaptionForCategoryInListView(Landroid/content/Context;I)Ljava/lang/String;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "stringResId"    # I

    .prologue
    .line 469
    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getDataPresentCountInPeriod(JJ)I
    .locals 7
    .param p0, "startTime"    # J
    .param p2, "endTime"    # J

    .prologue
    .line 922
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "select count(distinct (strftime(\"%d-%m-%Y\",([sample_time]/1000),\'unixepoch\',\'localtime\'))) as meal_count from meal where sample_time between "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " and "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 924
    .local v3, "query":Ljava/lang/String;
    const/4 v6, 0x0

    .line 927
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 928
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 930
    const-string/jumbo v0, "meal_count"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 935
    if-eqz v6, :cond_0

    .line 936
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 938
    :cond_0
    :goto_0
    return v0

    .line 935
    :cond_1
    if-eqz v6, :cond_2

    .line 936
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 938
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 935
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    .line 936
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method public static getDefaultIntakeCalorieGoal()I
    .locals 1

    .prologue
    .line 510
    invoke-static {}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->getRawDefaultIntakeCalorieGoal()F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method public static getFavoriteMeals(Landroid/content/Context;)Ljava/util/List;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 576
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 577
    .local v4, "result":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;>;"
    new-instance v2, Lcom/sec/android/app/shealth/food/fooddao/MealDaoMyFoodImplDb;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/food/fooddao/MealDaoMyFoodImplDb;-><init>(Landroid/content/Context;)V

    .line 578
    .local v2, "mealDao":Lcom/sec/android/app/shealth/food/fooddao/MealDao;
    invoke-interface {v2}, Lcom/sec/android/app/shealth/food/fooddao/MealDao;->getAllDatas()Ljava/util/List;

    move-result-object v0

    .line 579
    .local v0, "allItems":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    .line 580
    .local v3, "mealData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    invoke-virtual {v3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->isFavourite()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 581
    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 584
    .end local v3    # "mealData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    :cond_1
    return-object v4
.end method

.method public static getFavoritesFoods(Landroid/content/Context;)Ljava/util/List;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 557
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 558
    .local v4, "result":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;>;"
    new-instance v1, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;-><init>(Landroid/content/Context;)V

    .line 559
    .local v1, "foodInfoDao":Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;
    invoke-interface {v1}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;->getAllDatas()Ljava/util/List;

    move-result-object v0

    .line 560
    .local v0, "allItems":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    .line 561
    .local v2, "foodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getFavorite()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 562
    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 565
    .end local v2    # "foodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    :cond_1
    return-object v4
.end method

.method public static getFoodInfoItemFromMealItem(Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;Landroid/content/Context;)Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    .locals 3
    .param p0, "mealItem"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 202
    new-instance v0, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;

    invoke-direct {v0, p1}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;-><init>(Landroid/content/Context;)V

    .line 203
    .local v0, "foodInfoDAO":Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->getFoodInfoId()J

    move-result-wide v1

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;->getDataById(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    return-object v1
.end method

.method public static getGoalCaloriesByDate(Landroid/content/Context;J)F
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "endOfDayTime"    # J

    .prologue
    const v1, 0x9c43

    .line 481
    invoke-static {p0, v1, p1, p2}, Lcom/sec/android/app/shealth/common/utils/hcp/GoalUtils;->getLatestGoalDataForTime(Landroid/content/Context;IJ)Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    move-result-object v0

    .line 484
    .local v0, "goalData":Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;
    if-nez v0, :cond_0

    .line 486
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/sec/android/app/shealth/common/utils/hcp/GoalUtils;->getGoalDataForOldDates(Landroid/content/Context;Ljava/lang/Integer;)Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    move-result-object v0

    .line 487
    if-nez v0, :cond_0

    .line 489
    invoke-static {}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->getRawDefaultIntakeCalorieGoal()F

    move-result v1

    .line 493
    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->getValue()F

    move-result v1

    goto :goto_0
.end method

.method private static getImageFullPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p0, "dirPath"    # Ljava/lang/String;

    .prologue
    .line 122
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 123
    .local v0, "dirForFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    .line 124
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v1

    .line 126
    .local v1, "isDirCreated":Z
    if-nez v1, :cond_0

    .line 128
    new-instance v2, Ljava/lang/AssertionError;

    const-string v3, "Not able to create directory"

    invoke-direct {v2, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2

    .line 131
    .end local v1    # "isDirCreated":Z
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "food"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%d"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".jpg"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public static getImagePathByUri(Landroid/net/Uri;Landroid/content/Context;)Ljava/lang/String;
    .locals 12
    .param p0, "uri"    # Landroid/net/Uri;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v11, 0x0

    const/4 v3, 0x0

    .line 143
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "_data"

    aput-object v0, v2, v11

    .line 144
    .local v2, "filePathColumn":[Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v1, p0

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    invoke-virtual/range {v0 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;

    move-result-object v8

    .line 145
    .local v8, "cursor":Landroid/database/Cursor;
    if-eqz v8, :cond_2

    .line 146
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v10

    .line 149
    .local v10, "isMovedtoFirst":Z
    if-eqz v10, :cond_1

    .line 151
    aget-object v0, v2, v11

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    .line 152
    .local v7, "columnIndex":I
    invoke-interface {v8, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 153
    .local v9, "filePath":Ljava/lang/String;
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 155
    if-eqz p0, :cond_0

    if-nez v9, :cond_0

    .line 157
    invoke-virtual {p0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v9

    .line 163
    .end local v7    # "columnIndex":I
    .end local v9    # "filePath":Ljava/lang/String;
    .end local v10    # "isMovedtoFirst":Z
    :cond_0
    :goto_0
    return-object v9

    .restart local v10    # "isMovedtoFirst":Z
    :cond_1
    move-object v9, v3

    .line 161
    goto :goto_0

    .line 163
    .end local v10    # "isMovedtoFirst":Z
    :cond_2
    invoke-virtual {p0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v9

    goto :goto_0
.end method

.method public static getIntentFilterForClearDataAction()Landroid/content/IntentFilter;
    .locals 2

    .prologue
    .line 750
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.PACKAGE_DATA_CLEARED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 751
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    const-string/jumbo v1, "package"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 752
    return-object v0
.end method

.method public static getLatestDataTimestamp(Ljava/lang/String;J)J
    .locals 7
    .param p0, "format"    # Ljava/lang/String;
    .param p1, "dateTime"    # J

    .prologue
    .line 949
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "select sample_time from meal where strftime(\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\",("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "sample_time"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/1000),\'unixepoch\', \'localtime\') = strftime(\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\",("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/1000),\'unixepoch\', \'localtime\') order by "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "sample_time"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " desc limit 1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 952
    .local v3, "query":Ljava/lang/String;
    const/4 v6, 0x0

    .line 955
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 956
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 958
    const-string/jumbo v0, "sample_time"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 963
    if-eqz v6, :cond_0

    .line 964
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 966
    :cond_0
    :goto_0
    return-wide v0

    .line 963
    :cond_1
    if-eqz v6, :cond_2

    .line 964
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 966
    :cond_2
    const-wide/16 v0, 0x0

    goto :goto_0

    .line 963
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    .line 964
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method public static getMealCalories(Ljava/util/List;Landroid/content/Context;)I
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;",
            ">;",
            "Landroid/content/Context;",
            ")I"
        }
    .end annotation

    .prologue
    .line 100
    .local p0, "mealItems":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;>;"
    const/4 v0, 0x0

    .line 101
    .local v0, "calories":I
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    .line 102
    .local v3, "mealItem":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;
    invoke-static {v3, p1}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->getFoodInfoItemFromMealItem(Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;Landroid/content/Context;)Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    move-result-object v1

    .line 103
    .local v1, "foodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getServerSourceType()I

    move-result v4

    invoke-static {v4}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->isQuickInputType(I)Z

    move-result v4

    if-nez v4, :cond_0

    .line 104
    invoke-static {v3, v1, p1}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->getMealItemCalorie(Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;Landroid/content/Context;)I

    move-result v4

    int-to-float v4, v4

    invoke-static {v4}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->convertFloatKcalValueToInt(F)I

    move-result v4

    add-int/2addr v0, v4

    goto :goto_0

    .line 108
    .end local v1    # "foodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    .end local v3    # "mealItem":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;
    :cond_1
    return v0
.end method

.method public static getMealItemCalorie(Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;Landroid/content/Context;)I
    .locals 1
    .param p0, "mealItem"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 194
    invoke-static {p0, p1}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->getFoodInfoItemFromMealItem(Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;Landroid/content/Context;)Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    move-result-object v0

    invoke-static {p0, v0, p1}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->getMealItemCalorie(Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;Landroid/content/Context;)I

    move-result v0

    return v0
.end method

.method public static getMealItemCalorie(Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;Landroid/content/Context;)I
    .locals 9
    .param p0, "mealItem"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;
    .param p1, "foodInfoItem"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v8, 0x0

    const/4 v5, 0x0

    .line 219
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->getAmount()F

    move-result v0

    .line 220
    .local v0, "amount":F
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->getUnit()I

    move-result v6

    const v7, 0x1d4c4

    if-ne v6, v7, :cond_1

    .line 221
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->convertFloatKcalValueToInt(F)I

    move-result v5

    .line 255
    :cond_0
    :goto_0
    return v5

    .line 224
    :cond_1
    if-eqz p1, :cond_0

    .line 229
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->isAddedByUser()Z

    move-result v6

    if-nez v6, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->getUnit()I

    move-result v6

    const v7, 0x1d4c1

    if-ne v6, v7, :cond_3

    .line 230
    :cond_2
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getkCal()F

    move-result v5

    mul-float/2addr v5, v0

    invoke-static {v5}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->convertFloatKcalValueToInt(F)I

    move-result v5

    goto :goto_0

    .line 232
    :cond_3
    new-instance v2, Lcom/sec/android/app/shealth/food/fooddao/ExtraFoodInfoDaoImplDb;

    invoke-direct {v2, p2}, Lcom/sec/android/app/shealth/food/fooddao/ExtraFoodInfoDaoImplDb;-><init>(Landroid/content/Context;)V

    .line 233
    .local v2, "extraFoodInfoDao":Lcom/sec/android/app/shealth/food/fooddao/ExtraFoodInfoDao;
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getId()J

    move-result-wide v3

    .line 234
    .local v3, "foodInfoId":J
    invoke-interface {v2, v3, v4}, Lcom/sec/android/app/shealth/food/fooddao/ExtraFoodInfoDao;->getExtraByFoodId(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;

    move-result-object v1

    .line 236
    .local v1, "extendedFoodInfo":Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;
    if-eqz v1, :cond_4

    .line 237
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->getUnit()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    .line 251
    new-instance v5, Ljava/lang/AssertionError;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "if meal item has extended info, its unit should be one of "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-class v7, Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$MealItemUnit;

    invoke-virtual {v7}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v5

    .line 239
    :pswitch_0
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getOzInKcal()F

    move-result v6

    cmpl-float v6, v6, v8

    if-eqz v6, :cond_0

    .line 242
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getOzInKcal()F

    move-result v5

    div-float v5, v0, v5

    invoke-static {v5}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->convertFloatKcalValueToInt(F)I

    move-result v5

    goto :goto_0

    .line 245
    :pswitch_1
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getGrammInKcal()F

    move-result v6

    cmpl-float v6, v6, v8

    if-eqz v6, :cond_0

    .line 248
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getGrammInKcal()F

    move-result v5

    div-float v5, v0, v5

    invoke-static {v5}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->convertFloatKcalValueToInt(F)I

    move-result v5

    goto :goto_0

    .line 255
    :cond_4
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getkCal()F

    move-result v5

    mul-float/2addr v5, v0

    invoke-static {v5}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->convertFloatKcalValueToInt(F)I

    move-result v5

    goto :goto_0

    .line 237
    :pswitch_data_0
    .packed-switch 0x1d4c2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static getMealItemCalorieAndNutrients(Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;Landroid/content/Context;)Lcom/sec/android/app/shealth/food/utils/FoodItemCaloriesAndNutrients;
    .locals 1
    .param p0, "mealItem"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 269
    invoke-static {p0, p1}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->getFoodInfoItemFromMealItem(Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;Landroid/content/Context;)Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    move-result-object v0

    invoke-static {p0, v0, p1}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->getMealItemCalorieAndNutrients(Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;Landroid/content/Context;)Lcom/sec/android/app/shealth/food/utils/FoodItemCaloriesAndNutrients;

    move-result-object v0

    return-object v0
.end method

.method public static getMealItemCalorieAndNutrients(Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;Landroid/content/Context;)Lcom/sec/android/app/shealth/food/utils/FoodItemCaloriesAndNutrients;
    .locals 15
    .param p0, "mealItem"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;
    .param p1, "foodInfoItem"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 286
    if-nez p0, :cond_0

    .line 288
    sget-object v12, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v13, "mealItem is null"

    invoke-static {v12, v13}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 289
    new-instance v12, Ljava/lang/AssertionError;

    const-string v13, "MealItem cannot be null, it should be some valid entry"

    invoke-direct {v12, v13}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v12

    .line 291
    :cond_0
    if-nez p2, :cond_6

    .line 293
    sget-object v12, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->LOG_TAG:Ljava/lang/String;

    const-string v13, "context is null"

    invoke-static {v12, v13}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 294
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object p2

    .line 307
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->getAmount()F

    move-result v1

    .line 308
    .local v1, "amount":F
    const/4 v10, 0x0

    .line 309
    .local v10, "kCal":F
    const/4 v11, 0x0

    .line 310
    .local v11, "proteins":F
    const/4 v2, 0x0

    .line 311
    .local v2, "carbos":F
    const/4 v6, 0x0

    .line 313
    .local v6, "fat":F
    new-instance v5, Lcom/sec/android/app/shealth/food/fooddao/ExtraFoodInfoDaoImplDb;

    move-object/from16 v0, p2

    invoke-direct {v5, v0}, Lcom/sec/android/app/shealth/food/fooddao/ExtraFoodInfoDaoImplDb;-><init>(Landroid/content/Context;)V

    .line 314
    .local v5, "extraFoodInfoDao":Lcom/sec/android/app/shealth/food/fooddao/ExtraFoodInfoDao;
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getId()J

    move-result-wide v7

    .line 315
    .local v7, "foodInfoId":J
    invoke-interface {v5, v7, v8}, Lcom/sec/android/app/shealth/food/fooddao/ExtraFoodInfoDao;->getExtraByFoodId(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;

    move-result-object v4

    .line 317
    .local v4, "extendedFoodInfo":Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->getUnit()I

    move-result v12

    const v13, 0x1d4c4

    if-ne v12, v13, :cond_2

    .line 319
    move v10, v1

    .line 322
    :cond_2
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->isAddedByUser()Z

    move-result v12

    if-nez v12, :cond_3

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->getUnit()I

    move-result v12

    const v13, 0x1d4c1

    if-ne v12, v13, :cond_b

    .line 324
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->getUnit()I

    move-result v12

    const v13, 0x1d4c1

    if-ne v12, v13, :cond_4

    .line 326
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getkCal()F

    move-result v12

    mul-float v10, v12, v1

    .line 329
    :cond_4
    if-eqz v4, :cond_5

    .line 331
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->getUnit()I

    move-result v12

    const v13, 0x1d4c1

    if-ne v12, v13, :cond_7

    .line 333
    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getProtein()F

    move-result v12

    mul-float/2addr v12, v1

    invoke-static {v12}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->convertToSingleDecimal(F)F

    move-result v11

    .line 334
    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getCarbohydrate()F

    move-result v12

    mul-float/2addr v12, v1

    invoke-static {v12}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->convertToSingleDecimal(F)F

    move-result v2

    .line 335
    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getFat()F

    move-result v12

    mul-float/2addr v12, v1

    invoke-static {v12}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->convertToSingleDecimal(F)F

    move-result v6

    .line 344
    :cond_5
    :goto_0
    new-instance v12, Lcom/sec/android/app/shealth/food/utils/FoodItemCaloriesAndNutrients;

    invoke-static {v10}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->convertFloatKcalValueToInt(F)I

    move-result v13

    invoke-direct {v12, v13, v11, v2, v6}, Lcom/sec/android/app/shealth/food/utils/FoodItemCaloriesAndNutrients;-><init>(IFFF)V

    .line 372
    :goto_1
    return-object v12

    .line 296
    .end local v1    # "amount":F
    .end local v2    # "carbos":F
    .end local v4    # "extendedFoodInfo":Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;
    .end local v5    # "extraFoodInfoDao":Lcom/sec/android/app/shealth/food/fooddao/ExtraFoodInfoDao;
    .end local v6    # "fat":F
    .end local v7    # "foodInfoId":J
    .end local v10    # "kCal":F
    .end local v11    # "proteins":F
    :cond_6
    if-nez p1, :cond_1

    .line 298
    sget-object v12, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->LOG_TAG:Ljava/lang/String;

    const-string v13, "foodInfoItem is null"

    invoke-static {v12, v13}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 299
    move-object/from16 v0, p2

    invoke-static {p0, v0}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->getFoodInfoItemFromMealItem(Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;Landroid/content/Context;)Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    move-result-object p1

    .line 300
    if-nez p1, :cond_1

    .line 302
    sget-object v12, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->LOG_TAG:Ljava/lang/String;

    const-string v13, "foodInfoItem is not found in DB"

    invoke-static {v12, v13}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 303
    new-instance v12, Ljava/lang/AssertionError;

    const-string v13, "foodInfoItem is not found in DB"

    invoke-direct {v12, v13}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v12

    .line 339
    .restart local v1    # "amount":F
    .restart local v2    # "carbos":F
    .restart local v4    # "extendedFoodInfo":Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;
    .restart local v5    # "extraFoodInfoDao":Lcom/sec/android/app/shealth/food/fooddao/ExtraFoodInfoDao;
    .restart local v6    # "fat":F
    .restart local v7    # "foodInfoId":J
    .restart local v10    # "kCal":F
    .restart local v11    # "proteins":F
    :cond_7
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getkCal()F

    move-result v12

    const/4 v13, 0x0

    cmpl-float v12, v12, v13

    if-lez v12, :cond_8

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getProtein()F

    move-result v12

    mul-float/2addr v12, v10

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getkCal()F

    move-result v13

    div-float/2addr v12, v13

    :goto_2
    invoke-static {v12}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->convertToSingleDecimal(F)F

    move-result v11

    .line 340
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getkCal()F

    move-result v12

    const/4 v13, 0x0

    cmpl-float v12, v12, v13

    if-lez v12, :cond_9

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getCarbohydrate()F

    move-result v12

    mul-float/2addr v12, v10

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getkCal()F

    move-result v13

    div-float/2addr v12, v13

    :goto_3
    invoke-static {v12}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->convertToSingleDecimal(F)F

    move-result v2

    .line 341
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getkCal()F

    move-result v12

    const/4 v13, 0x0

    cmpl-float v12, v12, v13

    if-lez v12, :cond_a

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getFat()F

    move-result v12

    mul-float/2addr v12, v10

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getkCal()F

    move-result v13

    div-float/2addr v12, v13

    :goto_4
    invoke-static {v12}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->convertToSingleDecimal(F)F

    move-result v6

    goto :goto_0

    .line 339
    :cond_8
    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getProtein()F

    move-result v12

    goto :goto_2

    .line 340
    :cond_9
    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getCarbohydrate()F

    move-result v12

    goto :goto_3

    .line 341
    :cond_a
    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getFat()F

    move-result v12

    goto :goto_4

    .line 347
    :cond_b
    if-eqz v4, :cond_17

    .line 349
    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getDefaultNumber()F

    move-result v3

    .line 350
    .local v3, "defaultMetricNumber":F
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->getUnit()I

    move-result v12

    packed-switch v12, :pswitch_data_0

    .line 375
    new-instance v12, Ljava/lang/AssertionError;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "if meal item has extended info, its unit should be one of "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-class v14, Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$MealItemUnit;

    invoke-virtual {v14}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v12

    .line 353
    :pswitch_0
    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getOzInKcal()F

    move-result v12

    const/4 v13, 0x0

    cmpl-float v12, v12, v13

    if-lez v12, :cond_c

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getOzInKcal()F

    move-result v12

    div-float v10, v1, v12

    .line 354
    :goto_5
    const/4 v12, 0x0

    cmpl-float v12, v3, v12

    if-lez v12, :cond_d

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getProtein()F

    move-result v12

    mul-float/2addr v12, v1

    div-float/2addr v12, v3

    :goto_6
    invoke-static {v12}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->convertToSingleDecimal(F)F

    move-result v11

    .line 355
    const/4 v12, 0x0

    cmpl-float v12, v3, v12

    if-lez v12, :cond_e

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getCarbohydrate()F

    move-result v12

    mul-float/2addr v12, v1

    div-float/2addr v12, v3

    :goto_7
    invoke-static {v12}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->convertToSingleDecimal(F)F

    move-result v2

    .line 356
    const/4 v12, 0x0

    cmpl-float v12, v3, v12

    if-lez v12, :cond_f

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getFat()F

    move-result v12

    mul-float/2addr v12, v1

    div-float/2addr v12, v3

    :goto_8
    invoke-static {v12}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->convertToSingleDecimal(F)F

    move-result v6

    .line 358
    new-instance v12, Lcom/sec/android/app/shealth/food/utils/FoodItemCaloriesAndNutrients;

    invoke-static {v10}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->convertFloatKcalValueToInt(F)I

    move-result v13

    invoke-direct {v12, v13, v11, v2, v6}, Lcom/sec/android/app/shealth/food/utils/FoodItemCaloriesAndNutrients;-><init>(IFFF)V

    goto/16 :goto_1

    .line 353
    :cond_c
    const/4 v10, 0x0

    goto :goto_5

    .line 354
    :cond_d
    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getProtein()F

    move-result v12

    goto :goto_6

    .line 355
    :cond_e
    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getCarbohydrate()F

    move-result v12

    goto :goto_7

    .line 356
    :cond_f
    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getFat()F

    move-result v12

    goto :goto_8

    .line 360
    :pswitch_1
    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getGrammInKcal()F

    move-result v12

    const/4 v13, 0x0

    cmpl-float v12, v12, v13

    if-lez v12, :cond_10

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getGrammInKcal()F

    move-result v12

    div-float v10, v1, v12

    .line 361
    :goto_9
    const/4 v12, 0x0

    cmpl-float v12, v3, v12

    if-lez v12, :cond_11

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getProtein()F

    move-result v12

    mul-float/2addr v12, v1

    div-float/2addr v12, v3

    :goto_a
    invoke-static {v12}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->convertToSingleDecimal(F)F

    move-result v11

    .line 362
    const/4 v12, 0x0

    cmpl-float v12, v3, v12

    if-lez v12, :cond_12

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getCarbohydrate()F

    move-result v12

    mul-float/2addr v12, v1

    div-float/2addr v12, v3

    :goto_b
    invoke-static {v12}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->convertToSingleDecimal(F)F

    move-result v2

    .line 363
    const/4 v12, 0x0

    cmpl-float v12, v3, v12

    if-lez v12, :cond_13

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getFat()F

    move-result v12

    mul-float/2addr v12, v1

    div-float/2addr v12, v3

    :goto_c
    invoke-static {v12}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->convertToSingleDecimal(F)F

    move-result v6

    .line 365
    new-instance v12, Lcom/sec/android/app/shealth/food/utils/FoodItemCaloriesAndNutrients;

    invoke-static {v10}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->convertFloatKcalValueToInt(F)I

    move-result v13

    invoke-direct {v12, v13, v11, v2, v6}, Lcom/sec/android/app/shealth/food/utils/FoodItemCaloriesAndNutrients;-><init>(IFFF)V

    goto/16 :goto_1

    .line 360
    :cond_10
    const/4 v10, 0x0

    goto :goto_9

    .line 361
    :cond_11
    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getProtein()F

    move-result v12

    goto :goto_a

    .line 362
    :cond_12
    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getCarbohydrate()F

    move-result v12

    goto :goto_b

    .line 363
    :cond_13
    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getFat()F

    move-result v12

    goto :goto_c

    .line 367
    :pswitch_2
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getkCal()F

    move-result v9

    .line 368
    .local v9, "foodInfoKCal":F
    const/4 v12, 0x0

    cmpl-float v12, v9, v12

    if-lez v12, :cond_14

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getProtein()F

    move-result v12

    mul-float/2addr v12, v10

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getkCal()F

    move-result v13

    div-float/2addr v12, v13

    :goto_d
    invoke-static {v12}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->convertToSingleDecimal(F)F

    move-result v11

    .line 369
    const/4 v12, 0x0

    cmpl-float v12, v9, v12

    if-lez v12, :cond_15

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getCarbohydrate()F

    move-result v12

    mul-float/2addr v12, v10

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getkCal()F

    move-result v13

    div-float/2addr v12, v13

    :goto_e
    invoke-static {v12}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->convertToSingleDecimal(F)F

    move-result v2

    .line 370
    const/4 v12, 0x0

    cmpl-float v12, v9, v12

    if-lez v12, :cond_16

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getFat()F

    move-result v12

    mul-float/2addr v12, v10

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getkCal()F

    move-result v13

    div-float/2addr v12, v13

    :goto_f
    invoke-static {v12}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->convertToSingleDecimal(F)F

    move-result v6

    .line 372
    new-instance v12, Lcom/sec/android/app/shealth/food/utils/FoodItemCaloriesAndNutrients;

    invoke-static {v10}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->convertFloatKcalValueToInt(F)I

    move-result v13

    invoke-direct {v12, v13, v11, v2, v6}, Lcom/sec/android/app/shealth/food/utils/FoodItemCaloriesAndNutrients;-><init>(IFFF)V

    goto/16 :goto_1

    .line 368
    :cond_14
    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getProtein()F

    move-result v12

    goto :goto_d

    .line 369
    :cond_15
    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getCarbohydrate()F

    move-result v12

    goto :goto_e

    .line 370
    :cond_16
    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getFat()F

    move-result v12

    goto :goto_f

    .line 380
    .end local v3    # "defaultMetricNumber":F
    .end local v9    # "foodInfoKCal":F
    :cond_17
    new-instance v12, Ljava/lang/AssertionError;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "if meal item has extended info, its unit should be one of "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-class v14, Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$MealItemUnit;

    invoke-virtual {v14}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v12

    .line 350
    nop

    :pswitch_data_0
    .packed-switch 0x1d4c2
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static getMealTypeString(Landroid/content/Context;I)Ljava/lang/String;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "mealType"    # I

    .prologue
    .line 392
    invoke-static {p1}, Lcom/sec/android/app/shealth/food/constants/MealType;->getMealTypeByMealTypeId(I)Lcom/sec/android/app/shealth/food/constants/MealType;

    move-result-object v0

    iget v0, v0, Lcom/sec/android/app/shealth/food/constants/MealType;->textResourceId:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getMyFoods(Landroid/content/Context;)Ljava/util/List;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 606
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 607
    .local v4, "myFoods":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;>;"
    new-instance v2, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;-><init>(Landroid/content/Context;)V

    .line 608
    .local v2, "foodInfoDao":Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;
    invoke-interface {v2}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;->getAllDatas()Ljava/util/List;

    move-result-object v0

    .line 609
    .local v0, "allFoods":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    .line 610
    .local v1, "foodData":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getServerSourceType()I

    move-result v5

    const v6, 0x46cd1

    if-ne v5, v6, :cond_0

    .line 611
    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 614
    .end local v1    # "foodData":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    :cond_1
    new-instance v5, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils$1;

    invoke-direct {v5}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils$1;-><init>()V

    invoke-static {v4, v5}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 620
    return-object v4
.end method

.method public static getPathToImage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 117
    sget-object v0, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->GALLERY_PATH:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->getImageFullPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getRawDefaultIntakeCalorieGoal()F
    .locals 1

    .prologue
    .line 502
    invoke-static {}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->netCalorieValue()I

    move-result v0

    int-to-float v0, v0

    return v0
.end method

.method public static isAbnormalCase(JLcom/sec/android/app/shealth/food/fooddao/MealItemDao;Lcom/sec/android/app/shealth/food/fooddao/MealImageDao;)Z
    .locals 1
    .param p0, "mealId"    # J
    .param p2, "mealItemDao"    # Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;
    .param p3, "mealImageDao"    # Lcom/sec/android/app/shealth/food/fooddao/MealImageDao;

    .prologue
    .line 878
    invoke-static {p0, p1, p2}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->isMealItemHasNoFoodData(JLcom/sec/android/app/shealth/food/fooddao/MealItemDao;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0, p1, p3}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->isMealImageNotAvailable(JLcom/sec/android/app/shealth/food/fooddao/MealImageDao;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isCalorieIntakeGoalAchieved(F)Z
    .locals 3
    .param p0, "intakeToGoalCalorieRatio"    # F

    .prologue
    .line 533
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/Utils$BoundaryChecker;

    invoke-static {p0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/common/utils/Utils$BoundaryChecker;-><init>(Ljava/lang/Comparable;)V

    const v1, 0x3f666666    # 0.9f

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    const v2, 0x3f8ccccd    # 1.1f

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/Utils$BoundaryChecker;->isValueInBounds(Ljava/lang/Comparable;Ljava/lang/Comparable;)Z

    move-result v0

    return v0
.end method

.method public static isCalorieIntakeGoalExceeded(F)Z
    .locals 2
    .param p0, "intakeToGoalCalorieRatio"    # F

    .prologue
    .line 545
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/Utils$BoundaryChecker;

    invoke-static {p0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/common/utils/Utils$BoundaryChecker;-><init>(Ljava/lang/Comparable;)V

    const v1, 0x3f8ccccd    # 1.1f

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/utils/Utils$BoundaryChecker;->isValueLargerThan(Ljava/lang/Comparable;)Z

    move-result v0

    return v0
.end method

.method public static isCaloriesIntakeGoalAchieved(FF)Z
    .locals 1
    .param p0, "actualCalAmount"    # F
    .param p1, "goalCalAmount"    # F

    .prologue
    .line 522
    div-float v0, p0, p1

    invoke-static {v0}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->isCalorieIntakeGoalAchieved(F)Z

    move-result v0

    return v0
.end method

.method public static isClearDataActionFromHealthService(Landroid/content/Intent;)Z
    .locals 2
    .param p0, "intent"    # Landroid/content/Intent;

    .prologue
    .line 761
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.PACKAGE_DATA_CLEARED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 762
    sget-object v0, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->URI_SHEALTH_SERVICE_PACKAGE:Landroid/net/Uri;

    invoke-virtual {p0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 763
    const/4 v0, 0x1

    .line 766
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isDataAvailable()Z
    .locals 7

    .prologue
    .line 975
    const/4 v6, 0x0

    .line 978
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Meal;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 979
    if-eqz v6, :cond_1

    .line 981
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 986
    if-eqz v6, :cond_0

    .line 987
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 989
    :cond_0
    :goto_0
    return v0

    .line 986
    :cond_1
    if-eqz v6, :cond_2

    .line 987
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 989
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 986
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    .line 987
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method public static isDependentVowel(C)Z
    .locals 2
    .param p0, "primaryCodes"    # C

    .prologue
    const/4 v0, 0x1

    .line 997
    const/16 v1, 0x93e

    if-lt p0, v1, :cond_0

    const/16 v1, 0x944

    if-le p0, v1, :cond_3

    :cond_0
    const/16 v1, 0x946

    if-lt p0, v1, :cond_1

    const/16 v1, 0x94c

    if-le p0, v1, :cond_3

    :cond_1
    const/16 v1, 0x962

    if-lt p0, v1, :cond_2

    const/16 v1, 0x963

    if-le p0, v1, :cond_3

    :cond_2
    const/16 v1, 0x951

    if-lt p0, v1, :cond_4

    const/16 v1, 0x954

    if-gt p0, v1, :cond_4

    .line 1060
    :cond_3
    :goto_0
    return v0

    .line 1005
    :cond_4
    const/16 v1, 0x9be

    if-lt p0, v1, :cond_5

    const/16 v1, 0x9cc

    if-le p0, v1, :cond_3

    :cond_5
    const/16 v1, 0x9e2

    if-lt p0, v1, :cond_6

    const/16 v1, 0x9e3

    if-le p0, v1, :cond_3

    :cond_6
    const/16 v1, 0x9d7

    if-eq p0, v1, :cond_3

    .line 1011
    const/16 v1, 0xa3e

    if-lt p0, v1, :cond_7

    const/16 v1, 0xa4c

    if-le p0, v1, :cond_3

    .line 1016
    :cond_7
    const/16 v1, 0xabe

    if-lt p0, v1, :cond_8

    const/16 v1, 0xacc

    if-le p0, v1, :cond_3

    :cond_8
    const/16 v1, 0xae2

    if-lt p0, v1, :cond_9

    const/16 v1, 0xae3

    if-le p0, v1, :cond_3

    .line 1022
    :cond_9
    const/16 v1, 0xbbe

    if-lt p0, v1, :cond_a

    const/16 v1, 0xbcc

    if-le p0, v1, :cond_3

    :cond_a
    const/16 v1, 0xbd7

    if-eq p0, v1, :cond_3

    .line 1027
    const/16 v1, 0xc3e

    if-lt p0, v1, :cond_b

    const/16 v1, 0xc4c

    if-le p0, v1, :cond_3

    :cond_b
    const/16 v1, 0xc62

    if-lt p0, v1, :cond_c

    const/16 v1, 0xc63

    if-le p0, v1, :cond_3

    :cond_c
    const/16 v1, 0xc55

    if-lt p0, v1, :cond_d

    const/16 v1, 0xc56

    if-le p0, v1, :cond_3

    .line 1034
    :cond_d
    const/16 v1, 0xcbe

    if-lt p0, v1, :cond_e

    const/16 v1, 0xccc

    if-le p0, v1, :cond_3

    :cond_e
    const/16 v1, 0xcd5

    if-lt p0, v1, :cond_f

    const/16 v1, 0xcd6

    if-le p0, v1, :cond_3

    :cond_f
    const/16 v1, 0xce2

    if-lt p0, v1, :cond_10

    const/16 v1, 0xce3

    if-le p0, v1, :cond_3

    .line 1041
    :cond_10
    const/16 v1, 0xd3e

    if-lt p0, v1, :cond_11

    const/16 v1, 0xd4c

    if-le p0, v1, :cond_3

    :cond_11
    const/16 v1, 0xd62

    if-lt p0, v1, :cond_12

    const/16 v1, 0xd63

    if-le p0, v1, :cond_3

    :cond_12
    const/16 v1, 0xd57

    if-eq p0, v1, :cond_3

    .line 1047
    const/16 v1, 0xdca

    if-lt p0, v1, :cond_13

    const/16 v1, 0xdde

    if-le p0, v1, :cond_3

    :cond_13
    const/16 v1, 0xdf2

    if-lt p0, v1, :cond_14

    const/16 v1, 0xdf3

    if-le p0, v1, :cond_3

    .line 1053
    :cond_14
    const/16 v1, 0xb3e

    if-lt p0, v1, :cond_15

    const/16 v1, 0xb48

    if-le p0, v1, :cond_3

    :cond_15
    const/16 v1, 0xb4b

    if-lt p0, v1, :cond_16

    const/16 v1, 0xb4c

    if-le p0, v1, :cond_3

    :cond_16
    const/16 v1, 0xb62

    if-lt p0, v1, :cond_17

    const/16 v1, 0xb63

    if-le p0, v1, :cond_3

    :cond_17
    const/16 v1, 0xb56

    if-lt p0, v1, :cond_18

    const/16 v1, 0xb57

    if-le p0, v1, :cond_3

    .line 1060
    :cond_18
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method private static isExtraDataRefreshRequired(Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;Landroid/content/Context;)Z
    .locals 7
    .param p0, "foodInfoData"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 817
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    iget-object v5, v5, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v5}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    .line 818
    .local v0, "deviceLanguage":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getServerLocale()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    .line 819
    .local v2, "isLanguageSame":Z
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getServerSourceType()I

    move-result v5

    invoke-static {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchFactory;->getSearchApi(Landroid/content/Context;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;

    move-result-object v6

    invoke-interface {v6}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;->getServerSourceType()I

    move-result v6

    if-ne v5, v6, :cond_0

    move v1, v3

    .line 821
    .local v1, "isFoodProviderSame":Z
    :goto_0
    if-nez v2, :cond_1

    if-eqz v1, :cond_1

    :goto_1
    return v3

    .end local v1    # "isFoodProviderSame":Z
    :cond_0
    move v1, v4

    .line 819
    goto :goto_0

    .restart local v1    # "isFoodProviderSame":Z
    :cond_1
    move v3, v4

    .line 821
    goto :goto_1
.end method

.method public static isExtraFoodInfoDataDownloadRequired(Landroid/content/Context;Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "foodInfoData"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    .prologue
    .line 811
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->isAddedByUser()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/sec/android/app/shealth/food/fooddao/ExtraFoodInfoDaoImplDb;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/food/fooddao/ExtraFoodInfoDaoImplDb;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/food/fooddao/ExtraFoodInfoDaoImplDb;->getExtraByFoodId(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isFavoriteItemsLimitExceed(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 595
    invoke-static {p0}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->getFavoritesFoods(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/16 v1, 0x1e

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isHalant(I)Z
    .locals 1
    .param p0, "primaryCodes"    # I

    .prologue
    .line 1074
    const/16 v0, 0x94d

    if-eq p0, v0, :cond_0

    const/16 v0, 0x9cd

    if-eq p0, v0, :cond_0

    const/16 v0, 0xa4d

    if-eq p0, v0, :cond_0

    const/16 v0, 0xacd

    if-eq p0, v0, :cond_0

    const/16 v0, 0xbcd

    if-eq p0, v0, :cond_0

    const/16 v0, 0xc4d

    if-eq p0, v0, :cond_0

    const/16 v0, 0xccd

    if-eq p0, v0, :cond_0

    const/16 v0, 0xd4d

    if-eq p0, v0, :cond_0

    const/16 v0, 0xddf

    if-eq p0, v0, :cond_0

    const/16 v0, 0xb4d

    if-ne p0, v0, :cond_1

    .line 1078
    :cond_0
    const/4 v0, 0x1

    .line 1080
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isMealImageNotAvailable(JLcom/sec/android/app/shealth/food/fooddao/MealImageDao;)Z
    .locals 1
    .param p0, "mealId"    # J
    .param p2, "mealImageDao"    # Lcom/sec/android/app/shealth/food/fooddao/MealImageDao;

    .prologue
    .line 889
    invoke-interface {p2, p0, p1}, Lcom/sec/android/app/shealth/food/fooddao/MealImageDao;->getImagesListByMealId(J)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p2, p0, p1}, Lcom/sec/android/app/shealth/food/fooddao/MealImageDao;->getImagesListByMealId(J)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isMealItemHasNoFoodData(JLcom/sec/android/app/shealth/food/fooddao/MealItemDao;)Z
    .locals 1
    .param p0, "mealId"    # J
    .param p2, "mealItemDao"    # Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;

    .prologue
    .line 884
    invoke-interface {p2, p0, p1}, Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;->getMealItemsListByMealId(J)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p2, p0, p1}, Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;->getMealItemsListByMealId(J)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isMealSkipped(JLcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;)Z
    .locals 7
    .param p0, "mealId"    # J
    .param p2, "foodInfoDao"    # Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;
    .param p3, "mealItemDao"    # Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;

    .prologue
    .line 833
    invoke-interface {p3, p0, p1}, Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;->getMealItemsListByMealId(J)Ljava/util/List;

    move-result-object v4

    .line 834
    .local v4, "mealItems":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;>;"
    invoke-interface {p2, p0, p1}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;->getFoodInfoListByMealId(J)Ljava/util/Set;

    move-result-object v1

    .line 835
    .local v1, "foodInfoDatas":Ljava/util/Set;, "Ljava/util/Set<Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;>;"
    const/4 v3, 0x0

    .line 836
    .local v3, "isMealSkipped":Z
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_1

    .line 837
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    .line 839
    .local v0, "foodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    const v5, 0x46cd7

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getServerSourceType()I

    move-result v6

    if-ne v5, v6, :cond_0

    .line 842
    const/4 v3, 0x1

    goto :goto_0

    .line 850
    .end local v0    # "foodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    .end local v2    # "i$":Ljava/util/Iterator;
    :cond_1
    return v3
.end method

.method public static isMealSkippedWithFood(JLcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;Ljava/util/List;)Z
    .locals 6
    .param p0, "mealId"    # J
    .param p2, "foodInfoDao"    # Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 855
    .local p3, "mealItems":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;>;"
    invoke-interface {p2, p0, p1}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;->getFoodInfoListByMealId(J)Ljava/util/Set;

    move-result-object v1

    .line 856
    .local v1, "foodInfoDatas":Ljava/util/Set;, "Ljava/util/Set<Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;>;"
    const/4 v3, 0x0

    .line 857
    .local v3, "isMealSkipped":Z
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v4

    const/4 v5, 0x1

    if-le v4, v5, :cond_1

    .line 858
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    .line 860
    .local v0, "foodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    const v4, 0x46cd7

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getServerSourceType()I

    move-result v5

    if-ne v4, v5, :cond_0

    .line 863
    const/4 v3, 0x1

    goto :goto_0

    .line 871
    .end local v0    # "foodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    .end local v2    # "i$":Ljava/util/Iterator;
    :cond_1
    return v3
.end method

.method public static isMyFoodItemsLimitExceed(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 631
    invoke-static {p0}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->getMyFoods(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/16 v1, 0x1e

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isNuktaSymbol(I)Z
    .locals 1
    .param p0, "primaryCodes"    # I

    .prologue
    .line 1084
    const/16 v0, 0x93c

    if-eq p0, v0, :cond_0

    const/16 v0, 0x9bc

    if-eq p0, v0, :cond_0

    const/16 v0, 0xa3c

    if-eq p0, v0, :cond_0

    const/16 v0, 0xabc

    if-eq p0, v0, :cond_0

    const/16 v0, 0xcbc

    if-eq p0, v0, :cond_0

    const/16 v0, 0xb3c

    if-ne p0, v0, :cond_1

    .line 1086
    :cond_0
    const/4 v0, 0x1

    .line 1088
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isQuickInputType(I)Z
    .locals 1
    .param p0, "serverSourceId"    # I

    .prologue
    .line 901
    const v0, 0x46cd7

    if-eq p0, v0, :cond_0

    const v0, 0x46cd6

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isQuickInputType(Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;)Z
    .locals 1
    .param p0, "foodInfoData"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    .prologue
    .line 911
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getServerSourceType()I

    move-result v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->isQuickInputType(I)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isSpecialMatra(I)Z
    .locals 1
    .param p0, "primaryCodes"    # I

    .prologue
    .line 1064
    packed-switch p0, :pswitch_data_0

    .line 1068
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 1066
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 1064
    nop

    :pswitch_data_0
    .packed-switch 0x985
        :pswitch_0
    .end packed-switch
.end method

.method public static isSpecialVowel(I)Z
    .locals 2
    .param p0, "primaryCodes"    # I

    .prologue
    const/4 v0, 0x1

    .line 1094
    const/16 v1, 0x901

    if-lt p0, v1, :cond_0

    const/16 v1, 0x903

    if-le p0, v1, :cond_1

    :cond_0
    const/16 v1, 0x945

    if-ne p0, v1, :cond_2

    .line 1144
    :cond_1
    :goto_0
    return v0

    .line 1099
    :cond_2
    const/16 v1, 0x981

    if-lt p0, v1, :cond_3

    const/16 v1, 0x983

    if-le p0, v1, :cond_1

    .line 1104
    :cond_3
    const/16 v1, 0xa01

    if-lt p0, v1, :cond_4

    const/16 v1, 0xa03

    if-le p0, v1, :cond_1

    :cond_4
    const/16 v1, 0xa70

    if-lt p0, v1, :cond_5

    const/16 v1, 0xa71

    if-le p0, v1, :cond_1

    .line 1110
    :cond_5
    const/16 v1, 0xa81

    if-lt p0, v1, :cond_6

    const/16 v1, 0xa83

    if-le p0, v1, :cond_1

    .line 1115
    :cond_6
    const/16 v1, 0xb82

    if-eq p0, v1, :cond_1

    .line 1120
    const/16 v1, 0xc01

    if-lt p0, v1, :cond_7

    const/16 v1, 0xc03

    if-le p0, v1, :cond_1

    .line 1125
    :cond_7
    const/16 v1, 0xc82

    if-lt p0, v1, :cond_8

    const/16 v1, 0xc83

    if-le p0, v1, :cond_1

    .line 1130
    :cond_8
    const/16 v1, 0xd02

    if-lt p0, v1, :cond_9

    const/16 v1, 0xd03

    if-le p0, v1, :cond_1

    .line 1135
    :cond_9
    const/16 v1, 0xd82

    if-lt p0, v1, :cond_a

    const/16 v1, 0xd83

    if-le p0, v1, :cond_1

    .line 1140
    :cond_a
    const/16 v1, 0xb01

    if-lt p0, v1, :cond_b

    const/16 v1, 0xb03

    if-le p0, v1, :cond_1

    .line 1144
    :cond_b
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static prepareTextForOutOfRangePouup(Landroid/content/Context;F)Ljava/lang/String;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "maxAcceptableCaloriesValue"    # F

    .prologue
    .line 778
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0900b0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0900b1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : 0 ~ "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0900b9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static refreshLastTakenPhoto(Landroid/content/Context;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 685
    new-instance v1, Ljava/io/File;

    sget-object v3, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->GALLERY_PATH:Ljava/lang/String;

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 686
    .local v1, "folder":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v0

    .line 687
    .local v0, "allFiles":[Ljava/lang/String;
    if-eqz v0, :cond_0

    array-length v3, v0

    if-lez v3, :cond_0

    .line 688
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->GALLERY_PATH:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    array-length v4, v0

    add-int/lit8 v4, v4, -0x1

    aget-object v4, v0, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 689
    .local v2, "scanPath":Ljava/lang/String;
    new-instance v3, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils$2;

    invoke-direct {v3, p0, v2}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils$2;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 710
    .end local v2    # "scanPath":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public static saveToMyFood(Landroid/content/Context;Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "foodInfoData"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    .prologue
    .line 652
    new-instance v0, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;-><init>(Landroid/content/Context;)V

    .line 653
    .local v0, "foodInfoDao":Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;
    const v1, 0x46cd1

    invoke-virtual {p1, v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setServerSourceType(I)V

    .line 654
    invoke-static {p1, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/DataBaseUpdater;->insertOrUpdateData(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;Lcom/sec/android/app/shealth/common/commondao/CommonDao;)V

    .line 655
    return-void
.end method

.method public static showSoftInputKeypad(Landroid/content/Context;Landroid/view/View;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "focusedView"    # Landroid/view/View;

    .prologue
    .line 721
    invoke-static {p0}, Lcom/sec/android/app/shealth/common/utils/SoftInputUtils;->isHardKeyBoardPresent(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    instance-of v0, p1, Landroid/widget/EditText;

    if-eqz v0, :cond_0

    .line 723
    new-instance v0, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils$FocusRunnable;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils$FocusRunnable;-><init>(Landroid/content/Context;Landroid/view/View;)V

    const-wide/16 v1, 0xc8

    invoke-virtual {p1, v0, v1, v2}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 727
    :cond_0
    return-void
.end method

.method public static showToastAboutMyFoodItemsLimitExceeded(Landroid/content/Context;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 640
    const v1, 0x7f090927

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/16 v3, 0x1e

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 641
    .local v0, "message":Ljava/lang/String;
    invoke-static {p0, v0, v4}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 642
    return-void
.end method
