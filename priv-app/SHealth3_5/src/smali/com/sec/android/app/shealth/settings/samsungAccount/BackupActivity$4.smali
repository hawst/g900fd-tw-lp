.class Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$4;
.super Ljava/lang/Object;
.source "BackupActivity.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->startBackup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;)V
    .locals 0

    .prologue
    .line 219
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFinished(II)V
    .locals 10
    .param p1, "dataSyncType"    # I
    .param p2, "error"    # I

    .prologue
    const/4 v9, 0x0

    const v8, 0x7f090d15

    const v7, 0x7f0907fc

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 284
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->TAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->access$300(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Backup Completed. Type: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ". Error: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 286
    const/4 v0, 0x0

    .line 288
    .local v0, "cignaStatus":Z
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    const v3, 0x7f080089

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    invoke-virtual {v2, v5}, Landroid/widget/Button;->setClickable(Z)V

    .line 290
    if-nez p2, :cond_3

    .line 291
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mCignaBackupRestoreEnable:Z
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->access$1100(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 292
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->cignaBackup()Z

    move-result v0

    .line 348
    :goto_0
    return-void

    .line 294
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mBackupProgressBar:Landroid/widget/ProgressBar;
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->access$400(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;)Landroid/widget/ProgressBar;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mTextPercentage:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->access$500(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;)Landroid/widget/TextView;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 295
    const/16 v1, 0x64

    .line 296
    .local v1, "percentage":I
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mBackupProgressBar:Landroid/widget/ProgressBar;
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->access$400(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;)Landroid/widget/ProgressBar;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 297
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mTextPercentage:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->access$500(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;)Landroid/widget/TextView;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    invoke-virtual {v4, v8}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 300
    .end local v1    # "percentage":I
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mBackupProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->access$600(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mBackupProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->access$600(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isAdded()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 301
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mBackupProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->access$600(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->dismissAllowingStateLoss()V

    .line 302
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mBackupProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v2, v9}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->access$602(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 305
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    invoke-virtual {v3, v7}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v6}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 310
    :cond_3
    const/16 v2, 0x11

    if-ne p2, v2, :cond_6

    .line 313
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mBackupProgressBar:Landroid/widget/ProgressBar;
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->access$400(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;)Landroid/widget/ProgressBar;

    move-result-object v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mTextPercentage:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->access$500(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;)Landroid/widget/TextView;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 314
    const/16 v1, 0x64

    .line 315
    .restart local v1    # "percentage":I
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mBackupProgressBar:Landroid/widget/ProgressBar;
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->access$400(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;)Landroid/widget/ProgressBar;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 316
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mTextPercentage:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->access$500(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;)Landroid/widget/TextView;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    invoke-virtual {v4, v8}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 319
    .end local v1    # "percentage":I
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mBackupProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->access$600(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mBackupProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->access$600(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isAdded()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 320
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mBackupProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->access$600(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->dismissAllowingStateLoss()V

    .line 321
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mBackupProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v2, v9}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->access$602(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 324
    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    invoke-virtual {v3, v7}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v6}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 326
    :cond_6
    const/16 v2, 0xb

    if-ne p2, v2, :cond_7

    .line 328
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->stopBackup()V

    .line 329
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    invoke-static {v2}, Lcom/sec/android/app/shealth/common/utils/SamsungAccount;->requestCheckValication(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 333
    :cond_7
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->TAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->access$300(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "3.0 Backup error received : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 334
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->stopBackup()V

    .line 336
    const/16 v2, 0x10

    if-ne p2, v2, :cond_8

    .line 337
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->isDeviceStorageLow:Z
    invoke-static {v2, v5}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->access$702(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;Z)Z

    .line 338
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    sget-object v3, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$BackupStatus;->DEVICE_STORAGE_LOW:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$BackupStatus;

    # invokes: Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->showBackupPopup(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$BackupStatus;)V
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->access$100(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$BackupStatus;)V

    goto/16 :goto_0

    .line 340
    :cond_8
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mIsNetworkErrorOccured:Z
    invoke-static {v2, v5}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->access$1002(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;Z)Z

    .line 341
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    sget-object v3, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$BackupStatus;->FAIL_NETWORK_ERROR:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$BackupStatus;

    # invokes: Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->showBackupPopup(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$BackupStatus;)V
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->access$100(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$BackupStatus;)V

    goto/16 :goto_0
.end method

.method public onProgress(II)V
    .locals 5
    .param p1, "dataSyncType"    # I
    .param p2, "percent"    # I

    .prologue
    .line 271
    mul-int/lit8 v1, p2, 0x5f

    div-int/lit8 v0, v1, 0x64

    .line 272
    .local v0, "percentage":I
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mBackupProgressBar:Landroid/widget/ProgressBar;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->access$400(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;)Landroid/widget/ProgressBar;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 273
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mBackupProgressBar:Landroid/widget/ProgressBar;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->access$400(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;)Landroid/widget/ProgressBar;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 276
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mTextPercentage:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->access$500(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;)Landroid/widget/TextView;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 277
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mTextPercentage:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->access$500(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;)Landroid/widget/TextView;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    const v4, 0x7f090d15

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 280
    :cond_1
    return-void
.end method

.method public onStarted(IILjava/lang/String;)V
    .locals 6
    .param p1, "dataSyncType"    # I
    .param p2, "error"    # I
    .param p3, "message"    # Ljava/lang/String;

    .prologue
    const-wide/16 v4, 0xc8

    const/4 v3, 0x1

    .line 223
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->access$300(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Backup Started. Type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". Error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    if-nez p2, :cond_0

    .line 268
    :goto_0
    return-void

    .line 232
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    const v1, 0x7f080089

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setClickable(Z)V

    .line 234
    const/16 v0, 0x10

    if-ne p2, v0, :cond_1

    .line 235
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->isDeviceStorageLow:Z
    invoke-static {v0, v3}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->access$702(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;Z)Z

    .line 236
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$4$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$4$1;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$4;)V

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 244
    :cond_1
    const/16 v0, 0x12

    if-ne p2, v0, :cond_2

    .line 245
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->isServerUnderMaintenance:Z
    invoke-static {v0, v3}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->access$802(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;Z)Z

    .line 246
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->SERVER_ERROR_MESSAGE:Ljava/lang/String;
    invoke-static {v0, p3}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->access$902(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 247
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$4$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$4$2;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$4;)V

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 255
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mIsNetworkErrorOccured:Z
    invoke-static {v0, v3}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->access$1002(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;Z)Z

    .line 256
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$4$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$4$3;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$4;)V

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
