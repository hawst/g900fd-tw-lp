.class Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MyOnCheckedChildChangeListener;
.super Ljava/lang/Object;
.source "CignaMainExpandableAdapter.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MyOnCheckedChildChangeListener"
.end annotation


# instance fields
.field goalMissionDelete:Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$GoalMission;

.field private mChildPosition:I

.field private mGroupPosition:I

.field private mMissionData:Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;Lcom/sec/android/app/shealth/cignacoach/data/MissionData;II)V
    .locals 3
    .param p2, "missionData"    # Lcom/sec/android/app/shealth/cignacoach/data/MissionData;
    .param p3, "groupPosition"    # I
    .param p4, "childPosition"    # I

    .prologue
    .line 515
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MyOnCheckedChildChangeListener;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 520
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$GoalMission;

    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MyOnCheckedChildChangeListener;->mGroupPosition:I

    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MyOnCheckedChildChangeListener;->mChildPosition:I

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$GoalMission;-><init>(II)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MyOnCheckedChildChangeListener;->goalMissionDelete:Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$GoalMission;

    .line 516
    iput-object p2, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MyOnCheckedChildChangeListener;->mMissionData:Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    .line 517
    iput p3, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MyOnCheckedChildChangeListener;->mGroupPosition:I

    .line 518
    iput p4, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MyOnCheckedChildChangeListener;->mChildPosition:I

    .line 519
    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 3
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    .line 527
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MyOnCheckedChildChangeListener;->mMissionData:Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    invoke-virtual {v0, p2}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->setChecked(Z)V

    .line 529
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MyOnCheckedChildChangeListener;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mMainType:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->access$200(Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;)Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;->DELETE:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;

    if-ne v0, v1, :cond_0

    .line 531
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MyOnCheckedChildChangeListener;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mGoalMissionDelete:Ljava/util/HashSet;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->access$300(Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;)Ljava/util/HashSet;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 534
    if-eqz p2, :cond_2

    .line 536
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MyOnCheckedChildChangeListener;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mGoalMissionDelete:Ljava/util/HashSet;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->access$300(Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;)Ljava/util/HashSet;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MyOnCheckedChildChangeListener;->goalMissionDelete:Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$GoalMission;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 537
    const-string v0, "Mission Selection"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Selected Mission "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MyOnCheckedChildChangeListener;->mGroupPosition:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " , "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MyOnCheckedChildChangeListener;->mChildPosition:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 552
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MyOnCheckedChildChangeListener;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mCheckedCountListener:Lcom/sec/android/app/shealth/cignacoach/listener/CheckedCheckBoxCountListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->access$400(Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;)Lcom/sec/android/app/shealth/cignacoach/listener/CheckedCheckBoxCountListener;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 553
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MyOnCheckedChildChangeListener;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mCheckedCountListener:Lcom/sec/android/app/shealth/cignacoach/listener/CheckedCheckBoxCountListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->access$400(Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;)Lcom/sec/android/app/shealth/cignacoach/listener/CheckedCheckBoxCountListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MyOnCheckedChildChangeListener;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->getCheckedItemCount()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/listener/CheckedCheckBoxCountListener;->checkedCheckBoxCountChanged(I)V

    .line 555
    :cond_1
    return-void

    .line 540
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MyOnCheckedChildChangeListener;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mGoalMissionDelete:Ljava/util/HashSet;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->access$300(Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;)Ljava/util/HashSet;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MyOnCheckedChildChangeListener;->goalMissionDelete:Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$GoalMission;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 542
    const-string v0, "Mission Deselection"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Deselected Mission"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MyOnCheckedChildChangeListener;->mGroupPosition:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MyOnCheckedChildChangeListener;->mChildPosition:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 543
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MyOnCheckedChildChangeListener;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mGoalMissionDelete:Ljava/util/HashSet;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->access$300(Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;)Ljava/util/HashSet;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MyOnCheckedChildChangeListener;->goalMissionDelete:Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$GoalMission;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method
