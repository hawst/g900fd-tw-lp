.class public Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;
.super Landroid/widget/RelativeLayout;
.source "LibraryIndicatorViewPager.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager$OnDisableInterceptTouchEventListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final INDICATOR_RECTANGLE_HEIGHT:I

.field private final INDICATOR_RECTANGLE_TOP_MARGIN:I

.field private final INDICATOR_RECTANGLE_WIDTH:I

.field private isPagingEnabled:Z

.field private mOnDisableInceptTouchEventListener:Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager$OnDisableInterceptTouchEventListener;

.field private mOnPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

.field private mRectangleIndicatorContainer:Landroid/widget/LinearLayout;

.field private mTouchDownX:F

.field private mTouchSlop:I

.field private mViewPager:Landroid/support/v4/view/ViewPager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 39
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 24
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a02b8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;->INDICATOR_RECTANGLE_WIDTH:I

    .line 25
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a02b9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;->INDICATOR_RECTANGLE_HEIGHT:I

    .line 26
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a02b7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;->INDICATOR_RECTANGLE_TOP_MARGIN:I

    .line 40
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;->init()V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a02b8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;->INDICATOR_RECTANGLE_WIDTH:I

    .line 25
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a02b9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;->INDICATOR_RECTANGLE_HEIGHT:I

    .line 26
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a02b7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;->INDICATOR_RECTANGLE_TOP_MARGIN:I

    .line 45
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;->init()V

    .line 46
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;->mTouchSlop:I

    .line 47
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;

    .prologue
    .line 20
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;->isPagingEnabled:Z

    return v0
.end method

.method private getViewCount()I
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;->mViewPager:Landroid/support/v4/view/ViewPager;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/view/PagerAdapter;->getCount()I

    move-result v0

    goto :goto_0
.end method

.method private init()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 50
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;->isPagingEnabled:Z

    .line 52
    new-instance v1, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager$1;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager$1;-><init>(Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;->mViewPager:Landroid/support/v4/view/ViewPager;

    .line 72
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, p0}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 73
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;->mViewPager:Landroid/support/v4/view/ViewPager;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    .line 75
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 76
    .local v0, "lp":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 77
    return-void
.end method

.method private onPageSelectedRectangle(I)V
    .locals 4
    .param p1, "position"    # I

    .prologue
    .line 166
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;->mRectangleIndicatorContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    .line 167
    .local v1, "indicatorSize":I
    const/4 v0, 0x0

    .local v0, "index":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 168
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;->mRectangleIndicatorContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 169
    .local v2, "indicatorView":Landroid/widget/ImageView;
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 167
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 172
    .end local v2    # "indicatorView":Landroid/widget/ImageView;
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;->mRectangleIndicatorContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v3, p1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 173
    .restart local v2    # "indicatorView":Landroid/widget/ImageView;
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 174
    return-void
.end method

.method private setRectangleIndicator()V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 129
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;->getViewCount()I

    move-result v0

    .line 131
    .local v0, "dataLength":I
    const/4 v1, 0x0

    .local v1, "index":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 132
    new-instance v2, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v2, v4}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 133
    .local v2, "indicatorView":Landroid/widget/ImageView;
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    iget v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;->INDICATOR_RECTANGLE_WIDTH:I

    iget v6, p0, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;->INDICATOR_RECTANGLE_HEIGHT:I

    invoke-direct {v3, v4, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 134
    .local v3, "lp":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v3, v5, v5, v5, v5}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 135
    const v4, 0x7f0200c2

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 136
    if-nez v1, :cond_0

    const/4 v4, 0x1

    :goto_1
    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 137
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;->mRectangleIndicatorContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 131
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    move v4, v5

    .line 136
    goto :goto_1

    .line 139
    .end local v2    # "indicatorView":Landroid/widget/ImageView;
    .end local v3    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    :cond_1
    return-void
.end method

.method private setRectangleIndicatorContainer()V
    .locals 5

    .prologue
    const/4 v4, -0x2

    const/4 v3, 0x0

    .line 118
    new-instance v1, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;->mRectangleIndicatorContainer:Landroid/widget/LinearLayout;

    .line 119
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 121
    .local v0, "lp":Landroid/widget/RelativeLayout$LayoutParams;
    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;->INDICATOR_RECTANGLE_TOP_MARGIN:I

    invoke-virtual {v0, v3, v1, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 122
    const/16 v1, 0xe

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 123
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;->mRectangleIndicatorContainer:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 125
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;->mRectangleIndicatorContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->bringToFront()V

    .line 126
    return-void
.end method


# virtual methods
.method public getCurrentItem()I
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;->mViewPager:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    .line 94
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v0, 0x0

    .line 187
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;->isPagingEnabled:Z

    if-eqz v1, :cond_1

    .line 189
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 204
    :cond_0
    :goto_0
    :pswitch_0
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 207
    :cond_1
    return v0

    .line 191
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;->mTouchDownX:F

    .line 192
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;->mOnDisableInceptTouchEventListener:Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager$OnDisableInterceptTouchEventListener;

    if-eqz v1, :cond_0

    .line 193
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;->mOnDisableInceptTouchEventListener:Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager$OnDisableInterceptTouchEventListener;

    invoke-interface {v1, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager$OnDisableInterceptTouchEventListener;->disableInterceptTouchEvent(Z)V

    goto :goto_0

    .line 197
    :pswitch_2
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;->mTouchDownX:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;->mTouchSlop:I

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 198
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;->mOnDisableInceptTouchEventListener:Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager$OnDisableInterceptTouchEventListener;

    if-eqz v0, :cond_0

    .line 199
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;->mOnDisableInceptTouchEventListener:Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager$OnDisableInterceptTouchEventListener;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager$OnDisableInterceptTouchEventListener;->disableInterceptTouchEvent(Z)V

    goto :goto_0

    .line 189
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onPageScrollStateChanged(I)V
    .locals 1
    .param p1, "state"    # I

    .prologue
    .line 143
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;->mOnPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    if-eqz v0, :cond_0

    .line 144
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;->mOnPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    invoke-interface {v0, p1}, Landroid/support/v4/view/ViewPager$OnPageChangeListener;->onPageScrollStateChanged(I)V

    .line 146
    :cond_0
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 1
    .param p1, "position"    # I
    .param p2, "positionOffset"    # F
    .param p3, "positionOffsetPixels"    # I

    .prologue
    .line 150
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;->mOnPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    if-eqz v0, :cond_0

    .line 151
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;->mOnPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    invoke-interface {v0, p1, p2, p3}, Landroid/support/v4/view/ViewPager$OnPageChangeListener;->onPageScrolled(IFI)V

    .line 153
    :cond_0
    return-void
.end method

.method public onPageSelected(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 157
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;->onPageSelectedRectangle(I)V

    .line 158
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;->mOnPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    if-eqz v0, :cond_0

    .line 159
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;->mOnPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    invoke-interface {v0, p1}, Landroid/support/v4/view/ViewPager$OnPageChangeListener;->onPageSelected(I)V

    .line 161
    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 178
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;->isPagingEnabled:Z

    if-eqz v0, :cond_0

    .line 179
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 182
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setAdapter(Landroid/support/v4/view/PagerAdapter;)V
    .locals 1
    .param p1, "adapter"    # Landroid/support/v4/view/PagerAdapter;

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;->mViewPager:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p1}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 82
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;->setIndicator()V

    .line 84
    :cond_0
    return-void
.end method

.method public setIndicator()V
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v0

    if-nez v0, :cond_0

    .line 115
    :goto_0
    return-void

    .line 111
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;->mRectangleIndicatorContainer:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;->mRectangleIndicatorContainer:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;->removeView(Landroid/view/View;)V

    .line 113
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;->setRectangleIndicatorContainer()V

    .line 114
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;->setRectangleIndicator()V

    goto :goto_0
.end method

.method public setOnDisableInceptTouchEventListener(Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager$OnDisableInterceptTouchEventListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager$OnDisableInterceptTouchEventListener;

    .prologue
    .line 215
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;->mOnDisableInceptTouchEventListener:Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager$OnDisableInterceptTouchEventListener;

    .line 216
    return-void
.end method

.method public setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V
    .locals 0
    .param p1, "listener"    # Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    .prologue
    .line 87
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;->mOnPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    .line 88
    return-void
.end method

.method public setPagingEnabled(Z)V
    .locals 0
    .param p1, "isPagingEnabled"    # Z

    .prologue
    .line 211
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;->isPagingEnabled:Z

    .line 212
    return-void
.end method
