.class public Lcom/sec/android/app/shealth/logutils/graph/GraphInformationAreaView;
.super Lcom/sec/android/app/shealth/logutils/graph/InformationAreaViewInterLayer;
.source "GraphInformationAreaView.java"


# instance fields
.field private final mDateView:Landroid/widget/TextView;

.field private final mInformationItemContainer:Landroid/widget/LinearLayout;

.field private mIsInformationItemsTitleVisible:Z

.field private final mShouldSupportAverageCounting:Z

.field private mType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

.field private final mView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;ZLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "shouldSupportAverageCounting"    # Z
    .param p3, "periodType"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/logutils/graph/InformationAreaViewInterLayer;-><init>(Landroid/content/Context;)V

    .line 39
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationAreaView;->mIsInformationItemsTitleVisible:Z

    .line 50
    iput-boolean p2, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationAreaView;->mShouldSupportAverageCounting:Z

    .line 51
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationAreaView;->layoutInflater:Landroid/view/LayoutInflater;

    sget v1, Lcom/sec/android/app/shealth/logutils/R$layout;->graph_information_area:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationAreaView;->mView:Landroid/view/View;

    .line 52
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationAreaView;->mView:Landroid/view/View;

    sget v1, Lcom/sec/android/app/shealth/logutils/R$id;->graph_information_item_container:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationAreaView;->mInformationItemContainer:Landroid/widget/LinearLayout;

    .line 53
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationAreaView;->mView:Landroid/view/View;

    sget v1, Lcom/sec/android/app/shealth/logutils/R$id;->graph_information_area_date_value:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationAreaView;->mDateView:Landroid/widget/TextView;

    .line 54
    iput-object p3, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationAreaView;->mType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .line 55
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationAreaView;->mView:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationAreaView;->addView(Landroid/view/View;)V

    .line 56
    return-void
.end method

.method private setPaddingTop(I)V
    .locals 4
    .param p1, "value"    # I

    .prologue
    .line 126
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationAreaView;->mView:Landroid/view/View;

    iget-object v1, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationAreaView;->mView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getPaddingLeft()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationAreaView;->mView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getPaddingRight()I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationAreaView;->mView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getPaddingBottom()I

    move-result v3

    invoke-virtual {v0, v1, p1, v2, v3}, Landroid/view/View;->setPadding(IIII)V

    .line 127
    return-void
.end method

.method private updateInformationItem(Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;Lcom/samsung/android/sdk/chart/view/SchartHandlerData;)V
    .locals 3
    .param p1, "item"    # Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;
    .param p2, "data"    # Lcom/samsung/android/sdk/chart/view/SchartHandlerData;

    .prologue
    .line 174
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->getValuesCount()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 175
    invoke-virtual {p0, p2}, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationAreaView;->getSummarizedData(Lcom/samsung/android/sdk/chart/view/SchartHandlerData;)F

    move-result v0

    .line 176
    .local v0, "sum":F
    float-to-double v1, v0

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationAreaView;->isNotLessThanZero(D)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 177
    float-to-double v1, v0

    invoke-virtual {p1, v1, v2}, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->update(D)V

    .line 184
    .end local v0    # "sum":F
    :cond_0
    :goto_0
    return-void

    .line 179
    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->getValuesCount()I

    move-result v1

    invoke-virtual {p2}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesYValueList()Ljava/util/Vector;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    if-gt v1, v2, :cond_2

    .line 180
    invoke-virtual {p2}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesYValueList()Ljava/util/Vector;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->update(Ljava/util/List;)V

    goto :goto_0

    .line 182
    :cond_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Nothing to fill in information area"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public addInformationItem(Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;)V
    .locals 3
    .param p1, "item"    # Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;

    .prologue
    const/4 v2, -0x1

    .line 107
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationAreaView;->mIsInformationItemsTitleVisible:Z

    if-nez v1, :cond_0

    .line 108
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->hideTitle()V

    .line 111
    :cond_0
    const/16 v1, 0x8

    invoke-virtual {p1, v1}, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->setVisibility(I)V

    .line 113
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {v0, v2, v2, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 115
    .local v0, "layoutParams":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v1, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationAreaView;->mInformationItemContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1, p1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 116
    return-void
.end method

.method public dimInformationAreaView()V
    .locals 0

    .prologue
    .line 71
    return-void
.end method

.method public hideInformationItemsTitle()V
    .locals 4

    .prologue
    .line 91
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationAreaView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/sec/android/app/shealth/logutils/R$dimen;->graph_information_area_padding_top_single_item:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationAreaView;->setPaddingTop(I)V

    .line 93
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationAreaView;->mInformationItemContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 94
    iget-object v2, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationAreaView;->mInformationItemContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;

    .line 95
    .local v1, "item":Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->hideTitle()V

    .line 93
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 98
    .end local v1    # "item":Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;
    :cond_0
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationAreaView;->mIsInformationItemsTitleVisible:Z

    .line 99
    return-void
.end method

.method protected initInformationAreaView()Landroid/view/View;
    .locals 0

    .prologue
    .line 60
    return-object p0
.end method

.method public refreshInformationAreaView()V
    .locals 0

    .prologue
    .line 66
    return-void
.end method

.method public update(Ljava/util/ArrayList;Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;)V
    .locals 16
    .param p2, "handlerUpdateDataManager"    # Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/chart/view/SchartHandlerData;",
            ">;",
            "Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;",
            ")V"
        }
    .end annotation

    .prologue
    .line 132
    .local p1, "pointData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/chart/view/SchartHandlerData;>;"
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v14

    if-eqz v14, :cond_1

    .line 171
    :cond_0
    return-void

    .line 136
    :cond_1
    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;

    invoke-virtual {v14}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesXValue()Ljava/lang/String;

    move-result-object v3

    .line 137
    .local v3, "dateValue":Ljava/lang/String;
    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v12

    .line 138
    .local v12, "timeValue":J
    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v13}, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationAreaView;->setSelectedDateInChart(J)V

    .line 140
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationAreaView;->mType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v15, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v14, v15, :cond_2

    if-eqz p2, :cond_2

    .line 141
    move-object/from16 v0, p2

    invoke-interface {v0, v12, v13}, Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;->getLatestMeasureTimeInHour(J)J

    move-result-wide v12

    .line 143
    :cond_2
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2, v12, v13}, Ljava/util/Date;-><init>(J)V

    .line 145
    .local v2, "date":Ljava/util/Date;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationAreaView;->mDateView:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationAreaView;->dateFormat:Ljava/text/DateFormat;

    invoke-virtual {v15, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 147
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationAreaView;->mInformationItemContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v14}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v14

    if-ge v5, v14, :cond_0

    .line 148
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationAreaView;->mInformationItemContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v14, v5}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;

    .line 149
    .local v8, "item":Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;
    const/4 v4, 0x0

    .line 150
    .local v4, "hasData":Z
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :cond_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;

    .line 151
    .local v1, "data":Lcom/samsung/android/sdk/chart/view/SchartHandlerData;
    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesId()I

    move-result v9

    .line 152
    .local v9, "seriesId":I
    invoke-virtual {v8}, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->getSeriesId()I

    move-result v14

    if-ne v14, v9, :cond_3

    .line 153
    if-eqz p2, :cond_4

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationAreaView;->mShouldSupportAverageCounting:Z

    if-eqz v14, :cond_4

    .line 154
    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesXValue()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v10

    .line 155
    .local v10, "seriesTime":J
    invoke-virtual {v8}, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->getSeriesId()I

    move-result v14

    move-object/from16 v0, p2

    invoke-interface {v0, v14, v10, v11}, Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;->getMeasureCountForPeriod(IJ)I

    move-result v14

    const/4 v15, 0x1

    if-le v14, v15, :cond_7

    const/4 v7, 0x1

    .line 157
    .local v7, "isLabelVisible":Z
    :goto_1
    invoke-virtual {v8, v7}, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->setAverageLabelVisible(Z)V

    .line 159
    .end local v7    # "isLabelVisible":Z
    .end local v10    # "seriesTime":J
    :cond_4
    move-object/from16 v0, p0

    invoke-direct {v0, v8, v1}, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationAreaView;->updateInformationItem(Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;Lcom/samsung/android/sdk/chart/view/SchartHandlerData;)V

    .line 160
    const/4 v4, 0x1

    .line 165
    .end local v1    # "data":Lcom/samsung/android/sdk/chart/view/SchartHandlerData;
    .end local v9    # "seriesId":I
    :cond_5
    invoke-virtual {v8}, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->getVisibility()I

    move-result v14

    if-nez v14, :cond_8

    if-nez v4, :cond_8

    .line 166
    const/16 v14, 0x8

    invoke-virtual {v8, v14}, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->setVisibility(I)V

    .line 147
    :cond_6
    :goto_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 155
    .restart local v1    # "data":Lcom/samsung/android/sdk/chart/view/SchartHandlerData;
    .restart local v9    # "seriesId":I
    .restart local v10    # "seriesTime":J
    :cond_7
    const/4 v7, 0x0

    goto :goto_1

    .line 167
    .end local v1    # "data":Lcom/samsung/android/sdk/chart/view/SchartHandlerData;
    .end local v9    # "seriesId":I
    .end local v10    # "seriesTime":J
    :cond_8
    invoke-virtual {v8}, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->getVisibility()I

    move-result v14

    if-eqz v14, :cond_6

    if-eqz v4, :cond_6

    .line 168
    const/4 v14, 0x0

    invoke-virtual {v8, v14}, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->setVisibility(I)V

    goto :goto_2
.end method
