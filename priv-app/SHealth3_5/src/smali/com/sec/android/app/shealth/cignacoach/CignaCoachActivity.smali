.class public Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;
.super Lcom/sec/android/app/shealth/cignacoach/CignaBaseActivity;
.source "CignaCoachActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity$3;,
        Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity$GoalMissionEventReceiver;
    }
.end annotation


# static fields
.field private static TAG:Ljava/lang/String;


# instance fields
.field private mCignaSummaryFragment:Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;

.field private mCurrentGoalFragment:Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;

.field mDateChangeCallback:Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$DateChageCallback;

.field private mExpandedGoalId:I

.field private mFirstTimeFragment:Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;

.field private mGoalComplete:Z

.field private mGoalCompleteReceiver:Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity$GoalMissionEventReceiver;

.field private mImageCache:Lcom/sec/android/app/shealth/cignacoach/util/CignaImageCacheTool;

.field private mMissionAdd:Z

.field private mRestoreHelper:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

.field private mRestorePopUpHelper:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;

.field private mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

.field private mTermsOfUseVersion:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 47
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaBaseActivity;-><init>()V

    .line 54
    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mGoalCompleteReceiver:Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity$GoalMissionEventReceiver;

    .line 55
    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    .line 56
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mMissionAdd:Z

    .line 57
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mGoalComplete:Z

    .line 58
    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mImageCache:Lcom/sec/android/app/shealth/cignacoach/util/CignaImageCacheTool;

    .line 60
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mTermsOfUseVersion:F

    .line 62
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mRestorePopUpHelper:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;

    .line 484
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity$1;-><init>(Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mDateChangeCallback:Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$DateChageCallback;

    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 47
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mGoalComplete:Z

    return p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;

    .prologue
    .line 47
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mExpandedGoalId:I

    return v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;
    .param p1, "x1"    # I

    .prologue
    .line 47
    iput p1, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mExpandedGoalId:I

    return p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;)Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mCurrentGoalFragment:Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;

    return-object v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 47
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mMissionAdd:Z

    return p1
.end method

.method static synthetic access$400()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;)Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mRestoreHelper:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    return-object v0
.end method

.method static synthetic access$502(Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;)Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mRestoreHelper:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    return-object p1
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;)Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mRestorePopUpHelper:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;

    return-object v0
.end method

.method private isTermsOfUseUpdate()Z
    .locals 2

    .prologue
    .line 131
    new-instance v0, Lcom/sec/android/app/shealth/CheckVersionCode;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/CheckVersionCode;-><init>(Landroid/content/Context;)V

    .line 132
    .local v0, "checkCode":Lcom/sec/android/app/shealth/CheckVersionCode;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/CheckVersionCode;->readAssertFile()V

    .line 133
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/CheckVersionCode;->getCignaTermsOfUseUpdateCode()F

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mTermsOfUseVersion:F

    .line 134
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/CheckVersionCode;->isCignaTermsOfUseUpdate()Z

    move-result v1

    return v1
.end method

.method private registerGoalMissionEventReceiver()V
    .locals 2

    .prologue
    .line 373
    new-instance v1, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity$GoalMissionEventReceiver;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity$GoalMissionEventReceiver;-><init>(Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mGoalCompleteReceiver:Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity$GoalMissionEventReceiver;

    .line 374
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 375
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "com.sec.android.app.shealth.cignacoach.GOAL_COMPLETE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 376
    const-string v1, "com.sec.android.app.shealth.cignacoach.MISSION_COMPLETE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 377
    const-string v1, "com.sec.android.app.shealth.cignacoach.MISSION_ADD"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 378
    const-string v1, "com.sec.android.app.shealth.cignacoach.MISSION_PROGRESSED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 379
    const-string v1, "com.sec.shealth.UPDATE_PROFILE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 380
    const-string v1, "com.cigna.mobile.coach.SCORE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 381
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mGoalCompleteReceiver:Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity$GoalMissionEventReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 382
    return-void
.end method

.method private showFragment()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 221
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 222
    .local v3, "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v6, "intent_need_cigna_main_update"

    invoke-virtual {v5, v6, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    .line 223
    .local v4, "mainUpdate":Z
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v6, "EXTRA_NAME_FORCED_CIGNA_SCORE_SCREEN"

    invoke-virtual {v5, v6, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 225
    .local v2, "forcedScoreScreen":Z
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;

    move-result-object v5

    invoke-virtual {v5, p0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->hasCoachDBFile(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;->isFirstLoadingComplete()Z

    move-result v5

    if-nez v5, :cond_2

    .line 226
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->getFirstTimeFragment()Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->showFragment(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;)V

    .line 281
    :cond_1
    :goto_0
    iput-boolean v8, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mGoalComplete:Z

    .line 282
    return-void

    .line 227
    :cond_2
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->isFirstTimeNoScore()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 228
    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->showFirstTimeNoScoreFragment(Z)V

    goto :goto_0

    .line 230
    :cond_3
    if-eqz v2, :cond_7

    .line 231
    new-instance v5, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;

    invoke-direct {v5}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;-><init>()V

    iput-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mCignaSummaryFragment:Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;

    .line 232
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mCignaSummaryFragment:Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->showFragment(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;)V

    .line 244
    :cond_4
    :goto_1
    const-string v5, "EXTRA_NAME_DESTINATION"

    invoke-virtual {v3, v5}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 246
    const-string v5, "EXTRA_NAME_DESTINATION"

    invoke-virtual {v3, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 247
    .local v0, "actionName":Ljava/lang/String;
    sget-object v5, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "EXTRA_NAME_DESTINATION: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 249
    if-eqz v0, :cond_6

    .line 252
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;

    move-result-object v5

    invoke-virtual {v5, p0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->isCoachBREnabled(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 253
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mRestorePopUpHelper:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->isShowing()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 254
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mRestorePopUpHelper:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->dismissRestoreTriggerPopup()V

    .line 256
    :cond_5
    const-string v5, "com.sec.android.app.shealth.cignacoach.DESTINATION_CURRENT_MISSION_DETAIL"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_b

    .line 257
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 258
    .local v1, "destinationIntent":Landroid/content/Intent;
    const-string v5, "EXTRA_NAME_MISSION_ID"

    const-string v6, "EXTRA_NAME_MISSION_ID"

    const/4 v7, -0x1

    invoke-virtual {v3, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    invoke-virtual {v1, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 259
    const-string v5, "LAUNCHED_FROM_WIDGET"

    invoke-virtual {v1, v5, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 260
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->startActivity(Landroid/content/Intent;)V

    .line 277
    .end local v1    # "destinationIntent":Landroid/content/Intent;
    :cond_6
    :goto_2
    const-string v5, "EXTRA_NAME_DESTINATION"

    invoke-virtual {v3, v5}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 233
    .end local v0    # "actionName":Ljava/lang/String;
    :cond_7
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mCignaSummaryFragment:Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;

    if-nez v5, :cond_8

    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mCurrentGoalFragment:Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;

    if-eqz v5, :cond_9

    :cond_8
    if-eqz v4, :cond_4

    .line 234
    :cond_9
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getCurrentGoalCount()I

    move-result v5

    if-lez v5, :cond_a

    .line 235
    new-instance v5, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;

    invoke-direct {v5}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;-><init>()V

    iput-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mCurrentGoalFragment:Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;

    .line 236
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mCurrentGoalFragment:Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->showFragment(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;)V

    goto/16 :goto_1

    .line 238
    :cond_a
    new-instance v5, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;

    invoke-direct {v5}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;-><init>()V

    iput-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mCignaSummaryFragment:Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;

    .line 239
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mCignaSummaryFragment:Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->showFragment(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;)V

    goto/16 :goto_1

    .line 261
    .restart local v0    # "actionName":Ljava/lang/String;
    :cond_b
    const-string v5, "com.sec.android.app.shealth.cignacoach.DESTINATION_SUGGEST_GOAL"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_c

    .line 262
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 263
    .restart local v1    # "destinationIntent":Landroid/content/Intent;
    const-string v5, "LAUNCHED_FROM_WIDGET"

    invoke-virtual {v1, v5, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 264
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_2

    .line 265
    .end local v1    # "destinationIntent":Landroid/content/Intent;
    :cond_c
    const-string v5, "com.sec.android.app.shealth.cignacoach.DESTINATION_ASSESSMENT"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_d

    .line 266
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 267
    .restart local v1    # "destinationIntent":Landroid/content/Intent;
    const-string v5, "intent_lifestyle_categorys"

    const-string v6, "intent_lifestyle_categorys"

    invoke-virtual {v3, v6}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 268
    const-string v5, "LAUNCHED_FROM_WIDGET"

    invoke-virtual {v1, v5, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 269
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_2

    .line 270
    .end local v1    # "destinationIntent":Landroid/content/Intent;
    :cond_d
    const-string v5, "com.sec.android.app.shealth.cignacoach.DESTINATION_LIFESTYLE_SCORE"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 271
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 272
    .restart local v1    # "destinationIntent":Landroid/content/Intent;
    const-string v5, "LAUNCHED_FROM_WIDGET"

    invoke-virtual {v1, v5, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 273
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_2
.end method

.method private startDayBreakService(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 505
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/cignacoach/receiver/CignaDaybreakService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 506
    .local v0, "i":Landroid/content/Intent;
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 507
    return-void
.end method

.method private stopDayBreakService(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 510
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/cignacoach/receiver/CignaDaybreakService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 511
    .local v0, "i":Landroid/content/Intent;
    invoke-virtual {p1, v0}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    .line 512
    return-void
.end method

.method private unregisterGoalMissionEventReceiver()V
    .locals 1

    .prologue
    .line 385
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mGoalCompleteReceiver:Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity$GoalMissionEventReceiver;

    if-eqz v0, :cond_0

    .line 386
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mGoalCompleteReceiver:Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity$GoalMissionEventReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 387
    :cond_0
    return-void
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 5

    .prologue
    const v2, 0x7f090351

    const v3, 0x7f090350

    const v4, 0x7f09020b

    .line 318
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaBaseActivity;->customizeActionBar()V

    .line 322
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(II)V

    .line 323
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getActionBarLayout()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f080304

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09020e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090218

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 328
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    .line 329
    return-void
.end method

.method public getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;
    .locals 1

    .prologue
    .line 334
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaBaseActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    return-object v0
.end method

.method public getCignaSummaryFragment()Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;
    .locals 1

    .prologue
    .line 285
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mCignaSummaryFragment:Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;

    if-nez v0, :cond_0

    .line 286
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mCignaSummaryFragment:Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;

    .line 288
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mCignaSummaryFragment:Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;

    return-object v0
.end method

.method public getCurrentGoalFragment()Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;
    .locals 1

    .prologue
    .line 292
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mCurrentGoalFragment:Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;

    if-nez v0, :cond_0

    .line 293
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mCurrentGoalFragment:Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;

    .line 295
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mCurrentGoalFragment:Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;

    return-object v0
.end method

.method public getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 540
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity$2;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity$2;-><init>(Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;Ljava/lang/String;)V

    return-object v0
.end method

.method public getFirstTimeFragment()Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;
    .locals 1

    .prologue
    .line 299
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mFirstTimeFragment:Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;

    if-nez v0, :cond_0

    .line 300
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mFirstTimeFragment:Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;

    .line 302
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mFirstTimeFragment:Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;

    return-object v0
.end method

.method public getImageCache()Lcom/sec/android/app/shealth/cignacoach/util/CignaImageCacheTool;
    .locals 1

    .prologue
    .line 481
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mImageCache:Lcom/sec/android/app/shealth/cignacoach/util/CignaImageCacheTool;

    return-object v0
.end method

.method public initializeCignaEnvironment()V
    .locals 4

    .prologue
    const/16 v3, 0xdd

    .line 467
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;->initialize(Landroid/content/Context;)V

    .line 468
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;->initialize(Landroid/content/Context;)V

    .line 470
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mImageCache:Lcom/sec/android/app/shealth/cignacoach/util/CignaImageCacheTool;

    if-nez v0, :cond_0

    .line 471
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageCacheTool;

    const v1, 0x3e4ccccd    # 0.2f

    const/4 v2, 0x1

    invoke-direct {v0, v1, v3, v3, v2}, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageCacheTool;-><init>(FIIZ)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mImageCache:Lcom/sec/android/app/shealth/cignacoach/util/CignaImageCacheTool;

    .line 474
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    if-nez v0, :cond_1

    .line 475
    new-instance v0, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    .line 478
    :cond_1
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 516
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/cignacoach/CignaBaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 518
    const/16 v0, 0x71

    if-ne p1, v0, :cond_0

    .line 519
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->finish()V

    .line 521
    :cond_0
    return-void
.end method

.method public onChangeFragmentBtnClick()V
    .locals 2

    .prologue
    .line 524
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->getCignaSummaryFragment()Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 525
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->getCurrentGoalFragment()Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->showFragment(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;)V

    .line 530
    :cond_0
    :goto_0
    return-void

    .line 526
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->getCurrentGoalFragment()Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 527
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->getCignaSummaryFragment()Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->setAnimationEnable(Z)V

    .line 528
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->getCignaSummaryFragment()Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->showFragment(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v9, 0x0

    const/4 v8, -0x1

    .line 67
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/CignaBaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 69
    sget-boolean v5, Lcom/sec/android/app/shealth/SHealthApplication;->unSupported:Z

    if-eqz v5, :cond_0

    .line 70
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->finish()V

    .line 128
    :goto_0
    return-void

    .line 74
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v6, "launchWidget"

    invoke-virtual {v5, v6, v9}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 75
    .local v2, "isLaunchedFromWidget":Z
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v6, "COACH_WIDGET"

    invoke-virtual {v5, v6, v9}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 76
    .local v1, "isCoachWidgetIntent":Z
    if-eqz v2, :cond_1

    if-eqz v1, :cond_1

    .line 77
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    const-string v6, "com.sec.android.app.shealth.cignacoach"

    const-string v7, "0019"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->isTermsOfUseUpdate()Z

    move-result v3

    .line 81
    .local v3, "isTermsOfUseUpdate":Z
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;->isAgreeTermsOfUse()Z

    move-result v5

    if-eqz v5, :cond_2

    if-eqz v3, :cond_5

    .line 83
    :cond_2
    new-instance v0, Landroid/content/Intent;

    const-class v5, Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;

    invoke-direct {v0, p0, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 84
    .local v0, "intent":Landroid/content/Intent;
    iget v5, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mTermsOfUseVersion:F

    const/4 v6, 0x0

    cmpl-float v5, v5, v6

    if-lez v5, :cond_3

    .line 85
    const-string v5, "EXTRA_NAME_TERMS_OF_USE_VERSION"

    iget v6, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mTermsOfUseVersion:F

    invoke-virtual {v0, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;

    .line 89
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    if-eqz v5, :cond_4

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v6, "launchWidget"

    invoke-virtual {v5, v6}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 91
    const-string v5, "launchWidget"

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    const-string v7, "launchWidget"

    invoke-virtual {v6, v7, v9}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    invoke-virtual {v0, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 94
    :cond_4
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->startActivity(Landroid/content/Intent;)V

    .line 95
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->finish()V

    goto :goto_0

    .line 99
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->initializeCignaEnvironment()V

    .line 101
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->registerGoalMissionEventReceiver()V

    .line 103
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b0006

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 104
    invoke-direct {p0, p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->startDayBreakService(Landroid/content/Context;)V

    .line 106
    :cond_6
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mDateChangeCallback:Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$DateChageCallback;

    invoke-static {v5}, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService;->registerDayChangedListener(Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$DateChageCallback;)V

    .line 108
    new-instance v4, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-direct {v4, p0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    .line 109
    .local v4, "profile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getGender()I

    move-result v5

    if-eq v5, v8, :cond_7

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getHeightUnit()I

    move-result v5

    if-eq v5, v8, :cond_7

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getWeightUnit()I

    move-result v5

    if-eq v5, v8, :cond_7

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getActivityType()I

    move-result v5

    if-ne v5, v8, :cond_8

    .line 111
    :cond_7
    new-instance v0, Landroid/content/Intent;

    const-class v5, Lcom/sec/android/app/shealth/SplashScreenActivity;

    invoke-direct {v0, p0, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 112
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string v5, "com.sec.android.app.shealth"

    invoke-virtual {v0, v5}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 113
    const v5, 0x10018000

    invoke-virtual {v0, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 114
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->startActivity(Landroid/content/Intent;)V

    .line 115
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->finish()V

    goto/16 :goto_0

    .line 121
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_8
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;

    move-result-object v5

    invoke-virtual {v5, p0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->isCoachBREnabled(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_9

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getCignaRestoreTriggerPopup(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 122
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_9

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 123
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mRestorePopUpHelper:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->showDialog()V

    .line 125
    :cond_9
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->showFragment()V

    goto/16 :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 344
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->isMenuShown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 345
    const/4 v0, 0x0

    .line 350
    :goto_0
    return v0

    .line 348
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->customizeActionBar()V

    .line 350
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 195
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaBaseActivity;->onDestroy()V

    .line 196
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mImageCache:Lcom/sec/android/app/shealth/cignacoach/util/CignaImageCacheTool;

    if-eqz v0, :cond_0

    .line 197
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mImageCache:Lcom/sec/android/app/shealth/cignacoach/util/CignaImageCacheTool;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageCacheTool;->clearCache()V

    .line 199
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->unregisterGoalMissionEventReceiver()V

    .line 200
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mDateChangeCallback:Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$DateChageCallback;

    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService;->unregisterDayChangedListener(Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$DateChageCallback;)V

    .line 201
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0006

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 202
    invoke-direct {p0, p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->stopDayBreakService(Landroid/content/Context;)V

    .line 205
    :cond_1
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->isCoachBREnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 206
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mRestorePopUpHelper:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->destroyDialog()V

    .line 208
    :cond_2
    return-void
.end method

.method protected onLogSelected()V
    .locals 0

    .prologue
    .line 340
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x0

    .line 139
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/CignaBaseActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 140
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->setIntent(Landroid/content/Intent;)V

    .line 141
    const-string v2, "launchWidget"

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 142
    .local v1, "isLaunchedFromWidget":Z
    const-string v2, "COACH_WIDGET"

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 143
    .local v0, "isCoachWidgetIntent":Z
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 144
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "com.sec.android.app.shealth.cignacoach"

    const-string v4, "0019"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->showFragment()V

    .line 149
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->isCoachBREnabled(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p1}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    .line 150
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mRestorePopUpHelper:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 151
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mRestorePopUpHelper:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->dismissRestoreTriggerPopup()V

    .line 153
    :cond_1
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 355
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f080c90

    if-ne v0, v1, :cond_2

    .line 356
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->prepareShareView()V

    .line 358
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->getCignaSummaryFragment()Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 359
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth.cignacoach"

    const-string v2, "0027"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 364
    :cond_0
    :goto_0
    const/4 v0, 0x1

    .line 369
    :goto_1
    return v0

    .line 360
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->getCurrentGoalFragment()Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 361
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth.cignacoach"

    const-string v2, "0028"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 367
    :cond_2
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachMoreUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachMoreUtils;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachMoreUtils;->showMoreForContextMenu(Landroid/content/Context;Landroid/view/MenuItem;)V

    .line 369
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 157
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaBaseActivity;->onResume()V

    .line 159
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    .line 162
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mMissionAdd:Z

    if-eqz v0, :cond_4

    .line 164
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090c80

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 165
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->getCignaSummaryFragment()Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 167
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->getCurrentGoalFragment()Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mExpandedGoalId:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->setToBeExpandGoalId(I)V

    .line 169
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->getCurrentGoalFragment()Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->showFragment(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;)V

    .line 177
    :cond_0
    :goto_0
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mMissionAdd:Z

    .line 188
    :cond_1
    :goto_1
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->isCoachBREnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mRestoreHelper:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    if-eqz v0, :cond_2

    .line 189
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mRestoreHelper:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->restoreState()V

    .line 191
    :cond_2
    return-void

    .line 171
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->getCurrentGoalFragment()Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 173
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->getCurrentGoalFragment()Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mExpandedGoalId:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->updateList(I)V

    goto :goto_0

    .line 179
    :cond_4
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mGoalComplete:Z

    if-eqz v0, :cond_1

    .line 181
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getCurrentGoalCount()I

    move-result v0

    if-gtz v0, :cond_5

    .line 182
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_NAME_FORCED_CIGNA_SCORE_SCREEN"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 185
    :cond_5
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->showFragment()V

    goto :goto_1
.end method

.method public showCignaIntroScreenFragment()V
    .locals 1

    .prologue
    .line 306
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/CignaIntroScreenFragment;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/cignacoach/CignaIntroScreenFragment;-><init>()V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->showFragment(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;)V

    .line 307
    return-void
.end method

.method public showFirstTimeNoScoreFragment(Z)V
    .locals 1
    .param p1, "isDialogToBeShown"    # Z

    .prologue
    .line 310
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/FirstTimeNoScoreFragment;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/cignacoach/FirstTimeNoScoreFragment;-><init>()V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->showFragment(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;)V

    .line 312
    if-eqz p1, :cond_0

    .line 313
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mRestorePopUpHelper:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->showDialog()V

    .line 314
    :cond_0
    return-void
.end method

.method public showFragment(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;ZZZ)V
    .locals 1
    .param p1, "fragmentClass"    # Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;
    .param p2, "shouldAddToStack"    # Z
    .param p3, "animate"    # Z
    .param p4, "isDialogToBeShown"    # Z

    .prologue
    .line 212
    invoke-virtual {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->showFragment(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;ZZ)V

    .line 215
    if-eqz p4, :cond_0

    .line 216
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mRestorePopUpHelper:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->showDialog()V

    .line 218
    :cond_0
    return-void
.end method

.method protected updateUI()V
    .locals 0

    .prologue
    .line 534
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->showFragment()V

    .line 535
    return-void
.end method
