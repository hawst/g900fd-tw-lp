.class public Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;
.super Lcom/sec/android/app/shealth/food/foodpick/search/api/base/NotifyingFoodParser;
.source "BooheeFoodParser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser$DailyReferenceValuesByFDA;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/NotifyingFoodParser",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field private static final APPRAISE:Ljava/lang/String; = "appraise"

.field private static final BIG_OZ_IN_GRAM:F = 100.0f

.field private static final BOOHEE_FOOD_UNIT:F = 1.0f

.field private static final BOOHEE_UNIT:F = 100.0f

.field private static final CALCIUM:Ljava/lang/String; = "calcium"

.field private static final CALORY:Ljava/lang/String; = "calory"

.field private static final CARBOHYDRATE:Ljava/lang/String; = "carbohydrate"

.field public static final CATEGORY_DISHES:Ljava/lang/String; = "1"

.field public static final CATEGORY_FOOD:Ljava/lang/String; = "0"

.field private static final CHOLESTEROL:Ljava/lang/String; = "cholesterol"

.field private static final DIETARY_FIBER:Ljava/lang/String; = "fiber_dietary"

.field private static final FAT:Ljava/lang/String; = "fat"

.field private static final FOOD:Ljava/lang/String; = "food"

.field private static final FOODS:Ljava/lang/String; = "foods"

.field private static final FOOD_GROUPS:Ljava/lang/String; = "food_groups"

.field private static final FOOD_UNITS:Ljava/lang/String; = "food_units"

.field public static FOOD_UNIT_CALORY:F = 0.0f

.field private static final GRAM_UNIT:I = 0x64

.field private static final GROUP_ID:Ljava/lang/String; = "big_group"

.field private static final G_UNIT:Ljava/lang/String; = "100g"

.field private static HEALTH_STAR:F = 0.0f

.field private static final ID:Ljava/lang/String; = "id"

.field private static final IRON:Ljava/lang/String; = "iron"

.field private static final IS_LIQUID:Ljava/lang/String; = "is_liquid"

.field private static final LOG_TAG:Ljava/lang/String;

.field private static final ML_UNIT:Ljava/lang/String; = "100ml"

.field private static final NAME:Ljava/lang/String; = "name"

.field private static final NO_VALUE:F = 0.0f

.field private static final OZ_IN_GRAM:F = 28.35f

.field public static final PARSE_CATEGORY_NAME_ID:Ljava/lang/String; = "BOOHEE"

.field private static final POTASSIUM:Ljava/lang/String; = "kalium"

.field private static final PROTEIN:Ljava/lang/String; = "protein"

.field private static final SODIUM:Ljava/lang/String; = "natrium"

.field private static final STAR:Ljava/lang/String; = "healthy_star"

.field private static final VITAMIN_A:Ljava/lang/String; = "vitamin_a"

.field private static final VITAMIN_C:Ljava/lang/String; = "vitamin_c"

.field private static final WEIGHT:Ljava/lang/String; = "weight"


# instance fields
.field private mResources:Landroid/content/res/Resources;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 43
    const-class v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->getLogTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->LOG_TAG:Ljava/lang/String;

    .line 82
    sput v1, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->HEALTH_STAR:F

    .line 83
    sput v1, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->FOOD_UNIT_CALORY:F

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 87
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/NotifyingFoodParser;-><init>()V

    .line 88
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->mResources:Landroid/content/res/Resources;

    .line 89
    return-void
.end method

.method private checkGoodUnitsUsingJSONArray(Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;Lorg/json/JSONObject;)V
    .locals 8
    .param p1, "result"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;
    .param p2, "food"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    const/high16 v7, 0x42c80000    # 100.0f

    .line 353
    new-instance v4, Lorg/json/JSONArray;

    const-string v5, "food_units"

    invoke-virtual {p2, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 354
    .local v4, "jsonArray":Lorg/json/JSONArray;
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-nez v5, :cond_0

    .line 355
    iget-object v5, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->mResources:Landroid/content/res/Resources;

    const v6, 0x7f0900bf

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setServingDescription(Ljava/lang/String;)V

    .line 356
    invoke-virtual {p1, v7}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setDefaultNumber(F)V

    .line 357
    const-string v5, "calory"

    const-string v6, ""

    invoke-virtual {p2, v5, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->setUnitClory(Ljava/lang/String;)V

    .line 358
    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v5

    invoke-virtual {p1, v5}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setUnit(I)V

    .line 376
    :goto_0
    return-void

    .line 361
    :cond_0
    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v0

    .line 362
    .local v0, "foodUnits":Lorg/json/JSONObject;
    const-string/jumbo v5, "weight"

    const/4 v6, -0x1

    invoke-virtual {v0, v5, v6}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v3

    .line 363
    .local v3, "foodUnitsWeight":I
    const-string/jumbo v5, "name"

    const-string v6, ""

    invoke-virtual {v0, v5, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 364
    .local v2, "foodUnitsName":Ljava/lang/String;
    const-string v5, "calory"

    const-string v6, ""

    invoke-virtual {v0, v5, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 365
    .local v1, "foodUnitsCalory":Ljava/lang/String;
    invoke-virtual {p1, v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setServingDescription(Ljava/lang/String;)V

    .line 366
    if-lez v3, :cond_1

    int-to-float v5, v3

    invoke-virtual {p1, v5}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setDefaultNumber(F)V

    .line 367
    :cond_1
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->setUnitClory(Ljava/lang/String;)V

    .line 368
    const/high16 v5, 0x3f800000    # 1.0f

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    invoke-virtual {p1, v5}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setUnit(I)V

    .line 371
    sget-object v5, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->LOG_TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " foodUnitsWeight= "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "   foodUnitsName= "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "     foodUnitsCalory= "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private getNormalizedUnits(FF)F
    .locals 2
    .param p1, "calories"    # F
    .param p2, "initialValue"    # F

    .prologue
    .line 455
    move v0, p2

    .line 456
    .local v0, "resValue":F
    const/4 v1, 0x0

    cmpl-float v1, p1, v1

    if-eqz v1, :cond_0

    .line 458
    div-float/2addr v0, p1

    .line 462
    :goto_0
    return v0

    .line 460
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private parseBaseFoodInfo(Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;Lorg/json/JSONObject;)V
    .locals 10
    .param p1, "foodInfoData"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    .param p2, "jsonObject"    # Lorg/json/JSONObject;

    .prologue
    .line 428
    sget-object v5, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->LOG_TAG:Ljava/lang/String;

    const-string v6, " parseBaseFoodInf"

    invoke-static {v5, v6}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 430
    const-string v5, "id"

    const/4 v6, 0x0

    invoke-virtual {p2, v5, v6}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v1

    .line 431
    .local v1, "foodId":I
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setServerId(Ljava/lang/String;)V

    .line 433
    const-string/jumbo v5, "name"

    const-string v6, ""

    invoke-virtual {p2, v5, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 434
    .local v2, "foodName":Ljava/lang/String;
    invoke-virtual {p1, v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setName(Ljava/lang/String;)V

    .line 437
    const-string v5, "calory"

    const-string v6, ""

    invoke-virtual {p2, v5, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 438
    .local v4, "strKcal":Ljava/lang/String;
    const/4 v3, 0x0

    .line 440
    .local v3, "kcal":F
    :try_start_0
    invoke-static {v4}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 444
    :goto_0
    invoke-virtual {p1, v3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setkCal(F)V

    .line 445
    invoke-direct {p0, p2}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->parseHealthStar(Lorg/json/JSONObject;)V

    .line 446
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->mResources:Landroid/content/res/Resources;

    const/16 v7, 0x64

    iget-object v8, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->mResources:Landroid/content/res/Resources;

    const v9, 0x7f0900bf

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v3, v7, v8}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->createFoodDescription(Landroid/content/res/Resources;FILjava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " | "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->mResources:Landroid/content/res/Resources;

    const v7, 0x7f090f7d

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget v6, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->HEALTH_STAR:F

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " / 5.0"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setDescription(Ljava/lang/String;)V

    .line 449
    sget-object v5, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->LOG_TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " foodId = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "  foodName = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "   kcal = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 451
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->notifyFoodParsingComplete(Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;)V

    .line 452
    return-void

    .line 441
    :catch_0
    move-exception v0

    .line 442
    .local v0, "e":Ljava/lang/NumberFormatException;
    sget-object v5, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->LOG_TAG:Ljava/lang/String;

    invoke-static {v5, v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0
.end method

.method private parseCalcium(Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;Lorg/json/JSONObject;)V
    .locals 4
    .param p1, "result"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;
    .param p2, "food"    # Lorg/json/JSONObject;

    .prologue
    .line 332
    const-string v2, "calcium"

    invoke-direct {p0, p2, v2}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->parseFloat(Lorg/json/JSONObject;Ljava/lang/String;)F

    move-result v0

    .line 333
    .local v0, "calcium":F
    const/4 v2, 0x0

    cmpl-float v2, v0, v2

    if-eqz v2, :cond_0

    .line 334
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getDefaultNumber()F

    move-result v2

    mul-float/2addr v2, v0

    const/high16 v3, 0x447a0000    # 1000.0f

    div-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 335
    .local v1, "percentageCalcium":I
    invoke-virtual {p1, v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setCalcium(I)V

    .line 339
    .end local v1    # "percentageCalcium":I
    :goto_0
    return-void

    .line 337
    :cond_0
    const/4 v2, -0x1

    invoke-virtual {p1, v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setCalcium(I)V

    goto :goto_0
.end method

.method private parseCarbohydrate(Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;Lorg/json/JSONObject;)V
    .locals 5
    .param p1, "result"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;
    .param p2, "food"    # Lorg/json/JSONObject;

    .prologue
    .line 292
    const-string v1, "carbohydrate"

    invoke-direct {p0, p2, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->parseFloat(Lorg/json/JSONObject;Ljava/lang/String;)F

    move-result v0

    .line 293
    .local v0, "carbohydrate":F
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getDefaultNumber()F

    move-result v1

    mul-float/2addr v1, v0

    float-to-double v1, v1

    const-wide/high16 v3, 0x4059000000000000L    # 100.0

    div-double/2addr v1, v3

    double-to-float v1, v1

    invoke-virtual {p1, v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setCarbohydrate(F)V

    .line 294
    return-void
.end method

.method private parseDietaryFiber(Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;Lorg/json/JSONObject;)V
    .locals 5
    .param p1, "result"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;
    .param p2, "food"    # Lorg/json/JSONObject;

    .prologue
    .line 298
    const-string v1, "fiber_dietary"

    invoke-direct {p0, p2, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->parseFloat(Lorg/json/JSONObject;Ljava/lang/String;)F

    move-result v0

    .line 299
    .local v0, "dietaryFiber":F
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getDefaultNumber()F

    move-result v1

    mul-float/2addr v1, v0

    float-to-double v1, v1

    const-wide/high16 v3, 0x4059000000000000L    # 100.0

    div-double/2addr v1, v3

    double-to-float v1, v1

    invoke-virtual {p1, v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setDietary(F)V

    .line 300
    return-void
.end method

.method private parseFat(Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;Lorg/json/JSONObject;)V
    .locals 5
    .param p1, "result"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;
    .param p2, "food"    # Lorg/json/JSONObject;

    .prologue
    .line 259
    const-string v1, "fat"

    invoke-direct {p0, p2, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->parseFloat(Lorg/json/JSONObject;Ljava/lang/String;)F

    move-result v0

    .line 260
    .local v0, "fat":F
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getDefaultNumber()F

    move-result v1

    mul-float/2addr v1, v0

    float-to-double v1, v1

    const-wide/high16 v3, 0x4059000000000000L    # 100.0

    div-double/2addr v1, v3

    double-to-float v1, v1

    invoke-virtual {p1, v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setFat(F)V

    .line 261
    return-void
.end method

.method private parseFloat(Lorg/json/JSONObject;Ljava/lang/String;)F
    .locals 4
    .param p1, "food"    # Lorg/json/JSONObject;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 223
    const-string v3, ""

    invoke-virtual {p1, p2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 224
    .local v1, "str":Ljava/lang/String;
    const/4 v2, 0x0

    .line 226
    .local v2, "value":F
    :try_start_0
    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->convertToSingleDecimal(F)F
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 230
    :goto_0
    return v2

    .line 227
    :catch_0
    move-exception v0

    .line 228
    .local v0, "nfe":Ljava/lang/NumberFormatException;
    sget-object v3, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->LOG_TAG:Ljava/lang/String;

    invoke-static {v3, v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private parseFoodCalorie(Lorg/json/JSONObject;)F
    .locals 1
    .param p1, "food"    # Lorg/json/JSONObject;

    .prologue
    .line 234
    const-string v0, "calory"

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->parseFloat(Lorg/json/JSONObject;Ljava/lang/String;)F

    move-result v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->convertFloatKcalValueToInt(F)I

    move-result v0

    int-to-float v0, v0

    return v0
.end method

.method private parseFoodCholesterol(Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;Lorg/json/JSONObject;)V
    .locals 5
    .param p1, "result"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;
    .param p2, "food"    # Lorg/json/JSONObject;

    .prologue
    .line 274
    const-string v1, "cholesterol"

    invoke-direct {p0, p2, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->parseFloat(Lorg/json/JSONObject;Ljava/lang/String;)F

    move-result v0

    .line 275
    .local v0, "cholesterol":F
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getDefaultNumber()F

    move-result v1

    mul-float/2addr v1, v0

    float-to-double v1, v1

    const-wide/high16 v3, 0x4059000000000000L    # 100.0

    div-double/2addr v1, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->round(D)J

    move-result-wide v1

    long-to-int v1, v1

    invoke-virtual {p1, v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setCholesterol(I)V

    .line 276
    return-void
.end method

.method private parseHealthStar(Lorg/json/JSONObject;)V
    .locals 1
    .param p1, "food"    # Lorg/json/JSONObject;

    .prologue
    .line 238
    const-string v0, "healthy_star"

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->parseFloat(Lorg/json/JSONObject;Ljava/lang/String;)F

    move-result v0

    sput v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->HEALTH_STAR:F

    .line 239
    return-void
.end method

.method private parseIron(Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;Lorg/json/JSONObject;)V
    .locals 4
    .param p1, "result"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;
    .param p2, "food"    # Lorg/json/JSONObject;

    .prologue
    .line 343
    const-string v2, "iron"

    invoke-direct {p0, p2, v2}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->parseFloat(Lorg/json/JSONObject;Ljava/lang/String;)F

    move-result v0

    .line 344
    .local v0, "iron":F
    const/4 v2, 0x0

    cmpl-float v2, v0, v2

    if-eqz v2, :cond_0

    .line 345
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getDefaultNumber()F

    move-result v2

    mul-float/2addr v2, v0

    const/high16 v3, 0x41900000    # 18.0f

    div-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 346
    .local v1, "percentageIron":I
    invoke-virtual {p1, v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setIron(I)V

    .line 350
    .end local v1    # "percentageIron":I
    :goto_0
    return-void

    .line 348
    :cond_0
    const/4 v2, -0x1

    invoke-virtual {p1, v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setIron(I)V

    goto :goto_0
.end method

.method private parsePotassium(Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;Lorg/json/JSONObject;)V
    .locals 5
    .param p1, "result"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;
    .param p2, "food"    # Lorg/json/JSONObject;

    .prologue
    .line 286
    const-string v1, "kalium"

    invoke-direct {p0, p2, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->parseFloat(Lorg/json/JSONObject;Ljava/lang/String;)F

    move-result v0

    .line 287
    .local v0, "potassium":F
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getDefaultNumber()F

    move-result v1

    mul-float/2addr v1, v0

    float-to-double v1, v1

    const-wide/high16 v3, 0x4059000000000000L    # 100.0

    div-double/2addr v1, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->round(D)J

    move-result-wide v1

    long-to-int v1, v1

    invoke-virtual {p1, v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setPotassium(I)V

    .line 288
    return-void
.end method

.method private parseProtein(Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;Lorg/json/JSONObject;)V
    .locals 5
    .param p1, "result"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;
    .param p2, "food"    # Lorg/json/JSONObject;

    .prologue
    .line 304
    const-string/jumbo v1, "protein"

    invoke-direct {p0, p2, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->parseFloat(Lorg/json/JSONObject;Ljava/lang/String;)F

    move-result v0

    .line 305
    .local v0, "protein":F
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getDefaultNumber()F

    move-result v1

    mul-float/2addr v1, v0

    float-to-double v1, v1

    const-wide/high16 v3, 0x4059000000000000L    # 100.0

    div-double/2addr v1, v3

    double-to-float v1, v1

    invoke-virtual {p1, v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setProtein(F)V

    .line 306
    return-void
.end method

.method private parseSodium(Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;Lorg/json/JSONObject;)V
    .locals 5
    .param p1, "result"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;
    .param p2, "food"    # Lorg/json/JSONObject;

    .prologue
    .line 280
    const-string/jumbo v1, "natrium"

    invoke-direct {p0, p2, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->parseFloat(Lorg/json/JSONObject;Ljava/lang/String;)F

    move-result v0

    .line 281
    .local v0, "sodium":F
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getDefaultNumber()F

    move-result v1

    mul-float/2addr v1, v0

    float-to-double v1, v1

    const-wide/high16 v3, 0x4059000000000000L    # 100.0

    div-double/2addr v1, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->round(D)J

    move-result-wide v1

    long-to-int v1, v1

    invoke-virtual {p1, v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setSodium(I)V

    .line 282
    return-void
.end method

.method private parseVitaminA(Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;Lorg/json/JSONObject;)V
    .locals 4
    .param p1, "result"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;
    .param p2, "food"    # Lorg/json/JSONObject;

    .prologue
    .line 310
    const-string/jumbo v2, "vitamin_a"

    invoke-direct {p0, p2, v2}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->parseFloat(Lorg/json/JSONObject;Ljava/lang/String;)F

    move-result v1

    .line 311
    .local v1, "vitaminA":F
    const/4 v2, 0x0

    cmpl-float v2, v1, v2

    if-eqz v2, :cond_0

    .line 312
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getDefaultNumber()F

    move-result v2

    mul-float/2addr v2, v1

    const/high16 v3, 0x44610000    # 900.0f

    div-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 313
    .local v0, "percentageVitaminA":I
    invoke-virtual {p1, v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setVitaminA(I)V

    .line 317
    .end local v0    # "percentageVitaminA":I
    :goto_0
    return-void

    .line 315
    :cond_0
    const/4 v2, -0x1

    invoke-virtual {p1, v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setVitaminA(I)V

    goto :goto_0
.end method

.method private parseVitaminC(Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;Lorg/json/JSONObject;)V
    .locals 4
    .param p1, "result"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;
    .param p2, "food"    # Lorg/json/JSONObject;

    .prologue
    .line 321
    const-string/jumbo v2, "vitamin_c"

    invoke-direct {p0, p2, v2}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->parseFloat(Lorg/json/JSONObject;Ljava/lang/String;)F

    move-result v1

    .line 322
    .local v1, "vitaminC":F
    const/high16 v2, -0x40800000    # -1.0f

    cmpl-float v2, v1, v2

    if-eqz v2, :cond_0

    .line 323
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getDefaultNumber()F

    move-result v2

    mul-float/2addr v2, v1

    const/high16 v3, 0x42700000    # 60.0f

    div-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 324
    .local v0, "percentageVitaminC":I
    invoke-virtual {p1, v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setVitaminC(I)V

    .line 328
    .end local v0    # "percentageVitaminC":I
    :goto_0
    return-void

    .line 326
    :cond_0
    const/4 v2, -0x1

    invoke-virtual {p1, v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setVitaminC(I)V

    goto :goto_0
.end method

.method private setSaturatedValues(Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;)V
    .locals 1
    .param p1, "result"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;

    .prologue
    const/4 v0, 0x0

    .line 265
    invoke-virtual {p1, v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setSaturated(F)V

    .line 267
    invoke-virtual {p1, v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setPolysaturated(F)V

    .line 269
    invoke-virtual {p1, v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setMonosaturated(F)V

    .line 270
    return-void
.end method

.method private setUnitsDependingOnLiquid(Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;Lorg/json/JSONObject;)V
    .locals 3
    .param p1, "result"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;
    .param p2, "food"    # Lorg/json/JSONObject;

    .prologue
    .line 242
    const-string v1, "is_liquid"

    const/4 v2, 0x0

    invoke-virtual {p2, v1, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 243
    .local v0, "isLiquid":Z
    if-eqz v0, :cond_0

    .line 244
    const-string v1, "100ml"

    invoke-virtual {p1, v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setUnitName(Ljava/lang/String;)V

    .line 248
    :goto_0
    return-void

    .line 246
    :cond_0
    const-string v1, "100g"

    invoke-virtual {p1, v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setUnitName(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setUnitsInfo(Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;F)V
    .locals 2
    .param p1, "result"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;
    .param p2, "calorie"    # F

    .prologue
    .line 251
    const/high16 v0, 0x42c80000    # 100.0f

    .line 252
    .local v0, "value":F
    invoke-direct {p0, p2, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->getNormalizedUnits(FF)F

    move-result v0

    .line 253
    invoke-virtual {p1, v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setGrammInKcal(F)V

    .line 254
    const v1, 0x41e2cccd    # 28.35f

    div-float v1, v0, v1

    invoke-virtual {p1, v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setOzInKcal(F)V

    .line 255
    return-void
.end method


# virtual methods
.method public getUnitCalory()F
    .locals 3

    .prologue
    .line 388
    sget-object v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " FOOD_UNIT_CALORY"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->FOOD_UNIT_CALORY:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 389
    sget v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->FOOD_UNIT_CALORY:F

    return v0
.end method

.method public bridge synthetic parseAutoCompleteSearch(Ljava/lang/Object;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/AutoCompleteSearchResult;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 42
    check-cast p1, Ljava/lang/String;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->parseAutoCompleteSearch(Ljava/lang/String;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/AutoCompleteSearchResult;

    move-result-object v0

    return-object v0
.end method

.method public parseAutoCompleteSearch(Ljava/lang/String;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/AutoCompleteSearchResult;
    .locals 9
    .param p1, "requestResult"    # Ljava/lang/String;

    .prologue
    .line 93
    sget-object v6, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->LOG_TAG:Ljava/lang/String;

    const-string v7, " autoCompleteFoodName"

    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 95
    .local v5, "result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz p1, :cond_0

    .line 97
    :try_start_0
    new-instance v3, Lorg/json/JSONArray;

    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v7, "foods"

    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v6}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 98
    .local v3, "jsonArray":Lorg/json/JSONArray;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-ge v2, v6, :cond_0

    .line 99
    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 100
    .local v4, "jsonObject":Lorg/json/JSONObject;
    const-string/jumbo v6, "name"

    const-string v7, ""

    invoke-virtual {v4, v6, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 101
    .local v0, "autoCompleteFoodName":Ljava/lang/String;
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 103
    sget-object v6, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->LOG_TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, " autoCompleteFoodName = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 98
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 106
    .end local v0    # "autoCompleteFoodName":Ljava/lang/String;
    .end local v2    # "i":I
    .end local v3    # "jsonArray":Lorg/json/JSONArray;
    .end local v4    # "jsonObject":Lorg/json/JSONObject;
    :catch_0
    move-exception v1

    .line 107
    .local v1, "e":Lorg/json/JSONException;
    sget-object v6, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->LOG_TAG:Ljava/lang/String;

    invoke-static {v6, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 110
    .end local v1    # "e":Lorg/json/JSONException;
    :cond_0
    new-instance v6, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/AutoCompleteSearchResult;

    invoke-direct {v6, v5}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/AutoCompleteSearchResult;-><init>(Ljava/util/List;)V

    return-object v6
.end method

.method public bridge synthetic parseBarcodeSearch(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 42
    check-cast p1, Ljava/lang/String;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->parseBarcodeSearch(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public parseBarcodeSearch(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "requestResult"    # Ljava/lang/String;

    .prologue
    .line 394
    sget-object v3, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->LOG_TAG:Ljava/lang/String;

    const-string v4, " parseBarcodeSearch"

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 395
    if-eqz p1, :cond_1

    .line 397
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 398
    .local v2, "jsonObject":Lorg/json/JSONObject;
    const-string v3, "food"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 399
    .local v1, "food":Lorg/json/JSONObject;
    if-eqz v1, :cond_0

    .line 400
    const-string v3, "id"

    const-string v4, "0"

    invoke-virtual {v1, v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 408
    .end local v1    # "food":Lorg/json/JSONObject;
    .end local v2    # "jsonObject":Lorg/json/JSONObject;
    :goto_0
    return-object v3

    .line 402
    .restart local v1    # "food":Lorg/json/JSONObject;
    .restart local v2    # "jsonObject":Lorg/json/JSONObject;
    :cond_0
    new-instance v3, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodNotFoundException;

    invoke-direct {v3}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodNotFoundException;-><init>()V

    throw v3
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 404
    .end local v1    # "food":Lorg/json/JSONObject;
    .end local v2    # "jsonObject":Lorg/json/JSONObject;
    :catch_0
    move-exception v0

    .line 405
    .local v0, "e":Lorg/json/JSONException;
    sget-object v3, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->LOG_TAG:Ljava/lang/String;

    invoke-static {v3, v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 408
    .end local v0    # "e":Lorg/json/JSONException;
    :cond_1
    const-string v3, "0"

    goto :goto_0
.end method

.method public bridge synthetic parseCategorySearch(Ljava/lang/Object;)Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryResult;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 42
    check-cast p1, Ljava/lang/String;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->parseCategorySearch(Ljava/lang/String;)Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryResult;

    move-result-object v0

    return-object v0
.end method

.method public parseCategorySearch(Ljava/lang/String;)Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryResult;
    .locals 6
    .param p1, "requestResult"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 142
    sget-object v1, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->LOG_TAG:Ljava/lang/String;

    const-string v2, " parseCategorySearch"

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 144
    .local v0, "categories":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategory;>;"
    new-instance v1, Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategory;

    const-string v2, "0"

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->mResources:Landroid/content/res/Resources;

    const v4, 0x7f090789

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3, v5}, Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategory;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 145
    new-instance v1, Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategory;

    const-string v2, "1"

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->mResources:Landroid/content/res/Resources;

    const v4, 0x7f09078a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3, v5}, Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategory;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 146
    new-instance v1, Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryResult;

    invoke-direct {v1, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryResult;-><init>(Ljava/util/List;)V

    return-object v1
.end method

.method public bridge synthetic parseExtraFood(Ljava/lang/Object;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 42
    check-cast p1, Ljava/lang/String;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->parseExtraFood(Ljava/lang/String;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;

    move-result-object v0

    return-object v0
.end method

.method public parseExtraFood(Ljava/lang/String;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;
    .locals 12
    .param p1, "requestResult"    # Ljava/lang/String;

    .prologue
    .line 176
    sget-object v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->LOG_TAG:Ljava/lang/String;

    const-string v2, " parseExtraFood"

    invoke-static {v0, v2}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    new-instance v1, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;

    invoke-direct {v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;-><init>()V

    .line 178
    .local v1, "result":Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;
    if-eqz p1, :cond_0

    .line 180
    :try_start_0
    new-instance v10, Lorg/json/JSONObject;

    invoke-direct {v10, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 181
    .local v10, "jsonObject":Lorg/json/JSONObject;
    const-string v0, "food"

    invoke-virtual {v10, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v8

    .line 182
    .local v8, "food":Lorg/json/JSONObject;
    if-eqz v8, :cond_0

    .line 184
    const-string v0, "id"

    const/4 v2, 0x0

    invoke-virtual {v8, v0, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v9

    .line 186
    .local v9, "id":I
    const-string/jumbo v0, "name"

    const-string v2, ""

    invoke-virtual {v8, v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 187
    .local v11, "name":Ljava/lang/String;
    const/4 v0, -0x1

    if-eq v9, v0, :cond_0

    invoke-virtual {v11}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 188
    invoke-direct {p0, v8}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->parseFoodCalorie(Lorg/json/JSONObject;)F

    move-result v6

    .line 190
    .local v6, "calorie":F
    invoke-direct {p0, v1, v8}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->setUnitsDependingOnLiquid(Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;Lorg/json/JSONObject;)V

    .line 192
    invoke-direct {p0, v1, v6}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->setUnitsInfo(Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;F)V

    .line 193
    invoke-direct {p0, v1, v8}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->checkGoodUnitsUsingJSONArray(Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;Lorg/json/JSONObject;)V

    .line 194
    invoke-direct {p0, v1, v8}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->parseFat(Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;Lorg/json/JSONObject;)V

    .line 195
    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->setSaturatedValues(Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;)V

    .line 196
    invoke-direct {p0, v1, v8}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->parseFoodCholesterol(Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;Lorg/json/JSONObject;)V

    .line 197
    invoke-direct {p0, v1, v8}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->parseSodium(Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;Lorg/json/JSONObject;)V

    .line 198
    invoke-direct {p0, v1, v8}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->parsePotassium(Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;Lorg/json/JSONObject;)V

    .line 199
    invoke-direct {p0, v1, v8}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->parseCarbohydrate(Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;Lorg/json/JSONObject;)V

    .line 200
    invoke-direct {p0, v1, v8}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->parseDietaryFiber(Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;Lorg/json/JSONObject;)V

    .line 202
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setSugar(F)V

    .line 203
    invoke-direct {p0, v1, v8}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->parseProtein(Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;Lorg/json/JSONObject;)V

    .line 204
    invoke-direct {p0, v1, v8}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->parseVitaminA(Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;Lorg/json/JSONObject;)V

    .line 205
    invoke-direct {p0, v1, v8}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->parseVitaminC(Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;Lorg/json/JSONObject;)V

    .line 206
    invoke-direct {p0, v1, v8}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->parseCalcium(Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;Lorg/json/JSONObject;)V

    .line 207
    invoke-direct {p0, v1, v8}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->parseIron(Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;Lorg/json/JSONObject;)V

    .line 209
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->mResources:Landroid/content/res/Resources;

    const v2, 0x7f0900bf

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setUnitName(Ljava/lang/String;)V

    .line 211
    const-string v0, "big_group"

    const-string v2, ""

    invoke-virtual {v8, v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 212
    .local v4, "groupId":Ljava/lang/String;
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;

    const-string v2, ""

    const-string v3, ""

    const-string v5, ""

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;-><init>(Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 219
    .end local v4    # "groupId":Ljava/lang/String;
    .end local v6    # "calorie":F
    .end local v8    # "food":Lorg/json/JSONObject;
    .end local v9    # "id":I
    .end local v10    # "jsonObject":Lorg/json/JSONObject;
    .end local v11    # "name":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 215
    :catch_0
    move-exception v7

    .line 216
    .local v7, "e":Lorg/json/JSONException;
    sget-object v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->LOG_TAG:Ljava/lang/String;

    invoke-static {v0, v7}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 219
    .end local v7    # "e":Lorg/json/JSONException;
    :cond_0
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;-><init>(Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;)V

    goto :goto_0
.end method

.method public bridge synthetic parseFoodByCategorySearch(Ljava/lang/Object;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 42
    check-cast p1, Ljava/lang/String;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->parseFoodByCategorySearch(Ljava/lang/String;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult;

    move-result-object v0

    return-object v0
.end method

.method public parseFoodByCategorySearch(Ljava/lang/String;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult;
    .locals 2
    .param p1, "searchResult"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 137
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not permitted"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public parseFoodInfoFromExtraFoodInfoResult(Ljava/lang/String;Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;)Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    .locals 5
    .param p1, "requestResult"    # Ljava/lang/String;
    .param p2, "extraFoodInfoData"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;

    .prologue
    .line 412
    sget-object v3, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->LOG_TAG:Ljava/lang/String;

    const-string v4, " parseFoodInfoFromExtraFoodInfoResult"

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 413
    new-instance v1, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    invoke-direct {v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;-><init>()V

    .line 414
    .local v1, "foodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    if-eqz p1, :cond_0

    .line 416
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v4, "food"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 417
    .local v2, "jsonObject":Lorg/json/JSONObject;
    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->parseBaseFoodInfo(Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 422
    .end local v2    # "jsonObject":Lorg/json/JSONObject;
    :cond_0
    :goto_0
    invoke-virtual {p2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getDefaultNumber()F

    move-result v3

    invoke-virtual {p2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getGrammInKcal()F

    move-result v4

    div-float/2addr v3, v4

    invoke-virtual {v1, v3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setkCal(F)V

    .line 423
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getkCal()F

    move-result v4

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->createDefaultFoodDescription(Landroid/content/res/Resources;F)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setDescription(Ljava/lang/String;)V

    .line 424
    return-object v1

    .line 418
    :catch_0
    move-exception v0

    .line 419
    .local v0, "e":Lorg/json/JSONException;
    sget-object v3, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->LOG_TAG:Ljava/lang/String;

    invoke-static {v3, v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public bridge synthetic parseFoodSearch(Ljava/lang/Object;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 42
    check-cast p1, Ljava/lang/String;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->parseFoodSearch(Ljava/lang/String;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult;

    move-result-object v0

    return-object v0
.end method

.method public parseFoodSearch(Ljava/lang/String;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult;
    .locals 9
    .param p1, "requestResult"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/ParseException;
        }
    .end annotation

    .prologue
    .line 115
    sget-object v7, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->LOG_TAG:Ljava/lang/String;

    const-string v8, " parseFoodSearch"

    invoke-static {v7, v8}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 117
    .local v5, "result":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;>;"
    const/4 v6, 0x0

    .line 118
    .local v6, "resultSize":I
    if-eqz p1, :cond_0

    .line 120
    :try_start_0
    new-instance v3, Lorg/json/JSONArray;

    new-instance v7, Lorg/json/JSONObject;

    invoke-direct {v7, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v8, "foods"

    invoke-virtual {v7, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v3, v7}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 121
    .local v3, "jsonArray":Lorg/json/JSONArray;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v7

    if-ge v2, v7, :cond_0

    .line 122
    new-instance v1, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    invoke-direct {v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;-><init>()V

    .line 123
    .local v1, "foodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v6

    .line 124
    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 125
    .local v4, "jsonObject":Lorg/json/JSONObject;
    invoke-direct {p0, v1, v4}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->parseBaseFoodInfo(Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;Lorg/json/JSONObject;)V

    .line 126
    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 121
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 128
    .end local v1    # "foodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    .end local v2    # "i":I
    .end local v3    # "jsonArray":Lorg/json/JSONArray;
    .end local v4    # "jsonObject":Lorg/json/JSONObject;
    :catch_0
    move-exception v0

    .line 129
    .local v0, "e":Lorg/json/JSONException;
    sget-object v7, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->LOG_TAG:Ljava/lang/String;

    invoke-static {v7, v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 132
    .end local v0    # "e":Lorg/json/JSONException;
    :cond_0
    new-instance v7, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult;

    invoke-direct {v7, v5, v6}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult;-><init>(Ljava/util/List;I)V

    return-object v7
.end method

.method public bridge synthetic parseSubCategorySearch(Ljava/lang/Object;)Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategoryResult;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 42
    check-cast p1, Ljava/lang/String;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->parseSubCategorySearch(Ljava/lang/String;)Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategoryResult;

    move-result-object v0

    return-object v0
.end method

.method public parseSubCategorySearch(Ljava/lang/String;)Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategoryResult;
    .locals 11
    .param p1, "requestResult"    # Ljava/lang/String;

    .prologue
    .line 151
    sget-object v8, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->LOG_TAG:Ljava/lang/String;

    const-string v9, " parseSubCategorySearch"

    invoke-static {v8, v9}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 153
    .local v4, "subCategories":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategory;>;"
    if-eqz p1, :cond_0

    .line 155
    :try_start_0
    new-instance v2, Lorg/json/JSONArray;

    new-instance v8, Lorg/json/JSONObject;

    invoke-direct {v8, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v9, "food_groups"

    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v2, v8}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 156
    .local v2, "jsonArray":Lorg/json/JSONArray;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v8

    if-ge v1, v8, :cond_0

    .line 157
    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    .line 158
    .local v3, "jsonObject":Lorg/json/JSONObject;
    const-string v8, "id"

    const/4 v9, 0x0

    invoke-virtual {v3, v8, v9}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v6

    .line 159
    .local v6, "subCategoryId":I
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "name"

    const-string v10, ""

    invoke-virtual {v3, v9, v10}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "BOOHEE"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 160
    .local v7, "subCategoryName":Ljava/lang/String;
    const-string v8, "appraise"

    const-string v9, ""

    invoke-virtual {v3, v8, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 161
    .local v5, "subCategoryDescription":Ljava/lang/String;
    new-instance v8, Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategory;

    invoke-direct {v8, v7}, Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategory;-><init>(Ljava/lang/String;)V

    invoke-interface {v4, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 163
    sget-object v8, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->LOG_TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "subcategoryId = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "  subcategoryName = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "    subCategoryDescription = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 156
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 167
    .end local v1    # "i":I
    .end local v2    # "jsonArray":Lorg/json/JSONArray;
    .end local v3    # "jsonObject":Lorg/json/JSONObject;
    .end local v5    # "subCategoryDescription":Ljava/lang/String;
    .end local v6    # "subCategoryId":I
    .end local v7    # "subCategoryName":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 168
    .local v0, "e":Lorg/json/JSONException;
    sget-object v8, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->LOG_TAG:Ljava/lang/String;

    invoke-static {v8, v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 171
    .end local v0    # "e":Lorg/json/JSONException;
    :cond_0
    new-instance v8, Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategoryResult;

    invoke-direct {v8, v4}, Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategoryResult;-><init>(Ljava/util/List;)V

    return-object v8
.end method

.method public setUnitClory(Ljava/lang/String;)V
    .locals 4
    .param p1, "calory"    # Ljava/lang/String;

    .prologue
    .line 380
    :try_start_0
    invoke-static {p1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    sput v1, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->FOOD_UNIT_CALORY:F
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 384
    :goto_0
    sget-object v1, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " FOOD_UNIT_CALORY"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->FOOD_UNIT_CALORY:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 385
    return-void

    .line 381
    :catch_0
    move-exception v0

    .line 382
    .local v0, "nfe":Ljava/lang/NumberFormatException;
    sget-object v1, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->LOG_TAG:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
