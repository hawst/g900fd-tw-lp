.class Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$17;
.super Landroid/os/CountDownTimer;
.source "HeartrateSummaryFragmentNew.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;JJ)V
    .locals 0
    .param p2, "x0"    # J
    .param p4, "x1"    # J

    .prologue
    .line 1805
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$17;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    invoke-direct {p0, p2, p3, p4, p5}, Landroid/os/CountDownTimer;-><init>(JJ)V

    return-void
.end method


# virtual methods
.method public onFinish()V
    .locals 2

    .prologue
    .line 1816
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$17;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->isFingerDetected:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$2400(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$17;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    iget v0, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartRate:I

    if-lez v0, :cond_1

    .line 1821
    :cond_0
    :goto_0
    return-void

    .line 1819
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$17;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->isErrorByTimeout:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$4302(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;Z)Z

    .line 1820
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$17;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    const/4 v1, -0x6

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->stopSensorAndUpdateUI(I)V

    goto :goto_0
.end method

.method public onTick(J)V
    .locals 3
    .param p1, "millisUntilFinished"    # J

    .prologue
    .line 1809
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$17;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->isFingerDetected:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$2400(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$17;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    iget-object v0, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstStatus:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$17;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$2700(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090c05

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1810
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$17;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$17;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->isRetryMeasuring:Z
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$4100(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Z

    move-result v1

    # invokes: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->changeUIReadyToMeasuring(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$4200(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;Z)V

    .line 1812
    :cond_0
    return-void
.end method
