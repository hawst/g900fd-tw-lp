.class Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$20;
.super Ljava/lang/Object;
.source "HeartrateSummaryFragmentNew.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->showErrorDialog(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)V
    .locals 0

    .prologue
    .line 1930
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$20;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/app/Activity;)V
    .locals 3
    .param p1, "parentActivity"    # Landroid/app/Activity;

    .prologue
    const/4 v2, 0x0

    .line 1933
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$20;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    # setter for: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mErrorDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v0, v2}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$4402(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 1934
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$20;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->startSensorAndDelayUIForDetectedFinger(I)V

    .line 1935
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$20;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mAnimationViewErr0:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$4700(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1936
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$20;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mAnimationViewErr1:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$4800(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1937
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$20;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    # setter for: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mAnimationViewErr0:Landroid/widget/ImageView;
    invoke-static {v0, v2}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$4702(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;Landroid/widget/ImageView;)Landroid/widget/ImageView;

    .line 1938
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$20;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    # setter for: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mAnimationViewErr1:Landroid/widget/ImageView;
    invoke-static {v0, v2}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$4802(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;Landroid/widget/ImageView;)Landroid/widget/ImageView;

    .line 1939
    return-void
.end method
