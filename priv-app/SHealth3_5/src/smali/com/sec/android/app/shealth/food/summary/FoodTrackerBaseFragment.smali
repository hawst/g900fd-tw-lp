.class public Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;
.super Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;
.source "FoodTrackerBaseFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IContentInitializatorGetter;
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerProvider;
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/ISaveChosenItemListenerGetter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$DateChangeReceiver;
    }
.end annotation


# static fields
.field private static final ADD_CUSTOM_FOOD_CODE:I = 0x223d

.field private static final BAR_CODE_REQUEST_CODE:I = 0x223e

.field public static final DEFAULT_AMOUNT:I = 0x1

.field private static final LOG_TAG:Ljava/lang/String;

.field public static final NO_ID:I = -0x1

.field private static final OUT_OF_RANGE_POPUP:Ljava/lang/String; = "out_of_range_popup"

.field private static final QUICK_INPUT_POP_UP:Ljava/lang/String; = "quick_input_poup"

.field private static final SKIPPED_MEAL_CONFIRM_DIALOG:Ljava/lang/String; = "skipped_meal_confirm_dialog"

.field private static final TYPES_MAP:[I


# instance fields
.field private mAudioManager:Landroid/media/AudioManager;

.field private mDailyMeals:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;",
            ">;"
        }
    .end annotation
.end field

.field private mDateChangeReceiver:Landroid/content/BroadcastReceiver;

.field protected mFoodInfoDao:Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;

.field private mFoodTrackerBottomContainer:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBottomContainer;

.field private mFoodTrackerMainContainer:Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;

.field private mFoodTrackerTopContainer:Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer;

.field private mIncorrectRangeToast:Landroid/widget/Toast;

.field private mListPopup:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

.field private mListPopupShown:Z

.field private mMaxAcceptableCaloriesValue:F

.field private mMealDataHolder:Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;

.field protected mMealItemDao:Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;

.field private mMealType:I

.field private mMealTypeSelector:Landroid/widget/TextView;

.field private mMealTypeSelectorLL:Landroid/widget/LinearLayout;

.field mQuickInputController:Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;

.field mQuickInputPanel:Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;

.field private mSelectedIndexInPopup:I

.field mSkippedMealConfirmController:Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;

.field private mStringValuesMealTypes:[Ljava/lang/String;

.field private mTimeToInit:J

.field private mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

.field protected mealDao:Lcom/sec/android/app/shealth/food/fooddao/MealDao;

.field protected mealImageDao:Lcom/sec/android/app/shealth/food/fooddao/MealImageDao;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 101
    const-class v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->getLogTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->LOG_TAG:Ljava/lang/String;

    .line 143
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->TYPES_MAP:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x186a1
        0x186a2
        0x186a3
        0x186a4
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 99
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;-><init>()V

    .line 135
    const v0, 0x186a1

    iput v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mMealType:I

    .line 139
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mListPopupShown:Z

    .line 769
    new-instance v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$4;-><init>(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mQuickInputController:Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;

    .line 809
    new-instance v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$5;-><init>(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mSkippedMealConfirmController:Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;)Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    .prologue
    .line 99
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->initData()V

    return-void
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mMealTypeSelector:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 99
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mMealTypeSelector:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$1200(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mMealTypeSelectorLL:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$1202(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;
    .param p1, "x1"    # Landroid/widget/LinearLayout;

    .prologue
    .line 99
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mMealTypeSelectorLL:Landroid/widget/LinearLayout;

    return-object p1
.end method

.method static synthetic access$1300(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;)[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mStringValuesMealTypes:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1302(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;
    .param p1, "x1"    # [Ljava/lang/String;

    .prologue
    .line 99
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mStringValuesMealTypes:[Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1400(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    .prologue
    .line 99
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->handleSelectedIndexForListPopup()V

    return-void
.end method

.method static synthetic access$1600(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    .prologue
    .line 99
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mListPopupShown:Z

    return v0
.end method

.method static synthetic access$1700(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;)Lcom/sec/android/app/shealth/common/commonui/ListPopup;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mListPopup:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    return-object v0
.end method

.method static synthetic access$1702(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;Lcom/sec/android/app/shealth/common/commonui/ListPopup;)Lcom/sec/android/app/shealth/common/commonui/ListPopup;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    .prologue
    .line 99
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mListPopup:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    return-object p1
.end method

.method static synthetic access$1800(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    .prologue
    .line 99
    iget v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mSelectedIndexInPopup:I

    return v0
.end method

.method static synthetic access$1900()[I
    .locals 1

    .prologue
    .line 99
    sget-object v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->TYPES_MAP:[I

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    .prologue
    .line 99
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->initQuickInputPopUp()V

    return-void
.end method

.method static synthetic access$2000(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    .prologue
    .line 99
    iget v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mMaxAcceptableCaloriesValue:F

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;)Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    .prologue
    .line 99
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400()Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    sget-object v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->LOG_TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    .prologue
    .line 99
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->showMinMaxToast()V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    .prologue
    .line 99
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->updateQuickInput()V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    .prologue
    .line 99
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->isDaoInitNeeded()Z

    move-result v0

    return v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    .prologue
    .line 99
    iget v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mMealType:I

    return v0
.end method

.method static synthetic access$802(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;
    .param p1, "x1"    # I

    .prologue
    .line 99
    iput p1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mMealType:I

    return p1
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;
    .param p1, "x1"    # I

    .prologue
    .line 99
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->insertQuickInputMealDataToEmptyMeal(I)V

    return-void
.end method

.method private addCustomFood()V
    .locals 3

    .prologue
    .line 723
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 725
    .local v0, "intent":Landroid/content/Intent;
    const/16 v1, 0x223d

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 726
    return-void
.end method

.method private addToEditMealFragment(Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;)V
    .locals 12
    .param p1, "foodInfoData"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    .prologue
    const-wide/16 v1, -0x1

    .line 703
    new-instance v9, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;

    invoke-direct {v9}, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;-><init>()V

    .line 704
    .local v9, "dataHolder":Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getSelectedDate()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    invoke-virtual {v9, v3, v4}, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;->setMealTime(J)V

    .line 706
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 707
    .local v8, "bundle":Landroid/os/Bundle;
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 708
    .local v11, "meal":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;>;"
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getId()J

    move-result-wide v5

    const/high16 v7, 0x3f800000    # 1.0f

    move-wide v3, v1

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;-><init>(JJJF)V

    .line 710
    .local v0, "mealItemData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;
    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 711
    const-string v1, "MEAL_ITEM_LIST"

    invoke-virtual {v8, v1, v11}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 713
    new-instance v10, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;

    invoke-direct {v10, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 714
    .local v10, "intent":Landroid/content/Intent;
    const-string v1, "MEAL_DATA_HOLDER"

    invoke-virtual {v10, v1, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 715
    const/high16 v1, 0x24000000

    invoke-virtual {v10, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 717
    invoke-virtual {v10, v8}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 718
    invoke-virtual {p0, v10}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->startActivity(Landroid/content/Intent;)V

    .line 720
    return-void
.end method

.method private calculateSelectedIndexInListPopup(I)I
    .locals 3
    .param p1, "mealType"    # I

    .prologue
    .line 759
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v1, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->TYPES_MAP:[I

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 761
    sget-object v1, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->TYPES_MAP:[I

    aget v1, v1, v0

    if-ne v1, p1, :cond_0

    .line 763
    return v0

    .line 759
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 766
    :cond_1
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string/jumbo v2, "wrong mealType, value mealType must be one of ShealthContract.Constants.MealType"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private handleSelectedIndexForListPopup()V
    .locals 1

    .prologue
    .line 754
    iget v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mMealType:I

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->calculateSelectedIndexInListPopup(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mSelectedIndexInPopup:I

    .line 755
    return-void
.end method

.method private initContainers()V
    .locals 2

    .prologue
    .line 202
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0804a6

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer;

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mFoodTrackerTopContainer:Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer;

    .line 204
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0804a7

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mFoodTrackerMainContainer:Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;

    .line 206
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0804a8

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBottomContainer;

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mFoodTrackerBottomContainer:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBottomContainer;

    .line 208
    return-void
.end method

.method private initData()V
    .locals 21

    .prologue
    .line 403
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->initDao()V

    .line 405
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v1

    new-instance v2, Ljava/util/Date;

    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mTimeToInit:J

    invoke-direct {v2, v3, v4}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setSelecteddate(Ljava/util/Date;)V

    .line 406
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->setNextAndPrevDates()V

    .line 407
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mFoodTrackerMainContainer:Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;

    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mTimeToInit:J

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->resetMealsItemsContents(J)V

    .line 408
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mealDao:Lcom/sec/android/app/shealth/food/fooddao/MealDao;

    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mTimeToInit:J

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v2

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mTimeToInit:J

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v4

    invoke-interface {v1, v2, v3, v4, v5}, Lcom/sec/android/app/shealth/food/fooddao/MealDao;->getDataForPeriod(JJ)Ljava/util/List;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mDailyMeals:Ljava/util/List;

    .line 410
    const/4 v12, 0x0

    .line 411
    .local v12, "kCalSum":F
    const/16 v18, 0x0

    .line 412
    .local v18, "mProteinSum":F
    const/4 v13, 0x0

    .line 413
    .local v13, "mCarboSum":F
    const/4 v14, 0x0

    .line 415
    .local v14, "mFatSum":F
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mDailyMeals:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    .line 422
    .local v7, "data":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    invoke-virtual {v7}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getKcal()F

    move-result v1

    add-float/2addr v12, v1

    .line 423
    invoke-virtual {v7}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getKcal()F

    move-result v1

    const/4 v2, 0x0

    cmpg-float v1, v1, v2

    if-gez v1, :cond_0

    .line 424
    const-string v1, "FoodTrackerBaseFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Skipping Summary data for meal as total cal came negative: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getKcal()F

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "for meal: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getMealType()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 426
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mFoodTrackerMainContainer:Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getMealType()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->getDataByTypeId(I)Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;

    move-result-object v19

    .line 427
    .local v19, "meal":Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;
    move-object/from16 v0, v19

    invoke-virtual {v0, v7}, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;->setMealData(Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;)V

    .line 428
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mealImageDao:Lcom/sec/android/app/shealth/food/fooddao/MealImageDao;

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getId()J

    move-result-wide v2

    invoke-interface {v1, v2, v3}, Lcom/sec/android/app/shealth/food/fooddao/MealImageDao;->getImagesListByMealId(J)Ljava/util/List;

    move-result-object v11

    .line 429
    .local v11, "images":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;>;"
    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;

    .line 431
    .local v10, "image":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;
    invoke-virtual {v10}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;->getFilePath()Ljava/lang/String;

    move-result-object v20

    .line 432
    .local v20, "path":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    move-object/from16 v0, v20

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 434
    invoke-virtual/range {v19 .. v20}, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;->addBitmap(Ljava/lang/String;)V

    goto :goto_1

    .line 438
    .end local v10    # "image":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;
    .end local v20    # "path":Ljava/lang/String;
    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mMealItemDao:Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getId()J

    move-result-wide v2

    invoke-interface {v1, v2, v3}, Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;->getMealItemsListByMealId(J)Ljava/util/List;

    move-result-object v17

    .line 439
    .local v17, "mMealItems":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;>;"
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_3
    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    .line 442
    .local v16, "mMealItemData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mFoodInfoDao:Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->getFoodInfoId()J

    move-result-wide v2

    invoke-interface {v1, v2, v3}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;->getDataById(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->getMealItemCalorieAndNutrients(Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;Landroid/content/Context;)Lcom/sec/android/app/shealth/food/utils/FoodItemCaloriesAndNutrients;

    move-result-object v15

    .line 443
    .local v15, "mFoodItemCaloriesAndNutrients":Lcom/sec/android/app/shealth/food/utils/FoodItemCaloriesAndNutrients;
    if-eqz v15, :cond_3

    .line 445
    invoke-virtual {v15}, Lcom/sec/android/app/shealth/food/utils/FoodItemCaloriesAndNutrients;->getProteins()F

    move-result v1

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-lez v1, :cond_4

    invoke-virtual {v15}, Lcom/sec/android/app/shealth/food/utils/FoodItemCaloriesAndNutrients;->getProteins()F

    move-result v1

    :goto_3
    add-float v18, v18, v1

    .line 446
    invoke-virtual {v15}, Lcom/sec/android/app/shealth/food/utils/FoodItemCaloriesAndNutrients;->getCarbos()F

    move-result v1

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-lez v1, :cond_5

    invoke-virtual {v15}, Lcom/sec/android/app/shealth/food/utils/FoodItemCaloriesAndNutrients;->getCarbos()F

    move-result v1

    :goto_4
    add-float/2addr v13, v1

    .line 447
    invoke-virtual {v15}, Lcom/sec/android/app/shealth/food/utils/FoodItemCaloriesAndNutrients;->getFat()F

    move-result v1

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-lez v1, :cond_6

    invoke-virtual {v15}, Lcom/sec/android/app/shealth/food/utils/FoodItemCaloriesAndNutrients;->getFat()F

    move-result v1

    :goto_5
    add-float/2addr v14, v1

    goto :goto_2

    .line 445
    :cond_4
    const/4 v1, 0x0

    goto :goto_3

    .line 446
    :cond_5
    const/4 v1, 0x0

    goto :goto_4

    .line 447
    :cond_6
    const/4 v1, 0x0

    goto :goto_5

    .line 451
    .end local v15    # "mFoodItemCaloriesAndNutrients":Lcom/sec/android/app/shealth/food/utils/FoodItemCaloriesAndNutrients;
    .end local v16    # "mMealItemData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;
    :cond_7
    invoke-virtual {v7}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getId()J

    move-result-wide v1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mFoodInfoDao:Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mMealItemDao:Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;

    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->isMealSkipped(JLcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;)Z

    move-result v1

    move-object/from16 v0, v19

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;->setIsSkipped(Z)V

    goto/16 :goto_0

    .line 454
    .end local v7    # "data":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    .end local v9    # "i$":Ljava/util/Iterator;
    .end local v11    # "images":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;>;"
    .end local v17    # "mMealItems":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;>;"
    .end local v19    # "meal":Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;
    :cond_8
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mFoodTrackerMainContainer:Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->updateMealsItemsContents()V

    .line 455
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mFoodTrackerTopContainer:Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer;

    float-to-int v2, v12

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mTimeToInit:J

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->getGoalCaloriesByDate(Landroid/content/Context;J)F

    move-result v3

    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->convertToSingleDecimal(F)F

    move-result v4

    invoke-static {v13}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->convertToSingleDecimal(F)F

    move-result v5

    invoke-static {v14}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->convertToSingleDecimal(F)F

    move-result v6

    invoke-virtual/range {v1 .. v6}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer;->setCaloriesToDisplay(IFFFF)V

    .line 456
    return-void
.end method

.method private initListeners()V
    .locals 3

    .prologue
    .line 229
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mFoodTrackerBottomContainer:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBottomContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBottomContainer;->getFavouriteButton()Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$1;-><init>(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 239
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mFoodTrackerBottomContainer:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBottomContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBottomContainer;->getChartButton()Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/base/OnSingleClickListenerDecorator;

    new-instance v2, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$2;-><init>(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;)V

    invoke-direct {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/OnSingleClickListenerDecorator;-><init>(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 253
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mFoodTrackerBottomContainer:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBottomContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBottomContainer;->getQuickInputButton()Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$3;-><init>(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 265
    return-void
.end method

.method private initQuickInputPopUp()V
    .locals 3

    .prologue
    .line 730
    invoke-static {}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->calculateMealTypeAccordingToCurrentTime()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mMealType:I

    .line 731
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const v1, 0x7f0909c1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f030113

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f09004f

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setNeedRedraw(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->doNotDismissOnOkClick()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string/jumbo v2, "quick_input_poup"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 734
    return-void
.end method

.method private insertQuickInputMealDataToEmptyMeal(I)V
    .locals 14
    .param p1, "calorieValue"    # I

    .prologue
    const/16 v13, 0xc

    const/16 v12, 0xb

    .line 945
    const-wide/16 v5, 0x0

    .line 946
    .local v5, "mealId":J
    const-wide/16 v2, 0x0

    .line 947
    .local v2, "foodId":J
    const v9, 0x46cd6

    .line 948
    .local v9, "serverSourceType":I
    iget-object v10, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mQuickInputPanel:Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;

    if-eqz v10, :cond_0

    .line 950
    iget-object v10, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mQuickInputPanel:Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;

    invoke-virtual {v10}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->isSKippedSelected()Z

    move-result v10

    if-eqz v10, :cond_1

    const v9, 0x46cd7

    .line 954
    :cond_0
    :goto_0
    new-instance v8, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    invoke-direct {v8}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;-><init>()V

    .line 955
    .local v8, "quickInputMealData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    int-to-float v10, p1

    invoke-virtual {v8, v10}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->setKcal(F)V

    .line 956
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 957
    .local v1, "calendarForDefineTimeForMeal":Ljava/util/Calendar;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 958
    .local v0, "calendarForCurrentHourAndMinute":Ljava/util/Calendar;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-virtual {v0, v10, v11}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 960
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->getSelecteddate()Ljava/util/Date;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/Date;->getTime()J

    move-result-wide v10

    invoke-virtual {v1, v10, v11}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 961
    invoke-virtual {v0, v13}, Ljava/util/Calendar;->get(I)I

    move-result v10

    invoke-virtual {v1, v13, v10}, Ljava/util/Calendar;->set(II)V

    .line 962
    invoke-virtual {v0, v12}, Ljava/util/Calendar;->get(I)I

    move-result v10

    invoke-virtual {v1, v12, v10}, Ljava/util/Calendar;->set(II)V

    .line 963
    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->setMealTime(J)V

    .line 965
    iget v10, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mMealType:I

    invoke-virtual {v8, v10}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->setMealType(I)V

    .line 966
    iget-object v10, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mealDao:Lcom/sec/android/app/shealth/food/fooddao/MealDao;

    invoke-interface {v10, v8}, Lcom/sec/android/app/shealth/food/fooddao/MealDao;->insertOrUpdateData(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;)J

    .line 967
    invoke-virtual {v8}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getId()J

    move-result-wide v5

    .line 969
    new-instance v4, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    invoke-direct {v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;-><init>()V

    .line 970
    .local v4, "foodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0909c1

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v10}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setName(Ljava/lang/String;)V

    .line 971
    int-to-float v10, p1

    invoke-virtual {v4, v10}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setkCal(F)V

    .line 972
    invoke-virtual {v4, v9}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setServerSourceType(I)V

    .line 973
    iget-object v10, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mFoodInfoDao:Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;

    invoke-interface {v10, v4}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;->insertOrUpdateData(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;)J

    .line 974
    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getId()J

    move-result-wide v2

    .line 975
    new-instance v7, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    invoke-direct {v7}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;-><init>()V

    .line 976
    .local v7, "mealItemData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;
    invoke-virtual {v7, v5, v6}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->setMealId(J)V

    .line 977
    const/high16 v10, 0x3f800000    # 1.0f

    invoke-virtual {v7, v10}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->setAmount(F)V

    .line 978
    invoke-virtual {v7, v2, v3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->setFoodInfoId(J)V

    .line 979
    iget-object v10, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mMealItemDao:Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;

    invoke-interface {v10, v7}, Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;->insertData(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;)J

    .line 981
    return-void

    .line 950
    .end local v0    # "calendarForCurrentHourAndMinute":Ljava/util/Calendar;
    .end local v1    # "calendarForDefineTimeForMeal":Ljava/util/Calendar;
    .end local v4    # "foodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    .end local v7    # "mealItemData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;
    .end local v8    # "quickInputMealData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    :cond_1
    const v9, 0x46cd6

    goto/16 :goto_0
.end method

.method private isDaoInitNeeded()Z
    .locals 1

    .prologue
    .line 378
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mealDao:Lcom/sec/android/app/shealth/food/fooddao/MealDao;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mealImageDao:Lcom/sec/android/app/shealth/food/fooddao/MealImageDao;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mFoodInfoDao:Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mMealItemDao:Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isSelectedToday()Z
    .locals 5

    .prologue
    .line 533
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->getSelecteddate()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->getSystemDate()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->areTimesInSamePeriod(JJI)Z

    move-result v0

    return v0
.end method

.method private showMinMaxToast()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1430
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mIncorrectRangeToast:Landroid/widget/Toast;

    if-nez v1, :cond_0

    .line 1433
    const v1, 0x7f09098d

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    const/16 v3, 0x270f

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1437
    .local v0, "text":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1, v0, v5}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mIncorrectRangeToast:Landroid/widget/Toast;

    .line 1441
    .end local v0    # "text":Ljava/lang/String;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mIncorrectRangeToast:Landroid/widget/Toast;

    invoke-virtual {v1}, Landroid/widget/Toast;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowVisibility()I

    move-result v1

    if-eqz v1, :cond_1

    .line 1444
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mIncorrectRangeToast:Landroid/widget/Toast;

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 1447
    :cond_1
    return-void
.end method

.method private showSkippedMealConfirmDialog()V
    .locals 6

    .prologue
    .line 740
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const v1, 0x7f0909cd

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0909ce

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mMealType:I

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->getMealTypeString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string/jumbo v2, "skipped_meal_confirm_dialog"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 748
    return-void
.end method

.method private updateQuickInput()V
    .locals 23

    .prologue
    .line 832
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mQuickInputPanel:Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;

    move-object/from16 v18, v0

    if-eqz v18, :cond_5

    .line 835
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->initDao()V

    .line 836
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mQuickInputPanel:Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->getQuickInputCalorieValue()I

    move-result v3

    .line 837
    .local v3, "calorieValue":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mQuickInputPanel:Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->isSKippedSelected()Z

    move-result v18

    if-eqz v18, :cond_1

    const v17, 0x46cd7

    .line 838
    .local v17, "serverSourceType":I
    :goto_0
    const-wide/16 v13, 0x0

    .line 839
    .local v13, "mealId":J
    const-wide/16 v6, 0x0

    .line 841
    .local v6, "foodId":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mealDao:Lcom/sec/android/app/shealth/food/fooddao/MealDao;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mTimeToInit:J

    move-wide/from16 v19, v0

    invoke-static/range {v19 .. v20}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v19

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mTimeToInit:J

    move-wide/from16 v21, v0

    invoke-static/range {v21 .. v22}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v21

    invoke-interface/range {v18 .. v22}, Lcom/sec/android/app/shealth/food/fooddao/MealDao;->getDataForPeriod(JJ)Ljava/util/List;

    move-result-object v4

    .line 843
    .local v4, "dailyMealDatas":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;>;"
    const/4 v10, 0x0

    .line 844
    .local v10, "isMealExisting":Z
    const/4 v12, 0x0

    .line 845
    .local v12, "mealData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_2

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    .line 847
    .local v5, "data":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    invoke-virtual {v5}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getMealType()I

    move-result v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mMealType:I

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_0

    .line 850
    const/4 v10, 0x1

    .line 851
    move-object v12, v5

    .line 852
    invoke-virtual {v5}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getId()J

    move-result-wide v13

    goto :goto_1

    .line 837
    .end local v4    # "dailyMealDatas":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;>;"
    .end local v5    # "data":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    .end local v6    # "foodId":J
    .end local v9    # "i$":Ljava/util/Iterator;
    .end local v10    # "isMealExisting":Z
    .end local v12    # "mealData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    .end local v13    # "mealId":J
    .end local v17    # "serverSourceType":I
    :cond_1
    const v17, 0x46cd6

    goto :goto_0

    .line 856
    .restart local v4    # "dailyMealDatas":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;>;"
    .restart local v6    # "foodId":J
    .restart local v9    # "i$":Ljava/util/Iterator;
    .restart local v10    # "isMealExisting":Z
    .restart local v12    # "mealData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    .restart local v13    # "mealId":J
    .restart local v17    # "serverSourceType":I
    :cond_2
    if-nez v10, :cond_7

    .line 859
    const v11, 0x47c34f80    # 99999.0f

    .line 860
    .local v11, "max":F
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_3
    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_4

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    .line 861
    .restart local v5    # "data":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    invoke-virtual {v5}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getId()J

    move-result-wide v18

    const-wide/16 v20, -0x1

    cmp-long v18, v18, v20

    if-eqz v18, :cond_3

    .line 862
    invoke-virtual {v5}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getKcal()F

    move-result v18

    sub-float v11, v11, v18

    goto :goto_2

    .line 865
    .end local v5    # "data":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    :cond_4
    move-object/from16 v0, p0

    iput v11, v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mMaxAcceptableCaloriesValue:F

    .line 867
    int-to-float v0, v3

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mMaxAcceptableCaloriesValue:F

    move/from16 v19, v0

    cmpl-float v18, v18, v19

    if-lez v18, :cond_6

    .line 869
    new-instance v18, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v19

    const/16 v20, 0x0

    const v21, 0x7f0900e3

    invoke-direct/range {v18 .. v21}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;II)V

    const v19, 0x7f030009

    invoke-virtual/range {v18 .. v19}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v18

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v19

    const-string/jumbo v20, "out_of_range_popup"

    invoke-virtual/range {v18 .. v20}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 942
    .end local v3    # "calorieValue":I
    .end local v4    # "dailyMealDatas":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;>;"
    .end local v6    # "foodId":J
    .end local v9    # "i$":Ljava/util/Iterator;
    .end local v10    # "isMealExisting":Z
    .end local v11    # "max":F
    .end local v12    # "mealData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    .end local v13    # "mealId":J
    .end local v17    # "serverSourceType":I
    :cond_5
    :goto_3
    return-void

    .line 875
    .restart local v3    # "calorieValue":I
    .restart local v4    # "dailyMealDatas":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;>;"
    .restart local v6    # "foodId":J
    .restart local v9    # "i$":Ljava/util/Iterator;
    .restart local v10    # "isMealExisting":Z
    .restart local v11    # "max":F
    .restart local v12    # "mealData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    .restart local v13    # "mealId":J
    .restart local v17    # "serverSourceType":I
    :cond_6
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->insertQuickInputMealDataToEmptyMeal(I)V

    .line 876
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->initData()V

    goto :goto_3

    .line 883
    .end local v11    # "max":F
    :cond_7
    if-nez v3, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mQuickInputPanel:Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->isSKippedSelected()Z

    move-result v18

    if-eqz v18, :cond_8

    .line 886
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->showSkippedMealConfirmDialog()V

    goto :goto_3

    .line 893
    :cond_8
    const v11, 0x47c34f80    # 99999.0f

    .line 894
    .restart local v11    # "max":F
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_9
    :goto_4
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_a

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    .line 895
    .restart local v5    # "data":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    invoke-virtual {v5}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getId()J

    move-result-wide v18

    const-wide/16 v20, -0x1

    cmp-long v18, v18, v20

    if-eqz v18, :cond_9

    .line 896
    invoke-virtual {v5}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getKcal()F

    move-result v18

    sub-float v11, v11, v18

    goto :goto_4

    .line 899
    .end local v5    # "data":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    :cond_a
    move-object/from16 v0, p0

    iput v11, v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mMaxAcceptableCaloriesValue:F

    .line 901
    int-to-float v0, v3

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mMaxAcceptableCaloriesValue:F

    move/from16 v19, v0

    cmpl-float v18, v18, v19

    if-lez v18, :cond_b

    .line 903
    new-instance v18, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v19

    const/16 v20, 0x0

    const v21, 0x7f0900e3

    invoke-direct/range {v18 .. v21}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;II)V

    const v19, 0x7f030009

    invoke-virtual/range {v18 .. v19}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v18

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v19

    const-string/jumbo v20, "out_of_range_popup"

    invoke-virtual/range {v18 .. v20}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_3

    .line 911
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mMealItemDao:Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-interface {v0, v13, v14}, Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;->getMealItemsListByMealId(J)Ljava/util/List;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->size()I

    move-result v18

    const/16 v19, 0xf

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_c

    .line 912
    const v18, 0x7f090925

    const/16 v19, 0x1

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    const/16 v21, 0xf

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    aput-object v21, v19, v20

    move-object/from16 v0, p0

    move/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    .line 913
    .local v16, "message":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v18

    const/16 v19, 0x1

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    move/from16 v2, v19

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/widget/Toast;->show()V

    goto/16 :goto_3

    .line 917
    .end local v16    # "message":Ljava/lang/String;
    :cond_c
    int-to-float v0, v3

    move/from16 v18, v0

    invoke-virtual {v12}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getKcal()F

    move-result v19

    add-float v18, v18, v19

    move/from16 v0, v18

    invoke-virtual {v12, v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->setKcal(F)V

    .line 919
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mealDao:Lcom/sec/android/app/shealth/food/fooddao/MealDao;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-interface {v0, v12}, Lcom/sec/android/app/shealth/food/fooddao/MealDao;->insertOrUpdateData(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;)J

    .line 921
    new-instance v8, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    invoke-direct {v8}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;-><init>()V

    .line 922
    .local v8, "foodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    const v19, 0x7f0909c1

    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v8, v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setName(Ljava/lang/String;)V

    .line 923
    int-to-float v0, v3

    move/from16 v18, v0

    move/from16 v0, v18

    invoke-virtual {v8, v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setkCal(F)V

    .line 924
    move/from16 v0, v17

    invoke-virtual {v8, v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setServerSourceType(I)V

    .line 925
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mFoodInfoDao:Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-interface {v0, v8}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;->insertOrUpdateData(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;)J

    .line 927
    invoke-virtual {v8}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getId()J

    move-result-wide v6

    .line 928
    new-instance v15, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    invoke-direct {v15}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;-><init>()V

    .line 929
    .local v15, "mealItemData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;
    invoke-virtual {v15, v13, v14}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->setMealId(J)V

    .line 930
    const/high16 v18, 0x3f800000    # 1.0f

    move/from16 v0, v18

    invoke-virtual {v15, v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->setAmount(F)V

    .line 931
    invoke-virtual {v15, v6, v7}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->setFoodInfoId(J)V

    .line 932
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mMealItemDao:Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-interface {v0, v15}, Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;->insertData(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;)J

    .line 935
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->initData()V

    goto/16 :goto_3
.end method


# virtual methods
.method public assignFocus(Landroid/view/View;I)Z
    .locals 11
    .param p1, "focusedView"    # Landroid/view/View;
    .param p2, "keycode"    # I

    .prologue
    const v10, 0x7f0804ac

    const v9, 0x7f0804b8

    const v8, 0x7f0804ae

    const v7, 0x7f0804ad

    const v6, 0x7f0804b0

    .line 1129
    const/4 v3, 0x0

    .line 1130
    .local v3, "status":Z
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 1131
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_1

    .line 1134
    const/16 v4, 0x16

    if-ne p2, v4, :cond_d

    .line 1136
    invoke-virtual {v0, v10}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-ne p1, v4, :cond_3

    .line 1138
    invoke-virtual {v0, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 1140
    invoke-virtual {v0, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->requestFocus()Z

    .line 1143
    :cond_0
    const/4 v3, 0x1

    .line 1420
    :cond_1
    :goto_0
    if-eqz v3, :cond_2

    .line 1422
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mAudioManager:Landroid/media/AudioManager;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/media/AudioManager;->playSoundEffect(I)V

    .line 1425
    :cond_2
    return v3

    .line 1145
    :cond_3
    invoke-virtual {v0, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-ne p1, v4, :cond_5

    .line 1147
    invoke-virtual {v0, v8}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_4

    .line 1149
    invoke-virtual {v0, v8}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->requestFocus()Z

    .line 1152
    :cond_4
    const/4 v3, 0x1

    goto :goto_0

    .line 1154
    :cond_5
    invoke-virtual {v0, v8}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-ne p1, v4, :cond_7

    .line 1156
    const v4, 0x7f0804af

    invoke-virtual {v0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_6

    .line 1158
    const v4, 0x7f0804af

    invoke-virtual {v0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->requestFocus()Z

    .line 1161
    :cond_6
    const/4 v3, 0x1

    goto :goto_0

    .line 1163
    :cond_7
    const v4, 0x7f0804af

    invoke-virtual {v0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-ne p1, v4, :cond_8

    .line 1166
    const v4, 0x7f0804a9

    invoke-virtual {v0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->requestFocus()Z

    .line 1167
    const/4 v3, 0x1

    goto :goto_0

    .line 1169
    :cond_8
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v4

    const v5, 0x7f080321

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-ne p1, v4, :cond_a

    .line 1171
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v4

    const v5, 0x7f080322

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_9

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v4

    const v5, 0x7f080322

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->isEnabled()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 1173
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v4

    const v5, 0x7f080322

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->requestFocus()Z

    .line 1179
    :goto_1
    const/4 v3, 0x1

    goto/16 :goto_0

    .line 1177
    :cond_9
    invoke-virtual {v0, v10}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->requestFocus()Z

    goto :goto_1

    .line 1181
    :cond_a
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v4

    const v5, 0x7f080322

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-ne p1, v4, :cond_c

    .line 1183
    invoke-virtual {v0, v10}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_b

    .line 1185
    invoke-virtual {v0, v10}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->requestFocus()Z

    .line 1188
    :cond_b
    const/4 v3, 0x1

    goto/16 :goto_0

    .line 1190
    :cond_c
    const v4, 0x7f0804a9

    invoke-virtual {v0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-ne p1, v4, :cond_1

    .line 1193
    const v4, 0x7f0804aa

    invoke-virtual {v0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->requestFocus()Z

    .line 1194
    const/4 v3, 0x1

    goto/16 :goto_0

    .line 1197
    :cond_d
    const/16 v4, 0x15

    if-ne p2, v4, :cond_15

    .line 1199
    const v4, 0x7f0804af

    invoke-virtual {v0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-ne p1, v4, :cond_f

    .line 1201
    invoke-virtual {v0, v8}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_e

    .line 1203
    invoke-virtual {v0, v8}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->requestFocus()Z

    .line 1206
    :cond_e
    const/4 v3, 0x1

    goto/16 :goto_0

    .line 1208
    :cond_f
    invoke-virtual {v0, v8}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-ne p1, v4, :cond_11

    .line 1210
    invoke-virtual {v0, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_10

    .line 1212
    invoke-virtual {v0, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->requestFocus()Z

    .line 1215
    :cond_10
    const/4 v3, 0x1

    goto/16 :goto_0

    .line 1217
    :cond_11
    invoke-virtual {v0, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-ne p1, v4, :cond_13

    .line 1219
    invoke-virtual {v0, v10}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_12

    .line 1221
    invoke-virtual {v0, v10}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->requestFocus()Z

    .line 1224
    :cond_12
    const/4 v3, 0x1

    goto/16 :goto_0

    .line 1226
    :cond_13
    invoke-virtual {v0, v10}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-ne p1, v4, :cond_1

    .line 1228
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v4

    const v5, 0x7f080322

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_14

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v4

    const v5, 0x7f080322

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->isEnabled()Z

    move-result v4

    if-eqz v4, :cond_14

    .line 1230
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v4

    const v5, 0x7f080322

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->requestFocus()Z

    .line 1237
    :goto_2
    const/4 v3, 0x1

    goto/16 :goto_0

    .line 1234
    :cond_14
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v4

    const v5, 0x7f080321

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->requestFocus()Z

    goto :goto_2

    .line 1241
    :cond_15
    const/16 v4, 0x14

    if-ne p2, v4, :cond_23

    .line 1244
    const v4, 0x7f0804af

    invoke-virtual {v0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eq p1, v4, :cond_16

    const v4, 0x7f0804af

    invoke-virtual {v0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eq p1, v4, :cond_16

    const v4, 0x7f0804af

    invoke-virtual {v0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    const v5, 0x7f0804b1

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-ne p1, v4, :cond_17

    .line 1247
    :cond_16
    const v4, 0x7f0804a9

    invoke-virtual {v0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->requestFocus()Z

    .line 1248
    const/4 v3, 0x1

    goto/16 :goto_0

    .line 1250
    :cond_17
    invoke-virtual {v0, v10}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eq p1, v4, :cond_18

    invoke-virtual {v0, v10}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eq p1, v4, :cond_18

    invoke-virtual {v0, v10}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    const v5, 0x7f0804b1

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-ne p1, v4, :cond_1a

    .line 1252
    :cond_18
    invoke-virtual {v0, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_19

    .line 1254
    invoke-virtual {v0, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->requestFocus()Z

    .line 1256
    :cond_19
    const/4 v3, 0x1

    goto/16 :goto_0

    .line 1259
    :cond_1a
    invoke-virtual {v0, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eq p1, v4, :cond_1b

    invoke-virtual {v0, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eq p1, v4, :cond_1b

    invoke-virtual {v0, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    const v5, 0x7f0804b1

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-ne p1, v4, :cond_1d

    .line 1262
    :cond_1b
    invoke-virtual {v0, v8}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_1c

    .line 1264
    invoke-virtual {v0, v8}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->requestFocus()Z

    .line 1266
    :cond_1c
    const/4 v3, 0x1

    goto/16 :goto_0

    .line 1269
    :cond_1d
    invoke-virtual {v0, v8}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eq p1, v4, :cond_1e

    invoke-virtual {v0, v8}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eq p1, v4, :cond_1e

    invoke-virtual {v0, v8}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    const v5, 0x7f0804b1

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-ne p1, v4, :cond_20

    .line 1272
    :cond_1e
    const v4, 0x7f0804af

    invoke-virtual {v0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_1f

    .line 1274
    const v4, 0x7f0804af

    invoke-virtual {v0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->requestFocus()Z

    .line 1276
    :cond_1f
    const/4 v3, 0x1

    goto/16 :goto_0

    .line 1279
    :cond_20
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v4

    const v5, 0x7f080322

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eq p1, v4, :cond_21

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v4

    const v5, 0x7f080321

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eq p1, v4, :cond_21

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v4

    const v5, 0x7f08031f

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-ne p1, v4, :cond_1

    .line 1281
    :cond_21
    invoke-virtual {v0, v10}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_22

    .line 1283
    invoke-virtual {v0, v10}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->requestFocus()Z

    .line 1286
    :cond_22
    const/4 v3, 0x1

    goto/16 :goto_0

    .line 1290
    :cond_23
    const/16 v4, 0x13

    if-ne p2, v4, :cond_32

    .line 1292
    invoke-virtual {v0, v10}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eq p1, v4, :cond_24

    invoke-virtual {v0, v10}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eq p1, v4, :cond_24

    invoke-virtual {v0, v10}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    const v5, 0x7f0804b1

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-ne p1, v4, :cond_26

    .line 1294
    :cond_24
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v4

    const v5, 0x7f08031f

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 1295
    .local v2, "leftArrow":Landroid/view/View;
    if-eqz v2, :cond_25

    invoke-virtual {v2}, Landroid/view/View;->isEnabled()Z

    move-result v4

    if-eqz v4, :cond_25

    .line 1297
    invoke-virtual {v2}, Landroid/view/View;->requestFocus()Z

    .line 1298
    const/4 v3, 0x1

    goto/16 :goto_0

    .line 1302
    :cond_25
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v4

    const v5, 0x7f080321

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1303
    .local v1, "dateSwitcher":Landroid/view/View;
    if-eqz v1, :cond_1

    .line 1305
    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    .line 1306
    const/4 v3, 0x1

    goto/16 :goto_0

    .line 1311
    .end local v1    # "dateSwitcher":Landroid/view/View;
    .end local v2    # "leftArrow":Landroid/view/View;
    :cond_26
    invoke-virtual {v0, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eq p1, v4, :cond_27

    invoke-virtual {v0, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eq p1, v4, :cond_27

    invoke-virtual {v0, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    const v5, 0x7f0804b1

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-ne p1, v4, :cond_29

    .line 1314
    :cond_27
    invoke-virtual {v0, v10}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_28

    .line 1316
    invoke-virtual {v0, v10}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->requestFocus()Z

    .line 1318
    :cond_28
    const/4 v3, 0x1

    goto/16 :goto_0

    .line 1321
    :cond_29
    invoke-virtual {v0, v8}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eq p1, v4, :cond_2a

    invoke-virtual {v0, v8}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eq p1, v4, :cond_2a

    invoke-virtual {v0, v8}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    const v5, 0x7f0804b1

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-ne p1, v4, :cond_2c

    .line 1323
    :cond_2a
    invoke-virtual {v0, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_2b

    .line 1325
    invoke-virtual {v0, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->requestFocus()Z

    .line 1327
    :cond_2b
    const/4 v3, 0x1

    goto/16 :goto_0

    .line 1330
    :cond_2c
    const v4, 0x7f0804af

    invoke-virtual {v0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eq p1, v4, :cond_2d

    const v4, 0x7f0804af

    invoke-virtual {v0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eq p1, v4, :cond_2d

    const v4, 0x7f0804af

    invoke-virtual {v0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    const v5, 0x7f0804b1

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-ne p1, v4, :cond_2f

    .line 1332
    :cond_2d
    invoke-virtual {v0, v8}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_2e

    .line 1334
    invoke-virtual {v0, v8}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->requestFocus()Z

    .line 1336
    :cond_2e
    const/4 v3, 0x1

    goto/16 :goto_0

    .line 1338
    :cond_2f
    const v4, 0x7f0804a9

    invoke-virtual {v0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eq p1, v4, :cond_30

    const v4, 0x7f0804aa

    invoke-virtual {v0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eq p1, v4, :cond_30

    const v4, 0x7f0804ab

    invoke-virtual {v0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-ne p1, v4, :cond_1

    .line 1340
    :cond_30
    const v4, 0x7f0804af

    invoke-virtual {v0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_31

    .line 1342
    const v4, 0x7f0804af

    invoke-virtual {v0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->requestFocus()Z

    .line 1344
    :cond_31
    const/4 v3, 0x1

    goto/16 :goto_0

    .line 1347
    :cond_32
    const/16 v4, 0x3d

    if-ne p2, v4, :cond_1

    .line 1350
    const v4, 0x7f0804af

    invoke-virtual {v0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-ne p1, v4, :cond_34

    .line 1352
    const v4, 0x7f0804af

    invoke-virtual {v0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    const v5, 0x7f0804b1

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_33

    .line 1354
    const v4, 0x7f0804af

    invoke-virtual {v0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    const v5, 0x7f0804b1

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->requestFocus()Z

    .line 1356
    :cond_33
    const/4 v3, 0x1

    goto/16 :goto_0

    .line 1358
    :cond_34
    invoke-virtual {v0, v8}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-ne p1, v4, :cond_36

    .line 1360
    invoke-virtual {v0, v8}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    const v5, 0x7f0804b1

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_35

    .line 1362
    invoke-virtual {v0, v8}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    const v5, 0x7f0804b1

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->requestFocus()Z

    .line 1364
    :cond_35
    const/4 v3, 0x1

    goto/16 :goto_0

    .line 1366
    :cond_36
    invoke-virtual {v0, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-ne p1, v4, :cond_38

    .line 1368
    invoke-virtual {v0, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    const v5, 0x7f0804b1

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_37

    .line 1370
    invoke-virtual {v0, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    const v5, 0x7f0804b1

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->requestFocus()Z

    .line 1373
    :cond_37
    const/4 v3, 0x1

    goto/16 :goto_0

    .line 1375
    :cond_38
    invoke-virtual {v0, v10}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-ne p1, v4, :cond_3a

    .line 1377
    invoke-virtual {v0, v10}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    const v5, 0x7f0804b1

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_39

    .line 1379
    invoke-virtual {v0, v10}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    const v5, 0x7f0804b1

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->requestFocus()Z

    .line 1382
    :cond_39
    const/4 v3, 0x1

    goto/16 :goto_0

    .line 1384
    :cond_3a
    invoke-virtual {v0, v10}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-ne p1, v4, :cond_3c

    .line 1387
    invoke-virtual {v0, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_3b

    .line 1389
    invoke-virtual {v0, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->requestFocus()Z

    .line 1391
    :cond_3b
    const/4 v3, 0x1

    goto/16 :goto_0

    .line 1393
    :cond_3c
    invoke-virtual {v0, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-ne p1, v4, :cond_3e

    .line 1396
    invoke-virtual {v0, v8}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_3d

    .line 1398
    invoke-virtual {v0, v8}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->requestFocus()Z

    .line 1400
    :cond_3d
    const/4 v3, 0x1

    goto/16 :goto_0

    .line 1402
    :cond_3e
    invoke-virtual {v0, v8}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-ne p1, v4, :cond_40

    .line 1404
    const v4, 0x7f0804af

    invoke-virtual {v0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_3f

    .line 1406
    const v4, 0x7f0804af

    invoke-virtual {v0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->requestFocus()Z

    .line 1409
    :cond_3f
    const/4 v3, 0x1

    goto/16 :goto_0

    .line 1411
    :cond_40
    const v4, 0x7f0804af

    invoke-virtual {v0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-ne p1, v4, :cond_1

    .line 1414
    const v4, 0x7f0804a9

    invoke-virtual {v0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->requestFocus()Z

    .line 1415
    const/4 v3, 0x1

    goto/16 :goto_0
.end method

.method public createIntentCalculatingMealType(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "clazz"    # Ljava/lang/Class;

    .prologue
    .line 347
    invoke-static {}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->calculateMealTypeAccordingToCurrentTime()I

    move-result v0

    .line 349
    .local v0, "mealType":I
    int-to-long v1, v0

    invoke-virtual {p0, p1, p2, v1, v2}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->createIntentForMealType(Landroid/content/Context;Ljava/lang/Class;J)Landroid/content/Intent;

    move-result-object v1

    return-object v1
.end method

.method public createIntentForMealType(Landroid/content/Context;Ljava/lang/Class;J)Landroid/content/Intent;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "clazz"    # Ljava/lang/Class;
    .param p3, "mealType"    # J

    .prologue
    .line 321
    if-nez p1, :cond_0

    .line 322
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "Context should be not null"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 324
    :cond_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, p1, p2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 325
    .local v1, "intent":Landroid/content/Intent;
    new-instance v0, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;-><init>()V

    .line 326
    .local v0, "dataHolder":Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;
    invoke-virtual {p0, p3, p4}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getMealIdByMealTypeId(J)J

    move-result-wide v2

    .line 327
    .local v2, "mealId":J
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-gez v4, :cond_1

    const-wide/high16 v2, -0x8000000000000000L

    .end local v2    # "mealId":J
    :cond_1
    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;->setMealId(J)V

    .line 328
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->selectedDate:Ljava/util/Date;

    invoke-static {v4}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->isDateToday(Ljava/util/Date;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    :goto_0
    invoke-virtual {v0, v4, v5}, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;->setMealTime(J)V

    .line 330
    invoke-virtual {v0, p3, p4}, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;->setMealType(J)V

    .line 331
    const-string v4, "MEAL_DATA_HOLDER"

    invoke-virtual {v1, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 332
    return-object v1

    .line 328
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->selectedDate:Ljava/util/Date;

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    goto :goto_0
.end method

.method public createIntentForQuickInput(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "clazz"    # Ljava/lang/Class;

    .prologue
    .line 297
    if-nez p1, :cond_0

    .line 298
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Context should be not null"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 300
    :cond_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, p1, p2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 301
    .local v1, "intent":Landroid/content/Intent;
    new-instance v0, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;-><init>()V

    .line 302
    .local v0, "dataHolder":Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getSelectedDate()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;->setMealTime(J)V

    .line 303
    const-string v2, "MEAL_DATA_HOLDER"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 304
    return-object v1
.end method

.method public createIntentForQuickInput(Ljava/lang/Class;)Landroid/content/Intent;
    .locals 4
    .param p1, "clazz"    # Ljava/lang/Class;

    .prologue
    .line 268
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2, p1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 269
    .local v1, "intent":Landroid/content/Intent;
    new-instance v0, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;-><init>()V

    .line 270
    .local v0, "dataHolder":Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getSelectedDate()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;->setMealTime(J)V

    .line 271
    const-string v2, "MEAL_DATA_HOLDER"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 272
    return-object v1
.end method

.method protected getCalendarActivityClass()Ljava/lang/Class;
    .locals 1

    .prologue
    .line 569
    const-class v0, Lcom/sec/android/app/shealth/food/summary/FoodCalendarActivity;

    return-object v0
.end method

.method protected getColumnNameForTime()Ljava/lang/String;
    .locals 1

    .prologue
    .line 564
    const-string/jumbo v0, "sample_time"

    return-object v0
.end method

.method public getContentInitializationListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 986
    const-string/jumbo v0, "quick_input_poup"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 987
    new-instance v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;-><init>(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;)V

    .line 1114
    :goto_0
    return-object v0

    .line 1101
    :cond_0
    const-string/jumbo v0, "out_of_range_popup"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1102
    new-instance v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$7;-><init>(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;)V

    goto :goto_0

    .line 1114
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getContentURI()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 559
    sget-object v0, Lcom/samsung/android/sdk/health/content/ShealthContract$Meal;->CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method

.method protected getContentView(Landroid/content/Context;)Landroid/view/View;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 383
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 384
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v1, 0x7f03011a

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    return-object v1
.end method

.method public getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 650
    const-string/jumbo v0, "quick_input_poup"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 651
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mQuickInputController:Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;

    .line 656
    :goto_0
    return-object v0

    .line 653
    :cond_0
    const-string/jumbo v0, "skipped_meal_confirm_dialog"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 654
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mSkippedMealConfirmController:Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;

    goto :goto_0

    .line 656
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mFoodTrackerMainContainer:Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;

    move-result-object v0

    goto :goto_0
.end method

.method public getMealIdByMealTypeId(J)J
    .locals 4
    .param p1, "mealType"    # J

    .prologue
    .line 616
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mDailyMeals:Ljava/util/List;

    if-eqz v2, :cond_1

    .line 617
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mDailyMeals:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    .line 618
    .local v1, "mealData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getMealType()I

    move-result v2

    int-to-long v2, v2

    cmp-long v2, v2, p1

    if-nez v2, :cond_0

    .line 619
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getId()J

    move-result-wide v2

    .line 623
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "mealData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    :goto_0
    return-wide v2

    :cond_1
    const-wide/16 v2, -0x1

    goto :goto_0
.end method

.method public getOnSaveChosenItemListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/OnSaveChosenItemListener;
    .locals 2
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 630
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mFoodTrackerMainContainer:Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;

    if-nez v0, :cond_1

    .line 632
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->initContainers()V

    .line 634
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mFoodTrackerMainContainer:Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;

    if-eqz v0, :cond_0

    .line 636
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->initData()V

    .line 639
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->initListeners()V

    .line 641
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mTimeToInit:J

    .line 645
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mFoodTrackerMainContainer:Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->getOnSaveChosenItemListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/OnSaveChosenItemListener;

    move-result-object v0

    return-object v0
.end method

.method public initAccordingChanges()V
    .locals 0

    .prologue
    .line 478
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->initData()V

    .line 479
    return-void
.end method

.method protected initDao()V
    .locals 2

    .prologue
    .line 363
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 364
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mealDao:Lcom/sec/android/app/shealth/food/fooddao/MealDao;

    if-nez v0, :cond_0

    .line 365
    new-instance v0, Lcom/sec/android/app/shealth/food/fooddao/MealDaoImplDb;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/food/fooddao/MealDaoImplDb;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mealDao:Lcom/sec/android/app/shealth/food/fooddao/MealDao;

    .line 366
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mealImageDao:Lcom/sec/android/app/shealth/food/fooddao/MealImageDao;

    if-nez v0, :cond_1

    .line 367
    new-instance v0, Lcom/sec/android/app/shealth/food/fooddao/MealImageDaoImplDb;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/food/fooddao/MealImageDaoImplDb;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mealImageDao:Lcom/sec/android/app/shealth/food/fooddao/MealImageDao;

    .line 368
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mFoodInfoDao:Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;

    if-nez v0, :cond_2

    .line 369
    new-instance v0, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mFoodInfoDao:Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;

    .line 370
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mMealItemDao:Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;

    if-nez v0, :cond_3

    .line 371
    new-instance v0, Lcom/sec/android/app/shealth/food/fooddao/MealItemDaoImplDb;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/food/fooddao/MealItemDaoImplDb;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mMealItemDao:Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;

    .line 375
    :cond_3
    return-void
.end method

.method public initForTime(J)V
    .locals 0
    .param p1, "mealTime"    # J

    .prologue
    .line 468
    iput-wide p1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mTimeToInit:J

    .line 469
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->initData()V

    .line 470
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 180
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 181
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->initContainers()V

    .line 182
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->initDao()V

    .line 183
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->initListeners()V

    .line 184
    if-eqz p1, :cond_0

    .line 185
    const-string v0, "FOOD_TRACKER_INIT_TIME"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mTimeToInit:J

    .line 188
    const-string v0, "QUICK_INPUT_MEAL_TYPE"

    const v1, 0x186a1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mMealType:I

    .line 189
    const-string v0, "MAX_ACCEPTABLE_CALORIES_VALUE"

    iget v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mMaxAcceptableCaloriesValue:F

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;F)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mMaxAcceptableCaloriesValue:F

    .line 193
    :goto_0
    return-void

    .line 191
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mTimeToInit:J

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 7
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 661
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 662
    const/4 v4, -0x1

    if-ne p2, v4, :cond_0

    .line 663
    packed-switch p1, :pswitch_data_0

    .line 696
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "Wrong request code"

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 666
    :pswitch_0
    if-eqz p3, :cond_0

    .line 667
    const-string/jumbo v4, "result_my_food_id"

    const-wide/16 v5, -0x1

    invoke-virtual {p3, v4, v5, v6}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    .line 670
    .local v2, "id":J
    new-instance v0, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-direct {v0, v4}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;-><init>(Landroid/content/Context;)V

    .line 671
    .local v0, "db":Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;
    invoke-interface {v0, v2, v3}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;->getDataById(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    .line 672
    .local v1, "foodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    if-eqz v1, :cond_1

    .line 673
    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->addToEditMealFragment(Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;)V

    .line 699
    .end local v0    # "db":Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;
    .end local v1    # "foodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    .end local v2    # "id":J
    :cond_0
    :goto_0
    return-void

    .line 676
    .restart local v0    # "db":Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;
    .restart local v1    # "foodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    .restart local v2    # "id":J
    :cond_1
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "FoodInfoData was not found in DB by id="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 682
    .end local v0    # "db":Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;
    .end local v1    # "foodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    .end local v2    # "id":J
    :pswitch_1
    if-eqz p3, :cond_2

    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 684
    const-string v4, "BAR_CODE_RESULT_FOOD_INFO"

    invoke-virtual {p3, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    .line 686
    .restart local v1    # "foodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    if-eqz v1, :cond_0

    .line 687
    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->addToEditMealFragment(Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;)V

    goto :goto_0

    .line 691
    .end local v1    # "foodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->addCustomFood()V

    goto :goto_0

    .line 663
    :pswitch_data_0
    .packed-switch 0x223d
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 158
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->onCreate(Landroid/os/Bundle;)V

    .line 159
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "onCreate"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    new-instance v1, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$DateChangeReceiver;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$DateChangeReceiver;-><init>(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$1;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mDateChangeReceiver:Landroid/content/BroadcastReceiver;

    .line 161
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 162
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.TIME_TICK"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 163
    const-string v1, "android.intent.action.TIME_SET"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 164
    const-string v1, "android.intent.action.TIMEZONE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 165
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mDateChangeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 166
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplication()Landroid/app/Application;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLoggerHolder;

    invoke-interface {v1}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLoggerHolder;->getUserActionLogger()Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    .line 168
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-string v2, "audio"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioManager;

    iput-object v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mAudioManager:Landroid/media/AudioManager;

    .line 169
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 174
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "onCreateView"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDateChanged(Ljava/util/Date;)V
    .locals 2
    .param p1, "date"    # Ljava/util/Date;

    .prologue
    .line 483
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->onDateChanged(Ljava/util/Date;)V

    .line 484
    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->initForTime(J)V

    .line 485
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 212
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->onDestroy()V

    .line 213
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mFoodTrackerMainContainer:Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->clearImagesCache()V

    .line 214
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mDateChangeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 215
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 197
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->onResume()V

    .line 198
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->initAccordingChanges()V

    .line 199
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 574
    const-string v0, "FOOD_TRACKER_INIT_TIME"

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getSelectedDate()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 577
    const-string v0, "QUICK_INPUT_MEAL_TYPE"

    iget v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mMealType:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 578
    const-string v0, "MAX_ACCEPTABLE_CALORIES_VALUE"

    iget v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mMaxAcceptableCaloriesValue:F

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 579
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 580
    return-void
.end method

.method protected onSytemDateChanged()V
    .locals 2

    .prologue
    .line 489
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->initForTime(J)V

    .line 490
    return-void
.end method

.method public resetDailyData()V
    .locals 5

    .prologue
    .line 542
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mDailyMeals:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    .line 543
    .local v0, "data":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mealDao:Lcom/sec/android/app/shealth/food/fooddao/MealDao;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getId()J

    move-result-wide v3

    invoke-interface {v2, v3, v4}, Lcom/sec/android/app/shealth/food/fooddao/MealDao;->deleteDataById(J)Z

    goto :goto_0

    .line 545
    .end local v0    # "data":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const v3, 0x7f09094a

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 547
    return-void
.end method

.method protected setNextAndPrevDates()V
    .locals 15

    .prologue
    .line 496
    iget-object v8, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mealDao:Lcom/sec/android/app/shealth/food/fooddao/MealDao;

    const-wide/16 v9, 0x0

    iget-wide v11, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mTimeToInit:J

    invoke-static {v11, v12}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v11

    const-wide/16 v13, 0x1

    sub-long/2addr v11, v13

    invoke-interface {v8, v9, v10, v11, v12}, Lcom/sec/android/app/shealth/food/fooddao/MealDao;->getDataForPeriod(JJ)Ljava/util/List;

    move-result-object v3

    .line 498
    .local v3, "mealsPast":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;>;"
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_3

    .line 499
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v8

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setLeftArrowEnabled(Z)V

    .line 500
    const-wide/16 v6, 0x0

    .line 501
    .local v6, "previousTime":J
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    .line 502
    .local v0, "data":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getMealTime()J

    move-result-wide v8

    cmp-long v8, v8, v6

    if-lez v8, :cond_0

    .line 503
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getMealTime()J

    move-result-wide v6

    goto :goto_0

    .line 506
    .end local v0    # "data":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v8

    new-instance v9, Ljava/util/Date;

    invoke-direct {v9, v6, v7}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v8, v9}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setPrevDate(Ljava/util/Date;)V

    .line 511
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v6    # "previousTime":J
    :goto_1
    iget-object v8, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mealDao:Lcom/sec/android/app/shealth/food/fooddao/MealDao;

    iget-wide v9, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mTimeToInit:J

    invoke-static {v9, v10}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v9

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v11

    invoke-virtual {v11}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->getSystemDate()Ljava/util/Date;

    move-result-object v11

    invoke-virtual {v11}, Ljava/util/Date;->getTime()J

    move-result-wide v11

    invoke-interface {v8, v9, v10, v11, v12}, Lcom/sec/android/app/shealth/food/fooddao/MealDao;->getDataForPeriod(JJ)Ljava/util/List;

    move-result-object v2

    .line 514
    .local v2, "mealsFuture":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;>;"
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_5

    .line 515
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v8

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setRightArrowEnabled(Z)V

    .line 516
    const-wide v4, 0x7fffffffffffffffL

    .line 517
    .local v4, "nextTime":J
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_2
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    .line 518
    .restart local v0    # "data":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getMealTime()J

    move-result-wide v8

    cmp-long v8, v8, v4

    if-gez v8, :cond_2

    .line 519
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getMealTime()J

    move-result-wide v4

    goto :goto_2

    .line 508
    .end local v0    # "data":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "mealsFuture":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;>;"
    .end local v4    # "nextTime":J
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v8

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->getSelecteddate()Ljava/util/Date;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setPrevDate(Ljava/util/Date;)V

    .line 509
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setLeftArrowEnabled(Z)V

    goto :goto_1

    .line 522
    .restart local v1    # "i$":Ljava/util/Iterator;
    .restart local v2    # "mealsFuture":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;>;"
    .restart local v4    # "nextTime":J
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v8

    new-instance v9, Ljava/util/Date;

    invoke-direct {v9, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v8, v9}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setNextDate(Ljava/util/Date;)V

    .line 530
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v4    # "nextTime":J
    :goto_3
    return-void

    .line 523
    :cond_5
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->isSelectedToday()Z

    move-result v8

    if-nez v8, :cond_6

    .line 524
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v8

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setRightArrowEnabled(Z)V

    .line 525
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v8

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->getSystemDate()Ljava/util/Date;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setNextDate(Ljava/util/Date;)V

    goto :goto_3

    .line 527
    :cond_6
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v8

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->getSelecteddate()Ljava/util/Date;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setNextDate(Ljava/util/Date;)V

    .line 528
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setRightArrowEnabled(Z)V

    goto :goto_3
.end method
