.class public abstract Lcom/sec/android/app/shealth/common/utils/Utils$EditTextLimitHelper;
.super Ljava/lang/Object;
.source "Utils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/common/utils/Utils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "EditTextLimitHelper"
.end annotation


# instance fields
.field xmlLengthFilterPosition:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 266
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public addLimit(ILandroid/widget/EditText;)V
    .locals 8
    .param p1, "limit"    # I
    .param p2, "editText"    # Landroid/widget/EditText;

    .prologue
    const/4 v7, -0x1

    .line 270
    new-instance v3, Lcom/sec/android/app/shealth/common/utils/Utils$EditTextLimitHelper$1;

    invoke-direct {v3, p0, p1}, Lcom/sec/android/app/shealth/common/utils/Utils$EditTextLimitHelper$1;-><init>(Lcom/sec/android/app/shealth/common/utils/Utils$EditTextLimitHelper;I)V

    .line 280
    .local v3, "lengthFilter":Landroid/text/InputFilter$LengthFilter;
    iput v7, p0, Lcom/sec/android/app/shealth/common/utils/Utils$EditTextLimitHelper;->xmlLengthFilterPosition:I

    .line 281
    if-eqz p2, :cond_1

    .line 282
    invoke-virtual {p2}, Landroid/widget/EditText;->getFilters()[Landroid/text/InputFilter;

    move-result-object v1

    .line 283
    .local v1, "filters":[Landroid/text/InputFilter;
    if-eqz v1, :cond_0

    array-length v5, v1

    if-nez v5, :cond_2

    .line 284
    :cond_0
    const/4 v5, 0x1

    new-array v5, v5, [Landroid/text/InputFilter;

    const/4 v6, 0x0

    aput-object v3, v5, v6

    invoke-virtual {p2, v5}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 312
    .end local v1    # "filters":[Landroid/text/InputFilter;
    :cond_1
    :goto_0
    return-void

    .line 287
    .restart local v1    # "filters":[Landroid/text/InputFilter;
    :cond_2
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    array-length v5, v1

    if-ge v2, v5, :cond_3

    .line 288
    aget-object v0, v1, v2

    .line 289
    .local v0, "filter":Landroid/text/InputFilter;
    instance-of v5, v0, Landroid/text/InputFilter$LengthFilter;

    if-eqz v5, :cond_4

    .line 290
    iput v2, p0, Lcom/sec/android/app/shealth/common/utils/Utils$EditTextLimitHelper;->xmlLengthFilterPosition:I

    .line 291
    # getter for: Lcom/sec/android/app/shealth/common/utils/Utils;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/Utils;->access$000()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "xml\'s MaxLength input filter found!"

    invoke-static {v5, v6}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    .end local v0    # "filter":Landroid/text/InputFilter;
    :cond_3
    iget v5, p0, Lcom/sec/android/app/shealth/common/utils/Utils$EditTextLimitHelper;->xmlLengthFilterPosition:I

    if-eq v5, v7, :cond_1

    .line 296
    array-length v5, v1

    add-int/lit8 v5, v5, 0x1

    new-array v4, v5, [Landroid/text/InputFilter;

    .line 299
    .local v4, "newFilters":[Landroid/text/InputFilter;
    const/4 v2, 0x0

    :goto_2
    iget v5, p0, Lcom/sec/android/app/shealth/common/utils/Utils$EditTextLimitHelper;->xmlLengthFilterPosition:I

    if-ge v2, v5, :cond_5

    .line 300
    aget-object v5, v1, v2

    aput-object v5, v4, v2

    .line 299
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 287
    .end local v4    # "newFilters":[Landroid/text/InputFilter;
    .restart local v0    # "filter":Landroid/text/InputFilter;
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 304
    .end local v0    # "filter":Landroid/text/InputFilter;
    .restart local v4    # "newFilters":[Landroid/text/InputFilter;
    :cond_5
    iget v5, p0, Lcom/sec/android/app/shealth/common/utils/Utils$EditTextLimitHelper;->xmlLengthFilterPosition:I

    aput-object v3, v4, v5

    .line 305
    iget v5, p0, Lcom/sec/android/app/shealth/common/utils/Utils$EditTextLimitHelper;->xmlLengthFilterPosition:I

    add-int/lit8 v2, v5, 0x1

    :goto_3
    array-length v5, v1

    add-int/lit8 v5, v5, 0x1

    if-ge v2, v5, :cond_6

    .line 306
    add-int/lit8 v5, v2, -0x1

    aget-object v5, v1, v5

    aput-object v5, v4, v2

    .line 305
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 308
    :cond_6
    invoke-virtual {p2, v4}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    goto :goto_0
.end method

.method public abstract onLimitExceeded()V
.end method
