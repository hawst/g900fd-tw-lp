.class public Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$WFLDataReceivedListener;
.super Ljava/lang/Object;
.source "HomeLatestData.java"

# interfaces
.implements Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData$OnPedoMeterSensorDataReceiveListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "WFLDataReceivedListener"
.end annotation


# static fields
.field public static final DEVICE_DISCONNECTED:I = 0x3

.field public static final SYNC_PROCESSING:I = 0x2

.field public static final TIME_OUT:I = 0x0

.field public static final WRONG_DATA:I = 0x1


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;)V
    .locals 0

    .prologue
    .line 173
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$WFLDataReceivedListener;->this$0:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponseReceived(FFFIIIIIIIIJ)V
    .locals 6
    .param p1, "distance"    # F
    .param p2, "cal"    # F
    .param p3, "speed"    # F
    .param p4, "totalStep"    # I
    .param p5, "walkingStep"    # I
    .param p6, "runStep"    # I
    .param p7, "updownStep"    # I
    .param p8, "activeTime"    # I
    .param p9, "inactiveTime"    # I
    .param p10, "healthySteps"    # I
    .param p11, "deviceType"    # I
    .param p12, "updatTime"    # J

    .prologue
    .line 184
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v2

    const/16 v3, 0x2719

    if-eq v2, v3, :cond_1

    .line 202
    :cond_0
    :goto_0
    return-void

    .line 187
    :cond_1
    const-string v2, "HomeLatestData"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "onResponseReceived :: totalStep = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 188
    if-lez p4, :cond_0

    .line 191
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$WFLDataReceivedListener;->this$0:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    # getter for: Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->pedoData:Lcom/sec/android/app/shealth/home/data/HomePedoData;
    invoke-static {v2}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->access$300(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;)Lcom/sec/android/app/shealth/home/data/HomePedoData;

    move-result-object v2

    if-nez v2, :cond_2

    .line 192
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$WFLDataReceivedListener;->this$0:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    new-instance v3, Lcom/sec/android/app/shealth/home/data/HomePedoData;

    move-wide/from16 v0, p12

    invoke-direct {v3, p4, p2, v0, v1}, Lcom/sec/android/app/shealth/home/data/HomePedoData;-><init>(IFJ)V

    # setter for: Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->pedoData:Lcom/sec/android/app/shealth/home/data/HomePedoData;
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->access$302(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;Lcom/sec/android/app/shealth/home/data/HomePedoData;)Lcom/sec/android/app/shealth/home/data/HomePedoData;

    .line 199
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$WFLDataReceivedListener;->this$0:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    # setter for: Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->sendUpdateViewFromSync:Ljava/lang/Boolean;
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->access$002(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    .line 200
    const-string v2, "HomeLatestData"

    const-string v3, "\t[PERF] calling mUpdateViewRunnable from onResponseReceived"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 201
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$WFLDataReceivedListener;->this$0:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    # getter for: Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->access$200(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$WFLDataReceivedListener;->this$0:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    # getter for: Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mUpdateViewRunnable:Ljava/lang/Runnable;
    invoke-static {v3}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->access$100(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;)Ljava/lang/Runnable;

    move-result-object v3

    const-wide/16 v4, 0x64

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 194
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$WFLDataReceivedListener;->this$0:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    # getter for: Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->pedoData:Lcom/sec/android/app/shealth/home/data/HomePedoData;
    invoke-static {v2}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->access$300(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;)Lcom/sec/android/app/shealth/home/data/HomePedoData;

    move-result-object v2

    invoke-virtual {v2, p4}, Lcom/sec/android/app/shealth/home/data/HomePedoData;->setTotalSteps(I)V

    .line 195
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$WFLDataReceivedListener;->this$0:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    # getter for: Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->pedoData:Lcom/sec/android/app/shealth/home/data/HomePedoData;
    invoke-static {v2}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->access$300(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;)Lcom/sec/android/app/shealth/home/data/HomePedoData;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/sec/android/app/shealth/home/data/HomePedoData;->setCalories(F)V

    .line 196
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$WFLDataReceivedListener;->this$0:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    # getter for: Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->pedoData:Lcom/sec/android/app/shealth/home/data/HomePedoData;
    invoke-static {v2}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->access$300(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;)Lcom/sec/android/app/shealth/home/data/HomePedoData;

    move-result-object v2

    move-wide/from16 v0, p12

    invoke-virtual {v2, v0, v1}, Lcom/sec/android/app/shealth/home/data/HomePedoData;->setTime(J)V

    goto :goto_1
.end method

.method public onWalkingModeChanged(Ljava/lang/String;)V
    .locals 0
    .param p1, "walkingMode"    # Ljava/lang/String;

    .prologue
    .line 221
    return-void
.end method

.method public onWearableSyncComplete(II)V
    .locals 3
    .param p1, "error_code"    # I
    .param p2, "deviceType"    # I

    .prologue
    .line 206
    const-string v0, "HomeLatestData"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onWearableSyncComplete :: deviceType = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 207
    packed-switch p1, :pswitch_data_0

    .line 217
    :pswitch_0
    return-void

    .line 207
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
