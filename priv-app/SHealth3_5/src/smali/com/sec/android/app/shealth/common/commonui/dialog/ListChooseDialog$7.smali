.class Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$7;
.super Ljava/lang/Object;
.source "ListChooseDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->getGeneralParentContainer(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;)V
    .locals 0

    .prologue
    .line 397
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$7;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 401
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$7;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->getActivity()Landroid/app/Activity;

    move-result-object v1

    instance-of v1, v1, Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerGetter;

    if-eqz v1, :cond_0

    .line 403
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$7;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerGetter;

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$7;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->getTag()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerGetter;->getNegativeButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonController;

    move-result-object v0

    .line 404
    .local v0, "negativeButtonController":Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonController;
    if-eqz v0, :cond_0

    .line 406
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$7;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    iget-object v2, v0, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonController;->btnClickListener:Landroid/view/View$OnClickListener;

    # setter for: Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->cancelButtonClickListener:Landroid/view/View$OnClickListener;
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->access$402(Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;Landroid/view/View$OnClickListener;)Landroid/view/View$OnClickListener;

    .line 409
    .end local v0    # "negativeButtonController":Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonController;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$7;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->cancelButtonClickListener:Landroid/view/View$OnClickListener;
    invoke-static {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->access$400(Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;)Landroid/view/View$OnClickListener;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 411
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$7;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->cancelButtonClickListener:Landroid/view/View$OnClickListener;
    invoke-static {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->access$400(Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-interface {v1, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 413
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$7;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    # invokes: Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->dismissAllowStateLoss()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->access$000(Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;)V

    .line 414
    return-void
.end method
