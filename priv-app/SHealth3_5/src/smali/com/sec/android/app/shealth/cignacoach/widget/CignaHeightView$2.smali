.class Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$2;
.super Ljava/lang/Object;
.source "CignaHeightView.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->initLayout()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;)V
    .locals 0

    .prologue
    .line 137
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v0, 0x1

    .line 140
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_2

    .line 142
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->getListValue()I

    move-result v1

    if-nez v1, :cond_0

    .line 144
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->access$300(Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->isFocused()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->checkValueText()Z

    move-result v1

    if-nez v1, :cond_2

    .line 145
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->access$300(Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->clearFocus()V

    .line 160
    :goto_0
    return v0

    .line 151
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->access$300(Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->isFocused()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->checkValueText()Z

    move-result v1

    if-nez v1, :cond_1

    .line 152
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->access$300(Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->clearFocus()V

    goto :goto_0

    .line 154
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->access$400(Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->isFocused()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->checkValueText()Z

    move-result v1

    if-nez v1, :cond_2

    .line 155
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->access$400(Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->clearFocus()V

    goto :goto_0

    .line 160
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
