.class public Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;
.super Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;
.source "BooheeSearch.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch$OAuthBase;,
        Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch$Base32Util;
    }
.end annotation


# static fields
.field private static final CATEGORY:Ljava/lang/String; = "ifood_groups"

.field private static final DEFAULT_PAGE_NUM:I = 0x1

.field private static final DEFAULT_PER_PAGE_NUM:I = 0xa

.field private static final DEFAULT_SUGGEST_PER_PAGE_NUM:I = 0x4

.field private static final DESIRED_BARCODE_SIZE:I = 0xd

.field private static final FIND_BY_BARCODE:Ljava/lang/String; = "find_by_barcode"

.field private static final KEY:Ljava/lang/String; = "samsung"

.field private static final LOG_TAG:Ljava/lang/String;

.field private static final MAX_RETRY:I = 0x2

.field private static final SEARCH:Ljava/lang/String; = "search"

.field private static final SECRET:Ljava/lang/String; = "7859e98fa01a91550173539c37f8cdaa"

.field private static final SUB_CATEGORY:Ljava/lang/String; = "ifoods"

.field private static final SUGGEST_SEARCH:Ljava/lang/String; = "suggest_search"

.field private static final TRADITIONAL:Ljava/lang/String; = "trad"

.field private static final URL_BASE:Ljava/lang/String; = "http://ifood.boohee.com/v2/ifoods/"

.field private static final URL_BASE_CATEGORY:Ljava/lang/String; = "http://ifood.boohee.com/v2/"

.field private static final USER_AGENT:Ljava/lang/String; = "Mozilla/5.0 (Linux; U; Android 2.1; en-us; ADR6200 Build/ERD79) AppleWebKit/530.17(KHTML, like Gecko) Version/ 4.0 Mobile Safari/530.17"


# instance fields
.field private final mBooheeParser:Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    const-class v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->getLogTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 77
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;-><init>(Landroid/content/Context;)V

    .line 78
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;

    invoke-direct {v0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->mBooheeParser:Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;

    .line 79
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->mBooheeParser:Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;

    new-instance v1, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch$1;-><init>(Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->setOnFoodParsingCompleteListener(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/NotifyingFoodParser$OnFoodParsingCompleteListener;)V

    .line 86
    return-void
.end method

.method static synthetic access$300()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    sget-object v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->LOG_TAG:Ljava/lang/String;

    return-object v0
.end method

.method private doHttpGetRequest(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;
    .locals 7
    .param p1, "urlBaseStr"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 391
    .local p2, "requestParams":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    # invokes: Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch$OAuthBase;->createQueryString(Ljava/util/Map;)Ljava/lang/String;
    invoke-static {p2}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch$OAuthBase;->access$200(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 392
    .local v1, "queryString":Ljava/lang/String;
    new-instance v2, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v2, v1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 393
    .local v2, "request":Lorg/apache/http/client/methods/HttpGet;
    sget-object v4, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->LOG_TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " BooheeSearch----doHttpGetRequest ----  queryString = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/common/utils/Logging;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 394
    const-string v4, "Mozilla/5.0 (Linux; U; Android 2.1; en-us; ADR6200 Build/ERD79) AppleWebKit/530.17(KHTML, like Gecko) Version/ 4.0 Mobile Safari/530.17"

    invoke-static {v4}, Landroid/net/http/AndroidHttpClient;->newInstance(Ljava/lang/String;)Landroid/net/http/AndroidHttpClient;

    move-result-object v0

    .line 395
    .local v0, "client":Landroid/net/http/AndroidHttpClient;
    invoke-direct {p0, v2, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->tryGetHttpResponseSeveralTimes(Lorg/apache/http/client/methods/HttpGet;Landroid/net/http/AndroidHttpClient;)Lorg/apache/http/HttpResponse;

    move-result-object v3

    .line 396
    .local v3, "response":Lorg/apache/http/HttpResponse;
    invoke-direct {p0, v2, v0, v3}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->getResponseContent(Lorg/apache/http/client/methods/HttpGet;Landroid/net/http/AndroidHttpClient;Lorg/apache/http/HttpResponse;)Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method private formatBarcode(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "barcode"    # Ljava/lang/String;

    .prologue
    const/16 v3, 0xd

    .line 596
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 597
    .local v1, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 598
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    rsub-int/lit8 v2, v3, 0xd

    .line 599
    .local v2, "zeroCount":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_0

    .line 600
    const/16 v3, 0x30

    invoke-virtual {v1, v0, v3}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    .line 599
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 602
    :cond_0
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 603
    add-int v3, v0, v2

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-virtual {v1, v3, v4}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    .line 602
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 605
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method private getResponseContent(Lorg/apache/http/client/methods/HttpGet;Landroid/net/http/AndroidHttpClient;Lorg/apache/http/HttpResponse;)Ljava/lang/String;
    .locals 4
    .param p1, "request"    # Lorg/apache/http/client/methods/HttpGet;
    .param p2, "client"    # Landroid/net/http/AndroidHttpClient;
    .param p3, "response"    # Lorg/apache/http/HttpResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 424
    const/4 v1, 0x0

    .line 425
    .local v1, "responseContent":Ljava/lang/String;
    invoke-virtual {p1}, Lorg/apache/http/client/methods/HttpGet;->isAborted()Z

    move-result v2

    if-nez v2, :cond_1

    .line 426
    if-nez p3, :cond_0

    .line 427
    sget-object v2, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->LOG_TAG:Ljava/lang/String;

    const-string v3, " HttpResponse is null"

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/Logging;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 428
    new-instance v2, Ljava/io/IOException;

    const-string v3, "HttpResponse is null"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 431
    :cond_0
    :try_start_0
    invoke-interface {p3}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v2

    invoke-static {v2}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 435
    invoke-virtual {p2}, Landroid/net/http/AndroidHttpClient;->close()V

    .line 441
    :goto_0
    return-object v1

    .line 432
    :catch_0
    move-exception v0

    .line 433
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    sget-object v2, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->LOG_TAG:Ljava/lang/String;

    invoke-static {v2, v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 435
    invoke-virtual {p2}, Landroid/net/http/AndroidHttpClient;->close()V

    goto :goto_0

    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    invoke-virtual {p2}, Landroid/net/http/AndroidHttpClient;->close()V

    throw v2

    .line 438
    :cond_1
    sget-object v2, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->LOG_TAG:Ljava/lang/String;

    const-string v3, " HttpRequest is aborted"

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/Logging;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 439
    invoke-virtual {p2}, Landroid/net/http/AndroidHttpClient;->close()V

    goto :goto_0
.end method

.method private queryGetFoodByBarcode(Ljava/lang/String;)Ljava/lang/String;
    .locals 12
    .param p1, "barcode"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 352
    const/4 v10, 0x0

    .line 354
    .local v10, "response":Ljava/lang/String;
    :try_start_0
    const-string v11, "http://ifood.boohee.com/v2/ifoods/find_by_barcode"

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->setRequestParamMap(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;III)Ljava/util/Map;

    move-result-object v0

    invoke-direct {p0, v11, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->doHttpGetRequest(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v10

    .line 361
    :goto_0
    return-object v10

    .line 355
    :catch_0
    move-exception v9

    .line 356
    .local v9, "ioe":Ljava/io/IOException;
    sget-object v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->LOG_TAG:Ljava/lang/String;

    invoke-static {v0, v9}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 357
    throw v9

    .line 358
    .end local v9    # "ioe":Ljava/io/IOException;
    :catch_1
    move-exception v8

    .line 359
    .local v8, "e":Ljava/lang/Exception;
    sget-object v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->LOG_TAG:Ljava/lang/String;

    invoke-static {v0, v8}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private queryGetFoodByGroupId(Ljava/lang/String;)Ljava/lang/String;
    .locals 12
    .param p1, "foodGroupId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 276
    const/4 v10, 0x0

    .line 278
    .local v10, "response":Ljava/lang/String;
    :try_start_0
    const-string v11, "http://ifood.boohee.com/v2/ifoods"

    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->setRequestParamMap(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;III)Ljava/util/Map;

    move-result-object v0

    invoke-direct {p0, v11, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->doHttpGetRequest(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v10

    .line 286
    :goto_0
    return-object v10

    .line 280
    :catch_0
    move-exception v9

    .line 281
    .local v9, "ioe":Ljava/io/IOException;
    sget-object v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->LOG_TAG:Ljava/lang/String;

    invoke-static {v0, v9}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 282
    throw v9

    .line 283
    .end local v9    # "ioe":Ljava/io/IOException;
    :catch_1
    move-exception v8

    .line 284
    .local v8, "e":Ljava/lang/Exception;
    sget-object v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->LOG_TAG:Ljava/lang/String;

    invoke-static {v0, v8}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private queryGetFoodById(Ljava/lang/String;)Ljava/lang/String;
    .locals 12
    .param p1, "booheeId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 370
    const/4 v10, 0x0

    .line 372
    .local v10, "response":Ljava/lang/String;
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "http://ifood.boohee.com/v2/ifoods/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->setRequestParamMap(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;III)Ljava/util/Map;

    move-result-object v0

    invoke-direct {p0, v11, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->doHttpGetRequest(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v10

    .line 379
    :goto_0
    return-object v10

    .line 373
    :catch_0
    move-exception v9

    .line 374
    .local v9, "ioe":Ljava/io/IOException;
    sget-object v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->LOG_TAG:Ljava/lang/String;

    invoke-static {v0, v9}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 375
    throw v9

    .line 376
    .end local v9    # "ioe":Ljava/io/IOException;
    :catch_1
    move-exception v8

    .line 377
    .local v8, "e":Ljava/lang/Exception;
    sget-object v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->LOG_TAG:Ljava/lang/String;

    invoke-static {v0, v8}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private querySearchFood(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "keyword"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 343
    const/4 v0, 0x1

    const/16 v1, 0xa

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->querySearchFood(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private querySearchFood(Ljava/lang/String;II)Ljava/lang/String;
    .locals 12
    .param p1, "keyWord"    # Ljava/lang/String;
    .param p2, "page"    # I
    .param p3, "perPage"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 327
    const/4 v10, 0x0

    .line 329
    .local v10, "response":Ljava/lang/String;
    :try_start_0
    const-string v11, "http://ifood.boohee.com/v2/ifoods/search"

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v4, p1

    move v6, p2

    move v7, p3

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->setRequestParamMap(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;III)Ljava/util/Map;

    move-result-object v0

    invoke-direct {p0, v11, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->doHttpGetRequest(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v10

    .line 336
    :goto_0
    return-object v10

    .line 330
    :catch_0
    move-exception v9

    .line 331
    .local v9, "ioe":Ljava/io/IOException;
    sget-object v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->LOG_TAG:Ljava/lang/String;

    invoke-static {v0, v9}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 332
    throw v9

    .line 333
    .end local v9    # "ioe":Ljava/io/IOException;
    :catch_1
    move-exception v8

    .line 334
    .local v8, "e":Ljava/lang/Exception;
    sget-object v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->LOG_TAG:Ljava/lang/String;

    invoke-static {v0, v8}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private querySubCategorySearchFood(Ljava/lang/String;)Ljava/lang/String;
    .locals 12
    .param p1, "isMemu"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 256
    const/4 v10, 0x0

    .line 258
    .local v10, "response":Ljava/lang/String;
    :try_start_0
    const-string v11, "http://ifood.boohee.com/v2/ifood_groups"

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->setRequestParamMap(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;III)Ljava/util/Map;

    move-result-object v0

    invoke-direct {p0, v11, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->doHttpGetRequest(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v10

    .line 265
    :goto_0
    sget-object v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " queryCategorySearchFood ===  response = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    return-object v10

    .line 259
    :catch_0
    move-exception v9

    .line 260
    .local v9, "ioe":Ljava/io/IOException;
    sget-object v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->LOG_TAG:Ljava/lang/String;

    invoke-static {v0, v9}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 261
    throw v9

    .line 262
    .end local v9    # "ioe":Ljava/io/IOException;
    :catch_1
    move-exception v8

    .line 263
    .local v8, "e":Ljava/lang/Exception;
    sget-object v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->LOG_TAG:Ljava/lang/String;

    invoke-static {v0, v8}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private querySuggestSearchFood(Ljava/lang/String;I)Ljava/lang/String;
    .locals 12
    .param p1, "keyWord"    # Ljava/lang/String;
    .param p2, "max"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 299
    if-ltz p2, :cond_0

    const/4 v0, 0x4

    if-gt p2, v0, :cond_0

    .line 300
    move v5, p2

    .line 304
    .local v5, "maxValueForHttpRequest":I
    :goto_0
    const/4 v10, 0x0

    .line 306
    .local v10, "response":Ljava/lang/String;
    :try_start_0
    const-string v11, "http://ifood.boohee.com/v2/ifoods/suggest_search"

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    move-object v4, p1

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->setRequestParamMap(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;III)Ljava/util/Map;

    move-result-object v0

    invoke-direct {p0, v11, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->doHttpGetRequest(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v10

    .line 314
    :goto_1
    return-object v10

    .line 302
    .end local v5    # "maxValueForHttpRequest":I
    .end local v10    # "response":Ljava/lang/String;
    :cond_0
    const/4 v5, 0x4

    .restart local v5    # "maxValueForHttpRequest":I
    goto :goto_0

    .line 308
    .restart local v10    # "response":Ljava/lang/String;
    :catch_0
    move-exception v9

    .line 309
    .local v9, "ioe":Ljava/io/IOException;
    sget-object v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->LOG_TAG:Ljava/lang/String;

    invoke-static {v0, v9}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 310
    throw v9

    .line 311
    .end local v9    # "ioe":Ljava/io/IOException;
    :catch_1
    move-exception v8

    .line 312
    .local v8, "e":Ljava/lang/Exception;
    sget-object v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->LOG_TAG:Ljava/lang/String;

    invoke-static {v0, v8}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method private setParamMap(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;III)V
    .locals 2
    .param p2, "isMenu"    # Ljava/lang/String;
    .param p3, "ifoodGroupId"    # Ljava/lang/String;
    .param p4, "barCode"    # Ljava/lang/String;
    .param p5, "keyWord"    # Ljava/lang/String;
    .param p6, "max"    # I
    .param p7, "page"    # I
    .param p8, "perPage"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "III)V"
        }
    .end annotation

    .prologue
    .line 197
    .local p1, "paramMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz p2, :cond_2

    .line 198
    const-string v0, "is_menu"

    invoke-interface {p1, v0, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 216
    :cond_0
    :goto_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isHKTWModel()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 217
    const-string/jumbo v0, "zh"

    const-string/jumbo v1, "trad"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 220
    :cond_1
    return-void

    .line 200
    :cond_2
    if-eqz p3, :cond_3

    .line 201
    const-string v0, "ifood_group_id"

    invoke-interface {p1, v0, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 203
    :cond_3
    if-eqz p4, :cond_4

    .line 204
    const-string v0, "barcode"

    invoke-interface {p1, v0, p4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 206
    :cond_4
    if-eqz p5, :cond_0

    .line 207
    const-string/jumbo v0, "q"

    invoke-interface {p1, v0, p5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208
    if-eqz p6, :cond_5

    .line 209
    const-string v0, "limit"

    invoke-static {p6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 212
    :cond_5
    const-string/jumbo v0, "page"

    invoke-static {p7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 213
    const-string/jumbo v0, "per_page"

    invoke-static {p8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private setRequestParamMap(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;III)Ljava/util/Map;
    .locals 13
    .param p1, "isMenu"    # Ljava/lang/String;
    .param p2, "ifoodGroupId"    # Ljava/lang/String;
    .param p3, "barCode"    # Ljava/lang/String;
    .param p4, "keyWord"    # Ljava/lang/String;
    .param p5, "max"    # I
    .param p6, "page"    # I
    .param p7, "perPage"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "III)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 225
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 227
    .local v1, "paramMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v0, "app_key"

    const-string/jumbo v2, "samsung"

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    .line 228
    invoke-direct/range {v0 .. v8}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->setParamMap(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;III)V

    .line 230
    new-instance v12, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch$OAuthBase;

    const/4 v0, 0x0

    invoke-direct {v12, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch$OAuthBase;-><init>(Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch$1;)V

    .line 232
    .local v12, "oAuth":Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch$OAuthBase;
    const-string v0, "7859e98fa01a91550173539c37f8cdaa"

    # invokes: Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch$OAuthBase;->generateConsumerKey(Ljava/lang/String;Ljava/util/AbstractMap;)Ljava/lang/String;
    invoke-static {v12, v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch$OAuthBase;->access$100(Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch$OAuthBase;Ljava/lang/String;Ljava/util/AbstractMap;)Ljava/lang/String;

    move-result-object v11

    .line 236
    .local v11, "consumerKey":Ljava/lang/String;
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 238
    .local v3, "requestParamMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v0, "app_key"

    const-string/jumbo v2, "samsung"

    invoke-interface {v3, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 240
    const-string v0, "consumer_key"

    invoke-interface {v3, v0, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v2, p0

    move-object v4, p1

    move-object v5, p2

    move-object/from16 v6, p3

    move-object/from16 v7, p4

    move/from16 v8, p5

    move/from16 v9, p6

    move/from16 v10, p7

    .line 242
    invoke-direct/range {v2 .. v10}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->setParamMap(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;III)V

    .line 244
    return-object v3
.end method

.method private tryGetHttpResponseSeveralTimes(Lorg/apache/http/client/methods/HttpGet;Landroid/net/http/AndroidHttpClient;)Lorg/apache/http/HttpResponse;
    .locals 7
    .param p1, "request"    # Lorg/apache/http/client/methods/HttpGet;
    .param p2, "client"    # Landroid/net/http/AndroidHttpClient;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x2

    .line 400
    const/4 v2, 0x0

    .line 401
    .local v2, "response":Lorg/apache/http/HttpResponse;
    const/4 v3, 0x0

    .line 402
    .local v3, "retryCount":I
    :goto_0
    if-gt v3, v6, :cond_0

    .line 404
    add-int/lit8 v3, v3, 0x1

    .line 405
    :try_start_0
    invoke-virtual {p2, p1}, Landroid/net/http/AndroidHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 420
    :cond_0
    :goto_1
    return-object v2

    .line 407
    :catch_0
    move-exception v1

    .line 408
    .local v1, "ioe":Ljava/io/IOException;
    if-gt v3, v6, :cond_1

    .line 409
    sget-object v4, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->LOG_TAG:Ljava/lang/String;

    const-string v5, " HttpGet IOException"

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/common/utils/Logging;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 411
    :cond_1
    invoke-virtual {p1}, Lorg/apache/http/client/methods/HttpGet;->abort()V

    .line 412
    throw v1

    .line 414
    .end local v1    # "ioe":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 415
    .local v0, "e":Ljava/lang/Exception;
    sget-object v4, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->LOG_TAG:Ljava/lang/String;

    const-string v5, " HttpGet Exception"

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/common/utils/Logging;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 416
    invoke-virtual {p1}, Lorg/apache/http/client/methods/HttpGet;->abort()V

    goto :goto_1
.end method


# virtual methods
.method public editSearchPhrase(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 610
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isSupportsBoohee()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "BOOHEE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 611
    const/4 v0, 0x0

    const-string v1, "BOOHEE"

    invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 613
    .end local p1    # "text":Ljava/lang/String;
    :cond_0
    return-object p1
.end method

.method public getLogoResourceId()I
    .locals 1

    .prologue
    .line 187
    const v0, 0x7f02002c

    return v0
.end method

.method public getServerSourceType()I
    .locals 1

    .prologue
    .line 90
    const v0, 0x46cd4

    return v0
.end method

.method public isAutoCompleteSupported()Z
    .locals 1

    .prologue
    .line 192
    const/4 v0, 0x1

    return v0
.end method

.method protected performAutoCompleteSearchRealization(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchRequest;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/AutoCompleteSearchResult;
    .locals 4
    .param p1, "request"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/ParseException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 97
    sget-object v1, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " querySuggestSearchFood ===  request.getStringRequest() = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchRequest;->getStringRequest()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchRequest;->getStringRequest()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0xa

    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->querySuggestSearchFood(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 101
    .local v0, "response":Ljava/lang/String;
    sget-object v1, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " querySuggestSearchFood ===  response = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->mBooheeParser:Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->parseAutoCompleteSearch(Ljava/lang/String;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/AutoCompleteSearchResult;

    move-result-object v1

    return-object v1
.end method

.method protected performBarcodeSearchRealization(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchRequest;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;
    .locals 8
    .param p1, "request"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/ParseException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 168
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchRequest;->getStringRequest()Ljava/lang/String;

    move-result-object v0

    .line 169
    .local v0, "barcode":Ljava/lang/String;
    sget-object v5, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->LOG_TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " Barcode barcode 1 = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->formatBarcode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 171
    sget-object v5, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->LOG_TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " Barcode barcode 2 = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->queryGetFoodByBarcode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 173
    .local v4, "response":Ljava/lang/String;
    sget-object v5, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->LOG_TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " Barcode response 1 = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    iget-object v5, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->mBooheeParser:Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;

    invoke-virtual {v5, v4}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->parseBarcodeSearch(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 175
    .local v2, "foodId":Ljava/lang/String;
    sget-object v5, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->LOG_TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " Barcode foodId = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->queryGetFoodById(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 177
    sget-object v5, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->LOG_TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " Barcode response 2 = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    iget-object v5, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->mBooheeParser:Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;

    invoke-virtual {v5, v4}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->parseExtraFood(Ljava/lang/String;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;

    move-result-object v1

    .line 179
    .local v1, "extraFoodInfoResult":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;
    iget-object v6, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->mBooheeParser:Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;->getSearchResult()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;

    invoke-virtual {v6, v4, v5}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->parseFoodInfoFromExtraFoodInfoResult(Ljava/lang/String;Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;)Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    move-result-object v3

    .line 181
    .local v3, "foodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    new-instance v5, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoRequest;

    invoke-direct {v5, v3}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoRequest;-><init>(Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;)V

    invoke-virtual {v1, v5}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;->setRequest(Ljava/lang/Object;)V

    .line 182
    return-object v1
.end method

.method protected performCategorySearchRealization()Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryResult;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/ParseException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->mBooheeParser:Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->parseCategorySearch(Ljava/lang/String;)Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryResult;

    move-result-object v0

    return-object v0
.end method

.method protected performExtraFoodInfoRequestRealization(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoRequest;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;
    .locals 8
    .param p1, "extraFoodInfoRequest"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/ParseException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 150
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoRequest;->getFoodInfoData()Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    move-result-object v2

    .line 151
    .local v2, "foodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getServerId()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->queryGetFoodById(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 152
    .local v3, "response":Ljava/lang/String;
    sget-object v5, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->LOG_TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " response = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    sget-object v5, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->LOG_TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " performExtraFoodInfoRequestRealization === foodInfoData.getSecretId() = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getServerId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/shealth/common/utils/Logging;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    iget-object v5, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->mBooheeParser:Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;

    invoke-virtual {v5, v3}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->parseExtraFood(Ljava/lang/String;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;

    move-result-object v1

    .line 156
    .local v1, "extraFoodInfoResult":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;
    iget-object v5, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->mBooheeParser:Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;

    sget v5, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->FOOD_UNIT_CALORY:F

    const/4 v6, 0x0

    cmpl-float v5, v5, v6

    if-eqz v5, :cond_0

    .line 157
    iget-object v5, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->mBooheeParser:Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->getUnitCalory()F

    move-result v5

    invoke-virtual {v2, v5}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setkCal(F)V

    .line 159
    :cond_0
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;->getSubCategoriesId()Ljava/lang/String;

    move-result-object v4

    .line 160
    .local v4, "subCategoryId":Ljava/lang/String;
    invoke-virtual {v2, v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setServerRootCategory(Ljava/lang/String;)V

    .line 161
    new-instance v0, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;

    iget-object v5, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->context:Landroid/content/Context;

    invoke-direct {v0, v5}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;-><init>(Landroid/content/Context;)V

    .line 162
    .local v0, "dao":Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;
    invoke-static {v2, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/DataBaseUpdater;->insertOrUpdateData(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;Lcom/sec/android/app/shealth/common/commondao/CommonDao;)V

    .line 163
    return-object v1
.end method

.method protected performFoodSearchRealization(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult;
    .locals 6
    .param p1, "request"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;",
            ")",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/ParseException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 108
    const/4 v2, 0x0

    .line 109
    .local v2, "response":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;->getStringRequest()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x6

    if-le v3, v4, :cond_0

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;->getStringRequest()Ljava/lang/String;

    move-result-object v3

    const-string v4, "BOOHEE"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 110
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;->getStringRequest()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;->getStringRequest()Ljava/lang/String;

    move-result-object v4

    const-string v5, "BOOHEE"

    invoke-virtual {v4, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    add-int/lit8 v4, v4, 0x6

    invoke-virtual {v3, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 112
    .local v0, "requestId":Ljava/lang/String;
    sget-object v3, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->LOG_TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " queryGetFoodByGroupId ===  requestId = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "  getStringRequest = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;->getStringRequest()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->queryGetFoodByGroupId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 116
    sget-object v3, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->LOG_TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " queryGetFoodByGroupId ===  response = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    .end local v0    # "requestId":Ljava/lang/String;
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->mBooheeParser:Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;

    invoke-virtual {v3, v2}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->parseFoodSearch(Ljava/lang/String;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult;

    move-result-object v3

    return-object v3

    .line 118
    :cond_0
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;->getStringRequest()Ljava/lang/String;

    move-result-object v1

    .line 120
    .local v1, "requestName":Ljava/lang/String;
    sget-object v3, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->LOG_TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " querySearchFood ===  requestName = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "  getStringRequest = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;->getStringRequest()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->querySearchFood(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 125
    sget-object v3, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->LOG_TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " querySearchFood ===  response = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected performSubCategorySearchRealization(Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodSubCategoryRequest;)Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategoryResult;
    .locals 4
    .param p1, "foodSubCategoryRequest"    # Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodSubCategoryRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/ParseException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 138
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodSubCategoryRequest;->getCategory()Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategory;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->querySubCategorySearchFood(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 139
    .local v0, "response":Ljava/lang/String;
    sget-object v1, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " performSubCategorySearchRealization ===  foodSubCategoryRequest.getCategory().getId() = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodSubCategoryRequest;->getCategory()Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategory;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategory;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->mBooheeParser:Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeFoodParser;->parseSubCategorySearch(Ljava/lang/String;)Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategoryResult;

    move-result-object v1

    return-object v1
.end method
