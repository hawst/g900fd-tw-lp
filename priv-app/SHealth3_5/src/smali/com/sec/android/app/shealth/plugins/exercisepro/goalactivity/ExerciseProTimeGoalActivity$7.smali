.class Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity$7;
.super Ljava/lang/Object;
.source "ExerciseProTimeGoalActivity.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;)V
    .locals 0

    .prologue
    .line 255
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 259
    if-eqz p1, :cond_0

    if-nez p3, :cond_2

    :cond_0
    move v0, v1

    .line 273
    :cond_1
    :goto_0
    return v0

    .line 261
    :cond_2
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-ne v2, v0, :cond_3

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    const/16 v3, 0x16

    if-ne v2, v3, :cond_3

    .line 263
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;->minutesEditText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;->access$600(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->requestFocus()Z

    goto :goto_0

    .line 267
    :cond_3
    const/16 v2, 0x42

    if-ne p2, v2, :cond_4

    .line 268
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_1

    .line 269
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;->executeEnterAction(Z)V
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;->access$500(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;Z)V

    goto :goto_0

    :cond_4
    move v0, v1

    .line 273
    goto :goto_0
.end method
