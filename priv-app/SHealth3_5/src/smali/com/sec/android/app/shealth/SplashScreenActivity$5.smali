.class Lcom/sec/android/app/shealth/SplashScreenActivity$5;
.super Ljava/lang/Object;
.source "SplashScreenActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/SplashScreenActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/SplashScreenActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/SplashScreenActivity;)V
    .locals 0

    .prologue
    .line 604
    iput-object p1, p0, Lcom/sec/android/app/shealth/SplashScreenActivity$5;->this$0:Lcom/sec/android/app/shealth/SplashScreenActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x0

    .line 607
    iget-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity$5;->this$0:Lcom/sec/android/app/shealth/SplashScreenActivity;

    # getter for: Lcom/sec/android/app/shealth/SplashScreenActivity;->isNext:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->access$200(Lcom/sec/android/app/shealth/SplashScreenActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 608
    iget-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity$5;->this$0:Lcom/sec/android/app/shealth/SplashScreenActivity;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/shealth/SplashScreenActivity;->isNext:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/SplashScreenActivity;->access$202(Lcom/sec/android/app/shealth/SplashScreenActivity;Z)Z

    .line 609
    iget-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity$5;->this$0:Lcom/sec/android/app/shealth/SplashScreenActivity;

    # getter for: Lcom/sec/android/app/shealth/SplashScreenActivity;->mNextBtnLayout:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->access$300(Lcom/sec/android/app/shealth/SplashScreenActivity;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setSoundEffectsEnabled(Z)V

    .line 611
    iget-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity$5;->this$0:Lcom/sec/android/app/shealth/SplashScreenActivity;

    # getter for: Lcom/sec/android/app/shealth/SplashScreenActivity;->mCenterLayout:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->access$500(Lcom/sec/android/app/shealth/SplashScreenActivity;)Landroid/widget/RelativeLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/SplashScreenActivity$5;->this$0:Lcom/sec/android/app/shealth/SplashScreenActivity;

    # getter for: Lcom/sec/android/app/shealth/SplashScreenActivity;->mFadeOutAnime:Landroid/view/animation/Animation;
    invoke-static {v1}, Lcom/sec/android/app/shealth/SplashScreenActivity;->access$400(Lcom/sec/android/app/shealth/SplashScreenActivity;)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 612
    iget-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity$5;->this$0:Lcom/sec/android/app/shealth/SplashScreenActivity;

    # getter for: Lcom/sec/android/app/shealth/SplashScreenActivity;->mBottomLayout:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->access$600(Lcom/sec/android/app/shealth/SplashScreenActivity;)Landroid/widget/LinearLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/SplashScreenActivity$5;->this$0:Lcom/sec/android/app/shealth/SplashScreenActivity;

    # getter for: Lcom/sec/android/app/shealth/SplashScreenActivity;->mFadeOutAnime:Landroid/view/animation/Animation;
    invoke-static {v1}, Lcom/sec/android/app/shealth/SplashScreenActivity;->access$400(Lcom/sec/android/app/shealth/SplashScreenActivity;)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 613
    iget-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity$5;->this$0:Lcom/sec/android/app/shealth/SplashScreenActivity;

    # getter for: Lcom/sec/android/app/shealth/SplashScreenActivity;->mBottomLayout:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->access$600(Lcom/sec/android/app/shealth/SplashScreenActivity;)Landroid/widget/LinearLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 614
    iget-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity$5;->this$0:Lcom/sec/android/app/shealth/SplashScreenActivity;

    # getter for: Lcom/sec/android/app/shealth/SplashScreenActivity;->mBackgroundLayout:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->access$700(Lcom/sec/android/app/shealth/SplashScreenActivity;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 615
    iget-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity$5;->this$0:Lcom/sec/android/app/shealth/SplashScreenActivity;

    # getter for: Lcom/sec/android/app/shealth/SplashScreenActivity;->mBackgroundLayout:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->access$700(Lcom/sec/android/app/shealth/SplashScreenActivity;)Landroid/widget/RelativeLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/SplashScreenActivity$5;->this$0:Lcom/sec/android/app/shealth/SplashScreenActivity;

    # getter for: Lcom/sec/android/app/shealth/SplashScreenActivity;->mScaleExAnime:Landroid/view/animation/Animation;
    invoke-static {v1}, Lcom/sec/android/app/shealth/SplashScreenActivity;->access$800(Lcom/sec/android/app/shealth/SplashScreenActivity;)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 617
    :cond_0
    return-void
.end method
