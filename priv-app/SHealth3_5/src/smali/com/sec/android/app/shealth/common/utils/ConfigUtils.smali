.class public Lcom/sec/android/app/shealth/common/utils/ConfigUtils;
.super Ljava/lang/Object;
.source "ConfigUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/common/utils/ConfigUtils$1;,
        Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Languages;,
        Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;
    }
.end annotation


# static fields
.field public static final LOG_DELIMITER:Ljava/lang/String; = "-----<<<<<"

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 226
    return-void
.end method

.method public static getCountryCode()Ljava/lang/String;
    .locals 9

    .prologue
    .line 97
    const/4 v2, 0x0

    .line 101
    .local v2, "countryCode":Ljava/lang/String;
    :try_start_0
    const-string v5, "android.os.SystemProperties"

    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 102
    .local v1, "cl":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v5, "get"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v7, 0x0

    const-class v8, Ljava/lang/String;

    aput-object v8, v6, v7

    invoke-virtual {v1, v5, v6}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    .line 103
    .local v4, "method":Ljava/lang/reflect/Method;
    const/4 v5, 0x0

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-string/jumbo v8, "ro.csc.countryiso_code"

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    move-object v0, v5

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 111
    .end local v1    # "cl":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v4    # "method":Ljava/lang/reflect/Method;
    :goto_0
    return-object v2

    .line 107
    :catch_0
    move-exception v3

    .line 109
    .local v3, "e":Ljava/lang/Exception;
    invoke-static {v3}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->logThrowable(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static getLocaleCountry()Ljava/lang/String;
    .locals 1

    .prologue
    .line 265
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getOperatorNumeric()Ljava/lang/String;
    .locals 9

    .prologue
    .line 198
    const/4 v4, 0x0

    .line 202
    .local v4, "numeric":Ljava/lang/String;
    :try_start_0
    const-string v5, "android.os.SystemProperties"

    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 203
    .local v1, "cl":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v5, "get"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v7, 0x0

    const-class v8, Ljava/lang/String;

    aput-object v8, v6, v7

    invoke-virtual {v1, v5, v6}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 204
    .local v3, "method":Ljava/lang/reflect/Method;
    const/4 v5, 0x0

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-string v8, "gsm.operator.numeric"

    aput-object v8, v6, v7

    invoke-virtual {v3, v5, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    move-object v0, v5

    check-cast v0, Ljava/lang/String;

    move-object v4, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 210
    .end local v1    # "cl":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v3    # "method":Ljava/lang/reflect/Method;
    :goto_0
    return-object v4

    .line 206
    :catch_0
    move-exception v2

    .line 208
    .local v2, "e":Ljava/lang/Exception;
    invoke-static {v2}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->logThrowable(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static isChinaCTCNetworkUsing()Z
    .locals 2

    .prologue
    .line 215
    const-string v0, "46003"

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->getOperatorNumeric()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static isChinaModel()Z
    .locals 1

    .prologue
    .line 178
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->China:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isCountryModel(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isChinaNetworkUsing()Z
    .locals 1

    .prologue
    .line 220
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->getOperatorNumeric()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isChinaModel()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 221
    const/4 v0, 0x1

    .line 223
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isCountryModel(Ljava/lang/String;)Z
    .locals 2
    .param p0, "country"    # Ljava/lang/String;

    .prologue
    .line 117
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->getCountryCode()Ljava/lang/String;

    move-result-object v0

    .line 120
    .local v0, "countryCode":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isCurrentSettingSupportedByCigna()Z
    .locals 1

    .prologue
    .line 276
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isUSAModel()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isFranceModel()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isItalyModel()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isUKModel()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isGermanyModel()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isSpainModel()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isLanguageSupportedByCigna()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 277
    const/4 v0, 0x1

    .line 279
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isFranceModel()Z
    .locals 1

    .prologue
    .line 154
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->France:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isCountryModel(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isGermanyModel()Z
    .locals 1

    .prologue
    .line 166
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->Germany:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isCountryModel(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isHKTWModel()Z
    .locals 1

    .prologue
    .line 185
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->HongKong:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isCountryModel(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->Taiwan:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isCountryModel(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isItalyModel()Z
    .locals 1

    .prologue
    .line 160
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->Italy:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isCountryModel(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isJapanModel()Z
    .locals 1

    .prologue
    .line 134
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->Japan:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isCountryModel(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isKoreaModel()Z
    .locals 1

    .prologue
    .line 128
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->KoreaWW:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isCountryModel(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->KoreaKR:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isCountryModel(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isLanguageSupportedByCigna()Z
    .locals 1

    .prologue
    .line 290
    const-string v0, "es_US"

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isSelectedLanguageSupportsCigna(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "fr_FR"

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isSelectedLanguageSupportsCigna(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "it_IT"

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isSelectedLanguageSupportsCigna(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "es_ES"

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isSelectedLanguageSupportsCigna(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "de_DE"

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isSelectedLanguageSupportsCigna(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "en_GB"

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isSelectedLanguageSupportsCigna(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "en_US"

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isSelectedLanguageSupportsCigna(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 291
    :cond_0
    const/4 v0, 0x1

    .line 293
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isLatinModel()Z
    .locals 3

    .prologue
    .line 191
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    .line 192
    .local v1, "languageCode":Ljava/lang/String;
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    .line 193
    .local v0, "countryName":Ljava/lang/String;
    if-eqz v1, :cond_0

    const-string v2, "es"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "US"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static isRtlLanguage()Z
    .locals 2

    .prologue
    .line 254
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    .line 255
    .local v0, "language":Ljava/lang/String;
    sget-object v1, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Languages;->Arabic:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Languages;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Languages;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Languages;->Hebrew:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Languages;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Languages;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isSelectedLanguageSupportsCigna(Ljava/lang/String;)Z
    .locals 2
    .param p0, "checkedLanguage"    # Ljava/lang/String;

    .prologue
    .line 304
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    .line 305
    .local v0, "languageCode":Ljava/lang/String;
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 306
    const/4 v1, 0x1

    .line 308
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isSpainModel()Z
    .locals 1

    .prologue
    .line 172
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->Spain:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isCountryModel(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isSupportHRM()Z
    .locals 1

    .prologue
    .line 340
    const/4 v0, 0x1

    .line 356
    .local v0, "isSupport":Z
    return v0
.end method

.method public static isSupportsBoohee()Z
    .locals 3

    .prologue
    .line 318
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v1

    .line 319
    .local v1, "locale":Ljava/lang/String;
    const/4 v0, 0x0

    .line 320
    .local v0, "isGrandChinaLocale":Z
    sget-object v2, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->China:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->HongKong:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->Taiwan:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 324
    :cond_0
    const/4 v0, 0x1

    .line 326
    :cond_1
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isChinaModel()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isHKTWModel()Z

    move-result v2

    if-eqz v2, :cond_3

    if-eqz v0, :cond_3

    .line 327
    :cond_2
    const/4 v2, 0x1

    .line 329
    :goto_0
    return v2

    :cond_3
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static isUKModel()Z
    .locals 1

    .prologue
    .line 148
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->UnitedKingdom:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isCountryModel(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->GreatBritain:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isCountryModel(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isUSAModel()Z
    .locals 1

    .prologue
    .line 140
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->USA:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isCountryModel(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static logThrowable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "throwable"    # Ljava/lang/Throwable;

    .prologue
    .line 65
    invoke-static {p0, p1, p2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 66
    invoke-virtual {p2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    .line 67
    .local v0, "cause":Ljava/lang/Throwable;
    if-eqz v0, :cond_0

    .line 69
    const-string v1, "-----<<<<<Was caused by"

    invoke-static {p0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    invoke-static {p0, v0}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 72
    :cond_0
    return-void
.end method

.method public static logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "throwable"    # Ljava/lang/Throwable;

    .prologue
    .line 52
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "-----<<<<<"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, p1}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->logThrowable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 53
    return-void
.end method

.method public static logThrowable(Ljava/lang/Throwable;)V
    .locals 1
    .param p0, "throwable"    # Ljava/lang/Throwable;

    .prologue
    .line 40
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->TAG:Ljava/lang/String;

    invoke-static {v0, p0}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 41
    return-void
.end method
