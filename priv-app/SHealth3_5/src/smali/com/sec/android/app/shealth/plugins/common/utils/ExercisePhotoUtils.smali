.class public Lcom/sec/android/app/shealth/plugins/common/utils/ExercisePhotoUtils;
.super Ljava/lang/Object;
.source "ExercisePhotoUtils.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lcom/sec/android/app/shealth/plugins/common/utils/ExercisePhotoUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/plugins/common/utils/ExercisePhotoUtils;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static cursorToPhotoData(Landroid/database/Cursor;)Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;
    .locals 3
    .param p0, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 158
    new-instance v0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;-><init>()V

    .line 159
    .local v0, "photoData":Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;
    const-string v1, "_id"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->setRowId(J)V

    .line 160
    const-string v1, "exercise__id"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->setExerciseId(J)V

    .line 161
    const-string v1, "latitude"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v1

    const-string v2, "longitude"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->setLocate(FF)V

    .line 163
    const-string v1, "file_path"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->setFilePath(Ljava/lang/String;)V

    .line 164
    const-string v1, "create_time"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->setCreateTime(J)V

    .line 165
    const-string/jumbo v1, "update_time"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->setUpdateTime(J)V

    .line 166
    const-string/jumbo v1, "time_zone"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->setTimeZone(I)V

    .line 167
    return-object v0
.end method

.method public static deleteExtraAddedPhoto(Ljava/lang/String;Landroid/content/Context;)V
    .locals 4
    .param p0, "Path"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 308
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "file_path=\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 309
    .local v0, "selectionClause":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$ExercisePhoto;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 310
    return-void
.end method

.method public static deleteItem(Landroid/content/Context;J)J
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "rowId"    # J

    .prologue
    .line 144
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 147
    .local v2, "selectionClause":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/sdk/health/content/ShealthContract$ExercisePhoto;->CONTENT_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v2, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    int-to-long v0, v3

    .line 148
    .local v0, "rowsDeleted":J
    return-wide v0
.end method

.method public static getCameraPictureRealPaht(Landroid/content/Context;)Ljava/lang/String;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 171
    const/4 v6, 0x0

    .line 172
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v7, 0x0

    .line 174
    .local v7, "photoPath":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_data"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "date_added"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string/jumbo v4, "orientation"

    aput-object v4, v2, v3

    const-string v3, "date_added"

    const/4 v4, 0x0

    const-string v5, "date_added ASC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 175
    const/4 v7, 0x0

    .line 176
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 179
    :cond_0
    const-string v0, "_data"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    .line 180
    .local v8, "uri":Landroid/net/Uri;
    invoke-virtual {v8}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    .line 181
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 185
    .end local v8    # "uri":Landroid/net/Uri;
    :cond_1
    if-eqz v6, :cond_2

    .line 186
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 188
    :cond_2
    return-object v7

    .line 185
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    .line 186
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method public static getExercisePhotoDataList(Landroid/database/Cursor;)Ljava/util/ArrayList;
    .locals 4
    .param p0, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 43
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 44
    .local v0, "photoDataList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;>;"
    invoke-interface {p0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 45
    :goto_0
    invoke-interface {p0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v2

    if-nez v2, :cond_1

    .line 46
    invoke-static {p0}, Lcom/sec/android/app/shealth/plugins/common/utils/ExercisePhotoUtils;->cursorToPhotoData(Landroid/database/Cursor;)Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;

    move-result-object v1

    .line 48
    .local v1, "photodata":Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;
    new-instance v2, Ljava/io/File;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->getFilePath()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->isFile()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 49
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 51
    :cond_0
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_0

    .line 53
    .end local v1    # "photodata":Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;
    :cond_1
    return-object v0
.end method

.method public static getExercisePhotoPath(JLandroid/content/Context;)Ljava/util/List;
    .locals 8
    .param p0, "exerciseId"    # J
    .param p2, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 291
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 292
    .local v7, "photoId":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "file_path"

    aput-object v1, v2, v0

    .line 293
    .local v2, "projection":[Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "exercise__id = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 294
    .local v3, "selectionClause":Ljava/lang/String;
    const/4 v6, 0x0

    .line 296
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$ExercisePhoto;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 297
    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 298
    const-string v0, "file_path"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 301
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_0

    .line 302
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_0
    throw v0

    .line 301
    :cond_1
    if-eqz v6, :cond_2

    .line 302
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 304
    :cond_2
    return-object v7
.end method

.method public static getImageThumbnail(Landroid/content/Context;Ljava/lang/String;II)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "filepath"    # Ljava/lang/String;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 265
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, p3, v0}, Lcom/sec/android/app/shealth/plugins/common/utils/ExercisePhotoUtils;->getImageThumbnail(Landroid/content/Context;Ljava/lang/String;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public static getImageThumbnail(Landroid/content/Context;Ljava/lang/String;IIZ)Landroid/graphics/Bitmap;
    .locals 26
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "filepath"    # Ljava/lang/String;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "fitXY"    # Z

    .prologue
    .line 192
    const/4 v15, 0x0

    .line 193
    .local v15, "bitmap":Landroid/graphics/Bitmap;
    const/4 v2, 0x0

    .line 196
    .local v2, "imgthumBitmap":Landroid/graphics/Bitmap;
    :try_start_0
    new-instance v19, Landroid/media/ExifInterface;

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    .line 197
    .local v19, "ei":Landroid/media/ExifInterface;
    const-string v3, "Orientation"

    const/4 v4, 0x1

    move-object/from16 v0, v19

    invoke-virtual {v0, v3, v4}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result v21

    .line 198
    .local v21, "orientation":I
    const/16 v22, 0x0

    .line 199
    .local v22, "rotate":I
    const/16 v24, 0x0

    .local v24, "srcSize":I
    const/16 v17, 0x0

    .line 201
    .local v17, "decSize":I
    packed-switch v21, :pswitch_data_0

    .line 212
    :pswitch_0
    const/16 v22, 0x0

    .line 216
    :goto_0
    invoke-virtual/range {v19 .. v19}, Landroid/media/ExifInterface;->hasThumbnail()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 217
    invoke-virtual/range {v19 .. v19}, Landroid/media/ExifInterface;->getThumbnail()[B

    move-result-object v25

    .line 218
    .local v25, "thumbnail":[B
    const/4 v3, 0x0

    move-object/from16 v0, v25

    array-length v4, v0

    move-object/from16 v0, v25

    invoke-static {v0, v3, v4}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 219
    new-instance v7, Landroid/graphics/Matrix;

    invoke-direct {v7}, Landroid/graphics/Matrix;-><init>()V

    .line 220
    .local v7, "matrix":Landroid/graphics/Matrix;
    move/from16 v0, v22

    int-to-float v3, v0

    invoke-virtual {v7, v3}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 221
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    const/4 v8, 0x1

    invoke-static/range {v2 .. v8}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v2

    move-object v8, v15

    .end local v7    # "matrix":Landroid/graphics/Matrix;
    .end local v15    # "bitmap":Landroid/graphics/Bitmap;
    .end local v17    # "decSize":I
    .end local v19    # "ei":Landroid/media/ExifInterface;
    .end local v21    # "orientation":I
    .end local v22    # "rotate":I
    .end local v24    # "srcSize":I
    .end local v25    # "thumbnail":[B
    .local v8, "bitmap":Landroid/graphics/Bitmap;
    :goto_1
    move-object v3, v2

    .line 261
    :goto_2
    return-object v3

    .line 203
    .end local v8    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v15    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v17    # "decSize":I
    .restart local v19    # "ei":Landroid/media/ExifInterface;
    .restart local v21    # "orientation":I
    .restart local v22    # "rotate":I
    .restart local v24    # "srcSize":I
    :pswitch_1
    const/16 v22, 0x5a

    .line 204
    goto :goto_0

    .line 206
    :pswitch_2
    const/16 v22, 0xb4

    .line 207
    goto :goto_0

    .line 209
    :pswitch_3
    const/16 v22, 0x10e

    .line 210
    goto :goto_0

    .line 223
    :cond_0
    new-instance v7, Landroid/graphics/Matrix;

    invoke-direct {v7}, Landroid/graphics/Matrix;-><init>()V

    .line 224
    .restart local v7    # "matrix":Landroid/graphics/Matrix;
    new-instance v20, Landroid/graphics/BitmapFactory$Options;

    invoke-direct/range {v20 .. v20}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 225
    .local v20, "options":Landroid/graphics/BitmapFactory$Options;
    const/4 v3, 0x1

    move-object/from16 v0, v20

    iput-boolean v3, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 226
    sget-object v3, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    move-object/from16 v0, v20

    iput-object v3, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 227
    const/4 v3, 0x1

    move-object/from16 v0, v20

    iput-boolean v3, v0, Landroid/graphics/BitmapFactory$Options;->inDither:Z

    .line 229
    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 230
    if-eqz p4, :cond_1

    .line 232
    move-object/from16 v0, v20

    iget v3, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    move-object/from16 v0, v20

    iget v4, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v24

    .line 233
    invoke-static/range {p2 .. p3}, Ljava/lang/Math;->max(II)I

    move-result v17

    .line 236
    :cond_1
    const/16 v23, 0x1

    .line 238
    .local v23, "scale":I
    if-eqz p4, :cond_2

    .line 239
    move/from16 v0, v24

    int-to-float v3, v0

    move/from16 v0, v17

    int-to-float v4, v0

    div-float/2addr v3, v4

    float-to-double v3, v3

    const-wide/high16 v5, 0x3fe0000000000000L    # 0.5

    add-double/2addr v3, v5

    double-to-int v0, v3

    move/from16 v23, v0

    .line 245
    :goto_3
    div-int/lit8 v3, v23, 0x2

    if-nez v3, :cond_3

    .end local v23    # "scale":I
    :goto_4
    move/from16 v0, v23

    move-object/from16 v1, v20

    iput v0, v1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 246
    const/4 v3, 0x0

    move-object/from16 v0, v20

    iput-boolean v3, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 247
    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 248
    .end local v15    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v8    # "bitmap":Landroid/graphics/Bitmap;
    if-nez v8, :cond_4

    .line 249
    const/4 v3, 0x0

    goto :goto_2

    .line 241
    .end local v8    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v15    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v23    # "scale":I
    :cond_2
    move-object/from16 v0, v20

    iget v3, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    int-to-float v3, v3

    move/from16 v0, p2

    int-to-float v4, v0

    div-float/2addr v3, v4

    float-to-double v3, v3

    const-wide/high16 v5, 0x3fe0000000000000L    # 0.5

    add-double/2addr v3, v5

    double-to-int v0, v3

    move/from16 v23, v0

    .line 242
    const/4 v3, 0x0

    move-object/from16 v0, v20

    iput-boolean v3, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    .line 258
    .end local v7    # "matrix":Landroid/graphics/Matrix;
    .end local v17    # "decSize":I
    .end local v19    # "ei":Landroid/media/ExifInterface;
    .end local v20    # "options":Landroid/graphics/BitmapFactory$Options;
    .end local v21    # "orientation":I
    .end local v22    # "rotate":I
    .end local v23    # "scale":I
    .end local v24    # "srcSize":I
    :catch_0
    move-exception v18

    move-object v8, v15

    .line 259
    .end local v15    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v8    # "bitmap":Landroid/graphics/Bitmap;
    .local v18, "e":Ljava/io/IOException;
    :goto_5
    invoke-virtual/range {v18 .. v18}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_1

    .line 245
    .end local v8    # "bitmap":Landroid/graphics/Bitmap;
    .end local v18    # "e":Ljava/io/IOException;
    .restart local v7    # "matrix":Landroid/graphics/Matrix;
    .restart local v15    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v17    # "decSize":I
    .restart local v19    # "ei":Landroid/media/ExifInterface;
    .restart local v20    # "options":Landroid/graphics/BitmapFactory$Options;
    .restart local v21    # "orientation":I
    .restart local v22    # "rotate":I
    .restart local v23    # "scale":I
    .restart local v24    # "srcSize":I
    :cond_3
    add-int/lit8 v23, v23, 0x1

    goto :goto_4

    .line 251
    .end local v15    # "bitmap":Landroid/graphics/Bitmap;
    .end local v23    # "scale":I
    .restart local v8    # "bitmap":Landroid/graphics/Bitmap;
    :cond_4
    move/from16 v0, v22

    int-to-float v3, v0

    :try_start_1
    invoke-virtual {v7, v3}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 252
    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v11

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v12

    const/4 v14, 0x1

    move-object v13, v7

    invoke-static/range {v8 .. v14}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 253
    new-instance v16, Ljava/io/ByteArrayOutputStream;

    invoke-direct/range {v16 .. v16}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 254
    .local v16, "bytearroutstream":Ljava/io/ByteArrayOutputStream;
    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v4, 0x55

    move-object/from16 v0, v16

    invoke-virtual {v2, v3, v4, v0}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_1

    .line 258
    .end local v16    # "bytearroutstream":Ljava/io/ByteArrayOutputStream;
    :catch_1
    move-exception v18

    goto :goto_5

    .line 201
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static getImageThumbnailFitXY(Landroid/content/Context;Ljava/lang/String;II)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "filepath"    # Ljava/lang/String;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 269
    const/4 v0, 0x1

    invoke-static {p0, p1, p2, p3, v0}, Lcom/sec/android/app/shealth/plugins/common/utils/ExercisePhotoUtils;->getImageThumbnail(Landroid/content/Context;Ljava/lang/String;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public static getLocateMetaInfo(Ljava/lang/String;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;
    .locals 9
    .param p0, "file_path"    # Ljava/lang/String;

    .prologue
    .line 273
    const/4 v6, 0x0

    .line 275
    .local v6, "location":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;
    :try_start_0
    new-instance v5, Landroid/media/ExifInterface;

    invoke-direct {v5, p0}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    .line 276
    .local v5, "exif":Landroid/media/ExifInterface;
    const-string v8, "GPSLatitude"

    invoke-virtual {v5, v8}, Landroid/media/ExifInterface;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 277
    .local v0, "LATITUDE":Ljava/lang/String;
    const-string v8, "GPSLatitudeRef"

    invoke-virtual {v5, v8}, Landroid/media/ExifInterface;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 278
    .local v1, "LATITUDE_REF":Ljava/lang/String;
    const-string v8, "GPSLongitude"

    invoke-virtual {v5, v8}, Landroid/media/ExifInterface;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 279
    .local v2, "LONGITUDE":Ljava/lang/String;
    const-string v8, "GPSLongitudeRef"

    invoke-virtual {v5, v8}, Landroid/media/ExifInterface;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 280
    .local v3, "LONGITUDE_REF":Ljava/lang/String;
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    if-eqz v3, :cond_0

    .line 281
    new-instance v7, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    invoke-direct {v7}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;-><init>()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v6    # "location":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;
    .local v7, "location":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;
    move-object v6, v7

    .line 287
    .end local v0    # "LATITUDE":Ljava/lang/String;
    .end local v1    # "LATITUDE_REF":Ljava/lang/String;
    .end local v2    # "LONGITUDE":Ljava/lang/String;
    .end local v3    # "LONGITUDE_REF":Ljava/lang/String;
    .end local v5    # "exif":Landroid/media/ExifInterface;
    .end local v7    # "location":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;
    .restart local v6    # "location":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;
    :cond_0
    :goto_0
    return-object v6

    .line 283
    :catch_0
    move-exception v4

    .line 284
    .local v4, "e":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public static insertTempitem(Landroid/content/Context;Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;Z)J
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "phothData"    # Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;
    .param p2, "isEditMode"    # Z

    .prologue
    const-wide/16 v3, -0x1

    .line 103
    if-nez p1, :cond_1

    .line 134
    :cond_0
    :goto_0
    return-wide v3

    .line 107
    :cond_1
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 108
    .local v2, "values":Landroid/content/ContentValues;
    if-eqz p2, :cond_3

    .line 109
    const-string v5, "exercise__id"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->getExerciseId()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 110
    const-string/jumbo v5, "update_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 118
    :cond_2
    :goto_1
    const-string v5, "latitude"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->getLatitude()F

    move-result v6

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 119
    const-string v5, "longitude"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->getLongitude()F

    move-result v6

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 120
    const-string v5, "file_path"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->getFilePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    const-string v5, "create_time"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->getCreateTime()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 122
    const-string/jumbo v5, "time_zone"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->getTimeZone()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 124
    const/4 v1, 0x0

    .line 126
    .local v1, "rawContactUri":Landroid/net/Uri;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/sdk/health/content/ShealthContract$ExercisePhoto;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v5, v6, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 130
    :goto_2
    if-eqz v1, :cond_0

    .line 134
    invoke-static {v1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v3

    goto :goto_0

    .line 113
    .end local v1    # "rawContactUri":Landroid/net/Uri;
    :cond_3
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeExerciseId()J

    move-result-wide v5

    const-wide/16 v7, 0x0

    cmp-long v5, v5, v7

    if-eqz v5, :cond_2

    .line 114
    const-string v5, "exercise__id"

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeExerciseId()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    goto :goto_1

    .line 127
    .restart local v1    # "rawContactUri":Landroid/net/Uri;
    :catch_0
    move-exception v0

    .line 128
    .local v0, "ie":Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;
    sget-object v5, Lcom/sec/android/app/shealth/plugins/common/utils/ExercisePhotoUtils;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "insert exception -"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method
