.class public final enum Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;
.super Ljava/lang/Enum;
.source "ConfigUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/common/utils/ConfigUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Countries"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

.field public static final enum Bahrain:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

.field public static final enum Brazil:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

.field public static final enum Canada:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

.field public static final enum China:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

.field public static final enum France:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

.field public static final enum Germany:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

.field public static final enum GreatBritain:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

.field public static final enum HongKong:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

.field public static final enum Iran:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

.field public static final enum Italy:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

.field public static final enum Japan:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

.field public static final enum KoreaKR:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

.field public static final enum KoreaWW:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

.field public static final enum Laos:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

.field public static final enum Other:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

.field public static final enum Peru:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

.field public static final enum Spain:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

.field public static final enum Taiwan:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

.field public static final enum Tunisia:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

.field public static final enum USA:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

.field public static final enum UnitedKingdom:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;


# instance fields
.field private countryCode:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 76
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    const-string v1, "USA"

    const-string v2, "US"

    invoke-direct {v0, v1, v4, v2}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->USA:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    new-instance v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    const-string v1, "GreatBritain"

    const-string v2, "GB"

    invoke-direct {v0, v1, v5, v2}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->GreatBritain:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    new-instance v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    const-string v1, "UnitedKingdom"

    const-string v2, "UK"

    invoke-direct {v0, v1, v6, v2}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->UnitedKingdom:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    new-instance v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    const-string v1, "China"

    const-string v2, "CN"

    invoke-direct {v0, v1, v7, v2}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->China:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    new-instance v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    const-string v1, "Japan"

    const-string v2, "JP"

    invoke-direct {v0, v1, v8, v2}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->Japan:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    new-instance v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    const-string v1, "Canada"

    const/4 v2, 0x5

    const-string v3, "CA"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->Canada:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    new-instance v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    const-string v1, "Brazil"

    const/4 v2, 0x6

    const-string v3, "BR"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->Brazil:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    new-instance v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    const-string v1, "KoreaWW"

    const/4 v2, 0x7

    const-string v3, "WW"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->KoreaWW:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    new-instance v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    const-string v1, "KoreaKR"

    const/16 v2, 0x8

    const-string v3, "KR"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->KoreaKR:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    new-instance v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    const-string v1, "France"

    const/16 v2, 0x9

    const-string v3, "FR"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->France:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    .line 77
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    const-string v1, "Italy"

    const/16 v2, 0xa

    const-string v3, "IT"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->Italy:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    new-instance v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    const-string v1, "Germany"

    const/16 v2, 0xb

    const-string v3, "DE"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->Germany:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    new-instance v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    const-string v1, "Spain"

    const/16 v2, 0xc

    const-string v3, "ES"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->Spain:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    new-instance v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    const-string v1, "HongKong"

    const/16 v2, 0xd

    const-string v3, "HK"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->HongKong:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    new-instance v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    const-string v1, "Taiwan"

    const/16 v2, 0xe

    const-string v3, "TW"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->Taiwan:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    new-instance v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    const-string v1, "Laos"

    const/16 v2, 0xf

    const-string v3, "LA"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->Laos:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    new-instance v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    const-string v1, "Tunisia"

    const/16 v2, 0x10

    const-string v3, "TN"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->Tunisia:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    new-instance v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    const-string v1, "Bahrain"

    const/16 v2, 0x11

    const-string v3, "BH"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->Bahrain:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    new-instance v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    const-string v1, "Iran"

    const/16 v2, 0x12

    const-string v3, "IR"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->Iran:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    new-instance v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    const-string v1, "Peru"

    const/16 v2, 0x13

    const-string v3, "PE"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->Peru:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    new-instance v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    const-string v1, "Other"

    const/16 v2, 0x14

    const-string v3, "OTHER"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->Other:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    .line 74
    const/16 v0, 0x15

    new-array v0, v0, [Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    sget-object v1, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->USA:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->GreatBritain:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->UnitedKingdom:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->China:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->Japan:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->Canada:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->Brazil:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->KoreaWW:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->KoreaKR:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->France:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->Italy:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->Germany:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->Spain:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->HongKong:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->Taiwan:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->Laos:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->Tunisia:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->Bahrain:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->Iran:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->Peru:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->Other:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->$VALUES:[Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1
    .param p3, "countryCode"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 82
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 79
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->countryCode:Ljava/lang/String;

    .line 83
    iput-object p3, p0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->countryCode:Ljava/lang/String;

    .line 84
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 74
    const-class v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;
    .locals 1

    .prologue
    .line 74
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->$VALUES:[Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->countryCode:Ljava/lang/String;

    return-object v0
.end method
