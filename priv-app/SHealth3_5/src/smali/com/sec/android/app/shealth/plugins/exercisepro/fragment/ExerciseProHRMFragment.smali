.class public Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;
.super Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;
.source "ExerciseProHRMFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/heartrate/HeartrateSensorListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$27;,
        Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$SensorErrorTimer;
    }
.end annotation


# static fields
.field private static final DELAYTIME_CHECK_SENSOR_DETECT:I = 0x96

.field private static final MEASURE_FAIL_MAX_COUNT:I = 0x0

.field private static final PULSE_MODE_DELAYE_TIME:I = 0x370

.field private static final PULSE_MODE_DRAWING:I = 0x0

.field private static final PULSE_MODE_ERASER:I = 0x1

.field public static final TAG:Ljava/lang/String;

.field private static effectAudio:[I


# instance fields
.field private bpm:I

.field private errorImages:[I

.field private hrBpmText:Landroid/widget/TextView;

.field private hrBpmTextNonMedical:Landroid/widget/TextView;

.field private hrGuideText:Landroid/widget/TextView;

.field private isEndPulseDelay:Z

.field private isFingerDetected:Z

.field isMeasureCompleted:Z

.field private isRetryMeasuring:Z

.field private layoutMedical:Landroid/widget/LinearLayout;

.field private layoutNonMedical:Landroid/widget/LinearLayout;

.field private mAlertBuild_Error:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

.field private mAlertBuild_HRM_Measure:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

.field private mAlertBuild_HRM_Result:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

.field private mAnimationView:Landroid/widget/ImageView;

.field private mCircleView:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleProgressView;

.field private mContext:Landroid/content/Context;

.field private mCurrentActivity:Landroid/app/Activity;

.field private mDeviceConnector:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

.field private mErrorType:I

.field private mFirstMessage:Landroid/widget/TextView;

.field private mFirstStatus:Landroid/widget/TextView;

.field private mFragmentHandler:Landroid/os/Handler;

.field public mHandler:Landroid/os/Handler;

.field private mHeartRate:I

.field private mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

.field private mIsGreen:Z

.field private mIsOnConfigChanged:Z

.field private mIsRunPulseView:Z

.field private mMainView:Landroid/view/View;

.field private mMeasureFailCount:I

.field private mPolygonHeight:I

.field private mPolygonWidth:I

.field private mPrevError:I

.field private mScaleAnimation:Landroid/view/animation/Animation;

.field private mSecondCenterIcon:Landroid/widget/ImageView;

.field private mSecondCenterIconBig:Landroid/widget/ImageView;

.field private mSecondCenterPulseLayout:Landroid/widget/FrameLayout;

.field private mSecondCenterPulseViewGreenIcon:Lcom/sec/android/app/shealth/heartrate/HRMGIFView;

.field private mSecondCenterPulseViewIcon:Landroid/widget/ImageView;

.field private mSecondCenterPulseViewLayout:Landroid/widget/LinearLayout;

.field private mSecondCenterPulseViewMask:Landroid/widget/ImageView;

.field private mSecondCenterPulseViewOrangeIcon:Landroid/widget/ImageView;

.field private mSecondLayout:Landroid/widget/FrameLayout;

.field private mSensorErrorTimer:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$SensorErrorTimer;

.field private mStateBar:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateSummaryBar;

.field private maxBpmText:Landroid/widget/TextView;

.field private maxHRzone:I

.field private minBpmText:Landroid/widget/TextView;

.field private minHRzone:I

.field private pool:Landroid/media/SoundPool;

.field private pulseAnimationID:[I

.field private rangeText:Landroid/widget/TextView;

.field private readyTimer:Landroid/os/CountDownTimer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 89
    const-class v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->TAG:Ljava/lang/String;

    .line 104
    const/4 v0, 0x5

    new-array v0, v0, [I

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->effectAudio:[I

    return-void
.end method

.method public constructor <init>()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 212
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;-><init>()V

    .line 95
    iput-boolean v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->isFingerDetected:Z

    .line 96
    iput-boolean v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->isRetryMeasuring:Z

    .line 97
    iput-boolean v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mIsRunPulseView:Z

    .line 98
    iput-boolean v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mIsGreen:Z

    .line 99
    iput-boolean v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->isEndPulseDelay:Z

    .line 100
    iput-boolean v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mIsOnConfigChanged:Z

    .line 105
    iput v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mMeasureFailCount:I

    .line 106
    iput v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mHeartRate:I

    .line 110
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mContext:Landroid/content/Context;

    .line 132
    invoke-static {}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->getInstance()Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mDeviceConnector:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    .line 134
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$SensorErrorTimer;

    const-wide/16 v2, 0xbb8

    const-wide/16 v4, 0xc8

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$SensorErrorTimer;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;JJ)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mSensorErrorTimer:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$SensorErrorTimer;

    .line 136
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->pulseAnimationID:[I

    .line 151
    iput-boolean v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->isMeasureCompleted:Z

    .line 162
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->errorImages:[I

    .line 164
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$1;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mHandler:Landroid/os/Handler;

    .line 951
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$13;

    const-wide/16 v2, 0x2710

    const-wide/16 v4, 0x64

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$13;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;JJ)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->readyTimer:Landroid/os/CountDownTimer;

    .line 213
    return-void

    .line 136
    :array_0
    .array-data 4
        0x7f0201f5
        0x7f020202
    .end array-data

    .line 162
    :array_1
    .array-data 4
        0x7f0201f2
        0x7f0201f3
        0x7f0201f4
    .end array-data
.end method

.method public constructor <init>(Landroid/app/Activity;)V
    .locals 7
    .param p1, "_activity"    # Landroid/app/Activity;

    .prologue
    const/4 v6, 0x0

    .line 223
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;-><init>()V

    .line 95
    iput-boolean v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->isFingerDetected:Z

    .line 96
    iput-boolean v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->isRetryMeasuring:Z

    .line 97
    iput-boolean v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mIsRunPulseView:Z

    .line 98
    iput-boolean v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mIsGreen:Z

    .line 99
    iput-boolean v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->isEndPulseDelay:Z

    .line 100
    iput-boolean v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mIsOnConfigChanged:Z

    .line 105
    iput v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mMeasureFailCount:I

    .line 106
    iput v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mHeartRate:I

    .line 110
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mContext:Landroid/content/Context;

    .line 132
    invoke-static {}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->getInstance()Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mDeviceConnector:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    .line 134
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$SensorErrorTimer;

    const-wide/16 v2, 0xbb8

    const-wide/16 v4, 0xc8

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$SensorErrorTimer;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;JJ)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mSensorErrorTimer:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$SensorErrorTimer;

    .line 136
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->pulseAnimationID:[I

    .line 151
    iput-boolean v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->isMeasureCompleted:Z

    .line 162
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->errorImages:[I

    .line 164
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$1;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mHandler:Landroid/os/Handler;

    .line 951
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$13;

    const-wide/16 v2, 0x2710

    const-wide/16 v4, 0x64

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$13;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;JJ)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->readyTimer:Landroid/os/CountDownTimer;

    .line 224
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mCurrentActivity:Landroid/app/Activity;

    .line 225
    return-void

    .line 136
    :array_0
    .array-data 4
        0x7f0201f5
        0x7f020202
    .end array-data

    .line 162
    :array_1
    .array-data 4
        0x7f0201f2
        0x7f0201f3
        0x7f0201f4
    .end array-data
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mSecondCenterIcon:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;Landroid/widget/ImageView;)Landroid/widget/ImageView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;
    .param p1, "x1"    # Landroid/widget/ImageView;

    .prologue
    .line 88
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mSecondCenterIcon:Landroid/widget/ImageView;

    return-object p1
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)Landroid/widget/FrameLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mSecondCenterPulseLayout:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;Landroid/widget/FrameLayout;)Landroid/widget/FrameLayout;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;
    .param p1, "x1"    # Landroid/widget/FrameLayout;

    .prologue
    .line 88
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mSecondCenterPulseLayout:Landroid/widget/FrameLayout;

    return-object p1
.end method

.method static synthetic access$102(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;
    .param p1, "x1"    # I

    .prologue
    .line 88
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mPrevError:I

    return p1
.end method

.method static synthetic access$1102(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;
    .param p1, "x1"    # Landroid/widget/LinearLayout;

    .prologue
    .line 88
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mSecondCenterPulseViewLayout:Landroid/widget/LinearLayout;

    return-object p1
.end method

.method static synthetic access$1200(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)Lcom/sec/android/app/shealth/heartrate/HRMGIFView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mSecondCenterPulseViewGreenIcon:Lcom/sec/android/app/shealth/heartrate/HRMGIFView;

    return-object v0
.end method

.method static synthetic access$1202(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;Lcom/sec/android/app/shealth/heartrate/HRMGIFView;)Lcom/sec/android/app/shealth/heartrate/HRMGIFView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/heartrate/HRMGIFView;

    .prologue
    .line 88
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mSecondCenterPulseViewGreenIcon:Lcom/sec/android/app/shealth/heartrate/HRMGIFView;

    return-object p1
.end method

.method static synthetic access$1300(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)[I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->pulseAnimationID:[I

    return-object v0
.end method

.method static synthetic access$1402(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;Landroid/widget/ImageView;)Landroid/widget/ImageView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;
    .param p1, "x1"    # Landroid/widget/ImageView;

    .prologue
    .line 88
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mSecondCenterIconBig:Landroid/widget/ImageView;

    return-object p1
.end method

.method static synthetic access$1500(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)Landroid/app/Activity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mCurrentActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    .prologue
    .line 88
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->initAoudioFile()V

    return-void
.end method

.method static synthetic access$1702(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;Landroid/view/animation/Animation;)Landroid/view/animation/Animation;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;
    .param p1, "x1"    # Landroid/view/animation/Animation;

    .prologue
    .line 88
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mScaleAnimation:Landroid/view/animation/Animation;

    return-object p1
.end method

.method static synthetic access$1802(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 88
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mIsRunPulseView:Z

    return p1
.end method

.method static synthetic access$1900(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    .prologue
    .line 88
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mIsOnConfigChanged:Z

    return v0
.end method

.method static synthetic access$1902(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 88
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mIsOnConfigChanged:Z

    return p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;
    .param p1, "x1"    # I

    .prologue
    .line 88
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->setReadyUI(I)V

    return-void
.end method

.method static synthetic access$2000(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mAlertBuild_Error:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    return-object v0
.end method

.method static synthetic access$2002(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .prologue
    .line 88
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mAlertBuild_Error:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    return-object p1
.end method

.method static synthetic access$2100(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mAlertBuild_HRM_Measure:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    return-object v0
.end method

.method static synthetic access$2102(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .prologue
    .line 88
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mAlertBuild_HRM_Measure:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    return-object p1
.end method

.method static synthetic access$2200(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;
    .param p1, "x1"    # I

    .prologue
    .line 88
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->showErrorDialog(I)V

    return-void
.end method

.method static synthetic access$2300(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mFragmentHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mAlertBuild_HRM_Result:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    return-object v0
.end method

.method static synthetic access$2502(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .prologue
    .line 88
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mAlertBuild_HRM_Result:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    return-object p1
.end method

.method static synthetic access$2600(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    .prologue
    .line 88
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->isFingerDetected:Z

    return v0
.end method

.method static synthetic access$2700(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)Landroid/os/CountDownTimer;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->readyTimer:Landroid/os/CountDownTimer;

    return-object v0
.end method

.method static synthetic access$2800(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$SensorErrorTimer;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mSensorErrorTimer:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$SensorErrorTimer;

    return-object v0
.end method

.method static synthetic access$2900(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;
    .param p1, "x1"    # I

    .prologue
    .line 88
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->setMeasuringUI(I)V

    return-void
.end method

.method static synthetic access$3000(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 88
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->pulseAnimationShow(Z)V

    return-void
.end method

.method static synthetic access$3100()[I
    .locals 1

    .prologue
    .line 88
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->effectAudio:[I

    return-object v0
.end method

.method static synthetic access$3200(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)Landroid/media/SoundPool;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->pool:Landroid/media/SoundPool;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    .prologue
    .line 88
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->isRetryMeasuring:Z

    return v0
.end method

.method static synthetic access$3400(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 88
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->changeUIReadyToMeasuring(Z)V

    return-void
.end method

.method static synthetic access$3500(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mDeviceConnector:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    return-object v0
.end method

.method static synthetic access$3600(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;
    .param p1, "x1"    # I

    .prologue
    .line 88
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->playSound(I)V

    return-void
.end method

.method static synthetic access$3700(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    .prologue
    .line 88
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mHeartRate:I

    return v0
.end method

.method static synthetic access$3702(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;
    .param p1, "x1"    # I

    .prologue
    .line 88
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mHeartRate:I

    return p1
.end method

.method static synthetic access$3800(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    .prologue
    .line 88
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->bpm:I

    return v0
.end method

.method static synthetic access$3802(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;
    .param p1, "x1"    # I

    .prologue
    .line 88
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->bpm:I

    return p1
.end method

.method static synthetic access$3900(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->layoutMedical:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$3902(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;
    .param p1, "x1"    # Landroid/widget/LinearLayout;

    .prologue
    .line 88
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->layoutMedical:Landroid/widget/LinearLayout;

    return-object p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;
    .param p1, "x1"    # I

    .prologue
    .line 88
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->setMeasuringFailUI(I)V

    return-void
.end method

.method static synthetic access$4000(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->layoutNonMedical:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$4002(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;
    .param p1, "x1"    # Landroid/widget/LinearLayout;

    .prologue
    .line 88
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->layoutNonMedical:Landroid/widget/LinearLayout;

    return-object p1
.end method

.method static synthetic access$4100(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->hrBpmTextNonMedical:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$4102(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 88
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->hrBpmTextNonMedical:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$4200(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateSummaryBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mStateBar:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateSummaryBar;

    return-object v0
.end method

.method static synthetic access$4202(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateSummaryBar;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateSummaryBar;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateSummaryBar;

    .prologue
    .line 88
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mStateBar:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateSummaryBar;

    return-object p1
.end method

.method static synthetic access$4300(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->hrBpmText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$4302(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 88
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->hrBpmText:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$4400(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->minBpmText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$4402(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 88
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->minBpmText:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$4500(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->maxBpmText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$4502(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 88
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->maxBpmText:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$4600(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->rangeText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$4602(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 88
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->rangeText:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$4702(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;
    .param p1, "x1"    # I

    .prologue
    .line 88
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mPolygonWidth:I

    return p1
.end method

.method static synthetic access$4802(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;
    .param p1, "x1"    # I

    .prologue
    .line 88
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mPolygonHeight:I

    return p1
.end method

.method static synthetic access$4900(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;
    .param p1, "x1"    # I

    .prologue
    .line 88
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->setHRMStateBar(I)V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    .prologue
    .line 88
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->refreshFragmentFocusables()V

    return-void
.end method

.method static synthetic access$5000(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    .prologue
    .line 88
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->minHRzone:I

    return v0
.end method

.method static synthetic access$5100(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    .prologue
    .line 88
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->maxHRzone:I

    return v0
.end method

.method static synthetic access$5200(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mAnimationView:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$5202(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;Landroid/widget/ImageView;)Landroid/widget/ImageView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;
    .param p1, "x1"    # Landroid/widget/ImageView;

    .prologue
    .line 88
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mAnimationView:Landroid/widget/ImageView;

    return-object p1
.end method

.method static synthetic access$5300(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)[I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->errorImages:[I

    return-object v0
.end method

.method static synthetic access$5400(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    .prologue
    .line 88
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mErrorType:I

    return v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mFirstStatus:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$602(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 88
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mFirstStatus:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mFirstMessage:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$702(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 88
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mFirstMessage:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$802(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleProgressView;)Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleProgressView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleProgressView;

    .prologue
    .line 88
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mCircleView:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleProgressView;

    return-object p1
.end method

.method static synthetic access$902(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;Landroid/widget/FrameLayout;)Landroid/widget/FrameLayout;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;
    .param p1, "x1"    # Landroid/widget/FrameLayout;

    .prologue
    .line 88
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mSecondLayout:Landroid/widget/FrameLayout;

    return-object p1
.end method

.method private changeUIReadyToMeasuring(Z)V
    .locals 4
    .param p1, "isFail"    # Z

    .prologue
    .line 1395
    sget-object v2, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "sth  changeReadyToMeasuringUI "

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1398
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->readyTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v2}, Landroid/os/CountDownTimer;->cancel()V

    .line 1400
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mSensorErrorTimer:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$SensorErrorTimer;

    if-eqz v2, :cond_0

    .line 1401
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mSensorErrorTimer:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$SensorErrorTimer;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$SensorErrorTimer;->cancel()V

    .line 1405
    :cond_0
    if-eqz p1, :cond_1

    .line 1406
    const/4 v0, 0x0

    .line 1410
    .local v0, "failMsg":I
    :goto_0
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    .line 1411
    .local v1, "msg":Landroid/os/Message;
    const/4 v2, 0x1

    iput v2, v1, Landroid/os/Message;->what:I

    .line 1412
    iput v0, v1, Landroid/os/Message;->arg1:I

    .line 1413
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1414
    return-void

    .line 1408
    .end local v0    # "failMsg":I
    .end local v1    # "msg":Landroid/os/Message;
    :cond_1
    const/4 v0, 0x1

    .restart local v0    # "failMsg":I
    goto :goto_0
.end method

.method private checkCircleViewStatus()V
    .locals 2

    .prologue
    .line 755
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mCircleView:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleProgressView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleProgressView;->isRun()Z

    move-result v0

    if-nez v0, :cond_0

    .line 756
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mCircleView:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleProgressView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleProgressView;->setVisibility(I)V

    .line 757
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mCircleView:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleProgressView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleProgressView;->startAnimation()V

    .line 759
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mCircleView:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleProgressView;

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mIsGreen:Z

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleProgressView;->changeCircleColor(Z)V

    .line 760
    return-void
.end method

.method private checkRunPulseView(Z)V
    .locals 2
    .param p1, "isShowScaleAnimator"    # Z

    .prologue
    .line 763
    if-eqz p1, :cond_0

    .line 764
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mIsGreen:Z

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->scaleAnimationStarter(Z)V

    .line 769
    :goto_0
    return-void

    .line 766
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mSecondCenterPulseLayout:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 767
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mIsGreen:Z

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->showHRMPulseAnimation(Z)V

    goto :goto_0
.end method

.method private convertDptoPx(I)I
    .locals 5
    .param p1, "dp"    # I

    .prologue
    .line 1340
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 1341
    .local v2, "res":Landroid/content/res/Resources;
    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 1342
    .local v0, "metrics":Landroid/util/DisplayMetrics;
    int-to-float v3, p1

    iget v4, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float v1, v3, v4

    .line 1343
    .local v1, "px":F
    float-to-int v3, v1

    return v3
.end method

.method private firstAnimation(Landroid/view/View;I)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "stringID"    # I

    .prologue
    .line 544
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mCurrentActivity:Landroid/app/Activity;

    if-nez v0, :cond_0

    .line 573
    :goto_0
    return-void

    .line 546
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mCurrentActivity:Landroid/app/Activity;

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$10;

    invoke-direct {v1, p0, p1, p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$10;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;Landroid/view/View;I)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private getHRguidText(III)I
    .locals 1
    .param p1, "minhr"    # I
    .param p2, "maxhr"    # I
    .param p3, "hr"    # I

    .prologue
    .line 1758
    if-ge p3, p1, :cond_0

    .line 1759
    const/16 v0, 0x7531

    .line 1763
    :goto_0
    return v0

    .line 1760
    :cond_0
    if-le p3, p2, :cond_1

    .line 1761
    const/16 v0, 0x7530

    goto :goto_0

    .line 1763
    :cond_1
    const/16 v0, 0x7532

    goto :goto_0
.end method

.method private getMaxHR(I)I
    .locals 5
    .param p1, "age"    # I

    .prologue
    const/16 v4, 0x32

    const/16 v3, 0x2d

    const/16 v2, 0x28

    const/16 v1, 0x23

    const/16 v0, 0x1e

    .line 1733
    if-lez p1, :cond_0

    if-ge p1, v0, :cond_0

    .line 1734
    const/16 v0, 0xc8

    .line 1753
    :goto_0
    return v0

    .line 1735
    :cond_0
    if-lt p1, v0, :cond_1

    if-ge p1, v1, :cond_1

    .line 1736
    const/16 v0, 0xbe

    goto :goto_0

    .line 1737
    :cond_1
    if-lt p1, v1, :cond_2

    if-ge p1, v2, :cond_2

    .line 1738
    const/16 v0, 0xb9

    goto :goto_0

    .line 1739
    :cond_2
    if-lt p1, v2, :cond_3

    if-ge p1, v3, :cond_3

    .line 1740
    const/16 v0, 0xb4

    goto :goto_0

    .line 1741
    :cond_3
    if-lt p1, v3, :cond_4

    if-ge p1, v4, :cond_4

    .line 1742
    const/16 v0, 0xaf

    goto :goto_0

    .line 1743
    :cond_4
    if-lt p1, v4, :cond_5

    const/16 v0, 0x37

    if-ge p1, v0, :cond_5

    .line 1744
    const/16 v0, 0xaa

    goto :goto_0

    .line 1745
    :cond_5
    const/16 v0, 0x37

    if-lt p1, v0, :cond_6

    const/16 v0, 0x3c

    if-ge p1, v0, :cond_6

    .line 1746
    const/16 v0, 0xa5

    goto :goto_0

    .line 1747
    :cond_6
    const/16 v0, 0x3c

    if-lt p1, v0, :cond_7

    const/16 v0, 0x41

    if-ge p1, v0, :cond_7

    .line 1748
    const/16 v0, 0xa0

    goto :goto_0

    .line 1749
    :cond_7
    const/16 v0, 0x41

    if-lt p1, v0, :cond_8

    const/16 v0, 0x46

    if-ge p1, v0, :cond_8

    .line 1750
    const/16 v0, 0x9b

    goto :goto_0

    .line 1751
    :cond_8
    const/16 v0, 0x46

    if-lt p1, v0, :cond_9

    .line 1752
    const/16 v0, 0x96

    goto :goto_0

    .line 1753
    :cond_9
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getUserAge()I
    .locals 9

    .prologue
    const/4 v8, 0x6

    const/4 v7, 0x1

    .line 1713
    new-instance v3, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v3, v5}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    .line 1715
    .local v3, "profile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    new-instance v1, Ljava/util/GregorianCalendar;

    invoke-direct {v1}, Ljava/util/GregorianCalendar;-><init>()V

    .line 1716
    .local v1, "birth":Ljava/util/Calendar;
    new-instance v4, Ljava/util/GregorianCalendar;

    invoke-direct {v4}, Ljava/util/GregorianCalendar;-><init>()V

    .line 1718
    .local v4, "today":Ljava/util/Calendar;
    new-instance v0, Ljava/util/Date;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getBirthDate()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/shealth/common/utils/Utils;->convertDateStringToLong(Ljava/lang/String;)J

    move-result-wide v5

    invoke-direct {v0, v5, v6}, Ljava/util/Date;-><init>(J)V

    .line 1720
    .local v0, "bDate":Ljava/util/Date;
    invoke-virtual {v1, v0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 1721
    new-instance v5, Ljava/util/Date;

    invoke-direct {v5}, Ljava/util/Date;-><init>()V

    invoke-virtual {v4, v5}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 1723
    const/4 v2, 0x0

    .line 1724
    .local v2, "factor":I
    invoke-virtual {v4, v8}, Ljava/util/Calendar;->get(I)I

    move-result v5

    invoke-virtual {v1, v8}, Ljava/util/Calendar;->get(I)I

    move-result v6

    if-ge v5, v6, :cond_0

    .line 1725
    const/4 v2, -0x1

    .line 1728
    :cond_0
    invoke-virtual {v4, v7}, Ljava/util/Calendar;->get(I)I

    move-result v5

    invoke-virtual {v1, v7}, Ljava/util/Calendar;->get(I)I

    move-result v6

    sub-int/2addr v5, v6

    add-int/2addr v5, v2

    return v5
.end method

.method private initAoudioFile()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v4, 0x0

    const/4 v5, 0x1

    .line 853
    new-instance v0, Landroid/media/SoundPool;

    invoke-direct {v0, v5, v6, v4}, Landroid/media/SoundPool;-><init>(III)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->pool:Landroid/media/SoundPool;

    .line 854
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->effectAudio:[I

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->pool:Landroid/media/SoundPool;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mContext:Landroid/content/Context;

    const v3, 0x7f06002c

    invoke-virtual {v1, v2, v3, v5}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v1

    aput v1, v0, v4

    .line 855
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->effectAudio:[I

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->pool:Landroid/media/SoundPool;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mContext:Landroid/content/Context;

    const v3, 0x7f060027

    invoke-virtual {v1, v2, v3, v5}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v1

    aput v1, v0, v5

    .line 856
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->effectAudio:[I

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->pool:Landroid/media/SoundPool;

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mContext:Landroid/content/Context;

    const v4, 0x7f06002d

    invoke-virtual {v2, v3, v4, v5}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v2

    aput v2, v0, v1

    .line 857
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->effectAudio:[I

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->pool:Landroid/media/SoundPool;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mContext:Landroid/content/Context;

    const v3, 0x7f060028

    invoke-virtual {v1, v2, v3, v5}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v1

    aput v1, v0, v6

    .line 858
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->effectAudio:[I

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->pool:Landroid/media/SoundPool;

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mContext:Landroid/content/Context;

    const v4, 0x7f06002a

    invoke-virtual {v2, v3, v4, v5}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v2

    aput v2, v0, v1

    .line 859
    return-void
.end method

.method private initMeasureStatus()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1417
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->isFingerDetected:Z

    .line 1418
    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mMeasureFailCount:I

    .line 1419
    return-void
.end method

.method private playSound(I)V
    .locals 3
    .param p1, "effectAudio"    # I

    .prologue
    .line 863
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mContext:Landroid/content/Context;

    const-string v2, "audio"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 865
    .local v0, "aManager":Landroid/media/AudioManager;
    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v1

    if-nez v1, :cond_1

    .line 883
    :cond_0
    :goto_0
    return-void

    .line 869
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->pool:Landroid/media/SoundPool;

    if-eqz v1, :cond_0

    .line 870
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mCurrentActivity:Landroid/app/Activity;

    new-instance v2, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$12;

    invoke-direct {v2, p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$12;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;I)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private pulseAnimationShow(Z)V
    .locals 2
    .param p1, "isGreen"    # Z

    .prologue
    .line 1451
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mSecondCenterIcon:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 1452
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mSecondCenterIcon:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1453
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mSecondCenterPulseLayout:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1454
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->showHRMPulseAnimation(Z)V

    .line 1455
    return-void
.end method

.method private scaleAnimationStarter(Z)V
    .locals 3
    .param p1, "isGreen"    # Z

    .prologue
    .line 656
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "sth scaleAnimationStarter isGreen: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 657
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mScaleAnimation:Landroid/view/animation/Animation;

    if-nez v0, :cond_0

    .line 658
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mCurrentActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f04001b

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mScaleAnimation:Landroid/view/animation/Animation;

    .line 660
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mScaleAnimation:Landroid/view/animation/Animation;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 661
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mScaleAnimation:Landroid/view/animation/Animation;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setFillBefore(Z)V

    .line 662
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mScaleAnimation:Landroid/view/animation/Animation;

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$11;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$11;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;Z)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 683
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mSecondCenterIcon:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mScaleAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 688
    :goto_0
    return-void

    .line 685
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mSecondCenterIcon:Landroid/widget/ImageView;

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mIsGreen:Z

    if-eqz v0, :cond_1

    const v0, 0x7f020526

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :cond_1
    const v0, 0x7f020529

    goto :goto_1
.end method

.method private setHRMStateBar(I)V
    .locals 11
    .param p1, "bpm"    # I

    .prologue
    const/4 v8, 0x2

    const/4 v10, 0x0

    .line 1266
    new-array v0, v8, [I

    fill-array-data v0, :array_0

    .line 1267
    .local v0, "avgRange":[I
    new-array v6, v8, [I

    iget v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->minHRzone:I

    aput v8, v6, v10

    const/4 v8, 0x1

    iget v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->maxHRzone:I

    aput v9, v6, v8

    .line 1268
    .local v6, "percentile":[I
    const/16 v3, 0xc8

    .line 1269
    .local v3, "maxHR":I
    const/4 v1, 0x0

    .line 1270
    .local v1, "greenBarMargin":I
    const/4 v2, 0x0

    .line 1271
    .local v2, "greenBarWidth":I
    const/16 v8, 0x10c

    invoke-direct {p0, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->convertDptoPx(I)I

    move-result v7

    .line 1272
    .local v7, "width":I
    const/4 v4, 0x1

    .line 1274
    .local v4, "n":I
    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->minBpmText:Landroid/widget/TextView;

    invoke-virtual {v8}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 1276
    .local v5, "number":I
    iget v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->maxHRzone:I

    iget v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->minHRzone:I

    sub-int/2addr v8, v9

    int-to-float v8, v8

    int-to-float v9, v3

    div-float/2addr v8, v9

    int-to-float v9, v7

    mul-float/2addr v8, v9

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 1277
    iget v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->minHRzone:I

    int-to-float v8, v8

    int-to-float v9, v3

    div-float/2addr v8, v9

    int-to-float v9, v7

    mul-float/2addr v8, v9

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 1279
    :goto_0
    div-int/lit8 v5, v5, 0xa

    if-eqz v5, :cond_0

    .line 1280
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1283
    :cond_0
    const/4 v8, 0x3

    if-lt v4, v8, :cond_1

    .line 1284
    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->minBpmText:Landroid/widget/TextView;

    add-int/lit8 v9, v1, 0x10

    invoke-virtual {v8, v9, v10, v10, v10}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 1287
    :goto_1
    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->maxBpmText:Landroid/widget/TextView;

    add-int v9, v1, v2

    invoke-virtual {v8, v9, v10, v10, v10}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 1289
    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mStateBar:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateSummaryBar;

    invoke-virtual {v8, p1, v0, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateSummaryBar;->moveToPolygon(I[I[I)V

    .line 1290
    return-void

    .line 1286
    :cond_1
    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->minBpmText:Landroid/widget/TextView;

    add-int/lit8 v9, v1, 0x17

    invoke-virtual {v8, v9, v10, v10, v10}, Landroid/widget/TextView;->setPadding(IIII)V

    goto :goto_1

    .line 1266
    :array_0
    .array-data 4
        0x0
        0xc8
    .end array-data
.end method

.method private setMeasuringFailUI(I)V
    .locals 3
    .param p1, "errorType"    # I

    .prologue
    const/4 v2, 0x3

    const/4 v1, 0x0

    .line 692
    const/4 v0, -0x6

    if-ne p1, v0, :cond_3

    .line 693
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mCurrentActivity:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 694
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mCurrentActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 697
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mAlertBuild_Error:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mAlertBuild_Error:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mAlertBuild_HRM_Measure:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mAlertBuild_HRM_Measure:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 699
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mAlertBuild_Error:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mAlertBuild_HRM_Measure:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 700
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->effectAudio:[I

    aget v0, v0, v2

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->playSound(I)V

    .line 701
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->showErrorDialog(I)V

    .line 733
    :cond_1
    :goto_0
    return-void

    .line 703
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mAlertBuild_HRM_Measure:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mAlertBuild_HRM_Measure:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 704
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mAlertBuild_HRM_Measure:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 705
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->effectAudio:[I

    aget v0, v0, v2

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->playSound(I)V

    .line 706
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->showErrorDialog(I)V

    goto :goto_0

    .line 711
    :cond_3
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mIsGreen:Z

    .line 712
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mIsRunPulseView:Z

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->checkRunPulseView(Z)V

    .line 713
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mIsRunPulseView:Z

    .line 715
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->checkCircleViewStatus()V

    .line 718
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mFirstStatus:Landroid/widget/TextView;

    const v1, 0x7f090c04

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 719
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mFirstMessage:Landroid/widget/TextView;

    const v1, 0x7f090c23

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 722
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mFirstMessage:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070182

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 723
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mSecondLayout:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_4

    .line 724
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mSecondLayout:Landroid/widget/FrameLayout;

    const v1, 0x7f020524

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    .line 726
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mSecondCenterIcon:Landroid/widget/ImageView;

    const v1, 0x7f020529

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 729
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mErrorType:I

    .line 730
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;->MEASURING_ERROR:Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->startTimer(Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;)V

    goto :goto_0
.end method

.method private setMeasuringUI(I)V
    .locals 6
    .param p1, "isShowAnimation"    # I

    .prologue
    const v5, 0x7f090c23

    const v4, 0x7f090c04

    const v3, 0x106000c

    const/4 v2, 0x0

    .line 624
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mCurrentActivity:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 625
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mCurrentActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 627
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mSecondLayout:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_1

    .line 628
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mSecondLayout:Landroid/widget/FrameLayout;

    const v1, 0x7f020522

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    .line 630
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mSecondCenterIconBig:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 632
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mIsGreen:Z

    .line 633
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mIsRunPulseView:Z

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->checkRunPulseView(Z)V

    .line 634
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mIsRunPulseView:Z

    .line 636
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;->MEASURING:Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->startTimer(Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;)V

    .line 638
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->checkCircleViewStatus()V

    .line 640
    if-nez p1, :cond_2

    .line 641
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mFirstStatus:Landroid/widget/TextView;

    invoke-direct {p0, v0, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->firstAnimation(Landroid/view/View;I)V

    .line 642
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mFirstMessage:Landroid/widget/TextView;

    invoke-direct {p0, v0, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->firstAnimation(Landroid/view/View;I)V

    .line 653
    :goto_0
    return-void

    .line 644
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mFirstStatus:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 645
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mFirstMessage:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 647
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mFirstStatus:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(I)V

    .line 648
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mFirstStatus:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 649
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mFirstMessage:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(I)V

    .line 650
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mFirstMessage:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method

.method private setReadyUI(I)V
    .locals 7
    .param p1, "animation"    # I

    .prologue
    const v6, 0x7f090c22

    const v5, 0x7f090c05

    const v4, 0x106000c

    const/4 v3, 0x0

    .line 593
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mIsRunPulseView:Z

    .line 594
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mSecondCenterIconBig:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->clearAnimation()V

    .line 595
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mSecondCenterIcon:Landroid/widget/ImageView;

    const v2, 0x7f020527

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 597
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 598
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f04000e

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 599
    .local v0, "alphaAnimation":Landroid/view/animation/Animation;
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mSecondCenterIcon:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 601
    .end local v0    # "alphaAnimation":Landroid/view/animation/Animation;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mSecondCenterIcon:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 602
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mSecondLayout:Landroid/widget/FrameLayout;

    if-eqz v1, :cond_1

    .line 603
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mSecondLayout:Landroid/widget/FrameLayout;

    const v2, 0x7f020522

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    .line 606
    :cond_1
    if-nez p1, :cond_2

    .line 607
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mFirstStatus:Landroid/widget/TextView;

    invoke-direct {p0, v1, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->firstAnimation(Landroid/view/View;I)V

    .line 608
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mFirstMessage:Landroid/widget/TextView;

    invoke-direct {p0, v1, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->firstAnimation(Landroid/view/View;I)V

    .line 619
    :goto_0
    sget-object v1, Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;->READY:Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->startTimer(Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;)V

    .line 620
    return-void

    .line 610
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mFirstStatus:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 611
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mFirstMessage:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 613
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mFirstStatus:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(I)V

    .line 614
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mFirstStatus:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 615
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mFirstMessage:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setText(I)V

    .line 616
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mFirstMessage:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method

.method private showErrorDialog(I)V
    .locals 6
    .param p1, "errorType"    # I

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 1157
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "sth  showErrorDialog error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1159
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mFirstStatus:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1160
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mFirstMessage:Landroid/widget/TextView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1161
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mSecondCenterPulseLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v5}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1163
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->stopHRMPulseAnimation()V

    .line 1166
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mCircleView:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleProgressView;

    if-eqz v1, :cond_0

    .line 1167
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mCircleView:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleProgressView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleProgressView;->stopAnimation()V

    .line 1170
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mCurrentActivity:Landroid/app/Activity;

    if-nez v1, :cond_2

    .line 1263
    :cond_1
    :goto_0
    return-void

    .line 1173
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mCurrentActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x80

    invoke-virtual {v1, v2}, Landroid/view/Window;->clearFlags(I)V

    .line 1175
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mCurrentActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 1178
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelable(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1179
    const v1, 0x7f0907ad

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1180
    const v1, 0x7f090047

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1181
    const v1, 0x7f030142

    new-instance v2, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$19;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$19;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(ILcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1220
    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$20;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$20;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnBackPressListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1229
    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$21;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$21;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelButtonListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnCancelButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1237
    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$22;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$22;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1246
    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$23;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$23;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnDismissListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1256
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1257
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mAlertBuild_Error:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-nez v1, :cond_3

    .line 1258
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mAlertBuild_Error:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 1260
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mAlertBuild_Error:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "error"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private showHRMPulseAnimation(Z)V
    .locals 3
    .param p1, "isGreen"    # Z

    .prologue
    const/16 v2, 0x8

    .line 736
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mSecondCenterPulseViewGreenIcon:Lcom/sec/android/app/shealth/heartrate/HRMGIFView;

    if-eqz v0, :cond_0

    .line 737
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mSecondCenterIconBig:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 738
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mSecondCenterPulseViewGreenIcon:Lcom/sec/android/app/shealth/heartrate/HRMGIFView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->clearAnimation()V

    .line 739
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mSecondCenterPulseViewGreenIcon:Lcom/sec/android/app/shealth/heartrate/HRMGIFView;

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mIsGreen:Z

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->setPulseAnimationColor(Z)V

    .line 740
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mSecondCenterPulseViewGreenIcon:Lcom/sec/android/app/shealth/heartrate/HRMGIFView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->getVisibility()I

    move-result v0

    if-ne v0, v2, :cond_0

    .line 741
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mSecondCenterPulseViewGreenIcon:Lcom/sec/android/app/shealth/heartrate/HRMGIFView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->setVisibility(I)V

    .line 743
    :cond_0
    return-void
.end method

.method private startTimer(Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;)V
    .locals 2
    .param p1, "timerType"    # Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;

    .prologue
    .line 1423
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mCurrentActivity:Landroid/app/Activity;

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$25;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$25;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1448
    return-void
.end method

.method private stopHRMPulseAnimation()V
    .locals 2

    .prologue
    .line 746
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mSecondCenterPulseViewGreenIcon:Lcom/sec/android/app/shealth/heartrate/HRMGIFView;

    if-eqz v0, :cond_0

    .line 747
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mSecondCenterPulseViewGreenIcon:Lcom/sec/android/app/shealth/heartrate/HRMGIFView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->setVisibility(I)V

    .line 748
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mSecondCenterPulseViewGreenIcon:Lcom/sec/android/app/shealth/heartrate/HRMGIFView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->clearAnimation()V

    .line 749
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mSecondCenterPulseViewGreenIcon:Lcom/sec/android/app/shealth/heartrate/HRMGIFView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->stopAnimation()V

    .line 751
    :cond_0
    return-void
.end method

.method private stopSensorAndAnimationInitialize()V
    .locals 2

    .prologue
    .line 527
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mDeviceConnector:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->stopMeasuring()V

    .line 530
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mDeviceConnector:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->stopCountDownTimer()V

    .line 531
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$9;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$9;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 541
    return-void
.end method


# virtual methods
.method public animate(Landroid/widget/ImageView;[IIZ)V
    .locals 12
    .param p1, "imageView"    # Landroid/widget/ImageView;
    .param p2, "images"    # [I
    .param p3, "imageIndex"    # I
    .param p4, "forever"    # Z

    .prologue
    .line 1768
    const/16 v8, 0x1f4

    .line 1769
    .local v8, "fadeInDuration":I
    const/16 v11, 0x3e8

    .line 1770
    .local v11, "timeBetween":I
    const/16 v10, 0x1f4

    .line 1772
    .local v10, "fadeOutDuration":I
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1773
    aget v0, p2, p3

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1775
    new-instance v7, Landroid/view/animation/AlphaAnimation;

    const/4 v0, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {v7, v0, v1}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 1776
    .local v7, "fadeIn":Landroid/view/animation/Animation;
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v7, v0}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 1777
    int-to-long v0, v8

    invoke-virtual {v7, v0, v1}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1779
    new-instance v9, Landroid/view/animation/AlphaAnimation;

    const/high16 v0, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    invoke-direct {v9, v0, v1}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 1780
    .local v9, "fadeOut":Landroid/view/animation/Animation;
    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v9, v0}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 1781
    const/16 v0, 0x5dc

    int-to-long v0, v0

    invoke-virtual {v9, v0, v1}, Landroid/view/animation/Animation;->setStartOffset(J)V

    .line 1782
    int-to-long v0, v10

    invoke-virtual {v9, v0, v1}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1784
    new-instance v6, Landroid/view/animation/AnimationSet;

    const/4 v0, 0x0

    invoke-direct {v6, v0}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 1785
    .local v6, "animation":Landroid/view/animation/AnimationSet;
    invoke-virtual {v6, v7}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 1786
    invoke-virtual {v6, v9}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 1787
    const/4 v0, 0x1

    invoke-virtual {v6, v0}, Landroid/view/animation/AnimationSet;->setRepeatCount(I)V

    .line 1788
    invoke-virtual {p1, v6}, Landroid/widget/ImageView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 1790
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$26;

    move-object v1, p0

    move-object v2, p2

    move v3, p3

    move-object v4, p1

    move/from16 v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$26;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;[IILandroid/widget/ImageView;Z)V

    invoke-virtual {v6, v0}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1808
    return-void
.end method

.method public createMeasurementDialog()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 250
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mCurrentActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x80

    invoke-virtual {v1, v2}, Landroid/view/Window;->clearFlags(I)V

    .line 251
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    .line 252
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->isMeasureCompleted:Z

    .line 253
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mCurrentActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 255
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelable(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 256
    const v1, 0x7f090c04

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 257
    const v1, 0x7f090048

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 258
    const v1, 0x7f0301a3

    new-instance v2, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$2;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(ILcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 333
    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$3;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnDismissListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 348
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 349
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mAlertBuild_HRM_Measure:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-nez v1, :cond_0

    .line 350
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mAlertBuild_HRM_Measure:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 353
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mAlertBuild_HRM_Measure:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string/jumbo v3, "measure"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 355
    return-void
.end method

.method public deleteMeasurementDialog(IZ)V
    .locals 2
    .param p1, "hrmValue"    # I
    .param p2, "exitOnResume"    # Z

    .prologue
    .line 358
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mAlertBuild_HRM_Measure:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_0

    .line 359
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mAlertBuild_HRM_Measure:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 361
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mAlertBuild_HRM_Measure:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$4;

    invoke-direct {v1, p0, p2, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$4;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;ZI)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->setOnDismissListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;)V

    .line 376
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mAlertBuild_HRM_Measure:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->dismiss()V

    .line 383
    :cond_0
    :goto_0
    return-void

    .line 380
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mAlertBuild_HRM_Measure:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    goto :goto_0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 235
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mCurrentActivity:Landroid/app/Activity;

    .line 236
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onAttach(Landroid/app/Activity;)V

    .line 237
    return-void
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 395
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 387
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 388
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "sth  HeartrateSummaryFragmentNew onConfigurationChanged : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/res/Configuration;->getLayoutDirection()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p1, Landroid/content/res/Configuration;->userSetLocale:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 390
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mIsOnConfigChanged:Z

    .line 391
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 241
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "sth  onCreateView enter"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 242
    const v0, 0x7f0301a5

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mMainView:Landroid/view/View;

    .line 243
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->setRetainInstance(Z)V

    .line 245
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mMainView:Landroid/view/View;

    return-object v0
.end method

.method public onDataReceived(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor;)V
    .locals 10
    .param p1, "hrm"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor;

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v7, -0x1

    const/16 v6, -0x9

    const/4 v5, 0x2

    .line 887
    iget v1, p1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor;->heartRate:I

    .line 889
    .local v1, "rate":I
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mCircleView:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleProgressView;

    if-nez v2, :cond_1

    .line 890
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->stopSensorAndAnimationInitialize()V

    .line 935
    :cond_0
    :goto_0
    return-void

    .line 894
    :cond_1
    sget-object v2, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "sth  ShealthSensorDevice.DataListener onDataReceived: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " isDetected: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->isFingerDetected:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 896
    iget-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->isFingerDetected:Z

    if-eqz v2, :cond_3

    .line 897
    iput-boolean v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->isRetryMeasuring:Z

    .line 898
    if-eq v1, v7, :cond_0

    .line 901
    if-gez v1, :cond_2

    if-lt v1, v6, :cond_2

    .line 902
    iput-boolean v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->isFingerDetected:Z

    .line 904
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 905
    .local v0, "msg":Landroid/os/Message;
    iput v5, v0, Landroid/os/Message;->what:I

    .line 906
    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 907
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 909
    .end local v0    # "msg":Landroid/os/Message;
    :cond_2
    if-lez v1, :cond_0

    .line 910
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mContext:Landroid/content/Context;

    const-string v3, "com.sec.android.app.shealth.heartrate"

    const-string v4, "HR02"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 911
    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mHeartRate:I

    .line 912
    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mHeartRate:I

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->stopSensorAndUpdateUI(I)V

    .line 913
    sget-object v2, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->effectAudio:[I

    const/4 v3, 0x4

    aget v2, v2, v3

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->playSound(I)V

    goto :goto_0

    .line 916
    :cond_3
    if-ne v1, v7, :cond_5

    .line 917
    iput-boolean v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->isFingerDetected:Z

    .line 918
    iget-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->isRetryMeasuring:Z

    if-eqz v2, :cond_4

    .line 919
    sget-object v2, Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;->MEASURING:Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->startTimer(Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;)V

    .line 922
    :cond_4
    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mPrevError:I

    if-ne v2, v5, :cond_0

    .line 923
    iget-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->isRetryMeasuring:Z

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->changeUIReadyToMeasuring(Z)V

    goto :goto_0

    .line 925
    :cond_5
    if-gez v1, :cond_0

    if-lt v1, v6, :cond_0

    .line 926
    iput-boolean v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->isFingerDetected:Z

    .line 928
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 929
    .restart local v0    # "msg":Landroid/os/Message;
    iput v5, v0, Landroid/os/Message;->what:I

    .line 930
    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 931
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 485
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mSecondCenterPulseViewGreenIcon:Lcom/sec/android/app/shealth/heartrate/HRMGIFView;

    if-eqz v0, :cond_0

    .line 486
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mSecondCenterPulseViewGreenIcon:Lcom/sec/android/app/shealth/heartrate/HRMGIFView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->clear()V

    .line 487
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onDestroy()V

    .line 488
    return-void
.end method

.method public onDetach()V
    .locals 0

    .prologue
    .line 399
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onDetach()V

    .line 400
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 473
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->stopSensorAndUpdateUI(I)V

    .line 474
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mScaleAnimation:Landroid/view/animation/Animation;

    .line 475
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mCircleView:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleProgressView;

    if-eqz v0, :cond_0

    .line 476
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mCircleView:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleProgressView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleProgressView;->setVisibility(I)V

    .line 477
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mCircleView:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleProgressView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleProgressView;->stopAnimation()V

    .line 479
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->stopHRMPulseAnimation()V

    .line 480
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onPause()V

    .line 481
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 410
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "sth  onResume enter"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 412
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mAlertBuild_Error:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_0

    .line 413
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mAlertBuild_Error:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 414
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mAlertBuild_Error:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$5;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$5;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->setOnDismissListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;)V

    .line 421
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mAlertBuild_Error:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->dismiss()V

    .line 424
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mAlertBuild_HRM_Measure:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_3

    .line 425
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->deleteMeasurementDialog(IZ)V

    .line 448
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mAlertBuild_HRM_Result:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_2

    .line 449
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mAlertBuild_HRM_Result:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 450
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mAlertBuild_HRM_Result:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$6;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$6;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->setOnDismissListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;)V

    .line 458
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "sth  onResume  mAlertBuild_HRM_Result dismiss  "

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 459
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mAlertBuild_HRM_Result:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->dismiss()V

    .line 468
    :cond_2
    :goto_1
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onResume()V

    .line 469
    return-void

    .line 444
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mAlertBuild_HRM_Measure:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-nez v0, :cond_1

    .line 445
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->createMeasurementDialog()V

    goto :goto_0

    .line 462
    :cond_4
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "sth  onAttach  mAlertBuild_HRM_Result getDialog x  "

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 463
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mAlertBuild_HRM_Result:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    goto :goto_1
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 404
    const-string/jumbo v0, "mHeartRate"

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mHeartRate:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 405
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "sth  onSaveInstanceState mHeartRate = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mHeartRate:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 406
    return-void
.end method

.method public onTick(J)V
    .locals 0
    .param p1, "millisUntilFinished"    # J

    .prologue
    .line 949
    return-void
.end method

.method public onTimeout()V
    .locals 1

    .prologue
    .line 939
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mHeartRate:I

    if-lez v0, :cond_0

    .line 944
    :goto_0
    return-void

    .line 942
    :cond_0
    const/4 v0, -0x6

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->stopSensorAndUpdateUI(I)V

    goto :goto_0
.end method

.method public setFragmentHandler(Landroid/os/Handler;)V
    .locals 0
    .param p1, "_FragmentHandler"    # Landroid/os/Handler;

    .prologue
    .line 228
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mFragmentHandler:Landroid/os/Handler;

    .line 230
    return-void
.end method

.method public setMeasuringEndUI()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 773
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mSecondCenterIcon:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 774
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mSecondCenterIcon:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 776
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->stopHRMPulseAnimation()V

    .line 778
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mCurrentActivity:Landroid/app/Activity;

    if-eqz v0, :cond_4

    .line 779
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mCurrentActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 781
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->isMeasureCompleted:Z

    if-eqz v0, :cond_4

    .line 782
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mSecondCenterIcon:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 783
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mSecondCenterIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 785
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mSecondCenterPulseLayout:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_2

    .line 786
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mSecondCenterPulseLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 788
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mSecondCenterIconBig:Landroid/widget/ImageView;

    if-eqz v0, :cond_3

    .line 789
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mSecondCenterIconBig:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 793
    :cond_3
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mHeartRate:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->showHRdialog(I)V

    .line 797
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mSecondLayout:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_5

    .line 798
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mSecondLayout:Landroid/widget/FrameLayout;

    const v1, 0x7f020522

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    .line 802
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mCircleView:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleProgressView;

    if-eqz v0, :cond_6

    .line 803
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mCircleView:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleProgressView;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleProgressView;->setVisibility(I)V

    .line 804
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mCircleView:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleProgressView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleProgressView;->stopAnimation()V

    .line 808
    :cond_6
    return-void
.end method

.method public showHRdialog(I)V
    .locals 4
    .param p1, "hrValue"    # I

    .prologue
    const/high16 v3, 0x42c80000    # 100.0f

    .line 1012
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->bpm:I

    .line 1013
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->getUserAge()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->getMaxHR(I)I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v3

    const/high16 v2, 0x42480000    # 50.0f

    mul-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->minHRzone:I

    .line 1014
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->getUserAge()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->getMaxHR(I)I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v3

    const/high16 v2, 0x42aa0000    # 85.0f

    mul-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->maxHRzone:I

    .line 1016
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mCurrentActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x80

    invoke-virtual {v1, v2}, Landroid/view/Window;->clearFlags(I)V

    .line 1018
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mCurrentActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 1021
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelable(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1022
    const v1, 0x7f090a4c

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1023
    const v1, 0x7f090047

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1024
    const v1, 0x7f090057

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1025
    const v1, 0x7f0301a8

    new-instance v2, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$14;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$14;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(ILcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1106
    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$15;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$15;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnBackPressListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1115
    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$16;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$16;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelButtonListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnCancelButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1126
    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$17;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$17;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;I)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1135
    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$18;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$18;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnDismissListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1147
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1148
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mAlertBuild_HRM_Result:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-nez v1, :cond_0

    .line 1149
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mAlertBuild_HRM_Result:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 1151
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mAlertBuild_HRM_Result:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string/jumbo v3, "result"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 1154
    :cond_1
    return-void
.end method

.method public startSensorAndDelayUIForDetectedFinger(I)V
    .locals 5
    .param p1, "animation"    # I

    .prologue
    const/4 v4, 0x0

    .line 491
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->initMeasureStatus()V

    .line 493
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mDeviceConnector:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mCurrentActivity:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3, p0, v4}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->startMeasuring(Landroid/content/Context;Lcom/sec/android/app/shealth/heartrate/HeartrateSensorListener;Z)V

    .line 496
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mCurrentActivity:Landroid/app/Activity;

    new-instance v3, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$7;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$7;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)V

    invoke-virtual {v2, v3}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 503
    iput-boolean v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->isMeasureCompleted:Z

    .line 504
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$8;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$8;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;I)V

    .line 519
    .local v0, "task":Ljava/util/TimerTask;
    new-instance v1, Ljava/util/Timer;

    invoke-direct {v1}, Ljava/util/Timer;-><init>()V

    .line 520
    .local v1, "timer":Ljava/util/Timer;
    const-wide/16 v2, 0x96

    invoke-virtual {v1, v0, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 523
    return-void
.end method

.method public stopSensorAndUpdateUI(I)V
    .locals 4
    .param p1, "heartRate"    # I

    .prologue
    .line 1347
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "sth  stopSensorAndUpdateUI rate: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " RemainFailCount: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mMeasureFailCount:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1349
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mMeasureFailCount:I

    if-lez v1, :cond_1

    .line 1350
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mMeasureFailCount:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mMeasureFailCount:I

    .line 1392
    :cond_0
    :goto_0
    return-void

    .line 1354
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->stopSensorAndAnimationInitialize()V

    .line 1356
    if-nez p1, :cond_3

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->isMeasureCompleted:Z

    if-nez v1, :cond_3

    .line 1357
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mFirstStatus:Landroid/widget/TextView;

    if-eqz v1, :cond_2

    .line 1358
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mFirstStatus:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1360
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mFirstMessage:Landroid/widget/TextView;

    if-eqz v1, :cond_3

    .line 1361
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mFirstMessage:Landroid/widget/TextView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1366
    :cond_3
    if-ltz p1, :cond_4

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->isMeasureCompleted:Z

    if-nez v1, :cond_4

    .line 1367
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1370
    if-lez p1, :cond_0

    .line 1371
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->isMeasureCompleted:Z

    .line 1372
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mCurrentActivity:Landroid/app/Activity;

    new-instance v2, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$24;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$24;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 1385
    :cond_4
    if-gez p1, :cond_0

    .line 1386
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 1387
    .local v0, "msg":Landroid/os/Message;
    const/4 v1, 0x2

    iput v1, v0, Landroid/os/Message;->what:I

    .line 1388
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 1389
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method
