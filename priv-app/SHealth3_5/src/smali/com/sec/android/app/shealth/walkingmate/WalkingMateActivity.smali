.class public Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;
.source "WalkingMateActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerProvider;
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/ISaveChosenItemListenerGetter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$7;,
        Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$DevicePopUpDialogController;,
        Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$HelpDialogController;,
        Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$WearableStatusObserver;
    }
.end annotation


# static fields
.field private static ACCESSARY_ADD_REQ_CODE:I = 0x0

.field private static final LOG_REQUEST_CODE:I = 0x458

.field private static MAIN_DB_PEDOMETER_VIEW_STEP_COUNT_MOBILE:I = 0x0

.field private static MAIN_DB_PEDOMETER_VIEW_STEP_COUNT_WEARABLE:I = 0x0

.field private static final PER:Ljava/lang/String; = "PERFORMANCE"

.field private static REQUEST_CODE_CALENDAR:I = 0x0

.field private static REQUEST_CODE_SET_GOAL:I = 0x0

.field private static final SETTING_WEARABLE_ID:Ljava/lang/String; = "content://settings/system/connected_wearable_id"

.field public static final START_SAMUNG_SIGN_IN_ACTIVITY:I = 0x65

.field private static final SUMMARYVIEW_DEFAULT_ACTIONBAR_BUTTON_COUNT:I = 0x2

.field private static final TAG:Ljava/lang/String; = "WalkingMateActivity"

.field private static final VIEW_STEP_COUNT:I = 0x459

.field public static isPrviousHomeScreen:Z

.field public static isSummaryFragment:Z

.field public static isWalkingMateEnter:Z

.field private static mDevicePopupView:Landroid/view/View;

.field private static mIsLaunchedFromWidget:Z

.field public static selectedDate:J


# instance fields
.field private DialogTag:Ljava/lang/String;

.field private actionBarShareviaButton:Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

.field private mDeviceItemList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/walkingmate/utils/Utils$DeviceItem;",
            ">;"
        }
    .end annotation
.end field

.field private mDevicePopUpDialogController:Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$DevicePopUpDialogController;

.field private mGraphFrag:Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;

.field private mHelpDialogController:Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$HelpDialogController;

.field private mIntent:Landroid/content/Intent;

.field public mIsSensorHubInstalled:Z

.field private mPauseDialogController:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$PauseDialogController;

.field private mResetDialogController:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$ResetDialogController;

.field private mSamsungAccountDialogController:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$SamsungAccountDialogController;

.field private mSteps:I

.field mTimeZoneRecv:Landroid/content/BroadcastReceiver;

.field mTimeZoneRecvIF:Landroid/content/IntentFilter;

.field private mWalkingMateSummaryFragment:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

.field private statusObserver:Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$WearableStatusObserver;

.field private uiThreadHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 75
    const/16 v0, 0xc8

    sput v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->REQUEST_CODE_CALENDAR:I

    .line 76
    const/16 v0, 0xc9

    sput v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->REQUEST_CODE_SET_GOAL:I

    .line 77
    const/16 v0, 0x7b

    sput v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->ACCESSARY_ADD_REQ_CODE:I

    .line 82
    sput-boolean v2, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->isSummaryFragment:Z

    .line 83
    sput-boolean v2, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->isWalkingMateEnter:Z

    .line 84
    sput-boolean v2, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->isPrviousHomeScreen:Z

    .line 85
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->selectedDate:J

    .line 87
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mDevicePopupView:Landroid/view/View;

    .line 91
    sput v2, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->MAIN_DB_PEDOMETER_VIEW_STEP_COUNT_MOBILE:I

    .line 92
    const/4 v0, 0x1

    sput v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->MAIN_DB_PEDOMETER_VIEW_STEP_COUNT_WEARABLE:I

    .line 103
    sput-boolean v2, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mIsLaunchedFromWidget:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 69
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;-><init>()V

    .line 86
    iput-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mIntent:Landroid/content/Intent;

    .line 89
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->uiThreadHandler:Landroid/os/Handler;

    .line 105
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mIsSensorHubInstalled:Z

    .line 106
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.TIMEZONE_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mTimeZoneRecvIF:Landroid/content/IntentFilter;

    .line 108
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$1;-><init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mTimeZoneRecv:Landroid/content/BroadcastReceiver;

    .line 561
    iput-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mDeviceItemList:Ljava/util/ArrayList;

    .line 562
    const-string v0, "DialogViewStepCount"

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->DialogTag:Ljava/lang/String;

    .line 942
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;)Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mGraphFrag:Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->uiThreadHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;)Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mWalkingMateSummaryFragment:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    .prologue
    .line 69
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mWalkingMateSummaryFragment:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    return-object p1
.end method

.method static synthetic access$300()I
    .locals 1

    .prologue
    .line 69
    sget v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->MAIN_DB_PEDOMETER_VIEW_STEP_COUNT_MOBILE:I

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mDeviceItemList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$500()I
    .locals 1

    .prologue
    .line 69
    sget v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->MAIN_DB_PEDOMETER_VIEW_STEP_COUNT_WEARABLE:I

    return v0
.end method

.method static synthetic access$600()I
    .locals 1

    .prologue
    .line 69
    sget v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->ACCESSARY_ADD_REQ_CODE:I

    return v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;

    .prologue
    .line 69
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->prepareShareView()V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;)Landroid/content/Intent;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mIntent:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic access$900()Landroid/view/View;
    .locals 1

    .prologue
    .line 69
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mDevicePopupView:Landroid/view/View;

    return-object v0
.end method

.method private buildDialogBooleanArrayState(II)[Z
    .locals 2
    .param p1, "filterDialogState"    # I
    .param p2, "itemLen"    # I

    .prologue
    .line 579
    new-array v0, p2, [Z

    .line 580
    .local v0, "filterDialogBooleanArrayState":[Z
    const/4 v1, 0x1

    aput-boolean v1, v0, p1

    .line 581
    return-object v0
.end method

.method public static changeViewStepCountSetting(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 685
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.shealth.pedometer.viewstepcount"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 686
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 687
    return-void
.end method

.method private prepareListDialogItem()Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 565
    const-string v3, "WalkingMateActivity"

    const-string/jumbo v4, "prepareListDialogItem"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 566
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->getLoadUserDeviceName()Ljava/util/ArrayList;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mDeviceItemList:Ljava/util/ArrayList;

    .line 567
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 568
    .local v0, "deviceNames":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-boolean v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mIsSensorHubInstalled:Z

    if-eqz v3, :cond_0

    .line 569
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090bb8

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 570
    :cond_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mDeviceItemList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 571
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mDeviceItemList:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/walkingmate/utils/Utils$DeviceItem;

    .line 572
    .local v2, "item":Lcom/sec/android/app/shealth/walkingmate/utils/Utils$DeviceItem;
    iget-object v3, v2, Lcom/sec/android/app/shealth/walkingmate/utils/Utils$DeviceItem;->deviceName:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 570
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 575
    .end local v2    # "item":Lcom/sec/android/app/shealth/walkingmate/utils/Utils$DeviceItem;
    :cond_1
    return-object v0
.end method


# virtual methods
.method public addShareViaButton()V
    .locals 4

    .prologue
    .line 305
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getTotalActionBarButtonCount()I

    move-result v0

    const/4 v1, 0x3

    if-ge v0, v1, :cond_0

    .line 306
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->actionBarShareviaButton:Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 308
    :cond_0
    return-void
.end method

.method protected customizeActionBar()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 691
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->customizeActionBar()V

    .line 693
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->removeAllActionBarButtons()V

    .line 694
    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    const v3, 0x7f0208dd

    new-instance v4, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$3;

    invoke-direct {v4, p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$3;-><init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;)V

    invoke-direct {v1, v3, v4}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;-><init>(ILandroid/view/View$OnClickListener;)V

    .line 702
    .local v1, "actionBarLogButtonBuilder":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;
    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    const v3, 0x7f0208d2

    new-instance v4, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$4;

    invoke-direct {v4, p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$4;-><init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;)V

    invoke-direct {v0, v3, v4}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;-><init>(ILandroid/view/View$OnClickListener;)V

    .line 721
    .local v0, "actionBarConnectAccessoryButtonBuilder":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;
    new-instance v2, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    const v3, 0x7f0208e3

    new-instance v4, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$5;

    invoke-direct {v4, p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$5;-><init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;)V

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;-><init>(ILandroid/view/View$OnClickListener;)V

    .line 732
    .local v2, "actionBarShareviaButtonBuilder":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    const v4, 0x7f090020

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 734
    const v3, 0x7f09005a

    invoke-virtual {v1, v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->setHoverTextResID(I)Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    .line 735
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    new-array v4, v6, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->create()Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 736
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    invoke-virtual {v3, v6}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    .line 738
    const v3, 0x7f0907b4

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->setHoverTextResID(I)Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    .line 739
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    new-array v4, v6, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->create()Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 740
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    invoke-virtual {v3, v6}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    .line 742
    const v3, 0x7f090033

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->setHoverTextResID(I)Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    .line 743
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->create()Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->actionBarShareviaButton:Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    .line 744
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    invoke-virtual {v3, v6}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    .line 745
    return-void
.end method

.method public disableInactiveMonitor()V
    .locals 1

    .prologue
    .line 644
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->unsetInactiveMonitor()V

    .line 645
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->setNotificationONOFF(Z)V

    .line 646
    return-void
.end method

.method public enableInactiveMonitor()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 650
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->setInactiveMonitor(I)V

    .line 651
    invoke-static {v1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->setNotificationONOFF(Z)V

    .line 652
    return-void
.end method

.method public getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 905
    const-string v0, "PAUSE"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 906
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mPauseDialogController:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$PauseDialogController;

    .line 916
    :goto_0
    return-object v0

    .line 907
    :cond_0
    const-string v0, "RESET"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 908
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mResetDialogController:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$ResetDialogController;

    goto :goto_0

    .line 909
    :cond_1
    const-string v0, "SAMSUNGACCOUNT"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 910
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mSamsungAccountDialogController:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$SamsungAccountDialogController;

    goto :goto_0

    .line 911
    :cond_2
    const-string v0, "DEVICEPOPUP"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 912
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mDevicePopUpDialogController:Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$DevicePopUpDialogController;

    goto :goto_0

    .line 913
    :cond_3
    const-string v0, "HELP"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 914
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mHelpDialogController:Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$HelpDialogController;

    goto :goto_0

    .line 916
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getGraphFragment()Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;
    .locals 1

    .prologue
    .line 758
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mGraphFrag:Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;

    return-object v0
.end method

.method protected getHelpItem()Ljava/lang/String;
    .locals 1

    .prologue
    .line 962
    const-string v0, "com.sec.shealth.help.action.PEDOMETER"

    return-object v0
.end method

.method public getIsLaunchedFromWidget()Z
    .locals 3

    .prologue
    .line 269
    const-string v0, "WalkingMateActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getIsLaunchedFromWidget:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mIsLaunchedFromWidget:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 270
    sget-boolean v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mIsLaunchedFromWidget:Z

    return v0
.end method

.method public getOnSaveChosenItemListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/OnSaveChosenItemListener;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 586
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$2;-><init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;)V

    return-object v0
.end method

.method public getSummaryFragment()Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;
    .locals 1

    .prologue
    .line 754
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mWalkingMateSummaryFragment:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    return-object v0
.end method

.method public getUIFWActionBar()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;
    .locals 1

    .prologue
    .line 900
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    return-object v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 11
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/16 v10, 0x458

    .line 811
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 813
    sget v5, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->REQUEST_CODE_CALENDAR:I

    if-ne v5, p1, :cond_0

    if-eqz p3, :cond_0

    .line 814
    const-string v5, "calendar_result"

    const-wide/16 v6, -0x1

    invoke-virtual {p3, v5, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    .line 815
    .local v4, "timeInMili":Ljava/lang/Long;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-static {v5, v6}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 816
    .local v1, "currnetDayStartMili":Ljava/lang/Long;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    sub-long/2addr v5, v7

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 817
    .local v2, "diff":Ljava/lang/Long;
    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mWalkingMateSummaryFragment:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    add-long/2addr v6, v8

    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getDateInstance(J)Ljava/sql/Date;

    move-result-object v6

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->setSelectedDate(Ljava/util/Date;Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;)V

    .line 818
    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mWalkingMateSummaryFragment:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    add-long/2addr v6, v8

    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getDateInstance(J)Ljava/sql/Date;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->setSelectedText(Ljava/util/Date;)V

    .line 820
    .end local v1    # "currnetDayStartMili":Ljava/lang/Long;
    .end local v2    # "diff":Ljava/lang/Long;
    .end local v4    # "timeInMili":Ljava/lang/Long;
    :cond_0
    sget v5, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->REQUEST_CODE_SET_GOAL:I

    if-ne v5, p1, :cond_5

    .line 821
    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mWalkingMateSummaryFragment:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->refreshgoal()V

    .line 825
    :cond_1
    :goto_0
    if-ne v10, p1, :cond_2

    .line 826
    sget-boolean v5, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->isSummaryFragment:Z

    if-nez v5, :cond_2

    .line 827
    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mGraphFrag:Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;

    if-eqz v5, :cond_2

    .line 828
    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mGraphFrag:Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->updateGraphFragment()V

    .line 831
    :cond_2
    if-ne v10, p1, :cond_3

    const/4 v5, -0x1

    if-ne p2, v5, :cond_3

    .line 832
    iput-object p3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mIntent:Landroid/content/Intent;

    .line 833
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    .line 834
    .local v3, "fragmentManager":Landroid/support/v4/app/FragmentManager;
    const v5, 0x7f080091

    invoke-virtual {v3, v5}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;

    .line 835
    .local v0, "currentFragment":Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;
    instance-of v5, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    if-eqz v5, :cond_6

    .line 836
    if-eqz p3, :cond_3

    .line 837
    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mWalkingMateSummaryFragment:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    invoke-virtual {v5, p3}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->setDateFromLog(Landroid/content/Intent;)V

    .line 854
    .end local v0    # "currentFragment":Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;
    .end local v3    # "fragmentManager":Landroid/support/v4/app/FragmentManager;
    :cond_3
    :goto_1
    const/16 v5, 0x459

    if-ne v5, p1, :cond_4

    .line 855
    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mWalkingMateSummaryFragment:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->refreshgoal()V

    .line 856
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->invalidateOptionsMenu()V

    .line 859
    :cond_4
    if-eqz p3, :cond_7

    .line 860
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 863
    :goto_2
    return-void

    .line 822
    :cond_5
    const/16 v5, 0x65

    if-ne v5, p1, :cond_1

    .line 823
    new-instance v5, Landroid/content/Intent;

    const-class v6, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;

    invoke-direct {v5, p0, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 840
    .restart local v0    # "currentFragment":Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;
    .restart local v3    # "fragmentManager":Landroid/support/v4/app/FragmentManager;
    :cond_6
    new-instance v5, Landroid/os/Handler;

    invoke-direct {v5}, Landroid/os/Handler;-><init>()V

    new-instance v6, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$6;

    invoke-direct {v6, p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$6;-><init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;)V

    invoke-virtual {v5, v6}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_1

    .line 862
    .end local v0    # "currentFragment":Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;
    .end local v3    # "fragmentManager":Landroid/support/v4/app/FragmentManager;
    :cond_7
    const-string v5, "WalkingMateActivity"

    const-string v6, "data is null"

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    .line 869
    iget-boolean v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mIsSensorHubInstalled:Z

    if-nez v2, :cond_0

    .line 870
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->finish()V

    .line 890
    :goto_0
    return-void

    .line 872
    :cond_0
    sget-boolean v2, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->isPrviousHomeScreen:Z

    if-eqz v2, :cond_1

    .line 873
    const/4 v2, 0x0

    sput-boolean v2, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->isPrviousHomeScreen:Z

    .line 874
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onBackPressed()V

    goto :goto_0

    .line 878
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    .line 879
    .local v1, "fragmentManager":Landroid/support/v4/app/FragmentManager;
    const v2, 0x7f080091

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;

    .line 880
    .local v0, "currentFragment":Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;
    instance-of v2, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    if-eqz v2, :cond_2

    .line 883
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->finish()V

    goto :goto_0

    .line 885
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mWalkingMateSummaryFragment:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->showFragment(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;)V

    .line 886
    const/4 v2, 0x1

    sput-boolean v2, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->isSummaryFragment:Z

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 805
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 806
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->invalidateOptionsMenu()V

    .line 807
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 15
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 127
    invoke-super/range {p0 .. p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 128
    const-string v11, "SHEALT_PER"

    const-string v12, "WalkingMateActivity"

    invoke-static {v11, v12}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->getIntent()Landroid/content/Intent;

    move-result-object v11

    const-string v12, "isFromWidget"

    invoke-virtual {v11, v12}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    if-eqz v11, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->getIntent()Landroid/content/Intent;

    move-result-object v11

    const-string v12, "isFromWidget"

    invoke-virtual {v11, v12}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    const-string v12, "fromAbstractWidget"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 131
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v11

    const-string v12, "com.sec.android.app.shealth.walkingmate"

    const-string v13, "W030"

    invoke-static {v11, v12, v13}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 135
    .local v8, "pStart":J
    const-string v11, "PERFORMANCE"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Start: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    const-string v11, "PERFORMANCE"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "check wearable: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v13

    sub-long/2addr v13, v8

    invoke-virtual {v12, v13, v14}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 139
    new-instance v11, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$PauseDialogController;

    invoke-direct {v11}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$PauseDialogController;-><init>()V

    iput-object v11, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mPauseDialogController:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$PauseDialogController;

    .line 140
    new-instance v11, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$ResetDialogController;

    invoke-direct {v11}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$ResetDialogController;-><init>()V

    iput-object v11, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mResetDialogController:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$ResetDialogController;

    .line 141
    new-instance v11, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$SamsungAccountDialogController;

    invoke-direct {v11}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$SamsungAccountDialogController;-><init>()V

    iput-object v11, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mSamsungAccountDialogController:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$SamsungAccountDialogController;

    .line 142
    new-instance v11, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$HelpDialogController;

    invoke-direct {v11}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$HelpDialogController;-><init>()V

    iput-object v11, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mHelpDialogController:Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$HelpDialogController;

    .line 143
    new-instance v11, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$DevicePopUpDialogController;

    invoke-direct {v11}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$DevicePopUpDialogController;-><init>()V

    iput-object v11, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mDevicePopUpDialogController:Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$DevicePopUpDialogController;

    .line 145
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v10

    .line 146
    .local v10, "packageManager":Landroid/content/pm/PackageManager;
    const-string v11, "com.sec.feature.sensorhub"

    invoke-virtual {v10, v11}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v11

    const/4 v12, 0x1

    if-eq v11, v12, :cond_1

    const-string v11, "com.sec.feature.scontext_lite"

    invoke-virtual {v10, v11}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v11

    const/4 v12, 0x1

    if-ne v11, v12, :cond_2

    .line 148
    :cond_1
    const/4 v11, 0x1

    iput-boolean v11, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mIsSensorHubInstalled:Z

    .line 151
    :cond_2
    const-string v11, "PERFORMANCE"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "create graph: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v13

    sub-long/2addr v13, v8

    invoke-virtual {v12, v13, v14}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 152
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 153
    const-wide/16 v1, 0x0

    .line 154
    .local v1, "date":J
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 156
    .local v0, "args":Landroid/os/Bundle;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v11

    const-class v12, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    invoke-virtual {v12}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v5

    .line 158
    .local v5, "fgSummary":Landroid/support/v4/app/Fragment;
    if-eqz v5, :cond_3

    .line 160
    check-cast v5, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    .end local v5    # "fgSummary":Landroid/support/v4/app/Fragment;
    iput-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mWalkingMateSummaryFragment:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    .line 164
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v11

    const-class v12, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;

    invoke-virtual {v12}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v4

    .line 166
    .local v4, "fgGraph":Landroid/support/v4/app/Fragment;
    if-eqz v4, :cond_9

    .line 168
    check-cast v4, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;

    .end local v4    # "fgGraph":Landroid/support/v4/app/Fragment;
    iput-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mGraphFrag:Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;

    .line 177
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->getIntent()Landroid/content/Intent;

    move-result-object v11

    const-string v12, "launchWidget"

    invoke-virtual {v11, v12}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_a

    .line 178
    const/4 v11, 0x1

    invoke-virtual {p0, v11}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->setIsLaunchedFromWidget(Z)V

    .line 183
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->getIntent()Landroid/content/Intent;

    move-result-object v11

    const-string v12, "SelectedDate"

    invoke-virtual {v11, v12}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_b

    .line 184
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->getIntent()Landroid/content/Intent;

    move-result-object v11

    const-string v12, "SelectedDate"

    const-wide/16 v13, 0x0

    invoke-virtual {v11, v12, v13, v14}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v1

    .line 185
    const-string v11, "WalkingMateActivity"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "date = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    const-string v11, "Date"

    invoke-virtual {v0, v11, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 187
    iget-object v11, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mWalkingMateSummaryFragment:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    if-nez v11, :cond_4

    .line 189
    new-instance v11, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    invoke-direct {v11, v1, v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;-><init>(J)V

    iput-object v11, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mWalkingMateSummaryFragment:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    .line 198
    :cond_4
    :goto_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->getIntent()Landroid/content/Intent;

    move-result-object v11

    const-string v12, "Mode"

    invoke-virtual {v11, v12}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 199
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->getIntent()Landroid/content/Intent;

    move-result-object v11

    const-string v12, "CurrentSteps"

    invoke-virtual {v11, v12}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 200
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->getIntent()Landroid/content/Intent;

    move-result-object v11

    const-string v12, "CurrentSteps"

    const/4 v13, 0x0

    invoke-virtual {v11, v12, v13}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v11

    iput v11, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mSteps:I

    .line 202
    :cond_5
    const-string v11, "Steps"

    iget v12, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mSteps:I

    invoke-virtual {v0, v11, v12}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 203
    iget-object v11, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mWalkingMateSummaryFragment:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    invoke-virtual {v11, v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->setArguments(Landroid/os/Bundle;)V

    .line 206
    :cond_6
    const/4 v11, 0x1

    sput-boolean v11, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->isWalkingMateEnter:Z

    .line 208
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->getIntent()Landroid/content/Intent;

    move-result-object v11

    const-string/jumbo v12, "show_graph_fragment"

    const/4 v13, 0x0

    invoke-virtual {v11, v12, v13}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    .line 213
    .local v6, "isShowGraphFrag":Z
    iget-boolean v11, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mIsSensorHubInstalled:Z

    if-nez v11, :cond_7

    invoke-static {p0}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->isMobilePedometerDisabled(Landroid/content/Context;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 214
    const/4 v6, 0x1

    .line 217
    :cond_7
    sput-boolean v6, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->isPrviousHomeScreen:Z

    .line 219
    if-nez p1, :cond_d

    .line 221
    if-eqz v6, :cond_c

    .line 223
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v11

    invoke-virtual {v11}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v11

    const v12, 0x7f080091

    iget-object v13, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mGraphFrag:Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;

    const-class v14, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;

    invoke-virtual {v14}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v11, v12, v13, v14}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v11

    invoke-virtual {v11}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 227
    const/4 v11, 0x0

    sput-boolean v11, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->isSummaryFragment:Z

    .line 245
    :cond_8
    :goto_3
    new-instance v7, Landroid/content/IntentFilter;

    const-string v11, "android.bluetooth.device.action.ACL_CONNECTED"

    invoke-direct {v7, v11}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 246
    .local v7, "lFilter":Landroid/content/IntentFilter;
    const-string v11, "android.bluetooth.device.action.ACL_DISCONNECTED"

    invoke-virtual {v7, v11}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 247
    new-instance v11, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$WearableStatusObserver;

    const/4 v12, 0x0

    invoke-direct {v11, p0, v12}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$WearableStatusObserver;-><init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;Landroid/os/Handler;)V

    iput-object v11, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->statusObserver:Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$WearableStatusObserver;

    .line 248
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v11

    const-string v12, "content://settings/system/connected_wearable_id"

    invoke-static {v12}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v12

    const/4 v13, 0x1

    iget-object v14, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->statusObserver:Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$WearableStatusObserver;

    invoke-virtual {v11, v12, v13, v14}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 249
    iget-object v11, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mTimeZoneRecv:Landroid/content/BroadcastReceiver;

    iget-object v12, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mTimeZoneRecvIF:Landroid/content/IntentFilter;

    invoke-virtual {p0, v11, v12}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 250
    const-string v11, "PERFORMANCE"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v13, "register events: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v13

    sub-long/2addr v13, v8

    invoke-virtual {v12, v13, v14}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 251
    const-wide/16 v11, 0x0

    sput-wide v11, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->selectedTime:J

    .line 253
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->invalidateOptionsMenu()V

    .line 254
    const-string v11, "PERFORMANCE"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "END: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v13

    invoke-virtual {v12, v13, v14}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 255
    return-void

    .line 172
    .end local v6    # "isShowGraphFrag":Z
    .end local v7    # "lFilter":Landroid/content/IntentFilter;
    .restart local v4    # "fgGraph":Landroid/support/v4/app/Fragment;
    :cond_9
    new-instance v11, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;

    invoke-direct {v11}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;-><init>()V

    iput-object v11, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mGraphFrag:Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;

    .line 173
    iget-object v11, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mGraphFrag:Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;

    const/4 v12, 0x1

    invoke-virtual {v11, v12}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->setHasOptionsMenu(Z)V

    goto/16 :goto_0

    .line 180
    .end local v4    # "fgGraph":Landroid/support/v4/app/Fragment;
    :cond_a
    const/4 v11, 0x0

    invoke-virtual {p0, v11}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->setIsLaunchedFromWidget(Z)V

    goto/16 :goto_1

    .line 194
    :cond_b
    iget-object v11, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mWalkingMateSummaryFragment:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    if-nez v11, :cond_4

    .line 195
    new-instance v11, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    invoke-direct {v11}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;-><init>()V

    iput-object v11, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mWalkingMateSummaryFragment:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    goto/16 :goto_2

    .line 230
    .restart local v6    # "isShowGraphFrag":Z
    :cond_c
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v11

    invoke-virtual {v11}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v11

    const v12, 0x7f080091

    iget-object v13, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mWalkingMateSummaryFragment:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    const-class v14, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    invoke-virtual {v14}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v11, v12, v13, v14}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/support/v4/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v11

    invoke-virtual {v11}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 235
    const/4 v11, 0x1

    sput-boolean v11, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->isSummaryFragment:Z

    goto/16 :goto_3

    .line 239
    :cond_d
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v11

    const-class v12, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;

    invoke-virtual {v12}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v3

    .line 240
    .local v3, "fgGrahFragment":Landroid/support/v4/app/Fragment;
    if-eqz v3, :cond_8

    .line 241
    check-cast v3, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;

    .end local v3    # "fgGrahFragment":Landroid/support/v4/app/Fragment;
    iput-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mGraphFrag:Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;

    goto/16 :goto_3
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 4
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 275
    const-string v2, " "

    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;)V

    .line 276
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->isDrawerMenuShown()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 300
    :goto_0
    return v0

    .line 279
    :cond_0
    sget-boolean v2, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->isSummaryFragment:Z

    if-eqz v2, :cond_4

    .line 280
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v2

    const v3, 0x7f10002f

    invoke-virtual {v2, v3, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 282
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 283
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->removeShareViaButton()V

    .line 285
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-static {p0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewConfiguration;->hasPermanentMenuKey()Z

    move-result v2

    if-nez v2, :cond_3

    .line 286
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    :cond_2
    :goto_1
    move v0, v1

    .line 300
    goto :goto_0

    .line 288
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    goto :goto_1

    .line 291
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->actionBarShareviaButton:Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    if-eqz v2, :cond_2

    .line 293
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->isWalkingDataExist()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 294
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->addShareViaButton()V

    .line 296
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    goto :goto_1
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 359
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->statusObserver:Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$WearableStatusObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 360
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mTimeZoneRecv:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 361
    iput-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mGraphFrag:Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;

    .line 362
    iput-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mWalkingMateSummaryFragment:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    .line 363
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onDestroy()V

    .line 364
    return-void
.end method

.method protected onLogSelected()V
    .locals 3

    .prologue
    .line 749
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 750
    .local v0, "intent":Landroid/content/Intent;
    const/16 v1, 0x458

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 751
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 541
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 542
    if-nez p1, :cond_0

    .line 543
    const-string v2, "WalkingMateActivity"

    const-string/jumbo v3, "onNewIntent intent is null"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 559
    :goto_0
    return-void

    .line 545
    :cond_0
    const-string v2, "SelectedDate"

    const-wide/16 v3, 0x0

    invoke-virtual {p1, v2, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 546
    .local v0, "time":J
    const-string v2, "WalkingMateActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "onNewIntent SelectedDate : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 547
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mWalkingMateSummaryFragment:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    if-eqz v2, :cond_2

    .line 548
    sget-boolean v2, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->isSummaryFragment:Z

    if-nez v2, :cond_1

    .line 549
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mWalkingMateSummaryFragment:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->showFragment(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;)V

    .line 551
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mWalkingMateSummaryFragment:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    invoke-virtual {v2, v0, v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->onDateChangedFromNotification(J)V

    goto :goto_0

    .line 553
    :cond_2
    new-instance v2, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    invoke-direct {v2, v0, v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;-><init>(J)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mWalkingMateSummaryFragment:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    .line 554
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mWalkingMateSummaryFragment:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->showFragment(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;)V

    .line 555
    const/4 v2, 0x1

    sput-boolean v2, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->isSummaryFragment:Z

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 6
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v2, 0x1

    .line 492
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    .line 536
    const/4 v2, 0x0

    :goto_0
    return v2

    .line 494
    :sswitch_0
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;

    invoke-direct {v0, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 495
    .local v0, "intent":Landroid/content/Intent;
    sget v3, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->REQUEST_CODE_SET_GOAL:I

    invoke-virtual {p0, v0, v3}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 504
    .end local v0    # "intent":Landroid/content/Intent;
    :sswitch_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->showDevicesChooseDialog()V

    goto :goto_0

    .line 508
    :sswitch_2
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 509
    .local v1, "intent_info":Landroid/content/Intent;
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 513
    .end local v1    # "intent_info":Landroid/content/Intent;
    :sswitch_3
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mWalkingMateSummaryFragment:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->startPedometer()V

    goto :goto_0

    .line 517
    :sswitch_4
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mWalkingMateSummaryFragment:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->pausePedometer()V

    goto :goto_0

    .line 521
    :sswitch_5
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "com.sec.android.app.shealth.walkingmate"

    const-string v5, "W010"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 522
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->prepareShareView()V

    goto :goto_0

    .line 526
    :sswitch_6
    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.shealth.action.LAUNCH_SETTINGS"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 530
    :sswitch_7
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "com.sec.android.app.shealth.walkingmate"

    const-string v4, "W007"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 531
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v2

    goto :goto_0

    .line 492
    :sswitch_data_0
    .sparse-switch
        0x7f080022 -> :sswitch_6
        0x7f08036a -> :sswitch_2
        0x7f080c8a -> :sswitch_7
        0x7f080c98 -> :sswitch_5
        0x7f080cb1 -> :sswitch_0
        0x7f080cb2 -> :sswitch_1
        0x7f080cb3 -> :sswitch_3
        0x7f080cb4 -> :sswitch_4
    .end sparse-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 16
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 368
    if-nez p1, :cond_0

    .line 369
    const/4 v1, 0x0

    .line 487
    :goto_0
    return v1

    .line 372
    :cond_0
    sget-boolean v1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->isSummaryFragment:Z

    if-eqz v1, :cond_12

    .line 373
    const v1, 0x7f080cb2

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 374
    const v1, 0x7f080cb2

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 377
    :cond_1
    const/4 v12, 0x0

    .line 378
    .local v12, "isWearableDataOnBackupAndRestore":Z
    const/4 v7, 0x0

    .line 380
    .local v7, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$UserDevice;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-string v6, "device_type DESC "

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 382
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-eqz v1, :cond_4

    .line 383
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 384
    :goto_1
    invoke-interface {v7}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v1

    if-nez v1, :cond_4

    .line 385
    const-string v1, "device_type"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 386
    .local v9, "deviceType":I
    const/16 v1, 0x2719

    if-eq v1, v9, :cond_2

    .line 387
    const/4 v12, 0x1

    .line 389
    :cond_2
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 393
    .end local v9    # "deviceType":I
    :catchall_0
    move-exception v1

    if-eqz v7, :cond_3

    .line 394
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v1

    .line 393
    :cond_4
    if-eqz v7, :cond_5

    .line 394
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 398
    :cond_5
    invoke-static/range {p0 .. p0}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->isGearManagerAvailable(Landroid/content/Context;)Z

    move-result v11

    .line 399
    .local v11, "isGearProviderAvaialble":Z
    const v1, 0x7f080cb2

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v13

    .line 403
    .local v13, "item":Landroid/view/MenuItem;
    const v1, 0x7f080cb1

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v14

    .line 405
    .local v14, "itemSetGoal":Landroid/view/MenuItem;
    if-eqz v13, :cond_7

    .line 406
    if-nez v11, :cond_6

    if-eqz v12, :cond_13

    :cond_6
    const/4 v1, 0x1

    :goto_2
    invoke-interface {v13, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 407
    if-nez v11, :cond_7

    .line 413
    if-eqz v14, :cond_7

    .line 414
    const/4 v1, 0x1

    invoke-interface {v14, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 422
    :cond_7
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->getLoadUserDeviceName()Ljava/util/ArrayList;

    move-result-object v8

    .line 424
    .local v8, "deviceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/walkingmate/utils/Utils$DeviceItem;>;"
    if-eqz v13, :cond_b

    if-eqz v8, :cond_b

    .line 426
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x2

    if-ge v1, v2, :cond_8

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mIsSensorHubInstalled:Z

    if-eqz v1, :cond_9

    :cond_8
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_a

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mIsSensorHubInstalled:Z

    if-eqz v1, :cond_a

    .line 428
    :cond_9
    const/4 v1, 0x0

    invoke-interface {v13, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 432
    :cond_a
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mIsSensorHubInstalled:Z

    if-eqz v1, :cond_b

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v1

    const/16 v2, 0x2719

    if-eq v1, v2, :cond_b

    .line 434
    const/4 v1, 0x1

    invoke-interface {v13, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 438
    :cond_b
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;->getInstance()Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;

    move-result-object v10

    .line 439
    .local v10, "featureManager":Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;
    if-eqz v10, :cond_c

    .line 440
    const/4 v1, 0x6

    invoke-virtual {v10, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;->checkFeature(I)Z

    move-result v1

    if-nez v1, :cond_c

    .line 441
    const v1, 0x7f08036a

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v13

    if-eqz v13, :cond_c

    .line 442
    const/4 v1, 0x0

    invoke-interface {v13, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 446
    :cond_c
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->isStartWalking()Z

    move-result v15

    .line 447
    .local v15, "showPauseMenu":Z
    const v1, 0x7f080cb3

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v13

    if-eqz v13, :cond_d

    .line 448
    if-nez v15, :cond_14

    const/4 v1, 0x1

    :goto_3
    invoke-interface {v13, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 449
    :cond_d
    const v1, 0x7f080cb4

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v13

    if-eqz v13, :cond_e

    .line 450
    invoke-interface {v13, v15}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 452
    :cond_e
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mIsSensorHubInstalled:Z

    if-eqz v1, :cond_f

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v1

    const/16 v2, 0x2719

    if-eq v1, v2, :cond_11

    .line 453
    :cond_f
    const v1, 0x7f080cb3

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v13

    if-eqz v13, :cond_10

    .line 454
    const/4 v1, 0x0

    invoke-interface {v13, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 455
    :cond_10
    const v1, 0x7f080cb4

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v13

    if-eqz v13, :cond_11

    .line 456
    const/4 v1, 0x0

    invoke-interface {v13, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 459
    :cond_11
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 487
    .end local v7    # "cursor":Landroid/database/Cursor;
    .end local v8    # "deviceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/walkingmate/utils/Utils$DeviceItem;>;"
    .end local v10    # "featureManager":Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;
    .end local v11    # "isGearProviderAvaialble":Z
    .end local v12    # "isWearableDataOnBackupAndRestore":Z
    .end local v13    # "item":Landroid/view/MenuItem;
    .end local v14    # "itemSetGoal":Landroid/view/MenuItem;
    .end local v15    # "showPauseMenu":Z
    :cond_12
    :goto_4
    :pswitch_0
    invoke-super/range {p0 .. p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v1

    goto/16 :goto_0

    .line 406
    .restart local v7    # "cursor":Landroid/database/Cursor;
    .restart local v11    # "isGearProviderAvaialble":Z
    .restart local v12    # "isWearableDataOnBackupAndRestore":Z
    .restart local v13    # "item":Landroid/view/MenuItem;
    .restart local v14    # "itemSetGoal":Landroid/view/MenuItem;
    :cond_13
    const/4 v1, 0x0

    goto/16 :goto_2

    .line 448
    .restart local v8    # "deviceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/walkingmate/utils/Utils$DeviceItem;>;"
    .restart local v10    # "featureManager":Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;
    .restart local v15    # "showPauseMenu":Z
    :cond_14
    const/4 v1, 0x0

    goto :goto_3

    .line 468
    :pswitch_1
    if-eqz p1, :cond_12

    .line 476
    if-eqz v14, :cond_12

    .line 477
    const/4 v1, 0x0

    invoke-interface {v14, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_4

    .line 459
    :pswitch_data_0
    .packed-switch 0x2723
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 259
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onResume()V

    .line 260
    return-void
.end method

.method public removeShareViaButton()V
    .locals 3

    .prologue
    .line 312
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getTotalActionBarButtonCount()I

    move-result v0

    .line 313
    .local v0, "i":I
    :goto_0
    const/4 v1, 0x2

    if-le v0, v1, :cond_0

    .line 314
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    add-int/lit8 v2, v0, -0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->removeActionButton(I)V

    .line 316
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getTotalActionBarButtonCount()I

    move-result v0

    goto :goto_0

    .line 318
    :cond_0
    return-void
.end method

.method public saveViewCount(I)V
    .locals 2
    .param p1, "lev"    # I

    .prologue
    .line 631
    invoke-static {p1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->saveViewStepCount(I)V

    .line 632
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->changeViewStepCountSetting(Landroid/content/Context;)V

    .line 633
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v0

    const/16 v1, 0x2719

    if-ne v0, v1, :cond_0

    .line 634
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->enableInactiveMonitor()V

    .line 635
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mWalkingMateSummaryFragment:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->updateCurrentMode()V

    .line 640
    :goto_0
    return-void

    .line 637
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->disableInactiveMonitor()V

    .line 638
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mWalkingMateSummaryFragment:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->updateCurrentMode()V

    goto :goto_0
.end method

.method protected setIsLaunchedFromWidget(Z)V
    .locals 2
    .param p1, "isLaunchedFromWidget"    # Z

    .prologue
    .line 263
    sput-boolean p1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mIsLaunchedFromWidget:Z

    .line 264
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "launchWidget"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 266
    return-void
.end method

.method showDevicesChooseDialog()V
    .locals 9

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x2

    .line 762
    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;

    const v7, 0x7f090bb5

    const/16 v8, 0xc

    invoke-direct {v1, v7, v8}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;-><init>(II)V

    .line 764
    .local v1, "dialogBuilder":Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->prepareListDialogItem()Ljava/util/ArrayList;

    move-result-object v3

    .line 765
    .local v3, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v1, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->setItems(Ljava/util/List;)Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;

    .line 766
    const v7, 0x7f090bb6

    invoke-virtual {v1, v7}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->setTopTextResId(I)Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;

    .line 768
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v0

    .line 769
    .local v0, "connectAccessoryContant":I
    const/4 v2, 0x0

    .line 771
    .local v2, "dialogIdx":I
    const/16 v7, 0x2719

    if-ne v0, v7, :cond_0

    .line 772
    const/4 v2, 0x0

    .line 787
    :goto_0
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-direct {p0, v2, v5}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->buildDialogBooleanArrayState(II)[Z

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->setChecked([Z)Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;

    .line 788
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    move-result-object v4

    .line 789
    .local v4, "listChooseDialog":Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->DialogTag:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 790
    return-void

    .line 774
    .end local v4    # "listChooseDialog":Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;
    :cond_0
    const/16 v7, 0x2727

    if-ne v0, v7, :cond_4

    .line 775
    iget-boolean v7, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mIsSensorHubInstalled:Z

    if-eqz v7, :cond_2

    .line 776
    iget-object v7, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mDeviceItemList:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-lt v7, v5, :cond_1

    move v2, v5

    :goto_1
    goto :goto_0

    :cond_1
    move v2, v6

    goto :goto_1

    .line 778
    :cond_2
    iget-object v7, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mDeviceItemList:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-lt v7, v5, :cond_3

    move v2, v6

    :goto_2
    goto :goto_0

    :cond_3
    const/4 v2, 0x0

    goto :goto_2

    .line 781
    :cond_4
    iget-boolean v5, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mIsSensorHubInstalled:Z

    if-eqz v5, :cond_5

    .line 782
    const/4 v2, 0x1

    goto :goto_0

    .line 784
    :cond_5
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public showFragment(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;)V
    .locals 3
    .param p1, "fragmentClass"    # Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;

    .prologue
    .line 795
    instance-of v0, p1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    if-eqz v0, :cond_0

    .line 796
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->isPrviousHomeScreen:Z

    .line 799
    :cond_0
    const-string v0, "WalkingMateActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "showFragment = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 800
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->showFragment(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;)V

    .line 801
    return-void
.end method

.method public switchToWearableScreen()V
    .locals 3

    .prologue
    .line 670
    invoke-static {p0}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->getConnectedDeviceType(Landroid/content/Context;)I

    move-result v0

    .line 672
    .local v0, "deviceType":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 674
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->saveViewCount(I)V

    .line 675
    const/4 v1, 0x1

    invoke-static {v1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->setNeedDeviceSyncGuidePopup(Z)V

    .line 677
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mWalkingMateSummaryFragment:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->initCirclePercentValue()V

    .line 678
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mWalkingMateSummaryFragment:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->refreshgoal(Z)V

    .line 679
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mWalkingMateSummaryFragment:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->setNextAndPrevDates()V

    .line 682
    :cond_0
    return-void
.end method
