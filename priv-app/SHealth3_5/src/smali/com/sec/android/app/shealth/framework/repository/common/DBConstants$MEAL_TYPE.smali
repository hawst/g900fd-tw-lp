.class public final Lcom/sec/android/app/shealth/framework/repository/common/DBConstants$MEAL_TYPE;
.super Ljava/lang/Object;
.source "DBConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/framework/repository/common/DBConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "MEAL_TYPE"
.end annotation


# static fields
.field public static final BREAKFAST:I = 0x43

.field public static final DINNER:I = 0x54

.field public static final ID:I = 0x3ea

.field public static final KCAL_UNIT:Ljava/lang/String; = "kcal"

.field public static final LUNCH:I = 0x38

.field public static final OTHER:I = 0x312

.field public static final RECENT_DELETED:I = 0x1

.field public static final RECENT_NOT_DELETED:I = 0x0

.field public static final ROW_ID:I = 0x7d2


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
