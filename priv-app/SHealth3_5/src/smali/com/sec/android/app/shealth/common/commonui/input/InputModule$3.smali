.class Lcom/sec/android/app/shealth/common/commonui/input/InputModule$3;
.super Ljava/lang/Object;
.source "InputModule.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$OnUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->registerListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/common/commonui/input/InputModule;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/common/commonui/input/InputModule;)V
    .locals 0

    .prologue
    .line 309
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$3;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public update(F)V
    .locals 4
    .param p1, "changedNumber"    # F

    .prologue
    const/4 v2, 0x0

    .line 313
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$3;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    # setter for: Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mCurrentValue:F
    invoke-static {v0, p1}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->access$102(Lcom/sec/android/app/shealth/common/commonui/input/InputModule;F)F

    .line 314
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$3;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mMinNormalRange:Ljava/lang/Float;
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->access$200(Lcom/sec/android/app/shealth/common/commonui/input/InputModule;)Ljava/lang/Float;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$3;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mMaxNormalRange:Ljava/lang/Float;
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->access$300(Lcom/sec/android/app/shealth/common/commonui/input/InputModule;)Ljava/lang/Float;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 315
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$3;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    # invokes: Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->changeTextColorAccordingToValue(F)V
    invoke-static {v0, p1}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->access$400(Lcom/sec/android/app/shealth/common/commonui/input/InputModule;F)V

    .line 316
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$3;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    # invokes: Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->changeForegroundAccordingToValue(F)V
    invoke-static {v0, p1}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->access$500(Lcom/sec/android/app/shealth/common/commonui/input/InputModule;F)V

    .line 318
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$3;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    # invokes: Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->updateIncDecButtons(F)V
    invoke-static {v0, p1}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->access$600(Lcom/sec/android/app/shealth/common/commonui/input/InputModule;F)V

    .line 319
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$3;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mNeedToUpdateOtherView:Z

    if-eqz v0, :cond_1

    .line 320
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$3;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    iput-boolean v2, v0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mNeedToUpdateOtherView:Z

    .line 321
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$3;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mGradationView:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->access$700(Lcom/sec/android/app/shealth/common/commonui/input/InputModule;)Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->setValue(F)V

    .line 322
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$3;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mNeedToUpdateOtherView:Z

    .line 324
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$3;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mCurrentValue:F
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->access$100(Lcom/sec/android/app/shealth/common/commonui/input/InputModule;)F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$3;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mMaxInputRange:F
    invoke-static {v1}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->access$800(Lcom/sec/android/app/shealth/common/commonui/input/InputModule;)F

    move-result v1

    cmpl-float v0, v0, v1

    if-gez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$3;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mCurrentValue:F
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->access$100(Lcom/sec/android/app/shealth/common/commonui/input/InputModule;)F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$3;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mMinInputRange:F
    invoke-static {v1}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->access$900(Lcom/sec/android/app/shealth/common/commonui/input/InputModule;)F

    move-result v1

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_3

    .line 325
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$3;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->onTouchModeEditTextColor(Z)V

    .line 327
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$3;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mGradationView:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->access$700(Lcom/sec/android/app/shealth/common/commonui/input/InputModule;)Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$3;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/sec/android/app/shealth/common/commonui/R$string;->prompt_seek_control:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$3;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/sec/android/app/shealth/common/commonui/R$string;->prompt_drag_hold:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 329
    return-void
.end method
