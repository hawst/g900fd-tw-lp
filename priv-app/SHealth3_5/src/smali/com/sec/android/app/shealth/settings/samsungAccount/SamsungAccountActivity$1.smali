.class Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity$1;
.super Ljava/lang/Object;
.source "SamsungAccountActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;)V
    .locals 0

    .prologue
    .line 68
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity$1;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "arg0"    # Landroid/content/DialogInterface;
    .param p2, "keyCode"    # I
    .param p3, "keyEvent"    # Landroid/view/KeyEvent;

    .prologue
    .line 74
    const/4 v1, 0x4

    if-ne p2, v1, :cond_1

    .line 76
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity$1;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->mBackupGoingOn:Z
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->access$100(Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 78
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity$1;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->mBackupGoingOn:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->access$102(Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;Z)Z

    .line 79
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity$1;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->mShealthContentManager:Lcom/samsung/android/sdk/health/content/ShealthContentManager;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->access$200(Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;)Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->stopBackup()V

    .line 86
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity$1;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;

    iget-object v1, v1, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->dialog:Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity$1;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;

    iget-object v1, v1, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->dialog:Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 87
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity$1;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;

    iget-object v1, v1, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->dialog:Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;->dismiss()V

    .line 94
    :cond_1
    :goto_1
    const/4 v1, 0x1

    return v1

    .line 81
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity$1;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->mRestoreGoingOn:Z
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->access$300(Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 83
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity$1;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->mRestoreGoingOn:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->access$302(Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;Z)Z

    .line 84
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity$1;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->mShealthContentManager:Lcom/samsung/android/sdk/health/content/ShealthContentManager;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->access$200(Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;)Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->stopRestore()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 90
    :catch_0
    move-exception v0

    .line 92
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method
