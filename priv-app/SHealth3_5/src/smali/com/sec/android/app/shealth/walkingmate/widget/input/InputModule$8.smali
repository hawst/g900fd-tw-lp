.class Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$8;
.super Ljava/lang/Object;
.source "InputModule.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private skipUpdate:Z

.field final synthetic this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)V
    .locals 1

    .prologue
    .line 526
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$8;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 528
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$8;->skipUpdate:Z

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 12
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    const/16 v10, 0xa

    const/high16 v11, 0x41200000    # 10.0f

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 547
    if-eqz p1, :cond_0

    if-eqz p1, :cond_2

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v6

    if-nez v6, :cond_2

    .line 548
    :cond_0
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$8;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    iget-object v6, v6, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setCursorVisible(Z)V

    .line 549
    const-string v6, "WalkingMateInputModule"

    const-string v7, ""

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 644
    :cond_1
    :goto_0
    return-void

    .line 553
    :cond_2
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 554
    .local v2, "changedString":Ljava/lang/String;
    const/4 v4, 0x0

    .line 555
    .local v4, "isZero":Z
    const-string v6, "."

    invoke-virtual {v2, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 556
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "0"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 557
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$8;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # setter for: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mNeedCursorLastPosition:Z
    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$602(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;Z)Z

    .line 558
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$8;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    iget-object v6, v6, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    invoke-virtual {v6, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 634
    :goto_1
    if-nez v4, :cond_3

    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$8;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mNeedCursorLastPosition:Z
    invoke-static {v6}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$600(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 635
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v6

    invoke-static {p1, v6}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    .line 637
    :cond_3
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$8;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # setter for: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mNeedCursorLastPosition:Z
    invoke-static {v6, v8}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$602(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;Z)Z

    .line 639
    if-eqz p1, :cond_4

    if-eqz p1, :cond_1

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v6

    if-nez v6, :cond_1

    .line 641
    :cond_4
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$8;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    iget-object v6, v6, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setCursorVisible(Z)V

    goto :goto_0

    .line 560
    :cond_5
    const/4 v0, 0x0

    .line 562
    .local v0, "changedNumber":I
    if-eqz v2, :cond_9

    const-string v6, ""

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_9

    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$8;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mSystemNumberSeparator:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$400(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_9

    .line 565
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$8;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mSystemNumberSeparator:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$400(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)Ljava/lang/String;

    move-result-object v6

    const-string v9, ","

    invoke-virtual {v6, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$8;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mSystemNumberSeparator:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$400(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 567
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$8;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mSystemNumberSeparator:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$400(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)Ljava/lang/String;

    move-result-object v6

    const-string v9, "."

    invoke-virtual {v2, v6, v9}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 577
    :cond_6
    :goto_2
    :try_start_0
    invoke-static {v2}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v6

    mul-float/2addr v6, v11

    float-to-int v0, v6

    .line 584
    :goto_3
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$8;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mFilterText:Z
    invoke-static {v6}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$1200(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)Z

    move-result v6

    if-nez v6, :cond_7

    .line 585
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$8;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mMaxInputRange:I
    invoke-static {v6}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$1300(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)I

    move-result v6

    if-le v0, v6, :cond_c

    .line 586
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$8;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # setter for: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mNeedCursorLastPosition:Z
    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$602(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;Z)Z

    .line 587
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$8;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$8;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mMaxInputRange:I
    invoke-static {v6}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$1300(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)I

    move-result v10

    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$8;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # invokes: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->isDecimalNumber()Z
    invoke-static {v6}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$200(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)Z

    move-result v6

    if-eqz v6, :cond_b

    move v6, v7

    :goto_4
    # invokes: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->setNewValue(IZZ)V
    invoke-static {v9, v10, v6, v8}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$700(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;IZZ)V

    .line 588
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$8;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mMaxInputRange:I
    invoke-static {v6}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$1300(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)I

    move-result v0

    .line 606
    :cond_7
    :goto_5
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$8;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # setter for: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mCurrentValue:I
    invoke-static {v6, v0}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$1402(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;I)I

    .line 608
    iget-boolean v6, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$8;->skipUpdate:Z

    if-nez v6, :cond_9

    .line 609
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$8;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$8;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mCurrentValue:I
    invoke-static {v9}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$1400(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)I

    move-result v9

    # setter for: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mCurrentScaleValue:I
    invoke-static {v6, v9}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$1502(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;I)I

    .line 610
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$8;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->isEnabled()Z

    move-result v6

    if-eqz v6, :cond_8

    .line 611
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$8;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mMaxInputRange:I
    invoke-static {v6}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$1300(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)I

    move-result v6

    if-lt v0, v6, :cond_14

    .line 612
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$8;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # invokes: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->setEnableIncBtn(Z)V
    invoke-static {v6, v8}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$1600(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;Z)V

    .line 613
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$8;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # invokes: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->setEnableDecBtn(Z)V
    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$1700(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;Z)V

    .line 614
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$8;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mDecButton:Landroid/widget/ImageButton;
    invoke-static {v6}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$1800(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)Landroid/widget/ImageButton;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/ImageButton;->requestFocus()Z

    .line 626
    :cond_8
    :goto_6
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$8;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mController:Lcom/sec/android/app/shealth/walkingmate/widget/input/ControllerView;
    invoke-static {v6}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$2000(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)Lcom/sec/android/app/shealth/walkingmate/widget/input/ControllerView;

    move-result-object v6

    if-eqz v6, :cond_9

    .line 627
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$8;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mController:Lcom/sec/android/app/shealth/walkingmate/widget/input/ControllerView;
    invoke-static {v6}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$2000(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)Lcom/sec/android/app/shealth/walkingmate/widget/input/ControllerView;

    move-result-object v6

    int-to-float v9, v0

    div-float/2addr v9, v11

    invoke-virtual {v6, v9}, Lcom/sec/android/app/shealth/walkingmate/widget/input/ControllerView;->setValue(F)V

    .line 631
    :cond_9
    iput-boolean v8, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$8;->skipUpdate:Z

    goto/16 :goto_1

    .line 569
    :cond_a
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$8;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # invokes: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->isNumeric(Ljava/lang/String;)Z
    invoke-static {v6, v2}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$1100(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_6

    .line 570
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$8;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mBackupString:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$1000(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_6

    .line 571
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$8;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mBackupString:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$1000(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_2

    .line 579
    :catch_0
    move-exception v3

    .line 581
    .local v3, "e":Ljava/lang/NumberFormatException;
    const-class v6, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    invoke-virtual {v6}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v6

    const-string v9, "Input format is incorrect! Not a double"

    invoke-static {v6, v9}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .end local v3    # "e":Ljava/lang/NumberFormatException;
    :cond_b
    move v6, v8

    .line 587
    goto :goto_4

    .line 589
    :cond_c
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$8;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mMinInputRange:I
    invoke-static {v6}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$800(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)I

    move-result v6

    if-ge v0, v6, :cond_7

    .line 590
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$8;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mMinInputRange:I
    invoke-static {v6}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$800(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)I

    move-result v6

    rem-int/lit8 v6, v6, 0xa

    if-nez v6, :cond_11

    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$8;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mMinInputRange:I
    invoke-static {v6}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$800(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)I

    move-result v6

    int-to-float v6, v6

    :goto_7
    invoke-static {v6}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v5

    .line 591
    .local v5, "minValueLength":I
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v1

    .line 593
    .local v1, "changedNumberLength":I
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$8;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mMinInputRange:I
    invoke-static {v6}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$800(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)I

    move-result v6

    if-lt v6, v10, :cond_d

    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$8;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mSystemNumberSeparator:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$400(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_d

    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$8;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mMinInputRange:I
    invoke-static {v6}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$800(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)I

    move-result v6

    div-int/lit8 v6, v6, 0xa

    div-int/lit8 v9, v0, 0xa

    if-gt v6, v9, :cond_10

    :cond_d
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$8;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mSystemNumberSeparator:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$400(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_e

    if-ge v1, v5, :cond_10

    :cond_e
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$8;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mMinInputRange:I
    invoke-static {v6}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$800(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)I

    move-result v6

    if-ge v6, v10, :cond_f

    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$8;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mSystemNumberSeparator:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$400(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_f

    if-ge v1, v5, :cond_10

    :cond_f
    if-ne v1, v7, :cond_13

    if-nez v0, :cond_13

    .line 598
    :cond_10
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$8;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # setter for: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mNeedCursorLastPosition:Z
    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$602(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;Z)Z

    .line 599
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$8;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$8;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mMinInputRange:I
    invoke-static {v6}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$800(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)I

    move-result v10

    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$8;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # invokes: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->isDecimalNumber()Z
    invoke-static {v6}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$200(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)Z

    move-result v6

    if-eqz v6, :cond_12

    move v6, v7

    :goto_8
    # invokes: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->setNewValue(IZZ)V
    invoke-static {v9, v10, v6, v8}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$700(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;IZZ)V

    .line 600
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$8;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mMinInputRange:I
    invoke-static {v6}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$800(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)I

    move-result v0

    goto/16 :goto_5

    .line 590
    .end local v1    # "changedNumberLength":I
    .end local v5    # "minValueLength":I
    :cond_11
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$8;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mMinInputRange:I
    invoke-static {v6}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$800(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v6, v11

    goto/16 :goto_7

    .restart local v1    # "changedNumberLength":I
    .restart local v5    # "minValueLength":I
    :cond_12
    move v6, v8

    .line 599
    goto :goto_8

    .line 602
    :cond_13
    iput-boolean v7, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$8;->skipUpdate:Z

    goto/16 :goto_5

    .line 615
    .end local v1    # "changedNumberLength":I
    .end local v5    # "minValueLength":I
    :cond_14
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$8;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mMinInputRange:I
    invoke-static {v6}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$800(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)I

    move-result v6

    if-gt v0, v6, :cond_15

    .line 616
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$8;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # invokes: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->setEnableIncBtn(Z)V
    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$1600(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;Z)V

    .line 617
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$8;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # invokes: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->setEnableDecBtn(Z)V
    invoke-static {v6, v8}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$1700(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;Z)V

    .line 618
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$8;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mIncButton:Landroid/widget/ImageButton;
    invoke-static {v6}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$1900(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)Landroid/widget/ImageButton;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/ImageButton;->requestFocus()Z

    goto/16 :goto_6

    .line 620
    :cond_15
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$8;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # invokes: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->setEnableIncBtn(Z)V
    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$1600(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;Z)V

    .line 621
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$8;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # invokes: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->setEnableDecBtn(Z)V
    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$1700(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;Z)V

    goto/16 :goto_6
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 539
    if-eqz p1, :cond_0

    .line 540
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$8;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mBackupString:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$1002(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;Ljava/lang/String;)Ljava/lang/String;

    .line 543
    :cond_0
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 532
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$8;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mTextChangedListener:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$OnTextChangedListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$900(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$OnTextChangedListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 533
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$8;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mTextChangedListener:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$OnTextChangedListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$900(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$OnTextChangedListener;

    move-result-object v0

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$OnTextChangedListener;->onTextChanged(Ljava/lang/String;)V

    .line 535
    :cond_0
    return-void
.end method
