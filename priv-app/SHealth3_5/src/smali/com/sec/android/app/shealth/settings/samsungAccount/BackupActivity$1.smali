.class Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$1;
.super Ljava/lang/Object;
.source "BackupActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->initLayout()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;)V
    .locals 0

    .prologue
    .line 116
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$1;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$1;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth"

    const-string v2, "S009"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$1;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->netUtils:Lcom/sec/android/app/shealth/common/utils/NetworkUtils;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->access$000(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;)Lcom/sec/android/app/shealth/common/utils/NetworkUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;->isNetworkConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 124
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$1;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    const v1, 0x7f080089

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setClickable(Z)V

    .line 126
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$1;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$BackupStatus;->START:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$BackupStatus;

    # invokes: Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->showBackupPopup(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$BackupStatus;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->access$100(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$BackupStatus;)V

    .line 127
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$1;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    # invokes: Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->startBackup()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->access$200(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;)V

    .line 132
    :goto_0
    return-void

    .line 129
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$1;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$BackupStatus;->FAIL_NETWORK_ERROR:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$BackupStatus;

    # invokes: Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->showBackupPopup(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$BackupStatus;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->access$100(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$BackupStatus;)V

    goto :goto_0
.end method
