.class public Lcom/sec/android/app/shealth/uv/log/UvLogAdapter;
.super Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;
.source "UvLogAdapter.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field private dateFormatter:Ljava/text/SimpleDateFormat;

.field dateFormatterNew:Ljava/text/SimpleDateFormat;

.field private mContext:Landroid/content/Context;

.field private mUvDatabaseHelper:Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;

.field private mUvPluginUtils:Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;

.field private previousTime:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/database/Cursor;Landroid/content/Context;)V
    .locals 3
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;-><init>(Landroid/database/Cursor;Landroid/content/Context;)V

    .line 40
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "dd/MM/yyyy"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/log/UvLogAdapter;->dateFormatter:Ljava/text/SimpleDateFormat;

    .line 42
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/log/UvLogAdapter;->previousTime:Ljava/lang/String;

    .line 43
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, " dd"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/log/UvLogAdapter;->dateFormatterNew:Ljava/text/SimpleDateFormat;

    .line 48
    iput-object p2, p0, Lcom/sec/android/app/shealth/uv/log/UvLogAdapter;->mContext:Landroid/content/Context;

    .line 49
    invoke-static {p2}, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/log/UvLogAdapter;->mUvDatabaseHelper:Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;

    .line 50
    invoke-static {}, Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;->getInstance()Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/log/UvLogAdapter;->mUvPluginUtils:Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;

    .line 51
    return-void
.end method

.method private UvStateIcon(Landroid/widget/ImageView;D)I
    .locals 3
    .param p1, "ivBodyIcon"    # Landroid/widget/ImageView;
    .param p2, "uvScore"    # D

    .prologue
    .line 231
    const-wide/16 v1, 0x0

    cmpl-double v1, p2, v1

    if-ltz v1, :cond_0

    const-wide/high16 v1, 0x4000000000000000L    # 2.0

    cmpg-double v1, p2, v1

    if-gtz v1, :cond_0

    .line 232
    const v0, 0x7f090d94

    .line 233
    .local v0, "strLevelId":I
    const v1, 0x7f0201cc

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 247
    :goto_0
    return v0

    .line 234
    .end local v0    # "strLevelId":I
    :cond_0
    const-wide/high16 v1, 0x4008000000000000L    # 3.0

    cmpl-double v1, p2, v1

    if-ltz v1, :cond_1

    const-wide/high16 v1, 0x4014000000000000L    # 5.0

    cmpg-double v1, p2, v1

    if-gtz v1, :cond_1

    .line 235
    const v0, 0x7f090d95

    .line 236
    .restart local v0    # "strLevelId":I
    const v1, 0x7f0201cd

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 237
    .end local v0    # "strLevelId":I
    :cond_1
    const-wide/high16 v1, 0x4018000000000000L    # 6.0

    cmpl-double v1, p2, v1

    if-ltz v1, :cond_2

    const-wide/high16 v1, 0x401c000000000000L    # 7.0

    cmpg-double v1, p2, v1

    if-gtz v1, :cond_2

    .line 238
    const v0, 0x7f090d96

    .line 239
    .restart local v0    # "strLevelId":I
    const v1, 0x7f0201ce

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 240
    .end local v0    # "strLevelId":I
    :cond_2
    const-wide/high16 v1, 0x4020000000000000L    # 8.0

    cmpl-double v1, p2, v1

    if-ltz v1, :cond_3

    const-wide/high16 v1, 0x4024000000000000L    # 10.0

    cmpg-double v1, p2, v1

    if-gtz v1, :cond_3

    .line 241
    const v0, 0x7f090d97

    .line 242
    .restart local v0    # "strLevelId":I
    const v1, 0x7f0201cf

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 244
    .end local v0    # "strLevelId":I
    :cond_3
    const v0, 0x7f090d98

    .line 245
    .restart local v0    # "strLevelId":I
    const v1, 0x7f0201d0

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method private UvStateIconForChinese(Landroid/widget/ImageView;D)I
    .locals 3
    .param p1, "ivBodyIcon"    # Landroid/widget/ImageView;
    .param p2, "uvScore"    # D

    .prologue
    .line 252
    const-wide/16 v1, 0x0

    cmpl-double v1, p2, v1

    if-ltz v1, :cond_0

    const-wide/high16 v1, 0x4000000000000000L    # 2.0

    cmpg-double v1, p2, v1

    if-gtz v1, :cond_0

    .line 253
    const v0, 0x7f091158

    .line 254
    .local v0, "strLevelId":I
    const v1, 0x7f0201cc

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 268
    :goto_0
    return v0

    .line 255
    .end local v0    # "strLevelId":I
    :cond_0
    const-wide/high16 v1, 0x4008000000000000L    # 3.0

    cmpl-double v1, p2, v1

    if-ltz v1, :cond_1

    const-wide/high16 v1, 0x4010000000000000L    # 4.0

    cmpg-double v1, p2, v1

    if-gtz v1, :cond_1

    .line 256
    const v0, 0x7f091159

    .line 257
    .restart local v0    # "strLevelId":I
    const v1, 0x7f0201cd

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 258
    .end local v0    # "strLevelId":I
    :cond_1
    const-wide/high16 v1, 0x4014000000000000L    # 5.0

    cmpl-double v1, p2, v1

    if-ltz v1, :cond_2

    const-wide/high16 v1, 0x4018000000000000L    # 6.0

    cmpg-double v1, p2, v1

    if-gtz v1, :cond_2

    .line 259
    const v0, 0x7f09115a

    .line 260
    .restart local v0    # "strLevelId":I
    const v1, 0x7f0201ce

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 261
    .end local v0    # "strLevelId":I
    :cond_2
    const-wide/high16 v1, 0x401c000000000000L    # 7.0

    cmpl-double v1, p2, v1

    if-ltz v1, :cond_3

    const-wide/high16 v1, 0x4022000000000000L    # 9.0

    cmpg-double v1, p2, v1

    if-gtz v1, :cond_3

    .line 262
    const v0, 0x7f09115b

    .line 263
    .restart local v0    # "strLevelId":I
    const v1, 0x7f0201cf

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 265
    .end local v0    # "strLevelId":I
    :cond_3
    const v0, 0x7f09115c

    .line 266
    .restart local v0    # "strLevelId":I
    const v1, 0x7f0201d0

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method


# virtual methods
.method protected bindChildView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;Z)V
    .locals 46
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cursor"    # Landroid/database/Cursor;
    .param p4, "isLastChild"    # Z

    .prologue
    .line 55
    const v41, 0x7f080588

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v23

    check-cast v23, Landroid/widget/LinearLayout;

    .line 56
    .local v23, "llHeader":Landroid/widget/LinearLayout;
    const v41, 0x7f080589

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v35

    check-cast v35, Landroid/widget/TextView;

    .line 57
    .local v35, "tvHeaderLeft":Landroid/widget/TextView;
    const v41, 0x7f08058a

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v36

    check-cast v36, Landroid/widget/TextView;

    .line 58
    .local v36, "tvHeaderRight":Landroid/widget/TextView;
    const v41, 0x7f08058e

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v34

    check-cast v34, Landroid/widget/TextView;

    .line 59
    .local v34, "tvBodyTopLeft":Landroid/widget/TextView;
    const v41, 0x7f080afc

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v38

    check-cast v38, Landroid/widget/TextView;

    .line 60
    .local v38, "tvSpfBottomText":Landroid/widget/TextView;
    const v41, 0x7f080afd

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v37

    check-cast v37, Landroid/widget/TextView;

    .line 61
    .local v37, "tvRecommendedTime":Landroid/widget/TextView;
    const v41, 0x7f080591

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/ImageButton;

    .line 62
    .local v16, "ivBodyAccessory":Landroid/widget/ImageButton;
    const v41, 0x7f080afb

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/ImageView;

    .line 63
    .local v17, "ivBodyIcon":Landroid/widget/ImageView;
    const v41, 0x7f080592

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/ImageView;

    .line 64
    .local v18, "ivBodyMemo":Landroid/widget/ImageView;
    const v41, 0x7f080593

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v33

    check-cast v33, Landroid/widget/TextView;

    .line 65
    .local v33, "tvBodyRight":Landroid/widget/TextView;
    const v41, 0x7f08058d

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/CheckBox;

    .line 66
    .local v10, "cbBody":Landroid/widget/CheckBox;
    const v41, 0x7f08058b

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v22

    .line 67
    .local v22, "listTopDivider":Landroid/view/View;
    const v41, 0x7f080594

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v20

    .line 68
    .local v20, "listBottomDivider":Landroid/view/View;
    const v41, 0x7f0809e5

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v21

    .line 70
    .local v21, "listLeftSpace":Landroid/view/View;
    const-string/jumbo v41, "sample_time"

    move-object/from16 v0, p3

    move-object/from16 v1, v41

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v41

    move-object/from16 v0, p3

    move/from16 v1, v41

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v31

    .line 71
    .local v31, "time":J
    move-object/from16 v0, p2

    move-wide/from16 v1, v31

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/uv/utils/DateFormatUtil;->getTimeFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v41

    move-object/from16 v0, v33

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 73
    const-string v41, "AvgMonth"

    move-object/from16 v0, p3

    move-object/from16 v1, v41

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v41

    move-object/from16 v0, p3

    move/from16 v1, v41

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    .line 75
    .local v7, "avgUv":J
    long-to-float v0, v7

    move/from16 v41, v0

    invoke-static/range {v41 .. v41}, Ljava/lang/Math;->round(F)I

    move-result v41

    invoke-static/range {v41 .. v41}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    .line 76
    .local v6, "avgString":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v41

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/uv/log/UvLogAdapter;->mUvPluginUtils:Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;

    move-object/from16 v42, v0

    invoke-static {}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->isChinaModel()Z

    move-result v43

    long-to-double v0, v7

    move-wide/from16 v44, v0

    invoke-virtual/range {v42 .. v45}, Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;->getUvIndexString(ZD)I

    move-result v42

    invoke-virtual/range {v41 .. v42}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 78
    .local v5, "avgStateString":Ljava/lang/String;
    move-object/from16 v0, p2

    move-wide/from16 v1, v31

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/uv/utils/DateFormatUtil;->getChildFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v12

    .line 80
    .local v12, "currentTime":Ljava/lang/String;
    move-object/from16 v26, v12

    .line 81
    .local v26, "previousRecordTimeStr":Ljava/lang/String;
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->isFirst()Z

    move-result v41

    if-nez v41, :cond_0

    .line 83
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->moveToPrevious()Z

    .line 84
    const-string/jumbo v41, "sample_time"

    move-object/from16 v0, p3

    move-object/from16 v1, v41

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v41

    move-object/from16 v0, p3

    move/from16 v1, v41

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v24

    .line 85
    .local v24, "previousRecordTime":J
    move-object/from16 v0, p2

    move-wide/from16 v1, v24

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/uv/utils/DateFormatUtil;->getChildFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v26

    .line 86
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->moveToNext()Z

    .line 90
    .end local v24    # "previousRecordTime":J
    :goto_0
    move-object/from16 v0, v26

    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-nez v41, :cond_2

    .line 91
    move-object/from16 v0, p2

    move-wide/from16 v1, v31

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/uv/utils/DateFormatUtil;->getChildFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v41

    move-object/from16 v0, v35

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 92
    const/16 v41, 0x0

    move-object/from16 v0, v23

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 93
    const/16 v41, 0x8

    move-object/from16 v0, v22

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 95
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v9

    .line 96
    .local v9, "cal":Ljava/util/Calendar;
    move-wide/from16 v0, v31

    invoke-virtual {v9, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 97
    new-instance v41, Ljava/text/DateFormatSymbols;

    invoke-direct/range {v41 .. v41}, Ljava/text/DateFormatSymbols;-><init>()V

    invoke-virtual/range {v41 .. v41}, Ljava/text/DateFormatSymbols;->getMonths()[Ljava/lang/String;

    move-result-object v41

    const/16 v42, 0x2

    move/from16 v0, v42

    invoke-virtual {v9, v0}, Ljava/util/Calendar;->get(I)I

    move-result v42

    aget-object v13, v41, v42

    .line 98
    .local v13, "dMonth":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/uv/log/UvLogAdapter;->dateFormatterNew:Ljava/text/SimpleDateFormat;

    move-object/from16 v41, v0

    invoke-static/range {v31 .. v32}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v42

    invoke-virtual/range {v41 .. v42}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    .line 99
    .local v14, "dateNew":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v41

    move-object/from16 v0, v41

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    move-object/from16 v41, v0

    invoke-virtual/range {v41 .. v41}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v19

    .line 100
    .local v19, "language":Ljava/lang/String;
    const-string v41, "ko"

    move-object/from16 v0, v19

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_1

    .line 101
    new-instance v41, Ljava/lang/StringBuilder;

    invoke-direct/range {v41 .. v41}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v42, 0x2

    move/from16 v0, v42

    invoke-virtual {v9, v0}, Ljava/util/Calendar;->get(I)I

    move-result v42

    add-int/lit8 v42, v42, 0x1

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v41

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v42

    invoke-virtual/range {v42 .. v42}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v42

    const v43, 0x7f090212

    invoke-virtual/range {v42 .. v43}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v42

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    const-string v42, " "

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    move-object/from16 v0, v41

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v42

    invoke-virtual/range {v42 .. v42}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v42

    const v43, 0x7f090213

    invoke-virtual/range {v42 .. v43}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v42

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    const-string v42, " "

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    new-instance v42, Ljava/text/DateFormatSymbols;

    invoke-direct/range {v42 .. v42}, Ljava/text/DateFormatSymbols;-><init>()V

    invoke-virtual/range {v42 .. v42}, Ljava/text/DateFormatSymbols;->getWeekdays()[Ljava/lang/String;

    move-result-object v42

    const/16 v43, 0x7

    move/from16 v0, v43

    invoke-virtual {v9, v0}, Ljava/util/Calendar;->get(I)I

    move-result v43

    aget-object v42, v42, v43

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v41

    move-object/from16 v0, v35

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 112
    :goto_1
    new-instance v41, Ljava/lang/StringBuilder;

    invoke-direct/range {v41 .. v41}, Ljava/lang/StringBuilder;-><init>()V

    const v42, 0x7f090f20

    move-object/from16 v0, p2

    move/from16 v1, v42

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v42

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    const-string v42, " "

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    move-object/from16 v0, v41

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v41

    move-object/from16 v0, v36

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 113
    new-instance v41, Ljava/lang/StringBuilder;

    invoke-direct/range {v41 .. v41}, Ljava/lang/StringBuilder;-><init>()V

    const v42, 0x7f090204

    move-object/from16 v0, p2

    move/from16 v1, v42

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v42

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    const-string v42, " "

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    move-object/from16 v0, v41

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    const-string v42, " "

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v42

    const v43, 0x7f0901fd

    invoke-virtual/range {v42 .. v43}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v42

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v41

    move-object/from16 v0, v36

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 114
    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/sec/android/app/shealth/uv/log/UvLogAdapter;->previousTime:Ljava/lang/String;

    .line 120
    .end local v9    # "cal":Ljava/util/Calendar;
    .end local v13    # "dMonth":Ljava/lang/String;
    .end local v14    # "dateNew":Ljava/lang/String;
    .end local v19    # "language":Ljava/lang/String;
    :goto_2
    const/16 v41, 0x1

    move/from16 v0, p4

    move/from16 v1, v41

    if-ne v0, v1, :cond_3

    const/16 v41, 0x0

    :goto_3
    move-object/from16 v0, v20

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 122
    const-string/jumbo v41, "uv_index"

    move-object/from16 v0, p3

    move-object/from16 v1, v41

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v41

    move-object/from16 v0, p3

    move/from16 v1, v41

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v39

    .line 124
    .local v39, "uvScore":D
    const-string v41, "SUNPROTECTION"

    move-object/from16 v0, p3

    move-object/from16 v1, v41

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v41

    move-object/from16 v0, p3

    move/from16 v1, v41

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v29

    .line 125
    .local v29, "spf":I
    const-string v41, "RECOMMENDEDDURATION"

    move-object/from16 v0, p3

    move-object/from16 v1, v41

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v41

    move-object/from16 v0, p3

    move/from16 v1, v41

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v27

    .line 127
    .local v27, "recommendedTime":J
    move/from16 v0, v29

    int-to-long v0, v0

    move-wide/from16 v41, v0

    sget-object v43, Lcom/sec/android/app/shealth/uv/common/UvConstants;->INVALID:Ljava/lang/Long;

    invoke-virtual/range {v43 .. v43}, Ljava/lang/Long;->longValue()J

    move-result-wide v43

    cmp-long v41, v41, v43

    if-eqz v41, :cond_4

    .line 129
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v41

    const v42, 0x7f090db8

    const/16 v43, 0x1

    move/from16 v0, v43

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v43, v0

    const/16 v44, 0x0

    invoke-static/range {v29 .. v29}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v45

    aput-object v45, v43, v44

    invoke-virtual/range {v41 .. v43}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v41

    move-object/from16 v0, v38

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 130
    const/16 v41, 0x0

    move-object/from16 v0, v38

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 138
    :goto_4
    sget-object v41, Lcom/sec/android/app/shealth/uv/common/UvConstants;->INVALID:Ljava/lang/Long;

    invoke-virtual/range {v41 .. v41}, Ljava/lang/Long;->longValue()J

    move-result-wide v41

    cmp-long v41, v27, v41

    if-eqz v41, :cond_5

    .line 141
    new-instance v41, Ljava/lang/StringBuilder;

    invoke-direct/range {v41 .. v41}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v42

    invoke-virtual/range {v42 .. v42}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v42

    const v43, 0x7f090daf

    invoke-virtual/range {v42 .. v43}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v42

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    const-string v42, " "

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    invoke-static {}, Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;->getInstance()Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;

    move-result-object v42

    move-object/from16 v0, v42

    move-wide/from16 v1, v27

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;->calculateSafetyTime(J)Ljava/lang/String;

    move-result-object v42

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v41

    move-object/from16 v0, v37

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 143
    new-instance v41, Ljava/lang/StringBuilder;

    invoke-direct/range {v41 .. v41}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v42

    invoke-virtual/range {v42 .. v42}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v42

    const v43, 0x7f090daf

    invoke-virtual/range {v42 .. v43}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v42

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v42

    invoke-virtual/range {v42 .. v42}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v42

    const v43, 0x7f09020b

    invoke-virtual/range {v42 .. v43}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v42

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    const-string v42, " "

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    invoke-static {}, Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;->getInstance()Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;

    move-result-object v42

    move-object/from16 v0, v42

    move-wide/from16 v1, v27

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;->calculateSafetyTimeForTalkback(J)Ljava/lang/String;

    move-result-object v42

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v42

    invoke-virtual/range {v42 .. v42}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v42

    const v43, 0x7f090c9c

    invoke-virtual/range {v42 .. v43}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v42

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v41

    move-object/from16 v0, v37

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 147
    const/16 v41, 0x0

    move-object/from16 v0, v37

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 157
    :goto_5
    const v30, 0x7f090d94

    .line 160
    .local v30, "strLevelId":I
    invoke-static {}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->isChinaModel()Z

    move-result v41

    if-nez v41, :cond_6

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-wide/from16 v2, v39

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/uv/log/UvLogAdapter;->UvStateIcon(Landroid/widget/ImageView;D)I

    move-result v30

    .line 162
    :goto_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/uv/log/UvLogAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v41, v0

    invoke-static/range {v41 .. v41}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->isSamsungAccount(Landroid/content/Context;)Z

    move-result v41

    if-eqz v41, :cond_7

    sget-object v41, Landroid/os/Build;->TYPE:Ljava/lang/String;

    const-string v42, "eng"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v41

    if-eqz v41, :cond_7

    .line 163
    new-instance v41, Ljava/lang/StringBuilder;

    invoke-direct/range {v41 .. v41}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v42

    invoke-virtual/range {v42 .. v42}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v42

    move-object/from16 v0, v42

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v42

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    const-string v42, "( "

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    move-wide/from16 v0, v39

    double-to-int v0, v0

    move/from16 v42, v0

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v41

    const-string v42, " )"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v41

    move-object/from16 v0, v34

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 168
    :goto_7
    const-string v41, "comment"

    move-object/from16 v0, p3

    move-object/from16 v1, v41

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v41

    move-object/from16 v0, p3

    move/from16 v1, v41

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 169
    .local v11, "comment":Ljava/lang/String;
    if-eqz v11, :cond_8

    const-string v41, ""

    move-object/from16 v0, v41

    invoke-virtual {v11, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v41

    if-eqz v41, :cond_8

    .line 170
    const/16 v41, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 171
    sget-object v41, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;->TYPE_USER_CUSTOM:Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;

    const v42, 0x7f09007c

    move-object/from16 v0, p2

    move/from16 v1, v42

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v42

    move-object/from16 v0, v18

    move-object/from16 v1, v41

    move-object/from16 v2, v42

    invoke-static {v0, v1, v2, v11}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->setHoverPopupListener(Landroid/view/View;Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    :goto_8
    const-string/jumbo v41, "user_device__id"

    move-object/from16 v0, p3

    move-object/from16 v1, v41

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v41

    move-object/from16 v0, p3

    move/from16 v1, v41

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 177
    .local v4, "accessory":Ljava/lang/String;
    const/16 v41, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 178
    if-eqz v4, :cond_a

    .line 180
    const-string v41, "10029"

    move-object/from16 v0, v41

    invoke-virtual {v4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v41

    if-eqz v41, :cond_9

    .line 182
    const/16 v41, 0x4

    move-object/from16 v0, v16

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 195
    :goto_9
    new-instance v41, Ljava/lang/StringBuilder;

    invoke-direct/range {v41 .. v41}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/uv/log/UvLogAdapter;->dateFormatter:Ljava/text/SimpleDateFormat;

    move-object/from16 v42, v0

    invoke-static/range {v31 .. v32}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v43

    invoke-virtual/range {v42 .. v43}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v42

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    const-string v42, "_"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    const-string v42, "_id"

    move-object/from16 v0, p3

    move-object/from16 v1, v42

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v42

    move-object/from16 v0, p3

    move/from16 v1, v42

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v42

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v41

    move-object/from16 v0, p1

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 196
    new-instance v41, Ljava/lang/StringBuilder;

    invoke-direct/range {v41 .. v41}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/uv/log/UvLogAdapter;->dateFormatter:Ljava/text/SimpleDateFormat;

    move-object/from16 v42, v0

    invoke-static/range {v31 .. v32}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v43

    invoke-virtual/range {v42 .. v43}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v42

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    const-string v42, "_"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    const-string v42, "_id"

    move-object/from16 v0, p3

    move-object/from16 v1, v42

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v42

    move-object/from16 v0, p3

    move/from16 v1, v42

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v42

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v41

    move-object/from16 v0, v41

    invoke-virtual {v10, v0}, Landroid/widget/CheckBox;->setTag(Ljava/lang/Object;)V

    .line 197
    move-object/from16 v0, p0

    invoke-virtual {v10, v0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 198
    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v41

    const v42, 0x106000d

    invoke-virtual/range {v41 .. v42}, Landroid/content/res/Resources;->getColor(I)I

    move-result v41

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 200
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/uv/log/UvLogAdapter;->isMenuDeleteMode()Z

    move-result v41

    if-eqz v41, :cond_d

    .line 201
    const/16 v41, 0x0

    move/from16 v0, v41

    invoke-virtual {v10, v0}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 202
    const/16 v41, 0x8

    move-object/from16 v0, v21

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 203
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/uv/log/UvLogAdapter;->isCheckAll()Z

    move-result v41

    if-eqz v41, :cond_b

    .line 204
    const/16 v41, 0x1

    move/from16 v0, v41

    invoke-virtual {v10, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 227
    :goto_a
    return-void

    .line 89
    .end local v4    # "accessory":Ljava/lang/String;
    .end local v11    # "comment":Ljava/lang/String;
    .end local v27    # "recommendedTime":J
    .end local v29    # "spf":I
    .end local v30    # "strLevelId":I
    .end local v39    # "uvScore":D
    :cond_0
    const-string v26, ""

    goto/16 :goto_0

    .line 110
    .restart local v9    # "cal":Ljava/util/Calendar;
    .restart local v13    # "dMonth":Ljava/lang/String;
    .restart local v14    # "dateNew":Ljava/lang/String;
    .restart local v19    # "language":Ljava/lang/String;
    :cond_1
    new-instance v41, Ljava/lang/StringBuilder;

    invoke-direct/range {v41 .. v41}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v41

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    const-string v42, " "

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    move-object/from16 v0, v41

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    const-string v42, " "

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    new-instance v42, Ljava/text/DateFormatSymbols;

    invoke-direct/range {v42 .. v42}, Ljava/text/DateFormatSymbols;-><init>()V

    invoke-virtual/range {v42 .. v42}, Ljava/text/DateFormatSymbols;->getWeekdays()[Ljava/lang/String;

    move-result-object v42

    const/16 v43, 0x7

    move/from16 v0, v43

    invoke-virtual {v9, v0}, Ljava/util/Calendar;->get(I)I

    move-result v43

    aget-object v42, v42, v43

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v41

    move-object/from16 v0, v35

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 116
    .end local v9    # "cal":Ljava/util/Calendar;
    .end local v13    # "dMonth":Ljava/lang/String;
    .end local v14    # "dateNew":Ljava/lang/String;
    .end local v19    # "language":Ljava/lang/String;
    :cond_2
    const/16 v41, 0x8

    move-object/from16 v0, v23

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 117
    const/16 v41, 0x0

    move-object/from16 v0, v22

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_2

    .line 120
    :cond_3
    const/16 v41, 0x8

    goto/16 :goto_3

    .line 134
    .restart local v27    # "recommendedTime":J
    .restart local v29    # "spf":I
    .restart local v39    # "uvScore":D
    :cond_4
    const/16 v41, 0x8

    move-object/from16 v0, v38

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_4

    .line 151
    :cond_5
    const/16 v41, 0x8

    move-object/from16 v0, v37

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_5

    .line 160
    .restart local v30    # "strLevelId":I
    :cond_6
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-wide/from16 v2, v39

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/uv/log/UvLogAdapter;->UvStateIconForChinese(Landroid/widget/ImageView;D)I

    move-result v30

    goto/16 :goto_6

    .line 165
    :cond_7
    move-object/from16 v0, v34

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_7

    .line 173
    .restart local v11    # "comment":Ljava/lang/String;
    :cond_8
    const/16 v41, 0x8

    move-object/from16 v0, v18

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_8

    .line 186
    .restart local v4    # "accessory":Ljava/lang/String;
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/uv/log/UvLogAdapter;->mUvDatabaseHelper:Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;

    move-object/from16 v41, v0

    const-string v42, "_id"

    move-object/from16 v0, p3

    move-object/from16 v1, v42

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v42

    move-object/from16 v0, p3

    move/from16 v1, v42

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v42

    invoke-virtual/range {v41 .. v42}, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->getDeviceName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 187
    .local v15, "deviceName":Ljava/lang/String;
    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 188
    const/16 v41, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_9

    .line 192
    .end local v15    # "deviceName":Ljava/lang/String;
    :cond_a
    const/16 v41, 0x4

    move-object/from16 v0, v16

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_9

    .line 205
    :cond_b
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/uv/log/UvLogAdapter;->getCheckedItems()Ljava/util/ArrayList;

    move-result-object v41

    invoke-virtual {v10}, Landroid/widget/CheckBox;->getTag()Ljava/lang/Object;

    move-result-object v42

    invoke-virtual/range {v41 .. v42}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_c

    .line 206
    const/16 v41, 0x1

    move/from16 v0, v41

    invoke-virtual {v10, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto/16 :goto_a

    .line 208
    :cond_c
    const/16 v41, 0x0

    move/from16 v0, v41

    invoke-virtual {v10, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto/16 :goto_a

    .line 210
    :cond_d
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/uv/log/UvLogAdapter;->isDeleteMode()Z

    move-result v41

    if-eqz v41, :cond_10

    .line 211
    const/16 v41, 0x0

    move/from16 v0, v41

    invoke-virtual {v10, v0}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 212
    const/16 v41, 0x8

    move-object/from16 v0, v21

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 213
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/uv/log/UvLogAdapter;->isCheckAll()Z

    move-result v41

    if-eqz v41, :cond_e

    .line 214
    const v41, 0x7f02086c

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    goto/16 :goto_a

    .line 215
    :cond_e
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/uv/log/UvLogAdapter;->getCheckedItems()Ljava/util/ArrayList;

    move-result-object v41

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v42

    invoke-virtual/range {v41 .. v42}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_f

    .line 216
    const v41, 0x7f02086c

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    goto/16 :goto_a

    .line 218
    :cond_f
    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v41

    const v42, 0x106000d

    invoke-virtual/range {v41 .. v42}, Landroid/content/res/Resources;->getColor(I)I

    move-result v41

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    goto/16 :goto_a

    .line 222
    :cond_10
    const/16 v41, 0x8

    move-object/from16 v0, v21

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 223
    const/16 v41, 0x8

    move/from16 v0, v41

    invoke-virtual {v10, v0}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 224
    const/16 v41, 0x0

    move/from16 v0, v41

    invoke-virtual {v10, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 225
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/uv/log/UvLogAdapter;->getCheckedItems()Ljava/util/ArrayList;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Ljava/util/ArrayList;->clear()V

    goto/16 :goto_a
.end method

.method protected bindGroupView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;Z)V
    .locals 16
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cursor"    # Landroid/database/Cursor;
    .param p4, "isExpanded"    # Z

    .prologue
    .line 273
    const v11, 0x7f080583

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 274
    .local v8, "tvFirst":Landroid/widget/TextView;
    const v11, 0x7f080584

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    .line 275
    .local v9, "tvSecond":Landroid/widget/TextView;
    const v11, 0x7f080586

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    .line 276
    .local v4, "ivExpand":Landroid/widget/ImageView;
    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v11

    iget-object v11, v11, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v11}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v5

    .line 279
    .local v5, "strLanguage":Ljava/lang/String;
    const-string/jumbo v11, "sample_time"

    move-object/from16 v0, p3

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    move-object/from16 v0, p3

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 280
    .local v6, "time":J
    move-object/from16 v0, p2

    invoke-static {v0, v6, v7}, Lcom/sec/android/app/shealth/uv/utils/DateFormatUtil;->getYearShortMonthText(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v10

    .line 281
    .local v10, "yearMonth":Ljava/lang/String;
    invoke-virtual {v8, v10}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 283
    if-eqz v5, :cond_0

    const-string v11, "fi"

    invoke-virtual {v5, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 284
    invoke-virtual {v10}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v8, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 290
    :goto_0
    const-string v11, "AvgUv"

    move-object/from16 v0, p3

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    move-object/from16 v0, p3

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 291
    .local v2, "avgUv":J
    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/uv/log/UvLogAdapter;->mUvPluginUtils:Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;

    invoke-static {}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->isChinaModel()Z

    move-result v13

    long-to-double v14, v2

    invoke-virtual {v12, v13, v14, v15}, Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;->getUvIndexString(ZD)I

    move-result v12

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 293
    .local v1, "avgStateString":Ljava/lang/String;
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, " ("

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const v12, 0x7f090ae0

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ")"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 294
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, " ("

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const v12, 0x7f090204

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ")"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f09021c

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    if-eqz p4, :cond_1

    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v13, 0x7f0901ef

    invoke-virtual {v11, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    :goto_1
    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v11}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 298
    if-eqz p4, :cond_2

    .line 299
    const v11, 0x7f020845

    invoke-virtual {v4, v11}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 300
    sget-object v11, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;->TYPE_TOOL_TIP:Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;

    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f090b6d

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    const-string v13, ""

    invoke-static {v4, v11, v12, v13}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->setHoverPopupListener(Landroid/view/View;Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;Ljava/lang/String;Ljava/lang/String;)V

    .line 311
    :goto_2
    move/from16 v0, p4

    invoke-virtual {v4, v0}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 312
    return-void

    .line 287
    .end local v1    # "avgStateString":Ljava/lang/String;
    .end local v2    # "avgUv":J
    :cond_0
    invoke-virtual {v8, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 294
    .restart local v1    # "avgStateString":Ljava/lang/String;
    .restart local v2    # "avgUv":J
    :cond_1
    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v13, 0x7f090217

    invoke-virtual {v11, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    goto :goto_1

    .line 305
    :cond_2
    const v11, 0x7f02084c

    invoke-virtual {v4, v11}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 306
    sget-object v11, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;->TYPE_TOOL_TIP:Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;

    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f090b6e

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    const-string v13, ""

    invoke-static {v4, v11, v12, v13}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->setHoverPopupListener(Landroid/view/View;Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method protected getChildrenCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 1
    .param p1, "groupCursor"    # Landroid/database/Cursor;

    .prologue
    .line 326
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/log/UvLogAdapter;->mUvDatabaseHelper:Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->getChildCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method protected getColumnNameForCreateTime()Ljava/lang/String;
    .locals 1

    .prologue
    .line 348
    const-string/jumbo v0, "sample_time"

    return-object v0
.end method

.method protected getColumnNameForID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 343
    const-string v0, "_id"

    return-object v0
.end method

.method protected getTotalChildCount()I
    .locals 4

    .prologue
    .line 331
    const/4 v0, 0x0

    .line 332
    .local v0, "childCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/log/UvLogAdapter;->getGroupCount()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 333
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_1
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/uv/log/UvLogAdapter;->getChildrenCount(I)I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 334
    add-int/lit8 v0, v0, 0x1

    .line 333
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 332
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 338
    .end local v2    # "j":I
    :cond_1
    return v0
.end method

.method protected newChildView(Landroid/content/Context;Landroid/database/Cursor;ZLandroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cursor"    # Landroid/database/Cursor;
    .param p3, "isLastChild"    # Z
    .param p4, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 316
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03026a

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p4, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected newGroupView(Landroid/content/Context;Landroid/database/Cursor;ZLandroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cursor"    # Landroid/database/Cursor;
    .param p3, "isExpanded"    # Z
    .param p4, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 321
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030269

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p4, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 1
    .param p1, "button"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    .line 354
    new-instance v0, Lcom/sec/android/app/shealth/uv/log/UvLogAdapter$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/sec/android/app/shealth/uv/log/UvLogAdapter$1;-><init>(Lcom/sec/android/app/shealth/uv/log/UvLogAdapter;Landroid/widget/CompoundButton;Z)V

    invoke-virtual {p1, v0}, Landroid/widget/CompoundButton;->post(Ljava/lang/Runnable;)Z

    .line 360
    return-void
.end method
