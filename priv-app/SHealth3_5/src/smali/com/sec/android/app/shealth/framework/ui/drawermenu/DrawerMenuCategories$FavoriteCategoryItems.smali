.class public final enum Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FavoriteCategoryItems;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "FavoriteCategoryItems"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FavoriteCategoryItems;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FavoriteCategoryItems;

.field public static final enum FOOD:Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FavoriteCategoryItems;

.field public static final enum PEDOMETER:Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FavoriteCategoryItems;

.field public static final enum THERMO_HYGROMETER:Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FavoriteCategoryItems;


# instance fields
.field private itemName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FavoriteCategoryItems;

    const-string v1, "PEDOMETER"

    const-string v2, "Pedometer"

    invoke-direct {v0, v1, v3, v2}, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FavoriteCategoryItems;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FavoriteCategoryItems;->PEDOMETER:Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FavoriteCategoryItems;

    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FavoriteCategoryItems;

    const-string v1, "FOOD"

    const-string v2, "Food"

    invoke-direct {v0, v1, v4, v2}, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FavoriteCategoryItems;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FavoriteCategoryItems;->FOOD:Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FavoriteCategoryItems;

    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FavoriteCategoryItems;

    const-string v1, "THERMO_HYGROMETER"

    const-string v2, "Thermo-hygrometer"

    invoke-direct {v0, v1, v5, v2}, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FavoriteCategoryItems;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FavoriteCategoryItems;->THERMO_HYGROMETER:Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FavoriteCategoryItems;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FavoriteCategoryItems;

    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FavoriteCategoryItems;->PEDOMETER:Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FavoriteCategoryItems;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FavoriteCategoryItems;->FOOD:Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FavoriteCategoryItems;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FavoriteCategoryItems;->THERMO_HYGROMETER:Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FavoriteCategoryItems;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FavoriteCategoryItems;->$VALUES:[Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FavoriteCategoryItems;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FavoriteCategoryItems;->itemName:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FavoriteCategoryItems;
    .locals 1

    const-class v0, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FavoriteCategoryItems;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FavoriteCategoryItems;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FavoriteCategoryItems;
    .locals 1

    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FavoriteCategoryItems;->$VALUES:[Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FavoriteCategoryItems;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FavoriteCategoryItems;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FavoriteCategoryItems;

    return-object v0
.end method


# virtual methods
.method public getMenuItemName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$FavoriteCategoryItems;->itemName:Ljava/lang/String;

    return-object v0
.end method
