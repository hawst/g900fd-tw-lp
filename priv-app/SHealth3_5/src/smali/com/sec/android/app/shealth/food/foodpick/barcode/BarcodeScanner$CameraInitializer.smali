.class Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$CameraInitializer;
.super Ljava/lang/Object;
.source "BarcodeScanner.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CameraInitializer"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;)V
    .locals 0

    .prologue
    .line 370
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$CameraInitializer;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$1;

    .prologue
    .line 370
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$CameraInitializer;-><init>(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 373
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$CameraInitializer;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->camera:Landroid/hardware/Camera;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->access$700(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;)Landroid/hardware/Camera;

    move-result-object v1

    if-nez v1, :cond_2

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v1

    if-nez v1, :cond_2

    .line 375
    :try_start_0
    sget-object v1, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "camera initializing..."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 376
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$CameraInitializer;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->surfaceCreated:Z
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->access$800(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 377
    const-wide/16 v1, 0xc8

    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 387
    :catch_0
    move-exception v0

    .line 388
    .local v0, "exception":Ljava/lang/Exception;
    sget-object v1, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->LOG_TAG:Ljava/lang/String;

    const-string v2, "initialization failed"

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/common/utils/Logging;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 389
    sget-object v1, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->LOG_TAG:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 390
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$CameraInitializer;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;

    # invokes: Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->stopPreviewAndReleaseCamera()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->access$900(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;)V

    goto :goto_0

    .line 379
    .end local v0    # "exception":Ljava/lang/Exception;
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$CameraInitializer;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;

    invoke-static {}, Landroid/hardware/Camera;->open()Landroid/hardware/Camera;

    move-result-object v2

    # setter for: Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->camera:Landroid/hardware/Camera;
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->access$702(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;Landroid/hardware/Camera;)Landroid/hardware/Camera;

    .line 380
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$CameraInitializer;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->camera:Landroid/hardware/Camera;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->access$700(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;)Landroid/hardware/Camera;

    move-result-object v1

    if-nez v1, :cond_1

    .line 381
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$CameraInitializer;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;

    # invokes: Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->stopPreviewAndReleaseCamera()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->access$900(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;)V

    .line 382
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$CameraInitializer;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "No camera on this device"

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 384
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$CameraInitializer;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->camera:Landroid/hardware/Camera;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->access$700(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;)Landroid/hardware/Camera;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$CameraInitializer;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->holder:Landroid/view/SurfaceHolder;
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->access$1000(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;)Landroid/view/SurfaceHolder;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/hardware/Camera;->setPreviewDisplay(Landroid/view/SurfaceHolder;)V

    .line 385
    sget-object v1, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->LOG_TAG:Ljava/lang/String;

    const-string v2, "initialization success"

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 393
    :cond_2
    return-void
.end method
