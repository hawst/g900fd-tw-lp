.class public abstract Lcom/sec/android/app/shealth/cignacoach/CignaBaseActivity;
.super Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadBaseActivity;
.source "CignaBaseActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/cignacoach/CignaBaseActivity$ResetDataReceiver;
    }
.end annotation


# instance fields
.field private mResetDataReceiver:Lcom/sec/android/app/shealth/cignacoach/CignaBaseActivity$ResetDataReceiver;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadBaseActivity;-><init>()V

    .line 41
    return-void
.end method

.method private registerReceiver()V
    .locals 2

    .prologue
    .line 31
    new-instance v1, Lcom/sec/android/app/shealth/cignacoach/CignaBaseActivity$ResetDataReceiver;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/cignacoach/CignaBaseActivity$ResetDataReceiver;-><init>(Lcom/sec/android/app/shealth/cignacoach/CignaBaseActivity;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/CignaBaseActivity;->mResetDataReceiver:Lcom/sec/android/app/shealth/cignacoach/CignaBaseActivity$ResetDataReceiver;

    .line 32
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 33
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "com.sec.android.app.shealth.cignacoach.RESET_DATA"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 34
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/CignaBaseActivity;->mResetDataReceiver:Lcom/sec/android/app/shealth/cignacoach/CignaBaseActivity$ResetDataReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/shealth/cignacoach/CignaBaseActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 35
    return-void
.end method

.method private unregisterReceiver()V
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaBaseActivity;->mResetDataReceiver:Lcom/sec/android/app/shealth/cignacoach/CignaBaseActivity$ResetDataReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/CignaBaseActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 39
    return-void
.end method


# virtual methods
.method public isMenuShown()Z
    .locals 1

    .prologue
    .line 66
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaBaseActivity;->isDrawerMenuShown()Z

    move-result v0

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 19
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadBaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 21
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaBaseActivity;->registerReceiver()V

    .line 22
    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 26
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadBaseActivity;->onDestroy()V

    .line 27
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaBaseActivity;->unregisterReceiver()V

    .line 28
    return-void
.end method

.method protected onLogSelected()V
    .locals 0

    .prologue
    .line 63
    return-void
.end method

.method protected abstract updateUI()V
.end method
