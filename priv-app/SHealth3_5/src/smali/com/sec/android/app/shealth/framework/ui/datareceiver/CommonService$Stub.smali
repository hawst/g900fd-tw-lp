.class public abstract Lcom/sec/android/app/shealth/framework/ui/datareceiver/CommonService$Stub;
.super Landroid/os/Binder;

# interfaces
.implements Lcom/sec/android/app/shealth/framework/ui/datareceiver/CommonService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/framework/ui/datareceiver/CommonService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/framework/ui/datareceiver/CommonService$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.sec.android.app.shealth.framework.ui.datareceiver.CommonService"

.field static final TRANSACTION_getJoinedList:I = 0x5

.field static final TRANSACTION_joinWithDevice:I = 0x2

.field static final TRANSACTION_joinWithPairedDevice:I = 0x3

.field static final TRANSACTION_leaveDevice:I = 0x4

.field static final TRANSACTION_stopCommonService:I = 0x1


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    const-string v0, "com.sec.android.app.shealth.framework.ui.datareceiver.CommonService"

    invoke-virtual {p0, p0, v0}, Lcom/sec/android/app/shealth/framework/ui/datareceiver/CommonService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/sec/android/app/shealth/framework/ui/datareceiver/CommonService;
    .locals 2

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "com.sec.android.app.shealth.framework.ui.datareceiver.CommonService"

    invoke-interface {p0, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/sec/android/app/shealth/framework/ui/datareceiver/CommonService;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/datareceiver/CommonService;

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/datareceiver/CommonService$Stub$Proxy;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/framework/ui/datareceiver/CommonService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v1, 0x1

    sparse-switch p1, :sswitch_data_0

    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v1

    :goto_0
    return v1

    :sswitch_0
    const-string v0, "com.sec.android.app.shealth.framework.ui.datareceiver.CommonService"

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    :sswitch_1
    const-string v0, "com.sec.android.app.shealth.framework.ui.datareceiver.CommonService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/datareceiver/CommonService$Stub;->stopCommonService()V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    :sswitch_2
    const-string v0, "com.sec.android.app.shealth.framework.ui.datareceiver.CommonService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/framework/ui/datareceiver/CommonSensorServiceCallback$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/android/app/shealth/framework/ui/datareceiver/CommonSensorServiceCallback;

    move-result-object v2

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    invoke-virtual {p0, v0, v2, v3}, Lcom/sec/android/app/shealth/framework/ui/datareceiver/CommonService$Stub;->joinWithDevice(Ljava/lang/String;Lcom/sec/android/app/shealth/framework/ui/datareceiver/CommonSensorServiceCallback;I)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    :sswitch_3
    const-string v0, "com.sec.android.app.shealth.framework.ui.datareceiver.CommonService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/framework/ui/datareceiver/CommonSensorServiceCallback$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/android/app/shealth/framework/ui/datareceiver/CommonSensorServiceCallback;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/sec/android/app/shealth/framework/ui/datareceiver/CommonService$Stub;->joinWithPairedDevice(Ljava/lang/String;Lcom/sec/android/app/shealth/framework/ui/datareceiver/CommonSensorServiceCallback;)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    :sswitch_4
    const-string v0, "com.sec.android.app.shealth.framework.ui.datareceiver.CommonService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/datareceiver/CommonSensorServiceCallback$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/android/app/shealth/framework/ui/datareceiver/CommonSensorServiceCallback;

    move-result-object v3

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_1
    invoke-virtual {p0, v2, v3, v0}, Lcom/sec/android/app/shealth/framework/ui/datareceiver/CommonService$Stub;->leaveDevice(Ljava/lang/String;Lcom/sec/android/app/shealth/framework/ui/datareceiver/CommonSensorServiceCallback;Z)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :sswitch_5
    const-string v0, "com.sec.android.app.shealth.framework.ui.datareceiver.CommonService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/datareceiver/CommonService$Stub;->getJoinedList()[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
