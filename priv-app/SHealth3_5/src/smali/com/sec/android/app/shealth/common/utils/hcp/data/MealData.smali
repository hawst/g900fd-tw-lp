.class public Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
.super Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;
.source "MealData.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private comment:Ljava/lang/String;

.field private kcal:F

.field private mIsFavourite:Z

.field private mMealPlanId:J

.field private mealTime:J

.field private mealType:I

.field private name:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 214
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData$1;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const-wide/16 v1, -0x1

    .line 85
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;-><init>()V

    .line 7
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->name:Ljava/lang/String;

    .line 8
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->kcal:F

    .line 9
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->mealType:I

    .line 10
    iput-wide v1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->mealTime:J

    .line 12
    iput-wide v1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->mMealPlanId:J

    .line 86
    return-void
.end method

.method public constructor <init>(JLjava/lang/String;FIJLjava/lang/String;)V
    .locals 7
    .param p1, "id"    # J
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "kcal"    # F
    .param p5, "mealType"    # I
    .param p6, "mealTime"    # J
    .param p8, "comment"    # Ljava/lang/String;

    .prologue
    const-wide/16 v1, -0x1

    .line 62
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;-><init>(J)V

    .line 7
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->name:Ljava/lang/String;

    .line 8
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->kcal:F

    .line 9
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->mealType:I

    .line 10
    iput-wide v1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->mealTime:J

    .line 12
    iput-wide v1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->mMealPlanId:J

    move-object v0, p0

    move-object v1, p3

    move v2, p4

    move v3, p5

    move-wide v4, p6

    move-object v6, p8

    .line 63
    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->init(Ljava/lang/String;FIJLjava/lang/String;)V

    .line 64
    return-void
.end method

.method public constructor <init>(JLjava/lang/String;FIJLjava/lang/String;J)V
    .locals 9
    .param p1, "id"    # J
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "kcal"    # F
    .param p5, "mealType"    # I
    .param p6, "mealTime"    # J
    .param p8, "comment"    # Ljava/lang/String;
    .param p9, "mealPlanId"    # J

    .prologue
    .line 57
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;-><init>(J)V

    .line 7
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->name:Ljava/lang/String;

    .line 8
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->kcal:F

    .line 9
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->mealType:I

    .line 10
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->mealTime:J

    .line 12
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->mMealPlanId:J

    move-object v0, p0

    move-object v1, p3

    move v2, p4

    move v3, p5

    move-wide v4, p6

    move-object/from16 v6, p8

    move-wide/from16 v7, p9

    .line 58
    invoke-direct/range {v0 .. v8}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->init(Ljava/lang/String;FIJLjava/lang/String;J)V

    .line 59
    return-void
.end method

.method public constructor <init>(JLjava/lang/String;FIJLjava/lang/String;JJI)V
    .locals 16
    .param p1, "id"    # J
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "kcal"    # F
    .param p5, "mealType"    # I
    .param p6, "mealTime"    # J
    .param p8, "comment"    # Ljava/lang/String;
    .param p9, "createTime"    # J
    .param p11, "updateTime"    # J
    .param p13, "timeZone"    # I

    .prologue
    .line 67
    const-wide/16 v9, -0x1

    move-object/from16 v0, p0

    move-wide/from16 v1, p1

    move-object/from16 v3, p3

    move/from16 v4, p4

    move/from16 v5, p5

    move-wide/from16 v6, p6

    move-object/from16 v8, p8

    move-wide/from16 v11, p9

    move-wide/from16 v13, p11

    move/from16 v15, p13

    invoke-direct/range {v0 .. v15}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;-><init>(JLjava/lang/String;FIJLjava/lang/String;JJJI)V

    .line 68
    return-void
.end method

.method public constructor <init>(JLjava/lang/String;FIJLjava/lang/String;JJJI)V
    .locals 9
    .param p1, "id"    # J
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "kcal"    # F
    .param p5, "mealType"    # I
    .param p6, "mealTime"    # J
    .param p8, "comment"    # Ljava/lang/String;
    .param p9, "mealPlanId"    # J
    .param p11, "createTime"    # J
    .param p13, "updateTime"    # J
    .param p15, "timeZone"    # I

    .prologue
    .line 29
    move-object v0, p0

    move-wide v1, p1

    move-wide/from16 v3, p11

    move-wide/from16 v5, p13

    move/from16 v7, p15

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;-><init>(JJJI)V

    .line 7
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->name:Ljava/lang/String;

    .line 8
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->kcal:F

    .line 9
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->mealType:I

    .line 10
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->mealTime:J

    .line 12
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->mMealPlanId:J

    move-object v0, p0

    move-object v1, p3

    move v2, p4

    move v3, p5

    move-wide v4, p6

    move-object/from16 v6, p8

    move-wide/from16 v7, p9

    .line 30
    invoke-direct/range {v0 .. v8}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->init(Ljava/lang/String;FIJLjava/lang/String;J)V

    .line 31
    return-void
.end method

.method public constructor <init>(JLjava/lang/String;FIJLjava/lang/String;JJJIZ)V
    .locals 1
    .param p1, "id"    # J
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "kcal"    # F
    .param p5, "mealType"    # I
    .param p6, "mealTime"    # J
    .param p8, "comment"    # Ljava/lang/String;
    .param p9, "mealPlanId"    # J
    .param p11, "createTime"    # J
    .param p13, "updateTime"    # J
    .param p15, "timeZone"    # I
    .param p16, "isFavourite"    # Z

    .prologue
    .line 52
    invoke-direct/range {p0 .. p15}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;-><init>(JLjava/lang/String;FIJLjava/lang/String;JJJI)V

    .line 53
    move/from16 v0, p16

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->mIsFavourite:Z

    .line 54
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;FIJLjava/lang/String;)V
    .locals 16
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "kcal"    # F
    .param p3, "mealType"    # I
    .param p4, "mealTime"    # J
    .param p6, "comment"    # Ljava/lang/String;

    .prologue
    .line 78
    const-wide/16 v1, -0x1

    const-wide/16 v9, -0x1

    const-wide/16 v11, -0x1

    const-wide/16 v13, -0x1

    const/4 v15, -0x1

    move-object/from16 v0, p0

    move-object/from16 v3, p1

    move/from16 v4, p2

    move/from16 v5, p3

    move-wide/from16 v6, p4

    move-object/from16 v8, p6

    invoke-direct/range {v0 .. v15}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;-><init>(JLjava/lang/String;FIJLjava/lang/String;JJJI)V

    .line 79
    return-void
.end method

.method private init(Ljava/lang/String;FIJLjava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "kcal"    # F
    .param p3, "mealType"    # I
    .param p4, "mealTime"    # J
    .param p6, "comment"    # Ljava/lang/String;

    .prologue
    .line 94
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->name:Ljava/lang/String;

    .line 95
    iput p2, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->kcal:F

    .line 96
    iput p3, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->mealType:I

    .line 97
    iput-wide p4, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->mealTime:J

    .line 98
    iput-object p6, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->comment:Ljava/lang/String;

    .line 99
    return-void
.end method

.method private init(Ljava/lang/String;FIJLjava/lang/String;J)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "kcal"    # F
    .param p3, "mealType"    # I
    .param p4, "mealTime"    # J
    .param p6, "comment"    # Ljava/lang/String;
    .param p7, "mealPlanId"    # J

    .prologue
    .line 89
    invoke-direct/range {p0 .. p6}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->init(Ljava/lang/String;FIJLjava/lang/String;)V

    .line 90
    iput-wide p7, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->mMealPlanId:J

    .line 91
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 232
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 237
    if-ne p0, p1, :cond_1

    .line 247
    :cond_0
    :goto_0
    return v1

    .line 241
    :cond_1
    instance-of v3, p1, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    if-eqz v3, :cond_3

    move-object v0, p1

    .line 242
    check-cast v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    .line 247
    .local v0, "checkMeal":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    iget-object v3, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->comment:Ljava/lang/String;

    iget-object v4, v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->comment:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->name:Ljava/lang/String;

    iget-object v4, v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget v3, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->mealType:I

    iget v4, v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->mealType:I

    if-ne v3, v4, :cond_2

    iget v3, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->kcal:F

    iget v4, v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->kcal:F

    cmpl-float v3, v3, v4

    if-nez v3, :cond_2

    iget-wide v3, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->mMealPlanId:J

    iget-wide v5, v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->mMealPlanId:J

    cmp-long v3, v3, v5

    if-nez v3, :cond_2

    iget-boolean v3, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->mIsFavourite:Z

    iget-boolean v4, v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->mIsFavourite:Z

    if-eq v3, v4, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .end local v0    # "checkMeal":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    :cond_3
    move v1, v2

    .line 244
    goto :goto_0
.end method

.method public getComment()Ljava/lang/String;
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->comment:Ljava/lang/String;

    return-object v0
.end method

.method public getKcal()F
    .locals 1

    .prologue
    .line 119
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->kcal:F

    return v0
.end method

.method public getMealPlanId()J
    .locals 2

    .prologue
    .line 175
    iget-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->mMealPlanId:J

    return-wide v0
.end method

.method public getMealTime()J
    .locals 2

    .prologue
    .line 147
    iget-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->mealTime:J

    return-wide v0
.end method

.method public getMealType()I
    .locals 1

    .prologue
    .line 133
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->mealType:I

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->name:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    .line 257
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->name:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    int-to-float v0, v0

    const/high16 v1, 0x41000000    # 8.0f

    iget v2, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->kcal:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iget v1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->mealType:I

    mul-int/lit8 v1, v1, 0x5

    int-to-float v1, v1

    add-float/2addr v0, v1

    const-wide/16 v1, 0x3

    iget-wide v3, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->mMealPlanId:J

    mul-long/2addr v1, v3

    long-to-float v1, v1

    add-float/2addr v0, v1

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->comment:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v0, v1

    float-to-int v1, v0

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->mIsFavourite:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFavourite()Z
    .locals 1

    .prologue
    .line 189
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->mIsFavourite:Z

    return v0
.end method

.method public isPlanned()Z
    .locals 4

    .prologue
    .line 182
    iget-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->mMealPlanId:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setComment(Ljava/lang/String;)V
    .locals 0
    .param p1, "comment"    # Ljava/lang/String;

    .prologue
    .line 168
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->comment:Ljava/lang/String;

    .line 169
    return-void
.end method

.method public setFavourite(Z)V
    .locals 0
    .param p1, "isFavourite"    # Z

    .prologue
    .line 196
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->mIsFavourite:Z

    .line 197
    return-void
.end method

.method public setKcal(F)V
    .locals 0
    .param p1, "kcal"    # F

    .prologue
    .line 126
    iput p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->kcal:F

    .line 127
    return-void
.end method

.method public setMealTime(J)V
    .locals 0
    .param p1, "mealTime"    # J

    .prologue
    .line 154
    iput-wide p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->mealTime:J

    .line 155
    return-void
.end method

.method public setMealType(I)V
    .locals 0
    .param p1, "mealType"    # I

    .prologue
    .line 140
    iput p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->mealType:I

    .line 141
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 112
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->name:Ljava/lang/String;

    .line 113
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "parcel"    # Landroid/os/Parcel;
    .param p2, "i"    # I

    .prologue
    .line 201
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getId()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 202
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->name:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 203
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->kcal:F

    float-to-double v0, v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 204
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->mealType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 205
    iget-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->mealTime:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 206
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->comment:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 207
    iget-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->mMealPlanId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 208
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getCreateTime()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 209
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getUpdateTime()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 210
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getTimeZone()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 211
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->mIsFavourite:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 212
    return-void

    .line 211
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
