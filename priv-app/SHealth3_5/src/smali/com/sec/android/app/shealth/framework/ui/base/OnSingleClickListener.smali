.class public abstract Lcom/sec/android/app/shealth/framework/ui/base/OnSingleClickListener;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final DEFAULT_TIMEOUT:I = 0x1f4

.field private static mLastTime:J

.field private static mLastViewRootHash:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 7

    invoke-virtual {p1}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    sget-wide v3, Lcom/sec/android/app/shealth/framework/ui/base/OnSingleClickListener;->mLastTime:J

    const-wide/16 v5, 0x1f4

    add-long/2addr v3, v5

    cmp-long v3, v3, v1

    if-ltz v3, :cond_0

    sget v3, Lcom/sec/android/app/shealth/framework/ui/base/OnSingleClickListener;->mLastViewRootHash:I

    if-eq v3, v0, :cond_1

    :cond_0
    sput-wide v1, Lcom/sec/android/app/shealth/framework/ui/base/OnSingleClickListener;->mLastTime:J

    sput v0, Lcom/sec/android/app/shealth/framework/ui/base/OnSingleClickListener;->mLastViewRootHash:I

    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/OnSingleClickListener;->onClickAction(Landroid/view/View;)V

    :cond_1
    return-void
.end method

.method public abstract onClickAction(Landroid/view/View;)V
.end method
