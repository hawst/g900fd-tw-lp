.class Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$2$1$1$1;
.super Ljava/lang/Object;
.source "PortionSizePopup.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$2$1$1;->onFocusChange(Landroid/view/View;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$3:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$2$1$1;

.field final synthetic val$arg0:Landroid/view/View;

.field final synthetic val$focused:Z


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$2$1$1;ZLandroid/view/View;)V
    .locals 0

    .prologue
    .line 206
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$2$1$1$1;->this$3:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$2$1$1;

    iput-boolean p2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$2$1$1$1;->val$focused:Z

    iput-object p3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$2$1$1$1;->val$arg0:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 211
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$2$1$1$1;->this$3:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$2$1$1;

    iget-object v2, v2, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$2$1$1;->this$2:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$2$1;

    iget-object v2, v2, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$2$1;->this$1:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$2;

    iget-object v2, v2, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$2;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mFragmentManager:Landroid/support/v4/app/FragmentManager;
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->access$300(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;)Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$2$1$1$1;->val$focused:Z

    if-nez v2, :cond_0

    .line 214
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$2$1$1$1;->this$3:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$2$1$1;

    iget-object v2, v2, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$2$1$1;->this$2:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$2$1;

    iget-object v2, v2, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$2$1;->this$1:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$2;

    iget-object v2, v2, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$2;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mFragmentManager:Landroid/support/v4/app/FragmentManager;
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->access$300(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;)Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "PORTION_SIZE_POPUP_TAG"

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/DialogFragment;

    .line 215
    .local v0, "dialog":Landroid/support/v4/app/DialogFragment;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 218
    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Dialog;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    .line 219
    .local v1, "focusedView":Landroid/view/View;
    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$2$1$1$1;->this$3:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$2$1$1;

    iget-object v2, v2, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$2$1$1;->val$view:Landroid/view/View;

    if-eq v1, v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$2$1$1$1;->val$arg0:Landroid/view/View;

    if-eq v1, v2, :cond_0

    .line 222
    invoke-virtual {v1}, Landroid/view/View;->clearFocus()V

    .line 223
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$2$1$1$1;->this$3:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$2$1$1;

    iget-object v2, v2, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$2$1$1;->val$view:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->requestFocus()Z

    .line 229
    .end local v0    # "dialog":Landroid/support/v4/app/DialogFragment;
    .end local v1    # "focusedView":Landroid/view/View;
    :cond_0
    return-void
.end method
