.class public Lcom/sec/android/app/shealth/backup/server/data/Response;
.super Ljava/lang/Object;
.source "Response.java"


# static fields
.field public static final RES_CODE_BAD_REQUEST:I = 0x190

.field public static final RES_CODE_INTERNAL_SERVER_ERROR:I = 0x1f4

.field public static final RES_CODE_INVALID_TOKEN:I = 0x191

.field public static final RES_CODE_SUCCESS:I = 0xc8

.field public static final RES_CODE_UNAVAILABLE_TRANSACTION:I = 0x258


# instance fields
.field public accountid:Ljava/lang/String;

.field public deviceid:Ljava/lang/String;

.field public hmethod:Ljava/lang/String;

.field public hrescode:Ljava/lang/String;

.field public hresdesc:Ljava/lang/String;

.field public htxid:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public checkIsResCodeOK()Z
    .locals 2

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/app/shealth/backup/server/data/Response;->hrescode:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/16 v0, 0xc8

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/backup/server/data/Response;->getResCode()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 26
    const/4 v0, 0x1

    .line 28
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getResCode()I
    .locals 4

    .prologue
    .line 33
    const/16 v1, 0x190

    .line 36
    .local v1, "httpCode":I
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/backup/server/data/Response;->hrescode:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 53
    :goto_0
    return v1

    .line 38
    :catch_0
    move-exception v0

    .line 40
    .local v0, "e":Ljava/lang/Exception;
    iget-object v2, p0, Lcom/sec/android/app/shealth/backup/server/data/Response;->hrescode:Ljava/lang/String;

    if-eqz v2, :cond_0

    const-string v2, "PHR_6001"

    iget-object v3, p0, Lcom/sec/android/app/shealth/backup/server/data/Response;->hrescode:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 42
    const/16 v1, 0x258

    goto :goto_0

    .line 44
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/backup/server/data/Response;->hrescode:Ljava/lang/String;

    if-eqz v2, :cond_1

    const-string v2, "PHR_4301"

    iget-object v3, p0, Lcom/sec/android/app/shealth/backup/server/data/Response;->hrescode:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 46
    const/16 v1, 0x191

    goto :goto_0

    .line 50
    :cond_1
    const/16 v1, 0x190

    goto :goto_0
.end method
