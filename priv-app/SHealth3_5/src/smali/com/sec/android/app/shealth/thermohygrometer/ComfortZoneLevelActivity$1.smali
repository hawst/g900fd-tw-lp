.class Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$1;
.super Ljava/lang/Object;
.source "ComfortZoneLevelActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;)V
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$1;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 81
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$1;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    iget-object v1, v1, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->temperatureFrom:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v0

    .line 82
    .local v0, "cursorPosition":I
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$1;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    iget-object v1, v1, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->temperatureFrom:Landroid/widget/EditText;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/high16 v3, 0x41a00000    # 20.0f

    const-string v4, "C"

    iget-object v5, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$1;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->degreeName:Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->access$000(Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/shealth/thermohygrometer/utils/TGHUnitUtils;->convertTemperatureData(FLjava/lang/String;Ljava/lang/String;)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 84
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$1;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    iget-object v1, v1, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->temperatureFrom:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->length()I

    move-result v1

    if-le v0, v1, :cond_0

    .line 85
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$1;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    iget-object v1, v1, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->temperatureFrom:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$1;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    iget-object v2, v2, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->temperatureFrom:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->length()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setSelection(I)V

    .line 89
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$1;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    iget-object v1, v1, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->temperatureTo:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v0

    .line 90
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$1;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    iget-object v1, v1, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->temperatureTo:Landroid/widget/EditText;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/high16 v3, 0x41d00000    # 26.0f

    const-string v4, "C"

    iget-object v5, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$1;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->degreeName:Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->access$000(Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/shealth/thermohygrometer/utils/TGHUnitUtils;->convertTemperatureData(FLjava/lang/String;Ljava/lang/String;)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 92
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$1;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    iget-object v1, v1, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->temperatureTo:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->length()I

    move-result v1

    if-le v0, v1, :cond_1

    .line 93
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$1;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    iget-object v1, v1, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->temperatureTo:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$1;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    iget-object v2, v2, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->temperatureTo:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->length()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setSelection(I)V

    .line 97
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$1;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    iget-object v1, v1, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->humidityFrom:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v0

    .line 98
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$1;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    iget-object v1, v1, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->humidityFrom:Landroid/widget/EditText;

    const-string v2, "30"

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 99
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$1;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    iget-object v1, v1, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->humidityFrom:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setSelection(I)V

    .line 100
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$1;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    iget-object v1, v1, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->humidityTo:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v0

    .line 101
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$1;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    iget-object v1, v1, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->humidityTo:Landroid/widget/EditText;

    const-string v2, "70"

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 102
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$1;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    iget-object v1, v1, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->humidityTo:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setSelection(I)V

    .line 104
    return-void

    .line 87
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$1;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    iget-object v1, v1, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->temperatureFrom:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setSelection(I)V

    goto/16 :goto_0

    .line 95
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$1;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    iget-object v1, v1, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->temperatureTo:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setSelection(I)V

    goto :goto_1
.end method
