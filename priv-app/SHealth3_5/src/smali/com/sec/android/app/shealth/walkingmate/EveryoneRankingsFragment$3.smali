.class Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$3;
.super Ljava/lang/Object;
.source "EveryoneRankingsFragment.java"

# interfaces
.implements Lcom/sec/android/service/health/samsungaccount/ISamsungUserTokenListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->sendRequestForUserToken()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)V
    .locals 0

    .prologue
    .line 247
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$3;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceived(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "userToken"    # Ljava/lang/String;
    .param p2, "userId"    # Ljava/lang/String;
    .param p3, "mcc"    # Ljava/lang/String;

    .prologue
    .line 251
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$3;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->TAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$300(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "sendRequestForUserToken - accessToken : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 252
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$3;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->TAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$300(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "sendRequestForUserToken - userId : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 253
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$3;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->TAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$300(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "sendRequestForUserToken - clientId : 1y90e30264"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 255
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 257
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "access_token"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 258
    const-string v2, "email_id"

    sget-object v3, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->mLoginId:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 259
    const-string v2, "client_id"

    const-string v3, "1y90e30264"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 260
    const-string/jumbo v2, "type"

    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$3;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$3;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->period_select_idx:I
    invoke-static {v4}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$1900(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)I

    move-result v4

    # invokes: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->getExtraStringPeriod(I)Ljava/lang/String;
    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$2000(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 262
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/android/sdk/health/sensor/utils/HealthServiceUtil;->getDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 263
    .local v0, "deviceId":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$3;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->TAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$300(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "sendRequestForUserToken - deviceId : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    const-string v2, "device_id"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 266
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/loaders/WalkingMateRankingHelper;->setSamsungAccountInfoIntent(Landroid/content/Intent;)V

    .line 267
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$3;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mIsWalkersUpdated:Z
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$2100(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 268
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$3;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mTopWalkersUpdater:Lcom/sec/android/app/shealth/walkingmate/loaders/TopWalkersUpdater;
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$2200(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)Lcom/sec/android/app/shealth/walkingmate/loaders/TopWalkersUpdater;

    move-result-object v2

    if-nez v2, :cond_0

    .line 269
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$3;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    const/4 v3, 0x1

    # invokes: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->requestTopWalkersListUpdate(Landroid/content/Intent;Z)V
    invoke-static {v2, v1, v3}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$2300(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;Landroid/content/Intent;Z)V

    .line 272
    :cond_0
    return-void
.end method

.method public setFailureMessage(Ljava/lang/String;)V
    .locals 3
    .param p1, "errMsg"    # Ljava/lang/String;

    .prologue
    .line 276
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$3;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$300(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "sendRequestForUserToken - errMsg: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->I(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$3;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/SamsungAccount;->requestCheckValication(Landroid/content/Context;)V

    .line 280
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$3;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->dismissLoadingDialog()V

    .line 282
    return-void
.end method

.method public setNetworkFailure()V
    .locals 1

    .prologue
    .line 286
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/loaders/WalkingMateRankingHelper;->isAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 287
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$3;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # invokes: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->loadOfflineData()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$2400(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)V

    .line 289
    :cond_0
    return-void
.end method
