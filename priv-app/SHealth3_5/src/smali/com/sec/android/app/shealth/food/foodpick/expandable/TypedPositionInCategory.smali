.class Lcom/sec/android/app/shealth/food/foodpick/expandable/TypedPositionInCategory;
.super Ljava/lang/Object;
.source "TypedPositionInCategory.java"


# instance fields
.field public categoryHolder:Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;

.field public itemType:Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$ItemType;

.field public positionInList:I


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;)V
    .locals 2
    .param p1, "categoryHolder"    # Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;

    .prologue
    .line 39
    sget-object v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$ItemType;->LIST_HEADER:Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$ItemType;

    const/4 v1, -0x1

    invoke-direct {p0, v0, p1, v1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/TypedPositionInCategory;-><init>(Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$ItemType;Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;I)V

    .line 40
    return-void
.end method

.method constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;I)V
    .locals 1
    .param p1, "categoryHolder"    # Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;
    .param p2, "position"    # I

    .prologue
    .line 32
    sget-object v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$ItemType;->LIST_ITEM:Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$ItemType;

    invoke-direct {p0, v0, p1, p2}, Lcom/sec/android/app/shealth/food/foodpick/expandable/TypedPositionInCategory;-><init>(Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$ItemType;Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;I)V

    .line 33
    return-void
.end method

.method constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$ItemType;Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;I)V
    .locals 0
    .param p1, "itemType"    # Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$ItemType;
    .param p2, "categoryHolder"    # Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;
    .param p3, "position"    # I

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/TypedPositionInCategory;->itemType:Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$ItemType;

    .line 44
    iput-object p2, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/TypedPositionInCategory;->categoryHolder:Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;

    .line 45
    iput p3, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/TypedPositionInCategory;->positionInList:I

    .line 46
    return-void
.end method
