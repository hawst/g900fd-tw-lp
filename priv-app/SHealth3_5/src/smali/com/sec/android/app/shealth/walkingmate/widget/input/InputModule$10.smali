.class Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$10;
.super Ljava/lang/Object;
.source "InputModule.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$10$TimerTaskOne;
    }
.end annotation


# static fields
.field public static final INCREASE_DELAY:I = 0x32


# instance fields
.field public final LONGPRESS_TIMEOUT:I

.field private mCurrentlyPressedView:Landroid/view/View;

.field private mHandler:Landroid/os/Handler;

.field private volatile mIsQuickIncDecBegins:Z

.field private mTimer:Ljava/util/Timer;

.field final synthetic this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)V
    .locals 1

    .prologue
    .line 690
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$10;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 693
    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$10;->LONGPRESS_TIMEOUT:I

    .line 697
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$10;->mHandler:Landroid/os/Handler;

    .line 768
    return-void
.end method

.method static synthetic access$2302(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$10;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$10;
    .param p1, "x1"    # Z

    .prologue
    .line 690
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$10;->mIsQuickIncDecBegins:Z

    return p1
.end method

.method static synthetic access$2400(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$10;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$10;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 690
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$10;->valueIncOrDec(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$2500(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$10;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$10;

    .prologue
    .line 690
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$10;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private valueIncOrDec(Landroid/view/View;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 756
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$10;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mIncButton:Landroid/widget/ImageButton;
    invoke-static {v3}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$1900(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)Landroid/widget/ImageButton;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/ImageButton;->getId()I

    move-result v3

    if-ne v2, v3, :cond_2

    .line 757
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$10;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$10;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mCurrentScaleValue:I
    invoke-static {v3}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$1500(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$10;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mMoveDistance:I
    invoke-static {v4}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$2100(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)I

    move-result v4

    add-int/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$10;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # invokes: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->isDecimalNumber()Z
    invoke-static {v4}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$200(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)Z

    move-result v4

    if-eqz v4, :cond_1

    :goto_0
    # invokes: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->setNewValue(IZ)V
    invoke-static {v2, v3, v0}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$300(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;IZ)V

    .line 765
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$10;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$10;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mCurrentScaleValue:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$1500(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)I

    move-result v1

    # setter for: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mCurrentValue:I
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$1402(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;I)I

    .line 766
    return-void

    :cond_1
    move v0, v1

    .line 757
    goto :goto_0

    .line 758
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$10;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mDecButton:Landroid/widget/ImageButton;
    invoke-static {v3}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$1800(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)Landroid/widget/ImageButton;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/ImageButton;->getId()I

    move-result v3

    if-ne v2, v3, :cond_0

    .line 759
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$10;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mMinInputRange:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$800(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)I

    move-result v2

    const/16 v3, 0x2710

    if-ne v2, v3, :cond_4

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$10;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mCurrentScaleValue:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$1500(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)I

    move-result v2

    const/16 v3, 0x2af8

    if-gt v2, v3, :cond_4

    .line 760
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$10;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$10;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mMinInputRange:I
    invoke-static {v3}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$800(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$10;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # invokes: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->isDecimalNumber()Z
    invoke-static {v4}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$200(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)Z

    move-result v4

    if-eqz v4, :cond_3

    :goto_2
    # invokes: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->setNewValue(IZ)V
    invoke-static {v2, v3, v0}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$300(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;IZ)V

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2

    .line 762
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$10;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$10;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mCurrentScaleValue:I
    invoke-static {v3}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$1500(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$10;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mMoveDistance:I
    invoke-static {v4}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$2100(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)I

    move-result v4

    sub-int/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$10;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # invokes: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->isDecimalNumber()Z
    invoke-static {v4}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$200(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)Z

    move-result v4

    if-eqz v4, :cond_5

    :goto_3
    # invokes: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->setNewValue(IZ)V
    invoke-static {v2, v3, v0}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$300(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;IZ)V

    goto :goto_1

    :cond_5
    move v0, v1

    goto :goto_3
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ClickableViewAccessibility"
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v0, 0x0

    .line 703
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$10;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    iget-object v1, v1, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 704
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$10;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->showKeyboard()V

    .line 705
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$10;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->moveFocusFromValueEditText()V

    .line 706
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 752
    :cond_1
    :goto_0
    return v0

    .line 708
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$10;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    invoke-virtual {v1, v7}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->onTouchModeEditTextColor(Z)V

    .line 709
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$10;->mCurrentlyPressedView:Landroid/view/View;

    if-eqz v1, :cond_2

    .line 710
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$10;->mCurrentlyPressedView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->isPressed()Z

    move-result v1

    if-eqz v1, :cond_2

    move v0, v7

    .line 711
    goto :goto_0

    .line 714
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->isPressed()Z

    move-result v1

    if-nez v1, :cond_3

    .line 715
    invoke-virtual {p1, v7}, Landroid/view/View;->setPressed(Z)V

    .line 716
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$10;->mIsQuickIncDecBegins:Z

    .line 717
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$10;->mTimer:Ljava/util/Timer;

    new-instance v1, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$10$TimerTaskOne;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$10$TimerTaskOne;-><init>(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$10;Landroid/view/View;)V

    iget v2, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$10;->LONGPRESS_TIMEOUT:I

    int-to-long v2, v2

    const-wide/16 v4, 0x32

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 718
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$10;->mCurrentlyPressedView:Landroid/view/View;

    move v0, v7

    .line 719
    goto :goto_0

    .line 721
    :cond_3
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$10;->mCurrentlyPressedView:Landroid/view/View;

    goto :goto_0

    .line 725
    :pswitch_1
    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    .line 726
    .local v6, "rect":Landroid/graphics/Rect;
    invoke-virtual {p1, v6}, Landroid/view/View;->getLocalVisibleRect(Landroid/graphics/Rect;)Z

    .line 727
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v6, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_0

    .line 734
    .end local v6    # "rect":Landroid/graphics/Rect;
    :pswitch_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$10;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->onTouchModeEditTextColor(Z)V

    .line 735
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$10;->mCurrentlyPressedView:Landroid/view/View;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$10;->mCurrentlyPressedView:Landroid/view/View;

    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 739
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$10;->mTimer:Ljava/util/Timer;

    if-eqz v1, :cond_5

    .line 740
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$10;->mTimer:Ljava/util/Timer;

    invoke-virtual {v1}, Ljava/util/Timer;->cancel()V

    .line 742
    :cond_5
    invoke-virtual {p1}, Landroid/view/View;->isPressed()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 743
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$10;->mIsQuickIncDecBegins:Z

    if-nez v1, :cond_6

    .line 744
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$10;->valueIncOrDec(Landroid/view/View;)V

    .line 747
    :cond_6
    invoke-virtual {p1, v0}, Landroid/view/View;->setPressed(Z)V

    .line 748
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$10;->mIsQuickIncDecBegins:Z

    goto :goto_0

    .line 706
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
