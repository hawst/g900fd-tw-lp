.class Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$7;
.super Ljava/lang/Object;
.source "ExerciseProMapActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->initLayout(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)V
    .locals 0

    .prologue
    .line 451
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 454
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    iget-boolean v1, v1, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->isInCenter:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mLastLocation:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->access$1000(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 456
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMap:Lcom/google/android/gms/maps/GoogleMap;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->access$1100(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)Lcom/google/android/gms/maps/GoogleMap;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/maps/GoogleMap;->getCameraPosition()Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/maps/model/CameraPosition;->builder(Lcom/google/android/gms/maps/model/CameraPosition;)Lcom/google/android/gms/maps/model/CameraPosition$Builder;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/maps/model/LatLng;

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mLastLocation:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->access$1000(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    move-result-object v3

    iget-wide v3, v3, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->latitude:D

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mLastLocation:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;
    invoke-static {v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->access$1000(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    move-result-object v5

    iget-wide v5, v5, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->longitude:D

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    invoke-virtual {v1, v2}, Lcom/google/android/gms/maps/model/CameraPosition$Builder;->target(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/CameraPosition$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/CameraPosition$Builder;->build()Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object v0

    .line 457
    .local v0, "pos":Lcom/google/android/gms/maps/model/CameraPosition;
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    const/4 v2, 0x1

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->animationToCenterInProgress:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->access$102(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;Z)Z

    .line 458
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMap:Lcom/google/android/gms/maps/GoogleMap;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->access$1100(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)Lcom/google/android/gms/maps/GoogleMap;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/gms/maps/CameraUpdateFactory;->newCameraPosition(Lcom/google/android/gms/maps/model/CameraPosition;)Lcom/google/android/gms/maps/CameraUpdate;

    move-result-object v2

    const/16 v3, 0x438

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    iget-object v4, v4, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->animationCallback:Lcom/google/android/gms/maps/GoogleMap$CancelableCallback;

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/gms/maps/GoogleMap;->animateCamera(Lcom/google/android/gms/maps/CameraUpdate;ILcom/google/android/gms/maps/GoogleMap$CancelableCallback;)V

    .line 460
    .end local v0    # "pos":Lcom/google/android/gms/maps/model/CameraPosition;
    :cond_0
    return-void
.end method
