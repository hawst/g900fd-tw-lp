.class public interface abstract Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;
.super Ljava/lang/Object;
.source "MealItemDao.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime",
        "<",
        "Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;",
        ">;"
    }
.end annotation


# virtual methods
.method public abstract getMealItemsListByMealId(J)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;",
            ">;"
        }
    .end annotation
.end method
