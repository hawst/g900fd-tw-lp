.class public final enum Lcom/sec/android/app/shealth/logutils/graph/GraphParams;
.super Ljava/lang/Enum;
.source "GraphParams.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/logutils/graph/GraphParams;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/logutils/graph/GraphParams;

.field public static final enum DAY:Lcom/sec/android/app/shealth/logutils/graph/GraphParams;

.field public static final enum HOUR:Lcom/sec/android/app/shealth/logutils/graph/GraphParams;

.field public static final enum MONTH:Lcom/sec/android/app/shealth/logutils/graph/GraphParams;


# instance fields
.field public final bottomSeparatorDateFormatContents:[Ljava/lang/String;

.field public final dateFormatPattern:Ljava/lang/String;

.field private final mCalendarPeriodType:I

.field private final mHorizontalMarksCount:I

.field private final mHorizontalMarksInterval:I

.field private final mMaxZoomFactor:F

.field private final mMinZoomFactor:F

.field public final originPeriodH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

.field public final periodType:Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;

.field public final timeDepthLevel:I

.field public final xAxisLevelNameStringId:I


# direct methods
.method static constructor <clinit>()V
    .locals 14

    .prologue
    .line 30
    new-instance v0, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;

    const-string v1, "HOUR"

    const/4 v2, 0x0

    sget-object v3, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v4, Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;->HOUR:Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;

    const/4 v5, 0x1

    sget v6, Lcom/sec/android/app/shealth/logutils/R$string;->hour:I

    const/16 v7, 0xc

    const/4 v8, 0x1

    const/high16 v9, 0x40800000    # 4.0f

    const/high16 v10, 0x40000000    # 2.0f

    const/16 v11, 0xa

    sget-object v12, Lcom/sec/android/app/shealth/logutils/graph/GraphConstants;->HOUR_TAB_BOTTOM_SEPARATOR_DATE_FORMAT_CONTENTS:[Ljava/lang/String;

    const-string v13, "HH"

    invoke-direct/range {v0 .. v13}, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;-><init>(Ljava/lang/String;ILcom/sec/android/app/shealth/framework/ui/graph/PeriodH;Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;IIIIFFI[Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->HOUR:Lcom/sec/android/app/shealth/logutils/graph/GraphParams;

    .line 36
    new-instance v0, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;

    const-string v1, "DAY"

    const/4 v2, 0x1

    sget-object v3, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v4, Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;->DAY:Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;

    const/4 v5, 0x2

    sget v6, Lcom/sec/android/app/shealth/logutils/R$string;->day:I

    const/4 v7, 0x7

    const/4 v8, 0x1

    const/high16 v9, 0x3f800000    # 1.0f

    const v10, 0x408db6db

    const/4 v11, 0x5

    sget-object v12, Lcom/sec/android/app/shealth/logutils/graph/GraphConstants;->DAY_TAB_BOTTOM_SEPARATOR_DATE_FORMAT_CONTENTS:[Ljava/lang/String;

    const-string v13, "dd"

    invoke-direct/range {v0 .. v13}, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;-><init>(Ljava/lang/String;ILcom/sec/android/app/shealth/framework/ui/graph/PeriodH;Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;IIIIFFI[Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->DAY:Lcom/sec/android/app/shealth/logutils/graph/GraphParams;

    .line 42
    new-instance v0, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;

    const-string v1, "MONTH"

    const/4 v2, 0x2

    sget-object v3, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v4, Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;->MONTH:Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;

    const/4 v5, 0x5

    sget v6, Lcom/sec/android/app/shealth/logutils/R$string;->month:I

    const/16 v7, 0xc

    const/4 v8, 0x1

    const/high16 v9, 0x40000000    # 2.0f

    const/high16 v10, 0x40000000    # 2.0f

    const/4 v11, 0x2

    sget-object v12, Lcom/sec/android/app/shealth/logutils/graph/GraphConstants;->MONTH_TAB_BOTTOM_SEPARATOR_DATE_FORMAT_CONTENTS:[Ljava/lang/String;

    const-string v13, "MM"

    invoke-direct/range {v0 .. v13}, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;-><init>(Ljava/lang/String;ILcom/sec/android/app/shealth/framework/ui/graph/PeriodH;Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;IIIIFFI[Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->MONTH:Lcom/sec/android/app/shealth/logutils/graph/GraphParams;

    .line 29
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/android/app/shealth/logutils/graph/GraphParams;

    const/4 v1, 0x0

    sget-object v2, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->HOUR:Lcom/sec/android/app/shealth/logutils/graph/GraphParams;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->DAY:Lcom/sec/android/app/shealth/logutils/graph/GraphParams;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->MONTH:Lcom/sec/android/app/shealth/logutils/graph/GraphParams;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->$VALUES:[Lcom/sec/android/app/shealth/logutils/graph/GraphParams;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/sec/android/app/shealth/framework/ui/graph/PeriodH;Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;IIIIFFI[Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p3, "originPeriodH"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;
    .param p4, "periodType"    # Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;
    .param p5, "timeDepthLevel"    # I
    .param p6, "xAxisLevelNameStringId"    # I
    .param p7, "horizontalMarksCount"    # I
    .param p8, "horizontalMarksInterval"    # I
    .param p9, "minZoomFactor"    # F
    .param p10, "maxZoomFactor"    # F
    .param p11, "calendarPeriodType"    # I
    .param p12, "bottomSeparatorDateFormatContents"    # [Ljava/lang/String;
    .param p13, "dateFormatPattern"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;",
            "Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;",
            "IIIIFFI[",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 88
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 89
    iput-object p3, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->originPeriodH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .line 90
    iput-object p4, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->periodType:Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;

    .line 91
    iput p5, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->timeDepthLevel:I

    .line 92
    iput p6, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->xAxisLevelNameStringId:I

    .line 93
    iput-object p12, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->bottomSeparatorDateFormatContents:[Ljava/lang/String;

    .line 94
    iput-object p13, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->dateFormatPattern:Ljava/lang/String;

    .line 95
    iput p7, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->mHorizontalMarksCount:I

    .line 96
    iput p8, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->mHorizontalMarksInterval:I

    .line 97
    iput p9, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->mMinZoomFactor:F

    .line 98
    iput p10, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->mMaxZoomFactor:F

    .line 99
    iput p11, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->mCalendarPeriodType:I

    .line 100
    return-void
.end method

.method public static getGraphParams(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Lcom/sec/android/app/shealth/logutils/graph/GraphParams;
    .locals 8
    .param p0, "originPeriodH"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 103
    invoke-static {}, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->values()[Lcom/sec/android/app/shealth/logutils/graph/GraphParams;

    move-result-object v0

    .local v0, "arr$":[Lcom/sec/android/app/shealth/logutils/graph/GraphParams;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    .line 104
    .local v1, "graphParams":Lcom/sec/android/app/shealth/logutils/graph/GraphParams;
    iget-object v5, v1, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->originPeriodH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v5, p0, :cond_0

    .line 105
    return-object v1

    .line 103
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 108
    .end local v1    # "graphParams":Lcom/sec/android/app/shealth/logutils/graph/GraphParams;
    :cond_1
    const-string v4, ""

    .line 109
    .local v4, "permittedPeriodHValueNames":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->values()[Lcom/sec/android/app/shealth/logutils/graph/GraphParams;

    move-result-object v0

    array-length v3, v0

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v3, :cond_2

    aget-object v1, v0, v2

    .line 110
    .restart local v1    # "graphParams":Lcom/sec/android/app/shealth/logutils/graph/GraphParams;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v1, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->originPeriodH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->name()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "; "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 109
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 112
    .end local v1    # "graphParams":Lcom/sec/android/app/shealth/logutils/graph/GraphParams;
    :cond_2
    new-instance v5, Ljava/lang/IllegalStateException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Illegal originPeriodH value (only "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " are permitted), current: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->name()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v5
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/logutils/graph/GraphParams;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 29
    const-class v0, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/logutils/graph/GraphParams;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->$VALUES:[Lcom/sec/android/app/shealth/logutils/graph/GraphParams;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/logutils/graph/GraphParams;

    return-object v0
.end method


# virtual methods
.method public getCalendarPeriodType()I
    .locals 1

    .prologue
    .line 123
    iget v0, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->mCalendarPeriodType:I

    return v0
.end method

.method public getHorizontalMarksCount()I
    .locals 1

    .prologue
    .line 132
    iget v0, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->mHorizontalMarksCount:I

    return v0
.end method

.method public getHorizontalMarksInterval()I
    .locals 1

    .prologue
    .line 141
    iget v0, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->mHorizontalMarksInterval:I

    return v0
.end method

.method public getMaxZoomFactor()F
    .locals 1

    .prologue
    .line 159
    iget v0, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->mMaxZoomFactor:F

    return v0
.end method

.method public getMinZoomFactor()F
    .locals 1

    .prologue
    .line 150
    iget v0, p0, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->mMinZoomFactor:F

    return v0
.end method
