.class Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView$2;
.super Lcom/sec/android/app/shealth/common/commonui/summary/StubAnimatorListener;
.source "TGHHeaderView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->getImageAnimator()Landroid/animation/ObjectAnimator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;)V
    .locals 0

    .prologue
    .line 205
    iput-object p1, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView$2;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;

    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/commonui/summary/StubAnimatorListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 209
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView$2;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;

    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->mBalanceIcon:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->access$100(Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;)Landroid/widget/ImageView;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleX(F)V

    .line 210
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView$2;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;

    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->mBalanceIcon:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->access$100(Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView$2;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;

    # invokes: Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->getBalanceIconResId()I
    invoke-static {v1}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->access$200(Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 211
    return-void
.end method
