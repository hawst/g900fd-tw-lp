.class public final enum Lcom/sec/android/app/shealth/stress/data/StressStateType;
.super Ljava/lang/Enum;
.source "StressStateType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/stress/data/StressStateType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/stress/data/StressStateType;

.field public static final enum AVERAGE:Lcom/sec/android/app/shealth/stress/data/StressStateType;

.field public static final enum HIGH:Lcom/sec/android/app/shealth/stress/data/StressStateType;

.field public static final enum INVALID:Lcom/sec/android/app/shealth/stress/data/StressStateType;

.field public static final enum LOW:Lcom/sec/android/app/shealth/stress/data/StressStateType;

.field public static final enum VERY_HIGH:Lcom/sec/android/app/shealth/stress/data/StressStateType;

.field public static final enum VERY_LOW:Lcom/sec/android/app/shealth/stress/data/StressStateType;


# instance fields
.field private colorResource:I

.field private score:D

.field private state:I

.field private stringResource:I


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 6
    new-instance v0, Lcom/sec/android/app/shealth/stress/data/StressStateType;

    const-string v1, "INVALID"

    const v4, 0x7f090c3e

    const v5, 0x106000d

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/stress/data/StressStateType;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lcom/sec/android/app/shealth/stress/data/StressStateType;->INVALID:Lcom/sec/android/app/shealth/stress/data/StressStateType;

    .line 7
    new-instance v3, Lcom/sec/android/app/shealth/stress/data/StressStateType;

    const-string v4, "VERY_LOW"

    const v7, 0x7f090c3f

    const v8, 0x7f07025d

    move v5, v9

    move v6, v9

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/shealth/stress/data/StressStateType;-><init>(Ljava/lang/String;IIII)V

    sput-object v3, Lcom/sec/android/app/shealth/stress/data/StressStateType;->VERY_LOW:Lcom/sec/android/app/shealth/stress/data/StressStateType;

    .line 8
    new-instance v3, Lcom/sec/android/app/shealth/stress/data/StressStateType;

    const-string v4, "LOW"

    const v7, 0x7f090c3c

    const v8, 0x7f07025e

    move v5, v10

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/shealth/stress/data/StressStateType;-><init>(Ljava/lang/String;IIII)V

    sput-object v3, Lcom/sec/android/app/shealth/stress/data/StressStateType;->LOW:Lcom/sec/android/app/shealth/stress/data/StressStateType;

    .line 9
    new-instance v3, Lcom/sec/android/app/shealth/stress/data/StressStateType;

    const-string v4, "AVERAGE"

    const v7, 0x7f090c40

    const v8, 0x7f07025f

    move v5, v11

    move v6, v11

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/shealth/stress/data/StressStateType;-><init>(Ljava/lang/String;IIII)V

    sput-object v3, Lcom/sec/android/app/shealth/stress/data/StressStateType;->AVERAGE:Lcom/sec/android/app/shealth/stress/data/StressStateType;

    .line 10
    new-instance v3, Lcom/sec/android/app/shealth/stress/data/StressStateType;

    const-string v4, "HIGH"

    const v7, 0x7f090c3d

    const v8, 0x7f070260

    move v5, v12

    move v6, v12

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/shealth/stress/data/StressStateType;-><init>(Ljava/lang/String;IIII)V

    sput-object v3, Lcom/sec/android/app/shealth/stress/data/StressStateType;->HIGH:Lcom/sec/android/app/shealth/stress/data/StressStateType;

    .line 11
    new-instance v3, Lcom/sec/android/app/shealth/stress/data/StressStateType;

    const-string v4, "VERY_HIGH"

    const/4 v5, 0x5

    const/4 v6, 0x5

    const v7, 0x7f090c41

    const v8, 0x7f070261

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/shealth/stress/data/StressStateType;-><init>(Ljava/lang/String;IIII)V

    sput-object v3, Lcom/sec/android/app/shealth/stress/data/StressStateType;->VERY_HIGH:Lcom/sec/android/app/shealth/stress/data/StressStateType;

    .line 5
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/sec/android/app/shealth/stress/data/StressStateType;

    sget-object v1, Lcom/sec/android/app/shealth/stress/data/StressStateType;->INVALID:Lcom/sec/android/app/shealth/stress/data/StressStateType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/shealth/stress/data/StressStateType;->VERY_LOW:Lcom/sec/android/app/shealth/stress/data/StressStateType;

    aput-object v1, v0, v9

    sget-object v1, Lcom/sec/android/app/shealth/stress/data/StressStateType;->LOW:Lcom/sec/android/app/shealth/stress/data/StressStateType;

    aput-object v1, v0, v10

    sget-object v1, Lcom/sec/android/app/shealth/stress/data/StressStateType;->AVERAGE:Lcom/sec/android/app/shealth/stress/data/StressStateType;

    aput-object v1, v0, v11

    sget-object v1, Lcom/sec/android/app/shealth/stress/data/StressStateType;->HIGH:Lcom/sec/android/app/shealth/stress/data/StressStateType;

    aput-object v1, v0, v12

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/shealth/stress/data/StressStateType;->VERY_HIGH:Lcom/sec/android/app/shealth/stress/data/StressStateType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/shealth/stress/data/StressStateType;->$VALUES:[Lcom/sec/android/app/shealth/stress/data/StressStateType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIII)V
    .locals 0
    .param p3, "state"    # I
    .param p4, "stringResource"    # I
    .param p5, "colorResource"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III)V"
        }
    .end annotation

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 19
    iput p3, p0, Lcom/sec/android/app/shealth/stress/data/StressStateType;->state:I

    .line 20
    iput p4, p0, Lcom/sec/android/app/shealth/stress/data/StressStateType;->stringResource:I

    .line 21
    iput p5, p0, Lcom/sec/android/app/shealth/stress/data/StressStateType;->colorResource:I

    .line 22
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/stress/data/StressStateType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 5
    const-class v0, Lcom/sec/android/app/shealth/stress/data/StressStateType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/stress/data/StressStateType;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/stress/data/StressStateType;
    .locals 1

    .prologue
    .line 5
    sget-object v0, Lcom/sec/android/app/shealth/stress/data/StressStateType;->$VALUES:[Lcom/sec/android/app/shealth/stress/data/StressStateType;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/stress/data/StressStateType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/stress/data/StressStateType;

    return-object v0
.end method


# virtual methods
.method public getScore()D
    .locals 2

    .prologue
    .line 38
    iget-wide v0, p0, Lcom/sec/android/app/shealth/stress/data/StressStateType;->score:D

    return-wide v0
.end method

.method public getState()I
    .locals 1

    .prologue
    .line 25
    iget v0, p0, Lcom/sec/android/app/shealth/stress/data/StressStateType;->state:I

    return v0
.end method

.method public getStringResource()I
    .locals 1

    .prologue
    .line 29
    iget v0, p0, Lcom/sec/android/app/shealth/stress/data/StressStateType;->stringResource:I

    return v0
.end method

.method public setScore(D)V
    .locals 0
    .param p1, "score"    # D

    .prologue
    .line 34
    iput-wide p1, p0, Lcom/sec/android/app/shealth/stress/data/StressStateType;->score:D

    .line 35
    return-void
.end method
