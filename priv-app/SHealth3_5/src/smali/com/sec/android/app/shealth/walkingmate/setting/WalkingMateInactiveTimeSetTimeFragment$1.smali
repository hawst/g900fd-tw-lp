.class Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment$1;
.super Ljava/lang/Object;
.source "WalkingMateInactiveTimeSetTimeFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->getFromButtonController()Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;)V
    .locals 0

    .prologue
    .line 71
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDialogButtonClick(Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;)V
    .locals 9
    .param p1, "dialogButtonType"    # Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    .prologue
    .line 74
    sget-object v4, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->POSITIVE:Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    if-ne p1, v4, :cond_0

    .line 75
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->mFromPicker:Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;
    invoke-static {v4}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->access$000(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;)Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;->getDialog()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    .line 77
    .local v0, "fromdialog":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getView()Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 79
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->mFromPicker:Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;
    invoke-static {v4}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->access$000(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;)Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;->getDialog()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getView()Landroid/view/View;

    move-result-object v4

    const v5, 0x7f080a8c

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TimePicker;

    .line 80
    .local v1, "localInstance":Landroid/widget/TimePicker;
    invoke-virtual {v1}, Landroid/widget/TimePicker;->clearFocus()V

    .line 81
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;

    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->mCalendar:Ljava/util/Calendar;
    invoke-static {v5}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->access$100(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;)Ljava/util/Calendar;

    move-result-object v5

    invoke-virtual {v1}, Landroid/widget/TimePicker;->getCurrentHour()Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-virtual {v1}, Landroid/widget/TimePicker;->getCurrentMinute()Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->mFromPicker:Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;
    invoke-static {v8}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->access$000(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;)Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;

    move-result-object v8

    # invokes: Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->setTime(Ljava/util/Calendar;IILcom/sec/android/app/shealth/framework/ui/common/TimePicker;)V
    invoke-static {v4, v5, v6, v7, v8}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->access$200(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;Ljava/util/Calendar;IILcom/sec/android/app/shealth/framework/ui/common/TimePicker;)V

    .line 82
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->timeFormatter:Ljava/text/SimpleDateFormat;
    invoke-static {v4}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->access$400(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;)Ljava/text/SimpleDateFormat;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;

    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->mFromPicker:Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;
    invoke-static {v6}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->access$000(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;)Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;

    move-result-object v6

    # invokes: Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->getMeasureTimeInMillis(Lcom/sec/android/app/shealth/framework/ui/common/TimePicker;)J
    invoke-static {v5, v6}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->access$300(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;Lcom/sec/android/app/shealth/framework/ui/common/TimePicker;)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 84
    .local v2, "selectedTime":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;

    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->mFromPicker:Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;
    invoke-static {v5}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->access$000(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;)Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;->getCurrentHour()I

    move-result v5

    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->mFromPicker:Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;
    invoke-static {v6}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->access$000(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;)Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;->getCurrentMinute()I

    move-result v6

    # invokes: Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->validateFromTime(II)Z
    invoke-static {v4, v5, v6}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->access$500(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;II)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 85
    invoke-static {v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->setInactiveTimeTrackingAvailableTimeFrom(Ljava/lang/String;)V

    .line 93
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-static {v4}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 94
    :goto_1
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->mFromButton:Landroid/widget/Button;
    invoke-static {v4}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->access$700(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;)Landroid/widget/Button;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 97
    .end local v0    # "fromdialog":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .end local v1    # "localInstance":Landroid/widget/TimePicker;
    .end local v2    # "selectedTime":Ljava/lang/String;
    :cond_0
    return-void

    .line 87
    .restart local v0    # "fromdialog":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .restart local v1    # "localInstance":Landroid/widget/TimePicker;
    .restart local v2    # "selectedTime":Ljava/lang/String;
    :cond_1
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getInactiveTimeTrackingAvailableTimeFrom()Ljava/lang/String;

    move-result-object v2

    .line 88
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;

    # invokes: Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->getUtilDate(Ljava/lang/String;)Ljava/util/Date;
    invoke-static {v4, v2}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->access$600(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;Ljava/lang/String;)Ljava/util/Date;

    move-result-object v3

    .line 89
    .local v3, "tempDate":Ljava/util/Date;
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->mCalendar:Ljava/util/Calendar;
    invoke-static {v4}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->access$100(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;)Ljava/util/Calendar;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 90
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->mFromPicker:Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;
    invoke-static {v4}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->access$000(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;)Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->mCalendar:Ljava/util/Calendar;
    invoke-static {v5}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->access$100(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;)Ljava/util/Calendar;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;->setMeasureTime(J)Z

    goto :goto_0

    .line 93
    .end local v3    # "tempDate":Ljava/util/Date;
    :cond_2
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->get12Hrformat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method
