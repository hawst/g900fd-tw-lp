.class public Lcom/sec/android/app/shealth/widget/WalkMateAppWidget;
.super Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;
.source "WalkMateAppWidget.java"


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;-><init>()V

    .line 54
    const-class v0, Lcom/sec/android/app/shealth/widget/WalkMateAppWidget;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Constructor called"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    sget-object v0, Lcom/sec/android/app/shealth/widget/WalkMateAppWidget;->sWidgets:[Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;

    const/4 v1, 0x1

    aput-object p0, v0, v1

    .line 56
    return-void
.end method


# virtual methods
.method public onDisabled(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 60
    sget-object v0, Lcom/sec/android/app/shealth/widget/WalkMateAppWidget;->sWidgets:[Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;

    const/4 v1, 0x1

    const/4 v2, 0x0

    aput-object v2, v0, v1

    .line 61
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;->onDisabled(Landroid/content/Context;)V

    .line 62
    return-void
.end method

.method protected updateWidget(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
    .locals 27
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appWidgetManager"    # Landroid/appwidget/AppWidgetManager;
    .param p3, "appWidgetIds"    # [I

    .prologue
    .line 66
    invoke-super/range {p0 .. p3}, Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;->onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    .line 67
    const-string v21, "AbstractWalkMateAppWidget"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v23, "updateWidget() - appWidgetIds size = "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, p3

    array-length v0, v0

    move/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getHRAvailabliltyFalseIfUnknown(Landroid/content/Context;)Z

    move-result v21

    if-eqz v21, :cond_2

    sget-object v21, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->HeartRate:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->checkFeature(Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;)Lcom/sec/android/app/shealth/common/config/FeatureTable$Support;

    move-result-object v21

    sget-object v22, Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;->NOT_SUPPORTED:Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    if-eq v0, v1, :cond_2

    const/4 v6, 0x1

    .line 72
    .local v6, "hrAvailability":Z
    :goto_0
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/service/health/keyManager/KeyManager;->isContentProviderAccessible()Z

    move-result v21

    if-nez v21, :cond_3

    const/4 v10, 0x1

    .line 73
    .local v10, "isDBPasswordLocked":Z
    :goto_1
    const-string v21, "AbstractWalkMateAppWidget"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "isDBPasswordLocked : "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 74
    invoke-virtual/range {p0 .. p1}, Lcom/sec/android/app/shealth/widget/WalkMateAppWidget;->getCurrentMode(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    .line 75
    .local v5, "currentMode":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isInitializationNeeded(Landroid/content/Context;)Z

    move-result v21

    if-nez v21, :cond_0

    if-eqz v10, :cond_4

    .line 76
    :cond_0
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_2
    move-object/from16 v0, p3

    array-length v0, v0

    move/from16 v21, v0

    move/from16 v0, v21

    if-ge v7, v0, :cond_1d

    .line 77
    new-instance v19, Landroid/widget/RemoteViews;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v21

    const v22, 0x7f030282

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 78
    .local v19, "views":Landroid/widget/RemoteViews;
    if-eqz v6, :cond_1

    .line 79
    const v21, 0x7f080b62

    const/16 v22, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 81
    :cond_1
    new-instance v8, Landroid/content/Intent;

    const-class v21, Lcom/sec/android/app/shealth/receiver/LaunchWidgetReceiver;

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-direct {v8, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 82
    .local v8, "intent":Landroid/content/Intent;
    const-string v21, "com.sec.shealth.broadcast.LAUNCH_APP_WIDGET_ACTIVITY"

    move-object/from16 v0, v21

    invoke-virtual {v8, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 83
    const-string/jumbo v21, "widgetActivityAction"

    const-string v22, "com.sec.shealth.action.PEDOMETER"

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 84
    const-string/jumbo v21, "widgetActivityPackage"

    const-string v22, "com.sec.android.app.shealth"

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 85
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v21

    const/16 v22, 0x1ca8

    const/high16 v23, 0x8000000

    move-object/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-static {v0, v1, v8, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v14

    .line 86
    .local v14, "pendingIntent":Landroid/app/PendingIntent;
    const v21, 0x7f080b48

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v0, v1, v14}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 87
    aget v21, p3, v7

    move-object/from16 v0, p2

    move/from16 v1, v21

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    .line 76
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_2

    .line 69
    .end local v5    # "currentMode":Ljava/lang/String;
    .end local v6    # "hrAvailability":Z
    .end local v7    # "i":I
    .end local v8    # "intent":Landroid/content/Intent;
    .end local v10    # "isDBPasswordLocked":Z
    .end local v14    # "pendingIntent":Landroid/app/PendingIntent;
    .end local v19    # "views":Landroid/widget/RemoteViews;
    :cond_2
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 72
    .restart local v6    # "hrAvailability":Z
    :cond_3
    const/4 v10, 0x0

    goto/16 :goto_1

    .line 92
    .restart local v5    # "currentMode":Ljava/lang/String;
    .restart local v10    # "isDBPasswordLocked":Z
    :cond_4
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v4

    .line 93
    .local v4, "connectedDevice":I
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->isStartWalking()Z

    move-result v11

    .line 94
    .local v11, "lIsStartWalking":Z
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/widget/WalkMateAppWidget;->getLastUpdatedTime()Ljava/lang/String;

    move-result-object v12

    .line 95
    .local v12, "lastUpdatedTime":Ljava/lang/String;
    const-string v21, "AbstractWalkMateAppWidget"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "Last updated Time = "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    const-string v21, "gear"

    move-object/from16 v0, v21

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    .line 99
    .local v9, "isAccessoryConnected":Z
    const/16 v17, 0x0

    .line 100
    .local v17, "stepsValue":I
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/shealth/widget/WalkMateAppWidget;->mTotalStep:J

    move-wide/from16 v21, v0

    move-wide/from16 v0, v21

    long-to-float v0, v0

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/shealth/widget/WalkMateAppWidget;->mGoalStep:I

    move/from16 v22, v0

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    div-float v15, v21, v22

    .line 101
    .local v15, "percent":F
    float-to-double v0, v15

    move-wide/from16 v21, v0

    const-wide v23, 0x3fb999999999999aL    # 0.1

    cmpl-double v21, v21, v23

    if-ltz v21, :cond_9

    move/from16 v21, v15

    :goto_3
    const/16 v22, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-static {v0, v1, v11, v2}, Lcom/sec/android/app/shealth/walkingmate/widget/homescreenwidget/CircleProgressDrawer;->getSmallProgressBitmapBurned(Landroid/content/Context;FZZ)Landroid/graphics/Bitmap;

    move-result-object v16

    .line 103
    .local v16, "progressImage":Landroid/graphics/Bitmap;
    invoke-virtual/range {p0 .. p1}, Lcom/sec/android/app/shealth/widget/WalkMateAppWidget;->getLaunchPendingIntent(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v13

    .line 105
    .local v13, "launchIntent":Landroid/app/PendingIntent;
    const/4 v7, 0x0

    .restart local v7    # "i":I
    :goto_4
    move-object/from16 v0, p3

    array-length v0, v0

    move/from16 v21, v0

    move/from16 v0, v21

    if-ge v7, v0, :cond_1d

    .line 107
    new-instance v19, Landroid/widget/RemoteViews;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v21

    const v22, 0x7f030280

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 108
    .restart local v19    # "views":Landroid/widget/RemoteViews;
    if-eqz v6, :cond_5

    .line 109
    const v21, 0x7f080b56

    const/16 v22, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 112
    :cond_5
    const v21, 0x7f080b4d

    const/16 v22, 0x8

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 113
    const v21, 0x7f080b4e

    const/16 v22, 0x8

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 114
    const v21, 0x7f080b4f

    const/16 v22, 0x8

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 115
    const v21, 0x7f080b50

    const/16 v22, 0x8

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 117
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/shealth/widget/WalkMateAppWidget;->mTotalStep:J

    move-wide/from16 v21, v0

    const-wide/16 v23, 0x0

    cmp-long v21, v21, v23

    if-nez v21, :cond_a

    .line 119
    const v21, 0x7f080b4a

    const/16 v22, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 120
    const v21, 0x7f080b4b

    const/16 v22, 0x8

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 121
    const v21, 0x7f080b53

    const/16 v22, 0x8

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 142
    :goto_5
    const-string v21, "gear"

    move-object/from16 v0, v21

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_10

    .line 144
    const v21, 0x7f080b54

    const/16 v22, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 145
    const v21, 0x7f080b55

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v0, v1, v12}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 146
    const v21, 0x7f080b53

    const/16 v22, 0x8

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 148
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/widget/WalkMateAppWidget;->mWearableConnected:Z

    move/from16 v21, v0

    if-eqz v21, :cond_f

    .line 149
    const v21, 0x7f080520

    const/16 v22, 0x8

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 150
    const v21, 0x7f080521

    const/16 v22, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 151
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->isSyncing()Z

    move-result v21

    if-eqz v21, :cond_6

    .line 152
    const v21, 0x7f080520

    const/16 v22, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 153
    const v21, 0x7f080521

    const/16 v22, 0x8

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 164
    :cond_6
    :goto_6
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/shealth/widget/WalkMateAppWidget;->mTotalStep:J

    move-wide/from16 v21, v0

    const-wide/16 v23, 0x0

    cmp-long v21, v21, v23

    if-nez v21, :cond_11

    .line 194
    :goto_7
    const-string v21, "gear"

    move-object/from16 v0, v21

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_15

    .line 195
    const v21, 0x7f080b32

    move-object/from16 v0, v19

    move/from16 v1, v21

    move-object/from16 v2, v16

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 197
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/shealth/widget/WalkMateAppWidget;->mTotalStep:J

    move-wide/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/shealth/widget/WalkMateAppWidget;->mGoalStep:I

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-long v0, v0

    move-wide/from16 v23, v0

    cmp-long v21, v21, v23

    if-gez v21, :cond_14

    .line 198
    const v21, 0x7f080b31

    const v22, 0x7f020723

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2, v11, v4}, Lcom/sec/android/app/shealth/widget/WalkMateAppWidget;->getIconBitmap(Landroid/content/Context;IZI)Landroid/graphics/Bitmap;

    move-result-object v22

    move-object/from16 v0, v19

    move/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 222
    :goto_8
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/shealth/widget/WalkMateAppWidget;->mTotalStep:J

    move-wide/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/shealth/widget/WalkMateAppWidget;->mGoalStep:I

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-long v0, v0

    move-wide/from16 v23, v0

    cmp-long v21, v21, v23

    if-gez v21, :cond_19

    .line 223
    const-string/jumbo v21, "normal"

    move-object/from16 v0, v21

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_7

    if-nez v11, :cond_7

    .line 224
    const v21, 0x7f080b31

    const v22, 0x7f02072c

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2, v11, v4}, Lcom/sec/android/app/shealth/widget/WalkMateAppWidget;->getIconBitmap(Landroid/content/Context;IZI)Landroid/graphics/Bitmap;

    move-result-object v22

    move-object/from16 v0, v19

    move/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 227
    :cond_7
    const v21, 0x7f080b32

    move-object/from16 v0, v19

    move/from16 v1, v21

    move-object/from16 v2, v16

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 228
    const v21, 0x7f080b32

    const/16 v22, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 233
    :goto_9
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/shealth/widget/WalkMateAppWidget;->mTotalStep:J

    move-wide/from16 v21, v0

    const-wide/16 v23, 0x0

    cmp-long v21, v21, v23

    if-eqz v21, :cond_8

    .line 235
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    const v22, 0x7f070044

    invoke-virtual/range {v21 .. v22}, Landroid/content/res/Resources;->getColor(I)I

    move-result v20

    .line 236
    .local v20, "white":I
    const v21, 0x7f080b53

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 237
    move-object/from16 v0, v19

    move/from16 v1, v17

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 238
    const v21, 0x7f080b51

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 240
    const v21, 0x7f080b49

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v23

    const v24, 0x7f090019

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/shealth/widget/WalkMateAppWidget;->mTotalStep:J

    move-wide/from16 v23, v0

    invoke-static/range {v23 .. v24}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v23

    const v24, 0x7f0900c6

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v19

    move/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    .line 243
    .end local v20    # "white":I
    :cond_8
    const-string/jumbo v21, "normal"

    move-object/from16 v0, v21

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_1a

    if-nez v11, :cond_1a

    .line 244
    const/16 v21, 0x80

    const/16 v22, 0xff

    const/16 v23, 0xff

    const/16 v24, 0xff

    invoke-static/range {v21 .. v24}, Landroid/graphics/Color;->argb(IIII)I

    move-result v21

    move-object/from16 v0, v19

    move/from16 v1, v17

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 245
    const v21, 0x7f080b36

    const/16 v22, 0x80

    const/16 v23, 0xff

    const/16 v24, 0xff

    const/16 v25, 0xff

    invoke-static/range {v22 .. v25}, Landroid/graphics/Color;->argb(IIII)I

    move-result v22

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 252
    :goto_a
    const v21, 0x7f080b49

    invoke-virtual/range {p0 .. p1}, Lcom/sec/android/app/shealth/widget/WalkMateAppWidget;->getLaunchPendingIntent(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v22

    move-object/from16 v0, v19

    move/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 253
    const v21, 0x7f080b53

    invoke-virtual/range {p0 .. p1}, Lcom/sec/android/app/shealth/widget/WalkMateAppWidget;->getLaunchPendingIntent(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v22

    move-object/from16 v0, v19

    move/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 254
    if-eqz v6, :cond_1b

    .line 255
    const v21, 0x7f080b56

    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->getNotificationHRIntent()Landroid/app/PendingIntent;

    move-result-object v22

    move-object/from16 v0, v19

    move/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 259
    :goto_b
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/widget/WalkMateAppWidget;->mWearableConnected:Z

    move/from16 v21, v0

    if-eqz v21, :cond_1c

    .line 260
    const v21, 0x7f080b52

    invoke-virtual/range {p0 .. p1}, Lcom/sec/android/app/shealth/widget/WalkMateAppWidget;->getSyncPendingIntent(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v22

    move-object/from16 v0, v19

    move/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 264
    :goto_c
    aget v21, p3, v7

    move-object/from16 v0, p2

    move/from16 v1, v21

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    .line 105
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_4

    .line 101
    .end local v7    # "i":I
    .end local v13    # "launchIntent":Landroid/app/PendingIntent;
    .end local v16    # "progressImage":Landroid/graphics/Bitmap;
    .end local v19    # "views":Landroid/widget/RemoteViews;
    :cond_9
    const/16 v21, 0x0

    goto/16 :goto_3

    .line 125
    .restart local v7    # "i":I
    .restart local v13    # "launchIntent":Landroid/app/PendingIntent;
    .restart local v16    # "progressImage":Landroid/graphics/Bitmap;
    .restart local v19    # "views":Landroid/widget/RemoteViews;
    :cond_a
    const v21, 0x7f080b4a

    const/16 v22, 0x8

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 126
    const v21, 0x7f080b4b

    const/16 v22, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 127
    const v21, 0x7f080b53

    const/16 v22, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 128
    if-nez v11, :cond_c

    const-string/jumbo v21, "normal"

    move-object/from16 v0, v21

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_c

    .line 129
    if-eqz v9, :cond_b

    const v17, 0x7f080b50

    .line 130
    :goto_d
    const v21, 0x7f080b53

    const v22, 0x7f090a17

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v19

    move/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_5

    .line 129
    :cond_b
    const v17, 0x7f080b4e

    goto :goto_d

    .line 133
    :cond_c
    if-eqz v9, :cond_d

    const v17, 0x7f080b4f

    .line 134
    :goto_e
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->isArabLocale()Z

    move-result v21

    if-eqz v21, :cond_e

    .line 135
    const v21, 0x7f080b53

    sget-object v22, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v23

    const v24, 0x7f090b68

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v23

    const/16 v24, 0x1

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/shealth/widget/WalkMateAppWidget;->mGoalStep:I

    move/from16 v26, v0

    invoke-static/range {v26 .. v26}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v26

    aput-object v26, v24, v25

    invoke-static/range {v22 .. v24}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v19

    move/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_5

    .line 133
    :cond_d
    const v17, 0x7f080b4d

    goto :goto_e

    .line 137
    :cond_e
    const v21, 0x7f080b53

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    const v23, 0x7f090b68

    invoke-virtual/range {v22 .. v23}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v22

    const/16 v23, 0x1

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/shealth/widget/WalkMateAppWidget;->mGoalStep:I

    move/from16 v25, v0

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    aput-object v25, v23, v24

    invoke-static/range {v22 .. v23}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v19

    move/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_5

    .line 156
    :cond_f
    const v21, 0x7f080520

    const/16 v22, 0x8

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 157
    const v21, 0x7f080521

    const/16 v22, 0x8

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto/16 :goto_6

    .line 160
    :cond_10
    const v21, 0x7f080b54

    const/16 v22, 0x8

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto/16 :goto_6

    .line 170
    :cond_11
    const/16 v21, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v17

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 171
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/shealth/widget/WalkMateAppWidget;->mTotalStep:J

    move-wide/from16 v21, v0

    const-wide/16 v23, 0x2710

    cmp-long v21, v21, v23

    if-ltz v21, :cond_12

    .line 172
    const/16 v21, 0x1

    const/high16 v22, 0x42100000    # 36.0f

    move-object/from16 v0, v19

    move/from16 v1, v17

    move/from16 v2, v21

    move/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/RemoteViews;->setTextViewTextSize(IIF)V

    .line 176
    :goto_f
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/shealth/widget/WalkMateAppWidget;->mTotalStep:J

    move-wide/from16 v21, v0

    const-wide/32 v23, 0x186a0

    cmp-long v21, v21, v23

    if-ltz v21, :cond_13

    .line 177
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/shealth/widget/WalkMateAppWidget;->mTotalStep:J

    move-wide/from16 v21, v0

    move-wide/from16 v0, v21

    long-to-int v0, v0

    move/from16 v21, v0

    move/from16 v0, v21

    div-int/lit16 v0, v0, 0x3e8

    move/from16 v21, v0

    invoke-static/range {v21 .. v21}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v18

    .line 178
    .local v18, "value":Ljava/lang/String;
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v21

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const v22, 0x7f090bb4

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v19

    move/from16 v1, v17

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 183
    .end local v18    # "value":Ljava/lang/String;
    :goto_10
    const v21, 0x7f080b51

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    const v23, 0x7f0909d9

    invoke-virtual/range {v22 .. v23}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v19

    move/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_7

    .line 174
    :cond_12
    const/16 v21, 0x1

    const/high16 v22, 0x42200000    # 40.0f

    move-object/from16 v0, v19

    move/from16 v1, v17

    move/from16 v2, v21

    move/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/RemoteViews;->setTextViewTextSize(IIF)V

    goto :goto_f

    .line 180
    :cond_13
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/shealth/widget/WalkMateAppWidget;->mTotalStep:J

    move-wide/from16 v21, v0

    invoke-static/range {v21 .. v22}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v19

    move/from16 v1, v17

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto :goto_10

    .line 200
    :cond_14
    const v21, 0x7f080b31

    const v22, 0x7f02071e

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2, v11, v4}, Lcom/sec/android/app/shealth/widget/WalkMateAppWidget;->getIconBitmap(Landroid/content/Context;IZI)Landroid/graphics/Bitmap;

    move-result-object v22

    move-object/from16 v0, v19

    move/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    goto/16 :goto_8

    .line 203
    :cond_15
    if-nez v4, :cond_16

    .line 204
    const v21, 0x7f080b31

    const v22, 0x7f020728

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2, v11, v4}, Lcom/sec/android/app/shealth/widget/WalkMateAppWidget;->getIconBitmap(Landroid/content/Context;IZI)Landroid/graphics/Bitmap;

    move-result-object v22

    move-object/from16 v0, v19

    move/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 205
    const v21, 0x7f080b32

    move-object/from16 v0, v19

    move/from16 v1, v21

    move-object/from16 v2, v16

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    goto/16 :goto_8

    .line 207
    :cond_16
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/shealth/widget/WalkMateAppWidget;->mTotalStep:J

    move-wide/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/shealth/widget/WalkMateAppWidget;->mGoalStep:I

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-long v0, v0

    move-wide/from16 v23, v0

    cmp-long v21, v21, v23

    if-ltz v21, :cond_17

    .line 208
    const v21, 0x7f080b32

    move-object/from16 v0, v19

    move/from16 v1, v21

    move-object/from16 v2, v16

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 209
    const v21, 0x7f080b31

    const v22, 0x7f02072f

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2, v11, v4}, Lcom/sec/android/app/shealth/widget/WalkMateAppWidget;->getIconBitmap(Landroid/content/Context;IZI)Landroid/graphics/Bitmap;

    move-result-object v22

    move-object/from16 v0, v19

    move/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    goto/16 :goto_8

    .line 211
    :cond_17
    float-to-double v0, v15

    move-wide/from16 v21, v0

    const-wide v23, 0x3fb999999999999aL    # 0.1

    cmpl-double v21, v21, v23

    if-ltz v21, :cond_18

    .line 212
    const v21, 0x7f080b31

    const v22, 0x7f020727

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2, v11, v4}, Lcom/sec/android/app/shealth/widget/WalkMateAppWidget;->getIconBitmap(Landroid/content/Context;IZI)Landroid/graphics/Bitmap;

    move-result-object v22

    move-object/from16 v0, v19

    move/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 213
    const v21, 0x7f080b32

    move-object/from16 v0, v19

    move/from16 v1, v21

    move-object/from16 v2, v16

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    goto/16 :goto_8

    .line 215
    :cond_18
    const v21, 0x7f080b31

    const v22, 0x7f020728

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2, v11, v4}, Lcom/sec/android/app/shealth/widget/WalkMateAppWidget;->getIconBitmap(Landroid/content/Context;IZI)Landroid/graphics/Bitmap;

    move-result-object v22

    move-object/from16 v0, v19

    move/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 216
    const v21, 0x7f080b32

    move-object/from16 v0, v19

    move/from16 v1, v21

    move-object/from16 v2, v16

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    goto/16 :goto_8

    .line 230
    :cond_19
    const v21, 0x7f080b32

    const/16 v22, 0x8

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto/16 :goto_9

    .line 247
    :cond_1a
    const/16 v21, 0xff

    const/16 v22, 0xff

    const/16 v23, 0xff

    const/16 v24, 0xff

    invoke-static/range {v21 .. v24}, Landroid/graphics/Color;->argb(IIII)I

    move-result v21

    move-object/from16 v0, v19

    move/from16 v1, v17

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 248
    const v21, 0x7f080b36

    const/16 v22, 0xff

    const/16 v23, 0xff

    const/16 v24, 0xff

    const/16 v25, 0xff

    invoke-static/range {v22 .. v25}, Landroid/graphics/Color;->argb(IIII)I

    move-result v22

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextColor(II)V

    goto/16 :goto_a

    .line 257
    :cond_1b
    const v21, 0x7f080b3e

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v0, v1, v13}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    goto/16 :goto_b

    .line 262
    :cond_1c
    const v21, 0x7f080b52

    invoke-virtual/range {p0 .. p1}, Lcom/sec/android/app/shealth/widget/WalkMateAppWidget;->getLaunchPendingIntent(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v22

    move-object/from16 v0, v19

    move/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    goto/16 :goto_c

    .line 266
    .end local v4    # "connectedDevice":I
    .end local v9    # "isAccessoryConnected":Z
    .end local v11    # "lIsStartWalking":Z
    .end local v12    # "lastUpdatedTime":Ljava/lang/String;
    .end local v13    # "launchIntent":Landroid/app/PendingIntent;
    .end local v15    # "percent":F
    .end local v16    # "progressImage":Landroid/graphics/Bitmap;
    .end local v17    # "stepsValue":I
    .end local v19    # "views":Landroid/widget/RemoteViews;
    :cond_1d
    return-void
.end method
