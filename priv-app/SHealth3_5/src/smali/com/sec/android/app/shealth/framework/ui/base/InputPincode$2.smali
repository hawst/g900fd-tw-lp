.class Lcom/sec/android/app/shealth/framework/ui/base/InputPincode$2;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->toggleKeyBoard()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mOutStateBundle:Landroid/os/Bundle;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->access$500(Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;)Landroid/os/Bundle;

    move-result-object v0

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mKeyBoardStatus:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->access$600(Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mEditTextPinCode:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->access$700(Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;)Landroid/widget/EditText;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/SoftInputUtils;->showSoftInput(Landroid/content/Context;Landroid/view/View;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mKeyBoardStatus:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->access$602(Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;Z)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mIsKeyBoardShown:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->access$300(Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->hideHeightKeyboard()V

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mIsKeyBoardShown:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->access$300(Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mEditTextPinCode:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->access$700(Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;)Landroid/widget/EditText;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/SoftInputUtils;->showSoftInput(Landroid/content/Context;Landroid/view/View;)V

    goto :goto_0
.end method
