.class public Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;
.super Landroid/support/v4/app/Fragment;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored$CalendarDataObtainer;
    }
.end annotation


# static fields
.field private static final COLUMNS_IN_DAY_VIEW:I = 0x7

.field private static final ROWS_IN_DAY_VIEW:I = 0x6

.field protected static final UPDATE_DATA:I

.field private static gridId:I


# instance fields
.field private adapter:Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;

.field private days:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field gridView:Landroid/widget/GridView;

.field final handler:Landroid/os/Handler;

.field private mDaysInfo:Ljava/util/TreeMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mIsFinal:Z

.field private mSelectedTime:J

.field private mTime:Ljava/util/Calendar;

.field periodStartToDayButtonMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const v0, 0x1b207

    sput v0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;->gridId:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;->days:Ljava/util/List;

    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored$2;-><init>(Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;->handler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;)Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;->adapter:Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;)Ljava/util/TreeMap;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;->mDaysInfo:Ljava/util/TreeMap;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;Ljava/util/TreeMap;)Ljava/util/TreeMap;
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;->mDaysInfo:Ljava/util/TreeMap;

    return-object p1
.end method

.method private getDayView(Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 8

    const/4 v6, 0x0

    sget v0, Lcom/sec/android/app/shealth/framework/ui/R$layout;->calendar_month:I

    invoke-virtual {p1, v0, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;->getSundayIndex()I

    move-result v1

    sget v0, Lcom/sec/android/app/shealth/framework/ui/R$id;->days_of_week_container:I

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-direct {p0, v1, v0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;->initDaysOfWeekContainer(ILandroid/widget/LinearLayout;)V

    sget v0, Lcom/sec/android/app/shealth/framework/ui/R$id;->grid:I

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;->gridView:Landroid/widget/GridView;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;->gridView:Landroid/widget/GridView;

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored$1;-><init>(Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;)V

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;->mTime:Ljava/util/Calendar;

    if-nez v0, :cond_0

    move-object v0, v7

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;->mTime:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;->shiftCalendarToStartOfMonth(Ljava/util/Calendar;)V

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    const/4 v1, 0x2

    invoke-static {v2, v3, v1}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfCurrentPeriod(JI)J

    move-result-wide v4

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;->getFirstDayOfWeek()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;->shiftCalendarToFirstDayOfWeek(Ljava/util/Calendar;I)V

    new-instance v1, Ljava/util/TreeMap;

    invoke-direct {v1}, Ljava/util/TreeMap;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;->periodStartToDayButtonMap:Ljava/util/Map;

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;->getDayViewAndFillViewMaps(Ljava/util/Calendar;)V

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;->mIsFinal:Z

    if-eqz v0, :cond_1

    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored$CalendarDataObtainer;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored$CalendarDataObtainer;-><init>(Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;JJLcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored$1;)V

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored$CalendarDataObtainer;->start()V

    :cond_1
    invoke-direct {p0, v7}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;->refreshFocusable(Landroid/view/View;)V

    move-object v0, v7

    goto :goto_0
.end method

.method private getDayViewAndFillViewMaps(Ljava/util/Calendar;)V
    .locals 7

    const/4 v0, 0x0

    :goto_0
    const/16 v1, 0x2a

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;->days:Ljava/util/List;

    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v1, 0x6

    const/4 v2, 0x1

    invoke-virtual {p1, v1, v2}, Ljava/util/Calendar;->add(II)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;->days:Ljava/util/List;

    iget-object v3, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;->mTime:Ljava/util/Calendar;

    iget-wide v4, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;->mSelectedTime:J

    iget-boolean v6, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;->mIsFinal:Z

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;-><init>(Landroid/content/Context;Ljava/util/List;Ljava/util/Calendar;JZ)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;->adapter:Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;->gridView:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;->adapter:Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;->gridView:Landroid/widget/GridView;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;->gridId:I

    add-int/lit8 v2, v1, -0x1

    sput v2, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;->gridId:I

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setId(I)V

    return-void
.end method

.method private getFirstDayOfWeek()I
    .locals 1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Calendar;->getInstance(Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Calendar;->getFirstDayOfWeek()I

    move-result v0

    return v0
.end method

.method public static final getInstance(Landroid/content/Context;JIJLjava/lang/String;Ljava/lang/String;Z)Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;
    .locals 3

    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;-><init>()V

    new-instance v1, Landroid/os/Bundle;

    const/4 v2, 0x5

    invoke-direct {v1, v2}, Landroid/os/Bundle;-><init>(I)V

    const-string/jumbo v2, "mTime"

    invoke-virtual {v1, v2, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string/jumbo v2, "mPeriodType"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v2, "mSelectedTime"

    invoke-virtual {v1, v2, p4, p5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v2, "dataType"

    invoke-virtual {v1, v2, p6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v2, "timeColumnName"

    invoke-virtual {v1, v2, p7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v2, "mIsFinal"

    invoke-virtual {v1, v2, p8}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;->setArguments(Landroid/os/Bundle;)V

    return-object v0
.end method

.method private getSundayIndex()I
    .locals 6

    const/4 v0, 0x5

    const/4 v1, 0x4

    const/4 v2, 0x3

    const/4 v3, 0x2

    const/4 v4, 0x1

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;->getFirstDayOfWeek()I

    move-result v5

    if-ne v5, v4, :cond_1

    const/4 v0, 0x7

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-ne v5, v3, :cond_2

    const/4 v0, 0x6

    goto :goto_0

    :cond_2
    if-eq v5, v2, :cond_0

    if-ne v5, v1, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    if-ne v5, v0, :cond_4

    move v0, v2

    goto :goto_0

    :cond_4
    const/4 v0, 0x6

    if-ne v5, v0, :cond_5

    move v0, v3

    goto :goto_0

    :cond_5
    move v0, v4

    goto :goto_0
.end method

.method private initDaysOfWeekContainer(ILandroid/widget/LinearLayout;)V
    .locals 6

    const/4 v5, 0x1

    const/4 v3, -0x1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$array;->days_of_week:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-direct {v2, v3, v3, v0}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    const/4 v0, 0x0

    :goto_0
    array-length v3, v1

    if-ge v0, v3, :cond_0

    new-instance v3, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    rem-int/lit8 v4, p1, 0x7

    invoke-direct {p0, v4, v0, v3}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;->setCorrectDayTextColor(IILandroid/widget/TextView;)V

    sub-int v4, v0, p1

    add-int/lit8 v4, v4, 0x7

    rem-int/lit8 v4, v4, 0x7

    aget-object v4, v1, v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/high16 v4, 0x41400000    # 12.0f

    invoke-virtual {v3, v5, v4}, Landroid/widget/TextView;->setTextSize(IF)V

    const/16 v4, 0x11

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setGravity(I)V

    sget-object v4, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setImportantForAccessibility(I)V

    invoke-virtual {p2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private refreshFocusable(Landroid/view/View;)V
    .locals 4

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored$3;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored$3;-><init>(Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;Landroid/view/View;)V

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    return-void
.end method

.method private setCorrectDayTextColor(IILandroid/widget/TextView;)V
    .locals 2

    if-ne p2, p1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$color;->calendar_text_sunday:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p3, v0}, Landroid/widget/TextView;->setTextColor(I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$color;->day_color:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p3, v0}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method

.method private shiftCalendarToFirstDayOfWeek(Ljava/util/Calendar;I)V
    .locals 2

    :goto_0
    const/4 v0, 0x7

    invoke-virtual {p1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    if-eq v0, p2, :cond_0

    const/4 v0, 0x6

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Ljava/util/Calendar;->add(II)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private shiftCalendarToStartOfMonth(Ljava/util/Calendar;)V
    .locals 3

    const/4 v2, 0x0

    const/4 v0, 0x5

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Ljava/util/Calendar;->set(II)V

    const/16 v0, 0xb

    invoke-virtual {p1, v0, v2}, Ljava/util/Calendar;->set(II)V

    const/16 v0, 0xc

    invoke-virtual {p1, v0, v2}, Ljava/util/Calendar;->set(II)V

    const/16 v0, 0xd

    invoke-virtual {p1, v0, v2}, Ljava/util/Calendar;->set(II)V

    const/16 v0, 0xe

    invoke-virtual {p1, v0, v2}, Ljava/util/Calendar;->set(II)V

    return-void
.end method


# virtual methods
.method public getDaysInfo()Ljava/util/TreeMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;->mDaysInfo:Ljava/util/TreeMap;

    return-object v0
.end method

.method public getStartTime()J
    .locals 2

    iget-wide v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;->mSelectedTime:J

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfMonth(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "mTime"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;->mTime:Ljava/util/Calendar;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "mSelectedTime"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;->mSelectedTime:J

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "mIsFinal"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;->mIsFinal:Z

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;->getDayView(Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    const-string v0, "Calendar frag refactor"

    const-string v1, "Time stopped"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
