.class public Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "GalleryActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/gallery/GalleryView$CompleteItemAddNotifier;
.implements Lcom/sec/android/app/shealth/common/gallery/ViewPagerChangeStats;


# static fields
.field private static final NOT_FOUND_PATH_INDEX:I = -0x1


# instance fields
.field private mGallery:Lcom/sec/android/app/shealth/common/gallery/GalleryView;

.field private mGalleryViewPager:Lcom/sec/android/app/shealth/common/gallery/ViewPagerWithFlipControl;

.field private mImagePageAdapter:Lcom/sec/android/app/shealth/common/gallery/ImageViewPageAdapter;

.field private mImagePaths:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mLastPosition:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;)Lcom/sec/android/app/shealth/common/gallery/ViewPagerWithFlipControl;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;->mGalleryViewPager:Lcom/sec/android/app/shealth/common/gallery/ViewPagerWithFlipControl;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;)Lcom/sec/android/app/shealth/common/gallery/GalleryView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;->mGallery:Lcom/sec/android/app/shealth/common/gallery/GalleryView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;

    .prologue
    .line 33
    iget v0, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;->mLastPosition:I

    return v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;
    .param p1, "x1"    # I

    .prologue
    .line 33
    iput p1, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;->mLastPosition:I

    return p1
.end method

.method private filterImagePathOnExistance(Ljava/util/List;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 83
    .local p1, "imagePaths":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v0, Ljava/util/ArrayList;

    new-instance v1, Lcom/sec/android/app/shealth/common/gallery/GalleryActivity$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/common/gallery/GalleryActivity$1;-><init>(Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;)V

    invoke-static {p1, v1}, Lcom/sec/android/app/shealth/common/utils/Utils;->filterCollection(Ljava/util/Collection;Lcom/sec/android/app/shealth/common/utils/Filterable;)Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method private initGallery(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 92
    .local p1, "imagePaths":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;->mGallery:Lcom/sec/android/app/shealth/common/gallery/GalleryView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/gallery/GalleryView;->removeAllViews()V

    .line 93
    new-instance v0, Lcom/sec/android/app/shealth/common/gallery/ImageViewPageAdapter;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/shealth/common/gallery/ImageViewPageAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;->mImagePageAdapter:Lcom/sec/android/app/shealth/common/gallery/ImageViewPageAdapter;

    .line 94
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;->mGalleryViewPager:Lcom/sec/android/app/shealth/common/gallery/ViewPagerWithFlipControl;

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;->mImagePageAdapter:Lcom/sec/android/app/shealth/common/gallery/ImageViewPageAdapter;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/gallery/ViewPagerWithFlipControl;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 95
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;->mGallery:Lcom/sec/android/app/shealth/common/gallery/GalleryView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/common/gallery/GalleryView;->addItems(Ljava/util/List;)V

    .line 96
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;->mGallery:Lcom/sec/android/app/shealth/common/gallery/GalleryView;

    new-instance v1, Lcom/sec/android/app/shealth/common/gallery/GalleryActivity$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/common/gallery/GalleryActivity$2;-><init>(Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/gallery/GalleryView;->setOnItemSelectedListener(Lcom/sec/android/app/shealth/common/gallery/GalleryView$OnItemSelectedListener;)V

    .line 102
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;->mGalleryViewPager:Lcom/sec/android/app/shealth/common/gallery/ViewPagerWithFlipControl;

    new-instance v1, Lcom/sec/android/app/shealth/common/gallery/GalleryActivity$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/common/gallery/GalleryActivity$3;-><init>(Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/gallery/ViewPagerWithFlipControl;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 119
    return-void
.end method


# virtual methods
.method public executePostItemAddition()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 129
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 132
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;->mGallery:Lcom/sec/android/app/shealth/common/gallery/GalleryView;

    iget v1, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;->mLastPosition:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/gallery/GalleryView;->setCurrentItem(I)V

    .line 135
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;->mGalleryViewPager:Lcom/sec/android/app/shealth/common/gallery/ViewPagerWithFlipControl;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/gallery/ViewPagerWithFlipControl;->setVisibility(I)V

    .line 136
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;->mGallery:Lcom/sec/android/app/shealth/common/gallery/GalleryView;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/gallery/GalleryView;->setVisibility(I)V

    .line 138
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;->mGallery:Lcom/sec/android/app/shealth/common/gallery/GalleryView;

    new-instance v1, Lcom/sec/android/app/shealth/common/gallery/GalleryActivity$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/common/gallery/GalleryActivity$4;-><init>(Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/gallery/GalleryView;->post(Ljava/lang/Runnable;)Z

    .line 150
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x0

    .line 43
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 44
    const v2, 0x7f0300cd

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;->setContentView(I)V

    .line 45
    const v2, 0x7f0803b3

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/common/gallery/GalleryView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;->mGallery:Lcom/sec/android/app/shealth/common/gallery/GalleryView;

    .line 46
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;->mGallery:Lcom/sec/android/app/shealth/common/gallery/GalleryView;

    invoke-virtual {v2, p0}, Lcom/sec/android/app/shealth/common/gallery/GalleryView;->setCompleteItemAddNotifier(Lcom/sec/android/app/shealth/common/gallery/GalleryView$CompleteItemAddNotifier;)V

    .line 47
    const v2, 0x7f0803b2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/common/gallery/ViewPagerWithFlipControl;

    iput-object v2, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;->mGalleryViewPager:Lcom/sec/android/app/shealth/common/gallery/ViewPagerWithFlipControl;

    .line 48
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v4, "gallery_image_paths"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;->mImagePaths:Ljava/util/List;

    .line 49
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;->mImagePaths:Ljava/util/List;

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;->filterImagePathOnExistance(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;->mImagePaths:Ljava/util/List;

    .line 50
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;->mImagePaths:Ljava/util/List;

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;->initGallery(Ljava/util/List;)V

    .line 51
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v4, "gallery_selected_image_path"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 52
    .local v0, "selectedPath":Ljava/lang/String;
    if-nez v0, :cond_3

    move v1, v3

    .line 53
    .local v1, "startIndex":I
    :goto_0
    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    move v1, v3

    .end local v1    # "startIndex":I
    :cond_0
    iput v1, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;->mLastPosition:I

    .line 54
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;->mImagePageAdapter:Lcom/sec/android/app/shealth/common/gallery/ImageViewPageAdapter;

    if-eqz v2, :cond_1

    .line 55
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;->mImagePageAdapter:Lcom/sec/android/app/shealth/common/gallery/ImageViewPageAdapter;

    iget v4, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;->mLastPosition:I

    invoke-virtual {v2, v4}, Lcom/sec/android/app/shealth/common/gallery/ImageViewPageAdapter;->preparingAndCachingViewByPosition(I)Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    .line 57
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;->mGalleryViewPager:Lcom/sec/android/app/shealth/common/gallery/ViewPagerWithFlipControl;

    if-eqz v2, :cond_2

    .line 58
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;->mGalleryViewPager:Lcom/sec/android/app/shealth/common/gallery/ViewPagerWithFlipControl;

    iget v4, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;->mLastPosition:I

    const/4 v5, 0x1

    invoke-virtual {v2, v4, v5}, Lcom/sec/android/app/shealth/common/gallery/ViewPagerWithFlipControl;->setCurrentItem(IZ)V

    .line 59
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;->mGalleryViewPager:Lcom/sec/android/app/shealth/common/gallery/ViewPagerWithFlipControl;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/common/gallery/ViewPagerWithFlipControl;->setVisibility(I)V

    .line 61
    :cond_2
    return-void

    .line 52
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;->mImagePaths:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    goto :goto_0
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 65
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onResume()V

    .line 66
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;->mImagePaths:Ljava/util/List;

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;->filterImagePathOnExistance(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 67
    .local v0, "strings":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 69
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;->finish()V

    .line 80
    :cond_0
    :goto_0
    return-void

    .line 73
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;->mImagePaths:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-eq v1, v2, :cond_0

    .line 74
    iput-object v0, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;->mImagePaths:Ljava/util/List;

    .line 75
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;->mImagePaths:Ljava/util/List;

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;->initGallery(Ljava/util/List;)V

    .line 76
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;->mLastPosition:I

    goto :goto_0
.end method

.method public setEnableFlip(Z)V
    .locals 1
    .param p1, "isEnabled"    # Z

    .prologue
    .line 123
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;->mGalleryViewPager:Lcom/sec/android/app/shealth/common/gallery/ViewPagerWithFlipControl;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/common/gallery/ViewPagerWithFlipControl;->setEnabledFlip(Z)V

    .line 124
    return-void
.end method
