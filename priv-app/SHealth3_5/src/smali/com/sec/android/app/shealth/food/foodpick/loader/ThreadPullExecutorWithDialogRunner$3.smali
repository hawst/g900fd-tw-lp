.class Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner$3;
.super Ljava/lang/Object;
.source "ThreadPullExecutorWithDialogRunner.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;->getRunnable(Lcom/sec/android/app/shealth/food/foodpick/loader/Task;Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;)Ljava/lang/Runnable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;

.field final synthetic val$result:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;

.field final synthetic val$task:Lcom/sec/android/app/shealth/food/foodpick/loader/Task;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;Lcom/sec/android/app/shealth/food/foodpick/loader/Task;Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;)V
    .locals 0

    .prologue
    .line 84
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner$3;->this$0:Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;

    iput-object p2, p0, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner$3;->val$task:Lcom/sec/android/app/shealth/food/foodpick/loader/Task;

    iput-object p3, p0, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner$3;->val$result:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner$3;->val$task:Lcom/sec/android/app/shealth/food/foodpick/loader/Task;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/foodpick/loader/Task;->isCanceled()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner$3;->val$result:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;

    if-nez v0, :cond_1

    .line 101
    :cond_0
    :goto_0
    return-void

    .line 91
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner$3;->this$0:Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner$3;->val$task:Lcom/sec/android/app/shealth/food/foodpick/loader/Task;

    # invokes: Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;->hideDialogIfNeeded(Lcom/sec/android/app/shealth/food/foodpick/loader/Task;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;->access$300(Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;Lcom/sec/android/app/shealth/food/foodpick/loader/Task;)V

    .line 92
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner$3;->val$task:Lcom/sec/android/app/shealth/food/foodpick/loader/Task;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/foodpick/loader/Task;->clearFuture()Ljava/util/concurrent/Future;

    .line 93
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner$3;->val$result:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;->isSuccessful()Z

    move-result v0

    if-nez v0, :cond_3

    .line 94
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner$3;->val$task:Lcom/sec/android/app/shealth/food/foodpick/loader/Task;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/foodpick/loader/Task;->withErrorToast()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner$3;->val$result:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;->getErrorMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 95
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner$3;->this$0:Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;->access$400(Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;)Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner$3;->val$result:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;->getErrorMessage()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 97
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner$3;->this$0:Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner$3;->val$task:Lcom/sec/android/app/shealth/food/foodpick/loader/Task;

    # invokes: Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;->notifyWithErrorFinished(Lcom/sec/android/app/shealth/food/foodpick/loader/Task;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;->access$500(Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;Lcom/sec/android/app/shealth/food/foodpick/loader/Task;)V

    goto :goto_0

    .line 100
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner$3;->this$0:Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner$3;->val$task:Lcom/sec/android/app/shealth/food/foodpick/loader/Task;

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner$3;->val$result:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;

    # invokes: Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;->notifyFinishedIfNeeded(Lcom/sec/android/app/shealth/food/foodpick/loader/Task;Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;->access$600(Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;Lcom/sec/android/app/shealth/food/foodpick/loader/Task;Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;)V

    goto :goto_0
.end method
