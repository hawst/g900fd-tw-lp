.class Lcom/sec/android/app/shealth/walkingmate/widget/input/GradationObject;
.super Ljava/lang/Object;
.source "GradationObject.java"


# instance fields
.field private mIsBig:Z

.field private mIsMiddle:Z

.field private mOffsetMovement:I

.field private mValue:F


# direct methods
.method public constructor <init>(FI)V
    .locals 0
    .param p1, "value"    # F
    .param p2, "offsetMovement"    # I

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    iput p1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/GradationObject;->mValue:F

    .line 76
    iput p2, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/GradationObject;->mOffsetMovement:I

    .line 78
    return-void
.end method

.method public constructor <init>(FIZZ)V
    .locals 0
    .param p1, "value"    # F
    .param p2, "offsetMovement"    # I
    .param p3, "isBig"    # Z
    .param p4, "isMiddle"    # Z

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput p1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/GradationObject;->mValue:F

    .line 48
    iput p2, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/GradationObject;->mOffsetMovement:I

    .line 50
    iput-boolean p3, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/GradationObject;->mIsBig:Z

    .line 52
    iput-boolean p4, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/GradationObject;->mIsMiddle:Z

    .line 54
    return-void
.end method


# virtual methods
.method public getOffsetMovement()I
    .locals 1

    .prologue
    .line 104
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/GradationObject;->mOffsetMovement:I

    return v0
.end method

.method public getValue()F
    .locals 1

    .prologue
    .line 90
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/GradationObject;->mValue:F

    return v0
.end method

.method public isBig()Z
    .locals 1

    .prologue
    .line 132
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/GradationObject;->mIsBig:Z

    return v0
.end method

.method public isMiddle()Z
    .locals 1

    .prologue
    .line 146
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/GradationObject;->mIsMiddle:Z

    return v0
.end method

.method public setOffsetMovement(I)V
    .locals 0
    .param p1, "offsetMovement"    # I

    .prologue
    .line 118
    iput p1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/GradationObject;->mOffsetMovement:I

    .line 120
    return-void
.end method
