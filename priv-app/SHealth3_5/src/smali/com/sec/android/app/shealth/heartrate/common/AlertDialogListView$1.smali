.class Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$1;
.super Ljava/lang/Object;
.source "AlertDialogListView.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->showChooserDialog(ZLjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;)V
    .locals 0

    .prologue
    .line 107
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$1;->this$0:Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onContentInitialization(Landroid/view/View;Landroid/app/Activity;Landroid/app/Dialog;Landroid/os/Bundle;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V
    .locals 4
    .param p1, "content"    # Landroid/view/View;
    .param p2, "parentActivity"    # Landroid/app/Activity;
    .param p3, "dialog"    # Landroid/app/Dialog;
    .param p4, "savedInstanceState"    # Landroid/os/Bundle;
    .param p5, "okButtonHandler"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;

    .prologue
    .line 111
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$1;->this$0:Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;

    const v0, 0x7f080567

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    # setter for: Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->lvTagList:Landroid/widget/ListView;
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->access$002(Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;Landroid/widget/ListView;)Landroid/widget/ListView;

    .line 112
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$1;->this$0:Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;

    # getter for: Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->lvTagList:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->access$000(Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$1;->this$0:Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;

    iget-object v1, v1, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->mTagListadapter:Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$CustomTagList;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 113
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$1;->this$0:Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;

    # getter for: Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->lvTagList:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->access$000(Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;)Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 114
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$1;->this$0:Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;

    # getter for: Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->lvTagList:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->access$000(Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;)Landroid/widget/ListView;

    move-result-object v0

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, -0x1

    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$1;->this$0:Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;

    iget v3, v3, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->mHeight:I

    invoke-direct {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 115
    return-void
.end method
