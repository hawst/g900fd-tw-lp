.class Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment$1;
.super Ljava/lang/Object;
.source "FrequentFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter$OnFavoriteClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;->initListViewAdapter(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter$OnFavoriteClickListener",
        "<",
        "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;)V
    .locals 0

    .prologue
    .line 134
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment$1;->this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic onFavoriteClick(Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoritableListItem;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoritableListItem;

    .prologue
    .line 134
    check-cast p1, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;

    .end local p1    # "x0":Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoritableListItem;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment$1;->onFavoriteClick(Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;)V

    return-void
.end method

.method public onFavoriteClick(Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;)V
    .locals 2
    .param p1, "listItem"    # Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;

    .prologue
    .line 137
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment$1;->this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;->mFavoriteAdapter:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAndAllAdapter;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;->access$000(Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;)Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAndAllAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAndAllAdapter;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 138
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment$1;->this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment$1;->this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;->getView()Landroid/view/View;

    move-result-object v1

    # invokes: Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;->showNoDataScreen(Landroid/view/View;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;->access$100(Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;Landroid/view/View;)V

    .line 142
    :goto_0
    return-void

    .line 140
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment$1;->this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment$1;->this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;->getView()Landroid/view/View;

    move-result-object v1

    # invokes: Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;->updateFavoriteAdapter(Landroid/view/View;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;->access$200(Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;Landroid/view/View;)V

    goto :goto_0
.end method
