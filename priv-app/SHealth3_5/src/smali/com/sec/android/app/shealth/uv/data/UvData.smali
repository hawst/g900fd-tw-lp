.class public Lcom/sec/android/app/shealth/uv/data/UvData;
.super Ljava/lang/Object;
.source "UvData.java"


# instance fields
.field private applicationId:Ljava/lang/String;

.field private average:D

.field private comment:Ljava/lang/String;

.field private createTime:J

.field private heartRate:I

.field private id:J

.field private max:D

.field private min:D

.field private recomendedTime:J

.field private sampleTime:J

.field private score:D

.field private state:I

.field private sunProtection:I

.field private updateTime:J

.field private userDeviceId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAverage()D
    .locals 2

    .prologue
    .line 138
    iget-wide v0, p0, Lcom/sec/android/app/shealth/uv/data/UvData;->average:D

    return-wide v0
.end method

.method public getComment()Ljava/lang/String;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/data/UvData;->comment:Ljava/lang/String;

    return-object v0
.end method

.method public getCreateTime()J
    .locals 2

    .prologue
    .line 98
    iget-wide v0, p0, Lcom/sec/android/app/shealth/uv/data/UvData;->createTime:J

    return-wide v0
.end method

.method public getId()J
    .locals 2

    .prologue
    .line 42
    iget-wide v0, p0, Lcom/sec/android/app/shealth/uv/data/UvData;->id:J

    return-wide v0
.end method

.method public getMax()D
    .locals 2

    .prologue
    .line 130
    iget-wide v0, p0, Lcom/sec/android/app/shealth/uv/data/UvData;->max:D

    return-wide v0
.end method

.method public getMin()D
    .locals 2

    .prologue
    .line 122
    iget-wide v0, p0, Lcom/sec/android/app/shealth/uv/data/UvData;->min:D

    return-wide v0
.end method

.method public getRecomendedTime()J
    .locals 2

    .prologue
    .line 24
    iget-wide v0, p0, Lcom/sec/android/app/shealth/uv/data/UvData;->recomendedTime:J

    return-wide v0
.end method

.method public getSampleTime()J
    .locals 2

    .prologue
    .line 90
    iget-wide v0, p0, Lcom/sec/android/app/shealth/uv/data/UvData;->sampleTime:J

    return-wide v0
.end method

.method public getScore()D
    .locals 2

    .prologue
    .line 74
    iget-wide v0, p0, Lcom/sec/android/app/shealth/uv/data/UvData;->score:D

    return-wide v0
.end method

.method public getState()I
    .locals 1

    .prologue
    .line 66
    iget v0, p0, Lcom/sec/android/app/shealth/uv/data/UvData;->state:I

    return v0
.end method

.method public getSunProtection()I
    .locals 1

    .prologue
    .line 32
    iget v0, p0, Lcom/sec/android/app/shealth/uv/data/UvData;->sunProtection:I

    return v0
.end method

.method public getUpdateTime()J
    .locals 2

    .prologue
    .line 106
    iget-wide v0, p0, Lcom/sec/android/app/shealth/uv/data/UvData;->updateTime:J

    return-wide v0
.end method

.method public getUserDeviceId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/data/UvData;->userDeviceId:Ljava/lang/String;

    return-object v0
.end method

.method public setApplicationId(Ljava/lang/String;)V
    .locals 0
    .param p1, "applicationId"    # Ljava/lang/String;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/sec/android/app/shealth/uv/data/UvData;->applicationId:Ljava/lang/String;

    .line 55
    return-void
.end method

.method public setAverage(D)V
    .locals 0
    .param p1, "average"    # D

    .prologue
    .line 142
    iput-wide p1, p0, Lcom/sec/android/app/shealth/uv/data/UvData;->average:D

    .line 143
    return-void
.end method

.method public setComment(Ljava/lang/String;)V
    .locals 0
    .param p1, "comment"    # Ljava/lang/String;

    .prologue
    .line 118
    iput-object p1, p0, Lcom/sec/android/app/shealth/uv/data/UvData;->comment:Ljava/lang/String;

    .line 119
    return-void
.end method

.method public setCreateTime(J)V
    .locals 0
    .param p1, "createTime"    # J

    .prologue
    .line 102
    iput-wide p1, p0, Lcom/sec/android/app/shealth/uv/data/UvData;->createTime:J

    .line 103
    return-void
.end method

.method public setHeartRate(I)V
    .locals 0
    .param p1, "heartRate"    # I

    .prologue
    .line 86
    iput p1, p0, Lcom/sec/android/app/shealth/uv/data/UvData;->heartRate:I

    .line 87
    return-void
.end method

.method public setId(J)V
    .locals 0
    .param p1, "id"    # J

    .prologue
    .line 46
    iput-wide p1, p0, Lcom/sec/android/app/shealth/uv/data/UvData;->id:J

    .line 47
    return-void
.end method

.method public setMax(D)V
    .locals 0
    .param p1, "max"    # D

    .prologue
    .line 134
    iput-wide p1, p0, Lcom/sec/android/app/shealth/uv/data/UvData;->max:D

    .line 135
    return-void
.end method

.method public setMin(D)V
    .locals 0
    .param p1, "min"    # D

    .prologue
    .line 126
    iput-wide p1, p0, Lcom/sec/android/app/shealth/uv/data/UvData;->min:D

    .line 127
    return-void
.end method

.method public setRecomendedTime(J)V
    .locals 0
    .param p1, "recomendedTime"    # J

    .prologue
    .line 28
    iput-wide p1, p0, Lcom/sec/android/app/shealth/uv/data/UvData;->recomendedTime:J

    .line 29
    return-void
.end method

.method public setSampleTime(J)V
    .locals 0
    .param p1, "sampleTime"    # J

    .prologue
    .line 94
    iput-wide p1, p0, Lcom/sec/android/app/shealth/uv/data/UvData;->sampleTime:J

    .line 95
    return-void
.end method

.method public setScore(D)V
    .locals 0
    .param p1, "score"    # D

    .prologue
    .line 78
    iput-wide p1, p0, Lcom/sec/android/app/shealth/uv/data/UvData;->score:D

    .line 79
    return-void
.end method

.method public setState(I)V
    .locals 0
    .param p1, "state"    # I

    .prologue
    .line 70
    iput p1, p0, Lcom/sec/android/app/shealth/uv/data/UvData;->state:I

    .line 71
    return-void
.end method

.method public setSunProtection(I)V
    .locals 0
    .param p1, "sunProtection"    # I

    .prologue
    .line 36
    iput p1, p0, Lcom/sec/android/app/shealth/uv/data/UvData;->sunProtection:I

    .line 37
    return-void
.end method

.method public setUpdateTime(J)V
    .locals 0
    .param p1, "updateTime"    # J

    .prologue
    .line 110
    iput-wide p1, p0, Lcom/sec/android/app/shealth/uv/data/UvData;->updateTime:J

    .line 111
    return-void
.end method

.method public setUserDeviceId(Ljava/lang/String;)V
    .locals 0
    .param p1, "userDeviceId"    # Ljava/lang/String;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/sec/android/app/shealth/uv/data/UvData;->userDeviceId:Ljava/lang/String;

    .line 63
    return-void
.end method
