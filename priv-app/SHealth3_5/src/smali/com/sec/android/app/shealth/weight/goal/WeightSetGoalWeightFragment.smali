.class public Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;
.super Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment;
.source "WeightSetGoalWeightFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerGetter;
.implements Lcom/sec/android/app/shealth/weight/NonBackStackFragment;


# instance fields
.field private mCal:Ljava/util/Calendar;

.field private mDatePicker:Lcom/sec/android/app/shealth/framework/ui/common/DatePicker;

.field private mGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

.field private mGoalEditText:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

.field private mGradationView:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

.field private mTargetDateBtn:Landroid/widget/Button;

.field private mWeightInputModule:Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;

.field private mWeightStartValue:F

.field private mWeightTextView:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment;-><init>()V

    .line 77
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->mCal:Ljava/util/Calendar;

    .line 78
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->mTargetDateBtn:Landroid/widget/Button;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;)Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->getWeightData()Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;)Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->mWeightInputModule:Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;)Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->mGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;)Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    .prologue
    .line 71
    iput-object p1, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->mGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    return-object p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;)Ljava/util/Calendar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->mCal:Ljava/util/Calendar;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->showCancelDialog()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;)Lcom/sec/android/app/shealth/framework/ui/common/DatePicker;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->mDatePicker:Lcom/sec/android/app/shealth/framework/ui/common/DatePicker;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->mTargetDateBtn:Landroid/widget/Button;

    return-object v0
.end method

.method private getWeightData()Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;
    .locals 3

    .prologue
    .line 223
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->getWeightDao()Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;->getLastData(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    move-result-object v0

    return-object v0
.end method

.method private hideSIP()V
    .locals 3

    .prologue
    .line 217
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-string v2, "input_method"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 219
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->forceHideSoftInput()Z

    .line 220
    return-void
.end method

.method private initFooterButtons(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 169
    const v0, 0x7f080c7b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment$1;-><init>(Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 193
    const v0, 0x7f080c7a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment$2;-><init>(Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 200
    return-void
.end method

.method private initWeightValueAndUnit(Landroid/view/View;)V
    .locals 18
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 125
    new-instance v3, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v14

    invoke-direct {v3, v14}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    .line 126
    .local v3, "healthProfile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    new-instance v13, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v14

    invoke-direct {v13, v14}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;-><init>(Landroid/content/Context;)V

    .line 129
    .local v13, "weightUnitHelper":Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->mGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    if-eqz v14, :cond_2

    .line 130
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->mGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    invoke-virtual {v14}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->getValue()F

    move-result v14

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->getCurrentUnit()Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->convertKgToUnit(FLcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;)F

    move-result v2

    .line 131
    .local v2, "goalweight":F
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->mGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    invoke-virtual {v14}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->getPeriod()I

    move-result v14

    int-to-long v8, v14

    .line 132
    .local v8, "period":J
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->mGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    invoke-virtual {v14}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->getSetTime()J

    move-result-wide v14

    const-wide/16 v16, 0x3e8

    mul-long v16, v16, v8

    add-long v10, v14, v16

    .line 133
    .local v10, "targetDate":J
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->mCal:Ljava/util/Calendar;

    invoke-virtual {v14, v10, v11}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 140
    .end local v8    # "period":J
    :goto_0
    const v14, 0x7f080c5c

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->mWeightInputModule:Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;

    .line 141
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->mWeightInputModule:Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;

    const/high16 v15, 0x40000000    # 2.0f

    invoke-virtual {v13}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->getWeightUnit()Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->convertKgToUnit(FLcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;)F

    move-result v15

    const/high16 v16, 0x43fa0000    # 500.0f

    invoke-virtual {v13}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->getWeightUnit()Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->convertKgToUnit(FLcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;)F

    move-result v16

    invoke-virtual/range {v14 .. v16}, Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;->setInputRange(FF)V

    .line 143
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->mWeightInputModule:Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;

    const v15, 0x3dcccccd    # 0.1f

    invoke-virtual {v14, v15}, Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;->setMoveDistance(F)V

    .line 144
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->mWeightInputModule:Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;

    invoke-virtual {v13}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->getWeightUnit()Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    move-result-object v15

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;->getUnitName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;->setUnit(Ljava/lang/CharSequence;)V

    .line 145
    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getHeight()F

    move-result v4

    .line 146
    .local v4, "height":F
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v14

    invoke-static {v14, v4}, Lcom/sec/android/app/shealth/weight/utils/WeightNormalRangeHelper;->getNormalRangeValues(Landroid/content/Context;F)Landroid/util/Pair;

    move-result-object v7

    .line 147
    .local v7, "normalRange":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;"
    sget-object v14, Lcom/sec/android/app/shealth/weight/common/WeightConstants;->APP_TYPE:Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;

    sget-object v15, Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;->DOWNLOAD_MR_EXTENDED:Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;

    invoke-virtual {v14, v15}, Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_0

    .line 148
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->mWeightInputModule:Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;

    iget-object v14, v7, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v14, Ljava/lang/Float;

    invoke-virtual {v14}, Ljava/lang/Float;->floatValue()F

    move-result v16

    iget-object v14, v7, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v14, Ljava/lang/Float;

    invoke-virtual {v14}, Ljava/lang/Float;->floatValue()F

    move-result v14

    move/from16 v0, v16

    invoke-virtual {v15, v0, v14}, Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;->setNormalRange(FF)V

    .line 150
    :cond_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->mWeightInputModule:Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;

    invoke-virtual {v14, v2}, Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;->setValue(F)V

    .line 151
    sget-object v14, Lcom/sec/android/app/shealth/weight/common/WeightConstants;->APP_TYPE:Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;

    sget-object v15, Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;->DOWNLOAD_MR_EXTENDED:Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;

    invoke-virtual {v14, v15}, Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_1

    .line 152
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->mWeightInputModule:Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;

    sget-object v15, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AdditionalDrawingStrategy$AdditionalDrawingType;->EMPTY:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AdditionalDrawingStrategy$AdditionalDrawingType;

    invoke-virtual {v14, v15}, Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;->setAdditionalDrawingType(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AdditionalDrawingStrategy$AdditionalDrawingType;)V

    .line 155
    :cond_1
    const v14, 0x7f080c5b

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/Button;

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->mTargetDateBtn:Landroid/widget/Button;

    .line 156
    new-instance v14, Lcom/sec/android/app/shealth/framework/ui/common/DatePicker;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->mTargetDateBtn:Landroid/widget/Button;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    invoke-direct/range {v14 .. v17}, Lcom/sec/android/app/shealth/framework/ui/common/DatePicker;-><init>(Landroid/widget/Button;J)V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->mDatePicker:Lcom/sec/android/app/shealth/framework/ui/common/DatePicker;

    .line 157
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->mDatePicker:Lcom/sec/android/app/shealth/framework/ui/common/DatePicker;

    invoke-virtual {v14, v10, v11}, Lcom/sec/android/app/shealth/framework/ui/common/DatePicker;->setTimeInMillis(J)V

    .line 159
    const v14, 0x7f08063f

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 160
    .local v1, "goalWeight":Landroid/widget/TextView;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f090cb0

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v1, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 162
    const v14, 0x7f080640

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/TextView;

    .line 163
    .local v12, "weightInputScreenInfo":Landroid/widget/TextView;
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    const v16, 0x7f090cb2

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v13}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->getWeightUnit()Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    move-result-object v15

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;->getUnitName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 166
    return-void

    .line 135
    .end local v1    # "goalWeight":Landroid/widget/TextView;
    .end local v2    # "goalweight":F
    .end local v4    # "height":F
    .end local v7    # "normalRange":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;"
    .end local v10    # "targetDate":J
    .end local v12    # "weightInputScreenInfo":Landroid/widget/TextView;
    :cond_2
    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getWeight()F

    move-result v14

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->getCurrentUnit()Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->convertKgToUnit(FLcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;)F

    move-result v2

    .line 136
    .restart local v2    # "goalweight":F
    const-wide v5, 0x1cf7c5800L

    .line 137
    .local v5, "milliSecFor90Days":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    add-long v10, v14, v5

    .restart local v10    # "targetDate":J
    goto/16 :goto_0
.end method

.method private showCancelDialog()V
    .locals 3

    .prologue
    .line 203
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090048

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090ccd

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment$3;-><init>(Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 214
    return-void
.end method


# virtual methods
.method protected customizeActionBar(Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;)V
    .locals 1
    .param p1, "actionBarHelper"    # Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    .prologue
    .line 233
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment;->customizeActionBar(Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;)V

    .line 234
    const v0, 0x7f090031

    invoke-virtual {p1, v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 235
    return-void
.end method

.method protected getFragmentType()Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment$GoalFragment;
    .locals 1

    .prologue
    .line 228
    sget-object v0, Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment$GoalFragment;->ADD_NEW_GOAL:Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment$GoalFragment;

    return-object v0
.end method

.method public getNegativeButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonController;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 273
    const/4 v0, 0x0

    return-object v0
.end method

.method public getPositiveButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonController;
    .locals 2
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 240
    if-eqz p1, :cond_0

    const-string v1, "date_picker_dialog"

    invoke-virtual {v1, p1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 242
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonController;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonController;-><init>()V

    .line 243
    .local v0, "positiveButtonController":Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonController;
    const v1, 0x7f09004a

    iput v1, v0, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonController;->textResId:I

    .line 244
    new-instance v1, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment$4;-><init>(Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;)V

    iput-object v1, v0, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonController;->btnClickListener:Landroid/view/View$OnClickListener;

    .line 267
    .end local v0    # "positiveButtonController":Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonController;
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected initUIComponents(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 120
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->initWeightValueAndUnit(Landroid/view/View;)V

    .line 121
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->initFooterButtons(Landroid/view/View;)V

    .line 122
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 88
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->getActiveGoal()Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->mGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    .line 89
    const v1, 0x7f0302ba

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 90
    .local v0, "view":Landroid/view/View;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->initUIComponents(Landroid/view/View;)V

    .line 91
    return-object v0
.end method

.method public onDestroyView()V
    .locals 0

    .prologue
    .line 110
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->hideSIP()V

    .line 111
    invoke-super {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment;->onDestroyView()V

    .line 112
    return-void
.end method
