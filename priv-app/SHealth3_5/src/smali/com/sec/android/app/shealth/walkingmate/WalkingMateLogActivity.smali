.class public Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;
.source "WalkingMateLogActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$ActionModeChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity$TitleUpdater;
    }
.end annotation


# static fields
.field public static final FILTER_ALL_WALKING:Ljava/lang/String; = "All"

.field public static final FILTER_GOAL_ACHIEVED_WALKING:Ljava/lang/String; = "Goal Achieved"

.field public static final FILTER_GOAL_NOT_ACHIEVED_WALKING:Ljava/lang/String; = "Goal not achived"

.field public static final LOG_ACTIVITY_REQUEST_CODE:I = 0x65

.field public static SELECTED_FILTER_INDEX:I


# instance fields
.field private final TAG:Ljava/lang/String;

.field cal:Ljava/util/Calendar;

.field dateFormatter:Ljava/text/DateFormat;

.field private isDataAvailable:Z

.field private mCommonSelection:Ljava/lang/StringBuilder;

.field protected mLogAdapter:Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;

.field private mPrivateSelection1:Ljava/lang/StringBuilder;

.field private mPrivateSelection2:Ljava/lang/StringBuilder;

.field sharingdateFormatter:Ljava/text/DateFormat;

.field tagdateFormatter:Ljava/text/DateFormat;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->SELECTED_FILTER_INDEX:I

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;-><init>()V

    .line 49
    const-class v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->TAG:Ljava/lang/String;

    .line 51
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "dd/MM/yyyy"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->dateFormatter:Ljava/text/DateFormat;

    .line 52
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "MM/dd/yyyy"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->tagdateFormatter:Ljava/text/DateFormat;

    .line 53
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->getSystemTimeDateFormat()Ljava/text/SimpleDateFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->sharingdateFormatter:Ljava/text/DateFormat;

    .line 54
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->cal:Ljava/util/Calendar;

    .line 70
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->mCommonSelection:Ljava/lang/StringBuilder;

    .line 71
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->mCommonSelection:Ljava/lang/StringBuilder;

    const-string v1, "SELECT _id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->mCommonSelection:Ljava/lang/StringBuilder;

    const-string v1, " , "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 73
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->mCommonSelection:Ljava/lang/StringBuilder;

    const-string v1, "SUM(total_step) AS total_step"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 74
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->mCommonSelection:Ljava/lang/StringBuilder;

    const-string v1, " , "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 75
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->mCommonSelection:Ljava/lang/StringBuilder;

    const-string/jumbo v1, "start_time"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 76
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->mCommonSelection:Ljava/lang/StringBuilder;

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 77
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->mCommonSelection:Ljava/lang/StringBuilder;

    const-string v1, "FROM walk_info"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 78
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->mCommonSelection:Ljava/lang/StringBuilder;

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 79
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->mCommonSelection:Ljava/lang/StringBuilder;

    const-string v1, "WHERE total_step > 0 "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 81
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->mPrivateSelection1:Ljava/lang/StringBuilder;

    .line 82
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->mPrivateSelection1:Ljava/lang/StringBuilder;

    const-string v1, "SELECT w._id AS _id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 83
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->mPrivateSelection1:Ljava/lang/StringBuilder;

    const-string v1, " , w.start_time AS start_time"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 84
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->mPrivateSelection1:Ljava/lang/StringBuilder;

    const-string v1, " , SUM(w.total_step) AS total_step"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 85
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->mPrivateSelection1:Ljava/lang/StringBuilder;

    const-string v1, " , (SELECT value"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->mPrivateSelection1:Ljava/lang/StringBuilder;

    const-string v1, " FROM goal WHERE goal_type=40001 "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 88
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->mPrivateSelection2:Ljava/lang/StringBuilder;

    .line 89
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->mPrivateSelection2:Ljava/lang/StringBuilder;

    const-string v1, " AND set_time < w.eod"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 90
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->mPrivateSelection2:Ljava/lang/StringBuilder;

    const-string v1, " ORDER BY set_time DESC LIMIT 1) as goal,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 91
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->mPrivateSelection2:Ljava/lang/StringBuilder;

    const-string v1, " strftime(\'%m-%Y\', w.start_time / 1000, \'unixepoch\') AS date"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 92
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->mPrivateSelection2:Ljava/lang/StringBuilder;

    const-string v1, " FROM (SELECT _id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 93
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->mPrivateSelection2:Ljava/lang/StringBuilder;

    const-string v1, " , SUM(total_step) AS total_step"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 94
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->mPrivateSelection2:Ljava/lang/StringBuilder;

    const-string v1, " , start_time"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 95
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->mPrivateSelection2:Ljava/lang/StringBuilder;

    const-string v1, " , strftime(\'%s\',datetime(datetime(start_time/1000, \'unixepoch\'), \'start of day\', \'+1 day\', \'-1 second\'))*1000 AS eod"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 96
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->mPrivateSelection2:Ljava/lang/StringBuilder;

    const-string v1, " FROM walk_info"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 97
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->mPrivateSelection2:Ljava/lang/StringBuilder;

    const-string v1, " WHERE total_step > 0"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 98
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->mPrivateSelection2:Ljava/lang/StringBuilder;

    const-string v1, " GROUP BY strftime(\"%d-%m-%Y\",([start_time]/1000),\'unixepoch\',\'localtime\')"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 99
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->mPrivateSelection2:Ljava/lang/StringBuilder;

    const-string v1, " ORDER BY start_time DESC) AS w"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 100
    return-void
.end method

.method private StartGearLogDetailActivity(Ljava/lang/String;)V
    .locals 9
    .param p1, "tag"    # Ljava/lang/String;

    .prologue
    .line 492
    const-string v7, "_"

    invoke-virtual {p1, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    aget-object v0, v7, v8

    .line 493
    .local v0, "date":Ljava/lang/String;
    const-string v7, "_"

    invoke-virtual {p1, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x2

    aget-object v3, v7, v8

    .line 494
    .local v3, "gearDeviceNames":Ljava/lang/String;
    const-string v7, "_"

    invoke-virtual {p1, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x3

    aget-object v6, v7, v8

    .line 497
    .local v6, "totalSteps":Ljava/lang/String;
    const-string v7, ","

    invoke-virtual {v3, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 499
    .local v1, "deviceNames":[Ljava/lang/String;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    array-length v7, v1

    if-ge v4, v7, :cond_0

    .line 500
    aget-object v7, v1, v4

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v1, v4

    .line 499
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 504
    :cond_0
    new-instance v5, Landroid/content/Intent;

    const-class v7, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;

    invoke-direct {v5, p0, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 507
    .local v5, "intent":Landroid/content/Intent;
    :try_start_0
    const-string v7, "DATE"

    invoke-virtual {v5, v7, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 508
    const-string v7, "TODAY_STPES"

    invoke-virtual {v5, v7, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 509
    const-string v7, "DEVICE_NAMES"

    invoke-virtual {v5, v7, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 510
    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 516
    :goto_1
    return-void

    .line 511
    :catch_0
    move-exception v2

    .line 512
    .local v2, "e":Landroid/content/ActivityNotFoundException;
    invoke-virtual {v2}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_1

    .line 513
    .end local v2    # "e":Landroid/content/ActivityNotFoundException;
    :catch_1
    move-exception v2

    .line 514
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method private StartLogDetailActivity(Ljava/lang/String;I)V
    .locals 21
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "deviceType"    # I

    .prologue
    .line 405
    const/16 v20, 0x0

    .line 406
    .local v20, "total_steps":I
    const/4 v11, 0x0

    .line 407
    .local v11, "distance":F
    const/4 v8, 0x0

    .line 408
    .local v8, "calories":F
    const-wide/16 v16, 0x0

    .line 409
    .local v16, "start_time":J
    const/4 v14, 0x0

    .line 411
    .local v14, "running_steps":I
    const-string v2, "_"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aget-object v10, v2, v3

    .line 412
    .local v10, "dateString":Ljava/lang/String;
    if-nez v10, :cond_1

    .line 488
    :cond_0
    :goto_0
    return-void

    .line 415
    :cond_1
    const-wide/16 v18, 0x0

    .line 418
    .local v18, "time":J
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->dateFormatter:Ljava/text/DateFormat;

    invoke-virtual {v2, v10}, Ljava/text/DateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-wide v18

    .line 422
    :goto_1
    const-wide/16 v2, 0x0

    cmp-long v2, v18, v2

    if-eqz v2, :cond_0

    .line 425
    const/4 v9, 0x0

    .line 428
    .local v9, "cursor":Landroid/database/Cursor;
    :try_start_1
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    .line 430
    .local v15, "selection":Ljava/lang/StringBuilder;
    const/4 v2, 0x0

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 431
    const-string v2, "SELECT"

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 432
    const-string v2, " "

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 433
    const-string v2, "SUM(total_step) AS total_step"

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 434
    const-string v2, " , "

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 435
    const-string v2, "SUM(distance) AS distance"

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 436
    const-string v2, " , "

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 437
    const-string v2, "SUM(calorie) AS calorie"

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 438
    const-string v2, " , "

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 439
    const-string v2, "SUM(run_step) AS run_step"

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 440
    const-string v2, " , "

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 441
    const-string/jumbo v2, "start_time"

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 442
    const-string v2, " , "

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 443
    const-string v2, "end_time"

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 444
    const-string v2, " "

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 445
    const-string v2, "FROM walk_info"

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 446
    const-string v2, " "

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 447
    const-string v2, "WHERE"

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 448
    const-string v2, " "

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 449
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "start_time >= "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static/range {v18 .. v19}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 450
    const-string v2, " AND "

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 451
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "start_time <= "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static/range {v18 .. v19}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 452
    const-string v2, " AND "

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 453
    invoke-static/range {p2 .. p2}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->getQueryConditionByDeviceType(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 455
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 456
    if-eqz v9, :cond_2

    .line 457
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    .line 458
    const-string/jumbo v2, "total_step"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v20

    .line 459
    const-string/jumbo v2, "start_time"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    .line 460
    const-string v2, "distance"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v11

    .line 461
    const-string v2, "calorie"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v8

    .line 462
    const-string/jumbo v2, "run_step"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v14

    .line 465
    :cond_2
    if-eqz v9, :cond_3

    .line 466
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 470
    :cond_3
    new-instance v13, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;

    move-object/from16 v0, p0

    invoke-direct {v13, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 473
    .local v13, "intent":Landroid/content/Intent;
    :try_start_2
    const-string v2, "data_type"

    move/from16 v0, p2

    invoke-virtual {v13, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 474
    const-string v2, "distance"

    invoke-virtual {v13, v2, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;

    .line 475
    const-string/jumbo v2, "pick_calories"

    invoke-virtual {v13, v2, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;

    .line 476
    const-string/jumbo v2, "starttime"

    move-wide/from16 v0, v16

    invoke-virtual {v13, v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 477
    const-string/jumbo v2, "totalstep"

    move/from16 v0, v20

    invoke-virtual {v13, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 479
    const-string/jumbo v2, "runningsteps"

    invoke-virtual {v13, v2, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 480
    const-string/jumbo v2, "tag"

    move-object/from16 v0, p1

    invoke-virtual {v13, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 481
    const/16 v2, 0x65

    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_2
    .catch Landroid/content/ActivityNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_0

    .line 483
    :catch_0
    move-exception v12

    .line 484
    .local v12, "e":Landroid/content/ActivityNotFoundException;
    invoke-virtual {v12}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto/16 :goto_0

    .line 419
    .end local v9    # "cursor":Landroid/database/Cursor;
    .end local v12    # "e":Landroid/content/ActivityNotFoundException;
    .end local v13    # "intent":Landroid/content/Intent;
    .end local v15    # "selection":Ljava/lang/StringBuilder;
    :catch_1
    move-exception v12

    .line 420
    .local v12, "e":Ljava/text/ParseException;
    invoke-virtual {v12}, Ljava/text/ParseException;->printStackTrace()V

    goto/16 :goto_1

    .line 465
    .end local v12    # "e":Ljava/text/ParseException;
    .restart local v9    # "cursor":Landroid/database/Cursor;
    :catchall_0
    move-exception v2

    if-eqz v9, :cond_4

    .line 466
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v2

    .line 485
    .restart local v13    # "intent":Landroid/content/Intent;
    .restart local v15    # "selection":Ljava/lang/StringBuilder;
    :catch_2
    move-exception v12

    .line 486
    .local v12, "e":Ljava/lang/Exception;
    invoke-virtual {v12}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;)Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;)Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    return-object v0
.end method

.method private executeQuery(Ljava/lang/CharSequence;Z)V
    .locals 10
    .param p1, "constraint"    # Ljava/lang/CharSequence;
    .param p2, "isDeleteMode"    # Z

    .prologue
    const/4 v2, 0x0

    .line 149
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->TAG:Ljava/lang/String;

    const-string v1, " "

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    const v0, 0x7f09005a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 151
    .local v9, "title":Ljava/lang/String;
    if-eqz p1, :cond_0

    .line 152
    const-string v0, "Goal Achieved"

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 153
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f090077

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 158
    :cond_0
    :goto_0
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity$TitleUpdater;

    invoke-direct {v0, p0, v9}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity$TitleUpdater;-><init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 160
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v7

    .line 161
    .local v7, "deviceType":I
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->refreshGoalInfo()V

    .line 162
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 165
    .local v8, "selection":Ljava/lang/StringBuilder;
    const/4 v6, 0x0

    .line 166
    .local v6, "c":Landroid/database/Cursor;
    if-eqz p1, :cond_1

    .line 167
    const-string v0, "All"

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 168
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->mCommonSelection:Ljava/lang/StringBuilder;

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 169
    const-string v0, " AND "

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 170
    invoke-static {v7}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->getQueryConditionByDeviceType(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 171
    const-string v0, " GROUP BY (strftime(\"%m-%Y\",([start_time]/1000),\'unixepoch\',\'localtime\'))"

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 172
    const-string v0, " ORDER BY start_time DESC"

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 173
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 194
    :cond_1
    :goto_1
    if-nez v6, :cond_5

    .line 213
    :goto_2
    return-void

    .line 154
    .end local v6    # "c":Landroid/database/Cursor;
    .end local v7    # "deviceType":I
    .end local v8    # "selection":Ljava/lang/StringBuilder;
    :cond_2
    const-string v0, "Goal not achived"

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 155
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f090078

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_0

    .line 174
    .restart local v6    # "c":Landroid/database/Cursor;
    .restart local v7    # "deviceType":I
    .restart local v8    # "selection":Ljava/lang/StringBuilder;
    :cond_3
    const-string v0, "Goal Achieved"

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 175
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->mPrivateSelection1:Ljava/lang/StringBuilder;

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 176
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v7}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->getQueryConditionByDeviceType(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 177
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->mPrivateSelection2:Ljava/lang/StringBuilder;

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 178
    const-string v0, " WHERE w.total_step >= goal"

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 179
    const-string v0, " GROUP BY strftime(\"%m-%Y\",([start_time]/1000),\'unixepoch\',\'localtime\')"

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 180
    const-string v0, " ORDER BY start_time DESC;"

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 181
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    goto/16 :goto_1

    .line 183
    :cond_4
    const-string v0, "Goal not achived"

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 184
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->mPrivateSelection1:Ljava/lang/StringBuilder;

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 185
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v7}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->getQueryConditionByDeviceType(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 186
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->mPrivateSelection2:Ljava/lang/StringBuilder;

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 187
    const-string v0, " WHERE w.total_step < goal"

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 188
    const-string v0, " GROUP BY strftime(\"%m-%Y\",([start_time]/1000),\'unixepoch\',\'localtime\')"

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 189
    const-string v0, " ORDER BY start_time DESC;"

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 190
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    goto/16 :goto_1

    .line 199
    :cond_5
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_6

    const/4 v0, 0x1

    :goto_3
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->isDataAvailable:Z

    .line 201
    if-nez p2, :cond_7

    .line 202
    iput-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->mCursor:Landroid/database/Cursor;

    goto/16 :goto_2

    .line 199
    :cond_6
    const/4 v0, 0x0

    goto :goto_3

    .line 205
    :cond_7
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_2

    .line 206
    :catch_0
    move-exception v0

    goto/16 :goto_2
.end method

.method private resetDataInService([J)V
    .locals 11
    .param p1, "time"    # [J

    .prologue
    .line 592
    iget-object v8, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v9, "resetDataInService"

    invoke-static {v8, v9}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 593
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-static {v8, v9}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v1

    .line 595
    .local v1, "endDayOfCurrentTime":J
    array-length v7, p1

    .line 596
    .local v7, "timeArrayLength":I
    const/4 v0, -0x1

    .line 598
    .local v0, "currentTimeIndex":I
    const/4 v5, 0x0

    .local v5, "index":I
    :goto_0
    if-ge v5, v7, :cond_0

    .line 599
    aget-wide v8, p1, v5

    invoke-static {v8, v9}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v3

    .line 600
    .local v3, "endDayOfTime":J
    cmp-long v8, v3, v1

    if-nez v8, :cond_1

    .line 601
    move v0, v5

    .line 605
    .end local v3    # "endDayOfTime":J
    :cond_0
    new-instance v6, Landroid/content/Intent;

    const-string v8, "com.sec.android.app.shealth.command.deletetotalstep"

    invoke-direct {v6, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 606
    .local v6, "intent":Landroid/content/Intent;
    const/4 v8, -0x1

    if-eq v0, v8, :cond_2

    .line 607
    const-string/jumbo v8, "selectedDate"

    aget-wide v9, p1, v0

    invoke-virtual {v6, v8, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 610
    :goto_1
    const-string/jumbo v8, "selectedDateList"

    invoke-virtual {v6, v8, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[J)Landroid/content/Intent;

    .line 611
    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 612
    return-void

    .line 598
    .end local v6    # "intent":Landroid/content/Intent;
    .restart local v3    # "endDayOfTime":J
    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 609
    .end local v3    # "endDayOfTime":J
    .restart local v6    # "intent":Landroid/content/Intent;
    :cond_2
    const-string/jumbo v8, "selectedDate"

    const/4 v9, 0x0

    aget-wide v9, p1, v9

    invoke-virtual {v6, v8, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    goto :goto_1
.end method


# virtual methods
.method protected applyFilter(Ljava/lang/CharSequence;)V
    .locals 2
    .param p1, "constraint"    # Ljava/lang/CharSequence;

    .prologue
    .line 294
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->TAG:Ljava/lang/String;

    const-string v1, " "

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->executeQuery(Ljava/lang/CharSequence;Z)V

    .line 296
    return-void
.end method

.method protected customizeActionBar()V
    .locals 2

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->TAG:Ljava/lang/String;

    const-string v1, " "

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->customizeActionBar()V

    .line 134
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    .line 135
    return-void
.end method

.method public enterActionMode()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 661
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->TAG:Ljava/lang/String;

    const-string v3, " "

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 662
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v2

    const/16 v3, 0x2719

    if-ne v2, v3, :cond_0

    move v0, v1

    .line 663
    .local v0, "showDeleteOption":Z
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    invoke-virtual {v2, v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarButtonVisible(ZI)V

    .line 664
    return-void

    .line 662
    .end local v0    # "showDeleteOption":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public exitActionMode()V
    .locals 2

    .prologue
    .line 668
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->TAG:Ljava/lang/String;

    const-string v1, " "

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 669
    return-void
.end method

.method protected getAdatper()Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;
    .locals 3

    .prologue
    .line 139
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->TAG:Ljava/lang/String;

    const-string v1, " "

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    const-string v0, "All"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->executeQuery(Ljava/lang/CharSequence;Z)V

    .line 141
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "cursor size : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->mCursor:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->mCursor:Landroid/database/Cursor;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;-><init>(Landroid/database/Cursor;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->mLogAdapter:Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;

    .line 143
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->mLogAdapter:Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;

    return-object v0
.end method

.method protected getColumnNameForMemo()Ljava/lang/String;
    .locals 2

    .prologue
    .line 400
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->TAG:Ljava/lang/String;

    const-string v1, " "

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 401
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getContextMenuHeader(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 10
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v9, 0x0

    .line 266
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->TAG:Ljava/lang/String;

    const-string v7, " "

    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 267
    const/4 v2, 0x0

    .line 268
    .local v2, "header":Ljava/lang/String;
    const-string/jumbo v6, "total_step"

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getFloat(I)F

    move-result v3

    .line 270
    .local v3, "steps":F
    const-string/jumbo v6, "start_time"

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 271
    .local v4, "time":J
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->cal:Ljava/util/Calendar;

    invoke-virtual {v6, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 272
    new-instance v6, Ljava/text/DateFormatSymbols;

    invoke-direct {v6}, Ljava/text/DateFormatSymbols;-><init>()V

    invoke-virtual {v6}, Ljava/text/DateFormatSymbols;->getWeekdays()[Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->cal:Ljava/util/Calendar;

    const/4 v8, 0x7

    invoke-virtual {v7, v8}, Ljava/util/Calendar;->get(I)I

    move-result v7

    aget-object v1, v6, v7

    .line 273
    .local v1, "dname":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->dateFormatter:Ljava/text/DateFormat;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 274
    .local v0, "date":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v7, 0x5

    invoke-virtual {v0, v9, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/4 v7, 0x3

    invoke-virtual {v1, v9, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ") | "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 275
    return-object v2
.end method

.method protected getDeleteList(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .local p1, "selectedTagList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v13, 0x0

    .line 300
    iget-object v10, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->TAG:Ljava/lang/String;

    const-string v11, " "

    invoke-static {v10, v11}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    const-wide/16 v8, 0x0

    .line 302
    .local v8, "time":J
    const/4 v1, 0x0

    .line 304
    .local v1, "deleted_time":[J
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 306
    .local v0, "deleteList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 308
    .local v5, "sb":Ljava/lang/StringBuilder;
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-lez v10, :cond_2

    .line 309
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 310
    .local v6, "selectedTagSize":I
    if-lez v6, :cond_0

    .line 311
    new-array v1, v6, [J

    .line 314
    :cond_0
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-ge v4, v6, :cond_1

    .line 315
    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    const-string v11, "_"

    invoke-virtual {v10, v11}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v10

    aget-object v7, v10, v13

    .line 318
    .local v7, "tag":Ljava/lang/String;
    :try_start_0
    iget-object v10, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->dateFormatter:Ljava/text/DateFormat;

    invoke-virtual {v10, v7}, Ljava/text/DateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/Date;->getTime()J
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v8

    .line 324
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v2

    .line 325
    .local v2, "deviceType":I
    aput-wide v8, v1, v4

    .line 327
    invoke-virtual {v5, v13}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 328
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "start_time >= "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static {v8, v9}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v11

    invoke-virtual {v10, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 329
    const-string v10, " AND "

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 330
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "start_time <= "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static {v8, v9}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v11

    invoke-virtual {v10, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 331
    const-string v10, " AND "

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 332
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->getQueryConditionByDeviceType(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 333
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 314
    .end local v2    # "deviceType":I
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 319
    :catch_0
    move-exception v3

    .line 320
    .local v3, "e":Ljava/text/ParseException;
    invoke-virtual {v3}, Ljava/text/ParseException;->printStackTrace()V

    goto :goto_1

    .line 336
    .end local v3    # "e":Ljava/text/ParseException;
    .end local v7    # "tag":Ljava/lang/String;
    :cond_1
    if-lez v6, :cond_2

    .line 337
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v10

    invoke-static {v10}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    move-result-object v10

    invoke-virtual {v10, v1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->deleteListbyTime([J)V

    .line 338
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->deleteExercise([J)V

    .line 339
    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->resetDataInService([J)V

    .line 343
    .end local v4    # "i":I
    .end local v6    # "selectedTagSize":I
    :cond_2
    sget v10, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->SELECTED_FILTER_INDEX:I

    invoke-virtual {p0, v10}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->getFilterType(I)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x1

    invoke-direct {p0, v10, v11}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->executeQuery(Ljava/lang/CharSequence;Z)V

    .line 345
    return-object v0
.end method

.method protected getFilterRange()Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 375
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->TAG:Ljava/lang/String;

    const-string v2, " "

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 376
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 377
    .local v0, "filterList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090076

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 378
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090077

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 379
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090078

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 380
    return-object v0
.end method

.method protected getFilterType(I)Ljava/lang/String;
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 385
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->TAG:Ljava/lang/String;

    const-string v1, " "

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 386
    sput p1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->SELECTED_FILTER_INDEX:I

    .line 387
    if-nez p1, :cond_0

    .line 388
    const-string v0, "All"

    .line 394
    :goto_0
    return-object v0

    .line 389
    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 390
    const-string v0, "Goal Achieved"

    goto :goto_0

    .line 391
    :cond_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_2

    .line 392
    const-string v0, "Goal not achived"

    goto :goto_0

    .line 394
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getLogDataTypeURI()Landroid/net/Uri;
    .locals 2

    .prologue
    .line 260
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->TAG:Ljava/lang/String;

    const-string v1, " "

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    sget-object v0, Lcom/samsung/android/sdk/health/content/ShealthContract$WalkInfo;->CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method

.method protected getSelectedLogDataForSharing()Ljava/lang/String;
    .locals 23

    .prologue
    .line 520
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->TAG:Ljava/lang/String;

    const-string v2, " "

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 521
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    .line 522
    .local v17, "shareData":Ljava/lang/StringBuilder;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->mLogAdapter:Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->getCheckedItems()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v18

    .line 523
    .local v18, "size":I
    if-nez v18, :cond_0

    .line 524
    const-string v1, ""

    .line 588
    :goto_0
    return-object v1

    .line 526
    :cond_0
    new-instance v12, Landroid/util/LongSparseArray;

    invoke-direct {v12}, Landroid/util/LongSparseArray;-><init>()V

    .line 528
    .local v12, "hashMap":Landroid/util/LongSparseArray;, "Landroid/util/LongSparseArray<Ljava/lang/Long;>;"
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_1
    move/from16 v0, v18

    if-ge v13, v0, :cond_1

    .line 530
    const-wide/16 v20, 0x0

    .line 533
    .local v20, "time":J
    :try_start_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->mLogAdapter:Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->getCheckedItems()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aget-object v19, v1, v2

    .line 534
    .local v19, "tag":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->dateFormatter:Ljava/text/DateFormat;

    move-object/from16 v0, v19

    invoke-virtual {v1, v0}, Ljava/text/DateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-wide v20

    .line 540
    :try_start_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->mLogAdapter:Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->getCheckedItems()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v14

    .line 542
    .local v14, "id":J
    invoke-static/range {v20 .. v21}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v1

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v12, v1, v2, v3}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 528
    .end local v14    # "id":J
    .end local v19    # "tag":Ljava/lang/String;
    :goto_2
    add-int/lit8 v13, v13, 0x1

    goto :goto_1

    .line 535
    :catch_0
    move-exception v10

    .line 536
    .local v10, "e":Ljava/text/ParseException;
    invoke-virtual {v10}, Ljava/text/ParseException;->printStackTrace()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    .line 543
    .end local v10    # "e":Ljava/text/ParseException;
    :catch_1
    move-exception v11

    .line 544
    .local v11, "ex":Ljava/lang/Exception;
    invoke-virtual {v11}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 547
    .end local v11    # "ex":Ljava/lang/Exception;
    .end local v20    # "time":J
    :cond_1
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v9

    .line 548
    .local v9, "deviceType":I
    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    .line 550
    .local v16, "selection":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 551
    const-string v1, "SELECT "

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 552
    const-string v1, "SUM(total_step) AS total_step"

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 553
    const-string v1, " , "

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 554
    const-string/jumbo v1, "start_time"

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 555
    const-string v1, " "

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 556
    const-string v1, "FROM walk_info"

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 557
    const-string v1, " "

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 558
    const-string v1, "WHERE"

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 559
    const-string v1, " "

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 560
    invoke-static {v9}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->getQueryConditionByDeviceType(I)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 561
    const-string v1, " "

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 562
    const-string v1, "GROUP BY (strftime(\"%d-%m-%Y\",([start_time]/1000),\'unixepoch\',\'localtime\'))"

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 563
    const-string v1, " "

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 564
    const-string v1, "ORDER BY start_time ASC"

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 566
    const/4 v7, 0x0

    .line 568
    .local v7, "cursor":Landroid/database/Cursor;
    :try_start_2
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 569
    if-eqz v7, :cond_4

    .line 570
    :cond_2
    :goto_3
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 571
    const-string/jumbo v1, "total_step"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v22

    .line 572
    .local v22, "totalStep":I
    const-string/jumbo v1, "start_time"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v20

    .line 573
    .restart local v20    # "time":J
    invoke-static/range {v20 .. v21}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v1

    invoke-virtual {v12, v1, v2}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 574
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->sharingdateFormatter:Ljava/text/DateFormat;

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 575
    .local v8, "date":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f090b65

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 576
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    move/from16 v0, v22

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 577
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f0909d9

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 578
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f090135

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_3

    .line 583
    .end local v8    # "date":Ljava/lang/String;
    .end local v20    # "time":J
    .end local v22    # "totalStep":I
    :catchall_0
    move-exception v1

    if-eqz v7, :cond_3

    .line 584
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v1

    .line 583
    :cond_4
    if-eqz v7, :cond_5

    .line 584
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 587
    :cond_5
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "com.sec.android.app.shealth.walkingmate"

    const-string v3, "W021"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 588
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0
.end method

.method protected isMemoVisible(Landroid/database/Cursor;)Z
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 281
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->TAG:Ljava/lang/String;

    const-string v1, " "

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    const/4 v0, 0x0

    return v0
.end method

.method protected loadingCompleted()V
    .locals 4

    .prologue
    .line 363
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->mLogAdapter:Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->getGroupCount()I

    move-result v0

    .line 364
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "index":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 365
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->mExpandableListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v2, v1}, Landroid/widget/ExpandableListView;->expandGroup(I)Z

    .line 366
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "view refreshed"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 364
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 368
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->TAG:Ljava/lang/String;

    const-string v3, "loading completed"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 369
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 4
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 350
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->TAG:Ljava/lang/String;

    const-string v3, " "

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 352
    const/16 v2, 0x65

    if-ne p2, v2, :cond_0

    .line 353
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string/jumbo v3, "time"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 354
    .local v0, "time":J
    const/4 v2, 0x1

    new-array v2, v2, [J

    const/4 v3, 0x0

    aput-wide v0, v2, v3

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->resetDataInService([J)V

    .line 356
    sget v2, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->SELECTED_FILTER_INDEX:I

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->getFilterType(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->applyFilter(Ljava/lang/CharSequence;)V

    .line 357
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->refreshAdapter()V

    .line 359
    .end local v0    # "time":J
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 104
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->TAG:Ljava/lang/String;

    const-string v2, " "

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    const/4 v1, 0x0

    sput v1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->SELECTED_FILTER_INDEX:I

    .line 106
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/service/health/keyManager/KeyManager;->isContentProviderAccessible()Z

    move-result v1

    if-nez v1, :cond_0

    .line 108
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->TAG:Ljava/lang/String;

    const-string v2, "CP is not accessible!!"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.shealth.SplashScreenActivity.restart"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 110
    .local v0, "i":Landroid/content/Intent;
    const-string/jumbo v1, "temps"

    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/service/health/keyManager/KeyManager;->getRandomKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 111
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 112
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 113
    const/high16 v1, 0x20000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 114
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->startActivity(Landroid/content/Intent;)V

    .line 116
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v1

    invoke-static {v1}, Landroid/os/Process;->killProcess(I)V

    .line 118
    .end local v0    # "i":Landroid/content/Intent;
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->onCreate(Landroid/os/Bundle;)V

    .line 120
    const v1, 0x7f0205b7

    const v2, 0x7f09070c

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->setNoLogImageAndText(II)V

    .line 121
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "com.sec.android.app.shealth.walkingmate"

    const-string v3, "W003"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    invoke-virtual {p0, p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->setActionModeListener(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$ActionModeChangeListener;)V

    .line 128
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 616
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->TAG:Ljava/lang/String;

    const-string v1, " "

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 617
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f10002d

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 618
    const/4 v0, 0x1

    return v0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 623
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->TAG:Ljava/lang/String;

    const-string v2, " "

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 624
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    .line 625
    .local v0, "isLogadapter":Z
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->mLogAdapter:Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->isMenuDeleteMode()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 626
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    .line 628
    :cond_0
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->isDataAvailable:Z

    if-eqz v1, :cond_1

    sget v1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->SELECTED_FILTER_INDEX:I

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->getFilterType(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "Goal Achieved"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    sget v1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->SELECTED_FILTER_INDEX:I

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->getFilterType(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "Goal not achived"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 630
    const/4 v0, 0x0

    .line 633
    :cond_1
    return v0
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 217
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->TAG:Ljava/lang/String;

    const-string v1, " "

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->onResume()V

    .line 219
    return-void
.end method

.method protected setUpSelectMode()V
    .locals 2

    .prologue
    .line 639
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->TAG:Ljava/lang/String;

    const-string v1, " "

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 641
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->setUpSelectMode()V

    .line 643
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 657
    :goto_0
    :sswitch_0
    return-void

    .line 654
    :sswitch_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->disableActionBarButton(I)V

    goto :goto_0

    .line 643
    nop

    :sswitch_data_0
    .sparse-switch
        0x2719 -> :sswitch_0
        0x2723 -> :sswitch_1
        0x2724 -> :sswitch_1
        0x2726 -> :sswitch_1
        0x2727 -> :sswitch_1
        0x2728 -> :sswitch_1
        0x272e -> :sswitch_1
        0x272f -> :sswitch_1
        0x2730 -> :sswitch_1
    .end sparse-switch
.end method

.method protected showDetailScreen(Ljava/lang/Object;)V
    .locals 7
    .param p1, "tagObj"    # Ljava/lang/Object;

    .prologue
    .line 230
    if-nez p1, :cond_0

    .line 255
    :goto_0
    return-void

    :cond_0
    move-object v4, p1

    .line 234
    check-cast v4, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter$Tag;

    .line 236
    .local v4, "tagInstance":Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter$Tag;
    invoke-virtual {v4}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter$Tag;->uniqueTag()Ljava/lang/String;

    move-result-object v3

    .line 238
    .local v3, "tag":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->TAG:Ljava/lang/String;

    const-string v6, " "

    invoke-static {v5, v6}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v1

    .line 241
    .local v1, "deviceType":I
    const/16 v5, 0x272f

    if-ne v1, v5, :cond_2

    .line 243
    const-string v5, "_"

    invoke-virtual {v3, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x2

    aget-object v2, v5, v6

    .line 244
    .local v2, "gearDeviceNames":Ljava/lang/String;
    const-string v5, ","

    invoke-virtual {v2, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 246
    .local v0, "deviceNames":[Ljava/lang/String;
    array-length v5, v0

    const/4 v6, 0x1

    if-le v5, v6, :cond_1

    .line 247
    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->StartGearLogDetailActivity(Ljava/lang/String;)V

    goto :goto_0

    .line 249
    :cond_1
    invoke-direct {p0, v3, v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->StartLogDetailActivity(Ljava/lang/String;I)V

    goto :goto_0

    .line 253
    .end local v0    # "deviceNames":[Ljava/lang/String;
    .end local v2    # "gearDeviceNames":Ljava/lang/String;
    :cond_2
    invoke-direct {p0, v3, v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->StartLogDetailActivity(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method protected showDetailScreen(Ljava/lang/String;)V
    .locals 0
    .param p1, "tag"    # Ljava/lang/String;

    .prologue
    .line 224
    return-void
.end method

.method protected updateMemo(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "_id"    # Ljava/lang/String;
    .param p2, "comment"    # Ljava/lang/String;

    .prologue
    .line 288
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->TAG:Ljava/lang/String;

    const-string v1, " "

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    return-void
.end method
