.class Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector$4;
.super Landroid/os/CountDownTimer;
.source "HeartrateDeviceConnector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;JJ)V
    .locals 0
    .param p2, "x0"    # J
    .param p4, "x1"    # J

    .prologue
    .line 398
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector$4;->this$0:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    invoke-direct {p0, p2, p3, p4, p5}, Landroid/os/CountDownTimer;-><init>(JJ)V

    return-void
.end method


# virtual methods
.method public onFinish()V
    .locals 2

    .prologue
    .line 409
    # getter for: Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->access$200()Ljava/lang/String;

    move-result-object v0

    const-string v1, "CountDownTimer onFinish!!"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 410
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector$4;->this$0:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    # getter for: Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->mHeartrateSensorListener:Lcom/sec/android/app/shealth/heartrate/HeartrateSensorListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->access$500(Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;)Lcom/sec/android/app/shealth/heartrate/HeartrateSensorListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 411
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector$4;->this$0:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    # getter for: Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->mHeartrateSensorListener:Lcom/sec/android/app/shealth/heartrate/HeartrateSensorListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->access$500(Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;)Lcom/sec/android/app/shealth/heartrate/HeartrateSensorListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSensorListener;->onTimeout()V

    .line 412
    :cond_0
    return-void
.end method

.method public onTick(J)V
    .locals 2
    .param p1, "millisUntilFinished"    # J

    .prologue
    .line 402
    # getter for: Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->access$200()Ljava/lang/String;

    move-result-object v0

    const-string v1, "CountDownTimer onTick: "

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 403
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector$4;->this$0:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    # getter for: Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->mHeartrateSensorListener:Lcom/sec/android/app/shealth/heartrate/HeartrateSensorListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->access$500(Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;)Lcom/sec/android/app/shealth/heartrate/HeartrateSensorListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 404
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector$4;->this$0:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    # getter for: Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->mHeartrateSensorListener:Lcom/sec/android/app/shealth/heartrate/HeartrateSensorListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->access$500(Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;)Lcom/sec/android/app/shealth/heartrate/HeartrateSensorListener;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/sec/android/app/shealth/heartrate/HeartrateSensorListener;->onTick(J)V

    .line 405
    :cond_0
    return-void
.end method
