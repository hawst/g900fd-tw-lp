.class Lcom/sec/android/app/shealth/home/HomeActivity$12;
.super Ljava/lang/Object;
.source "HomeActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/settings/SHealthUpdation$OnAppUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/home/HomeActivity;->checkForUpdates()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/home/HomeActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/home/HomeActivity;)V
    .locals 0

    .prologue
    .line 1599
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/HomeActivity$12;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResult(ZZ)V
    .locals 2
    .param p1, "result"    # Z
    .param p2, "isForced"    # Z

    .prologue
    .line 1602
    if-eqz p1, :cond_0

    .line 1603
    const-string v0, "SHealthUpdation"

    const-string v1, "SHealthUpdation : Update!!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1604
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity$12;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/home/HomeActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1605
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity$12;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    # invokes: Lcom/sec/android/app/shealth/home/HomeActivity;->startUpdateActivity(Z)V
    invoke-static {v0, p2}, Lcom/sec/android/app/shealth/home/HomeActivity;->access$2200(Lcom/sec/android/app/shealth/home/HomeActivity;Z)V

    .line 1608
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity$12;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    # getter for: Lcom/sec/android/app/shealth/home/HomeActivity;->updateCheck:Lcom/sec/android/app/shealth/settings/SHealthUpdation;
    invoke-static {v0}, Lcom/sec/android/app/shealth/home/HomeActivity;->access$2300(Lcom/sec/android/app/shealth/home/HomeActivity;)Lcom/sec/android/app/shealth/settings/SHealthUpdation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->unregisterAppUpdateListener()V

    .line 1609
    return-void
.end method
