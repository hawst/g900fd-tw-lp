.class public Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;
.super Ljava/lang/Object;
.source "SwipeTouchListener.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener$DismissCallbacks;
    }
.end annotation


# instance fields
.field private mAnimationTime:J

.field private mCallbacks:Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener$DismissCallbacks;

.field private mDownX:F

.field private mDownY:F

.field private mMaxFlingVelocity:I

.field private mMinFlingVelocity:I

.field private mObj:Ljava/lang/Object;

.field private mSlop:I

.field private mSwipeSensitiveConstant:F

.field private mSwiping:Z

.field private mSwipingSlop:I

.field private mTranslationX:F

.field private mVelocityTracker:Landroid/view/VelocityTracker;

.field private mView:Landroid/view/View;

.field private mViewWidth:I


# direct methods
.method public constructor <init>(Landroid/view/View;Ljava/lang/Object;Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener$DismissCallbacks;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "obj"    # Ljava/lang/Object;
    .param p3, "callbacks"    # Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener$DismissCallbacks;

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    const/4 v1, 0x1

    iput v1, p0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mViewWidth:I

    .line 29
    const v1, 0x3f19999a    # 0.6f

    iput v1, p0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mSwipeSensitiveConstant:F

    .line 37
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 38
    .local v0, "vc":Landroid/view/ViewConfiguration;
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mSlop:I

    .line 39
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v1

    mul-int/lit8 v1, v1, 0x28

    iput v1, p0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mMinFlingVelocity:I

    .line 40
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mMaxFlingVelocity:I

    .line 41
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x10e0000

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-long v1, v1

    iput-wide v1, p0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mAnimationTime:J

    .line 43
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mView:Landroid/view/View;

    .line 44
    iput-object p2, p0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mObj:Ljava/lang/Object;

    .line 45
    iput-object p3, p0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mCallbacks:Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener$DismissCallbacks;

    .line 46
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->performDismiss()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;

    .prologue
    .line 12
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;

    .prologue
    .line 12
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mObj:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;)Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener$DismissCallbacks;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;

    .prologue
    .line 12
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mCallbacks:Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener$DismissCallbacks;

    return-object v0
.end method

.method private performDismiss()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 173
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 174
    .local v1, "lp":Landroid/view/ViewGroup$LayoutParams;
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v2

    .line 176
    .local v2, "originalHeight":I
    const/4 v3, 0x2

    new-array v3, v3, [I

    const/4 v4, 0x0

    aput v2, v3, v4

    aput v5, v3, v5

    invoke-static {v3}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v3

    iget-wide v4, p0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mAnimationTime:J

    invoke-virtual {v3, v4, v5}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 178
    .local v0, "animator":Landroid/animation/ValueAnimator;
    new-instance v3, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener$2;

    invoke-direct {v3, p0, v1, v2}, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener$2;-><init>(Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;Landroid/view/ViewGroup$LayoutParams;I)V

    invoke-virtual {v0, v3}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 189
    new-instance v3, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener$3;

    invoke-direct {v3, p0, v1}, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener$3;-><init>(Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v0, v3}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 197
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 198
    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 16
    .param p1, "view"    # Landroid/view/View;
    .param p2, "motionEvent"    # Landroid/view/MotionEvent;

    .prologue
    .line 50
    move-object/from16 v0, p0

    iget v10, v0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mTranslationX:F

    const/4 v11, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v10, v11}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 52
    move-object/from16 v0, p0

    iget v10, v0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mViewWidth:I

    const/4 v11, 0x2

    if-ge v10, v11, :cond_0

    .line 53
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mView:Landroid/view/View;

    invoke-virtual {v10}, Landroid/view/View;->getWidth()I

    move-result v10

    move-object/from16 v0, p0

    iput v10, v0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mViewWidth:I

    .line 56
    :cond_0
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v10

    packed-switch v10, :pswitch_data_0

    .line 169
    :cond_1
    :goto_0
    const/4 v10, 0x0

    :goto_1
    return v10

    .line 58
    :pswitch_0
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v10

    move-object/from16 v0, p0

    iput v10, v0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mDownX:F

    .line 59
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v10

    move-object/from16 v0, p0

    iput v10, v0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mDownY:F

    .line 60
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mCallbacks:Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener$DismissCallbacks;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mObj:Ljava/lang/Object;

    invoke-interface {v10, v11}, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener$DismissCallbacks;->canDismiss(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 61
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v10

    move-object/from16 v0, p0

    iput-object v10, v0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 62
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v0, p2

    invoke-virtual {v10, v0}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 64
    :cond_2
    const/4 v10, 0x0

    goto :goto_1

    .line 68
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-eqz v10, :cond_1

    .line 72
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v10

    move-object/from16 v0, p0

    iget v11, v0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mDownX:F

    sub-float v4, v10, v11

    .line 73
    .local v4, "deltaX":F
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v0, p2

    invoke-virtual {v10, v0}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 74
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mVelocityTracker:Landroid/view/VelocityTracker;

    const/16 v11, 0x3e8

    invoke-virtual {v10, v11}, Landroid/view/VelocityTracker;->computeCurrentVelocity(I)V

    .line 75
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v10}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v9

    .line 76
    .local v9, "velocityX":F
    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v1

    .line 77
    .local v1, "absVelocityX":F
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v10}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v10

    invoke-static {v10}, Ljava/lang/Math;->abs(F)F

    move-result v2

    .line 78
    .local v2, "absVelocityY":F
    const/4 v6, 0x0

    .line 79
    .local v6, "dismiss":Z
    const/4 v7, 0x0

    .line 80
    .local v7, "dismissRight":Z
    move-object/from16 v0, p0

    iget v10, v0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mViewWidth:I

    int-to-double v10, v10

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mViewWidth:I

    int-to-double v12, v12

    const-wide v14, 0x3fc3333333333333L    # 0.15

    mul-double/2addr v12, v14

    sub-double/2addr v10, v12

    double-to-int v8, v10

    .line 81
    .local v8, "minWidthToDelete":I
    float-to-double v10, v4

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mSwipeSensitiveConstant:F

    float-to-double v12, v12

    const-wide v14, 0x3fd999999999999aL    # 0.4

    add-double/2addr v12, v14

    mul-double/2addr v10, v12

    invoke-static {v10, v11}, Ljava/lang/Math;->abs(D)D

    move-result-wide v10

    int-to-double v12, v8

    cmpl-double v10, v10, v12

    if-lez v10, :cond_6

    move-object/from16 v0, p0

    iget-boolean v10, v0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mSwiping:Z

    if-eqz v10, :cond_6

    .line 82
    const/4 v6, 0x1

    .line 83
    const/4 v10, 0x0

    cmpl-float v10, v4, v10

    if-lez v10, :cond_5

    const/4 v7, 0x1

    .line 89
    :cond_3
    :goto_2
    if-eqz v6, :cond_8

    .line 91
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mView:Landroid/view/View;

    invoke-virtual {v10}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v11

    if-eqz v7, :cond_7

    move-object/from16 v0, p0

    iget v10, v0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mViewWidth:I

    int-to-float v10, v10

    :goto_3
    invoke-virtual {v11, v10}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v10

    move-object/from16 v0, p0

    iget-wide v11, v0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mAnimationTime:J

    invoke-virtual {v10, v11, v12}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v10

    new-instance v11, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener$1;

    move-object/from16 v0, p0

    invoke-direct {v11, v0}, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener$1;-><init>(Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;)V

    invoke-virtual {v10, v11}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 109
    :cond_4
    :goto_4
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v10}, Landroid/view/VelocityTracker;->recycle()V

    .line 110
    const/4 v10, 0x0

    move-object/from16 v0, p0

    iput-object v10, v0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 111
    const/4 v10, 0x0

    move-object/from16 v0, p0

    iput v10, v0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mTranslationX:F

    .line 112
    const/4 v10, 0x0

    move-object/from16 v0, p0

    iput v10, v0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mDownX:F

    .line 113
    const/4 v10, 0x0

    move-object/from16 v0, p0

    iput v10, v0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mDownY:F

    .line 114
    const/4 v10, 0x0

    move-object/from16 v0, p0

    iput-boolean v10, v0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mSwiping:Z

    goto/16 :goto_0

    .line 83
    :cond_5
    const/4 v7, 0x0

    goto :goto_2

    .line 84
    :cond_6
    move-object/from16 v0, p0

    iget v10, v0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mMinFlingVelocity:I

    int-to-float v10, v10

    cmpg-float v10, v10, v1

    if-gtz v10, :cond_3

    move-object/from16 v0, p0

    iget v10, v0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mMaxFlingVelocity:I

    int-to-float v10, v10

    cmpg-float v10, v1, v10

    if-gtz v10, :cond_3

    cmpg-float v10, v2, v1

    if-gez v10, :cond_3

    move-object/from16 v0, p0

    iget-boolean v10, v0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mSwiping:Z

    if-eqz v10, :cond_3

    goto :goto_2

    .line 91
    :cond_7
    move-object/from16 v0, p0

    iget v10, v0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mViewWidth:I

    neg-int v10, v10

    int-to-float v10, v10

    goto :goto_3

    .line 101
    :cond_8
    move-object/from16 v0, p0

    iget-boolean v10, v0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mSwiping:Z

    if-eqz v10, :cond_4

    .line 103
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mView:Landroid/view/View;

    invoke-virtual {v10}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v10

    const/high16 v11, 0x3f800000    # 1.0f

    invoke-virtual {v10, v11}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v10

    move-object/from16 v0, p0

    iget-wide v11, v0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mAnimationTime:J

    invoke-virtual {v10, v11, v12}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    goto :goto_4

    .line 119
    .end local v1    # "absVelocityX":F
    .end local v2    # "absVelocityY":F
    .end local v4    # "deltaX":F
    .end local v6    # "dismiss":Z
    .end local v7    # "dismissRight":Z
    .end local v8    # "minWidthToDelete":I
    .end local v9    # "velocityX":F
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-eqz v10, :cond_1

    .line 123
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mView:Landroid/view/View;

    invoke-virtual {v10}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v10

    const/high16 v11, 0x3f800000    # 1.0f

    invoke-virtual {v10, v11}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v10

    move-object/from16 v0, p0

    iget-wide v11, v0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mAnimationTime:J

    invoke-virtual {v10, v11, v12}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 128
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v10}, Landroid/view/VelocityTracker;->recycle()V

    .line 129
    const/4 v10, 0x0

    move-object/from16 v0, p0

    iput-object v10, v0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 130
    const/4 v10, 0x0

    move-object/from16 v0, p0

    iput v10, v0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mTranslationX:F

    .line 131
    const/4 v10, 0x0

    move-object/from16 v0, p0

    iput v10, v0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mDownX:F

    .line 132
    const/4 v10, 0x0

    move-object/from16 v0, p0

    iput v10, v0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mDownY:F

    .line 133
    const/4 v10, 0x0

    move-object/from16 v0, p0

    iput-boolean v10, v0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mSwiping:Z

    goto/16 :goto_0

    .line 138
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-eqz v10, :cond_1

    .line 142
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v0, p2

    invoke-virtual {v10, v0}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 143
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v10

    move-object/from16 v0, p0

    iget v11, v0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mDownX:F

    sub-float v4, v10, v11

    .line 144
    .restart local v4    # "deltaX":F
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v10

    move-object/from16 v0, p0

    iget v11, v0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mDownY:F

    sub-float v5, v10, v11

    .line 145
    .local v5, "deltaY":F
    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v10

    move-object/from16 v0, p0

    iget v11, v0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mSlop:I

    int-to-float v11, v11

    cmpl-float v10, v10, v11

    if-lez v10, :cond_9

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v10

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v11

    const/high16 v12, 0x40000000    # 2.0f

    div-float/2addr v11, v12

    cmpg-float v10, v10, v11

    if-gez v10, :cond_9

    .line 146
    const/4 v10, 0x1

    move-object/from16 v0, p0

    iput-boolean v10, v0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mSwiping:Z

    .line 147
    const/4 v10, 0x0

    cmpl-float v10, v4, v10

    if-lez v10, :cond_a

    move-object/from16 v0, p0

    iget v10, v0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mSlop:I

    :goto_5
    move-object/from16 v0, p0

    iput v10, v0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mSwipingSlop:I

    .line 148
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mView:Landroid/view/View;

    invoke-virtual {v10}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v10

    const/4 v11, 0x1

    invoke-interface {v10, v11}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 151
    invoke-static/range {p2 .. p2}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v3

    .line 152
    .local v3, "cancelEvent":Landroid/view/MotionEvent;
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v10

    shl-int/lit8 v10, v10, 0x8

    or-int/lit8 v10, v10, 0x3

    invoke-virtual {v3, v10}, Landroid/view/MotionEvent;->setAction(I)V

    .line 155
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mView:Landroid/view/View;

    invoke-virtual {v10, v3}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 156
    invoke-virtual {v3}, Landroid/view/MotionEvent;->recycle()V

    .line 159
    .end local v3    # "cancelEvent":Landroid/view/MotionEvent;
    :cond_9
    move-object/from16 v0, p0

    iget-boolean v10, v0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mSwiping:Z

    if-eqz v10, :cond_1

    .line 160
    move-object/from16 v0, p0

    iput v4, v0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mTranslationX:F

    .line 161
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mView:Landroid/view/View;

    move-object/from16 v0, p0

    iget v11, v0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mSwipingSlop:I

    int-to-float v11, v11

    sub-float v11, v4, v11

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mSwipeSensitiveConstant:F

    mul-float/2addr v11, v12

    invoke-virtual {v10, v11}, Landroid/view/View;->setTranslationX(F)V

    .line 162
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mView:Landroid/view/View;

    const/4 v11, 0x0

    const/high16 v12, 0x3f800000    # 1.0f

    const/high16 v13, 0x3f800000    # 1.0f

    const/high16 v14, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mSwipeSensitiveConstant:F

    mul-float/2addr v15, v4

    invoke-static {v15}, Ljava/lang/Math;->abs(F)F

    move-result v15

    mul-float/2addr v14, v15

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mViewWidth:I

    int-to-float v15, v15

    div-float/2addr v14, v15

    sub-float/2addr v13, v14

    invoke-static {v12, v13}, Ljava/lang/Math;->min(FF)F

    move-result v12

    invoke-static {v11, v12}, Ljava/lang/Math;->max(FF)F

    move-result v11

    invoke-virtual {v10, v11}, Landroid/view/View;->setAlpha(F)V

    .line 164
    const/4 v10, 0x1

    goto/16 :goto_1

    .line 147
    :cond_a
    move-object/from16 v0, p0

    iget v10, v0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;->mSlop:I

    neg-int v10, v10

    goto :goto_5

    .line 56
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method
