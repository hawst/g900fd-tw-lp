.class Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$5;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->getContentInitializationListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$5;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onContentInitialization(Landroid/view/View;Landroid/app/Activity;Landroid/app/Dialog;Landroid/os/Bundle;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V
    .locals 18

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$5;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$id;->rename:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    # setter for: Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->mInput:Landroid/widget/EditText;
    invoke-static {v3, v2}, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->access$802(Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;Landroid/widget/EditText;)Landroid/widget/EditText;

    new-instance v2, Lcom/sec/android/app/shealth/common/utils/Utils$EditTextLimitHelperThatShowsToast;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$5;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->mActivity:Landroid/app/Activity;
    invoke-static {v3}, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->access$000(Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;)Landroid/app/Activity;

    move-result-object v3

    sget v4, Lcom/sec/android/app/shealth/framework/ui/R$string;->msg_alert_input_rename_device:I

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/utils/Utils$EditTextLimitHelperThatShowsToast;-><init>(Landroid/content/Context;I)V

    const/16 v3, 0x20

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$5;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->mInput:Landroid/widget/EditText;
    invoke-static {v4}, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->access$800(Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;)Landroid/widget/EditText;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/shealth/common/utils/Utils$EditTextLimitHelperThatShowsToast;->addLimit(ILandroid/widget/EditText;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$5;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->mInput:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->access$800(Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;)Landroid/widget/EditText;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setSingleLine(Z)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$5;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$5;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->mNameView:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->access$600(Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    # setter for: Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->mRename:Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->access$502(Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;Ljava/lang/String;)Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$5;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->mInput:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->access$800(Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;)Landroid/widget/EditText;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$5;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->mRename:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->access$500(Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$5;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->mInput:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->access$800(Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;)Landroid/widget/EditText;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$5;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->mRename:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->access$500(Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setSelection(I)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$5;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;

    invoke-static {v2}, Lcom/sec/android/app/shealth/common/utils/SoftInputUtils;->isHardKeyBoardPresent(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$5;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;

    const/16 v2, 0xc

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, -0x4fdc

    aput v16, v3, v15

    const/16 v15, -0x21

    aput v15, v3, v14

    const/16 v14, -0x56a8

    aput v14, v3, v13

    const/16 v13, -0x23

    aput v13, v3, v12

    const/16 v12, -0xb8b

    aput v12, v3, v11

    const/16 v11, -0x67

    aput v11, v3, v10

    const/16 v10, -0x56b4

    aput v10, v3, v9

    const/16 v9, -0x23

    aput v9, v3, v8

    const/16 v8, -0x43

    aput v8, v3, v7

    const/16 v7, -0x12

    aput v7, v3, v6

    const/16 v6, -0x15

    aput v6, v3, v4

    const/16 v4, 0x5464

    aput v4, v3, v2

    const/16 v2, 0xc

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, -0x4fc0

    aput v17, v2, v16

    const/16 v16, -0x50

    aput v16, v2, v15

    const/16 v15, -0x56d0

    aput v15, v2, v14

    const/16 v14, -0x57

    aput v14, v2, v13

    const/16 v13, -0xbf0

    aput v13, v2, v12

    const/16 v12, -0xc

    aput v12, v2, v11

    const/16 v11, -0x56ed

    aput v11, v2, v10

    const/16 v10, -0x57

    aput v10, v2, v9

    const/16 v9, -0x38

    aput v9, v2, v8

    const/16 v8, -0x62

    aput v8, v2, v7

    const/16 v7, -0x7b

    aput v7, v2, v6

    const/16 v6, 0x540d

    aput v6, v2, v4

    const/4 v4, 0x0

    :goto_0
    array-length v6, v2

    if-lt v4, v6, :cond_1

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_1
    array-length v6, v2

    if-lt v4, v6, :cond_2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/inputmethod/InputMethodManager;

    # setter for: Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->mImm:Landroid/view/inputmethod/InputMethodManager;
    invoke-static {v5, v2}, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->access$902(Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;Landroid/view/inputmethod/InputMethodManager;)Landroid/view/inputmethod/InputMethodManager;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$5;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->mImm:Landroid/view/inputmethod/InputMethodManager;
    invoke-static {v2}, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->access$900(Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v2

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/inputmethod/InputMethodManager;->toggleSoftInput(II)V

    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$5;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->mInput:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->access$800(Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;)Landroid/widget/EditText;

    move-result-object v2

    new-instance v3, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$5$1;

    move-object/from16 v0, p0

    move-object/from16 v1, p5

    invoke-direct {v3, v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$5$1;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$5;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-void

    :cond_1
    aget v6, v2, v4

    aget v7, v3, v4

    xor-int/2addr v6, v7

    aput v6, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_2
    aget v6, v3, v4

    int-to-char v6, v6

    aput-char v6, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_1
.end method
