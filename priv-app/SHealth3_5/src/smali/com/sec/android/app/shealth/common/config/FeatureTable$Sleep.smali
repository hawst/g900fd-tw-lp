.class public final enum Lcom/sec/android/app/shealth/common/config/FeatureTable$Sleep;
.super Ljava/lang/Enum;
.source "FeatureTable.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/config/FeatureTable$Support;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/common/config/FeatureTable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Sleep"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/common/config/FeatureTable$Sleep;",
        ">;",
        "Lcom/sec/android/app/shealth/common/config/FeatureTable$Support;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/common/config/FeatureTable$Sleep;

.field public static final enum NOT_SUPPORTED:Lcom/sec/android/app/shealth/common/config/FeatureTable$Sleep;

.field public static final enum SUPPORTED:Lcom/sec/android/app/shealth/common/config/FeatureTable$Sleep;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 47
    new-instance v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$Sleep;

    const-string v1, "SUPPORTED"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Sleep;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$Sleep;->SUPPORTED:Lcom/sec/android/app/shealth/common/config/FeatureTable$Sleep;

    .line 48
    new-instance v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$Sleep;

    const-string v1, "NOT_SUPPORTED"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Sleep;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$Sleep;->NOT_SUPPORTED:Lcom/sec/android/app/shealth/common/config/FeatureTable$Sleep;

    .line 45
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/android/app/shealth/common/config/FeatureTable$Sleep;

    sget-object v1, Lcom/sec/android/app/shealth/common/config/FeatureTable$Sleep;->SUPPORTED:Lcom/sec/android/app/shealth/common/config/FeatureTable$Sleep;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/shealth/common/config/FeatureTable$Sleep;->NOT_SUPPORTED:Lcom/sec/android/app/shealth/common/config/FeatureTable$Sleep;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$Sleep;->$VALUES:[Lcom/sec/android/app/shealth/common/config/FeatureTable$Sleep;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/config/FeatureTable$Sleep;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 45
    const-class v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$Sleep;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$Sleep;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/common/config/FeatureTable$Sleep;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$Sleep;->$VALUES:[Lcom/sec/android/app/shealth/common/config/FeatureTable$Sleep;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/common/config/FeatureTable$Sleep;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/common/config/FeatureTable$Sleep;

    return-object v0
.end method
