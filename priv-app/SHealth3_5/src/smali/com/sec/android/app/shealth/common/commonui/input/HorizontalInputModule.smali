.class public Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;
.super Lcom/sec/android/app/shealth/common/commonui/input/InputModule;
.source "HorizontalInputModule.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule$OnHorizontalListItemClickListener;
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 11
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;-><init>(Landroid/content/Context;)V

    .line 12
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 15
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 16
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 19
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 20
    return-void
.end method


# virtual methods
.method protected getAbnormalBackgroundDrawableID()I
    .locals 1

    .prologue
    .line 38
    sget v0, Lcom/sec/android/app/shealth/common/commonui/R$drawable;->horizontal_input_module_controller_view_background_orange_pin:I

    return v0
.end method

.method protected getNormalBackgroundDrawableID()I
    .locals 1

    .prologue
    .line 43
    sget v0, Lcom/sec/android/app/shealth/common/commonui/R$drawable;->horizontal_input_module_controller_view_background_green_pin:I

    return v0
.end method

.method protected getRootLayoutId()I
    .locals 1

    .prologue
    .line 28
    sget v0, Lcom/sec/android/app/shealth/common/commonui/R$layout;->inputmodule_horizontal_layout:I

    return v0
.end method

.method protected initAdditionalViews(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 34
    return-void
.end method
