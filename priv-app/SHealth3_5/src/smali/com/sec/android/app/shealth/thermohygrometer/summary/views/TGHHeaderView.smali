.class public abstract Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;
.super Lcom/sec/android/app/shealth/common/commonui/summary/SummaryContainerView;
.source "TGHHeaderView.java"


# static fields
.field public static final FADE_DELAY_DURATION:I = 0x42

.field public static final FADE_DURATION:I = 0xa7

.field public static final IMAGE_ANIMATION_DURATION:I = 0x190


# instance fields
.field private anim_image:Landroid/widget/ImageView;

.field private isAnimationStarted:Z

.field mBalanceAnim:Landroid/view/animation/Animation;

.field private mBalanceIcon:Landroid/widget/ImageView;

.field private mBalanceIconAnimator:Landroid/animation/ObjectAnimator;

.field private mBalanceState:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

.field private mBalanceStateTextView:Landroid/widget/TextView;

.field private mBalanceTextFadeInAnimator:Landroid/animation/ObjectAnimator;

.field private mBalanceTextFadeOutAnimator:Landroid/animation/ObjectAnimator;

.field private xpos:F

.field private ypos:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/common/commonui/summary/SummaryContainerView;-><init>(Landroid/content/Context;)V

    .line 59
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 68
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/common/commonui/summary/SummaryContainerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 69
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 79
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/common/commonui/summary/SummaryContainerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 80
    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;
    .param p1, "x1"    # Z

    .prologue
    .line 36
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->isAnimationStarted:Z

    return p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->mBalanceIcon:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->getBalanceIconResId()I

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->mBalanceStateTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;)Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->mBalanceState:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

    return-object v0
.end method

.method private cancelAllAnimation()V
    .locals 1

    .prologue
    .line 175
    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->getImageAnimator()Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 176
    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->getFadeInAnimator()Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 177
    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->getFadeOutAnimator()Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 178
    return-void
.end method

.method private getBalanceIconResId()I
    .locals 1

    .prologue
    .line 181
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->getIconImageResourceIdForBalanceState()I

    move-result v0

    .line 197
    .local v0, "imageResId":I
    return v0
.end method

.method private getFadeInAnimator()Landroid/animation/ObjectAnimator;
    .locals 3

    .prologue
    .line 218
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->mBalanceTextFadeInAnimator:Landroid/animation/ObjectAnimator;

    if-nez v0, :cond_0

    .line 219
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->mBalanceStateTextView:Landroid/widget/TextView;

    const-string v1, "alpha"

    const/4 v2, 0x2

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->mBalanceTextFadeInAnimator:Landroid/animation/ObjectAnimator;

    .line 220
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->mBalanceTextFadeInAnimator:Landroid/animation/ObjectAnimator;

    const-wide/16 v1, 0xa7

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 221
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->mBalanceTextFadeInAnimator:Landroid/animation/ObjectAnimator;

    new-instance v1, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView$3;-><init>(Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 229
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->mBalanceTextFadeInAnimator:Landroid/animation/ObjectAnimator;

    return-object v0

    .line 219
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private getFadeOutAnimator()Landroid/animation/ObjectAnimator;
    .locals 3

    .prologue
    .line 233
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->mBalanceTextFadeOutAnimator:Landroid/animation/ObjectAnimator;

    if-nez v0, :cond_0

    .line 234
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->mBalanceStateTextView:Landroid/widget/TextView;

    const-string v1, "alpha"

    const/4 v2, 0x2

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->mBalanceTextFadeOutAnimator:Landroid/animation/ObjectAnimator;

    .line 235
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->mBalanceTextFadeOutAnimator:Landroid/animation/ObjectAnimator;

    new-instance v1, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView$4;-><init>(Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 253
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->mBalanceTextFadeOutAnimator:Landroid/animation/ObjectAnimator;

    const-wide/16 v1, 0xa7

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 255
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->mBalanceTextFadeOutAnimator:Landroid/animation/ObjectAnimator;

    return-object v0

    .line 234
    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method private getImageAnimator()Landroid/animation/ObjectAnimator;
    .locals 3

    .prologue
    .line 201
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->mBalanceIconAnimator:Landroid/animation/ObjectAnimator;

    if-nez v0, :cond_0

    .line 202
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->mBalanceIcon:Landroid/widget/ImageView;

    const-string/jumbo v1, "scaleX"

    const/4 v2, 0x4

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->mBalanceIconAnimator:Landroid/animation/ObjectAnimator;

    .line 203
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->mBalanceIconAnimator:Landroid/animation/ObjectAnimator;

    const-wide/16 v1, 0x190

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 204
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->mBalanceIconAnimator:Landroid/animation/ObjectAnimator;

    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 205
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->mBalanceIconAnimator:Landroid/animation/ObjectAnimator;

    new-instance v1, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView$2;-><init>(Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 214
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->mBalanceIconAnimator:Landroid/animation/ObjectAnimator;

    return-object v0

    .line 202
    nop

    :array_0
    .array-data 4
        0x3f19999a    # 0.6f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private startAnimation()V
    .locals 9

    .prologue
    const-wide/16 v7, 0x3e8

    const/4 v6, 0x1

    .line 121
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    iget v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->xpos:F

    iget v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->xpos:F

    iget v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->ypos:F

    const/high16 v4, 0x43340000    # 180.0f

    add-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->ypos:F

    const/high16 v5, 0x42820000    # 65.0f

    sub-float/2addr v4, v5

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->mBalanceAnim:Landroid/view/animation/Animation;

    .line 122
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->mBalanceAnim:Landroid/view/animation/Animation;

    invoke-virtual {v0, v7, v8}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 123
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->mBalanceAnim:Landroid/view/animation/Animation;

    invoke-virtual {v0, v6}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 124
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->mBalanceAnim:Landroid/view/animation/Animation;

    invoke-virtual {v0, v6}, Landroid/view/animation/Animation;->setFillEnabled(Z)V

    .line 125
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->mBalanceAnim:Landroid/view/animation/Animation;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setRepeatCount(I)V

    .line 126
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->mBalanceAnim:Landroid/view/animation/Animation;

    invoke-virtual {v0, v7, v8}, Landroid/view/animation/Animation;->setStartOffset(J)V

    .line 127
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->mBalanceAnim:Landroid/view/animation/Animation;

    new-instance v1, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView$1;-><init>(Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 144
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->anim_image:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->mBalanceAnim:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 145
    return-void
.end method

.method private stopAnimation()V
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->mBalanceAnim:Landroid/view/animation/Animation;

    invoke-virtual {v0}, Landroid/view/animation/Animation;->cancel()V

    .line 155
    return-void
.end method


# virtual methods
.method protected abstract getIconImageResourceIdForBalanceState()I
.end method

.method protected abstract getIconImageResourceIdForNoDataState()I
.end method

.method protected abstract getIconImageResourceIdForUnbalanceState()I
.end method

.method protected initializeContentView()V
    .locals 3

    .prologue
    .line 104
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f030255

    invoke-static {v1, v2, p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 105
    .local v0, "contentView":Landroid/view/View;
    const v1, 0x7f080a84

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->mBalanceIcon:Landroid/widget/ImageView;

    .line 106
    const v1, 0x7f080a85

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->anim_image:Landroid/widget/ImageView;

    .line 107
    const v1, 0x7f080a86

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->mBalanceStateTextView:Landroid/widget/TextView;

    .line 108
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->anim_image:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getLeft()I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->xpos:F

    .line 109
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->anim_image:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getBottom()I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->ypos:F

    .line 110
    sget-object v1, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;->DEFAULT:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->refreshBalanceState(Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;)V

    .line 112
    return-void
.end method

.method public refreshBalanceState(Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;)V
    .locals 4
    .param p1, "balanceState"    # Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

    .prologue
    .line 164
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->mBalanceState:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

    if-eq v1, p1, :cond_0

    .line 165
    iput-object p1, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->mBalanceState:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

    .line 166
    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->cancelAllAnimation()V

    .line 167
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    .line 168
    .local v0, "animatorSet":Landroid/animation/AnimatorSet;
    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->getFadeOutAnimator()Landroid/animation/ObjectAnimator;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v1

    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->getFadeInAnimator()Landroid/animation/ObjectAnimator;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet$Builder;->before(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v1

    const-wide/16 v2, 0x42

    invoke-virtual {v1, v2, v3}, Landroid/animation/AnimatorSet$Builder;->after(J)Landroid/animation/AnimatorSet$Builder;

    .line 169
    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->getImageAnimator()Landroid/animation/ObjectAnimator;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 170
    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 172
    .end local v0    # "animatorSet":Landroid/animation/AnimatorSet;
    :cond_0
    return-void
.end method

.method public startBalanceAnim()V
    .locals 1

    .prologue
    .line 115
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->isAnimationStarted:Z

    if-eqz v0, :cond_0

    .line 118
    :goto_0
    return-void

    .line 117
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->startAnimation()V

    goto :goto_0
.end method

.method public stopBalanceAnim()V
    .locals 1

    .prologue
    .line 148
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->isAnimationStarted:Z

    if-nez v0, :cond_0

    .line 151
    :goto_0
    return-void

    .line 150
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->stopAnimation()V

    goto :goto_0
.end method
