.class public final Lcom/sec/android/app/shealth/food/mask/MaskTools;
.super Ljava/lang/Object;
.source "MaskTools.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/food/mask/MaskTools$DrawingResultListener;,
        Lcom/sec/android/app/shealth/food/mask/MaskTools$CollectingBitmapTask;
    }
.end annotation


# instance fields
.field private mHeight:I

.field mMaskPixels:[I

.field private mMaskedColor:I

.field private mSquaresCount:I

.field private mSquaresMatrix:[I

.field private mWidth:I


# direct methods
.method private constructor <init>(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "mask"    # Landroid/graphics/Bitmap;

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/mask/MaskTools;->init(Landroid/graphics/Bitmap;)V

    .line 37
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/food/mask/MaskTools;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mask/MaskTools;

    .prologue
    .line 27
    iget v0, p0, Lcom/sec/android/app/shealth/food/mask/MaskTools;->mWidth:I

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/food/mask/MaskTools;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mask/MaskTools;

    .prologue
    .line 27
    iget v0, p0, Lcom/sec/android/app/shealth/food/mask/MaskTools;->mHeight:I

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/food/mask/MaskTools;ILjava/lang/String;Landroid/graphics/Bitmap;Lcom/sec/android/app/shealth/food/mask/BitmapProvider;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mask/MaskTools;
    .param p1, "x1"    # I
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Landroid/graphics/Bitmap;
    .param p4, "x4"    # Lcom/sec/android/app/shealth/food/mask/BitmapProvider;

    .prologue
    .line 27
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/app/shealth/food/mask/MaskTools;->drawBitmap(ILjava/lang/String;Landroid/graphics/Bitmap;Lcom/sec/android/app/shealth/food/mask/BitmapProvider;)V

    return-void
.end method

.method public static createDefaultMaskTools(Landroid/graphics/Bitmap;)Lcom/sec/android/app/shealth/food/mask/MaskTools;
    .locals 1
    .param p0, "mask"    # Landroid/graphics/Bitmap;

    .prologue
    .line 62
    new-instance v0, Lcom/sec/android/app/shealth/food/mask/MaskTools;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/food/mask/MaskTools;-><init>(Landroid/graphics/Bitmap;)V

    return-object v0
.end method

.method private drawBitmap(ILjava/lang/String;Landroid/graphics/Bitmap;Lcom/sec/android/app/shealth/food/mask/BitmapProvider;)V
    .locals 26
    .param p1, "maskSquareIndex"    # I
    .param p2, "sourceBitmapPath"    # Ljava/lang/String;
    .param p3, "destinationBitmap"    # Landroid/graphics/Bitmap;
    .param p4, "bitmapProvider"    # Lcom/sec/android/app/shealth/food/mask/BitmapProvider;

    .prologue
    .line 143
    if-ltz p1, :cond_0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/shealth/food/mask/MaskTools;->mSquaresCount:I

    add-int/lit8 v4, v4, -0x1

    move/from16 v0, p1

    if-le v0, v4, :cond_1

    .line 144
    :cond_0
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Square index should be in range[0, "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/app/shealth/food/mask/MaskTools;->mSquaresCount:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 146
    :cond_1
    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/shealth/food/mask/MaskTools;->mWidth:I

    if-ne v4, v6, :cond_2

    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/shealth/food/mask/MaskTools;->mHeight:I

    if-eq v4, v6, :cond_3

    .line 147
    :cond_2
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Destination bitmap is invalid, bitmap size - "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "x"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " , should be - "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/app/shealth/food/mask/MaskTools;->mWidth:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "x"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/app/shealth/food/mask/MaskTools;->mHeight:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 150
    :cond_3
    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/app/shealth/food/mask/MaskTools;->mWidth:I

    .line 151
    .local v14, "minX":I
    const/16 v20, 0x0

    .line 152
    .local v20, "maxX":I
    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/app/shealth/food/mask/MaskTools;->mHeight:I

    .line 153
    .local v15, "minY":I
    const/16 v21, 0x0

    .line 154
    .local v21, "maxY":I
    const/16 v24, 0x0

    .local v24, "x":I
    :goto_0
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/shealth/food/mask/MaskTools;->mWidth:I

    move/from16 v0, v24

    if-ge v0, v4, :cond_9

    .line 155
    const/16 v25, 0x0

    .local v25, "y":I
    :goto_1
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/shealth/food/mask/MaskTools;->mHeight:I

    move/from16 v0, v25

    if-ge v0, v4, :cond_8

    .line 156
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/food/mask/MaskTools;->mSquaresMatrix:[I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/shealth/food/mask/MaskTools;->mWidth:I

    mul-int v6, v6, v25

    add-int v6, v6, v24

    aget v4, v4, v6

    add-int/lit8 v6, p1, 0x1

    if-ne v4, v6, :cond_7

    .line 157
    move/from16 v0, v24

    if-le v14, v0, :cond_4

    move/from16 v14, v24

    .line 158
    :cond_4
    move/from16 v0, v20

    move/from16 v1, v24

    if-ge v0, v1, :cond_5

    move/from16 v20, v24

    .line 159
    :cond_5
    move/from16 v0, v25

    if-le v15, v0, :cond_6

    move/from16 v15, v25

    .line 160
    :cond_6
    move/from16 v0, v21

    move/from16 v1, v25

    if-ge v0, v1, :cond_7

    move/from16 v21, v25

    .line 155
    :cond_7
    add-int/lit8 v25, v25, 0x1

    goto :goto_1

    .line 154
    :cond_8
    add-int/lit8 v24, v24, 0x1

    goto :goto_0

    .line 164
    .end local v25    # "y":I
    :cond_9
    sub-int v4, v20, v14

    add-int/lit8 v5, v4, 0x1

    .line 165
    .local v5, "resultWidth":I
    sub-int v4, v21, v15

    add-int/lit8 v9, v4, 0x1

    .line 167
    .local v9, "resultHeight":I
    move-object/from16 v0, p4

    move-object/from16 v1, p2

    invoke-virtual {v0, v1, v5, v9}, Lcom/sec/android/app/shealth/food/mask/BitmapProvider;->getImage(Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v19

    .line 168
    .local v19, "bitmapFromPath":Landroid/graphics/Bitmap;
    if-eqz v19, :cond_a

    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    if-eqz v4, :cond_a

    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    if-nez v4, :cond_b

    .line 189
    :cond_a
    :goto_2
    return-void

    .line 171
    :cond_b
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1, v5, v9}, Lcom/sec/android/app/shealth/food/mask/MaskTools;->getProportionalBitmap(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v22

    .line 172
    .local v22, "result":Landroid/graphics/Bitmap;
    const/4 v4, 0x0

    move-object/from16 v0, v22

    invoke-static {v0, v5, v9, v4}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 173
    .local v2, "scaledBitmap":Landroid/graphics/Bitmap;
    mul-int v4, v5, v9

    new-array v3, v4, [I

    .line 174
    .local v3, "sourcePixelsArray":[I
    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move v8, v5

    invoke-virtual/range {v2 .. v9}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 175
    mul-int v4, v5, v9

    new-array v11, v4, [I

    .line 176
    .local v11, "pixelsArray":[I
    const/16 v23, 0x0

    .line 177
    .local v23, "sourcePixelData":I
    const/16 v18, 0x0

    .line 178
    .local v18, "alphaPixel":I
    move/from16 v24, v14

    :goto_3
    move/from16 v0, v24

    move/from16 v1, v20

    if-gt v0, v1, :cond_e

    .line 179
    move/from16 v25, v15

    .restart local v25    # "y":I
    :goto_4
    move/from16 v0, v25

    move/from16 v1, v21

    if-gt v0, v1, :cond_d

    .line 180
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/food/mask/MaskTools;->mSquaresMatrix:[I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/shealth/food/mask/MaskTools;->mWidth:I

    mul-int v6, v6, v25

    add-int v6, v6, v24

    aget v4, v4, v6

    add-int/lit8 v6, p1, 0x1

    if-ne v4, v6, :cond_c

    .line 181
    sub-int v4, v24, v14

    sub-int v6, v25, v15

    mul-int/2addr v6, v5

    add-int/2addr v4, v6

    aget v23, v3, v4

    .line 182
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/food/mask/MaskTools;->mMaskPixels:[I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/shealth/food/mask/MaskTools;->mWidth:I

    mul-int v6, v6, v25

    add-int v6, v6, v24

    aget v4, v4, v6

    invoke-static {v4}, Landroid/graphics/Color;->alpha(I)I

    move-result v4

    invoke-static/range {v23 .. v23}, Landroid/graphics/Color;->red(I)I

    move-result v6

    invoke-static/range {v23 .. v23}, Landroid/graphics/Color;->green(I)I

    move-result v7

    invoke-static/range {v23 .. v23}, Landroid/graphics/Color;->blue(I)I

    move-result v8

    invoke-static {v4, v6, v7, v8}, Landroid/graphics/Color;->argb(IIII)I

    move-result v18

    .line 183
    sub-int v4, v24, v14

    sub-int v6, v25, v15

    mul-int/2addr v6, v5

    add-int/2addr v4, v6

    aput v18, v11, v4

    .line 179
    :cond_c
    add-int/lit8 v25, v25, 0x1

    goto :goto_4

    .line 178
    :cond_d
    add-int/lit8 v24, v24, 0x1

    goto :goto_3

    .line 187
    .end local v25    # "y":I
    :cond_e
    const/4 v12, 0x0

    move-object/from16 v10, p3

    move v13, v5

    move/from16 v16, v5

    move/from16 v17, v9

    invoke-virtual/range {v10 .. v17}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    goto/16 :goto_2
.end method

.method private findSquare([I)Ljava/lang/Integer;
    .locals 3
    .param p1, "mask"    # [I

    .prologue
    .line 127
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_1

    .line 128
    aget v1, p1, v0

    iget v2, p0, Lcom/sec/android/app/shealth/food/mask/MaskTools;->mMaskedColor:I

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mask/MaskTools;->mSquaresMatrix:[I

    aget v1, v1, v0

    if-nez v1, :cond_0

    .line 129
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 132
    :goto_1
    return-object v1

    .line 127
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 132
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private getProportionalBitmap(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;
    .locals 10
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    const/high16 v9, 0x40000000    # 2.0f

    .line 204
    if-lez p2, :cond_0

    if-gtz p3, :cond_1

    .line 205
    :cond_0
    new-instance v7, Ljava/lang/IllegalArgumentException;

    const-string v8, "Height and width should be > 0"

    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 207
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    int-to-float v1, v7

    .line 208
    .local v1, "bitmapWidth":F
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    int-to-float v0, v7

    .line 209
    .local v0, "bitmapHeight":F
    int-to-float v7, p2

    div-float v7, v1, v7

    int-to-float v8, p3

    div-float v8, v0, v8

    cmpg-float v7, v7, v8

    if-gez v7, :cond_2

    int-to-float v7, p2

    div-float v2, v1, v7

    .line 210
    .local v2, "minScale":F
    :goto_0
    int-to-float v7, p2

    mul-float v4, v2, v7

    .line 211
    .local v4, "resultWidth":F
    int-to-float v7, p3

    mul-float v3, v2, v7

    .line 212
    .local v3, "resultHeight":F
    sub-float v7, v1, v4

    div-float/2addr v7, v9

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v5

    .line 213
    .local v5, "x":I
    sub-float v7, v0, v3

    div-float/2addr v7, v9

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v6

    .line 214
    .local v6, "y":I
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v7

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v8

    invoke-static {p1, v5, v6, v7, v8}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v7

    return-object v7

    .line 209
    .end local v2    # "minScale":F
    .end local v3    # "resultHeight":F
    .end local v4    # "resultWidth":F
    .end local v5    # "x":I
    .end local v6    # "y":I
    :cond_2
    int-to-float v7, p3

    div-float v2, v0, v7

    goto :goto_0
.end method

.method private init(Landroid/graphics/Bitmap;)V
    .locals 8
    .param p1, "mask"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v2, 0x0

    .line 40
    if-nez p1, :cond_0

    .line 41
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Bitmap should be not null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 43
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/food/mask/MaskTools;->mWidth:I

    .line 44
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/food/mask/MaskTools;->mHeight:I

    .line 45
    iget v0, p0, Lcom/sec/android/app/shealth/food/mask/MaskTools;->mWidth:I

    iget v1, p0, Lcom/sec/android/app/shealth/food/mask/MaskTools;->mHeight:I

    mul-int/2addr v0, v1

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mask/MaskTools;->mMaskPixels:[I

    .line 46
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mask/MaskTools;->mMaskPixels:[I

    iget v3, p0, Lcom/sec/android/app/shealth/food/mask/MaskTools;->mWidth:I

    iget v6, p0, Lcom/sec/android/app/shealth/food/mask/MaskTools;->mWidth:I

    iget v7, p0, Lcom/sec/android/app/shealth/food/mask/MaskTools;->mHeight:I

    move-object v0, p1

    move v4, v2

    move v5, v2

    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 47
    iget v0, p0, Lcom/sec/android/app/shealth/food/mask/MaskTools;->mWidth:I

    iget v1, p0, Lcom/sec/android/app/shealth/food/mask/MaskTools;->mHeight:I

    mul-int/2addr v0, v1

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mask/MaskTools;->mSquaresMatrix:[I

    .line 49
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mask/MaskTools;->mMaskPixels:[I

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/food/mask/MaskTools;->initMaskColor([I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/food/mask/MaskTools;->mMaskedColor:I

    .line 51
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mask/MaskTools;->mMaskPixels:[I

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/food/mask/MaskTools;->initSquaresMatrix([I)V

    .line 52
    return-void
.end method

.method private initMaskColor([I)I
    .locals 13
    .param p1, "mask"    # [I

    .prologue
    .line 66
    const/4 v11, 0x0

    const/4 v12, 0x2

    filled-new-array {v11, v12}, [I

    move-result-object v11

    sget-object v12, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v12, v11}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [[I

    .line 67
    .local v3, "colorsWithCounts":[[I
    move-object v0, p1

    .local v0, "arr$":[I
    array-length v8, v0

    .local v8, "len$":I
    const/4 v6, 0x0

    .local v6, "i$":I
    move v7, v6

    .end local v0    # "arr$":[I
    .end local v6    # "i$":I
    .end local v8    # "len$":I
    .local v7, "i$":I
    :goto_0
    if-ge v7, v8, :cond_4

    aget v5, v0, v7

    .line 68
    .local v5, "currentPixel":I
    if-nez v5, :cond_1

    .line 67
    .end local v7    # "i$":I
    :cond_0
    :goto_1
    add-int/lit8 v6, v7, 0x1

    .restart local v6    # "i$":I
    move v7, v6

    .end local v6    # "i$":I
    .restart local v7    # "i$":I
    goto :goto_0

    .line 71
    :cond_1
    const/4 v4, 0x0

    .line 72
    .local v4, "contains":Z
    move-object v1, v3

    .local v1, "arr$":[[I
    array-length v9, v1

    .local v9, "len$":I
    const/4 v6, 0x0

    .end local v7    # "i$":I
    .restart local v6    # "i$":I
    :goto_2
    if-ge v6, v9, :cond_2

    aget-object v2, v1, v6

    .line 73
    .local v2, "color":[I
    const/4 v11, 0x0

    aget v11, v2, v11

    if-ne v5, v11, :cond_3

    .line 74
    const/4 v11, 0x1

    aget v12, v2, v11

    add-int/lit8 v12, v12, 0x1

    aput v12, v2, v11

    .line 75
    const/4 v4, 0x1

    .line 79
    .end local v2    # "color":[I
    :cond_2
    if-nez v4, :cond_0

    .line 80
    array-length v11, v3

    add-int/lit8 v11, v11, 0x1

    invoke-static {v3, v11}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "colorsWithCounts":[[I
    check-cast v3, [[I

    .line 81
    .restart local v3    # "colorsWithCounts":[[I
    array-length v11, v3

    add-int/lit8 v11, v11, -0x1

    const/4 v12, 0x2

    new-array v12, v12, [I

    aput-object v12, v3, v11

    .line 82
    array-length v11, v3

    add-int/lit8 v11, v11, -0x1

    aget-object v11, v3, v11

    const/4 v12, 0x0

    aput v5, v11, v12

    goto :goto_1

    .line 72
    .restart local v2    # "color":[I
    :cond_3
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 85
    .end local v1    # "arr$":[[I
    .end local v2    # "color":[I
    .end local v4    # "contains":Z
    .end local v5    # "currentPixel":I
    .end local v6    # "i$":I
    .end local v9    # "len$":I
    .restart local v7    # "i$":I
    :cond_4
    const/4 v11, 0x2

    new-array v10, v11, [I

    .line 86
    .local v10, "maskColorWithCount":[I
    move-object v0, v3

    .local v0, "arr$":[[I
    array-length v8, v0

    .restart local v8    # "len$":I
    const/4 v6, 0x0

    .end local v7    # "i$":I
    .restart local v6    # "i$":I
    :goto_3
    if-ge v6, v8, :cond_6

    aget-object v2, v0, v6

    .line 87
    .restart local v2    # "color":[I
    const/4 v11, 0x1

    aget v11, v10, v11

    const/4 v12, 0x1

    aget v12, v2, v12

    if-ge v11, v12, :cond_5

    .line 88
    move-object v10, v2

    .line 86
    :cond_5
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 91
    .end local v2    # "color":[I
    :cond_6
    const/4 v11, 0x0

    aget v11, v10, v11

    return v11
.end method

.method private initSquaresMatrix([I)V
    .locals 14
    .param p1, "mask"    # [I

    .prologue
    const/4 v13, 0x1

    .line 95
    const/16 v12, 0x8

    new-array v4, v12, [I

    const/4 v12, 0x0

    aput v13, v4, v12

    const/4 v12, -0x1

    aput v12, v4, v13

    const/4 v12, 0x2

    iget v13, p0, Lcom/sec/android/app/shealth/food/mask/MaskTools;->mWidth:I

    neg-int v13, v13

    aput v13, v4, v12

    const/4 v12, 0x3

    iget v13, p0, Lcom/sec/android/app/shealth/food/mask/MaskTools;->mWidth:I

    neg-int v13, v13

    add-int/lit8 v13, v13, -0x1

    aput v13, v4, v12

    const/4 v12, 0x4

    iget v13, p0, Lcom/sec/android/app/shealth/food/mask/MaskTools;->mWidth:I

    neg-int v13, v13

    add-int/lit8 v13, v13, 0x1

    aput v13, v4, v12

    const/4 v12, 0x5

    iget v13, p0, Lcom/sec/android/app/shealth/food/mask/MaskTools;->mWidth:I

    aput v13, v4, v12

    const/4 v12, 0x6

    iget v13, p0, Lcom/sec/android/app/shealth/food/mask/MaskTools;->mWidth:I

    add-int/lit8 v13, v13, -0x1

    aput v13, v4, v12

    const/4 v12, 0x7

    iget v13, p0, Lcom/sec/android/app/shealth/food/mask/MaskTools;->mWidth:I

    add-int/lit8 v13, v13, 0x1

    aput v13, v4, v12

    .line 96
    .local v4, "factors":[I
    const/4 v1, 0x0

    .line 98
    .local v1, "count":I
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/mask/MaskTools;->findSquare([I)Ljava/lang/Integer;

    move-result-object v10

    .line 99
    .local v10, "startPoint":Ljava/lang/Integer;
    if-nez v10, :cond_1

    .line 100
    iput v1, p0, Lcom/sec/android/app/shealth/food/mask/MaskTools;->mSquaresCount:I

    .line 124
    return-void

    .line 103
    :cond_1
    add-int/lit8 v1, v1, 0x1

    .line 104
    iget-object v12, p0, Lcom/sec/android/app/shealth/food/mask/MaskTools;->mSquaresMatrix:[I

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v13

    aput v1, v12, v13

    .line 105
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 106
    .local v2, "currentStep":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 108
    :goto_0
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 109
    .local v8, "nextStep":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Integer;

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v11

    .line 110
    .local v11, "step":I
    move-object v0, v4

    .local v0, "arr$":[I
    array-length v7, v0

    .local v7, "len$":I
    const/4 v6, 0x0

    .local v6, "i$":I
    :goto_1
    if-ge v6, v7, :cond_2

    aget v3, v0, v6

    .line 111
    .local v3, "factor":I
    add-int v9, v11, v3

    .line 112
    .local v9, "point":I
    aget v12, p1, v9

    if-eqz v12, :cond_3

    iget-object v12, p0, Lcom/sec/android/app/shealth/food/mask/MaskTools;->mSquaresMatrix:[I

    aget v12, v12, v9

    if-nez v12, :cond_3

    .line 113
    iget-object v12, p0, Lcom/sec/android/app/shealth/food/mask/MaskTools;->mSquaresMatrix:[I

    aput v1, v12, v9

    .line 114
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v8, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 110
    :cond_3
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 118
    .end local v0    # "arr$":[I
    .end local v3    # "factor":I
    .end local v6    # "i$":I
    .end local v7    # "len$":I
    .end local v9    # "point":I
    .end local v11    # "step":I
    :cond_4
    invoke-virtual {v8}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v12

    if-nez v12, :cond_0

    .line 121
    move-object v2, v8

    .line 122
    goto :goto_0
.end method


# virtual methods
.method public createCollectingBitmapTask(Ljava/util/List;Lcom/sec/android/app/shealth/food/mask/MaskTools$DrawingResultListener;Lcom/sec/android/app/shealth/food/mask/BitmapProvider;)Lcom/sec/android/app/shealth/food/mask/MaskTools$CollectingBitmapTask;
    .locals 1
    .param p2, "resultListener"    # Lcom/sec/android/app/shealth/food/mask/MaskTools$DrawingResultListener;
    .param p3, "bitmapProvider"    # Lcom/sec/android/app/shealth/food/mask/BitmapProvider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/sec/android/app/shealth/food/mask/MaskTools$DrawingResultListener;",
            "Lcom/sec/android/app/shealth/food/mask/BitmapProvider;",
            ")",
            "Lcom/sec/android/app/shealth/food/mask/MaskTools$CollectingBitmapTask;"
        }
    .end annotation

    .prologue
    .line 199
    .local p1, "bitmaps":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v0, Lcom/sec/android/app/shealth/food/mask/MaskTools$CollectingBitmapTask;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/sec/android/app/shealth/food/mask/MaskTools$CollectingBitmapTask;-><init>(Lcom/sec/android/app/shealth/food/mask/MaskTools;Ljava/util/List;Lcom/sec/android/app/shealth/food/mask/MaskTools$DrawingResultListener;Lcom/sec/android/app/shealth/food/mask/BitmapProvider;)V

    .line 200
    .local v0, "task":Lcom/sec/android/app/shealth/food/mask/MaskTools$CollectingBitmapTask;
    return-object v0
.end method

.method public getAvailableSquaresCount()I
    .locals 1

    .prologue
    .line 139
    iget v0, p0, Lcom/sec/android/app/shealth/food/mask/MaskTools;->mSquaresCount:I

    return v0
.end method
