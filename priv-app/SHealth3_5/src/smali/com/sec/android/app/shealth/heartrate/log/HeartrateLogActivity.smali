.class public Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;
.source "HeartrateLogActivity.java"


# static fields
.field private static DEFAULT_TAG_SET:Ljava/lang/String;


# instance fields
.field private mHearterateObserver:Landroid/database/ContentObserver;

.field private mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

.field private mHeartrateLogAdapter:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogAdapter;

.field private mPrevCount:I

.field private menu:Landroid/view/Menu;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 59
    const-string v0, "default_tag_set"

    sput-object v0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;->DEFAULT_TAG_SET:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;-><init>()V

    .line 60
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;->mPrevCount:I

    .line 62
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity$1;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity$1;-><init>(Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;->mHearterateObserver:Landroid/database/ContentObserver;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;

    .prologue
    .line 53
    iget v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;->mPrevCount:I

    return v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;
    .param p1, "x1"    # I

    .prologue
    .line 53
    iput p1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;->mPrevCount:I

    return p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;)Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;
    .param p1, "x1"    # Landroid/database/Cursor;

    .prologue
    .line 53
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;->mCursor:Landroid/database/Cursor;

    return-object p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;

    .prologue
    .line 53
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;->refreshAdapter()V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;)Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;->mHeartrateLogAdapter:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogAdapter;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;

    .prologue
    .line 53
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;->refreshAdapter()V

    return-void
.end method


# virtual methods
.method protected applyFilter(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "constraint"    # Ljava/lang/CharSequence;

    .prologue
    .line 193
    return-void
.end method

.method protected getAdatper()Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;
    .locals 2

    .prologue
    .line 150
    invoke-static {p0}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    .line 151
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->executeQuery()Landroid/database/Cursor;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;->mCursor:Landroid/database/Cursor;

    .line 152
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogAdapter;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;->mCursor:Landroid/database/Cursor;

    invoke-direct {v0, v1, p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogAdapter;-><init>(Landroid/database/Cursor;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;->mHeartrateLogAdapter:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogAdapter;

    .line 153
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;->mHeartrateLogAdapter:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogAdapter;

    return-object v0
.end method

.method protected getColumnNameForMemo()Ljava/lang/String;
    .locals 1

    .prologue
    .line 210
    const-string v0, "comment"

    return-object v0
.end method

.method protected getContextMenuHeader(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 10
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v9, 0x0

    .line 170
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v0

    .line 171
    .local v0, "calendar":Ljava/util/Calendar;
    const-string/jumbo v6, "value"

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getFloat(I)F

    move-result v3

    .line 172
    .local v3, "heartRate":F
    const-string/jumbo v6, "start_time"

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 173
    .local v4, "time":J
    invoke-virtual {v0, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 174
    new-instance v6, Ljava/text/DateFormatSymbols;

    invoke-direct {v6}, Ljava/text/DateFormatSymbols;-><init>()V

    invoke-virtual {v6}, Ljava/text/DateFormatSymbols;->getWeekdays()[Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x7

    invoke-virtual {v0, v7}, Ljava/util/Calendar;->get(I)I

    move-result v7

    aget-object v2, v6, v7

    .line 175
    .local v2, "dname":Ljava/lang/String;
    new-instance v6, Ljava/text/SimpleDateFormat;

    const-string v7, "dd/MM/yyyy hh:mm"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 177
    .local v1, "date":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v7, 0x5

    invoke-virtual {v1, v9, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/4 v7, 0x3

    invoke-virtual {v2, v9, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ") | "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    const v7, 0x7f0900d2

    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6
.end method

.method protected getFilterRange()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 197
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 198
    .local v0, "filterList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const v1, 0x7f090076

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 199
    return-object v0
.end method

.method protected getFilterType(I)Ljava/lang/String;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 204
    sput p1, Lcom/sec/android/app/shealth/heartrate/common/HeartrateConstants;->SELECTED_FILTER_INDEX:I

    .line 205
    const-string v0, "0==0"

    return-object v0
.end method

.method protected getLogDataTypeURI()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 158
    sget-object v0, Lcom/samsung/android/sdk/health/content/ShealthContract$HeartRate;->CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method

.method protected getSelectedLogDataForSharing()Ljava/lang/String;
    .locals 3

    .prologue
    .line 215
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth.heartrate"

    const-string v2, "HR09"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;->mHeartrateLogAdapter:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogAdapter;->getCheckedItems()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getShareData(Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected isMemoVisible(Landroid/database/Cursor;)Z
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 182
    const-string v1, "comment"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 183
    .local v0, "comment":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 221
    packed-switch p1, :pswitch_data_0

    .line 235
    :cond_0
    :goto_0
    return-void

    .line 223
    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 224
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;->mHeartrateLogAdapter:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogAdapter;

    if-nez v0, :cond_1

    .line 225
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;->getAdatper()Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogAdapter;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;->mHeartrateLogAdapter:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogAdapter;

    .line 226
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;->mHeartrateLogAdapter:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogAdapter;->notifyDataSetChanged()V

    .line 227
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;->mHeartrateLogAdapter:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogAdapter;->getTotalChildCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 228
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;->menu:Landroid/view/Menu;

    if-eqz v0, :cond_2

    .line 229
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;->menu:Landroid/view/Menu;

    invoke-interface {v0}, Landroid/view/Menu;->clear()V

    .line 230
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;->showNoDataView()V

    goto :goto_0

    .line 221
    nop

    :pswitch_data_0
    .packed-switch 0x45b
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 95
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/service/health/keyManager/KeyManager;->isContentProviderAccessible()Z

    move-result v1

    if-nez v1, :cond_0

    .line 97
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.shealth.SplashScreenActivity.restart"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 98
    .local v0, "i":Landroid/content/Intent;
    const-string/jumbo v1, "temps"

    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/service/health/keyManager/KeyManager;->getRandomKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 99
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 100
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 101
    const/high16 v1, 0x20000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 102
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;->startActivity(Landroid/content/Intent;)V

    .line 104
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v1

    invoke-static {v1}, Landroid/os/Process;->killProcess(I)V

    .line 106
    .end local v0    # "i":Landroid/content/Intent;
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->onCreate(Landroid/os/Bundle;)V

    .line 107
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;->registerObserver()V

    .line 108
    const v1, 0x7f0205b7

    const v2, 0x7f090c10

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;->setNoLogImageAndText(II)V

    .line 110
    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 115
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;->unregisterObserver()V

    .line 117
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->onDestroy()V

    .line 118
    return-void
.end method

.method protected onPause()V
    .locals 4

    .prologue
    .line 122
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->onPause()V

    .line 123
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 124
    .local v1, "mPref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 125
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "FromLogScreen"

    const/4 v3, 0x1

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 126
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 127
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 7
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v6, 0x7f080c8c

    const v5, 0x7f080c8b

    const/4 v0, 0x1

    const/4 v4, 0x0

    .line 131
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;->menu:Landroid/view/Menu;

    .line 132
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    .line 133
    invoke-static {p0}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getDataCount()I

    move-result v1

    if-lez v1, :cond_0

    .line 135
    invoke-interface {p1, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090f13

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 136
    invoke-interface {p1, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 138
    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 139
    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09110b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 145
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    :goto_0
    return v0

    .line 142
    :cond_0
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    goto :goto_0
.end method

.method public registerObserver()V
    .locals 4

    .prologue
    .line 83
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$HeartRate;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;->mHearterateObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 84
    return-void
.end method

.method protected showDetailScreen(Ljava/lang/String;)V
    .locals 4
    .param p1, "tag"    # Ljava/lang/String;

    .prologue
    .line 163
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 164
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "HEART_RATE_ID_KEY"

    const-string v2, "_"

    invoke-virtual {p1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aget-object v2, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 165
    const/16 v1, 0x45b

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 166
    return-void
.end method

.method protected showFilterDialog()V
    .locals 2

    .prologue
    .line 240
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;->startActivity(Landroid/content/Intent;)V

    .line 241
    return-void
.end method

.method public unregisterObserver()V
    .locals 2

    .prologue
    .line 87
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;->mHearterateObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 88
    return-void
.end method

.method protected updateMemo(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "_id"    # Ljava/lang/String;
    .param p2, "comment"    # Ljava/lang/String;

    .prologue
    .line 188
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->updateCommentById(Ljava/lang/String;Ljava/lang/String;)Z

    .line 189
    return-void
.end method
