.class public Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;
.super Ljava/lang/Object;
.source "UtilsCommon.java"


# static fields
.field public static final DEFAULT_GALLERY_PATH:Ljava/lang/String;

.field public static final GALLERY_PATH:Ljava/lang/String;

.field public static final HAPTIC_MODE:I = 0x0

.field private static final NUMBER_OF_STREAMS:I = 0x2

.field private static final ONE_MONTH_IN_MILLIS:J = 0x9a7ec800L

.field public static SOUND_BUTTON:I = 0x0

.field public static SOUND_CHECK:I = 0x0

.field public static SOUND_CHECKBOX:I = 0x0

.field public static SOUND_GRADUATION_CONTROL:I = 0x0

.field public static SOUND_LOCK:I = 0x0

.field public static SOUND_MATE_EFFECT:I = 0x0

.field public static SOUND_MODE:I = 0x0

.field public static SOUND_NEGATIVE:I = 0x0

.field public static SOUND_PAUSE:I = 0x0

.field public static SOUND_POSITIVE:I = 0x0

.field public static SOUND_REMIND:I = 0x0

.field public static SOUND_START:I = 0x0

.field public static SOUND_STOP:I = 0x0

.field public static SOUND_SUCCESS_ALERT:I = 0x0

.field public static SOUND_TOUCH:I = 0x0

.field public static SOUND_UNLOCK:I = 0x0

.field static final TAG:Ljava/lang/String;

.field static exerciseAll:[Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise; = null

.field private static formatterYMDH:Ljava/text/SimpleDateFormat; = null

.field private static formatterYMDHMS:Ljava/text/SimpleDateFormat; = null

.field public static final loggingEnableFile:Ljava/lang/String; = "ExerciseLoggingEnable"

.field static mGoalCalorie:I

.field public static mListGoal:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;",
            ">;"
        }
    .end annotation
.end field

.field private static sTickSoundId:I

.field private static sTicker:Landroid/media/SoundPool;

.field private static soundPlayer:Lcom/sec/android/app/shealth/plugins/common/utils/appSoundplayer;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 74
    const-class v0, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->TAG:Ljava/lang/String;

    .line 80
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Landroid/os/Environment;->DIRECTORY_DCIM:Ljava/lang/String;

    invoke-static {v1}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/SHealth/Exercise/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->DEFAULT_GALLERY_PATH:Ljava/lang/String;

    .line 83
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/shealth/common/utils/constants/FilePathsConstants;->EXERCISE_IMAGES_FOLDER_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->GALLERY_PATH:Ljava/lang/String;

    .line 88
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "yyyy-MM-dd HH"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->formatterYMDH:Ljava/text/SimpleDateFormat;

    .line 89
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "yyyy-MM-dd HH:mm:ss"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->formatterYMDHMS:Ljava/text/SimpleDateFormat;

    .line 744
    const/4 v0, 0x1

    sput v0, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->SOUND_START:I

    .line 745
    const/4 v0, 0x2

    sput v0, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->SOUND_PAUSE:I

    .line 746
    const/4 v0, 0x3

    sput v0, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->SOUND_STOP:I

    .line 747
    const/4 v0, 0x4

    sput v0, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->SOUND_LOCK:I

    .line 748
    const/4 v0, 0x5

    sput v0, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->SOUND_UNLOCK:I

    .line 749
    const/4 v0, 0x6

    sput v0, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->SOUND_BUTTON:I

    .line 750
    const/4 v0, 0x7

    sput v0, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->SOUND_CHECK:I

    .line 751
    const/16 v0, 0x8

    sput v0, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->SOUND_TOUCH:I

    .line 752
    const/16 v0, 0x9

    sput v0, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->SOUND_MATE_EFFECT:I

    .line 753
    const/16 v0, 0xa

    sput v0, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->SOUND_POSITIVE:I

    .line 754
    const/16 v0, 0xb

    sput v0, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->SOUND_NEGATIVE:I

    .line 755
    const/16 v0, 0xc

    sput v0, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->SOUND_SUCCESS_ALERT:I

    .line 756
    const/16 v0, 0xd

    sput v0, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->SOUND_REMIND:I

    .line 757
    const/16 v0, 0xe

    sput v0, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->SOUND_GRADUATION_CONTROL:I

    .line 758
    const/16 v0, 0xf

    sput v0, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->SOUND_CHECKBOX:I

    .line 762
    sput v4, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->SOUND_MODE:I

    .line 764
    sput-object v3, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->soundPlayer:Lcom/sec/android/app/shealth/plugins/common/utils/appSoundplayer;

    .line 902
    sput v4, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->mGoalCalorie:I

    .line 903
    sput-object v3, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->mListGoal:Ljava/util/ArrayList;

    .line 941
    sput-object v3, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->exerciseAll:[Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static DeletePedoData(Landroid/content/Context;J)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "time"    # J

    .prologue
    .line 1033
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.shealth.command.deletetotalstep"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1034
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "selectedDate"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1035
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1036
    return-void
.end method

.method public static SecToContentDescription(JLandroid/content/Context;)Ljava/lang/String;
    .locals 13
    .param p0, "milliseconds"    # J
    .param p2, "con"    # Landroid/content/Context;

    .prologue
    const-wide/16 v9, 0x3c

    const v12, 0x7f090216

    const v11, 0x7f0901ce

    .line 866
    const-wide/16 v7, 0x3e8

    div-long v7, p0, v7

    rem-long v5, v7, v9

    .line 867
    .local v5, "second":J
    const-wide/32 v7, 0xea60

    div-long v7, p0, v7

    rem-long v3, v7, v9

    .line 868
    .local v3, "minute":J
    const-wide/32 v7, 0x36ee80

    div-long v7, p0, v7

    const-wide/16 v9, 0x18

    rem-long v1, v7, v9

    .line 870
    .local v1, "hour":J
    const-wide/16 v7, 0x0

    cmp-long v7, v1, v7

    if-lez v7, :cond_1

    .line 871
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 872
    .local v0, "HourString":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_0

    .line 873
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "0"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 875
    :cond_0
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const v8, 0x7f0901cc

    invoke-virtual {p2, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p2, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p2, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 881
    .end local v0    # "HourString":Ljava/lang/String;
    :goto_0
    return-object v7

    :cond_1
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p2, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p2, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    goto :goto_0
.end method

.method public static SecToString(JLandroid/content/Context;)Ljava/lang/String;
    .locals 7
    .param p0, "milliseconds"    # J
    .param p2, "con"    # Landroid/content/Context;

    .prologue
    .line 843
    const v0, 0x36ee80

    .line 844
    .local v0, "Hour":I
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string/jumbo v3, "mm:ss"

    invoke-direct {v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 845
    .local v2, "simpleDateFormat":Ljava/text/SimpleDateFormat;
    const-string v3, "GMT"

    invoke-static {v3}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 846
    int-to-long v3, v0

    div-long v3, p0, v3

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-lez v3, :cond_1

    .line 847
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    int-to-long v4, v0

    div-long v4, p0, v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 848
    .local v1, "HourString":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 849
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "0"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 851
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v4, Ljava/util/Date;

    invoke-direct {v4, p0, p1}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 855
    .end local v1    # "HourString":Ljava/lang/String;
    :goto_0
    return-object v3

    :cond_1
    new-instance v3, Ljava/util/Date;

    invoke-direct {v3, p0, p1}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method public static areStringsEqual(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p0, "s1"    # Ljava/lang/String;
    .param p1, "s2"    # Ljava/lang/String;

    .prologue
    .line 379
    if-eqz p0, :cond_1

    .line 380
    if-eqz p1, :cond_0

    .line 381
    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 386
    :goto_0
    return v0

    .line 383
    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    goto :goto_0

    .line 386
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static convertDpToPx(Landroid/content/Context;F)F
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "dp"    # F

    .prologue
    .line 489
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->convertDpToPx(Landroid/content/res/Resources;F)F

    move-result v0

    return v0
.end method

.method public static convertDpToPx(Landroid/content/res/Resources;F)F
    .locals 1
    .param p0, "resources"    # Landroid/content/res/Resources;
    .param p1, "dp"    # F

    .prologue
    .line 501
    invoke-virtual {p0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->convertDpToPx(Landroid/util/DisplayMetrics;F)F

    move-result v0

    return v0
.end method

.method public static convertDpToPx(Landroid/util/DisplayMetrics;F)F
    .locals 1
    .param p0, "displayMetrics"    # Landroid/util/DisplayMetrics;
    .param p1, "dp"    # F

    .prologue
    .line 513
    const/4 v0, 0x1

    invoke-static {v0, p1, p0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    return v0
.end method

.method public static getImagePathByUri(Landroid/net/Uri;Landroid/content/Context;)Ljava/lang/String;
    .locals 11
    .param p0, "uri"    # Landroid/net/Uri;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v10, 0x0

    const/4 v3, 0x0

    .line 648
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "_data"

    aput-object v0, v2, v10

    .line 649
    .local v2, "filePathColumn":[Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v1, p0

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    invoke-virtual/range {v0 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;

    move-result-object v8

    .line 650
    .local v8, "cursor":Landroid/database/Cursor;
    if-eqz v8, :cond_0

    .line 651
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 652
    aget-object v0, v2, v10

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    .line 653
    .local v7, "columnIndex":I
    invoke-interface {v8, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 654
    .local v9, "filePath":Ljava/lang/String;
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 657
    .end local v7    # "columnIndex":I
    .end local v9    # "filePath":Ljava/lang/String;
    :goto_0
    return-object v9

    :cond_0
    invoke-virtual {p0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v9

    goto :goto_0
.end method

.method public static getMergedCalorieGetEarlier(JJ)I
    .locals 9
    .param p0, "startTime"    # J
    .param p2, "endTime"    # J

    .prologue
    const/4 v6, 0x0

    .line 988
    new-instance v5, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v5, v7}, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;-><init>(Landroid/content/Context;)V

    .line 990
    .local v5, "mergeCal":Lcom/sec/android/app/shealth/common/utils/MergeCalorie;
    sget-object v7, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->exerciseAll:[Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;

    if-nez v7, :cond_1

    .line 1002
    :cond_0
    :goto_0
    return v6

    .line 993
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 994
    .local v1, "exercise":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;>;"
    sget-object v0, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->exerciseAll:[Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;

    .local v0, "arr$":[Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_1
    if-ge v2, v4, :cond_3

    aget-object v3, v0, v2

    .line 995
    .local v3, "item":Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;
    iget-wide v7, v3, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;->start:J

    cmp-long v7, v7, p0

    if-ltz v7, :cond_2

    iget-wide v7, v3, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;->start:J

    cmp-long v7, v7, p2

    if-gtz v7, :cond_2

    .line 996
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 994
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 999
    .end local v3    # "item":Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;
    :cond_3
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-eqz v7, :cond_0

    .line 1002
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v6

    new-array v6, v6, [Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;->getMergedCalorieGetEarlier([Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;Ljava/util/LinkedList;)F

    move-result v6

    float-to-int v6, v6

    goto :goto_0
.end method

.method public static getPathToImage()Ljava/lang/String;
    .locals 4

    .prologue
    .line 632
    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->GALLERY_PATH:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 633
    .local v0, "dirForFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 634
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 636
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->GALLERY_PATH:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Exercise"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".jpg"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private static getSingleTouchInstance()Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch;
    .locals 1

    .prologue
    .line 329
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch;->getInstance()Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch;

    move-result-object v0

    return-object v0
.end method

.method private static getVersionOfContextProviders(Landroid/content/Context;)I
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 595
    const/4 v2, -0x1

    .line 597
    .local v2, "version":I
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const-string v4, "com.samsung.android.providers.context"

    const/16 v5, 0x80

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 598
    .local v1, "pInfo":Landroid/content/pm/PackageInfo;
    iget v2, v1, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 602
    .end local v1    # "pInfo":Landroid/content/pm/PackageInfo;
    :goto_0
    return v2

    .line 599
    :catch_0
    move-exception v0

    .line 600
    .local v0, "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    sget-object v3, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->TAG:Ljava/lang/String;

    const-string v4, "[SW] Could not find ContextProvider"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static getWeek_three_chars(Landroid/content/Context;J)Ljava/lang/String;
    .locals 3
    .param p0, "con"    # Landroid/content/Context;
    .param p1, "time"    # J

    .prologue
    .line 693
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 694
    .local v0, "CAL":Ljava/util/Calendar;
    invoke-virtual {v0, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 695
    const-string v1, ""

    .line 696
    .local v1, "WEEKDAY":Ljava/lang/String;
    const/4 v2, 0x7

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 719
    :goto_0
    return-object v1

    .line 698
    :pswitch_0
    const v2, 0x7f090d05

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 699
    goto :goto_0

    .line 701
    :pswitch_1
    const v2, 0x7f090d06

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 702
    goto :goto_0

    .line 704
    :pswitch_2
    const v2, 0x7f090d07

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 705
    goto :goto_0

    .line 707
    :pswitch_3
    const v2, 0x7f090d08

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 708
    goto :goto_0

    .line 710
    :pswitch_4
    const v2, 0x7f090d09

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 711
    goto :goto_0

    .line 713
    :pswitch_5
    const v2, 0x7f090d0a

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 714
    goto :goto_0

    .line 716
    :pswitch_6
    const v2, 0x7f090d0b

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 696
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public static hideKeyboard(Landroid/content/Context;Landroid/widget/EditText;)V
    .locals 3
    .param p0, "mContext"    # Landroid/content/Context;
    .param p1, "et"    # Landroid/widget/EditText;

    .prologue
    .line 1009
    const-string v1, "input_method"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 1010
    .local v0, "im":Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {p1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1011
    return-void
.end method

.method public static initializeHapticSound(Landroid/content/Context;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const v5, 0x7f060024

    const/4 v4, 0x1

    .line 774
    new-instance v1, Lcom/sec/android/app/shealth/plugins/common/utils/appSoundplayer;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/common/utils/appSoundplayer;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->soundPlayer:Lcom/sec/android/app/shealth/plugins/common/utils/appSoundplayer;

    .line 775
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 776
    .local v0, "soundMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    sget v1, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->SOUND_START:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f06002c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 777
    sget v1, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->SOUND_PAUSE:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f060029

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 778
    sget v1, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->SOUND_STOP:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f06002d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 779
    sget v1, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->SOUND_LOCK:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f060026

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 780
    sget v1, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->SOUND_UNLOCK:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f060030

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 781
    sget v1, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->SOUND_BUTTON:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f060020

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 782
    sget v1, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->SOUND_CHECK:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f060021

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 783
    sget v1, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->SOUND_TOUCH:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f06002f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 784
    sget v1, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->SOUND_MATE_EFFECT:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f06001d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 785
    sget v1, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->SOUND_POSITIVE:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f06002a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 786
    sget v1, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->SOUND_NEGATIVE:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f060028

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 787
    sget v1, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->SOUND_SUCCESS_ALERT:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f06002e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 788
    sget v1, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->SOUND_REMIND:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f06002b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 789
    sget v1, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->SOUND_GRADUATION_CONTROL:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 790
    sget v1, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->SOUND_CHECKBOX:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f060022

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 792
    sget-object v1, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->soundPlayer:Lcom/sec/android/app/shealth/plugins/common/utils/appSoundplayer;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/plugins/common/utils/appSoundplayer;->initPlayer(Ljava/util/Map;)V

    .line 794
    new-instance v1, Landroid/media/SoundPool;

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-direct {v1, v2, v4, v3}, Landroid/media/SoundPool;-><init>(III)V

    sput-object v1, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->sTicker:Landroid/media/SoundPool;

    .line 795
    sget-object v1, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->sTicker:Landroid/media/SoundPool;

    invoke-virtual {v1, p0, v5, v4}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v1

    sput v1, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->sTickSoundId:I

    .line 796
    return-void
.end method

.method public static isAvailableBarometer(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 1018
    if-nez p0, :cond_1

    .line 1023
    :cond_0
    :goto_0
    return v0

    .line 1020
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "android.hardware.sensor.barometer"

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1021
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static isGoalAchived(JI)Z
    .locals 7
    .param p0, "date"    # J
    .param p2, "totalCalorie"    # I

    .prologue
    .line 923
    sget v3, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->mGoalCalorie:I

    .line 924
    .local v3, "goalCalorie":I
    invoke-static {p0, p1}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v0

    .line 926
    .local v0, "endOfDay":J
    sget-object v5, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->mListGoal:Ljava/util/ArrayList;

    if-eqz v5, :cond_1

    .line 927
    sget-object v5, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->mListGoal:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    .line 928
    .local v2, "goal":Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->getCreateTime()J

    move-result-wide v5

    cmp-long v5, v5, v0

    if-gtz v5, :cond_0

    .line 929
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->getValue()F

    move-result v5

    float-to-int v3, v5

    .line 935
    .end local v2    # "goal":Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;
    .end local v4    # "i$":Ljava/util/Iterator;
    :cond_1
    if-lt p2, v3, :cond_2

    .line 936
    const/4 v5, 0x1

    .line 938
    :goto_0
    return v5

    :cond_2
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public static isMenuSupported()Z
    .locals 2

    .prologue
    .line 96
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static makeSingleViewSingleTouchable(Landroid/view/View;)V
    .locals 1
    .param p0, "view"    # Landroid/view/View;

    .prologue
    .line 325
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->getSingleTouchInstance()Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->makeSingleViewSingleTouchable(Landroid/view/View;Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch;)V

    .line 326
    return-void
.end method

.method public static makeSingleViewSingleTouchable(Landroid/view/View;Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch;)V
    .locals 1
    .param p0, "view"    # Landroid/view/View;
    .param p1, "singleTouchInstance"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch;

    .prologue
    .line 321
    invoke-virtual {p1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch;->getTouchDelegate(Landroid/view/View;)Landroid/view/TouchDelegate;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/view/View;->setTouchDelegate(Landroid/view/TouchDelegate;)V

    .line 322
    return-void
.end method

.method public static varargs makeViewsBlockSingleTouchable([Landroid/view/View;)V
    .locals 5
    .param p0, "viewsToBeMadeSingleTouchable"    # [Landroid/view/View;

    .prologue
    .line 333
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->getSingleTouchInstance()Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch;

    move-result-object v3

    .line 334
    .local v3, "singleTouchInstance":Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch;
    move-object v0, p0

    .local v0, "arr$":[Landroid/view/View;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v4, v0, v1

    .line 335
    .local v4, "view":Landroid/view/View;
    invoke-static {v4, v3}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->makeSingleViewSingleTouchable(Landroid/view/View;Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch;)V

    .line 334
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 337
    .end local v4    # "view":Landroid/view/View;
    :cond_0
    return-void
.end method

.method public static playSound(I)V
    .locals 7
    .param p0, "soundIdx"    # I

    .prologue
    const/4 v4, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    .line 799
    sget v0, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->SOUND_GRADUATION_CONTROL:I

    if-ne p0, v0, :cond_0

    sget-object v0, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->sTicker:Landroid/media/SoundPool;

    if-eqz v0, :cond_0

    .line 800
    sget-object v0, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->sTicker:Landroid/media/SoundPool;

    sget v1, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->sTickSoundId:I

    move v3, v2

    move v5, v4

    move v6, v2

    invoke-virtual/range {v0 .. v6}, Landroid/media/SoundPool;->play(IFFIIF)I

    .line 809
    :goto_0
    return-void

    .line 803
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->soundPlayer:Lcom/sec/android/app/shealth/plugins/common/utils/appSoundplayer;

    if-eqz v0, :cond_1

    .line 804
    sget-object v0, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->soundPlayer:Lcom/sec/android/app/shealth/plugins/common/utils/appSoundplayer;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/shealth/plugins/common/utils/appSoundplayer;->playSoundIndex(I)V

    goto :goto_0

    .line 806
    :cond_1
    sget-object v0, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->TAG:Ljava/lang/String;

    const-string v1, "Sound player is null while start syncing. Did you initialize sound player?"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static printExerciseLog(Ljava/lang/String;)V
    .locals 21
    .param p0, "msg"    # Ljava/lang/String;

    .prologue
    .line 102
    if-eqz p0, :cond_0

    const-string v17, ""

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_1

    .line 171
    :cond_0
    :goto_0
    return-void

    .line 107
    :cond_1
    const/4 v15, 0x0

    .line 111
    .local v15, "out":Ljava/io/PrintWriter;
    :try_start_0
    new-instance v14, Ljava/io/File;

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "/Android/data/com.sec.android.app.shealth/file/exercise_logs"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-direct {v14, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 112
    .local v14, "logFolder":Ljava/io/File;
    invoke-virtual {v14}, Ljava/io/File;->exists()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v17

    if-nez v17, :cond_2

    .line 166
    if-eqz v15, :cond_0

    .line 168
    invoke-virtual {v15}, Ljava/io/PrintWriter;->close()V

    goto :goto_0

    .line 114
    :cond_2
    :try_start_1
    new-instance v11, Ljava/io/File;

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "/"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "ExerciseLoggingEnable"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-direct {v11, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 115
    .local v11, "logCheckFile":Ljava/io/File;
    invoke-virtual {v11}, Ljava/io/File;->exists()Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v17

    if-nez v17, :cond_3

    .line 166
    if-eqz v15, :cond_0

    .line 168
    invoke-virtual {v15}, Ljava/io/PrintWriter;->close()V

    goto :goto_0

    .line 118
    :cond_3
    :try_start_2
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v5

    .line 119
    .local v5, "currentTime":Ljava/util/Calendar;
    new-instance v12, Ljava/io/File;

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "/"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    sget-object v18, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->formatterYMDH:Ljava/text/SimpleDateFormat;

    invoke-virtual {v5}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, ".log"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-direct {v12, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 123
    .local v12, "logFile":Ljava/io/File;
    invoke-virtual {v12}, Ljava/io/File;->createNewFile()Z

    move-result v17

    if-eqz v17, :cond_6

    .line 125
    invoke-virtual {v14}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v13

    .line 126
    .local v13, "logFiles":[Ljava/io/File;
    move-object v3, v13

    .local v3, "arr$":[Ljava/io/File;
    array-length v9, v3

    .local v9, "len$":I
    const/4 v8, 0x0

    .local v8, "i$":I
    :goto_1
    if-ge v8, v9, :cond_6

    aget-object v7, v3, v8
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 130
    .local v7, "f":Ljava/io/File;
    :try_start_3
    invoke-virtual {v7}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v17

    const-string v18, "ExerciseLoggingEnable"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v17

    if-eqz v17, :cond_5

    .line 126
    :cond_4
    :goto_2
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 132
    :cond_5
    invoke-virtual {v5}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v17

    sget-object v19, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->formatterYMDH:Ljava/text/SimpleDateFormat;

    invoke-virtual {v7}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/util/Date;->getTime()J

    move-result-wide v19

    sub-long v17, v17, v19

    const-wide v19, 0x9a7ec800L

    cmp-long v17, v17, v19

    if-lez v17, :cond_4

    .line 134
    invoke-virtual {v7}, Ljava/io/File;->delete()Z
    :try_end_3
    .catch Ljava/text/ParseException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    .line 137
    :catch_0
    move-exception v6

    .line 139
    .local v6, "e":Ljava/text/ParseException;
    :try_start_4
    sget-object v17, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->TAG:Ljava/lang/String;

    const-string v18, "File format is not the custom log file"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    .line 156
    .end local v3    # "arr$":[Ljava/io/File;
    .end local v5    # "currentTime":Ljava/util/Calendar;
    .end local v6    # "e":Ljava/text/ParseException;
    .end local v7    # "f":Ljava/io/File;
    .end local v8    # "i$":I
    .end local v9    # "len$":I
    .end local v11    # "logCheckFile":Ljava/io/File;
    .end local v12    # "logFile":Ljava/io/File;
    .end local v13    # "logFiles":[Ljava/io/File;
    .end local v14    # "logFolder":Ljava/io/File;
    :catch_1
    move-exception v6

    .line 158
    .local v6, "e":Ljava/io/IOException;
    :goto_3
    :try_start_5
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 166
    if-eqz v15, :cond_0

    .line 168
    invoke-virtual {v15}, Ljava/io/PrintWriter;->close()V

    goto/16 :goto_0

    .line 143
    .end local v6    # "e":Ljava/io/IOException;
    .restart local v5    # "currentTime":Ljava/util/Calendar;
    .restart local v11    # "logCheckFile":Ljava/io/File;
    .restart local v12    # "logFile":Ljava/io/File;
    .restart local v14    # "logFolder":Ljava/io/File;
    :cond_6
    :try_start_6
    new-instance v16, Ljava/io/PrintWriter;

    new-instance v17, Ljava/io/BufferedWriter;

    new-instance v18, Ljava/io/FileWriter;

    const/16 v19, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-direct {v0, v12, v1}, Ljava/io/FileWriter;-><init>(Ljava/io/File;Z)V

    invoke-direct/range {v17 .. v18}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    invoke-direct/range {v16 .. v17}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 144
    .end local v15    # "out":Ljava/io/PrintWriter;
    .local v16, "out":Ljava/io/PrintWriter;
    const/16 v10, 0x1000

    .line 145
    .local v10, "length":I
    const/4 v4, 0x1

    .line 147
    .local v4, "count":I
    :try_start_7
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v18, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->formatterYMDHMS:Ljava/text/SimpleDateFormat;

    invoke-virtual {v5}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 148
    :goto_4
    invoke-virtual/range {p0 .. p0}, Ljava/lang/String;->length()I

    move-result v17

    mul-int v18, v10, v4

    move/from16 v0, v17

    move/from16 v1, v18

    if-le v0, v1, :cond_7

    .line 150
    add-int/lit8 v17, v4, -0x1

    mul-int v17, v17, v10

    mul-int v18, v10, v4

    move-object/from16 v0, p0

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 151
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 153
    :cond_7
    add-int/lit8 v17, v4, -0x1

    mul-int v17, v17, v10

    invoke-virtual/range {p0 .. p0}, Ljava/lang/String;->length()I

    move-result v18

    move-object/from16 v0, p0

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 166
    if-eqz v16, :cond_9

    .line 168
    invoke-virtual/range {v16 .. v16}, Ljava/io/PrintWriter;->close()V

    move-object/from16 v15, v16

    .end local v16    # "out":Ljava/io/PrintWriter;
    .restart local v15    # "out":Ljava/io/PrintWriter;
    goto/16 :goto_0

    .line 160
    .end local v4    # "count":I
    .end local v5    # "currentTime":Ljava/util/Calendar;
    .end local v10    # "length":I
    .end local v11    # "logCheckFile":Ljava/io/File;
    .end local v12    # "logFile":Ljava/io/File;
    .end local v14    # "logFolder":Ljava/io/File;
    :catch_2
    move-exception v17

    .line 166
    :goto_5
    if-eqz v15, :cond_0

    .line 168
    invoke-virtual {v15}, Ljava/io/PrintWriter;->close()V

    goto/16 :goto_0

    .line 166
    :catchall_0
    move-exception v17

    :goto_6
    if-eqz v15, :cond_8

    .line 168
    invoke-virtual {v15}, Ljava/io/PrintWriter;->close()V

    :cond_8
    throw v17

    .line 166
    .end local v15    # "out":Ljava/io/PrintWriter;
    .restart local v4    # "count":I
    .restart local v5    # "currentTime":Ljava/util/Calendar;
    .restart local v10    # "length":I
    .restart local v11    # "logCheckFile":Ljava/io/File;
    .restart local v12    # "logFile":Ljava/io/File;
    .restart local v14    # "logFolder":Ljava/io/File;
    .restart local v16    # "out":Ljava/io/PrintWriter;
    :catchall_1
    move-exception v17

    move-object/from16 v15, v16

    .end local v16    # "out":Ljava/io/PrintWriter;
    .restart local v15    # "out":Ljava/io/PrintWriter;
    goto :goto_6

    .line 160
    .end local v15    # "out":Ljava/io/PrintWriter;
    .restart local v16    # "out":Ljava/io/PrintWriter;
    :catch_3
    move-exception v17

    move-object/from16 v15, v16

    .end local v16    # "out":Ljava/io/PrintWriter;
    .restart local v15    # "out":Ljava/io/PrintWriter;
    goto :goto_5

    .line 156
    .end local v15    # "out":Ljava/io/PrintWriter;
    .restart local v16    # "out":Ljava/io/PrintWriter;
    :catch_4
    move-exception v6

    move-object/from16 v15, v16

    .end local v16    # "out":Ljava/io/PrintWriter;
    .restart local v15    # "out":Ljava/io/PrintWriter;
    goto/16 :goto_3

    .end local v15    # "out":Ljava/io/PrintWriter;
    .restart local v16    # "out":Ljava/io/PrintWriter;
    :cond_9
    move-object/from16 v15, v16

    .end local v16    # "out":Ljava/io/PrintWriter;
    .restart local v15    # "out":Ljava/io/PrintWriter;
    goto/16 :goto_0
.end method

.method public static recursiveRecycle(Landroid/view/View;)V
    .locals 5
    .param p0, "root"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x0

    .line 607
    if-nez p0, :cond_0

    .line 627
    :goto_0
    return-void

    .line 610
    :cond_0
    invoke-virtual {p0, v4}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 611
    instance-of v3, p0, Landroid/view/ViewGroup;

    if-eqz v3, :cond_2

    move-object v1, p0

    .line 612
    check-cast v1, Landroid/view/ViewGroup;

    .line 613
    .local v1, "group":Landroid/view/ViewGroup;
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    .line 614
    .local v0, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v0, :cond_1

    .line 615
    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->recursiveRecycle(Landroid/view/View;)V

    .line 614
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 618
    :cond_1
    instance-of v3, p0, Landroid/widget/AdapterView;

    if-nez v3, :cond_2

    .line 619
    invoke-virtual {v1}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 623
    .end local v0    # "count":I
    .end local v1    # "group":Landroid/view/ViewGroup;
    .end local v2    # "i":I
    :cond_2
    instance-of v3, p0, Landroid/widget/ImageView;

    if-eqz v3, :cond_3

    .line 624
    check-cast p0, Landroid/widget/ImageView;

    .end local p0    # "root":Landroid/view/View;
    invoke-virtual {p0, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 626
    :cond_3
    const/4 p0, 0x0

    .line 627
    .restart local p0    # "root":Landroid/view/View;
    goto :goto_0
.end method

.method public static refreshExerciseInfo(Landroid/content/Context;)V
    .locals 11
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v9, 0x2

    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 943
    const/4 v6, 0x0

    .line 945
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->exerciseAll:[Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;

    .line 947
    const/4 v0, 0x3

    new-array v2, v0, [Ljava/lang/String;

    .line 948
    .local v2, "projection":[Ljava/lang/String;
    const-string/jumbo v0, "start_time"

    aput-object v0, v2, v1

    .line 949
    const-string v0, "end_time"

    aput-object v0, v2, v5

    .line 950
    const-string/jumbo v0, "total_calorie"

    aput-object v0, v2, v9

    .line 952
    const-string v3, "end_time >= ? AND exercise_type != ? "

    .line 953
    .local v3, "selection":Ljava/lang/String;
    new-array v4, v9, [Ljava/lang/String;

    .line 954
    .local v4, "selectionArgs":[Ljava/lang/String;
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v1

    .line 955
    const/16 v0, 0x4e23

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    .line 957
    const/4 v8, 0x0

    .line 958
    .local v8, "itemCount":I
    const/4 v7, 0x0

    .line 961
    .local v7, "i":I
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Exercise;->CONTENT_URI:Landroid/net/Uri;

    const-string/jumbo v5, "start_time ASC "

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 964
    if-eqz v6, :cond_1

    .line 966
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v8

    .line 967
    sget-object v0, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exercise Count = "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 968
    if-lez v8, :cond_1

    .line 969
    new-array v0, v8, [Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;

    sput-object v0, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->exerciseAll:[Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;

    .line 971
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_1

    .line 972
    sget-object v0, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->exerciseAll:[Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;

    new-instance v1, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;

    invoke-direct {v1}, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;-><init>()V

    aput-object v1, v0, v7

    .line 973
    sget-object v0, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->exerciseAll:[Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;

    aget-object v0, v0, v7

    const-string/jumbo v1, "start_time"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    iput-wide v9, v0, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;->start:J

    .line 974
    sget-object v0, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->exerciseAll:[Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;

    aget-object v0, v0, v7

    const-string v1, "end_time"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    iput-wide v9, v0, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;->end:J

    .line 975
    sget-object v0, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->exerciseAll:[Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;

    aget-object v0, v0, v7

    const-string/jumbo v1, "total_calorie"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v1

    iput v1, v0, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;->calorie:F

    .line 976
    add-int/lit8 v7, v7, 0x1

    .line 971
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 982
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_0

    .line 983
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_0
    throw v0

    .line 982
    :cond_1
    if-eqz v6, :cond_2

    .line 983
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 985
    :cond_2
    return-void
.end method

.method public static refreshGoalInfo(Landroid/content/Context;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 906
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/NetCalorieUtils;->getNetCalorie()I

    move-result v0

    sput v0, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->mGoalCalorie:I

    .line 907
    sput-object v1, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->mListGoal:Ljava/util/ArrayList;

    .line 908
    const/4 v6, 0x0

    .line 909
    .local v6, "cursor":Landroid/database/Cursor;
    const-string/jumbo v3, "select * from goal where goal_type == 40002 order by create_time desc"

    .line 911
    .local v3, "selectionClause":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 912
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 913
    invoke-static {v6}, Lcom/sec/android/app/shealth/common/utils/hcp/GoalUtils;->getGoalList(Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->mListGoal:Ljava/util/ArrayList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 916
    :cond_0
    if-eqz v6, :cond_1

    .line 917
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 920
    :cond_1
    return-void

    .line 916
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 917
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method public static refreshLastTakenPhoto(Landroid/content/Context;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 812
    new-instance v1, Ljava/io/File;

    sget-object v3, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->GALLERY_PATH:Ljava/lang/String;

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 813
    .local v1, "folder":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v0

    .line 814
    .local v0, "allFiles":[Ljava/lang/String;
    if-eqz v0, :cond_0

    array-length v3, v0

    if-lez v3, :cond_0

    .line 815
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->GALLERY_PATH:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    array-length v4, v0

    add-int/lit8 v4, v4, -0x1

    aget-object v4, v0, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 817
    .local v2, "scanPath":Ljava/lang/String;
    new-instance v3, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon$1;

    invoke-direct {v3, p0, v2}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon$1;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 840
    .end local v2    # "scanPath":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public static setImageDrawable(Landroid/widget/ImageView;Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p0, "view"    # Landroid/widget/ImageView;
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 679
    if-eqz p0, :cond_0

    .line 680
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 681
    invoke-virtual {p0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 683
    :cond_0
    return-void
.end method

.method public static setImageResource(Landroid/widget/ImageView;I)V
    .locals 1
    .param p0, "view"    # Landroid/widget/ImageView;
    .param p1, "resId"    # I

    .prologue
    .line 674
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 675
    invoke-virtual {p0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 676
    return-void
.end method

.method public static showKeyboard(Landroid/content/Context;Landroid/widget/EditText;)V
    .locals 2
    .param p0, "mContext"    # Landroid/content/Context;
    .param p1, "et"    # Landroid/widget/EditText;

    .prologue
    .line 1005
    const-string v1, "input_method"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 1006
    .local v0, "im":Landroid/view/inputmethod/InputMethodManager;
    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 1007
    return-void
.end method

.method public static showToast(Landroid/content/Context;II)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "resId"    # I
    .param p2, "durationType"    # I

    .prologue
    .line 591
    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, p2}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->showToast(Landroid/content/Context;Ljava/lang/String;I)V

    .line 592
    return-void
.end method

.method public static showToast(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "msg"    # Ljava/lang/String;
    .param p2, "durationType"    # I

    .prologue
    const/high16 v7, 0x41900000    # 18.0f

    const/high16 v8, 0x41200000    # 10.0f

    .line 563
    const/4 v0, 0x0

    .line 565
    .local v0, "length":I
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090a87

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 566
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090a89

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 567
    .local v2, "toast_msg":Ljava/lang/String;
    const/4 v0, 0x1

    .line 578
    :goto_0
    invoke-static {p0, p1, p2}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v1

    .line 580
    .local v1, "toast":Landroid/widget/Toast;
    invoke-virtual {v1}, Landroid/widget/Toast;->getView()Landroid/view/View;

    move-result-object v4

    .line 581
    .local v4, "view":Landroid/view/View;
    const v5, 0x7f0208b2

    invoke-virtual {v4, v5}, Landroid/view/View;->setBackgroundResource(I)V

    .line 582
    const v5, 0x102000b

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 583
    .local v3, "tv":Landroid/widget/TextView;
    invoke-static {p0, v7}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->convertDpToPx(Landroid/content/Context;F)F

    move-result v5

    float-to-int v5, v5

    invoke-static {p0, v8}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->convertDpToPx(Landroid/content/Context;F)F

    move-result v6

    float-to-int v6, v6

    invoke-static {p0, v7}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->convertDpToPx(Landroid/content/Context;F)F

    move-result v7

    float-to-int v7, v7

    invoke-static {p0, v8}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->convertDpToPx(Landroid/content/Context;F)F

    move-result v8

    float-to-int v8, v8

    invoke-virtual {v3, v5, v6, v7, v8}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 584
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x106000e

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 585
    invoke-virtual {v1, v4}, Landroid/widget/Toast;->setView(Landroid/view/View;)V

    .line 586
    const/16 v5, 0x51

    const/4 v6, 0x0

    const/high16 v7, 0x42c80000    # 100.0f

    invoke-static {p0, v7}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->convertDpToPx(Landroid/content/Context;F)F

    move-result v7

    float-to-int v7, v7

    invoke-virtual {v1, v5, v6, v7}, Landroid/widget/Toast;->setGravity(III)V

    .line 588
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 589
    return-void

    .line 569
    .end local v1    # "toast":Landroid/widget/Toast;
    .end local v2    # "toast_msg":Ljava/lang/String;
    .end local v3    # "tv":Landroid/widget/TextView;
    .end local v4    # "view":Landroid/view/View;
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090a88

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 570
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090acb

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 571
    .restart local v2    # "toast_msg":Ljava/lang/String;
    const/4 v0, 0x1

    goto :goto_0

    .line 574
    .end local v2    # "toast_msg":Ljava/lang/String;
    :cond_1
    move-object v2, p1

    .line 575
    .restart local v2    # "toast_msg":Ljava/lang/String;
    move v0, p2

    goto :goto_0
.end method

.method public static startCamera(Landroid/app/Activity;I)Ljava/lang/String;
    .locals 4
    .param p0, "ac"    # Landroid/app/Activity;
    .param p1, "ResultCode"    # I

    .prologue
    .line 661
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.media.action.IMAGE_CAPTURE"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 662
    .local v0, "cameraIntent":Landroid/content/Intent;
    sget-object v1, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->DEFAULT_GALLERY_PATH:Ljava/lang/String;

    .line 663
    .local v1, "path":Ljava/lang/String;
    const-string v2, "android.intent.extras.CAMERA_FACING"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 669
    invoke-virtual {p0, v0, p1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 670
    return-object v1
.end method

.method public static startExerciseProPhotoGallery(Landroid/content/Context;Ljava/util/List;I)V
    .locals 6
    .param p0, "mContext"    # Landroid/content/Context;
    .param p2, "pos"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 729
    .local p1, "photoList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;>;"
    new-instance v2, Landroid/content/Intent;

    const-class v4, Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;

    invoke-direct {v2, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 731
    .local v2, "intent":Landroid/content/Intent;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 732
    .local v1, "imagePaths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;

    .line 733
    .local v3, "pagerHolder":Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;
    invoke-virtual {v3}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->getFilePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 735
    .end local v3    # "pagerHolder":Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;
    :cond_0
    const-string v4, "gallery_image_paths"

    invoke-virtual {v2, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 736
    const-string v5, "gallery_selected_image_path"

    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->getFilePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 737
    invoke-virtual {p0, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 738
    return-void
.end method
