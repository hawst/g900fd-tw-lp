.class Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$3;
.super Ljava/lang/Object;
.source "ImagePager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->setPhotoBitmap(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;

.field final synthetic val$datas:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 238
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$3;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;

    iput-object p2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$3;->val$datas:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 241
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$3;->val$datas:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;

    .line 242
    .local v1, "data":Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$3;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->getFilePath()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$3;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->mWidth:I
    invoke-static {v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->access$600(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;)I

    move-result v6

    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$3;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->mHeight:I
    invoke-static {v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->access$700(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;)I

    move-result v7

    invoke-static {v4, v5, v6, v7}, Lcom/sec/android/app/shealth/plugins/common/utils/ExercisePhotoUtils;->getImageThumbnail(Landroid/content/Context;Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 243
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    .line 244
    new-instance v2, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$DataHolder;

    invoke-direct {v2, v1, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$DataHolder;-><init>(Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;Landroid/graphics/Bitmap;)V

    .line 245
    .local v2, "holder":Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$DataHolder;
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$3;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;

    iget-object v4, v4, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->updateHandler:Landroid/os/Handler;

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$3;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;

    iget-object v5, v5, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->updateHandler:Landroid/os/Handler;

    const/4 v6, 0x0

    invoke-virtual {v5, v6, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 248
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v1    # "data":Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;
    .end local v2    # "holder":Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$DataHolder;
    :cond_1
    return-void
.end method
