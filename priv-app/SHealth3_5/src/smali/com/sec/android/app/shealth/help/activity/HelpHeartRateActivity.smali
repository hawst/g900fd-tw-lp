.class public Lcom/sec/android/app/shealth/help/activity/HelpHeartRateActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "HelpHeartRateActivity.java"


# instance fields
.field private tip_4_1:Landroid/widget/TextView;

.field private tip_4_2:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    return-void
.end method

.method private chageString()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 34
    iget-object v0, p0, Lcom/sec/android/app/shealth/help/activity/HelpHeartRateActivity;->tip_4_1:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/help/activity/HelpHeartRateActivity;->tip_4_2:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 35
    iget-object v0, p0, Lcom/sec/android/app/shealth/help/activity/HelpHeartRateActivity;->tip_4_1:Landroid/widget/TextView;

    const v1, 0x7f091015

    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x5

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/shealth/help/activity/HelpHeartRateActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 36
    iget-object v0, p0, Lcom/sec/android/app/shealth/help/activity/HelpHeartRateActivity;->tip_4_2:Landroid/widget/TextView;

    const v1, 0x7f091016

    new-array v2, v5, [Ljava/lang/Object;

    const/16 v3, 0x1e

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/shealth/help/activity/HelpHeartRateActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 38
    :cond_0
    return-void
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 2

    .prologue
    .line 49
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    .line 50
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getHRAvailablilty(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 51
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/help/activity/HelpHeartRateActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const v1, 0x7f091007

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 58
    :goto_0
    return-void

    .line 55
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/help/activity/HelpHeartRateActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const v1, 0x7f091006

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 44
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 17
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 18
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getHRAvailablilty(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 19
    const v0, 0x7f030128

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/help/activity/HelpHeartRateActivity;->setContentView(I)V

    .line 31
    :goto_0
    return-void

    .line 23
    :cond_0
    const v0, 0x7f030201

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/help/activity/HelpHeartRateActivity;->setContentView(I)V

    .line 25
    const v0, 0x7f080911

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/help/activity/HelpHeartRateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/help/activity/HelpHeartRateActivity;->tip_4_1:Landroid/widget/TextView;

    .line 26
    const v0, 0x7f080913

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/help/activity/HelpHeartRateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/help/activity/HelpHeartRateActivity;->tip_4_2:Landroid/widget/TextView;

    .line 28
    invoke-direct {p0}, Lcom/sec/android/app/shealth/help/activity/HelpHeartRateActivity;->chageString()V

    goto :goto_0
.end method
