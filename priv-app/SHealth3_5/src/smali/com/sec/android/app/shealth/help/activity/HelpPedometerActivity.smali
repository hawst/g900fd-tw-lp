.class public Lcom/sec/android/app/shealth/help/activity/HelpPedometerActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "HelpPedometerActivity.java"


# instance fields
.field private mHasDevicePedo:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 2

    .prologue
    .line 76
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    .line 77
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/help/activity/HelpPedometerActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->isSensorHubSupported()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f090fdf

    :goto_0
    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 78
    return-void

    .line 77
    :cond_0
    const v0, 0x7f09110c

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 11
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v10, 0x7f0808fa

    const v7, 0x7f0804e0

    const/16 v8, 0x8

    const/4 v9, 0x0

    .line 26
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 28
    const v6, 0x7f030207

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/help/activity/HelpPedometerActivity;->setContentView(I)V

    .line 30
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->isSensorHubSupported()Z

    move-result v6

    iput-boolean v6, p0, Lcom/sec/android/app/shealth/help/activity/HelpPedometerActivity;->mHasDevicePedo:Z

    .line 32
    iget-boolean v6, p0, Lcom/sec/android/app/shealth/help/activity/HelpPedometerActivity;->mHasDevicePedo:Z

    if-nez v6, :cond_0

    .line 36
    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/help/activity/HelpPedometerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/view/View;->setVisibility(I)V

    .line 38
    invoke-virtual {p0, v10}, Lcom/sec/android/app/shealth/help/activity/HelpPedometerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v9}, Landroid/view/View;->setVisibility(I)V

    .line 40
    const v6, 0x7f08090c

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/help/activity/HelpPedometerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/view/View;->setVisibility(I)V

    .line 43
    const v6, 0x7f080926

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/help/activity/HelpPedometerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    const v7, 0x7f09110e

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(I)V

    .line 45
    const v6, 0x7f08090f

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/help/activity/HelpPedometerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/view/View;->setVisibility(I)V

    .line 49
    const v6, 0x7f08092a

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/help/activity/HelpPedometerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    const v7, 0x7f090bd2

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(I)V

    .line 72
    :goto_0
    return-void

    .line 54
    :cond_0
    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/help/activity/HelpPedometerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v9}, Landroid/view/View;->setVisibility(I)V

    .line 55
    invoke-virtual {p0, v10}, Lcom/sec/android/app/shealth/help/activity/HelpPedometerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/view/View;->setVisibility(I)V

    .line 56
    const v6, 0x7f080927

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/help/activity/HelpPedometerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 57
    .local v5, "stepGoal":Landroid/widget/TextView;
    const v6, 0x7f080928

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/help/activity/HelpPedometerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 59
    .local v4, "selectDeviceToView":Landroid/widget/TextView;
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v2, v6}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 60
    .local v2, "sb":Landroid/text/SpannableStringBuilder;
    new-instance v3, Landroid/text/SpannableStringBuilder;

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v6}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 61
    .local v3, "sb2":Landroid/text/SpannableStringBuilder;
    const-string v6, "- "

    invoke-virtual {v2, v9, v6}, Landroid/text/SpannableStringBuilder;->insert(ILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 62
    const-string v6, "- "

    invoke-virtual {v3, v9, v6}, Landroid/text/SpannableStringBuilder;->insert(ILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 63
    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    const-string v7, ":"

    invoke-virtual {v6, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 64
    .local v0, "i":I
    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    const-string v7, ":"

    invoke-virtual {v6, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 68
    .local v1, "j":I
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "<b><font color=#000000 font face=sec-roboto-light> "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    add-int/lit8 v7, v0, 0x3

    invoke-virtual {v2, v9, v7}, Landroid/text/SpannableStringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "</font></b>"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    add-int/lit8 v7, v0, 0x4

    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v8

    invoke-virtual {v2, v7, v8}, Landroid/text/SpannableStringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 69
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "<b><font color=#000000 font face=sec-roboto-light> "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    add-int/lit8 v7, v1, 0x3

    invoke-virtual {v3, v9, v7}, Landroid/text/SpannableStringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "</font></b>"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    add-int/lit8 v7, v1, 0x4

    invoke-virtual {v3}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v8

    invoke-virtual {v3, v7, v8}, Landroid/text/SpannableStringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method
