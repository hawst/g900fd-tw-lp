.class Lcom/sec/android/app/shealth/home/EditFavoritesActivity$3;
.super Ljava/lang/Object;
.source "EditFavoritesActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->setIconsToGrid()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/home/EditFavoritesActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/home/EditFavoritesActivity;)V
    .locals 0

    .prologue
    .line 225
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity$3;->this$0:Lcom/sec/android/app/shealth/home/EditFavoritesActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 8
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v5, 0x1

    .line 231
    iget-object v4, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity$3;->this$0:Lcom/sec/android/app/shealth/home/EditFavoritesActivity;

    # getter for: Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->adapter:Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter;
    invoke-static {v4}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->access$100(Lcom/sec/android/app/shealth/home/EditFavoritesActivity;)Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter;

    move-result-object v4

    invoke-virtual {v4, p3}, Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/home/data/IconData;

    .line 232
    .local v1, "icondata":Lcom/sec/android/app/shealth/home/data/IconData;
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/home/data/IconData;->getIconType()I

    move-result v4

    if-ne v4, v5, :cond_0

    .line 234
    new-instance v2, Landroid/content/Intent;

    iget-object v4, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity$3;->this$0:Lcom/sec/android/app/shealth/home/EditFavoritesActivity;

    const-class v5, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 235
    .local v2, "intent":Landroid/content/Intent;
    const/high16 v4, 0x24000000

    invoke-virtual {v2, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 236
    iget-object v4, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity$3;->this$0:Lcom/sec/android/app/shealth/home/EditFavoritesActivity;

    const/16 v5, 0x65

    invoke-virtual {v4, v2, v5}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 251
    .end local v2    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 240
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity$3;->this$0:Lcom/sec/android/app/shealth/home/EditFavoritesActivity;

    # setter for: Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->isChanged:Z
    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->access$202(Lcom/sec/android/app/shealth/home/EditFavoritesActivity;Z)Z

    .line 241
    iget-object v4, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity$3;->this$0:Lcom/sec/android/app/shealth/home/EditFavoritesActivity;

    # getter for: Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->adapter:Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter;
    invoke-static {v4}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->access$100(Lcom/sec/android/app/shealth/home/EditFavoritesActivity;)Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter;

    move-result-object v4

    invoke-virtual {v4, p3}, Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/home/data/IconData;

    .line 242
    .local v0, "iconData":Lcom/sec/android/app/shealth/home/data/IconData;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/home/data/IconData;->getPackageName()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/home/data/IconData;->getAppPluginId()I

    move-result v7

    invoke-static {v4, v5, v6, v7}, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryDbManagerWithProvider;->updateAppFavoriteStatus(Landroid/content/Context;Ljava/lang/String;II)I

    .line 243
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/home/data/IconData;->getAppPluginId()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/home/data/IconData;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->remove(Ljava/lang/String;)V

    .line 244
    iget-object v4, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity$3;->this$0:Lcom/sec/android/app/shealth/home/EditFavoritesActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "com.sec.android.app.shealth"

    const-string v6, "HF02"

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/home/data/IconData;->getAppName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v5, v6, v7}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    invoke-static {}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->getTotalFavoriteCount()I

    move-result v3

    .line 246
    .local v3, "totalCount":I
    add-int/lit8 v4, v3, -0x1

    invoke-static {v4}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->setTotalFavoriteCount(I)V

    .line 247
    iget-object v4, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity$3;->this$0:Lcom/sec/android/app/shealth/home/EditFavoritesActivity;

    # getter for: Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->adapter:Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter;
    invoke-static {v4}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->access$100(Lcom/sec/android/app/shealth/home/EditFavoritesActivity;)Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter;->remove(Ljava/lang/Object;)V

    .line 248
    iget-object v4, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity$3;->this$0:Lcom/sec/android/app/shealth/home/EditFavoritesActivity;

    # invokes: Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->setFavOrder()V
    invoke-static {v4}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->access$300(Lcom/sec/android/app/shealth/home/EditFavoritesActivity;)V

    .line 249
    iget-object v4, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity$3;->this$0:Lcom/sec/android/app/shealth/home/EditFavoritesActivity;

    # invokes: Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->setDividers()V
    invoke-static {v4}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->access$400(Lcom/sec/android/app/shealth/home/EditFavoritesActivity;)V

    goto :goto_0
.end method
