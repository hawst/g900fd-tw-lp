.class Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$10;
.super Ljava/lang/Object;
.source "ExerciseProDetailsActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->showDeleteDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;)V
    .locals 0

    .prologue
    .line 612
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onOKButtonClick(Landroid/app/Activity;Landroid/view/View;Landroid/app/Dialog;)V
    .locals 5
    .param p1, "parentActivity"    # Landroid/app/Activity;
    .param p2, "content"    # Landroid/view/View;
    .param p3, "dialog"    # Landroid/app/Dialog;

    .prologue
    .line 615
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->rowId:J
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->access$1200(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;)J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseUtilsEx;->deleteItem(Landroid/content/Context;J)J

    move-result-wide v0

    .line 616
    .local v0, "result":J
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;

    const/4 v3, 0x0

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mDeleteDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->access$1102(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 617
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 618
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;

    const/16 v3, 0x5dc

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->setResult(I)V

    .line 619
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;

    const/4 v3, -0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->setResult(I)V

    .line 620
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->finish()V

    .line 622
    :cond_0
    return-void
.end method
