.class public Lcom/sec/android/app/shealth/framework/ui/base/ShowAlertActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IBackPressControllerProvider;
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/framework/ui/base/ShowAlertActivity$3;,
        Lcom/sec/android/app/shealth/framework/ui/base/ShowAlertActivity$BackupErrorDialogBackPressController;,
        Lcom/sec/android/app/shealth/framework/ui/base/ShowAlertActivity$BackupErrorDialogButtonController;
    }
.end annotation


# static fields
.field private static BACKUP_ERROR:Ljava/lang/String;

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mBackPressMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/shealth/common/commonui/dialog/IBackPressController;",
            ">;"
        }
    .end annotation
.end field

.field private final mDialogControllerMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 15

    const/4 v14, 0x3

    const/4 v13, 0x2

    const/4 v12, 0x1

    const/16 v11, -0x7f

    const/4 v3, 0x0

    const/16 v0, 0xc

    new-array v1, v0, [I

    const/4 v0, 0x4

    const/4 v2, 0x5

    const/4 v4, 0x6

    const/4 v5, 0x7

    const/16 v6, 0x8

    const/16 v7, 0x9

    const/16 v8, 0xa

    const/16 v9, 0xb

    const/4 v10, -0x6

    aput v10, v1, v9

    const/16 v9, -0x22

    aput v9, v1, v8

    aput v11, v1, v7

    const/16 v7, -0x77

    aput v7, v1, v6

    aput v11, v1, v5

    const/16 v5, -0x21

    aput v5, v1, v4

    const/16 v4, -0x63

    aput v4, v1, v2

    const/16 v2, -0x4da1

    aput v2, v1, v0

    const/16 v0, -0x27

    aput v0, v1, v14

    const/16 v0, -0x28

    aput v0, v1, v13

    const/16 v0, -0x25

    aput v0, v1, v12

    const/16 v0, -0x3c

    aput v0, v1, v3

    const/16 v0, 0xc

    new-array v0, v0, [I

    const/4 v2, 0x4

    const/4 v4, 0x5

    const/4 v5, 0x6

    const/4 v6, 0x7

    const/16 v7, 0x8

    const/16 v8, 0x9

    const/16 v9, 0xa

    const/16 v10, 0xb

    const/16 v11, -0x78

    aput v11, v0, v10

    const/16 v10, -0x4f

    aput v10, v0, v9

    const/16 v9, -0xd

    aput v9, v0, v8

    const/4 v8, -0x5

    aput v8, v0, v7

    const/16 v7, -0x1c

    aput v7, v0, v6

    const/4 v6, -0x1

    aput v6, v0, v5

    const/16 v5, -0x13

    aput v5, v0, v4

    const/16 v4, -0x4dd6

    aput v4, v0, v2

    const/16 v2, -0x4e

    aput v2, v0, v14

    const/16 v2, -0x45

    aput v2, v0, v13

    const/16 v2, -0x46

    aput v2, v0, v12

    const/16 v2, -0x5a

    aput v2, v0, v3

    move v2, v3

    :goto_0
    array-length v4, v0

    if-lt v2, v4, :cond_0

    array-length v0, v1

    new-array v0, v0, [C

    :goto_1
    array-length v2, v0

    if-lt v3, v2, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/base/ShowAlertActivity;->BACKUP_ERROR:Ljava/lang/String;

    const-class v0, Lcom/sec/android/app/shealth/framework/ui/base/ShowAlertActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/base/ShowAlertActivity;->TAG:Ljava/lang/String;

    return-void

    :cond_0
    aget v4, v0, v2

    aget v5, v1, v2

    xor-int/2addr v4, v5

    aput v4, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    aget v2, v1, v3

    int-to-char v2, v2

    aput-char v2, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;-><init>()V

    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/base/ShowAlertActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/framework/ui/base/ShowAlertActivity$1;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/ShowAlertActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ShowAlertActivity;->mDialogControllerMap:Ljava/util/Map;

    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/base/ShowAlertActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/framework/ui/base/ShowAlertActivity$2;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/ShowAlertActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ShowAlertActivity;->mBackPressMap:Ljava/util/Map;

    return-void
.end method

.method private showPopup(Landroid/content/Intent;)V
    .locals 31

    sget-object v4, Lcom/sec/android/app/shealth/framework/ui/base/ShowAlertActivity;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x19

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, -0x4a

    aput v29, v2, v28

    const/16 v28, 0x6507

    aput v28, v2, v27

    const/16 v27, -0x7dbb

    aput v27, v2, v26

    const/16 v26, -0x14

    aput v26, v2, v25

    const/16 v25, 0x3f4e

    aput v25, v2, v24

    const/16 v24, 0x256

    aput v24, v2, v23

    const/16 v23, -0x5a8a

    aput v23, v2, v22

    const/16 v22, -0x3a

    aput v22, v2, v21

    const/16 v21, -0x35

    aput v21, v2, v20

    const/16 v20, -0x5690

    aput v20, v2, v19

    const/16 v19, -0x23

    aput v19, v2, v18

    const/16 v18, -0x60

    aput v18, v2, v17

    const/16 v17, -0x74

    aput v17, v2, v16

    const/16 v16, 0x774

    aput v16, v2, v15

    const/16 v15, 0x5469

    aput v15, v2, v14

    const/16 v14, 0x2f3d

    aput v14, v2, v13

    const/16 v13, -0x56f1

    aput v13, v2, v12

    const/16 v12, -0x34

    aput v12, v2, v11

    const/16 v11, 0x3c11

    aput v11, v2, v10

    const/16 v10, 0x7f55

    aput v10, v2, v9

    const/16 v9, -0x1de6

    aput v9, v2, v8

    const/16 v8, -0x7f

    aput v8, v2, v7

    const/16 v7, 0x4543

    aput v7, v2, v6

    const/16 v6, 0x5617

    aput v6, v2, v3

    const/16 v3, -0x4f8a

    aput v3, v2, v1

    const/16 v1, 0x19

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, 0x11

    const/16 v23, 0x12

    const/16 v24, 0x13

    const/16 v25, 0x14

    const/16 v26, 0x15

    const/16 v27, 0x16

    const/16 v28, 0x17

    const/16 v29, 0x18

    const/16 v30, -0x6a

    aput v30, v1, v29

    const/16 v29, 0x653a

    aput v29, v1, v28

    const/16 v28, -0x7d9b

    aput v28, v1, v27

    const/16 v27, -0x7e

    aput v27, v1, v26

    const/16 v26, 0x3f21

    aput v26, v1, v25

    const/16 v25, 0x23f

    aput v25, v1, v24

    const/16 v24, -0x5afe

    aput v24, v1, v23

    const/16 v23, -0x5b

    aput v23, v1, v22

    const/16 v22, -0x56

    aput v22, v1, v21

    const/16 v21, -0x56b0

    aput v21, v1, v20

    const/16 v20, -0x57

    aput v20, v1, v19

    const/16 v19, -0x32

    aput v19, v1, v18

    const/16 v18, -0x17

    aput v18, v1, v17

    const/16 v17, 0x700

    aput v17, v1, v16

    const/16 v16, 0x5407

    aput v16, v1, v15

    const/16 v15, 0x2f54

    aput v15, v1, v14

    const/16 v14, -0x56d1

    aput v14, v1, v13

    const/16 v13, -0x57

    aput v13, v1, v12

    const/16 v12, 0x3c67

    aput v12, v1, v11

    const/16 v11, 0x7f3c

    aput v11, v1, v10

    const/16 v10, -0x1d81

    aput v10, v1, v9

    const/16 v9, -0x1e

    aput v9, v1, v8

    const/16 v8, 0x4526

    aput v8, v1, v7

    const/16 v7, 0x5645

    aput v7, v1, v6

    const/16 v6, -0x4faa

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v6, v1

    if-lt v3, v6, :cond_0

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v6, v1

    if-lt v3, v6, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-direct {v1, v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelable(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$string;->alert:I

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$string;->unable_backup_during_shealth_running:I

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$string;->ok:I

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/ShowAlertActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v5

    const/16 v1, 0xc

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, -0x1bd8

    aput v16, v2, v15

    const/16 v15, -0x75

    aput v15, v2, v14

    const/16 v14, 0x3f7c

    aput v14, v2, v13

    const/16 v13, -0xcb3

    aput v13, v2, v12

    const/16 v12, -0x6a

    aput v12, v2, v11

    const/16 v11, -0x2def

    aput v11, v2, v10

    const/16 v10, -0x5e

    aput v10, v2, v9

    const/16 v9, 0x652

    aput v9, v2, v8

    const/16 v8, 0x696d

    aput v8, v2, v7

    const/16 v7, 0x130a

    aput v7, v2, v6

    const/16 v6, -0x6a8e

    aput v6, v2, v3

    const/16 v3, -0x9

    aput v3, v2, v1

    const/16 v1, 0xc

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, -0x1ba6

    aput v17, v1, v16

    const/16 v16, -0x1c

    aput v16, v1, v15

    const/16 v15, 0x3f0e

    aput v15, v1, v14

    const/16 v14, -0xcc1

    aput v14, v1, v13

    const/16 v13, -0xd

    aput v13, v1, v12

    const/16 v12, -0x2dcf

    aput v12, v1, v11

    const/16 v11, -0x2e

    aput v11, v1, v10

    const/16 v10, 0x627

    aput v10, v1, v9

    const/16 v9, 0x6906

    aput v9, v1, v8

    const/16 v8, 0x1369

    aput v8, v1, v7

    const/16 v7, -0x6aed

    aput v7, v1, v6

    const/16 v6, -0x6b

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v6, v1

    if-lt v3, v6, :cond_2

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v6, v1

    if-lt v3, v6, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v5, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void

    :cond_0
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_1
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_2
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_3
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3
.end method


# virtual methods
.method public getBackPressController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IBackPressController;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ShowAlertActivity;->mBackPressMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/commonui/dialog/IBackPressController;

    return-object v0
.end method

.method public getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ShowAlertActivity;->mDialogControllerMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->onCreate(Landroid/os/Bundle;)V

    sget v0, Lcom/sec/android/app/shealth/framework/ui/R$layout;->blank_layout_for_popup:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/base/ShowAlertActivity;->setContentView(I)V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/ShowAlertActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/base/ShowAlertActivity;->showPopup(Landroid/content/Intent;)V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
