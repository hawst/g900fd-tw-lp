.class Lcom/sec/android/app/shealth/stress/StressSummaryFragment$9;
.super Ljava/lang/Object;
.source "StressSummaryFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->setMeasuringEndUI()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)V
    .locals 0

    .prologue
    .line 1315
    iput-object p1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$9;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 1319
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$9;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondIcon:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$500(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/widget/ImageView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1320
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$9;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondCenterIconFinish:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$2600(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$9;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    const v2, 0x7f020633

    iget-object v3, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$9;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStateBar:Lcom/sec/android/app/shealth/stress/custom/StressStateSummaryBar;
    invoke-static {v3}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$400(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Lcom/sec/android/app/shealth/stress/custom/StressStateSummaryBar;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/stress/custom/StressStateSummaryBar;->getStateColor()I

    move-result v3

    # invokes: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->maskingImage(II)Landroid/graphics/Bitmap;
    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$2700(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;II)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1321
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$9;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondIcon:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$500(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$9;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    const v2, 0x7f02062a

    iget-object v3, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$9;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStateBar:Lcom/sec/android/app/shealth/stress/custom/StressStateSummaryBar;
    invoke-static {v3}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$400(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Lcom/sec/android/app/shealth/stress/custom/StressStateSummaryBar;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/stress/custom/StressStateSummaryBar;->getStateColor()I

    move-result v3

    # invokes: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->maskingImage(II)Landroid/graphics/Bitmap;
    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$2700(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;II)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1322
    return-void
.end method
