.class public final enum Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$ResourcedMealType;
.super Ljava/lang/Enum;
.source "DayMealDataHolder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ResourcedMealType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$ResourcedMealType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$ResourcedMealType;

.field public static final enum BREAKFAST:Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$ResourcedMealType;

.field public static final enum DINNER:Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$ResourcedMealType;

.field public static final enum LUNCH:Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$ResourcedMealType;

.field public static final enum SNACKS:Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$ResourcedMealType;


# instance fields
.field private mLayoutResourceId:I

.field private mMealType:Lcom/sec/android/app/shealth/food/constants/MealType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 70
    new-instance v0, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$ResourcedMealType;

    const-string v1, "BREAKFAST"

    const v2, 0x7f0804ac

    sget-object v3, Lcom/sec/android/app/shealth/food/constants/MealType;->BREAKFAST:Lcom/sec/android/app/shealth/food/constants/MealType;

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$ResourcedMealType;-><init>(Ljava/lang/String;IILcom/sec/android/app/shealth/food/constants/MealType;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$ResourcedMealType;->BREAKFAST:Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$ResourcedMealType;

    .line 71
    new-instance v0, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$ResourcedMealType;

    const-string v1, "LUNCH"

    const v2, 0x7f0804ad

    sget-object v3, Lcom/sec/android/app/shealth/food/constants/MealType;->LUNCH:Lcom/sec/android/app/shealth/food/constants/MealType;

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$ResourcedMealType;-><init>(Ljava/lang/String;IILcom/sec/android/app/shealth/food/constants/MealType;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$ResourcedMealType;->LUNCH:Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$ResourcedMealType;

    .line 72
    new-instance v0, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$ResourcedMealType;

    const-string v1, "DINNER"

    const v2, 0x7f0804ae

    sget-object v3, Lcom/sec/android/app/shealth/food/constants/MealType;->DINNER:Lcom/sec/android/app/shealth/food/constants/MealType;

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$ResourcedMealType;-><init>(Ljava/lang/String;IILcom/sec/android/app/shealth/food/constants/MealType;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$ResourcedMealType;->DINNER:Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$ResourcedMealType;

    .line 73
    new-instance v0, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$ResourcedMealType;

    const-string v1, "SNACKS"

    const v2, 0x7f0804af

    sget-object v3, Lcom/sec/android/app/shealth/food/constants/MealType;->OTHER:Lcom/sec/android/app/shealth/food/constants/MealType;

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$ResourcedMealType;-><init>(Ljava/lang/String;IILcom/sec/android/app/shealth/food/constants/MealType;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$ResourcedMealType;->SNACKS:Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$ResourcedMealType;

    .line 69
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$ResourcedMealType;

    sget-object v1, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$ResourcedMealType;->BREAKFAST:Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$ResourcedMealType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$ResourcedMealType;->LUNCH:Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$ResourcedMealType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$ResourcedMealType;->DINNER:Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$ResourcedMealType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$ResourcedMealType;->SNACKS:Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$ResourcedMealType;

    aput-object v1, v0, v7

    sput-object v0, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$ResourcedMealType;->$VALUES:[Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$ResourcedMealType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILcom/sec/android/app/shealth/food/constants/MealType;)V
    .locals 0
    .param p3, "layoutResourceId"    # I
    .param p4, "mealType"    # Lcom/sec/android/app/shealth/food/constants/MealType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/sec/android/app/shealth/food/constants/MealType;",
            ")V"
        }
    .end annotation

    .prologue
    .line 78
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 79
    iput-object p4, p0, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$ResourcedMealType;->mMealType:Lcom/sec/android/app/shealth/food/constants/MealType;

    .line 80
    iput p3, p0, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$ResourcedMealType;->mLayoutResourceId:I

    .line 81
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$ResourcedMealType;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$ResourcedMealType;

    .prologue
    .line 69
    iget v0, p0, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$ResourcedMealType;->mLayoutResourceId:I

    return v0
.end method

.method static getResourcedTypeByMealType(Lcom/sec/android/app/shealth/food/constants/MealType;)Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$ResourcedMealType;
    .locals 7
    .param p0, "mealType"    # Lcom/sec/android/app/shealth/food/constants/MealType;

    .prologue
    .line 84
    invoke-static {}, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$ResourcedMealType;->values()[Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$ResourcedMealType;

    move-result-object v0

    .local v0, "arr$":[Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$ResourcedMealType;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 85
    .local v3, "type":Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$ResourcedMealType;
    iget-object v4, v3, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$ResourcedMealType;->mMealType:Lcom/sec/android/app/shealth/food/constants/MealType;

    if-ne v4, p0, :cond_0

    .line 86
    return-object v3

    .line 84
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 89
    .end local v3    # "type":Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$ResourcedMealType;
    :cond_1
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Wrong meal type: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$ResourcedMealType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 69
    const-class v0, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$ResourcedMealType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$ResourcedMealType;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$ResourcedMealType;
    .locals 1

    .prologue
    .line 69
    sget-object v0, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$ResourcedMealType;->$VALUES:[Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$ResourcedMealType;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$ResourcedMealType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$ResourcedMealType;

    return-object v0
.end method
