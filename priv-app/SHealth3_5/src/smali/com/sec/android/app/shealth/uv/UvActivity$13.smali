.class Lcom/sec/android/app/shealth/uv/UvActivity$13;
.super Ljava/lang/Object;
.source "UvActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/uv/UvActivity;->showInfomationDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/uv/UvActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/uv/UvActivity;)V
    .locals 0

    .prologue
    .line 718
    iput-object p1, p0, Lcom/sec/android/app/shealth/uv/UvActivity$13;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onContentInitialization(Landroid/view/View;Landroid/app/Activity;Landroid/app/Dialog;Landroid/os/Bundle;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V
    .locals 5
    .param p1, "content"    # Landroid/view/View;
    .param p2, "parentActivity"    # Landroid/app/Activity;
    .param p3, "dialog"    # Landroid/app/Dialog;
    .param p4, "savedInstanceState"    # Landroid/os/Bundle;
    .param p5, "okButtonHandler"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;

    .prologue
    .line 724
    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvActivity$13;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    const v2, 0x7f080af4

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    # setter for: Lcom/sec/android/app/shealth/uv/UvActivity;->mInformationText1:Landroid/widget/TextView;
    invoke-static {v3, v2}, Lcom/sec/android/app/shealth/uv/UvActivity;->access$1302(Lcom/sec/android/app/shealth/uv/UvActivity;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 725
    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvActivity$13;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    const v2, 0x7f080af5

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    # setter for: Lcom/sec/android/app/shealth/uv/UvActivity;->mInformationText2:Landroid/widget/TextView;
    invoke-static {v3, v2}, Lcom/sec/android/app/shealth/uv/UvActivity;->access$1402(Lcom/sec/android/app/shealth/uv/UvActivity;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 726
    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvActivity$13;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    const v2, 0x7f0804ee

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    # setter for: Lcom/sec/android/app/shealth/uv/UvActivity;->mShowAgainCheckLayout:Landroid/widget/RelativeLayout;
    invoke-static {v3, v2}, Lcom/sec/android/app/shealth/uv/UvActivity;->access$1502(Lcom/sec/android/app/shealth/uv/UvActivity;Landroid/widget/RelativeLayout;)Landroid/widget/RelativeLayout;

    .line 728
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvActivity$13;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    const v4, 0x7f090deb

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/uv/UvActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 729
    .local v0, "informationString":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvActivity$13;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    const v4, 0x7f090dee

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/uv/UvActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 731
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/Utils;->isHestiaModel()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 732
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvActivity$13;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    # getter for: Lcom/sec/android/app/shealth/uv/UvActivity;->mInformationText1:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/app/shealth/uv/UvActivity;->access$1300(Lcom/sec/android/app/shealth/uv/UvActivity;)Landroid/widget/TextView;

    move-result-object v2

    const v3, 0x7f090dea

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 734
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvActivity$13;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    # getter for: Lcom/sec/android/app/shealth/uv/UvActivity;->mInformationText2:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/app/shealth/uv/UvActivity;->access$1400(Lcom/sec/android/app/shealth/uv/UvActivity;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 735
    const/4 v1, 0x0

    .line 736
    .local v1, "isChecked":Z
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvActivity$13;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    # getter for: Lcom/sec/android/app/shealth/uv/UvActivity;->mShowAgain:Landroid/widget/CheckBox;
    invoke-static {v2}, Lcom/sec/android/app/shealth/uv/UvActivity;->access$1100(Lcom/sec/android/app/shealth/uv/UvActivity;)Landroid/widget/CheckBox;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 737
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvActivity$13;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    # getter for: Lcom/sec/android/app/shealth/uv/UvActivity;->mShowAgain:Landroid/widget/CheckBox;
    invoke-static {v2}, Lcom/sec/android/app/shealth/uv/UvActivity;->access$1100(Lcom/sec/android/app/shealth/uv/UvActivity;)Landroid/widget/CheckBox;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    .line 739
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvActivity$13;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    # setter for: Lcom/sec/android/app/shealth/uv/UvActivity;->mInfoView:Landroid/view/View;
    invoke-static {v2, p1}, Lcom/sec/android/app/shealth/uv/UvActivity;->access$1602(Lcom/sec/android/app/shealth/uv/UvActivity;Landroid/view/View;)Landroid/view/View;

    .line 740
    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvActivity$13;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    const v2, 0x7f0804ef

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    # setter for: Lcom/sec/android/app/shealth/uv/UvActivity;->mShowAgain:Landroid/widget/CheckBox;
    invoke-static {v3, v2}, Lcom/sec/android/app/shealth/uv/UvActivity;->access$1102(Lcom/sec/android/app/shealth/uv/UvActivity;Landroid/widget/CheckBox;)Landroid/widget/CheckBox;

    .line 741
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvActivity$13;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    # getter for: Lcom/sec/android/app/shealth/uv/UvActivity;->mShowAgain:Landroid/widget/CheckBox;
    invoke-static {v2}, Lcom/sec/android/app/shealth/uv/UvActivity;->access$1100(Lcom/sec/android/app/shealth/uv/UvActivity;)Landroid/widget/CheckBox;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 742
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvActivity$13;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    # getter for: Lcom/sec/android/app/shealth/uv/UvActivity;->mShowAgain:Landroid/widget/CheckBox;
    invoke-static {v2}, Lcom/sec/android/app/shealth/uv/UvActivity;->access$1100(Lcom/sec/android/app/shealth/uv/UvActivity;)Landroid/widget/CheckBox;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvActivity$13;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    iget-object v3, v3, Lcom/sec/android/app/shealth/uv/UvActivity;->againOnCheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 743
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvActivity$13;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    # getter for: Lcom/sec/android/app/shealth/uv/UvActivity;->mShowAgainCheckLayout:Landroid/widget/RelativeLayout;
    invoke-static {v2}, Lcom/sec/android/app/shealth/uv/UvActivity;->access$1500(Lcom/sec/android/app/shealth/uv/UvActivity;)Landroid/widget/RelativeLayout;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvActivity$13;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    iget-object v3, v3, Lcom/sec/android/app/shealth/uv/UvActivity;->mCheckboxLayoutClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 744
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvActivity$13;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    # invokes: Lcom/sec/android/app/shealth/uv/UvActivity;->onInitWarningVideo(Landroid/view/View;)V
    invoke-static {v2, p1}, Lcom/sec/android/app/shealth/uv/UvActivity;->access$1700(Lcom/sec/android/app/shealth/uv/UvActivity;Landroid/view/View;)V

    .line 745
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvActivity$13;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/uv/UvActivity;->startInformationVideo()V

    .line 746
    return-void
.end method
