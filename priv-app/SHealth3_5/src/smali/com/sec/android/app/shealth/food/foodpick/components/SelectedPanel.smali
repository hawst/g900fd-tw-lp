.class public Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;
.super Landroid/widget/HorizontalScrollView;
.source "SelectedPanel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel$OnSelectedItemRemovedListener;,
        Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel$OnSelectedItemClickListener;
    }
.end annotation


# static fields
.field private static final IS_ALWAYS_VISIBLE_DEFAULT:Z = false

.field private static final PARENT_STATE_KEY:Ljava/lang/String; = "parent_state_key"

.field private static final SELECTED_PANEL_ITEMS_KEY:Ljava/lang/String; = "selected_panel_items_key"


# instance fields
.field private isAlwaysVisible:Z

.field private mDynamicViewId:I

.field private mHolderViewIdToViewLink:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mLinearLayoutSelectedItemsHolder:Landroid/widget/LinearLayout;

.field private mMealItemDataHolder:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;",
            ">;"
        }
    .end annotation
.end field

.field private mOnSelectedItemClickListener:Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel$OnSelectedItemClickListener;

.field private mOnSelectedItemRemovedListener:Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel$OnSelectedItemRemovedListener;

.field private mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 56
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 57
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 62
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 63
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 66
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 51
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->isAlwaysVisible:Z

    .line 59
    const/16 v0, 0x7d0

    iput v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->mDynamicViewId:I

    .line 67
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->init(Landroid/content/Context;)V

    .line 68
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->mMealItemDataHolder:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;)Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel$OnSelectedItemClickListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->mOnSelectedItemClickListener:Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel$OnSelectedItemClickListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;)Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->removeItem(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;)Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel$OnSelectedItemRemovedListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->mOnSelectedItemRemovedListener:Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel$OnSelectedItemRemovedListener;

    return-object v0
.end method

.method private createAndInitTextView(Ljava/lang/String;Ljava/lang/String;)Landroid/view/View;
    .locals 7
    .param p1, "itemId"    # Ljava/lang/String;
    .param p2, "label"    # Ljava/lang/String;

    .prologue
    .line 182
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f03010c

    iget-object v5, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->mLinearLayoutSelectedItemsHolder:Landroid/widget/LinearLayout;

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 183
    .local v1, "selectedItem":Landroid/view/View;
    const v3, 0x7f08046e

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 184
    .local v2, "textView":Landroid/widget/TextView;
    invoke-virtual {v2, p1}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 185
    new-instance v3, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel$2;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel$2;-><init>(Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;)V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 197
    iget v3, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->mDynamicViewId:I

    add-int/lit8 v4, v3, -0x1

    iput v4, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->mDynamicViewId:I

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setId(I)V

    .line 198
    invoke-virtual {v2, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 200
    const v3, 0x7f08046f

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 201
    .local v0, "cancelView":Landroid/view/View;
    invoke-virtual {v0, p1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 202
    new-instance v3, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel$3;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel$3;-><init>(Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 215
    iget v3, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->mDynamicViewId:I

    add-int/lit8 v4, v3, -0x1

    iput v4, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->mDynamicViewId:I

    invoke-virtual {v0, v3}, Landroid/view/View;->setId(I)V

    .line 216
    return-object v1
.end method

.method private init(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 71
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->removeAllViews()V

    .line 72
    const v0, 0x7f030114

    invoke-static {p1, v0, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 73
    const v0, 0x7f080494

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->mLinearLayoutSelectedItemsHolder:Landroid/widget/LinearLayout;

    .line 74
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->mHolderViewIdToViewLink:Ljava/util/Map;

    .line 75
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->mMealItemDataHolder:Ljava/util/Map;

    .line 76
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->refreshVisibility()V

    .line 78
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLoggerHolder;

    invoke-interface {v0}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLoggerHolder;->getUserActionLogger()Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    .line 80
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->mLinearLayoutSelectedItemsHolder:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel$1;-><init>(Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnHierarchyChangeListener(Landroid/view/ViewGroup$OnHierarchyChangeListener;)V

    .line 94
    return-void
.end method

.method private refreshVisibility()V
    .locals 1

    .prologue
    .line 260
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->isSelectedListEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->isAlwaysVisible:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->setVisibility(I)V

    .line 261
    return-void

    .line 260
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private removeItem(Ljava/lang/String;)V
    .locals 2
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 159
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->mLinearLayoutSelectedItemsHolder:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->mHolderViewIdToViewLink:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 160
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->mHolderViewIdToViewLink:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 161
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->mMealItemDataHolder:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 162
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->refreshVisibility()V

    .line 163
    return-void
.end method


# virtual methods
.method public clearAllItems()V
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->mLinearLayoutSelectedItemsHolder:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 148
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->mHolderViewIdToViewLink:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 149
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->mMealItemDataHolder:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 150
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->refreshVisibility()V

    .line 151
    return-void
.end method

.method public getSelectedItems()Ljava/util/ArrayList;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 172
    iget-object v6, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->mMealItemDataHolder:Ljava/util/Map;

    invoke-interface {v6}, Ljava/util/Map;->size()I

    move-result v6

    new-array v5, v6, [Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;

    .line 173
    .local v5, "selectedPanelHolders":[Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;
    iget-object v6, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->mMealItemDataHolder:Ljava/util/Map;

    invoke-interface {v6}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v6

    invoke-interface {v6, v5}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 174
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 175
    .local v3, "mealItemDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;>;"
    move-object v0, v5

    .local v0, "arr$":[Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v4, v0, v1

    .line 176
    .local v4, "selectedPanelHolder":Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;
    invoke-virtual {v4}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;->getMealItemDatas()Ljava/util/List;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 175
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 178
    .end local v4    # "selectedPanelHolder":Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;
    :cond_0
    return-object v3
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->mMealItemDataHolder:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public isSelectedListEmpty()Z
    .locals 1

    .prologue
    .line 223
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->mHolderViewIdToViewLink:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    return v0
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 5
    .param p1, "state"    # Landroid/os/Parcelable;

    .prologue
    .line 108
    instance-of v4, p1, Landroid/os/Bundle;

    if-eqz v4, :cond_1

    move-object v0, p1

    .line 109
    check-cast v0, Landroid/os/Bundle;

    .line 110
    .local v0, "bundle":Landroid/os/Bundle;
    const-string/jumbo v4, "selected_panel_items_key"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 111
    .local v2, "panelItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;

    .line 112
    .local v3, "selectedPanelHolder":Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;
    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->putItem(Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;)Z

    goto :goto_0

    .line 114
    .end local v3    # "selectedPanelHolder":Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;
    :cond_0
    const-string/jumbo v4, "parent_state_key"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    invoke-super {p0, v4}, Landroid/widget/HorizontalScrollView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 118
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "panelItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;>;"
    :goto_1
    return-void

    .line 116
    :cond_1
    invoke-super {p0, p1}, Landroid/widget/HorizontalScrollView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    goto :goto_1
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 4

    .prologue
    .line 98
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 99
    .local v0, "bundle":Landroid/os/Bundle;
    const-string/jumbo v2, "parent_state_key"

    invoke-super {p0}, Landroid/widget/HorizontalScrollView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 101
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->mMealItemDataHolder:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 102
    .local v1, "panelItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;>;"
    const-string/jumbo v2, "selected_panel_items_key"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 103
    return-object v0
.end method

.method public putItem(Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;)Z
    .locals 3
    .param p1, "selectedPanelHolder"    # Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;

    .prologue
    .line 129
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->mMealItemDataHolder:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;->getUniqueId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->mHolderViewIdToViewLink:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;->getUniqueId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 131
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->mMealItemDataHolder:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;->getUniqueId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->refreshVisibility()V

    .line 133
    const/4 v1, 0x0

    .line 140
    :goto_0
    return v1

    .line 135
    :cond_0
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;->getUniqueId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->createAndInitTextView(Ljava/lang/String;Ljava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 136
    .local v0, "selectedView":Landroid/view/View;
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->mLinearLayoutSelectedItemsHolder:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 137
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->mHolderViewIdToViewLink:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;->getUniqueId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->mMealItemDataHolder:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;->getUniqueId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->refreshVisibility()V

    .line 140
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public setAlwaysVisible(Z)V
    .locals 0
    .param p1, "isAlwaysVisible"    # Z

    .prologue
    .line 256
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->isAlwaysVisible:Z

    .line 257
    return-void
.end method

.method public setOnSelectedItemClickListener(Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel$OnSelectedItemClickListener;)V
    .locals 0
    .param p1, "onSelectedItemClickListener"    # Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel$OnSelectedItemClickListener;

    .prologue
    .line 237
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->mOnSelectedItemClickListener:Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel$OnSelectedItemClickListener;

    .line 238
    return-void
.end method

.method public setOnSelectedItemRemovedListener(Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel$OnSelectedItemRemovedListener;)V
    .locals 0
    .param p1, "onSelectedItemRemoved"    # Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel$OnSelectedItemRemovedListener;

    .prologue
    .line 247
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->mOnSelectedItemRemovedListener:Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel$OnSelectedItemRemovedListener;

    .line 248
    return-void
.end method
