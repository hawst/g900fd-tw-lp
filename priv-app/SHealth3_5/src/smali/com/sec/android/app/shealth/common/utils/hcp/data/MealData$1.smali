.class final Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData$1;
.super Ljava/lang/Object;
.source "MealData.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 214
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    .locals 20
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 218
    new-instance v2, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v3

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readFloat()F

    move-result v6

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v7

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v8

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v11

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v13

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v15

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v17

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v18

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_0

    const/16 v18, 0x1

    :goto_0
    invoke-direct/range {v2 .. v18}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;-><init>(JLjava/lang/String;FIJLjava/lang/String;JJJIZ)V

    return-object v2

    :cond_0
    const/16 v18, 0x0

    goto :goto_0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Landroid/os/Parcel;

    .prologue
    .line 214
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData$1;->createFromParcel(Landroid/os/Parcel;)Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 226
    new-array v0, p1, [Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 214
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData$1;->newArray(I)[Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    move-result-object v0

    return-object v0
.end method
