.class Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$5$1;
.super Ljava/lang/Object;
.source "PortionSizePopup.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/ListPopup$OnItemClickedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$5;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$5;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$5;)V
    .locals 0

    .prologue
    .line 428
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$5$1;->this$1:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$5;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClicked(ILjava/lang/String;Landroid/widget/PopupWindow;)V
    .locals 4
    .param p1, "itemIndex"    # I
    .param p2, "itemContent"    # Ljava/lang/String;
    .param p3, "popupWindow"    # Landroid/widget/PopupWindow;

    .prologue
    .line 431
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$5$1;->this$1:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$5;

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$5;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mListPopup:Lcom/sec/android/app/shealth/common/commonui/ListPopup;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->access$1100(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;)Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->dismiss()V

    .line 432
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$5$1;->this$1:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$5;

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$5;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mListPopup:Lcom/sec/android/app/shealth/common/commonui/ListPopup;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->access$1100(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;)Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->setCurrentItem(I)V

    .line 433
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$5$1;->this$1:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$5;

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$5;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mUnitDropDownButton:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->access$400(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 435
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$5$1;->this$1:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$5;

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$5;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mUnitDropDownButton:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->access$400(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$5$1;->this$1:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$5;

    iget-object v2, v2, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$5;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mActivity:Landroid/app/Activity;
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->access$200(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;)Landroid/app/Activity;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$5$1;->this$1:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$5;

    iget-object v3, v3, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$5;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mUnitDropDownButton:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->access$400(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->convertToProperUnitsText(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0901ec

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 436
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$5$1;->this$1:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$5;

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$5;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mPortionSizeInputModule:Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeInputModule;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->access$000(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;)Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeInputModule;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeInputModule;->setUnit(Ljava/lang/CharSequence;)V

    .line 437
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$5$1;->this$1:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$5;

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$5;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mPortionSizeHolder:Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->access$700(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;)Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;

    move-result-object v1

    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$5$1;->this$1:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$5;

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$5;->val$unitsMap:Ljava/util/Map;

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$5$1;->this$1:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$5;

    iget-object v2, v2, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$5;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mUnitDropDownButton:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->access$400(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->setUnit(I)V

    .line 438
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$5$1;->this$1:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$5;

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$5;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$5$1;->this$1:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$5;

    iget-object v1, v1, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$5;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mTextViewTotalKcalInPrecisePopup:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->access$900(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;)Landroid/widget/TextView;

    move-result-object v1

    # invokes: Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->calculateAndSetTotalKcalTexView(Landroid/widget/TextView;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->access$1000(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;Landroid/widget/TextView;)V

    .line 439
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$5$1;->this$1:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$5;

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$5;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    # invokes: Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->updateDefaultValueOfUnit(Ljava/lang/String;)V
    invoke-static {v0, p2}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->access$1200(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;Ljava/lang/String;)V

    .line 440
    return-void
.end method
