.class public final enum Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$TYPE;
.super Ljava/lang/Enum;
.source "InputModule.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TYPE"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$TYPE;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$TYPE;

.field public static final enum HORIZONTAL:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$TYPE;

.field public static final enum VERTICAL:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$TYPE;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 57
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$TYPE;

    const-string v1, "VERTICAL"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$TYPE;->VERTICAL:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$TYPE;

    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$TYPE;

    const-string v1, "HORIZONTAL"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$TYPE;->HORIZONTAL:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$TYPE;

    .line 56
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$TYPE;

    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$TYPE;->VERTICAL:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$TYPE;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$TYPE;->HORIZONTAL:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$TYPE;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$TYPE;->$VALUES:[Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$TYPE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$TYPE;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 56
    const-class v0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$TYPE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$TYPE;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$TYPE;
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$TYPE;->$VALUES:[Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$TYPE;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$TYPE;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$TYPE;

    return-object v0
.end method
