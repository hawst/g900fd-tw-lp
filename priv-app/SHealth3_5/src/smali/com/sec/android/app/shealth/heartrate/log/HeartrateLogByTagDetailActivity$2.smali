.class Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$2;
.super Ljava/lang/Object;
.source "HeartrateLogByTagDetailActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;)V
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$2;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 91
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const v1, 0x7f02086c

    invoke-virtual {p2, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 93
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$2;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;

    const-class v2, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 94
    .local v0, "mIntent":Landroid/content/Intent;
    const-string/jumbo v1, "tag"

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 95
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$2;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;->startActivity(Landroid/content/Intent;)V

    .line 96
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$2;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x106000d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {p2, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 97
    return-void
.end method
