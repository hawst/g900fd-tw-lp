.class public Lcom/sec/android/app/shealth/home/data/IconData;
.super Ljava/lang/Object;
.source "IconData.java"


# static fields
.field public static final ADD_TYPE:I = 0x1

.field public static final APP_TYPE:I = 0x0

.field public static final EDIT_TYPE:I = 0x1


# instance fields
.field private action:Ljava/lang/String;

.field private appDisplayPlugInIcons:Ljava/lang/String;

.field private appIcon:Landroid/graphics/drawable/Drawable;

.field private appIconResId:I

.field private appName:Ljava/lang/String;

.field private appPackageName:Ljava/lang/String;

.field private appPluginId:I

.field private appType:I

.field private iconType:I

.field private isSelected:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object v1, p0, Lcom/sec/android/app/shealth/home/data/IconData;->appName:Ljava/lang/String;

    .line 28
    iput-object v1, p0, Lcom/sec/android/app/shealth/home/data/IconData;->appPackageName:Ljava/lang/String;

    .line 29
    iput-object v1, p0, Lcom/sec/android/app/shealth/home/data/IconData;->appDisplayPlugInIcons:Ljava/lang/String;

    .line 30
    iput v0, p0, Lcom/sec/android/app/shealth/home/data/IconData;->appPluginId:I

    .line 31
    iput-object v1, p0, Lcom/sec/android/app/shealth/home/data/IconData;->appIcon:Landroid/graphics/drawable/Drawable;

    .line 32
    iput v0, p0, Lcom/sec/android/app/shealth/home/data/IconData;->appIconResId:I

    .line 33
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/home/data/IconData;->isSelected:Z

    .line 34
    iput v0, p0, Lcom/sec/android/app/shealth/home/data/IconData;->appType:I

    .line 35
    iput v0, p0, Lcom/sec/android/app/shealth/home/data/IconData;->iconType:I

    .line 36
    const-string v0, "com.sec.shealth.action.STEALTH_MODE"

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/data/IconData;->action:Ljava/lang/String;

    .line 45
    return-void
.end method


# virtual methods
.method public getAction()Ljava/lang/String;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/data/IconData;->action:Ljava/lang/String;

    return-object v0
.end method

.method public getAppDisplayPlugInIcons()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/data/IconData;->appDisplayPlugInIcons:Ljava/lang/String;

    return-object v0
.end method

.method public getAppIconDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/data/IconData;->appIcon:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getAppName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/data/IconData;->appName:Ljava/lang/String;

    return-object v0
.end method

.method public getAppPluginId()I
    .locals 1

    .prologue
    .line 68
    iget v0, p0, Lcom/sec/android/app/shealth/home/data/IconData;->appPluginId:I

    return v0
.end method

.method public getAppType()I
    .locals 1

    .prologue
    .line 104
    iget v0, p0, Lcom/sec/android/app/shealth/home/data/IconData;->appType:I

    return v0
.end method

.method public getIconResId()I
    .locals 1

    .prologue
    .line 60
    iget v0, p0, Lcom/sec/android/app/shealth/home/data/IconData;->appIconResId:I

    return v0
.end method

.method public getIconType()I
    .locals 1

    .prologue
    .line 109
    iget v0, p0, Lcom/sec/android/app/shealth/home/data/IconData;->iconType:I

    return v0
.end method

.method public getIsSelected()Z
    .locals 1

    .prologue
    .line 96
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/home/data/IconData;->isSelected:Z

    return v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/data/IconData;->appPackageName:Ljava/lang/String;

    return-object v0
.end method

.method public setAction(Ljava/lang/String;)V
    .locals 0
    .param p1, "action"    # Ljava/lang/String;

    .prologue
    .line 119
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/data/IconData;->action:Ljava/lang/String;

    .line 120
    return-void
.end method

.method public setAppDisplayPlugInIcons(Ljava/lang/String;)V
    .locals 0
    .param p1, "appDisplayPlugInIcons"    # Ljava/lang/String;

    .prologue
    .line 72
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/data/IconData;->appDisplayPlugInIcons:Ljava/lang/String;

    .line 73
    return-void
.end method

.method public setAppName(Ljava/lang/String;)V
    .locals 0
    .param p1, "appName"    # Ljava/lang/String;

    .prologue
    .line 48
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/data/IconData;->appName:Ljava/lang/String;

    .line 49
    return-void
.end method

.method public setAppPluginId(I)V
    .locals 0
    .param p1, "appPluginId"    # I

    .prologue
    .line 64
    iput p1, p0, Lcom/sec/android/app/shealth/home/data/IconData;->appPluginId:I

    .line 65
    return-void
.end method

.method public setAppType(I)V
    .locals 0
    .param p1, "appType"    # I

    .prologue
    .line 100
    iput p1, p0, Lcom/sec/android/app/shealth/home/data/IconData;->appType:I

    .line 101
    return-void
.end method

.method public setIconResId(I)V
    .locals 0
    .param p1, "resId"    # I

    .prologue
    .line 56
    iput p1, p0, Lcom/sec/android/app/shealth/home/data/IconData;->appIconResId:I

    .line 57
    return-void
.end method

.method public setIconType(I)V
    .locals 0
    .param p1, "iconType"    # I

    .prologue
    .line 114
    iput p1, p0, Lcom/sec/android/app/shealth/home/data/IconData;->iconType:I

    .line 115
    return-void
.end method

.method public setIsSelected(Z)V
    .locals 0
    .param p1, "isSelect"    # Z

    .prologue
    .line 92
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/home/data/IconData;->isSelected:Z

    .line 93
    return-void
.end method

.method public setPackageName(Ljava/lang/String;)V
    .locals 0
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 80
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/data/IconData;->appPackageName:Ljava/lang/String;

    .line 81
    return-void
.end method
