.class public Lcom/sec/android/app/shealth/uv/UvSummaryFragment;
.super Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;
.source "UvSummaryFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/uv/UvSensorListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/uv/UvSummaryFragment$measuringHapticThread;,
        Lcom/sec/android/app/shealth/uv/UvSummaryFragment$DeviceConnChkListner;
    }
.end annotation


# static fields
.field public static final IS_SUPPORT_CIRCLE_VI_2:Z = true

.field private static final MEASURING_TIMEOUT:J = 0x2ee0L

.field public static final TAG:Ljava/lang/String;

.field public static final TEST_UVI_ADDRESS:Ljava/lang/String; = "samstresstest@gmail.com"

.field static effectAudio:[I


# instance fields
.field private UvParentView:Landroid/widget/LinearLayout;

.field private checkSPF:Z

.field public dataCollection:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private divider:Landroid/view/View;

.field private fromMeasuringState:Z

.field private hasNoUvSensor:Z

.field private images:[I

.field public intervalCollection:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public isFailurePopupVisible:Z

.field public isFragmentInitialized:Z

.field public isHideSkinSettings:Z

.field public isSPFDialogDisplayed:Z

.field public isShownRecomendedDialog:Z

.field public ismFinishedState:Z

.field private mCheckBoxLayout:Landroid/widget/LinearLayout;

.field mCheckboxLayoutClickListener:Landroid/view/View$OnClickListener;

.field private mCircleAnimationSet:Landroid/view/animation/AnimationSet;

.field private mContext:Landroid/content/Context;

.field private mCurrDateTxt:Landroid/widget/TextView;

.field private mCustomStateBarParent:Landroid/widget/RelativeLayout;

.field private mDataCollectedProgressPercent:Landroid/widget/TextView;

.field private mDeviceConnectedStatusView:Landroid/view/View;

.field private mDeviceConnectedTextView:Landroid/widget/TextView;

.field private mDeviceConnector:Lcom/sec/android/app/shealth/uv/common/UvDeviceConnector;

.field private mDeviceFinder:Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;

.field private mDialogFailureListener:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;

.field private mDialogLocationListener:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;

.field private mDialogMovie:Landroid/widget/ImageView;

.field private mFinishTopLayout:Landroid/widget/RelativeLayout;

.field private mFirstLayout:Landroid/widget/FrameLayout;

.field private mFirstMessage:Landroid/widget/TextView;

.field private mFirstReadyLayout:Landroid/widget/LinearLayout;

.field private mFirstStateLayout:Landroid/widget/LinearLayout;

.field public mFirstStatus:Landroid/widget/TextView;

.field public mHandler:Landroid/os/Handler;

.field public mIsDummyMode:Z

.field public mIsDummyStart:Z

.field private mIsGreen:Z

.field private mIsOnConfigChanged:Z

.field private mLine:Landroid/view/View;

.field private mMeasuringAniLinear:Landroid/widget/LinearLayout;

.field private mMeasuringHapticThread:Lcom/sec/android/app/shealth/uv/UvSummaryFragment$measuringHapticThread;

.field private mMeasuringRunnable:Ljava/lang/Runnable;

.field public mMeasuringTimer:Landroid/os/CountDownTimer;

.field private mNoDataDescription:Landroid/widget/TextView;

.field private mNoDataText:Landroid/widget/TextView;

.field private mNonSensorDateLayout:Landroid/widget/LinearLayout;

.field private mReadyMessage:Landroid/widget/TextView;

.field private mReadyTitle:Landroid/widget/TextView;

.field private mSafeHr:Landroid/widget/TextView;

.field private mSafeHrVal:Landroid/widget/TextView;

.field private mSafeMin:Landroid/widget/TextView;

.field private mSafeMinVal:Landroid/widget/TextView;

.field private mScaleAnimator:Landroid/animation/ValueAnimator;

.field private mScaleAnimatorFinished:Landroid/animation/ValueAnimator;

.field private mSecondCenterIcon:Landroid/widget/ImageView;

.field private mSecondCenterIconBig:Landroid/widget/ImageView;

.field private mSecondCenterIconBigAnimator:Lcom/sec/android/app/shealth/uv/custom/UvGIFView;

.field private mSecondCenterIconBigLayout:Landroid/widget/RelativeLayout;

.field private mSecondCenterIconNoSkin:Landroid/widget/ImageView;

.field private mSecondCenterTextFinish:Landroid/widget/TextView;

.field private mSecondDiscardText:Landroid/widget/TextView;

.field private mSecondIcon:Landroid/widget/ImageView;

.field private mSecondLayout:Landroid/widget/RelativeLayout;

.field private mSecondMeasuringProgressView:Landroid/widget/ImageView;

.field private mSecondMeasuringProgressView1:Landroid/widget/ImageView;

.field private mSecondStart:Landroid/widget/LinearLayout;

.field private mSecondStartText:Landroid/widget/TextView;

.field private mSpfContainer:Landroid/widget/RelativeLayout;

.field private mSpfIcon:Landroid/widget/ImageView;

.field protected mSpfNumberPicker:Landroid/widget/NumberPicker;

.field mState:Lcom/sec/android/app/shealth/uv/state/UvState;

.field private mStateBar:Lcom/sec/android/app/shealth/uv/custom/UvStateSummaryBar;

.field private mStateChineseBar:Lcom/sec/android/app/shealth/uv/custom/UvStateSummaryChineseBar;

.field private mStateFinished:Lcom/sec/android/app/shealth/uv/state/UvState;

.field private mStateMeasureFailed:Lcom/sec/android/app/shealth/uv/state/UvState;

.field private mStateMeasureWarning:Lcom/sec/android/app/shealth/uv/state/UvState;

.field private mStateMeasuring:Lcom/sec/android/app/shealth/uv/state/UvState;

.field private mStateReady:Lcom/sec/android/app/shealth/uv/state/UvState;

.field private mStopSensor:Z

.field private mSystemVibrator:Landroid/os/SystemVibrator;

.field private mThirdComment:Landroid/widget/TextView;

.field private mThirdCommentLayout:Landroid/widget/LinearLayout;

.field private mThirdDate:Landroid/widget/TextView;

.field private mThirdDiscard:Landroid/widget/LinearLayout;

.field private mThirdGraph:Landroid/widget/ImageButton;

.field private mThirdPreviousComment:Landroid/widget/TextView;

.field private mThirdPreviousImage:Landroid/widget/ImageView;

.field private mThirdPreviousLayout:Landroid/widget/LinearLayout;

.field private mThirdTime:Landroid/widget/TextView;

.field private mUvActivity:Lcom/sec/android/app/shealth/uv/UvActivity;

.field private mUvClickListener:Landroid/view/View$OnClickListener;

.field public mUvFailureAlertDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

.field private mUvHandler:Landroid/os/Handler;

.field private mUvIndex:I

.field public mUvLocationAlertDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

.field private mUvObserver:Landroid/database/ContentObserver;

.field private mUvPluginUtils:Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;

.field private mUvRecommendDialog:Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;

.field public mUvRecommendedDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

.field public mUvSPFDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

.field private mUvValue:I

.field private mWearableSummaryNoData:Landroid/widget/RelativeLayout;

.field private mspfCheckBoxLayout:Landroid/widget/RelativeLayout;

.field public needsLocationPopup:Z

.field public percent:I

.field private pool:Landroid/media/SoundPool;

.field private progressSPF:I

.field public recomMsgIndex:I

.field sHealthAlertDialogbuild:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

.field private spfCheckBox:Landroid/widget/CheckBox;

.field private spfFinishedsunprotection:Landroid/widget/LinearLayout;

.field private spfFinishedsunprotectionText:Landroid/widget/TextView;

.field private spfSeekText:Landroid/widget/RelativeLayout;

.field private spfTextView:Landroid/widget/TextView;

.field private spfchecklay:Landroid/widget/LinearLayout;

.field public sumInterval:I

.field private wasShowInformationDialog:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 112
    const-class v0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->TAG:Ljava/lang/String;

    .line 118
    const/4 v0, 0x5

    new-array v0, v0, [I

    sput-object v0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->effectAudio:[I

    return-void
.end method

.method public constructor <init>()V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v6, 0x0

    const/4 v1, 0x0

    .line 111
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;-><init>()V

    .line 161
    iput-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mDialogMovie:Landroid/widget/ImageView;

    .line 167
    iput-boolean v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mIsOnConfigChanged:Z

    .line 171
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mIsDummyMode:Z

    .line 172
    iput-boolean v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mIsDummyStart:Z

    .line 187
    const/16 v0, 0x1e

    iput v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->progressSPF:I

    .line 188
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->checkSPF:Z

    .line 196
    iput-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvLocationAlertDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 197
    iput-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvFailureAlertDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 198
    iput-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvSPFDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 199
    iput-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvRecommendedDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 211
    iput-boolean v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->wasShowInformationDialog:Z

    .line 212
    iput-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSystemVibrator:Landroid/os/SystemVibrator;

    .line 215
    iput-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mMeasuringHapticThread:Lcom/sec/android/app/shealth/uv/UvSummaryFragment$measuringHapticThread;

    .line 219
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->images:[I

    .line 223
    new-instance v0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$1;-><init>(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mHandler:Landroid/os/Handler;

    .line 262
    new-instance v0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$2;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$2;-><init>(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvObserver:Landroid/database/ContentObserver;

    .line 423
    new-instance v0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$3;-><init>(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mCheckboxLayoutClickListener:Landroid/view/View$OnClickListener;

    .line 1006
    new-instance v0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$7;-><init>(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvClickListener:Landroid/view/View$OnClickListener;

    .line 1467
    new-instance v0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$13;

    const-wide/16 v2, 0x2ee0

    const-wide/16 v4, 0x3e8

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$13;-><init>(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;JJ)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mMeasuringTimer:Landroid/os/CountDownTimer;

    .line 1532
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->dataCollection:Ljava/util/ArrayList;

    .line 1543
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->intervalCollection:Ljava/util/ArrayList;

    .line 1553
    iput v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->sumInterval:I

    .line 1554
    iput v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->percent:I

    .line 2467
    new-instance v0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$23;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$23;-><init>(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mDialogLocationListener:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;

    .line 2474
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvIndex:I

    .line 2994
    new-instance v0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$38;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$38;-><init>(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mDialogFailureListener:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;

    return-void

    .line 219
    nop

    :array_0
    .array-data 4
        0x7f0208bd
        0x7f0208be
        0x7f0208bf
        0x7f0208c0
        0x7f0208c1
        0x7f0208c2
    .end array-data
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvSummaryFragment;
    .param p1, "x1"    # I

    .prologue
    .line 111
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->setReadyUI(I)V

    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvSummaryFragment;
    .param p1, "x1"    # I

    .prologue
    .line 111
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->playSound(I)V

    return-void
.end method

.method static synthetic access$102(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvSummaryFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 111
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->fromMeasuringState:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    .prologue
    .line 111
    invoke-direct {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->startMeasuringUv()V

    return-void
.end method

.method static synthetic access$1200(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    .prologue
    .line 111
    invoke-direct {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->initToReadyState()V

    return-void
.end method

.method static synthetic access$1300(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    .prologue
    .line 111
    invoke-direct {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->showSpfPickerDialog()V

    return-void
.end method

.method static synthetic access$1400(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)Lcom/sec/android/app/shealth/uv/UvActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    .prologue
    .line 111
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvActivity:Lcom/sec/android/app/shealth/uv/UvActivity;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    .prologue
    .line 111
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mFirstMessage:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)Landroid/widget/FrameLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    .prologue
    .line 111
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mFirstLayout:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)Landroid/view/animation/AnimationSet;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    .prologue
    .line 111
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mCircleAnimationSet:Landroid/view/animation/AnimationSet;

    return-object v0
.end method

.method static synthetic access$1702(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;Landroid/view/animation/AnimationSet;)Landroid/view/animation/AnimationSet;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvSummaryFragment;
    .param p1, "x1"    # Landroid/view/animation/AnimationSet;

    .prologue
    .line 111
    iput-object p1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mCircleAnimationSet:Landroid/view/animation/AnimationSet;

    return-object p1
.end method

.method static synthetic access$1800(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    .prologue
    .line 111
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondMeasuringProgressView:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    .prologue
    .line 111
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondMeasuringProgressView1:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    .prologue
    .line 111
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->spfCheckBox:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    .prologue
    .line 111
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mMeasuringAniLinear:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvSummaryFragment;
    .param p1, "x1"    # I

    .prologue
    .line 111
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->showRecommendedDialog(I)V

    return-void
.end method

.method static synthetic access$2200(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    .prologue
    .line 111
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mThirdComment:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    .prologue
    .line 111
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIcon:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    .prologue
    .line 111
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIconBigLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$2502(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;Landroid/animation/ValueAnimator;)Landroid/animation/ValueAnimator;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvSummaryFragment;
    .param p1, "x1"    # Landroid/animation/ValueAnimator;

    .prologue
    .line 111
    iput-object p1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mScaleAnimator:Landroid/animation/ValueAnimator;

    return-object p1
.end method

.method static synthetic access$2600(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    .prologue
    .line 111
    invoke-direct {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->setSecondCenterIconAnimation()V

    return-void
.end method

.method static synthetic access$2700(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    .prologue
    .line 111
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIconNoSkin:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$2800(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    .prologue
    .line 111
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIconBig:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$2902(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;Landroid/animation/ValueAnimator;)Landroid/animation/ValueAnimator;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvSummaryFragment;
    .param p1, "x1"    # Landroid/animation/ValueAnimator;

    .prologue
    .line 111
    iput-object p1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mScaleAnimatorFinished:Landroid/animation/ValueAnimator;

    return-object p1
.end method

.method static synthetic access$3000(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)Landroid/media/SoundPool;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    .prologue
    .line 111
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->pool:Landroid/media/SoundPool;

    return-object v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvSummaryFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 111
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->checkSPF:Z

    return p1
.end method

.method static synthetic access$3100(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)Landroid/os/SystemVibrator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    .prologue
    .line 111
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSystemVibrator:Landroid/os/SystemVibrator;

    return-object v0
.end method

.method static synthetic access$3200(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;Landroid/widget/ImageView;[IIZZ)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvSummaryFragment;
    .param p1, "x1"    # Landroid/widget/ImageView;
    .param p2, "x2"    # [I
    .param p3, "x3"    # I
    .param p4, "x4"    # Z
    .param p5, "x5"    # Z

    .prologue
    .line 111
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->animate(Landroid/widget/ImageView;[IIZZ)V

    return-void
.end method

.method static synthetic access$3300(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    .prologue
    .line 111
    iget v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->progressSPF:I

    return v0
.end method

.method static synthetic access$3302(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvSummaryFragment;
    .param p1, "x1"    # I

    .prologue
    .line 111
    iput p1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->progressSPF:I

    return p1
.end method

.method static synthetic access$3400(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    .prologue
    .line 111
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->spfSeekText:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$3500(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvSummaryFragment;
    .param p1, "x1"    # I

    .prologue
    .line 111
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->saveSelectedSPF(I)V

    return-void
.end method

.method static synthetic access$3600(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)Lcom/sec/android/app/shealth/uv/custom/UvGIFView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    .prologue
    .line 111
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIconBigAnimator:Lcom/sec/android/app/shealth/uv/custom/UvGIFView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    .prologue
    .line 111
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSpfIcon:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    .prologue
    .line 111
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->spfTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvSummaryFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 111
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->saveSPFChecked(Z)V

    return-void
.end method

.method static synthetic access$702(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvSummaryFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 111
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mStopSensor:Z

    return p1
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    .prologue
    .line 111
    invoke-direct {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->showGraphFragment()V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    .prologue
    .line 111
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private animate(Landroid/widget/ImageView;[IIZZ)V
    .locals 13
    .param p1, "imageView"    # Landroid/widget/ImageView;
    .param p2, "images"    # [I
    .param p3, "imageIndex"    # I
    .param p4, "forever"    # Z
    .param p5, "issunLoading"    # Z

    .prologue
    .line 2484
    if-eqz p5, :cond_0

    const/4 v9, 0x0

    .line 2485
    .local v9, "fadeInDuration":I
    :goto_0
    if-eqz p5, :cond_1

    const/16 v12, 0x32

    .line 2486
    .local v12, "timeBetween":I
    :goto_1
    if-eqz p5, :cond_2

    const/4 v11, 0x0

    .line 2488
    .local v11, "fadeOutDuration":I
    :goto_2
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2489
    aget v0, p2, p3

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2491
    new-instance v8, Landroid/view/animation/AlphaAnimation;

    const/4 v0, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {v8, v0, v1}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 2492
    .local v8, "fadeIn":Landroid/view/animation/Animation;
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v8, v0}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 2493
    int-to-long v0, v9

    invoke-virtual {v8, v0, v1}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 2495
    new-instance v10, Landroid/view/animation/AlphaAnimation;

    const/high16 v0, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    invoke-direct {v10, v0, v1}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 2496
    .local v10, "fadeOut":Landroid/view/animation/Animation;
    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v10, v0}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 2497
    add-int v0, v9, v12

    int-to-long v0, v0

    invoke-virtual {v10, v0, v1}, Landroid/view/animation/Animation;->setStartOffset(J)V

    .line 2498
    int-to-long v0, v11

    invoke-virtual {v10, v0, v1}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 2500
    new-instance v7, Landroid/view/animation/AnimationSet;

    const/4 v0, 0x0

    invoke-direct {v7, v0}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 2501
    .local v7, "animation":Landroid/view/animation/AnimationSet;
    invoke-virtual {v7, v8}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 2502
    invoke-virtual {v7, v10}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 2503
    const/4 v0, 0x1

    invoke-virtual {v7, v0}, Landroid/view/animation/AnimationSet;->setRepeatCount(I)V

    .line 2504
    invoke-virtual {p1, v7}, Landroid/widget/ImageView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 2506
    new-instance v0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$24;

    move-object v1, p0

    move-object v2, p2

    move/from16 v3, p3

    move-object v4, p1

    move/from16 v5, p4

    move/from16 v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$24;-><init>(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;[IILandroid/widget/ImageView;ZZ)V

    invoke-virtual {v7, v0}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 2526
    return-void

    .line 2484
    .end local v7    # "animation":Landroid/view/animation/AnimationSet;
    .end local v8    # "fadeIn":Landroid/view/animation/Animation;
    .end local v9    # "fadeInDuration":I
    .end local v10    # "fadeOut":Landroid/view/animation/Animation;
    .end local v11    # "fadeOutDuration":I
    .end local v12    # "timeBetween":I
    :cond_0
    const/16 v9, 0x1f4

    goto :goto_0

    .line 2485
    .restart local v9    # "fadeInDuration":I
    :cond_1
    const/16 v12, 0x3e8

    goto :goto_1

    .line 2486
    .restart local v12    # "timeBetween":I
    :cond_2
    const/16 v11, 0x1f4

    goto :goto_2
.end method

.method private calculateUvData()Lcom/sec/android/app/shealth/uv/data/UvStateType;
    .locals 3

    .prologue
    .line 1881
    sget-object v0, Lcom/sec/android/app/shealth/uv/data/UvStateType;->INVALID:Lcom/sec/android/app/shealth/uv/data/UvStateType;

    .line 1882
    .local v0, "stateType":Lcom/sec/android/app/shealth/uv/data/UvStateType;
    iget v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvIndex:I

    int-to-double v1, v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/uv/data/UvStateType;->setScore(D)V

    .line 1883
    return-object v0
.end method

.method private displayLocationDialogIfNeeded()V
    .locals 4

    .prologue
    .line 2934
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$34;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$34;-><init>(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2944
    return-void
.end method

.method private firstAnimation(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 1160
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvActivity:Lcom/sec/android/app/shealth/uv/UvActivity;

    if-nez v0, :cond_0

    .line 1193
    :goto_0
    return-void

    .line 1163
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvActivity:Lcom/sec/android/app/shealth/uv/UvActivity;

    new-instance v1, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$9;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$9;-><init>(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/uv/UvActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private getCheckedStatus()Z
    .locals 3

    .prologue
    .line 2714
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "spf_checked"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private getSPFValue()I
    .locals 3

    .prologue
    .line 2726
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "spf_value"

    const/16 v2, 0x1e

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method private getSkinTypeArrayindex(I)I
    .locals 3
    .param p1, "skinType"    # I

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x3

    const/4 v0, 0x2

    .line 2400
    if-gt p1, v0, :cond_1

    .line 2401
    const/4 v0, 0x0

    .line 2413
    :cond_0
    :goto_0
    return v0

    .line 2403
    :cond_1
    if-gt p1, v1, :cond_2

    .line 2404
    const/4 v0, 0x1

    goto :goto_0

    .line 2406
    :cond_2
    if-le p1, v2, :cond_0

    .line 2409
    const/4 v0, 0x5

    if-gt p1, v0, :cond_3

    move v0, v1

    .line 2410
    goto :goto_0

    :cond_3
    move v0, v2

    .line 2413
    goto :goto_0
.end method

.method private getUVArrayindex(I)I
    .locals 2
    .param p1, "uvScore"    # I

    .prologue
    const/4 v0, 0x2

    .line 2381
    if-gt p1, v0, :cond_1

    .line 2382
    const/4 v0, 0x0

    .line 2394
    :cond_0
    :goto_0
    return v0

    .line 2384
    :cond_1
    const/4 v1, 0x5

    if-gt p1, v1, :cond_2

    .line 2385
    const/4 v0, 0x1

    goto :goto_0

    .line 2387
    :cond_2
    const/4 v1, 0x7

    if-le p1, v1, :cond_0

    .line 2390
    const/16 v0, 0xa

    if-gt p1, v0, :cond_3

    .line 2391
    const/4 v0, 0x3

    goto :goto_0

    .line 2394
    :cond_3
    const/4 v0, 0x4

    goto :goto_0
.end method

.method private getZoomInAnimation()Landroid/view/animation/Animation;
    .locals 2

    .prologue
    .line 3018
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mContext:Landroid/content/Context;

    const v1, 0x7f040025

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    return-object v0
.end method

.method private initAoudioFile()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v4, 0x0

    const/4 v5, 0x1

    .line 2053
    new-instance v0, Landroid/media/SoundPool;

    invoke-direct {v0, v5, v6, v4}, Landroid/media/SoundPool;-><init>(III)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->pool:Landroid/media/SoundPool;

    .line 2054
    sget-object v0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->effectAudio:[I

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->pool:Landroid/media/SoundPool;

    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mContext:Landroid/content/Context;

    const v3, 0x7f06002c

    invoke-virtual {v1, v2, v3, v5}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v1

    aput v1, v0, v4

    .line 2056
    sget-object v0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->effectAudio:[I

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->pool:Landroid/media/SoundPool;

    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mContext:Landroid/content/Context;

    const v3, 0x7f060037

    invoke-virtual {v1, v2, v3, v5}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v1

    aput v1, v0, v5

    .line 2058
    sget-object v0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->effectAudio:[I

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->pool:Landroid/media/SoundPool;

    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mContext:Landroid/content/Context;

    const v4, 0x7f06002d

    invoke-virtual {v2, v3, v4, v5}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v2

    aput v2, v0, v1

    .line 2060
    sget-object v0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->effectAudio:[I

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->pool:Landroid/media/SoundPool;

    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mContext:Landroid/content/Context;

    const v3, 0x7f060028

    invoke-virtual {v1, v2, v3, v5}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v1

    aput v1, v0, v6

    .line 2062
    sget-object v0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->effectAudio:[I

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->pool:Landroid/media/SoundPool;

    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mContext:Landroid/content/Context;

    const v4, 0x7f06002a

    invoke-virtual {v2, v3, v4, v5}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v2

    aput v2, v0, v1

    .line 2064
    return-void
.end method

.method private initBuilder()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    .locals 3

    .prologue
    .line 2555
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mContext:Landroid/content/Context;

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 2558
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelable(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 2559
    const v1, 0x7f090db0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 2560
    const v1, 0x7f090044

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 2561
    const v1, 0x7f090048

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 2562
    const v1, 0x7f030266

    new-instance v2, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$25;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$25;-><init>(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(ILcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 2667
    new-instance v1, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$26;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$26;-><init>(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 2696
    new-instance v1, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$27;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$27;-><init>(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnDismissListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 2703
    return-object v0
.end method

.method private initToReadyState()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/16 v5, 0x8

    .line 1064
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvActivity:Lcom/sec/android/app/shealth/uv/UvActivity;

    invoke-static {v2}, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->selectedDate:Ljava/util/Date;

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->getLastTwoDataByTime(J)Ljava/util/ArrayList;

    move-result-object v1

    .line 1066
    .local v1, "uvLastDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/uv/data/UvData;>;"
    if-nez v1, :cond_0

    .line 1093
    :goto_0
    return-void

    .line 1068
    :cond_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_1

    .line 1069
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/uv/data/UvData;

    .line 1071
    .local v0, "msressLatestData":Lcom/sec/android/app/shealth/uv/data/UvData;
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvActivity:Lcom/sec/android/app/shealth/uv/UvActivity;

    invoke-static {v2}, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/uv/data/UvData;->getId()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->deleteRowById(Ljava/lang/String;)Z

    .line 1076
    .end local v0    # "msressLatestData":Lcom/sec/android/app/shealth/uv/data/UvData;
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->spfFinishedsunprotection:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1077
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondIcon:Landroid/widget/ImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1078
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mNoDataDescription:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1079
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mFinishTopLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1080
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mWearableSummaryNoData:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1081
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterTextFinish:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1082
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mStateBar:Lcom/sec/android/app/shealth/uv/custom/UvStateSummaryBar;

    invoke-virtual {v2, v5}, Lcom/sec/android/app/shealth/uv/custom/UvStateSummaryBar;->setVisibility(I)V

    .line 1083
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mStateChineseBar:Lcom/sec/android/app/shealth/uv/custom/UvStateSummaryChineseBar;

    invoke-virtual {v2, v5}, Lcom/sec/android/app/shealth/uv/custom/UvStateSummaryChineseBar;->setVisibility(I)V

    .line 1084
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->divider:Landroid/view/View;

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 1085
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mThirdCommentLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1086
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mState:Lcom/sec/android/app/shealth/uv/state/UvState;

    instance-of v2, v2, Lcom/sec/android/app/shealth/uv/state/UvStateReady;

    if-nez v2, :cond_2

    .line 1087
    iput-boolean v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mStopSensor:Z

    .line 1088
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->stopSensor()V

    .line 1089
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mState:Lcom/sec/android/app/shealth/uv/state/UvState;

    const/16 v3, -0x3e9

    const/16 v4, -0x7d1

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/shealth/uv/state/UvState;->updateNextState(II)V

    .line 1092
    :cond_2
    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->updateUvDataView(Z)V

    goto :goto_0
.end method

.method private initView(Landroid/view/View;)Landroid/view/View;
    .locals 10
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v9, 0x1

    const v8, 0x7f09020b

    const v7, 0x7f09020a

    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 447
    invoke-direct {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->getSPFValue()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->progressSPF:I

    .line 448
    const v0, 0x7f0805d1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mMeasuringAniLinear:Landroid/widget/LinearLayout;

    .line 450
    const v0, 0x7f0805d2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondMeasuringProgressView:Landroid/widget/ImageView;

    .line 452
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondMeasuringProgressView:Landroid/widget/ImageView;

    const/high16 v3, 0x42340000    # 45.0f

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setRotation(F)V

    .line 453
    const v0, 0x7f0809fd

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondMeasuringProgressView1:Landroid/widget/ImageView;

    .line 455
    const v0, 0x7f0809f6

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mFirstLayout:Landroid/widget/FrameLayout;

    .line 457
    const v0, 0x7f0809f7

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mFirstReadyLayout:Landroid/widget/LinearLayout;

    .line 459
    const v0, 0x7f0809fa

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mFirstStateLayout:Landroid/widget/LinearLayout;

    .line 461
    const v0, 0x7f0805ca

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mFirstStatus:Landroid/widget/TextView;

    .line 463
    const v0, 0x7f0805cb

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mFirstMessage:Landroid/widget/TextView;

    .line 466
    const v0, 0x7f080b10

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mFinishTopLayout:Landroid/widget/RelativeLayout;

    .line 468
    const v0, 0x7f080b22

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mWearableSummaryNoData:Landroid/widget/RelativeLayout;

    .line 470
    const v0, 0x7f080b24

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mNoDataText:Landroid/widget/TextView;

    .line 472
    const v0, 0x7f080b00

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mNoDataDescription:Landroid/widget/TextView;

    .line 474
    const v0, 0x7f080b01

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mNonSensorDateLayout:Landroid/widget/LinearLayout;

    .line 476
    const v0, 0x7f080b02

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mCurrDateTxt:Landroid/widget/TextView;

    .line 480
    const v0, 0x7f0805da

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/uv/custom/UvStateSummaryBar;

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mStateBar:Lcom/sec/android/app/shealth/uv/custom/UvStateSummaryBar;

    .line 482
    const v0, 0x7f080b0f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/uv/custom/UvStateSummaryChineseBar;

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mStateChineseBar:Lcom/sec/android/app/shealth/uv/custom/UvStateSummaryChineseBar;

    .line 485
    const v0, 0x7f080595

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondLayout:Landroid/widget/RelativeLayout;

    .line 487
    const v0, 0x7f0805bb

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondIcon:Landroid/widget/ImageView;

    .line 489
    const v0, 0x7f0805c1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIcon:Landroid/widget/ImageView;

    .line 491
    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIcon:Landroid/widget/ImageView;

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->isHideSkinSettings:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mState:Lcom/sec/android/app/shealth/uv/state/UvState;

    instance-of v0, v0, Lcom/sec/android/app/shealth/uv/state/UvStateReady;

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 495
    const v0, 0x7f080b21

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIconNoSkin:Landroid/widget/ImageView;

    .line 497
    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIconNoSkin:Landroid/widget/ImageView;

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->isHideSkinSettings:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mState:Lcom/sec/android/app/shealth/uv/state/UvState;

    instance-of v0, v0, Lcom/sec/android/app/shealth/uv/state/UvStateReady;

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 500
    const v0, 0x7f0809c1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIconBigLayout:Landroid/widget/RelativeLayout;

    .line 502
    const v0, 0x7f0805c8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIconBig:Landroid/widget/ImageView;

    .line 504
    const v0, 0x7f080b20

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIconBigAnimator:Lcom/sec/android/app/shealth/uv/custom/UvGIFView;

    .line 507
    const v0, 0x7f080b11

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterTextFinish:Landroid/widget/TextView;

    .line 509
    const v0, 0x7f080b19

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSpfContainer:Landroid/widget/RelativeLayout;

    .line 510
    const v0, 0x7f080a01

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mDataCollectedProgressPercent:Landroid/widget/TextView;

    .line 512
    const v0, 0x7f080aff

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondStart:Landroid/widget/LinearLayout;

    .line 514
    const v0, 0x7f080a03

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mThirdDiscard:Landroid/widget/LinearLayout;

    .line 516
    const v0, 0x7f080afe

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->divider:Landroid/view/View;

    .line 517
    const v0, 0x7f080b29

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondStartText:Landroid/widget/TextView;

    .line 519
    const v0, 0x7f080b0a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondDiscardText:Landroid/widget/TextView;

    .line 522
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mThirdDiscard:Landroid/widget/LinearLayout;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090f4d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 530
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondStart:Landroid/widget/LinearLayout;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0901ea

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 539
    const v0, 0x7f080b1b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSpfIcon:Landroid/widget/ImageView;

    .line 540
    const v0, 0x7f080b16

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSafeHr:Landroid/widget/TextView;

    .line 543
    const v0, 0x7f080b18

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSafeMin:Landroid/widget/TextView;

    .line 545
    const v0, 0x7f080b15

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSafeHrVal:Landroid/widget/TextView;

    .line 547
    const v0, 0x7f080b17

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSafeMinVal:Landroid/widget/TextView;

    .line 550
    const v0, 0x7f080b1d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->spfchecklay:Landroid/widget/LinearLayout;

    .line 553
    const v0, 0x7f080b1c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->spfTextView:Landroid/widget/TextView;

    .line 554
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->spfTextView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090db8

    new-array v5, v9, [Ljava/lang/Object;

    iget v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->progressSPF:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 557
    const v0, 0x7f080b1a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->spfSeekText:Landroid/widget/RelativeLayout;

    .line 559
    const v0, 0x7f080b0e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mCustomStateBarParent:Landroid/widget/RelativeLayout;

    .line 561
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->spfSeekText:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 563
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->spfSeekText:Landroid/widget/RelativeLayout;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->spfTextView:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 567
    const v0, 0x7f080b1f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->spfCheckBox:Landroid/widget/CheckBox;

    .line 568
    const v0, 0x7f080b1e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mspfCheckBoxLayout:Landroid/widget/RelativeLayout;

    .line 570
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->spfCheckBox:Landroid/widget/CheckBox;

    invoke-direct {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->getCheckedStatus()Z

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 571
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->spfCheckBox:Landroid/widget/CheckBox;

    const v3, 0x7f090db0

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setText(I)V

    .line 572
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mspfCheckBoxLayout:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mCheckboxLayoutClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 574
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->spfCheckBox:Landroid/widget/CheckBox;

    new-instance v3, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$4;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$4;-><init>(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)V

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 594
    const v0, 0x7f080b12

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->spfFinishedsunprotection:Landroid/widget/LinearLayout;

    .line 596
    const v0, 0x7f080b13

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->spfFinishedsunprotectionText:Landroid/widget/TextView;

    .line 600
    const v0, 0x7f0805e5

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mThirdPreviousLayout:Landroid/widget/LinearLayout;

    .line 602
    const v0, 0x7f080b03

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mLine:Landroid/view/View;

    .line 603
    const v0, 0x7f080b0c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mThirdCommentLayout:Landroid/widget/LinearLayout;

    .line 605
    const v0, 0x7f080b0d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mThirdComment:Landroid/widget/TextView;

    .line 607
    const v0, 0x7f080b26

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mThirdPreviousComment:Landroid/widget/TextView;

    .line 609
    const v0, 0x7f080b25

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mThirdPreviousImage:Landroid/widget/ImageView;

    .line 612
    const v0, 0x7f0805ec

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mThirdDate:Landroid/widget/TextView;

    .line 613
    const v0, 0x7f0809cd

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mThirdTime:Landroid/widget/TextView;

    .line 614
    const v0, 0x7f0805ef

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mThirdGraph:Landroid/widget/ImageButton;

    .line 617
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondStart:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setSoundEffectsEnabled(Z)V

    .line 618
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondStart:Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 619
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mThirdDiscard:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setSoundEffectsEnabled(Z)V

    .line 620
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mThirdDiscard:Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 621
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mThirdPreviousLayout:Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 622
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mThirdGraph:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 624
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setVisibility(I)V

    .line 626
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvActivity:Lcom/sec/android/app/shealth/uv/UvActivity;

    if-nez v0, :cond_0

    .line 627
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/uv/UvActivity;

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvActivity:Lcom/sec/android/app/shealth/uv/UvActivity;

    .line 628
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvActivity:Lcom/sec/android/app/shealth/uv/UvActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/uv/UvActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ActionBar;->show()V

    .line 629
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setVisibility(I)V

    .line 630
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->summaryFragContainer:Lcom/sec/android/app/shealth/framework/ui/gesture/GestureDetectorLinearLayout;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/gesture/GestureDetectorLinearLayout;->setGesturesEnabled(Z)V

    .line 632
    const v0, 0x7f0809f8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mReadyTitle:Landroid/widget/TextView;

    .line 634
    const v0, 0x7f0809f9

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mReadyMessage:Landroid/widget/TextView;

    .line 636
    const v0, 0x7f080b27

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mDeviceConnectedStatusView:Landroid/view/View;

    .line 638
    const v0, 0x7f080b28

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mDeviceConnectedTextView:Landroid/widget/TextView;

    .line 640
    invoke-direct {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->initializeDeviceFinder()V

    .line 641
    iput-boolean v9, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->isFragmentInitialized:Z

    .line 642
    const v0, 0x7f080b0b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->UvParentView:Landroid/widget/LinearLayout;

    .line 644
    return-object p1

    .line 491
    :cond_1
    const/4 v0, 0x4

    goto/16 :goto_0

    :cond_2
    move v0, v2

    .line 497
    goto/16 :goto_1
.end method

.method private initializeDeviceFinder()V
    .locals 3

    .prologue
    .line 678
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mDeviceConnectedStatusView:Landroid/view/View;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->getDeviceConnectionCheckListener(Landroid/view/View;)Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceConnectionCheckListener;

    move-result-object v0

    .line 679
    .local v0, "listener":Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceConnectionCheckListener;
    new-instance v1, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$5;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, p0, v2, v0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$5;-><init>(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;Landroid/content/Context;Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceConnectionCheckListener;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mDeviceFinder:Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;

    .line 686
    return-void
.end method

.method public static isSamsungAccount(Landroid/content/Context;)Z
    .locals 9
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2731
    const/4 v5, 0x0

    .line 2732
    .local v5, "isSamAccount":Z
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v8

    invoke-virtual {v8}, Landroid/accounts/AccountManager;->getAccounts()[Landroid/accounts/Account;

    move-result-object v1

    .line 2733
    .local v1, "accounts":[Landroid/accounts/Account;
    sget-object v3, Landroid/util/Patterns;->EMAIL_ADDRESS:Ljava/util/regex/Pattern;

    .line 2735
    .local v3, "emailPattern":Ljava/util/regex/Pattern;
    move-object v2, v1

    .local v2, "arr$":[Landroid/accounts/Account;
    array-length v6, v2

    .local v6, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v6, :cond_1

    aget-object v0, v2, v4

    .line 2736
    .local v0, "account":Landroid/accounts/Account;
    iget-object v8, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v3, v8}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/regex/Matcher;->matches()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 2737
    iget-object v7, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 2738
    .local v7, "possibleEmail":Ljava/lang/String;
    const-string/jumbo v8, "samstresstest@gmail.com"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 2739
    const/4 v5, 0x1

    .line 2735
    .end local v7    # "possibleEmail":Ljava/lang/String;
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 2743
    .end local v0    # "account":Landroid/accounts/Account;
    :cond_1
    return v5
.end method

.method private measuringCircleAnimationStart(Z)V
    .locals 2
    .param p1, "isFailed"    # Z

    .prologue
    .line 1207
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvActivity:Lcom/sec/android/app/shealth/uv/UvActivity;

    new-instance v1, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$11;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$11;-><init>(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;Z)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/uv/UvActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1241
    return-void
.end method

.method private measuringCircleAnimationStop(Z)V
    .locals 3
    .param p1, "isEnd"    # Z

    .prologue
    .line 1245
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mMeasuringAniLinear:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->clearAnimation()V

    .line 1246
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mCircleAnimationSet:Landroid/view/animation/AnimationSet;

    if-eqz v1, :cond_0

    .line 1247
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mCircleAnimationSet:Landroid/view/animation/AnimationSet;

    invoke-virtual {v1}, Landroid/view/animation/AnimationSet;->cancel()V

    .line 1248
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mCircleAnimationSet:Landroid/view/animation/AnimationSet;

    .line 1251
    :cond_0
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 1252
    .local v0, "fadeOut":Landroid/view/animation/Animation;
    const-wide/16 v1, 0x14d

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1253
    new-instance v1, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$12;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$12;-><init>(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1270
    if-eqz p1, :cond_2

    .line 1271
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mCircleAnimationSet:Landroid/view/animation/AnimationSet;

    if-eqz v1, :cond_1

    .line 1272
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondMeasuringProgressView:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1278
    :cond_1
    :goto_0
    return-void

    .line 1275
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mMeasuringAniLinear:Landroid/widget/LinearLayout;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1276
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mMeasuringAniLinear:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->clearAnimation()V

    goto :goto_0
.end method

.method private playSound(I)V
    .locals 5
    .param p1, "effectAudio"    # I

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 2067
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvActivity:Lcom/sec/android/app/shealth/uv/UvActivity;

    if-nez v1, :cond_1

    .line 2118
    :cond_0
    :goto_0
    return-void

    .line 2071
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mContext:Landroid/content/Context;

    const-string v2, "audio"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 2074
    .local v0, "aManager":Landroid/media/AudioManager;
    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v1

    if-eqz v1, :cond_0

    .line 2077
    sget-object v1, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->effectAudio:[I

    aget v1, v1, v3

    if-ne p1, v1, :cond_2

    .line 2078
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mMeasuringHapticThread:Lcom/sec/android/app/shealth/uv/UvSummaryFragment$measuringHapticThread;

    if-nez v1, :cond_2

    .line 2079
    new-instance v1, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$measuringHapticThread;

    invoke-direct {v1, p0, v3}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$measuringHapticThread;-><init>(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;Z)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mMeasuringHapticThread:Lcom/sec/android/app/shealth/uv/UvSummaryFragment$measuringHapticThread;

    .line 2080
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mMeasuringHapticThread:Lcom/sec/android/app/shealth/uv/UvSummaryFragment$measuringHapticThread;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$measuringHapticThread;->start()V

    .line 2084
    :cond_2
    sget-object v1, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->effectAudio:[I

    const/4 v2, 0x4

    aget v1, v1, v2

    if-ne p1, v1, :cond_4

    .line 2085
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mMeasuringHapticThread:Lcom/sec/android/app/shealth/uv/UvSummaryFragment$measuringHapticThread;

    if-eqz v1, :cond_3

    .line 2086
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSystemVibrator:Landroid/os/SystemVibrator;

    invoke-virtual {v1}, Landroid/os/SystemVibrator;->cancel()V

    .line 2087
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mMeasuringHapticThread:Lcom/sec/android/app/shealth/uv/UvSummaryFragment$measuringHapticThread;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$measuringHapticThread;->stopThread()V

    .line 2088
    iput-object v4, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mMeasuringHapticThread:Lcom/sec/android/app/shealth/uv/UvSummaryFragment$measuringHapticThread;

    .line 2090
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSystemVibrator:Landroid/os/SystemVibrator;

    sget-object v2, Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;->ivt_finish:[B

    invoke-virtual {v1, v2}, Landroid/os/SystemVibrator;->vibrateImmVibe([B)V

    .line 2092
    :cond_4
    sget-object v1, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->effectAudio:[I

    const/4 v2, 0x3

    aget v1, v1, v2

    if-ne p1, v1, :cond_6

    .line 2093
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mMeasuringHapticThread:Lcom/sec/android/app/shealth/uv/UvSummaryFragment$measuringHapticThread;

    if-eqz v1, :cond_5

    .line 2094
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSystemVibrator:Landroid/os/SystemVibrator;

    invoke-virtual {v1}, Landroid/os/SystemVibrator;->cancel()V

    .line 2095
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mMeasuringHapticThread:Lcom/sec/android/app/shealth/uv/UvSummaryFragment$measuringHapticThread;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$measuringHapticThread;->stopThread()V

    .line 2096
    iput-object v4, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mMeasuringHapticThread:Lcom/sec/android/app/shealth/uv/UvSummaryFragment$measuringHapticThread;

    .line 2098
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSystemVibrator:Landroid/os/SystemVibrator;

    sget-object v2, Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;->ivt_error:[B

    invoke-virtual {v1, v2}, Landroid/os/SystemVibrator;->vibrateImmVibe([B)V

    .line 2101
    :cond_6
    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v1

    if-eq v1, v3, :cond_0

    .line 2104
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->pool:Landroid/media/SoundPool;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvActivity:Lcom/sec/android/app/shealth/uv/UvActivity;

    if-eqz v1, :cond_0

    .line 2105
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvActivity:Lcom/sec/android/app/shealth/uv/UvActivity;

    new-instance v2, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$21;

    invoke-direct {v2, p0, p1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$21;-><init>(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;I)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/uv/UvActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private saveSPFChecked(Z)V
    .locals 3
    .param p1, "flag"    # Z

    .prologue
    .line 2707
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 2709
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string/jumbo v1, "spf_checked"

    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->spfCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 2710
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2711
    return-void
.end method

.method private saveSelectedSPF(I)V
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 2719
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 2721
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string/jumbo v1, "spf_value"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 2722
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2723
    return-void
.end method

.method private scaleAnimationFinished(Z)V
    .locals 3
    .param p1, "isGreen"    # Z

    .prologue
    .line 1998
    sget-object v0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "scaleAnimationFinished isGreen: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 2000
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mScaleAnimatorFinished:Landroid/animation/ValueAnimator;

    if-nez v0, :cond_0

    .line 2001
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIconBig:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 2002
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIconBig:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIconBig:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v1

    int-to-float v1, v1

    const v2, 0x3eeb851f    # 0.46f

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setPivotX(F)V

    .line 2003
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIcon:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIconBig:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v1

    int-to-float v1, v1

    const v2, 0x3ecccccd    # 0.4f

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setPivotY(F)V

    .line 2004
    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mScaleAnimatorFinished:Landroid/animation/ValueAnimator;

    .line 2005
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mScaleAnimatorFinished:Landroid/animation/ValueAnimator;

    const-wide/16 v1, 0x14d

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 2006
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mScaleAnimatorFinished:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$19;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$19;-><init>(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 2014
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mScaleAnimatorFinished:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$20;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$20;-><init>(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 2040
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mScaleAnimatorFinished:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 2044
    :goto_0
    return-void

    .line 2042
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIconBig:Landroid/widget/ImageView;

    const v1, 0x7f0201df

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 2004
    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        -0x403d70a4    # -1.52f
    .end array-data
.end method

.method private scaleAnimationStarter(Z)V
    .locals 4
    .param p1, "isGreen"    # Z

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    .line 1887
    sget-object v0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "scaleAnimationStarter isGreen: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1889
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mScaleAnimator:Landroid/animation/ValueAnimator;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIconBigLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_2

    .line 1891
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIcon:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 1892
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIcon:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIcon:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v1

    int-to-float v1, v1

    const v2, 0x3eeb851f    # 0.46f

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setPivotX(F)V

    .line 1894
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIcon:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIcon:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v3

    neg-float v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setPivotY(F)V

    .line 1896
    const/4 v0, 0x2

    new-array v1, v0, [F

    const/4 v0, 0x0

    aput v3, v1, v0

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->spfCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x3fc28f5c    # 1.52f

    :goto_0
    aput v0, v1, v2

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mScaleAnimator:Landroid/animation/ValueAnimator;

    .line 1898
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mScaleAnimator:Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->spfCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    const-wide/16 v0, 0x14d

    :goto_1
    invoke-virtual {v2, v0, v1}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 1899
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mScaleAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$15;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$15;-><init>(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 1908
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mScaleAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$16;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$16;-><init>(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1937
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mScaleAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 1942
    :goto_2
    return-void

    .line 1896
    :cond_0
    const/high16 v0, 0x3fa00000    # 1.25f

    goto :goto_0

    .line 1898
    :cond_1
    const-wide/16 v0, 0x113

    goto :goto_1

    .line 1939
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIcon:Landroid/widget/ImageView;

    const v1, 0x7f0201e0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_2
.end method

.method private scaleAnimatorStarterNoSKin(Z)V
    .locals 3
    .param p1, "isGreen"    # Z

    .prologue
    .line 1945
    sget-object v0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "scaleAnimationStarter isGreen: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1947
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mScaleAnimator:Landroid/animation/ValueAnimator;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIconBigLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 1949
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIconNoSkin:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 1950
    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mScaleAnimator:Landroid/animation/ValueAnimator;

    .line 1951
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mScaleAnimator:Landroid/animation/ValueAnimator;

    const-wide/16 v1, 0x14d

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 1952
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mScaleAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$17;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$17;-><init>(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 1961
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mScaleAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$18;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$18;-><init>(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1990
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mScaleAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 1994
    :goto_0
    return-void

    .line 1992
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIconNoSkin:Landroid/widget/ImageView;

    const v1, 0x7f0201e0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 1950
    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x3fc28f5c    # 1.52f
    .end array-data
.end method

.method private setReadyUI(I)V
    .locals 8
    .param p1, "animation"    # I

    .prologue
    const/4 v7, 0x1

    const v6, 0x7f0201e0

    const/4 v5, 0x0

    const/16 v4, 0x8

    .line 1335
    sget-object v1, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "setReadyUI : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1336
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->stopSensor()V

    .line 1337
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mMeasuringTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v1}, Landroid/os/CountDownTimer;->cancel()V

    .line 1339
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->clearDataCollection()V

    .line 1340
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIconBigAnimator:Lcom/sec/android/app/shealth/uv/custom/UvGIFView;

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->setVisibility(I)V

    .line 1341
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIconBigAnimator:Lcom/sec/android/app/shealth/uv/custom/UvGIFView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->clear()V

    .line 1342
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSpfContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1343
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mDataCollectedProgressPercent:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1345
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mStateBar:Lcom/sec/android/app/shealth/uv/custom/UvStateSummaryBar;

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/uv/custom/UvStateSummaryBar;->setVisibility(I)V

    .line 1346
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mStateChineseBar:Lcom/sec/android/app/shealth/uv/custom/UvStateSummaryChineseBar;

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/uv/custom/UvStateSummaryChineseBar;->setVisibility(I)V

    .line 1347
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondIcon:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1348
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mNoDataDescription:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1349
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mFinishTopLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1350
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mWearableSummaryNoData:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1351
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIconBig:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->clearAnimation()V

    .line 1352
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterTextFinish:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 1353
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterTextFinish:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1355
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIcon:Landroid/widget/ImageView;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1357
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvActivity:Lcom/sec/android/app/shealth/uv/UvActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/uv/UvActivity;->getAppContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f040022

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 1359
    .local v0, "alphaAnimation":Landroid/view/animation/Animation;
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIcon:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1361
    if-nez p1, :cond_1

    .line 1363
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mFirstStateLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1364
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mFirstReadyLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1369
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIconBigAnimator:Lcom/sec/android/app/shealth/uv/custom/UvGIFView;

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->setVisibility(I)V

    .line 1370
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIconBigAnimator:Lcom/sec/android/app/shealth/uv/custom/UvGIFView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->clear()V

    .line 1373
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondStartText:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090050

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1375
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondStart:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1376
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondStart:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v7}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 1377
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->refreshFragmentFocusables()V

    .line 1379
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->isHideSkinSettings:Z

    if-nez v1, :cond_2

    .line 1380
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIcon:Landroid/widget/ImageView;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1382
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIcon:Landroid/widget/ImageView;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1383
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIconNoSkin:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->clearAnimation()V

    .line 1384
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIconNoSkin:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1385
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIcon:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1386
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->spfchecklay:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1387
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->spfSeekText:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1399
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->spfFinishedsunprotection:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1400
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->UvParentView:Landroid/widget/LinearLayout;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mReadyTitle:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mReadyMessage:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1402
    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->updateUvDataView(Z)V

    .line 1403
    return-void

    .line 1366
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mFirstReadyLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1367
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mFirstStateLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_0

    .line 1390
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIconNoSkin:Landroid/widget/ImageView;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1392
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIconNoSkin:Landroid/widget/ImageView;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1393
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIcon:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->clearAnimation()V

    .line 1394
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIcon:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1395
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIconNoSkin:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1396
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->spfchecklay:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1397
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->spfSeekText:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_1
.end method

.method private setSecondCenterIconAnimation()V
    .locals 3

    .prologue
    .line 3022
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIconBigAnimator:Lcom/sec/android/app/shealth/uv/custom/UvGIFView;

    const v2, 0x7f0208cd

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/uv/custom/UvGIFView;->setMovieResource(I)V

    .line 3024
    invoke-direct {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->getZoomInAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    .line 3025
    .local v0, "zoomInAnimation":Landroid/view/animation/Animation;
    new-instance v1, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$39;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$39;-><init>(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 3042
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIconBig:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 3043
    return-void
.end method

.method private setUVReadyState()V
    .locals 1

    .prologue
    .line 1000
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->hasNoUvSensor:Z

    if-nez v0, :cond_0

    .line 1001
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->getStateReady()Lcom/sec/android/app/shealth/uv/state/UvState;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->setState(Lcom/sec/android/app/shealth/uv/state/UvState;)V

    .line 1002
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->setReadyUI(I)V

    .line 1004
    :cond_0
    return-void
.end method

.method private setUvLevelComment(Lcom/sec/android/app/shealth/uv/data/UvStateType;)V
    .locals 11
    .param p1, "stateType"    # Lcom/sec/android/app/shealth/uv/data/UvStateType;

    .prologue
    .line 1585
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/uv/data/UvStateType;->getScore()D

    move-result-wide v8

    double-to-int v4, v8

    .line 1586
    .local v4, "score":I
    const v7, 0x7f090d94

    .line 1587
    .local v7, "strLevelId":I
    const v1, 0x7f0201d9

    .line 1588
    .local v1, "imgLevelId":I
    const-string v3, ""

    .line 1589
    .local v3, "recomTitle":Ljava/lang/String;
    const/4 v8, -0x1

    iput v8, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->recomMsgIndex:I

    .line 1590
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1592
    .local v0, "context":Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->isChinaModel()Z

    move-result v8

    if-nez v8, :cond_5

    .line 1594
    const/4 v8, 0x2

    if-gt v4, v8, :cond_1

    .line 1595
    const v7, 0x7f090d94

    .line 1596
    const v1, 0x7f0201d9

    .line 1597
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f090dca

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1601
    const/4 v8, 0x0

    iput v8, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->recomMsgIndex:I

    .line 1670
    :cond_0
    :goto_0
    iget-object v8, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mContext:Landroid/content/Context;

    invoke-static {v8}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->isSamsungAccount(Landroid/content/Context;)Z

    move-result v8

    if-eqz v8, :cond_a

    sget-object v8, Landroid/os/Build;->TYPE:Ljava/lang/String;

    const-string v9, "eng"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_a

    .line 1672
    iget-object v8, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterTextFinish:Landroid/widget/TextView;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    invoke-virtual {v10, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "( "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " )"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1679
    :goto_1
    const/4 v8, -0x2

    if-eq v4, v8, :cond_b

    .line 1680
    iget-object v8, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterTextFinish:Landroid/widget/TextView;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1681
    iget-object v8, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondIcon:Landroid/widget/ImageView;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1682
    iget-object v8, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mFinishTopLayout:Landroid/widget/RelativeLayout;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1683
    iget-object v8, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondIcon:Landroid/widget/ImageView;

    invoke-virtual {v8, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1684
    iget-object v8, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mNoDataDescription:Landroid/widget/TextView;

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1685
    iget-object v8, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mWearableSummaryNoData:Landroid/widget/RelativeLayout;

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1694
    :goto_2
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f09090b

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1696
    .local v2, "moreString":Ljava/lang/String;
    new-instance v6, Landroid/text/SpannableString;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v8}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 1699
    .local v6, "ss":Landroid/text/SpannableString;
    new-instance v5, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$14;

    invoke-direct {v5, p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$14;-><init>(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)V

    .line 1708
    .local v5, "span2":Landroid/text/style/ClickableSpan;
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, 0x1

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, 0x1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v10

    add-int/2addr v9, v10

    const/16 v10, 0x21

    invoke-virtual {v6, v5, v8, v9, v10}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 1710
    iget-object v8, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mThirdComment:Landroid/widget/TextView;

    invoke-virtual {v8, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1711
    iget-object v8, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mThirdComment:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 1712
    iget-object v8, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mThirdCommentLayout:Landroid/widget/LinearLayout;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1713
    return-void

    .line 1602
    .end local v2    # "moreString":Ljava/lang/String;
    .end local v5    # "span2":Landroid/text/style/ClickableSpan;
    .end local v6    # "ss":Landroid/text/SpannableString;
    :cond_1
    const/4 v8, 0x5

    if-gt v4, v8, :cond_2

    .line 1603
    const v7, 0x7f090d95

    .line 1604
    const v1, 0x7f0201da

    .line 1605
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f090dcb

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1609
    const/4 v8, 0x1

    iput v8, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->recomMsgIndex:I

    goto/16 :goto_0

    .line 1610
    :cond_2
    const/4 v8, 0x7

    if-gt v4, v8, :cond_3

    .line 1611
    const v7, 0x7f090d96

    .line 1612
    const v1, 0x7f0201db

    .line 1613
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f090dcf

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1617
    const/4 v8, 0x2

    iput v8, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->recomMsgIndex:I

    goto/16 :goto_0

    .line 1618
    :cond_3
    const/16 v8, 0xa

    if-gt v4, v8, :cond_4

    .line 1619
    const v7, 0x7f090d97

    .line 1620
    const v1, 0x7f0201dc

    .line 1621
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f090dd0

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1625
    const/4 v8, 0x3

    iput v8, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->recomMsgIndex:I

    goto/16 :goto_0

    .line 1626
    :cond_4
    const/16 v8, 0xa

    if-le v4, v8, :cond_0

    .line 1627
    const v7, 0x7f090d98

    .line 1628
    const v1, 0x7f0201dd

    .line 1629
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f090dd1

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1633
    const/4 v8, 0x4

    iput v8, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->recomMsgIndex:I

    goto/16 :goto_0

    .line 1636
    :cond_5
    const/4 v8, 0x2

    if-gt v4, v8, :cond_6

    .line 1637
    const v7, 0x7f091158

    .line 1638
    const v1, 0x7f0201d9

    .line 1639
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f09114f

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1641
    const/4 v8, 0x0

    iput v8, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->recomMsgIndex:I

    goto/16 :goto_0

    .line 1642
    :cond_6
    const/4 v8, 0x4

    if-gt v4, v8, :cond_7

    .line 1643
    const v7, 0x7f091159

    .line 1644
    const v1, 0x7f0201da

    .line 1645
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f091151

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1647
    const/4 v8, 0x1

    iput v8, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->recomMsgIndex:I

    goto/16 :goto_0

    .line 1648
    :cond_7
    const/4 v8, 0x6

    if-gt v4, v8, :cond_8

    .line 1649
    const v7, 0x7f09115a

    .line 1650
    const v1, 0x7f0201db

    .line 1651
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f091153

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1653
    const/4 v8, 0x2

    iput v8, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->recomMsgIndex:I

    goto/16 :goto_0

    .line 1654
    :cond_8
    const/16 v8, 0x9

    if-gt v4, v8, :cond_9

    .line 1655
    const v7, 0x7f09115b

    .line 1656
    const v1, 0x7f0201dc

    .line 1657
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f091155

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1659
    const/4 v8, 0x3

    iput v8, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->recomMsgIndex:I

    goto/16 :goto_0

    .line 1660
    :cond_9
    const/16 v8, 0x9

    if-le v4, v8, :cond_0

    .line 1661
    const v7, 0x7f09115c

    .line 1662
    const v1, 0x7f0201dd

    .line 1663
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f091157

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1665
    const/4 v8, 0x4

    iput v8, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->recomMsgIndex:I

    goto/16 :goto_0

    .line 1676
    :cond_a
    iget-object v8, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterTextFinish:Landroid/widget/TextView;

    invoke-virtual {v8, v7}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_1

    .line 1687
    :cond_b
    iget-object v8, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mFinishTopLayout:Landroid/widget/RelativeLayout;

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1688
    iget-object v8, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mNoDataDescription:Landroid/widget/TextView;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1689
    iget-object v8, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterTextFinish:Landroid/widget/TextView;

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1690
    iget-object v8, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondIcon:Landroid/widget/ImageView;

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1691
    iget-object v8, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mWearableSummaryNoData:Landroid/widget/RelativeLayout;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto/16 :goto_2
.end method

.method private showGraphFragment()V
    .locals 4

    .prologue
    .line 1143
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->stopSensor()V

    .line 1144
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mMeasuringTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v1}, Landroid/os/CountDownTimer;->cancel()V

    .line 1145
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->clearDataCollection()V

    .line 1146
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mState:Lcom/sec/android/app/shealth/uv/state/UvState;

    instance-of v1, v1, Lcom/sec/android/app/shealth/uv/state/UvStateFinished;

    if-nez v1, :cond_0

    .line 1147
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->getStateReady()Lcom/sec/android/app/shealth/uv/state/UvState;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->setState(Lcom/sec/android/app/shealth/uv/state/UvState;)V

    .line 1148
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->getStateReady()Lcom/sec/android/app/shealth/uv/state/UvState;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/uv/state/UvState;->updateViewWithAnimation(Z)V

    .line 1151
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvActivity:Lcom/sec/android/app/shealth/uv/UvActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/uv/UvActivity;->switchFragmentToGraph()V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1156
    return-void

    .line 1152
    :catch_0
    move-exception v0

    .line 1153
    .local v0, "e":Ljava/lang/ClassCastException;
    new-instance v1, Ljava/lang/ClassCastException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-class v3, Lcom/sec/android/app/shealth/uv/UvActivity;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " must be instance of HeartrateActivity"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private showRecommendedDialog(I)V
    .locals 9
    .param p1, "recommendIndex"    # I

    .prologue
    const v8, 0x7f090dba

    const/4 v5, 0x1

    const/4 v7, 0x0

    .line 2747
    const/4 v2, 0x0

    .line 2748
    .local v2, "title":Ljava/lang/String;
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mContext:Landroid/content/Context;

    invoke-direct {v0, v3, v7}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 2750
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-virtual {v0, v7}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelable(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 2751
    const v3, 0x7f090047

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 2752
    invoke-static {}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->isChinaModel()Z

    move-result v1

    .line 2754
    .local v1, "isChinaModel":Z
    packed-switch p1, :pswitch_data_0

    .line 2892
    :goto_0
    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 2894
    new-instance v3, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$33;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$33;-><init>(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)V

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnDismissListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 2901
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 2902
    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvRecommendedDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-nez v3, :cond_0

    .line 2903
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvRecommendedDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 2905
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvRecommendedDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    const-string/jumbo v5, "recommendedDialog"

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 2909
    :cond_1
    return-void

    .line 2757
    :pswitch_0
    iget-object v4, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mContext:Landroid/content/Context;

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_2

    const v3, 0x7f091158

    :goto_1
    invoke-virtual {v6, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v7

    invoke-virtual {v4, v8, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2760
    if-eqz v1, :cond_3

    const v3, 0x7f030262

    :goto_2
    new-instance v4, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$28;

    invoke-direct {v4, p0, v1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$28;-><init>(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;Z)V

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(ILcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    goto :goto_0

    .line 2757
    :cond_2
    const v3, 0x7f090d94

    goto :goto_1

    .line 2760
    :cond_3
    const v3, 0x7f030270

    goto :goto_2

    .line 2787
    :pswitch_1
    iget-object v4, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mContext:Landroid/content/Context;

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_4

    const v3, 0x7f091159

    :goto_3
    invoke-virtual {v6, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v7

    invoke-virtual {v4, v8, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2790
    if-eqz v1, :cond_5

    const v3, 0x7f030261

    :goto_4
    new-instance v4, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$29;

    invoke-direct {v4, p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$29;-><init>(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)V

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(ILcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    goto :goto_0

    .line 2787
    :cond_4
    const v3, 0x7f090d95

    goto :goto_3

    .line 2790
    :cond_5
    const v3, 0x7f030271

    goto :goto_4

    .line 2806
    :pswitch_2
    iget-object v4, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mContext:Landroid/content/Context;

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_6

    const v3, 0x7f09115a

    :goto_5
    invoke-virtual {v6, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v7

    invoke-virtual {v4, v8, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2809
    if-eqz v1, :cond_7

    const v3, 0x7f03025e

    :goto_6
    new-instance v4, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$30;

    invoke-direct {v4, p0, v1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$30;-><init>(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;Z)V

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(ILcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    goto/16 :goto_0

    .line 2806
    :cond_6
    const v3, 0x7f090d96

    goto :goto_5

    .line 2809
    :cond_7
    const v3, 0x7f03026f

    goto :goto_6

    .line 2834
    :pswitch_3
    iget-object v4, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mContext:Landroid/content/Context;

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_8

    const v3, 0x7f09115b

    :goto_7
    invoke-virtual {v6, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v7

    invoke-virtual {v4, v8, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2837
    if-eqz v1, :cond_9

    const v3, 0x7f03025f

    :goto_8
    new-instance v4, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$31;

    invoke-direct {v4, p0, v1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$31;-><init>(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;Z)V

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(ILcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    goto/16 :goto_0

    .line 2834
    :cond_8
    const v3, 0x7f090d97

    goto :goto_7

    .line 2837
    :cond_9
    const v3, 0x7f030272

    goto :goto_8

    .line 2863
    :pswitch_4
    iget-object v4, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mContext:Landroid/content/Context;

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_a

    const v3, 0x7f09115c

    :goto_9
    invoke-virtual {v6, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v7

    invoke-virtual {v4, v8, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2866
    if-eqz v1, :cond_b

    const v3, 0x7f030260

    :goto_a
    new-instance v4, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$32;

    invoke-direct {v4, p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$32;-><init>(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)V

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(ILcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    goto/16 :goto_0

    .line 2863
    :cond_a
    const v3, 0x7f090d98

    goto :goto_9

    .line 2866
    :cond_b
    const v3, 0x7f03026e

    goto :goto_a

    .line 2754
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private showSkinTypeDialog(I)V
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 2419
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvRecommendDialog:Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;

    if-nez v0, :cond_0

    .line 2420
    new-instance v0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvRecommendDialog:Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;

    .line 2421
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvRecommendDialog:Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->updateContent(I)V

    .line 2424
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvRecommendDialog:Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->show()V

    .line 2425
    return-void
.end method

.method private showSpfPickerDialog()V
    .locals 4

    .prologue
    .line 2537
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvActivity:Lcom/sec/android/app/shealth/uv/UvActivity;

    if-eqz v1, :cond_0

    .line 2538
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvActivity:Lcom/sec/android/app/shealth/uv/UvActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/uv/UvActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x80

    invoke-virtual {v1, v2}, Landroid/view/Window;->clearFlags(I)V

    .line 2540
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->initBuilder()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    .line 2542
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 2543
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvSPFDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-nez v1, :cond_1

    .line 2544
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvSPFDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 2546
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSpfNumberPicker:Landroid/widget/NumberPicker;

    if-eqz v1, :cond_2

    .line 2547
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSpfNumberPicker:Landroid/widget/NumberPicker;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/NumberPicker;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iget v2, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->progressSPF:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2550
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvSPFDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string/jumbo v3, "spfDialog"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 2552
    :cond_3
    return-void
.end method

.method private startMeasuringUv()V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    const/16 v4, 0x8

    .line 1096
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->spfCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->isHideSkinSettings:Z

    if-nez v1, :cond_1

    move v1, v2

    :goto_0
    invoke-static {v1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v0

    .line 1098
    .local v0, "extra":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIconBigLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1099
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->spfFinishedsunprotection:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1100
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondIcon:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1101
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mNoDataDescription:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1102
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mFinishTopLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1103
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mWearableSummaryNoData:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1104
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterTextFinish:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1105
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mStateBar:Lcom/sec/android/app/shealth/uv/custom/UvStateSummaryBar;

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/uv/custom/UvStateSummaryBar;->setVisibility(I)V

    .line 1106
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mStateChineseBar:Lcom/sec/android/app/shealth/uv/custom/UvStateSummaryChineseBar;

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/uv/custom/UvStateSummaryChineseBar;->setVisibility(I)V

    .line 1107
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mThirdDiscard:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1108
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->divider:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1110
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mThirdCommentLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1111
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mState:Lcom/sec/android/app/shealth/uv/state/UvState;

    instance-of v1, v1, Lcom/sec/android/app/shealth/uv/state/UvStateReady;

    if-nez v1, :cond_0

    .line 1112
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mStopSensor:Z

    .line 1113
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->stopSensor()V

    .line 1114
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mState:Lcom/sec/android/app/shealth/uv/state/UvState;

    const/16 v4, -0x3e9

    const/16 v5, -0x7d1

    invoke-virtual {v1, v4, v5}, Lcom/sec/android/app/shealth/uv/state/UvState;->updateNextState(II)V

    .line 1117
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mState:Lcom/sec/android/app/shealth/uv/state/UvState;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/uv/state/UvState;->clearForNextState()V

    .line 1118
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->getStateMeasuring()Lcom/sec/android/app/shealth/uv/state/UvState;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->setState(Lcom/sec/android/app/shealth/uv/state/UvState;)V

    .line 1119
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->getStateMeasuring()Lcom/sec/android/app/shealth/uv/state/UvState;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/sec/android/app/shealth/uv/state/UvState;->updateViewWithAnimation(Z)V

    .line 1120
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mStopSensor:Z

    .line 1125
    new-instance v1, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$8;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$8;-><init>(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mMeasuringRunnable:Ljava/lang/Runnable;

    .line 1133
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvHandler:Landroid/os/Handler;

    .line 1134
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mMeasuringRunnable:Ljava/lang/Runnable;

    const-wide/16 v4, 0x3e8

    invoke-virtual {v1, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1137
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mContext:Landroid/content/Context;

    const-string v3, "com.sec.android.app.shealth.uv"

    const-string v4, "UV01"

    invoke-static {v1, v3, v4}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 1139
    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->updateUvDataView(Z)V

    .line 1140
    return-void

    .end local v0    # "extra":Ljava/lang/String;
    :cond_1
    move v1, v3

    .line 1096
    goto/16 :goto_0
.end method

.method private updateFinishLayoutParams()V
    .locals 6

    .prologue
    const/16 v3, 0xd

    const/16 v5, 0xc

    const/4 v4, -0x1

    .line 1861
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mCustomStateBarParent:Landroid/widget/RelativeLayout;

    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1863
    .local v0, "params":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->spfFinishedsunprotection:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    .line 1865
    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 1866
    const/16 v1, 0xe

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1867
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvPluginUtils:Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;

    const/16 v2, 0x4e

    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;->convertDptoPx(ILandroid/content/Context;)I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 1868
    invoke-virtual {v0, v5, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1877
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mCustomStateBarParent:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1878
    return-void

    .line 1872
    :cond_0
    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 1873
    invoke-virtual {v0, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1874
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvPluginUtils:Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;->convertDptoPx(ILandroid/content/Context;)I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    goto :goto_0
.end method

.method private updateText()V
    .locals 3

    .prologue
    .line 733
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mReadyTitle:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090c05

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 735
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mReadyMessage:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090d9a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 737
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->spfCheckBox:Landroid/widget/CheckBox;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090db0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    .line 739
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mThirdGraph:Landroid/widget/ImageButton;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090063

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 742
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mNoDataText:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09006a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 744
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mNoDataDescription:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090d9d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 747
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mDeviceConnectedTextView:Landroid/widget/TextView;

    const v1, 0x7f090060

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 749
    return-void
.end method


# virtual methods
.method public cancelSoundVibration()V
    .locals 1

    .prologue
    .line 3002
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->pool:Landroid/media/SoundPool;

    if-eqz v0, :cond_0

    .line 3003
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->pool:Landroid/media/SoundPool;

    invoke-virtual {v0}, Landroid/media/SoundPool;->autoPause()V

    .line 3004
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->pool:Landroid/media/SoundPool;

    invoke-virtual {v0}, Landroid/media/SoundPool;->release()V

    .line 3007
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSystemVibrator:Landroid/os/SystemVibrator;

    if-eqz v0, :cond_1

    .line 3008
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSystemVibrator:Landroid/os/SystemVibrator;

    invoke-virtual {v0}, Landroid/os/SystemVibrator;->cancel()V

    .line 3009
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mMeasuringHapticThread:Lcom/sec/android/app/shealth/uv/UvSummaryFragment$measuringHapticThread;

    if-eqz v0, :cond_1

    .line 3010
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mMeasuringHapticThread:Lcom/sec/android/app/shealth/uv/UvSummaryFragment$measuringHapticThread;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$measuringHapticThread;->stopThread()V

    .line 3011
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mMeasuringHapticThread:Lcom/sec/android/app/shealth/uv/UvSummaryFragment$measuringHapticThread;

    .line 3015
    :cond_1
    return-void
.end method

.method public checkforLocation(Landroid/content/Context;)Z
    .locals 12
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const v10, 0x1001d

    const/4 v7, 0x0

    const/4 v8, 0x1

    .line 331
    const-string/jumbo v9, "sensor"

    invoke-virtual {p1, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/hardware/SensorManager;

    .line 332
    .local v4, "mSensorManager":Landroid/hardware/SensorManager;
    invoke-virtual {v4, v10}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v5

    .line 334
    .local v5, "mUvSensor":Landroid/hardware/Sensor;
    if-nez v5, :cond_1

    .line 335
    sget-object v9, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "mUvsensor is "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " !!!!!"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 336
    invoke-static {p1, v7}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setUVAvailablilty(Landroid/content/Context;Z)V

    .line 337
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    invoke-virtual {v7}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 356
    :cond_0
    :goto_0
    return v8

    .line 340
    :cond_1
    invoke-virtual {v5}, Landroid/hardware/Sensor;->getVendor()Ljava/lang/String;

    move-result-object v6

    .line 341
    .local v6, "mVendor":Ljava/lang/String;
    if-eqz v6, :cond_0

    const-string v9, "MAXIM"

    invoke-virtual {v9, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 342
    const/4 v0, 0x0

    .line 344
    .local v0, "isLocationAvailable":Z
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 345
    .local v3, "mPackageManager":Landroid/content/pm/PackageManager;
    const-string v9, "com.sec.feature.slocation"

    invoke-virtual {v3, v9}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 346
    new-instance v2, Lcom/maximintegrated/bio/uv/MaximUVSensor;

    invoke-direct {v2, p1, v10}, Lcom/maximintegrated/bio/uv/MaximUVSensor;-><init>(Landroid/content/Context;I)V

    .line 347
    .local v2, "mMaximUvSensor":Lcom/maximintegrated/bio/uv/MaximUVSensor;
    invoke-virtual {v2}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->checkPassiveLocation()Z

    move-result v0

    .line 350
    .end local v2    # "mMaximUvSensor":Lcom/maximintegrated/bio/uv/MaximUVSensor;
    :cond_2
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->checkforProvider(Landroid/content/Context;)Z

    move-result v1

    .line 352
    .local v1, "isProviderAvailable":Z
    sget-object v9, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "checkforProvider : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " / "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 354
    if-nez v1, :cond_3

    if-eqz v0, :cond_4

    :cond_3
    move v7, v8

    :cond_4
    move v8, v7

    goto :goto_0
.end method

.method public checkforProvider(Landroid/content/Context;)Z
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 316
    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mContext:Landroid/content/Context;

    const-string v3, "location"

    invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/location/LocationManager;

    .line 317
    .local v2, "lm":Landroid/location/LocationManager;
    const-string v3, "gps"

    invoke-virtual {v2, v3}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    .line 318
    .local v0, "isGPS":Z
    const-string/jumbo v3, "network"

    invoke-virtual {v2, v3}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v1

    .line 320
    .local v1, "isNetworkEnabled":Z
    sget-object v3, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "checkforProvider : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " / "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 322
    if-nez v0, :cond_0

    if-nez v1, :cond_0

    .line 323
    const/4 v3, 0x0

    .line 325
    :goto_0
    return v3

    :cond_0
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public clearDataCollection()V
    .locals 2

    .prologue
    .line 1535
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->dataCollection:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 1536
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->dataCollection:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1538
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvValue:I

    .line 1539
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mDataCollectedProgressPercent:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 1540
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mDataCollectedProgressPercent:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1541
    :cond_1
    return-void
.end method

.method public clearDeviceConnector()V
    .locals 1

    .prologue
    .line 3046
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->stopSensor()V

    .line 3047
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mDeviceConnector:Lcom/sec/android/app/shealth/uv/common/UvDeviceConnector;

    if-eqz v0, :cond_0

    .line 3048
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mDeviceConnector:Lcom/sec/android/app/shealth/uv/common/UvDeviceConnector;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/uv/common/UvDeviceConnector;->clear()V

    .line 3049
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mDeviceConnector:Lcom/sec/android/app/shealth/uv/common/UvDeviceConnector;

    .line 3051
    :cond_0
    return-void
.end method

.method public clearintervalCollection()V
    .locals 2

    .prologue
    .line 1546
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->intervalCollection:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 1547
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->intervalCollection:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1549
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->sumInterval:I

    .line 1550
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mDataCollectedProgressPercent:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1551
    return-void
.end method

.method public createLocationOnPopup(Landroid/content/Context;)V
    .locals 4
    .param p1, "mContext"    # Landroid/content/Context;

    .prologue
    const v3, 0x7f090d92

    .line 2437
    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;II)V

    const v2, 0x7f090d93

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$22;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$22;-><init>(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mDialogLocationListener:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnDismissListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    .line 2459
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2460
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvLocationAlertDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-nez v1, :cond_0

    .line 2461
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvLocationAlertDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 2463
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvLocationAlertDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "location-popUp"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 2465
    :cond_1
    return-void
.end method

.method public dataCollectingUI(II)V
    .locals 8
    .param p1, "rateData"    # I
    .param p2, "interval"    # I

    .prologue
    const/16 v7, 0x64

    .line 1559
    sget-object v3, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->TAG:Ljava/lang/String;

    const-string v4, "dataCollectingUI"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1561
    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->intervalCollection:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->intervalCollection:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v4, 0x1

    if-ge v3, v4, :cond_1

    .line 1562
    :cond_0
    const/4 v3, 0x0

    iput v3, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->sumInterval:I

    .line 1564
    :cond_1
    const/4 v2, 0x0

    .line 1565
    .local v2, "sumValue":I
    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->intervalCollection:Ljava/util/ArrayList;

    if-eqz v3, :cond_2

    .line 1566
    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->intervalCollection:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 1567
    .local v1, "intervalValue":Ljava/lang/Integer;
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    .line 1570
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "intervalValue":Ljava/lang/Integer;
    :cond_2
    iput v2, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->sumInterval:I

    .line 1572
    sget-object v3, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "dataCollectingUI : sumInterval = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->sumInterval:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1573
    iget v3, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->sumInterval:I

    int-to-double v3, v3

    const-wide v5, 0x40dd4c0000000000L    # 30000.0

    div-double/2addr v3, v5

    const-wide/high16 v5, 0x4059000000000000L    # 100.0

    mul-double/2addr v3, v5

    double-to-int v3, v3

    iput v3, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->percent:I

    .line 1575
    iget v3, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->percent:I

    if-le v3, v7, :cond_3

    .line 1576
    iput v7, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->percent:I

    .line 1579
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mDataCollectedProgressPercent:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget v5, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->percent:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "%"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1580
    return-void
.end method

.method public deleteDailyData()V
    .locals 3

    .prologue
    .line 2049
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvActivity:Lcom/sec/android/app/shealth/uv/UvActivity;

    invoke-static {v0}, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->selectedDate:Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->deleteDailyDataByTime(J)Z

    .line 2050
    return-void
.end method

.method public firstAnimationClear()V
    .locals 2

    .prologue
    .line 1196
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvActivity:Lcom/sec/android/app/shealth/uv/UvActivity;

    new-instance v1, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$10;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$10;-><init>(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/uv/UvActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1204
    return-void
.end method

.method protected getCalendarActivityClass()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 894
    const-class v0, Lcom/sec/android/app/shealth/uv/calendar/UvCalendarActivity;

    return-object v0
.end method

.method protected getColumnNameForTime()Ljava/lang/String;
    .locals 1

    .prologue
    .line 884
    const-string/jumbo v0, "sample_time"

    return-object v0
.end method

.method protected getContentURI()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 879
    sget-object v0, Lcom/samsung/android/sdk/health/content/ShealthContract$UltravioletRays;->CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method

.method protected getContentView(Landroid/content/Context;)Landroid/view/View;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 295
    sget-object v1, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->TAG:Ljava/lang/String;

    const-string v2, "getContentView"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 296
    iput-object p1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mContext:Landroid/content/Context;

    .line 297
    invoke-static {}, Lcom/sec/android/app/shealth/uv/common/UvDeviceConnector;->getInstance()Lcom/sec/android/app/shealth/uv/common/UvDeviceConnector;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mDeviceConnector:Lcom/sec/android/app/shealth/uv/common/UvDeviceConnector;

    .line 298
    const-string v1, "layout_inflater"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 300
    .local v0, "inflater":Landroid/view/LayoutInflater;
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSystemVibrator:Landroid/os/SystemVibrator;

    if-nez v1, :cond_0

    .line 301
    const-string/jumbo v1, "vibrator"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/SystemVibrator;

    iput-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSystemVibrator:Landroid/os/SystemVibrator;

    .line 304
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string/jumbo v2, "skin_type_checked"

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->isHideSkinSettings:Z

    .line 306
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvPluginUtils:Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;

    if-nez v1, :cond_1

    .line 307
    invoke-static {}, Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;->getInstance()Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvPluginUtils:Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;

    .line 309
    :cond_1
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->hasNoUvSensor:Z

    if-eqz v1, :cond_2

    .line 310
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->registerObserver()V

    .line 312
    :cond_2
    const v1, 0x7f03026b

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->initView(Landroid/view/View;)Landroid/view/View;

    move-result-object v1

    return-object v1
.end method

.method protected getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;
    .locals 1

    .prologue
    .line 899
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v0

    return-object v0
.end method

.method protected getDeviceConnectionCheckListener(Landroid/view/View;)Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceConnectionCheckListener;
    .locals 1
    .param p1, "deviceConnectedStatusView"    # Landroid/view/View;

    .prologue
    .line 693
    new-instance v0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$DeviceConnChkListner;

    invoke-direct {v0, p1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$DeviceConnChkListner;-><init>(Landroid/view/View;)V

    return-object v0
.end method

.method public getSafetyTime(I)J
    .locals 9
    .param p1, "score"    # I

    .prologue
    .line 2361
    const-wide/16 v0, 0x0

    .line 2362
    .local v0, "safetyTime":J
    const/4 v5, 0x3

    .line 2363
    .local v5, "skinType":I
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/sec/android/app/shealth/uv/common/SkinDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/uv/common/SkinDatabaseHelper;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/uv/common/SkinDatabaseHelper;->getData()Lcom/sec/android/app/shealth/uv/data/SkinData;

    move-result-object v4

    .line 2365
    .local v4, "skinData":Lcom/sec/android/app/shealth/uv/data/SkinData;
    if-nez v4, :cond_0

    move-wide v2, v0

    .line 2377
    .end local v0    # "safetyTime":J
    .local v2, "safetyTime":J
    :goto_0
    return-wide v2

    .line 2368
    .end local v2    # "safetyTime":J
    .restart local v0    # "safetyTime":J
    :cond_0
    invoke-virtual {v4}, Lcom/sec/android/app/shealth/uv/data/SkinData;->getSkinType()I

    move-result v5

    .line 2370
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->spfCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v6}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v6

    if-eqz v6, :cond_1

    iget v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->progressSPF:I

    if-eqz v6, :cond_1

    .line 2371
    iget v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->progressSPF:I

    sget-object v7, Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;->SkinUV:[[I

    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->getUVArrayindex(I)I

    move-result v8

    aget-object v7, v7, v8

    invoke-direct {p0, v5}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->getSkinTypeArrayindex(I)I

    move-result v8

    aget v7, v7, v8

    mul-int/2addr v6, v7

    int-to-long v0, v6

    :goto_1
    move-wide v2, v0

    .line 2377
    .end local v0    # "safetyTime":J
    .restart local v2    # "safetyTime":J
    goto :goto_0

    .line 2375
    .end local v2    # "safetyTime":J
    .restart local v0    # "safetyTime":J
    :cond_1
    sget-object v6, Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;->SkinUV:[[I

    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->getUVArrayindex(I)I

    move-result v7

    aget-object v6, v6, v7

    invoke-direct {p0, v5}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->getSkinTypeArrayindex(I)I

    move-result v7

    aget v6, v6, v7

    int-to-long v0, v6

    goto :goto_1
.end method

.method public getStateFinished()Lcom/sec/android/app/shealth/uv/state/UvState;
    .locals 1

    .prologue
    .line 417
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mStateFinished:Lcom/sec/android/app/shealth/uv/state/UvState;

    return-object v0
.end method

.method public getStateMeasureFailed()Lcom/sec/android/app/shealth/uv/state/UvState;
    .locals 1

    .prologue
    .line 413
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mStateMeasureFailed:Lcom/sec/android/app/shealth/uv/state/UvState;

    return-object v0
.end method

.method public getStateMeasureWarning()Lcom/sec/android/app/shealth/uv/state/UvState;
    .locals 1

    .prologue
    .line 409
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mStateMeasureWarning:Lcom/sec/android/app/shealth/uv/state/UvState;

    return-object v0
.end method

.method public getStateMeasuring()Lcom/sec/android/app/shealth/uv/state/UvState;
    .locals 1

    .prologue
    .line 405
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mStateMeasuring:Lcom/sec/android/app/shealth/uv/state/UvState;

    return-object v0
.end method

.method public getStateReady()Lcom/sec/android/app/shealth/uv/state/UvState;
    .locals 1

    .prologue
    .line 401
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mStateReady:Lcom/sec/android/app/shealth/uv/state/UvState;

    return-object v0
.end method

.method public initUvSensorAvaialability(Z)V
    .locals 0
    .param p1, "hasNoUVSensor"    # Z

    .prologue
    .line 3055
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->hasNoUvSensor:Z

    .line 3056
    return-void
.end method

.method public initUvState()V
    .locals 2

    .prologue
    .line 372
    new-instance v0, Lcom/sec/android/app/shealth/uv/state/UvStateReady;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/uv/state/UvStateReady;-><init>(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mStateReady:Lcom/sec/android/app/shealth/uv/state/UvState;

    .line 373
    new-instance v0, Lcom/sec/android/app/shealth/uv/state/UvStateMeasuring;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/uv/state/UvStateMeasuring;-><init>(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mStateMeasuring:Lcom/sec/android/app/shealth/uv/state/UvState;

    .line 374
    new-instance v0, Lcom/sec/android/app/shealth/uv/state/UvStateMeasureWarning;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/uv/state/UvStateMeasureWarning;-><init>(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mStateMeasureWarning:Lcom/sec/android/app/shealth/uv/state/UvState;

    .line 375
    new-instance v0, Lcom/sec/android/app/shealth/uv/state/UvStateMeasureFailed;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/uv/state/UvStateMeasureFailed;-><init>(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mStateMeasureFailed:Lcom/sec/android/app/shealth/uv/state/UvState;

    .line 376
    new-instance v0, Lcom/sec/android/app/shealth/uv/state/UvStateFinished;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/uv/state/UvStateFinished;-><init>(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mStateFinished:Lcom/sec/android/app/shealth/uv/state/UvState;

    .line 378
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvPluginUtils:Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;

    if-nez v0, :cond_0

    .line 379
    invoke-static {}, Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;->getInstance()Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvPluginUtils:Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;

    .line 382
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->hasNoUvSensor:Z

    if-eqz v0, :cond_2

    .line 383
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mStateFinished:Lcom/sec/android/app/shealth/uv/state/UvState;

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mState:Lcom/sec/android/app/shealth/uv/state/UvState;

    .line 384
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->setUiWithNoSensor()V

    .line 393
    :cond_1
    :goto_0
    return-void

    .line 386
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mStateReady:Lcom/sec/android/app/shealth/uv/state/UvState;

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mState:Lcom/sec/android/app/shealth/uv/state/UvState;

    .line 387
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSystemVibrator:Landroid/os/SystemVibrator;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_1

    .line 388
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "vibrator"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/SystemVibrator;

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSystemVibrator:Landroid/os/SystemVibrator;

    goto :goto_0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 286
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->onAttach(Landroid/app/Activity;)V

    .line 287
    sget-object v0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onAttach"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 288
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvActivity:Lcom/sec/android/app/shealth/uv/UvActivity;

    if-nez v0, :cond_0

    .line 289
    check-cast p1, Lcom/sec/android/app/shealth/uv/UvActivity;

    .end local p1    # "activity":Landroid/app/Activity;
    iput-object p1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvActivity:Lcom/sec/android/app/shealth/uv/UvActivity;

    .line 291
    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 2201
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 2202
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2203
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setVisibility(I)V

    .line 2205
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mIsOnConfigChanged:Z

    .line 2206
    return-void
.end method

.method public onDataReceived(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$UvRay;)V
    .locals 8
    .param p1, "uv"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$UvRay;

    .prologue
    const/4 v6, -0x1

    .line 2145
    iget v0, p1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$UvRay;->index:I

    .line 2146
    .local v0, "index":I
    sget-object v4, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "onDataReceived"

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 2147
    iget-boolean v4, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mIsDummyMode:Z

    if-nez v4, :cond_0

    .line 2148
    iget-object v4, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->dataCollection:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2150
    :cond_0
    iget-boolean v4, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mStopSensor:Z

    if-nez v4, :cond_2

    .line 2151
    iget-object v4, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mMeasuringTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v4}, Landroid/os/CountDownTimer;->cancel()V

    .line 2152
    if-ne v0, v6, :cond_1

    .line 2153
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mStopSensor:Z

    .line 2154
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->stopSensor()V

    .line 2155
    iget-object v4, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mState:Lcom/sec/android/app/shealth/uv/state/UvState;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/uv/state/UvState;->reStartStates()V

    .line 2156
    iget-object v4, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mHandler:Landroid/os/Handler;

    invoke-virtual {v4, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 2177
    :goto_0
    return-void

    .line 2159
    :cond_1
    const/16 v3, 0x1770

    .line 2160
    .local v3, "min_interval":I
    const/16 v2, 0x1f40

    .line 2161
    .local v2, "max_interval":I
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v4

    const/16 v6, 0x7d1

    int-to-double v6, v6

    mul-double/2addr v4, v6

    double-to-int v4, v4

    add-int v1, v4, v3

    .line 2164
    .local v1, "interval":I
    iget-object v4, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mState:Lcom/sec/android/app/shealth/uv/state/UvState;

    invoke-virtual {v4, v0, v1}, Lcom/sec/android/app/shealth/uv/state/UvState;->updateNextState(II)V

    goto :goto_0

    .line 2172
    .end local v1    # "interval":I
    .end local v2    # "max_interval":I
    .end local v3    # "min_interval":I
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->stopSensor()V

    .line 2173
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->clearDataCollection()V

    .line 2174
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->clearintervalCollection()V

    .line 2175
    iget-object v4, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mMeasuringTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v4}, Landroid/os/CountDownTimer;->cancel()V

    goto :goto_0
.end method

.method public onDataReceivedArray([Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;)V
    .locals 3
    .param p1, "uv"    # [Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;

    .prologue
    .line 2181
    sget-object v1, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "onDataReceivedArray"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 2183
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mIsDummyMode:Z

    if-nez v1, :cond_0

    .line 2184
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_0

    .line 2185
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->dataCollection:Ljava/util/ArrayList;

    aget-object v1, p1, v0

    check-cast v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$UvRay;

    iget v1, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$UvRay;->index:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2184
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2188
    .end local v0    # "i":I
    :cond_0
    const/4 v1, 0x0

    aget-object v1, p1, v1

    check-cast v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$UvRay;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->onDataReceived(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$UvRay;)V

    .line 2189
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 855
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvActivity:Lcom/sec/android/app/shealth/uv/UvActivity;

    if-eqz v0, :cond_0

    .line 856
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvActivity:Lcom/sec/android/app/shealth/uv/UvActivity;

    .line 858
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onDestroy"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 859
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->onDestroy()V

    .line 860
    return-void
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 840
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->hasNoUvSensor:Z

    if-eqz v0, :cond_0

    .line 841
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->unregisterObserver()V

    .line 843
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvPluginUtils:Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;

    .line 844
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mDeviceFinder:Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;->clearListener()V

    .line 845
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mDeviceFinder:Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;->release()V

    .line 846
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->clearInstance()V

    .line 847
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvHandler:Landroid/os/Handler;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mMeasuringRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_1

    .line 848
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mMeasuringRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 850
    :cond_1
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->onDestroyView()V

    .line 851
    return-void
.end method

.method public onPause()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 820
    sget-object v0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onPause"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 821
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->stopSensor()V

    .line 822
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->clearDataCollection()V

    .line 823
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->clearintervalCollection()V

    .line 824
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mMeasuringTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 825
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mDeviceConnector:Lcom/sec/android/app/shealth/uv/common/UvDeviceConnector;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/uv/common/UvDeviceConnector;->stopCountDownTimer()V

    .line 826
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mState:Lcom/sec/android/app/shealth/uv/state/UvState;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/uv/state/UvState;->terminateState()V

    .line 827
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->firstAnimationClear()V

    .line 828
    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mScaleAnimator:Landroid/animation/ValueAnimator;

    .line 829
    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mScaleAnimatorFinished:Landroid/animation/ValueAnimator;

    .line 830
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->measuringCircleAnimationStop(Z)V

    .line 831
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->cancelSoundVibration()V

    .line 832
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mMeasuringRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 833
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mMeasuringRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 835
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->onPause()V

    .line 836
    return-void
.end method

.method public onResume()V
    .locals 6

    .prologue
    const/16 v2, 0x8

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 753
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->onResume()V

    .line 754
    sget-object v0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onResume"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 756
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->needsLocationPopup:Z

    if-eqz v0, :cond_0

    .line 757
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->needsLocationPopup:Z

    .line 758
    invoke-direct {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->displayLocationDialogIfNeeded()V

    .line 761
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->getCheckedStatus()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 762
    iput-boolean v4, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->checkSPF:Z

    .line 763
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSpfIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 764
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->spfTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 765
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->spfCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 772
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->spfCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->updateLayoutParams(Z)V

    .line 773
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->spfCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->updateSecondIconParams(Z)V

    .line 775
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mDeviceFinder:Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;->updateDeviceConnectionStatus()V

    .line 776
    invoke-direct {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->initAoudioFile()V

    .line 777
    invoke-direct {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->updateText()V

    .line 778
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mState:Lcom/sec/android/app/shealth/uv/state/UvState;

    if-nez v0, :cond_1

    .line 779
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->initUvState()V

    .line 782
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->wasShowInformationDialog:Z

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvActivity:Lcom/sec/android/app/shealth/uv/UvActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/uv/UvActivity;->getCallFromHome()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvActivity:Lcom/sec/android/app/shealth/uv/UvActivity;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "uv_warning_checked"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_5

    .line 786
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvActivity:Lcom/sec/android/app/shealth/uv/UvActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/uv/UvActivity;->callShowInformationDialog()V

    .line 787
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mState:Lcom/sec/android/app/shealth/uv/state/UvState;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/uv/state/UvState;->reStartStates()V

    .line 788
    iput-boolean v4, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->wasShowInformationDialog:Z

    .line 812
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mState:Lcom/sec/android/app/shealth/uv/state/UvState;

    instance-of v0, v0, Lcom/sec/android/app/shealth/uv/state/UvStateMeasureFailed;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mDialogMovie:Landroid/widget/ImageView;

    if-eqz v0, :cond_3

    .line 813
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mDialogMovie:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->images:[I

    move-object v0, p0

    move v5, v3

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->animate(Landroid/widget/ImageView;[IIZZ)V

    .line 816
    :cond_3
    return-void

    .line 767
    :cond_4
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->checkSPF:Z

    .line 768
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSpfIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 769
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->spfTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 770
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->spfCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0

    .line 789
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->sHealthAlertDialogbuild:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->sHealthAlertDialogbuild:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->sHealthAlertDialogbuild:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isVisible()Z

    move-result v0

    if-nez v0, :cond_7

    .line 792
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mState:Lcom/sec/android/app/shealth/uv/state/UvState;

    instance-of v0, v0, Lcom/sec/android/app/shealth/uv/state/UvStateMeasureFailed;

    if-nez v0, :cond_2

    .line 793
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mState:Lcom/sec/android/app/shealth/uv/state/UvState;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/uv/state/UvState;->reStartStates()V

    goto :goto_1

    .line 795
    :cond_7
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mIsOnConfigChanged:Z

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->sHealthAlertDialogbuild:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->sHealthAlertDialogbuild:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 797
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->sHealthAlertDialogbuild:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    new-instance v1, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$6;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$6;-><init>(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->setOnDismissListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;)V

    .line 806
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->sHealthAlertDialogbuild:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->dismiss()V

    .line 807
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mIsOnConfigChanged:Z

    goto :goto_1

    .line 808
    :cond_8
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->sHealthAlertDialogbuild:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mDialogMovie:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    .line 809
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mDialogMovie:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->images:[I

    move-object v0, p0

    move v5, v3

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->animate(Landroid/widget/ImageView;[IIZZ)V

    goto :goto_1
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 889
    const-string v0, "Date"

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->selectedDate:Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 890
    return-void
.end method

.method protected onSytemDateChanged()V
    .locals 6

    .prologue
    .line 864
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->selectedDate:Ljava/util/Date;

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/uv/utils/DateFormatUtil;->getStartDayToMillis(J)J

    move-result-wide v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/uv/utils/DateFormatUtil;->getStartDayToMillis(J)J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 866
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 867
    .local v0, "sytemChangedTime":J
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2, v0, v1}, Ljava/util/Date;-><init>(J)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->selectedDate:Ljava/util/Date;

    .line 868
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v2

    new-instance v3, Ljava/util/Date;

    invoke-direct {v3, v0, v1}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setSelecteddate(Ljava/util/Date;)V

    .line 869
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->updateUvDataView(Z)V

    .line 871
    .end local v0    # "sytemChangedTime":J
    :cond_0
    return-void
.end method

.method public onTick(J)V
    .locals 0
    .param p1, "millisUntilFinished"    # J

    .prologue
    .line 2216
    return-void
.end method

.method public onTimeout()V
    .locals 3

    .prologue
    .line 2210
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mState:Lcom/sec/android/app/shealth/uv/state/UvState;

    const/4 v1, -0x6

    const/16 v2, -0x7d1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/uv/state/UvState;->updateNextState(II)V

    .line 2212
    return-void
.end method

.method public registerObserver()V
    .locals 4

    .prologue
    .line 272
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$UltravioletRays;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 277
    return-void
.end method

.method public resetViews()V
    .locals 2

    .prologue
    .line 3060
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIconNoSkin:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 3061
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIconNoSkin:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 3062
    :cond_0
    return-void
.end method

.method public restartCurrentState(I)V
    .locals 1
    .param p1, "animation"    # I

    .prologue
    .line 918
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mState:Lcom/sec/android/app/shealth/uv/state/UvState;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/uv/state/UvState;->reStartStates()V

    .line 919
    return-void
.end method

.method public setMeasuringEndUI()V
    .locals 14

    .prologue
    const/4 v13, 0x4

    const/4 v12, 0x1

    const/16 v11, 0x8

    const/4 v10, 0x0

    .line 1716
    sget-object v6, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v7, "setMeasuringEndUI"

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1717
    iput-boolean v12, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->ismFinishedState:Z

    .line 1718
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondMeasuringProgressView:Landroid/widget/ImageView;

    invoke-virtual {v6, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1719
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSpfContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v6, v11}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1721
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mDeviceConnector:Lcom/sec/android/app/shealth/uv/common/UvDeviceConnector;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/uv/common/UvDeviceConnector;->stopCountDownTimer()V

    .line 1722
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mMeasuringTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v6}, Landroid/os/CountDownTimer;->cancel()V

    .line 1724
    iput-boolean v12, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mIsGreen:Z

    .line 1725
    iget-boolean v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mIsGreen:Z

    invoke-direct {p0, v6}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->scaleAnimationFinished(Z)V

    .line 1727
    invoke-direct {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->calculateUvData()Lcom/sec/android/app/shealth/uv/data/UvStateType;

    move-result-object v3

    .line 1728
    .local v3, "stateType":Lcom/sec/android/app/shealth/uv/data/UvStateType;
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->spfFinishedsunprotectionText:Landroid/widget/TextView;

    iget-object v7, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f090daf

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1730
    const/4 v0, 0x0

    .line 1732
    .local v0, "noDataFromAccesory":Z
    invoke-virtual {v3}, Lcom/sec/android/app/shealth/uv/data/UvStateType;->getScore()D

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmpl-double v6, v6, v8

    if-ltz v6, :cond_9

    .line 1733
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mContext:Landroid/content/Context;

    const-string v7, "com.sec.android.app.shealth.uv"

    const-string v8, "UV02"

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 1735
    iget-boolean v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->isHideSkinSettings:Z

    if-nez v6, :cond_0

    .line 1736
    invoke-virtual {v3}, Lcom/sec/android/app/shealth/uv/data/UvStateType;->getScore()D

    move-result-wide v6

    double-to-int v6, v6

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->getSafetyTime(I)J

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Lcom/sec/android/app/shealth/uv/data/UvStateType;->setSafetyTime(J)V

    .line 1738
    invoke-virtual {v3}, Lcom/sec/android/app/shealth/uv/data/UvStateType;->getScore()D

    move-result-wide v6

    double-to-int v6, v6

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->getSafetyTime(I)J

    move-result-wide v6

    invoke-virtual {p0, v6, v7}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->setSafetyTime(J)V

    .line 1740
    :cond_0
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->spfCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v6}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v6

    if-eqz v6, :cond_7

    .line 1741
    iget v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->progressSPF:I

    invoke-virtual {v3, v6}, Lcom/sec/android/app/shealth/uv/data/UvStateType;->setSPFvalue(I)V

    .line 1744
    :goto_0
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->spfFinishedsunprotection:Landroid/widget/LinearLayout;

    invoke-virtual {v6, v10}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1745
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvActivity:Lcom/sec/android/app/shealth/uv/UvActivity;

    invoke-static {v6}, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;

    move-result-object v6

    iget-boolean v7, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->isHideSkinSettings:Z

    invoke-virtual {v6, v3, v7}, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->insertData(Lcom/sec/android/app/shealth/uv/data/UvStateType;Z)V

    .line 1751
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->clearDataCollection()V

    .line 1752
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->clearintervalCollection()V

    .line 1753
    iget-boolean v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->isHideSkinSettings:Z

    if-nez v6, :cond_8

    .line 1754
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->spfFinishedsunprotection:Landroid/widget/LinearLayout;

    invoke-virtual {v6, v10}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1782
    :cond_1
    :goto_1
    if-eqz v0, :cond_d

    .line 1783
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mStateBar:Lcom/sec/android/app/shealth/uv/custom/UvStateSummaryBar;

    invoke-virtual {v6, v11}, Lcom/sec/android/app/shealth/uv/custom/UvStateSummaryBar;->setVisibility(I)V

    .line 1784
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mStateChineseBar:Lcom/sec/android/app/shealth/uv/custom/UvStateSummaryChineseBar;

    invoke-virtual {v6, v11}, Lcom/sec/android/app/shealth/uv/custom/UvStateSummaryChineseBar;->setVisibility(I)V

    .line 1795
    :goto_2
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIcon:Landroid/widget/ImageView;

    invoke-virtual {v6}, Landroid/widget/ImageView;->clearAnimation()V

    .line 1797
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvActivity:Lcom/sec/android/app/shealth/uv/UvActivity;

    if-eqz v6, :cond_2

    .line 1798
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvActivity:Lcom/sec/android/app/shealth/uv/UvActivity;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/uv/UvActivity;->getWindow()Landroid/view/Window;

    move-result-object v6

    const/16 v7, 0x80

    invoke-virtual {v6, v7}, Landroid/view/Window;->clearFlags(I)V

    .line 1801
    :cond_2
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIcon:Landroid/widget/ImageView;

    invoke-virtual {v6, v13}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1802
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIconBigLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v6, v11}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1804
    invoke-direct {p0, v12}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->measuringCircleAnimationStop(Z)V

    .line 1806
    if-eqz v0, :cond_f

    .line 1807
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mFirstStateLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v6, v11}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1811
    :goto_3
    iget-boolean v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->hasNoUvSensor:Z

    if-eqz v6, :cond_10

    .line 1812
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mFirstStatus:Landroid/widget/TextView;

    invoke-virtual {v6, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1819
    :goto_4
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mFirstMessage:Landroid/widget/TextView;

    invoke-virtual {v6, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1820
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mDataCollectedProgressPercent:Landroid/widget/TextView;

    invoke-virtual {v6, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1822
    iget-boolean v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->hasNoUvSensor:Z

    if-nez v6, :cond_3

    .line 1823
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondStart:Landroid/widget/LinearLayout;

    invoke-virtual {v6, v10}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1824
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mThirdDiscard:Landroid/widget/LinearLayout;

    invoke-virtual {v6, v10}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1826
    :cond_3
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondDiscardText:Landroid/widget/TextView;

    iget-object v7, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f090f4d

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1828
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondStartText:Landroid/widget/TextView;

    iget-object v7, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f090050

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1830
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->divider:Landroid/view/View;

    invoke-virtual {v6, v10}, Landroid/view/View;->setVisibility(I)V

    .line 1831
    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->setUvLevelComment(Lcom/sec/android/app/shealth/uv/data/UvStateType;)V

    .line 1832
    invoke-virtual {p0, v10}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->updateUvDataView(Z)V

    .line 1833
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->UvParentView:Landroid/widget/LinearLayout;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mFirstStatus:Landroid/widget/TextView;

    invoke-virtual {v8}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v8

    invoke-interface {v8}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mThirdComment:Landroid/widget/TextView;

    invoke-virtual {v8}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v8

    invoke-interface {v8}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1835
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondStart:Landroid/widget/LinearLayout;

    invoke-virtual {v6, v12}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 1836
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mThirdDiscard:Landroid/widget/LinearLayout;

    invoke-virtual {v6, v12}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 1837
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->refreshFragmentFocusables()V

    .line 1842
    iget-boolean v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->fromMeasuringState:Z

    if-eqz v6, :cond_6

    .line 1843
    iput-boolean v10, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->fromMeasuringState:Z

    .line 1844
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSystemVibrator:Landroid/os/SystemVibrator;

    if-eqz v6, :cond_4

    .line 1845
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSystemVibrator:Landroid/os/SystemVibrator;

    invoke-virtual {v6}, Landroid/os/SystemVibrator;->cancel()V

    .line 1846
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mMeasuringHapticThread:Lcom/sec/android/app/shealth/uv/UvSummaryFragment$measuringHapticThread;

    if-eqz v6, :cond_4

    .line 1847
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mMeasuringHapticThread:Lcom/sec/android/app/shealth/uv/UvSummaryFragment$measuringHapticThread;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$measuringHapticThread;->stopThread()V

    .line 1848
    const/4 v6, 0x0

    iput-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mMeasuringHapticThread:Lcom/sec/android/app/shealth/uv/UvSummaryFragment$measuringHapticThread;

    .line 1851
    :cond_4
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->pool:Landroid/media/SoundPool;

    if-eqz v6, :cond_5

    .line 1852
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->pool:Landroid/media/SoundPool;

    invoke-virtual {v6}, Landroid/media/SoundPool;->autoPause()V

    .line 1854
    :cond_5
    sget-object v6, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->effectAudio:[I

    aget v6, v6, v13

    invoke-direct {p0, v6}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->playSound(I)V

    .line 1856
    :cond_6
    const/4 v6, -0x1

    iput v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvIndex:I

    .line 1857
    invoke-direct {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->updateFinishLayoutParams()V

    .line 1858
    return-void

    .line 1743
    :cond_7
    invoke-virtual {v3, v10}, Lcom/sec/android/app/shealth/uv/data/UvStateType;->setSPFvalue(I)V

    goto/16 :goto_0

    .line 1756
    :cond_8
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->spfFinishedsunprotection:Landroid/widget/LinearLayout;

    invoke-virtual {v6, v13}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_1

    .line 1759
    :cond_9
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvActivity:Lcom/sec/android/app/shealth/uv/UvActivity;

    invoke-static {v6}, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->selectedDate:Ljava/util/Date;

    invoke-virtual {v7}, Ljava/util/Date;->getTime()J

    move-result-wide v7

    invoke-virtual {v6, v7, v8}, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->getLastTwoDataByTime(J)Ljava/util/ArrayList;

    move-result-object v4

    .line 1760
    .local v4, "uvLastDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/uv/data/UvData;>;"
    if-eqz v4, :cond_c

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lez v6, :cond_c

    .line 1761
    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/shealth/uv/data/UvData;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/uv/data/UvData;->getScore()D

    move-result-wide v6

    double-to-int v5, v6

    .line 1762
    .local v5, "uvLastScore":I
    int-to-double v6, v5

    invoke-virtual {v3, v6, v7}, Lcom/sec/android/app/shealth/uv/data/UvStateType;->setScore(D)V

    .line 1763
    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/shealth/uv/data/UvData;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/uv/data/UvData;->getRecomendedTime()J

    move-result-wide v1

    .line 1765
    .local v1, "safeTime":J
    invoke-virtual {v3, v1, v2}, Lcom/sec/android/app/shealth/uv/data/UvStateType;->setSafetyTime(J)V

    .line 1766
    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->setSafetyTime(J)V

    .line 1767
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSafeHr:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getVisibility()I

    move-result v6

    if-eqz v6, :cond_a

    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSafeMin:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getVisibility()I

    move-result v6

    if-nez v6, :cond_b

    .line 1769
    :cond_a
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->spfFinishedsunprotection:Landroid/widget/LinearLayout;

    invoke-virtual {v6, v10}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_1

    .line 1771
    :cond_b
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->spfFinishedsunprotection:Landroid/widget/LinearLayout;

    invoke-virtual {v6, v13}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_1

    .line 1774
    .end local v1    # "safeTime":J
    .end local v5    # "uvLastScore":I
    :cond_c
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->spfFinishedsunprotection:Landroid/widget/LinearLayout;

    invoke-virtual {v6, v13}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1775
    iget-boolean v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->hasNoUvSensor:Z

    if-eqz v6, :cond_1

    .line 1776
    const-wide/high16 v6, -0x4000000000000000L    # -2.0

    invoke-virtual {v3, v6, v7}, Lcom/sec/android/app/shealth/uv/data/UvStateType;->setScore(D)V

    .line 1777
    const/4 v0, 0x1

    goto/16 :goto_1

    .line 1786
    .end local v4    # "uvLastDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/uv/data/UvData;>;"
    :cond_d
    invoke-static {}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->isChinaModel()Z

    move-result v6

    if-nez v6, :cond_e

    .line 1787
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mStateBar:Lcom/sec/android/app/shealth/uv/custom/UvStateSummaryBar;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/uv/data/UvStateType;->getScore()D

    move-result-wide v7

    iget-boolean v9, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->hasNoUvSensor:Z

    invoke-virtual {v6, v7, v8, v9}, Lcom/sec/android/app/shealth/uv/custom/UvStateSummaryBar;->moveToPolygon(DZ)V

    .line 1788
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mStateBar:Lcom/sec/android/app/shealth/uv/custom/UvStateSummaryBar;

    invoke-virtual {v6, v10}, Lcom/sec/android/app/shealth/uv/custom/UvStateSummaryBar;->setVisibility(I)V

    goto/16 :goto_2

    .line 1790
    :cond_e
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mStateChineseBar:Lcom/sec/android/app/shealth/uv/custom/UvStateSummaryChineseBar;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/uv/data/UvStateType;->getScore()D

    move-result-wide v7

    iget-boolean v9, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->hasNoUvSensor:Z

    invoke-virtual {v6, v7, v8, v9}, Lcom/sec/android/app/shealth/uv/custom/UvStateSummaryChineseBar;->moveToPolygon(DZ)V

    .line 1792
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mStateChineseBar:Lcom/sec/android/app/shealth/uv/custom/UvStateSummaryChineseBar;

    invoke-virtual {v6, v10}, Lcom/sec/android/app/shealth/uv/custom/UvStateSummaryChineseBar;->setVisibility(I)V

    goto/16 :goto_2

    .line 1809
    :cond_f
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mFirstStateLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v6, v10}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_3

    .line 1814
    :cond_10
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mFirstStatus:Landroid/widget/TextView;

    const v7, 0x7f090c06

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(I)V

    .line 1815
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mFirstStatus:Landroid/widget/TextView;

    iget-object v7, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f070253

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_4
.end method

.method public setMeasuringFailUI(I)V
    .locals 4
    .param p1, "errorType"    # I

    .prologue
    const/16 v3, 0x8

    .line 1488
    sget-object v0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setMeasuringFailUI : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1489
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvActivity:Lcom/sec/android/app/shealth/uv/UvActivity;

    if-eqz v0, :cond_0

    .line 1490
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvActivity:Lcom/sec/android/app/shealth/uv/UvActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/uv/UvActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 1494
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->dataCollection:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 1495
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->clearDataCollection()V

    .line 1496
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->clearintervalCollection()V

    .line 1499
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIconBigLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1500
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterTextFinish:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1501
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mDataCollectedProgressPercent:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1508
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->showFailurePopup()V

    .line 1509
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIconBigLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1510
    return-void
.end method

.method public setMeasuringUI(I)V
    .locals 6
    .param p1, "isShowAnimation"    # I

    .prologue
    const v5, 0x106000c

    const/4 v4, 0x0

    const/16 v3, 0x8

    .line 1408
    sget-object v0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setMeasuringUI : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1409
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondMeasuringProgressView:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1410
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvActivity:Lcom/sec/android/app/shealth/uv/UvActivity;

    if-eqz v0, :cond_0

    .line 1411
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvActivity:Lcom/sec/android/app/shealth/uv/UvActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/uv/UvActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 1414
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1415
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mNoDataDescription:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1416
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mFinishTopLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1417
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mWearableSummaryNoData:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1418
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIconBig:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 1420
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mIsGreen:Z

    .line 1421
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->isHideSkinSettings:Z

    if-nez v0, :cond_3

    .line 1422
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mIsGreen:Z

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->scaleAnimationStarter(Z)V

    .line 1427
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mCircleAnimationSet:Landroid/view/animation/AnimationSet;

    if-eqz v0, :cond_1

    .line 1428
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mCircleAnimationSet:Landroid/view/animation/AnimationSet;

    invoke-virtual {v0}, Landroid/view/animation/AnimationSet;->cancel()V

    .line 1429
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mCircleAnimationSet:Landroid/view/animation/AnimationSet;

    .line 1431
    :cond_1
    invoke-direct {p0, v4}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->measuringCircleAnimationStart(Z)V

    .line 1432
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIconBig:Landroid/widget/ImageView;

    const v1, 0x7f0201df

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1435
    if-nez p1, :cond_4

    .line 1436
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mFirstLayout:Landroid/widget/FrameLayout;

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->firstAnimation(Landroid/view/View;)V

    .line 1437
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mFirstReadyLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1438
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mFirstStateLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1453
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->UvParentView:Landroid/widget/LinearLayout;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mFirstStatus:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mFirstMessage:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1457
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondStart:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1459
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->isHideSkinSettings:Z

    if-nez v0, :cond_2

    .line 1460
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->spfchecklay:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1461
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->spfSeekText:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1462
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->spfFinishedsunprotection:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1465
    :cond_2
    return-void

    .line 1424
    :cond_3
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mIsGreen:Z

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->scaleAnimatorStarterNoSKin(Z)V

    goto :goto_0

    .line 1440
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mFirstLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v4}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1441
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mFirstReadyLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1442
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mFirstStateLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1443
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mFirstMessage:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1445
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mFirstStatus:Landroid/widget/TextView;

    const v1, 0x7f090c04

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 1446
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mFirstStatus:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1448
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mFirstMessage:Landroid/widget/TextView;

    const v1, 0x7f090d9b

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 1449
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mFirstMessage:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_1
.end method

.method public setMeasuringWarningUI(I)V
    .locals 4
    .param p1, "errorType"    # I

    .prologue
    const/4 v3, 0x0

    .line 1513
    sget-object v0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setMeasuringWarningUI : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1514
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mDeviceConnector:Lcom/sec/android/app/shealth/uv/common/UvDeviceConnector;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/uv/common/UvDeviceConnector;->stopCountDownTimer()V

    .line 1515
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mIsGreen:Z

    .line 1516
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mIsGreen:Z

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->scaleAnimationStarter(Z)V

    .line 1518
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mCircleAnimationSet:Landroid/view/animation/AnimationSet;

    if-nez v0, :cond_0

    .line 1519
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->measuringCircleAnimationStart(Z)V

    .line 1524
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mFirstStatus:Landroid/widget/TextView;

    const v1, 0x7f090c04

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 1525
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mFirstMessage:Landroid/widget/TextView;

    const v1, 0x7f090d9b

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 1526
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mFirstMessage:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070252

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1528
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIcon:Landroid/widget/ImageView;

    const v1, 0x7f0201e0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1529
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIconBig:Landroid/widget/ImageView;

    const v1, 0x7f0201de

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1530
    return-void

    .line 1521
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondMeasuringProgressView:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1522
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondMeasuringProgressView1:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method protected setNextAndPrevDates()V
    .locals 0

    .prologue
    .line 875
    return-void
.end method

.method public setPreviousData(I)V
    .locals 9
    .param p1, "uvScore"    # I

    .prologue
    const v8, 0x7f0201dc

    const v7, 0x7f0201db

    const v6, 0x7f0201da

    const v5, 0x7f0201d9

    const/4 v4, 0x2

    .line 2219
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2220
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->isChinaModel()Z

    move-result v2

    if-eqz v2, :cond_1

    const v2, 0x7f091158

    :goto_0
    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2224
    .local v1, "indexString":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->isChinaModel()Z

    move-result v2

    if-nez v2, :cond_6

    .line 2225
    if-gt p1, v4, :cond_2

    .line 2226
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090d94

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2227
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mThirdPreviousImage:Landroid/widget/ImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2287
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->isSamsungAccount(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v2, Landroid/os/Build;->TYPE:Ljava/lang/String;

    const-string v3, "eng"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2288
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "( "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " )"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2291
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mThirdPreviousComment:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2292
    return-void

    .line 2220
    .end local v1    # "indexString":Ljava/lang/String;
    :cond_1
    const v2, 0x7f090d94

    goto :goto_0

    .line 2230
    .restart local v1    # "indexString":Ljava/lang/String;
    :cond_2
    const/4 v2, 0x5

    if-gt p1, v2, :cond_3

    .line 2231
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090d95

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2233
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mThirdPreviousImage:Landroid/widget/ImageView;

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 2236
    :cond_3
    const/4 v2, 0x7

    if-gt p1, v2, :cond_4

    .line 2237
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090d96

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2239
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mThirdPreviousImage:Landroid/widget/ImageView;

    invoke-virtual {v2, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 2242
    :cond_4
    const/16 v2, 0xa

    if-gt p1, v2, :cond_5

    .line 2243
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090d97

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2245
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mThirdPreviousImage:Landroid/widget/ImageView;

    invoke-virtual {v2, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 2249
    :cond_5
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090d98

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2251
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mThirdPreviousImage:Landroid/widget/ImageView;

    const v3, 0x7f0201dd

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_1

    .line 2255
    :cond_6
    if-gt p1, v4, :cond_7

    .line 2256
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f091158

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2258
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mThirdPreviousImage:Landroid/widget/ImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_1

    .line 2261
    :cond_7
    const/4 v2, 0x4

    if-gt p1, v2, :cond_8

    .line 2262
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f091159

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2264
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mThirdPreviousImage:Landroid/widget/ImageView;

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_1

    .line 2267
    :cond_8
    const/4 v2, 0x6

    if-gt p1, v2, :cond_9

    .line 2268
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09115a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2270
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mThirdPreviousImage:Landroid/widget/ImageView;

    invoke-virtual {v2, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_1

    .line 2273
    :cond_9
    const/16 v2, 0x9

    if-gt p1, v2, :cond_a

    .line 2274
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09115b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2276
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mThirdPreviousImage:Landroid/widget/ImageView;

    invoke-virtual {v2, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_1

    .line 2280
    :cond_a
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09115c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2282
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mThirdPreviousImage:Landroid/widget/ImageView;

    const v3, 0x7f0201dd

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_1
.end method

.method public setSafetyTime(J)V
    .locals 8
    .param p1, "safetyTime"    # J

    .prologue
    .line 2300
    iget-object v4, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSafeHr:Landroid/widget/TextView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2301
    iget-object v4, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSafeMin:Landroid/widget/TextView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2302
    iget-object v4, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSafeHrVal:Landroid/widget/TextView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2303
    iget-object v4, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSafeMinVal:Landroid/widget/TextView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2305
    const-wide/16 v4, 0x0

    cmp-long v4, p1, v4

    if-nez v4, :cond_1

    .line 2357
    :cond_0
    :goto_0
    return-void

    .line 2308
    :cond_1
    const-wide/16 v0, 0x0

    .line 2310
    .local v0, "hour":J
    const-wide/16 v2, 0x0

    .line 2312
    .local v2, "min":J
    const-wide/16 v4, 0x3c

    cmp-long v4, p1, v4

    if-lez v4, :cond_3

    .line 2314
    const-wide/16 v4, 0x78

    cmp-long v4, p1, v4

    if-gtz v4, :cond_2

    .line 2315
    const-wide/16 v4, 0x3c

    div-long v0, p1, v4

    .line 2316
    const-wide/16 v4, 0x3c

    rem-long v2, p1, v4

    .line 2326
    :goto_1
    iget-object v5, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSafeHr:Landroid/widget/TextView;

    const-wide/16 v6, 0x1

    cmp-long v4, v0, v6

    if-nez v4, :cond_4

    const v4, 0x7f0901db

    :goto_2
    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(I)V

    .line 2328
    iget-object v5, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSafeHr:Landroid/widget/TextView;

    const-wide/16 v6, 0x1

    cmp-long v4, v0, v6

    if-nez v4, :cond_5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f090dc1

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    :goto_3
    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2332
    iget-object v5, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSafeMin:Landroid/widget/TextView;

    const-wide/16 v6, 0x1

    cmp-long v4, v2, v6

    if-nez v4, :cond_6

    const v4, 0x7f0901dd

    :goto_4
    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(I)V

    .line 2333
    iget-object v5, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSafeMin:Landroid/widget/TextView;

    const-wide/16 v6, 0x1

    cmp-long v4, v2, v6

    if-nez v4, :cond_7

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f090dc2

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    :goto_5
    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2337
    const-wide/16 v4, 0x0

    cmp-long v4, v0, v4

    if-lez v4, :cond_8

    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-lez v4, :cond_8

    .line 2338
    iget-object v4, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSafeHrVal:Landroid/widget/TextView;

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2339
    iget-object v4, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSafeHr:Landroid/widget/TextView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2340
    iget-object v4, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSafeHrVal:Landroid/widget/TextView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2341
    iget-object v4, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSafeMinVal:Landroid/widget/TextView;

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2342
    iget-object v4, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSafeMin:Landroid/widget/TextView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2343
    iget-object v4, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSafeMinVal:Landroid/widget/TextView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 2318
    :cond_2
    const-wide/16 v0, 0x2

    .line 2319
    const-wide/16 v2, 0x0

    goto/16 :goto_1

    .line 2322
    :cond_3
    const-wide/16 v0, 0x0

    .line 2323
    move-wide v2, p1

    goto/16 :goto_1

    .line 2326
    :cond_4
    const v4, 0x7f090d5b

    goto/16 :goto_2

    .line 2328
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f0900e8

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_3

    .line 2332
    :cond_6
    const v4, 0x7f0900eb

    goto :goto_4

    .line 2333
    :cond_7
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f0900ec

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_5

    .line 2344
    :cond_8
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-lez v4, :cond_9

    const-wide/16 v4, 0x0

    cmp-long v4, v0, v4

    if-gtz v4, :cond_9

    .line 2345
    iget-object v4, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSafeMinVal:Landroid/widget/TextView;

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2346
    iget-object v4, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSafeMin:Landroid/widget/TextView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2347
    iget-object v4, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSafeMinVal:Landroid/widget/TextView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2348
    iget-object v4, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSafeHr:Landroid/widget/TextView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2349
    iget-object v4, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSafeHrVal:Landroid/widget/TextView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 2350
    :cond_9
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-gtz v4, :cond_0

    const-wide/16 v4, 0x0

    cmp-long v4, v0, v4

    if-lez v4, :cond_0

    .line 2351
    iget-object v4, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSafeHrVal:Landroid/widget/TextView;

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2352
    iget-object v4, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSafeHr:Landroid/widget/TextView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2353
    iget-object v4, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSafeHrVal:Landroid/widget/TextView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2354
    iget-object v4, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSafeMin:Landroid/widget/TextView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2355
    iget-object v4, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSafeMinVal:Landroid/widget/TextView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method public setState(Lcom/sec/android/app/shealth/uv/state/UvState;)V
    .locals 0
    .param p1, "nextState"    # Lcom/sec/android/app/shealth/uv/state/UvState;

    .prologue
    .line 396
    iput-object p1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mState:Lcom/sec/android/app/shealth/uv/state/UvState;

    .line 398
    return-void
.end method

.method public setUVData(I)V
    .locals 0
    .param p1, "rate"    # I

    .prologue
    .line 2530
    iput p1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvIndex:I

    .line 2531
    return-void
.end method

.method public setUiWithNoSensor()V
    .locals 2

    .prologue
    .line 2295
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mState:Lcom/sec/android/app/shealth/uv/state/UvState;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/uv/state/UvState;->updateViewWithAnimation(Z)V

    .line 2296
    return-void
.end method

.method public showFailurePopup()V
    .locals 5

    .prologue
    .line 2947
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mContext:Landroid/content/Context;

    const-string v2, "com.sec.android.app.shealth.uv"

    const-string v3, "UV05"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 2949
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->pool:Landroid/media/SoundPool;

    if-eqz v1, :cond_0

    .line 2950
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->pool:Landroid/media/SoundPool;

    invoke-virtual {v1}, Landroid/media/SoundPool;->autoPause()V

    .line 2952
    :cond_0
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    new-instance v2, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$35;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$35;-><init>(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)V

    const-wide/16 v3, 0x1f4

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2961
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->measuringCircleAnimationStop(Z)V

    .line 2963
    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x2

    const v4, 0x7f09002f

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;II)V

    const v2, 0x7f090dc6

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f09004b

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$37;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$37;-><init>(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$36;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$36;-><init>(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelButtonListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnCancelButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mDialogFailureListener:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnDismissListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    .line 2989
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvFailureAlertDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 2990
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvFailureAlertDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "error"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 2992
    return-void
.end method

.method public startSensor()V
    .locals 3

    .prologue
    .line 903
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mDeviceConnector:Lcom/sec/android/app/shealth/uv/common/UvDeviceConnector;

    if-nez v0, :cond_0

    .line 907
    :goto_0
    return-void

    .line 905
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mDeviceConnector:Lcom/sec/android/app/shealth/uv/common/UvDeviceConnector;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvActivity:Lcom/sec/android/app/shealth/uv/UvActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/uv/UvActivity;->getAppContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Lcom/sec/android/app/shealth/uv/common/UvDeviceConnector;->startMeasuring(Landroid/content/Context;Lcom/sec/android/app/shealth/uv/UvSensorListener;Z)V

    goto :goto_0
.end method

.method public stopSensor()V
    .locals 1

    .prologue
    .line 911
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mDeviceConnector:Lcom/sec/android/app/shealth/uv/common/UvDeviceConnector;

    if-nez v0, :cond_0

    .line 912
    invoke-static {}, Lcom/sec/android/app/shealth/uv/common/UvDeviceConnector;->getInstance()Lcom/sec/android/app/shealth/uv/common/UvDeviceConnector;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mDeviceConnector:Lcom/sec/android/app/shealth/uv/common/UvDeviceConnector;

    .line 914
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mDeviceConnector:Lcom/sec/android/app/shealth/uv/common/UvDeviceConnector;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/uv/common/UvDeviceConnector;->stopMeasuring()V

    .line 915
    return-void
.end method

.method public terminateCurrentState()V
    .locals 1

    .prologue
    .line 2196
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mState:Lcom/sec/android/app/shealth/uv/state/UvState;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/uv/state/UvState;->terminateState()V

    .line 2197
    return-void
.end method

.method public unregisterObserver()V
    .locals 2

    .prologue
    .line 280
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 282
    return-void
.end method

.method public updateLayout()V
    .locals 2

    .prologue
    .line 2429
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mState:Lcom/sec/android/app/shealth/uv/state/UvState;

    instance-of v0, v0, Lcom/sec/android/app/shealth/uv/state/UvStateReady;

    if-eqz v0, :cond_1

    .line 2430
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->setReadyUI(I)V

    .line 2434
    :cond_0
    :goto_0
    return-void

    .line 2431
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mState:Lcom/sec/android/app/shealth/uv/state/UvState;

    instance-of v0, v0, Lcom/sec/android/app/shealth/uv/state/UvStateMeasuring;

    if-eqz v0, :cond_0

    .line 2432
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->spfFinishedsunprotection:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method protected updateLayoutParams(Z)V
    .locals 4
    .param p1, "checked"    # Z

    .prologue
    .line 666
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->spfchecklay:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 668
    .local v0, "params":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvPluginUtils:Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;->convertDptoPx(ILandroid/content/Context;)I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 669
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->spfchecklay:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 671
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->spfchecklay:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .end local v0    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 672
    .restart local v0    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvPluginUtils:Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;

    const/4 v2, 0x5

    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;->convertDptoPx(ILandroid/content/Context;)I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 674
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->spfchecklay:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 675
    return-void
.end method

.method protected updateSecondIconParams(Z)V
    .locals 4
    .param p1, "checked"    # Z

    .prologue
    .line 649
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIcon:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 651
    .local v0, "params":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvPluginUtils:Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;->convertDptoPx(ILandroid/content/Context;)I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 652
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIcon:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 654
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIcon:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .end local v0    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 656
    .restart local v0    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    if-nez p1, :cond_0

    .line 657
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvPluginUtils:Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;

    const/16 v2, 0x3c

    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;->convertDptoPx(ILandroid/content/Context;)I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 661
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSecondCenterIcon:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 662
    return-void

    .line 659
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvPluginUtils:Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;

    const/16 v2, 0x21

    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;->convertDptoPx(ILandroid/content/Context;)I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    goto :goto_0
.end method

.method public updateUvDataView(Z)V
    .locals 12
    .param p1, "isMeasuring"    # Z

    .prologue
    .line 922
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvActivity:Lcom/sec/android/app/shealth/uv/UvActivity;

    invoke-static {v6}, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->selectedDate:Ljava/util/Date;

    invoke-virtual {v7}, Ljava/util/Date;->getTime()J

    move-result-wide v7

    invoke-virtual {v6, v7, v8}, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->getLastTwoDataByTime(J)Ljava/util/ArrayList;

    move-result-object v4

    .line 924
    .local v4, "uvDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/uv/data/UvData;>;"
    if-nez v4, :cond_0

    .line 997
    :goto_0
    return-void

    .line 927
    :cond_0
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mThirdCommentLayout:Landroid/widget/LinearLayout;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 929
    if-eqz p1, :cond_2

    .line 931
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-nez v6, :cond_1

    .line 932
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mThirdPreviousLayout:Landroid/widget/LinearLayout;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 933
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mLine:Landroid/view/View;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/view/View;->setVisibility(I)V

    .line 996
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->setNextAndPrevDates()V

    goto :goto_0

    .line 935
    :cond_1
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mThirdPreviousLayout:Landroid/widget/LinearLayout;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 936
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mLine:Landroid/view/View;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/view/View;->setVisibility(I)V

    .line 937
    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/shealth/uv/data/UvData;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/uv/data/UvData;->getScore()D

    move-result-wide v6

    double-to-int v5, v6

    .line 938
    .local v5, "uvScore":I
    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->setPreviousData(I)V

    .line 939
    iget-object v7, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mThirdDate:Landroid/widget/TextView;

    iget-object v8, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mContext:Landroid/content/Context;

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/shealth/uv/data/UvData;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/uv/data/UvData;->getSampleTime()J

    move-result-wide v9

    invoke-static {v8, v9, v10}, Lcom/sec/android/app/shealth/uv/utils/DateFormatUtil;->getDateFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 941
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string/jumbo v6, "yyyy/MM/dd"

    invoke-direct {v2, v6}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 942
    .local v2, "dformat":Ljava/text/SimpleDateFormat;
    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/shealth/uv/data/UvData;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/uv/data/UvData;->getSampleTime()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 943
    .local v1, "ddate":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mThirdDate:Landroid/widget/TextView;

    invoke-virtual {v6, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 944
    iget-object v7, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mThirdTime:Landroid/widget/TextView;

    iget-object v8, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mContext:Landroid/content/Context;

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/shealth/uv/data/UvData;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/uv/data/UvData;->getSampleTime()J

    move-result-wide v9

    invoke-static {v8, v9, v10}, Lcom/sec/android/app/shealth/uv/utils/DateFormatUtil;->getTimeFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 950
    .end local v1    # "ddate":Ljava/lang/String;
    .end local v2    # "dformat":Ljava/text/SimpleDateFormat;
    .end local v5    # "uvScore":I
    :cond_2
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-nez v6, :cond_3

    .line 951
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mThirdPreviousLayout:Landroid/widget/LinearLayout;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 952
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mLine:Landroid/view/View;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/view/View;->setVisibility(I)V

    .line 953
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mThirdDiscard:Landroid/widget/LinearLayout;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 954
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->divider:Landroid/view/View;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/view/View;->setVisibility(I)V

    .line 955
    invoke-direct {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->setUVReadyState()V

    .line 956
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mNonSensorDateLayout:Landroid/widget/LinearLayout;

    const/4 v7, 0x4

    invoke-virtual {v6, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_1

    .line 959
    :cond_3
    iget-boolean v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->hasNoUvSensor:Z

    if-eqz v6, :cond_4

    .line 960
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mNonSensorDateLayout:Landroid/widget/LinearLayout;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 961
    iget-object v7, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mCurrDateTxt:Landroid/widget/TextView;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mContext:Landroid/content/Context;

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/shealth/uv/data/UvData;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/uv/data/UvData;->getSampleTime()J

    move-result-wide v10

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/shealth/uv/utils/DateFormatUtil;->getDateFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mContext:Landroid/content/Context;

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/shealth/uv/data/UvData;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/uv/data/UvData;->getSampleTime()J

    move-result-wide v10

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/shealth/uv/utils/DateFormatUtil;->getTimeFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 966
    new-instance v3, Ljava/text/SimpleDateFormat;

    const-string/jumbo v6, "yyyy/MM/dd"

    invoke-direct {v3, v6}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 967
    .local v3, "format":Ljava/text/SimpleDateFormat;
    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/shealth/uv/data/UvData;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/uv/data/UvData;->getSampleTime()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 968
    .local v0, "date":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 969
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mContext:Landroid/content/Context;

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/shealth/uv/data/UvData;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/uv/data/UvData;->getSampleTime()J

    move-result-wide v9

    invoke-static {v8, v9, v10}, Lcom/sec/android/app/shealth/uv/utils/DateFormatUtil;->getTimeFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 971
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mCurrDateTxt:Landroid/widget/TextView;

    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 974
    .end local v0    # "date":Ljava/lang/String;
    .end local v3    # "format":Ljava/text/SimpleDateFormat;
    :cond_4
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v6

    const/4 v7, 0x2

    if-ne v6, v7, :cond_5

    .line 975
    const/4 v6, 0x1

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/shealth/uv/data/UvData;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/uv/data/UvData;->getScore()D

    move-result-wide v6

    double-to-int v5, v6

    .line 976
    .restart local v5    # "uvScore":I
    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->setPreviousData(I)V

    .line 977
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mThirdPreviousLayout:Landroid/widget/LinearLayout;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 978
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mLine:Landroid/view/View;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/view/View;->setVisibility(I)V

    .line 979
    iget-object v7, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mThirdDate:Landroid/widget/TextView;

    iget-object v8, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mContext:Landroid/content/Context;

    const/4 v6, 0x1

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/shealth/uv/data/UvData;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/uv/data/UvData;->getSampleTime()J

    move-result-wide v9

    invoke-static {v8, v9, v10}, Lcom/sec/android/app/shealth/uv/utils/DateFormatUtil;->getDateFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 981
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string/jumbo v6, "yyyy/MM/dd"

    invoke-direct {v2, v6}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 983
    .restart local v2    # "dformat":Ljava/text/SimpleDateFormat;
    const/4 v6, 0x1

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/shealth/uv/data/UvData;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/uv/data/UvData;->getSampleTime()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 985
    .restart local v1    # "ddate":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mThirdDate:Landroid/widget/TextView;

    invoke-virtual {v6, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 986
    iget-object v7, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mThirdTime:Landroid/widget/TextView;

    iget-object v8, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mContext:Landroid/content/Context;

    const/4 v6, 0x1

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/shealth/uv/data/UvData;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/uv/data/UvData;->getSampleTime()J

    move-result-wide v9

    invoke-static {v8, v9, v10}, Lcom/sec/android/app/shealth/uv/utils/DateFormatUtil;->getTimeFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 992
    .end local v1    # "ddate":Ljava/lang/String;
    .end local v2    # "dformat":Ljava/text/SimpleDateFormat;
    .end local v5    # "uvScore":I
    :goto_2
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mThirdCommentLayout:Landroid/widget/LinearLayout;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_1

    .line 989
    :cond_5
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mThirdPreviousLayout:Landroid/widget/LinearLayout;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 990
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mLine:Landroid/view/View;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2
.end method

.method public verifyConfigurationChanges()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2913
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->isSPFDialogDisplayed:Z

    if-eqz v0, :cond_0

    .line 2914
    invoke-direct {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->showSpfPickerDialog()V

    .line 2915
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->isSPFDialogDisplayed:Z

    .line 2918
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->isShownRecomendedDialog:Z

    if-eqz v0, :cond_1

    .line 2919
    iget v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->recomMsgIndex:I

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->showRecommendedDialog(I)V

    .line 2921
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->needsLocationPopup:Z

    if-eqz v0, :cond_2

    .line 2922
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->needsLocationPopup:Z

    .line 2923
    invoke-direct {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->displayLocationDialogIfNeeded()V

    .line 2926
    :cond_2
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->isFailurePopupVisible:Z

    if-eqz v0, :cond_3

    .line 2927
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->isFailurePopupVisible:Z

    .line 2928
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->showFailurePopup()V

    .line 2930
    :cond_3
    return-void
.end method
