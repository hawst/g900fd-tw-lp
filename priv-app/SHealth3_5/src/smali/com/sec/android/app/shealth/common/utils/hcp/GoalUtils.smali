.class public Lcom/sec/android/app/shealth/common/utils/hcp/GoalUtils;
.super Ljava/lang/Object;
.source "GoalUtils.java"


# static fields
.field public static final DAYS_IN_WEEK:B = 0x7t

.field public static final HOURS_IN_DAY:B = 0x18t

.field public static final LAST_DAY_IN_WEEK:B = 0x6t

.field public static final LAST_HOUR_IN_DAY:B = 0x17t

.field public static final LAST_MILLI_IN_SECOND:I = 0x3e7

.field public static final LAST_MINUTE_IN_HOUR:B = 0x3bt

.field public static final LAST_SECOND_IN_MINUTE:B = 0x3bt

.field private static final LOG_TAG:Ljava/lang/String;

.field public static final MILLIS_IN_DAY:I = 0x5265c00

.field public static final MILLIS_IN_HALFDAY:I = 0x2932e00

.field public static final MILLIS_IN_HOUR:I = 0x36ee80

.field public static final MILLIS_IN_SECOND:I = 0x3e8

.field public static final MILLIS_IN_WEEK:I = 0x240c8400

.field public static final MINUTES_IN_HOUR:B = 0x3ct

.field public static final MONTH_IN_YEAR:I = 0xc

.field public static final SECONDS_IN_MINUTE:B = 0x3ct


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/sec/android/app/shealth/common/utils/hcp/GoalUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/GoalUtils;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static cursorToGoal(Landroid/database/Cursor;)Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;
    .locals 3
    .param p0, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 149
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;-><init>()V

    .line 151
    .local v0, "goalData":Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;
    const-string/jumbo v1, "period"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->setPeriod(I)V

    .line 152
    const-string v1, "goal_type"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->setGoalType(I)V

    .line 153
    const-string v1, "goal_subtype"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->setGoalSubType(I)V

    .line 154
    const-string/jumbo v1, "value"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->setValue(F)V

    .line 155
    const-string v1, "create_time"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->setCreateTime(J)V

    .line 156
    const-string/jumbo v1, "update_time"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->setUpdateTime(J)V

    .line 157
    const-string/jumbo v1, "time_zone"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->setTimeZone(I)V

    .line 159
    return-object v0
.end method

.method public static getGoalDataForOldDates(Landroid/content/Context;Ljava/lang/Integer;)Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;
    .locals 18
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "type"    # Ljava/lang/Integer;

    .prologue
    .line 214
    const/4 v1, 0x4

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "create_time"

    aput-object v2, v3, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "value"

    aput-object v2, v3, v1

    const/4 v1, 0x2

    const-string v2, "goal_type"

    aput-object v2, v3, v1

    const/4 v1, 0x3

    const-string v2, "goal_subtype"

    aput-object v2, v3, v1

    .line 215
    .local v3, "projection":[Ljava/lang/String;
    const-string v4, "goal_type=? "

    .line 216
    .local v4, "selectionClause":Ljava/lang/String;
    const/4 v1, 0x1

    new-array v5, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, ""

    aput-object v2, v5, v1

    .line 217
    .local v5, "mSelectionArgs":[Ljava/lang/String;
    const/4 v1, 0x0

    invoke-virtual/range {p1 .. p1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v1

    .line 218
    const/4 v13, 0x0

    .line 221
    .local v13, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$Goal;->CONTENT_URI:Landroid/net/Uri;

    const-string v6, "create_time ASC  LIMIT 1"

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 222
    if-eqz v13, :cond_1

    invoke-interface {v13}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_1

    .line 224
    invoke-interface {v13}, Landroid/database/Cursor;->moveToFirst()Z

    .line 225
    const-string v1, "create_time"

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v12

    .line 226
    .local v12, "createTime":I
    const-string v1, "goal_type"

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v15

    .line 227
    .local v15, "measureType":I
    const-string/jumbo v1, "value"

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v17

    .line 228
    .local v17, "value":I
    const-string v1, "goal_subtype"

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v16

    .line 230
    .local v16, "subType":I
    new-instance v6, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    invoke-interface {v13, v12}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    invoke-interface {v13, v15}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    move/from16 v0, v17

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getFloat(I)F

    move-result v10

    move/from16 v0, v16

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    invoke-direct/range {v6 .. v11}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;-><init>(JIFI)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 244
    .local v6, "goal":Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;
    if-eqz v13, :cond_0

    .line 246
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 249
    .end local v6    # "goal":Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;
    .end local v12    # "createTime":I
    .end local v15    # "measureType":I
    .end local v16    # "subType":I
    .end local v17    # "value":I
    :cond_0
    :goto_0
    return-object v6

    .line 235
    :cond_1
    const/4 v6, 0x0

    .line 244
    if-eqz v13, :cond_0

    .line 246
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 238
    :catch_0
    move-exception v14

    .line 240
    .local v14, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v14}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 244
    if-eqz v13, :cond_2

    .line 246
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 249
    :cond_2
    const/4 v6, 0x0

    goto :goto_0

    .line 244
    .end local v14    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    if-eqz v13, :cond_3

    .line 246
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v1
.end method

.method public static getGoalList(Landroid/database/Cursor;)Ljava/util/ArrayList;
    .locals 3
    .param p0, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 91
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 93
    .local v0, "GoalList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;>;"
    invoke-interface {p0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 94
    :goto_0
    invoke-interface {p0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v2

    if-nez v2, :cond_0

    .line 96
    invoke-static {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/GoalUtils;->cursorToGoal(Landroid/database/Cursor;)Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    move-result-object v1

    .line 97
    .local v1, "goalData":Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 98
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_0

    .line 101
    .end local v1    # "goalData":Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;
    :cond_0
    return-object v0
.end method

.method public static getGoalValueForPeriod(IJJ)Landroid/util/LongSparseArray;
    .locals 10
    .param p0, "type"    # I
    .param p1, "periodStartTime"    # J
    .param p3, "periodEndTime"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IJJ)",
            "Landroid/util/LongSparseArray",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 288
    new-instance v7, Landroid/util/LongSparseArray;

    invoke-direct {v7}, Landroid/util/LongSparseArray;-><init>()V

    .line 289
    .local v7, "mGoalDataMap":Landroid/util/LongSparseArray;, "Landroid/util/LongSparseArray<Ljava/lang/Float;>;"
    const/4 v0, 0x4

    new-array v2, v0, [Ljava/lang/String;

    const-string/jumbo v0, "set_time"

    aput-object v0, v2, v1

    const-string/jumbo v0, "value"

    aput-object v0, v2, v5

    const-string v0, "goal_type"

    aput-object v0, v2, v8

    const-string v0, "goal_subtype"

    aput-object v0, v2, v9

    .line 290
    .local v2, "projection":[Ljava/lang/String;
    const-string v3, "goal_type = ? AND set_time >= ? AND set_time <= ? "

    .line 291
    .local v3, "selectionClause":Ljava/lang/String;
    new-array v4, v9, [Ljava/lang/String;

    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v1

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-static {p3, p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v8

    .line 292
    .local v4, "mSelectionArgs":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 295
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Goal;->CONTENT_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 296
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 298
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 299
    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_1

    .line 301
    const-string/jumbo v0, "set_time"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v0

    const-string/jumbo v5, "value"

    invoke-interface {v6, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v6, v5}, Landroid/database/Cursor;->getFloat(I)F

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    invoke-virtual {v7, v0, v1, v5}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 302
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 309
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_0

    .line 311
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_0
    throw v0

    .line 309
    :cond_1
    if-eqz v6, :cond_2

    .line 311
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    return-object v7
.end method

.method public static getLatestGoalData(Landroid/content/Context;Ljava/lang/Integer;)Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "type"    # Ljava/lang/Integer;

    .prologue
    .line 164
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v1

    invoke-static {p0, v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/hcp/GoalUtils;->getLatestGoalDataForTime(Landroid/content/Context;IJ)Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    move-result-object v0

    return-object v0
.end method

.method public static getLatestGoalDataForTime(Landroid/content/Context;IJ)Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;
    .locals 16
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "type"    # I
    .param p2, "time"    # J

    .prologue
    .line 254
    const/4 v0, 0x7

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string/jumbo v1, "period"

    aput-object v1, v2, v0

    const/4 v0, 0x1

    const-string v1, "create_time"

    aput-object v1, v2, v0

    const/4 v0, 0x2

    const-string/jumbo v1, "value"

    aput-object v1, v2, v0

    const/4 v0, 0x3

    const-string v1, "goal_type"

    aput-object v1, v2, v0

    const/4 v0, 0x4

    const-string v1, "goal_subtype"

    aput-object v1, v2, v0

    const/4 v0, 0x5

    const-string/jumbo v1, "update_time"

    aput-object v1, v2, v0

    const/4 v0, 0x6

    const-string/jumbo v1, "time_zone"

    aput-object v1, v2, v0

    .line 256
    .local v2, "projection":[Ljava/lang/String;
    const-string v3, "goal_type=? AND create_time<=?"

    .line 257
    .local v3, "selectionClause":Ljava/lang/String;
    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x1

    invoke-static/range {p2 .. p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    .line 258
    .local v4, "mSelectionArgs":[Ljava/lang/String;
    const/4 v12, 0x0

    .line 260
    .local v12, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Goal;->CONTENT_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 261
    if-eqz v12, :cond_1

    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 263
    invoke-interface {v12}, Landroid/database/Cursor;->moveToLast()Z

    .line 264
    const-string v0, "create_time"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v11

    .line 265
    .local v11, "createTime":I
    const-string v0, "goal_type"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v13

    .line 266
    .local v13, "measureType":I
    const-string/jumbo v0, "value"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v15

    .line 267
    .local v15, "value":I
    const-string v0, "goal_subtype"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v14

    .line 268
    .local v14, "subType":I
    new-instance v5, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    invoke-interface {v12, v11}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-interface {v12, v13}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    invoke-interface {v12, v15}, Landroid/database/Cursor;->getFloat(I)F

    move-result v9

    invoke-interface {v12, v14}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    invoke-direct/range {v5 .. v10}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;-><init>(JIFI)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 273
    if-eqz v12, :cond_0

    .line 274
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .end local v11    # "createTime":I
    .end local v13    # "measureType":I
    .end local v14    # "subType":I
    .end local v15    # "value":I
    :cond_0
    :goto_0
    return-object v5

    .line 270
    :cond_1
    const/4 v5, 0x0

    .line 273
    if-eqz v12, :cond_0

    .line 274
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 273
    :catchall_0
    move-exception v0

    if-eqz v12, :cond_2

    .line 274
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method public static insertitem(Landroid/content/Context;IIIFJJI)J
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "period"    # I
    .param p2, "goalType"    # I
    .param p3, "goalSubType"    # I
    .param p4, "value"    # F
    .param p5, "createTime"    # J
    .param p7, "updateTime"    # J
    .param p9, "timeZone"    # I

    .prologue
    .line 118
    move v0, p1

    move v1, p2

    move v2, p4

    move-wide/from16 v3, p5

    move-wide/from16 v5, p7

    move/from16 v7, p9

    invoke-static/range {v0 .. v7}, Lcom/sec/android/app/shealth/common/utils/hcp/GoalUtils;->prepareGoalContentValuesWithoutSubType(IIFJJI)Landroid/content/ContentValues;

    move-result-object v10

    .line 119
    .local v10, "values":Landroid/content/ContentValues;
    const-string v0, "goal_subtype"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 122
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Goal;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v10}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v9

    .line 123
    .local v9, "rawContactUri":Landroid/net/Uri;
    invoke-static {v9}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J
    :try_end_0
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 127
    .end local v9    # "rawContactUri":Landroid/net/Uri;
    :goto_0
    return-wide v0

    .line 124
    :catch_0
    move-exception v8

    .line 125
    .local v8, "e":Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/GoalUtils;->LOG_TAG:Ljava/lang/String;

    const-string v1, "Got invalid Data, Unable to insert"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method private static prepareGoalContentValuesWithoutSubType(IIFJJI)Landroid/content/ContentValues;
    .locals 3
    .param p0, "period"    # I
    .param p1, "goalType"    # I
    .param p2, "value"    # F
    .param p3, "createTime"    # J
    .param p5, "updateTime"    # J
    .param p7, "timeZone"    # I

    .prologue
    .line 132
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 133
    .local v0, "values":Landroid/content/ContentValues;
    const-string/jumbo v1, "period"

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 134
    const-string v1, "goal_type"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 135
    const-string/jumbo v1, "value"

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 136
    const-string v1, "create_time"

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 137
    const-string/jumbo v1, "update_time"

    invoke-static {p5, p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 138
    const-string/jumbo v1, "time_zone"

    invoke-static {p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 139
    return-object v0
.end method
