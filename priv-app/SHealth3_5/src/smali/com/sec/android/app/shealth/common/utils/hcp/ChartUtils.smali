.class public Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils;
.super Ljava/lang/Object;
.source "ChartUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$IOnDBDataFetchListener;,
        Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;,
        Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$Operation;
    }
.end annotation


# static fields
.field public static final SQLITE_BLOOD_GLUCOSE_GROUPBY_DAY:Ljava/lang/String; = "strftime(\"%d-%m-%Y\",([blood_glucose].[create_time]/1000),\'unixepoch\')"

.field public static final SQLITE_BLOOD_GLUCOSE_GROUPBY_MONTH:Ljava/lang/String; = "strftime(\"%m-%Y\",([blood_glucose].[create_time]/1000),\'unixepoch\')"

.field public static final SQLITE_BLOOD_PRESSURE_GROUPBY_DAY:Ljava/lang/String; = "strftime(\"%d-%m-%Y\",([blood_pressure].[create_time]/1000),\'unixepoch\')"

.field public static final SQLITE_BLOOD_PRESSURE_GROUPBY_MONTH:Ljava/lang/String; = "strftime(\"%m-%Y\",([blood_pressure].[create_time]/1000),\'unixepoch\')"

.field public static final SQLITE_WEIGHT_GROUPBY_DAY:Ljava/lang/String; = "strftime(\"%d-%m-%Y\",([Weight].[sample_time]/1000),\'unixepoch\')"

.field public static final SQLITE_WEIGHT_GROUPBY_MONTH:Ljava/lang/String; = "strftime(\"%m-%Y\",([Weight].[sample_time]/1000),\'unixepoch\')"

.field public static final SQLITE_WFL_GROUPBY_DAY:Ljava/lang/String; = "strftime(\"%d-%m-%Y\",([pedometer_data].[create_time]/1000),\'unixepoch\')"

.field public static final SQLITE_WFL_GROUPBY_HOUR:Ljava/lang/String; = "strftime(\"%d-%m-%Y %H\",([pedometer_data].[create_time]/1000),\'unixepoch\')"

.field public static final SQLITE_WFL_GROUPBY_MONTH:Ljava/lang/String; = "strftime(\"%m-%Y\",([pedometer_data].[create_time]/1000),\'unixepoch\')"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    return-void
.end method
