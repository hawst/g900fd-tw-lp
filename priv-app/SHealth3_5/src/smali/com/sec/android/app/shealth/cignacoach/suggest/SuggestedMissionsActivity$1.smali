.class Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity$1;
.super Ljava/lang/Object;
.source "SuggestedMissionsActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$DateChageCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;)V
    .locals 0

    .prologue
    .line 293
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDateChanged()V
    .locals 0

    .prologue
    .line 313
    return-void
.end method

.method public onGoalMissionStatusChange(II)V
    .locals 2
    .param p1, "goalId"    # I
    .param p2, "missionId"    # I

    .prologue
    .line 297
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->mSelectedGoalId:I
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->access$000(Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;)I

    move-result v0

    if-ne v0, p1, :cond_0

    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    .line 298
    # getter for: Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Because user mission completed or expired, suggested mission list update."

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 299
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;

    new-instance v1, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity$1$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity$1$1;-><init>(Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity$1;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 310
    :goto_0
    return-void

    .line 308
    :cond_0
    # getter for: Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "User goal/mission completed or expired, but different goalId, then suggested mission list no update."

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
