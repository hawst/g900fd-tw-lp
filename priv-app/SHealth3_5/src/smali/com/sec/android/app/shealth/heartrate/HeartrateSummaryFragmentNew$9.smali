.class Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$9;
.super Ljava/lang/Object;
.source "HeartrateSummaryFragmentNew.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)V
    .locals 0

    .prologue
    .line 1102
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$9;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const v8, 0x7f090c22

    const/16 v7, 0x8

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1106
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 1148
    :goto_0
    return-void

    .line 1108
    :sswitch_0
    sget-object v2, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "mHeartrateOnClcickListener event : ib_summary_third_graph"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1109
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$9;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    # invokes: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->showGraphFragment()V
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$2600(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)V

    goto :goto_0

    .line 1113
    :sswitch_1
    sget-object v2, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "mHeartrateOnClcickListener event : ll_summary_third_previous"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1114
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$9;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$9;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$2700(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogActivity;

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 1117
    :sswitch_2
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$9;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    iput-boolean v5, v2, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mStateMeasuringEndUI:Z

    .line 1118
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$9;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$1000(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    move-result-object v2

    iput-boolean v5, v2, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->isDialogshown:Z

    .line 1119
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$9;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    invoke-virtual {v2, v6}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->setNeedsReadyUI(Z)V

    .line 1120
    sget-object v2, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "mHeartrateOnClcickListener event : bt_summary_second_retry"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1121
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$9;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->effectAudio:[I
    invoke-static {}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$2800()[I

    move-result-object v3

    aget v3, v3, v5

    # invokes: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->playSound(I)V
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$2900(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;I)V

    .line 1122
    invoke-virtual {p1, v7}, Landroid/view/View;->setVisibility(I)V

    .line 1123
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$9;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    invoke-virtual {v2, v5}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->startSensorAndDelayUIForDetectedFinger(I)V

    .line 1124
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$9;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    invoke-virtual {v2, v6}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->updateBpmDataView(Z)V

    .line 1125
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$9;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    # invokes: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->refreshFragmentFocusables()V
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$3000(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)V

    .line 1126
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$9;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstMessage:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$3100(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Landroid/widget/TextView;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$9;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$2700(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1129
    :sswitch_3
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$9;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$1000(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    move-result-object v2

    iput-boolean v5, v2, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->isDialogshown:Z

    .line 1130
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$9;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    iput-boolean v5, v2, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mStateMeasuringEndUI:Z

    .line 1131
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$9;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    invoke-virtual {v2, v6}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->setNeedsReadyUI(Z)V

    .line 1132
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$9;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$1100(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getLastTwoDataByTime(J)Ljava/util/ArrayList;

    move-result-object v1

    .line 1133
    .local v1, "heartRateData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;>;"
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 1134
    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    .line 1135
    .local v0, "hdata":Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$9;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$1100(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getId()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->deleteRowById(Ljava/lang/String;)Z

    .line 1137
    .end local v0    # "hdata":Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$9;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->effectAudio:[I
    invoke-static {}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$2800()[I

    move-result-object v3

    const/4 v4, 0x2

    aget v3, v3, v4

    # invokes: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->playSound(I)V
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$2900(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;I)V

    .line 1138
    invoke-virtual {p1, v7}, Landroid/view/View;->setVisibility(I)V

    .line 1139
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$9;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    invoke-virtual {v2, v5}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->startSensorAndDelayUIForDetectedFinger(I)V

    .line 1140
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$9;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    invoke-virtual {v2, v6}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->updateBpmDataView(Z)V

    .line 1141
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$9;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    # invokes: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->refreshFragmentFocusables()V
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$3200(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)V

    .line 1142
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$9;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstMessage:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$3100(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Landroid/widget/TextView;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$9;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$2700(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1143
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$9;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$2700(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f090f93

    invoke-static {v2, v3, v5}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 1106
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0805c0 -> :sswitch_2
        0x7f0805e4 -> :sswitch_3
        0x7f0805e7 -> :sswitch_1
        0x7f0805e8 -> :sswitch_1
        0x7f0805ef -> :sswitch_0
    .end sparse-switch
.end method
