.class Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$2;
.super Ljava/lang/Object;
.source "EveryoneRankingsFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->addFootMyRanking()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)V
    .locals 0

    .prologue
    .line 219
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 223
    # getter for: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mIsBlocked:Z
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$1700()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 232
    :goto_0
    return-void

    .line 224
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # invokes: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->blockManyClick()V
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$1800(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)V

    .line 226
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogFragment;

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->myValuesHolder:Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$1000(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogFragment;-><init>(Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;)V

    .line 228
    .local v0, "dialog":Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogFragment;
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 229
    :catch_0
    move-exception v1

    .line 230
    .local v1, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0
.end method
