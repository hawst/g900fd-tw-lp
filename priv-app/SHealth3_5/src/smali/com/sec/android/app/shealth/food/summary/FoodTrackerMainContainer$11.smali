.class Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$11;
.super Ljava/lang/Object;
.source "FoodTrackerMainContainer.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/OnSaveChosenItemListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->getOnSaveChosenItemListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/OnSaveChosenItemListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;)V
    .locals 0

    .prologue
    .line 621
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$11;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSave(I)V
    .locals 9
    .param p1, "index"    # I

    .prologue
    .line 627
    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    .line 629
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$11;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;

    # invokes: Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->showOkCancelDeleteMealDialog()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->access$500(Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;)V

    .line 647
    :goto_0
    return-void

    .line 633
    :cond_0
    new-instance v8, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$11;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->getContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;

    invoke-direct {v8, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 635
    .local v8, "intent":Landroid/content/Intent;
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 637
    .local v7, "bundle":Landroid/os/Bundle;
    new-instance v0, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$11;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;

    # getter for: Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->mCurrentMealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->access$600(Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;)Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getId()J

    move-result-wide v1

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$11;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;

    # getter for: Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->mCurrentMealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    invoke-static {v3}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->access$600(Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;)Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getMealType()I

    move-result v3

    int-to-long v3, v3

    iget-object v5, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$11;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;

    # getter for: Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->mCurrentMealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    invoke-static {v5}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->access$600(Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;)Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getMealTime()J

    move-result-wide v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;-><init>(JJJ)V

    .line 639
    .local v0, "mealDataHolder":Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;
    const-string v1, "MEAL_DATA_HOLDER"

    invoke-virtual {v7, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 641
    invoke-virtual {v8, v7}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 643
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$11;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v8}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
