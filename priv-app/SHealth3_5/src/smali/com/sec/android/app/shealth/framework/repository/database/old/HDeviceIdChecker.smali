.class public Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;
.super Ljava/lang/Object;
.source "HDeviceIdChecker.java"

# interfaces
.implements Lcom/sec/android/service/health/connectionmanager2/IOnResponseListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker$IHDeviceIdListener;
    }
.end annotation


# static fields
.field static final CMGR_BK_HTTP:Ljava/lang/String; = "ServerBackupMgrHttp"

.field private static final CONN_TIMEOUT:I = 0x2710

.field private static final REQUEST_ID_CHECKSTATUS:I = 0x6a

.field private static final REQUEST_ID_TRANSACTION_CANCEL:I = 0x67

.field private static final REQUEST_ID_TRANSACTION_END:I = 0x66

.field private static final REQUEST_ID_TRANSACTION_START:I = 0x65

.field private static final SO_TIMEOUT:I = 0x7530

.field private static mInstance:Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;


# instance fields
.field private final CMGR_HTTPS:Ljava/lang/String;

.field private final SERVER_URL_CHECKSTATUS:Ljava/lang/String;

.field private final SERVER_URL_TRANSACTION:Ljava/lang/String;

.field private final TAG:Ljava/lang/String;

.field private currentRequestId:J

.field private mAccountID:Ljava/lang/String;

.field private mDeviceID:Ljava/lang/String;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "InlinedApi"
        }
    .end annotation
.end field

.field private mDeviceIdListener:Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker$IHDeviceIdListener;

.field private mTransactionID:Ljava/lang/String;

.field private mUserToken:Ljava/lang/String;

.field private resCheckStatus:Lcom/sec/android/app/shealth/backup/server/data/ResponseCheckStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 69
    new-instance v0, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->mInstance:Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    const-class v0, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->TAG:Ljava/lang/String;

    .line 41
    sget-object v0, Landroid/os/Build;->SERIAL:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->mDeviceID:Ljava/lang/String;

    .line 45
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->mTransactionID:Ljava/lang/String;

    .line 47
    const-string v0, "ServerMgrHttps"

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->CMGR_HTTPS:Ljava/lang/String;

    .line 51
    const-string/jumbo v0, "v2/shealth/phr/txmanager"

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->SERVER_URL_TRANSACTION:Ljava/lang/String;

    .line 52
    const-string/jumbo v0, "v2/shealth/phr/chkstatus"

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->SERVER_URL_CHECKSTATUS:Ljava/lang/String;

    .line 60
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->currentRequestId:J

    .line 73
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;)Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker$IHDeviceIdListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->mDeviceIdListener:Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker$IHDeviceIdListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->mAccountID:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->mAccountID:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->mUserToken:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->mUserToken:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->mDeviceID:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->startTransaction()V

    return-void
.end method

.method private cancelTransaction()V
    .locals 12

    .prologue
    const/4 v6, 0x0

    .line 221
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->TAG:Ljava/lang/String;

    const-string v1, "cancelTransaction"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 223
    new-instance v5, Lcom/sec/android/service/health/connectionmanager2/RequestParam;

    invoke-direct {v5}, Lcom/sec/android/service/health/connectionmanager2/RequestParam;-><init>()V

    .line 224
    .local v5, "params":Lcom/sec/android/service/health/connectionmanager2/RequestParam;
    const-string v0, "htxmethod"

    const-string v1, "cancel"

    invoke-virtual {v5, v0, v1}, Lcom/sec/android/service/health/connectionmanager2/RequestParam;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    const-wide/16 v10, 0x0

    const-string v0, "ServerMgrHttps"

    invoke-static {v0}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;->getInstance(Ljava/lang/String;)Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    move-result-object v0

    const/16 v2, 0x67

    sget-object v3, Lcom/sec/android/service/health/connectionmanager2/MethodType;->GET:Lcom/sec/android/service/health/connectionmanager2/MethodType;

    const-string/jumbo v4, "v2/shealth/phr/txmanager"

    const-string v8, "cancel"

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->getDefaultHeader()Ljava/util/HashMap;

    move-result-object v9

    move-object v1, p0

    move-object v7, p0

    invoke-virtual/range {v0 .. v9}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->placeRequest(Ljava/lang/Object;ILcom/sec/android/service/health/connectionmanager2/MethodType;Ljava/lang/String;Lcom/sec/android/service/health/connectionmanager2/RequestParam;Ljava/lang/String;Lcom/sec/android/service/health/connectionmanager2/IOnResponseListener;Ljava/lang/Object;Ljava/util/HashMap;)J

    move-result-wide v0

    cmp-long v0, v10, v0

    if-nez v0, :cond_0

    .line 230
    :cond_0
    iput-object v6, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->mTransactionID:Ljava/lang/String;

    .line 231
    return-void
.end method

.method private endTransaction()V
    .locals 10

    .prologue
    const/4 v6, 0x0

    .line 207
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->TAG:Ljava/lang/String;

    const-string v1, "endTransaction"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    new-instance v5, Lcom/sec/android/service/health/connectionmanager2/RequestParam;

    invoke-direct {v5}, Lcom/sec/android/service/health/connectionmanager2/RequestParam;-><init>()V

    .line 210
    .local v5, "params":Lcom/sec/android/service/health/connectionmanager2/RequestParam;
    const-string v0, "htxmethod"

    const-string v1, "end"

    invoke-virtual {v5, v0, v1}, Lcom/sec/android/service/health/connectionmanager2/RequestParam;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    const-string v0, "ServerMgrHttps"

    invoke-static {v0}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;->getInstance(Ljava/lang/String;)Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    move-result-object v0

    const/16 v2, 0x66

    sget-object v3, Lcom/sec/android/service/health/connectionmanager2/MethodType;->GET:Lcom/sec/android/service/health/connectionmanager2/MethodType;

    const-string/jumbo v4, "v2/shealth/phr/txmanager"

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->getDefaultHeader()Ljava/util/HashMap;

    move-result-object v9

    move-object v1, p0

    move-object v7, p0

    move-object v8, v6

    invoke-virtual/range {v0 .. v9}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->placeRequest(Ljava/lang/Object;ILcom/sec/android/service/health/connectionmanager2/MethodType;Ljava/lang/String;Lcom/sec/android/service/health/connectionmanager2/RequestParam;Ljava/lang/String;Lcom/sec/android/service/health/connectionmanager2/IOnResponseListener;Ljava/lang/Object;Ljava/util/HashMap;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->currentRequestId:J

    .line 213
    iput-object v6, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->mTransactionID:Ljava/lang/String;

    .line 214
    return-void
.end method

.method private getDefaultHeader()Ljava/util/HashMap;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 168
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 169
    .local v0, "headerParams":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;[Ljava/lang/String;>;"
    const-string v1, "accept"

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "application/json"

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 170
    const-string v1, "accountid"

    new-array v2, v5, [Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->mAccountID:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 171
    const-string v1, "deviceid"

    new-array v2, v5, [Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->mDeviceID:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 172
    const-string/jumbo v1, "user_token"

    new-array v2, v5, [Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->mUserToken:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 173
    const-string/jumbo v1, "x-osp-appId"

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "1y90e30264"

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 175
    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->mTransactionID:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 177
    const-string v1, "htxid"

    new-array v2, v5, [Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->mTransactionID:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 179
    :cond_0
    const-string v1, "country_code"

    new-array v2, v5, [Ljava/lang/String;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 180
    return-object v0
.end method

.method public static getInstance()Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;
    .locals 1

    .prologue
    .line 82
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->mInstance:Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;

    return-object v0
.end method

.method private initializeConnectionmanager(Z)Z
    .locals 7
    .param p1, "isHttpsEnabled"    # Z

    .prologue
    .line 240
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->TAG:Ljava/lang/String;

    const-string v1, "initializeConnectionmanager"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 241
    const-string v0, "ServerMgrHttps"

    invoke-static {v0}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;->getInstance(Ljava/lang/String;)Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    move-result-object v0

    if-nez v0, :cond_0

    .line 243
    const-string v0, "ServerMgrHttps"

    invoke-static {v0}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;->createInstance(Ljava/lang/String;)Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    .line 244
    const-string v0, "ServerMgrHttps"

    invoke-static {v0}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;->getInstance(Ljava/lang/String;)Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    move-result-object v0

    sget-object v1, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$HServer;->SERVER_URL_BASE_H:Ljava/lang/String;

    sget v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$HServer;->HTTPS_PORT_NO_H:I

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->setAddressWithoutLookUp(Ljava/lang/String;I)Z

    .line 247
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->TAG:Ljava/lang/String;

    const-string v1, "initializeConnectionmanager - initConnectionManager"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 248
    const-string v0, "ServerMgrHttps"

    invoke-static {v0}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;->getInstance(Ljava/lang/String;)Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    move-result-object v0

    const/16 v1, 0x2710

    const/16 v2, 0x7530

    const/4 v4, 0x0

    const/4 v5, 0x0

    move v3, p1

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->initConnectionManager(IIZLjava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 257
    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 250
    :catch_0
    move-exception v6

    .line 252
    .local v6, "ex":Ljava/lang/Exception;
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "initializeConnectionmanager Exception"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v6}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 253
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->mDeviceIdListener:Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker$IHDeviceIdListener;

    invoke-interface {v0, v6}, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker$IHDeviceIdListener;->onException(Ljava/lang/Exception;)V

    .line 254
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private startTransaction()V
    .locals 10

    .prologue
    const/4 v6, 0x0

    .line 189
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "startTransaction"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->initializeConnectionmanager(Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 200
    :goto_0
    return-void

    .line 196
    :cond_0
    new-instance v5, Lcom/sec/android/service/health/connectionmanager2/RequestParam;

    invoke-direct {v5}, Lcom/sec/android/service/health/connectionmanager2/RequestParam;-><init>()V

    .line 197
    .local v5, "params":Lcom/sec/android/service/health/connectionmanager2/RequestParam;
    const-string v0, "htxmethod"

    const-string/jumbo v1, "start"

    invoke-virtual {v5, v0, v1}, Lcom/sec/android/service/health/connectionmanager2/RequestParam;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    const-string v0, "ServerMgrHttps"

    invoke-static {v0}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;->getInstance(Ljava/lang/String;)Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    move-result-object v0

    const/16 v2, 0x65

    sget-object v3, Lcom/sec/android/service/health/connectionmanager2/MethodType;->GET:Lcom/sec/android/service/health/connectionmanager2/MethodType;

    const-string/jumbo v4, "v2/shealth/phr/txmanager"

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->getDefaultHeader()Ljava/util/HashMap;

    move-result-object v9

    move-object v1, p0

    move-object v7, p0

    move-object v8, v6

    invoke-virtual/range {v0 .. v9}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->placeRequest(Ljava/lang/Object;ILcom/sec/android/service/health/connectionmanager2/MethodType;Ljava/lang/String;Lcom/sec/android/service/health/connectionmanager2/RequestParam;Ljava/lang/String;Lcom/sec/android/service/health/connectionmanager2/IOnResponseListener;Ljava/lang/Object;Ljava/util/HashMap;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->currentRequestId:J

    goto :goto_0
.end method

.method private stopCheckRequest()V
    .locals 1

    .prologue
    .line 390
    const-string v0, "ServerMgrHttps"

    invoke-static {v0}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;->getInstance(Ljava/lang/String;)Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 392
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->cancelTransaction()V

    .line 394
    :cond_0
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 3

    .prologue
    .line 137
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->TAG:Ljava/lang/String;

    const-string v1, "cancel requested"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    const-string v0, "ServerMgrHttps"

    invoke-static {v0}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;->getInstance(Ljava/lang/String;)Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 140
    const-string v0, "ServerMgrHttps"

    invoke-static {v0}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;->getInstance(Ljava/lang/String;)Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    move-result-object v0

    iget-wide v1, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->currentRequestId:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->cancelRequest(Ljava/lang/Object;)V

    .line 142
    :cond_0
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->currentRequestId:J

    .line 144
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->stopCheckRequest()V

    .line 145
    return-void
.end method

.method public checkLastBackedUpDeviceId(Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker$IHDeviceIdListener;)Z
    .locals 5
    .param p1, "listener"    # Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker$IHDeviceIdListener;

    .prologue
    .line 92
    if-nez p1, :cond_0

    .line 94
    const/4 v0, 0x0

    .line 129
    :goto_0
    return v0

    .line 96
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "start HDeviceId Checker"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->mDeviceIdListener:Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker$IHDeviceIdListener;

    .line 100
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->mTransactionID:Ljava/lang/String;

    .line 102
    invoke-static {}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->getInstance()Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "1y90e30264"

    const-string v3, "80E7ECD9D301CB7888C73703639302E5"

    new-instance v4, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker$1;

    invoke-direct {v4, p0}, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker$1;-><init>(Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;)V

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->sendRequestForUserToken(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/service/health/samsungaccount/ISamsungUserTokenListener;)V

    .line 129
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onExceptionReceived(JILcom/sec/android/service/health/connectionmanager2/NetException;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3
    .param p1, "requestId"    # J
    .param p3, "privateId"    # I
    .param p4, "ex"    # Lcom/sec/android/service/health/connectionmanager2/NetException;
    .param p5, "tag"    # Ljava/lang/Object;
    .param p6, "ReRequest"    # Ljava/lang/Object;

    .prologue
    .line 362
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "HDeviceIdChecker: onExceptionReceived"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p4}, Lcom/sec/android/service/health/connectionmanager2/NetException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 364
    const/16 v0, 0x67

    if-eq p3, v0, :cond_0

    .line 366
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->stopCheckRequest()V

    .line 373
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->mDeviceIdListener:Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker$IHDeviceIdListener;

    invoke-interface {v0, p4}, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker$IHDeviceIdListener;->onException(Ljava/lang/Exception;)V

    .line 375
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->mTransactionID:Ljava/lang/String;

    .line 376
    return-void
.end method

.method public onRequestCancelled(JILjava/lang/Object;Ljava/lang/Object;)V
    .locals 2
    .param p1, "requestId"    # J
    .param p3, "privateId"    # I
    .param p4, "tag"    # Ljava/lang/Object;
    .param p5, "ReRequest"    # Ljava/lang/Object;

    .prologue
    .line 384
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->TAG:Ljava/lang/String;

    const-string v1, "HDeviceIdChecker: onRequestCancelled"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 385
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->mTransactionID:Ljava/lang/String;

    .line 386
    return-void
.end method

.method public onResponseReceived(JILjava/lang/Object;Ljava/lang/Object;)V
    .locals 13
    .param p1, "requestId"    # J
    .param p3, "privateId"    # I
    .param p4, "response"    # Ljava/lang/Object;
    .param p5, "tag"    # Ljava/lang/Object;

    .prologue
    .line 267
    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "onResponseReceived HDeviceId Checker"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    new-instance v1, Lcom/google/gson/GsonBuilder;

    invoke-direct {v1}, Lcom/google/gson/GsonBuilder;-><init>()V

    invoke-virtual {v1}, Lcom/google/gson/GsonBuilder;->create()Lcom/google/gson/Gson;

    move-result-object v11

    .line 270
    .local v11, "gson":Lcom/google/gson/Gson;
    sparse-switch p3, :sswitch_data_0

    .line 354
    .end local p4    # "response":Ljava/lang/Object;
    :goto_0
    return-void

    .line 274
    .restart local p4    # "response":Ljava/lang/Object;
    :sswitch_0
    check-cast p4, Ljava/lang/String;

    .end local p4    # "response":Ljava/lang/Object;
    const-class v1, Lcom/sec/android/app/shealth/backup/server/data/ResponseTranaction;

    move-object/from16 v0, p4

    invoke-virtual {v11, v0, v1}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/app/shealth/backup/server/data/ResponseTranaction;

    .line 275
    .local v12, "res":Lcom/sec/android/app/shealth/backup/server/data/ResponseTranaction;
    if-nez v12, :cond_0

    .line 277
    new-instance v5, Lcom/sec/android/service/health/connectionmanager2/NetException;

    const/4 v1, -0x6

    const/16 v2, 0x1f4

    invoke-direct {v5, v1, v2}, Lcom/sec/android/service/health/connectionmanager2/NetException;-><init>(II)V

    const/4 v7, 0x0

    move-object v1, p0

    move-wide v2, p1

    move/from16 v4, p3

    move-object/from16 v6, p5

    invoke-virtual/range {v1 .. v7}, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->onExceptionReceived(JILcom/sec/android/service/health/connectionmanager2/NetException;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 280
    :cond_0
    invoke-virtual {v12}, Lcom/sec/android/app/shealth/backup/server/data/ResponseTranaction;->checkIsResCodeOK()Z

    move-result v1

    if-nez v1, :cond_2

    .line 282
    invoke-virtual {v12}, Lcom/sec/android/app/shealth/backup/server/data/ResponseTranaction;->getResCode()I

    move-result v1

    const/16 v2, 0x191

    if-ne v1, v2, :cond_1

    .line 285
    invoke-static {}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->getInstance()Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker$2;-><init>(Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->sendRequestForNewUserToken(Lcom/sec/android/service/health/samsungaccount/ISamsungUserTokenListener;)V

    goto :goto_0

    .line 313
    :cond_1
    new-instance v5, Lcom/sec/android/service/health/connectionmanager2/NetException;

    const/4 v1, -0x6

    invoke-virtual {v12}, Lcom/sec/android/app/shealth/backup/server/data/ResponseTranaction;->getResCode()I

    move-result v2

    invoke-direct {v5, v1, v2}, Lcom/sec/android/service/health/connectionmanager2/NetException;-><init>(II)V

    const/4 v7, 0x0

    move-object v1, p0

    move-wide v2, p1

    move/from16 v4, p3

    move-object/from16 v6, p5

    invoke-virtual/range {v1 .. v7}, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->onExceptionReceived(JILcom/sec/android/service/health/connectionmanager2/NetException;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 326
    :cond_2
    iget-object v1, v12, Lcom/sec/android/app/shealth/backup/server/data/ResponseTranaction;->htxid:Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->mTransactionID:Ljava/lang/String;

    .line 327
    const-string v1, "ServerMgrHttps"

    invoke-static {v1}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;->getInstance(Ljava/lang/String;)Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    move-result-object v1

    const/16 v3, 0x6a

    sget-object v4, Lcom/sec/android/service/health/connectionmanager2/MethodType;->GET:Lcom/sec/android/service/health/connectionmanager2/MethodType;

    const-string/jumbo v5, "v2/shealth/phr/chkstatus"

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v9, 0x0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->getDefaultHeader()Ljava/util/HashMap;

    move-result-object v10

    move-object v2, p0

    move-object v8, p0

    invoke-virtual/range {v1 .. v10}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->placeRequest(Ljava/lang/Object;ILcom/sec/android/service/health/connectionmanager2/MethodType;Ljava/lang/String;Lcom/sec/android/service/health/connectionmanager2/RequestParam;Ljava/lang/String;Lcom/sec/android/service/health/connectionmanager2/IOnResponseListener;Ljava/lang/Object;Ljava/util/HashMap;)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->currentRequestId:J

    goto :goto_0

    .line 334
    .end local v12    # "res":Lcom/sec/android/app/shealth/backup/server/data/ResponseTranaction;
    .restart local p4    # "response":Ljava/lang/Object;
    :sswitch_1
    check-cast p4, Ljava/lang/String;

    .end local p4    # "response":Ljava/lang/Object;
    const-class v1, Lcom/sec/android/app/shealth/backup/server/data/ResponseCheckStatus;

    move-object/from16 v0, p4

    invoke-virtual {v11, v0, v1}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/backup/server/data/ResponseCheckStatus;

    iput-object v1, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->resCheckStatus:Lcom/sec/android/app/shealth/backup/server/data/ResponseCheckStatus;

    .line 335
    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->resCheckStatus:Lcom/sec/android/app/shealth/backup/server/data/ResponseCheckStatus;

    if-nez v1, :cond_3

    .line 337
    new-instance v5, Lcom/sec/android/service/health/connectionmanager2/NetException;

    const/4 v1, -0x1

    const/16 v2, 0x1f4

    invoke-direct {v5, v1, v2}, Lcom/sec/android/service/health/connectionmanager2/NetException;-><init>(II)V

    const/4 v7, 0x0

    move-object v1, p0

    move-wide v2, p1

    move/from16 v4, p3

    move-object/from16 v6, p5

    invoke-virtual/range {v1 .. v7}, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->onExceptionReceived(JILcom/sec/android/service/health/connectionmanager2/NetException;Ljava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 342
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->resCheckStatus:Lcom/sec/android/app/shealth/backup/server/data/ResponseCheckStatus;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/backup/server/data/ResponseCheckStatus;->checkIsResCodeOK()Z

    move-result v1

    if-nez v1, :cond_4

    .line 344
    new-instance v5, Lcom/sec/android/service/health/connectionmanager2/NetException;

    const/4 v1, -0x6

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->resCheckStatus:Lcom/sec/android/app/shealth/backup/server/data/ResponseCheckStatus;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/backup/server/data/ResponseCheckStatus;->getResCode()I

    move-result v2

    invoke-direct {v5, v1, v2}, Lcom/sec/android/service/health/connectionmanager2/NetException;-><init>(II)V

    const/4 v7, 0x0

    move-object v1, p0

    move-wide v2, p1

    move/from16 v4, p3

    move-object/from16 v6, p5

    invoke-virtual/range {v1 .. v7}, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->onExceptionReceived(JILcom/sec/android/service/health/connectionmanager2/NetException;Ljava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 348
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->mDeviceIdListener:Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker$IHDeviceIdListener;

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->resCheckStatus:Lcom/sec/android/app/shealth/backup/server/data/ResponseCheckStatus;

    iget-object v2, v2, Lcom/sec/android/app/shealth/backup/server/data/ResponseCheckStatus;->deviceid:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker$IHDeviceIdListener;->onLastDeviceId(Ljava/lang/String;)V

    .line 349
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->endTransaction()V

    goto/16 :goto_0

    .line 270
    nop

    :sswitch_data_0
    .sparse-switch
        0x65 -> :sswitch_0
        0x6a -> :sswitch_1
    .end sparse-switch
.end method
