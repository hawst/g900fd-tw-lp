.class Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel$8;
.super Lcom/sec/android/app/shealth/food/utils/MinMaxIntegerInputFilter;
.source "QuickInputImagePanel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;FFI)V
    .locals 0
    .param p2, "x0"    # F
    .param p3, "x1"    # F
    .param p4, "x2"    # I

    .prologue
    .line 398
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel$8;->this$0:Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;

    invoke-direct {p0, p2, p3, p4}, Lcom/sec/android/app/shealth/food/utils/MinMaxIntegerInputFilter;-><init>(FFI)V

    return-void
.end method


# virtual methods
.method public showAlertToast()V
    .locals 1

    .prologue
    .line 405
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel$8;->this$0:Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;

    # invokes: Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->showMinMaxToast()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->access$600(Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;)V

    .line 407
    return-void
.end method

.method public showInvalidInputToast()V
    .locals 4

    .prologue
    .line 411
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel$8;->this$0:Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;

    # getter for: Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mInvalidInputToast:Landroid/widget/Toast;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->access$700(Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;)Landroid/widget/Toast;

    move-result-object v0

    if-nez v0, :cond_0

    .line 413
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel$8;->this$0:Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel$8;->this$0:Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;

    # getter for: Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mViewContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->access$800(Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel$8;->this$0:Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09092e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v1

    # setter for: Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mInvalidInputToast:Landroid/widget/Toast;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->access$702(Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;Landroid/widget/Toast;)Landroid/widget/Toast;

    .line 414
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel$8;->this$0:Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;

    # getter for: Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mInvalidInputToast:Landroid/widget/Toast;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->access$700(Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 421
    :goto_0
    return-void

    .line 419
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel$8;->this$0:Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;

    # getter for: Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mInvalidInputToast:Landroid/widget/Toast;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->access$700(Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
