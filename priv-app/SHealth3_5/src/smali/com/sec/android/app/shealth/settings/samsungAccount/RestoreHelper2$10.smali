.class Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$10;
.super Ljava/lang/Object;
.source "RestoreHelper2.java"

# interfaces
.implements Lcom/sec/android/service/health/samsungaccount/ISamsungUserTokenListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->onExceptionReceived(JILcom/sec/android/service/health/connectionmanager2/NetException;Ljava/lang/Object;Ljava/lang/Object;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

.field final synthetic val$ex:Lcom/sec/android/service/health/connectionmanager2/NetException;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Lcom/sec/android/service/health/connectionmanager2/NetException;)V
    .locals 0

    .prologue
    .line 1081
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$10;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    iput-object p2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$10;->val$ex:Lcom/sec/android/service/health/connectionmanager2/NetException;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceived(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "userToken"    # Ljava/lang/String;
    .param p2, "userId"    # Ljava/lang/String;
    .param p3, "mcc"    # Ljava/lang/String;

    .prologue
    .line 1101
    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "3.0 Restoration: sendRequestForNewUserToken: onReceived()"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1102
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$10;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # invokes: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->requestProfileFromServer(Ljava/lang/String;)V
    invoke-static {v0, p1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$1800(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Ljava/lang/String;)V

    .line 1103
    return-void
.end method

.method public setFailureMessage(Ljava/lang/String;)V
    .locals 2
    .param p1, "errMsg"    # Ljava/lang/String;

    .prologue
    .line 1092
    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "RE-request token, Samsung account app issue occured"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1094
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$10;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->closeServerCheckingPopup()V

    .line 1095
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$10;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$300(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/SamsungAccount;->requestCheckValication(Landroid/content/Context;)V

    .line 1096
    return-void
.end method

.method public setNetworkFailure()V
    .locals 2

    .prologue
    .line 1085
    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "3.0 Restoration: sendRequestForNewUserToken: Network error"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 1086
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$10;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$10;->val$ex:Lcom/sec/android/service/health/connectionmanager2/NetException;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->onException(Ljava/lang/Exception;)V

    .line 1087
    return-void
.end method
