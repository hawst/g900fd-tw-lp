.class public Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;
.super Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;
.source "ExerciseMateLogAdapter.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field cal:Ljava/util/Calendar;

.field dateFormatter:Ljava/text/SimpleDateFormat;

.field dateFormatterNew:Ljava/text/SimpleDateFormat;

.field private dateFormatterTag:Ljava/text/SimpleDateFormat;

.field public hBitmapHashmap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field private isPhotoData:Z

.field private mContext:Landroid/content/Context;

.field private mPhotoDatas:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;",
            ">;"
        }
    .end annotation
.end field

.field viewHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/database/Cursor;Landroid/content/Context;)V
    .locals 3
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 78
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;-><init>(Landroid/database/Cursor;Landroid/content/Context;)V

    .line 63
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->cal:Ljava/util/Calendar;

    .line 64
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, " dd"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->dateFormatterNew:Ljava/text/SimpleDateFormat;

    .line 65
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->getSystemDateFormat()Ljava/text/SimpleDateFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->dateFormatter:Ljava/text/SimpleDateFormat;

    .line 67
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "dd/MM/yyyy"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->dateFormatterTag:Ljava/text/SimpleDateFormat;

    .line 69
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->isPhotoData:Z

    .line 680
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->viewHandler:Landroid/os/Handler;

    .line 79
    iput-object p2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->mContext:Landroid/content/Context;

    .line 82
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->hBitmapHashmap:Ljava/util/HashMap;

    .line 83
    return-void
.end method

.method private getAccessoryName(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1, "device_id"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    .line 418
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " SELECT custom_name, _id FROM user_device WHERE _id = \""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 420
    .local v3, "query":Ljava/lang/String;
    const/4 v6, 0x0

    .line 422
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 423
    if-eqz v6, :cond_1

    .line 424
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 425
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_1

    .line 426
    const-string v0, "custom_name"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 430
    if-eqz v6, :cond_0

    .line 431
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 434
    :cond_0
    :goto_0
    return-object v0

    .line 430
    :cond_1
    if-eqz v6, :cond_2

    .line 431
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    move-object v0, v7

    .line 434
    goto :goto_0

    .line 430
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    .line 431
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method public static getSystemDateFormat()Ljava/text/SimpleDateFormat;
    .locals 3

    .prologue
    .line 665
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "dd/MM/yyyy"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    return-object v0
.end method

.method public static getSystemtxtTimeDateFormat()Ljava/text/SimpleDateFormat;
    .locals 4

    .prologue
    .line 670
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v2

    check-cast v2, Ljava/text/SimpleDateFormat;

    invoke-virtual {v2}, Ljava/text/SimpleDateFormat;->toPattern()Ljava/lang/String;

    move-result-object v1

    .line 672
    .local v1, "pattern":Ljava/lang/String;
    const-string v2, "dd"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 673
    const-string v0, "dd/MM"

    .line 678
    .local v0, "format":Ljava/lang/String;
    :goto_0
    new-instance v2, Ljava/text/SimpleDateFormat;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-direct {v2, v0, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    return-object v2

    .line 675
    .end local v0    # "format":Ljava/lang/String;
    :cond_0
    const-string v0, "MM/dd"

    .restart local v0    # "format":Ljava/lang/String;
    goto :goto_0
.end method

.method private updateImage(JLcom/sec/android/app/shealth/plugins/exercisemate/log/IloadImageReturn;)V
    .locals 2
    .param p1, "exerciseId"    # J
    .param p3, "iloadImageReturn"    # Lcom/sec/android/app/shealth/plugins/exercisemate/log/IloadImageReturn;

    .prologue
    .line 442
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->isPhotoData(J)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 443
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoader;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p3}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoader;-><init>(Landroid/content/Context;Lcom/sec/android/app/shealth/plugins/exercisemate/log/IloadImageReturn;)V

    .line 444
    .local v0, "Loader":Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoader;
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->isPhotoData:Z

    .line 450
    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoader;->execute(J)V

    .line 457
    .end local v0    # "Loader":Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoader;
    :goto_0
    return-void

    .line 453
    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->isPhotoData:Z

    .line 454
    invoke-interface {p3, p1, p2}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/IloadImageReturn;->onNotReturnLoadImage(J)V

    goto :goto_0
.end method


# virtual methods
.method protected bindChildView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;Z)V
    .locals 52
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cursor"    # Landroid/database/Cursor;
    .param p4, "isLastChild"    # Z

    .prologue
    .line 88
    if-nez p1, :cond_0

    .line 414
    :goto_0
    return-void

    .line 91
    :cond_0
    const-wide/16 v9, 0x0

    .line 92
    .local v9, "ExerciseId":J
    if-eqz p3, :cond_1

    .line 94
    const-string v3, "_id"

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    .line 98
    :cond_1
    const v3, 0x7f08016d

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v49

    check-cast v49, Landroid/widget/TextView;

    .line 99
    .local v49, "txtKcal":Landroid/widget/TextView;
    const v3, 0x7f080350

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v20

    .line 100
    .local v20, "deviceInfoImage":Landroid/view/View;
    const v3, 0x7f08034f

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v41

    .line 101
    .local v41, "pedometerImage":Landroid/view/View;
    const v3, 0x7f08034e

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    .line 102
    .local v12, "accessoryImage":Landroid/view/View;
    if-eqz v20, :cond_2

    .line 103
    const/16 v3, 0x8

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 104
    :cond_2
    if-eqz v41, :cond_3

    .line 105
    const/16 v3, 0x8

    move-object/from16 v0, v41

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 106
    :cond_3
    if-eqz v12, :cond_4

    .line 107
    const/16 v3, 0x8

    invoke-virtual {v12, v3}, Landroid/view/View;->setVisibility(I)V

    .line 108
    :cond_4
    const/16 v34, 0x0

    .line 109
    .local v34, "kcal":I
    const-wide/16 v46, 0x0

    .line 110
    .local v46, "time":J
    const-wide/16 v22, 0x0

    .line 111
    .local v22, "duration":J
    const/4 v15, 0x0

    .line 112
    .local v15, "comment":Ljava/lang/String;
    if-eqz p3, :cond_5

    .line 114
    const-string/jumbo v3, "total_calorie"

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v3

    float-to-int v0, v3

    move/from16 v34, v0

    .line 116
    :cond_5
    if-eqz v49, :cond_6

    .line 118
    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    if-eqz v3, :cond_6

    .line 120
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move/from16 v0, v34

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0900b9

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v49

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 121
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move/from16 v0, v34

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0900b9

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-static {v0, v4}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->convertToProperUnitsText(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v49

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 124
    :cond_6
    const v3, 0x7f080171

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v50

    check-cast v50, Landroid/widget/TextView;

    .line 125
    .local v50, "txtTime":Landroid/widget/TextView;
    const v3, 0x7f080352

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v51

    check-cast v51, Landroid/widget/TextView;

    .line 126
    .local v51, "txtTimeDuration":Landroid/widget/TextView;
    if-eqz p3, :cond_7

    .line 128
    const-string/jumbo v3, "start_time"

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v46

    .line 129
    const-string v3, "duration_millisecond"

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v22

    .line 132
    :cond_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->cal:Ljava/util/Calendar;

    move-wide/from16 v0, v46

    invoke-virtual {v3, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 133
    move-object/from16 v0, p2

    move-wide/from16 v1, v46

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->getWeek_three_chars(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v11

    .line 136
    .local v11, "WeekDayString":Ljava/lang/String;
    invoke-static/range {p2 .. p2}, Lcom/sec/android/app/shealth/common/utils/DateFormatHelper;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v3

    invoke-static/range {v46 .. v47}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v50

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 137
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->mContext:Landroid/content/Context;

    move-wide/from16 v0, v22

    invoke-static {v0, v1, v3}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->SecToString(JLandroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v51

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 138
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->mContext:Landroid/content/Context;

    move-wide/from16 v0, v22

    invoke-static {v0, v1, v3}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->SecToContentDescription(JLandroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v51

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 139
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->getSystemtxtTimeDateFormat()Ljava/text/SimpleDateFormat;

    move-result-object v3

    invoke-static/range {v46 .. v47}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    .line 140
    .local v18, "date":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->dateFormatterTag:Ljava/text/SimpleDateFormat;

    invoke-static/range {v46 .. v47}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 142
    const v3, 0x7f080169

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v43

    check-cast v43, Landroid/widget/LinearLayout;

    .line 144
    .local v43, "teamHeader":Landroid/widget/LinearLayout;
    if-eqz p3, :cond_8

    .line 146
    const-string v3, "comment"

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 154
    :cond_8
    new-instance v24, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;

    invoke-direct/range {v24 .. v24}, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;-><init>()V

    .line 155
    .local v24, "exerciseInfo":Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;
    if-eqz p3, :cond_9

    .line 157
    const-string/jumbo v3, "name"

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;->setName(Ljava/lang/String;)V

    .line 159
    :cond_9
    const/16 v37, 0x0

    .line 160
    .local v37, "name_category":Ljava/lang/String;
    const/16 v38, 0x0

    .line 161
    .local v38, "name_exercise":Ljava/lang/String;
    const-wide/16 v31, 0x0

    .line 162
    .local v31, "info_id":J
    if-eqz p3, :cond_a

    .line 164
    const-string v3, "exercise_info__id"

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v31

    .line 167
    :cond_a
    const/16 v16, 0x0

    .line 170
    .local v16, "cursor2":Landroid/database/Cursor;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "select ca._id, ca.name  ca_Name, ex._id, ex.name ex_Name from exercise_info ex left outer join exercise_info ca on ca._id = ex.exercise_info__id  where ex._id ="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, v31

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v16

    .line 171
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_b

    .line 172
    const-string v3, "ca_Name"

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v37

    .line 173
    const-string v3, "ex_Name"

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v38

    .line 174
    const-string v3, "ExerciseMateLogAdapter"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "name_categoty = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v37

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "name_exercise ="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v38

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 178
    :cond_b
    if-eqz v16, :cond_c

    .line 179
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    .line 181
    :cond_c
    if-nez v37, :cond_d

    if-eqz v38, :cond_d

    .line 182
    move-object/from16 v37, v38

    .line 184
    :cond_d
    move-object/from16 v0, p0

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->upgradeSpecificCode(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_12

    .line 186
    move-object/from16 v0, p0

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->upgradeSpecificCode_getNameId(Ljava/lang/String;)I

    move-result v25

    .line 198
    .local v25, "exerciseInfoNameResId":I
    :goto_1
    const-string v3, "ExerciseMateLogAdapter"

    move-object/from16 v0, v37

    invoke-static {v3, v0}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 201
    move-object/from16 v0, p0

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->upgradeSpecificCode(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_15

    .line 203
    move-object/from16 v0, p0

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->upgradeSpecificCode_getImageId(Ljava/lang/String;)I

    move-result v29

    .line 220
    .local v29, "imgExerciseTypeImgId":I
    :goto_2
    const v3, 0x7f08034c

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v28

    check-cast v28, Landroid/widget/ImageView;

    .line 221
    .local v28, "imgExerciseType":Landroid/widget/ImageView;
    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 223
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->hBitmapHashmap:Ljava/util/HashMap;

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1a

    .line 224
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->hBitmapHashmap:Ljava/util/HashMap;

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Landroid/graphics/drawable/Drawable;

    .line 225
    .local v27, "imgD":Landroid/graphics/drawable/Drawable;
    if-nez v27, :cond_19

    .line 226
    invoke-virtual/range {v28 .. v29}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 227
    const v3, 0x7f020335

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 278
    .end local v27    # "imgD":Landroid/graphics/drawable/Drawable;
    :cond_e
    :goto_3
    const v3, 0x7f08034d

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v48

    check-cast v48, Landroid/widget/TextView;

    .line 279
    .local v48, "txtGroupExerciseInfo":Landroid/widget/TextView;
    const/4 v3, -0x1

    move/from16 v0, v25

    if-ne v0, v3, :cond_1e

    .line 280
    invoke-virtual/range {v24 .. v24}, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;->getName()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1d

    invoke-virtual/range {v24 .. v24}, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_1d

    .line 281
    invoke-virtual/range {v24 .. v24}, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;->getName()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v48

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 293
    :goto_4
    if-eqz p3, :cond_1f

    const-string/jumbo v3, "user_device__id"

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-eqz v3, :cond_1f

    .line 332
    :cond_f
    :goto_5
    const-string v3, ""

    invoke-virtual {v3, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_24

    if-eqz v15, :cond_24

    .line 333
    const v3, 0x7f080170

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 334
    const v3, 0x7f080170

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    sget-object v4, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;->TYPE_USER_CUSTOM:Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;

    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f09007c

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5, v15}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->setHoverPopupListener(Landroid/view/View;Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;Ljava/lang/String;Ljava/lang/String;)V

    .line 340
    :goto_6
    new-instance v36, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    move-object/from16 v0, v36

    invoke-direct {v0, v3}, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;-><init>(Landroid/content/Context;)V

    .line 343
    .local v36, "mergeCal":Lcom/sec/android/app/shealth/common/utils/MergeCalorie;
    invoke-static/range {v46 .. v47}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v3

    invoke-static/range {v46 .. v47}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v5

    const-wide/16 v7, 0x1

    add-long/2addr v5, v7

    move-object/from16 v0, v36

    invoke-virtual {v0, v3, v4, v5, v6}, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;->getMergedCalorieGetEarlier(JJ)F

    move-result v3

    float-to-int v0, v3

    move/from16 v42, v0

    .line 345
    .local v42, "sum_Kal":I
    const-string v3, "SUM_KCAL"

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v33

    .line 346
    .local v33, "isHeaderView":Z
    if-nez v33, :cond_25

    .line 347
    const v3, 0x7f08034b

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v45

    check-cast v45, Landroid/widget/TextView;

    .line 348
    .local v45, "teamHeaderWeight":Landroid/widget/TextView;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v42

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0900b9

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v45

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 349
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->mContext:Landroid/content/Context;

    const v5, 0x7f090939

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v42

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0900b9

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->convertToProperUnitsText(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v45

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 351
    const v3, 0x7f080349

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 352
    const v3, 0x7f080129

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 357
    .end local v45    # "teamHeaderWeight":Landroid/widget/TextView;
    :goto_7
    if-nez v33, :cond_28

    .line 358
    const/4 v3, 0x0

    move-object/from16 v0, v43

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 359
    const v3, 0x7f08016a

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v44

    check-cast v44, Landroid/widget/TextView;

    .line 360
    .local v44, "teamHeaderDate":Landroid/widget/TextView;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v44

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 361
    new-instance v3, Ljava/text/DateFormatSymbols;

    invoke-direct {v3}, Ljava/text/DateFormatSymbols;-><init>()V

    invoke-virtual {v3}, Ljava/text/DateFormatSymbols;->getMonths()[Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->cal:Ljava/util/Calendar;

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Ljava/util/Calendar;->get(I)I

    move-result v4

    aget-object v17, v3, v4

    .line 362
    .local v17, "dMonth":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->dateFormatterNew:Ljava/text/SimpleDateFormat;

    invoke-static/range {v46 .. v47}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    .line 363
    .local v19, "dateNew":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget-object v3, v3, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v35

    .line 364
    .local v35, "language":Ljava/lang/String;
    const-string v3, "ko"

    move-object/from16 v0, v35

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_26

    .line 365
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->cal:Ljava/util/Calendar;

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Ljava/util/Calendar;->get(I)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090212

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090213

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v4, Ljava/text/DateFormatSymbols;

    invoke-direct {v4}, Ljava/text/DateFormatSymbols;-><init>()V

    invoke-virtual {v4}, Ljava/text/DateFormatSymbols;->getWeekdays()[Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->cal:Ljava/util/Calendar;

    const/4 v6, 0x7

    invoke-virtual {v5, v6}, Ljava/util/Calendar;->get(I)I

    move-result v5

    aget-object v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v44

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 383
    .end local v17    # "dMonth":Ljava/lang/String;
    .end local v19    # "dateNew":Ljava/lang/String;
    .end local v35    # "language":Ljava/lang/String;
    .end local v44    # "teamHeaderDate":Landroid/widget/TextView;
    :goto_8
    if-eqz p4, :cond_29

    .line 384
    const v3, 0x7f08002d

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 390
    :goto_9
    const v3, 0x7f080172

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/CheckBox;

    .line 391
    .local v14, "cb":Landroid/widget/CheckBox;
    move-object/from16 v0, p0

    invoke-virtual {v14, v0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 393
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x106000d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/view/View;->setBackgroundColor(I)V

    .line 399
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->isMenuDeleteMode()Z

    move-result v3

    if-eqz v3, :cond_2b

    .line 401
    const/4 v3, 0x0

    invoke-virtual {v14, v3}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 402
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->isCheckAll()Z

    move-result v3

    if-nez v3, :cond_10

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->isLogSelected(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2a

    :cond_10
    const/4 v3, 0x1

    :goto_a
    invoke-virtual {v14, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 403
    invoke-virtual/range {v48 .. v48}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v39

    check-cast v39, Landroid/widget/LinearLayout$LayoutParams;

    .line 404
    .local v39, "params":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0647

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    move-object/from16 v0, v39

    iput v3, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 405
    move-object/from16 v0, v48

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    .line 178
    .end local v14    # "cb":Landroid/widget/CheckBox;
    .end local v25    # "exerciseInfoNameResId":I
    .end local v28    # "imgExerciseType":Landroid/widget/ImageView;
    .end local v29    # "imgExerciseTypeImgId":I
    .end local v33    # "isHeaderView":Z
    .end local v36    # "mergeCal":Lcom/sec/android/app/shealth/common/utils/MergeCalorie;
    .end local v39    # "params":Landroid/widget/LinearLayout$LayoutParams;
    .end local v42    # "sum_Kal":I
    .end local v48    # "txtGroupExerciseInfo":Landroid/widget/TextView;
    :catchall_0
    move-exception v3

    if-eqz v16, :cond_11

    .line 179
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    :cond_11
    throw v3

    .line 190
    :cond_12
    const-string v3, "RealtimeWorkout"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_13

    .line 191
    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    move-object/from16 v0, v38

    invoke-virtual {v3, v0}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v25

    .restart local v25    # "exerciseInfoNameResId":I
    goto/16 :goto_1

    .line 192
    .end local v25    # "exerciseInfoNameResId":I
    :cond_13
    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    invoke-virtual {v3}, Ljava/util/TreeMap;->keySet()Ljava/util/Set;

    move-result-object v3

    move-object/from16 v0, v37

    invoke-interface {v3, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_14

    .line 193
    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    move-object/from16 v0, v37

    invoke-virtual {v3, v0}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v25

    .restart local v25    # "exerciseInfoNameResId":I
    goto/16 :goto_1

    .line 195
    .end local v25    # "exerciseInfoNameResId":I
    :cond_14
    const/16 v25, -0x1

    .restart local v25    # "exerciseInfoNameResId":I
    goto/16 :goto_1

    .line 207
    :cond_15
    const-string v3, "RealtimeWorkout"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_16

    .line 208
    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    move-object/from16 v0, v38

    invoke-virtual {v3, v0}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v29

    .restart local v29    # "imgExerciseTypeImgId":I
    goto/16 :goto_2

    .line 209
    .end local v29    # "imgExerciseTypeImgId":I
    :cond_16
    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    invoke-virtual {v3}, Ljava/util/TreeMap;->keySet()Ljava/util/Set;

    move-result-object v3

    move-object/from16 v0, v37

    invoke-interface {v3, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_17

    .line 210
    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    move-object/from16 v0, v37

    invoke-virtual {v3, v0}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v29

    .restart local v29    # "imgExerciseTypeImgId":I
    goto/16 :goto_2

    .line 211
    .end local v29    # "imgExerciseTypeImgId":I
    :cond_17
    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES_XL:Ljava/util/TreeMap;

    invoke-virtual {v3}, Ljava/util/TreeMap;->keySet()Ljava/util/Set;

    move-result-object v3

    move-object/from16 v0, v37

    invoke-interface {v3, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_18

    .line 212
    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES_XL:Ljava/util/TreeMap;

    move-object/from16 v0, v37

    invoke-virtual {v3, v0}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v29

    .restart local v29    # "imgExerciseTypeImgId":I
    goto/16 :goto_2

    .line 214
    .end local v29    # "imgExerciseTypeImgId":I
    :cond_18
    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v4, "Walking"

    invoke-virtual {v3, v4}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v29

    .restart local v29    # "imgExerciseTypeImgId":I
    goto/16 :goto_2

    .line 229
    .restart local v27    # "imgD":Landroid/graphics/drawable/Drawable;
    .restart local v28    # "imgExerciseType":Landroid/widget/ImageView;
    :cond_19
    const/4 v3, 0x0

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 230
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->hBitmapHashmap:Ljava/util/HashMap;

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_3

    .line 233
    .end local v27    # "imgD":Landroid/graphics/drawable/Drawable;
    :cond_1a
    invoke-virtual/range {v28 .. v29}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 234
    const v3, 0x7f020335

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 235
    new-instance v3, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter$1;

    move-object/from16 v0, p0

    move-object/from16 v1, v28

    move/from16 v2, v29

    invoke-direct {v3, v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter$1;-><init>(Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;Landroid/widget/ImageView;I)V

    move-object/from16 v0, p0

    invoke-direct {v0, v9, v10, v3}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->updateImage(JLcom/sec/android/app/shealth/plugins/exercisemate/log/IloadImageReturn;)V

    .line 263
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->isPhotoData:Z

    if-eqz v3, :cond_1c

    .line 264
    new-instance v40, Ljava/util/ArrayList;

    invoke-direct/range {v40 .. v40}, Ljava/util/ArrayList;-><init>()V

    .line 265
    .local v40, "path":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/16 v26, 0x0

    .local v26, "i":I
    :goto_b
    const/4 v3, 0x4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->mPhotoDatas:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    move/from16 v0, v26

    if-ge v0, v3, :cond_1b

    .line 267
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->mPhotoDatas:Ljava/util/ArrayList;

    move/from16 v0, v26

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->getFilePath()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v40

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 265
    add-int/lit8 v26, v26, 0x1

    goto :goto_b

    .line 269
    :cond_1b
    invoke-interface/range {v40 .. v40}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_e

    .line 270
    sget-object v3, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;->TYPE_MULTI_PIC:Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;

    move-object/from16 v0, v28

    move-object/from16 v1, v40

    invoke-static {v0, v3, v1}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->setHoverPopupListener(Landroid/view/View;Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;Ljava/util/List;)V

    goto/16 :goto_3

    .line 274
    .end local v26    # "i":I
    .end local v40    # "path":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_1c
    sget-object v3, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;->TYPE_NONE:Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;

    const-string v4, ""

    const-string v5, ""

    move-object/from16 v0, v28

    invoke-static {v0, v3, v4, v5}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->setHoverPopupListener(Landroid/view/View;Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 283
    .restart local v48    # "txtGroupExerciseInfo":Landroid/widget/TextView;
    :cond_1d
    const v25, 0x7f090ab9

    .line 284
    move-object/from16 v0, v48

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_4

    .line 287
    :cond_1e
    move-object/from16 v0, v48

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_4

    .line 296
    :cond_1f
    const/16 v30, 0x0

    .line 297
    .local v30, "index":Ljava/lang/String;
    if-eqz p3, :cond_20

    .line 299
    const-string/jumbo v3, "user_device__id"

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v30

    .line 301
    :cond_20
    const/16 v21, 0x0

    .line 302
    .local v21, "deviceType":I
    if-eqz v30, :cond_21

    const-string v3, "_"

    move-object/from16 v0, v30

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    if-lez v3, :cond_21

    .line 303
    const-string v3, "_"

    move-object/from16 v0, v30

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aget-object v3, v3, v4

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v21

    .line 305
    :cond_21
    sparse-switch v21, :sswitch_data_0

    .line 322
    if-eqz v20, :cond_22

    .line 323
    const/16 v3, 0x8

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 324
    :cond_22
    if-eqz v41, :cond_23

    .line 325
    const/16 v3, 0x8

    move-object/from16 v0, v41

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 326
    :cond_23
    if-eqz v12, :cond_f

    .line 327
    const/16 v3, 0x8

    invoke-virtual {v12, v3}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_5

    .line 314
    :sswitch_0
    if-eqz v12, :cond_f

    move-object v13, v12

    .line 315
    check-cast v13, Landroid/widget/ImageView;

    .line 316
    .local v13, "accessory_image":Landroid/widget/ImageView;
    sget-object v3, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;->TYPE_TOOL_TIP:Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->getAccessoryName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, ""

    invoke-static {v13, v3, v4, v5}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->setHoverPopupListener(Landroid/view/View;Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;Ljava/lang/String;Ljava/lang/String;)V

    .line 317
    const/4 v3, 0x0

    invoke-virtual {v13, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_5

    .line 336
    .end local v13    # "accessory_image":Landroid/widget/ImageView;
    .end local v21    # "deviceType":I
    .end local v30    # "index":Ljava/lang/String;
    :cond_24
    const v3, 0x7f080170

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_6

    .line 354
    .restart local v33    # "isHeaderView":Z
    .restart local v36    # "mergeCal":Lcom/sec/android/app/shealth/common/utils/MergeCalorie;
    .restart local v42    # "sum_Kal":I
    :cond_25
    const v3, 0x7f080129

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_7

    .line 372
    .restart local v17    # "dMonth":Ljava/lang/String;
    .restart local v19    # "dateNew":Ljava/lang/String;
    .restart local v35    # "language":Ljava/lang/String;
    .restart local v44    # "teamHeaderDate":Landroid/widget/TextView;
    :cond_26
    const-string v3, "fr"

    move-object/from16 v0, v35

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_27

    .line 373
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v4, Ljava/text/DateFormatSymbols;

    invoke-direct {v4}, Ljava/text/DateFormatSymbols;-><init>()V

    invoke-virtual {v4}, Ljava/text/DateFormatSymbols;->getWeekdays()[Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->cal:Ljava/util/Calendar;

    const/4 v6, 0x7

    invoke-virtual {v5, v6}, Ljava/util/Calendar;->get(I)I

    move-result v5

    aget-object v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v44

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_8

    .line 376
    :cond_27
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v4, Ljava/text/DateFormatSymbols;

    invoke-direct {v4}, Ljava/text/DateFormatSymbols;-><init>()V

    invoke-virtual {v4}, Ljava/text/DateFormatSymbols;->getWeekdays()[Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->cal:Ljava/util/Calendar;

    const/4 v6, 0x7

    invoke-virtual {v5, v6}, Ljava/util/Calendar;->get(I)I

    move-result v5

    aget-object v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v44

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_8

    .line 379
    .end local v17    # "dMonth":Ljava/lang/String;
    .end local v19    # "dateNew":Ljava/lang/String;
    .end local v35    # "language":Ljava/lang/String;
    .end local v44    # "teamHeaderDate":Landroid/widget/TextView;
    :cond_28
    const/16 v3, 0x8

    move-object/from16 v0, v43

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_8

    .line 386
    :cond_29
    const v3, 0x7f08002d

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_9

    .line 402
    .restart local v14    # "cb":Landroid/widget/CheckBox;
    :cond_2a
    const/4 v3, 0x0

    goto/16 :goto_a

    .line 407
    :cond_2b
    const/16 v3, 0x8

    invoke-virtual {v14, v3}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 408
    const/4 v3, 0x0

    invoke-virtual {v14, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 409
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->clearAllSelection()V

    .line 410
    invoke-virtual/range {v48 .. v48}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v39

    check-cast v39, Landroid/widget/LinearLayout$LayoutParams;

    .line 411
    .restart local v39    # "params":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0646

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    move-object/from16 v0, v39

    iput v3, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 412
    move-object/from16 v0, v48

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    .line 305
    nop

    :sswitch_data_0
    .sparse-switch
        0x271d -> :sswitch_0
        0x2723 -> :sswitch_0
        0x2724 -> :sswitch_0
        0x2726 -> :sswitch_0
        0x2728 -> :sswitch_0
        0x272e -> :sswitch_0
        0x272f -> :sswitch_0
        0x2730 -> :sswitch_0
    .end sparse-switch
.end method

.method protected bindGroupView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;Z)V
    .locals 26
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cursor"    # Landroid/database/Cursor;
    .param p4, "isExpanded"    # Z

    .prologue
    .line 480
    const v20, 0x7f080372

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v19

    check-cast v19, Landroid/widget/TextView;

    .line 482
    .local v19, "txtTime":Landroid/widget/TextView;
    const-string/jumbo v20, "start_time"

    move-object/from16 v0, p3

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    move-object/from16 v0, p3

    move/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v17

    .line 483
    .local v17, "time":J
    const-wide/16 v9, 0x0

    .line 491
    .local v9, "kcal":J
    new-instance v12, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-direct {v12, v0}, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;-><init>(Landroid/content/Context;)V

    .line 492
    .local v12, "mergeCal":Lcom/sec/android/app/shealth/common/utils/MergeCalorie;
    invoke-static/range {v17 .. v18}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfMonth(J)J

    move-result-wide v20

    invoke-static/range {v17 .. v18}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfMonth(J)J

    move-result-wide v22

    const-wide/16 v24, 0x1

    add-long v22, v22, v24

    move-wide/from16 v0, v20

    move-wide/from16 v2, v22

    invoke-virtual {v12, v0, v1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;->getMergedCalorieGetEarlier(JJ)F

    move-result v20

    move/from16 v0, v20

    float-to-double v15, v0

    .line 494
    .local v15, "sum_kcal":D
    const-string v20, "COUNT"

    move-object/from16 v0, p3

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    move-object/from16 v0, p3

    move/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v20

    if-lez v20, :cond_0

    .line 495
    const-string v20, "COUNT"

    move-object/from16 v0, p3

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    move-object/from16 v0, p3

    move/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v20

    move/from16 v0, v20

    int-to-double v0, v0

    move-wide/from16 v20, v0

    div-double v20, v15, v20

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->round(D)J

    move-result-wide v9

    .line 497
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->cal:Ljava/util/Calendar;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-wide/from16 v1, v17

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 498
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->cal:Ljava/util/Calendar;

    move-object/from16 v20, v0

    const/16 v21, 0x2

    invoke-virtual/range {v20 .. v21}, Ljava/util/Calendar;->get(I)I

    move-result v20

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->getMonthString(I)Ljava/lang/String;

    move-result-object v14

    .line 499
    .local v14, "monthSubString":Ljava/lang/String;
    new-instance v20, Ljava/text/DateFormatSymbols;

    invoke-direct/range {v20 .. v20}, Ljava/text/DateFormatSymbols;-><init>()V

    invoke-virtual/range {v20 .. v20}, Ljava/text/DateFormatSymbols;->getMonths()[Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->cal:Ljava/util/Calendar;

    move-object/from16 v21, v0

    const/16 v22, 0x2

    invoke-virtual/range {v21 .. v22}, Ljava/util/Calendar;->get(I)I

    move-result v21

    aget-object v13, v20, v21

    .line 500
    .local v13, "monthFullName":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v20

    move-object/from16 v0, v20

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v11

    .line 502
    .local v11, "language":Ljava/lang/String;
    const/4 v5, 0x0

    .line 503
    .local v5, "date":Ljava/lang/String;
    const-string v20, "ko"

    move-object/from16 v0, v20

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-nez v20, :cond_1

    const-string/jumbo v20, "zh"

    move-object/from16 v0, v20

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-nez v20, :cond_1

    const-string v20, "ja"

    move-object/from16 v0, v20

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_3

    .line 504
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-wide/from16 v1, v17

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getKoreanMonthGroupDateString(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v5

    .line 510
    :goto_0
    move-object v6, v5

    .line 513
    .local v6, "dateSubString":Ljava/lang/String;
    const-string v8, ""

    .line 514
    .local v8, "description":Ljava/lang/String;
    const-string v20, "ko"

    move-object/from16 v0, v20

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-nez v20, :cond_2

    const-string/jumbo v20, "zh"

    move-object/from16 v0, v20

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-nez v20, :cond_2

    const-string v20, "ja"

    move-object/from16 v0, v20

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_5

    .line 515
    :cond_2
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->dateFormatter:Ljava/text/SimpleDateFormat;

    move-object/from16 v21, v0

    invoke-static/range {v17 .. v18}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v21

    const/16 v22, 0x6

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    const v22, 0x7f090066

    invoke-virtual/range {v21 .. v22}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " ("

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    const v22, 0x7f090ae0

    invoke-virtual/range {v21 .. v22}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    const v22, 0x7f0900b9

    invoke-virtual/range {v21 .. v22}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ")"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 516
    .local v7, "datetext":Ljava/lang/String;
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->dateFormatter:Ljava/text/SimpleDateFormat;

    move-object/from16 v21, v0

    invoke-static/range {v17 .. v18}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v21

    const/16 v22, 0x6

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    const v22, 0x7f090066

    invoke-virtual/range {v21 .. v22}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    const v22, 0x7f090204

    invoke-virtual/range {v21 .. v22}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    const v23, 0x7f0900b9

    invoke-virtual/range {v22 .. v23}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->convertToProperUnitsText(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 521
    :goto_1
    move-object/from16 v0, v19

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 522
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ", "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    const v22, 0x7f09021c

    invoke-virtual/range {v21 .. v22}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    if-eqz p4, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    const v22, 0x7f0901ef

    move-object/from16 v0, v20

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v20

    :goto_2
    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 525
    const v20, 0x7f080374

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    .line 526
    .local v4, "arrow":Landroid/widget/ImageView;
    if-eqz p4, :cond_7

    .line 527
    sget-object v20, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;->TYPE_TOOL_TIP:Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    const v22, 0x7f09021b

    invoke-virtual/range {v21 .. v22}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v21

    const/16 v22, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-static {v4, v0, v1, v2}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->setHoverPopupListener(Landroid/view/View;Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;Ljava/lang/String;Ljava/lang/String;)V

    .line 535
    :goto_3
    if-nez p4, :cond_8

    const/16 v20, 0x1

    :goto_4
    move/from16 v0, v20

    invoke-virtual {v4, v0}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 536
    return-void

    .line 505
    .end local v4    # "arrow":Landroid/widget/ImageView;
    .end local v6    # "dateSubString":Ljava/lang/String;
    .end local v7    # "datetext":Ljava/lang/String;
    .end local v8    # "description":Ljava/lang/String;
    :cond_3
    const-string v20, "fi"

    move-object/from16 v0, v20

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_4

    .line 506
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getMonthGroupDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v20

    invoke-static/range {v17 .. v18}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_0

    .line 508
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getMonthGroupDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v20

    invoke-static/range {v17 .. v18}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_0

    .line 518
    .restart local v6    # "dateSubString":Ljava/lang/String;
    .restart local v8    # "description":Ljava/lang/String;
    :cond_5
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " ("

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    const v22, 0x7f090ae0

    invoke-virtual/range {v21 .. v22}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    const v22, 0x7f0900b9

    invoke-virtual/range {v21 .. v22}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ")"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 519
    .restart local v7    # "datetext":Ljava/lang/String;
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    const v22, 0x7f090204

    invoke-virtual/range {v21 .. v22}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    const v23, 0x7f0900b9

    invoke-virtual/range {v22 .. v23}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->convertToProperUnitsText(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_1

    .line 522
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    const v22, 0x7f090217

    move-object/from16 v0, v20

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v20

    goto/16 :goto_2

    .line 531
    .restart local v4    # "arrow":Landroid/widget/ImageView;
    :cond_7
    sget-object v20, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;->TYPE_TOOL_TIP:Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    const v22, 0x7f09021a

    invoke-virtual/range {v21 .. v22}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v21

    const/16 v22, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-static {v4, v0, v1, v2}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->setHoverPopupListener(Landroid/view/View;Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 535
    :cond_8
    const/16 v20, 0x0

    goto/16 :goto_4
.end method

.method protected getChildrenCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 16
    .param p1, "groupCursor"    # Landroid/database/Cursor;

    .prologue
    .line 545
    const-string v4, ""

    .line 547
    .local v4, "query":Ljava/lang/String;
    const-string/jumbo v1, "start_time"

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v13

    .line 548
    .local v13, "time":J
    const/4 v8, 0x0

    .line 550
    .local v8, "cursor":Landroid/database/Cursor;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "select A.[_id], A.[total_calorie], A.[comment], A.[exercise_type], A.[duration_millisecond], A.[start_time] ,A.[exercise_info__id] , A.[sync_status] ,DE.[user_device__id], EXER_INFO.[name], B.SUM_KCAL from exercise AS A  JOIN  exercise_info AS EXER_INFO ON A.[exercise_info__id] = EXER_INFO.[_id] LEFT OUTER JOIN  exercise_device_info AS DE ON A.[_id] = DE.[exercise__id]   LEFT OUTER JOIN (SELECT C.[_id], C.[total_calorie] , ifnull(SUM(ROUND(C.[total_calorie])), 0) as SUM_KCAL, MAX(C.[_id]) from exercise as C where end_time > 0 AND exercise_type != 20003 AND exercise_info__id!=18001 group by strftime(\"%d-%m-%Y\", (C.[start_time]/1000),\'unixepoch\',\'localtime\')) as B on A.[_id] = B.[_id] where A.start_time >= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v13, v14}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfMonth(J)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and A."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "start_time"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " <= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v13, v14}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfMonth(J)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and A."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "end_time"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " > "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " AND "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "exercise_type"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " != "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x4e23

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " AND A.["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "exercise_info__id"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] !="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x4651

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " order by A.[_id] desc, strftime(\"%d-%m-%Y\", (A.["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "start_time"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]/1000),\'unixepoch\',\'localtime\') desc, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "start_time"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " desc"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 588
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 590
    sget v1, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->SELECTED_FILTER_INDEX:I

    if-eqz v1, :cond_8

    .line 591
    const-wide/16 v9, -0x1

    .line 592
    .local v9, "startOfDay":J
    const/4 v15, 0x0

    .line 593
    .local v15, "totalCalorie":I
    const/4 v7, 0x0

    .line 594
    .local v7, "c":Landroid/database/MatrixCursor;
    if-eqz v8, :cond_7

    .line 595
    new-instance v7, Landroid/database/MatrixCursor;

    .end local v7    # "c":Landroid/database/MatrixCursor;
    const/4 v1, 0x7

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "_id"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "total_calorie"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "SUM_KCAL"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "comment"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "start_time"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string/jumbo v3, "name"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string/jumbo v3, "user_device__id"

    aput-object v3, v1, v2

    invoke-direct {v7, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 596
    .restart local v7    # "c":Landroid/database/MatrixCursor;
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    :goto_0
    invoke-interface {v8}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v1

    if-nez v1, :cond_6

    .line 597
    const-string/jumbo v1, "start_time"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v11

    .line 598
    .local v11, "startTime":J
    invoke-static {v11, v12}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v1

    cmp-long v1, v9, v1

    if-eqz v1, :cond_0

    .line 599
    invoke-static {v11, v12}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v1

    invoke-static {v11, v12}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v5

    invoke-static {v1, v2, v5, v6}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->getMergedCalorieGetEarlier(JJ)I

    move-result v15

    .line 600
    invoke-static {v11, v12}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v9

    .line 602
    :cond_0
    sget v1, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->SELECTED_FILTER_INDEX:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    invoke-static {v11, v12, v15}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->isGoalAchived(JI)Z

    move-result v1

    if-nez v1, :cond_2

    :cond_1
    sget v1, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->SELECTED_FILTER_INDEX:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_3

    invoke-static {v11, v12, v15}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->isGoalAchived(JI)Z

    move-result v1

    if-nez v1, :cond_3

    .line 604
    :cond_2
    const/4 v1, 0x7

    new-array v2, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v3, "_id"

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v1

    const/4 v1, 0x1

    const-string/jumbo v3, "total_calorie"

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v1

    const/4 v3, 0x2

    const-string v1, "SUM_KCAL"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x0

    :goto_1
    aput-object v1, v2, v3

    const/4 v3, 0x3

    const-string v1, "comment"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x0

    :goto_2
    aput-object v1, v2, v3

    const/4 v1, 0x4

    const-string/jumbo v3, "start_time"

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v1

    const/4 v1, 0x5

    const-string/jumbo v3, "name"

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    const/4 v1, 0x6

    const-string/jumbo v3, "user_device__id"

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    invoke-virtual {v7, v2}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 596
    :cond_3
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    goto/16 :goto_0

    .line 604
    :cond_4
    const-string v1, "SUM_KCAL"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    goto :goto_1

    :cond_5
    const-string v1, "comment"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 615
    .end local v11    # "startTime":J
    :cond_6
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 619
    .end local v7    # "c":Landroid/database/MatrixCursor;
    .end local v9    # "startOfDay":J
    .end local v15    # "totalCalorie":I
    :cond_7
    :goto_3
    return-object v7

    :cond_8
    move-object v7, v8

    goto :goto_3
.end method

.method protected getColumnNameForCreateTime()Ljava/lang/String;
    .locals 1

    .prologue
    .line 661
    const-string/jumbo v0, "start_time"

    return-object v0
.end method

.method protected getColumnNameForID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 656
    const-string v0, "_id"

    return-object v0
.end method

.method public getMonthString(I)Ljava/lang/String;
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 539
    new-instance v1, Ljava/text/DateFormatSymbols;

    invoke-direct {v1}, Ljava/text/DateFormatSymbols;-><init>()V

    invoke-virtual {v1}, Ljava/text/DateFormatSymbols;->getShortMonths()[Ljava/lang/String;

    move-result-object v1

    aget-object v0, v1, p1

    .line 540
    .local v0, "month":Ljava/lang/String;
    return-object v0
.end method

.method protected getTotalChildCount()I
    .locals 4

    .prologue
    .line 641
    const/4 v0, 0x0

    .line 643
    .local v0, "childCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->getGroupCount()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 645
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_1
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->getChildrenCount(I)I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 647
    add-int/lit8 v0, v0, 0x1

    .line 645
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 643
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 651
    .end local v2    # "j":I
    :cond_1
    return v0
.end method

.method public isPhotoData(J)Z
    .locals 7
    .param p1, "rowId"    # J

    .prologue
    .line 459
    const/4 v6, 0x0

    .line 462
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "exercise__id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 463
    .local v3, "selection":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$ExercisePhoto;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 464
    invoke-static {v6}, Lcom/sec/android/app/shealth/plugins/common/utils/ExercisePhotoUtils;->getExercisePhotoDataList(Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->mPhotoDatas:Ljava/util/ArrayList;

    .line 465
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->mPhotoDatas:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-lez v0, :cond_1

    .line 466
    const/4 v0, 0x1

    .line 469
    if-eqz v6, :cond_0

    .line 470
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 472
    :cond_0
    :goto_0
    return v0

    .line 469
    :cond_1
    if-eqz v6, :cond_2

    .line 470
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 472
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 469
    .end local v3    # "selection":Ljava/lang/String;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    .line 470
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method protected newChildView(Landroid/content/Context;Landroid/database/Cursor;ZLandroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cursor"    # Landroid/database/Cursor;
    .param p3, "isLastChild"    # Z
    .param p4, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 625
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 626
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x7f0300b5

    const/4 v3, 0x0

    invoke-virtual {v0, v2, p4, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 628
    .local v1, "v":Landroid/view/View;
    return-object v1
.end method

.method protected newGroupView(Landroid/content/Context;Landroid/database/Cursor;ZLandroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cursor"    # Landroid/database/Cursor;
    .param p3, "isExpanded"    # Z
    .param p4, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 633
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 635
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x7f0300ba

    const/4 v3, 0x0

    invoke-virtual {v0, v2, p4, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 636
    .local v1, "v":Landroid/view/View;
    return-object v1
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 1
    .param p1, "button"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    .line 685
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter$2;

    invoke-direct {v0, p0, p1, p2}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter$2;-><init>(Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;Landroid/widget/CompoundButton;Z)V

    invoke-virtual {p1, v0}, Landroid/widget/CompoundButton;->post(Ljava/lang/Runnable;)Z

    .line 691
    return-void
.end method

.method public upgradeSpecificCode(Ljava/lang/String;)Z
    .locals 1
    .param p1, "l_name_exercise"    # Ljava/lang/String;

    .prologue
    .line 695
    const-string v0, "Ballroom Dance, Slow"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Pull-up(Chin-ups)"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Pull-up ( Chin-ups )"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Martial arts, Moderate pace"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Sit-up ( Sit-ups )"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Push-up ( Press-ups )"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Fast Walking"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Walking Downstairs"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Martial arts, Slower pace"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Walking Upstairs"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Tai chi, General"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Running"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 698
    :cond_0
    const/4 v0, 0x1

    .line 701
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public upgradeSpecificCode_getImageId(Ljava/lang/String;)I
    .locals 2
    .param p1, "l_name_exercise"    # Ljava/lang/String;

    .prologue
    .line 722
    const-string v0, "Fast Walking"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 724
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Power walking"

    invoke-virtual {v0, v1}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 732
    :goto_0
    return v0

    .line 726
    :cond_0
    const-string v0, "Tai chi, General"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 728
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "T\'ai chi"

    invoke-virtual {v0, v1}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    .line 732
    :cond_1
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    invoke-virtual {v0, p1}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public upgradeSpecificCode_getNameId(Ljava/lang/String;)I
    .locals 2
    .param p1, "l_name_exercise"    # Ljava/lang/String;

    .prologue
    .line 706
    const-string v0, "Fast Walking"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 708
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Power walking"

    invoke-virtual {v0, v1}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 716
    :goto_0
    return v0

    .line 710
    :cond_0
    const-string v0, "Tai chi, General"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 712
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "T\'ai chi"

    invoke-virtual {v0, v1}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    .line 716
    :cond_1
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    invoke-virtual {v0, p1}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method
