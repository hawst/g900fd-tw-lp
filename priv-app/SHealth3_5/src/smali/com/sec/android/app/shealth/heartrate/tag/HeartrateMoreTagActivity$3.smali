.class Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$3;
.super Ljava/lang/Object;
.source "HeartrateMoreTagActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->customizeActionBar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;)V
    .locals 0

    .prologue
    .line 364
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 370
    instance-of v1, p1, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 372
    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;

    .line 374
    .local v0, "ab":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;->getActionButtonIndex()I

    move-result v1

    if-nez v1, :cond_1

    .line 376
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->finish()V

    .line 388
    .end local v0    # "ab":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;
    :cond_0
    :goto_0
    return-void

    .line 379
    .restart local v0    # "ab":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;
    :cond_1
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;->getActionButtonIndex()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 381
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->mSelTag:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->access$700(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;)Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 382
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->mSelTag:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->access$700(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;)Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    move-result-object v2

    # invokes: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->returnActivity(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;)V
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->access$800(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;)V

    goto :goto_0

    .line 384
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->finish()V

    goto :goto_0
.end method
