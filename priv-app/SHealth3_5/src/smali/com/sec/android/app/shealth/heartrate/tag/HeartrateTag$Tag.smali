.class public Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
.super Ljava/lang/Object;
.source "HeartrateTag.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Tag"
.end annotation


# instance fields
.field public mIconId:I

.field public mName:Ljava/lang/String;

.field public mNameId:I

.field public mTagIconId:I

.field public mTagId:I

.field public mType:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;III)V
    .locals 2
    .param p1, "type"    # Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;
    .param p2, "tagId"    # I
    .param p3, "tagNameId"    # I
    .param p4, "iconResourceId"    # I

    .prologue
    const/4 v1, 0x0

    .line 126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 119
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;->TAG_TYPE_PREDEFINED:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mType:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;

    .line 120
    iput v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mTagId:I

    .line 121
    iput v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mNameId:I

    .line 122
    iput v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mIconId:I

    .line 123
    iput v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mTagIconId:I

    .line 124
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mName:Ljava/lang/String;

    .line 127
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mType:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;

    .line 128
    iput p2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mTagId:I

    .line 129
    iput p3, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mNameId:I

    .line 130
    iput p4, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mIconId:I

    .line 131
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;IIILjava/lang/String;)V
    .locals 2
    .param p1, "type"    # Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;
    .param p2, "tagId"    # I
    .param p3, "iconResourceId"    # I
    .param p4, "tagIconId"    # I
    .param p5, "tagName"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 133
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 119
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;->TAG_TYPE_PREDEFINED:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mType:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;

    .line 120
    iput v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mTagId:I

    .line 121
    iput v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mNameId:I

    .line 122
    iput v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mIconId:I

    .line 123
    iput v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mTagIconId:I

    .line 124
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mName:Ljava/lang/String;

    .line 134
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mType:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;

    .line 135
    iput p2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mTagId:I

    .line 136
    iput p3, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mIconId:I

    .line 137
    iput p4, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mTagIconId:I

    .line 138
    iput-object p5, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mName:Ljava/lang/String;

    .line 139
    return-void
.end method
