.class public Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;
.super Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;
.source "ExtendedSchartXyTimeSeries.java"

# interfaces
.implements Lcom/sec/android/app/shealth/logutils/graph/data/ICommonChartSeries;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;-><init>()V

    return-void
.end method


# virtual methods
.method public getMaxDataValue()D
    .locals 6

    .prologue
    .line 26
    const-wide/16 v2, 0x1

    .line 27
    .local v2, "max":D
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    .line 28
    .local v0, "data":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    check-cast v0, Lcom/sec/android/app/shealth/logutils/graph/data/ICommonChartData;

    .end local v0    # "data":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    invoke-interface {v0}, Lcom/sec/android/app/shealth/logutils/graph/data/ICommonChartData;->getMaxDataValue()D

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(DD)D

    move-result-wide v2

    .line 29
    goto :goto_0

    .line 30
    :cond_0
    return-wide v2
.end method

.method public getMinDataValue()D
    .locals 6

    .prologue
    .line 36
    const-wide v2, 0x7fefffffffffffffL    # Double.MAX_VALUE

    .line 38
    .local v2, "min":D
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    .line 40
    .local v0, "data":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    check-cast v0, Lcom/sec/android/app/shealth/logutils/graph/data/ICommonChartData;

    .end local v0    # "data":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    invoke-interface {v0}, Lcom/sec/android/app/shealth/logutils/graph/data/ICommonChartData;->getMinDataValue()D

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(DD)D

    move-result-wide v2

    .line 42
    goto :goto_0

    .line 44
    :cond_0
    return-wide v2
.end method
