.class Lcom/sec/android/app/shealth/home/HomeActivity$11;
.super Landroid/content/BroadcastReceiver;
.source "HomeActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/home/HomeActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/home/HomeActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/home/HomeActivity;)V
    .locals 0

    .prologue
    .line 1551
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/HomeActivity$11;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1554
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1555
    .local v0, "action":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 1592
    :cond_0
    :goto_0
    return-void

    .line 1557
    :cond_1
    const-string v2, "com.samsung.android.sdk.health.sensor.action.DATA_UPDATED"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1558
    # getter for: Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/home/HomeActivity;->access$600()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "syncDevice complete"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1559
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity$11;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    # operator++ for: Lcom/sec/android/app/shealth/home/HomeActivity;->mSyncCounter:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/home/HomeActivity;->access$908(Lcom/sec/android/app/shealth/home/HomeActivity;)I

    .line 1560
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity$11;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    # getter for: Lcom/sec/android/app/shealth/home/HomeActivity;->mSyncing:Z
    invoke-static {v2}, Lcom/sec/android/app/shealth/home/HomeActivity;->access$800(Lcom/sec/android/app/shealth/home/HomeActivity;)Z

    move-result v2

    if-ne v2, v5, :cond_2

    .line 1561
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity$11;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    # getter for: Lcom/sec/android/app/shealth/home/HomeActivity;->mHomeWearableManager:Lcom/sec/android/app/shealth/home/HomeWearableManager;
    invoke-static {v2}, Lcom/sec/android/app/shealth/home/HomeActivity;->access$2000(Lcom/sec/android/app/shealth/home/HomeActivity;)Lcom/sec/android/app/shealth/home/HomeWearableManager;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity$11;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    # getter for: Lcom/sec/android/app/shealth/home/HomeActivity;->mHomeWearableManager:Lcom/sec/android/app/shealth/home/HomeWearableManager;
    invoke-static {v2}, Lcom/sec/android/app/shealth/home/HomeActivity;->access$2000(Lcom/sec/android/app/shealth/home/HomeActivity;)Lcom/sec/android/app/shealth/home/HomeWearableManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/home/HomeWearableManager;->getSensorDeivce()Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity$11;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    # getter for: Lcom/sec/android/app/shealth/home/HomeActivity;->mHomeWearableManager:Lcom/sec/android/app/shealth/home/HomeWearableManager;
    invoke-static {v2}, Lcom/sec/android/app/shealth/home/HomeActivity;->access$2000(Lcom/sec/android/app/shealth/home/HomeActivity;)Lcom/sec/android/app/shealth/home/HomeWearableManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/home/HomeWearableManager;->getSensorDeivce()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/home/HomeActivity$11;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    # getter for: Lcom/sec/android/app/shealth/home/HomeActivity;->mSyncCounter:I
    invoke-static {v3}, Lcom/sec/android/app/shealth/home/HomeActivity;->access$900(Lcom/sec/android/app/shealth/home/HomeActivity;)I

    move-result v3

    if-gt v2, v3, :cond_2

    .line 1562
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity$11;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    # setter for: Lcom/sec/android/app/shealth/home/HomeActivity;->mSyncing:Z
    invoke-static {v2, v4}, Lcom/sec/android/app/shealth/home/HomeActivity;->access$802(Lcom/sec/android/app/shealth/home/HomeActivity;Z)Z

    .line 1563
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity$11;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    # setter for: Lcom/sec/android/app/shealth/home/HomeActivity;->mSyncCounter:I
    invoke-static {v2, v4}, Lcom/sec/android/app/shealth/home/HomeActivity;->access$902(Lcom/sec/android/app/shealth/home/HomeActivity;I)I

    .line 1564
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity$11;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    iget-object v2, v2, Lcom/sec/android/app/shealth/home/HomeActivity;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/sec/android/app/shealth/home/HomeActivity$11;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    # getter for: Lcom/sec/android/app/shealth/home/HomeActivity;->mSyncTimeoutRunnable:Ljava/lang/Runnable;
    invoke-static {v3}, Lcom/sec/android/app/shealth/home/HomeActivity;->access$700(Lcom/sec/android/app/shealth/home/HomeActivity;)Ljava/lang/Runnable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1565
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity$11;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    iget-object v3, p0, Lcom/sec/android/app/shealth/home/HomeActivity$11;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    # getter for: Lcom/sec/android/app/shealth/home/HomeActivity;->mSyncing:Z
    invoke-static {v3}, Lcom/sec/android/app/shealth/home/HomeActivity;->access$800(Lcom/sec/android/app/shealth/home/HomeActivity;)Z

    move-result v3

    # invokes: Lcom/sec/android/app/shealth/home/HomeActivity;->setSyncProgressbarStatus(Z)V
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/home/HomeActivity;->access$1000(Lcom/sec/android/app/shealth/home/HomeActivity;Z)V

    .line 1570
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity$11;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    iget-object v2, v2, Lcom/sec/android/app/shealth/home/HomeActivity;->mSummayFrag:Lcom/sec/android/app/shealth/home/HomeFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/home/HomeFragment;->getHomeLatestData()Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    move-result-object v1

    .line 1571
    .local v1, "dashboard":Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;
    if-eqz v1, :cond_0

    .line 1572
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->refreshDashboard()V

    goto :goto_0

    .line 1576
    .end local v1    # "dashboard":Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;
    :cond_3
    const-string v2, "com.samsung.android.shealth.HEALTH_SYNC_ERROR"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1577
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity$11;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    # operator++ for: Lcom/sec/android/app/shealth/home/HomeActivity;->mSyncCounter:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/home/HomeActivity;->access$908(Lcom/sec/android/app/shealth/home/HomeActivity;)I

    .line 1578
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity$11;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    # getter for: Lcom/sec/android/app/shealth/home/HomeActivity;->mSyncing:Z
    invoke-static {v2}, Lcom/sec/android/app/shealth/home/HomeActivity;->access$800(Lcom/sec/android/app/shealth/home/HomeActivity;)Z

    move-result v2

    if-ne v2, v5, :cond_0

    .line 1579
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity$11;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    # getter for: Lcom/sec/android/app/shealth/home/HomeActivity;->mHomeWearableManager:Lcom/sec/android/app/shealth/home/HomeWearableManager;
    invoke-static {v2}, Lcom/sec/android/app/shealth/home/HomeActivity;->access$2000(Lcom/sec/android/app/shealth/home/HomeActivity;)Lcom/sec/android/app/shealth/home/HomeWearableManager;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity$11;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    # getter for: Lcom/sec/android/app/shealth/home/HomeActivity;->mHomeWearableManager:Lcom/sec/android/app/shealth/home/HomeWearableManager;
    invoke-static {v2}, Lcom/sec/android/app/shealth/home/HomeActivity;->access$2000(Lcom/sec/android/app/shealth/home/HomeActivity;)Lcom/sec/android/app/shealth/home/HomeWearableManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/home/HomeWearableManager;->getSensorDeivce()Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity$11;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    # getter for: Lcom/sec/android/app/shealth/home/HomeActivity;->mHomeWearableManager:Lcom/sec/android/app/shealth/home/HomeWearableManager;
    invoke-static {v2}, Lcom/sec/android/app/shealth/home/HomeActivity;->access$2000(Lcom/sec/android/app/shealth/home/HomeActivity;)Lcom/sec/android/app/shealth/home/HomeWearableManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/home/HomeWearableManager;->getSensorDeivce()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/home/HomeActivity$11;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    # getter for: Lcom/sec/android/app/shealth/home/HomeActivity;->mSyncCounter:I
    invoke-static {v3}, Lcom/sec/android/app/shealth/home/HomeActivity;->access$900(Lcom/sec/android/app/shealth/home/HomeActivity;)I

    move-result v3

    if-gt v2, v3, :cond_0

    .line 1580
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity$11;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    # setter for: Lcom/sec/android/app/shealth/home/HomeActivity;->mSyncing:Z
    invoke-static {v2, v4}, Lcom/sec/android/app/shealth/home/HomeActivity;->access$802(Lcom/sec/android/app/shealth/home/HomeActivity;Z)Z

    .line 1581
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity$11;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    # setter for: Lcom/sec/android/app/shealth/home/HomeActivity;->mSyncCounter:I
    invoke-static {v2, v4}, Lcom/sec/android/app/shealth/home/HomeActivity;->access$902(Lcom/sec/android/app/shealth/home/HomeActivity;I)I

    .line 1582
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity$11;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    iget-object v2, v2, Lcom/sec/android/app/shealth/home/HomeActivity;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/sec/android/app/shealth/home/HomeActivity$11;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    # getter for: Lcom/sec/android/app/shealth/home/HomeActivity;->mSyncTimeoutRunnable:Ljava/lang/Runnable;
    invoke-static {v3}, Lcom/sec/android/app/shealth/home/HomeActivity;->access$700(Lcom/sec/android/app/shealth/home/HomeActivity;)Ljava/lang/Runnable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1583
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity$11;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    iget-object v3, p0, Lcom/sec/android/app/shealth/home/HomeActivity$11;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    # getter for: Lcom/sec/android/app/shealth/home/HomeActivity;->mSyncing:Z
    invoke-static {v3}, Lcom/sec/android/app/shealth/home/HomeActivity;->access$800(Lcom/sec/android/app/shealth/home/HomeActivity;)Z

    move-result v3

    # invokes: Lcom/sec/android/app/shealth/home/HomeActivity;->setSyncProgressbarStatus(Z)V
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/home/HomeActivity;->access$1000(Lcom/sec/android/app/shealth/home/HomeActivity;Z)V

    goto/16 :goto_0

    .line 1586
    :cond_4
    const-string v2, "com.sec.shealth.action.burnt_calories"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1587
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity$11;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    const-string v3, "burnt_calories"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getIntArrayExtra(Ljava/lang/String;)[I

    move-result-object v3

    # setter for: Lcom/sec/android/app/shealth/home/HomeActivity;->mBurntCalories:[I
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/home/HomeActivity;->access$1402(Lcom/sec/android/app/shealth/home/HomeActivity;[I)[I

    .line 1588
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity$11;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    iget-object v3, p0, Lcom/sec/android/app/shealth/home/HomeActivity$11;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    # getter for: Lcom/sec/android/app/shealth/home/HomeActivity;->mBurntCalories:[I
    invoke-static {v3}, Lcom/sec/android/app/shealth/home/HomeActivity;->access$1400(Lcom/sec/android/app/shealth/home/HomeActivity;)[I

    move-result-object v3

    # invokes: Lcom/sec/android/app/shealth/home/HomeActivity;->createBurntCaloriesPopup([I)V
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/home/HomeActivity;->access$2100(Lcom/sec/android/app/shealth/home/HomeActivity;[I)V

    .line 1589
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity$11;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    iget-object v2, v2, Lcom/sec/android/app/shealth/home/HomeActivity;->mSummayFrag:Lcom/sec/android/app/shealth/home/HomeFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/home/HomeFragment;->getHomeLatestData()Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->setWorkingOnClickListener(Z)V

    goto/16 :goto_0
.end method
