.class public final enum Lcom/sec/android/app/shealth/food/app/UserActionLog;
.super Ljava/lang/Enum;
.source "UserActionLog.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/food/app/UserActionLog;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum ACCEPT_VOICE_SEARCH_TERMS_OF_SERVICE_POP_UP:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum ADD_FAVORITE_AMONG_THE_SEARCH_RESULT:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum ADD_PHOTO_BY_ALBUM:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum ADD_PHOTO_BY_CAMERA:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum ALL_ITEM_FILTERING:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum CANCEL_ADD_MY_FOOD:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum CANCEL_AFTER_BARCODE_SEARCH_BY_COMPLETE_POPUP:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum CANCEL_AFTER_VOICE_SEARCH_BY_COMPLETE_POPUP:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum CANCEL_MEAL_EDIT_BY_ACTION_BAR_BUTTON:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum CANCEL_MY_FOOD_BY_ACTION_BAR_BUTTON:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum CANCEL_PICK_FOOD_BY_ACTION_BAR_BUTTON:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum CANCEL_SET_GOAL_BY_ACTION_BAR_BUTTON:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum CATEGORY_TAP:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum CHECK_SAVE_IN_MY_FOOD:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum DECLINE_VOICE_SEARCH_TERMS_OF_SERVICE_POP_UP:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum DELETE_FOOD_BY_CONFIRM_POPUP:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum DELETE_SELECTED_FOOD:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum DONE_ADD_MY_FOOD:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum DONE_MEAL_PLAN_CREATION:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum DONE_MY_FOOD_BY_ACTION_BAR_BUTTON:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum DONE_PICK_FOOD_BY_ACTION_BAR_BUTTON:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum DONE_SET_GOAL_BY_ACTION_BAR_BUTTON:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum ENTER_FAVORITE_BY_ICON:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum ENTER_FOOD_CHART_VIEW:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum ENTER_FOOD_EDIT_VIEW:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum ENTER_FOOD_LOG_LIST:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum ENTER_FOOD_SUMMARY_VIEW:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum ENTER_PHOTO_BY_ICON:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum ENTER_VIEW_BY_MENU:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum FOOD_SEARCH_USING_SEARCH_BUTTON_ON_SIP:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum FREQUENT_TAP:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum GOAL_ACHIEVED_ITEM_FILTERING:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum GOAL_NOT_ACHIEVED_ITEM_FILTERING:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum LAUNCH_CALENDAR:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum LOG_LIST_ENTER_DELETE_MENU:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum LOG_LIST_ENTER_PRINT_MENU:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum LOG_LIST_ENTER_SHARE_VIA_MENU:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum MY_FOOD_TAP:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum OK_AFTER_BARCODE_SEARCH_BY_COMPLETE_POPUP:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum OK_AFTER_VOICE_SEARCH_BY_COMPLETE_POPUP:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum PRESS_MORE_MENU_BUTTON:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum ROTATE_TO_THE_LANDSCAPE_MODE_IN_CHART_VIEW:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum ROTATE_TO_THE_LANDSCAPE_MODE_IN_LOG_LIST_VIEW:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum ROTATE_TO_THE_LANDSCAPE_MODE_IN_SUMMARY_VIEW:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum SAVE_MEAL_EDIT_BY_ACTION_BAR_BUTTON:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum SELECT_CATEGORY_LIST_ITEM:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum SELECT_FOOD_AMONG_THE_SEARCH_RESULT:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum SELECT_FOOD_IN_FAVORITE_FOOD_LISTS_ITEM:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum SELECT_FOOD_IN_MOST_USED_FOOD_LISTS_ITEM:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum SELECT_FOOD_IN_MY_FOOD_LIST:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum SELECT_FOOD_IN_MY_MEAL_LIST:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum SELECT_LOG_LISTS_MEAL_ITEM:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum SELECT_SUBCATEGORY_LIST_ITEM:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum SELECT_TO_AUTO_COMPLETE_LISTS_ITEM:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum SET_MEAL_DATE_BY_SET_DATE_BUTTON:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum SET_MEAL_TIME_BY_SET_TIME_BUTTON:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum SET_UNIT_POPUP_ABOUT:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum SET_UNIT_POPUP_PRECISE:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum SUMMARY_ENTER_HELP_MENU:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum SUMMARY_ENTER_MEAL_PLAN_MENU:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum SUMMARY_ENTER_MY_FOOD_MENU:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum SUMMARY_ENTER_PRINT_MENU:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum SUMMARY_ENTER_RESET_MENU:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum SUMMARY_ENTER_SETTINGS_MENU:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum SUMMARY_ENTER_SET_GOAL_MENU:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum SUMMARY_ENTER_SHARE_VIA_MENU:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum SUMMARY_ENTER_VIEW_PHOTOS_MENU:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum TAP_DAY_CHART_VIEW:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum TAP_HOUR_CHART_VIEW:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum TAP_MONTH_CHART_VIEW:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum TEXT_SEARCH_FOOD:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum TRY_AGAIN_AFTER_BARCODE_SEARCH_BY_COMPLETE_POPUP:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum TRY_AGAIN_AFTER_VOICE_SEARCH_BY_COMPLETE_POPUP:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum TRY_TO_ADD_FOOD_INFORMATION_PLUS:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum TRY_TO_ADD_MY_FOOD:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum TRY_TO_BARCODE_SEARCH:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum TRY_TO_CHANGE_MEAL_TYPE:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum TRY_TO_DELETE_FOOD:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum TRY_TO_INPUT_BREAKFAST_MEAL:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum TRY_TO_INPUT_DINNER_MEAL:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum TRY_TO_INPUT_LUNCH_MEAL:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum TRY_TO_INPUT_MEAL:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum TRY_TO_INPUT_SNACKS_MEAL:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum TRY_TO_SET_UNIT_AMOUNT_FOR_FOOD:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum TRY_TO_VOICE_SEARCH:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum UNCHECK_SAVE_IN_MY_FOOD:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum VIEW_BREAKFAST_MEAL_DETAIL:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum VIEW_DINNER_MEAL_DETAIL:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum VIEW_LUNCH_MEAL_DETAIL:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum VIEW_MEAL_DETAIL:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum VIEW_SNACKS_MEAL_DETAIL:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field public static final enum VIEW_VIEW_NUTRITION_INFO_BY_BUTTON:Lcom/sec/android/app/shealth/food/app/UserActionLog;


# instance fields
.field private final mId:Ljava/lang/String;

.field private final mIsEnabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 22
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "ENTER_FOOD_SUMMARY_VIEW"

    const-string v2, "1000"

    invoke-direct {v0, v1, v5, v2}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->ENTER_FOOD_SUMMARY_VIEW:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 23
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "ENTER_FOOD_LOG_LIST"

    const-string v2, "2000"

    invoke-direct {v0, v1, v4, v2, v4}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->ENTER_FOOD_LOG_LIST:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 24
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "ENTER_FOOD_CHART_VIEW"

    const-string v2, "3000"

    invoke-direct {v0, v1, v6, v2, v4}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->ENTER_FOOD_CHART_VIEW:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 25
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "LAUNCH_CALENDAR"

    const-string v2, "1001"

    invoke-direct {v0, v1, v7, v2}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->LAUNCH_CALENDAR:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 26
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "TRY_TO_INPUT_MEAL"

    const-string v2, "1001"

    invoke-direct {v0, v1, v8, v2, v4}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->TRY_TO_INPUT_MEAL:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 27
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "VIEW_MEAL_DETAIL"

    const/4 v2, 0x5

    const-string v3, "1002"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->VIEW_MEAL_DETAIL:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 28
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "TRY_TO_INPUT_BREAKFAST_MEAL"

    const/4 v2, 0x6

    const-string v3, "1002"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->TRY_TO_INPUT_BREAKFAST_MEAL:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 29
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "TRY_TO_INPUT_LUNCH_MEAL"

    const/4 v2, 0x7

    const-string v3, "1003"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->TRY_TO_INPUT_LUNCH_MEAL:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 30
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "TRY_TO_INPUT_DINNER_MEAL"

    const/16 v2, 0x8

    const-string v3, "1004"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->TRY_TO_INPUT_DINNER_MEAL:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 31
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "TRY_TO_INPUT_SNACKS_MEAL"

    const/16 v2, 0x9

    const-string v3, "1005"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->TRY_TO_INPUT_SNACKS_MEAL:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 32
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "VIEW_BREAKFAST_MEAL_DETAIL"

    const/16 v2, 0xa

    const-string v3, "1006"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->VIEW_BREAKFAST_MEAL_DETAIL:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 33
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "VIEW_LUNCH_MEAL_DETAIL"

    const/16 v2, 0xb

    const-string v3, "1007"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->VIEW_LUNCH_MEAL_DETAIL:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 34
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "VIEW_DINNER_MEAL_DETAIL"

    const/16 v2, 0xc

    const-string v3, "1008"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->VIEW_DINNER_MEAL_DETAIL:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 35
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "VIEW_SNACKS_MEAL_DETAIL"

    const/16 v2, 0xd

    const-string v3, "1009"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->VIEW_SNACKS_MEAL_DETAIL:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 37
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "ENTER_FAVORITE_BY_ICON"

    const/16 v2, 0xe

    const-string v3, "1030"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->ENTER_FAVORITE_BY_ICON:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 38
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "ENTER_PHOTO_BY_ICON"

    const/16 v2, 0xf

    const-string v3, "1031"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->ENTER_PHOTO_BY_ICON:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 39
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "ROTATE_TO_THE_LANDSCAPE_MODE_IN_SUMMARY_VIEW"

    const/16 v2, 0x10

    const-string v3, "1012"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->ROTATE_TO_THE_LANDSCAPE_MODE_IN_SUMMARY_VIEW:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 40
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "PRESS_MORE_MENU_BUTTON"

    const/16 v2, 0x11

    const-string v3, "1013"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->PRESS_MORE_MENU_BUTTON:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 41
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "SUMMARY_ENTER_SET_GOAL_MENU"

    const/16 v2, 0x12

    const-string v3, "1005"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->SUMMARY_ENTER_SET_GOAL_MENU:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 42
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "SUMMARY_ENTER_VIEW_PHOTOS_MENU"

    const/16 v2, 0x13

    const-string v3, "1015"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->SUMMARY_ENTER_VIEW_PHOTOS_MENU:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 43
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "SUMMARY_ENTER_MEAL_PLAN_MENU"

    const/16 v2, 0x14

    const-string v3, "1006"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->SUMMARY_ENTER_MEAL_PLAN_MENU:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 44
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "SUMMARY_ENTER_MY_FOOD_MENU"

    const/16 v2, 0x15

    const-string v3, "1007"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->SUMMARY_ENTER_MY_FOOD_MENU:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 45
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "SUMMARY_ENTER_RESET_MENU"

    const/16 v2, 0x16

    const-string v3, "1008"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->SUMMARY_ENTER_RESET_MENU:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 46
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "SUMMARY_ENTER_SHARE_VIA_MENU"

    const/16 v2, 0x17

    const-string v3, "1019"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->SUMMARY_ENTER_SHARE_VIA_MENU:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 47
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "SUMMARY_ENTER_PRINT_MENU"

    const/16 v2, 0x18

    const-string v3, "1020"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->SUMMARY_ENTER_PRINT_MENU:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 48
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "SUMMARY_ENTER_SETTINGS_MENU"

    const/16 v2, 0x19

    const-string v3, "1021"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->SUMMARY_ENTER_SETTINGS_MENU:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 49
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "SUMMARY_ENTER_HELP_MENU"

    const/16 v2, 0x1a

    const-string v3, "1022"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->SUMMARY_ENTER_HELP_MENU:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 52
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "CANCEL_PICK_FOOD_BY_ACTION_BAR_BUTTON"

    const/16 v2, 0x1b

    const-string v3, "1023"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->CANCEL_PICK_FOOD_BY_ACTION_BAR_BUTTON:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 53
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "DONE_PICK_FOOD_BY_ACTION_BAR_BUTTON"

    const/16 v2, 0x1c

    const-string v3, "1024"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->DONE_PICK_FOOD_BY_ACTION_BAR_BUTTON:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 54
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "TEXT_SEARCH_FOOD"

    const/16 v2, 0x1d

    const-string v3, "1025"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->TEXT_SEARCH_FOOD:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 55
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "SELECT_TO_AUTO_COMPLETE_LISTS_ITEM"

    const/16 v2, 0x1e

    const-string v3, "1026"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->SELECT_TO_AUTO_COMPLETE_LISTS_ITEM:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 56
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "FOOD_SEARCH_USING_SEARCH_BUTTON_ON_SIP"

    const/16 v2, 0x1f

    const-string v3, "1009"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->FOOD_SEARCH_USING_SEARCH_BUTTON_ON_SIP:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 57
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "FREQUENT_TAP"

    const/16 v2, 0x20

    const-string v3, "1010"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->FREQUENT_TAP:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 58
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "MY_FOOD_TAP"

    const/16 v2, 0x21

    const-string v3, "1011"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->MY_FOOD_TAP:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 59
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "CATEGORY_TAP"

    const/16 v2, 0x22

    const-string v3, "1012"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->CATEGORY_TAP:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 60
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "TRY_TO_VOICE_SEARCH"

    const/16 v2, 0x23

    const-string v3, "1013"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->TRY_TO_VOICE_SEARCH:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 61
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "TRY_TO_BARCODE_SEARCH"

    const/16 v2, 0x24

    const-string v3, "1014"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->TRY_TO_BARCODE_SEARCH:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 62
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "SELECT_FOOD_AMONG_THE_SEARCH_RESULT"

    const/16 v2, 0x25

    const-string v3, "1032"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->SELECT_FOOD_AMONG_THE_SEARCH_RESULT:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 63
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "ADD_FAVORITE_AMONG_THE_SEARCH_RESULT"

    const/16 v2, 0x26

    const-string v3, "1033"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->ADD_FAVORITE_AMONG_THE_SEARCH_RESULT:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 64
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "DELETE_SELECTED_FOOD"

    const/16 v2, 0x27

    const-string v3, "1034"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->DELETE_SELECTED_FOOD:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 67
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "SELECT_FOOD_IN_MOST_USED_FOOD_LISTS_ITEM"

    const/16 v2, 0x28

    const-string v3, "1035"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->SELECT_FOOD_IN_MOST_USED_FOOD_LISTS_ITEM:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 68
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "SELECT_FOOD_IN_FAVORITE_FOOD_LISTS_ITEM"

    const/16 v2, 0x29

    const-string v3, "1036"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->SELECT_FOOD_IN_FAVORITE_FOOD_LISTS_ITEM:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 71
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "TRY_TO_ADD_MY_FOOD"

    const/16 v2, 0x2a

    const-string v3, "1037"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->TRY_TO_ADD_MY_FOOD:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 72
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "CHECK_SAVE_IN_MY_FOOD"

    const/16 v2, 0x2b

    const-string v3, "1038"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->CHECK_SAVE_IN_MY_FOOD:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 73
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "CANCEL_ADD_MY_FOOD"

    const/16 v2, 0x2c

    const-string v3, "1039"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->CANCEL_ADD_MY_FOOD:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 74
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "DONE_ADD_MY_FOOD"

    const/16 v2, 0x2d

    const-string v3, "1040"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->DONE_ADD_MY_FOOD:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 75
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "SELECT_FOOD_IN_MY_FOOD_LIST"

    const/16 v2, 0x2e

    const-string v3, "1041"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->SELECT_FOOD_IN_MY_FOOD_LIST:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 76
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "SELECT_FOOD_IN_MY_MEAL_LIST"

    const/16 v2, 0x2f

    const-string v3, "1042"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->SELECT_FOOD_IN_MY_MEAL_LIST:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 77
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "UNCHECK_SAVE_IN_MY_FOOD"

    const/16 v2, 0x30

    const-string v3, "3005"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->UNCHECK_SAVE_IN_MY_FOOD:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 80
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "SELECT_CATEGORY_LIST_ITEM"

    const/16 v2, 0x31

    const-string v3, "1043"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->SELECT_CATEGORY_LIST_ITEM:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 81
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "SELECT_SUBCATEGORY_LIST_ITEM"

    const/16 v2, 0x32

    const-string v3, "1044"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->SELECT_SUBCATEGORY_LIST_ITEM:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 82
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "DECLINE_VOICE_SEARCH_TERMS_OF_SERVICE_POP_UP"

    const/16 v2, 0x33

    const-string v3, "1045"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->DECLINE_VOICE_SEARCH_TERMS_OF_SERVICE_POP_UP:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 83
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "ACCEPT_VOICE_SEARCH_TERMS_OF_SERVICE_POP_UP"

    const/16 v2, 0x34

    const-string v3, "1046"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->ACCEPT_VOICE_SEARCH_TERMS_OF_SERVICE_POP_UP:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 84
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "CANCEL_AFTER_VOICE_SEARCH_BY_COMPLETE_POPUP"

    const/16 v2, 0x35

    const-string v3, "1047"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->CANCEL_AFTER_VOICE_SEARCH_BY_COMPLETE_POPUP:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 85
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "TRY_AGAIN_AFTER_VOICE_SEARCH_BY_COMPLETE_POPUP"

    const/16 v2, 0x36

    const-string v3, "1048"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->TRY_AGAIN_AFTER_VOICE_SEARCH_BY_COMPLETE_POPUP:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 86
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "OK_AFTER_VOICE_SEARCH_BY_COMPLETE_POPUP"

    const/16 v2, 0x37

    const-string v3, "1049"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->OK_AFTER_VOICE_SEARCH_BY_COMPLETE_POPUP:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 87
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "CANCEL_AFTER_BARCODE_SEARCH_BY_COMPLETE_POPUP"

    const/16 v2, 0x38

    const-string v3, "1050"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->CANCEL_AFTER_BARCODE_SEARCH_BY_COMPLETE_POPUP:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 88
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "TRY_AGAIN_AFTER_BARCODE_SEARCH_BY_COMPLETE_POPUP"

    const/16 v2, 0x39

    const-string v3, "1051"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->TRY_AGAIN_AFTER_BARCODE_SEARCH_BY_COMPLETE_POPUP:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 89
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "OK_AFTER_BARCODE_SEARCH_BY_COMPLETE_POPUP"

    const/16 v2, 0x3a

    const-string v3, "1052"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->OK_AFTER_BARCODE_SEARCH_BY_COMPLETE_POPUP:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 90
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "ENTER_FOOD_EDIT_VIEW"

    const/16 v2, 0x3b

    const-string v3, "1015"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->ENTER_FOOD_EDIT_VIEW:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 91
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "SET_MEAL_DATE_BY_SET_DATE_BUTTON"

    const/16 v2, 0x3c

    const-string v3, "1054"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->SET_MEAL_DATE_BY_SET_DATE_BUTTON:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 92
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "SET_MEAL_TIME_BY_SET_TIME_BUTTON"

    const/16 v2, 0x3d

    const-string v3, "1055"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->SET_MEAL_TIME_BY_SET_TIME_BUTTON:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 93
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "TRY_TO_CHANGE_MEAL_TYPE"

    const/16 v2, 0x3e

    const-string v3, "1056"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->TRY_TO_CHANGE_MEAL_TYPE:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 94
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "TRY_TO_ADD_FOOD_INFORMATION_PLUS"

    const/16 v2, 0x3f

    const-string v3, "1057"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->TRY_TO_ADD_FOOD_INFORMATION_PLUS:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 95
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "TRY_TO_SET_UNIT_AMOUNT_FOR_FOOD"

    const/16 v2, 0x40

    const-string v3, "1058"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->TRY_TO_SET_UNIT_AMOUNT_FOR_FOOD:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 96
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "SET_UNIT_POPUP_ABOUT"

    const/16 v2, 0x41

    const-string v3, "1059"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->SET_UNIT_POPUP_ABOUT:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 97
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "SET_UNIT_POPUP_PRECISE"

    const/16 v2, 0x42

    const-string v3, "1060"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->SET_UNIT_POPUP_PRECISE:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 98
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "TRY_TO_DELETE_FOOD"

    const/16 v2, 0x43

    const-string v3, "1061"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->TRY_TO_DELETE_FOOD:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 99
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "DELETE_FOOD_BY_CONFIRM_POPUP"

    const/16 v2, 0x44

    const-string v3, "1062"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->DELETE_FOOD_BY_CONFIRM_POPUP:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 100
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "VIEW_VIEW_NUTRITION_INFO_BY_BUTTON"

    const/16 v2, 0x45

    const-string v3, "1016"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->VIEW_VIEW_NUTRITION_INFO_BY_BUTTON:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 101
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "ADD_PHOTO_BY_CAMERA"

    const/16 v2, 0x46

    const-string v3, "1064"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->ADD_PHOTO_BY_CAMERA:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 102
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "ADD_PHOTO_BY_ALBUM"

    const/16 v2, 0x47

    const-string v3, "1065"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->ADD_PHOTO_BY_ALBUM:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 103
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "CANCEL_MEAL_EDIT_BY_ACTION_BAR_BUTTON"

    const/16 v2, 0x48

    const-string v3, "1066"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->CANCEL_MEAL_EDIT_BY_ACTION_BAR_BUTTON:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 104
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "SAVE_MEAL_EDIT_BY_ACTION_BAR_BUTTON"

    const/16 v2, 0x49

    const-string v3, "1067"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->SAVE_MEAL_EDIT_BY_ACTION_BAR_BUTTON:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 105
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "CANCEL_MY_FOOD_BY_ACTION_BAR_BUTTON"

    const/16 v2, 0x4a

    const-string v3, "1068"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->CANCEL_MY_FOOD_BY_ACTION_BAR_BUTTON:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 106
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "DONE_MY_FOOD_BY_ACTION_BAR_BUTTON"

    const/16 v2, 0x4b

    const-string v3, "1069"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->DONE_MY_FOOD_BY_ACTION_BAR_BUTTON:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 107
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "CANCEL_SET_GOAL_BY_ACTION_BAR_BUTTON"

    const/16 v2, 0x4c

    const-string v3, "1070"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->CANCEL_SET_GOAL_BY_ACTION_BAR_BUTTON:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 108
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "DONE_SET_GOAL_BY_ACTION_BAR_BUTTON"

    const/16 v2, 0x4d

    const-string v3, "1071"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->DONE_SET_GOAL_BY_ACTION_BAR_BUTTON:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 109
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "DONE_MEAL_PLAN_CREATION"

    const/16 v2, 0x4e

    const-string v3, "1072"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->DONE_MEAL_PLAN_CREATION:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 110
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "ROTATE_TO_THE_LANDSCAPE_MODE_IN_LOG_LIST_VIEW"

    const/16 v2, 0x4f

    const-string v3, "2001"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->ROTATE_TO_THE_LANDSCAPE_MODE_IN_LOG_LIST_VIEW:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 111
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "SELECT_LOG_LISTS_MEAL_ITEM"

    const/16 v2, 0x50

    const-string v3, "2002"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->SELECT_LOG_LISTS_MEAL_ITEM:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 112
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "ENTER_VIEW_BY_MENU"

    const/16 v2, 0x51

    const-string v3, "2003"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->ENTER_VIEW_BY_MENU:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 113
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "ALL_ITEM_FILTERING"

    const/16 v2, 0x52

    const-string v3, "2004"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->ALL_ITEM_FILTERING:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 114
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "GOAL_ACHIEVED_ITEM_FILTERING"

    const/16 v2, 0x53

    const-string v3, "2005"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->GOAL_ACHIEVED_ITEM_FILTERING:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 115
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "GOAL_NOT_ACHIEVED_ITEM_FILTERING"

    const/16 v2, 0x54

    const-string v3, "2006"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->GOAL_NOT_ACHIEVED_ITEM_FILTERING:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 116
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "LOG_LIST_ENTER_DELETE_MENU"

    const/16 v2, 0x55

    const-string v3, "2007"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->LOG_LIST_ENTER_DELETE_MENU:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 117
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "LOG_LIST_ENTER_SHARE_VIA_MENU"

    const/16 v2, 0x56

    const-string v3, "2008"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->LOG_LIST_ENTER_SHARE_VIA_MENU:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 118
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "LOG_LIST_ENTER_PRINT_MENU"

    const/16 v2, 0x57

    const-string v3, "2009"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->LOG_LIST_ENTER_PRINT_MENU:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 119
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "ROTATE_TO_THE_LANDSCAPE_MODE_IN_CHART_VIEW"

    const/16 v2, 0x58

    const-string v3, "3001"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->ROTATE_TO_THE_LANDSCAPE_MODE_IN_CHART_VIEW:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 120
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "TAP_HOUR_CHART_VIEW"

    const/16 v2, 0x59

    const-string v3, "3002"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->TAP_HOUR_CHART_VIEW:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 121
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "TAP_DAY_CHART_VIEW"

    const/16 v2, 0x5a

    const-string v3, "3003"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->TAP_DAY_CHART_VIEW:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 122
    new-instance v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    const-string v1, "TAP_MONTH_CHART_VIEW"

    const/16 v2, 0x5b

    const-string v3, "3004"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->TAP_MONTH_CHART_VIEW:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 19
    const/16 v0, 0x5c

    new-array v0, v0, [Lcom/sec/android/app/shealth/food/app/UserActionLog;

    sget-object v1, Lcom/sec/android/app/shealth/food/app/UserActionLog;->ENTER_FOOD_SUMMARY_VIEW:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/shealth/food/app/UserActionLog;->ENTER_FOOD_LOG_LIST:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/shealth/food/app/UserActionLog;->ENTER_FOOD_CHART_VIEW:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/shealth/food/app/UserActionLog;->LAUNCH_CALENDAR:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sec/android/app/shealth/food/app/UserActionLog;->TRY_TO_INPUT_MEAL:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->VIEW_MEAL_DETAIL:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->TRY_TO_INPUT_BREAKFAST_MEAL:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->TRY_TO_INPUT_LUNCH_MEAL:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->TRY_TO_INPUT_DINNER_MEAL:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->TRY_TO_INPUT_SNACKS_MEAL:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->VIEW_BREAKFAST_MEAL_DETAIL:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->VIEW_LUNCH_MEAL_DETAIL:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->VIEW_DINNER_MEAL_DETAIL:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->VIEW_SNACKS_MEAL_DETAIL:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->ENTER_FAVORITE_BY_ICON:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->ENTER_PHOTO_BY_ICON:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->ROTATE_TO_THE_LANDSCAPE_MODE_IN_SUMMARY_VIEW:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->PRESS_MORE_MENU_BUTTON:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->SUMMARY_ENTER_SET_GOAL_MENU:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->SUMMARY_ENTER_VIEW_PHOTOS_MENU:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->SUMMARY_ENTER_MEAL_PLAN_MENU:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->SUMMARY_ENTER_MY_FOOD_MENU:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->SUMMARY_ENTER_RESET_MENU:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->SUMMARY_ENTER_SHARE_VIA_MENU:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->SUMMARY_ENTER_PRINT_MENU:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->SUMMARY_ENTER_SETTINGS_MENU:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->SUMMARY_ENTER_HELP_MENU:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->CANCEL_PICK_FOOD_BY_ACTION_BAR_BUTTON:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->DONE_PICK_FOOD_BY_ACTION_BAR_BUTTON:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->TEXT_SEARCH_FOOD:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->SELECT_TO_AUTO_COMPLETE_LISTS_ITEM:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->FOOD_SEARCH_USING_SEARCH_BUTTON_ON_SIP:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->FREQUENT_TAP:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->MY_FOOD_TAP:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->CATEGORY_TAP:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->TRY_TO_VOICE_SEARCH:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->TRY_TO_BARCODE_SEARCH:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->SELECT_FOOD_AMONG_THE_SEARCH_RESULT:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->ADD_FAVORITE_AMONG_THE_SEARCH_RESULT:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->DELETE_SELECTED_FOOD:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->SELECT_FOOD_IN_MOST_USED_FOOD_LISTS_ITEM:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->SELECT_FOOD_IN_FAVORITE_FOOD_LISTS_ITEM:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->TRY_TO_ADD_MY_FOOD:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->CHECK_SAVE_IN_MY_FOOD:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->CANCEL_ADD_MY_FOOD:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->DONE_ADD_MY_FOOD:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->SELECT_FOOD_IN_MY_FOOD_LIST:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->SELECT_FOOD_IN_MY_MEAL_LIST:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->UNCHECK_SAVE_IN_MY_FOOD:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->SELECT_CATEGORY_LIST_ITEM:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->SELECT_SUBCATEGORY_LIST_ITEM:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->DECLINE_VOICE_SEARCH_TERMS_OF_SERVICE_POP_UP:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->ACCEPT_VOICE_SEARCH_TERMS_OF_SERVICE_POP_UP:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->CANCEL_AFTER_VOICE_SEARCH_BY_COMPLETE_POPUP:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->TRY_AGAIN_AFTER_VOICE_SEARCH_BY_COMPLETE_POPUP:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x37

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->OK_AFTER_VOICE_SEARCH_BY_COMPLETE_POPUP:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x38

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->CANCEL_AFTER_BARCODE_SEARCH_BY_COMPLETE_POPUP:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x39

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->TRY_AGAIN_AFTER_BARCODE_SEARCH_BY_COMPLETE_POPUP:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->OK_AFTER_BARCODE_SEARCH_BY_COMPLETE_POPUP:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->ENTER_FOOD_EDIT_VIEW:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->SET_MEAL_DATE_BY_SET_DATE_BUTTON:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->SET_MEAL_TIME_BY_SET_TIME_BUTTON:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->TRY_TO_CHANGE_MEAL_TYPE:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->TRY_TO_ADD_FOOD_INFORMATION_PLUS:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x40

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->TRY_TO_SET_UNIT_AMOUNT_FOR_FOOD:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x41

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->SET_UNIT_POPUP_ABOUT:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x42

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->SET_UNIT_POPUP_PRECISE:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x43

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->TRY_TO_DELETE_FOOD:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x44

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->DELETE_FOOD_BY_CONFIRM_POPUP:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x45

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->VIEW_VIEW_NUTRITION_INFO_BY_BUTTON:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x46

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->ADD_PHOTO_BY_CAMERA:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x47

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->ADD_PHOTO_BY_ALBUM:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x48

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->CANCEL_MEAL_EDIT_BY_ACTION_BAR_BUTTON:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x49

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->SAVE_MEAL_EDIT_BY_ACTION_BAR_BUTTON:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->CANCEL_MY_FOOD_BY_ACTION_BAR_BUTTON:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->DONE_MY_FOOD_BY_ACTION_BAR_BUTTON:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->CANCEL_SET_GOAL_BY_ACTION_BAR_BUTTON:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->DONE_SET_GOAL_BY_ACTION_BAR_BUTTON:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->DONE_MEAL_PLAN_CREATION:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->ROTATE_TO_THE_LANDSCAPE_MODE_IN_LOG_LIST_VIEW:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x50

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->SELECT_LOG_LISTS_MEAL_ITEM:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x51

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->ENTER_VIEW_BY_MENU:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x52

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->ALL_ITEM_FILTERING:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x53

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->GOAL_ACHIEVED_ITEM_FILTERING:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x54

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->GOAL_NOT_ACHIEVED_ITEM_FILTERING:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x55

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->LOG_LIST_ENTER_DELETE_MENU:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x56

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->LOG_LIST_ENTER_SHARE_VIA_MENU:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x57

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->LOG_LIST_ENTER_PRINT_MENU:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x58

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->ROTATE_TO_THE_LANDSCAPE_MODE_IN_CHART_VIEW:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x59

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->TAP_HOUR_CHART_VIEW:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x5a

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->TAP_DAY_CHART_VIEW:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    const/16 v1, 0x5b

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->TAP_MONTH_CHART_VIEW:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->$VALUES:[Lcom/sec/android/app/shealth/food/app/UserActionLog;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1
    .param p3, "id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 134
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/sec/android/app/shealth/food/app/UserActionLog;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    .line 135
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Z)V
    .locals 0
    .param p3, "id"    # Ljava/lang/String;
    .param p4, "isEnabled"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 142
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 143
    iput-object p3, p0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->mId:Ljava/lang/String;

    .line 144
    iput-boolean p4, p0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->mIsEnabled:Z

    .line 145
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/food/app/UserActionLog;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 19
    const-class v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/food/app/UserActionLog;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->$VALUES:[Lcom/sec/android/app/shealth/food/app/UserActionLog;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/food/app/UserActionLog;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/food/app/UserActionLog;

    return-object v0
.end method


# virtual methods
.method getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->mId:Ljava/lang/String;

    return-object v0
.end method

.method isEnabled()Z
    .locals 1

    .prologue
    .line 152
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/food/app/UserActionLog;->mIsEnabled:Z

    return v0
.end method
