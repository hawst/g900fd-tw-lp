.class public Lcom/sec/android/app/shealth/thermohygrometer/summary/views/ThermoHygrometerDateView;
.super Landroid/widget/TextView;
.source "ThermoHygrometerDateView.java"


# static fields
.field private static final DEFAULT_VALUE:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 32
    invoke-direct {p0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 33
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/ThermoHygrometerDateView;->resetView()V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 38
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/ThermoHygrometerDateView;->resetView()V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 42
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 43
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/ThermoHygrometerDateView;->resetView()V

    .line 44
    return-void
.end method


# virtual methods
.method public resetView()V
    .locals 2

    .prologue
    .line 66
    const-wide/16 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/ThermoHygrometerDateView;->setValue(J)V

    .line 67
    return-void
.end method

.method public setValue(J)V
    .locals 7
    .param p1, "value"    # J

    .prologue
    const v6, 0x7f0910b9

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 52
    const-wide/16 v2, 0x0

    cmp-long v2, p1, v2

    if-nez v2, :cond_0

    .line 53
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/ThermoHygrometerDateView;->getContext()Landroid/content/Context;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    const-string v4, "-"

    aput-object v4, v3, v5

    invoke-virtual {v2, v6, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/ThermoHygrometerDateView;->setText(Ljava/lang/CharSequence;)V

    .line 60
    :goto_0
    return-void

    .line 55
    :cond_0
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string/jumbo v2, "yyyy.MM.dd HH:mm"

    invoke-direct {v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 57
    .local v1, "dateFormat":Ljava/text/SimpleDateFormat;
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 58
    .local v0, "date":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/ThermoHygrometerDateView;->getContext()Landroid/content/Context;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    aput-object v0, v3, v5

    invoke-virtual {v2, v6, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/ThermoHygrometerDateView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
