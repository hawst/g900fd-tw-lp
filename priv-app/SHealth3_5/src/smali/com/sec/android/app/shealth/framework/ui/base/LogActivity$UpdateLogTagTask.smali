.class Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$UpdateLogTagTask;
.super Landroid/os/AsyncTask;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "UpdateLogTagTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private isChecked:Z

.field private final mActivityReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;Z)V
    .locals 1

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$UpdateLogTagTask;->mActivityReference:Ljava/lang/ref/WeakReference;

    iput-boolean p2, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$UpdateLogTagTask;->isChecked:Z

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$UpdateLogTagTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$UpdateLogTagTask;->mActivityReference:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$UpdateLogTagTask;->mActivityReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$UpdateLogTagTask;->isChecked:Z

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    # invokes: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->loadAllData(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)V
    invoke-static {v0, v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$1400(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)V

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$UpdateLogTagTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$UpdateLogTagTask;->mActivityReference:Ljava/lang/ref/WeakReference;

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$UpdateLogTagTask;->mActivityReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$UpdateLogTagTask;->isChecked:Z

    if-eqz v1, :cond_0

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$400(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->setCheckAll(Z)V

    :goto_0
    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$400(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    move-result-object v1

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mCursor:Landroid/database/Cursor;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->changeCursor(Landroid/database/Cursor;)V

    # invokes: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->performNotifyDataChange()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$1500(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)V

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$400(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->getSelectedLogCount()I

    move-result v1

    # invokes: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->updateSelectedCount(I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$500(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;I)V

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->isSelectMode:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$200(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)Z

    move-result v1

    if-eqz v1, :cond_2

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$400(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->getSelectedLogCount()I

    move-result v1

    if-nez v1, :cond_1

    # invokes: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->toggleActionButtonsVisibilityforSelectMode(Z)V
    invoke-static {v0, v3}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$700(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;Z)V

    :goto_1
    # invokes: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->toggleShareViewButton()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$800(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)V

    :goto_2
    # invokes: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->hideLoadingPopup()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$1600(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)V

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->refreshFocusables()V

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->registerContentObserver()V

    :goto_3
    return-void

    :cond_0
    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$400(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->setCheckAll(Z)V

    goto :goto_0

    :cond_1
    # invokes: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->toggleActionButtonsVisibilityforSelectMode(Z)V
    invoke-static {v0, v4}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$700(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;Z)V

    goto :goto_1

    :cond_2
    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$400(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->getSelectedLogCount()I

    move-result v1

    if-nez v1, :cond_3

    # invokes: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->enableActionBarButtons(Z)V
    invoke-static {v0, v3}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$900(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;Z)V

    goto :goto_2

    :cond_3
    # invokes: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->enableActionBarButtons(Z)V
    invoke-static {v0, v4}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$900(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;Z)V

    goto :goto_2

    :catch_0
    move-exception v0

    goto :goto_3

    :catch_1
    move-exception v0

    goto :goto_3
.end method

.method protected onPreExecute()V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$UpdateLogTagTask;->mActivityReference:Ljava/lang/ref/WeakReference;

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$UpdateLogTagTask;->mActivityReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->unregisterContentObserver()V

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$400(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->clearAllSelection()V

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$string;->loading:I

    # invokes: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->showLoadingPopup(I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$1300(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;I)V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_0
.end method
