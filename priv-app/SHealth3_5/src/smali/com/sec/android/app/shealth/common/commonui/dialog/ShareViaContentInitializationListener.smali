.class public Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener;
.super Ljava/lang/Object;
.source "ShareViaContentInitializationListener.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener$AppAdapter;
    }
.end annotation


# instance fields
.field private adapter:Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener$AppAdapter;

.field private context:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener;->context:Landroid/content/Context;

    .line 52
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener$AppAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener;->adapter:Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener$AppAdapter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener;Landroid/content/pm/ResolveInfo;Landroid/support/v4/app/DialogFragment;)Landroid/content/Intent;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener;
    .param p1, "x1"    # Landroid/content/pm/ResolveInfo;
    .param p2, "x2"    # Landroid/support/v4/app/DialogFragment;

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener;->getIntentFromShareExtras(Landroid/content/pm/ResolveInfo;Landroid/support/v4/app/DialogFragment;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private getIntentFromShareExtras(Landroid/content/pm/ResolveInfo;Landroid/support/v4/app/DialogFragment;)Landroid/content/Intent;
    .locals 9
    .param p1, "launchable"    # Landroid/content/pm/ResolveInfo;
    .param p2, "dialogFragment"    # Landroid/support/v4/app/DialogFragment;

    .prologue
    const/4 v6, 0x0

    .line 170
    if-nez p2, :cond_1

    .line 187
    :cond_0
    :goto_0
    return-object v6

    .line 172
    :cond_1
    invoke-virtual {p2}, Landroid/support/v4/app/DialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    .line 173
    .local v4, "shareViaArguments":Landroid/os/Bundle;
    if-eqz v4, :cond_0

    .line 175
    const-string v7, "android.intent.extra.LOCAL_ONLY"

    invoke-virtual {v4, v7}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v5

    check-cast v5, Landroid/os/Bundle;

    .line 176
    .local v5, "shareViaBundle":Landroid/os/Bundle;
    if-eqz v5, :cond_0

    .line 178
    const-string v7, "android.intent.extra.INTENT"

    invoke-virtual {v5, v7}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Landroid/content/Intent;

    .line 179
    .local v3, "sendingIntent":Landroid/content/Intent;
    if-eqz v3, :cond_0

    if-eqz p1, :cond_0

    .line 181
    iget-object v7, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v2, v7, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 182
    .local v2, "packageName":Ljava/lang/String;
    iget-object v0, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 183
    .local v0, "activity":Landroid/content/pm/ActivityInfo;
    new-instance v1, Landroid/content/ComponentName;

    iget-object v7, v0, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v7, v7, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v8, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v1, v7, v8}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    .local v1, "name":Landroid/content/ComponentName;
    new-instance v6, Landroid/content/Intent;

    invoke-direct {v6, v3}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 185
    .local v6, "targetedIntent":Landroid/content/Intent;
    invoke-virtual {v6, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 186
    invoke-virtual {v6, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    goto :goto_0
.end method


# virtual methods
.method public onContentInitialization(Landroid/view/View;Landroid/app/Activity;Landroid/app/Dialog;Landroid/os/Bundle;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V
    .locals 11
    .param p1, "content"    # Landroid/view/View;
    .param p2, "parentActivity"    # Landroid/app/Activity;
    .param p3, "dialog"    # Landroid/app/Dialog;
    .param p4, "savedInstanceState"    # Landroid/os/Bundle;
    .param p5, "okButtonHandler"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;

    .prologue
    .line 57
    if-nez p4, :cond_1

    .line 122
    :cond_0
    :goto_0
    return-void

    .line 60
    :cond_1
    const-string v9, "android.intent.extra.LOCAL_ONLY"

    invoke-virtual {p4, v9}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v8

    .line 61
    .local v8, "shareViaBundle":Landroid/os/Bundle;
    if-eqz v8, :cond_0

    .line 63
    const-string v9, "android.intent.extra.INITIAL_INTENTS"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v2

    .line 64
    .local v2, "extraResolveInfos":[Landroid/os/Parcelable;
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 66
    .local v5, "launchables":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/pm/ResolveInfo;>;"
    if-eqz v2, :cond_0

    .line 69
    move-object v0, v2

    .local v0, "arr$":[Landroid/os/Parcelable;
    array-length v6, v0

    .local v6, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_1
    if-ge v4, v6, :cond_2

    aget-object v1, v0, v4

    .line 71
    .local v1, "extraResolveInfo":Landroid/os/Parcelable;
    check-cast v1, Landroid/content/pm/ResolveInfo;

    .end local v1    # "extraResolveInfo":Landroid/os/Parcelable;
    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 69
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 74
    :cond_2
    iget-object v9, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener;->context:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    .line 75
    .local v7, "pm":Landroid/content/pm/PackageManager;
    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_0

    .line 77
    new-instance v9, Landroid/content/pm/ResolveInfo$DisplayNameComparator;

    invoke-direct {v9, v7}, Landroid/content/pm/ResolveInfo$DisplayNameComparator;-><init>(Landroid/content/pm/PackageManager;)V

    invoke-static {v5, v9}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 78
    new-instance v9, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener$AppAdapter;

    iget-object v10, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener;->context:Landroid/content/Context;

    invoke-direct {v9, v10, v7, v5}, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener$AppAdapter;-><init>(Landroid/content/Context;Landroid/content/pm/PackageManager;Ljava/util/List;)V

    iput-object v9, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener;->adapter:Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener$AppAdapter;

    .line 79
    sget v9, Lcom/sec/android/app/shealth/common/commonui/R$id;->gridview:I

    invoke-virtual {p1, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/GridView;

    .line 80
    .local v3, "gridView":Landroid/widget/GridView;
    iget-object v9, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener;->adapter:Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener$AppAdapter;

    invoke-virtual {v3, v9}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 81
    new-instance v9, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener$1;

    invoke-direct {v9, p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener$1;-><init>(Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener;)V

    invoke-virtual {v3, v9}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 105
    new-instance v9, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener$2;

    invoke-direct {v9, p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener$2;-><init>(Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener;)V

    invoke-virtual {v3, v9}, Landroid/widget/GridView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    goto :goto_0
.end method
