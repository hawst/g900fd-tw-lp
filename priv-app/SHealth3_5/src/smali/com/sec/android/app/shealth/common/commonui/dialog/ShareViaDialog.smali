.class public Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaDialog;
.super Ljava/lang/Object;
.source "ShareViaDialog.java"


# static fields
.field public static final SHARE_VIA_POPUP:Ljava/lang/String; = "share_via_popup"

.field private static final TAG:Ljava/lang/String;

.field private static final THEME_CHOOSER:Ljava/lang/String; = "theme"

.field private static final THEME_DEVICE_DEFAULT_LIGHT:I = 0x2

.field private static mShareData:Ljava/lang/String;

.field private static shareUris:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaDialog;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaDialog;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Landroid/content/Context;Landroid/view/View;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Landroid/content/Context;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 28
    invoke-static {p0, p1}, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaDialog;->getActivityScreenShotForShare(Landroid/content/Context;Landroid/view/View;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaDialog;->shareUris:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$102(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0
    .param p0, "x0"    # Ljava/util/ArrayList;

    .prologue
    .line 28
    sput-object p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaDialog;->shareUris:Ljava/util/ArrayList;

    return-object p0
.end method

.method static synthetic access$200(Landroid/content/Context;)V
    .locals 0
    .param p0, "x0"    # Landroid/content/Context;

    .prologue
    .line 28
    invoke-static {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaDialog;->createAndShowShareViaPopup(Landroid/content/Context;)V

    return-void
.end method

.method private static createAndShowShareViaPopup(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 182
    sget-object v1, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaDialog;->mShareData:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 184
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SEND"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 185
    .local v0, "sendingIntent":Landroid/content/Intent;
    const-string/jumbo v1, "text/plain"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 186
    const-string v1, "android.intent.extra.TEXT"

    sget-object v2, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaDialog;->mShareData:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 187
    const-string/jumbo v1, "theme"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 188
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/common/commonui/R$string;->share_via:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 195
    .end local v0    # "sendingIntent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 192
    :cond_0
    sget-object v1, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaDialog;->shareUris:Ljava/util/ArrayList;

    invoke-static {p0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaDialog;->showShareViaPopup(Landroid/content/Context;Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method private static getActivityScreenShotForShare(Landroid/content/Context;Landroid/view/View;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 41
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/sec/android/app/shealth/common/utils/BitmapUtil;->getScreenshot(Landroid/view/View;I)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public static showShareViaDialog(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 51
    move-object v0, p0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaDialog;->showShareViaDialog(Landroid/content/Context;Landroid/view/View;)V

    .line 52
    return-void
.end method

.method public static showShareViaDialog(Landroid/content/Context;Landroid/graphics/Bitmap;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "bitmapToShare"    # Landroid/graphics/Bitmap;

    .prologue
    .line 137
    const/4 v3, 0x0

    sput-object v3, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaDialog;->mShareData:Ljava/lang/String;

    .line 139
    invoke-static {p0, p1}, Lcom/sec/android/app/shealth/common/utils/BitmapUtil;->saveShareImageToSdCard(Landroid/content/Context;Landroid/graphics/Bitmap;)Ljava/io/File;

    move-result-object v0

    .line 141
    .local v0, "file":Ljava/io/File;
    if-eqz p1, :cond_0

    .line 143
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    .line 146
    :cond_0
    if-nez v0, :cond_1

    .line 148
    check-cast p0, Landroid/app/Activity;

    .end local p0    # "context":Landroid/content/Context;
    sget v3, Lcom/sec/android/app/shealth/common/commonui/R$string;->share_image_storage_error:I

    const/4 v4, 0x1

    invoke-static {p0, v3, v4}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 178
    :goto_0
    return-void

    .line 151
    .restart local p0    # "context":Landroid/content/Context;
    :cond_1
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    .line 153
    .local v1, "filePath":Ljava/lang/String;
    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    .line 154
    .local v2, "shareUri":Landroid/net/Uri;
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    sput-object v3, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaDialog;->shareUris:Ljava/util/ArrayList;

    .line 155
    sget-object v3, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaDialog;->shareUris:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 156
    new-instance v3, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaDialog$2;

    invoke-direct {v3, p0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaDialog$2;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static showShareViaDialog(Landroid/content/Context;Landroid/view/View;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 72
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaDialog;->mShareData:Ljava/lang/String;

    .line 74
    if-eqz p1, :cond_0

    .line 76
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaDialog$1;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaDialog$1;-><init>(Landroid/content/Context;Landroid/view/View;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 128
    :cond_0
    return-void
.end method

.method public static showShareViaDialog(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "shareData"    # Ljava/lang/String;

    .prologue
    .line 61
    sput-object p1, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaDialog;->mShareData:Ljava/lang/String;

    .line 62
    invoke-static {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaDialog;->createAndShowShareViaPopup(Landroid/content/Context;)V

    .line 63
    return-void
.end method

.method public static showShareViaPopup(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "shareFileUris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const/4 v3, 0x2

    .line 205
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 208
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Context is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " and Uri List object is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 212
    :cond_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-gtz v1, :cond_2

    .line 215
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Uri List size is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 216
    :cond_2
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_3

    .line 217
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SEND"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 218
    .local v0, "sendingIntent":Landroid/content/Intent;
    const-string v1, "image/jpeg"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 219
    const-string v2, "android.intent.extra.STREAM"

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Parcelable;

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 220
    const-string/jumbo v1, "theme"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 221
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/common/commonui/R$string;->share_via:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 229
    :goto_0
    return-void

    .line 223
    .end local v0    # "sendingIntent":Landroid/content/Intent;
    :cond_3
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SEND_MULTIPLE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 224
    .restart local v0    # "sendingIntent":Landroid/content/Intent;
    const-string v1, "image/jpeg"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 225
    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 226
    const-string/jumbo v1, "theme"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 227
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/common/commonui/R$string;->share_via:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
