.class public Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogFragment;
.super Landroid/app/DialogFragment;
.source "StatisticsDialogFragment.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ValidFragment"
    }
.end annotation


# instance fields
.field private holderLeft:Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

.field private holderRight:Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

.field private isComparative:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 41
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;)V
    .locals 1
    .param p1, "holderMine"    # Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

    .prologue
    .line 34
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 35
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogFragment;->setRetainInstance(Z)V

    .line 36
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogFragment;->holderLeft:Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

    .line 37
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogFragment;->isComparative:Z

    .line 38
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;)V
    .locals 1
    .param p1, "holderLeft"    # Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;
    .param p2, "holderRight"    # Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

    .prologue
    const/4 v0, 0x1

    .line 27
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 28
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogFragment;->setRetainInstance(Z)V

    .line 29
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogFragment;->holderLeft:Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

    .line 30
    iput-object p2, p0, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogFragment;->holderRight:Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

    .line 31
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogFragment;->isComparative:Z

    .line 32
    return-void
.end method

.method private initChallenger(Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;)V
    .locals 1
    .param p1, "challenger"    # Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;
    .param p2, "holder"    # Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

    .prologue
    .line 165
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogFragment;->isComparative:Z

    invoke-virtual {p1, v0}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;->init(Z)V

    .line 166
    invoke-virtual {p1, p2}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;->setValues(Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;)V

    .line 167
    return-void
.end method

.method private initChallengers(Landroid/view/View;)V
    .locals 14
    .param p1, "content"    # Landroid/view/View;

    .prologue
    .line 115
    const v8, 0x7f080bfa

    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 116
    .local v0, "dialogContent":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0a051d

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v9

    float-to-int v9, v9

    iput v9, v8, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 117
    const v8, 0x7f080bfb

    invoke-virtual {v0, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0a051e

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v9

    float-to-int v9, v9

    iput v9, v8, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 118
    const v8, 0x7f080bfd

    invoke-virtual {v0, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0a051e

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v9

    float-to-int v9, v9

    iput v9, v8, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 120
    iget-object v8, p0, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogFragment;->holderLeft:Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->getStep()I

    move-result v8

    int-to-long v2, v8

    .line 121
    .local v2, "leftHolderDistance":J
    iget-object v8, p0, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogFragment;->holderRight:Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->getStep()I

    move-result v8

    int-to-long v5, v8

    .line 123
    .local v5, "rightHolderDistance":J
    const v8, 0x7f080bfb

    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;

    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogFragment;->holderLeft:Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

    invoke-direct {p0, v8, v9}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogFragment;->initChallenger(Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;)V

    .line 124
    const v8, 0x7f080bfd

    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;

    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogFragment;->holderRight:Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

    invoke-direct {p0, v8, v9}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogFragment;->initChallenger(Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;)V

    .line 126
    const v8, 0x7f080bfe

    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 127
    .local v7, "textView":Landroid/widget/TextView;
    cmp-long v8, v2, v5

    if-lez v8, :cond_1

    .line 129
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->isArabLocale()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 130
    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f090b7e

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    sub-long v12, v2, v5

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v8, v9, v10}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 135
    .local v4, "loseText":Ljava/lang/String;
    :goto_0
    invoke-virtual {v7, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 148
    .end local v4    # "loseText":Ljava/lang/String;
    :goto_1
    return-void

    .line 132
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f090b7e

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    sub-long v11, v2, v5

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .restart local v4    # "loseText":Ljava/lang/String;
    goto :goto_0

    .line 136
    .end local v4    # "loseText":Ljava/lang/String;
    :cond_1
    cmp-long v8, v2, v5

    if-gez v8, :cond_3

    .line 138
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->isArabLocale()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 139
    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f090b82

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    sub-long v12, v5, v2

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v8, v9, v10}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 144
    .local v1, "leadText":Ljava/lang/String;
    :goto_2
    invoke-virtual {v7, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 141
    .end local v1    # "leadText":Ljava/lang/String;
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f090b82

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    sub-long v11, v5, v2

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "leadText":Ljava/lang/String;
    goto :goto_2

    .line 146
    .end local v1    # "leadText":Ljava/lang/String;
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f090b81

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method private initMyInformation(Landroid/view/View;)V
    .locals 7
    .param p1, "content"    # Landroid/view/View;

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x0

    .line 151
    const v2, 0x7f08004d

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090b7d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 152
    const v2, 0x7f080bfa

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 153
    .local v0, "dialogContent":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a051d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 154
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0ab5

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 155
    invoke-virtual {v0, v5, v5, v5, v5}, Landroid/view/View;->setPadding(IIII)V

    .line 156
    const v2, 0x7f080bfd

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    .line 157
    const v2, 0x7f080bfc

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    .line 158
    const v2, 0x7f080bfe

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    .line 159
    const v2, 0x7f080bfb

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;

    .line 161
    .local v1, "myInfo":Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogFragment;->holderLeft:Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogFragment;->initChallenger(Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;)V

    .line 162
    return-void
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 57
    if-eqz p1, :cond_0

    .line 58
    const-string v0, "holderLeft"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogFragment;->holderLeft:Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

    .line 59
    const-string v0, "holderRight"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogFragment;->holderRight:Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

    .line 60
    const-string v0, "isComparative"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogFragment;->isComparative:Z

    .line 62
    :cond_0
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 63
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 67
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 68
    const/4 v0, 0x2

    const v1, 0x7f0c0010

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogFragment;->setStyle(II)V

    .line 69
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogFragment;->setCancelable(Z)V

    .line 70
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogFragment;->setRetainInstance(Z)V

    .line 71
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 100
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogFragment$2;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogFragment;->getTheme()I

    move-result v3

    invoke-direct {v0, p0, v2, v3}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogFragment$2;-><init>(Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogFragment;Landroid/content/Context;I)V

    .line 107
    .local v0, "dialog":Landroid/app/Dialog;
    new-instance v1, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v1}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    .line 108
    .local v1, "lpWindow":Landroid/view/WindowManager$LayoutParams;
    const/4 v2, 0x2

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 109
    const v2, 0x3e99999a    # 0.3f

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->dimAmount:F

    .line 110
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 111
    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 75
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f0302ae

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 76
    .local v0, "content":Landroid/view/View;
    const v1, 0x7f0800b3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogFragment$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogFragment$1;-><init>(Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogFragment;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 83
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogFragment;->isComparative:Z

    if-eqz v1, :cond_0

    .line 84
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogFragment;->initChallengers(Landroid/view/View;)V

    .line 88
    :goto_0
    return-object v0

    .line 86
    :cond_0
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogFragment;->initMyInformation(Landroid/view/View;)V

    goto :goto_0
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 93
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogFragment;->getRetainInstance()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 94
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setDismissMessage(Landroid/os/Message;)V

    .line 95
    :cond_0
    invoke-super {p0}, Landroid/app/DialogFragment;->onDestroyView()V

    .line 96
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogFragment;->holderLeft:Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

    if-eqz v0, :cond_0

    .line 46
    const-string v0, "holderLeft"

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogFragment;->holderLeft:Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 48
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogFragment;->holderRight:Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

    if-eqz v0, :cond_1

    .line 49
    const-string v0, "holderRight"

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogFragment;->holderRight:Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 51
    :cond_1
    const-string v0, "isComparative"

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogFragment;->isComparative:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 52
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 53
    return-void
.end method
