.class public Lcom/sec/android/app/shealth/framework/repository/database/old/DatabaseHelper;
.super Landroid/database/sqlite/SQLiteSecureOpenHelper;
.source "DatabaseHelper.java"


# static fields
.field private static final DATABASE_VERSION:I = 0x5

.field private static final TAG:Ljava/lang/String;

.field private static dataBasePath:Ljava/lang/String;

.field private static mPassword:[B


# instance fields
.field private activeDatabaseCount:I

.field private mContext:Landroid/content/Context;

.field private mDb:Landroid/database/sqlite/SQLiteDatabase;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 23
    const-class v1, Lcom/sec/android/app/shealth/framework/repository/database/old/DatabaseHelper;

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/sec/android/app/shealth/framework/repository/database/old/DatabaseHelper;->TAG:Ljava/lang/String;

    .line 40
    new-instance v0, Lcom/sec/android/service/health/cp/common/AESEncryption;

    invoke-direct {v0}, Lcom/sec/android/service/health/cp/common/AESEncryption;-><init>()V

    .line 41
    .local v0, "enc":Lcom/sec/android/service/health/cp/common/AESEncryption;
    iget-object v1, v0, Lcom/sec/android/service/health/cp/common/AESEncryption;->str:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    sput-object v1, Lcom/sec/android/app/shealth/framework/repository/database/old/DatabaseHelper;->mPassword:[B

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 47
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/old/DatabaseHelper;->dataBasePath:Ljava/lang/String;

    const/4 v1, 0x0

    const/4 v2, 0x5

    invoke-direct {p0, p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteSecureOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 198
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/DatabaseHelper;->activeDatabaseCount:I

    .line 48
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/old/DatabaseHelper;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "initialisation "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/shealth/framework/repository/database/old/DatabaseHelper;->dataBasePath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/DatabaseHelper;->mContext:Landroid/content/Context;

    .line 50
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "databaseNAme"    # Ljava/lang/String;

    .prologue
    .line 54
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-direct {p0, p1, p2, v0, v1}, Landroid/database/sqlite/SQLiteSecureOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 198
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/DatabaseHelper;->activeDatabaseCount:I

    .line 55
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/old/DatabaseHelper;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "initialisation "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 56
    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/DatabaseHelper;->mContext:Landroid/content/Context;

    .line 57
    return-void
.end method

.method public static checkDataBasePath()V
    .locals 3

    .prologue
    .line 34
    const-string/jumbo v0, "secure_shealth2.db"

    sput-object v0, Lcom/sec/android/app/shealth/framework/repository/database/old/DatabaseHelper;->dataBasePath:Ljava/lang/String;

    .line 35
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/old/DatabaseHelper;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "checked database path - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/shealth/framework/repository/database/old/DatabaseHelper;->dataBasePath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 36
    return-void
.end method


# virtual methods
.method public declared-synchronized closeDatabase(Landroid/database/sqlite/SQLiteDatabase;)Z
    .locals 3
    .param p1, "connection"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 210
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/DatabaseHelper;->activeDatabaseCount:I

    if-lez v0, :cond_0

    .line 211
    iget v0, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/DatabaseHelper;->activeDatabaseCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/DatabaseHelper;->activeDatabaseCount:I

    .line 212
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/old/DatabaseHelper;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "closeDatabase, database connections =  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/DatabaseHelper;->activeDatabaseCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 213
    iget v0, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/DatabaseHelper;->activeDatabaseCount:I

    if-nez v0, :cond_1

    .line 215
    if-eqz p1, :cond_1

    .line 217
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 219
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 220
    const/4 v0, 0x1

    .line 224
    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 210
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    .locals 4

    .prologue
    .line 95
    sget-object v1, Lcom/sec/android/app/shealth/framework/repository/database/old/DatabaseHelper;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getWritableDatabase "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/repository/database/old/DatabaseHelper;->getDatabaseName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    sget-object v1, Lcom/sec/android/app/shealth/framework/repository/database/old/DatabaseHelper;->mPassword:[B

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/framework/repository/database/old/DatabaseHelper;->getWritableDatabase([B)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 103
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string/jumbo v1, "pragma cipher_page_size=4096;"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 104
    const-string/jumbo v1, "pragma page_size=4096;"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 105
    const-string v1, "getWritableDatabase"

    const-string v2, "Open successfully"

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    return-object v0
.end method

.method public getWritableDatabase([B)Landroid/database/sqlite/SQLiteDatabase;
    .locals 12
    .param p1, "password"    # [B

    .prologue
    const/4 v11, 0x5

    .line 114
    monitor-enter p0

    .line 116
    :try_start_0
    sget-object v6, Lcom/sec/android/app/shealth/framework/repository/database/old/DatabaseHelper;->TAG:Ljava/lang/String;

    const-string v7, "getWritableDatabase(pwd)"

    invoke-static {v6, v7}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    iget-object v6, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/DatabaseHelper;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    if-eqz v6, :cond_0

    .line 119
    iget-object v6, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/DatabaseHelper;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v6

    if-nez v6, :cond_3

    .line 122
    const/4 v6, 0x0

    iput-object v6, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/DatabaseHelper;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    .line 131
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/DatabaseHelper;->mDb:Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 136
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_1
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/DatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v7

    invoke-virtual {v7}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/databases/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 137
    .local v2, "folderName":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "secure_shealth2.db"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 138
    .local v4, "secFilePath":Ljava/lang/String;
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 140
    .local v3, "secDBFile":Ljava/io/File;
    const-string v6, "getWritableDatabase(pwd)"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "SecDatabase : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const/high16 v8, 0x10000000

    new-instance v9, Landroid/database/DefaultDatabaseErrorHandler;

    invoke-direct {v9}, Landroid/database/DefaultDatabaseErrorHandler;-><init>()V

    sget-object v10, Lcom/sec/android/app/shealth/framework/repository/database/old/DatabaseHelper;->mPassword:[B

    invoke-static {v6, v7, v8, v9, v10}, Landroid/database/sqlite/SQLiteDatabase;->openSecureDatabase(Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;ILandroid/database/DatabaseErrorHandler;[B)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 143
    const-string/jumbo v6, "pragma cipher_page_size=4096;"

    invoke-virtual {v0, v6}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 144
    const-string/jumbo v6, "pragma page_size=4096;"

    invoke-virtual {v0, v6}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 145
    const-string v6, "getWritableDatabase(pwd)"

    const-string v7, "Open successfully"

    invoke-static {v6, v7}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 152
    :try_start_2
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->getVersion()I

    move-result v5

    .line 154
    .local v5, "version":I
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 157
    if-nez v5, :cond_5

    .line 159
    :try_start_3
    const-string v6, "getWritableDatabase(pwd)"

    const-string/jumbo v7, "onCreate"

    invoke-static {v6, v7}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/repository/database/old/DatabaseHelper;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 161
    const-string v6, "getWritableDatabase(pwd)"

    const-string v7, "...onCreate"

    invoke-static {v6, v7}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 172
    :cond_1
    :goto_0
    const/4 v6, 0x5

    invoke-virtual {v0, v6}, Landroid/database/sqlite/SQLiteDatabase;->setVersion(I)V

    .line 173
    const-string v6, "getWritableDatabase(pwd)"

    const-string/jumbo v7, "setVersion"

    invoke-static {v6, v7}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 174
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 178
    :try_start_4
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 179
    const-string v6, "getWritableDatabase(pwd)"

    const-string v7, "finally"

    invoke-static {v6, v7}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/DatabaseHelper;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    .line 182
    sget-object v6, Lcom/sec/android/app/shealth/framework/repository/database/old/DatabaseHelper;->TAG:Ljava/lang/String;

    const-string v7, "...getWritableDatabase(pwd)"

    invoke-static {v6, v7}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 188
    if-eqz v0, :cond_2

    :try_start_5
    iget-object v6, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/DatabaseHelper;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    if-eq v0, v6, :cond_2

    .line 190
    const-string v6, "getWritableDatabase(pwd)"

    const-string v7, "close db"

    invoke-static {v6, v7}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 193
    :cond_2
    sget-object v6, Lcom/sec/android/app/shealth/framework/repository/database/old/DatabaseHelper;->TAG:Ljava/lang/String;

    const-string v7, "...getWritableDatabase(pwd)"

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit p0

    .end local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v2    # "folderName":Ljava/lang/String;
    .end local v3    # "secDBFile":Ljava/io/File;
    .end local v4    # "secFilePath":Ljava/lang/String;
    .end local v5    # "version":I
    :goto_1
    return-object v0

    .line 127
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/DatabaseHelper;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    monitor-exit p0

    goto :goto_1

    .line 195
    :catchall_0
    move-exception v6

    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    throw v6

    .line 147
    .restart local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :catch_0
    move-exception v1

    .line 149
    .local v1, "ex":Landroid/database/sqlite/SQLiteException;
    :try_start_6
    throw v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 188
    .end local v1    # "ex":Landroid/database/sqlite/SQLiteException;
    :catchall_1
    move-exception v6

    if-eqz v0, :cond_4

    :try_start_7
    iget-object v7, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/DatabaseHelper;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    if-eq v0, v7, :cond_4

    .line 190
    const-string v7, "getWritableDatabase(pwd)"

    const-string v8, "close db"

    invoke-static {v7, v8}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 193
    :cond_4
    sget-object v7, Lcom/sec/android/app/shealth/framework/repository/database/old/DatabaseHelper;->TAG:Ljava/lang/String;

    const-string v8, "...getWritableDatabase(pwd)"

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    throw v6
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 165
    .restart local v2    # "folderName":Ljava/lang/String;
    .restart local v3    # "secDBFile":Ljava/io/File;
    .restart local v4    # "secFilePath":Ljava/lang/String;
    .restart local v5    # "version":I
    :cond_5
    if-ge v5, v11, :cond_1

    .line 167
    :try_start_8
    const-string v6, "getWritableDatabase(pwd)"

    const-string/jumbo v7, "onUpgrade"

    invoke-static {v6, v7}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 168
    const/4 v6, 0x5

    invoke-virtual {p0, v0, v5, v6}, Lcom/sec/android/app/shealth/framework/repository/database/old/DatabaseHelper;->onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V

    .line 169
    const-string v6, "getWritableDatabase(pwd)"

    const-string v7, "...onUpgrade"

    invoke-static {v6, v7}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    goto :goto_0

    .line 178
    :catchall_2
    move-exception v6

    :try_start_9
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 179
    const-string v7, "getWritableDatabase(pwd)"

    const-string v8, "finally"

    invoke-static {v7, v8}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    throw v6
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1
.end method

.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    const/4 v2, 0x5

    .line 62
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/old/DatabaseHelper;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onCreate"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->getVersion()I

    move-result v0

    if-ne v0, v2, :cond_0

    .line 70
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2}, Lcom/sec/android/app/shealth/framework/repository/database/old/DatabaseHelper;->onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V

    .line 73
    :cond_0
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 78
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/old/DatabaseHelper;->TAG:Ljava/lang/String;

    const-string v1, "Upgrade is handled by BackupManager"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    return-void
.end method

.method public declared-synchronized openDatabase()Landroid/database/sqlite/SQLiteDatabase;
    .locals 4

    .prologue
    .line 202
    monitor-enter p0

    :try_start_0
    sget-object v1, Lcom/sec/android/app/shealth/framework/repository/database/old/DatabaseHelper;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "openDatabase "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/repository/database/old/DatabaseHelper;->getDatabaseName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 203
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/repository/database/old/DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 204
    .local v0, "connection":Landroid/database/sqlite/SQLiteDatabase;
    iget v1, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/DatabaseHelper;->activeDatabaseCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/DatabaseHelper;->activeDatabaseCount:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 205
    monitor-exit p0

    return-object v0

    .line 202
    .end local v0    # "connection":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method
