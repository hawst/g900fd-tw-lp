.class public Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;
.super Ljava/lang/Object;
.source "PhotoListUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil$PhotoListFactoryTask;,
        Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil$PhotoMarkerOptions;,
        Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil$OnUpdatePhotoListListener;
    }
.end annotation


# static fields
.field private static MIN_LOCATION_VALUE:F

.field private static aHandler:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapPhotoList$UpdateHandler;

.field private static gHandler:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapPhotoList$UpdateHandler;

.field static mContext:Landroid/content/Context;

.field static mHeight:I

.field static mHeightPortrait:I

.field static mIsAmap:Z

.field static mPhotoList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;",
            ">;>;"
        }
    .end annotation
.end field

.field private static mPhotoListener:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil$OnUpdatePhotoListListener;

.field static mSrcOffsetX:I

.field static mSrcOffsetY:I

.field static mSrcheight:I

.field static mSrcheightPortrait:I

.field static mSrcwidth:I

.field static mTextPading:I

.field static mTextSize:I

.field static mTextStartY:I

.field static mTextStartYPortrait:I

.field static mWidth:I

.field static markerOPList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil$PhotoMarkerOptions;",
            ">;"
        }
    .end annotation
.end field

.field static res:Landroid/content/res/Resources;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const v0, 0x38d1b717    # 1.0E-4f

    sput v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->MIN_LOCATION_VALUE:F

    .line 33
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->markerOPList:Ljava/util/ArrayList;

    .line 34
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mPhotoList:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 290
    return-void
.end method

.method public static SetPhotoList(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 206
    .local p0, "photodatas":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;>;"
    if-nez p0, :cond_1

    .line 211
    :cond_0
    :goto_0
    return-void

    .line 208
    :cond_1
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 210
    invoke-static {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->makePhotoMarker(Ljava/util/List;)V

    goto :goto_0
.end method

.method static synthetic access$000()Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapPhotoList$UpdateHandler;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->aHandler:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapPhotoList$UpdateHandler;

    return-object v0
.end method

.method static synthetic access$100()Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapPhotoList$UpdateHandler;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->gHandler:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapPhotoList$UpdateHandler;

    return-object v0
.end method

.method public static getMapIconThumbnail(Ljava/lang/String;I)Landroid/graphics/Bitmap;
    .locals 22
    .param p0, "filepath"    # Ljava/lang/String;
    .param p1, "count"    # I

    .prologue
    .line 153
    const/4 v4, 0x0

    .line 154
    .local v4, "backgroundBitmap":Landroid/graphics/Bitmap;
    const/4 v12, 0x0

    .line 155
    .local v12, "srcBitmap":Landroid/graphics/Bitmap;
    invoke-static/range {p1 .. p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v15

    .line 157
    .local v15, "text":Ljava/lang/String;
    invoke-static/range {p0 .. p0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 158
    .local v5, "bitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v16

    .line 159
    .local v16, "width":I
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 160
    .local v7, "height":I
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v17

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v18

    move/from16 v0, v17

    move/from16 v1, v18

    if-ge v0, v1, :cond_0

    .line 161
    sget-object v17, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mContext:Landroid/content/Context;

    check-cast v17, Landroid/app/Activity;

    invoke-virtual/range {v17 .. v17}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    const v18, 0x7f0a097c

    invoke-virtual/range {v17 .. v18}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    sput v17, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mHeight:I

    .line 162
    sget v17, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mSrcwidth:I

    sput v17, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mSrcheight:I

    .line 163
    sget v17, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mTextStartY:I

    sget v18, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mTextSize:I

    sget v19, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mSrcOffsetX:I

    add-int v18, v18, v19

    add-int v17, v17, v18

    sput v17, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mTextStartY:I

    .line 166
    :cond_0
    sget-object v17, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mContext:Landroid/content/Context;

    invoke-virtual/range {v17 .. v17}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    const v18, 0x7f0203fb

    invoke-static/range {v17 .. v18}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 167
    sget v17, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mWidth:I

    sget v18, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mHeight:I

    const/16 v19, 0x0

    move/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-static {v4, v0, v1, v2}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 169
    new-instance v6, Landroid/graphics/Canvas;

    invoke-direct {v6, v4}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 170
    .local v6, "canvas":Landroid/graphics/Canvas;
    new-instance v8, Landroid/graphics/Matrix;

    invoke-direct {v8}, Landroid/graphics/Matrix;-><init>()V

    .line 171
    .local v8, "mMatrix":Landroid/graphics/Matrix;
    new-instance v13, Landroid/graphics/Matrix;

    invoke-direct {v13}, Landroid/graphics/Matrix;-><init>()V

    .line 172
    .local v13, "srcMatrix":Landroid/graphics/Matrix;
    new-instance v11, Landroid/graphics/RectF;

    const/16 v17, 0x0

    const/16 v18, 0x0

    sget v19, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mWidth:I

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    sget v20, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mHeight:I

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    move/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-direct {v11, v0, v1, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 173
    .local v11, "mRect":Landroid/graphics/RectF;
    new-instance v14, Landroid/graphics/RectF;

    sget v17, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mSrcOffsetX:I

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    sget v18, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mSrcOffsetY:I

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    sget v19, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mSrcwidth:I

    sget v20, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mSrcOffsetX:I

    sub-int v19, v19, v20

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    sget v20, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mSrcheight:I

    sget v21, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mSrcOffsetY:I

    sub-int v20, v20, v21

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    move/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-direct {v14, v0, v1, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 174
    .local v14, "srcRect":Landroid/graphics/RectF;
    new-instance v9, Landroid/graphics/Paint;

    invoke-direct {v9}, Landroid/graphics/Paint;-><init>()V

    .line 175
    .local v9, "mPaint":Landroid/graphics/Paint;
    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v9, v0}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 176
    new-instance v10, Landroid/graphics/Paint;

    invoke-direct {v10}, Landroid/graphics/Paint;-><init>()V

    .line 177
    .local v10, "mPaint2":Landroid/graphics/Paint;
    invoke-virtual {v8, v11}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 178
    invoke-virtual {v13, v14}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 179
    invoke-virtual {v6, v8}, Landroid/graphics/Canvas;->setMatrix(Landroid/graphics/Matrix;)V

    .line 180
    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v10, v0}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 181
    sget-object v17, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    move-object/from16 v0, v17

    invoke-virtual {v10, v0}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 182
    const/high16 v17, -0x1000000

    move/from16 v0, v17

    invoke-virtual {v10, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 183
    sget v17, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mTextSize:I

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    invoke-virtual {v10, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 184
    sget-object v17, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    move-object/from16 v0, v17

    invoke-virtual {v10, v0}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 186
    sget-object v17, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mContext:Landroid/content/Context;

    sget v18, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mWidth:I

    sget v19, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mHeight:I

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    move/from16 v2, v18

    move/from16 v3, v19

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/common/utils/ExercisePhotoUtils;->getImageThumbnailFitXY(Landroid/content/Context;Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v12

    .line 187
    if-nez v12, :cond_2

    .line 188
    const/4 v4, 0x0

    .line 201
    .end local v4    # "backgroundBitmap":Landroid/graphics/Bitmap;
    :cond_1
    :goto_0
    return-object v4

    .line 191
    .restart local v4    # "backgroundBitmap":Landroid/graphics/Bitmap;
    :cond_2
    sget v17, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mSrcwidth:I

    sget v18, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mSrcheight:I

    const/16 v19, 0x0

    move/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-static {v12, v0, v1, v2}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v12

    .line 192
    sget v17, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mSrcOffsetX:I

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    sget v18, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mSrcOffsetY:I

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v6, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 193
    invoke-virtual {v6, v12, v13, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 194
    const/16 v17, 0x0

    const/16 v18, 0x0

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v6, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 195
    sget v17, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mSrcwidth:I

    sget v18, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mSrcOffsetX:I

    sub-int v17, v17, v18

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    sget v18, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mTextStartY:I

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v6, v15, v0, v1, v10}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 196
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v17

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v18

    move/from16 v0, v17

    move/from16 v1, v18

    if-ge v0, v1, :cond_1

    .line 197
    sget v17, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mHeightPortrait:I

    sput v17, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mHeight:I

    .line 198
    sget v17, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mSrcheightPortrait:I

    sput v17, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mSrcheight:I

    .line 199
    sget v17, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mTextStartYPortrait:I

    sput v17, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mTextStartY:I

    goto :goto_0
.end method

.method public static getMarkerOPList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil$PhotoMarkerOptions;",
            ">;"
        }
    .end annotation

    .prologue
    .line 96
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->markerOPList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public static getScreenPhotoList(I)Ljava/util/List;
    .locals 1
    .param p0, "index"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 112
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mPhotoList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gt v0, p0, :cond_0

    .line 113
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mPhotoList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 p0, v0, -0x1

    .line 115
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mPhotoList:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public static getSize()I
    .locals 1

    .prologue
    .line 104
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->markerOPList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method private static init()V
    .locals 2

    .prologue
    .line 81
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0973

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mWidth:I

    .line 82
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0974

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mHeight:I

    .line 83
    sget v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mHeight:I

    sput v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mHeightPortrait:I

    .line 84
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0975

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mSrcwidth:I

    .line 85
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0976

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mSrcheight:I

    .line 86
    sget v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mSrcheight:I

    sput v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mSrcheightPortrait:I

    .line 87
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0977

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mSrcOffsetX:I

    .line 88
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0978

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mSrcOffsetY:I

    .line 89
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0979

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mTextSize:I

    .line 90
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a097a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mTextPading:I

    .line 91
    sget v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mSrcheight:I

    sget v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mTextSize:I

    add-int/2addr v0, v1

    sput v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mTextStartY:I

    .line 92
    sget v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mTextStartY:I

    sput v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mTextStartYPortrait:I

    .line 93
    return-void
.end method

.method public static init(Landroid/content/Context;Landroid/content/res/Resources;Ljava/lang/Object;Z)V
    .locals 0
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "r"    # Landroid/content/res/Resources;
    .param p2, "handler"    # Ljava/lang/Object;
    .param p3, "isAmap"    # Z

    .prologue
    .line 66
    sput-object p1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->res:Landroid/content/res/Resources;

    .line 67
    sput-boolean p3, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mIsAmap:Z

    .line 68
    invoke-static {p0, p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->init(Landroid/content/Context;Ljava/lang/Object;)V

    .line 69
    return-void
.end method

.method public static init(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "handler"    # Ljava/lang/Object;

    .prologue
    .line 71
    sget-boolean v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mIsAmap:Z

    if-eqz v0, :cond_0

    .line 72
    check-cast p1, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapPhotoList$UpdateHandler;

    .end local p1    # "handler":Ljava/lang/Object;
    sput-object p1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->aHandler:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapPhotoList$UpdateHandler;

    .line 76
    :goto_0
    sput-object p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mContext:Landroid/content/Context;

    .line 77
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->init()V

    .line 78
    return-void

    .line 74
    .restart local p1    # "handler":Ljava/lang/Object;
    :cond_0
    check-cast p1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapPhotoList$UpdateHandler;

    .end local p1    # "handler":Ljava/lang/Object;
    sput-object p1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->gHandler:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapPhotoList$UpdateHandler;

    goto :goto_0
.end method

.method private static makePhotoMarker(Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;)V
    .locals 5
    .param p0, "photodata"    # Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;

    .prologue
    const/high16 v4, -0x40800000    # -1.0f

    .line 232
    if-nez p0, :cond_1

    .line 259
    :cond_0
    :goto_0
    return-void

    .line 234
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->getFilePath()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->getLatitude()F

    move-result v3

    cmpl-float v3, v3, v4

    if-nez v3, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->getLongitude()F

    move-result v3

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_0

    .line 238
    :cond_2
    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mPhotoList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_3

    .line 239
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 240
    .local v2, "ph":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;>;"
    invoke-virtual {v2, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 241
    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mPhotoList:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 244
    .end local v2    # "ph":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;>;"
    :cond_3
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mPhotoList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_4

    .line 245
    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mPhotoList:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;

    .line 247
    .local v0, "data2":Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->getLatitude()F

    move-result v3

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->getLatitude()F

    move-result v4

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    sget v4, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->MIN_LOCATION_VALUE:F

    cmpg-float v3, v3, v4

    if-gez v3, :cond_5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->getLongitude()F

    move-result v3

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->getLongitude()F

    move-result v4

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    sget v4, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->MIN_LOCATION_VALUE:F

    cmpg-float v3, v3, v4

    if-gez v3, :cond_5

    .line 249
    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mPhotoList:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/ArrayList;

    invoke-virtual {v3, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 253
    .end local v0    # "data2":Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;
    :cond_4
    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mPhotoList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ne v1, v3, :cond_0

    .line 254
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 255
    .restart local v2    # "ph":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;>;"
    invoke-virtual {v2, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 256
    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mPhotoList:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 244
    .end local v2    # "ph":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;>;"
    .restart local v0    # "data2":Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private static makePhotoMarker(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p0, "photodatas":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;>;"
    const/4 v4, 0x0

    .line 214
    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mPhotoList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 215
    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->markerOPList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 216
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;

    .line 217
    .local v2, "phData":Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->getLatitude()F

    move-result v3

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->getLongitude()F

    move-result v3

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_0

    .line 218
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->makePhotoMarker(Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;)V

    goto :goto_0

    .line 221
    .end local v2    # "phData":Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mPhotoList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_2

    .line 222
    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mPhotoList:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;

    .line 223
    .restart local v2    # "phData":Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;
    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mPhotoList:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-static {v2, v0, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->setPhotoItem(Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;II)V

    .line 221
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 226
    .end local v2    # "phData":Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;
    :cond_2
    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mPhotoListener:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil$OnUpdatePhotoListListener;

    if-eqz v3, :cond_3

    .line 227
    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mPhotoListener:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil$OnUpdatePhotoListListener;

    sget-object v4, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->markerOPList:Ljava/util/ArrayList;

    invoke-interface {v3, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil$OnUpdatePhotoListListener;->onUpdatePhotoList(Ljava/util/List;)V

    .line 229
    :cond_3
    return-void
.end method

.method public static setOnUpdatePhotoListListener(Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil$OnUpdatePhotoListListener;)V
    .locals 0
    .param p0, "listener"    # Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil$OnUpdatePhotoListListener;

    .prologue
    .line 62
    sput-object p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mPhotoListener:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil$OnUpdatePhotoListListener;

    .line 63
    return-void
.end method

.method public static setPhotoItem(Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;II)V
    .locals 11
    .param p0, "photodata"    # Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;
    .param p1, "index"    # I
    .param p2, "count"    # I

    .prologue
    const/high16 v8, 0x3f800000    # 1.0f

    const/high16 v7, 0x3f000000    # 0.5f

    .line 119
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->getFilePath()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->getMapIconThumbnail(Ljava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 121
    .local v2, "bitmap":Landroid/graphics/Bitmap;
    if-nez v2, :cond_0

    .line 150
    :goto_0
    return-void

    .line 125
    :cond_0
    new-instance v5, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil$PhotoMarkerOptions;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->getFilePath()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil$PhotoMarkerOptions;-><init>(Ljava/lang/String;)V

    .line 127
    .local v5, "pMk":Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil$PhotoMarkerOptions;
    new-instance v4, Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-direct {v4}, Lcom/google/android/gms/maps/model/MarkerOptions;-><init>()V

    .line 128
    .local v4, "gmk":Lcom/google/android/gms/maps/model/MarkerOptions;
    new-instance v1, Lcom/amap/api/maps2d/model/MarkerOptions;

    invoke-direct {v1}, Lcom/amap/api/maps2d/model/MarkerOptions;-><init>()V

    .line 130
    .local v1, "aMK":Lcom/amap/api/maps2d/model/MarkerOptions;
    sget-boolean v6, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mIsAmap:Z

    if-eqz v6, :cond_1

    .line 131
    invoke-static {v2}, Lcom/amap/api/maps2d/model/BitmapDescriptorFactory;->fromBitmap(Landroid/graphics/Bitmap;)Lcom/amap/api/maps2d/model/BitmapDescriptor;

    move-result-object v0

    .line 132
    .local v0, "aDescriptor":Lcom/amap/api/maps2d/model/BitmapDescriptor;
    invoke-virtual {v1, v0}, Lcom/amap/api/maps2d/model/MarkerOptions;->icon(Lcom/amap/api/maps2d/model/BitmapDescriptor;)Lcom/amap/api/maps2d/model/MarkerOptions;

    .line 133
    invoke-virtual {v1, v7, v8}, Lcom/amap/api/maps2d/model/MarkerOptions;->anchor(FF)Lcom/amap/api/maps2d/model/MarkerOptions;

    .line 134
    new-instance v6, Lcom/amap/api/maps2d/model/LatLng;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->getLatitude()F

    move-result v7

    float-to-double v7, v7

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->getLongitude()F

    move-result v9

    float-to-double v9, v9

    invoke-direct {v6, v7, v8, v9, v10}, Lcom/amap/api/maps2d/model/LatLng;-><init>(DD)V

    invoke-virtual {v1, v6}, Lcom/amap/api/maps2d/model/MarkerOptions;->position(Lcom/amap/api/maps2d/model/LatLng;)Lcom/amap/api/maps2d/model/MarkerOptions;

    .line 135
    const-string v6, ""

    invoke-virtual {v1, v6}, Lcom/amap/api/maps2d/model/MarkerOptions;->title(Ljava/lang/String;)Lcom/amap/api/maps2d/model/MarkerOptions;

    .line 136
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/amap/api/maps2d/model/MarkerOptions;->snippet(Ljava/lang/String;)Lcom/amap/api/maps2d/model/MarkerOptions;

    .line 137
    invoke-virtual {v5, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil$PhotoMarkerOptions;->setMarkerOptions(Ljava/lang/Object;)V

    .line 138
    sget-object v6, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->markerOPList:Ljava/util/ArrayList;

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 141
    .end local v0    # "aDescriptor":Lcom/amap/api/maps2d/model/BitmapDescriptor;
    :cond_1
    invoke-static {v2}, Lcom/google/android/gms/maps/model/BitmapDescriptorFactory;->fromBitmap(Landroid/graphics/Bitmap;)Lcom/google/android/gms/maps/model/BitmapDescriptor;

    move-result-object v3

    .line 142
    .local v3, "gDescriptor":Lcom/google/android/gms/maps/model/BitmapDescriptor;
    invoke-virtual {v4, v3}, Lcom/google/android/gms/maps/model/MarkerOptions;->icon(Lcom/google/android/gms/maps/model/BitmapDescriptor;)Lcom/google/android/gms/maps/model/MarkerOptions;

    .line 143
    invoke-virtual {v4, v7, v8}, Lcom/google/android/gms/maps/model/MarkerOptions;->anchor(FF)Lcom/google/android/gms/maps/model/MarkerOptions;

    .line 144
    new-instance v6, Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->getLatitude()F

    move-result v7

    float-to-double v7, v7

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->getLongitude()F

    move-result v9

    float-to-double v9, v9

    invoke-direct {v6, v7, v8, v9, v10}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    invoke-virtual {v4, v6}, Lcom/google/android/gms/maps/model/MarkerOptions;->position(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/MarkerOptions;

    .line 145
    const-string v6, ""

    invoke-virtual {v4, v6}, Lcom/google/android/gms/maps/model/MarkerOptions;->title(Ljava/lang/String;)Lcom/google/android/gms/maps/model/MarkerOptions;

    .line 146
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/google/android/gms/maps/model/MarkerOptions;->snippet(Ljava/lang/String;)Lcom/google/android/gms/maps/model/MarkerOptions;

    .line 147
    invoke-virtual {v5, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil$PhotoMarkerOptions;->setMarkerOptions(Ljava/lang/Object;)V

    .line 148
    sget-object v6, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->markerOPList:Ljava/util/ArrayList;

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
