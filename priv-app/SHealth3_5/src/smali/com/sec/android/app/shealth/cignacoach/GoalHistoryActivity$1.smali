.class Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity$1;
.super Ljava/lang/Object;
.source "GoalHistoryActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$DateChageCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;)V
    .locals 0

    .prologue
    .line 288
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDateChanged()V
    .locals 0

    .prologue
    .line 301
    return-void
.end method

.method public onGoalMissionStatusChange(II)V
    .locals 4
    .param p1, "goalId"    # I
    .param p2, "missionId"    # I

    .prologue
    const/4 v3, 0x0

    .line 292
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mTask:Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity$OnGoalMissionStatusChangedTask;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->access$400(Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;)Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity$OnGoalMissionStatusChangedTask;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 294
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mTask:Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity$OnGoalMissionStatusChangedTask;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->access$400(Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;)Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity$OnGoalMissionStatusChangedTask;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity$OnGoalMissionStatusChangedTask;->cancel(Z)Z

    .line 296
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;

    new-instance v1, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity$OnGoalMissionStatusChangedTask;

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;

    invoke-direct {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity$OnGoalMissionStatusChangedTask;-><init>(Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;)V

    # setter for: Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mTask:Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity$OnGoalMissionStatusChangedTask;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->access$402(Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity$OnGoalMissionStatusChangedTask;)Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity$OnGoalMissionStatusChangedTask;

    .line 297
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mTask:Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity$OnGoalMissionStatusChangedTask;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->access$400(Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;)Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity$OnGoalMissionStatusChangedTask;

    move-result-object v0

    new-array v1, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity$OnGoalMissionStatusChangedTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 298
    return-void
.end method
