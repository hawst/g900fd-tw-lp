.class public Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;
.super Ljava/lang/Object;
.source "AwardUserBackUpActivity.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final BACKUP_PROGRESS_POPUP:Ljava/lang/String;

.field final back:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

.field private mAccountBackupNow:Landroid/widget/LinearLayout;

.field private mAccountlist:Landroid/widget/LinearLayout;

.field private mBackupProgress:Landroid/widget/TextView;

.field private mBackupProgressBar:Landroid/widget/ProgressBar;

.field private mBackupProgressBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

.field private mBackupProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

.field private mContext:Landroid/content/Context;

.field private mEmptyData:Landroid/widget/TextView;

.field private mSamsungAccount:Landroid/widget/TextView;

.field private mTextPercentage:Landroid/widget/TextView;

.field private mView:Landroid/view/View;

.field private mlastSync:Landroid/widget/TextView;

.field private nameOfPreviousActivity:Ljava/lang/String;

.field protected shcm:Lcom/samsung/android/sdk/health/content/ShealthContentManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 71
    const-class v0, Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .locals 2
    .param p1, "mContext"    # Landroid/content/Context;
    .param p2, "mView"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x0

    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    iput-object v1, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;->mContext:Landroid/content/Context;

    .line 73
    iput-object v1, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;->mView:Landroid/view/View;

    .line 74
    iput-object v1, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;->mEmptyData:Landroid/widget/TextView;

    .line 80
    const-string v0, "backup_progress_popup"

    iput-object v0, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;->BACKUP_PROGRESS_POPUP:Ljava/lang/String;

    .line 82
    iput-object v1, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;->nameOfPreviousActivity:Ljava/lang/String;

    .line 83
    iput-object v1, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;->mAccountBackupNow:Landroid/widget/LinearLayout;

    .line 84
    iput-object v1, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;->mAccountlist:Landroid/widget/LinearLayout;

    .line 87
    new-instance v0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;->back:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    .line 91
    iput-object p1, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;->mContext:Landroid/content/Context;

    .line 92
    iput-object p2, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;->mView:Landroid/view/View;

    .line 93
    invoke-direct {p0}, Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;->setViewArea()V

    .line 94
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private static getSystemDateFormat()Ljava/text/SimpleDateFormat;
    .locals 3

    .prologue
    .line 211
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    check-cast v1, Ljava/text/SimpleDateFormat;

    invoke-virtual {v1}, Ljava/text/SimpleDateFormat;->toPattern()Ljava/lang/String;

    move-result-object v0

    .line 214
    .local v0, "pattern":Ljava/lang/String;
    const-string v1, "dd"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 216
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "dd/MM/yyyy HH:mm"

    invoke-direct {v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 224
    :goto_0
    return-object v1

    .line 218
    :cond_0
    const-string v1, "MM"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 220
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "MM/dd/yyyy HH:mm"

    invoke-direct {v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 224
    :cond_1
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string/jumbo v2, "yyyy/MM/dd HH:mm"

    invoke-direct {v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private lastBackupData()V
    .locals 17

    .prologue
    .line 147
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v13

    invoke-static {v13}, Lcom/sec/android/app/shealth/common/utils/SamsungAccount;->getSamsungAccount(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 149
    .local v3, "email":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;->mlastSync:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->invalidate()V

    .line 150
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;->mSamsungAccount:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->invalidate()V

    .line 151
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;->mEmptyData:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->invalidate()V

    .line 153
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 154
    .local v4, "extras":Landroid/os/Bundle;
    const-string v13, "key"

    const-string v14, "last_backup_time"

    invoke-virtual {v4, v13, v14}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    const-string/jumbo v13, "value"

    const-wide/16 v14, 0x0

    invoke-virtual {v4, v13, v14, v15}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 156
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v13

    sget-object v14, Lcom/samsung/android/sdk/health/content/ShealthContract;->AUTHORITY_URI:Landroid/net/Uri;

    const-string v15, "CONFIG_OPTION_GET"

    const/16 v16, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v13, v14, v15, v0, v4}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v9

    .line 158
    .local v9, "result":Landroid/os/Bundle;
    const-wide/16 v7, 0x0

    .line 160
    .local v7, "lastBackupTime":J
    if-eqz v9, :cond_0

    .line 161
    const-string/jumbo v13, "value"

    invoke-virtual {v9, v13}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v7

    .line 164
    :cond_0
    if-nez v3, :cond_1

    .line 166
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;->mSamsungAccount:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;->mContext:Landroid/content/Context;

    const v15, 0x7f090905

    invoke-virtual {v14, v15}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 167
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;->mlastSync:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;->mContext:Landroid/content/Context;

    const v15, 0x7f090900

    invoke-virtual {v14, v15}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 169
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;->mEmptyData:Landroid/widget/TextView;

    const/16 v14, 0x8

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setVisibility(I)V

    .line 204
    :goto_0
    return-void

    .line 170
    :cond_1
    const-wide/16 v13, 0x0

    cmp-long v13, v7, v13

    if-nez v13, :cond_2

    .line 171
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;->mSamsungAccount:Landroid/widget/TextView;

    const/16 v14, 0x8

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setVisibility(I)V

    .line 172
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;->mlastSync:Landroid/widget/TextView;

    const/16 v14, 0x8

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setVisibility(I)V

    .line 173
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;->mEmptyData:Landroid/widget/TextView;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 175
    :cond_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v13

    const-string v14, "date_format"

    invoke-static {v13, v14}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 176
    .local v10, "sysdateformat":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v13

    const-string/jumbo v14, "time_12_24"

    invoke-static {v13, v14}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 177
    .local v11, "systimeformat":Ljava/lang/String;
    const-string v13, "-"

    const-string v14, "/"

    invoke-virtual {v10, v13, v14}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 178
    .local v2, "date":Ljava/lang/String;
    const/4 v12, 0x0

    .line 180
    .local v12, "time":Ljava/lang/String;
    if-eqz v11, :cond_3

    const-string v13, "12"

    invoke-virtual {v11, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_3

    .line 181
    const-string v12, "hh:mm a"

    .line 187
    :goto_1
    :try_start_0
    new-instance v5, Ljava/text/SimpleDateFormat;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v5, v13}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 194
    .local v5, "formatter":Ljava/text/DateFormat;
    :goto_2
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 195
    .local v1, "calendar":Ljava/util/Calendar;
    invoke-virtual {v1, v7, v8}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 196
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;->mSamsungAccount:Landroid/widget/TextView;

    invoke-virtual {v13, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 197
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;->mSamsungAccount:Landroid/widget/TextView;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setVisibility(I)V

    .line 198
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;->mlastSync:Landroid/widget/TextView;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setVisibility(I)V

    .line 199
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;->mlastSync:Landroid/widget/TextView;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;->mContext:Landroid/content/Context;

    const v16, 0x7f090d3e

    invoke-virtual/range {v15 .. v16}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v15

    invoke-virtual {v5, v15}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 200
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;->mEmptyData:Landroid/widget/TextView;

    const/16 v14, 0x8

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 183
    .end local v1    # "calendar":Ljava/util/Calendar;
    .end local v5    # "formatter":Ljava/text/DateFormat;
    :cond_3
    const-string v12, "HH:mm"

    goto :goto_1

    .line 189
    :catch_0
    move-exception v6

    .line 191
    .local v6, "iex":Ljava/lang/IllegalArgumentException;
    invoke-static {}, Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;->getSystemDateFormat()Ljava/text/SimpleDateFormat;

    move-result-object v5

    .restart local v5    # "formatter":Ljava/text/DateFormat;
    goto :goto_2
.end method

.method private setViewArea()V
    .locals 6

    .prologue
    .line 100
    iget-object v3, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;->mContext:Landroid/content/Context;

    const-string v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 101
    .local v0, "inflater":Landroid/view/LayoutInflater;
    iget-object v3, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;->mView:Landroid/view/View;

    const v4, 0x7f080081

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 102
    .local v2, "layout":Landroid/widget/LinearLayout;
    const/4 v1, 0x0

    .line 103
    .local v1, "item":Landroid/widget/LinearLayout;
    const v3, 0x7f030011

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .end local v1    # "item":Landroid/widget/LinearLayout;
    check-cast v1, Landroid/widget/LinearLayout;

    .line 106
    .restart local v1    # "item":Landroid/widget/LinearLayout;
    const v3, 0x7f08005e

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;->mlastSync:Landroid/widget/TextView;

    .line 107
    const v3, 0x7f08005d

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;->mSamsungAccount:Landroid/widget/TextView;

    .line 108
    const v3, 0x7f08005c

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;->mEmptyData:Landroid/widget/TextView;

    .line 109
    const v3, 0x7f08005f

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;->mAccountBackupNow:Landroid/widget/LinearLayout;

    .line 110
    iget-object v3, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;->mAccountBackupNow:Landroid/widget/LinearLayout;

    iget-object v4, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090d20

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 111
    const v3, 0x7f08005b

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;->mAccountlist:Landroid/widget/LinearLayout;

    .line 113
    iget-object v3, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;->mAccountlist:Landroid/widget/LinearLayout;

    new-instance v4, Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity$1;

    invoke-direct {v4, p0}, Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity$1;-><init>(Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;)V

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 133
    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 135
    invoke-direct {p0}, Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;->settingAccount()V

    .line 136
    invoke-direct {p0}, Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;->lastBackupData()V

    .line 142
    return-void
.end method

.method private settingAccount()V
    .locals 2

    .prologue
    .line 232
    iget-object v0, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;->mAccountBackupNow:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity$2;-><init>(Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 251
    return-void
.end method
