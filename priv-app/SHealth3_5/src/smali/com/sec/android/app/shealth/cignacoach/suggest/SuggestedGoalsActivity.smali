.class public Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;
.super Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedActivity;
.source "SuggestedGoalsActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity$2;,
        Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity$UpdateListAsyncTask;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mDateChangeCallback:Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$DateChageCallback;

.field private mGoalCategory:I

.field private mUpdateListAsyncTask:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity$UpdateListAsyncTask;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedActivity;-><init>()V

    .line 28
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->mGoalCategory:I

    .line 170
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity$1;-><init>(Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->mDateChangeCallback:Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$DateChageCallback;

    .line 191
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->updateHeaderMessage(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;

    .prologue
    .line 25
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->mGoalCategory:I

    return v0
.end method

.method private updateHeaderMessage(Ljava/lang/String;)V
    .locals 1
    .param p1, "coachMsg"    # Ljava/lang/String;

    .prologue
    .line 149
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->mCommentView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 150
    return-void
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 2

    .prologue
    .line 59
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedActivity;->customizeActionBar()V

    .line 60
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const v1, 0x7f090c5c

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 61
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitleIconVisibility(Z)V

    .line 62
    return-void
.end method

.method protected getAdapter()Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;
    .locals 4

    .prologue
    .line 119
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->clearAllData()V

    .line 120
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->mGoalCategory:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 121
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->mGoalCategory:I

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/util/SuggestedDataFactory;->createGoalListDataByCategory(Landroid/content/Context;I)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->mListData:Ljava/util/List;

    .line 125
    :goto_0
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->mListData:Ljava/util/List;

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;-><init>(Landroid/content/Context;Ljava/util/List;Ljava/util/List;)V

    return-object v0

    .line 123
    :cond_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/util/SuggestedDataFactory;->createGoalListData(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->mListData:Ljava/util/List;

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v2, -0x1

    .line 92
    const/16 v1, 0x6e

    if-ne p1, v1, :cond_0

    .line 94
    const/16 v1, 0x6f

    if-ne p2, v1, :cond_1

    .line 96
    if-eqz p3, :cond_0

    .line 97
    const-string v1, "intent_extra_index"

    invoke-virtual {p3, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 98
    .local v0, "removeIndex":I
    if-eq v0, v2, :cond_0

    .line 99
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->mSuggestedListAdapter:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;->getSectionForPosition(I)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->mSuggestedListAdapter:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;->getItem(I)Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->getTitleString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->removeItem(ILjava/lang/String;)V

    .line 101
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->mSuggestedListAdapter:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;->notifyDataSetChanged()V

    .line 114
    .end local v0    # "removeIndex":I
    :cond_0
    :goto_0
    return-void

    .line 104
    :cond_1
    const/16 v1, 0x70

    if-ne p2, v1, :cond_0

    .line 105
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->clearAllData()V

    .line 106
    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->mGoalCategory:I

    if-eq v1, v2, :cond_2

    .line 107
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->mGoalCategory:I

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/util/SuggestedDataFactory;->createGoalListDataByCategory(Landroid/content/Context;I)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->mListData:Ljava/util/List;

    .line 111
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->mSuggestedListAdapter:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->mListData:Ljava/util/List;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;->setItemsList(Ljava/util/List;)V

    goto :goto_0

    .line 109
    :cond_2
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/util/SuggestedDataFactory;->createGoalListData(Landroid/content/Context;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->mListData:Ljava/util/List;

    goto :goto_1
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, -0x1

    .line 34
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 35
    .local v0, "receiveIntent":Landroid/content/Intent;
    if-eqz v0, :cond_0

    .line 36
    const-string v1, "EXTRA_NAME_SUGGESTED_GOAL_CATEGORY"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->mGoalCategory:I

    .line 39
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedActivity;->onCreate(Landroid/os/Bundle;)V

    .line 41
    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->mGoalCategory:I

    if-eq v1, v2, :cond_1

    .line 42
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->mReassessBtn:Landroid/widget/LinearLayout;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 45
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->setActionBarNormalMode()V

    .line 46
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->setHeaderMessage()V

    .line 48
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->mDateChangeCallback:Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$DateChageCallback;

    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService;->registerDayChangedListener(Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$DateChageCallback;)V

    .line 49
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 68
    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 53
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedActivity;->onDestroy()V

    .line 54
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->mDateChangeCallback:Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$DateChageCallback;

    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService;->unregisterDayChangedListener(Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$DateChageCallback;)V

    .line 55
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 73
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    invoke-super/range {p0 .. p5}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedActivity;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    .line 74
    sget-object v2, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "position : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    if-nez p3, :cond_0

    .line 87
    :goto_0
    return-void

    .line 80
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->mSuggestedListAdapter:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;

    add-int/lit8 v3, p3, -0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;->getItem(I)Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;

    move-result-object v1

    .line 81
    .local v1, "selectedItem":Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsDetailActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 82
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v2, 0x24000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 83
    const-string v2, "EXTRA_NAME_GOAL_ID"

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->getId()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 84
    const-string v2, "intent_data"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 85
    const-string v2, "intent_extra_index"

    invoke-virtual {v0, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 86
    const/16 v2, 0x6e

    invoke-virtual {p0, v0, v2}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method public setActionBarNormalMode()V
    .locals 1

    .prologue
    .line 129
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    .line 131
    .local v0, "actoiniBarHelper":Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;
    if-nez v0, :cond_0

    .line 134
    :goto_0
    return-void

    .line 133
    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->removeAllActionBarButtons()V

    goto :goto_0
.end method

.method protected setHeaderMessage()V
    .locals 3

    .prologue
    .line 138
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->mSuggestedListAdapter:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;->getCount()I

    move-result v1

    if-nez v1, :cond_0

    .line 139
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->mCommentView:Landroid/widget/TextView;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 140
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->mNoSuggestTextView:Landroid/widget/TextView;

    const v2, 0x7f090331

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 141
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->mNoSuggestDescView:Landroid/widget/TextView;

    const v2, 0x7f090330

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 146
    :goto_0
    return-void

    .line 143
    :cond_0
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/SuggestedDataFactory;->getGoalCoachMessage()Ljava/lang/String;

    move-result-object v0

    .line 144
    .local v0, "coachMsg":Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->updateHeaderMessage(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected updateList()V
    .locals 3

    .prologue
    .line 154
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->mUpdateListAsyncTask:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity$UpdateListAsyncTask;

    if-eqz v0, :cond_0

    .line 155
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "UpdateListAsyncTask status: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->mUpdateListAsyncTask:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity$UpdateListAsyncTask;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity$UpdateListAsyncTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity$2;->$SwitchMap$android$os$AsyncTask$Status:[I

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->mUpdateListAsyncTask:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity$UpdateListAsyncTask;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity$UpdateListAsyncTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/AsyncTask$Status;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 166
    :cond_0
    :goto_0
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity$UpdateListAsyncTask;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity$UpdateListAsyncTask;-><init>(Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->mUpdateListAsyncTask:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity$UpdateListAsyncTask;

    .line 167
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->mUpdateListAsyncTask:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity$UpdateListAsyncTask;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity$UpdateListAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 168
    return-void

    .line 159
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->mUpdateListAsyncTask:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity$UpdateListAsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity$UpdateListAsyncTask;->cancel(Z)Z

    goto :goto_0

    .line 156
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
