.class public Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter;
.super Landroid/widget/BaseAdapter;
.source "CignaLibraryBaseListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter$TYPE;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Landroid/widget/BaseAdapter;"
    }
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDataList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 23
    .local p0, "this":Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter;, "Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter<TT;>;"
    .local p2, "dataList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TT;>;"
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter;->mContext:Landroid/content/Context;

    .line 25
    iput-object p2, p0, Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter;->mDataList:Ljava/util/ArrayList;

    .line 26
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 30
    .local p0, "this":Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter;, "Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter<TT;>;"
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter;->mDataList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter;->mDataList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    .prologue
    .line 35
    .local p0, "this":Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter;, "Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter<TT;>;"
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter;->mDataList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter;->mDataList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 40
    .local p0, "this":Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter;, "Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter<TT;>;"
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 11
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 46
    .local p0, "this":Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter;, "Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter<TT;>;"
    iget-object v8, p0, Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter;->mDataList:Ljava/util/ArrayList;

    invoke-virtual {v8, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    .line 48
    .local v5, "positionData":Ljava/lang/Object;, "TT;"
    instance-of v8, v5, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryBaseListData;

    if-eqz v8, :cond_a

    move-object v1, v5

    .line 50
    check-cast v1, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryBaseListData;

    .line 52
    .local v1, "cignaLibraryData":Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryBaseListData;
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryBaseListData;->getIcon()I

    move-result v8

    const/4 v9, -0x1

    if-eq v8, v9, :cond_5

    const/4 v3, 0x1

    .line 54
    .local v3, "haveIcon":Z
    :goto_0
    if-nez p2, :cond_7

    .line 55
    if-eqz v3, :cond_6

    .line 56
    iget-object v8, p0, Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter;->mContext:Landroid/content/Context;

    const v9, 0x7f030058

    const/4 v10, 0x0

    invoke-static {v8, v9, v10}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 57
    sget-object v8, Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter$TYPE;->WITH_ICON:Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter$TYPE;

    invoke-virtual {p2, v8}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 75
    :cond_0
    :goto_1
    const/4 v4, 0x0

    .line 76
    .local v4, "iconView":Landroid/widget/ImageView;
    if-eqz v3, :cond_1

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v8

    sget-object v9, Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter$TYPE;->WITH_ICON:Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter$TYPE;

    invoke-virtual {v8, v9}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 77
    const v8, 0x7f0801c6

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .end local v4    # "iconView":Landroid/widget/ImageView;
    check-cast v4, Landroid/widget/ImageView;

    .line 80
    .restart local v4    # "iconView":Landroid/widget/ImageView;
    :cond_1
    const v8, 0x7f0801c7

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 81
    .local v7, "titleView":Landroid/widget/TextView;
    const v8, 0x7f0801c8

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 83
    .local v6, "subInformationView":Landroid/widget/TextView;
    if-eqz v3, :cond_2

    if-eqz v4, :cond_2

    .line 84
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryBaseListData;->getIcon()I

    move-result v8

    invoke-virtual {v4, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 87
    :cond_2
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryBaseListData;->getTitle()Ljava/lang/CharSequence;

    move-result-object v8

    if-eqz v8, :cond_3

    .line 88
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryBaseListData;->getTitle()Ljava/lang/CharSequence;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 91
    :cond_3
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryBaseListData;->getSubInformaion()Ljava/lang/CharSequence;

    move-result-object v8

    if-eqz v8, :cond_9

    .line 92
    const/4 v8, 0x0

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 93
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryBaseListData;->getSubInformaion()Ljava/lang/CharSequence;

    move-result-object v8

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 114
    .end local v1    # "cignaLibraryData":Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryBaseListData;
    .end local v3    # "haveIcon":Z
    .end local v4    # "iconView":Landroid/widget/ImageView;
    .end local v6    # "subInformationView":Landroid/widget/TextView;
    .end local v7    # "titleView":Landroid/widget/TextView;
    :cond_4
    :goto_2
    const v8, 0x7f080129

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 115
    .local v2, "divider":Landroid/view/View;
    const/4 v8, 0x0

    invoke-virtual {v2, v8}, Landroid/view/View;->setVisibility(I)V

    .line 117
    return-object p2

    .line 52
    .end local v2    # "divider":Landroid/view/View;
    .restart local v1    # "cignaLibraryData":Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryBaseListData;
    :cond_5
    const/4 v3, 0x0

    goto :goto_0

    .line 60
    .restart local v3    # "haveIcon":Z
    :cond_6
    iget-object v8, p0, Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter;->mContext:Landroid/content/Context;

    const v9, 0x7f030059

    const/4 v10, 0x0

    invoke-static {v8, v9, v10}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 61
    sget-object v8, Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter$TYPE;->NO_ICON:Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter$TYPE;

    invoke-virtual {p2, v8}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_1

    .line 65
    :cond_7
    if-eqz v3, :cond_8

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v8

    sget-object v9, Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter$TYPE;->WITH_ICON:Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter$TYPE;

    invoke-virtual {v8, v9}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_8

    .line 66
    iget-object v8, p0, Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter;->mContext:Landroid/content/Context;

    const v9, 0x7f030058

    const/4 v10, 0x0

    invoke-static {v8, v9, v10}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 67
    sget-object v8, Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter$TYPE;->WITH_ICON:Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter$TYPE;

    invoke-virtual {p2, v8}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 69
    :cond_8
    if-nez v3, :cond_0

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v8

    sget-object v9, Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter$TYPE;->NO_ICON:Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter$TYPE;

    invoke-virtual {v8, v9}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 70
    iget-object v8, p0, Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter;->mContext:Landroid/content/Context;

    const v9, 0x7f030059

    const/4 v10, 0x0

    invoke-static {v8, v9, v10}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 71
    sget-object v8, Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter$TYPE;->NO_ICON:Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter$TYPE;

    invoke-virtual {p2, v8}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 96
    .restart local v4    # "iconView":Landroid/widget/ImageView;
    .restart local v6    # "subInformationView":Landroid/widget/TextView;
    .restart local v7    # "titleView":Landroid/widget/TextView;
    :cond_9
    const/16 v8, 0x8

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    .line 98
    .end local v1    # "cignaLibraryData":Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryBaseListData;
    .end local v3    # "haveIcon":Z
    .end local v4    # "iconView":Landroid/widget/ImageView;
    .end local v6    # "subInformationView":Landroid/widget/TextView;
    .end local v7    # "titleView":Landroid/widget/TextView;
    :cond_a
    instance-of v8, v5, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;

    if-eqz v8, :cond_4

    move-object v0, v5

    .line 100
    check-cast v0, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;

    .line 102
    .local v0, "articleData":Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;
    if-nez p2, :cond_b

    .line 104
    iget-object v8, p0, Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter;->mContext:Landroid/content/Context;

    const v9, 0x7f030059

    const/4 v10, 0x0

    invoke-static {v8, v9, v10}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 107
    :cond_b
    const v8, 0x7f0801c7

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 108
    .restart local v7    # "titleView":Landroid/widget/TextView;
    const v8, 0x7f0801c8

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 110
    .restart local v6    # "subInformationView":Landroid/widget/TextView;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->getTitle()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 111
    const/16 v8, 0x8

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_2
.end method
