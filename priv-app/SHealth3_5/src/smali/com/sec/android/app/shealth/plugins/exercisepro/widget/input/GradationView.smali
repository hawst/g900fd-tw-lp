.class public abstract Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;
.super Landroid/view/View;
.source "GradationView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$InertiaTimerTask;,
        Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$OnControllerTapListener;,
        Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$OnBackgroundChangedListener;,
        Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$OnValueChangedListener;
    }
.end annotation


# static fields
.field private static final DECADES_QUANTITY:I = 0x5

.field protected static final DEFALUT_NORMAL_RANGE_INDEX:I = -0x1

.field private static final DEFAULT_INTERVAL_VALUE:I = 0x1

.field private static final DEFAULT_MAX_INPUT_RANGE:I = 0x1388

.field private static final DEFAULT_MAX_NORMAL_RANGE:I = 0x2710

.field private static final DEFAULT_MIN_INPUT_RANGE:I = 0x64

.field private static final DEFAULT_MIN_NORMAL_RANGE:I = -0x3e8

.field private static final DEFAULT_START_VALUE:I = 0x1f4

.field private static final GRADATIONS_IN_DECADE_QUANTITY:I = 0xa

.field private static final INERTIA_ANIMATION_TIME_INTERVAL:I = 0x14

.field protected static final INERTIA_LIST_SIZE:I = 0x3

.field private static final MAX_DISTANCE:I = 0x2710

.field private static final NONE_CHANGE:I = -0x1

.field private static final NUMBER_FORMAT_STRING:Ljava/lang/String; = "###"


# instance fields
.field private isDrawNomalBottom:Z

.field protected isEnabledNormalRange:Z

.field private isTouchMoving:Z

.field protected mBetweenGradationBarsDistance:I

.field protected mDistanceToRedrawGradationView:I

.field protected mDrawableGreen:Landroid/graphics/drawable/ColorDrawable;

.field protected mDrawableOrange:Landroid/graphics/drawable/ColorDrawable;

.field protected mGradationArrayList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationObject;",
            ">;"
        }
    .end annotation
.end field

.field private mGradationWidth:I

.field protected mInertiaCoordList:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mInertionTimerTask:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$InertiaTimerTask;

.field private mInterval:I

.field private mIsInertia:Z

.field public mMaxInputRange:I

.field private mMaxNormalRange:I

.field protected mMaxNormalRangeIndex:I

.field private mMinInputRange:I

.field private mMinNormalRange:I

.field protected mMinNormalRangeIndex:I

.field protected mNumberDecimalFormat:Ljava/text/DecimalFormat;

.field protected mOnBackgroundChangedListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$OnBackgroundChangedListener;

.field private mOnControllerTapListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$OnControllerTapListener;

.field private mOnValueChangedListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$OnValueChangedListener;

.field protected mPaint:Landroid/graphics/Paint;

.field private mShouldRedrawGradationView:Z

.field protected mTextHalfHeight:I

.field private mTextSize:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 191
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 51
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "###"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mNumberDecimalFormat:Ljava/text/DecimalFormat;

    .line 67
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mInterval:I

    .line 69
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mMinInputRange:I

    .line 71
    const/16 v0, 0x1388

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mMaxInputRange:I

    .line 73
    const/16 v0, -0x3e8

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mMinNormalRange:I

    .line 75
    const/16 v0, 0x2710

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mMaxNormalRange:I

    .line 79
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->isTouchMoving:Z

    .line 81
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->isEnabledNormalRange:Z

    .line 83
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->isDrawNomalBottom:Z

    .line 91
    iput v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mMinNormalRangeIndex:I

    .line 93
    iput v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mMaxNormalRangeIndex:I

    .line 97
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mInertiaCoordList:Ljava/util/LinkedList;

    .line 193
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->initView()V

    .line 195
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 211
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 51
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "###"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mNumberDecimalFormat:Ljava/text/DecimalFormat;

    .line 67
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mInterval:I

    .line 69
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mMinInputRange:I

    .line 71
    const/16 v0, 0x1388

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mMaxInputRange:I

    .line 73
    const/16 v0, -0x3e8

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mMinNormalRange:I

    .line 75
    const/16 v0, 0x2710

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mMaxNormalRange:I

    .line 79
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->isTouchMoving:Z

    .line 81
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->isEnabledNormalRange:Z

    .line 83
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->isDrawNomalBottom:Z

    .line 91
    iput v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mMinNormalRangeIndex:I

    .line 93
    iput v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mMaxNormalRangeIndex:I

    .line 97
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mInertiaCoordList:Ljava/util/LinkedList;

    .line 213
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->initView()V

    .line 215
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 233
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 51
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "###"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mNumberDecimalFormat:Ljava/text/DecimalFormat;

    .line 67
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mInterval:I

    .line 69
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mMinInputRange:I

    .line 71
    const/16 v0, 0x1388

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mMaxInputRange:I

    .line 73
    const/16 v0, -0x3e8

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mMinNormalRange:I

    .line 75
    const/16 v0, 0x2710

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mMaxNormalRange:I

    .line 79
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->isTouchMoving:Z

    .line 81
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->isEnabledNormalRange:Z

    .line 83
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->isDrawNomalBottom:Z

    .line 91
    iput v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mMinNormalRangeIndex:I

    .line 93
    iput v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mMaxNormalRangeIndex:I

    .line 97
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mInertiaCoordList:Ljava/util/LinkedList;

    .line 235
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->initView()V

    .line 237
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;

    .prologue
    .line 23
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mIsInertia:Z

    return v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;
    .param p1, "x1"    # Z

    .prologue
    .line 23
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mShouldRedrawGradationView:Z

    return p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;
    .param p1, "x1"    # I

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->moveGradationView(I)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->changeValue()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$OnControllerTapListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mOnControllerTapListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$OnControllerTapListener;

    return-object v0
.end method

.method private changeValue()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 754
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mOnValueChangedListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$OnValueChangedListener;

    if-eqz v1, :cond_1

    .line 756
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->getValue()I

    move-result v0

    .line 758
    .local v0, "currentValue":I
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mMinInputRange:I

    if-gt v0, v1, :cond_2

    .line 760
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->stopInertia()V

    .line 762
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mMinInputRange:I

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->setValue(I)V

    .line 764
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mMinInputRange:I

    .line 765
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mOnControllerTapListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$OnControllerTapListener;

    invoke-interface {v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$OnControllerTapListener;->onControllerTapped(Z)V

    .line 778
    :cond_0
    :goto_0
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    .line 780
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mOnValueChangedListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$OnValueChangedListener;

    int-to-float v2, v0

    const/high16 v3, 0x41200000    # 10.0f

    div-float/2addr v2, v3

    invoke-interface {v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$OnValueChangedListener;->onValueChanged(F)V

    .line 786
    .end local v0    # "currentValue":I
    :cond_1
    return-void

    .line 767
    .restart local v0    # "currentValue":I
    :cond_2
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mMaxInputRange:I

    if-lt v0, v1, :cond_0

    .line 769
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->stopInertia()V

    .line 771
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mMaxInputRange:I

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->setValue(I)V

    .line 773
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mMaxInputRange:I

    .line 774
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mOnControllerTapListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$OnControllerTapListener;

    invoke-interface {v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$OnControllerTapListener;->onControllerTapped(Z)V

    goto :goto_0
.end method

.method private initView()V
    .locals 3

    .prologue
    .line 313
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0108

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mGradationWidth:I

    .line 315
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0106

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mTextSize:I

    .line 317
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mPaint:Landroid/graphics/Paint;

    .line 319
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mPaint:Landroid/graphics/Paint;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 321
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mTextSize:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 323
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 325
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mPaint:Landroid/graphics/Paint;

    const-string/jumbo v1, "sec-roboto-light"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 329
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->descent()F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->ascent()F

    move-result v1

    add-float/2addr v0, v1

    float-to-int v0, v0

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mTextHalfHeight:I

    .line 333
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070078

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mDrawableOrange:Landroid/graphics/drawable/ColorDrawable;

    .line 335
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070077

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mDrawableGreen:Landroid/graphics/drawable/ColorDrawable;

    .line 339
    const/16 v0, 0x1f4

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->setValue(I)V

    .line 343
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->initGradationBarDistance()V

    .line 345
    return-void
.end method

.method private moveGradationView(I)V
    .locals 3
    .param p1, "offset"    # I

    .prologue
    .line 863
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mGradationArrayList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationObject;

    .line 865
    .local v1, "object":Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationObject;
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationObject;->getOffsetMovement()I

    move-result v2

    add-int/2addr v2, p1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationObject;->setOffsetMovement(I)V

    goto :goto_0

    .line 869
    .end local v1    # "object":Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationObject;
    :cond_0
    return-void
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 10
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v8, 0x0

    const/4 v9, 0x1

    .line 650
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    move v1, v8

    .line 746
    :goto_0
    return v1

    .line 658
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 742
    :cond_1
    :goto_1
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->changeValue()V

    .line 744
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    invoke-interface {v1, v9}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    move v1, v9

    .line 746
    goto :goto_0

    .line 661
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mOnControllerTapListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$OnControllerTapListener;

    invoke-interface {v1, v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$OnControllerTapListener;->onControllerTapped(Z)V

    .line 662
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->stopInertia()V

    .line 664
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->putTouchCoords(Landroid/view/MotionEvent;)V

    .line 666
    iput-boolean v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->isTouchMoving:Z

    goto :goto_1

    .line 671
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mOnControllerTapListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$OnControllerTapListener;

    invoke-interface {v1, v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$OnControllerTapListener;->onControllerTapped(Z)V

    .line 672
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->putTouchCoords(Landroid/view/MotionEvent;)V

    .line 674
    iput-boolean v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->isTouchMoving:Z

    .line 676
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mInertiaCoordList:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    if-le v1, v9, :cond_1

    .line 678
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mInertiaCoordList:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v2

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mInertiaCoordList:Ljava/util/LinkedList;

    invoke-virtual {v1, v9}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    sub-float v1, v2, v1

    float-to-int v7, v1

    .line 680
    .local v7, "offsetMovement":I
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->getValue()I

    move-result v6

    .line 682
    .local v6, "currentValue":I
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mMinInputRange:I

    if-gt v6, v1, :cond_2

    if-gez v7, :cond_3

    :cond_2
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mMaxInputRange:I

    if-lt v6, v1, :cond_6

    if-gtz v7, :cond_6

    .line 686
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mOnControllerTapListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$OnControllerTapListener;

    invoke-interface {v1, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$OnControllerTapListener;->onControllerTapped(Z)V

    .line 687
    const/4 v1, 0x3

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->setAction(I)V

    .line 689
    iput-boolean v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->isTouchMoving:Z

    .line 690
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mMinInputRange:I

    if-gt v6, v1, :cond_5

    .line 691
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mMinInputRange:I

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->setValue(I)V

    .line 695
    :cond_4
    :goto_2
    iput-boolean v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->isTouchMoving:Z

    .line 702
    :goto_3
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->invalidate()V

    goto :goto_1

    .line 692
    :cond_5
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mMaxInputRange:I

    if-lt v6, v1, :cond_4

    .line 693
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mMaxInputRange:I

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->setValue(I)V

    goto :goto_2

    .line 698
    :cond_6
    invoke-direct {p0, v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->moveGradationView(I)V

    goto :goto_3

    .line 712
    .end local v6    # "currentValue":I
    .end local v7    # "offsetMovement":I
    :pswitch_2
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mIsInertia:Z

    if-nez v1, :cond_8

    .line 714
    iput-boolean v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mIsInertia:Z

    .line 716
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    .line 718
    .local v0, "inertiaTimer":Ljava/util/Timer;
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mInertiaCoordList:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_7

    .line 720
    new-instance v2, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$InertiaTimerTask;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mInertiaCoordList:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v3

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mInertiaCoordList:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    sub-float v1, v3, v1

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v1, v3

    invoke-direct {v2, p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$InertiaTimerTask;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;F)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mInertionTimerTask:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$InertiaTimerTask;

    .line 724
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mInertionTimerTask:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$InertiaTimerTask;

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x14

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 730
    :goto_4
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mInertiaCoordList:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->clear()V

    .line 736
    .end local v0    # "inertiaTimer":Ljava/util/Timer;
    :goto_5
    iput-boolean v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->isTouchMoving:Z

    move v1, v9

    .line 738
    goto/16 :goto_0

    .line 727
    .restart local v0    # "inertiaTimer":Ljava/util/Timer;
    :cond_7
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mOnControllerTapListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$OnControllerTapListener;

    invoke-interface {v1, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$OnControllerTapListener;->onControllerTapped(Z)V

    goto :goto_4

    .line 733
    .end local v0    # "inertiaTimer":Ljava/util/Timer;
    :cond_8
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mOnControllerTapListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$OnControllerTapListener;

    invoke-interface {v1, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$OnControllerTapListener;->onControllerTapped(Z)V

    goto :goto_5

    .line 658
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected drawAbnormalRangeBottomLine(IIIILandroid/graphics/Canvas;)V
    .locals 2
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I
    .param p5, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 391
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, p1, p2, p3, p4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 393
    .local v0, "rect":Landroid/graphics/Rect;
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mDrawableOrange:Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/ColorDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 395
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mDrawableOrange:Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {v1, p5}, Landroid/graphics/drawable/ColorDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 397
    return-void
.end method

.method protected abstract drawAbnormalRangeElements(Landroid/graphics/Canvas;)V
.end method

.method protected abstract drawGradationBars(Landroid/graphics/Canvas;)V
.end method

.method protected abstract drawNormalBottomLine(Landroid/graphics/Canvas;)V
.end method

.method public getInterval()I
    .locals 1

    .prologue
    .line 578
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mInterval:I

    return v0
.end method

.method public getValue()I
    .locals 8

    .prologue
    .line 409
    const/4 v5, -0x1

    .line 411
    .local v5, "result":I
    const/16 v3, 0x2710

    .line 413
    .local v3, "minDistance":I
    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mGradationArrayList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationObject;

    .line 415
    .local v4, "object":Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationObject;
    invoke-virtual {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationObject;->getOffsetMovement()I

    move-result v1

    .line 417
    .local v1, "changedMovement":I
    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v6

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v7

    invoke-static {v6, v7}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 419
    .local v0, "absMinMovement":I
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v6

    if-ne v0, v6, :cond_0

    .line 421
    move v3, v1

    .line 423
    invoke-virtual {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationObject;->getValue()F

    move-result v6

    const/high16 v7, 0x41200000    # 10.0f

    mul-float/2addr v6, v7

    float-to-int v5, v6

    goto :goto_0

    .line 429
    .end local v0    # "absMinMovement":I
    .end local v1    # "changedMovement":I
    .end local v4    # "object":Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationObject;
    :cond_1
    return v5
.end method

.method protected abstract initGradationBarDistance()V
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 355
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mGradationWidth:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 357
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->drawGradationBars(Landroid/graphics/Canvas;)V

    .line 358
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->isDrawNomalBottom:Z

    if-eqz v0, :cond_0

    .line 359
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->drawNormalBottomLine(Landroid/graphics/Canvas;)V

    .line 360
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->drawAbnormalRangeElements(Landroid/graphics/Canvas;)V

    .line 365
    :cond_0
    return-void
.end method

.method protected onFocusLost()V
    .locals 0

    .prologue
    .line 634
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->stopInertia()V

    .line 636
    invoke-super {p0}, Landroid/view/View;->onFocusLost()V

    .line 638
    return-void
.end method

.method protected abstract putTouchCoords(Landroid/view/MotionEvent;)V
.end method

.method public setInputRange(II)V
    .locals 1
    .param p1, "minInputRange"    # I
    .param p2, "maxInputRange"    # I

    .prologue
    .line 596
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mMinInputRange:I

    .line 598
    iput p2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mMaxInputRange:I

    .line 600
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->getValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->setValue(I)V

    .line 602
    return-void
.end method

.method public setInterval(I)V
    .locals 1
    .param p1, "interval"    # I

    .prologue
    .line 562
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mInterval:I

    .line 564
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->getValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->setValue(I)V

    .line 566
    return-void
.end method

.method public setNoamlLanghtVisible(Z)V
    .locals 0
    .param p1, "visible"    # Z

    .prologue
    .line 368
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->isDrawNomalBottom:Z

    .line 369
    return-void
.end method

.method public setNormalRange(II)V
    .locals 1
    .param p1, "minNormalRange"    # I
    .param p2, "maxNormalRange"    # I

    .prologue
    .line 618
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->isEnabledNormalRange:Z

    .line 620
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mMinNormalRange:I

    .line 622
    iput p2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mMaxNormalRange:I

    .line 624
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->getValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->setValue(I)V

    .line 626
    return-void
.end method

.method public setOnBackgroundChangedListener(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$OnBackgroundChangedListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$OnBackgroundChangedListener;

    .prologue
    .line 839
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mOnBackgroundChangedListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$OnBackgroundChangedListener;

    .line 841
    return-void
.end method

.method public setOnControllerTapListener(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$OnControllerTapListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$OnControllerTapListener;

    .prologue
    .line 855
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mOnControllerTapListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$OnControllerTapListener;

    .line 857
    return-void
.end method

.method public setOnValueChangedListener(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$OnValueChangedListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$OnValueChangedListener;

    .prologue
    .line 823
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mOnValueChangedListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$OnValueChangedListener;

    .line 825
    return-void
.end method

.method public setValue(I)V
    .locals 14
    .param p1, "value"    # I

    .prologue
    .line 445
    iget-boolean v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->isTouchMoving:Z

    if-nez v11, :cond_0

    iget-boolean v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mIsInertia:Z

    if-eqz v11, :cond_1

    iget-boolean v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mShouldRedrawGradationView:Z

    if-nez v11, :cond_1

    .line 548
    :cond_0
    :goto_0
    return-void

    .line 453
    :cond_1
    const/4 v11, 0x0

    iput-boolean v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mShouldRedrawGradationView:Z

    .line 455
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    iput-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mGradationArrayList:Ljava/util/ArrayList;

    .line 457
    const/4 v11, -0x1

    iput v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mMaxNormalRangeIndex:I

    iput v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mMinNormalRangeIndex:I

    .line 461
    const/4 v7, 0x0

    .line 465
    .local v7, "nonBigAndThickLineValue":I
    iget v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mInterval:I

    mul-int/lit8 v11, v11, 0xa

    rem-int v11, p1, v11

    if-eqz v11, :cond_2

    .line 467
    iget v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mInterval:I

    mul-int/lit8 v11, v11, 0xa

    rem-int v7, p1, v11

    .line 469
    sub-int/2addr p1, v7

    .line 473
    :cond_2
    const/4 v3, 0x0

    .line 475
    .local v3, "isBig":Z
    const/4 v4, 0x0

    .line 477
    .local v4, "isMiddle":Z
    const/4 v0, 0x0

    .line 479
    .local v0, "distanceMovement":I
    const/4 v9, -0x5

    .local v9, "tensPlace":I
    :goto_1
    const/4 v11, 0x5

    if-ge v9, v11, :cond_8

    .line 481
    const/4 v10, 0x0

    .local v10, "unitsPlace":I
    :goto_2
    const/16 v11, 0xa

    if-ge v10, v11, :cond_7

    .line 483
    if-nez v10, :cond_4

    .line 485
    const/4 v3, 0x1

    .line 487
    const/4 v4, 0x1

    .line 505
    :goto_3
    int-to-float v11, p1

    const/high16 v12, 0x41200000    # 10.0f

    div-float/2addr v11, v12

    int-to-float v12, v9

    const/high16 v13, 0x41200000    # 10.0f

    mul-float/2addr v12, v13

    int-to-float v13, v10

    add-float/2addr v12, v13

    iget v13, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mInterval:I

    int-to-float v13, v13

    mul-float/2addr v12, v13

    const/high16 v13, 0x41200000    # 10.0f

    div-float/2addr v12, v13

    add-float v1, v11, v12

    .line 511
    .local v1, "gradationValue":F
    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mGradationArrayList:Ljava/util/ArrayList;

    new-instance v12, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationObject;

    invoke-direct {v12, v1, v0, v3, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationObject;-><init>(FIZZ)V

    invoke-virtual {v11, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 514
    iget v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mMinNormalRange:I

    int-to-float v11, v11

    const/high16 v12, 0x41200000    # 10.0f

    div-float/2addr v11, v12

    cmpg-float v11, v1, v11

    if-gtz v11, :cond_6

    .line 516
    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mGradationArrayList:Ljava/util/ArrayList;

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v11

    add-int/lit8 v11, v11, -0x1

    iput v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mMinNormalRangeIndex:I

    .line 526
    :cond_3
    :goto_4
    iget v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mBetweenGradationBarsDistance:I

    iget v12, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mGradationWidth:I

    add-int/2addr v11, v12

    add-int/2addr v0, v11

    .line 481
    add-int/lit8 v10, v10, 0x1

    goto :goto_2

    .line 489
    .end local v1    # "gradationValue":F
    :cond_4
    const/4 v11, 0x5

    if-ne v10, v11, :cond_5

    .line 491
    const/4 v3, 0x0

    .line 493
    const/4 v4, 0x1

    goto :goto_3

    .line 497
    :cond_5
    const/4 v3, 0x0

    .line 499
    const/4 v4, 0x0

    goto :goto_3

    .line 518
    .restart local v1    # "gradationValue":F
    :cond_6
    iget v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mMaxNormalRange:I

    int-to-float v11, v11

    const/high16 v12, 0x41200000    # 10.0f

    div-float/2addr v11, v12

    cmpl-float v11, v1, v11

    if-lez v11, :cond_3

    iget v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mMaxNormalRangeIndex:I

    const/4 v12, -0x1

    if-ne v11, v12, :cond_3

    .line 522
    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mGradationArrayList:Ljava/util/ArrayList;

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v11

    add-int/lit8 v11, v11, -0x1

    iput v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mMaxNormalRangeIndex:I

    goto :goto_4

    .line 479
    .end local v1    # "gradationValue":F
    :cond_7
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 534
    .end local v10    # "unitsPlace":I
    :cond_8
    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mGradationArrayList:Ljava/util/ArrayList;

    iget-object v12, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mGradationArrayList:Ljava/util/ArrayList;

    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v12

    div-int/lit8 v12, v12, 0x2

    iget v13, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mInterval:I

    div-int v13, v7, v13

    add-int/2addr v12, v13

    invoke-virtual {v11, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationObject;

    .line 538
    .local v5, "middleObject":Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationObject;
    invoke-virtual {v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationObject;->getOffsetMovement()I

    move-result v6

    .line 540
    .local v6, "middleObjectMovementPosition":I
    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mGradationArrayList:Ljava/util/ArrayList;

    invoke-virtual {v11}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationObject;

    .line 542
    .local v8, "object":Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationObject;
    invoke-virtual {v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationObject;->getOffsetMovement()I

    move-result v11

    sub-int/2addr v11, v6

    invoke-virtual {v8, v11}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationObject;->setOffsetMovement(I)V

    goto :goto_5

    .line 546
    .end local v8    # "object":Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationObject;
    :cond_9
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->invalidate()V

    goto/16 :goto_0
.end method

.method public stopInertia()V
    .locals 1

    .prologue
    .line 798
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mInertionTimerTask:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$InertiaTimerTask;

    if-eqz v0, :cond_0

    .line 800
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mIsInertia:Z

    .line 802
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mInertionTimerTask:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$InertiaTimerTask;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$InertiaTimerTask;->cancel()Z

    .line 805
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->invalidate()V

    .line 809
    :cond_0
    return-void
.end method
