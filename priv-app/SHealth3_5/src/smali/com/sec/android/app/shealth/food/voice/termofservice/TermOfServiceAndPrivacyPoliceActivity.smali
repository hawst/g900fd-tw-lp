.class public Lcom/sec/android/app/shealth/food/voice/termofservice/TermOfServiceAndPrivacyPoliceActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "TermOfServiceAndPrivacyPoliceActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 3

    .prologue
    .line 29
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    .line 30
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/voice/termofservice/TermOfServiceAndPrivacyPoliceActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/voice/termofservice/TermOfServiceAndPrivacyPoliceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "ACTION_BAR_TITLE"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(Ljava/lang/String;)V

    .line 31
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 35
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 37
    new-instance v0, Landroid/view/View;

    invoke-direct {v0, p0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/voice/termofservice/TermOfServiceAndPrivacyPoliceActivity;->setContentView(Landroid/view/View;)V

    .line 38
    return-void
.end method
