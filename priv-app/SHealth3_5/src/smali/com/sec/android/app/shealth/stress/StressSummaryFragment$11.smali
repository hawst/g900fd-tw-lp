.class Lcom/sec/android/app/shealth/stress/StressSummaryFragment$11;
.super Ljava/lang/Object;
.source "StressSummaryFragment.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->scaleAnimationStarter(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

.field final synthetic val$isGreen:Z


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;Z)V
    .locals 0

    .prologue
    .line 1406
    iput-object p1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$11;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    iput-boolean p2, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$11;->val$isGreen:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 1430
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$11;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondCenterIcon:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$000(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleX(F)V

    .line 1431
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$11;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondCenterIcon:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$000(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleY(F)V

    .line 1432
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$11;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mScaleAnimator:Landroid/animation/ValueAnimator;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$1102(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;Landroid/animation/ValueAnimator;)Landroid/animation/ValueAnimator;

    .line 1433
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 4
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    .line 1419
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$11;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # invokes: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->lightningImageChange()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$2800(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)V

    .line 1420
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$11;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    const-wide/16 v1, 0x3e8

    # invokes: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->stressLightningAlphaAnimation(J)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$2900(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;J)V

    .line 1421
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$11;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondCenterIcon:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$000(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/widget/ImageView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1422
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$11;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondCenterIconBigLayout:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$200(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/widget/RelativeLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1423
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$11;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondCenterIcon:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$000(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setScaleX(F)V

    .line 1424
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$11;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondCenterIcon:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$000(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setScaleY(F)V

    .line 1425
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$11;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mScaleAnimator:Landroid/animation/ValueAnimator;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$1102(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;Landroid/animation/ValueAnimator;)Landroid/animation/ValueAnimator;

    .line 1426
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 1415
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 3
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 1410
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$11;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondCenterIcon:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$000(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/widget/ImageView;

    move-result-object v1

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$11;->val$isGreen:Z

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    const v0, 0x7f020633

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1411
    return-void

    .line 1410
    :cond_0
    const v0, 0x7f020634

    goto :goto_0
.end method
