.class public Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;
.super Ljava/lang/Object;
.source "CoachRestoreHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$6;,
        Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;
    }
.end annotation


# static fields
.field public static final RESTORE_ERROR_POPUP:Ljava/lang/String; = "RESTORE_ERROR_POPUP"

.field public static final RESTORE_PROGRESS_POPUP:Ljava/lang/String; = "RESTORE_PROGRESS_POPUP"

.field public static final RESTORE_SUCCESS_POPUP:Ljava/lang/String; = "RESTORE_SUCCESS_POPUP"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private currentStep:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;

.field private isBackup:Z

.field private mContext:Landroid/content/Context;

.field private mFailedMsgDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

.field private mPopupHandler:Landroid/os/Handler;

.field private mRestartAppDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

.field private mRestoreProgress:Landroid/widget/TextView;

.field private mRestoreProgressBar:Landroid/widget/ProgressBar;

.field private mRestoreProgressBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

.field private mRestoreProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

.field private mRestoreProgressMsg:Landroid/widget/TextView;

.field private mTextPercentage:Landroid/widget/TextView;

.field private netUtils:Lcom/sec/android/app/shealth/common/utils/NetworkUtils;

.field private popupStatus:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mRestoreProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 37
    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mFailedMsgDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 38
    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mRestartAppDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 46
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;->NONE:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->currentStep:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;

    .line 47
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;->NONE:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->popupStatus:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;

    .line 49
    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->netUtils:Lcom/sec/android/app/shealth/common/utils/NetworkUtils;

    .line 50
    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mPopupHandler:Landroid/os/Handler;

    .line 69
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mContext:Landroid/content/Context;

    .line 70
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;

    invoke-direct {v0, p1}, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->netUtils:Lcom/sec/android/app/shealth/common/utils/NetworkUtils;

    .line 71
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mPopupHandler:Landroid/os/Handler;

    .line 72
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;)Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->currentStep:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;)Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->currentStep:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mRestoreProgressBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    return-object v0
.end method

.method static synthetic access$1000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mRestoreProgressBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->cignaBackupRestore(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    .prologue
    .line 30
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->isBackup:Z

    return v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mPopupHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->showRestoreSuccessPopup()V

    return-void
.end method

.method static synthetic access$1500(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;
    .param p1, "x1"    # Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->showRestoreFailedPopup(Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;)V

    return-void
.end method

.method static synthetic access$1600(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mFailedMsgDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    return-object v0
.end method

.method static synthetic access$1602(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mFailedMsgDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    return-object p1
.end method

.method static synthetic access$1700(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;)Lcom/sec/android/app/shealth/common/utils/NetworkUtils;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->netUtils:Lcom/sec/android/app/shealth/common/utils/NetworkUtils;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mRestartAppDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    return-object v0
.end method

.method static synthetic access$1802(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mRestartAppDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mRestoreProgressMsg:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mRestoreProgressMsg:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;)Landroid/widget/ProgressBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mRestoreProgressBar:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$402(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;Landroid/widget/ProgressBar;)Landroid/widget/ProgressBar;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;
    .param p1, "x1"    # Landroid/widget/ProgressBar;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mRestoreProgressBar:Landroid/widget/ProgressBar;

    return-object p1
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mRestoreProgress:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$502(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mRestoreProgress:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mTextPercentage:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$602(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mTextPercentage:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$702(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;)Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->popupStatus:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;

    return-object p1
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->stopBackupOrRestore()V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mRestoreProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    return-object v0
.end method

.method static synthetic access$902(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mRestoreProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    return-object p1
.end method

.method private cignaBackupRestore(Ljava/lang/String;)Z
    .locals 5
    .param p1, "isBackUpRestore"    # Ljava/lang/String;

    .prologue
    const v3, 0x7f090139

    .line 175
    new-instance v0, Lcom/cigna/coach/utils/backuprestore/JournalHelper;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;-><init>(Landroid/content/Context;)V

    .line 176
    .local v0, "helper":Lcom/cigna/coach/utils/backuprestore/JournalHelper;
    const/4 v1, 0x0

    .line 177
    .local v1, "result":Z
    const-string v2, "BACKUP"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 178
    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->showPopupWithProgressBar(I)V

    .line 179
    sget-object v2, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;->START_BACKUP:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;

    iput-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->currentStep:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;

    .line 180
    new-instance v2, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$2;-><init>(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;)V

    invoke-virtual {v0, v2}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->backup(Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;)Z

    move-result v1

    .line 292
    :goto_0
    if-nez v1, :cond_0

    .line 293
    sget-object v2, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "FAILED <in cignaBackupRestore> "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 296
    :cond_0
    return v1

    .line 217
    :cond_1
    iget-boolean v2, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->isBackup:Z

    if-nez v2, :cond_2

    .line 218
    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->showPopupWithProgressBar(I)V

    .line 220
    :cond_2
    sget-object v2, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;->START_RESTORE:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;

    iput-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->currentStep:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;

    .line 221
    new-instance v2, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$3;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$3;-><init>(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;)V

    invoke-virtual {v0, v2}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->restore(Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;)Z

    move-result v1

    goto :goto_0
.end method

.method private showRestoreFailedPopup(Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;)V
    .locals 4
    .param p1, "taskError"    # Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    .prologue
    .line 339
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "show coach restore error popup"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 341
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mContext:Landroid/content/Context;

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 342
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    const v1, 0x7f090139

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 343
    const v1, 0x7f09004c

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 344
    const v1, 0x7f09004b

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 346
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$6;->$SwitchMap$com$cigna$coach$interfaces$IBackupAndRestore$BRError:[I

    invoke-virtual {p1}, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 355
    const v1, 0x7f090d2e

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 358
    :goto_0
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mFailedMsgDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 359
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;->FAILED_POPUP:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->popupStatus:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;

    .line 360
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mFailedMsgDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v3, "RESTORE_ERROR_POPUP"

    invoke-virtual {v2, v1, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 361
    return-void

    .line 349
    :pswitch_0
    const v1, 0x7f0907f7

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    goto :goto_0

    .line 352
    :pswitch_1
    const v1, 0x7f0907f6

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    goto :goto_0

    .line 346
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private showRestoreSuccessPopup()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 365
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "show coach restore success popup"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 367
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 368
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    const v1, 0x7f0900d8

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 369
    const v1, 0x7f0907fb

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 370
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mRestartAppDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 371
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mRestartAppDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v1, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->setCancelable(Z)V

    .line 372
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;->SUCCESS_POPUP:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->popupStatus:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;

    .line 373
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mRestartAppDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v3, "RESTORE_SUCCESS_POPUP"

    invoke-virtual {v2, v1, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 374
    return-void
.end method

.method private stopBackupOrRestore()V
    .locals 3

    .prologue
    .line 100
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "stopBackupOrRestore called for "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->isBackup:Z

    if-eqz v0, :cond_0

    const-string v0, "Backup"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " work"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->closeProgressDialog()V

    .line 102
    return-void

    .line 100
    :cond_0
    const-string v0, "Restore"

    goto :goto_0
.end method


# virtual methods
.method public closeProgressDialog()V
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mRestoreProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mRestoreProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mRestoreProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->dismissAllowingStateLoss()V

    .line 93
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mRestoreProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 95
    :cond_0
    return-void
.end method

.method public getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 406
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$5;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$5;-><init>(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;Ljava/lang/String;)V

    return-object v0
.end method

.method public onException(Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;)V
    .locals 3
    .param p1, "taskError"    # Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    .prologue
    .line 318
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onException error : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 320
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->closeProgressDialog()V

    .line 322
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->stopBackupOrRestore()V

    .line 324
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;->NONE:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->currentStep:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;

    .line 325
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;->NONE:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->popupStatus:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;

    .line 328
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mPopupHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$4;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$4;-><init>(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 335
    return-void
.end method

.method public restoreState()V
    .locals 3

    .prologue
    .line 378
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$6;->$SwitchMap$com$sec$android$app$shealth$cignacoach$restore$CoachRestoreHelper$Step:[I

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->popupStatus:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 401
    :cond_0
    :goto_0
    return-void

    .line 380
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->popupStatus:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;

    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;->RESTORATION_PROGRESS_POPUP:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;

    if-ne v0, v1, :cond_0

    .line 381
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mRestoreProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mRestoreProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isVisible()Z

    move-result v0

    if-nez v0, :cond_0

    .line 382
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mRestoreProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v2, "RESTORE_PROGRESS_POPUP"

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 387
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mFailedMsgDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mFailedMsgDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isVisible()Z

    move-result v0

    if-nez v0, :cond_0

    .line 388
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Show again "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->popupStatus:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 389
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mFailedMsgDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v2, "RESTORE_ERROR_POPUP"

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 393
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mRestartAppDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mRestartAppDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isVisible()Z

    move-result v0

    if-nez v0, :cond_0

    .line 394
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Show again "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->popupStatus:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 395
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mRestartAppDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v2, "RESTORE_SUCCESS_POPUP"

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 378
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public showPopupWithProgressBar(I)V
    .locals 4
    .param p1, "popup_msg"    # I

    .prologue
    .line 107
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "progress popup showing"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->closeProgressDialog()V

    .line 111
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mPopupHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$1;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$1;-><init>(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;I)V

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 172
    return-void
.end method

.method public startCignaRestore(Z)V
    .locals 3
    .param p1, "isBackup"    # Z

    .prologue
    .line 300
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Backup before Restore?? : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 302
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->isBackup:Z

    .line 303
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->netUtils:Lcom/sec/android/app/shealth/common/utils/NetworkUtils;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;->isNetworkConnected()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 305
    if-eqz p1, :cond_0

    .line 306
    const-string v0, "BACKUP"

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->cignaBackupRestore(Ljava/lang/String;)Z

    .line 315
    :goto_0
    return-void

    .line 308
    :cond_0
    const-string v0, "RESTORE"

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->cignaBackupRestore(Ljava/lang/String;)Z

    goto :goto_0

    .line 313
    :cond_1
    sget-object v0, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->ERROR_NETWORK:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->onException(Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;)V

    goto :goto_0
.end method

.method public updateProgressPopupText(I)V
    .locals 2
    .param p1, "textID"    # I

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mRestoreProgressMsg:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mRestoreProgressMsg:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v1, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 87
    :cond_0
    return-void
.end method

.method public updateProgressPopupValue(I)V
    .locals 4
    .param p1, "percentage"    # I

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mRestoreProgressBar:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mRestoreProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 78
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mTextPercentage:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 79
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mTextPercentage:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mContext:Landroid/content/Context;

    const v3, 0x7f090d15

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 81
    :cond_1
    return-void
.end method
