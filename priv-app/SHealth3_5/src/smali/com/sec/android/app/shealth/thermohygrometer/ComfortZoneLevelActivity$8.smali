.class Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$8;
.super Ljava/lang/Object;
.source "ComfortZoneLevelActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->getContentInitializationListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;)V
    .locals 0

    .prologue
    .line 683
    iput-object p1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$8;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onContentInitialization(Landroid/view/View;Landroid/app/Activity;Landroid/app/Dialog;Landroid/os/Bundle;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V
    .locals 6
    .param p1, "content"    # Landroid/view/View;
    .param p2, "parentActivity"    # Landroid/app/Activity;
    .param p3, "dialog"    # Landroid/app/Dialog;
    .param p4, "savedInstanceState"    # Landroid/os/Bundle;
    .param p5, "okButtonHandler"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;

    .prologue
    const v3, 0x7f0802c9

    const/4 v2, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 686
    const-string v0, "C"

    sget-object v1, Lcom/sec/android/app/shealth/thermohygrometer/utils/TGHCache;->infoTemperatureUnit:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 687
    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0910a6

    invoke-virtual {p2, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$8;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->acceptableRangeTemperature:F
    invoke-static {v3}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->access$1400(Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    const-string v3, "16%"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 693
    :goto_0
    return-void

    .line 690
    :cond_0
    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0910a5

    invoke-virtual {p2, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$8;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->acceptableRangeTemperature:F
    invoke-static {v3}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->access$1400(Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    const-string v3, "16%"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
