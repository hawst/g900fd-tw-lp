.class public Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;
.super Ljava/lang/Object;
.source "WorkoutResult.java"


# instance fields
.field public activity_exID:J

.field public activity_type:I

.field public ave_speed_cnt:I

.field public ave_speed_for_calorie:F

.field public cadence_count:I

.field public duration:F

.field public elevation:F

.field public heartrate_count:I

.field public last_cadence:F

.field public last_calories:F

.field public last_calories_ete:F

.field public last_calories_pedo:F

.field public last_calories_sum:F

.field public last_distance:F

.field public last_distance_sum:F

.field public last_ete_calorie_speed_source:I

.field public last_heartrate:F

.field public last_pedo_calorie:F

.field public last_pedo_distance:F

.field public last_pedo_speed:F

.field public last_speed:F

.field public max_cadence:F

.field public max_heartrate:F

.field public max_speed:F

.field public paceGuide:I

.field public speed_count:I

.field public start_time:J

.field public total_cadence:D

.field public total_heartrate:D

.field public total_speed:D

.field public total_speed_for_calorie:F

.field public type:I


# direct methods
.method public constructor <init>(I)V
    .locals 7
    .param p1, "type"    # I

    .prologue
    const/4 v6, 0x0

    const/4 v5, -0x1

    const-wide/16 v3, 0x0

    const/4 v2, 0x0

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->type:I

    .line 47
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 48
    iput v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->activity_type:I

    .line 52
    :goto_0
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->activity_exID:J

    .line 53
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->start_time:J

    .line 54
    iput v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->duration:F

    .line 55
    iput v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_calories:F

    .line 57
    iput v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_calories_ete:F

    .line 58
    iput v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_calories_pedo:F

    .line 59
    iput v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_pedo_speed:F

    .line 60
    iput v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_pedo_distance:F

    .line 61
    iput v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_pedo_calorie:F

    .line 62
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_ete_calorie_speed_source:I

    .line 64
    iput v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_distance:F

    .line 65
    iput v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_speed:F

    .line 66
    iput-wide v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->total_speed:D

    .line 67
    iput v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->speed_count:I

    .line 68
    iput v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->max_speed:F

    .line 69
    iput v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_heartrate:F

    .line 70
    iput-wide v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->total_heartrate:D

    .line 71
    iput v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->heartrate_count:I

    .line 72
    iput v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->max_heartrate:F

    .line 73
    iput v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->elevation:F

    .line 74
    iput v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->paceGuide:I

    .line 75
    return-void

    .line 50
    :cond_0
    const/16 v0, 0x4653

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->activity_type:I

    goto :goto_0
.end method


# virtual methods
.method public setCadence(F)V
    .locals 4
    .param p1, "cadence"    # F

    .prologue
    .line 125
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_cadence:F

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_0

    .line 126
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_cadence:F

    .line 128
    :cond_0
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->cadence_count:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->cadence_count:I

    .line 129
    iget-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->total_cadence:D

    float-to-double v2, p1

    add-double/2addr v0, v2

    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->total_cadence:D

    .line 130
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->max_cadence:F

    cmpg-float v0, v0, p1

    if-gez v0, :cond_1

    .line 131
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->max_cadence:F

    .line 132
    :cond_1
    return-void
.end method

.method public setHeartrate(F)V
    .locals 4
    .param p1, "heartrate"    # F

    .prologue
    .line 109
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_heartrate:F

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_0

    .line 110
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_heartrate:F

    .line 112
    :cond_0
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->heartrate_count:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->heartrate_count:I

    .line 113
    iget-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->total_heartrate:D

    float-to-double v2, p1

    add-double/2addr v0, v2

    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->total_heartrate:D

    .line 114
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->max_heartrate:F

    cmpg-float v0, v0, p1

    if-gez v0, :cond_1

    .line 115
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->max_heartrate:F

    .line 116
    :cond_1
    return-void
.end method

.method public setSpeed(F)V
    .locals 5
    .param p1, "speed"    # F

    .prologue
    .line 78
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_speed:F

    cmpl-float v1, v1, p1

    if-eqz v1, :cond_0

    .line 79
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_speed:F

    .line 81
    :cond_0
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->speed_count:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->speed_count:I

    .line 82
    iget-wide v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->total_speed:D

    float-to-double v3, p1

    add-double/2addr v1, v3

    iput-wide v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->total_speed:D

    .line 83
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->max_speed:F

    cmpg-float v1, v1, p1

    if-gez v1, :cond_1

    .line 84
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->max_speed:F

    .line 88
    :cond_1
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->ave_speed_cnt:I

    if-nez v1, :cond_3

    .line 89
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->total_speed_for_calorie:F

    .line 93
    :goto_0
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->ave_speed_cnt:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->ave_speed_cnt:I

    .line 95
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->ave_speed_cnt:I

    .line 96
    .local v0, "cnt":I
    if-lez v0, :cond_2

    .line 97
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->total_speed_for_calorie:F

    int-to-float v2, v0

    div-float/2addr v1, v2

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->ave_speed_for_calorie:F

    .line 100
    :cond_2
    return-void

    .line 91
    .end local v0    # "cnt":I
    :cond_3
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->total_speed_for_calorie:F

    add-float/2addr v1, p1

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->total_speed_for_calorie:F

    goto :goto_0
.end method
