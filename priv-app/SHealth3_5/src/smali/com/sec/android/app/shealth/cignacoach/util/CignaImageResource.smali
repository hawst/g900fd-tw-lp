.class public Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;
.super Ljava/lang/Object;
.source "CignaImageResource.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource$1;
    }
.end annotation


# static fields
.field public static final mBadgeIconResource:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final mBadgeImageResource:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final mGoalImageResource:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final mImageResource:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 20
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mImageResource:Ljava/util/HashMap;

    .line 21
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mImageResource:Ljava/util/HashMap;

    const-string v1, "CURRENT_GOAL_ICON_EXERCISE"

    const v2, 0x7f0200cf

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 22
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mImageResource:Ljava/util/HashMap;

    const-string v1, "CURRENT_GOAL_ICON_FOOD"

    const v2, 0x7f0200d0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 23
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mImageResource:Ljava/util/HashMap;

    const-string v1, "CURRENT_GOAL_ICON_SLEEP"

    const v2, 0x7f0200d1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 24
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mImageResource:Ljava/util/HashMap;

    const-string v1, "CURRENT_GOAL_ICON_STRESS"

    const v2, 0x7f0200d2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 25
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mImageResource:Ljava/util/HashMap;

    const-string v1, "CURRENT_GOAL_ICON_WEIGHT"

    const v2, 0x7f0200d3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 27
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mImageResource:Ljava/util/HashMap;

    const-string v1, "LIST_ICON_EXERCISE"

    const v2, 0x7f0200fb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 28
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mImageResource:Ljava/util/HashMap;

    const-string v1, "LIST_ICON_FOOD"

    const v2, 0x7f0200fc

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mImageResource:Ljava/util/HashMap;

    const-string v1, "LIST_ICON_SLEEP"

    const v2, 0x7f0200fd

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 30
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mImageResource:Ljava/util/HashMap;

    const-string v1, "LIST_ICON_STRESS"

    const v2, 0x7f0200fe

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mImageResource:Ljava/util/HashMap;

    const-string v1, "LIST_ICON_WEIGHT"

    const v2, 0x7f0200ff

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mImageResource:Ljava/util/HashMap;

    const-string v1, "TITLE_CATEGORY_ICON_EXERCISE"

    const v2, 0x7f02008d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mImageResource:Ljava/util/HashMap;

    const-string v1, "TITLE_CATEGORY_ICON_FOOD"

    const v2, 0x7f020091

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mImageResource:Ljava/util/HashMap;

    const-string v1, "TITLE_CATEGORY_ICON_SLEEP"

    const v2, 0x7f020095

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mImageResource:Ljava/util/HashMap;

    const-string v1, "TITLE_CATEGORY_ICON_STRESS"

    const v2, 0x7f020099

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mImageResource:Ljava/util/HashMap;

    const-string v1, "TITLE_CATEGORY_ICON_WEIGHT"

    const v2, 0x7f02009d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeImageResource:Ljava/util/HashMap;

    .line 42
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeImageResource:Ljava/util/HashMap;

    const-string v1, "cigna_badge_icon_actionhero"

    const v2, 0x7f02005b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeImageResource:Ljava/util/HashMap;

    const-string v1, "cigna_badge_icon_athlete"

    const v2, 0x7f02005c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeImageResource:Ljava/util/HashMap;

    const-string v1, "cigna_badge_icon_champ"

    const v2, 0x7f02005d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeImageResource:Ljava/util/HashMap;

    const-string v1, "cigna_badge_icon_coach"

    const v2, 0x7f02005e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeImageResource:Ljava/util/HashMap;

    const-string v1, "cigna_badge_icon_dreamcatcher"

    const v2, 0x7f02005f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeImageResource:Ljava/util/HashMap;

    const-string v1, "cigna_badge_icon_fitnessguru"

    const v2, 0x7f020061

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeImageResource:Ljava/util/HashMap;

    const-string v1, "cigna_badge_icon_foodie"

    const v2, 0x7f020062

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeImageResource:Ljava/util/HashMap;

    const-string v1, "cigna_badge_icon_foursquare"

    const v2, 0x7f020063

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeImageResource:Ljava/util/HashMap;

    const-string v1, "cigna_badge_icon_gearedup"

    const v2, 0x7f020064

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeImageResource:Ljava/util/HashMap;

    const-string v1, "cigna_badge_icon_gogetter"

    const v2, 0x7f020065

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeImageResource:Ljava/util/HashMap;

    const-string v1, "cigna_badge_icon_hattrick"

    const v2, 0x7f020066

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeImageResource:Ljava/util/HashMap;

    const-string v1, "cigna_badge_icon_healthnut"

    const v2, 0x7f020067

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeImageResource:Ljava/util/HashMap;

    const-string v1, "cigna_badge_icon_heavyweight"

    const v2, 0x7f020068

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeImageResource:Ljava/util/HashMap;

    const-string v1, "cigna_badge_icon_highfive"

    const v2, 0x7f020069

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeImageResource:Ljava/util/HashMap;

    const-string v1, "cigna_badge_icon_knowthyself"

    const v2, 0x7f02006a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeImageResource:Ljava/util/HashMap;

    const-string v1, "cigna_badge_icon_layer_2296"

    const v2, 0x7f020060

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeImageResource:Ljava/util/HashMap;

    const-string v1, "cigna_badge_icon_lightweight"

    const v2, 0x7f02006b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeImageResource:Ljava/util/HashMap;

    const-string v1, "cigna_badge_icon_luckyseven"

    const v2, 0x7f02006c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeImageResource:Ljava/util/HashMap;

    const-string v1, "cigna_badge_icon_mirromirro"

    const v2, 0x7f02006d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeImageResource:Ljava/util/HashMap;

    const-string v1, "cigna_badge_icon_movingtheneedle"

    const v2, 0x7f02006e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeImageResource:Ljava/util/HashMap;

    const-string v1, "cigna_badge_icon_outofthegate"

    const v2, 0x7f02006f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeImageResource:Ljava/util/HashMap;

    const-string v1, "cigna_badge_icon_platebalancer"

    const v2, 0x7f020070

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeImageResource:Ljava/util/HashMap;

    const-string v1, "cigna_badge_icon_relaxmaster"

    const v2, 0x7f020071

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeImageResource:Ljava/util/HashMap;

    const-string v1, "cigna_badge_icon_rockstar"

    const v2, 0x7f020072

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeImageResource:Ljava/util/HashMap;

    const-string v1, "cigna_badge_icon_sixpack"

    const v2, 0x7f020073

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeImageResource:Ljava/util/HashMap;

    const-string v1, "cigna_badge_icon_sleepingbeauty"

    const v2, 0x7f020074

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeImageResource:Ljava/util/HashMap;

    const-string v1, "cigna_badge_icon_slimntrim"

    const v2, 0x7f020075

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeImageResource:Ljava/util/HashMap;

    const-string v1, "cigna_badge_icon_stickwithit"

    const v2, 0x7f020076

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeImageResource:Ljava/util/HashMap;

    const-string v1, "cigna_badge_icon_wellrounded"

    const v2, 0x7f020078

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeImageResource:Ljava/util/HashMap;

    const-string v1, "cigna_badge_icon_zenmaster"

    const v2, 0x7f020079

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeImageResource:Ljava/util/HashMap;

    const-string v1, "cigna_badge_icon_polygon"

    const v2, 0x7f020077

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeIconResource:Ljava/util/HashMap;

    .line 78
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeIconResource:Ljava/util/HashMap;

    const-string v1, "cigna_badge_icon_actionhero"

    const v2, 0x7f0200dd

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeIconResource:Ljava/util/HashMap;

    const-string v1, "cigna_badge_icon_athlete"

    const v2, 0x7f0200de

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeIconResource:Ljava/util/HashMap;

    const-string v1, "cigna_badge_icon_champ"

    const v2, 0x7f0200df

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeIconResource:Ljava/util/HashMap;

    const-string v1, "cigna_badge_icon_dreamcatcher"

    const v2, 0x7f0200e0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeIconResource:Ljava/util/HashMap;

    const-string v1, "cigna_badge_icon_fitnessguru"

    const v2, 0x7f0200e2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeIconResource:Ljava/util/HashMap;

    const-string v1, "cigna_badge_icon_foodie"

    const v2, 0x7f0200e3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeIconResource:Ljava/util/HashMap;

    const-string v1, "cigna_badge_icon_foursquare"

    const v2, 0x7f0200e4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeIconResource:Ljava/util/HashMap;

    const-string v1, "cigna_badge_icon_gearedup"

    const v2, 0x7f0200e5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeIconResource:Ljava/util/HashMap;

    const-string v1, "cigna_badge_icon_gogetter"

    const v2, 0x7f0200e6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeIconResource:Ljava/util/HashMap;

    const-string v1, "cigna_badge_icon_hattrick"

    const v2, 0x7f0200e7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeIconResource:Ljava/util/HashMap;

    const-string v1, "cigna_badge_icon_healthnut"

    const v2, 0x7f0200e8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeIconResource:Ljava/util/HashMap;

    const-string v1, "cigna_badge_icon_heavyweight"

    const v2, 0x7f0200e9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeIconResource:Ljava/util/HashMap;

    const-string v1, "cigna_badge_icon_highfive"

    const v2, 0x7f0200ea

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeIconResource:Ljava/util/HashMap;

    const-string v1, "cigna_badge_icon_knowthyself"

    const v2, 0x7f0200eb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeIconResource:Ljava/util/HashMap;

    const-string v1, "cigna_badge_icon_layer_2296"

    const v2, 0x7f0200e1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeIconResource:Ljava/util/HashMap;

    const-string v1, "cigna_badge_icon_lightweight"

    const v2, 0x7f0200ec

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeIconResource:Ljava/util/HashMap;

    const-string v1, "cigna_badge_icon_luckyseven"

    const v2, 0x7f0200ed

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeIconResource:Ljava/util/HashMap;

    const-string v1, "cigna_badge_icon_mirromirro"

    const v2, 0x7f0200ee

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeIconResource:Ljava/util/HashMap;

    const-string v1, "cigna_badge_icon_movingtheneedle"

    const v2, 0x7f0200ef

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeIconResource:Ljava/util/HashMap;

    const-string v1, "cigna_badge_icon_outofthegate"

    const v2, 0x7f0200f0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeIconResource:Ljava/util/HashMap;

    const-string v1, "cigna_badge_icon_platebalancer"

    const v2, 0x7f0200f1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeIconResource:Ljava/util/HashMap;

    const-string v1, "cigna_badge_icon_relaxmaster"

    const v2, 0x7f0200f2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeIconResource:Ljava/util/HashMap;

    const-string v1, "cigna_badge_icon_rockstar"

    const v2, 0x7f0200f3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeIconResource:Ljava/util/HashMap;

    const-string v1, "cigna_badge_icon_sixpack"

    const v2, 0x7f0200f4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 104
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeIconResource:Ljava/util/HashMap;

    const-string v1, "cigna_badge_icon_sleepingbeauty"

    const v2, 0x7f0200f5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeIconResource:Ljava/util/HashMap;

    const-string v1, "cigna_badge_icon_slimntrim"

    const v2, 0x7f0200f6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeIconResource:Ljava/util/HashMap;

    const-string v1, "cigna_badge_icon_stickwithit"

    const v2, 0x7f0200f7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeIconResource:Ljava/util/HashMap;

    const-string v1, "cigna_badge_icon_wellrounded"

    const v2, 0x7f0200f9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeIconResource:Ljava/util/HashMap;

    const-string v1, "cigna_badge_icon_zenmaster"

    const v2, 0x7f0200fa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeIconResource:Ljava/util/HashMap;

    const-string v1, "cigna_badge_icon_polygon"

    const v2, 0x7f0200f8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 112
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mGoalImageResource:Ljava/util/HashMap;

    .line 113
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mGoalImageResource:Ljava/util/HashMap;

    const-string v1, "food1"

    const v2, 0x7f0200ac

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mGoalImageResource:Ljava/util/HashMap;

    const-string v1, "food2"

    const v2, 0x7f0200ad

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 115
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mGoalImageResource:Ljava/util/HashMap;

    const-string v1, "food3a"

    const v2, 0x7f0200ae

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mGoalImageResource:Ljava/util/HashMap;

    const-string v1, "food3b"

    const v2, 0x7f0200af

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mGoalImageResource:Ljava/util/HashMap;

    const-string v1, "food4"

    const v2, 0x7f0200b0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mGoalImageResource:Ljava/util/HashMap;

    const-string v1, "food5"

    const v2, 0x7f0200b1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 119
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mGoalImageResource:Ljava/util/HashMap;

    const-string v1, "food6"

    const v2, 0x7f0200b2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 120
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mGoalImageResource:Ljava/util/HashMap;

    const-string v1, "food7"

    const v2, 0x7f0200b3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 121
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mGoalImageResource:Ljava/util/HashMap;

    const-string v1, "exercise1"

    const v2, 0x7f0200b4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 122
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mGoalImageResource:Ljava/util/HashMap;

    const-string v1, "exercise2"

    const v2, 0x7f0200b5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 123
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mGoalImageResource:Ljava/util/HashMap;

    const-string v1, "exercise3"

    const v2, 0x7f0200b6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mGoalImageResource:Ljava/util/HashMap;

    const-string v1, "exercise4"

    const v2, 0x7f0200b7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mGoalImageResource:Ljava/util/HashMap;

    const-string/jumbo v1, "weight"

    const v2, 0x7f0200b8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mGoalImageResource:Ljava/util/HashMap;

    const-string/jumbo v1, "sleep"

    const v2, 0x7f0200b9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 127
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mGoalImageResource:Ljava/util/HashMap;

    const-string/jumbo v1, "stress1"

    const v2, 0x7f0200ba

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mGoalImageResource:Ljava/util/HashMap;

    const-string/jumbo v1, "stress2"

    const v2, 0x7f0200bb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 129
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 236
    return-void
.end method

.method public static getCignaBadgeIconByName(Ljava/lang/String;)Ljava/lang/Integer;
    .locals 4
    .param p0, "key"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 443
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 444
    .local v0, "resId":Ljava/lang/Integer;
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 445
    :cond_0
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 549
    :cond_1
    :goto_0
    if-nez v0, :cond_2

    .line 550
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 553
    :cond_2
    return-object v0

    .line 447
    :cond_3
    const-string v1, "cigna_badge_icon_actionhero"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 448
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeIconResource:Ljava/util/HashMap;

    const-string v2, "cigna_badge_icon_actionhero"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto :goto_0

    .line 450
    :cond_4
    const-string v1, "cigna_badge_icon_athlete"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 451
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeIconResource:Ljava/util/HashMap;

    const-string v2, "cigna_badge_icon_athlete"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto :goto_0

    .line 453
    :cond_5
    const-string v1, "cigna_badge_icon_champ"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 454
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeIconResource:Ljava/util/HashMap;

    const-string v2, "cigna_badge_icon_champ"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto :goto_0

    .line 456
    :cond_6
    const-string v1, "cigna_badge_icon_coach"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 457
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeIconResource:Ljava/util/HashMap;

    const-string v2, "cigna_badge_icon_coach"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto :goto_0

    .line 459
    :cond_7
    const-string v1, "cigna_badge_icon_dreamcatcher"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 460
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeIconResource:Ljava/util/HashMap;

    const-string v2, "cigna_badge_icon_dreamcatcher"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto :goto_0

    .line 462
    :cond_8
    const-string v1, "cigna_badge_icon_fitnessguru"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 463
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeIconResource:Ljava/util/HashMap;

    const-string v2, "cigna_badge_icon_fitnessguru"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto :goto_0

    .line 465
    :cond_9
    const-string v1, "cigna_badge_icon_foodie"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 466
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeIconResource:Ljava/util/HashMap;

    const-string v2, "cigna_badge_icon_foodie"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 468
    :cond_a
    const-string v1, "cigna_badge_icon_foursquare"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 469
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeIconResource:Ljava/util/HashMap;

    const-string v2, "cigna_badge_icon_foursquare"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 471
    :cond_b
    const-string v1, "cigna_badge_icon_gearedup"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 472
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeIconResource:Ljava/util/HashMap;

    const-string v2, "cigna_badge_icon_gearedup"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 474
    :cond_c
    const-string v1, "cigna_badge_icon_gogetter"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 475
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeIconResource:Ljava/util/HashMap;

    const-string v2, "cigna_badge_icon_gogetter"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 478
    :cond_d
    const-string v1, "cigna_badge_icon_hattrick"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 479
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeIconResource:Ljava/util/HashMap;

    const-string v2, "cigna_badge_icon_hattrick"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 481
    :cond_e
    const-string v1, "cigna_badge_icon_healthnut"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 482
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeIconResource:Ljava/util/HashMap;

    const-string v2, "cigna_badge_icon_healthnut"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 484
    :cond_f
    const-string v1, "cigna_badge_icon_heavyweight"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 485
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeImageResource:Ljava/util/HashMap;

    const-string v2, "cigna_badge_icon_heavyweight"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 487
    :cond_10
    const-string v1, "cigna_badge_icon_highfive"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 488
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeIconResource:Ljava/util/HashMap;

    const-string v2, "cigna_badge_icon_highfive"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 490
    :cond_11
    const-string v1, "cigna_badge_icon_knowthyself"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 491
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeIconResource:Ljava/util/HashMap;

    const-string v2, "cigna_badge_icon_knowthyself"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 493
    :cond_12
    const-string v1, "cigna_badge_icon_knowthyself"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 494
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeIconResource:Ljava/util/HashMap;

    const-string v2, "cigna_badge_icon_knowthyself"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 496
    :cond_13
    const-string v1, "cigna_badge_icon_layer_2296"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_14

    .line 497
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeIconResource:Ljava/util/HashMap;

    const-string v2, "cigna_badge_icon_layer_2296"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 499
    :cond_14
    const-string v1, "cigna_badge_icon_lightweight"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_15

    .line 500
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeIconResource:Ljava/util/HashMap;

    const-string v2, "cigna_badge_icon_lightweight"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 502
    :cond_15
    const-string v1, "cigna_badge_icon_luckyseven"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_16

    .line 503
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeIconResource:Ljava/util/HashMap;

    const-string v2, "cigna_badge_icon_luckyseven"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 505
    :cond_16
    const-string v1, "cigna_badge_icon_mirromirro"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_17

    .line 506
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeIconResource:Ljava/util/HashMap;

    const-string v2, "cigna_badge_icon_mirromirro"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 508
    :cond_17
    const-string v1, "cigna_badge_icon_mirror"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_18

    .line 509
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeIconResource:Ljava/util/HashMap;

    const-string v2, "cigna_badge_icon_mirror"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 511
    :cond_18
    const-string v1, "cigna_badge_icon_movingtheneedle"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_19

    .line 512
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeIconResource:Ljava/util/HashMap;

    const-string v2, "cigna_badge_icon_movingtheneedle"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 514
    :cond_19
    const-string v1, "cigna_badge_icon_outofthegate"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1a

    .line 515
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeIconResource:Ljava/util/HashMap;

    const-string v2, "cigna_badge_icon_outofthegate"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 517
    :cond_1a
    const-string v1, "cigna_badge_icon_platebalancer"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1b

    .line 518
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeIconResource:Ljava/util/HashMap;

    const-string v2, "cigna_badge_icon_platebalancer"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 520
    :cond_1b
    const-string v1, "cigna_badge_icon_relaxmaster"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1c

    .line 521
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeIconResource:Ljava/util/HashMap;

    const-string v2, "cigna_badge_icon_relaxmaster"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 523
    :cond_1c
    const-string v1, "cigna_badge_icon_rockstar"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1d

    .line 524
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeIconResource:Ljava/util/HashMap;

    const-string v2, "cigna_badge_icon_rockstar"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 527
    :cond_1d
    const-string v1, "cigna_badge_icon_sixpack"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1e

    .line 528
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeIconResource:Ljava/util/HashMap;

    const-string v2, "cigna_badge_icon_sixpack"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 530
    :cond_1e
    const-string v1, "cigna_badge_icon_sleepingbeauty"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1f

    .line 531
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeIconResource:Ljava/util/HashMap;

    const-string v2, "cigna_badge_icon_sleepingbeauty"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 533
    :cond_1f
    const-string v1, "cigna_badge_icon_slimntrim"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_20

    .line 534
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeIconResource:Ljava/util/HashMap;

    const-string v2, "cigna_badge_icon_slimntrim"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 536
    :cond_20
    const-string v1, "cigna_badge_icon_stickwithit"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_21

    .line 537
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeIconResource:Ljava/util/HashMap;

    const-string v2, "cigna_badge_icon_stickwithit"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 539
    :cond_21
    const-string v1, "cigna_badge_icon_wellrounded"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_22

    .line 540
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeIconResource:Ljava/util/HashMap;

    const-string v2, "cigna_badge_icon_wellrounded"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 542
    :cond_22
    const-string v1, "cigna_badge_icon_zenmaster"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_23

    .line 543
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeIconResource:Ljava/util/HashMap;

    const-string v2, "cigna_badge_icon_zenmaster"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 545
    :cond_23
    const-string v1, "cigna_badge_icon_polygon"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 546
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeIconResource:Ljava/util/HashMap;

    const-string v2, "cigna_badge_icon_polygon"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto/16 :goto_0
.end method

.method public static getCignaGoalIconIdByCategory(I)Ljava/lang/Integer;
    .locals 3
    .param p0, "goalId"    # I

    .prologue
    .line 243
    const/4 v1, 0x1

    if-lt p0, v1, :cond_0

    const/16 v1, 0x1c

    if-gt p0, v1, :cond_0

    .line 244
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mImageResource:Ljava/util/HashMap;

    const-string v2, "LIST_ICON_FOOD"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 261
    .local v0, "resourceId":I
    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    return-object v1

    .line 246
    .end local v0    # "resourceId":I
    :cond_0
    const/16 v1, 0x1d

    if-lt p0, v1, :cond_1

    const/16 v1, 0x2c

    if-gt p0, v1, :cond_1

    .line 247
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mImageResource:Ljava/util/HashMap;

    const-string v2, "LIST_ICON_EXERCISE"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .restart local v0    # "resourceId":I
    goto :goto_0

    .line 249
    .end local v0    # "resourceId":I
    :cond_1
    const/16 v1, 0x2d

    if-lt p0, v1, :cond_2

    const/16 v1, 0x30

    if-gt p0, v1, :cond_2

    .line 250
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mImageResource:Ljava/util/HashMap;

    const-string v2, "LIST_ICON_WEIGHT"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .restart local v0    # "resourceId":I
    goto :goto_0

    .line 252
    .end local v0    # "resourceId":I
    :cond_2
    const/16 v1, 0x31

    if-lt p0, v1, :cond_3

    const/16 v1, 0x34

    if-gt p0, v1, :cond_3

    .line 253
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mImageResource:Ljava/util/HashMap;

    const-string v2, "LIST_ICON_SLEEP"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .restart local v0    # "resourceId":I
    goto :goto_0

    .line 255
    .end local v0    # "resourceId":I
    :cond_3
    const/16 v1, 0x35

    if-lt p0, v1, :cond_4

    const/16 v1, 0x3c

    if-gt p0, v1, :cond_4

    .line 256
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mImageResource:Ljava/util/HashMap;

    const-string v2, "LIST_ICON_STRESS"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .restart local v0    # "resourceId":I
    goto :goto_0

    .line 259
    .end local v0    # "resourceId":I
    :cond_4
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mImageResource:Ljava/util/HashMap;

    const-string v2, "LIST_ICON_EXERCISE"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .restart local v0    # "resourceId":I
    goto :goto_0
.end method

.method public static getCignaGoalIconIdByCategory(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)Ljava/lang/Integer;
    .locals 3
    .param p0, "category"    # Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    .prologue
    .line 217
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource$1;->$SwitchMap$com$cigna$coach$interfaces$ILifeStyle$CategoryType:[I

    invoke-virtual {p0}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 235
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mImageResource:Ljava/util/HashMap;

    const-string v2, "LIST_ICON_EXERCISE"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 238
    .local v0, "resourceId":I
    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    return-object v1

    .line 220
    .end local v0    # "resourceId":I
    :pswitch_0
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mImageResource:Ljava/util/HashMap;

    const-string v2, "LIST_ICON_FOOD"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 221
    .restart local v0    # "resourceId":I
    goto :goto_0

    .line 223
    .end local v0    # "resourceId":I
    :pswitch_1
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mImageResource:Ljava/util/HashMap;

    const-string v2, "LIST_ICON_EXERCISE"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 224
    .restart local v0    # "resourceId":I
    goto :goto_0

    .line 226
    .end local v0    # "resourceId":I
    :pswitch_2
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mImageResource:Ljava/util/HashMap;

    const-string v2, "LIST_ICON_SLEEP"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 227
    .restart local v0    # "resourceId":I
    goto :goto_0

    .line 229
    .end local v0    # "resourceId":I
    :pswitch_3
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mImageResource:Ljava/util/HashMap;

    const-string v2, "LIST_ICON_STRESS"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 230
    .restart local v0    # "resourceId":I
    goto :goto_0

    .line 232
    .end local v0    # "resourceId":I
    :pswitch_4
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mImageResource:Ljava/util/HashMap;

    const-string v2, "LIST_ICON_WEIGHT"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 233
    .restart local v0    # "resourceId":I
    goto :goto_0

    .line 217
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static getCignaMissionBadgeByName(Ljava/lang/String;)Ljava/lang/Integer;
    .locals 4
    .param p0, "key"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 329
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 330
    .local v0, "resId":Ljava/lang/Integer;
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 331
    :cond_0
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 434
    :cond_1
    :goto_0
    if-nez v0, :cond_2

    .line 435
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 438
    :cond_2
    return-object v0

    .line 333
    :cond_3
    const-string v1, "cigna_badge_icon_actionhero"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 334
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeImageResource:Ljava/util/HashMap;

    const-string v2, "cigna_badge_icon_actionhero"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto :goto_0

    .line 336
    :cond_4
    const-string v1, "cigna_badge_icon_athlete"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 337
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeImageResource:Ljava/util/HashMap;

    const-string v2, "cigna_badge_icon_athlete"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto :goto_0

    .line 339
    :cond_5
    const-string v1, "cigna_badge_icon_champ"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 340
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeImageResource:Ljava/util/HashMap;

    const-string v2, "cigna_badge_icon_champ"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto :goto_0

    .line 342
    :cond_6
    const-string v1, "cigna_badge_icon_coach"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 343
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeImageResource:Ljava/util/HashMap;

    const-string v2, "cigna_badge_icon_coach"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto :goto_0

    .line 345
    :cond_7
    const-string v1, "cigna_badge_icon_dreamcatcher"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 346
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeImageResource:Ljava/util/HashMap;

    const-string v2, "cigna_badge_icon_dreamcatcher"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto :goto_0

    .line 348
    :cond_8
    const-string v1, "cigna_badge_icon_fitnessguru"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 349
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeImageResource:Ljava/util/HashMap;

    const-string v2, "cigna_badge_icon_fitnessguru"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto :goto_0

    .line 351
    :cond_9
    const-string v1, "cigna_badge_icon_foodie"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 352
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeImageResource:Ljava/util/HashMap;

    const-string v2, "cigna_badge_icon_foodie"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 354
    :cond_a
    const-string v1, "cigna_badge_icon_foursquare"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 355
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeImageResource:Ljava/util/HashMap;

    const-string v2, "cigna_badge_icon_foursquare"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 357
    :cond_b
    const-string v1, "cigna_badge_icon_gearedup"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 358
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeImageResource:Ljava/util/HashMap;

    const-string v2, "cigna_badge_icon_gearedup"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 360
    :cond_c
    const-string v1, "cigna_badge_icon_gogetter"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 361
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeImageResource:Ljava/util/HashMap;

    const-string v2, "cigna_badge_icon_gogetter"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 363
    :cond_d
    const-string v1, "cigna_badge_icon_hattrick"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 364
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeImageResource:Ljava/util/HashMap;

    const-string v2, "cigna_badge_icon_hattrick"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 366
    :cond_e
    const-string v1, "cigna_badge_icon_healthnut"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 367
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeImageResource:Ljava/util/HashMap;

    const-string v2, "cigna_badge_icon_healthnut"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 369
    :cond_f
    const-string v1, "cigna_badge_icon_heavyweight"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 370
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeImageResource:Ljava/util/HashMap;

    const-string v2, "cigna_badge_icon_heavyweight"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 372
    :cond_10
    const-string v1, "cigna_badge_icon_highfive"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 373
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeImageResource:Ljava/util/HashMap;

    const-string v2, "cigna_badge_icon_highfive"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 375
    :cond_11
    const-string v1, "cigna_badge_icon_knowthyself"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 376
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeImageResource:Ljava/util/HashMap;

    const-string v2, "cigna_badge_icon_knowthyself"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 378
    :cond_12
    const-string v1, "cigna_badge_icon_knowthyself"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 379
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeImageResource:Ljava/util/HashMap;

    const-string v2, "cigna_badge_icon_knowthyself"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 381
    :cond_13
    const-string v1, "cigna_badge_icon_layer_2296"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_14

    .line 382
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeImageResource:Ljava/util/HashMap;

    const-string v2, "cigna_badge_icon_layer_2296"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 384
    :cond_14
    const-string v1, "cigna_badge_icon_lightweight"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_15

    .line 385
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeImageResource:Ljava/util/HashMap;

    const-string v2, "cigna_badge_icon_lightweight"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 387
    :cond_15
    const-string v1, "cigna_badge_icon_luckyseven"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_16

    .line 388
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeImageResource:Ljava/util/HashMap;

    const-string v2, "cigna_badge_icon_luckyseven"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 390
    :cond_16
    const-string v1, "cigna_badge_icon_mirromirro"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_17

    .line 391
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeImageResource:Ljava/util/HashMap;

    const-string v2, "cigna_badge_icon_mirromirro"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 393
    :cond_17
    const-string v1, "cigna_badge_icon_mirror"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_18

    .line 394
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeImageResource:Ljava/util/HashMap;

    const-string v2, "cigna_badge_icon_mirror"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 396
    :cond_18
    const-string v1, "cigna_badge_icon_movingtheneedle"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_19

    .line 397
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeImageResource:Ljava/util/HashMap;

    const-string v2, "cigna_badge_icon_movingtheneedle"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 399
    :cond_19
    const-string v1, "cigna_badge_icon_outofthegate"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1a

    .line 400
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeImageResource:Ljava/util/HashMap;

    const-string v2, "cigna_badge_icon_outofthegate"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 402
    :cond_1a
    const-string v1, "cigna_badge_icon_platebalancer"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1b

    .line 403
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeImageResource:Ljava/util/HashMap;

    const-string v2, "cigna_badge_icon_platebalancer"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 405
    :cond_1b
    const-string v1, "cigna_badge_icon_relaxmaster"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1c

    .line 406
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeImageResource:Ljava/util/HashMap;

    const-string v2, "cigna_badge_icon_relaxmaster"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 408
    :cond_1c
    const-string v1, "cigna_badge_icon_rockstar"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1d

    .line 409
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeImageResource:Ljava/util/HashMap;

    const-string v2, "cigna_badge_icon_rockstar"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 412
    :cond_1d
    const-string v1, "cigna_badge_icon_sixpack"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1e

    .line 413
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeImageResource:Ljava/util/HashMap;

    const-string v2, "cigna_badge_icon_sixpack"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 415
    :cond_1e
    const-string v1, "cigna_badge_icon_sleepingbeauty"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1f

    .line 416
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeImageResource:Ljava/util/HashMap;

    const-string v2, "cigna_badge_icon_sleepingbeauty"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 418
    :cond_1f
    const-string v1, "cigna_badge_icon_slimntrim"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_20

    .line 419
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeImageResource:Ljava/util/HashMap;

    const-string v2, "cigna_badge_icon_slimntrim"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 421
    :cond_20
    const-string v1, "cigna_badge_icon_stickwithit"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_21

    .line 422
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeImageResource:Ljava/util/HashMap;

    const-string v2, "cigna_badge_icon_stickwithit"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 424
    :cond_21
    const-string v1, "cigna_badge_icon_wellrounded"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_22

    .line 425
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeImageResource:Ljava/util/HashMap;

    const-string v2, "cigna_badge_icon_wellrounded"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 427
    :cond_22
    const-string v1, "cigna_badge_icon_zenmaster"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_23

    .line 428
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeImageResource:Ljava/util/HashMap;

    const-string v2, "cigna_badge_icon_zenmaster"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 430
    :cond_23
    const-string v1, "cigna_badge_icon_polygon"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 431
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mBadgeImageResource:Ljava/util/HashMap;

    const-string v2, "cigna_badge_icon_polygon"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto/16 :goto_0
.end method

.method public static getCurrentGoalIconIdByCategory(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)Ljava/lang/Integer;
    .locals 3
    .param p0, "category"    # Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    .prologue
    .line 266
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource$1;->$SwitchMap$com$cigna$coach$interfaces$ILifeStyle$CategoryType:[I

    invoke-virtual {p0}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 283
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mImageResource:Ljava/util/HashMap;

    const-string v2, "CURRENT_GOAL_ICON_EXERCISE"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 286
    .local v0, "resourceId":I
    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    return-object v1

    .line 268
    .end local v0    # "resourceId":I
    :pswitch_0
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mImageResource:Ljava/util/HashMap;

    const-string v2, "CURRENT_GOAL_ICON_FOOD"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 269
    .restart local v0    # "resourceId":I
    goto :goto_0

    .line 271
    .end local v0    # "resourceId":I
    :pswitch_1
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mImageResource:Ljava/util/HashMap;

    const-string v2, "CURRENT_GOAL_ICON_EXERCISE"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 272
    .restart local v0    # "resourceId":I
    goto :goto_0

    .line 274
    .end local v0    # "resourceId":I
    :pswitch_2
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mImageResource:Ljava/util/HashMap;

    const-string v2, "CURRENT_GOAL_ICON_SLEEP"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 275
    .restart local v0    # "resourceId":I
    goto :goto_0

    .line 277
    .end local v0    # "resourceId":I
    :pswitch_3
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mImageResource:Ljava/util/HashMap;

    const-string v2, "CURRENT_GOAL_ICON_STRESS"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 278
    .restart local v0    # "resourceId":I
    goto :goto_0

    .line 280
    .end local v0    # "resourceId":I
    :pswitch_4
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mImageResource:Ljava/util/HashMap;

    const-string v2, "CURRENT_GOAL_ICON_WEIGHT"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 281
    .restart local v0    # "resourceId":I
    goto :goto_0

    .line 266
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static getGoalImageResourceID(Ljava/lang/String;)Ljava/lang/Integer;
    .locals 4
    .param p0, "key"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 133
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 135
    .local v0, "resId":Ljava/lang/Integer;
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 137
    :cond_0
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 204
    :cond_1
    :goto_0
    if-nez v0, :cond_2

    .line 205
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 208
    :cond_2
    return-object v0

    .line 139
    :cond_3
    const-string v1, "food1"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 141
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mGoalImageResource:Ljava/util/HashMap;

    const-string v2, "food1"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto :goto_0

    .line 143
    :cond_4
    const-string v1, "food2"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 145
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mGoalImageResource:Ljava/util/HashMap;

    const-string v2, "food2"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto :goto_0

    .line 147
    :cond_5
    const-string v1, "food3a"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 149
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mGoalImageResource:Ljava/util/HashMap;

    const-string v2, "food3a"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto :goto_0

    .line 151
    :cond_6
    const-string v1, "food3b"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 153
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mGoalImageResource:Ljava/util/HashMap;

    const-string v2, "food3b"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto :goto_0

    .line 155
    :cond_7
    const-string v1, "food4"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 157
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mGoalImageResource:Ljava/util/HashMap;

    const-string v2, "food4"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto :goto_0

    .line 159
    :cond_8
    const-string v1, "food5"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 161
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mGoalImageResource:Ljava/util/HashMap;

    const-string v2, "food5"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto :goto_0

    .line 163
    :cond_9
    const-string v1, "food6"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 165
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mGoalImageResource:Ljava/util/HashMap;

    const-string v2, "food6"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 167
    :cond_a
    const-string v1, "food7"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 169
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mGoalImageResource:Ljava/util/HashMap;

    const-string v2, "food7"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 171
    :cond_b
    const-string v1, "exercise1"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 173
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mGoalImageResource:Ljava/util/HashMap;

    const-string v2, "exercise1"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 175
    :cond_c
    const-string v1, "exercise2"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 177
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mGoalImageResource:Ljava/util/HashMap;

    const-string v2, "exercise2"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 179
    :cond_d
    const-string v1, "exercise3"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 181
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mGoalImageResource:Ljava/util/HashMap;

    const-string v2, "exercise3"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 183
    :cond_e
    const-string v1, "exercise4"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 185
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mGoalImageResource:Ljava/util/HashMap;

    const-string v2, "exercise4"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 187
    :cond_f
    const-string/jumbo v1, "weight"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 189
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mGoalImageResource:Ljava/util/HashMap;

    const-string/jumbo v2, "weight"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 191
    :cond_10
    const-string/jumbo v1, "sleep"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 193
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mGoalImageResource:Ljava/util/HashMap;

    const-string/jumbo v2, "sleep"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 195
    :cond_11
    const-string/jumbo v1, "stress1"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 197
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mGoalImageResource:Ljava/util/HashMap;

    const-string/jumbo v2, "stress1"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 199
    :cond_12
    const-string/jumbo v1, "stress2"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 201
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mGoalImageResource:Ljava/util/HashMap;

    const-string/jumbo v2, "stress2"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "resId":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "resId":Ljava/lang/Integer;
    goto/16 :goto_0
.end method

.method public static getScoreImageByPoint(I)Ljava/lang/Integer;
    .locals 3
    .param p0, "point"    # I

    .prologue
    .line 560
    if-ltz p0, :cond_0

    const/16 v1, 0x1e

    if-gt p0, v1, :cond_0

    .line 561
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mImageResource:Ljava/util/HashMap;

    const-string v2, "ONE_SCORE_IMAGE1"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 570
    .local v0, "resourceId":I
    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    return-object v1

    .line 562
    .end local v0    # "resourceId":I
    :cond_0
    const/16 v1, 0x1f

    if-lt p0, v1, :cond_1

    const/16 v1, 0x46

    if-gt p0, v1, :cond_1

    .line 563
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mImageResource:Ljava/util/HashMap;

    const-string v2, "ONE_SCORE_IMAGE2"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .restart local v0    # "resourceId":I
    goto :goto_0

    .line 564
    .end local v0    # "resourceId":I
    :cond_1
    const/16 v1, 0x47

    if-lt p0, v1, :cond_2

    const/16 v1, 0x64

    if-gt p0, v1, :cond_2

    .line 565
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mImageResource:Ljava/util/HashMap;

    const-string v2, "ONE_SCORE_IMAGE3"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .restart local v0    # "resourceId":I
    goto :goto_0

    .line 567
    .end local v0    # "resourceId":I
    :cond_2
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->mImageResource:Ljava/util/HashMap;

    const-string v2, "ONE_SCORE_IMAGE1"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .restart local v0    # "resourceId":I
    goto :goto_0
.end method
