.class public Lcom/sec/android/app/shealth/framework/ui/hovering/HoverGridAdapter;
.super Landroid/widget/BaseAdapter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/framework/ui/hovering/HoverGridAdapter$1;,
        Lcom/sec/android/app/shealth/framework/ui/hovering/HoverGridAdapter$ImageViewHolder;
    }
.end annotation


# instance fields
.field context:Landroid/content/Context;

.field listOfImages:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverGridAdapter;->context:Landroid/content/Context;

    iput-object p2, p0, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverGridAdapter;->listOfImages:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverGridAdapter;->listOfImages:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverGridAdapter;->listOfImages:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    const/4 v2, 0x0

    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverGridAdapter;->context:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$layout;->hover_grid_child:I

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverGridAdapter$ImageViewHolder;

    invoke-direct {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverGridAdapter$ImageViewHolder;-><init>(Lcom/sec/android/app/shealth/framework/ui/hovering/HoverGridAdapter$1;)V

    sget v0, Lcom/sec/android/app/shealth/framework/ui/R$id;->hoverGridChild:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    # setter for: Lcom/sec/android/app/shealth/framework/ui/hovering/HoverGridAdapter$ImageViewHolder;->mImageView:Landroid/widget/ImageView;
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverGridAdapter$ImageViewHolder;->access$102(Lcom/sec/android/app/shealth/framework/ui/hovering/HoverGridAdapter$ImageViewHolder;Landroid/widget/ImageView;)Landroid/widget/ImageView;

    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v0, v1

    :goto_0
    :try_start_0
    # getter for: Lcom/sec/android/app/shealth/framework/ui/hovering/HoverGridAdapter$ImageViewHolder;->mImageView:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverGridAdapter$ImageViewHolder;->access$100(Lcom/sec/android/app/shealth/framework/ui/hovering/HoverGridAdapter$ImageViewHolder;)Landroid/widget/ImageView;

    move-result-object v1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverGridAdapter;->listOfImages:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/BitmapUtil;->getBitmapByPath(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverGridAdapter;->listOfImages:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/android/app/shealth/common/utils/BitmapUtil;->getBitmapWithCorrectingOrientationToNormal(Landroid/graphics/Bitmap;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    return-object p2

    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverGridAdapter$ImageViewHolder;

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_1
.end method
