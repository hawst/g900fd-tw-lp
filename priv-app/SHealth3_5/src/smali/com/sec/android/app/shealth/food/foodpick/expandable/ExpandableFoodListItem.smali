.class Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableFoodListItem;
.super Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;
.source "ExpandableFoodListItem.java"

# interfaces
.implements Lcom/sec/android/app/shealth/food/foodpick/expandable/Expandable;


# static fields
.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private mChildItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/expandable/ChildFoodListItem;",
            ">;"
        }
    .end annotation
.end field

.field private mMealDao:Lcom/sec/android/app/shealth/food/fooddao/MealDao;

.field private mMealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableFoodListItem;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->getLogTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableFoodListItem;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;Lcom/sec/android/app/shealth/food/fooddao/MealDao;)V
    .locals 1
    .param p1, "mMealData"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    .param p2, "mMealDao"    # Lcom/sec/android/app/shealth/food/fooddao/MealDao;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableFoodListItem;->mMealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    .line 43
    iput-object p2, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableFoodListItem;->mMealDao:Lcom/sec/android/app/shealth/food/fooddao/MealDao;

    .line 44
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableFoodListItem;->mChildItems:Ljava/util/List;

    .line 45
    return-void
.end method

.method private saveToDatabase()V
    .locals 2

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableFoodListItem;->mMealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableFoodListItem;->mMealDao:Lcom/sec/android/app/shealth/food/fooddao/MealDao;

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/DataBaseUpdater;->insertOrUpdateData(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;Lcom/sec/android/app/shealth/common/commondao/CommonDao;)V

    .line 88
    return-void
.end method


# virtual methods
.method public addChild(Lcom/sec/android/app/shealth/food/foodpick/expandable/ChildFoodListItem;)V
    .locals 1
    .param p1, "foodListItem"    # Lcom/sec/android/app/shealth/food/foodpick/expandable/ChildFoodListItem;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableFoodListItem;->mChildItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 55
    return-void
.end method

.method public addToFavorites()V
    .locals 3

    .prologue
    .line 69
    sget-object v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableFoodListItem;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "added to favorite: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableFoodListItem;->mMealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableFoodListItem;->mMealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->setFavourite(Z)V

    .line 71
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableFoodListItem;->saveToDatabase()V

    .line 72
    return-void
.end method

.method public createSelectedPanelHolder()Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;
    .locals 7

    .prologue
    .line 92
    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableFoodListItem;->mChildItems:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 93
    .local v2, "list":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;>;"
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableFoodListItem;->mChildItems:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ChildFoodListItem;

    .line 94
    .local v0, "childItem":Lcom/sec/android/app/shealth/food/foodpick/expandable/ChildFoodListItem;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ChildFoodListItem;->getMealItemData()Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 96
    .end local v0    # "childItem":Lcom/sec/android/app/shealth/food/foodpick/expandable/ChildFoodListItem;
    :cond_0
    new-instance v3, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;

    iget-object v4, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableFoodListItem;->mMealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getId()J

    move-result-wide v4

    iget-object v6, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableFoodListItem;->mMealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v4, v5, v6, v2}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;-><init>(JLjava/lang/String;Ljava/util/List;)V

    return-object v3
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 121
    instance-of v2, p1, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableFoodListItem;

    if-nez v2, :cond_1

    .line 125
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 124
    check-cast v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableFoodListItem;

    .line 125
    .local v0, "that":Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableFoodListItem;
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableFoodListItem;->mMealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableFoodListItem;->mMealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    iget-object v3, v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableFoodListItem;->mMealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    iget-object v2, v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableFoodListItem;->mMealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    if-nez v2, :cond_0

    goto :goto_1
.end method

.method public getChildList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/expandable/ChildFoodListItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableFoodListItem;->mChildItems:Ljava/util/List;

    return-object v0
.end method

.method protected getDescription(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1
    .param p1, "resources"    # Landroid/content/res/Resources;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableFoodListItem;->mMealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getKcal()F

    move-result v0

    invoke-static {p1, v0}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->getCaloriesString(Landroid/content/res/Resources;F)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getListItemId()J
    .locals 2

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableFoodListItem;->mMealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getId()J

    move-result-wide v0

    return-wide v0
.end method

.method public getListItemType()Lcom/sec/android/app/shealth/food/foodpick/expandable/IListItem$ListItemType;
    .locals 1

    .prologue
    .line 111
    sget-object v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/IListItem$ListItemType;->MEAL:Lcom/sec/android/app/shealth/food/foodpick/expandable/IListItem$ListItemType;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableFoodListItem;->mMealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableFoodListItem;->mMealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableFoodListItem;->mMealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->hashCode()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected initExpandableIndicator(Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItemStateHolder;)Landroid/widget/ImageView;
    .locals 2
    .param p1, "state"    # Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItemStateHolder;

    .prologue
    .line 130
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->initExpandableIndicator(Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItemStateHolder;)Landroid/widget/ImageView;

    move-result-object v0

    .line 131
    .local v0, "indicator":Landroid/widget/ImageView;
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItemStateHolder;->isExpanded()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 132
    return-object v0
.end method

.method public isFavorite()Z
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableFoodListItem;->mMealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->isFavourite()Z

    move-result v0

    return v0
.end method

.method public removeFromFavorites()V
    .locals 3

    .prologue
    .line 76
    sget-object v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableFoodListItem;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "removed from favorite:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableFoodListItem;->mMealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableFoodListItem;->mMealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->setFavourite(Z)V

    .line 78
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableFoodListItem;->saveToDatabase()V

    .line 79
    return-void
.end method

.method public removeFromMyCategory()V
    .locals 3

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableFoodListItem;->mMealDao:Lcom/sec/android/app/shealth/food/fooddao/MealDao;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableFoodListItem;->getListItemId()J

    move-result-wide v1

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/shealth/food/fooddao/MealDao;->deleteDataById(J)Z

    .line 102
    return-void
.end method
