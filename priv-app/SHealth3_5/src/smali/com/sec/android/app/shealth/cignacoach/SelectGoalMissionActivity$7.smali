.class Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$7;
.super Ljava/lang/Object;
.source "SelectGoalMissionActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/cignacoach/listener/CheckedCheckBoxCountListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->setAdapter()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;)V
    .locals 0

    .prologue
    .line 391
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$7;->this$0:Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public checkedCheckBoxCountChanged(I)V
    .locals 2
    .param p1, "checkedCount"    # I

    .prologue
    .line 394
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$7;->this$0:Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;

    # invokes: Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->updateActionBar(I)V
    invoke-static {v0, p1}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->access$300(Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;I)V

    .line 396
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$7;->this$0:Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mTotalCount:I
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->access$800(Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;)I

    move-result v0

    if-ne p1, v0, :cond_0

    .line 397
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$7;->this$0:Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mSelectAllCheckBox:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->access$400(Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;)Landroid/widget/CheckBox;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 401
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$7;->this$0:Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->access$200(Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;)Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->notifyDataSetChanged()V

    .line 402
    return-void

    .line 399
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$7;->this$0:Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mSelectAllCheckBox:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->access$400(Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;)Landroid/widget/CheckBox;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0
.end method
