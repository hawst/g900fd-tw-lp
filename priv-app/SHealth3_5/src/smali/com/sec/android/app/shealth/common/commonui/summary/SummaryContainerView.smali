.class public abstract Lcom/sec/android/app/shealth/common/commonui/summary/SummaryContainerView;
.super Landroid/widget/LinearLayout;
.source "SummaryContainerView.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 30
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 31
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/commonui/summary/SummaryContainerView;->initialize()V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 42
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/commonui/summary/SummaryContainerView;->initialize()V

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 53
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 54
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/commonui/summary/SummaryContainerView;->initialize()V

    .line 55
    return-void
.end method

.method private initialize()V
    .locals 1

    .prologue
    .line 63
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/common/commonui/summary/SummaryContainerView;->setOrientation(I)V

    .line 64
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/summary/SummaryContainerView;->initializeContentView()V

    .line 65
    return-void
.end method


# virtual methods
.method protected abstract initializeContentView()V
.end method
