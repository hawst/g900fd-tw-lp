.class Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker$2;
.super Ljava/lang/Object;
.source "HDeviceIdChecker.java"

# interfaces
.implements Lcom/sec/android/service/health/samsungaccount/ISamsungUserTokenListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->onResponseReceived(JILjava/lang/Object;Ljava/lang/Object;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;)V
    .locals 0

    .prologue
    .line 286
    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker$2;->this$0:Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceived(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "userToken"    # Ljava/lang/String;
    .param p2, "userId"    # Ljava/lang/String;
    .param p3, "mcc"    # Ljava/lang/String;

    .prologue
    .line 304
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker$2;->this$0:Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;

    # setter for: Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->mAccountID:Ljava/lang/String;
    invoke-static {v0, p2}, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->access$202(Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;Ljava/lang/String;)Ljava/lang/String;

    .line 305
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker$2;->this$0:Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;

    # setter for: Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->mUserToken:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->access$302(Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;Ljava/lang/String;)Ljava/lang/String;

    .line 306
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker$2;->this$0:Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;

    # getter for: Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->access$000(Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "mUserToken ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker$2;->this$0:Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;

    # getter for: Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->mUserToken:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->access$300(Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  mAccountID="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker$2;->this$0:Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;

    # getter for: Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->mAccountID:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->access$200(Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mDeviceID="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker$2;->this$0:Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;

    # getter for: Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->mDeviceID:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->access$400(Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 307
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker$2;->this$0:Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;

    # invokes: Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->startTransaction()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->access$500(Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;)V

    .line 308
    return-void
.end method

.method public setFailureMessage(Ljava/lang/String;)V
    .locals 2
    .param p1, "errMsg"    # Ljava/lang/String;

    .prologue
    .line 290
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker$2;->this$0:Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;

    # getter for: Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->access$000(Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "SamsungAccountUserTokenManager setFailureMessage 2nd try"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 291
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker$2;->this$0:Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;

    # getter for: Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->mDeviceIdListener:Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker$IHDeviceIdListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->access$100(Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;)Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker$IHDeviceIdListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker$IHDeviceIdListener;->onAccountException(Ljava/lang/String;)V

    .line 292
    return-void
.end method

.method public setNetworkFailure()V
    .locals 2

    .prologue
    .line 297
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker$2;->this$0:Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;

    # getter for: Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->access$000(Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "SamsungAccountUserTokenManager setNetworkFailure 2nd try"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 298
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker$2;->this$0:Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;

    # getter for: Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->mDeviceIdListener:Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker$IHDeviceIdListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->access$100(Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;)Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker$IHDeviceIdListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker$IHDeviceIdListener;->onAccountNetworkError()V

    .line 299
    return-void
.end method
