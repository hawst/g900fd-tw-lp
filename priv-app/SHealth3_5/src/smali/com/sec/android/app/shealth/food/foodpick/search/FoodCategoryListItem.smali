.class public Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryListItem;
.super Ljava/lang/Object;
.source "FoodCategoryListItem.java"

# interfaces
.implements Lcom/sec/android/app/shealth/food/foodpick/search/frequent/IListItem;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryListItem$1;,
        Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryListItem$ViewHolder;
    }
.end annotation


# instance fields
.field private mTextOnlyData:Lcom/sec/android/app/shealth/food/foodpick/search/TextOnlyData;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/search/TextOnlyData;)V
    .locals 0
    .param p1, "category"    # Lcom/sec/android/app/shealth/food/foodpick/search/TextOnlyData;

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryListItem;->mTextOnlyData:Lcom/sec/android/app/shealth/food/foodpick/search/TextOnlyData;

    .line 37
    return-void
.end method


# virtual methods
.method public getData()Lcom/sec/android/app/shealth/food/foodpick/search/TextOnlyData;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryListItem;->mTextOnlyData:Lcom/sec/android/app/shealth/food/foodpick/search/TextOnlyData;

    return-object v0
.end method

.method public getView(Landroid/view/LayoutInflater;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v5, 0x0

    .line 42
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_1

    .line 43
    :cond_0
    const v2, 0x7f030103

    invoke-virtual {p1, v2, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 44
    new-instance v1, Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryListItem$ViewHolder;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryListItem$ViewHolder;-><init>(Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryListItem$1;)V

    .line 45
    .local v1, "holder":Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryListItem$ViewHolder;
    const v2, 0x7f080450

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v1, Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryListItem$ViewHolder;->nameText:Landroid/widget/TextView;

    .line 46
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 51
    :goto_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isSupportsBoohee()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryListItem;->mTextOnlyData:Lcom/sec/android/app/shealth/food/foodpick/search/TextOnlyData;

    invoke-interface {v2}, Lcom/sec/android/app/shealth/food/foodpick/search/TextOnlyData;->getText()Ljava/lang/String;

    move-result-object v2

    const-string v3, "BOOHEE"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 52
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryListItem;->mTextOnlyData:Lcom/sec/android/app/shealth/food/foodpick/search/TextOnlyData;

    invoke-interface {v2}, Lcom/sec/android/app/shealth/food/foodpick/search/TextOnlyData;->getText()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryListItem;->mTextOnlyData:Lcom/sec/android/app/shealth/food/foodpick/search/TextOnlyData;

    invoke-interface {v3}, Lcom/sec/android/app/shealth/food/foodpick/search/TextOnlyData;->getText()Ljava/lang/String;

    move-result-object v3

    const-string v4, "BOOHEE"

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v5, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 53
    .local v0, "booheeFoodName":Ljava/lang/String;
    iget-object v2, v1, Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryListItem$ViewHolder;->nameText:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 57
    .end local v0    # "booheeFoodName":Ljava/lang/String;
    :goto_1
    return-object p2

    .line 48
    .end local v1    # "holder":Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryListItem$ViewHolder;
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryListItem$ViewHolder;

    .restart local v1    # "holder":Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryListItem$ViewHolder;
    goto :goto_0

    .line 55
    :cond_2
    iget-object v2, v1, Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryListItem$ViewHolder;->nameText:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryListItem;->mTextOnlyData:Lcom/sec/android/app/shealth/food/foodpick/search/TextOnlyData;

    invoke-interface {v3}, Lcom/sec/android/app/shealth/food/foodpick/search/TextOnlyData;->getText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method
