.class public Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;
.super Landroid/view/View;
.source "ProfileThirdView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView$OnLevelListClickListener;
    }
.end annotation


# instance fields
.field private OnLevelListClickListener:Landroid/view/View$OnClickListener;

.field private lev_1:Landroid/widget/RelativeLayout;

.field private lev_2:Landroid/widget/RelativeLayout;

.field private lev_3:Landroid/widget/RelativeLayout;

.field private lev_4:Landroid/widget/RelativeLayout;

.field private lev_5:Landroid/widget/RelativeLayout;

.field private mActivityType:I

.field private mContext:Landroid/content/Context;

.field private mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

.field private mView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/sdk/health/content/ShealthProfile;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "shealthProfile"    # Lcom/samsung/android/sdk/health/content/ShealthProfile;

    .prologue
    const/4 v1, 0x0

    .line 49
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 38
    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->mView:Landroid/view/View;

    .line 39
    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->mContext:Landroid/content/Context;

    .line 41
    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    .line 50
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->mContext:Landroid/content/Context;

    .line 52
    const v0, 0x7f0301e8

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->mView:Landroid/view/View;

    .line 54
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->initLayout()V

    .line 56
    iput-object p2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    .line 57
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->setDataProfileValue()V

    .line 58
    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;
    .param p1, "x1"    # I

    .prologue
    .line 36
    iput p1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->mActivityType:I

    return p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;
    .param p1, "x1"    # I

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->initView(I)V

    return-void
.end method

.method private initLayout()V
    .locals 10

    .prologue
    const/4 v9, 0x3

    const v8, 0x7f090821

    const/4 v7, 0x2

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 66
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->mView:Landroid/view/View;

    const v1, 0x7f080863

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090819

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09020b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0901fd

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 68
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->mView:Landroid/view/View;

    const v1, 0x7f080867

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 69
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->mView:Landroid/view/View;

    const v1, 0x7f08086e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 70
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->mView:Landroid/view/View;

    const v1, 0x7f080875

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 71
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->mView:Landroid/view/View;

    const v1, 0x7f08087d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v5, [Ljava/lang/Object;

    const/4 v4, 0x4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 72
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->mView:Landroid/view/View;

    const v1, 0x7f080885

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v5, [Ljava/lang/Object;

    const/4 v4, 0x5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 77
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->mView:Landroid/view/View;

    const v1, 0x7f080870

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->mContext:Landroid/content/Context;

    const v3, 0x7f09082c

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 78
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->mView:Landroid/view/View;

    const v1, 0x7f080878

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->mContext:Landroid/content/Context;

    const v3, 0x7f09082c

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    const/4 v4, 0x5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 79
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->mView:Landroid/view/View;

    const v1, 0x7f080880

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->mContext:Landroid/content/Context;

    const v3, 0x7f09082c

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v7, [Ljava/lang/Object;

    const/4 v4, 0x6

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    const/4 v4, 0x7

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 81
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->mView:Landroid/view/View;

    const v1, 0x7f080864

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->lev_1:Landroid/widget/RelativeLayout;

    .line 82
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->mView:Landroid/view/View;

    const v1, 0x7f08086b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->lev_2:Landroid/widget/RelativeLayout;

    .line 83
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->mView:Landroid/view/View;

    const v1, 0x7f080872

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->lev_3:Landroid/widget/RelativeLayout;

    .line 84
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->mView:Landroid/view/View;

    const v1, 0x7f08087a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->lev_4:Landroid/widget/RelativeLayout;

    .line 85
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->mView:Landroid/view/View;

    const v1, 0x7f080882

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->lev_5:Landroid/widget/RelativeLayout;

    .line 87
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->lev_1:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView$OnLevelListClickListener;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView$OnLevelListClickListener;-><init>(Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 88
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->lev_2:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView$OnLevelListClickListener;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView$OnLevelListClickListener;-><init>(Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 89
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->lev_3:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView$OnLevelListClickListener;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView$OnLevelListClickListener;-><init>(Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 90
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->lev_4:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView$OnLevelListClickListener;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView$OnLevelListClickListener;-><init>(Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 91
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->lev_5:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView$OnLevelListClickListener;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView$OnLevelListClickListener;-><init>(Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 94
    const v0, 0x2bf22

    iput v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->mActivityType:I

    .line 95
    invoke-direct {p0, v7}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->initView(I)V

    .line 96
    return-void
.end method

.method private initView(I)V
    .locals 20
    .param p1, "lev"    # I

    .prologue
    .line 122
    if-nez p1, :cond_1

    .line 163
    :cond_0
    return-void

    .line 125
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    .line 131
    .local v5, "packName":Ljava/lang/String;
    const/4 v3, 0x1

    .local v3, "i":I
    :goto_0
    const/16 v17, 0x6

    move/from16 v0, v17

    if-ge v3, v0, :cond_0

    .line 132
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "lev"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "_image"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    const-string v19, "id"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v7

    .line 133
    .local v7, "resID_image":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "s_health_profilesetting_lev_"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "_nor"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    const-string v19, "drawable"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v8

    .line 134
    .local v8, "resID_image_nor":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "s_health_profilesetting_lev_"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "_sel"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    const-string v19, "drawable"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v9

    .line 135
    .local v9, "resID_image_sel":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "lev"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "_text"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    const-string v19, "id"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v12

    .line 136
    .local v12, "resID_text":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "lev"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "_title"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    const-string v19, "id"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v13

    .line 137
    .local v13, "resID_title":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "lev"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "_sub_title"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    const-string v19, "id"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v11

    .line 138
    .local v11, "resID_sub_title":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "lev"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "_radio_button"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    const-string v19, "id"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v10

    .line 140
    .local v10, "resID_radio":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->mView:Landroid/view/View;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    .line 141
    .local v4, "image":Landroid/widget/ImageView;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->mView:Landroid/view/View;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/TextView;

    .line 142
    .local v15, "text":Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->mView:Landroid/view/View;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/TextView;

    .line 143
    .local v16, "title":Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->mView:Landroid/view/View;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/TextView;

    .line 144
    .local v14, "sub_title":Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->mView:Landroid/view/View;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/RadioButton;

    .line 146
    .local v6, "radio":Landroid/widget/RadioButton;
    move/from16 v0, p1

    if-ne v3, v0, :cond_2

    .line 147
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v9}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 148
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    const v18, 0x7f070100

    invoke-virtual/range {v17 .. v18}, Landroid/content/res/Resources;->getColor(I)I

    move-result v17

    move/from16 v0, v17

    invoke-virtual {v15, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 149
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    const v18, 0x7f0205ea

    invoke-virtual/range {v17 .. v18}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 150
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    const v18, 0x7f07011a

    invoke-virtual/range {v17 .. v18}, Landroid/content/res/Resources;->getColor(I)I

    move-result v17

    invoke-virtual/range {v16 .. v17}, Landroid/widget/TextView;->setTextColor(I)V

    .line 151
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    const v18, 0x7f07011a

    invoke-virtual/range {v17 .. v18}, Landroid/content/res/Resources;->getColor(I)I

    move-result v17

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 152
    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v6, v0}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 131
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    .line 154
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 155
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    const v18, 0x7f070119

    invoke-virtual/range {v17 .. v18}, Landroid/content/res/Resources;->getColor(I)I

    move-result v17

    move/from16 v0, v17

    invoke-virtual {v15, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 156
    const/16 v17, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 157
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    const v18, 0x7f070119

    invoke-virtual/range {v17 .. v18}, Landroid/content/res/Resources;->getColor(I)I

    move-result v17

    invoke-virtual/range {v16 .. v17}, Landroid/widget/TextView;->setTextColor(I)V

    .line 158
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    const v18, 0x7f070119

    invoke-virtual/range {v17 .. v18}, Landroid/content/res/Resources;->getColor(I)I

    move-result v17

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 159
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v6, v0}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_1
.end method

.method private setDataProfileValue()V
    .locals 3

    .prologue
    .line 98
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getActivityType()I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 99
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getActivityType()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->mActivityType:I

    .line 100
    const/4 v0, 0x0

    .line 101
    .local v0, "lev":I
    iget v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->mActivityType:I

    packed-switch v1, :pswitch_data_0

    .line 118
    :goto_0
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->initView(I)V

    .line 120
    .end local v0    # "lev":I
    :cond_0
    return-void

    .line 103
    .restart local v0    # "lev":I
    :pswitch_0
    const/4 v0, 0x1

    .line 104
    goto :goto_0

    .line 106
    :pswitch_1
    const/4 v0, 0x2

    .line 107
    goto :goto_0

    .line 109
    :pswitch_2
    const/4 v0, 0x3

    .line 110
    goto :goto_0

    .line 112
    :pswitch_3
    const/4 v0, 0x4

    .line 113
    goto :goto_0

    .line 115
    :pswitch_4
    const/4 v0, 0x5

    goto :goto_0

    .line 101
    nop

    :pswitch_data_0
    .packed-switch 0x2bf21
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public getView()Landroid/view/View;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->mView:Landroid/view/View;

    return-object v0
.end method

.method public saveProfile()Z
    .locals 3

    .prologue
    .line 201
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    iget v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileThirdView;->mActivityType:I

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setActivityType(I)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 206
    const/4 v1, 0x1

    :goto_0
    return v1

    .line 202
    :catch_0
    move-exception v0

    .line 203
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v1, "ProfileThirdView"

    const-string v2, "Select Activity Type"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 204
    const/4 v1, 0x0

    goto :goto_0
.end method
