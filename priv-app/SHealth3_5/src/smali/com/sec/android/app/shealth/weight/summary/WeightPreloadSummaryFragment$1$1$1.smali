.class Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1$1$1;
.super Ljava/lang/Object;
.source "WeightPreloadSummaryFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1$1;->onReceived()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1$1;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1$1;)V
    .locals 0

    .prologue
    .line 122
    iput-object p1, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1$1$1;->this$2:Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 126
    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1$1$1;->this$2:Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1$1;

    iget-object v1, v1, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1$1;->this$1:Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1;

    iget-object v1, v1, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1;->this$0:Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->getLastWeightDataBefore(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    move-result-object v0

    .line 127
    .local v0, "lastSampleWeightData":Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;
    if-eqz v0, :cond_0

    .line 128
    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1$1$1;->this$2:Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1$1;

    iget-object v1, v1, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1$1;->this$1:Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1;

    iget-object v1, v1, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1;->this$0:Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;

    new-instance v2, Ljava/util/Date;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getSampleTime()J

    move-result-wide v3

    invoke-direct {v2, v3, v4}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->onDateChanged(Ljava/util/Date;)V

    .line 129
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1$1$1;->this$2:Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1$1;

    iget-object v1, v1, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1$1;->this$1:Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1;

    iget-object v1, v1, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1;->this$0:Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;

    # invokes: Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;
    invoke-static {v1}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->access$300(Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;)Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1$1$1;->this$2:Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1$1;

    iget-object v2, v2, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1$1;->this$1:Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1;

    iget-object v2, v2, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1;->this$0:Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->selectedDate:Ljava/util/Date;
    invoke-static {v2}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->access$200(Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;)Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setSelecteddate(Ljava/util/Date;)V

    .line 130
    return-void
.end method
