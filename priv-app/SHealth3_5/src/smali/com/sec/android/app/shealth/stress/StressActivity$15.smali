.class Lcom/sec/android/app/shealth/stress/StressActivity$15;
.super Ljava/lang/Object;
.source "StressActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/stress/StressActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/stress/StressActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/stress/StressActivity;)V
    .locals 0

    .prologue
    .line 720
    iput-object p1, p0, Lcom/sec/android/app/shealth/stress/StressActivity$15;->this$0:Lcom/sec/android/app/shealth/stress/StressActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onContentInitialization(Landroid/view/View;Landroid/app/Activity;Landroid/app/Dialog;Landroid/os/Bundle;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V
    .locals 8
    .param p1, "content"    # Landroid/view/View;
    .param p2, "parentActivity"    # Landroid/app/Activity;
    .param p3, "dialog"    # Landroid/app/Dialog;
    .param p4, "savedInstanceState"    # Landroid/os/Bundle;
    .param p5, "okButtonHandler"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;

    .prologue
    .line 727
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity$15;->this$0:Lcom/sec/android/app/shealth/stress/StressActivity;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    # invokes: Lcom/sec/android/app/shealth/stress/StressActivity;->infoDiagContentInitialization(Landroid/view/View;Landroid/app/Activity;Landroid/app/Dialog;Landroid/os/Bundle;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V
    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/shealth/stress/StressActivity;->access$1000(Lcom/sec/android/app/shealth/stress/StressActivity;Landroid/view/View;Landroid/app/Activity;Landroid/app/Dialog;Landroid/os/Bundle;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V

    .line 730
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity$15;->this$0:Lcom/sec/android/app/shealth/stress/StressActivity;

    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity$15;->this$0:Lcom/sec/android/app/shealth/stress/StressActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/stress/StressActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string/jumbo v2, "stm_information_dialog"

    invoke-virtual {v0, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    # setter for: Lcom/sec/android/app/shealth/stress/StressActivity;->mInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/stress/StressActivity;->access$1302(Lcom/sec/android/app/shealth/stress/StressActivity;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 731
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    move-object v7, v0

    check-cast v7, Landroid/view/View;

    .line 732
    .local v7, "vparent":Landroid/view/View;
    const v0, 0x7f0800b3

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/Button;

    .line 733
    .local v6, "okBtn":Landroid/widget/Button;
    new-instance v0, Lcom/sec/android/app/shealth/stress/StressActivity$15$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/stress/StressActivity$15$1;-><init>(Lcom/sec/android/app/shealth/stress/StressActivity$15;)V

    invoke-virtual {v6, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 740
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity$15;->this$0:Lcom/sec/android/app/shealth/stress/StressActivity;

    # getter for: Lcom/sec/android/app/shealth/stress/StressActivity;->mInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v0}, Lcom/sec/android/app/shealth/stress/StressActivity;->access$1300(Lcom/sec/android/app/shealth/stress/StressActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity$15;->this$0:Lcom/sec/android/app/shealth/stress/StressActivity;

    # getter for: Lcom/sec/android/app/shealth/stress/StressActivity;->mInfoDiagDismissHandler:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;
    invoke-static {v1}, Lcom/sec/android/app/shealth/stress/StressActivity;->access$1400(Lcom/sec/android/app/shealth/stress/StressActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->setOnDismissListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;)V

    .line 741
    return-void
.end method
